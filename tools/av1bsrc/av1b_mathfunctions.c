/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

abs(x) {
    return (x < 0) ? -x : x
}

Clip1(x) {
    return Clip3(0, (1 << BitDepth) - 1, x)
}

Clip3(x, y, z) {
    return (z < x) ? x : ((z > y) ? y : z)
}

Min(x, y) {
    return (x > y) ? y : x
}

Max(x, y) {
    return (x < y) ? y : x
}

int64 convert_to_int64(x) {
  return x
}

Round2(int64 x, n) {
    if (n == 0)
        return x
    return (x + (convert_to_int64(1) << (n-1))) >> n
}

Round2Signed(int64 x, n) {
    return x < 0 ? ( - Round2(-x, n) ) : Round2(x, n)
}
