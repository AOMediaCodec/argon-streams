/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_CDEF 1
#else
#define VALIDATE_SPEC_CDEF 0
#endif
#define VALIDATE_SPEC_INTERNAL_CDEF 0

int Cdef_Uv_Dir[ 2 ][ 2 ][ 8 ] = {
  { {0, 1, 2, 3, 4, 5, 6, 7},
    {1, 2, 2, 2, 3, 4, 6, 0} },
  { {7, 0, 2, 4, 5, 6, 6, 6},
    {0, 1, 2, 3, 4, 5, 6, 7} }
};

cdef( ) {
    step4 = Num_4x4_Blocks_Wide[ BLOCK_8X8 ]
    cdefSize4 = Num_4x4_Blocks_Wide[ BLOCK_64X64 ]
    cdefMask4 = ~(cdefSize4 - 1)
#if VALIDATE_SPEC_INTERNAL_CDEF
    for (r64 = 0; r64 < MiRows; r64 += cdefSize4) {
        for (c64 = 0; c64 < MiCols; c64 += cdefSize4) {
            for (r = r64; r < Min(r64+cdefSize4,MiRows); r += step4) {
                for (c = c64; c < Min(c64+cdefSize4,MiCols); c += step4) {
                      baseR = r & cdefMask4
                      baseC = c & cdefMask4
                      idx = cdef_idx[ baseR ][ baseC ]
                      cdef_block(r, c, idx)
                }
            }
        }
    }
#else
    for (r = 0; r < MiRows; r += step4) {
        for (c = 0; c < MiCols; c += step4) {
              baseR = r & cdefMask4
              baseC = c & cdefMask4
              idx = cdef_idx[ baseR ][ baseC ]
              cdef_block(r, c, idx)
        }
    }
#endif
#if VALIDATE_SPEC_CDEF
    if (!( CodedLossless || !enable_cdef || allow_intrabc)) {
        for ( plane = 0; plane < NumPlanes; plane++ ) {
            subX = ( plane == 0 ) ? 0 : subsampling_x
            subY = ( plane == 0 ) ? 0 : subsampling_y
            for ( i = 0; i < FrameHeight >> subY; i++) {
                for ( j = 0; j < FrameWidth >> subX; j++ ) {
                    validate(100001)
                    validate(plane)
                    validate(j)
                    validate(i)
                    validate( CdefFrame[ plane ][ i ][ j ] )
                }
            }
        }
    }
#endif
}

cdef_block( r, c, idx ) {
    int cdout[ 2 ]
    uint3 yDir
    uint3 dir

    startY = r * MI_SIZE
    endY = startY + MI_SIZE * 2
    startX = c * MI_SIZE
    endX = startX + MI_SIZE * 2
    for( y = startY; y < endY; y++ ) {
        for( x = startX; x < endX; x++ ) {
            CdefFrame[ 0 ][ y ][ x ] = CurrFrame[ 0 ][ y ][ x ]
        }
    }
    if ( COVERCLASS(1, (PROFILE0 || PROFILE2), NumPlanes > 1) ) {
      startY = startY >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
      endY = endY >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
      startX = startX >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
      endX = endX >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
      for( y = startY; y < endY; y++ ) {
          for( x = startX; x < endX; x++ ) {
              CdefFrame[ 1 ][ y ][ x ] = CurrFrame[ 1 ][ y ][ x ]
              CdefFrame[ 2 ][ y ][ x ] = CurrFrame[ 2 ][ y ][ x ]
          }
      }
    }
    if ( idx != -1 ) {
        coeffShift = BitDepth - 8
        skip = ( Skips[ r ][ c ] && Skips[ r + 1 ][ c ] && Skips[ r ][ c + 1 ] && Skips[ r + 1 ][ c + 1 ] )
#if VALIDATE_SPEC_INTERNAL_CDEF
        validate(100002)
        validate(r)
        validate(c)
        validate(skip)
#endif
        if ( skip == 0 ) {
            cdef_direction( r, c, cdout )
            COVERCLASS (0) {
               yDir = cdout[ 0 ]
               var = cdout[ 1 ]
            }

            COVERCROSS(VALUE_CDEF_DIRECTION, yDir)
            COVERCROSS(CROSS_CDEF_STRENGTH,
                       cdef_y_pri_strength[ idx ],
                       cdef_y_sec_strength[ idx ], CdefDamping)

            priStr = cdef_y_pri_strength[ idx ] << coeffShift
            secStr = cdef_y_sec_strength[ idx ] << coeffShift
            dir = ( priStr == 0 ) ? 0 : yDir
            varStr = ( var >> 6 ) ? Min( FloorLog2( var >> 6 ), 12) : 0
            priStr = ( var ? ( priStr * ( 4 + varStr ) + 8 ) >> 4 : 0 )
            damping = CdefDamping + coeffShift
            cdef_filter( 0, r, c, priStr, secStr, damping, dir )
            if ( COVERCLASS(1, (PROFILE0 || PROFILE2), NumPlanes > 1) ) {
                priStr = cdef_uv_pri_strength[ idx ] << coeffShift
                secStr = cdef_uv_sec_strength[ idx ] << coeffShift

                COVERCROSS(CROSS_CDEF_STRENGTH,
                           cdef_uv_pri_strength[ idx ],
                           cdef_uv_sec_strength[ idx ], CdefDamping)
#if COVER
                COVERCLASS(0) {
                    if (priStr != 0) {
                        COVERCROSS(CROSS_CDEF_UV_DIR,
                                   subsampling_x,
                                   subsampling_y, yDir)
                    }
                }
#endif // COVER
                dir = ( priStr == 0 ) ? 0 : Cdef_Uv_Dir[ subsampling_x ][ subsampling_y ][ yDir ]
                damping = CdefDamping + coeffShift - 1
                cdef_filter( 1, r, c, priStr, secStr, damping, dir )
                cdef_filter( 2, r, c, priStr, secStr, damping, dir )
            }
        }
    }
}

int Div_Table[9] = {
    0, 840, 420, 280, 210, 168, 140, 120, 105
};

cdef_direction( r, c, int32pointer out ) {
#if COVER
    uint_1_8 divTableIdx
#endif // COVER
    int cost[ 8 ]
    int partial[ 8 ][ 15 ]

    for (i = 0; i < 8; i++) {
        cost[i] = 0
        for (j = 0; j < 15; j++)
            partial[i][j] = 0
    }
    bestCost = 0
    out[ 0 ] = 0
    x0 = c << MI_SIZE_LOG2
    y0 = r << MI_SIZE_LOG2
    for (i = 0; i < 8; i++) {
        for (j = 0; j < 8; j++) {
            x = (CurrFrame[ 0 ][y0 + i][x0 + j] >> (BitDepth - 8)) - 128
            partial[0][i + j] += x // E-100 [RNG-CDEFDir0]
            partial[1][i + j / 2] += x // E-101 [RNG-CDEFDir1]
            partial[2][i] += x // E-102 [RNG-CDEFDir2]
            partial[3][3 + i - j / 2] += x // E-103 [RNG-CDEFDir3]
            partial[4][7 + i - j] += x // E-104 [RNG-CDEFDir4]
            partial[5][3 - i / 2 + j] += x // E-105 [RNG-CDEFDir5]
            partial[6][j] += x // E-106 [RNG-CDEFDir6]
            partial[7][i / 2 + j] += x // E-107 [RNG-CDEFDir7]
        }
    }
    for (i = 0; i < 8; i++) {
        cost[2] += partial[2][i] * partial[2][i] // E-108 [RNG-CDEFDir8]
        cost[6] += partial[6][i] * partial[6][i] // E-109 [RNG-CDEFDir9]
    }
#if COVER
    divTableIdx = 8
    COVERCROSS(VALUE_DIV_TABLE, divTableIdx)
#endif // COVER
    cost[2] *= Div_Table[8] // E-110 [RNG-CDEFDir10]
    cost[6] *= Div_Table[8] // E-111 [RNG-CDEFDir11]
    for (i = 0; i < 7; i++) {
#if COVER
        divTableIdx = i + 1
        COVERCROSS(VALUE_DIV_TABLE, divTableIdx)
#endif // COVER
        cost[0] += (partial[0][i] * partial[0][i] + partial[0][14 - i] * partial[0][14 - i]) * Div_Table[i + 1] // E-112 [RNG-CDEFDir12]
        cost[4] += (partial[4][i] * partial[4][i] + partial[4][14 - i] * partial[4][14 - i]) * Div_Table[i + 1] // E-113 [RNG-CDEFDir13]
    }
    cost[0] += partial[0][7] * partial[0][7] * Div_Table[8] // E-114 [RNG-CDEFDir14]
    cost[4] += partial[4][7] * partial[4][7] * Div_Table[8] // E-115 [RNG-CDEFDir15]
    for (i = 1; i < 8; i += 2) {
        for (j = 0; j < 4 + 1; j++) {
          cost[i] += partial[i][3 + j] * partial[i][3 + j] // E-116 [RNG-CDEFDir16]
        }
#if COVER
        divTableIdx = 8
        COVERCROSS(VALUE_DIV_TABLE, divTableIdx)
#endif // COVER
        cost[i] *= Div_Table[8] // E-117 [RNG-CDEFDir17]
        for (j = 0; j < 4 - 1; j++) {
#if COVER
            divTableIdx = 2 * j + 2
            COVERCROSS(VALUE_DIV_TABLE, divTableIdx)
#endif // COVER
            cost[i] += (partial[i][j] * partial[i][j] + partial[i][10 - j] * partial[i][10 - j]) * Div_Table[2 * j + 2] // E-118 [RNG-CDEFDir18]
        }
    }
    for (i = 0; i < 8; i++) {
        if (cost[i] > bestCost) {
          bestCost = cost[i]
          out[ 0 ] = i
        }
    }
    out[ 1 ] = (bestCost - cost[(out[ 0 ] + 4) & 7]) >> 10 // E-119 [RNG-CDEFDir19]
}

int Cdef_Pri_Taps[2][2] = {
    { 4, 2 }, { 3, 3 }
};

int Cdef_Sec_Taps[2][2] = {
    { 2, 1 }, { 2, 1 }
};

constrain(diff, threshold, damping) {
    if ( !threshold )
      return 0
    dampingAdj = Max(0, damping - FloorLog2( threshold ) )
    sign = (diff < 0) ? -1 : 1
    return sign * Clip3(0, Abs(diff), threshold - (Abs(diff) >> dampingAdj) )
}

int Cdef_Directions[8][2][2] = {
  { { -1, 1 }, { -2,  2 } },
  { {  0, 1 }, { -1,  2 } },
  { {  0, 1 }, {  0,  2 } },
  { {  0, 1 }, {  1,  2 } },
  { {  1, 1 }, {  2,  2 } },
  { {  1, 0 }, {  2,  1 } },
  { {  1, 0 }, {  2,  0 } },
  { {  1, 0 }, {  2, -1 } }
};

cdef_get_at(plane, x0, y0, i, j, uint_0_7 dir, uint1 k, sign, subX, subY) {
#if COVER
    uint1 cdef_get_at_idx
    COVERCLASS (0) {
        cdef_get_at_idx = 0
        COVERCROSS(CROSS_CDEF_DIRECTIONS, dir, k, cdef_get_at_idx)
        cdef_get_at_idx = 1
        COVERCROSS(CROSS_CDEF_DIRECTIONS, dir, k, cdef_get_at_idx)
    }
#endif // COVER
    y = y0 + i + sign * Cdef_Directions[dir][k][0]
    x = x0 + j + sign * Cdef_Directions[dir][k][1]

    candidateR = (y << subY) >> MI_SIZE_LOG2
    candidateC = (x << subX) >> MI_SIZE_LOG2
#if VALIDATE_SPEC_INTERNAL_CDEF
    validate(100005)
    validate(y0)
    validate(x0)
    validate(i)
    validate(j)
    validate(dir)
    validate(k)
    validate(sign)
    validate(Cdef_Directions[dir][k][0])
    validate(Cdef_Directions[dir][k][1])
    validate(y)
    validate(x)
#endif
    if ( is_inside_filter_region( candidateR, candidateC ) ) {
        CdefAvailable = 1
#if VALIDATE_SPEC_INTERNAL_CDEF
        validate(1)
#endif
        return CurrFrame[ plane ][ y ][ x ]
    } else {
        CdefAvailable = 0
        return 0
    }
}

cdef_filter( plane, r, c, priStr, secStr, damping, dir ) {
#if VALIDATE_SPEC_INTERNAL_CDEF
    validate(100003)
    validate(r)
    validate(c)
    validate(plane)
    validate(priStr)
    validate(secStr)
    validate(damping)
    validate(dir)
#endif
    MiColStart         = MiColStartGrid     [ r ][ c ]
    MiRowStart         = MiRowStartGrid     [ r ][ c ]
    MiColEnd           = MiColEndGrid       [ r ][ c ]
    MiRowEnd           = MiRowEndGrid       [ r ][ c ]
    coeffShift = BitDepth - 8
    subX = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) : 0)
    subY = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) : 0)
    x0 = (c * MI_SIZE ) >> subX
    y0 = (r * MI_SIZE ) >> subY
    w = 8 >> subX
    h = 8 >> subY
    for ( i = 0; i < h; i++ ) {
        for (j = 0; j < w; j++) {
            sum = 0
            x = CurrFrame[plane][y0 + i][x0 + j]
            max = x
            min = x
            for (k = 0; k < 2; k++) {
#if VALIDATE_SPEC_INTERNAL_CDEF
                for (sign = 1; sign >= -1; sign += -2) {
#else
                for (sign = -1; sign <= 1; sign += 2) {
#endif
                    pp = cdef_get_at(plane, x0, y0, i, j, dir, k, sign, subX, subY)
                    if ( CdefAvailable ) {
#if COVER
                        uint1 pri_1
                        pri_1 = (priStr >> coeffShift) & 1
                        uint1 K_1
                        K_1 = k
                        COVERCROSS(CROSS_CDEF_PRI_TAPS, pri_1, K_1)
#endif // COVER
                        sum += Cdef_Pri_Taps[(priStr >> coeffShift) & 1][k] * constrain(pp - x, priStr, damping) // E-120 [RNG-CDEF1]
                        max = Max(pp, max)
                        min = Min(pp, min)
                    }
#if VALIDATE_SPEC_INTERNAL_CDEF
                }
                for ( dirOff = 2; dirOff >= -2; dirOff -= 4) {
                    for (sign = 1; sign >= -1; sign += -2) {
#else
                    for ( dirOff = -2; dirOff <= 2; dirOff += 4) {
#endif
                        s = cdef_get_at(plane, x0, y0, i, j, (dir + dirOff) & 7, k, sign, subX, subY)
                        if ( CdefAvailable ) {
#if COVER
                            uint1 pri_2
                            pri_2 = (priStr >> coeffShift) & 1
                            uint1 K_2
                            K_2 = k
                            COVERCROSS(CROSS_CDEF_SEC_TAPS, pri_2, K_2)
#endif // COVER
                            sum += Cdef_Sec_Taps[(priStr >> coeffShift) & 1][k] * constrain(s - x, secStr, damping) // E-121 [RNG-CDEF2]
                            max = Max(s, max)
                            min = Min(s, min)
                        }
                    }
                }
            }
#if VALIDATE_SPEC_INTERNAL_CDEF
            validate(100004)
            validate(i)
            validate(j)
            validate(min)
            validate(max)
            validate(x)
            validate(sum)
#endif
            CdefFrame[plane][y0 + i][x0 + j] = Clip3(min, max, x + ((8 + sum - (sum < 0)) >> 4) ) // E-122 [RNG-CDEF3]
        }
    }
}
