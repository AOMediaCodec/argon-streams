/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_SPEC_CFL 0

predict_chroma_from_luma( plane, startX, startY, uint_0_18 txSz ) {
    int L[ 32 ][ 32 ]

#if VALIDATE_SPEC_CFL
    validate(60001)
#endif
    w = Tx_Width[ txSz ]
    h = Tx_Height[ txSz ]
    COVERCROSS(VALUE_TX_WIDTH, txSz)
    COVERCROSS(VALUE_TX_HEIGHT, txSz)
    subX = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
    subY = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)

    if ( plane == 1 ) alpha = CflAlphaU
    else alpha = CflAlphaV
#if VALIDATE_SPEC_CFL
    validate(alpha)
#endif
    lumaAvg = 0
    for ( i = 0; i < h; i++ ) {
        lumaY = (startY + i) << COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subY)
        lumaY = Min( lumaY, MaxLumaH - (1 << COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subY)) )
        for ( j = 0; j < w; j++ ) {
            lumaX = (startX + j) << COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subX)
            lumaX = Min( lumaX, MaxLumaW - (1 << COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subX)) )
            t = 0
            for ( dy = 0; dy <= COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subY); dy = COVERCLASS((PROFILE0 || PROFILE2), 1, dy) + 1 )
                for ( dx = 0; dx <= COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subX); dx = COVERCLASS((PROFILE0 || PROFILE2), 1, dx) + 1 )
                    t += CurrFrame[ 0 ][ lumaY + COVERCLASS((PROFILE0 || PROFILE2), 1, dy) ][ lumaX + COVERCLASS((PROFILE0 || PROFILE2), 1, dx) ] // E-54 [RNG-CfL1]
            v = t << ( 3 - COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subX) - COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subY) ) // E-55 [RNG-CfL2]
            L[ i ][ j ] = v
            lumaAvg += v // E-56 [RNG-CfL3]
        }
    }
#if VALIDATE_SPEC_CFL
    validate(lumaAvg)
#endif
    lumaAvg = Round2( lumaAvg, Tx_Width_Log2[ txSz ] + Tx_Height_Log2[ txSz ] ) // E-57 [RNG-CfL4]
    COVERCROSS(VALUE_TX_WIDTH_LOG2, txSz)
    COVERCROSS(VALUE_TX_HEIGHT_LOG2, txSz)
#if VALIDATE_SPEC_CFL
    validate(lumaAvg)
#endif

    for ( i = 0; i < h; i++ ) {
        for ( j = 0; j < w; j++ ) {
            dc = CurrFrame[ plane ][ startY + i ][ startX + j ]
            scaledLuma = Round2Signed( alpha * ( L[ i ][ j ] - lumaAvg ), 6 ) // E-58 [RNG-CfL5]
#if VALIDATE_SPEC_CFL
            validate(60002)
            validate(L[ i ][ j ])
            validate(dc)
            validate(scaledLuma)
#endif
            CurrFrame[ plane ][ startY + i ][ startX + j ] = Clip1( dc + scaledLuma ) // E-59 [RNG-CfL6]
#if VALIDATE_SPEC_CFL
            validate(CurrFrame[ plane ][ startY + i ][ startX + j ])
#endif
        }
    }
}

