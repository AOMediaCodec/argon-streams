/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

predict_palette( plane, startX, startY, x, y, uint_0_18 txSz ) {
    w = Tx_Width[ txSz ]
    h = Tx_Height[ txSz ]
    COVERCROSS(VALUE_TX_WIDTH, txSz)
    COVERCROSS(VALUE_TX_HEIGHT, txSz)

    if ( plane == 0 )
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ )
                CurrFrame[ plane ][ startY + i ][ startX + j ] = get_palette(plane, ColorMapY[ y * 4 + i ][ x * 4 + j ] )
    else
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ )
                CurrFrame[ plane ][ startY + i ][ startX + j ] = get_palette(plane, ColorMapUV[ y * 4 + i ][ x * 4 + j ] )
}

get_palette( plane, i ) {
    if ( plane == 0 ) return palette_colors_y[ i ]
    else if ( plane == 1 ) return palette_colors_u[ i ]
    else return palette_colors_v[ i ]
}
