/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define FIX_FOR_COMPARISON 1

// Default settings
#ifndef WRITE_YUV
#define WRITE_YUV 1
#endif

#ifndef USE_PROFILE
#define USE_PROFILE 0
#endif

// Maximum size of frames we support
// Allow up to 2x the level 6.3 limits on each of
// width, height, and area
// We also add a 128-pixel border on each side when calculating
// the maximum area, so that we don't have to worry about values
// wrapping around
#define MAX_WIDTH (16384*2)
#define MAX_HEIGHT (8704*2)
#define MAX_PX ((16384*2 + 128) * (2176*2 + 128))

#define REFS_PER_FRAME 7                                               // Number of reference frames that can be used for inter prediction
#define TOTAL_REFS_PER_FRAME 8                                         // Number of reference frame types (including intra type)
#define BLOCK_SIZE_GROUPS 4                                            // Number of contexts when decoding y_mode
#define BLOCK_SIZES 22                                                 // Number of different block sizes used
#define BLOCK_INVALID 22                                               // Sentinel value to mark partition choices that are illegal
#define MAX_SB_SIZE 128                                                // Maximum size of a superblock in pixels
#define MI_SIZE 4                                                      // Smallest size of a mode info block in pixels
#define MI_SIZE_LOG2 2                                                 // Base 2 logarithm of smallest size of a mode info block
#define MAX_TILE_WIDTH 4096                                            // Maximum width of a tile in units of luma samples
#define MAX_TILE_AREA 4096 * 2304                                      // Maximum area of a tile in units of luma samples
#define MAX_TILE_ROWS 64                                               // Maximum number of tile rows
#define MAX_TILE_COLS 64                                               // Maximum number of tile columns
#define INTRABC_DELAY_PIXELS 256                                       // Number of horizontal pixels before intra block copy can be used
#define INTRABC_DELAY_SB64 4                                           // Number of 64 by 64 blocks before intra block copy can be used
#define NUM_REF_FRAMES 8                                               // Number of frames that can be stored for future reference
#define IS_INTER_CONTEXTS 4                                            // Number of contexts for is_inter
#define REF_CONTEXTS 3                                                 // Number of contexts for `single_ref`, `comp_ref`, `comp_bwdref`, `uni_comp_ref`, `uni_comp_ref_p1` and `uni_comp_ref_p2`
#define MAX_SEGMENTS 8                                                 // Number of segments allowed in segmentation map
#define SEGMENT_ID_CONTEXTS 3                                          // Number of contexts for `segment_id`
#define SEG_LVL_ALT_Q 0                                                // Index for quantizer segment feature
#define SEG_LVL_ALT_LF_Y_V 1                                           // Index for vertical luma loop filter segment feature
#define SEG_LVL_ALT_LF_Y_H 2                                           // Index for horizontal luma loop filter segment feature
#define SEG_LVL_ALT_LF_U   3                                           // Index for U plane loop filter segment feature
#define SEG_LVL_ALT_LF_V   4                                           // Index for V plane loop filter segment feature
#define SEG_LVL_REF_FRAME  5                                           // Index for reference frame segment feature
#define SEG_LVL_SKIP       6                                           // Index for skip segment feature
#define SEG_LVL_GLOBALMV   7                                           // Index for global mv feature
#define SEG_LVL_MAX        8                                           // Number of segment features
#define TX_SIZE_CONTEXTS 3                                             // Number of contexts for transform size
#define INTERP_FILTERS 3                                               // Number of values for interp_filter
#define INTERP_FILTER_CONTEXTS 16                                      // Number of contexts for interp_filter
#define SKIP_MODE_CONTEXTS 3                                           // Number of contexts for decoding skip_mode
#define SKIP_CONTEXTS 3                                                // Number of contexts for decoding skip
#define PARTITION_CONTEXTS 4                                           // Number of contexts when decoding partition
#define TX_SIZES 5                                                     // Number of square transform sizes
#define TX_SIZES_ALL 19                                                // Number of transform sizes (including non-square sizes)
#define TX_MODES 3                                                     // Number of values for tx_mode
#define DCT_DCT 0                                                      // Inverse transform rows with DCT and columns with DCT
#define ADST_DCT 1                                                     // Inverse transform rows with DCT and columns with ADST
#define DCT_ADST 2                                                     // Inverse transform rows with ADST and columns with DCT
#define ADST_ADST 3                                                    // Inverse transform rows with ADST and columns with ADST
#define FLIPADST_DCT 4                                                 // Inverse transform rows with FLIPADST and columns with DCT
#define DCT_FLIPADST 5                                                 // Inverse transform rows with DCT and columns with FLIPADST
#define FLIPADST_FLIPADST 6                                            // Inverse transform rows with FLIPADST and columns with FLIPADST
#define ADST_FLIPADST 7                                                // Inverse transform rows with ADST and columns with FLIPADST
#define FLIPADST_ADST 8                                                // Inverse transform rows with FLIPADST and columns with ADST
#define IDTX 9                                                         // Inverse transform rows with identity and columns with identity
#define V_DCT 10                                                       // Inverse transform rows with identity and columns with DCT
#define H_DCT 11                                                       // Inverse transform rows with DCT and columns with identity
#define V_ADST 12                                                      // Inverse transform rows with identity and columns with ADST
#define H_ADST 13                                                      // Inverse transform rows with ADST and columns with identity
#define V_FLIPADST 14                                                  // Inverse transform rows with identity and columns with FLIPADST
#define H_FLIPADST 15                                                  // Inverse transform rows with FLIPADST and columns with identity
#define TX_TYPES 16                                                    // Number of inverse transform types
#define MB_MODE_COUNT 17                                               // Number of values for YMode
#define INTRA_MODES 13                                                 // Number of values for y_mode
#define UV_INTRA_MODES_CFL_NOT_ALLOWED 13                              // Number of values for `uv_mode` when chroma from luma is not allowed
#define UV_INTRA_MODES_CFL_ALLOWED 14                                  // Number of values for `uv_mode` when chroma from luma is allowed
#define COMPOUND_MODES 8                                               // Number of values for compound_mode
#define COMPOUND_MODE_CONTEXTS 8                                       // Number of contexts for compound_mode
#define COMP_NEWMV_CTXS 5                                              // Number of new mv values used when constructing context for `compound_mode`
#define NEW_MV_CONTEXTS 6                                              // Number of contexts for new_mv
#define ZERO_MV_CONTEXTS 2                                             // Number of contexts for zero_mv
#define REF_MV_CONTEXTS 6                                              // Number of contexts for ref_mv
#define DRL_MODE_CONTEXTS 3                                            // Number of contexts for drl_mode
#define MV_CONTEXTS 2                                                  // Number of contexts for decoding motion vectors
#define MV_INTRABC_CONTEXT 1                                           // Motion vector context used for intra block copy
#define MV_JOINTS 4                                                    // Number of values for mv_joint
#define MV_CLASSES 11                                                  // Number of values for mv_class
#define CLASS0_SIZE 2                                                  // Number of values for mv_class0_bit
#define MV_OFFSET_BITS 10                                              // Maximum number of bits for decoding motion vectors
#define MAX_LOOP_FILTER 63                                             // Maximum value used for loop filtering
#define REF_SCALE_SHIFT 14                                             // Number of bits of precision when scaling reference frames
#define SUBPEL_BITS 4                                                  // Number of bits of precision when choosing an inter prediction filter kernel
#define SUBPEL_MASK 15
#define SCALE_SUBPEL_BITS 10                                           // Number of bits of precision when computing inter prediction locations
#define MV_BORDER 128                                                  // Value used when clipping motion vectors
#define PALETTE_COLOR_CONTEXTS 5                                       // Number of values for color contexts
#define PALETTE_MAX_COLOR_CONTEXT_HASH 8                               // Number of mappings between color context hash and color context
#define PALETTE_BLOCK_SIZE_CONTEXTS 7                                  // Number of values for palette block size
#define PALETTE_Y_MODE_CONTEXTS 3                                      // Number of values for palette Y plane mode contexts
#define PALETTE_UV_MODE_CONTEXTS 2                                     // Number of values for palette U and V plane mode contexts
#define PALETTE_SIZES 7                                                // Number of values for palette_size
#define PALETTE_COLORS 8                                               // Number of values for palette_color
#define PALETTE_NUM_NEIGHBORS 3                                        // Number of neighbors considered within palette computation
#define DELTA_Q_SMALL 3                                                // Value indicating alternative encoding of quantizer index delta values
#define DELTA_LF_SMALL 3                                               // Value indicating alternative encoding of loop filter delta values
#define QM_TOTAL_SIZE 3344                                             // Number of values in the quantizer matrix
#define MAX_ANGLE_DELTA 3                                              // Maximum magnitude of AngleDeltaY and AngleDeltaUV
#define DIRECTIONAL_MODES 8                                            // Number of directional intra modes
#define ANGLE_STEP 3                                                   // Number of degrees of step per unit increase in AngleDeltaY or AngleDeltaUV.
#define TX_SET_TYPES_INTRA 3                                           // Number of intra transform set types
#define TX_SET_TYPES_INTER 4                                           // Number of inter transform set types
#define WARPEDMODEL_PREC_BITS 16                                       // Internal precision of warped motion models
#define IDENTITY 0                                                     // Warp model is just an identity transform
#define TRANSLATION 1                                                  // Warp model is a pure translation
#define ROTZOOM 2                                                      // Warp model is a rotation + symmetric zoom + translation
#define AFFINE 3                                                       // Warp model is a general affine transform
#define GM_ABS_TRANS_BITS 12                                           // Number of bits encoded for translational components of global motion models, if part of a ROTZOOM or AFFINE model
#define GM_ABS_TRANS_ONLY_BITS 9                                       // Number of bits encoded for translational components of global motion models, if part of a TRANSLATION model
#define GM_ABS_ALPHA_BITS 12                                           // Number of bits encoded for non-translational components of global motion models
#define DIV_LUT_PREC_BITS 14                                           // Number of fractional bits of entries in divisor lookup table
#define DIV_LUT_BITS 8                                                 // Number of fractional bits for lookup in divisor lookup table
#define DIV_LUT_NUM 257                                                // Number of entries in divisor lookup table
#define MOTION_MODES 3                                                 // Number of values for motion modes
#define SIMPLE 0                                                       // Use translation or global motion compensation
#define OBMC 1                                                         // Use overlapped block motion compensation
#define LOCALWARP 2                                                    // Use local warp motion compensation
#define LEAST_SQUARES_SAMPLES_MAX 8                                    // Largest number of samples used when computing a local warp
#define LS_MV_MAX 256                                                  // Largest motion vector difference to include in local warp computation
#define WARPEDMODEL_TRANS_CLAMP (1<<23)                                // Clamping value used for translation components of warp
#define WARPEDMODEL_NONDIAGAFFINE_CLAMP (1 << 13)                      // Clamping value used for matrix components of warp
#define GM_ALPHA_PREC_BITS 15                                          // Number of fractional bits for sending non-translational warp model coefficients
#define GM_TRANS_PREC_BITS 6                                           // Number of fractional bits for sending translational warp model coefficients
#define GM_TRANS_ONLY_PREC_BITS 3                                      // Number of fractional bits used for pure translational warps
#define INTERINTRA_MODES 4                                             // Number of inter intra modes
#define MASK_MASTER_SIZE 64                                            // Size of MasterMask array
#define SEGMENT_ID_PREDICTED_CONTEXTS 3                                // Number of contexts for segment_id_predicted
#define IS_INTER_CONTEXTS 4                                            // Number of contexts for is_inter
#define SKIP_CONTEXTS 3                                                // Number of contexts for skip
#define FWD_REFS 4                                                     // Number of syntax elements for forward reference frames
#define BWD_REFS 3                                                     // Number of syntax elements for backward reference frames
#define SINGLE_REFS 7                                                  // Number of syntax elements for single reference frames
#define UNIDIR_COMP_REFS 4                                             // Number of syntax elements for unidirectional compound reference frames
#define COMPOUND_TYPES 2                                               // Number of values for compound_type
#define CFL_JOINT_SIGNS 8                                              // Number of values for cfl_alpha_signs
#define CFL_ALPHABET_SIZE 16                                           // Number of values for cfl_alpha_u and cfl_alpha_v
#define COMP_INTER_CONTEXTS 5                                          // Number of contexts for comp_mode
#define COMP_REF_TYPE_CONTEXTS 5                                       // Number of contexts for comp_ref_type
#define CFL_ALPHA_CONTEXTS 6                                           // Number of contexts for cfl_alpha_u and cfl_alpha_v
#define INTRA_MODE_CONTEXTS 5                                          // Number of contexts for intra_frame_y_mode
#define COMP_GROUP_IDX_CONTEXTS 6                                      // Number of contexts for `comp_group_idx`
#define COMPOUND_IDX_CONTEXTS   6                                      // Number of contexts for `compound_idx`
#define INTRA_EDGE_KERNELS 3                                           // Number of filter kernels for the intra edge filter
#define INTRA_EDGE_TAPS 5                                              // Number of kernel taps for the intra edge filter
#define FRAME_LF_COUNT 4                                               // Number of loop filter strength values
#define MAX_VARTX_DEPTH 2                                              // Maximum depth for variable transform trees
#define TXFM_PARTITION_CONTEXTS 21                                     // Number of contexts for txfm_split
#define REF_CAT_LEVEL 640                                              // Bonus weight for close motion vectors
#define MAX_REF_MV_STACK_SIZE 8                                        // Maximum number of motion vectors in the stack
#define MFMV_STACK_SIZE 3                                              // Stack size for motion field motion vectors
#define MAX_TX_DEPTH 2                                                 // Number of contexts for tx_size when the maximum transform size is 8x8
#define WEDGE_TYPES 16                                                 // Number of directions for the wedge mask process
#define FILTER_BITS          7                                         // Number of bits used in Wiener filter coefficients
#define WIENER_COEFFS        3                                         // Number of Wiener filter coefficients to read
#define SGRPROJ_PARAMS_BITS  4                                         // Number of bits needed to specify self guided filter set
#define SGRPROJ_PRJ_SUBEXP_K 4                                         // Controls how self guided deltas are read
#define SGRPROJ_PRJ_BITS     7                                         // Precision bits during self guided restoration
#define SGRPROJ_RST_BITS     4                                         // Restoration precision bits generated higher than source before projection
#define SGRPROJ_MTABLE_BITS  20                                        // Precision of mtable division table
#define SGRPROJ_RECIP_BITS   12                                        // Precision of division by n table
#define SGRPROJ_SGR_BITS     8                                         // Internal precision bits for core selfguided_restoration
#define EC_PROB_SHIFT        6                                         // Number of bits to reduce CDF precision during arithmetic coding
#define EC_MIN_PROB          4                                         // Minimum probability assigned to each symbol during arithmetic coding
#define SELECT_SCREEN_CONTENT_TOOLS 2                                  // Value that indicates the allow_screen_content_tools syntax element is coded
#define SELECT_INTEGER_MV    2                                         // Value that indicates the force_integer_mv syntax element is coded
#define WARPEDPIXEL_PREC_SHIFTS (1<<6)                                   // Number of phases used in warped filtering
#define WARPEDDIFF_PREC_BITS    10                                     // Number of extra bits of precision in warped filtering
#define PLANE_TYPES 2                                                  // Number of different plane types (luma or chroma)
#define TXB_SKIP_CONTEXTS    13                                        // Number of contexts for `all_zero`
#define EOB_COEF_CONTEXTS    9                                         // Number of contexts for `eob_extra`
#define DC_SIGN_CONTEXTS     3                                         // Number of contexts for `dc_sign`
#define LEVEL_CONTEXTS       21                                        // Number of contexts for `coeff_br`
#define TX_CLASS_2D          0                                         // Transform class for transform types performing non-identity transforms in both directions
#define TX_CLASS_HORIZ       1                                         // Transform class for transforms performing only a horizontal non-identity transform
#define TX_CLASS_VERT        2                                         // Transform class for transforms performing only a vertical non-identity transform
#define REFMVS_LIMIT         (( 1 << 12 ) - 1)                           // Largest reference MV component that can be saved
#define INTRA_FILTER_SCALE_BITS 4                                      // Scaling shift for intra filtering process
#define INTRA_FILTER_MODES 5                                           // Number of types of intra filtering

#define RESTORE_NONE       0
#define RESTORE_SWITCHABLE 3
#define RESTORE_WIENER     1
#define RESTORE_SGRPROJ    2
#define RESTORATION_TILESIZE_MAX 256
#define MAX_FRAME_DISTANCE 31                                          // Maximum distance when computing weighted prediction
#define MAX_OFFSET_WIDTH 8
#define MAX_OFFSET_HEIGHT 0
#define WARP_PARAM_REDUCE_BITS  6
#define NUM_BASE_LEVELS         2                                      // Number of quantizer base levels
#define COEFF_BASE_RANGE        12                                     // The quantizer range above NUM_BASE_LEVELS above which the Exp-Golomb coding process is activated
#define BR_CDF_SIZE             4                                      // Number of contexts for `coeff_br`
#define SIG_COEF_CONTEXTS_EOB   4                                      // Number of contexts for `coeff_base_eob`
#define SIG_COEF_CONTEXTS_2D    26                                     // Context offset for `coeff_base` for horizontal-only or vertical-only transforms.
#define SIG_COEF_CONTEXTS       42                                     // Number of contexts for `coeff_base`
#define SIG_REF_DIFF_OFFSET_NUM 5                                      // Maximum number of context samples to be used in determining the context index for `coeff_base` and `coeff_base_eob`.
#define SUPERRES_NUM            8                                      // Numerator for upscaling ratio
#define SUPERRES_DENOM_MIN      9                                      // Smallest denominator for upscaling ratio
#define SUPERRES_DENOM_BITS     3                                      // Number of bits sent to specify denominator of upscaling ratio
#define SUPERRES_FILTER_BITS    6                                      // Number of bits of fractional precision during upscaling
#define SUPERRES_FILTER_SHIFTS  (1 << SUPERRES_FILTER_BITS)            // Number of phases of upscaling filters
#define SUPERRES_FILTER_TAPS 8                                         // Number of taps of upscaling filters
#define SUPERRES_FILTER_OFFSET 3                                       // Pixel offset for upscaling filters
#define SUPERRES_SCALE_BITS     14                                     // Number of fractional bits for computing initial position of upscaling filter
#define SUPERRES_SCALE_MASK     ((1 << 14) - 1)                          // Mask for computing initial position of upscaling filter
#define SUPERRES_EXTRA_BITS     8                                      // Difference in precision between SUPERRES_SCALE_BITS and SUPERRES_FILTER_BITS
#define COEFF_CDF_Q_CTXS        4                                      // Number of selectable context types for the coeff( ) syntax structure
#define PRIMARY_REF_NONE        7                                      // Value of `primary_ref_frame` indicating that there is no primary reference frame

#define OBU_SEQUENCE_HEADER        1
#define OBU_TEMPORAL_DELIMITER     2
#define OBU_FRAME_HEADER           3
#define OBU_TILE_GROUP             4
#define OBU_METADATA               5
#define OBU_FRAME                  6
#define OBU_REDUNDANT_FRAME_HEADER 7
#define OBU_TILE_LIST              8
#define OBU_PADDING                15

#define CP_BT_709       1  // BT.709
#define CP_UNSPECIFIED  2  // Unspecified
#define CP_BT_470_M     4  // BT.470 System M (historical)
#define CP_BT_470_B_G   5  // BT.470 System B, G (historical)
#define CP_BT_601       6  // BT.601
#define CP_SMPTE_240    7  // SMPTE 240
#define CP_GENERIC_FILM 8  // Generic film (color filters using illuminant C)
#define CP_BT_2020      9  // BT.2020, BT.2100
#define CP_XYZ          10 // SMPTE 428 (CIE 1921 XYZ)
#define CP_SMPTE_431    11 // SMPTE RP 431-2
#define CP_SMPTE_432    12 // SMPTE EG 432-1
#define CP_EBU_3213     22 // EBU Tech. 3213-E

#define TC_RESERVED_0     0  // For future use
#define TC_BT_709         1  // BT.709
#define TC_UNSPECIFIED    2  // Unspecified
#define TC_RESERVED_3     3  // For future use
#define TC_BT_470_M       4  // BT.470 System M (historical)
#define TC_BT_470_B_G     5  // BT.470 System B, G (historical)
#define TC_BT_601         6  // BT.601
#define TC_SMPTE_240      7  // SMPTE 240 M
#define TC_LINEAR         8  // Linear
#define TC_LOG_100        9  // Logarithmic (100 : 1 range)
#define TC_LOG_100_SQRT10 10 // Logarithmic (100 * Sqrt(10) : 1 range)
#define TC_IEC_61966      11 // IEC 61966-2-4
#define TC_BT_1361        12 // BT.1361
#define TC_SRGB           13 // sRGB or sYCC
#define TC_BT_2020_10_BIT 14 // BT.2020 10-bit systems
#define TC_BT_2020_12_BIT 15 // BT.2020 12-bit systems
#define TC_SMPTE_2084     16 // SMPTE ST 2084, ITU BT.2100 PQ
#define TC_SMPTE_428      17 // SMPTE ST 428
#define TC_HLG            18 // BT.2100 HLG, ARIB STD-B67

#define MC_IDENTITY    0  // Identity matrix
#define MC_BT_709      1  // BT.709
#define MC_UNSPECIFIED 2  // Unspecified
#define MC_RESERVED_3  3  // For future use
#define MC_FCC         4  // US FCC 73.628
#define MC_BT_470_B_G  5  // BT.470 System B, G (historical)
#define MC_BT_601      6  // BT.601
#define MC_SMPTE_240   7  // SMPTE 240 M
#define MC_SMPTE_YCGCO 8  // YCgCo
#define MC_BT_2020_NCL 9  // BT.2020 non-constant luminance, BT.2100 YCbCr
#define MC_BT_2020_CL  10 // BT.2020 constant luminance
#define MC_SMPTE_2085  11 // SMPTE ST 2085 YDzDx
#define MC_CHROMAT_NCL 12 // Chromaticity-derived non-constant luminance
#define MC_CHROMAT_CL  13 // Chromaticity-derived constant luminance
#define MC_ICTCP       14 // BT.2100 ICtCp

#define CSP_UNKNOWN   0  // Unknown (in this case the source video transfer function must be signaled outside the AV1 bitstream)
#define CSP_VERTICAL  1  // Horizontally co-located with (0, 0) luma sample, vertical position in the middle between two luma samples
#define CSP_COLOCATED 2  // co-located with (0, 0) luma sample
#define CSP_RESERVED  3

#define METADATA_TYPE_RESERVED_ZERO    0
#define METADATA_TYPE_HDR_CLL          1
#define METADATA_TYPE_HDR_MDCV         2
#define METADATA_TYPE_SCALABILITY      3
#define METADATA_TYPE_ITUT_T35         4
#define METADATA_TYPE_TIMECODE         5
#define METADATA_TYPE_ARGON_DESIGN_LST 30

#define SCALABILITY_L1T2  0
#define SCALABILITY_L1T3  1
#define SCALABILITY_L2T1  2
#define SCALABILITY_L2T2  3
#define SCALABILITY_L2T3  4
#define SCALABILITY_S2T1  5
#define SCALABILITY_S2T2  6
#define SCALABILITY_S2T3  7
#define SCALABILITY_L2T1h 8
#define SCALABILITY_L2T2h 9
#define SCALABILITY_L2T3h 10
#define SCALABILITY_S2T1h 11
#define SCALABILITY_S2T2h 12
#define SCALABILITY_S2T3h 13
#define SCALABILITY_SS    14
#define SCALABILITY_L3T1 15
#define SCALABILITY_L3T2 16
#define SCALABILITY_L3T3 17
#define SCALABILITY_S3T1 18
#define SCALABILITY_S3T2 19
#define SCALABILITY_S3T3 20
#define SCALABILITY_L3T2_KEY 21
#define SCALABILITY_L3T3_KEY 22
#define SCALABILITY_L4T5_KEY 23
#define SCALABILITY_L4T7_KEY 24
#define SCALABILITY_L3T2_KEY_SHIFT 25
#define SCALABILITY_L3T3_KEY_SHIFT 26
#define SCALABILITY_L4T5_KEY_SHIFT 27
#define SCALABILITY_L4T7_KEY_SHIFT 28

#define KEY_FRAME        0
#define INTER_FRAME      1
#define INTRA_ONLY_FRAME 2
#define SWITCH_FRAME     3

#define EIGHTTAP        0
#define EIGHTTAP_SMOOTH 1
#define EIGHTTAP_SHARP  2
#define BILINEAR        3
#define SWITCHABLE      4

#define ONLY_4X4        0
#define TX_MODE_LARGEST 1
#define TX_MODE_SELECT  2

#define FILTER_DC_PRED 0
#define FILTER_V_PRED 1
#define FILTER_H_PRED 2
#define FILTER_D157_PRED 3
#define FILTER_PAETH_PRED 4

#define SINGLE_REFERENCE      0
#define COMPOUND_REFERENCE    1

#define PARTITION_NONE   0
#define PARTITION_HORZ   1
#define PARTITION_VERT   2
#define PARTITION_SPLIT  3
#define PARTITION_HORZ_A 4
#define PARTITION_HORZ_B 5
#define PARTITION_VERT_A 6
#define PARTITION_VERT_B 7
#define PARTITION_HORZ_4 8
#define PARTITION_VERT_4 9

#define BLOCK_4X4       0
#define BLOCK_4X8       1
#define BLOCK_8X4       2
#define BLOCK_8X8       3
#define BLOCK_8X16      4
#define BLOCK_16X8      5
#define BLOCK_16X16     6
#define BLOCK_16X32     7
#define BLOCK_32X16     8
#define BLOCK_32X32     9
#define BLOCK_32X64    10
#define BLOCK_64X32    11
#define BLOCK_64X64    12
#define BLOCK_64X128   13
#define BLOCK_128X64   14
#define BLOCK_128X128  15
#define BLOCK_4X16     16
#define BLOCK_16X4     17
#define BLOCK_8X32     18
#define BLOCK_32X8     19
#define BLOCK_16X64    20
#define BLOCK_64X16    21

#define DC_PRED        0
#define V_PRED         1
#define H_PRED         2
#define D45_PRED       3
#define D135_PRED      4
#define D113_PRED      5
#define D157_PRED      6
#define D203_PRED      7
#define D67_PRED       8
#define SMOOTH_PRED    9
#define SMOOTH_V_PRED 10
#define SMOOTH_H_PRED 11
#define PAETH_PRED    12
#define UV_CFL_PRED   13

#define TX_4X4   0
#define TX_8X8   1
#define TX_16X16 2
#define TX_32X32 3
#define TX_64X64 4
#define TX_4X8   5
#define TX_8X4   6
#define TX_8X16  7
#define TX_16X8  8
#define TX_16X32 9
#define TX_32X16 10
#define TX_32X64 11
#define TX_64X32 12
#define TX_4X16  13
#define TX_16X4  14
#define TX_8X32  15
#define TX_32X8  16
#define TX_16X64 17
#define TX_64X16 18

#define TX_SET_DCTONLY 0
#define TX_SET_INTRA_1 1
#define TX_SET_INTRA_2 2
#define TX_SET_INTER_1 1
#define TX_SET_INTER_2 2
#define TX_SET_INTER_3 3

#define NEARESTMV         14
#define NEARMV            15
#define GLOBALMV          16
#define NEWMV             17
#define NEAREST_NEARESTMV 18
#define NEAR_NEARMV       19
#define NEAREST_NEWMV     20
#define NEW_NEARESTMV     21
#define NEAR_NEWMV        22
#define NEW_NEARMV        23
#define GLOBAL_GLOBALMV   24
#define NEW_NEWMV         25

#define UNIDIR_COMP_REFERENCE 0
#define BIDIR_COMP_REFERENCE  1

#define NONE         -1
#define INTRA_FRAME   0
#define LAST_FRAME    1
#define LAST2_FRAME   2
#define LAST3_FRAME   3
#define GOLDEN_FRAME  4
#define BWDREF_FRAME  5
#define ALTREF2_FRAME 6
#define ALTREF_FRAME  7

#define SIMPLE    0
#define OBMC      1
#define LOCALWARP 2

#define II_DC_PRED     0
#define II_V_PRED      1
#define II_H_PRED      2
#define II_SMOOTH_PRED 3

#define COMPOUND_WEDGE    0
#define COMPOUND_DIFFWTD  1
#define COMPOUND_AVERAGE  2
#define COMPOUND_INTRA    3
#define COMPOUND_DISTANCE 4

#define WEDGE_HORIZONTAL 0
#define WEDGE_VERTICAL   1
#define WEDGE_OBLIQUE27  2
#define WEDGE_OBLIQUE63  3
#define WEDGE_OBLIQUE117 4
#define WEDGE_OBLIQUE153 5

#define UNIFORM_45     0
#define UNIFORM_45_INV 1

#define MV_JOINT_ZERO   0
#define MV_JOINT_HNZVZ  1
#define MV_JOINT_HZVNZ  2
#define MV_JOINT_HNZVNZ 3

#define MV_CLASS_0  0
#define MV_CLASS_1  1
#define MV_CLASS_2  2
#define MV_CLASS_3  3
#define MV_CLASS_4  4
#define MV_CLASS_5  5
#define MV_CLASS_6  6
#define MV_CLASS_7  7
#define MV_CLASS_8  8
#define MV_CLASS_9  9
#define MV_CLASS_10 10

#define DCT_VAL_CATEGORY1 5
#define DCT_VAL_CATEGORY2 6
#define DCT_VAL_CATEGORY3 7
#define DCT_VAL_CATEGORY4 8
#define DCT_VAL_CATEGORY5 9
#define DCT_VAL_CATEGORY6 10

#define CFL_SIGN_ZERO 0
#define CFL_SIGN_NEG  1
#define CFL_SIGN_POS  2

#define CONFIG_COLORSPACE_HEADERS 0

#define VALIDATE_SPEC_USUAL_SUSPECTS 0

#define MAX_MI_IN_FRAME ((MAX_PX / (MI_SIZE * MI_SIZE)) + ((MAX_SB_SIZE/MI_SIZE)*(MAX_HEIGHT/MI_SIZE)))
#define MAX_LR_UNITS_IN_FRAME ((MAX_PX + (64*64) - 1) / (64*64))


#define PROFILES_AND_LEVELS_Level_Major 0
#define PROFILES_AND_LEVELS_Level_Minor 1
#define PROFILES_AND_LEVELS_MaxPicSize 2
#define PROFILES_AND_LEVELS_MaxHSize 3
#define PROFILES_AND_LEVELS_MaxVSize 4
#define PROFILES_AND_LEVELS_MaxDisplayRate 5
#define PROFILES_AND_LEVELS_MaxDecodeRate 6
#define PROFILES_AND_LEVELS_MaxHeaderRate 7
#define PROFILES_AND_LEVELS_MainMbps_x10 8
#define PROFILES_AND_LEVELS_HighMbps_x10 9
#define PROFILES_AND_LEVELS_MinCompBasisMain 10
#define PROFILES_AND_LEVELS_MinCompBasisHigh 11
#define PROFILES_AND_LEVELS_MaxTiles 12
#define PROFILES_AND_LEVELS_MaxTileCols 13
#define PROFILES_AND_LEVELS_Count 14
#define PROFILES_AND_LEVELS_Num_Valid_Levels 14

#define COMP_BASIS_INVALID (1<<31) /* Something so big it shouldn't be possible to achieve */

#define MAX_SPATIAL_ID 3

#define MAX_SEQUENCE_HEADER_SIZE 512
#define MAX_SCALABILITY_STRUCTURE_SIZE 2062
