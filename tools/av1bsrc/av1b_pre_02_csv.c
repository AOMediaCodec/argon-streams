/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define CSV_NEWCVS -1
enum {
  //Insert new entries here
  CSV_ENTRY_COUNT
  //Don't insert anything here!
};

#define MAX_CSV_VALUES 5

void CSV(int id, ...);
