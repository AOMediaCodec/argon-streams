/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#line 8 "av1bsrc/av1b_base.cpp"

#include <math.h>
#include <libgen.h>

char *realpath(const char *path, char *resolved_path);

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>

// set this to zero to always output the bpp in the first frame
int outputBppPerFrame=1;

const char *current_file;

void readFileList(const char *fileListFile);

//int readFileList(const char *fileListFile, const char *** streamv_out, int *streamc_out);
//void cleanupFileList(const char **streamv, int streamc);

void init_profile(void);

#define MAX(a,b) ((a)>(b)?(a):(b))

#if WRITE_YUV
FILE *fid_yuv=NULL;
void write_yuv_init(const char* filename) {
  fid_yuv = fopen(filename,"wb");
  assert(fid_yuv);
}

void write_yuv_exit(void) {
  assert(fid_yuv);
  fclose(fid_yuv);
}

static void write_yuv_plane (FILE *file,
                             const uint16_t *samples,
                             int width, int height, int stride,
                             int lshift, unsigned offset, unsigned numbytes)
{
    assert (numbytes <= 2);

    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            uint16_t sample = samples [stride * y + x];
            uint16_t shifted = ((lshift >= 0) ?
                                (sample << lshift) :
                                (sample >> (- lshift)));
            uint16_t data = shifted + offset;

            fwrite (& data, sizeof(uint8), numbytes, file);
        }
    }
}

void write_yuv (const uint16 *yp, const uint16 *up, const uint16 *vp,
                int width, int height, int stride,
                int bps, int numbytes, int sw, int sh, int mono)
{
    assert (fid_yuv);
    assert (numbytes <= 2);

    static int write_yuv_bits = 0;

    if (outputBppPerFrame || ! write_yuv_bits) {
        write_yuv_bits = bps;
    }

    int      lshift = write_yuv_bits - bps;
    unsigned offset = (lshift > 0) ? (1 << (lshift - 1)) - 1 : 0;

    write_yuv_plane (fid_yuv, yp, width, height, stride,
                     lshift, offset, numbytes);

    if (! mono) {
        int w_uv = (width + sw - 1) / sw;
        int h_uv = (height + sh - 1) / sh;

        write_yuv_plane (fid_yuv, up, w_uv, h_uv, stride,
                         lshift, offset, numbytes);
        write_yuv_plane (fid_yuv, vp, w_uv, h_uv, stride,
                         lshift, offset, numbytes);
    }

    fflush(fid_yuv);
}

#endif

#if !COVER
void estimate_command(TOPLEVEL_T *b, const InputFile &file, bool useAnnexB) {

  // The reference command should be based on the basename of the
  // file, not its entire path.
  std::string::size_type last_slash = file.path.find_last_of ('/');
  const char *basename = file.path.c_str ();
  if (last_slash != std::string::npos) {
    basename = file.path.c_str () + last_slash + 1;
  }

  if (b->global_data->num_large_scale_tile_tile_list_obus) {
    printf("reference decoder command: "
           "\"$LIBAV1/examples/lightfield_tile_list_decoder\" "
           "\"$ARGON_STREAMS_INPUT/%s\" "
           "\"$ARGON_STREAMS_OUTPUT/out.yuv\" "
           "%d %d\n",
           basename,
           b->global_data->num_large_scale_tile_anchor_frames,
           b->global_data->num_large_scale_tile_tile_list_obus);
  } else {
    printf("reference decoder command: "
           "\"$LIBAV1/aomdec\" "
           "\"$ARGON_STREAMS_INPUT/%s\" "
           "--rawvideo "
           "-o \"$ARGON_STREAMS_OUTPUT/out.yuv\"",
           basename);

    if (useAnnexB) {
      printf(" --annexb");
    }

    if (b->global_data->DesiredOpPoint) {
      printf(" --oppoint=%u", b->global_data->DesiredOpPoint);
    }

    // Note that --all-layers is orthogonal to the operating point.
    // The operating point decides which frames to read. The
    // --all-layers flag says to output all the applicable frames in a
    // temporal unit. If it isn't set then aomdec just writes out the
    // last one.
    printf(" --all-layers");

    printf("\n");
  }
}
#endif // !COVER

int h265_find_startcode(uint8 *data,int start,int len)
{
  int numzeros=0;
  while(start<len) {
    uint8 c = data[start];
    if (c==0)
      numzeros++;
    else if (c==1 && numzeros>=2)
      return start-2;
    else
      numzeros=0;
    start++;
  }
  return start; // No startcode found
}

void *vp9_alloc_align32(const char *name, size_t sz) {
  void *bRaw=malloc(sz+31);
  if (!bRaw) {
    printf("Couldn't allocate memory for %s! (requested %llu bytes)\n",name,(unsigned long long) (sz+31));

    for (size_t sz2=sz+30; !bRaw; sz2-=1024) {
      bRaw=malloc(sz2);
      if (bRaw) {
        printf("Largest possible allocation: %llu\n",(unsigned long long) sz2);
        free(bRaw);
      }
    }
    return NULL;
  }
  return bRaw;
}

// This is a wrapper around a TOPLEVEL_T and a GLOBAL_DATA_T using
// unique_ptr (which means you don't have to do manual deletion). Note
// that the big structures get allocated on the heap, so you can make
// a safe_toplevel_t on the stack without an explosion.
struct safe_toplevel_t
{
    std::unique_ptr<TOPLEVEL_T> b;
    std::unique_ptr<GLOBAL_DATA_T> g;

    safe_toplevel_t (const CommandLine &params,
                     const InputFile   &file,
                     uint8             *data,
                     int                len)
        : b (new TOPLEVEL_T ())
        , g (new GLOBAL_DATA_T ())
    {
        b->global_data = g.get ();

        g->max_num_oppoints = params.max_num_oppoints;
        g->TargetDisplayFrameRate = params.display_frame_rate;

#if WRITE_YUV
        g->outputBytes = params.output_bytes;
#endif
        g->DesiredOpPoint = params.operating_point;

        hevc_parse_global_data (b.get (), g.get (),
                                file.numLargeScaleTileAnchorFrames,
                                file.numLargeScaleTileTileListObus,
                                params.override_no_film_grain);

        bitstream_init (b.get (), data, len);
    }

    TOPLEVEL_T *get ()
    {
        return b.get ();
    }
};

void av1_decode_obus(TOPLEVEL_T *b,
                     const InputFile &file, int fcount, int ftotal,
                     int len, int frameLimit)
{
  if (file.annexB) {
    int sz=hevc_parse_bitstream(b, NULL, len, frameLimit);
    (void)sz;
  } else {
    for(int obu=0;(frameLimit==-1 || b->global_data->OutputFrameCount<frameLimit);obu++) {
#if PRINT_BIT_READS
      printf("Calling open_bitstream_unit %d | %d\n", bitstream_current_position(b), len);
#endif
      int sz=hevc_parse_open_bitstream_unit(b, NULL, 0);
      (void)sz;
#if PRINT_BIT_READS
      printf("Finished open_bitstream_unit %d | %d\n", bitstream_current_position(b), len);
#endif
#if !COVER
      if (b->global_data->estimateCmd) {
        break;
      }
#endif // !COVER
      //printf(".%d %d %d\n",sz,bitstream_current_position(b),len);
      if (bitstream_current_position(b)>=len)
        break;
    }
  }
#if !COVER
  if (b->global_data->estimateCmd) {
    estimate_command(b, file, file.annexB);
  }

#endif // !COVER
  printf("Processed file #%d: %s                                      \n",
         fcount, file.path.c_str ());
}

static size_t
av1_read_file (const CommandLine &params, unsigned file_idx)
{
  const InputFile &file = params.input_files.at (file_idx);
  const size_t ftotal = params.input_files.size ();

  const char *path = file.path.c_str ();

  uint8 *data;
  printf("Opening %s%s", path, debugMode ? "\n" : "\r");
  FILE *fid = fopen(path, "rb");

  if (!fid) {
    printf("Couldn't open input file `%s'.\n", path);
    exit(1);
  }
  fseek(fid,0,SEEK_END);
  int len = ftell(fid);

  data = (uint8 *)malloc(len);
  assert(data);
  fseek(fid,0,SEEK_SET);
  size_t bytes = fread(data,sizeof(uint8),len,fid);
#if PRINT_BIT_READS
  printf("Read %d bytes / %d bytes\n", (int)bytes, len);
#endif
  (void) bytes;

  CheckDumpInfo cd_info (params.dump_base, params.dump_flags,
                         params.check_base, params.check_flags,
                         file_idx);

  g_cd_info = & cd_info;
  current_file = path;

  safe_toplevel_t toplevel (params, file, data, len);

  TOPLEVEL_T *b = toplevel.get ();
#if !COVER
  b->global_data->estimateCmd = params.estimate_command ? 1 : 0;
#endif
  av1_decode_obus (b, file, file_idx, ftotal, len, params.frame_limit);

  if (b->global_data->max_num_oppoints >= 0) {
     printf("Max num oppoints: %d\n", b->global_data->max_num_oppoints);
  }

  current_file = NULL;
  g_cd_info = NULL;

  fclose(fid);
  free(data);

  return len;
}

#define path_to_ref "../../../../work/hevc/HM-9.1rc1/bin/"
FILE *ref_log=NULL;
FILE *val_fid=NULL;
int val_count=0;
int val_critical=-1;

void init_validation(void) {
    FILE *fid = fopen("validate_critical.txt", "r");
    int numread;
    if (!fid) fid = fopen("../validate_critical.txt", "r");
    if (!fid) return;
    numread=fscanf(fid, "%d", &val_critical);
    assert(numread==1);
    printf("Read val_critical = %d\n", val_critical);
    fclose(fid);
}

void log_init(void) {
  ref_log=fopen(path_to_ref "ref_bins.txt","rb");
}

void decode_log(int r,int off,int s,int v) {
  char lbuf[256];
  char ref_buf[256];
  //static int count=0;
  //printf("Check%d\n",count++);
  sprintf(lbuf,"%d %d %d %d",r,off,s,v);
  assert(ref_log);
  char *ch = fgets(ref_buf,256,ref_log);
  (void) ch;
  if (strncmp(lbuf,ref_buf,strlen(lbuf))==0)
    return;
  if (debugMode) printf("Ref=%s\nOurs=%s\n",ref_buf,lbuf);
  assert(0);
}

void validate_stop(void) {
  if (debugMode) printf("Stop");
}

void validate(int x) {
  if (!val_fid) {
    val_fid = fopen("validate_c_spec.txt","wb");
    assert(val_fid);
  }
  fprintf(val_fid,"%d %d\n",val_count,x);
  if (val_count==val_critical) {
    validate_stop();
  }
  //fflush(val_fid);
  val_count++;
}

const char *argon_basename(const char *p) {
  const char *pp1;
  const char *pp2=p;
  for (pp1=p; *pp1; pp1++) {
    if ( (*pp1=='/' || *pp1=='\\') && pp1[1]) pp2=pp1+1;
  }
  return pp2;
}

const char *argon_dirname(char *p) {
  char *pp1;
  char *pp2=NULL;
  for (pp1=p; *pp1; pp1++) {
    if ((*pp1=='/' || *pp1=='\\') && pp1[1]) pp2=pp1;
  }
  if (!pp2) return "";
  *pp2='\0';
  return p;
}

char streamType[1024];
char fullPathName[32768];

#if USE_DB
int profile_init_db(const char *dbURL);
#endif

int main (int argc, char *argv[])
{
    log_init();

    // Parse parameters, calling exit on error.
    CommandLine params (argc, argv);
    if (params.verbose) {
      debugMode = 1;
    }

#if USE_PROFILE
    init_profile ();
#endif

    init_validation ();

#if WRITE_YUV
    write_yuv_init (params.yuv_output_file_name);
#endif

#if USE_PROFILE && USE_DB
    if (params.db_init)
        return profile_init_db (params.db_url);
#endif

    if (params.max_num_oppoints >= 0 &&
        params.input_files.size () != 1) {
       printf("Cannot use --max-num-oppoints switch with multiple streams\n");
       return 1;
    }

    size_t last_file_size = 0;
    for (unsigned i = 0; i < params.input_files.size (); ++ i) {
        last_file_size = av1_read_file (params, i);
    }

#if WRITE_YUV
    write_yuv_exit();
#endif

  // If we failed any checks, stop here with error code 2.
  if (some_check_failed) {
    fflush (stdout);
    fprintf (stderr, "One or more checks failed. Exiting with status 2.\n");
    return 2;
  }

#if USE_PROFILE
    if (params.input_files.size ()) {
        profile_report();
        profile_output(params.prof_xml_file,
                       params.prof_py_file,
                       params.input_files [0].path.c_str (),
# if USE_DB
                       params.db_url, params.db_subset, params.db_seed,
# endif
                       last_file_size,
                       params.prof_output_all_streams);
    }
#endif

    return 0;
}

/* Called when a frame is output from the decoded picture buffer.
These will be returned in decode order. */
void video_output (TOPLEVEL_T *b, int bps,
                   int w, int h, int sw, int sh, int mono)
{
#if WRITE_YUV
    const flat_array<uint16, 2> *planes[3];
    planes[0] = & b->global_data->OutY;
    planes[1] = & b->global_data->OutU;
    planes[2] = & b->global_data->OutV;

    size_t stride = planes[0]->lengths ()[1];
    for (unsigned i = 1; i < (mono ? 1 : 3); ++ i) {
        assert (planes[i]->lengths ()[1] == stride);
    }

    if (outputBppPerFrame || b->global_data->outputBytes == 0) {
        // To follow the reference decoder we pick a bits per pixel
        // based on the first output frame. (LibVideoIO)
        b->global_data->outputBytes = (bps > 8) ? 2 : 1;
    }

    write_yuv (planes [0]->data (),
               planes [1]->data (),
               planes [2]->data (),
               w, h, stride,
               bps, b->global_data->outputBytes,
               sw, sh, mono);
#endif

    if (debugMode) {
        printf ("Frame %d (Y%d UV%d) (post-crop size: %dx%d)\n",
                b->global_data->OutputFrameCount, bps, bps, w, h);

        // This flush is useful if you're writing to some log file
        // with output redirection: we wouldn't be line buffered
        // otherwise.
        fflush (stdout);

#if USE_PROFILE
        profile_report();
#endif
    }
}

void hevc_parse_record(void *b, void *b2, int id, int num, ... ) {
  static FILE *record_fid=NULL;
  if (!record_fid) {
    record_fid = fopen("record.txt","wb");
  }
  assert(record_fid);
  va_list args;
  va_start(args, num);
  int i;
  fprintf(record_fid,"\n%d %d ",id, num);
  for(i=0;i<num;i++) {
    int p = va_arg(args,int);
    fprintf(record_fid,"%d ",p);
  }
  va_end(args);
}
