/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#line 8 "av1bsrc/av1b_base_profile.c"

#include "range_equations.h"

#if USE_PROFILE
#if USE_DB
#include <soci/soci.h>
#include <soci/sqlite3/soci-sqlite3.h>
#endif

#include <set>

struct SProfileCoveringStream {
  const char *name;
  // bit 0==covered zero
  // bit 1==covered any other value
  // bit 2==covered minimum
  // bit 3==covered maximum
  char cover;
  struct SProfileCoveringStream *next;
};

typedef struct SProfileCoveringStream TProfileCoveringStream;

typedef struct PROFILE_ENTRY_T {

  long long               min;
  long long               max;
  TProfileCoveringStream *files;

} profile_entry_t;

static profile_entry_t profile_table [PROFILE_POINTS_TABLE_SIZE];

void init_profile() {
  ALL_PROFILER_POINTS_UNUSED;
  memset (profile_table, 0, sizeof profile_table);

  for (unsigned i = 0; i < PROFILE_POINTS_TABLE_SIZE; ++ i) {
    profile_table[i].min = LLONG_MAX;
    profile_table[i].max = LLONG_MIN;
  }
}

int profile_enabled = 1;

void profile_enable_func() {
  profile_enabled = 1;
}

void profile_disable_func() {
  profile_enabled = 0;
}

// Update coverage of a profiling point with the given value.
//
// This only gets called when the value is "interesting", which
// doesn't happen very often. As a result, we force it not to be
// inlined, making sure that the compiler doesn't allocate stack space
// in profile_func for this.
static void __attribute__ ((noinline))
update_profile_coverage (profile_entry_t &entry, long long val)
{
  TProfileCoveringStream *cs = entry.files;
  if (! (cs  && cs->name == current_file)) {
    cs = (TProfileCoveringStream *) malloc (sizeof (TProfileCoveringStream));
    assert (cs);

    cs->name = current_file;
    cs->cover = 0;
    cs->next = entry.files;

    entry.files = cs;
  }

  bool is_zero = val == 0;
  bool is_min = val == entry.min;
  bool is_max = val == entry.max;

  char cover = ((is_zero ? 1 : 2) |
                (is_min  ? 4 : 0) |
                (is_max  ? 8 : 0));

  cs->cover |= cover;
}

long long profile_func(unsigned idx, long long val)
{
  if (! profile_enabled) return val;

  profile_entry_t &entry = profile_table [idx];
  bool this_point_useful = false;

  if (__builtin_expect (! entry.files, 0)) {
    // This is the first time we've got to this entry.
    entry.min = val;
    entry.max = val;
    this_point_useful = true;
  } else {
    // We've been here before. Have we got a new min or max?
    if (__builtin_expect (val < entry.min, 0)) {
      entry.min = val;
      this_point_useful = true;
    }
    if (__builtin_expect (val > entry.max, 0)) {
      entry.max = val;
      this_point_useful = true;
    }
  }

  if (__builtin_expect (this_point_useful, 0)) {
    update_profile_coverage (entry, val);
  }

  return val;
}

void profile_report(void)
{
  unsigned count = 0;
  for (int i = 0; i < PROFILE_POINTS_TABLE_SIZE; ++ i) {
    if (profile_table[i].files)
      count ++;
  }
  printf("Covered %u profiling points\n", count);
}

const int bitfield_size = 8;
int      profile_cross_component_low   [CROSS_ENTRY_COUNT][CROSS_MAX_ORDER];
int      profile_cross_component_high  [CROSS_ENTRY_COUNT][CROSS_MAX_ORDER];
int      profile_cross_component_step  [CROSS_ENTRY_COUNT][CROSS_MAX_ORDER];
uint8_t *profile_cross_bitfields       [CROSS_ENTRY_COUNT];
int      profile_cross_bitfield_lengths[CROSS_ENTRY_COUNT];

int log2int(int operand) {
  int r = 0; // r will be lg(v)
  assert(operand >= 0);
  while (operand >>= 1) // unroll for more speed...
    r++;

  return r;
}

static void
update_bitfield (int id, int point, int total_points)
{
  int num_elts;
  if (profile_cross_bitfields[id]) {
    num_elts = profile_cross_bitfield_lengths[id];
  } else {
    num_elts = (total_points + (bitfield_size - 1)) / bitfield_size;
    int num_bytes = num_elts * (bitfield_size / 8);
    profile_cross_bitfields[id] = (unsigned char *) malloc (num_bytes);
    assert(profile_cross_bitfields[id]);
    memset(profile_cross_bitfields[id], 0, num_bytes);
    profile_cross_bitfield_lengths[id] = num_elts;
  }

  int byte_covered = point / bitfield_size;
  uint8_t bit_covered = 1 << (point % bitfield_size);

  if (byte_covered < 0 || byte_covered >= num_elts)
  {
    printf("\n\n%s: pointCovered %d, bitfield_size %d, "
           "num_elts %d, totalPoints %d\n",
           profile_cross_entries[id].name, point,
           bitfield_size, num_elts, total_points);
    abort();
  }

  profile_cross_bitfields[id][byte_covered] |= bit_covered;
}

static void
profile_cross_offset_width (int *offset, int *width,
                            int val, int low, int high, int step)
{
  if (step <= 0) {
    * offset = log2int(val/low);
    * width = 1 + log2int(high/low);
  } else {
    * offset = val - low;
    int range_len = high - low;
    if (step != 1) {
      * offset /= step;
      * width = 1 + range_len / step;
    } else {
      * width = 1 + range_len;
    }
  }
}

// A specialization of profile_cross for the case when the order is 1.
static void
profile_cross_1d (int id, int val, int low, int high, int step)
{
  int offset, width;
  profile_cross_offset_width (& offset, & width, val, low, high, step);
  update_bitfield (id, offset, width);
}

void profile_cross(int id, ...) {
  if (! profile_enabled) return;

  int order = profile_cross_entries[id].order;

  if (order == 1) {
    va_list args;
    va_start(args, id);
    int val  = va_arg (args,int);
    int low  = va_arg (args,int);
    int high = va_arg (args,int);
    int step = va_arg (args,int);
    va_end(args);

    profile_cross_component_low[id][0] = low;
    profile_cross_component_high[id][0] = high;
    profile_cross_component_step[id][0] = step;

    profile_cross_1d (id, val, low, high, step);
    return;
  }

  // pointCovered will be the coverage point within the flattened
  // N-dimensional array for the cross bins. The indexing means that
  // the major axis comes first.
  unsigned pointCovered = 0;

  // totalPoints is the total size of the N-dimensional array, given
  // by the product of the widths.
  unsigned totalPoints = 1;

  // Extract the varargs
  va_list args;
  va_start(args, id);

  for (int i = 0; i < order; ++i) {
    int val=va_arg(args,int);
    int low=va_arg(args,int);
    int high=va_arg(args,int);
    int step=va_arg(args,int);

    profile_cross_component_low[id][i] = low;
    profile_cross_component_high[id][i] = high;
    profile_cross_component_step[id][i] = step;

    // The "point" that is covered depends on how the range for this
    // cross point is defined. If the "step" parameter is
    // non-positive, that means we have a power law distribution and
    // want to track log2(x / low). If the "step" parameter is
    // nonzero, this is a uniform distribution and we want to track
    // the difference (x - low)/step.
    //
    // Since it's overwhelmingly common for step to be 1, we special
    // case this (to avoid the slow division instruction)
    int offset, width;
    profile_cross_offset_width (& offset, & width, val, low, high, step);

    pointCovered = pointCovered * width + offset;
    totalPoints *= width;
  }
  va_end(args);

  update_bitfield (id, pointCovered, totalPoints);
}

void profile_indent(FILE *fid, int indentation);

static int
view_excluded (int view) {
    for (int ev = 0; excluded_views [ev] != -1; ++ ev) {
        if (view == excluded_views[ev])
            return 1;
    }
    return 0;
}

static int
cross_excluded (int id) {
    for (int i = 0; excluded_crosses[i] >= 0; ++ i) {
        if (id == excluded_crosses[i])
            return 1;
    }
    return 0;
}

void profile_cross_output(FILE *fid, FILE *pyProfileFid, int indentation) {
  int norm_id = 0;
  for (int id=0; id<CROSS_ENTRY_COUNT; ++id) {

    if (cross_excluded(id)) continue;

    ++ norm_id;

    if (!profile_cross_bitfields[id]) continue;

    if (pyProfileFid) fprintf(pyProfileFid, "'%s': ( 0x",profile_cross_entries[id].name);
    profile_indent(fid, indentation); fprintf(fid, "<crossPoint>\r\n");
    profile_indent(fid, indentation+2); fprintf(fid, "<id>%d</id>\r\n", norm_id - 1);
    profile_indent(fid, indentation+2); fprintf(fid, "<name>%s</name>\r\n",profile_cross_entries[id].name);
    int order=profile_cross_entries[id].order;
    profile_indent(fid, indentation+2); fprintf(fid, "<order>%d</order>\r\n",order);
    for (int i=0; i<order; ++i) {
      profile_indent(fid, indentation+2); fprintf(fid, "<component>\r\n");
      profile_indent(fid, indentation+4); fprintf(fid, "<name>%s</name>\r\n",profile_cross_entries[id].componentNames[i]);
      profile_indent(fid, indentation+4); fprintf(fid, "<low>%d</low>\r\n",profile_cross_component_low[id][i]);
      profile_indent(fid, indentation+4); fprintf(fid, "<high>%d</high>\r\n",profile_cross_component_high[id][i]);
      if (profile_cross_component_step[id][i]>=0) {
        profile_indent(fid, indentation+4); fprintf(fid, "<linearStep>%d</linearStep>\r\n",profile_cross_component_step[id][i]);
      } else {
        profile_indent(fid, indentation+4); fprintf(fid, "<powerOf2Step />\r\n");
      }
      profile_indent(fid, indentation+2); fprintf(fid, "</component>\r\n");
    }
    profile_indent(fid, indentation+2); fprintf(fid, "<bitfield>");
    for (int i=profile_cross_bitfield_lengths[id]-1; i>=0; --i) {
      fprintf(fid, "%02hhx", profile_cross_bitfields[id][i]);
      if (pyProfileFid) fprintf(pyProfileFid, "%02hhx", profile_cross_bitfields[id][i]);
    }

    fprintf(fid, "</bitfield>\r\n");
    if (pyProfileFid) {
      int totalBits=1;
      for (int i=0; i<order; ++i) {
        totalBits*=(1+profile_cross_component_high[id][i]-profile_cross_component_low[id][i]);
      }
      fprintf(pyProfileFid, ", %d, {\n", totalBits);
    }
    for (int v=0; v<VIEW_ENTRY_COUNT; v++) {
      if (view_excluded(v)) continue;

      for (int i=0; i<order; ++i)
        crossImpossibleCaseValues[i]=profile_cross_component_low[id][i];
      for (int i=0; i<profile_cross_bitfield_lengths[id]; ++i) {
        profile_cross_bitfields[id][i]=0;
        for (int j=0; j<bitfield_size; ++j) {
          if (crossImpossibleCaseValues[0]>profile_cross_component_high[id][0]) break;
          if (profile_cross_entries[id].icsFunc && (*profile_cross_entries[id].icsFunc)(crossImpossibleCaseValues,v)) {
            profile_cross_bitfields[id][i]|=(1<<j);
          }
          int k=order-1;
          while (k>=0) {
            int low=profile_cross_component_low[id][k];
            int high=profile_cross_component_high[id][k];
            int step=profile_cross_component_step[id][k];
            if (step>=0) crossImpossibleCaseValues[k]+=step;
            else crossImpossibleCaseValues[k]<<=1;
            if (crossImpossibleCaseValues[k]>high) crossImpossibleCaseValues[k]=low;
            else break;
            --k;
          }
          if (k < 0) break;
        }
      }
      profile_indent(fid, indentation+2); fprintf(fid, "<impossibleCasesBitfield view=\"%s\">",profile_views[v].id);
      for (int i=profile_cross_bitfield_lengths[id]-1; i>=0; --i) {
        fprintf(fid, "%02hhx", profile_cross_bitfields[id][i]);
      }
      fprintf(fid, "</impossibleCasesBitfield>\r\n");
      if (pyProfileFid) {
        fprintf(pyProfileFid, "  '%s': 0x", profile_views[v].id);
        for (int i=profile_cross_bitfield_lengths[id]-1; i>=0; --i) {
          fprintf(pyProfileFid, "%02hhx", profile_cross_bitfields[id][i]);
        }
        fprintf(pyProfileFid, ",\n");
      }
    }
    profile_indent(fid, indentation); fprintf(fid, "</crossPoint>\r\n");
    if (pyProfileFid) fprintf(pyProfileFid, "} ),\n");
  }
}

/* Structures for storing coverage tests */
typedef struct {
  long long specifiedMin;
  long long specifiedMax;
  int specifiedStep;
  long long actualMin;
  long long actualMax;
  TProfileCoveringStream *coveringStreams;
} TProfileCoverageData;


struct SProfileTestGroup {
  char *name;
  char *origName;
  char *sectionName;
  char *equationName;
  char *rangeEquationName;
  char *functionName;
  char *id;
  TProfileCoverageData *coverage;
  struct SProfileTestGroup *children;
  struct SProfileTestGroup *next;
  struct SProfileTestGroup *hash_next;
  unsigned int hash;
  const char *hash_str;
  int hash_len;
  const char *visibilityConditionTrue;
  const char *visibilityConditionFalse;
};

typedef struct SProfileTestGroup TProfileTestGroup;

//Start of the top-level linked list of groups
TProfileTestGroup *profile_groups=NULL;

// List of sections and equations we've seen (these only want outputting once each)
static std::set<std::string> sections_seen, equations_seen;

/* Look for blah_1_2_3 and extract section name */
char *profile_getSectionName(const char *groupName) {
  int state=0;
  const char *sectionNameStart=NULL;
  int sectionCount=0;
  for (int i=strlen(groupName)-1; i>=1; --i) {
    char c=groupName[i];
    //State 0 means we're looking for a digit, exit if we find anything else
    //Special case: if we find a single capital letter and we've already found some _<digits>
    //matches then extract that before we exit
    if (state==0) {
      if (c<'0' || c>'9') {
        if (sectionNameStart && c>='A' && c<='Z' && i>=2 && groupName[i-1]=='_') {
          sectionNameStart=groupName+i;
          sectionCount++;
        }
        break;
      }
      state=1;
    }
    //State 1 means we're looking for a digit or an underscore, exit if we find anything else
    else if (state==1) {
      if (c<'0' || c>'9') {
        if (c!='_') break;
        sectionCount++;
        sectionNameStart=groupName+i+1;
        state=0;
      }
    }
  }
  if (!sectionNameStart) return NULL;
  if (sectionCount<2) return NULL;
  char *result=(char *)malloc(strlen(sectionNameStart)+1);
  assert(result);
  {
    const char *sptr;
    char *rptr;
    for (sptr=sectionNameStart, rptr=result; ; sptr++, rptr++) {
    char c=*sptr;
    if (c=='_') *rptr='.';
    else *rptr=c;
    if (c=='\0') break;
    }
  }
  return result;
}

/* Look for 1.2.3.blah and extract equation name */
int profile_getEquationNames(TProfileTestGroup *group, const char *groupName) {

  //State 0: looking for a digit or a single capital letter.
  //      Digit -> state 1
  //      Capital letter -> state 2

  //State 1: looking for digits or a period.
  //      Digit -> state 1
  //      Period -> state 3

  //State 2: looking for a period.
  //      Period -> state 3

  //State 3: looking for digits, string end, or open square bracket.
  //      Digit -> state 3
  //      String end -> { equation = entire string, rangeEquation = NULL }
  //      Open square bracket -> rangeEqnPos=currentPos. state 4

  //State 4: Looking for a close square bracket or anything else.
  //      Close square bracket -> state 5
  //      Anything else -> state 4

  //State 5: Looking for end of string or period.
  //      Period -> state 6
  //      End of string -> { equation = string[0,rangeEqnPos], rangeEquation = string[rangeEqnPos+1,len-1] }

  //State 6: Looking for period.
  //      Period -> state 7

  //State 7: Looking for period.
  //      Period -> state 8

  //State 8: Looking for end of string.
  //      End of string -> { equation = string[0,rangeEqnPos], rangeEquation = NULL }

  int state=0;
  int len=strlen(groupName);
  int i;
  int rangeEqnPos=-1;

  group->equationName=NULL;
  group->rangeEquationName=NULL;

  for (i=0; i<len; i++) {
    char c=groupName[i];
    if (state==0) {
      if (c>='0' && c<='9') state=1;
      else if (c>='A' && c<='Z') state=2;
      else return 0;
    }
    else if (state==1) {
      if (c=='.') state=3;
      else if (c<'0' || c>'9') return 0;
    }
    else if (state==2) {
      if (c=='.') state=3;
    }
    else if (state==3) {
      if (c=='[') {
        rangeEqnPos=i;
        state=4;
      }
      else if (c<'0' || c>'9') return 0;
    }
    else if (state==4) {
      if (c==']') state=5;
    }
    else if (state>=5 && state<=7) {
      if (c=='.') state++;
      else return 0;
    }
    else {
      return 0;
    }
  }
  if (state==3) {
    group->equationName=(char *)malloc((len+1)*sizeof(char));
    assert(group->equationName);
    strcpy(group->equationName, groupName);
    group->name=group->equationName;
    return 1;
  }
  else if (state==5 || state==8) {
    assert(rangeEqnPos);
    group->equationName=(char *)malloc((rangeEqnPos+1)*sizeof(char));
    assert(group->equationName);
    strncpy(group->equationName,groupName,rangeEqnPos);
    group->equationName[rangeEqnPos]='\0';
    if (state==5) {
      group->name=group->equationName;
    }
    else {
      group->name=(char *)malloc((rangeEqnPos+4)*sizeof(char));
      assert(group->equationName);
      strcpy(group->name, group->equationName);
      for (i=0; i<4; ++i) {
        group->name[rangeEqnPos+3-i]=(i==0)?'\0':'.';
      }
    }
    if (state==5) {
      int rangeEquationLen=len-rangeEqnPos-1;
      group->rangeEquationName=(char *)malloc(rangeEquationLen*sizeof(char));
      assert(group->rangeEquationName);
      strncpy(group->rangeEquationName,groupName+rangeEqnPos+1,rangeEquationLen-1);
      group->rangeEquationName[rangeEquationLen-1]='\0';
    }
    return 1;
  }
  return 0;
}

void profile_storeInHierarchy(const char *str,long long actualLow, long long actualHigh,long long low,long long high,
TProfileCoveringStream *coveringStreams, const char *visibilityConditionTrue, const char *visibilityConditionFalse)
{
  TProfileTestGroup *parentGroup=NULL;
  //Make a copy for strtok to mess up
  char *strCopy=(char *)malloc((strlen(str)+1)*sizeof(char));
  assert(strCopy);
  strcpy(strCopy, str);
  //Tokenize the string
  TProfileTestGroup **groupsAtLevel=&profile_groups;
  char *token=strtok(strCopy, ";");
  int i=0;
  while (token)
  {
    char *origName = token;
    char *id=NULL;
    if (i==3) {
      char *colonPtr = strstr(token, ":");
      *colonPtr = '\0';
      origName=colonPtr + 1;
      while (*origName==' ') origName++;
      id=token;
    }
    if (i>=4) {
      id=token;
      token=strtok(NULL, ";");
      origName = token;
    }
    TProfileTestGroup *nextGroup=NULL;
    //Try and find an existing group
    if (*groupsAtLevel) {
      for (TProfileTestGroup *possible=*groupsAtLevel; possible; possible=possible->next) {
        //Check for a match
        if (strcmp(possible->origName, origName)!=0) continue;
        if (id) {
          if (!possible->id || strcmp(possible->id, id)!=0) continue;
        }
        else {
          if (possible->id) continue;
        }
        //Got a match
        nextGroup=possible;
        break;
      }
    }
    if (nextGroup) {
      parentGroup=nextGroup;
    }
    else {
      //Create new group and set it up
      TProfileTestGroup *ourNewGroup=(TProfileTestGroup *)malloc(sizeof(TProfileTestGroup));
      assert(ourNewGroup);
      ourNewGroup->origName=(char *)malloc(strlen(origName)+1);
      assert(ourNewGroup->origName);
      strcpy(ourNewGroup->origName, origName);
      profile_getEquationNames(ourNewGroup, origName);
      if (!(ourNewGroup->equationName)) ourNewGroup->name=ourNewGroup->origName;
      if (i==1) ourNewGroup->functionName=ourNewGroup->origName;
      else ourNewGroup->functionName=NULL;
      ourNewGroup->sectionName=profile_getSectionName(origName);
      ourNewGroup->children=NULL;
      ourNewGroup->coverage=NULL;
      ourNewGroup->next=NULL;
      ourNewGroup->visibilityConditionTrue=NULL;
      ourNewGroup->visibilityConditionFalse=NULL;
      if (id) {
        ourNewGroup->id=(char *)malloc(strlen(id)+1);
        assert(ourNewGroup->id);
        strcpy(ourNewGroup->id, id);
      } else ourNewGroup->id=NULL;
      //If there are already groups at this level, insert our new one in order
      for (TProfileTestGroup *nextGroup=*groupsAtLevel, **prevGroupNext=groupsAtLevel; ; prevGroupNext=&nextGroup->next, nextGroup=nextGroup->next) {
        //Check whether we should go BEFORE nextGroup
        if (nextGroup) {
          int nameCmp=strcmp(ourNewGroup->name,nextGroup->name);
          if (nameCmp>0) continue;
          //If names are the same, compare the IDs
          if (nameCmp==0) {
            if (id && (!nextGroup->id || strcmp(id, nextGroup->id)>0)) continue;
          }
        }
        //Insert the new group
        ourNewGroup->next=*prevGroupNext;
        *prevGroupNext=ourNewGroup;
        break;
      }
      parentGroup=ourNewGroup;
    }
    groupsAtLevel=&(parentGroup->children);
    token=strtok(NULL, ";");
    i++;
  }
  free(strCopy);
  //We've now found our target group, time to set the coverage data
  parentGroup->coverage=(TProfileCoverageData *)malloc(sizeof(TProfileCoverageData));
  assert(parentGroup->coverage);
  parentGroup->coverage->specifiedMin=low;
  parentGroup->coverage->specifiedMax=high;
  parentGroup->coverage->actualMin=actualLow;
  parentGroup->coverage->actualMax=actualHigh;
  parentGroup->coverage->coveringStreams=coveringStreams;
  parentGroup->visibilityConditionTrue=visibilityConditionTrue;
  parentGroup->visibilityConditionFalse=visibilityConditionFalse;
}
//Recursively clean up a group structure
void profile_cleanup_inner(TProfileTestGroup *groups) {
  while (groups) {
    TProfileTestGroup *currentGroup=groups;
    groups=groups->next;
    if (currentGroup->children) {
      profile_cleanup_inner(currentGroup->children);
    }
    if (currentGroup->coverage) free(currentGroup->coverage);
    if (currentGroup->origName) free(currentGroup->origName);
    if (currentGroup->equationName) free(currentGroup->equationName);
    if (currentGroup->rangeEquationName) free(currentGroup->rangeEquationName);
    if (currentGroup->id) free(currentGroup->id);
    if (currentGroup->sectionName) free(currentGroup->sectionName);
    free(currentGroup);
  }
}

void findCoveredValues_inner(TProfileTestGroup *groups, int count, const char *names[], int *results_out) {
  int i;
  while (groups) {
    TProfileTestGroup *currentGroup=groups;
    groups=groups->next;
    if (currentGroup->children) {
      findCoveredValues_inner(currentGroup->children, count, names, results_out);
    }
    for (i=0; i<count; i++) {
      if (strcmp(currentGroup->origName, names[i])==0) {
        results_out[i*3]   = 1;
        results_out[i*3+1] = currentGroup->coverage->actualMin;
        results_out[i*3+2] = currentGroup->coverage->actualMax;
      }
    }
  }
}

void findCoveredValues(int count, const char *names[], int *results_out) {
  memset(results_out, 0, sizeof(int) * count * 3);
  //Traverse the group structure looking for the names
  findCoveredValues_inner(profile_groups, count, names, results_out);
}

//Clean all our structures up
void profile_cleanup() {
  profile_cleanup_inner(profile_groups);
  profile_groups=NULL;
  equations_seen.clear ();
  sections_seen.clear ();

  for (int i = 0; i < PROFILE_POINTS_TABLE_SIZE; ++ i) {
    profile_entry_t &entry = profile_table [i];
    while (entry.files) {
      TProfileCoveringStream *next = entry.files->next;
      free (entry.files);
      entry.files = next;
    }
  }
}

/*
 * Functions to output XML from our structures
 * -------------------------------------------
 */

/* Output a certain level of indentation */
void profile_indent(FILE *fid, int indentation) {
  for (int i=0; i<indentation; ++i) {
    fprintf(fid, " ");
  }
}
/* Output a string, applying XML entities */
void profile_print_string_xmlescape(FILE *fid, const char *str) {
  int len=strlen(str);
  for (int i=0; i<len; i++) {
    char c=str[i];
    if (c=='\"') fprintf(fid, "&quot;");
    else if (c=='&') fprintf(fid, "&amp;");
    else if (c=='\'') fprintf(fid, "&apos;");
    else if (c=='<') fprintf(fid, "&lt;");
    else if (c=='>') fprintf(fid, "&gt;");
    else fprintf(fid, "%c", c);
  }
}

// Write a string element <name>value</name>. If value is NULL,
// nothing is written.
static void
write_string_elt (FILE *fid, int indentation,
                  const char *name, const char *value,
                  bool needs_escape=false)
{
  if (! value)
    return;

  profile_indent (fid, indentation);
  if (needs_escape) {
    fprintf (fid, "<%s>", name);
    profile_print_string_xmlescape (fid, value);
    fprintf (fid, "</%s>\r\n", name);
  } else {
    fprintf (fid, "<%s>%s</%s>\r\n", name, value, name);
  }
}

static void
write_int_elt (FILE *fid, int indentation,
               const char *name, long long int value)
{
  profile_indent (fid, indentation);
  fprintf (fid, "<%s>%lld</%s>\r\n", name, value, name);
}

static void
indented_fputs (FILE *fid, int indentation, const char *value)
{
  profile_indent (fid, indentation);
  fputs (value, fid);
  fputs ("\r\n", fid);
}

/* Recursively output XML for a set of test groups */
static void
profile_output_inner (FILE *fid, TProfileTestGroup *groups,
                      int indentation, int outputAllCoveringStreams)
{
  int id2 = indentation + 2;
  int id4 = indentation + 4;

  while (groups) {
    const char *open_tag = groups->children ? "<testGroup>" : "<testPoint>";
    const char *close_tag = groups->children ? "</testGroup>" : "</testPoint>";

    indented_fputs (fid, indentation, open_tag);

    if (groups->sectionName  && ! sections_seen.count (groups->sectionName)) {
      sections_seen.insert (groups->sectionName);
      write_string_elt (fid, id2, "section", groups->sectionName);
    }
    if (groups->equationName && ! equations_seen.count (groups->equationName)) {
      equations_seen.insert (groups->equationName);
      write_string_elt (fid, id2, "equation", groups->equationName);
      write_string_elt (fid, id2, "rangeEquation", groups->rangeEquationName);
    }
    write_string_elt (fid, id2, "function", groups->functionName);
    write_string_elt (fid, id2, "id", groups->id);
    write_string_elt (fid, id2, "name", groups->name, true);

    if (groups->visibilityConditionTrue) {
      if (groups->visibilityConditionFalse) {
        write_string_elt (fid, id2, "visibilityConditionTrue",
                          groups->visibilityConditionTrue, true);
        write_string_elt (fid, id2, "visibilityConditionFalse",
                          groups->visibilityConditionFalse, true);
      }
      else {
        write_string_elt (fid, id2, "visibilityCondition",
                          groups->visibilityConditionTrue, true);
      }
    }

    if (groups->coverage) {
      indented_fputs (fid, id2, "<coverage>");
      write_int_elt (fid, id4, "specifiedMin", groups->coverage->specifiedMin);
      write_int_elt (fid, id4, "specifiedMax", groups->coverage->specifiedMax);

      if (groups->coverage->coveringStreams) {
        write_int_elt (fid, id4, "actualMin", groups->coverage->actualMin);
        write_int_elt (fid, id4, "actualMax", groups->coverage->actualMax);

        if (outputAllCoveringStreams) {
            for (const auto *cs = groups->coverage->coveringStreams;
                 cs;
                 cs = cs->next) {
              write_string_elt (fid, id4, "coveringStream", cs->name, true);
            }
        }

        // Find covering streams for [zero, nonzero, min, max].
        const TProfileCoveringStream *cs_ptrs[4] = {NULL, NULL, NULL, NULL};
        int cs_found = 0;

        for (const auto *cs = groups->coverage->coveringStreams;
             cs && cs_found < 4;
             cs = cs->next) {

            for (int i = 0; i < 4; ++ i) {
                if ((! cs_ptrs[i]) && ((cs->cover >> i) & 1)) cs_ptrs[i] = cs;
            }
        }

        // Write out the covering streams we found
        static const char *cs_names[4] =
          { "zeroCoveringStream", "nonzeroCoveringStream",
            "minCoveringStream", "maxCoveringStream" };

        for (int i = 0; i < 4; ++ i) {
          if (cs_ptrs[i]) {
            write_string_elt (fid, id4, cs_names [i], cs_ptrs[i]->name, true);
          }
        }
      }
      indented_fputs (fid, id2, "</coverage>");
    }

    if (groups->children)
      profile_output_inner(fid, groups->children,
                           id2, outputAllCoveringStreams);

    indented_fputs (fid, indentation, close_tag);
    groups=groups->next;
  }
}

void profile_output_range_equations(FILE *fid, int indentation) {
  int id2 = indentation + 2;
  int id4 = indentation + 4;

  indented_fputs (fid, indentation, "<rangeEquationVisibilityTypes>");

  for (int k=0; rangeEquationVisibilityTypes[k].id; k++) {
    indented_fputs (fid, id2, "<rangeEquationVisibilityType>");
    write_string_elt (fid, id4, "id", rangeEquationVisibilityTypes[k].id);
    write_string_elt (fid, id4, "name", rangeEquationVisibilityTypes[k].name);
    indented_fputs (fid, id2, "</rangeEquationVisibilityType>");
  }
  indented_fputs (fid, indentation, "</rangeEquationVisibilityTypes>");
  indented_fputs (fid, indentation, "<rangeEquationGroups>");
  for (int i=0; i<rangeEquationGroupCount; i++) {
    TRangeEquationGroup *g = &(rangeEquationGroups[i]);
    // Skip any empty groups
    if (! g->equationCount)
        continue;

    //If at least one equation is present create the group
    indented_fputs (fid, id2, "<rangeEquationGroup>");
    write_string_elt (fid, id4, "name", g->name);
    indented_fputs (fid, id4, "<rangeEquations>");

    for (int j=0; j < g->equationCount; j++) {
      TRangeEquation *e = &(g->equations[j]);
      int id6 = indentation + 6;
      int id8 = indentation + 8;

      indented_fputs (fid, id6, "<rangeEquation>");
      write_string_elt (fid, id8, "name", e->name);
      write_string_elt (fid, id8, "mathml", e->mathML);
      if (e->extension) {
        indented_fputs (fid, id8, "<extension />");
      }
      for (int k=0; rangeEquationVisibilityTypes[k].id; k++) {
        profile_indent(fid, id8);
        fprintf(fid, "<theoreticalRange type=\"%s\">\r\n",
                rangeEquationVisibilityTypes[k].id);

        profile_indent(fid, indentation+10);
        fprintf(fid, "<min>%lld</min><max>%lld</max>\r\n",
                e->theoreticalRange[2*k], e->theoreticalRange[2*k+1]);

        indented_fputs (fid, id8, "</theoreticalRange>");
      }
      indented_fputs (fid, id6, "</rangeEquation>");
    }
    indented_fputs (fid, id4, "</rangeEquations>");
    indented_fputs (fid, id2, "</rangeEquationGroup>");
  }
  indented_fputs (fid, indentation, "</rangeEquationGroups>");
}

const char *getInitialView();
#if USE_DB
int profile_output_db(const char *dbURL, const char *firstStreamPath, int subset, int seed, int size);
#endif

/* Write all strings seen to a file */
void profile_output(const char *profXmlFile,
                    const char *profPyFile,
                    const char *firstStreamPath,
#if USE_DB
                    const char *dbURL, int subset, int seed,
#endif
                    size_t size,
                    int outputAllCoveringStreams)
{
  FILE *pyProfileFid=NULL;

  if (profPyFile) pyProfileFid = fopen(profPyFile, "w");

  if (pyProfileFid) {
    fprintf(pyProfileFid, "v={\n");
    for (int v=0; v<VIEW_ENTRY_COUNT; v++) {
      fprintf(pyProfileFid, "  '%s': ([ ", profile_views[v].id);
      int firstEntry=1;
      for (int i=0; i<VC_ENTRY_COUNT; i++) {
        if (profile_views[v].invisibleCategories[i]<0) break;
        if (firstEntry) firstEntry=0; else fprintf(pyProfileFid, ",");
        fprintf(pyProfileFid, "'%s'", profile_visibility_category_names[profile_views[v].invisibleCategories[i]]);
      }
      fprintf(pyProfileFid, " ], '%s'),\n", profile_views[v].rangeVisibilityType);
    }
    fprintf(pyProfileFid, "}\n");
    fprintf(pyProfileFid, "vc=[\n");
    for (int i=0; i<VC_ENTRY_COUNT; i++) {
      fprintf(pyProfileFid, "  '%s',\n", profile_visibility_category_names[i]);
    }
    fprintf(pyProfileFid, "]\n");

    fprintf(pyProfileFid, "t={\n");
  }

  for (int i = 0; i < PROFILE_POINTS_TABLE_SIZE; i++)
  {
    const profile_entry_t &entry = profile_table [i];
    const PROFILER_POINT_T &pd   = allProfilerPoints [i];

    profile_storeInHierarchy (pd.name,
                              entry.min,
                              entry.max,
                              pd.low,
                              pd.high,
                              entry.files,
                              pd.visibilityConditionTrue,
                              pd.visibilityConditionFalse);
    if (pyProfileFid) {
      const char *visF = (pd.visibilityConditionFalse ?
                          pd.visibilityConditionFalse :
                          "None");

      if (entry.files)
        fprintf (pyProfileFid, "'%s':(%lld,%lld,%lld,%lld,'%s','%s'),\n",
                 pd.name, entry.min, entry.max, pd.low, pd.high,
                 pd.visibilityConditionTrue, visF);
      else
        fprintf (pyProfileFid, "'%s':(None,None,%lld,%lld,'%s','%s'),\n",
                 pd.name, pd.low, pd.high,
                 pd.visibilityConditionTrue, visF);
    }
  }
  if (pyProfileFid) {
    fprintf(pyProfileFid,"}\n");
  }
#if USE_DB
  if (dbURL) {
      if (profile_output_db(dbURL, firstStreamPath, subset, seed, size) != 0) {
          printf("profile_output_db returned non-zero!\n");
          exit(1);
      }
  }
#endif
  FILE *fid=fopen(profXmlFile,"wb");
  if (!fid) {
    fprintf(stderr,"Cannot write to file %s\n.",profXmlFile);
    printf("Cannot write to file %s\n.",profXmlFile);
    exit(-1);
  }
  fprintf(fid, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n");
  //Write out the magical IE comment that forces this document to end up in the Internet zone
  fprintf(fid, "<!-- saved from url=(0014)about:internet -->\r\n");
  fprintf(fid, "<?xml-stylesheet type=\"text/xsl\" href=\"ArgonViewerV14/coverage.xslt\"?>\r\n");
  fprintf(fid, "<report>\r\n");
#ifdef CODEC
  #define SCODEC2(C) #C
  #define SCODEC(C) SCODEC2(C)
  profile_indent(fid, 2); fprintf(fid, "<codec>" SCODEC(CODEC) "</codec>\r\n");
#endif
  profile_indent(fid, 2); fprintf(fid, "<visibilityCategories>\r\n");
  for (int i=0; i<VC_ENTRY_COUNT; i++) {
    profile_indent(fid, 4); fprintf(fid, "<visibilityCategory>%s</visibilityCategory>\r\n", profile_visibility_category_names[i]);
  }
  profile_indent(fid, 2); fprintf(fid, "</visibilityCategories>\r\n");
  profile_indent(fid, 2); fprintf(fid, "<views>\r\n");
  for (int i=0; i<VIEW_ENTRY_COUNT; i++) {
    if (view_excluded(i)) continue;

    profile_indent(fid, 4); fprintf(fid, "<view>\r\n");
    profile_indent(fid, 6); fprintf(fid, "<id>%s</id>\r\n", profile_views[i].id);
    profile_indent(fid, 6); fprintf(fid, "<name>%s</name>\r\n", profile_views[i].displayName);
    profile_indent(fid, 6); fprintf(fid, "<rangeVisibilityType>%s</rangeVisibilityType>\r\n", profile_views[i].rangeVisibilityType);
    profile_indent(fid, 6); fprintf(fid, "<invisibleCategories>");
    int j;
    for (j=0; profile_views[i].invisibleCategories[j]>=0; j++) {
      if (j!=0) fprintf(fid, ",");
      fprintf(fid, "%s", profile_visibility_category_names[profile_views[i].invisibleCategories[j]]);
    }
    fprintf(fid, "</invisibleCategories>\r\n");
    profile_indent(fid, 4); fprintf(fid, "</view>\r\n");
  }
  profile_indent(fid, 2); fprintf(fid, "</views>\r\n");
  profile_indent(fid, 2); fprintf(fid, "<initialView>%s</initialView>\r\n", getInitialView());
  profile_output_range_equations(fid, 2);
  if (profile_groups) {
    profile_output_inner(fid, profile_groups, 2, outputAllCoveringStreams);
  }
  if (pyProfileFid) fprintf(pyProfileFid, "c={\n");
  profile_cross_output(fid, pyProfileFid, 2);
  if (pyProfileFid) {
    fprintf(pyProfileFid,"}\n");
    fclose(pyProfileFid);
  }
  fprintf(fid, "</report>\r\n");
  fclose(fid);
  profile_cleanup();
}

#if USE_DB
void sql_error(const char *err) {
    fprintf(stderr, "SQL error: %s\n", err);
}

int profile_exec_sql(soci::session &s, const char *sql) {
    try {
      s << sql;
    } catch (soci::soci_error const &e) {
      std::cerr << "SQL error: " << e.what() << std::endl;
      return 1;
    }
    return 0;
}

int findRangeEquation(const char *branchPoint, const char *rangeVisibilityType, std::string &rangeEquationNameOut, long long *theoreticalRangeOut) {
    int semicolonCount = 0;
    const char *foundEqnName = 0;
    for (const char *c=branchPoint; *c; c++) {
      if (*c == ';') semicolonCount++;
      if (semicolonCount == 2) {
        if (*c == '[') {
          foundEqnName = c+1;
        }
        if (*c == ']') {
          if (*(c+1) != ';' && *(c+1) != '\0') return 0;
          const char *lastSemicolon = NULL;
          for (const char *c2=c; *c2; c2++) {
                if (*c2==';') lastSemicolon = c2;
          }
          if (strncmp(lastSemicolon+1, "Clip3", 5) == 0 ||
              strncmp(lastSemicolon+1, "ROUND_POWER_OF_TWO", 18) == 0 ||
              strncmp(lastSemicolon+1, "ROUND_POWER_OF_TWO_SATCHECK", 27) == 0 ||
              strncmp(lastSemicolon+1, "clip_pixel", 10) == 0)
              return 0;
          rangeEquationNameOut.assign(foundEqnName, c - foundEqnName);
          int visibilityType = -1;
          for (int k=0; rangeEquationVisibilityTypes[k].id; k++) {
            if (strcmp(rangeVisibilityType, rangeEquationVisibilityTypes[i].id)==0) {
              visibilityType = i;
              break;
            }
          }
          assert(visibilityType >= 0);
          for (int i=0; i < rangeEquationGroupCount; i++) {
            TRangeEquationGroup *g = rangeEquationGroups+i;
            for (int j=0; j < g->equationCount; j++) {
              if (rangeEquationNameOut == g->equations[j].name) {
                theoreticalRangeOut[0] = g->equations[j].theoreticalRange[2*visibilityType];
                theoreticalRangeOut[1] = g->equations[j].theoreticalRange[2*visibilityType+1];
                return 1;
              }
            }
          }
          assert(0);
        }
      }
      else if (semicolonCount == 3) {
        return 0;
      }
    }
    return 0;
}

CrossRange *get_cross_range(const CrossEntry *c) {
    for (CrossRange *cr = cross_ranges; cr->name; cr++) {
        if (strcmp(cr->name, c->name) == 0) {
            return cr;
        }
    }
    return NULL;
}

int get_cross_bits(const CrossEntry *c, const CrossRange *cr) {
    int bits = 1;
    for (int o=0; o<c->order; o++) {
        long long low = cr->range[o*3];
        long long high = cr->range[o*3+1];
        long long step = cr->range[o*3+2];
        if (step > 0) {
            bits *= (high + 1 - low) / step;
        }
        else {
            bits *= builtin_Log2(high/low)+1;
        }
    }
    return bits;
}

void get_cross_values(const CrossEntry *c, const CrossRange *cr, int bit, int *valuesOut) {
    for (int o=c->order-1; o>=0; o--) {
        int bits;
        long long low = cr->range[o*3];
        long long high = cr->range[o*3+1];
        long long step = cr->range[o*3+2];
        if (step > 0) {
            bits = (high + 1 - low) / step;
            valuesOut[o] = low + step * (bit % bits);
        }
        else {
            bits = builtin_Log2(high/low)+1;
            valuesOut[o] = low << (bit % bits);
        }
        bit /= bits;
    }
}

#include <unistd.h>
int begin_exclusive_transaction(soci::session &s) {
    int count = 0;
    while (1) {
        try {
          s << "BEGIN EXCLUSIVE TRANSACTION";
          return 0;
        } catch (soci::sqlite3_soci_error const &e) {
          if (e.result() == SQLITE_BUSY) {
            count++;
            if ((count % 10) == 0)
                printf("Waiting for database lock...\n");
            sleep(1);
            continue;
          }
          else return 1;
        }
    }
}

int profile_init_db(const char *dbURL) {
    std::string rangeEquationName;
    long long rangeEquationTheoreticalRange[2];
    duk_context *dukCtx;
    dukCtx = duk_create_heap_default();

    try {
        soci::session s(dbURL);

        if (begin_exclusive_transaction(s) != 0) return 1;
        if (profile_exec_sql(s, "CREATE TABLE IF NOT EXISTS tool_branch_points (build_number TEXT NOT NULL, branch_point_id INTEGER NOT NULL, UNIQUE(build_number, branch_point_id) ON CONFLICT IGNORE)")) return 1;
        if (profile_exec_sql(s, "CREATE TABLE IF NOT EXISTS tool_binary_branch_points (build_number TEXT NOT NULL, binary_branch_point_id INTEGER NOT NULL, UNIQUE(build_number, binary_branch_point_id) ON CONFLICT IGNORE)")) return 1;
        if (profile_exec_sql(s, "CREATE TABLE IF NOT EXISTS tool_cross_points (build_number TEXT NOT NULL, cross_point_id INTEGER NOT NULL, UNIQUE(build_number, cross_point_id) ON CONFLICT IGNORE)")) return 1;

        if (profile_exec_sql(s, "CREATE TABLE IF NOT EXISTS branch_points (name TEXT NOT NULL UNIQUE ON CONFLICT IGNORE)")) return 1;
        if (profile_exec_sql(s, "CREATE TABLE IF NOT EXISTS binary_branch_points (name TEXT NOT NULL UNIQUE ON CONFLICT IGNORE)")) return 1;
        if (profile_exec_sql(s, "CREATE TABLE IF NOT EXISTS cross_points (name TEXT NOT NULL, bit_id INTEGER NOT NULL, UNIQUE(name, bit_id) ON CONFLICT IGNORE)")) return 1;

        if (profile_exec_sql(s, "DROP TABLE IF EXISTS views")) return 1;
        if (profile_exec_sql(s, "DROP TABLE IF EXISTS range_points")) return 1;
        if (profile_exec_sql(s, "DROP TABLE IF EXISTS visible_branch_points")) return 1;
        if (profile_exec_sql(s, "DROP TABLE IF EXISTS visible_binary_branch_points")) return 1;
        if (profile_exec_sql(s, "DROP TABLE IF EXISTS visible_cross_points")) return 1;

        if (profile_exec_sql(s, "CREATE TABLE views (name TEXT NOT NULL UNIQUE ON CONFLICT IGNORE)")) return 1;
        if (profile_exec_sql(s, "CREATE TABLE range_points (branch_point_id INTEGER NOT NULL, view_id INTEGER NOT NULL, "
                                 "name TEXT NOT NULL, low INTEGER NOT NULL, high INTEGER NOT NULL, UNIQUE (view_id, name) ON CONFLICT REPLACE)")) return 1;
        if (profile_exec_sql(s, "CREATE TABLE visible_branch_points (view_id INTEGER NOT NULL, branch_point_id INTEGER NOT NULL)")) return 1;
        if (profile_exec_sql(s, "CREATE TABLE visible_binary_branch_points (view_id INTEGER NOT NULL, binary_branch_point_id INTEGER NOT NULL, type INTEGER NOT NULL)")) return 1;
        if (profile_exec_sql(s, "CREATE TABLE visible_cross_points (view_id INTEGER NOT NULL, cross_point_id INTEGER NOT NULL)")) return 1;

        if (profile_exec_sql(s, "CREATE TABLE IF NOT EXISTS streams (name TEXT NOT NULL UNIQUE, subset INTEGER NOT NULL, seed INTEGER NOT NULL, size INTEGER NOT NULL)")) return 1;
        if (profile_exec_sql(s, "CREATE TABLE IF NOT EXISTS branch_coverage (stream_id INTEGER NOT NULL, branch_point_id INTEGER NOT NULL, min INTEGER NOT NULL, max INTEGER NOT NULL)")) return 1;
        if (profile_exec_sql(s, "CREATE TABLE IF NOT EXISTS binary_branch_coverage (stream_id INTEGER NOT NULL, binary_branch_point_id INTEGER NOT NULL, type INTEGER NOT NULL)")) return 1;
        if (profile_exec_sql(s, "CREATE TABLE IF NOT EXISTS cross_coverage (stream_id INTEGER NOT NULL, cross_point_id INTEGER NOT NULL)")) return 1;

        if (profile_exec_sql(s, "CREATE INDEX IF NOT EXISTS branch_coverage_by_point_id ON branch_coverage(branch_point_id)")) return 1;
        if (profile_exec_sql(s, "CREATE INDEX IF NOT EXISTS branch_coverage_by_stream_id ON branch_coverage(stream_id)")) return 1;
        if (profile_exec_sql(s, "CREATE INDEX IF NOT EXISTS binary_branch_coverage_by_point_id_and_type ON binary_branch_coverage(binary_branch_point_id, type)")) return 1;
        if (profile_exec_sql(s, "CREATE INDEX IF NOT EXISTS binary_branch_coverage_by_stream_id ON binary_branch_coverage(stream_id)")) return 1;
        if (profile_exec_sql(s, "CREATE INDEX IF NOT EXISTS cross_coverage_by_point_id ON cross_coverage(cross_point_id)")) return 1;
        if (profile_exec_sql(s, "CREATE INDEX IF NOT EXISTS cross_coverage_by_stream_id ON cross_coverage(stream_id)")) return 1;

        std::string pointName;
        std::string build = AV1_VERSION;
        soci::statement insertBinaryBranchPointStmt =
            (s.prepare << "INSERT INTO binary_branch_points(name) VALUES (:pname)",
                          soci::use(pointName));
        soci::statement insertToolBinaryBranchPointsStmt =
            (s.prepare << "INSERT INTO tool_binary_branch_points(build_number, binary_branch_point_id) SELECT :build, rowid FROM binary_branch_points WHERE name=:pname",
                          soci::use(build),
                          soci::use(pointName));
        soci::statement insertBranchPointStmt =
            (s.prepare << "INSERT INTO branch_points(name) VALUES (:pname)",
                          soci::use(pointName));
        soci::statement insertToolBranchPointsStmt =
            (s.prepare << "INSERT INTO tool_branch_points(build_number, branch_point_id) SELECT :build, rowid FROM branch_points WHERE name=:pname",
                          soci::use(build),
                          soci::use(pointName));
        for (const PROFILER_POINT_T *point=allProfilerPoints; point->name; ++point) {
          pointName = point->name;
          if (point->low == 0 && point->high == 1) {
            insertBinaryBranchPointStmt.execute(true);
            insertToolBinaryBranchPointsStmt.execute(true);
          } else {
            insertBranchPointStmt.execute(true);
            insertToolBranchPointsStmt.execute(true);
          }
        }

        std::string viewName;
        int asserted;
        int bitID;

        soci::statement insertViewStmt =
            (s.prepare << "INSERT INTO views(name) VALUES (:vname)",
                          soci::use(viewName));
        soci::statement insertVisibleBinaryBranchPointStmt =
            (s.prepare << "INSERT INTO visible_binary_branch_points(view_id, binary_branch_point_id, type) "
                          "SELECT views.rowid, binary_branch_points.rowid, :asserted FROM views JOIN binary_branch_points "
                          "WHERE views.name=:vname AND binary_branch_points.name=:pname",
                          soci::use(asserted),
                          soci::use(viewName),
                          soci::use(pointName));
        soci::statement insertVisibleBranchPointStmt =
            (s.prepare << "INSERT INTO visible_branch_points(view_id, branch_point_id) "
                          "SELECT views.rowid, branch_points.rowid FROM views JOIN branch_points "
                          "WHERE views.name=:vname AND branch_points.name=:pname",
                          soci::use(viewName),
                          soci::use(pointName));
        soci::statement insertRangePointStmt =
            (s.prepare << "INSERT INTO range_points(branch_point_id, view_id, name, low, high) "
                          "SELECT branch_points.rowid, views.rowid, :rname, :low, :high FROM branch_points JOIN views "
                          "WHERE branch_points.name=:pname AND views.name=:vname",
                          soci::use(rangeEquationName),
                          soci::use(rangeEquationTheoreticalRange[0]),
                          soci::use(rangeEquationTheoreticalRange[1]),
                          soci::use(pointName),
                          soci::use(viewName));
        soci::statement insertToolCrossPointsStmt =
            (s.prepare << "INSERT INTO tool_cross_points(build_number, cross_point_id) "
                          "SELECT :build, rowid FROM cross_points WHERE name=:pname AND bit_id=:bitid",
                          soci::use(build),
                          soci::use(pointName),
                          soci::use(bitID));
        soci::statement insertCrossPointStmt =
            (s.prepare << "INSERT INTO cross_points(name, bit_id) VALUES (:pname, :bitid)",
                          soci::use(pointName),
                          soci::use(bitID));
        soci::statement insertVisibleCrossPointStmt =
            (s.prepare << "INSERT INTO visible_cross_points(view_id, cross_point_id) "
                          "SELECT views.rowid, cross_points.rowid FROM views JOIN cross_points "
                          "WHERE views.name=:vname AND cross_points.name=:pname AND cross_points.bit_id=:bitid",
                          soci::use(viewName),
                          soci::use(pointName),
                          soci::use(bitID));
        for (int i = 0; i < VIEW_ENTRY_COUNT; i++) {
          const CoverageReportView *v = profile_views + i;
          viewName = v->id;
//          int asserted;
//          std::string pointName;
//          int bitID;
          //Set all visibility categories to be visible first
          for (int j=0; j<VC_ENTRY_COUNT; j++) {
            duk_push_int(dukCtx, 1);
            duk_put_global_string(dukCtx, profile_visibility_category_names[j]);
          }
          //Set invisible ones for this view to zero
          for (int j=0; v->invisibleCategories[j] != -1; j++) {
            duk_push_int(dukCtx, 0);
            duk_put_global_string(dukCtx, profile_visibility_category_names[v->invisibleCategories[j]]);
          }
          insertViewStmt.execute(true);
          for (const PROFILER_POINT_T *point=allProfilerPoints; point->name; ++point) {
            pointName = point->name;
            if (point->low == 0 && point->high == 1) {
              duk_eval_string(dukCtx, point->visibilityConditionTrue);
              int pointVisibleTrue = duk_to_boolean(dukCtx, -1);
              duk_pop(dukCtx);
              int pointVisibleFalse = pointVisibleTrue;
              if (point->visibilityConditionFalse) {
                duk_eval_string(dukCtx, point->visibilityConditionFalse);
                pointVisibleFalse = duk_to_boolean(dukCtx, -1);
                duk_pop(dukCtx);
              }

              if (pointVisibleFalse) {
                asserted = 0;
                insertVisibleBinaryBranchPointStmt.execute(true);
              }
              if (pointVisibleTrue) {
                asserted = 1;
                insertVisibleBinaryBranchPointStmt.execute(true);
              }
            } else {
              duk_eval_string(dukCtx, point->visibilityConditionTrue);
              int pointVisible = duk_to_boolean(dukCtx, -1);
              duk_pop(dukCtx);

              if (pointVisible) {
                insertVisibleBranchPointStmt.execute(true);
                if (findRangeEquation(point->name, v->rangeVisibilityType, rangeEquationName, rangeEquationTheoreticalRange)) {
                  insertRangePointStmt.execute(true);
                }
              }
            }
          }
          for (int j=0; j < CROSS_ENTRY_COUNT; j++) {
            const CrossEntry *c = profile_cross_entries + j;
            const CrossRange *cr = get_cross_range(c);
            if (!cr) {
                printf("Skipping unused cross entry %s\n", c->name);
                continue;
            }
            pointName = c->name;
            int crossBits = get_cross_bits(c, cr);
            int crossVals[CROSS_MAX_ORDER];
            for (bitID=0; bitID < crossBits; bitID++) {
              insertCrossPointStmt.execute(true);
              insertToolCrossPointsStmt.execute(true);
              get_cross_values(c, cr, bitID, crossVals);
              if (!(c->icsFunc) || !((*(c->icsFunc))(crossVals, i))) {
                  insertVisibleCrossPointStmt.execute(true);
                  get_cross_values(c, cr, bitID, crossVals);
              }
            }
          }
        }
        if (profile_exec_sql(s, "END TRANSACTION")) return 1;
    } catch (soci::soci_error &e) {
      std::cerr << "SQL error: " << e.what() << std::endl;
      return 1;
    }
    return 0;
}

int profile_output_db(const char *dbURL, const char *firstStreamPath, int subset, int seed, int size) {
    int i;

    // Find the first stream's basename
    std::string firstStreamName = firstStreamPath;
    for (i=0; firstStreamPath[i]; i++) {
        if (firstStreamPath[i] == '/' || firstStreamPath[i] == '\\') {
            firstStreamName = &(firstStreamPath[i+1]);
        }
    }

    try {
        soci::session s(dbURL);
        if (begin_exclusive_transaction(s) != 0) return 1;

        soci::statement insertStream =
            (s.prepare << "INSERT INTO streams(name, subset, seed, size) "
                          "VALUES (:name, :subset, :seed, :size)",
                          soci::use(firstStreamName),
                          soci::use(subset),
                          soci::use(seed),
                          soci::use(size));
        insertStream.execute(true);

        int type;
        std::string pointName;
        long long pmin, pmax;

        soci::statement insertBinaryBranchCoverage =
            (s.prepare << "INSERT INTO binary_branch_coverage(stream_id, binary_branch_point_id, type) "
                          "SELECT streams.rowid, binary_branch_points.rowid, :type FROM streams JOIN binary_branch_points "
                          "WHERE streams.name=:sname AND binary_branch_points.name=:pname",
                          soci::use(type),
                          soci::use(firstStreamName),
                          soci::use(pointName));
        soci::statement insertBranchCoverage =
            (s.prepare << "INSERT INTO branch_coverage(stream_id, branch_point_id, min, max) "
                          "SELECT streams.rowid, branch_points.rowid, :min, :max FROM streams JOIN branch_points "
                          "WHERE streams.name=:sname AND branch_points.name=:pname",
                          soci::use(pmin),
                          soci::use(pmax),
                          soci::use(firstStreamName),
                          soci::use(pointName));

        for(i = 0; i < PROFILE_POINTS_TABLE_SIZE; i++)
        {
          if (! profile_files[i])
            continue;

          const PROFILER_POINT_T &pd = allProfilerPoints [i];
          const profile_entry_t &entry = profile_table [i];

          pointName = pd.name;
          if (pd.low == 0 && pd.high == 1) {
              for (type = 0; type < 2; type++) {
                  if (type ? (entry.max < 1) : (entry.min > 0)) continue;
                  insertBinaryBranchCoverage.execute(true);
              }
          } else {
              pmin = entry.min;
              pmax = entry.max;
              insertBranchCoverage.execute(true);
          }
        }

        int bit_id;

        soci::statement insertCrossCoverage =
            (s.prepare << "INSERT INTO cross_coverage(stream_id, cross_point_id) "
                          "SELECT streams.rowid, cross_points.rowid FROM streams JOIN cross_points "
                          "WHERE streams.name=:sname AND cross_points.name=:pname AND cross_points.bit_id=:bitid",
                          soci::use(firstStreamName),
                          soci::use(pointName),
                          soci::use(bit_id));

        for (int id=0; id<CROSS_ENTRY_COUNT; ++id) {
          if (!profile_cross_bitfields[id]) continue;
          const CrossEntry *c = profile_cross_entries + id;
          pointName = c->name;
          const CrossRange *cr = get_cross_range(c);
          if (!cr) {
              printf("Skipping unused cross entry %s\n", c->name);
              continue;
          }
          int crossBits = get_cross_bits(c, cr);
          int order=c->order;
          int totalBits=1;
          for (int i=0; i<order; ++i) {
            totalBits*=(1+profile_cross_component_high[id][i]-profile_cross_component_low[id][i]);
          }
          for (int i=0; i < profile_cross_bitfield_lengths[id]; i++) {
            for (int j=0; j < bitfield_size; j++) {
              bit_id = i*bitfield_size+j;
              if (bit_id >= crossBits) break;
              insertCrossCoverage.execute(true);
            }
          }
        }
        if (profile_exec_sql(s, "END TRANSACTION")) return 1;
    } catch (soci::soci_error &e) {
      std::cerr << "SQL error: " << e.what() << std::endl;
      return 1;
    }
    return 0;
}
#endif
#endif

// Local Variables:
// mode: c++
// End:
