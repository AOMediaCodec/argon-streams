/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#line 8 "av1b_command_line.cc"

// AV1B command-line parameter parsing

#include <libgen.h>
#include <getopt.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>

class InputFile {
public:
    std::string path;
    bool annexB;
    int numLargeScaleTileAnchorFrames;
    int numLargeScaleTileTileListObus;

public:
    InputFile(std::string path, bool annexB,
              int numLargeScaleTileAnchorFrames = 0,
              int numLargeScaleTileTileListObus = 0):
        path (path),
        annexB (annexB),
        numLargeScaleTileAnchorFrames (numLargeScaleTileAnchorFrames),
        numLargeScaleTileTileListObus (numLargeScaleTileTileListObus)
    {}
};

struct CommandLine
{
    CommandLine (int argc, char **argv);

    std::string app_name;

    int         frame_limit;
    int         display_frame_rate;
    bool        is_annex_b;
    int         max_num_oppoints;
    int         operating_point;
    bool        estimate_command;
    bool        override_no_film_grain;

    const char *dump_base;
    int         dump_flags;

    const char *check_base;
    int         check_flags;

#if WRITE_YUV
    const char *yuv_output_file_name;
    unsigned    output_bytes;
#endif

#if USE_PROFILE
    const char *prof_xml_file;
    const char *prof_py_file;
    bool        prof_output_all_streams;
#endif

#if USE_DB
    const char *db_url;
    int         db_subset;
    int         db_seed;
    bool        db_init;
#endif

    bool        verbose;

    std::vector<InputFile> input_files;

private:
    void syntax (std::ostream &os) const;
    void die (const char *msg) const;
    unsigned read_uint_arg (const char *arg_name, const char *arg) const;
    void parse (int argc, char **argv);

    void add_input_file (const char *path,
                         int num_lst_anchor_frames = 0,
                         int num_lst_tile_list_obus = 0);

    void read_file_list (const char *path);
    void read_file_list_stream (std::istream &is);
    void read_dump_flags (const char *arg);
    void read_check_flags (const char *arg);

    void read_flags (int *dest,
                     const char *str,
                     const char *what);

#if WRITE_YUV
    void take_bit_depth (const char *arg);
#endif
};

CommandLine::CommandLine (int argc, char **argv)
    : app_name (basename (argv [0])),
      frame_limit (-1),
      display_frame_rate (0),
      is_annex_b (true),
      max_num_oppoints(-1),
      operating_point (0),
      estimate_command (false),
      override_no_film_grain (false),
      dump_base (NULL),
      dump_flags (0),
      check_base (NULL),
      check_flags (0),
#if WRITE_YUV
      yuv_output_file_name ("out.yuv"),
      output_bytes (0),
#endif
#if USE_PROFILE
      prof_xml_file ("prof.xml"),
      prof_py_file (NULL),
      prof_output_all_streams (false),
#endif
#if USE_DB
      db_url (NULL),
      db_subset (-1),
      db_seed (-1),
      db_init (false),
#endif
      verbose (false)
{
    parse (argc, argv);
}

void CommandLine::syntax (std::ostream &os) const {
#if COVER
    os << ("\n  Argon Design AV1 Coverage Tool build " AV1_VERSION "\n"
#else // COVER
    os << ("\n  Argon Design AV1 Decoder build " AV1_VERSION "\n"
#endif // COVER
           "  Copyright (C) Argon Design 2016-2019\n\n"
           "    Syntax:\n")
       << "      " << app_name;

    static const char *short_args[] = {
        "[-v]",
#if USE_PROFILE && COVER
        "[-p <XML output file name>]",
#endif
        "[-f <number of frames>]",
        "<input file> [<input file>...]",
        NULL
    };

    const unsigned arg_indent_len = 6 + app_name.size () + 1;
    const std::string arg_indent (arg_indent_len, ' ');
    unsigned hpos = arg_indent_len;

    for (unsigned i = 0; short_args [i]; ++ i) {
        unsigned w = strlen (short_args [i]);
        if (hpos + 1 + w >= 80) {
            os << '\n' << arg_indent;
            hpos = arg_indent_len;
        } else {
            os << ' ';
        }
        os << short_args [i];
        hpos += 1 + w;
    }
    os << "\n\n";

    os << ("    Options:\n"
           "      -h, --help\n\n"
           "        Display this message\n\n\n"
           "      -v, --verbose\n\n"
           "        Enable internal verbose messages\n\n\n"
           "      -f <num-frames>, --frame-limit <num-frames>\n\n"
           "        Limit number of frames to be decoded (default is to decode all frames)\n\n\n"
           "      -T <file-list>, --inputs <file-list>\n\n"
           "        Read input file paths from file list rather than command line\n"
           "        (use \"-T -\" to read from stdin)\n\n\n"
           "      --large-scale-tile <num Anchor Frames> <num Tile List OBUs> <input file>\n\n"
           "        Provide large scale tile parameters, followed by exactly one input file\n\n\n"
           "      --no-film-grain\n\n"
           "        Don't apply the film grain noise process\n\n\n"
           "      --oppoint <N>\n\n"
           "        Operating point.\n\n\n"
#if !COVER
           "      --estimate-cmd\n\n"
           "        Print out the corresponding reference decoder commands.\n\n\n"
#endif // !COVER
           "      --not-annexb, --annexb\n\n"
           "        Configure whether to use annex B (default is enabled)\n\n\n"
#if !RELEASE
#if !COVER
           "      --max-num-oppoints <N>\n\n"
           "        Returns the maximum number of oppoints in this stream.\n\n\n"
           "      --decode-frame-rate <frame-rate>\n\n"
           "        Set the decode frame rate\n\n\n"
#endif // !COVER
           "      --dump-base <path>\n\n"
           "        Dump debug/validation information to files starting with path. If --dump\n"
           "        is specified without --dump-base, this defaults to `av1b', which means\n"
           "        lines will be dumped to files like `av1b.<stream_num>.cabac.log'.\n\n\n"
           "      --dump=[<feature1>,<feature2>]\n\n"
           "        Dump given information (to compare with libaom or other decoders). If --dump\n"
           "        is specified with no arguments, or if --dump-base is specified\n"
           "        without --dump, all features are dumped.\n"
           "        Specific features to dump: cabac, mv, intra, txform\n\n\n"
           "      --check-base <path>\n\n"
           "        Read validation information from files starting with path. For example,\n"
           "        cabac information is read from <path>.cabac.log. Specifying --check\n"
           "        without --check-base is an error. If --check-base is specified without\n"
           "        --check, log files will be read if they are readable and silently ignored\n"
           "        if not. For the paranoid 'check everything' mode, specify --check-base and\n"
           "        also --check=all.\n\n\n"
           "      --check=[<feature1>,<feature2>]\n\n"
           "        Specify what information is checked from paths beginning with\n"
           "        --check-base. The special feature 'all' means to check all features,\n"
           "        failing if a log file cannot be opened. Otherwise, the available features\n"
           "        are the same as for --dump.\n\n\n"
#endif // !RELEASE
#if WRITE_YUV
           "     YUV-writing options:\n"
           "      -o <filename>, --output <filename>\n\n"
           "        YUV output file\n\n\n"
           "      -d <bit-depth>, --bit-depth <bit-depth>\n\n"
           "        Specify output bit depth\n\n\n"
#endif
#if USE_PROFILE
           "     Profile options:\n"
           "      -p <filename>, --profile <filename>\n\n"
           "        Specify XML output filename (default is prof.xml)\n\n\n"
# if !RELEASE
           "      --pyf <filename>\n\n"
           "        Specify Python-format output filename (default disabled)\n\n\n"
           "      --output-all-streams\n\n"
           "        Output all covering streams elements\n\n\n"
# endif
#endif
#if USE_DB
           "     Database options:\n"
           "      --db <filename>\n\n"
           "        Sqlite database to use (default disabled)\n\n\n"
           "      --subset <filename>\n\n"
           "      --seed <seed>\n\n"
           "      --initialise-db\n\n\n"
#endif
           );
}

void CommandLine::die (const char *msg) const {
    if (msg)
        std::cerr << msg << "\n\n";

    syntax (std::cerr);
    exit (1);
}

unsigned
CommandLine::read_uint_arg (const char *arg_name, const char *arg) const
{
    assert (arg_name && arg);

    // We want to use strtoul, but want to catch leading '-' signs and
    // disallow them (to avoid "hey, that's too big" when given -1).
    while (isspace (* arg)) ++ arg;
    bool is_neg = arg [0] == '-';

    const char *problem = NULL;
    char *endptr;
    errno = 0;
    unsigned long num = strtoul (arg, & endptr, 0);
    if (arg [0] == '\0' || *endptr != '\0') {
        problem = "not a non-negative integer";
    } else if (errno == ERANGE) {
        problem = is_neg ? "negative" : "too large";
    } else if (std::numeric_limits<unsigned>::max () < num) {
        problem = is_neg ? "negative" : "too large";
    }

    if (problem) {
        std::cerr << "Failed to parse " << arg_name << " argument: `"
                  << arg << "' is " << problem << ".\n";
        die (NULL);
    }

    return static_cast<unsigned> (num);
}

void
CommandLine::parse (int argc, char **argv)
{
    static const int LARGE_SCALE_TILE_IDX   = 256;
    static const int NO_FILM_GRAIN_IDX      = 257;
    static const int NOT_ANNEXB_IDX         = 258;
    static const int ANNEXB_IDX             = 259;
#if !RELEASE
    static const int DISPLAY_FRAME_RATE_IDX = 260;
    static const int DUMP_IDX               = 262;
    static const int DUMP_BASE_IDX          = 263;
    static const int CHECK_IDX              = 264;
    static const int CHECK_BASE_IDX         = 265;
# if USE_PROFILE
    static const int PROFILE_PYFILE_IDX     = 266;
    static const int PROFILE_OOS_IDX        = 267;
# endif
#endif // !RELEASE
#if USE_DB
    static const int DB_DB_IDX              = 268;
    static const int DB_SUBSET_IDX          = 269;
    static const int DB_SEED_IDX            = 270;
    static const int DB_INIT_IDX            = 271;
#endif
    static const int OPERATING_POINT_IDX    = 272;
#if !RELEASE
#if !COVER
    static const int MAX_NUM_OPPOINTS_IDX   = 273;
#endif // !RELEASE
#endif // !COVER
#if !COVER
    static const int ESTIMATE_CMD_IDX       = 274;
#endif // !COVER

    static struct option long_options[] = {
        { "frame-limit",        required_argument, 0, 'f' },
        { "inputs",             required_argument, 0, 'T' },
        { "large-scale-tile",   no_argument,       0, LARGE_SCALE_TILE_IDX },
        { "no-film-grain",      no_argument,       0, NO_FILM_GRAIN_IDX },
        { "oppoint",            required_argument, 0, OPERATING_POINT_IDX },
#if !COVER
        { "estimate-cmd",       no_argument,       0, ESTIMATE_CMD_IDX },
#endif // !COVER
        { "not-annexb",         no_argument,       0, NOT_ANNEXB_IDX },
        { "annexb",             no_argument,       0, ANNEXB_IDX },
#if !RELEASE
#if !COVER
        { "max-num-oppoints",   no_argument,       0, MAX_NUM_OPPOINTS_IDX },
        { "display-frame-rate", required_argument, 0, DISPLAY_FRAME_RATE_IDX },
#endif // !COVER
        { "dump",               optional_argument, 0, DUMP_IDX },
        { "dump-base",          required_argument, 0, DUMP_BASE_IDX },
        { "check",              optional_argument, 0, CHECK_IDX },
        { "check-base",         required_argument, 0, CHECK_BASE_IDX },
#endif // !RELEASE
#if WRITE_YUV
        { "output",    required_argument, 0, 'o' },
        { "bit-depth", required_argument, 0, 'd' },
#endif
#if USE_PROFILE
        { "profile",            required_argument, 0, 'p' },
# if !RELEASE
        { "pyf",                required_argument, 0, PROFILE_PYFILE_IDX },
        { "output-all-streams", no_argument,       0, PROFILE_OOS_IDX },
# endif
#endif
#if USE_DB
        { "db",                 required_argument, 0, DB_DB_IDX },
        { "subset",             required_argument, 0, DB_SUBSET_IDX },
        { "seed",               required_argument, 0, DB_SEED_IDX },
        { "initialise-db",      no_argument,       0, DB_INIT_IDX },
#endif
        { "help",               no_argument,       0, 'h' },
        { "verbose",            no_argument,       0, 'v' },
        { NULL,                 0,                 0, 0 },
    };

    // The '-' at the start means to read options in order (which
    // means that --not-annexb A --annexb B will parse A without
    // annex-b and B with).
    static const char *short_options =
        "-f:T:"
#if WRITE_YUV
        "o:d:"
#endif
#if USE_PROFILE
        "p:"
#endif
        "bhv";

    // Parse options with getopt
    while (optind < argc) {
        int option_index = 0;
        int c = getopt_long (argc, argv,
                             short_options, long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
        case 1:
            // Positional argument. Read it.
            add_input_file (argv [optind - 1]);
            break;

        case 'f':
            frame_limit = read_uint_arg ("frame-limit", optarg);
            break;

        case 'T':
            read_file_list (optarg);
            break;

        case LARGE_SCALE_TILE_IDX:
            // --large-scale-tile is a little odd and expects 3
            // arguments (num anchor frames, num tile list obus, input
            // file). We've told getopt that it takes none, so we eat
            // three manually and then update optind to match.
            //
            // Index of next elt is optind, so the arguments we need
            // to read are argv[optind], argv[optind+1] and
            // argv[optind+2].
            if (! (optind + 2 < argc))
                die ("Not enough arguments for --large-scale-tile.");

            add_input_file (argv [optind + 2],
                            read_uint_arg ("num anchor frames",
                                           argv [optind + 0]),
                            read_uint_arg ("num tile list obus",
                                           argv [optind + 1]));

            optind += 3;
            break;

        case NO_FILM_GRAIN_IDX:
            override_no_film_grain = true;
            break;

        case OPERATING_POINT_IDX:
            operating_point = read_uint_arg ("oppoint", optarg);
            break;
#if !COVER
        case ESTIMATE_CMD_IDX:
            estimate_command = true;
            break;
#endif // !COVER

        case NOT_ANNEXB_IDX:
            is_annex_b = false;
            break;

        case ANNEXB_IDX:
            is_annex_b = true;
            break;

#if !RELEASE
#if !COVER
        case MAX_NUM_OPPOINTS_IDX:
            max_num_oppoints = 0; // 0 means active
            break;
#endif // !COVER
        case DISPLAY_FRAME_RATE_IDX:
            display_frame_rate = read_uint_arg ("display frame rate", optarg);
            break;

        case DUMP_IDX:
            read_dump_flags (optarg);
            break;

        case DUMP_BASE_IDX:
            dump_base = optarg;
            break;

        case CHECK_IDX:
            read_check_flags (optarg);
            break;

        case CHECK_BASE_IDX:
            check_base = optarg;
            break;
#endif // !RELEASE

#if WRITE_YUV
        case 'o':
            yuv_output_file_name = optarg;
            break;

        case 'd':
            take_bit_depth (optarg);
            break;
#endif

#if USE_PROFILE
        case 'p':
            prof_xml_file = optarg;
            break;

# if !RELEASE
        case PROFILE_PYFILE_IDX:
            prof_py_file = optarg;
            break;

        case PROFILE_OOS_IDX:
            prof_output_all_streams = true;
            break;
# endif
#endif

#if USE_DB
        case DB_DB_IDX:
            db_url = optarg;
            break;

        case DB_SUBSET_IDX:
            db_subset = read_uint_arg ("database subset", optarg);
            break;

        case DB_SEED_IDX:
            db_seed = read_uint_arg ("database seed", optarg);
            break;

        case DB_INIT_IDX:
            db_init = true;
            break;
#endif
        case 'h':
            syntax (std::cout);
            exit (0);
            break;

        case 'v':
            verbose = true;
            break;

        case 'b':
            // Ignore '-b' options for compatibility with reference decoder
            break;

        case '?':
            // Invalid option or missing argument.
            die ("Failed to parse command line.");
            break;

        default:
            // Huh? That shouldn't ever happen
            assert (0);
        }
    }
    //There must be at least one input file
    if (input_files.size () < 1) {
      die ("There must be at least one input file");
    }

    // If dump_base is set and --dump wasn't used to set specific
    // features, turn everything on.
    if (dump_base && ! dump_flags) {
        dump_flags = CD_ALL;
    }

    // If dump_flags are nonzero and dump_base wasn't set, the default
    // is just "av1b" (generating files like av1b.<stream_num>.cabac.log)
    if (dump_flags && ! dump_base) {
        dump_base = "av1b";
    }

    // It is an error to set any check flags without setting check-base.
    if (check_flags && ! check_base) {
        die ("--check passed without --check-base.");
    }

    // If check_base is set and check_flags are not, we leave them
    // zero and treat "all zero" as a special case when trying to load
    // check files later.

    // If check_base is set, we must have exactly one input file. If
    // we had more, we wouldn't know where to read the check streams
    // from (if this is a problem, we could allow e.g. comma-separated
    // arguments for the --check parameter).
    if (check_base && (input_files.size () != 1)) {
        die ("When checking is enabled, "
             "there must be exactly one input file.");
    }

#if USE_PROFILE
# if USE_DB
    if (db_url) {
        if (db_init) {
            if (input_files.size () > 0) {
                die ("--initialise-db supplied as well as stream file(s) - "
                 "please supply only one or the other.");
            }
        } else {
            if (input_files.size () > 1) {
                die ("Cannot output to database when "
                 "more than one stream file supplied.");
            }
            if (db_subset < 0)
                die ("Must supply --subset <subset> for output to database.");
            if (db_seed < 0)
                die ("Must supply --seed <seed> for output to database.");
        }
    } else {
        if (db_init)
            die ("--initialise-db supplied but no --db <url>.");
    }
# endif
#endif
}

void
CommandLine::add_input_file (const char *path,
                             int num_lst_anchor_frames,
                             int num_lst_tile_list_obus)
{
    input_files.push_back (InputFile (path,
                                      is_annex_b,
                                      num_lst_anchor_frames,
                                      num_lst_tile_list_obus));
}

void
CommandLine::read_file_list (const char *path)
{
    if (0 == strcmp (path, "-")) {
        read_file_list_stream (std::cin);
    } else {
        std::ifstream lst (path, std::ifstream::in);
        if (! lst.good ()) {
            std::ostringstream oss;
            oss << "Can't open file list at `" << path << "'.";
            die (oss.str ().c_str ());
        }
        read_file_list_stream (lst);
    }
}

void
CommandLine::read_file_list_stream (std::istream &is)
{
    std::string line;
    while (std::getline (is, line)) {
        input_files.push_back (InputFile (line, is_annex_b));
    }
}

void
CommandLine::read_flags (int *dest,
                         const char *str,
                         const char *what)
{
    const char *end = str + strlen (str);
    do {
        const char *comma = strchr (str, ',');
        const char *word_end = comma ? comma : end;
        const char *next_word = comma ? comma + 1 : end;

        // The word we want starts at str and is word_len characters long.
        assert (str < word_end);
        size_t word_len = word_end - str;

        if (word_len == 3 && 0 == strncmp ("all", str, 3)) {
            * dest |= CD_ALL;
        } else if (word_len == 5 && 0 == strncmp ("cabac", str, 5)) {
            * dest |= CD_CABAC;
        } else if (word_len == 2 && 0 == strncmp("mv", str, 2)) {
            * dest |= CD_MV;
        } else if (word_len == 5 && 0 == strncmp("intra", str, 5)) {
            * dest |= CD_INTRA;
        } else if (word_len == 6 && 0 == strncmp("txform", str, 6)) {
            * dest |= CD_TXFORM;
        } else {
            std::ostringstream oss;
            oss << "Unknown " << what << " type: `"
                << std::string (str, word_len)
                << "'. (Expected one of [all, cabac, mv, intra, txform]).";
            die (oss.str ().c_str ());
        }

        str = next_word;
    } while (str < end);
}

void
CommandLine::read_dump_flags (const char *arg)
{
    if (! arg) {
        dump_flags = CD_ALL;
        return;
    }

    read_flags (& dump_flags, arg, "dump");
}

void
CommandLine::read_check_flags (const char *arg)
{
    if (! arg) {
        // This is the special case value of "load anything going".
        check_flags = 0;
        return;
    }

    read_flags (& check_flags, arg, "check");
}

#if WRITE_YUV
void
CommandLine::take_bit_depth (const char *arg)
{
    if (0 == strcmp ("8", arg)) {
        output_bytes = 1;
    } else if (0 == strcmp ("16", arg)) {
        output_bytes = 2;
    } else {
        die ("Invalid output bit depth (should be 8 or 16).");
    }
}
#endif
