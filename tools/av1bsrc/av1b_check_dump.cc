/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

//#line 2 "av1b_check_dump.cc"

#include <iostream>
#include <fstream>
#include <sstream>

// Utility code for dumping debug/validation information about the
// current frame.

enum CheckDumpFlags {
    CD_CABAC  =  1,
    CD_MV     =  2,
    CD_INTRA  =  4,
    CD_TXFORM =  8,
    CD_ALL    = 15
};

class CheckDumpInfo
{
public:
    CheckDumpInfo (const char *dump_base,
                   int         dump_flags,
                   const char *check_base,
                   int         check_flags,
                   unsigned    file_idx);

    // Check or dump a symbol in the cabac stream.
    void cabac_symbol (int symbol, int nsyms, int rng, int bits);

    // Log a motion vector
    void log_mv (int mv_x, int mv_y, int mv_n, int is_compound, int mode);

    // Intra-prediction check/dump
    void predict_intra (int plane, int x, int y,
                        int have_left, int have_above,
                        int have_above_right, int have_below_left,
                        int mode, int log2_width, int log2_height,
                        int maxX, int maxY);

    void predict_intra_directional (int angle,
                                    flat_array<int, 2> &data,
                                    int h, int w);

    // Transform check/dump
    void inverse_transform (int plane, int x, int y, int w, int h, int tx_type,
                            flat_array<int16, 3> &image, int stride);

private:
    int           dump_flags;
    int           check_flags;

    std::ofstream cabac_o;
    std::ifstream cabac_i;
    unsigned      cabac_counter;

    std::ofstream mv_o;
    std::ifstream mv_i;
    unsigned      mv_counter;

    std::ofstream intra_o;
    std::ifstream intra_i;
    unsigned      intra_counter;

    std::ofstream txform_o;
    std::ifstream txform_i;
    unsigned      txform_counter;
};

// This global pointer is set up in av1_read_file.
CheckDumpInfo *g_cd_info;

static void
open_dump_stream (std::ofstream     *stream,
                  const std::string &base,
                  const char        *suffix,
                  const char        *what)
{
    assert (stream);

    std::string path = base + suffix;
    stream->open (path);
    if (! (* stream)) {
        std::ostringstream os;
        os << "Couldn't open `" << path << "' to dump "
           << what << " data.";
        throw std::runtime_error (os.str ());
    }
}

static void
open_check_stream (std::ifstream     *stream,
                   const std::string &base,
                   const char        *suffix,
                   bool               force,
                   const char        *what)
{
    assert (stream);

    std::string path = base + suffix;
    stream->open (path);
    if (force && ! (* stream)) {
        std::ostringstream os;
        os << "Couldn't open `" << path << "' to check "
           << what << " data.\n";
        throw std::runtime_error (os.str ());
    }
}

static void
dump_and_check (std::ofstream     &dump,
                std::ifstream     &check,
                const std::string &line,
                unsigned           counter,
                const char        *what)
{
    if (dump.is_open ())
        dump << line << "\n";

    if (check.is_open ()) {
        std::string read_line;
        std::getline (check, read_line);
        if (check.bad ()) {
            std::ostringstream os;
            os << "Failed to read " << what
               << " check line for entry " << counter << ".";
            throw std::runtime_error (os.str ());
        }

        if (read_line != line) {
            std::ostringstream os;
            os << "Mismatch for " << what << " at entry " << counter
               << ".\nWe got:    `" << line
               << "'\nInput was: `" << read_line << "'\n";
            throw std::runtime_error (os.str ());
        }
    }
}

CheckDumpInfo::CheckDumpInfo (const char *dump_base,
                              int         dump_flags,
                              const char *check_base,
                              int         check_flags,
                              unsigned    file_idx)
    : dump_flags (dump_flags),
      check_flags (check_flags),
      cabac_counter (0),
      mv_counter (0),
      intra_counter (0),
      txform_counter (0)
{
    // dump_base is set iff dump_flags is nonzero.
    assert ((dump_flags == 0) == (dump_base == NULL));

    if (dump_base) {
        std::ostringstream db_stream;
        db_stream << dump_base << "." << file_idx;

        if (dump_flags & CD_CABAC) {
            open_dump_stream (& cabac_o, db_stream.str (),
                              ".cabac.log", "cabac");
        }

        if (dump_flags & CD_MV) {
            open_dump_stream (& mv_o, db_stream.str (),
                              ".mv.log", "motion vector");
        }

        if (dump_flags & CD_INTRA) {
            open_dump_stream (& intra_o, db_stream.str (),
                              ".intra.log", "intra-prediction");
        }

        if (dump_flags & CD_TXFORM) {
            open_dump_stream (& txform_o, db_stream.str (),
                              ".txform.log", "transform");
        }
    }

    if (check_base) {
        std::string cb (check_base);

        if ((check_flags & CD_CABAC) || check_flags == 0) {
            open_check_stream (& cabac_i, check_base,
                               ".cabac.log", check_flags != 0, "cabac");
        }

        if ((check_flags & CD_MV) || check_flags == 0) {
            open_check_stream (& mv_i, check_base,
                               ".mv.log", check_flags != 0, "mv");
        }

        if ((check_flags & CD_INTRA) || check_flags == 0) {
            open_check_stream (& intra_i, check_base,
                               ".intra.log", check_flags != 0,
                               "intra-prediction");
        }

        if ((check_flags & CD_TXFORM) || check_flags == 0) {
            open_check_stream (& txform_i, check_base,
                               ".txform.log", check_flags != 0,
                               "transform");
        }
    }
}

void
CheckDumpInfo::cabac_symbol (int symbol, int nsyms, int rng, int bits)
{
    if (! (cabac_o.is_open () || cabac_i.is_open ()))
        return;

    std::ostringstream os;
    os << cabac_counter
       << " bin=" << symbol << "/" << nsyms
       << ", rng=0x" << std::hex << rng << std::dec
       << ", bits=" << bits;

    dump_and_check (cabac_o, cabac_i, os.str (), cabac_counter, "cabac");
    ++ cabac_counter;
}

void
CheckDumpInfo::log_mv (int mv_x, int mv_y, int mv_n, int is_compound, int mode)
{
    if (! (mv_o.is_open () || mv_i.is_open ()))
        return;

    std::ostringstream os;
    os << mv_counter
       << ", mv_n = " << mv_n
       << ", is_compound = " << is_compound
       << ", mode = " << mode
       << ", Vector = (" << mv_x << ", " << mv_y <<  ")";

    dump_and_check (mv_o, mv_i, os.str (), mv_counter, "mv");
    ++ mv_counter;
}

void
CheckDumpInfo::predict_intra (int plane, int x, int y,
                              int have_left, int have_above,
                              int have_above_right, int have_below_left,
                              int mode, int log2_width, int log2_height,
                              int maxX, int maxY)
{
    if (! (intra_o.is_open () || intra_i.is_open ()))
        return;

    std::ostringstream os;
    os << intra_counter
       << " predict_intra(plane=" << plane
       << ", x=" << x << ", y=" << y << ", have=[";
    bool comma = false;
    if (have_left) {
        os << "L";
        comma = true;
    }
    if (have_above) {
        if (comma) os << ", ";
        os << "A";
        comma = true;
    }
    // The AR and BL flags get set slightly strangely in the AV1B code
    // (and in the spec). However, they are ignored unless A or L
    // respectively is set.
    //
    // Similarly, if the bottom of the block is at or below the bottom
    // of the image, the BL flag has no effect (see section 7.11.2.1
    // of the spec). In the same way the AR flag has no effect when
    // the right of the block is at or past the right of the image.
    //
    // Since it makes more sense to only say AR when there is indeed a
    // sample above and right, we filter here. This makes it much
    // easier to match with an implementation that calculates the
    // flags based on current position rather than a giant array.
    //
    // Also, we ignore AR and BL when the mode is DC_PRED. This is
    // fine because DC predictions don't use those flags (it also
    // makes it easier to match with dav1d).
    int block_max_x = x + (1 << log2_width);
    int block_max_y = y + (1 << log2_height);

    if (have_above_right &&
        have_above && block_max_x < maxX && mode != DC_PRED) {
        if (comma) os << ", ";
        os << "AR";
        comma = true;
    }
    if (have_below_left &&
        have_left && block_max_y < maxY && mode != DC_PRED) {
        if (comma) os << ", ";
        os << "BL";
        comma = true;
    }
    os << "], mode=" << mode << ", lg2_width="
       << log2_width << ", lg2_height=" << log2_height << ")";

    dump_and_check (intra_o, intra_i, os.str (), intra_counter, "intra");
    ++ intra_counter;
}

void
CheckDumpInfo::predict_intra_directional (int angle,
                                          flat_array<int, 2> &data,
                                          int h, int w)
{
    if (! (intra_o.is_open () || intra_i.is_open ()))
        return;

    std::ostringstream os;
    os << intra_counter
       << "  directional(angle=" << angle << ", h=" << h << ", w=" << w
       << "): [";
    os << std::hex;
    for (int y = 0; y < h; ++ y) {
        os << "[";
        for (int x = 0; x < w; ++ x) {
            if (x) os << ", ";
            os << data[y][x];
        }
        os << "]";
    }
    os << "]";
    os << std::dec;

    dump_and_check (intra_o, intra_i, os.str (), intra_counter, "intra");
    ++ intra_counter;
}

void
CheckDumpInfo::inverse_transform (int plane, int x, int y, int w, int h,
                                  int tx_type,
                                  flat_array<int16, 3> &image, int stride)
{
    if (! (txform_o.is_open () || txform_i.is_open ()))
        return;

    std::ostringstream os;
    os << txform_counter
       << " inverse_transform(plane=" << plane
       << ", x=" << x << ", y=" << y
       << ", w=" << w << ", h=" << h
       << ", tx_type=" << tx_type << "): [";
    os << std::hex;
    for (int dy = 0; dy < h; ++ dy) {
        os << "[";
        for (int dx = 0; dx < w; ++ dx) {
            if (dx) os << ", ";
            os << image[plane][y + dy][x + dx];
        }
        os << "]";
    }
    os << "]";
    os << std::dec;

    dump_and_check (txform_o, txform_i, os.str (), txform_counter, "txform");
    ++ txform_counter;
}
