/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#if USE_PROFILE
enum {
  //  VALUE_CABAC_TILE_SIZE_MINUS_1, -- not useful to cover this
  //  VALUE_CABAC_METADATA_TYPE, -- covered by branch coverage in metadata_obu()
  VALUE_CABAC_LOOP_FILTER_REF_DELTAS_INTRA,
  VALUE_CABAC_LOOP_FILTER_REF_DELTAS_LAST,
  VALUE_CABAC_LOOP_FILTER_REF_DELTAS_LAST2,
  VALUE_CABAC_LOOP_FILTER_REF_DELTAS_LAST3,
  VALUE_CABAC_LOOP_FILTER_REF_DELTAS_GOLDEN,
  VALUE_CABAC_LOOP_FILTER_REF_DELTAS_BWDREF,
  VALUE_CABAC_LOOP_FILTER_REF_DELTAS_ALTREF2,
  VALUE_CABAC_LOOP_FILTER_REF_DELTAS_ALTREF,
  VALUE_CABAC_LOOP_FILTER_MODE_DELTAS_MODE0,
  VALUE_CABAC_LOOP_FILTER_MODE_DELTAS_MODE1,
  VALUE_CABAC_DELTAQYDc,
  VALUE_CABAC_DELTAQUDc,
  VALUE_CABAC_DELTAQUAc,
  VALUE_CABAC_DELTAQVDc,
  VALUE_CABAC_DELTAQVAc,
  //  VALUE_CABAC_FEATURE_VALUE_SIGNED, -- covered by VALUE_SEG_LVL_*
  //  VALUE_CABAC_FEATURE_VALUE_UNSIGNED, -- covered by VALUE_SEG_LVL_*
  VALUE_CABAC_DELTA_Q_REM_BITS,
  VALUE_CABAC_DELTA_Q_ABS_BITS,
  VALUE_CABAC_DELTA_Q_SIGN_BIT,
  VALUE_CABAC_DELTA_LF_REM_BITS,
  VALUE_CABAC_DELTA_LF_ABS_BITS,
  VALUE_CABAC_DELTA_LF_SIGN_BIT,
  VALUE_CABAC_WEDGE_INDEX,
  VALUE_CABAC_WEDGE_SIGN,
  VALUE_CABAC_MASK_TYPE,
  VALUE_CABAC_SIGN_BIT,
  VALUE_CABAC_USE_PALETTE_COLOR_CACHE_Y,
  VALUE_CABAC_PALETTE_COLORS_Y,
  VALUE_CABAC_PALETTE_NUM_EXTRA_BITS_Y,
  VALUE_CABAC_PALETTE_DELTA_Y,
  VALUE_CABAC_USE_PALETTE_COLOR_CACHE_U,
  VALUE_CABAC_PALETTE_COLORS_U,
  VALUE_CABAC_PALETTE_NUM_EXTRA_BITS_U,
  VALUE_CABAC_PALETTE_DELTA_U,
  VALUE_CABAC_DELTA_ENCODE_PALETTE_COLORS_V,
  VALUE_CABAC_PALETTE_NUM_EXTRA_BITS_V,
  VALUE_CABAC_PALETTE_COLORS_V,
  VALUE_CABAC_PALETTE_DELTA_V,
  VALUE_CABAC_PALETTE_DELTA_SIGN_BIT_V,
  VALUE_CABAC_CDEF_IDX,
  VALUE_CABAC_ALL_ZERO,
  VALUE_CABAC_EOB_PT_16,
  VALUE_CABAC_EOB_PT_32,
  VALUE_CABAC_EOB_PT_64,
  VALUE_CABAC_EOB_PT_128,
  VALUE_CABAC_EOB_PT_256,
  VALUE_CABAC_EOB_PT_512,
  VALUE_CABAC_EOB_PT_1024,
  VALUE_CABAC_EOB_EXTRA,
  VALUE_CABAC_EOB_EXTRA_BIT,
  VALUE_CABAC_COEFF_BASE,
  VALUE_CABAC_COEFF_BASE_EOB,
  VALUE_CABAC_DC_SIGN,
  VALUE_CABAC_COEFF_BR,
  VALUE_CABAC_GOLOMB_LENGTH_BIT,
  VALUE_CABAC_GOLOMB_DATA_BIT,
  VALUE_CABAC_ANGLE_DELTA_Y,
  VALUE_CABAC_ANGLE_DELTA_UV,
  VALUE_CABAC_COLOR_INDEX_MAP_Y,
  VALUE_CABAC_COLOR_INDEX_MAP_UV,
  VALUE_CABAC_PARTITION,
  VALUE_CABAC_SPLIT_OR_HORZ,
  VALUE_CABAC_SPLIT_OR_VERT,
  VALUE_CABAC_USE_INTRABC,
  VALUE_CABAC_INTRA_FRAME_Y_MODE,
  VALUE_CABAC_UV_MODE,
  VALUE_CABAC_SEGMENT_ID,
  VALUE_CABAC_SKIP_MODE,
  VALUE_CABAC_SKIP,
  VALUE_CABAC_DELTA_Q_ABS,
  VALUE_CABAC_DELTA_LF_ABS,
  VALUE_CABAC_TX_DEPTH,
  VALUE_CABAC_TXFM_SPLIT,
  VALUE_CABAC_SEG_ID_PREDICTED,
  VALUE_CABAC_IS_INTER,
  VALUE_CABAC_USE_FILTER_INTRA,
  VALUE_CABAC_FILTER_INTRA_MODE,
  VALUE_CABAC_Y_MODE,
  VALUE_CABAC_COMPOUND_MODE,
  VALUE_CABAC_NEW_MV,
  VALUE_CABAC_ZERO_MV,
  VALUE_CABAC_REF_MV,
  VALUE_CABAC_DRL_MODE,
  VALUE_CABAC_INTERP_FILTER,
  VALUE_CABAC_COMP_MODE,
  VALUE_CABAC_COMP_REF_TYPE,
  VALUE_CABAC_UNI_COMP_REF,
  VALUE_CABAC_UNI_COMP_REF_P1,
  VALUE_CABAC_UNI_COMP_REF_P2,
  VALUE_CABAC_COMP_REF,
  VALUE_CABAC_COMP_REF_P1,
  VALUE_CABAC_COMP_REF_P2,
  VALUE_CABAC_COMP_BWDREF,
  VALUE_CABAC_COMP_BWDREF_P1,
  VALUE_CABAC_SINGLE_REF_P1,
  VALUE_CABAC_SINGLE_REF_P2,
  VALUE_CABAC_SINGLE_REF_P3,
  VALUE_CABAC_SINGLE_REF_P4,
  VALUE_CABAC_SINGLE_REF_P5,
  VALUE_CABAC_SINGLE_REF_P6,
  VALUE_CABAC_USE_OBMC,
  VALUE_CABAC_MOTION_MODE,
  VALUE_CABAC_INTERINTRA,
  VALUE_CABAC_INTERINTRA_MODE,
  VALUE_CABAC_WEDGE_INTERINTRA,
  VALUE_CABAC_COMP_GROUP_IDX,
  VALUE_CABAC_COMPOUND_IDX,
  VALUE_CABAC_COMPOUND_TYPE,
  VALUE_CABAC_MV_JOINT,
  VALUE_CABAC_MV_SIGN,
  VALUE_CABAC_MV_CLASS,
  VALUE_CABAC_MV_CLASS0_BIT,
  VALUE_CABAC_MV_CLASS0_FR,
  VALUE_CABAC_MV_CLASS0_HP,
  VALUE_CABAC_MV_BIT,
  VALUE_CABAC_MV_FR,
  VALUE_CABAC_MV_HP,
  VALUE_CABAC_CFL_ALPHA_SIGNS,
  VALUE_CABAC_CFL_ALPHA_U,
  VALUE_CABAC_CFL_ALPHA_V,
  VALUE_CABAC_HAS_PALETTE_Y,
  VALUE_CABAC_PALETTE_SIZE_Y_MINUS_2,
  VALUE_CABAC_HAS_PALETTE_UV,
  VALUE_CABAC_PALETTE_SIZE_UV_MINUS_2,
  VALUE_CABAC_INTER_TX_TYPE,
  VALUE_CABAC_INTRA_TX_TYPE,
  VALUE_CABAC_PALETTE_COLOR_IDX_Y,
  VALUE_CABAC_PALETTE_COLOR_IDX_UV,
  VALUE_CABAC_USE_WIENER,
  VALUE_CABAC_USE_SGRPROJ,
  VALUE_CABAC_RESTORATION_TYPE,
  VALUE_CABAC_LR_SGR_SET,
  VALUE_CABAC_SUBEXP_UNIF_BOOLS,
  VALUE_CABAC_SUBEXP_MORE_BOOLS,
  VALUE_CABAC_SUBEXP_BOOLS,
  //  VALUE_CABAC_WIDTH_IN_SBS_MINUS_1,  -- not useful to cover this this way
  //  VALUE_CABAC_HEIGHT_IN_SBS_MINUS_1,  -- not useful to cover this this way
  //  VALUE_CABAC_SUBEXP_FINAL_BITS, -- covered by function which call decode_*_subexp
  //  VALUE_CABAC_SUBEXP_BITS, -- covered by function which call decode_*_subexp
  VALUE_INTRA_DIRECTIONAL_PANGLE,
  VALUE_LR_WIENER,
  VALUE_LR_SGR_XQD,
  VALUE_GM_PARAMS_PRE_SHIFT,
  CROSS_TILE_WIDTH_SB,
  CROSS_TILE_AREA_SB,

  CROSS_PARTITION_CDF,
  CROSS_SPLIT_OR_HORZ_CDF,
  CROSS_SPLIT_OR_VERT_CDF,
  CROSS_INTRA_FRAME_Y_MODE_CDF,
  CROSS_UV_MODE_CFL_ALLOWED_CDF,
  CROSS_UV_MODE_CFL_NOT_ALLOWED_CDF,
  CROSS_TILE_SEGMENT_ID_CDF,
  CROSS_TILE_SKIP_MODE_CDF,
  CROSS_TILE_SKIP_CDF,
  VALUE_TILE_DELTA_Q_ABS,
  VALUE_TILE_DELTA_LF_SINGLE_CDF,
  CROSS_TILE_DELTA_LF_MULTI_CDF,
  CROSS_MAX_TX_DEPTH_CDF,
  CROSS_TXFM_SPLIT_CDF,
  CROSS_SEGMENT_ID_PREDICTED_CDF,
  CROSS_IS_INTER_CDF,
  VALUE_FILTER_INTRA_MODE,
  CROSS_FILTER_INTRA_CDF,
  CROSS_Y_MODE_CDF,
  CROSS_COMPOUND_MODE_CDF,
  CROSS_NEW_MV_CDF,
  CROSS_ZERO_MV_CDF,
  CROSS_REF_MV_CDF,
  CROSS_DRL_MODE_CDF,
  CROSS_INTERP_FILTER_CDF,
  CROSS_COMP_MODE_CDF,
  CROSS_COMP_REF_TYPE_CDF,
  CROSS_UNI_COMP_REF_0_CDF,
  CROSS_UNI_COMP_REF_1_CDF,
  CROSS_UNI_COMP_REF_2_CDF,
  CROSS_COMP_REF_0_CDF,
  CROSS_COMP_REF_1_CDF,
  CROSS_COMP_REF_2_CDF,
  CROSS_COMP_BWDREF_0_CDF,
  CROSS_COMP_BWDREF_1_CDF,
  CROSS_COMP_SINGLE_REF_0_CDF,
  CROSS_COMP_SINGLE_REF_1_CDF,
  CROSS_COMP_SINGLE_REF_2_CDF,
  CROSS_COMP_SINGLE_REF_3_CDF,
  CROSS_COMP_SINGLE_REF_4_CDF,
  CROSS_COMP_SINGLE_REF_5_CDF,
  CROSS_USE_OBMC_CDF,
  CROSS_MOTION_MODE_CDF,
  CROSS_INTER_INTRA_CDF,
  CROSS_INTER_INTRA_MODE_CDF,
  CROSS_WEDGE_INDEX_CDF,
  CROSS_WEDGE_INTERINTRA_CDF,
  CROSS_COMP_GROUP_IDX_CDF,
  CROSS_COMPOUND_IDX_CDF,
  CROSS_COMPOUND_TYPE_CDF,
  CROSS_MV_JOINT_CDF,
  CROSS_MV_SIGN_CDF,
  CROSS_MV_CLASS_CDF,
  CROSS_MV_CLASS0_BIT_CDF,
  CROSS_MV_CLASS0_FR_CDF,
  CROSS_MV_CLASS0_HP_CDF,
  CROSS_MV_BIT_CDF,
  CROSS_MV_FR_CDF,
  CROSS_MV_HP_CDF,
  CROSS_TXB_SKIP_CDF,
  CROSS_EOB_PT_16_CDF,
  CROSS_EOB_PT_32_CDF,
  CROSS_EOB_PT_64_CDF,
  CROSS_EOB_PT_128_CDF,
  CROSS_EOB_PT_256_CDF,
  CROSS_EOB_PT_512_CDF,
  CROSS_EOB_PT_1024_CDF,
  CROSS_EOB_EXTRA_CDF,
  CROSS_COEFF_BASE_CDF,
  CROSS_COEFF_BASE_EOB_CDF,
  CROSS_DC_SIGN_CDF,
  CROSS_COEFF_BR_CDF,
  VALUE_CFL_ALPHA_SIGNS,
  CROSS_CFL_ALPHA_U_CDF,
  CROSS_CFL_ALPHA_V_CDF,
  CROSS_PALETTE_Y_MODE_CDF,
  CROSS_PALETTE_Y_SIZE_CDF,
  CROSS_PALETTE_UV_MODE_CDF,
  CROSS_PALETTE_UV_SIZE_CDF,
  CROSS_INTER_TX_TYPE_SET1_CDF,
  VALUE_INTER_TX_TYPE_SET2_CDF,
  CROSS_INTER_TX_TYPE_SET3_CDF,
  CROSS_INTRA_TX_TYPE_SET1_CDF,
  CROSS_INTRA_TX_TYPE_SET2_CDF,
  CROSS_PALETTE_SIZE_2_Y_COLOR_CDF,
  CROSS_PALETTE_SIZE_3_Y_COLOR_CDF,
  CROSS_PALETTE_SIZE_4_Y_COLOR_CDF,
  CROSS_PALETTE_SIZE_5_Y_COLOR_CDF,
  CROSS_PALETTE_SIZE_6_Y_COLOR_CDF,
  CROSS_PALETTE_SIZE_7_Y_COLOR_CDF,
  CROSS_PALETTE_SIZE_8_Y_COLOR_CDF,
  CROSS_PALETTE_SIZE_2_UV_COLOR_CDF,
  CROSS_PALETTE_SIZE_3_UV_COLOR_CDF,
  CROSS_PALETTE_SIZE_4_UV_COLOR_CDF,
  CROSS_PALETTE_SIZE_5_UV_COLOR_CDF,
  CROSS_PALETTE_SIZE_6_UV_COLOR_CDF,
  CROSS_PALETTE_SIZE_7_UV_COLOR_CDF,
  CROSS_PALETTE_SIZE_8_UV_COLOR_CDF,
  VALUE_RESTORATION_TYPE,
  CROSS_ANGLE_DELTA_Y_CDF,
  CROSS_ANGLE_DELTA_UV_CDF,

  CROSS_LOSSLESS,
  CROSS_SEG_FEATURES,
  CROSS_FILM_GRAIN_POINTS,
  VALUE_NUM_LR_PARAMS_IN_SB,
  VALUE_TILE_GROUP_COUNT,
  VALUE_SCALABILITY_MODE,
  CROSS_LOSSLESS_QINDEX,
  CROSS_LEVEL_TIER,
  VALUE_SUPERRES_DENOM,
  VALUE_SEG_LVL_ALT_Q,
  VALUE_SEG_LVL_ALT_LF_Y_V,
  VALUE_SEG_LVL_ALT_LF_Y_H,
  VALUE_SEG_LVL_ALT_LF_U,
  VALUE_SEG_LVL_ALT_LF_V,
  VALUE_SEG_LVL_REF_FRAME,
  CROSS_TRANSFORM_TYPE_INTRA,
  CROSS_TRANSFORM_TYPE_INTER,
  CROSS_COMP_REF_FRAMES,
  VALUE_NUM_ANCHOR_FRAMES,
  CROSS_LF_STRENGTH,
  CROSS_NARROW_FILTER,
  CROSS_WIDE_FILTER,
  VALUE_CDEF_DIRECTION,
  CROSS_CDEF_STRENGTH,
  VALUE_SGR_PARAM_SET,
  CROSS_NUM_MV_FOUND,
  CROSS_PROFILE_SWITCH,
  VALUE_BUTTERFLY_ANGLE,
  VALUE_FRAME_TO_SHOW_MAP_IDX,
  VALUE_LAST_FRAME_IDX,
  VALUE_GOLD_FRAME_IDX,
  VALUE_REF_FRAME_IDX_0,
  VALUE_REF_FRAME_IDX_1,
  VALUE_REF_FRAME_IDX_2,
  VALUE_REF_FRAME_IDX_3,
  VALUE_REF_FRAME_IDX_4,
  VALUE_REF_FRAME_IDX_5,
  VALUE_REF_FRAME_IDX_6,
  VALUE_LOOP_FILTER_LEVEL0,
  VALUE_LOOP_FILTER_LEVEL1,
  VALUE_LOOP_FILTER_LEVEL2,
  VALUE_LOOP_FILTER_LEVEL3,
  VALUE_LOOP_FILTER_SHARPNESS,
  CROSS_QUANTIZER_MATRIX,
  VALUE_QM_OFFSET,
  VALUE_MROW_SCAN,
  VALUE_MCOL_SCAN,
  VALUE_DEFAULT_SCAN,
  VALUE_NUM_4X4_BLOCKS_WIDE,
  VALUE_NUM_4X4_BLOCKS_HIGH,
  VALUE_MI_WIDTH_LOG2,
  VALUE_MI_HEIGHT_LOG2,
  VALUE_SIZE_GROUP,
  VALUE_MAX_TX_SIZE_RECT,
  CROSS_PARTITION_SUBSIZE,
  VALUE_SPLIT_TX_SIZE,
  VALUE_MODE_TO_TXFM,
  VALUE_PALETTE_COLOR_CONTEXT,
  VALUE_PALETTE_COLOR_HASH_MULTIPLIERS,
  VALUE_SM_WEIGHTS_TX_4X4,
  VALUE_SM_WEIGHTS_TX_8X8,
  VALUE_SM_WEIGHTS_TX_16X16,
  VALUE_SM_WEIGHTS_TX_32X32,
  VALUE_SM_WEIGHTS_TX_64X64,
  VALUE_MODE_TO_ANGLE,
  VALUE_DR_INTRA_DERIVATIVE,
  CROSS_INTRA_FILTER_TAPS,
  VALUE_TX_SIZE_SQR,
  VALUE_TX_SIZE_SQR_UP,
  VALUE_TX_WIDTH,
  VALUE_TX_HEIGHT,
  VALUE_TX_WIDTH_LOG2,
  VALUE_TX_HEIGHT_LOG2,
  VALUE_WEDGE_BITS,
  CROSS_SIG_REG_DIFF_OFFSET,
  VALUE_ADJUSTED_TX_SIZE,
  VALUE_GAUSSIAN_SEQUENCE,
  VALUE_SEGMENTATION_FEATURE_BITS,
  VALUE_SEGMENTATION_FEATURE_SIGNED,
  VALUE_SEGMENTATION_FEATURE_MAX,
  VALUE_REMAP_LR_TYPE,
  VALUE_SGRPROJ_XQD_MID,
  VALUE_WIENER_TAPS_MID,
  VALUE_MAX_TX_DEPTH,
  CROSS_SUBSAMPLED_SIZE,
  CROSS_TX_TYPE_IN_SET_INTER,
  CROSS_TX_TYPE_IN_SET_INTRA,
  VALUE_TX_TYPE_INTRA_INV_SET1,
  VALUE_TX_TYPE_INTRA_INV_SET2,
  VALUE_TX_TYPE_INTER_INV_SET1,
  VALUE_TX_TYPE_INTER_INV_SET2,
  VALUE_TX_TYPE_INTER_INV_SET3,
  VALUE_WIENER_TAPS_MIN,
  VALUE_WIENER_TAPS_MAX,
  VALUE_WIENER_TAPS_K,
  VALUE_SGRPROJ_XQD_MIN,
  VALUE_SGRPROJ_XQD_MAX,
  VALUE_REF_FRAME_LIST,
  VALUE_DIV_MULT,
  CROSS_INTRA_EDGE_KERNEL,
  CROSS_SUBPEL_FILTERS,
  CROSS_WARPED_FILTERS,
  VALUE_DIV_LUT,
  VALUE_OBMC_MASK_2,
  VALUE_OBMC_MASK_4,
  VALUE_OBMC_MASK_8,
  VALUE_OBMC_MASK_16,
  VALUE_OBMC_MASK_32,
  VALUE_WEDGE_MASTER_OBLIQUE_EVEN,
  VALUE_WEDGE_MASTER_OBLIQUE_ODD,
  VALUE_WEDGE_MASTER_OBLIQUE_VERTICAL,
  CROSS_WEDGE_CODEBOOK,
  VALUE_II_WEIGHTS_1D,
  CROSS_QUANT_DIST_WEIGHT,
  CROSS_QUANT_DIST_LOOKUP,
  CROSS_DC_QLOOKUP,
  CROSS_AC_QLOOKUP,
  VALUE_COS128_LOOKUP,
  VALUE_TRANSFORM_ROW_SHIFT,
  CROSS_CDEF_UV_DIR,
  VALUE_DIV_TABLE,
  CROSS_CDEF_PRI_TAPS,
  CROSS_CDEF_SEC_TAPS,
  CROSS_CDEF_DIRECTIONS,
  CROSS_UPSCALE_FILTER,
  CROSS_SGR_PARAMS,
  VALUE_INTRA_MODE_CONTEXT,
  CROSS_COMPOUND_MODE_CTX_MAP,
  CROSS_COEFF_BASE_CTX_OFFSET,
  VALUE_COEFF_BASE_POS_CTX_OFFSET,
  CROSS_MAG_REF_OFFSET_WITH_TX_CLASS,
  VALUE_FILTER_INTRA_MODE_TO_INTRA_DIR,
  //Insert new entries here
  CROSS_ENTRY_COUNT
  //Don't insert anything here!
};

#define CROSS_MAX_ORDER 6

typedef struct {
  const char *name;
  long long range[3*CROSS_MAX_ORDER];
} CrossRange;

#include "cross_ranges.gen.c"

static int crossImpossibleCaseValues[CROSS_MAX_ORDER];

static int excluded_crosses[] = {
#if RELEASE
  CROSS_PROFILE_SWITCH,
#endif
  -1
};

typedef int (*CrossImpossibleCaseFunc) (int *crossValues, int view);

struct CrossEntry {
  const char *name;
  int order;
  const char *componentNames[CROSS_MAX_ORDER];
  CrossImpossibleCaseFunc icsFunc;
};

int view_to_profile(int view) {
  switch (view) {
    case VIEW_PROFILE0_LEVEL2:
    case VIEW_PROFILE0_LEVEL5:
    case VIEW_PROFILE0_LEVEL6:
    case VIEW_PROFILE0_CORE:
    case VIEW_PROFILE0_LARGE_SCALE_TILES:
    case VIEW_PROFILE0_NON_ANNEX_B:
    case VIEW_PROFILE0:
#ifdef PROF_SHOW_CLIENT_A
    case VIEW_PROFILE0_CLIENT_A_LEVEL2:
    case VIEW_PROFILE0_CLIENT_A_LEVEL5:
    case VIEW_PROFILE0_CLIENT_A_CORE:
    case VIEW_PROFILE0_CLIENT_A_NON_ANNEX_B:
    case VIEW_PROFILE0_CLIENT_A:
    case VIEW_POSSIBLE_CLIENT_A:
#endif
#ifdef PROF_SHOW_CLIENT_B
    case VIEW_PROFILE0_CLIENT_B_CORE:
    case VIEW_PROFILE0_CLIENT_B:
#endif
#ifdef PROF_SHOW_CLIENT_C
    case VIEW_PROFILE0_CLIENT_C_CORE:
    case VIEW_PROFILE0_CLIENT_C_LEVEL5:
    case VIEW_PROFILE0_CLIENT_C_LARGE_SCALE_TILES:
    case VIEW_PROFILE0_CLIENT_C_NON_ANNEX_B:
    case VIEW_PROFILE0_CLIENT_C:
#endif
#ifdef PROF_SHOW_CLIENT_D
    case VIEW_PROFILE0_CLIENT_D_CORE:
    case VIEW_PROFILE0_CLIENT_D:
#endif
      return 0;

    case VIEW_PROFILE1_LEVEL2:
    case VIEW_PROFILE1_LEVEL5:
    case VIEW_PROFILE1_LEVEL6:
    case VIEW_PROFILE1_CORE:
    case VIEW_PROFILE1_LARGE_SCALE_TILES:
    case VIEW_PROFILE1_NON_ANNEX_B:
    case VIEW_PROFILE1:
      return 1;

    case VIEW_PROFILE2_LEVEL2:
    case VIEW_PROFILE2_LEVEL5:
    case VIEW_PROFILE2_LEVEL6:
    case VIEW_PROFILE2_CORE:
    case VIEW_PROFILE2_LARGE_SCALE_TILES:
    case VIEW_PROFILE2_NON_ANNEX_B:
    case VIEW_PROFILE2:
    case VIEW_POSSIBLE:
    case VIEW_ALL:
      return 2;

    default:
      printf("Unrecognised view %d\n", view);
      assert(0);
  }
  assert(0);
  return -1;
}

int view_to_max_level(int view) {
  switch (view) {
    case VIEW_PROFILE0_LEVEL2:
    case VIEW_PROFILE1_LEVEL2:
    case VIEW_PROFILE2_LEVEL2:
#ifdef PROF_SHOW_CLIENT_A
    case VIEW_PROFILE0_CLIENT_A_LEVEL2:
#endif
      return 2;

    case VIEW_PROFILE0_LEVEL5:
    case VIEW_PROFILE1_LEVEL5:
    case VIEW_PROFILE2_LEVEL5:
#ifdef PROF_SHOW_CLIENT_A
    case VIEW_PROFILE0_CLIENT_A_LEVEL5:
    case VIEW_PROFILE0_CLIENT_A_CORE:
    case VIEW_PROFILE0_CLIENT_A_NON_ANNEX_B:
    case VIEW_PROFILE0_CLIENT_A:
    case VIEW_POSSIBLE_CLIENT_A:
#endif
      return 5;

    case VIEW_PROFILE0_LEVEL6:
    case VIEW_PROFILE1_LEVEL6:
    case VIEW_PROFILE2_LEVEL6:
#ifdef PROF_SHOW_CLIENT_D
    case VIEW_PROFILE0_CLIENT_D_CORE:
    case VIEW_PROFILE0_CLIENT_D:
#endif
      return 6;

    case VIEW_PROFILE0_CORE:
    case VIEW_PROFILE0_LARGE_SCALE_TILES:
    case VIEW_PROFILE0_NON_ANNEX_B:
    case VIEW_PROFILE0:
    case VIEW_PROFILE1_CORE:
    case VIEW_PROFILE1_LARGE_SCALE_TILES:
    case VIEW_PROFILE1_NON_ANNEX_B:
    case VIEW_PROFILE1:
    case VIEW_PROFILE2_CORE:
    case VIEW_PROFILE2_LARGE_SCALE_TILES:
    case VIEW_PROFILE2_NON_ANNEX_B:
    case VIEW_PROFILE2:
    case VIEW_POSSIBLE:
    case VIEW_ALL:
#ifdef PROF_SHOW_CLIENT_B
    case VIEW_PROFILE0_CLIENT_B_CORE:
    case VIEW_PROFILE0_CLIENT_B:
#endif
#ifdef PROF_SHOW_CLIENT_C
    case VIEW_PROFILE0_CLIENT_C_CORE:
    case VIEW_PROFILE0_CLIENT_C_LEVEL5:
    case VIEW_PROFILE0_CLIENT_C_LARGE_SCALE_TILES:
    case VIEW_PROFILE0_CLIENT_C_NON_ANNEX_B:
    case VIEW_PROFILE0_CLIENT_C:
#endif
      return 9;

    default:
      abort ();
  }
}

int view_max_bitdepth(int view) {
  int profile = view_to_profile(view);
  return (profile == 2) ? 12 : 10;
}

int invalid_subsampling(int profile, int subx, int suby) {
  if (profile == 0) {
    if (subx == 1 && suby == 1) {
      return 0;
    }
    return 1;
  }
  if (profile == 1) {
    if (subx == 0 && suby == 0) {
      return 0;
    }
    return 1;
  }
  if (profile == 2) {
    if (subx == 0 && suby == 1) {
      return 1;
    }
    return 0;
  }
  return 1;
}


// Used for all of {Y,U,V}
int cicf_VALUE_CABAC_PALETTE_COLORS(int *crossValues, int view)
{
  if (view == VIEW_ALL) return 0;
  int maxbd = view_max_bitdepth(view);
  return (crossValues[0] >= (1 << maxbd));
}

// Used for {Y,U} but *not* V
int cicf_VALUE_CABAC_PALETTE_DELTA_YU(int *crossValues, int view)
{
  if (view == VIEW_ALL) return 0;
  int maxbd = view_max_bitdepth(view);
  return (crossValues[0] >= (1 << maxbd));
}

// Used for V plane only - the range of palette_delta_v is
// one bit narrower than palette_delta_{y,u}
int cicf_VALUE_CABAC_PALETTE_DELTA_V(int *crossValues, int view)
{
  if (view == VIEW_ALL) return 0;
  int maxbd = view_max_bitdepth(view);
  return (crossValues[0] >= (1 << (maxbd - 1)));
}

int cicf_VALUE_INTRA_DIRECTIONAL_PANGLE(int *crossValues, int view)
{
  if (view == VIEW_ALL) return 0;
  int Mode_To_Angle[ 8 ] = { 90, 180, 45, 135, 113, 157, 203, 67 };
  for (int i = 0; i < 8; i++) {
    for (int j = -3; j <= 3; j++) {
      if (crossValues[0] == Mode_To_Angle[i] + 3*j) return 0;
    }
  }
  return 1;
}

int cicf_CROSS_TILE_WIDTH_SB (int *crossValues, int view)
{
  int isMax = crossValues [0];

  // We can always hit isMax = 0.
  if (isMax == 0)
    return 0;

#ifdef PROF_SHOW_CLIENT_A
  // ClientA-specific streams can't hit isMax = 1 because it requires
  // a tile of width strictly greater than 4096 - sb_size, which is at
  // least 3968. However, the largest frames generated for the
  // ClientA set are 3840 in width.
  if ((view == VIEW_PROFILE0_CLIENT_A_LEVEL2) ||
      (view == VIEW_PROFILE0_CLIENT_A_LEVEL5) ||
      (view == VIEW_PROFILE0_CLIENT_A_CORE) ||
      (view == VIEW_PROFILE0_CLIENT_A_NON_ANNEX_B) ||
      (view == VIEW_PROFILE0_CLIENT_A) ||
      (view == VIEW_POSSIBLE_CLIENT_A))
      return 1;
#endif

  // We can only hit isMax = 1 if level 5 or higher.
  return view_to_max_level (view) < 5;
}

int cicf_CROSS_TILE_AREA_SB (int *crossValues, int view)
{
  int isMax = crossValues [0];

  // We can always hit isMax = 0.
  if (isMax == 0)
    return 0;

  // We can only hit isMax = 1 if level 6 or higher.
  return view_to_max_level (view) < 6;
}

int cicf_CROSS_PARTITION_CDF(int *crossValues, int view)
{
  if (view == VIEW_ALL) return 0;
  if (crossValues[0] == 1 && crossValues[2] >= 4) {
    return 1;
  }
  if (crossValues[0] > 4 && crossValues[2] >= 8) {
    return 1;
  }
  return 0;
}

int cicf_CROSS_SPLIT_OR_HORZ_CDF(int *crossValues, int view)
{
  return 0;
}

int cicf_CROSS_MAX_TX_DEPTH_CDF(int *crossValues, int view)
{
  if (view == VIEW_ALL) return 0;
  if (crossValues[0] == 1 && crossValues[2] >= 2) {
    return 1;
  }
  return 0;
}

int cicf_CROSS_FILTER_INTRA_CDF(int *crossValues, int view)
{
  if (view == VIEW_ALL) return 0;
  if ((crossValues[0] >=10 && crossValues[0] <= 15) || crossValues[0] >= 20)
    return 1;
  return 0;
}

int cicf_CROSS_USE_OBMC_CDF(int *crossValues, int view)
{
  if (view == VIEW_ALL) return 0;
  if ((crossValues[0] >= 16 && crossValues[0] <= 17) || crossValues[0] <= 2)
    return 1;
  return 0;
}

int cicf_CROSS_COMPOUND_TYPE_CDF(int *crossValues, int view)
{
  if (view == VIEW_ALL) return 0;
  if (crossValues[0] <= 2 || (crossValues[0] >= 10 && crossValues[0] <= 17) || crossValues[0] >= 20)
    return 1;
  return 0;
}

int cicf_CROSS_WEDGE_INTERINTRA_CDF(int *crossValues, int view)
{
  if (view == VIEW_ALL) return 0;
  if (crossValues[0] <= 2 || crossValues[0] >= 10)
    return 1;
  return 0;
}

int cicf_CROSS_PALETTE_SIZE_2_COLOR_CDF(int *crossValues, int view)
{
  if (view == VIEW_ALL) return 0;
  return crossValues[0] == 1;
}

int cicf_CROSS_MV_CLASS_CDF(int *crossValues, int view) {
#ifdef PROF_SHOW_CLIENT_A
  const int MvCtx = crossValues[0];
  //const int comp = crossValues[1];
  const int syntax_mv_class = crossValues[2]; //uint_0_10
  if (view == VIEW_PROFILE0_CLIENT_A_LEVEL2) {
    if (MvCtx == 1 && syntax_mv_class >= 9) {
      return 1;
    }
  }
#endif

  return 0;
}

// Fractional MV bits are never coded in intrabc mode (MvCtx == 1)
// There are four such symbols which need to be covered:
int cicf_CROSS_MV_CLASS0_FR_CDF(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;
  return crossValues[0] == 1;
}
int cicf_CROSS_MV_CLASS0_HP_CDF(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;
  return crossValues[0] == 1;
}
int cicf_CROSS_MV_BIT_CDF(int *crossValues, int view) {
  const int MvCtx = crossValues[0];
  const int comp = crossValues[1];
  const int mv_bit_i = crossValues[2]; //uint_0_9
  const int syntax = crossValues[3];

#ifdef PROF_SHOW_CLIENT_A
  if (view == VIEW_PROFILE0_CLIENT_A_LEVEL2) {
    if (MvCtx == 1 && mv_bit_i >= 8) {
      return 1;
    }
    if (MvCtx == 1 && mv_bit_i == 7 && syntax == 1) {
      return 1;
    }
  }
#endif

  // For "standard" mvs we can achieve both, as MVs can go off the edge of the image
  // For "intrabc" mvs (MVCtx == 1)) the MVs need to be in bounds, so +/- 1280 (class == 9, mvbits == 0x100) is not possible for vertical MVs (comp == 0)
  if (view_to_max_level(view) == 2 &&
      MvCtx == 1 &&
      comp == 0 &&
      mv_bit_i == 9 &&
      syntax == 1) {
    return 1;
  }

  return 0;
}
int cicf_CROSS_MV_FR_CDF(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;
  return crossValues[0] == 1;
}
int cicf_CROSS_MV_HP_CDF(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;
  return crossValues[0] == 1;
}

int cicf_CROSS_TXB_SKIP_CDF(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;
  const int txSzCtx = crossValues[0];
  const int ctx = crossValues[1];
  if (ctx >= 7) {
    // For UV, the transform size is determined by:
    // * In lossless mode, 4x4 txfms are used (txSzCtx == 0)
    // * For 64xH and Wx64 txfms (txSzCtx == 4) are not allowed at all;
    //   for blocks of these sizes or larger, 32xH and Wx32 (txSzCtx == 3)
    //   transforms are used, where W/H is as large as possible.
    // * Otherwise, the txfm size is the same as the (downsampled) block size.
    //
    // Finally, ctx >= 10 iff the transform is smaller than the downsampled block
    // Due to the above logic, this can only happen for txSzCtx == 0 or txSzCtx == 3.
    if (txSzCtx == 4) {
      return 1;
    }
    if (ctx >= 10) {
      return (txSzCtx == 1 || txSzCtx == 2);
    }
  }
  return 0;
}

int cicf_CROSS_EOB_EXTRA_CDF(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;
  const int txSzCtx = crossValues[0];
  const int ptype = crossValues[1];
  const int ctx = crossValues[2];
  if (txSzCtx == 4 && ptype == 1) {
    // 64xH and Wx64 txfms are not allowed for UV planes
    return 1;
  }

  // Work out the number of coeffs in the largest transform
  // which has this txSxCtx:
  const int maxTxDim = 1 << (txSzCtx + 2); // 4x4, 8x8, 16x16, 32x32, 64x64
  const int maxTxEob = maxTxDim * maxTxDim;
  // Meanwhile, ctx == eobPt - 3, so the *minimum* eob which uses
  // this value of ctx is:
  const int minCtxEob = (1 << (ctx+1)) + 1; // 3, 5, 9, 17, ...
  // So certain combinations of {txSzCtx, ctx} are impossible:
  if (minCtxEob > maxTxEob) {
    return 1;
  }

  return 0;
}

int cicf_CROSS_COEFF_BASE_CDF(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;
  const int txSzCtx = crossValues[0];
  const int ptype = crossValues[1];
  const int ctx = crossValues[2];
  if (txSzCtx == 4 && ptype == 1) {
    // 64xH and Wx64 txfms are not allowed for UV planes
    return 1;
  }
  if (txSzCtx == 0 && ctx >= 11 && ctx <= 20) {
    // Coeff_Base_Ctx_Offset[] does not include 11 or 16 as base
    // offsets for 4x4 transforms, so this context range is impossible
    return 1;
  }
  if (txSzCtx == 0 && ctx >= 40) {
    // To get ctx >= 36, we need to be using a 1d transform and have
    // the relevant coordinate (row for vertical txfms, col for horizontal)
    // be at least 2.
    // For a 4x4 transform, this means that we can only look at
    // *at most 2* neighbouring coefficients, as the remaining neighbour
    // locations lie outside the transform block.
    // Thus mag <= 2 * 3 = 6, so ctx = (mag + 1) >> 1 <= 3.
    // So the highest context reachable for a 4x4 transform is 39.
    return 1;
  }
  if (txSzCtx >= 3 && ctx >= 26) {
    // These transforms all have txsize_sqr_up_map[txSz] >= TX_32X32,
    // and so are limited to DCT_DCT and maybe IDTX. In particular,
    // they can't use any 1D transforms, which correspond to ctx >= 26 here.
    return 1;
  }
  if (ctx == 41) {
    // Similarly, Coeff_Base_Pos_Ctx_Offset[] has a max value of 36,
    // and the remaining part of the context has value at most 4,
    // so the final context is at most 40.
    return 1;
  }
  return 0;
}

int cicf_CROSS_COEFF_BASE_EOB_CDF(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;
  if (crossValues[0] == 4 && crossValues[1] == 1) {
    // 64xH and Wx64 txfms are not allowed for UV planes
    return 1;
  }
  return 0;
}

int cicf_CROSS_LOSSLESS(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;
  if (crossValues[0] == 0 && crossValues[1] == 1) return 1;
  return 0;
}

int cicf_CROSS_FILM_GRAIN_POINTS(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  if (view_to_profile(view) == 0) {
    // In profile 0, the subsampling mode must be 4:2:0, and for
    // that subsampling we have a few constraints:
    // * If num_y_points is 0, then num_cb_points and num_cr_points
    //   must be 0
    // * num_cb_points and num_cr_points must either both be zero,
    //   or both be nonzero
    //
    // For profiles 1 and 2, we can use other subsampling modes
    // which don't have these constraints. So we only need to mark
    // the impossible cases for profile 0.
    int num_cb_points_nz = crossValues[0];
    int num_cr_points_nz = crossValues[1];
    int num_y_points_nz = crossValues[2];
    if (!num_y_points_nz && (num_cb_points_nz || num_cr_points_nz)) {
      return 1;
    }
    if (num_cb_points_nz && !num_cr_points_nz) {
      return 1;
    }
    if (num_cr_points_nz && !num_cb_points_nz) {
      return 1;
    }
  }

  return 0;
}

int cicf_VALUE_TILE_GROUP_COUNT(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int view_max_level = view_to_max_level(view);

  if (view_max_level == 2) {
    // For level 2.0, the maximum number of tile groups is only 8
    return (crossValues[0] > 8);
  }
  if (view_max_level == 5) {
    // For level 5.3, the maximum number of tile groups is 64
    return (crossValues[0] > 64);
  }

  return 0;
}

int cicf_VALUE_SCALABILITY_MODE(int *crossValues, int view) {
#ifdef PROF_SHOW_CLIENT_A
  int scalability_mode_idc = crossValues[0];
  if (view == VIEW_PROFILE0_CLIENT_A_LEVEL2) {
    switch (scalability_mode_idc) {
    //These values of scalability break resize constraints using specified level2 resolutions
    case 15: case 16: case 17: case 18: case 19: case 20: case 23: case 24: case 27: case 28:
      return 1;
      break;
    default:
      return 0;
    }
  }
#endif
  return 0;
}

int cicf_CROSS_LEVEL_TIER(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int tier = crossValues[0];
  int level = crossValues[1];
  int major = (level >> 2) + 2;
  int minor = (level & 3);

  int view_max_level = view_to_max_level(view);

  // Levels 2.x and 3.x don't signal 'tier'
  if (major < 4 && tier == 1) {
    return 1;
  }

  // Rule out undefined levels
  if (major <= 4 && minor >= 2) {
    return 1;
  }
  if (major >= 7 && level != 31) {
    return 1;
  }

  // Finally, constrain level based on the selected view
  if (view_max_level == 2) {
    return (level != 0);
  }
  if (view_max_level == 5) {
    return (major > 5);
  }
  if (view_max_level == 6) {
    return (major > 6);
  }

  return 0;
}

int cicf_VALUE_SEG_LVL_ALT_Q(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  if (-255 <= crossValues[0] &&
      crossValues[0] <= 255) {
    return 0;
  }

  return 1;
}

int cicf_VALUE_SEG_LVL_ALT_LF(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  if (-63 <= crossValues[0] &&
      crossValues[0] <= 63) {
    return 0;
  }

  return 1;
}

// Forward-declare arrays so that we can use them
extern flat_array<int,2> Tx_Type_In_Set_Inter;
extern flat_array<int,2> Tx_Type_In_Set_Intra;
extern flat_array<int,1> Tx_Size_Sqr;
extern flat_array<int,1> Tx_Size_Sqr_Up;

// We unfortunately can't quite reuse the functions from the main code,
// as we don't have 'b' available and we want to avoid depending on
// 'reduced_tx_set'. So we partially reimplement the logic of get_tx_set()
// and is_tx_type_in_set() here
int is_tx_allowed(int txSz, int txType, int inter) {
    int txSet;

    int txSzSqr = Tx_Size_Sqr[ txSz ];
    int txSzSqrUp = Tx_Size_Sqr_Up[ txSz ];
    if ( txSzSqrUp > TX_32X32 ) {
        txSet = 0;
    } else if ( inter ) {
        if ( txSzSqrUp == TX_32X32 ) txSet = 3;
        else if ( txSzSqr == TX_16X16 ) txSet = 2;
        else txSet = 1;
    } else {
        if ( txSzSqrUp == TX_32X32 ) txSet = 0;
        else if ( txSzSqr == TX_16X16 ) txSet = 2;
        else txSet = 1;
    }

    return inter ? Tx_Type_In_Set_Inter[ txSet ][ txType ] :
                   Tx_Type_In_Set_Intra[ txSet ][ txType ];
}

int cicf_CROSS_TRANSFORM_TYPE_INTRA(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int txSz = crossValues[0];
  int txType = crossValues[1];
  int ptype = crossValues[2];

  if (ptype == 1) {
    // For chroma intra blocks, the transform type is determined
    // by uv_mode via Mode_To_Txfm[]. This only contains entries
    // DCT_DCT up to ADST_ADST, so higher modes are impossible
    if (txType > 3) {
      return 1;
    }

    // 64xH and Wx64 size transforms are disallowed for chroma
    // planes regardless of subsampling (even for 4:4:4)
    if (Tx_Size_Sqr_Up[txSz] == TX_64X64) {
      return 1;
    }
  }
  return !is_tx_allowed(txSz, txType, 0);
}

int cicf_CROSS_TRANSFORM_TYPE_INTER(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int txSz = crossValues[0];
  int txType = crossValues[1];
  int ptype = crossValues[2];

  if (ptype == 1) {
    // 64xH and Wx64 size transforms are disallowed for chroma
    // planes regardless of subsampling (even for 4:4:4)
    if (Tx_Size_Sqr_Up[txSz] == TX_64X64) {
      return 1;
    }
  }
  return !is_tx_allowed(txSz, txType, 1);
}

int cicf_CROSS_COMP_REF_FRAMES(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int rf0 = crossValues[0];
  int rf1 = crossValues[1];

  // These are the combinations which can be achieved in read_ref_frames()
  if ((rf0 == BWDREF_FRAME && rf1 == ALTREF_FRAME) ||
      (rf0 == LAST_FRAME && rf1 == GOLDEN_FRAME) ||
      (rf0 == LAST_FRAME && rf1 == LAST3_FRAME) ||
      (rf0 == LAST_FRAME && rf1 == LAST2_FRAME) ||
      ((rf0 == LAST_FRAME || rf0 == LAST2_FRAME || rf0 == LAST3_FRAME || rf0 == GOLDEN_FRAME) &&
       (rf1 == BWDREF_FRAME || rf1 == ALTREF2_FRAME || rf1 == ALTREF_FRAME))) {
    return 0;
  }
  return 1;
}

int cicf_VALUE_NUM_ANCHOR_FRAMES(int *crossValues, int view) {
  // VALUE_NUM_ANCHOR_FRAMES can only be seen for large scale tile
  // streams. We can spot this by checking profile_views to see
  // whether the given view hides VC_LARGE_SCALE_TILES.
  assert (view < VIEW_ENTRY_COUNT);
  const int8_t *invis = profile_views [view].invisibleCategories;
  while (* invis != -1) {
    if (* invis == VC_LARGE_SCALE_TILES)
      return 1;

    ++ invis;
  }

  return 0;
}

int cicf_CROSS_WIDE_FILTER(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  // Chroma planes can't use the 13-tap filter
  if (crossValues[0] == 4 && crossValues[1] == 1) {
    return 1;
  }

  return 0;
}

int cicf_CROSS_CDEF_STRENGTH(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  // Secondary strength cannot be 3 - it can only be 0,1,2, or 4
  if (crossValues[1] == 3) {
    return 1;
  }

  return 0;
}

int cicf_CROSS_NUM_MV_FOUND(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  // Due to the extra search, it is impossible to have fewer
  // than 2 motion vectors in compound mode. But it is possible
  // to have 0 or 1 in single mode.
  if (crossValues[0] < 2 && crossValues[1] == 1) {
    return 1;
  }

  return 0;
}

int cicf_CROSS_PROFILE_SWITCH(int *crossValues, int view) {
  // We only generate profile 0 for ClientB, and ClientC so don't want to track profile
  // switching at all.
  if (
#ifdef PROF_SHOW_CLIENT_B
      view == VIEW_PROFILE0_CLIENT_B ||
      view == VIEW_PROFILE0_CLIENT_B_CORE ||
#endif
#ifdef PROF_SHOW_CLIENT_C
      view == VIEW_PROFILE0_CLIENT_C ||
      view == VIEW_PROFILE0_CLIENT_C_CORE ||
      view == VIEW_PROFILE0_CLIENT_C_LEVEL5 ||
      view == VIEW_PROFILE0_CLIENT_C_LARGE_SCALE_TILES ||
      view == VIEW_PROFILE0_CLIENT_C_NON_ANNEX_B ||
#endif
#ifdef PROF_SHOW_CLIENT_D
      view == VIEW_PROFILE0_CLIENT_D ||
      view == VIEW_PROFILE0_CLIENT_D_CORE ||
#endif
      0)
      return (crossValues[0] != 0) || (crossValues[1] != 0);

  //return crossValues[0] == crossValues[1];
  return 0;
}

// The list of possible angles passed to B()
#define NUM_ALLOWED_BUTTERFLY_ANGLES 42
static const int allowed_butterfly_angles[NUM_ALLOWED_BUTTERFLY_ANGLES] = {
    3,   6,   7,   8,  11,  12,  14,  15,  16,  19,  22,  23,  24,  27,
   28,  30,  31,  32,  35,  38,  39,  40,  43,  44,  46,  47,  48,  51,
   54,  55,  56,  59,  60,  62,  63,  76,  88,  92, 108, 112, 120, 124
};

int cicf_VALUE_BUTTERFLY_ANGLE(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  for (int i = 0; i < NUM_ALLOWED_BUTTERFLY_ANGLES; i++) {
    if (crossValues[0] == allowed_butterfly_angles[i]) {
      // This value is possible
      return 0;
    }
  }

  return 1;
}

int cicf_VALUE_MROW_SCAN(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  if ( crossValues[0] == TX_4X4 ||
       crossValues[0] == TX_4X8 ||
       crossValues[0] == TX_8X4 ||
       crossValues[0] == TX_8X8 ||
       crossValues[0] == TX_8X16 ||
       crossValues[0] == TX_16X8 ||
       crossValues[0] == TX_16X16 ||
       crossValues[0] == TX_4X16 ||
       crossValues[0] == TX_16X4 ) {
    return 0;
  }
  return 1;
}

int cicf_VALUE_MCOL_SCAN(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  if ( crossValues[0] == TX_4X4 ||
       crossValues[0] == TX_4X8 ||
       crossValues[0] == TX_8X4 ||
       crossValues[0] == TX_8X8 ||
       crossValues[0] == TX_8X16 ||
       crossValues[0] == TX_16X8 ||
       crossValues[0] == TX_16X16 ||
       crossValues[0] == TX_4X16 ||
       crossValues[0] == TX_16X4 ) {
    return 0;
  }
  return 1;
}

int cicf_VALUE_DEFAULT_SCAN(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  if ( crossValues[0] == TX_4X4 ||
       crossValues[0] == TX_4X8 ||
       crossValues[0] == TX_8X4 ||
       crossValues[0] == TX_8X8 ||
       crossValues[0] == TX_8X16 ||
       crossValues[0] == TX_16X8 ||
       crossValues[0] == TX_16X16 ||
       crossValues[0] == TX_16X32 ||
       crossValues[0] == TX_32X16 ||
       crossValues[0] == TX_4X16 ||
       crossValues[0] == TX_16X4 ||
       crossValues[0] == TX_8X32 ||
       crossValues[0] == TX_32X8 ||
       crossValues[0] == TX_32X32 ) {
    return 0;
  }
  return 1;
}

extern flat_array<int,2> Partition_Subsize;
int cicf_CROSS_PARTITION_SUBSIZE(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int partition = crossValues[0];
  int bsize = crossValues[1];
  if ( Partition_Subsize[ partition ][ bsize ] == BLOCK_INVALID ) {
    return 1;
  }
  if (bsize == BLOCK_8X8 &&
      ((partition == PARTITION_HORZ_A) ||
       (partition == PARTITION_HORZ_B) ||
       (partition == PARTITION_VERT_A) ||
       (partition == PARTITION_VERT_B))) {
    return 1;
  }
  return 0;
}

int cicf_VALUE_SPLIT_TX_SIZE(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  if ( crossValues[0] == TX_4X4 ) {
    return 1;
  }
  return 0;
}

int cicf_VALUE_PALETTE_COLOR_CONTEXT(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  if ((crossValues[0] == 0) ||
      (crossValues[0] == 1) ||
      (crossValues[0] == 3) ||
      (crossValues[0] == 4)) {
    return 1;
  }
  return 0;
}

int cicf_VALUE_MODE_TO_ANGLE(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  if ( ( crossValues[0] >= V_PRED ) &&
       ( crossValues[0] <= D67_PRED ) ) {
    return 0;
  }
  return 1;
}

int cicf_VALUE_DR_INTRA_DERIVATIVE(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  if (crossValues[0] == 3 ||
      crossValues[0] == 6 ||
      crossValues[0] == 9 ||
      crossValues[0] == 14 ||
      crossValues[0] == 17 ||
      crossValues[0] == 20 ||
      crossValues[0] == 23 ||
      crossValues[0] == 26 ||
      crossValues[0] == 29 ||
      crossValues[0] == 32 ||
      crossValues[0] == 36 ||
      crossValues[0] == 39 ||
      crossValues[0] == 42 ||
      crossValues[0] == 45 ||
      crossValues[0] == 48 ||
      crossValues[0] == 51 ||
      crossValues[0] == 54 ||
      crossValues[0] == 58 ||
      crossValues[0] == 61 ||
      crossValues[0] == 64 ||
      crossValues[0] == 67 ||
      crossValues[0] == 70 ||
      crossValues[0] == 73 ||
      crossValues[0] == 76 ||
      crossValues[0] == 81 ||
      crossValues[0] == 84 ||
      crossValues[0] == 87) {
    return 0;
  }
  return 1;
}

int cicf_VALUE_WEDGE_BITS (int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  // We only read from the the Wedge_Bits array when looking at a
  // compound block. The (single) cross value is the block size.
  // However, we only read compound references when the block is at
  // least 8 pixels wide and high.
  return ((crossValues [0] == BLOCK_4X4) ||
          (crossValues [0] == BLOCK_4X8) ||
          (crossValues [0] == BLOCK_8X4) ||
          (crossValues [0] == BLOCK_4X16) ||
          (crossValues [0] == BLOCK_16X4));
}

extern flat_array<int,3> Subsampled_Size;

int cicf_CROSS_SUBSAMPLED_SIZE(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int subsize = crossValues[0];
  int subx = crossValues[1];
  int suby = crossValues[2];
  int profile = view_to_profile(view);

  // can be looked up for Y plane too
  if (profile == 0 && subx == 0 && suby == 0) {
    return 0;
  }
  if (invalid_subsampling(profile, subx, suby) == 1) {
    return 1;
  }
  if (profile == 2) {
    if (Subsampled_Size[subsize][subx][suby] == BLOCK_INVALID) {
      return 1;
    }
  }

  return 0;
}

int cicf_CROSS_TX_TYPE_IN_SET_INTER(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int txSet = crossValues[0];
  int txType = crossValues[1];
  int profile = view_to_profile(view);

  // txSet can only be set to DCT only in get_tx_set() if txSzSqrUp > TX_32X32,
  // however this can never be reached, as compute_tx_type() will return
  // DCT_DCT directly in that case
  if (txSet == TX_SET_DCTONLY) {
    return 1;
  }

  // cannot hit this unless subsampling_x == subsampling_y == 0
  if (profile == 0 &&
      txSet == TX_SET_INTER_3 &&
      (txType == V_ADST ||
       txType == H_ADST ||
       txType == V_FLIPADST ||
       txType == H_FLIPADST)) {
    return 1;
  }

  return 0;
}

extern flat_array<int,1> Mode_To_Txfm;

int cicf_CROSS_TX_TYPE_IN_SET_INTRA(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  //int txSet = crossValues[0];
  int txType = crossValues[1];

  // only values returned by Mode_To_Txfm can be used here
  for (int i=0; i<UV_INTRA_MODES_CFL_ALLOWED; i++) {
    if (txType == Mode_To_Txfm[i]) {
      return 0;
    }
  }

  // Not in list, not possible
  return 1;
}

int cicf_VALUE_DIV_MULT(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int clippedDenominator = crossValues[0];
  if (clippedDenominator == 0) {
    return 1;
  }
  return 0;
}

int cicf_VALUE_II_WEIGHTS_1D(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int idx = crossValues[0];

  // Interintra can only be signalled if MiSize >= BLOCK_8X8 && MiSize <= BLOCK_32X32
  // So sizeScale’s max divisor is 32.  So can only access every 128/32=4th element
  if (idx % 4 != 0) {
    return 1;
  }
  return 0;
}

int cicf_CROSS_QUANT_DIST_WEIGHT(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int idx = crossValues[0];
  //int order = crossValues[1];

  // Accessed within a loop: "for (i=0; i<3; i++)", so i==3 is never possible
  if (idx == 3) {
    return 1;
  }
  return 0;
}

int cicf_CROSS_XC_LOOKUP(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int bitDepthIdx = crossValues[0];
  int profile = view_to_profile(view);

  if ((profile != 2) &&
      (bitDepthIdx == 2)) {
    return 1;
  }

  return 0;
}

int cicf_VALUE_COS128_LOOKUP(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int angle = crossValues[0];

  // I prove by brute force that these two values cannot be looked up
  if (angle == 0 ||
      angle == 64) {
    return 1;
  }

  return 0;
}

int cicf_CROSS_CDEF_UV_DIR(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int subx = crossValues[0];
  int suby = crossValues[1];
  //int ydir = crossValues[2];
  int profile = view_to_profile(view);

  return invalid_subsampling(profile, subx, suby);
}

extern flat_array<int,2> Sgr_Params;
int cicf_CROSS_SGR_PARAMS(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int set = crossValues[0];
  int x = crossValues[1];

  for (int s=0; s<(1 << SGRPROJ_PARAMS_BITS); s++) {
    // For even x, Sgr_params[set][x+1] is used if Sgr_params[set][x] is non-zero
    if ((x&1) &&
        (Sgr_Params[set][x-1] == 0)) {
      return 1;
    }
  }
  return 0;
}

int cicf_CROSS_COMPOUND_MODE_CTX_MAP(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int refMvCtx = crossValues[0];
  int newMvCtx = crossValues[1];

  // if CloseMatches==0 (is_inside will fail for the above row (scan_row) and left col (scan_col),
  // necessarily TotalMatches must equal 0 (is_inside will fail for the top left (scan_point(-1,-1,))
  if (refMvCtx == 1 &&
      newMvCtx == 0) {
    return 1;
  }

  // working through the logic in context_and_clamping, these are the only remaining possible cases
  if (refMvCtx == 0 &&
      (newMvCtx == 0 ||
       newMvCtx == 1)) {
    return 0;
  }
  if (refMvCtx == 1 &&
      (/*newMvCtx == 0 ||*/ // impossible from above
       newMvCtx == 1 ||
       newMvCtx == 2 ||
       newMvCtx == 3)) {
    return 0;
  }
  if (refMvCtx == 2 &&
      (newMvCtx == 2 ||
       newMvCtx == 3 ||
       newMvCtx == 4 ||
       newMvCtx == 5)) {
    return 0;
  }

  return 1;
}

extern flat_array<int,1> Tx_Height;
extern flat_array<int,1> Tx_Width;
int cicf_CROSS_COEFF_BASE_CTX_OFFSET(int *crossValues, int view) {
  if (view == VIEW_ALL) return 0;

  int txSz = crossValues[0];
  int row = crossValues[1];
  int col = crossValues[2];

  // always excluded
  if (row == 0 &&
      col == 0) {
    return 1;
  }

  // cannot exceed size of transform
  if (row >= Tx_Height[txSz]) {
    return 1;
  }
  if (col >= Tx_Width[txSz]) {
    return 1;
  }

  // if this is the last coeff in the transform, the ctx goes down the isEob route instead
  if (row == (Tx_Height[txSz]-1) &&
      col == (Tx_Width[txSz]-1)) {
    return 1;
  }

  return 0;
}


const struct CrossEntry profile_cross_entries[CROSS_ENTRY_COUNT] = {
  //{ "VALUE_CABAC_TILE_SIZE_MINUS_1", 1, { "tile_size_minus_1" }, NULL },
  //  { "VALUE_CABAC_METADATA_TYPE", 1, { "metadata_type" }, NULL },
  { "VALUE_CABAC_LOOP_FILTER_REF_DELTAS_INTRA", 1, { "loop_filter_ref_deltas for INTRA_FRAME" }, NULL },
  { "VALUE_CABAC_LOOP_FILTER_REF_DELTAS_LAST", 1, { "loop_filter_ref_deltas for LAST_FRAME" }, NULL },
  { "VALUE_CABAC_LOOP_FILTER_REF_DELTAS_LAST2", 1, { "loop_filter_ref_deltas for LAST2_FRAME" }, NULL },
  { "VALUE_CABAC_LOOP_FILTER_REF_DELTAS_LAST3", 1, { "loop_filter_ref_deltas for LAST3_FRAME" }, NULL },
  { "VALUE_CABAC_LOOP_FILTER_REF_DELTAS_GOLDEN", 1, { "loop_filter_ref_deltas for GOLDEN_FRAME" }, NULL },
  { "VALUE_CABAC_LOOP_FILTER_REF_DELTAS_BWDREF", 1, { "loop_filter_ref_deltas for BWDREF_FRAME" }, NULL },
  { "VALUE_CABAC_LOOP_FILTER_REF_DELTAS_ALTREF2", 1, { "loop_filter_ref_deltas for ALTREF2_FRAME" }, NULL },
  { "VALUE_CABAC_LOOP_FILTER_REF_DELTAS_ALTREF", 1, { "loop_filter_ref_deltas for ALTREF_FRAME" }, NULL },
  { "VALUE_CABAC_LOOP_FILTER_MODE_DELTAS_MODE0", 1, { "loop_filter_mode_deltas for mode 0" }, NULL },
  { "VALUE_CABAC_LOOP_FILTER_MODE_DELTAS_MODE1", 1, { "loop_filter_mode_deltas for mode 1" }, NULL },
  { "VALUE_CABAC_DELTAQYDc", 1, { "DeltaQYDc" }, NULL },
  { "VALUE_CABAC_DELTAQUDc", 1, { "DeltaQUDc" }, NULL },
  { "VALUE_CABAC_DELTAQUAc", 1, { "DeltaQUAc" }, NULL },
  { "VALUE_CABAC_DELTAQVDc", 1, { "DeltaQVDc" }, NULL },
  { "VALUE_CABAC_DELTAQVAc", 1, { "DeltaQVAc" }, NULL },
  //{ "VALUE_CABAC_FEATURE_VALUE_SIGNED", 1, { "feature_value_signed" }, NULL },
  //{ "VALUE_CABAC_FEATURE_VALUE_UNSIGNED", 1, { "feature_value_unsigned" }, NULL },
  { "VALUE_CABAC_DELTA_Q_REM_BITS", 1, { "delta_q_rem_bits" }, NULL },
  { "VALUE_CABAC_DELTA_Q_ABS_BITS", 1, { "delta_q_abs_bits" }, NULL },
  { "VALUE_CABAC_DELTA_Q_SIGN_BIT", 1, { "delta_q_sign_bit" }, NULL },
  { "VALUE_CABAC_DELTA_LF_REM_BITS", 1, { "delta_lf_rem_bits" }, NULL },
  { "VALUE_CABAC_DELTA_LF_ABS_BITS", 1, { "delta_lf_abs_bits" }, NULL },
  { "VALUE_CABAC_DELTA_LF_SIGN_BIT", 1, { "delta_lf_sign_bit" }, NULL },
  { "VALUE_CABAC_WEDGE_INDEX", 1, { "wedge_index" }, NULL },
  { "VALUE_CABAC_WEDGE_SIGN", 1, { "wedge_sign" }, NULL },
  { "VALUE_CABAC_MASK_TYPE", 1, { "mask_type" }, NULL },
  { "VALUE_CABAC_SIGN_BIT", 1, { "sign_bit" }, NULL },
  { "VALUE_CABAC_USE_PALETTE_COLOR_CACHE_Y", 1, { "use_palette_color_cache_y" }, NULL },
  { "VALUE_CABAC_PALETTE_COLORS_Y", 1, { "palette_colors_y" }, &cicf_VALUE_CABAC_PALETTE_COLORS },
  { "VALUE_CABAC_PALETTE_NUM_EXTRA_BITS_Y", 1, { "palette_num_extra_bits_y" }, NULL },
  { "VALUE_CABAC_PALETTE_DELTA_Y", 1, { "palette_delta_y" }, &cicf_VALUE_CABAC_PALETTE_DELTA_YU },
  { "VALUE_CABAC_USE_PALETTE_COLOR_CACHE_U", 1, { "use_palette_color_cache_u" }, NULL },
  { "VALUE_CABAC_PALETTE_COLORS_U", 1, { "palette_colors_u" }, &cicf_VALUE_CABAC_PALETTE_COLORS },
  { "VALUE_CABAC_PALETTE_NUM_EXTRA_BITS_U", 1, { "palette_num_extra_bits_u" }, NULL },
  { "VALUE_CABAC_PALETTE_DELTA_U", 1, { "palette_delta_u" }, &cicf_VALUE_CABAC_PALETTE_DELTA_YU },
  { "VALUE_CABAC_DELTA_ENCODE_PALETTE_COLORS_V", 1, { "delta_encode_palette_colors_v" }, NULL },
  { "VALUE_CABAC_PALETTE_NUM_EXTRA_BITS_V", 1, { "palette_num_extra_bits_v" }, NULL },
  { "VALUE_CABAC_PALETTE_COLORS_V", 1, { "palette_colors_v" }, &cicf_VALUE_CABAC_PALETTE_COLORS },
  { "VALUE_CABAC_PALETTE_DELTA_V", 1, { "palette_delta_v" }, &cicf_VALUE_CABAC_PALETTE_DELTA_V },
  { "VALUE_CABAC_PALETTE_DELTA_SIGN_BIT_V", 1, { "palette_delta_sign_bit_v" }, NULL },
  { "VALUE_CABAC_CDEF_IDX", 1, { "cdef_idx" }, NULL },
  { "VALUE_CABAC_ALL_ZERO", 1, { "all_zero" }, NULL },
  { "VALUE_CABAC_EOB_PT_16", 1, { "eob_pt_16" }, NULL },
  { "VALUE_CABAC_EOB_PT_32", 1, { "eob_pt_32" }, NULL },
  { "VALUE_CABAC_EOB_PT_64", 1, { "eob_pt_64" }, NULL },
  { "VALUE_CABAC_EOB_PT_128", 1, { "eob_pt_128" }, NULL },
  { "VALUE_CABAC_EOB_PT_256", 1, { "eob_pt_256" }, NULL },
  { "VALUE_CABAC_EOB_PT_512", 1, { "eob_pt_512" }, NULL },
  { "VALUE_CABAC_EOB_PT_1024", 1, { "eob_pt_1024" }, NULL },
  { "VALUE_CABAC_EOB_EXTRA", 1, { "eob_extra" }, NULL },
  { "VALUE_CABAC_EOB_EXTRA_BIT", 1, { "eob_extra_bit" }, NULL },
  { "VALUE_CABAC_COEFF_BASE", 1, { "coeff_base" }, NULL },
  { "VALUE_CABAC_COEFF_BASE_EOB", 1, { "coeff_base_eob" }, NULL },
  { "VALUE_CABAC_DC_SIGN", 1, { "dc_sign" }, NULL },
  { "VALUE_CABAC_COEFF_BR", 1, { "coeff_br" }, NULL },
  { "VALUE_CABAC_GOLOMB_LENGTH_BIT", 1, { "golomb_length_bit" }, NULL },
  { "VALUE_CABAC_GOLOMB_DATA_BIT", 1, { "golomb_data_bit" }, NULL },
  { "VALUE_CABAC_ANGLE_DELTA_Y", 1, { "angle_delta_y" }, NULL },
  { "VALUE_CABAC_ANGLE_DELTA_UV", 1, { "angle_delta_uv" }, NULL },
  { "VALUE_CABAC_COLOR_INDEX_MAP_Y", 1, { "color_index_map_y" }, NULL },
  { "VALUE_CABAC_COLOR_INDEX_MAP_UV", 1, { "color_index_map_uv" }, NULL },
  { "VALUE_CABAC_PARTITION", 1, { "partition" }, NULL },
  { "VALUE_CABAC_SPLIT_OR_HORZ", 1, { "split_or_horz" }, NULL },
  { "VALUE_CABAC_SPLIT_OR_VERT", 1, { "split_or_vert" }, NULL },
  { "VALUE_CABAC_USE_INTRABC", 1, { "use_intrabc" }, NULL },
  { "VALUE_CABAC_INTRA_FRAME_Y_MODE", 1, { "intra_frame_y_mode" }, NULL },
  { "VALUE_CABAC_UV_MODE", 1, { "uv_mode" }, NULL },
  { "VALUE_CABAC_SEGMENT_ID", 1, { "segment_id" }, NULL },
  { "VALUE_CABAC_SKIP_MODE", 1, { "skip_mode" }, NULL },
  { "VALUE_CABAC_SKIP", 1, { "skip" }, NULL },
  { "VALUE_CABAC_DELTA_Q_ABS", 1, { "delta_q_abs" }, NULL },
  { "VALUE_CABAC_DELTA_LF_ABS", 1, { "delta_lf_abs" }, NULL },
  { "VALUE_CABAC_TX_DEPTH", 1, { "tx_depth" }, NULL },
  { "VALUE_CABAC_TXFM_SPLIT", 1, { "txfm_split" }, NULL },
  { "VALUE_CABAC_SEG_ID_PREDICTED", 1, { "seg_id_predicted" }, NULL },
  { "VALUE_CABAC_IS_INTER", 1, { "is_inter" }, NULL },
  { "VALUE_CABAC_USE_FILTER_INTRA", 1, { "use_filter_intra" }, NULL },
  { "VALUE_CABAC_FILTER_INTRA_MODE", 1, { "filter_intra_mode" }, NULL },
  { "VALUE_CABAC_Y_MODE", 1, { "y_mode" }, NULL },
  { "VALUE_CABAC_COMPOUND_MODE", 1, { "compound_mode" }, NULL },
  { "VALUE_CABAC_NEW_MV", 1, { "new_mv" }, NULL },
  { "VALUE_CABAC_ZERO_MV", 1, { "zero_mv" }, NULL },
  { "VALUE_CABAC_REF_MV", 1, { "ref_mv" }, NULL },
  { "VALUE_CABAC_DRL_MODE", 1, { "drl_mode" }, NULL },
  { "VALUE_CABAC_INTERP_FILTER", 1, { "interp_filter" }, NULL },
  { "VALUE_CABAC_COMP_MODE", 1, { "comp_mode" }, NULL },
  { "VALUE_CABAC_COMP_REF_TYPE", 1, { "comp_ref_type" }, NULL },
  { "VALUE_CABAC_UNI_COMP_REF", 1, { "uni_comp_ref" }, NULL },
  { "VALUE_CABAC_UNI_COMP_REF_P1", 1, { "uni_comp_ref_p1" }, NULL },
  { "VALUE_CABAC_UNI_COMP_REF_P2", 1, { "uni_comp_ref_p2" }, NULL },
  { "VALUE_CABAC_COMP_REF", 1, { "comp_ref" }, NULL },
  { "VALUE_CABAC_COMP_REF_P1", 1, { "comp_ref_p1" }, NULL },
  { "VALUE_CABAC_COMP_REF_P2", 1, { "comp_ref_p2" }, NULL },
  { "VALUE_CABAC_COMP_BWDREF", 1, { "comp_bwdref" }, NULL },
  { "VALUE_CABAC_COMP_BWDREF_P1", 1, { "comp_bwdref_p1" }, NULL },
  { "VALUE_CABAC_SINGLE_REF_P1", 1, { "single_ref_p1" }, NULL },
  { "VALUE_CABAC_SINGLE_REF_P2", 1, { "single_ref_p2" }, NULL },
  { "VALUE_CABAC_SINGLE_REF_P3", 1, { "single_ref_p3" }, NULL },
  { "VALUE_CABAC_SINGLE_REF_P4", 1, { "single_ref_p4" }, NULL },
  { "VALUE_CABAC_SINGLE_REF_P5", 1, { "single_ref_p5" }, NULL },
  { "VALUE_CABAC_SINGLE_REF_P6", 1, { "single_ref_p6" }, NULL },
  { "VALUE_CABAC_USE_OBMC", 1, { "use_obmc" }, NULL },
  { "VALUE_CABAC_MOTION_MODE", 1, { "motion_mode" }, NULL },
  { "VALUE_CABAC_INTERINTRA", 1, { "interintra" }, NULL },
  { "VALUE_CABAC_INTERINTRA_MODE", 1, { "interintra_mode" }, NULL },
  { "VALUE_CABAC_WEDGE_INTERINTRA", 1, { "wedge_interintra" }, NULL },
  { "VALUE_CABAC_COMP_GROUP_IDX", 1, { "comp_group_idx" }, NULL },
  { "VALUE_CABAC_COMPOUND_IDX", 1, { "compound_idx" }, NULL },
  { "VALUE_CABAC_COMPOUND_TYPE", 1, { "compound_type" }, NULL },
  { "VALUE_CABAC_MV_JOINT", 1, { "mv_joint" }, NULL },
  { "VALUE_CABAC_MV_SIGN", 1, { "mv_sign" }, NULL },
  { "VALUE_CABAC_MV_CLASS", 1, { "mv_class" }, NULL },
  { "VALUE_CABAC_MV_CLASS0_BIT", 1, { "mv_class0_bit" }, NULL },
  { "VALUE_CABAC_MV_CLASS0_FR", 1, { "mv_class0_fr" }, NULL },
  { "VALUE_CABAC_MV_CLASS0_HP", 1, { "mv_class0_hp" }, NULL },
  { "VALUE_CABAC_MV_BIT", 1, { "mv_bit" }, NULL },
  { "VALUE_CABAC_MV_FR", 1, { "mv_fr" }, NULL },
  { "VALUE_CABAC_MV_HP", 1, { "mv_hp" }, NULL },
  { "VALUE_CABAC_CFL_ALPHA_SIGNS", 1, { "cfl_alpha_signs" }, NULL },
  { "VALUE_CABAC_CFL_ALPHA_U", 1, { "cfl_alpha_u" }, NULL },
  { "VALUE_CABAC_CFL_ALPHA_V", 1, { "cfl_alpha_v" }, NULL },
  { "VALUE_CABAC_HAS_PALETTE_Y", 1, { "has_palette_y" }, NULL},
  { "VALUE_CABAC_PALETTE_SIZE_Y_MINUS_2", 1, { "palette_size_y_minus_2" }, NULL },
  { "VALUE_CABAC_HAS_PALETTE_UV", 1, { "has_palette_uv" }, NULL },
  { "VALUE_CABAC_PALETTE_SIZE_UV_MINUS_2", 1, { "palette_size_uv_minus_2" }, NULL },
  { "VALUE_CABAC_INTER_TX_TYPE", 1, { "inter_tx_type" }, NULL },
  { "VALUE_CABAC_INTRA_TX_TYPE", 1, { "intra_tx_type" }, NULL },
  { "VALUE_CABAC_PALETTE_COLOR_IDX_Y", 1, { "palette_color_idx_y" }, NULL },
  { "VALUE_CABAC_PALETTE_COLOR_IDX_UV", 1, { "palette_color_idx_uv" }, NULL },
  { "VALUE_CABAC_USE_WIENER", 1, { "use_wiener" }, NULL },
  { "VALUE_CABAC_USE_SGRPROJ", 1, { "use_sgrproj" }, NULL },
  { "VALUE_CABAC_RESTORATION_TYPE", 1, { "restoration_type" }, NULL },
  { "VALUE_CABAC_LR_SGR_SET", 1, { "lr_sgr_set" }, NULL },
  { "VALUE_CABAC_SUBEXP_UNIF_BOOLS", 1, { "subexp_unif_bools" }, NULL },
  { "VALUE_CABAC_SUBEXP_MORE_BOOLS", 1, { "subexp_more_bools" }, NULL },
  { "VALUE_CABAC_SUBEXP_BOOLS", 1, { "subexp_bools" }, NULL },
  //{ "VALUE_CABAC_WIDTH_IN_SBS_MINUS_1", 1, { "width_in_sbs_minus_1" }, NULL },
  //{ "VALUE_CABAC_HEIGHT_IN_SBS_MINUS_1", 1, { "height_in_sbs_minus_1" }, NULL },
  //{ "VALUE_CABAC_SUBEXP_FINAL_BITS", 1, { "subexp_final_bits" }, NULL },
  //{ "VALUE_CABAC_SUBEXP_BITS", 1, { "subexp_bits" }, NULL },
  { "VALUE_INTRA_DIRECTIONAL_PANGLE", 1, { "pAngle" }, &cicf_VALUE_INTRA_DIRECTIONAL_PANGLE },
  { "VALUE_LR_WIENER", 1, { "LrWiener" }, NULL },
  { "VALUE_LR_SGR_XQD", 1, { "LrSgrXqd" }, NULL },
  { "VALUE_GM_PARAMS_PRE_SHIFT", 1, { "gmParamsPreShift" }, NULL },
  { "CROSS_TILE_WIDTH_SB", 2, { "isMax", "sb128" }, & cicf_CROSS_TILE_WIDTH_SB },
  { "CROSS_TILE_AREA_SB",  2, { "isMax", "sb128" }, & cicf_CROSS_TILE_AREA_SB },

  { "CROSS_PARTITION_CDF", 3, { "bsl", "ctx", "syntax" }, &cicf_CROSS_PARTITION_CDF },
  { "CROSS_SPLIT_OR_HORZ_CDF", 2, { "ctx", "syntax" }, &cicf_CROSS_SPLIT_OR_HORZ_CDF },
  { "CROSS_SPLIT_OR_VERT_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_INTRA_FRAME_Y_MODE_CDF", 3, { "abovemode", "leftmode", "syntax" }, NULL },
  { "CROSS_UV_MODE_CFL_ALLOWED_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_UV_MODE_CFL_NOT_ALLOWED_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_TILE_SEGMENT_ID_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_TILE_SKIP_MODE_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_TILE_SKIP_CDF", 2, { "ctx", "syntax" }, NULL },
  { "VALUE_TILE_DELTA_Q_ABS", 1, { "syntax" }, NULL },
  { "VALUE_TILE_DELTA_LF_SINGLE_CDF", 1, { "syntax" }, NULL },
  { "CROSS_TILE_DELTA_LF_MULTI_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_MAX_TX_DEPTH_CDF", 3, { "maxTxDepth", "ctx", "syntax" }, &cicf_CROSS_MAX_TX_DEPTH_CDF },
  { "CROSS_TXFM_SPLIT_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_SEGMENT_ID_PREDICTED_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_IS_INTER_CDF", 2, { "ctx", "syntax" }, NULL },
  { "VALUE_FILTER_INTRA_MODE", 1, { "syntax" }, NULL },
  { "CROSS_FILTER_INTRA_CDF", 2, { "ctx", "syntax" }, &cicf_CROSS_FILTER_INTRA_CDF },
  { "CROSS_Y_MODE_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMPOUND_MODE_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_NEW_MV_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_ZERO_MV_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_REF_MV_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_DRL_MODE_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_INTERP_FILTER_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_MODE_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_REF_TYPE_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_UNI_COMP_REF_0_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_UNI_COMP_REF_1_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_UNI_COMP_REF_2_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_REF_0_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_REF_1_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_REF_2_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_BWDREF_0_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_BWDREF_1_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_SINGLE_REF_0_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_SINGLE_REF_1_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_SINGLE_REF_2_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_SINGLE_REF_3_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_SINGLE_REF_4_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMP_SINGLE_REF_5_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_USE_OBMC_CDF", 2, { "MiSize", "syntax" }, &cicf_CROSS_USE_OBMC_CDF },
  { "CROSS_MOTION_MODE_CDF", 2, { "MiSize", "syntax" }, &cicf_CROSS_USE_OBMC_CDF },
  { "CROSS_INTER_INTRA_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_INTER_INTRA_MODE_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_WEDGE_INDEX_CDF", 2, { "MiSize", "syntax" }, &cicf_CROSS_COMPOUND_TYPE_CDF },
  { "CROSS_WEDGE_INTERINTRA_CDF", 2, { "MiSize", "syntax" }, &cicf_CROSS_WEDGE_INTERINTRA_CDF },
  { "CROSS_COMP_GROUP_IDX_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMPOUND_IDX_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_COMPOUND_TYPE_CDF", 2, { "MiSize", "syntax" }, &cicf_CROSS_COMPOUND_TYPE_CDF },
  { "CROSS_MV_JOINT_CDF", 2, { "MvCtx", "syntax" }, NULL },
  { "CROSS_MV_SIGN_CDF", 3, { "MvCtx", "comp", "syntax" }, NULL },
  { "CROSS_MV_CLASS_CDF", 3, { "MvCtx", "comp", "syntax" }, &cicf_CROSS_MV_CLASS_CDF },
  { "CROSS_MV_CLASS0_BIT_CDF", 3, { "MvCtx", "comp", "syntax" }, NULL },
  { "CROSS_MV_CLASS0_FR_CDF", 4, { "MvCtx", "comp", "bit", "syntax" }, &cicf_CROSS_MV_CLASS0_FR_CDF },
  { "CROSS_MV_CLASS0_HP_CDF", 3, { "MvCtx", "comp", "syntax" }, &cicf_CROSS_MV_CLASS0_HP_CDF },
  { "CROSS_MV_BIT_CDF", 4, { "MvCtx", "comp", "i", "syntax" }, &cicf_CROSS_MV_BIT_CDF },
  { "CROSS_MV_FR_CDF", 3, { "MvCtx", "comp", "syntax" }, &cicf_CROSS_MV_FR_CDF },
  { "CROSS_MV_HP_CDF", 3, { "MvCtx", "comp", "syntax" }, &cicf_CROSS_MV_HP_CDF },
  { "CROSS_TXB_SKIP_CDF", 3, { "txSzCtx", "ctx", "syntax" }, &cicf_CROSS_TXB_SKIP_CDF },
  { "CROSS_EOB_PT_16_CDF", 3, { "ptype", "ctx", "syntax" }, NULL },
  { "CROSS_EOB_PT_32_CDF", 3, { "ptype", "ctx", "syntax" }, NULL },
  { "CROSS_EOB_PT_64_CDF", 3, { "ptype", "ctx", "syntax" }, NULL },
  { "CROSS_EOB_PT_128_CDF", 3, { "ptype", "ctx", "syntax" }, NULL },
  { "CROSS_EOB_PT_256_CDF", 3, { "ptype", "ctx", "syntax" }, NULL },
  { "CROSS_EOB_PT_512_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_EOB_PT_1024_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_EOB_EXTRA_CDF", 4, { "txSzCtx", "ptype", "ctx", "syntax" }, &cicf_CROSS_EOB_EXTRA_CDF },
  { "CROSS_COEFF_BASE_CDF", 4, { "txSzCtx", "ptype", "ctx", "syntax" }, &cicf_CROSS_COEFF_BASE_CDF },
  { "CROSS_COEFF_BASE_EOB_CDF", 4, { "txSzCtx", "ptype", "ctx", "syntax" }, &cicf_CROSS_COEFF_BASE_EOB_CDF },
  { "CROSS_DC_SIGN_CDF", 3, { "ptype", "ctx", "syntax" }, NULL },
  { "CROSS_COEFF_BR_CDF", 4, { "txSzCtx", "ptype", "ctx", "syntax" }, NULL },
  { "VALUE_CFL_ALPHA_SIGNS", 1, { "syntax" }, NULL },
  { "CROSS_CFL_ALPHA_U_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_CFL_ALPHA_V_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_PALETTE_Y_MODE_CDF", 3, { "bsizeCtx", "ctx", "syntax" }, NULL },
  { "CROSS_PALETTE_Y_SIZE_CDF", 2, { "bsizeCtx", "syntax" }, NULL },
  { "CROSS_PALETTE_UV_MODE_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_PALETTE_UV_SIZE_CDF", 2, { "bsizeCtx", "syntax" }, NULL },
  { "CROSS_INTER_TX_TYPE_SET1_CDF", 2, { "ctx", "syntax" }, NULL },
  { "VALUE_INTER_TX_TYPE_SET2_CDF", 1, { "syntax" }, NULL },
  { "CROSS_INTER_TX_TYPE_SET3_CDF", 2, { "ctx", "syntax" }, NULL },
  { "CROSS_INTRA_TX_TYPE_SET1_CDF", 3, { "ctx", "intraDir", "syntax" }, NULL },
  { "CROSS_INTRA_TX_TYPE_SET2_CDF", 3, { "ctx", "intraDir", "syntax" }, NULL },
  { "CROSS_PALETTE_SIZE_2_Y_COLOR_CDF", 2, { "ctx", "syntax", }, &cicf_CROSS_PALETTE_SIZE_2_COLOR_CDF },
  { "CROSS_PALETTE_SIZE_3_Y_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_PALETTE_SIZE_4_Y_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_PALETTE_SIZE_5_Y_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_PALETTE_SIZE_6_Y_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_PALETTE_SIZE_7_Y_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_PALETTE_SIZE_8_Y_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_PALETTE_SIZE_2_UV_COLOR_CDF", 2, { "ctx", "syntax", }, &cicf_CROSS_PALETTE_SIZE_2_COLOR_CDF },
  { "CROSS_PALETTE_SIZE_3_UV_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_PALETTE_SIZE_4_UV_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_PALETTE_SIZE_5_UV_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_PALETTE_SIZE_6_UV_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_PALETTE_SIZE_7_UV_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_PALETTE_SIZE_8_UV_COLOR_CDF", 2, { "ctx", "syntax", }, NULL },
  { "VALUE_RESTORATION_TYPE", 1, { "syntax", }, NULL },
  { "CROSS_ANGLE_DELTA_Y_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_ANGLE_DELTA_UV_CDF", 2, { "ctx", "syntax", }, NULL },
  { "CROSS_LOSSLESS", 3, { "CodedLossless", "AllLossless", "(base_q_idx==0)" }, &cicf_CROSS_LOSSLESS },
  { "CROSS_SEG_FEATURES", 2, { "feature", "enabled" }, NULL },
  { "CROSS_FILM_GRAIN_POINTS", 3, { "num_cb_points_nonzero", "num_cr_points_nonzero", "num_y_points_nonzero" }, &cicf_CROSS_FILM_GRAIN_POINTS },
  { "VALUE_NUM_LR_PARAMS_IN_SB", 1, { "LR parameter sets in superblock" }, NULL },
  { "VALUE_TILE_GROUP_COUNT", 1, { "Num tile groups in frame" }, &cicf_VALUE_TILE_GROUP_COUNT },

  { "VALUE_SCALABILITY_MODE", 1, { "Scalability mode" }, &cicf_VALUE_SCALABILITY_MODE },
  { "CROSS_LOSSLESS_QINDEX", 2, { "lossless", "(qindex==0)" }, NULL },
  { "CROSS_LEVEL_TIER", 2, { "tier", "seq_level_idx" }, &cicf_CROSS_LEVEL_TIER },
  { "VALUE_SUPERRES_DENOM", 1, { "SuperresDenom" }, NULL },
  { "VALUE_SEG_LVL_ALT_Q", 1, { "Data for SEG_LVL_ALT_Q" }, &cicf_VALUE_SEG_LVL_ALT_Q },
  { "VALUE_SEG_LVL_ALT_LF_Y_V", 1, { "Data for SEG_LVL_ALT_LF_Y_V" }, &cicf_VALUE_SEG_LVL_ALT_LF },
  { "VALUE_SEG_LVL_ALT_LF_Y_H", 1, { "Data for SEG_LVL_ALT_LF_Y_H" }, &cicf_VALUE_SEG_LVL_ALT_LF },
  { "VALUE_SEG_LVL_ALT_LF_U", 1, { "Data for SEG_LVL_ALT_LF_U" }, &cicf_VALUE_SEG_LVL_ALT_LF },
  { "VALUE_SEG_LVL_ALT_LF_V", 1, { "Data for SEG_LVL_ALT_LF_V" }, &cicf_VALUE_SEG_LVL_ALT_LF },
  { "VALUE_SEG_LVL_REF_FRAME", 1, { "Data for SEG_LVL_REF_FRAME" }, NULL },
  { "CROSS_TRANSFORM_TYPE_INTRA", 3, { "Tx size", "Tx type", "ptype" }, &cicf_CROSS_TRANSFORM_TYPE_INTRA },
  { "CROSS_TRANSFORM_TYPE_INTER", 3, { "Tx size", "Tx type", "ptype" }, &cicf_CROSS_TRANSFORM_TYPE_INTER },
  { "CROSS_COMP_REF_FRAMES", 2, { "RefFrame[0]", "RefFrame[1]" }, &cicf_CROSS_COMP_REF_FRAMES },
  { "VALUE_NUM_ANCHOR_FRAMES", 1, { "Num anchor frames" }, &cicf_VALUE_NUM_ANCHOR_FRAMES },
  { "CROSS_LF_STRENGTH", 2, { "loop_filter_sharpness", "lvl" }, NULL },
  { "CROSS_NARROW_FILTER", 2, { "hevMask", "ptype" }, NULL },
  { "CROSS_WIDE_FILTER", 2, { "log2Size", "ptype" }, &cicf_CROSS_WIDE_FILTER },
  { "VALUE_CDEF_DIRECTION", 1, { "yDir" }, NULL },
  { "CROSS_CDEF_STRENGTH", 3, { "cdef_y_pri_strength", "cdef_y_sec_strength", "CdefDamping" }, &cicf_CROSS_CDEF_STRENGTH },
  { "VALUE_SGR_PARAM_SET", 1, { "set" }, NULL },
  { "CROSS_NUM_MV_FOUND", 2, { "NumMvFound", "isCompound"}, &cicf_CROSS_NUM_MV_FOUND },
  { "CROSS_PROFILE_SWITCH", 2, { "Previous profile", "Profile" }, &cicf_CROSS_PROFILE_SWITCH },
  { "VALUE_BUTTERFLY_ANGLE", 1, { "angle" }, &cicf_VALUE_BUTTERFLY_ANGLE },
  { "VALUE_FRAME_TO_SHOW_MAP_IDX", 1, { "frame_to_show_map_idx" }, NULL },
  { "VALUE_LAST_FRAME_IDX", 1, { "last_frame_idx" }, NULL },
  { "VALUE_GOLD_FRAME_IDX", 1, { "gold_frame_idx" }, NULL },
  { "VALUE_REF_FRAME_IDX_0", 1, { "ref_frame_idx[0]" }, NULL },
  { "VALUE_REF_FRAME_IDX_1", 1, { "ref_frame_idx[1]" }, NULL },
  { "VALUE_REF_FRAME_IDX_2", 1, { "ref_frame_idx[2]" }, NULL },
  { "VALUE_REF_FRAME_IDX_3", 1, { "ref_frame_idx[3]" }, NULL },
  { "VALUE_REF_FRAME_IDX_4", 1, { "ref_frame_idx[4]" }, NULL },
  { "VALUE_REF_FRAME_IDX_5", 1, { "ref_frame_idx[5]" }, NULL },
  { "VALUE_REF_FRAME_IDX_6", 1, { "ref_frame_idx[6]" }, NULL },
  { "VALUE_LOOP_FILTER_LEVEL0", 1, { "loop_filter_level[0]" }, NULL },
  { "VALUE_LOOP_FILTER_LEVEL1", 1, { "loop_filter_level[1]" }, NULL },
  { "VALUE_LOOP_FILTER_LEVEL2", 1, { "loop_filter_level[2]" }, NULL },
  { "VALUE_LOOP_FILTER_LEVEL3", 1, { "loop_filter_level[3]" }, NULL },
  { "VALUE_LOOP_FILTER_SHARPNESS", 1, { "loop_filter_sharpness" }, NULL },
  { "CROSS_QUANTIZER_MATRIX", 3, { "SegQMLevel", "plane", "QMOffset" }, NULL },
  { "VALUE_QM_OFFSET", 1, { "txSz" }, NULL },
  { "VALUE_MROW_SCAN", 1, { "txSz" }, &cicf_VALUE_MROW_SCAN },
  { "VALUE_MCOL_SCAN", 1, { "txSz" }, &cicf_VALUE_MCOL_SCAN },
  { "VALUE_DEFAULT_SCAN", 1, { "txSz" }, &cicf_VALUE_DEFAULT_SCAN },
  { "VALUE_NUM_4X4_BLOCKS_WIDE", 1, { "planeSz" }, NULL },
  { "VALUE_NUM_4X4_BLOCKS_HIGH", 1, { "planeSz" }, NULL },
  { "VALUE_MI_WIDTH_LOG2", 1, { "planeSz" }, NULL },
  { "VALUE_MI_HEIGHT_LOG2", 1, { "planeSz" }, NULL },
  { "VALUE_SIZE_GROUP", 1, { "MiSize" }, NULL },
  { "VALUE_MAX_TX_SIZE_RECT", 1, { "MiSize" }, NULL },
  { "CROSS_PARTITION_SUBSIZE", 2, { "partition", "bSize" }, &cicf_CROSS_PARTITION_SUBSIZE },
  { "VALUE_SPLIT_TX_SIZE", 1, { "TxSize" }, &cicf_VALUE_SPLIT_TX_SIZE },
  { "VALUE_MODE_TO_TXFM", 1, { "UVMode" }, NULL },
  { "VALUE_PALETTE_COLOR_CONTEXT", 1, { "ColorContextHash" }, &cicf_VALUE_PALETTE_COLOR_CONTEXT },
  { "VALUE_PALETTE_COLOR_HASH_MULTIPLIERS", 1, { "i" }, NULL },
  { "VALUE_SM_WEIGHTS_TX_4X4", 1, { "i" }, NULL },
  { "VALUE_SM_WEIGHTS_TX_8X8", 1, { "i" }, NULL },
  { "VALUE_SM_WEIGHTS_TX_16X16", 1, { "i" }, NULL },
  { "VALUE_SM_WEIGHTS_TX_32X32", 1, { "i" }, NULL },
  { "VALUE_SM_WEIGHTS_TX_64X64", 1, { "i" }, NULL },
  { "VALUE_MODE_TO_ANGLE", 1, { "mode" }, &cicf_VALUE_MODE_TO_ANGLE },
  { "VALUE_DR_INTRA_DERIVATIVE", 1, { "pAngle" }, &cicf_VALUE_DR_INTRA_DERIVATIVE },
  { "CROSS_INTRA_FILTER_TAPS", 3, { "filter_intra_mode", "i1j1", "i" }, NULL },
  { "VALUE_TX_SIZE_SQR", 1, { "txSz" }, NULL },
  { "VALUE_TX_SIZE_SQR_UP", 1, { "txSz" }, NULL },
  { "VALUE_TX_WIDTH", 1, { "txSz" }, NULL },
  { "VALUE_TX_HEIGHT", 1, { "txSz" }, NULL },
  { "VALUE_TX_WIDTH_LOG2", 1, { "txSz" }, NULL },
  { "VALUE_TX_HEIGHT_LOG2", 1, { "txSz" }, NULL },
  { "VALUE_WEDGE_BITS", 1, { "bSize" }, & cicf_VALUE_WEDGE_BITS },
  { "CROSS_SIG_REG_DIFF_OFFSET", 3, { "txClass", "idx", "col" }, NULL },
  { "VALUE_ADJUSTED_TX_SIZE", 1, { "txSz" }, NULL },
  { "VALUE_GAUSSIAN_SEQUENCE", 1, { "randomNum" }, NULL },
  { "VALUE_SEGMENTATION_FEATURE_BITS", 1, { "feature" }, NULL },
  { "VALUE_SEGMENTATION_FEATURE_SIGNED", 1, { "feature" }, NULL },
  { "VALUE_SEGMENTATION_FEATURE_MAX", 1, { "feature" }, NULL },
  { "VALUE_REMAP_LR_TYPE", 1, { "lr_type" }, NULL },
  { "VALUE_SGRPROJ_XQD_MID", 1, { "ppass" }, NULL },
  { "VALUE_WIENER_TAPS_MID", 1, { "i" }, NULL },
  { "VALUE_MAX_TX_DEPTH", 1, { "MiSize" }, NULL },
  { "CROSS_SUBSAMPLED_SIZE", 3, { "subsize", "subx", "suby" }, &cicf_CROSS_SUBSAMPLED_SIZE },
  { "CROSS_TX_TYPE_IN_SET_INTER", 2, { "txSet", "txType" }, &cicf_CROSS_TX_TYPE_IN_SET_INTER },
  { "CROSS_TX_TYPE_IN_SET_INTRA", 2, { "txSet", "txType" }, &cicf_CROSS_TX_TYPE_IN_SET_INTRA },
  { "VALUE_TX_TYPE_INTRA_INV_SET1", 1, { "intraSet1TxType" }, NULL },
  { "VALUE_TX_TYPE_INTRA_INV_SET2", 1, { "intraSet2TxType" }, NULL },
  { "VALUE_TX_TYPE_INTER_INV_SET1", 1, { "interSet1TxType" }, NULL },
  { "VALUE_TX_TYPE_INTER_INV_SET2", 1, { "interSet2TxType" }, NULL },
  { "VALUE_TX_TYPE_INTER_INV_SET3", 1, { "interSet3TxType" }, NULL },
  { "VALUE_WIENER_TAPS_MIN", 1, { "j" }, NULL },
  { "VALUE_WIENER_TAPS_MAX", 1, { "j" }, NULL },
  { "VALUE_WIENER_TAPS_K", 1, { "j" }, NULL },
  { "VALUE_SGRPROJ_XQD_MIN", 1, { "i" }, NULL },
  { "VALUE_SGRPROJ_XQD_MAX", 1, { "i" }, NULL },
  { "VALUE_REF_FRAME_LIST", 1, { "i" }, NULL },
  { "VALUE_DIV_MULT", 1, { "clippedDenominator" }, &cicf_VALUE_DIV_MULT },
  { "CROSS_INTRA_EDGE_KERNEL", 2, { "kernel", "edge" }, NULL },
  { "CROSS_SUBPEL_FILTERS", 3, { "interpFilter", "offset", "t" }, NULL },
  { "CROSS_WARPED_FILTERS", 2, { "offset", "m" }, NULL },
  { "VALUE_DIV_LUT", 1, { "f" }, NULL },
  { "VALUE_OBMC_MASK_2", 1, { "i" }, NULL },
  { "VALUE_OBMC_MASK_4", 1, { "i" }, NULL },
  { "VALUE_OBMC_MASK_8", 1, { "i" }, NULL },
  { "VALUE_OBMC_MASK_16", 1, { "i" }, NULL },
  { "VALUE_OBMC_MASK_32", 1, { "i" }, NULL },
  { "VALUE_WEDGE_MASTER_OBLIQUE_EVEN", 1, { "mask_master" }, NULL },
  { "VALUE_WEDGE_MASTER_OBLIQUE_ODD", 1, { "mask_master" }, NULL },
  { "VALUE_WEDGE_MASTER_OBLIQUE_VERTICAL", 1, { "j" }, NULL },
  { "CROSS_WEDGE_CODEBOOK", 3, { "shape", "index", "pos" }, NULL },
  { "VALUE_II_WEIGHTS_1D", 1, { "SbSize" }, &cicf_VALUE_II_WEIGHTS_1D },
  { "CROSS_QUANT_DIST_WEIGHT", 2, { "idx", "order" }, &cicf_CROSS_QUANT_DIST_WEIGHT },
  { "CROSS_QUANT_DIST_LOOKUP", 2, { "idx", "order" }, NULL },
  { "CROSS_DC_QLOOKUP", 2, { "bitDepthIdx", "B" }, &cicf_CROSS_XC_LOOKUP },
  { "CROSS_AC_QLOOKUP", 2, { "bitDepthIdx", "B" }, &cicf_CROSS_XC_LOOKUP },
  { "VALUE_COS128_LOOKUP", 1, { "angleIdx" }, &cicf_VALUE_COS128_LOOKUP },
  { "VALUE_TRANSFORM_ROW_SHIFT", 1, { "txSz" }, NULL },
  { "CROSS_CDEF_UV_DIR", 3, { "subsampling_x", "subsampling_y", "yDir" }, &cicf_CROSS_CDEF_UV_DIR  },
  { "VALUE_DIV_TABLE", 1, { "idx" }, NULL },
  { "CROSS_CDEF_PRI_TAPS", 2, { "pri", "k" }, NULL },
  { "CROSS_CDEF_SEC_TAPS", 2, { "pri", "k" }, NULL },
  { "CROSS_CDEF_DIRECTIONS", 3, { "dir", "k", "idx" }, NULL },
  { "CROSS_UPSCALE_FILTER", 2, { "srcXSubpel", "k" }, NULL },
  { "CROSS_SGR_PARAMS", 2, { "xa", "xb" }, &cicf_CROSS_SGR_PARAMS },
  { "VALUE_INTRA_MODE_CONTEXT", 1, { "intra_mode" }, NULL },
  { "CROSS_COMPOUND_MODE_CTX_MAP", 2, { "refMvCtx", "newMvCtx" }, &cicf_CROSS_COMPOUND_MODE_CTX_MAP },
  { "CROSS_COEFF_BASE_CTX_OFFSET", 3, { "txSz", "row", "col" }, &cicf_CROSS_COEFF_BASE_CTX_OFFSET },
  { "VALUE_COEFF_BASE_POS_CTX_OFFSET", 1, { "idx" }, NULL },
  { "CROSS_MAG_REF_OFFSET_WITH_TX_CLASS", 3, { "txClass", "cbrIdx", "ref" }, NULL },
  { "VALUE_FILTER_INTRA_MODE_TO_INTRA_DIR", 1, { "filter_intra_mode" }, NULL },
};

void profile_cross(int id, ...);
#endif
