/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_OUTPUT 1
#else
#define VALIDATE_SPEC_OUTPUT 0
#endif
#define VALIDATE_SPEC_FILM_GRAIN 0
#define VALIDATE_SPEC_MFMV_STORAGE 0
int Gaussian_Sequence[ 2048 ] = {
    56,    568,   -180,  172,   124,   -84,   172,   -64,   -900,  24,   820,
  224,   1248,  996,   272,   -8,    -916,  -388,  -732,  -104,  -188, 800,
  112,   -652,  -320,  -376,  140,   -252,  492,   -168,  44,    -788, 588,
  -584,  500,   -228,  12,    680,   272,   -476,  972,   -100,  652,  368,
  432,   -196,  -720,  -192,  1000,  -332,  652,   -136,  -552,  -604, -4,
  192,   -220,  -136,  1000,  -52,   372,   -96,   -624,  124,   -24,  396,
  540,   -12,   -104,  640,   464,   244,   -208,  -84,   368,   -528, -740,
  248,   -968,  -848,  608,   376,   -60,   -292,  -40,   -156,  252,  -292,
  248,   224,   -280,  400,   -244,  244,   -60,   76,    -80,   212,  532,
  340,   128,   -36,   824,   -352,  -60,   -264,  -96,   -612,  416,  -704,
  220,   -204,  640,   -160,  1220,  -408,  900,   336,   20,    -336, -96,
  -792,  304,   48,    -28,   -1232, -1172, -448,  104,   -292,  -520, 244,
  60,    -948,  0,     -708,  268,   108,   356,   -548,  488,   -344, -136,
  488,   -196,  -224,  656,   -236,  -1128, 60,    4,     140,   276,  -676,
  -376,  168,   -108,  464,   8,     564,   64,    240,   308,   -300, -400,
  -456,  -136,  56,    120,   -408,  -116,  436,   504,   -232,  328,  844,
  -164,  -84,   784,   -168,  232,   -224,  348,   -376,  128,   568,  96,
  -1244, -288,  276,   848,   832,   -360,  656,   464,   -384,  -332, -356,
  728,   -388,  160,   -192,  468,   296,   224,   140,   -776,  -100, 280,
  4,     196,   44,    -36,   -648,  932,   16,    1428,  28,    528,  808,
  772,   20,    268,   88,    -332,  -284,  124,   -384,  -448,  208,  -228,
  -1044, -328,  660,   380,   -148,  -300,  588,   240,   540,   28,   136,
  -88,   -436,  256,   296,   -1000, 1400,  0,     -48,   1056,  -136, 264,
  -528,  -1108, 632,   -484,  -592,  -344,  796,   124,   -668,  -768, 388,
  1296,  -232,  -188,  -200,  -288,  -4,    308,   100,   -168,  256,  -500,
  204,   -508,  648,   -136,  372,   -272,  -120,  -1004, -552,  -548, -384,
  548,   -296,  428,   -108,  -8,    -912,  -324,  -224,  -88,   -112, -220,
  -100,  996,   -796,  548,   360,   -216,  180,   428,   -200,  -212, 148,
  96,    148,   284,   216,   -412,  -320,  120,   -300,  -384,  -604, -572,
  -332,  -8,    -180,  -176,  696,   116,   -88,   628,   76,    44,   -516,
  240,   -208,  -40,   100,   -592,  344,   -308,  -452,  -228,  20,   916,
  -1752, -136,  -340,  -804,  140,   40,    512,   340,   248,   184,  -492,
  896,   -156,  932,   -628,  328,   -688,  -448,  -616,  -752,  -100, 560,
  -1020, 180,   -800,  -64,   76,    576,   1068,  396,   660,   552,  -108,
  -28,   320,   -628,  312,   -92,   -92,   -472,  268,   16,    560,  516,
  -672,  -52,   492,   -100,  260,   384,   284,   292,   304,   -148, 88,
  -152,  1012,  1064,  -228,  164,   -376,  -684,  592,   -392,  156,  196,
  -524,  -64,   -884,  160,   -176,  636,   648,   404,   -396,  -436, 864,
  424,   -728,  988,   -604,  904,   -592,  296,   -224,  536,   -176, -920,
  436,   -48,   1176,  -884,  416,   -776,  -824,  -884,  524,   -548, -564,
  -68,   -164,  -96,   692,   364,   -692,  -1012, -68,   260,   -480, 876,
  -1116, 452,   -332,  -352,  892,   -1088, 1220,  -676,  12,    -292, 244,
  496,   372,   -32,   280,   200,   112,   -440,  -96,   24,    -644, -184,
  56,    -432,  224,   -980,  272,   -260,  144,   -436,  420,   356,  364,
  -528,  76,    172,   -744,  -368,  404,   -752,  -416,  684,   -688, 72,
  540,   416,   92,    444,   480,   -72,   -1416, 164,   -1172, -68,  24,
  424,   264,   1040,  128,   -912,  -524,  -356,  64,    876,   -12,  4,
  -88,   532,   272,   -524,  320,   276,   -508,  940,   24,    -400, -120,
  756,   60,    236,   -412,  100,   376,   -484,  400,   -100,  -740, -108,
  -260,  328,   -268,  224,   -200,  -416,  184,   -604,  -564,  -20,  296,
  60,    892,   -888,  60,    164,   68,    -760,  216,   -296,  904,  -336,
  -28,   404,   -356,  -568,  -208,  -1480, -512,  296,   328,   -360, -164,
  -1560, -776,  1156,  -428,  164,   -504,  -112,  120,   -216,  -148, -264,
  308,   32,    64,    -72,   72,    116,   176,   -64,   -272,  460,  -536,
  -784,  -280,  348,   108,   -752,  -132,  524,   -540,  -776,  116,  -296,
  -1196, -288,  -560,  1040,  -472,  116,   -848,  -1116, 116,   636,  696,
  284,   -176,  1016,  204,   -864,  -648,  -248,  356,   972,   -584, -204,
  264,   880,   528,   -24,   -184,  116,   448,   -144,  828,   524,  212,
  -212,  52,    12,    200,   268,   -488,  -404,  -880,  824,   -672, -40,
  908,   -248,  500,   716,   -576,  492,   -576,  16,    720,   -108, 384,
  124,   344,   280,   576,   -500,  252,   104,   -308,  196,   -188, -8,
  1268,  296,   1032,  -1196, 436,   316,   372,   -432,  -200,  -660, 704,
  -224,  596,   -132,  268,   32,    -452,  884,   104,   -1008, 424,  -1348,
  -280,  4,     -1168, 368,   476,   696,   300,   -8,    24,    180,  -592,
  -196,  388,   304,   500,   724,   -160,  244,   -84,   272,   -256, -420,
  320,   208,   -144,  -156,  156,   364,   452,   28,    540,   316,  220,
  -644,  -248,  464,   72,    360,   32,    -388,  496,   -680,  -48,  208,
  -116,  -408,  60,    -604,  -392,  548,   -840,  784,   -460,  656,  -544,
  -388,  -264,  908,   -800,  -628,  -612,  -568,  572,   -220,  164,  288,
  -16,   -308,  308,   -112,  -636,  -760,  280,   -668,  432,   364,  240,
  -196,  604,   340,   384,   196,   592,   -44,   -500,  432,   -580, -132,
  636,   -76,   392,   4,     -412,  540,   508,   328,   -356,  -36,  16,
  -220,  -64,   -248,  -60,   24,    -192,  368,   1040,  92,    -24,  -1044,
  -32,   40,    104,   148,   192,   -136,  -520,  56,    -816,  -224, 732,
  392,   356,   212,   -80,   -424,  -1008, -324,  588,   -1496, 576,  460,
  -816,  -848,  56,    -580,  -92,   -1372, -112,  -496,  200,   364,  52,
  -140,  48,    -48,   -60,   84,    72,    40,    132,   -356,  -268, -104,
  -284,  -404,  732,   -520,  164,   -304,  -540,  120,   328,   -76,  -460,
  756,   388,   588,   236,   -436,  -72,   -176,  -404,  -316,  -148, 716,
  -604,  404,   -72,   -88,   -888,  -68,   944,   88,    -220,  -344, 960,
  472,   460,   -232,  704,   120,   832,   -228,  692,   -508,  132,  -476,
  844,   -748,  -364,  -44,   1116,  -1104, -1056, 76,    428,   552,  -692,
  60,    356,   96,    -384,  -188,  -612,  -576,  736,   508,   892,  352,
  -1132, 504,   -24,   -352,  324,   332,   -600,  -312,  292,   508,  -144,
  -8,    484,   48,    284,   -260,  -240,  256,   -100,  -292,  -204, -44,
  472,   -204,  908,   -188,  -1000, -256,  92,    1164,  -392,  564,  356,
  652,   -28,   -884,  256,   484,   -192,  760,   -176,  376,   -524, -452,
  -436,  860,   -736,  212,   124,   504,   -476,  468,   76,    -472, 552,
  -692,  -944,  -620,  740,   -240,  400,   132,   20,    192,   -196, 264,
  -668,  -1012, -60,   296,   -316,  -828,  76,    -156,  284,   -768, -448,
  -832,  148,   248,   652,   616,   1236,  288,   -328,  -400,  -124, 588,
  220,   520,   -696,  1032,  768,   -740,  -92,   -272,  296,   448,  -464,
  412,   -200,  392,   440,   -200,  264,   -152,  -260,  320,   1032, 216,
  320,   -8,    -64,   156,   -1016, 1084,  1172,  536,   484,   -432, 132,
  372,   -52,   -256,  84,    116,   -352,  48,    116,   304,   -384, 412,
  924,   -300,  528,   628,   180,   648,   44,    -980,  -220,  1320, 48,
  332,   748,   524,   -268,  -720,  540,   -276,  564,   -344,  -208, -196,
  436,   896,   88,    -392,  132,   80,    -964,  -288,  568,   56,   -48,
  -456,  888,   8,     552,   -156,  -292,  948,   288,   128,   -716, -292,
  1192,  -152,  876,   352,   -600,  -260,  -812,  -468,  -28,   -120, -32,
  -44,   1284,  496,   192,   464,   312,   -76,   -516,  -380,  -456, -1012,
  -48,   308,   -156,  36,    492,   -156,  -808,  188,   1652,  68,   -120,
  -116,  316,   160,   -140,  352,   808,   -416,  592,   316,   -480, 56,
  528,   -204,  -568,  372,   -232,  752,   -344,  744,   -4,    324,  -416,
  -600,  768,   268,   -248,  -88,   -132,  -420,  -432,  80,    -288, 404,
  -316,  -1216, -588,  520,   -108,  92,    -320,  368,   -480,  -216, -92,
  1688,  -300,  180,   1020,  -176,  820,   -68,   -228,  -260,  436,  -904,
  20,    40,    -508,  440,   -736,  312,   332,   204,   760,   -372, 728,
  96,    -20,   -632,  -520,  -560,  336,   1076,  -64,   -532,  776,  584,
  192,   396,   -728,  -520,  276,   -188,  80,    -52,   -612,  -252, -48,
  648,   212,   -688,  228,   -52,   -260,  428,   -412,  -272,  -404, 180,
  816,   -796,  48,    152,   484,   -88,   -216,  988,   696,   188,  -528,
  648,   -116,  -180,  316,   476,   12,    -564,  96,    476,   -252, -364,
  -376,  -392,  556,   -256,  -576,  260,   -352,  120,   -16,   -136, -260,
  -492,  72,    556,   660,   580,   616,   772,   436,   424,   -32,  -324,
  -1268, 416,   -324,  -80,   920,   160,   228,   724,   32,    -516, 64,
  384,   68,    -128,  136,   240,   248,   -204,  -68,   252,   -932, -120,
  -480,  -628,  -84,   192,   852,   -404,  -288,  -132,  204,   100,  168,
  -68,   -196,  -868,  460,   1080,  380,   -80,   244,   0,     484,  -888,
  64,    184,   352,   600,   460,   164,   604,   -196,  320,   -64,  588,
  -184,  228,   12,    372,   48,    -848,  -344,  224,   208,   -200, 484,
  128,   -20,   272,   -468,  -840,  384,   256,   -720,  -520,  -464, -580,
  112,   -120,  644,   -356,  -208,  -608,  -528,  704,   560,   -424, 392,
  828,   40,    84,    200,   -152,  0,     -144,  584,   280,   -120, 80,
  -556,  -972,  -196,  -472,  724,   80,    168,   -32,   88,    160,  -688,
  0,     160,   356,   372,   -776,  740,   -128,  676,   -248,  -480, 4,
  -364,  96,    544,   232,   -1032, 956,   236,   356,   20,    -40,  300,
  24,    -676,  -596,  132,   1120,  -104,  532,   -1096, 568,   648,  444,
  508,   380,   188,   -376,  -604,  1488,  424,   24,    756,   -220, -192,
  716,   120,   920,   688,   168,   44,    -460,  568,   284,   1144, 1160,
  600,   424,   888,   656,   -356,  -320,  220,   316,   -176,  -724, -188,
  -816,  -628,  -348,  -228,  -380,  1012,  -452,  -660,  736,   928,  404,
  -696,  -72,   -268,  -892,  128,   184,   -344,  -780,  360,   336,  400,
  344,   428,   548,   -112,  136,   -228,  -216,  -820,  -516,  340,  92,
  -136,  116,   -300,  376,   -244,  100,   -316,  -520,  -284,  -12,  824,
  164,   -548,  -180,  -128,  116,   -924,  -828,  268,   -368,  -580, 620,
  192,   160,   0,     -1676, 1068,  424,   -56,   -360,  468,   -156, 720,
  288,   -528,  556,   -364,  548,   -148,  504,   316,   152,   -648, -620,
  -684,  -24,   -376,  -384,  -108,  -920,  -1032, 768,   180,   -264, -508,
  -1268, -260,  -60,   300,   -240,  988,   724,   -376,  -576,  -212, -736,
  556,   192,   1092,  -620,  -880,  376,   -56,   -4,    -216,  -32,  836,
  268,   396,   1332,  864,   -600,  100,   56,    -412,  -92,   356,  180,
  884,   -468,  -436,  292,   -388,  -804,  -704,  -840,  368,   -348, 140,
  -724,  1536,  940,   372,   112,   -372,  436,   -480,  1136,  296,  -32,
  -228,  132,   -48,   -220,  868,   -1016, -60,   -1044, -464,  328,  916,
  244,   12,    -736,  -296,  360,   468,   -376,  -108,  -92,   788,  368,
  -56,   544,   400,   -672,  -420,  728,   16,    320,   44,    -284, -380,
  -796,  488,   132,   204,   -596,  -372,  88,    -152,  -908,  -636, -572,
  -624,  -116,  -692,  -200,  -56,   276,   -88,   484,   -324,  948,  864,
  1000,  -456,  -184,  -276,  292,   -296,  156,   676,   320,   160,  908,
  -84,   -1236, -288,  -116,  260,   -372,  -644,  732,   -756,  -96,  84,
  344,   -520,  348,   -688,  240,   -84,   216,   -1044, -136,  -676, -396,
  -1500, 960,   -40,   176,   168,   1516,  420,   -504,  -344,  -364, -360,
  1216,  -940,  -380,  -212,  252,   -660,  -708,  484,   -444,  -152, 928,
  -120,  1112,  476,   -260,  560,   -148,  -344,  108,   -196,  228,  -288,
  504,   560,   -328,  -88,   288,   -1008, 460,   -228,  468,   -836, -196,
  76,    388,   232,   412,   -1168, -716,  -644,  756,   -172,  -356, -504,
  116,   432,   528,   48,    476,   -168,  -608,  448,   160,   -532, -272,
  28,    -676,  -12,   828,   980,   456,   520,   104,   -104,  256,  -344,
  -4,    -28,   -368,  -52,   -524,  -572,  -556,  -200,  768,   1124, -208,
  -512,  176,   232,   248,   -148,  -888,  604,   -600,  -304,  804,  -156,
  -212,  488,   -192,  -804,  -256,  368,   -360,  -916,  -328,  228,  -240,
  -448,  -472,  856,   -556,  -364,  572,   -12,   -156,  -368,  -340, 432,
  252,   -752,  -152,  288,   268,   -580,  -848,  -592,  108,   -76,  244,
  312,   -716,  592,   -80,   436,   360,   4,     -248,  160,   516,  584,
  732,   44,    -468,  -280,  -292,  -156,  -588,  28,    308,   912,  24,
  124,   156,   180,   -252,  944,   -924,  -772,  -520,  -428,  -624, 300,
  -212,  -1144, 32,    -724,  800,   -1128, -212,  -1288, -848,  180,  -416,
  440,   192,   -576,  -792,  -76,   -1080, 80,    -532,  -352,  -132, 380,
  -820,  148,   1112,  128,   164,   456,   700,   -924,  144,   -668, -384,
  648,   -832,  508,   552,   -52,   -100,  -656,  208,   -568,  748,  -88,
  680,   232,   300,   192,   -408,  -1012, -152,  -252,  -268,  272,  -876,
  -664,  -648,  -332,  -136,  16,    12,    1152,  -28,   332,   -536, 320,
  -672,  -460,  -316,  532,   -260,  228,   -40,   1052,  -816,  180,  88,
  -496,  -556,  -672,  -368,  428,   92,    356,   404,   -408,  252,  196,
  -176,  -556,  792,   268,   32,    372,   40,    96,    -332,  328,  120,
  372,   -900,  -40,   472,   -264,  -592,  952,   128,   656,   112,  664,
  -232,  420,   4,     -344,  -464,  556,   244,   -416,  -32,   252,  0,
  -412,  188,   -696,  508,   -476,  324,   -1096, 656,   -312,  560,  264,
  -136,  304,   160,   -64,   -580,  248,   336,   -720,  560,   -348, -288,
  -276,  -196,  -500,  852,   -544,  -236,  -1128, -992,  -776,  116,  56,
  52,    860,   884,   212,   -12,   168,   1020,  512,   -552,  924,  -148,
  716,   188,   164,   -340,  -520,  -184,  880,   -152,  -680,  -208, -1156,
  -300,  -528,  -472,  364,   100,   -744,  -1056, -32,   540,   280,  144,
  -676,  -32,   -232,  -280,  -224,  96,    568,   -76,   172,   148,  148,
  104,   32,    -296,  -32,   788,   -80,   32,    -16,   280,   288,  944,
  428,   -484
};

get_random_number( bits ) {
    r = RandomRegister
    bit = ((r >> 0) ^ (r >> 1) ^ (r >> 3) ^ (r >> 12)) & 1
    r = (r >> 1) | (bit << 15)
    result = (r >> (16 - bits)) & ((1 << bits) - 1)
    RandomRegister = r
    return result
}

generate_grain( ) {
    uint_0_2047 randomNum
    shift = 12 - BitDepth + grain_scale_shift
    for ( y = 0; y < 73; y++ ) {
      for ( x = 0; x < 82; x++ ) {
        if ( num_y_points > 0 ) {
          randomNum = get_random_number( 11 )
          COVERCROSS(VALUE_GAUSSIAN_SEQUENCE, randomNum)
          g = Gaussian_Sequence[ randomNum ]
        } else {
          g = 0
        }
        LumaGrain[ y ][ x ] = Round2( g, shift )
      }
    }
    shift = ar_coeff_shift_minus_6 + 6
    for ( y = 3; y < 73; y++ ) {
      for ( x = 3; x < 82 - 3; x++ ) {
        s = 0
        pos = 0
        for ( deltaRow = -ar_coeff_lag; deltaRow <= 0; deltaRow++ ) {
          for ( deltaCol = -ar_coeff_lag; deltaCol <= ar_coeff_lag; deltaCol++ ) {
            if ( deltaRow == 0 && deltaCol == 0 )
              break
            c = ar_coeffs_y_plus_128[ pos ] - 128
            s += LumaGrain[ y + deltaRow ][ x + deltaCol ] * c // E-401 [RNG-FilmGrain1]
            pos++
          }
        }
        LumaGrain[ y ][ x ] = Clip3( GrainMin, GrainMax, LumaGrain[ y ][ x ] + Round2( s, shift ) ) // E-402 [RNG-FilmGrain2]
      }
    }
    chromaW = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) ? 44 : 82
    chromaH = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) ? 38 : 73
    shift = 12 - BitDepth + grain_scale_shift
    RandomRegister = grain_seed ^ 0xb524
    for ( y = 0; y < chromaH; y++ ) {
      for ( x = 0; x < chromaW; x++ ) {
        if ( num_cb_points > 0 || chroma_scaling_from_luma ) {
          randomNum = get_random_number( 11 )
          COVERCROSS(VALUE_GAUSSIAN_SEQUENCE, randomNum)
          g = Gaussian_Sequence[ randomNum ]
        } else {
          g = 0
        }
        CbGrain[ y ][ x ] = Round2( g, shift )
      }
    }
    RandomRegister = grain_seed ^ 0x49d8
    for ( y = 0; y < chromaH; y++ ) {
      for ( x = 0; x < chromaW; x++ ) {
        if ( num_cr_points > 0 || chroma_scaling_from_luma ) {
          randomNum = get_random_number( 11 )
          COVERCROSS(VALUE_GAUSSIAN_SEQUENCE, randomNum)
          g = Gaussian_Sequence[ randomNum ]
        } else {
          g = 0
        }
        CrGrain[ y ][ x ] = Round2( g, shift )
      }
    }
    shift = ar_coeff_shift_minus_6 + 6
    for ( y = 3; y < chromaH; y++ ) {
      for ( x = 3; x < chromaW - 3; x++ ) {
        s0 = 0
        s1 = 0
        pos = 0
        for ( deltaRow = -ar_coeff_lag; deltaRow <= 0; deltaRow++ ) {
          for ( deltaCol = -ar_coeff_lag; deltaCol <= ar_coeff_lag; deltaCol++ ) {
            c0 = ar_coeffs_cb_plus_128[ pos ] - 128
            c1 = ar_coeffs_cr_plus_128[ pos ] - 128
            if ( deltaRow == 0 && deltaCol == 0 ) {
              if ( num_y_points > 0 ) {
                luma = 0
                lumaX = ( (x - 3) << COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) ) + 3
                lumaY = ( (y - 3) << COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) ) + 3
                for ( i = 0; i <= COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y); i = COVERCLASS((PROFILE0 || PROFILE2), 1, i) + 1 )
                    for ( j = 0; j <= COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x); j = COVERCLASS((PROFILE0 || PROFILE2), 1, j) + 1 )
                        luma += LumaGrain[ lumaY + COVERCLASS((PROFILE0 || PROFILE2), 1, i) ][ lumaX + COVERCLASS((PROFILE0 || PROFILE2), 1, j) ] // E-403 [RNG-FilmGrain3]
                luma = Round2( luma, COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) + COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) ) // E-404 [RNG-FilmGrain4]
                s0 += luma * c0 // E-405 [RNG-FilmGrain5]
                s1 += luma * c1 // E-406 [RNG-FilmGrain6]
              }
              break
            }
            s0 += CbGrain[ y + deltaRow ][ x + deltaCol ] * c0 // E-407 [RNG-FilmGrain7]
            s1 += CrGrain[ y + deltaRow ][ x + deltaCol ] * c1 // E-408 [RNG-FilmGrain8]
            pos++
          }
        }
        CbGrain[ y ][ x ] = Clip3( GrainMin, GrainMax, CbGrain[ y ][ x ] + Round2( s0, shift ) ) // E-409 [RNG-FilmGrain9]
        CrGrain[ y ][ x ] = Clip3( GrainMin, GrainMax, CrGrain[ y ][ x ] + Round2( s1, shift ) ) // E-410 [RNG-FilmGrain10]
      }
    }
}

get_x( plane, i ) {
    if ( plane == 0 || chroma_scaling_from_luma)
        return point_y_value[ i ]
    else if ( plane == 1 )
        return point_cb_value[ i ]
    else
        return point_cr_value[ i ]
}

get_y( plane, i ) {
    if ( plane == 0 || chroma_scaling_from_luma)
        return point_y_scaling[ i ]
    else if ( plane == 1 )
        return point_cb_scaling[ i ]
    else
        return point_cr_scaling[ i ]
}

scaling_lookup_initialization( ) {
    for ( plane = 0; plane < NumPlanes; plane++ ) {
        if ( plane == 0 || chroma_scaling_from_luma)
            numPoints = num_y_points
        else if ( plane == 1 )
            numPoints = num_cb_points
        else
            numPoints = num_cr_points
        if ( numPoints == 0 ) {
            for ( x = 0; x < 256; x++ ) {
                ScalingLut[ plane ][ x ] = 0
            }
        } else {
            for ( x = 0; x < get_x( plane, 0 ); x++ ) {
                ScalingLut[ plane ][ x ] = get_y( plane, 0 )
            }
            for ( i = 0; i < numPoints - 1; i++ ) {
                deltaY = get_y( plane, i + 1 ) - get_y( plane, i )
                deltaX = get_x( plane, i + 1 ) - get_x( plane, i )
                delta = deltaY * ( ( 65536 + (deltaX >> 1) ) / deltaX )
                for ( x = 0; x < deltaX; x++ ) {
                    v = get_y( plane, i ) + ( ( x * delta + 32768 ) >> 16 )
                    ScalingLut[ plane ][ get_x( plane, i )  + x ] = v
                }
            }
            for ( x = get_x( plane, numPoints - 1 ); x < 256; x++ ) {
                ScalingLut[ plane ][ x ] = get_y( plane, numPoints - 1 )
            }
        }
    }
}

scale_lut( plane, index ) {
  shift = BitDepth - 8
  x = index >> shift
  rem = index - ( x << shift )
  if ( BitDepth == 8 || x == 255) {
    return ScalingLut[ plane ][ x ]
  } else {
    start = ScalingLut[ plane ][ x ]
    end = ScalingLut[ plane ][ x + 1 ]
    return start + Round2( (end - start) * rem, shift )
  }
}

add_noise_synthesis( w, h, subX, subY ) {
    lumaNum = 0
    for ( y = 0; y < (h + 1)/2 ; y += 16 ) {
        RandomRegister = grain_seed
        RandomRegister ^= ((lumaNum * 37 + 178) & 255) << 8
        RandomRegister ^= ((lumaNum * 173 + 105) & 255)
#if VALIDATE_SPEC_FILM_GRAIN
        validate(19002)
        validate(grain_seed)
        validate(RandomRegister)
#endif
        for ( x = 0; x < (w + 1)/2 ; x += 16 ) {
            rand = get_random_number( 8 )
            offsetX = rand >> 4
            offsetY = rand & 15
#if VALIDATE_SPEC_FILM_GRAIN
            validate(19003)
            validate(rand)
            validate(offsetX)
            validate(offsetY)
#endif
            for ( plane = 0 ; plane < NumPlanes; plane++ ) {
                planeSubX = ( plane > 0) ? subX : 0
                planeSubY = ( plane > 0) ? subY : 0
                planeOffsetX = COVERCLASS((PROFILE0 || PROFILE2), 1, planeSubX) ? 6 + offsetX : 9 + offsetX * 2
                planeOffsetY = COVERCLASS((PROFILE0 || PROFILE2), 1, planeSubY) ? 6 + offsetY : 9 + offsetY * 2
                for ( i = 0; i < 34 >> planeSubY ; i++ ) {
                    for ( j = 0; j < 34 >> planeSubX ; j++ ) {
#if VALIDATE_SPEC_FILM_GRAIN
                        validate(19001)
                        validate(lumaNum)
                        validate(plane)
                        validate(i)
                        validate(x+j)
                        validate(planeSubX)
#endif
                        if ( plane == 0 )
                            g = LumaGrain[ planeOffsetY + i ][ planeOffsetX + j ]
                        else if ( plane == 1 )
                            g = CbGrain[ planeOffsetY + i ][ planeOffsetX + j ]
                        else
                            g = CrGrain[ planeOffsetY + i ][ planeOffsetX + j ]
#if VALIDATE_SPEC_FILM_GRAIN
                        validate(g)
#endif
                        if ( COVERCLASS(1, (PROFILE0 || PROFILE2), planeSubX == 0) ) {
                            if ( j < 2 && overlap_flag && x > 0 ) {
                                old = noiseStripe[ lumaNum ][ plane ][ i ][ x * 2 + j ]
                                if ( j == 0 ) {
                                    g = old * 27 + g * 17 // E-411 [RNG-FilmGrain11]
                                } else {
                                    g = old * 17 + g * 27 // E-412 [RNG-FilmGrain12]
                                }
                                g = Clip3( GrainMin, GrainMax, Round2(g, 5) ) // E-413 [RNG-FilmGrain13]
                            }
                            noiseStripe[ lumaNum ][ plane ][ i ][ x * 2 + j ] = g
                        } else {
                            if ( j == 0 && overlap_flag && x > 0 ) {
                                old = noiseStripe[ lumaNum ][ plane ][ i ][ x + j ]
                                g = old * 23 + g * 22 // E-414 [RNG-FilmGrain14]
                                g = Clip3( GrainMin, GrainMax, Round2(g, 5) ) // E-415 [RNG-FilmGrain15]
                            }
                            noiseStripe[ lumaNum ][ plane ][ i ][ x + j ] = g
                        }
#if VALIDATE_SPEC_FILM_GRAIN
                        validate(g)
#endif
                    }
                }
            }
        }
        lumaNum++
    }
    for ( plane = 0; plane < NumPlanes; plane++ ) {
        planeSubX = ( plane > 0) ? subX : 0
        planeSubY = ( plane > 0) ? subY : 0
        for ( y = 0; y < ( (h + planeSubY) >> planeSubY ) ; y++ ) {
            lumaNum = y >> ( 5 - planeSubY )
            i = y - (lumaNum << ( 5 - planeSubY ) )
            for ( x = 0; x < ( (w + planeSubX) >> planeSubX) ; x++ ) {
                g = noiseStripe[ lumaNum ][ plane ][ i ][ x ]
                if ( COVERCLASS(1, (PROFILE0 || PROFILE2), planeSubY == 0) ) {
                    if ( i < 2 && lumaNum > 0 && overlap_flag ) {
                        old = noiseStripe[ lumaNum - 1 ][ plane ][ i + 32 ][ x ]
                        if ( i == 0 ) {
                            g = old * 27 + g * 17 // E-416 [RNG-FilmGrain16]
                        } else {
                            g = old * 17 + g * 27 // E-417 [RNG-FilmGrain17]
                        }
                        g = Clip3( GrainMin, GrainMax, Round2(g, 5) ) // E-418 [RNG-FilmGrain18]
                    }
                } else {
                    if ( i < 1 && lumaNum > 0 && overlap_flag ) {
                        old = noiseStripe[ lumaNum - 1 ][ plane ][ i + 16 ][ x ]
                        g = old * 23 + g * 22 // E-419 [RNG-FilmGrain19]
                        g = Clip3( GrainMin, GrainMax, Round2(g, 5) ) // E-420 [RNG-FilmGrain20]
                    }
                }
                noiseImage[ plane ][ y ][ x ] = g
#if VALIDATE_SPEC_FILM_GRAIN
                validate(19000)
                validate(plane)
                validate(y)
                validate(x)
                validate(g)
#endif
            }
        }
    }
    if ( clip_to_restricted_range ) {
        minValue = 16 << (BitDepth - 8)
        maxLuma = 235 << (BitDepth - 8)
        if ( COVERCLASS((PROFILE1 || PROFILE2), 1, matrix_coefficients == MC_IDENTITY) )
          maxChroma = maxLuma
        else
          maxChroma = 240 << (BitDepth - 8)
    } else {
        minValue = 0
        maxLuma = (256 << (BitDepth - 8)) - 1
        maxChroma = maxLuma
    }
    ScalingShift = grain_scaling_minus_8 + 8
    for ( y = 0; y < ( (h + subY) >> subY) ; y++ ) {
        for ( x = 0; x < ( (w + subX) >> subX) ; x++ ) {
            lumaX = x << subX
            lumaY = y << subY
            lumaNextX = Min( lumaX + 1, w - 1 )
            if ( COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subX) )
                averageLuma = Round2( OutY[ lumaY ][ lumaX ] + OutY[ lumaY ][ lumaNextX ], 1)
            else
                averageLuma = OutY[ lumaY ][ lumaX ]
            if ( num_cb_points > 0 || chroma_scaling_from_luma ) {
                orig = OutU[ y ][ x ]
                if ( chroma_scaling_from_luma ) {
                    merged = averageLuma
                } else {
                    combined = averageLuma * ( cb_luma_mult - 128 ) + orig * ( cb_mult - 128 ) // E-421 [RNG-FilmGrain21]
                    // TODO: The range equations don't currently look inside Clip1(). So we calculate
                    // the inner value as its own variable here.
                    // Later, we can re-combine this into a single line.
                    tmp = ( combined >> 6 ) + ( ( cb_offset - 256 ) << ( BitDepth - 8 ) ) // E-422 [RNG-FilmGrain22]
                    merged = Clip1( tmp )
                }
                noise = noiseImage[ 1 ][ y ][ x ]
                noise = Round2( scale_lut( 1, merged ) * noise, ScalingShift) // E-423 [RNG-FilmGrain23]
                OutU[ y ][ x ] = Clip3( minValue, maxChroma, orig + noise ) // E-424 [RNG-FilmGrain24]
            }

            if ( num_cr_points > 0 || chroma_scaling_from_luma ) {
                orig = OutV[ y ][ x ]
                if ( chroma_scaling_from_luma ) {
                    merged = averageLuma
                } else {
                    combined = averageLuma * ( cr_luma_mult - 128 ) + orig * ( cr_mult - 128 ) // E-425 [RNG-FilmGrain25]
                    // TODO: Same as above.
                    tmp = ( combined >> 6 ) + ( ( cr_offset - 256 ) << ( BitDepth - 8) ) // E-426 [RNG-FilmGrain26]
                    merged = Clip1( tmp )
                }
                noise = noiseImage[ 2 ][ y ][ x ]
                noise = Round2( scale_lut( 2, merged ) * noise, ScalingShift) // E-427 [RNG-FilmGrain27]
                OutV[ y ][ x ] = Clip3( minValue, maxChroma, orig + noise ) // E-428 [RNG-FilmGrain28]
            }
        }
    }
    for ( y = 0; y < h ; y++ ) {
        for ( x = 0; x < w ; x++ ) {
            orig = OutY[ y ][ x ]
            noise = noiseImage[ 0 ][ y ][ x ]
            noise = Round2( scale_lut( 0, orig ) * noise, ScalingShift) // E-429 [RNG-FilmGrain29]
            if ( num_y_points > 0 ) {
                OutY[ y ][ x ] = Clip3( minValue, maxLuma, orig + noise ) // E-430 [RNG-FilmGrain30]
            }
        }
    }
}

film_grain_synthesis( w, h, subX, subY ) {
    RandomRegister = grain_seed
    GrainCenter = 128 << ( BitDepth - 8 )
    GrainMin = - GrainCenter
    GrainMax = ( 256 << ( BitDepth - 8 ) ) - 1 - GrainCenter
    generate_grain( )
    scaling_lookup_initialization( )
    add_noise_synthesis( w, h, subX, subY )
}

output( ) {
    if ( show_existing_frame == 1 ) {
        w = RefUpscaledWidth[ frame_to_show_map_idx ]
        h = RefFrameHeight[ frame_to_show_map_idx ]
        subX = RefSubsamplingX[ frame_to_show_map_idx ]
        subY = RefSubsamplingY[ frame_to_show_map_idx ]
        for (x = 0; x < w; x++)
            for (y = 0; y < h; y++)
                OutY[ y ][ x ] = FrameStore[ frame_to_show_map_idx ][ 0 ][ y ][ x ]
        if ( COVERCLASS(1, (PROFILE0 || PROFILE2), NumPlanes > 1) ) {  // TODO, add to spec?
            for (x = 0; x < ((w + subX) >> subX); x++)
                for (y = 0; y < ((h + subY) >> subY); y++) {
                    OutU[ y ][ x ] = FrameStore[ frame_to_show_map_idx ][ 1 ][ y ][ x ]
                    OutV[ y ][ x ] = FrameStore[ frame_to_show_map_idx ][ 2 ][ y ][ x ]
                }
        }
        BitDepth = RefBitDepth[ frame_to_show_map_idx ]
    } else {
        w = UpscaledWidth
        h = FrameHeight
        subX = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
        subY = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
        for (x = 0; x < w; x++)
            for (y = 0; y < h; y++)
                OutY[ y ][ x ] = LrFrame[ 0 ][ y ][ x ]
        if ( COVERCLASS(1, (PROFILE0 || PROFILE2), NumPlanes > 1) ) {  // TODO, add to spec?
            for (x = 0; x < ((w + subX) >> subX); x++)
                for (y = 0; y < ((h + subY) >> subY); y++) {
                    OutU[ y ][ x ] = LrFrame[ 1 ][ y ][ x ]
                    OutV[ y ][ x ] = LrFrame[ 2 ][ y ][ x ]
                }
        }
    }
    DisplayedLumaSamplesInTU += UpscaledWidth * FrameHeight
    FramesDisplayedInTU++
#if VALIDATE_SPEC_OUTPUT
    for (plane = 0; plane < NumPlanes; plane++) {
        if (plane == 0) {
            for (y = 0; y < h; y++) {
                for (x = 0; x < w; x++) {
                    validate(90001)
                    validate(x)
                    validate(y)
                    validate(0)
                    validate(OutY[ y ][ x ])
                }
            }
        } else {
            for (y = 0; y < ((h + subY) >> subY); y++) {
                for (x = 0; x < ((w + subX) >> subX); x++) {
                    validate(90001)
                    validate(x)
                    validate(y)
                    validate(plane)
                    validate(plane == 1 ? OutU[ y ][ x ] : OutV[ y ][ x ])
                }
            }
        }
    }
#endif
    if ( film_grain_params_present == 1 && apply_grain == 1 ) {
        if ( COVERCLASS(1, IMPOSSIBLE, !COVERCLASS(IMPOSSIBLE, 1, override_no_film_grain)) ) {
            film_grain_synthesis( w, h, subX, subY )
        }
    }
#if VALIDATE_SPEC_OUTPUT
    for (plane = 0; plane < NumPlanes; plane++) {
        if (plane == 0) {
            for (y = 0; y < h; y++) {
                for (x = 0; x < w; x++) {
                    validate(90000)
                    validate(x)
                    validate(y)
                    validate(0)
                    validate(OutY[ y ][ x ])
                }
            }
        } else {
            for (y = 0; y < ((h + subY) >> subY); y++) {
                for (x = 0; x < ((w + subX) >> subX); x++) {
                    validate(90000)
                    validate(x)
                    validate(y)
                    validate(plane)
                    validate(plane == 1 ? OutU[ y ][ x ] : OutV[ y ][ x ])
                }
            }
        }
    }
#endif
#if COVER
    profile_enable()
#endif
    if ( COVERCLASS(LARGE_SCALE_TILES,1,num_large_scale_tile_anchor_frames > 0 )) {
        if ( COVERCLASS(LARGE_SCALE_TILES,IMPOSSIBLE,FrameNumber < num_large_scale_tile_anchor_frames) ) {
            AnchorWidth[ FrameNumber ] = w
            AnchorHeight[ FrameNumber ] = h
            AnchorWidthC[ FrameNumber ] = ((w + subX) >> subX)
            for (plane = 0; plane < NumPlanes; plane++) {
                if (plane == 0) {
                    rAnchorY[ FrameNumber ] = alloc [ w*h ]
                    for (y = 0; y < h; y++) {
                        for (x = 0; x < w; x++) {
                            AnchorY[ FrameNumber ][ y ][ x ] = OutY[ y ][ x ]
                        }
                    }
                } else {
                    if ( plane == 1 ) {
                        rAnchorU[ FrameNumber ] = alloc [ ((h + subY) >> subY) * ((w + subX) >> subX) ]
                    } else {
                        rAnchorV[ FrameNumber ] = alloc [ ((h + subY) >> subY) * ((w + subX) >> subX) ]
                    }
                    for (y = 0; y < ((h + subY) >> subY); y++) {
                        for (x = 0; x < ((w + subX) >> subX); x++) {
                            if ( plane == 1 ) {
                                AnchorU[ FrameNumber ][ y ][ x ] = OutU[ y ][ x ]
                            } else {
                                AnchorV[ FrameNumber ][ y ][ x ] = OutV[ y ][ x ]
                            }
                        }
                    }
                }
            }
            FrameNumber = FrameNumber + 1
#if COVER
            profile_disable()
#endif
            return 0
        }
    }

    // Now output OutY, OutU, OutV where the bit depth for each sample is BitDepth.
    bps = BitDepth
    mono = COVERCLASS((PROFILE0 || PROFILE2), 1, mono_chrome)
    //pitch = UpscaledWidth
    sw = 1<<subX
    sh = 1<<subY
    :video_output(b, bps, w, h, sw, sh, mono)
    :Cvideo_output(b, bps, w, h, sw, sh, mono)
    OutputFrameCount++
}

output_tile_list( tile ) COVERCLASS(LARGE_SCALE_TILES) {
    x0 = MiColStart * MI_SIZE
    y0 = MiRowStart * MI_SIZE
    subX = COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_x)
    subY = COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_y)
    xC0 = ( MiColStart * MI_SIZE ) >> subX
    yC0 = ( MiRowStart * MI_SIZE ) >> subY
    destX = TileWidth * ( tile % (output_frame_width_in_tiles_minus_1 + 1) )
    destY = TileHeight * ( tile / (output_frame_width_in_tiles_minus_1 + 1) )
    w = TileWidth
    h = TileHeight

    //This bit is from the spec...
    for ( y = 0; y < h; y++ ) {
      for ( x = 0; x < w; x++ ) {
        OutputFrameY[ y + destY ][ x + destX ] = CurrFrame[ 0 ][ y0 + y ][ x0 + x ]
      }
    }
    w = w >> COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_x)
    h = h >> COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_y)
    destX = destX >> COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_x)
    destY = destY >> COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_y)
    for ( y = 0; y < h; y++ ) {
      for ( x = 0; x < w; x++ ) {
        OutputFrameU[ y + destY ][ x + destX ] = CurrFrame[ 1 ][ yC0 + y ][ xC0 + x ]
      }
    }
    for ( y = 0; y < h; y++ ) {
      for ( x = 0; x < w; x++ ) {
        OutputFrameV[ y + destY ][ x + destX ] = CurrFrame[ 2 ][ yC0 + y ][ xC0 + x ]
      }
    }

    //... and this bit is from the Argon Streams documentation. They should be the exact
    // reverse of each other...!
    destX = TileWidth * ( tile % ( output_frame_width_in_tiles_minus_1 + 1 ) )
    destY = TileHeight * ( tile / ( output_frame_width_in_tiles_minus_1 + 1 ) )
    w = TileWidth
    h = TileHeight
    for ( y = 0; y < h; y++ ) {
      for ( x = 0; x < w; x++ ) {
        OutY[ y ][ x ] = OutputFrameY[ y + destY ][ x + destX ]
      }
    }
    w = w >> COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_x)
    h = h >> COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_y)
    destX = destX >> COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_x)
    destY = destY >> COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_y)
    for ( y = 0; y < h; y++ ) {
      for ( x = 0; x < w; x++ ) {
        OutU[ y ][ x ] = OutputFrameU[ y + destY ][ x + destX ]
      }
    }
    for ( y = 0; y < h; y++ ) {
      for ( x = 0; x < w; x++ ) {
        OutV[ y ][ x ] = OutputFrameV[ y + destY ][ x + destX ]
      }
    }

    // Now output OutY, OutU, OutV where the bit depth for each sample is BitDepth.
    w = TileWidth
    h = TileHeight
    bps = BitDepth
    //pitch = UpscaledWidth
    sw = 1<<subX
    sh = 1<<subY
    :video_output(b, bps, w, h, sw, sh, 0)
    :Cvideo_output(b, bps, w, h, sw, sh, 0)
    OutputFrameCount++
}

motion_field_motion_vector_storage( ) {
    for ( row = 0; row < MiRows; row++ ) {
        for ( col = 0; col < MiCols; col++ ) {
            MfRefFrames[ row ][ col ] = NONE
            MfMvs[ row ][ col ][ 0 ] = 0
            MfMvs[ row ][ col ][ 1 ] = 0
            for ( list = 0; list < 2; list++ ) {
                r = RefFrames[ row ][ col ][ list ]
                if ( r > INTRA_FRAME ) {
                    refIdx = ref_frame_idx[ r - LAST_FRAME ]
                    dist = get_relative_dist( RefOrderHint[ refIdx ], OrderHint )
                    distEqualsZero = (dist==0)
#if VALIDATE_SPEC_MFMV_STORAGE
                    validate(42003)
                    validate(dist<0)
                    validate(Mvs[ row ][ col ][ list ][ 0 ])
                    validate(Mvs[ row ][ col ][ list ][ 1 ])
                    validate(row)
                    validate(col)
#endif
                    if ( dist < 0 ) {
                        mvRow = Mvs[ row ][ col ][ list ][ 0 ]
                        mvCol = Mvs[ row ][ col ][ list ][ 1 ]
                        if ( Abs( mvRow ) <= REFMVS_LIMIT && Abs( mvCol ) <= REFMVS_LIMIT ) {
#if VALIDATE_SPEC_MFMV_STORAGE
                            validate(r)
#endif
                            MfRefFrames[ row ][ col ] = r
                            MfMvs[ row ][ col ][ 0 ] = mvRow
                            MfMvs[ row ][ col ][ 1 ] = mvCol
                        }
                    }
                }
            }
        }
    }
}

reference_frame_update( ) {
#if !COVER
    int ss_x
    int ss_y
#endif

    // Unset all refresh frame flags which will no longer apply after this update
    for (i = 0; i < NUM_REF_FRAMES; i++) {
        if (RefValid[ i ])
            RefRefreshFrameFlags[ i ] = RefRefreshFrameFlags[ i ] & ( ~refresh_frame_flags )
    }
    for (i = 0; i < NUM_REF_FRAMES; i++) {
        if ( ( (refresh_frame_flags >> i) & 1 ) == 1 ) {
            RefValid[ i ] = 1
            RefFrameId[ i ] = current_frame_id
            RefUpscaledWidth[ i ] = UpscaledWidth
            RefFrameWidth[ i ] = FrameWidth
            RefFrameHeight[ i ] = FrameHeight
            RefRenderWidth[ i ] = RenderWidth
            RefRenderHeight[ i ] = RenderHeight
            RefMiCols[ i ] = MiCols
            RefMiRows[ i ] = MiRows
            RefFrameType[ i ] = frame_type
            RefSubsamplingX[ i ] = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
            RefSubsamplingY[ i ] = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
            RefBitDepth[ i ] = BitDepth
            RefShowableFrame[ i ] = showable_frame
            RefShownCount[ i ] = 0
            RefFrameUID[ i ] = FrameUID
            RefRefreshFrameFlags[ i ] = refresh_frame_flags
            RefSeqProfile[ i ] = seq_profile
            RefColorPrimaries[ i ] = color_primaries
            RefTransferCharacteristics[ i ] = transfer_characteristics
            RefMatrixCoefficients[ i ] = matrix_coefficients

            for ( j = 0; j < REFS_PER_FRAME; j++ )
                SavedOrderHints[ i ][ j + LAST_FRAME ] = OrderHints[ j + LAST_FRAME ]

            RefStride[ i ] = Stride

#if COVER
            for (x = 0; x < UpscaledWidth; x++) {
                for (y = 0; y < FrameHeight; y++) {
                    FrameStore[ i ][ 0 ][ y ][ x ] = LrFrame[ 0 ][ y ][ x ]
                }
            }
            for (plane = 1; plane < NumPlanes; plane++) {  // TODO should the spec use NumPlanes here?
                for (x = 0; x < ((UpscaledWidth + COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)) >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)); x++) {
                    for (y = 0; y < ((FrameHeight + COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)) >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)); y++) {
                        FrameStore[ i ][ plane ][ y ][ x ] = LrFrame[ plane ][ y ][ x ]
                    }
                }
            }
#else
            // Use a builtin (which is basically memcpy) to copy the
            // frame store when we're not tracking coverage to speed
            // things up.
            for (plane = 0; plane < NumPlanes; plane++) {
                ss_x = (plane > 0) ? subsampling_x : 0
                ss_y = (plane > 0) ? subsampling_y : 0
                Copy2D (FrameStore[i][plane], LrFrame[plane],
                        (UpscaledWidth + ss_x) >> ss_x,
                        (FrameHeight + ss_y) >> ss_y)
            }
#endif

            for ( row = 0; row < MiRows; row++ ) {
                for ( col = 0; col < MiCols; col++ ) {
                    SavedRefFrames[ i ][ row ][ col ] = MfRefFrames[ row ][ col ]
                    SavedMvs[ i ][ row ][ col ][ 0 ] = MfMvs[ row ][ col ][ 0 ]
                    SavedMvs[ i ][ row ][ col ][ 1 ] = MfMvs[ row ][ col ][ 1 ]
                }
            }

            for (ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++) {
                for (j = 0; j < 6; j++) {
                    SavedGmParams[ i ][ ref ][ j ] = gm_params[ ref ][ j ]
                }
            }
            for (row = 0; row < MiRows; row++) {
                for (col = 0; col < MiCols; col++) {
                    SavedSegmentIds[ i ][ row ][ col ] = SegmentIds[ row ][ col ]
                }
            }
            save_cdfs( i )
            if ( film_grain_params_present == 1 )
                save_grain_params( i )
            save_loop_filter_params( i )
            save_segmentation_params( i )
        }
    }
    for (i = 0; i < NUM_REF_FRAMES; i++) {
        if ( ( (refresh_frame_flags >> i) & 1 ) == 1 ) {
            RefOrderHint[ i ] = OrderHint
        }
    }
    FrameUID++
}

save_loop_filter_params( i ) {
    for ( j = 0; j < TOTAL_REFS_PER_FRAME; j++ ) {
        SavedLoopFilterRefDeltas[ i ][ j ] = loop_filter_ref_deltas[ j ]
    }
    for ( j = 0; j < 2; j++ ) {
        SavedLoopFilterModeDeltas[ i ][ j ] = loop_filter_mode_deltas[ j ]
    }
}

save_segmentation_params( i ) {
    for ( j = 0; j < MAX_SEGMENTS; j++ ) {
        for ( k = 0; k < SEG_LVL_MAX; k++ ) {
            SavedFeatureEnabled[ i ][ j ][ k ] = FeatureEnabled[ j ][ k ]
            SavedFeatureData[ i ][ j ][ k ] = FeatureData[ j ][ k ]
        }
    }
}

reference_frame_loading( ) {
#if !COVER
    int ss_x
    int ss_y
#endif

    current_frame_id = RefFrameId[ frame_to_show_map_idx ]
    UpscaledWidth = RefUpscaledWidth[ frame_to_show_map_idx ]
    FrameWidth = RefFrameWidth[ frame_to_show_map_idx ]
    FrameHeight = RefFrameHeight[ frame_to_show_map_idx ]
    RenderWidth = RefRenderWidth[ frame_to_show_map_idx ]
    RenderHeight = RefRenderHeight[ frame_to_show_map_idx ]
    MiCols = RefMiCols[ frame_to_show_map_idx ]
    MiRows = RefMiRows[ frame_to_show_map_idx ]
    subsampling_x = RefSubsamplingX[ frame_to_show_map_idx ]
    subsampling_y = RefSubsamplingY[ frame_to_show_map_idx ]
    BitDepth = RefBitDepth[ frame_to_show_map_idx ]
    OrderHint = RefOrderHint[ frame_to_show_map_idx ]
    for ( j = 0; j < REFS_PER_FRAME; j++ )
        OrderHints[ j + LAST_FRAME ] = SavedOrderHints[ frame_to_show_map_idx ][ j + LAST_FRAME ]

#if COVER
    for ( x = 0; x < UpscaledWidth; x++ )
        for ( y = 0; y < FrameHeight; y++ )
            LrFrame[ 0 ][ y ][ x ] = FrameStore[ frame_to_show_map_idx ][ 0 ][ y ][ x ]

    for ( plane = 1; plane < NumPlanes; plane++ )  // TODO should the spec use NumPlanes here?
        for ( x = 0; x < ((UpscaledWidth + COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)) >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)); x++ )
            for ( y = 0; y < ((FrameHeight + COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)) >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)); y++ )
                LrFrame[ plane ][ y ][ x ] = FrameStore[ frame_to_show_map_idx ][ plane ][ y ][ x ]
#else
    // Use a builtin (which is basically memcpy) to copy the
    // frame store when we're not tracking coverage to speed
    // things up.
    for (plane = 0; plane < NumPlanes; plane++) {
        ss_x = (plane > 0) ? subsampling_x : 0
        ss_y = (plane > 0) ? subsampling_y : 0
        Copy2D (LrFrame[plane], FrameStore[frame_to_show_map_idx][plane],
                (UpscaledWidth + ss_x) >> ss_x,
                (FrameHeight + ss_y) >> ss_y)
    }
#endif


    for ( row = 0; row < MiRows; row++ ) {
        for ( col = 0; col < MiCols; col++ ) {
            MfRefFrames[ row ][ col ] = SavedRefFrames[ frame_to_show_map_idx ][ row ][ col ]
            MfMvs[ row ][ col ][ 0 ] = SavedMvs[ frame_to_show_map_idx ][ row ][ col ][ 0 ]
            MfMvs[ row ][ col ][ 1 ] = SavedMvs[ frame_to_show_map_idx ][ row ][ col ][ 1 ]
        }
    }

    for ( ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++ )
        for ( j = 0; j < 6; j++ )
            gm_params[ ref ][ j ] = SavedGmParams[ frame_to_show_map_idx ][ ref ][ j ]

    for ( row = 0; row < MiRows; row++ )
        for ( col = 0; col < MiCols; col++ )
            SegmentIds[ row ][ col ] = SavedSegmentIds[ frame_to_show_map_idx ][ row ][ col ]

    load_cdfs( frame_to_show_map_idx )

    if ( film_grain_params_present == 1)
        load_grain_params( frame_to_show_map_idx )

    load_loop_filter_params( frame_to_show_map_idx )
    load_segmentation_params( frame_to_show_map_idx )
}

load_loop_filter_params( i ) {
    for ( j = 0; j < TOTAL_REFS_PER_FRAME; j++ ) {
        loop_filter_ref_deltas[ j ] = SavedLoopFilterRefDeltas[ i ][ j ]
    }
    for ( j = 0; j < 2; j++ ) {
        loop_filter_mode_deltas[ j ] = SavedLoopFilterModeDeltas[ i ][ j ]
    }
}

load_segmentation_params( i ) {
    for ( j = 0; j < MAX_SEGMENTS; j++ ) {
        for ( k = 0; k < SEG_LVL_MAX; k++ ) {
            FeatureEnabled[ j ][ k ] = SavedFeatureEnabled[ i ][ j ][ k ]
            FeatureData[ j ][ k ] = SavedFeatureData[ i ][ j ][ k ]
        }
    }
}
