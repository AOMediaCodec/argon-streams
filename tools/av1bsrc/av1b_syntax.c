/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_SPEC_STREAM_SEPARATOR 0
#define VALIDATE_SPEC_STRUCTURE 0
#define VALIDATE_SPEC_QUANT 0
#define VALIDATE_SPEC_VARTX 0
#define VALIDATE_SPEC_NEWMV 0
#define VALIDATE_SPEC_TILES 0
#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_SKIP 1
#else
#define VALIDATE_SPEC_SKIP 0
#endif
#define VALIDATE_MVS 0
#define VALIDATE_SPEC_GM_PARAM 0

#define OUTPUT_TRANSFORM_BLOCKS 0
#define OUTPUT_TILES 0

//Conversion Tables
//This section defines the constant lookup tables used to convert between different representations.
//For a block size x (with values having the same interpretation as for the variable subSize), Mi_Width_Log2[ x ] gives the base 2 logarithm of the width of the block in units of 4 samples.
int Mi_Width_Log2[ BLOCK_SIZES ] = {
    0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4,
    4, 4, 5, 5, 0, 2, 1, 3, 2, 4
};

//For a block size x, Mi_Height_Log2[ x ] gives the base 2 logarithm of the height of the block in units of 4 samples.
int Mi_Height_Log2[ BLOCK_SIZES ] = {
    0, 1, 0, 1, 2, 1, 2, 3, 2, 3, 4, 3,
    4, 5, 4, 5, 2, 0, 3, 1, 4, 2
};

//For a block size x, Num_4x4_Blocks_Wide[ x ] gives the width of the block in units of 4 samples.
int Num_4x4_Blocks_Wide[ BLOCK_SIZES ] = {
    1, 1, 2, 2, 2, 4, 4, 4, 8, 8, 8, 16,
    16, 16, 32, 32, 1, 4, 2, 8, 4, 16
};

//For a block size x, Block_Width[ x ] gives the width of the block in units of samples. Block_Width[ x ] is defined to be equal to 4 * Num_4x4_Blocks_Wide[ x ].
//For a block size x, Num_4x4_Blocks_High[ x ] gives the the height of the block in units of 4 samples.
int Num_4x4_Blocks_High[ BLOCK_SIZES ] = {
    1, 2, 1, 2, 4, 2, 4, 8, 4, 8, 16, 8,
    16, 32, 16, 32, 4, 1, 8, 2, 16, 4
};

//For a block size x,
//Block_Width[ x ] gives the width of the block in units of samples.
//Block_Width[ x ] is defined to be equal to 4 * Num_4x4_Blocks_Wide[ x ].

int Block_Width[ BLOCK_SIZES ] = {
    4, 4, 8, 8, 8, 16, 16, 16, 32, 32, 32, 64,
    64, 64, 128, 128, 4, 16, 8, 32, 16, 64
};

//For a block size x,
//Block_Height[ x ] gives the width of the block in units of samples.
//Block_Height[ x ] is defined to be equal to 4 * Num_4x4_Blocks_High[ x ].

int Block_Height[ BLOCK_SIZES ] = {
    4, 8, 4, 8, 16, 8, 16, 32, 16, 32, 64, 32,
    64, 128, 64, 128, 16, 4, 32, 8, 64, 16
};

//Size_Group is used to map a block size into a context for intra syntax elements.
int Size_Group[ BLOCK_SIZES ] = {
  0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3,
  3, 3, 3, 3, 0, 0, 1, 1, 2, 2
};


//For a luma block size x, Max_Tx_Size_Rect[ x ] returns the largest transform size that can be used for blocks of size x (this can be either square or rectangular).
int Max_Tx_Size_Rect[ BLOCK_SIZES ] = {
  TX_4X4, TX_4X8, TX_8X4, TX_8X8,
  TX_8X16, TX_16X8, TX_16X16, TX_16X32,
  TX_32X16, TX_32X32, TX_32X64, TX_64X32,
  TX_64X64, TX_64X64, TX_64X64, TX_64X64,
  TX_4X16, TX_16X4, TX_8X32, TX_32X8,
  TX_16X64, TX_64X16
};

//For a square block size x, and a partition type p, Partition_Subsize[ p ][ x ] returns the size of the sub-blocks used by this partition. (If the partition produces blocks of different sizes, then the table contains the largest sub-block size.)
//The table will never get accessed for rectangular block sizes.
int Partition_Subsize[ 10 ][ BLOCK_SIZES ] = {
  {
                                  BLOCK_4X4,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X8,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X16,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X32,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X64,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_128X128,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID
  }, {
                                  BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X4,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X8,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X16,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X32,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_128X64,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID
  }, {
                                  BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_4X8,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X16,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X32,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X64,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X128,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID
  }, {
                                  BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_4X4,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X8,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X16,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X32,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X64,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID
  }, {
                                  BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X4,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X8,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X16,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X32,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_128X64,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID
  }, {
                                  BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X4,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X8,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X16,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X32,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_128X64,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID
  }, {
                                  BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_4X8,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X16,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X32,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X64,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X128,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID
  }, {
                                  BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_4X8,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X16,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X32,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X64,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X128,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID
  }, {
                                  BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X4,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X8,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X16,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID
  }, {
                                  BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_4X16,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X32,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X64,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID
  }
};

int Mode_To_Txfm[ UV_INTRA_MODES_CFL_ALLOWED ] = {
    DCT_DCT, // DC
    ADST_DCT, // V
    DCT_ADST, // H
    DCT_DCT, // D45
    ADST_ADST, // D135
    ADST_DCT, // D117
    DCT_ADST, // D153
    DCT_ADST, // D207
    ADST_DCT, // D63
    ADST_ADST, // SMOOTH
    ADST_DCT,  // SMOOTH_V
    DCT_ADST,  // SMOOTH_H
    ADST_ADST, // TM
    DCT_DCT // UV_CFL_PRED
};

int Palette_Color_Context[ PALETTE_MAX_COLOR_CONTEXT_HASH + 1 ] =
  { -1, -1, 0, -1, -1, 4,  3,  2, 1 };

// Note: The negative numbers in the array Palette_Color_Context indicate values that will never be accessed.

int Palette_Color_Hash_Multipliers[ PALETTE_NUM_NEIGHBORS ] = { 1, 2, 2 };
int Sm_Weights_Tx_4x4  [  4 ] = { 255, 149,  85,  64 };
int Sm_Weights_Tx_8x8  [  8 ] = { 255, 197, 146, 105,  73,  50,  37,  32 };
int Sm_Weights_Tx_16x16[ 16 ] = { 255, 225, 196, 170, 145, 123, 102,  84,  68,  54,  43,  33,  26, 20, 17, 16 };
int Sm_Weights_Tx_32x32[ 32 ] = { 255, 240, 225, 210, 196, 182, 169, 157, 145, 133, 122, 111, 101, 92, 83, 74,
                              66,  59,  52,  45,  39,  34,  29,  25,  21,  17,  14,  12,  10,  9,  8,  8 };
int Sm_Weights_Tx_64x64[ 64 ] = { 255, 248, 240, 233, 225, 218, 210, 203, 196, 189, 182, 176, 169, 163, 156,
                              150, 144, 138, 133, 127, 121, 116, 111, 106, 101, 96, 91, 86, 82, 77, 73, 69,
                              65, 61, 57, 54, 50, 47, 44, 41, 38, 35, 32, 29, 27, 25, 22, 20, 18, 16, 15,
                              13, 12, 10, 9, 8, 7, 6, 6, 5, 5, 4, 4, 4 };
int Mode_To_Angle[ INTRA_MODES ] = { 0, 90, 180, 45, 135, 113, 157, 203, 67, 0, 0, 0, 0 };
int Dr_Intra_Derivative[ 90 ] = {
  0, 0, 0, 1023, 0, 0, 547, 0, 0, 372, 0, 0, 0, 0,
  273, 0, 0, 215, 0, 0, 178, 0, 0, 151, 0, 0, 132, 0, 0,
  116, 0, 0, 102, 0, 0, 0, 90, 0, 0, 80, 0, 0, 71, 0, 0,
  64, 0, 0, 57, 0, 0, 51, 0, 0, 45, 0, 0, 0, 40, 0, 0,
  35, 0, 0, 31, 0, 0, 27, 0, 0, 23, 0, 0, 19, 0, 0,
  15, 0, 0, 0, 0, 11, 0, 0, 7, 0, 0, 3, 0, 0
};

//For a transform size t (of width w and height h) (with the same interpretation as for the tx_size syntax element), Tx_Size_Sqr[ t ] returns a square tx size with side length Min( w, h ).
int Tx_Size_Sqr[ TX_SIZES_ALL ] = {
    TX_4X4,
    TX_8X8,
    TX_16X16,
    TX_32X32,
    TX_64X64,
    TX_4X4,
    TX_4X4,
    TX_8X8,
    TX_8X8,
    TX_16X16,
    TX_16X16,
    TX_32X32,
    TX_32X32,
    TX_4X4,
    TX_4X4,
    TX_8X8,
    TX_8X8,
    TX_16X16,
    TX_16X16
};

//For a transform size t (of width w and height h), Tx_Size_Sqr_Up[ t ] returns a square tx size with side length Max( w, h ).
int Tx_Size_Sqr_Up[ TX_SIZES_ALL ] = {
    TX_4X4,
    TX_8X8,
    TX_16X16,
    TX_32X32,
    TX_64X64,
    TX_8X8,
    TX_8X8,
    TX_16X16,
    TX_16X16,
    TX_32X32,
    TX_32X32,
    TX_64X64,
    TX_64X64,
    TX_16X16,
    TX_16X16,
    TX_32X32,
    TX_32X32,
    TX_64X64,
    TX_64X64
};

//For a transform size t (of width w and height h), Tx_Width[ t ] returns w.
int Tx_Width[ TX_SIZES_ALL ] = {
    4,  8, 16, 32, 64, 4,  8, 8,  16, 16, 32, 32, 64, 4, 16, 8, 32, 16, 64
};

//For a transform size t (of width w and height h), Tx_Height[ t ] returns h.
int Tx_Height[ TX_SIZES_ALL ] = {
    4,  8, 16, 32, 64, 8,  4, 16, 8,  32, 16, 64, 32, 16, 4, 32, 8, 64, 16
};

//For a transform size t (of width w and height h), Tx_Width_Log2[ t ] returns the base 2 logarithm of w.
int Tx_Width_Log2[ TX_SIZES_ALL ] = {
    2, 3, 4, 5, 6, 2, 3, 3, 4, 4, 5, 5, 6, 2, 4, 3, 5, 4, 6
};

//For a transform size t (of width w and height h), Tx_Height_Log2[ t ] returns the base 2 logarithm of h.
int Tx_Height_Log2[ TX_SIZES_ALL ] = {
    2, 3, 4, 5, 6, 3, 2, 4, 3, 5, 4, 6, 5, 4, 2, 5, 3, 6, 4
};

int Tx_Type_In_Set_Intra[ TX_SET_TYPES_INTRA ][ TX_TYPES ] = {
  {
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  },
  {
    1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0,
  },
  {
    1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
  }
};

int Tx_Type_In_Set_Inter[ TX_SET_TYPES_INTER ][ TX_TYPES ] = {
  {
    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  },
  {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
  },
  {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
  },
  {
    1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0,
  }
};

int Wedge_Bits[ BLOCK_SIZES ] = {
  0, 0, 0, 4, 4, 4, 4, 4, 4, 4, 0, 0,
  0, 0, 0, 0, 0, 0, 4, 4, 0, 0
};

//For a transform size t (of width w and height h), Transpose_Tx_Size[ t ] returns a transform size with width h and height w.
int Transpose_Tx_Size[ TX_SIZES_ALL ] = {
  TX_4X4, TX_8X8, TX_16X16, TX_32X32, TX_64X64, TX_8X4, TX_4X8,
  TX_16X8, TX_8X16, TX_32X16, TX_16X32, TX_64X32, TX_32X64,
  TX_16X4, TX_4X16, TX_32X8, TX_8X32, TX_64X16, TX_16X64
};

//For a transform type t, Transpose_Tx_Type[ t ] returns a transform type with the horizontal and vertical operations transposed.
int Transpose_Tx_Type[ TX_TYPES ] = {
  DCT_DCT, DCT_ADST, ADST_DCT, ADST_ADST, DCT_FLIPADST, FLIPADST_DCT,
  FLIPADST_FLIPADST, FLIPADST_ADST, ADST_FLIPADST, IDTX, H_DCT, V_DCT,
  H_ADST, V_ADST, H_FLIPADST, V_FLIPADST,
};

int Intra_Mode_Context[ INTRA_MODES ] = {
  0, 1, 2, 3, 4, 4, 4, 4, 3, 0, 1, 2, 0
};

int Max_Tx_Depth[ BLOCK_SIZES ] = {
    0, 1, 1, 1,
    2, 2, 2, 3,
    3, 3, 4, 4,
    4, 4, 4, 4,
    2, 2, 3, 3,
    4, 4
};

int64 Profiles_Levels[ PROFILES_AND_LEVELS_Num_Valid_Levels ][ PROFILES_AND_LEVELS_Count ] = {
  { 2, 0, 147456,   2048,  1152, 4423680,    5529600,    150,   15,   -1, 2, COMP_BASIS_INVALID, 8, 4 },
  { 2, 1, 278784,   2816,  1584, 8363520,    10454400,   150,   30,   -1, 2, COMP_BASIS_INVALID, 8, 4 },
  { 3, 0, 665856,   4352,  2448, 19975680,   24969600,   150,   60,   -1, 2, COMP_BASIS_INVALID, 16, 6 },
  { 3, 1, 1065024,  5504,  3096, 31950720,   39938400,   150,  100,   -1, 2, COMP_BASIS_INVALID, 16, 6 },
  { 4, 0, 2359296,  6144,  3456, 70778880,   77856768,   300,  120,  300, 4, 4, 32, 8 },
  { 4, 1, 2359296,  6144,  3456, 141557760,  155713536,  300,  200,  500, 4, 4, 32, 8 },
  { 5, 0, 8912896,  8192,  4352, 267386880,  273715200,  300,  300, 1000, 6, 4, 64, 8 },
  { 5, 1, 8912896,  8192,  4352, 534773760,  547430400,  300,  400, 1600, 8, 4, 64, 8 },
  { 5, 2, 8912896,  8192,  4352, 1069547520, 1094860800, 300,  600, 2400, 8, 4, 64, 8 },
  { 5, 3, 8912896,  8192,  4352, 1069547520, 1176502272, 300,  600, 2400, 8, 4, 64, 8 },
  { 6, 0, 35651584, 16384, 8704, 1069547520, 1176502272, 300,  600, 2400, 8, 4, 128, 16 },
  { 6, 1, 35651584, 16384, 8704, 2139095040, 2189721600, 300, 1000, 4800, 8, 4, 128, 16 },
  { 6, 2, 35651584, 16384, 8704, 4278190080, 4379443200, 300, 1600, 8000, 8, 4, 128, 16 },
  { 6, 3, 35651584, 16384, 8704, 4278190080, 4706009088, 300, 1600, 8000, 8, 4, 128, 16 }
};

// Used for debug to check that Python ad C agree on some variable
validate(x) {
  :validate(x)
  :C validate(x);
}

global_data( argv_num_large_scale_tile_anchor_frames,
             argv_num_large_scale_tile_tile_list_obus,
             overrideNoFilmGrain ) {
    override_no_film_grain = COVERCLASS(IMPOSSIBLE, 1, overrideNoFilmGrain)
#if VALIDATE_SPEC_STREAM_SEPARATOR
    validate(999999)
    :C printf("Stream separator\n");
#endif
    ReceivedSequenceHeader = 0
    for (ctx = 0; ctx < NUM_REF_FRAMES; ctx++)
        StoredCdfInitialised[ ctx ] = 0
    WedgeMaskTableInitialised = 0
    seq_force_integer_mv = 0
    seq_profile = 0
    SeenTemporalDelimiter = 0
    SeenTemporalDelimiterBeforeSequenceHeader = 0
    SeenSeqHeader = 0
    SeenSeqHeaderBeforeFirstFrameHeader = 0
    SeenFrameHeader = 0
    IsFirstFrame = 1
    HasEnhancement = 0
    FrameUID = 0
    FrameNumber = 0
    OutputFrameCount = 0
    num_large_scale_tile_anchor_frames = argv_num_large_scale_tile_anchor_frames
    num_large_scale_tile_tile_list_obus = argv_num_large_scale_tile_tile_list_obus
    for (i = 0; i < NUM_REF_FRAMES; i++) {
        RefValid[ i ] = 0
        RefOrderHint[ i ] = 57
    }

    // Set each entry of OrderHints to some invalid value. We need to
    // initialise the array because it gets copied in
    // reference_frame_update, possibly before its values are set, and
    // we profile its contents, causing Valgrind errors if we haven't
    // put anything in it yet.
    for (i = 0; i < REFS_PER_FRAME; i++) {
        OrderHints[ i + LAST_FRAME ] = 57
    }

    // For conformant bitstreams, we don't need to initialise
    // OperatingPointIdc: it gets initialised when reading a sequence
    // header and isn't read until we start getting frames. However,
    // we want to avoid reading uninitialised memory if a
    // non-conformant stream contains a frame before the first
    // sequence header.
    OperatingPointIdc = 0

    // We haven't allocated frame buffers yet
    aloc_frame_stride = -1
    aloc_frame_height = -1
    aloc_mi_width = -1
    aloc_mi_height = -1

#if COVER
    if ( COVERCLASS(IMPOSSIBLE, 1, num_large_scale_tile_anchor_frames != 0) )
        profile_disable()
#endif
    tg_end = 0
    CVSTemporalGroupDescriptionPresentFlag = 0
    sequenceHeaderChanged = 0
    prevSequenceHeaderSize = -1
    scalabilityStructureChanged = 0
    prevScalabilityStructureSize = -1
}

// Annex-B Syntax
bitstream( sz, frameLimit ) {
  isAnnexB = 1
  while ( (current_position() < sz && COVERCLASS(1,IMPOSSIBLE, COVERCLASS(1,IMPOSSIBLE,frameLimit < 0) || COVERCLASS(IMPOSSIBLE,OutputFrameCount < frameLimit) ) ) /* more_data_in_bitstream() */ ) {
       temporal_unit_size  ae(v)
       temporal_unit( temporal_unit_size, frameLimit )

#if !COVER
       if (estimateCmd) {
         break
       }
#endif // !COVER
   }
}
temporal_unit( sz, frameLimit ) {
   SeenTemporalDelimiter = 0
   SeenTemporalDelimiterBeforeSequenceHeader = 0
   while ( sz > 0 && COVERCLASS(1,IMPOSSIBLE, COVERCLASS(1,IMPOSSIBLE,frameLimit < 0) || COVERCLASS(IMPOSSIBLE,OutputFrameCount < frameLimit) ) ) {
       frame_unit_size  ae(v)
       sz -= Leb128Bytes
       frame_unit( frame_unit_size, frameLimit )
       sz -= frame_unit_size
   }
#if !COVER
   check_tu_level_rates()
#endif // !COVER
}

frame_unit( sz, frameLimit ) {
  BytesInFrame = 0
   while ( sz > 0 && COVERCLASS(1,IMPOSSIBLE, COVERCLASS(1,IMPOSSIBLE,frameLimit < 0) || COVERCLASS(IMPOSSIBLE,OutputFrameCount < frameLimit) ) ) {
       obu_length  ae(v)
       sz -= Leb128Bytes
       open_bitstream_unit( obu_length )
       sz -= obu_length
   }
#if !COVER
   check_frame_level_rates()
#endif // !COVER
}

check_frame_header_cvs () {
    // This code runs for each frame header to make sure that if the
    // sequence header or scalability structure has changed then we
    // have started a new CVS.
    uint1 is_new_cvs

    COVERCLASS (0) {
        is_new_cvs = (SeenTemporalDelimiterBeforeSequenceHeader == 1 &&
                      SeenSeqHeaderBeforeFirstFrameHeader == 1 &&
                      frame_type == KEY_FRAME &&
                      show_frame == 1 &&
                      show_existing_frame == 0 &&
                      temporal_id == 0)

        // If this isn't a new CVS, we'd better hope that the
        // sequence header and scalability structure are
        // unchanged.
        if ((sequenceHeaderChanged || scalabilityStructureChanged) &&
            ! is_new_cvs) {

            // Oh dear. We'll hit an assertion in a second, but let's
            // print out what's going on first.
            if (sequenceHeaderChanged) {
                :Cfprintf (stderr, "\nSequence header changed without starting a new CVS\n")
            }
            if (scalabilityStructureChanged) {
                :Cfprintf (stderr, "\nScalability structure changed without starting a new CVS\n")
            }

            if (SeenTemporalDelimiterBeforeSequenceHeader != 1) {
                :Cfprintf (stderr, " - No TD before sequence header\n")
            }
            if (SeenSeqHeaderBeforeFirstFrameHeader != 1) {
                :Cfprintf (stderr, " - No seq header before first frame header\n")
            }
            if (frame_type != KEY_FRAME) {
                :Cfprintf (stderr, " - Frame type is %d, not KEY_FRAME.\n", (int) b->global_data->frame_type)
            }
            if (show_frame != 1) {
                :Cfprintf (stderr, " - show_frame is %d, not 1.\n", (int) b->global_data->show_frame)
            }
            if (show_existing_frame) {
                :Cfprintf (stderr, " - show_existing_frame is true.\n")
            }
            if (temporal_id) {
                :Cfprintf (stderr, " - temporal_id is %d, not 0.\n", (int) b->global_data->temporal_id)
            }

            ASSERT (0, "Bad CVS structure.")
        }

        sequenceHeaderChanged = 0
        scalabilityStructureChanged = 0
        SeenSeqHeaderBeforeFirstFrameHeader = 0
        SeenTemporalDelimiterBeforeSequenceHeader = 0
    }
}

perform_new_cvs_checks() {
#if !COVER // work around irritating compiler bug
    int64 value
#endif //!COVER
    uint64 start
    uint64 bitpos
    uint64 metadataStart
    uint32 metadata_type

    COVERCLASS(0) {
      // need to also check for a new CVS in the dropped frames
      if (obu_type == OBU_FRAME ||
          obu_type == OBU_FRAME_HEADER) {
          start = current_position()

            // read just enough of the uncompressed header
            if (reduced_still_picture_hdr) {
              show_existing_frame = 0
            } else {
              show_existing_frame u(1)
            }
            if (show_existing_frame) {
              show_frame = 0
            } else if (reduced_still_picture_hdr) {
              frame_type = KEY_FRAME
              show_frame = 1
            } else {
              frame_type u(2)
              show_frame u(1)
            }

            // perform the new CVS checks
            check_frame_header_cvs ()

            // skip the rest
            bitpos = current_bitposition()
            if (bitpos % 8) {
              padBits = 8 - (bitpos % 8)
              padding u(padBits)
            }
            frameHeaderSize = current_position() - start
            obu_size -= frameHeaderSize
        }
        // need to also parse the metadata in the dropped frames to handle scalability structure changes associated with specific layers
        if (obu_type == OBU_METADATA) {
            metadataStart = current_position()

            metadata_type                                                            ae(v)
            if ( metadata_type == METADATA_TYPE_SCALABILITY ) {
                metadata_scalability( )
            }

            metadataSize = current_position() - metadataStart
            obu_size -= metadataSize
        }
      }
}

// OBU Syntax
open_bitstream_unit( sz ) {
    CHECK(byte_aligned(), "OBUs must be byte aligned")

    obu_header()
    if ( COVERCLASS(1, (!NON_ANNEX_B), obu_has_size_field) ) {
          obu_size                                                     ae(v)
          sz = Leb128Bytes + obu_size + 1 + obu_extension_flag
    } else {
        ASSERT( sz > 0, "Unsupported OBU size" )
        obu_size = sz - 1 - obu_extension_flag
    }
    startPosition = get_position( )
     if ( obu_type != OBU_SEQUENCE_HEADER &&
          obu_type != OBU_TEMPORAL_DELIMITER &&
          OperatingPointIdc != 0 &&
          obu_extension_flag == 1 )
     {
         inTemporalLayer = (OperatingPointIdc >> temporal_id ) & 1
         inSpatialLayer = (OperatingPointIdc >> ( spatial_id + 8 ) ) & 1
         if ( !inTemporalLayer || ! inSpatialLayer ) {
             COVERCLASS(0) {
                 perform_new_cvs_checks()
             }
             drop_obu( )
             return 0
         }
    }
    BytesInFrame += sz

#if !COVER
    if (obu_type == OBU_FRAME_HEADER || obu_type == OBU_FRAME) {
        // Starting a new actual frame. Mark it as something to count.
        // This flag will be cleared in drop_obu or if we see a tile
        // list OBU.
        count_frame_size = 1
    }
#endif

    if ( obu_type == OBU_SEQUENCE_HEADER ) {
        int64 sequenceHeaderStartPosition
        int64 sequenceHeaderCurrentPosition
        int64 sequenceHeaderSize
        int64 i
        COVERCLASS(0) {
            ASSERT(byte_aligned(), "Expected sequence header to be byte aligned at start")
            sequenceHeaderStartPosition = current_bitposition( )
        }

        ASSERT(SeenTemporalDelimiter, "Must have seen a temporal delimiter before a sequence header")
        sequence_header_obu()
        ReceivedSequenceHeader = 1

        COVERCLASS(0) {
           if (max_num_oppoints >= 0) {
              max_num_oppoints = Max(max_num_oppoints, (operating_points_cnt_minus_1 + 1))
           }

            // see whether the sequence headers have changed and store this one
            sequenceHeaderCurrentPosition = current_bitposition( )
            sequenceHeaderSize = (sequenceHeaderCurrentPosition - sequenceHeaderStartPosition + 7) / 8
            ASSERT(sequenceHeaderSize <= MAX_SEQUENCE_HEADER_SIZE, "Need to increase MAX_SEQUENCE_HEADER_SIZE, sequenceHeaderSize=%d", sequenceHeaderSize)

            unused = peek_last_bytes(sequenceHeaderSize, curSequenceHeader)

            if (!byte_aligned()) { // zero out additional bytes
              extraBits = 8 - ((sequenceHeaderCurrentPosition - sequenceHeaderStartPosition) % 8)
              mask = ~((1<<extraBits)-1)
              curSequenceHeader[sequenceHeaderSize - 1] &= mask
            }

            if (prevSequenceHeaderSize == -1) {
                // treat this as unchanged, so we can start streams without a shown key frame
                sequenceHeaderChanged = 0
            } else if (prevSequenceHeaderSize != sequenceHeaderSize) {
                sequenceHeaderChanged = 1
            } else {
                sequenceHeaderChanged = 0
                for (i=0; i<sequenceHeaderSize; i++) {
                  if (prevSequenceHeader[i] != curSequenceHeader[i]) {
                    sequenceHeaderChanged = 1
                    break
                  }
                }
            }
            prevSequenceHeaderSize = sequenceHeaderSize
            for (i=0; i<sequenceHeaderSize; i++) {
              prevSequenceHeader[i] = curSequenceHeader[i]
            }
        }
    }
    else if ( obu_type == OBU_TEMPORAL_DELIMITER )
        temporal_delimiter_obu()
    else if ( obu_type == OBU_FRAME_HEADER ) {
        CHECK( (tg_end == 0) || (tg_end == ((TileCols * TileRows) - 1)), "It is a requirement of bitstream conformance that the value of tg_end for the last tile group in each frame is equal to NumTiles - 1")

        CHECK(ReceivedSequenceHeader, "Must have received a sequence header before a frame header")
        frame_header_obu( )
    }
    else if ( obu_type == OBU_REDUNDANT_FRAME_HEADER )
        frame_header_obu( )
    else if ( obu_type == OBU_TILE_GROUP )
        tile_group_obu( obu_size )
    else if ( obu_type == OBU_METADATA )
        metadata_obu( obu_size )
    else if ( obu_type == OBU_FRAME ) {
        CHECK( (tg_end == 0) || (tg_end == ((TileCols * TileRows) - 1)), "It is a requirement of bitstream conformance that the value of tg_end for the last tile group in each frame is equal to NumTiles - 1")

        if ( COVERCLASS(IMPOSSIBLE, 1, num_large_scale_tile_anchor_frames > 0) ) {
            CHECK( FrameNumber != num_large_scale_tile_anchor_frames, "large_scale_tile: frame data not allowed in camera frame header")
        }
        frame_obu( obu_size )
    }
    else if ( COVERCLASS(LARGE_SCALE_TILES, 1, obu_type == OBU_TILE_LIST) )
        tile_list_obu( )
    else if ( COVERCLASS(1,IMPOSSIBLE,obu_type == OBU_PADDING) )
        padding_obu( obu_size )
    else
        reserved_obu( )

    currentPosition = get_position( )
    payloadBits = currentPosition - startPosition
    if ( obu_size > 0 && obu_type != OBU_TILE_GROUP &&
         COVERCLASS(1, LARGE_SCALE_TILES, obu_type != OBU_TILE_LIST) &&
         obu_type != OBU_FRAME ) {
        trailing_bits( obu_size * 8 - payloadBits )
    }
    return sz
}

drop_obu() {
#if !COVER
    // Make sure that this frame doesn't contribute to compressed size
    // or luma sample counts for this TU.
    count_frame_size = 0
#endif

    for ( i = 0; i < obu_size; i++ ) {
        dropped_byte                                                           u(8)
    }
}

// OBU Header Syntax
obu_header() {
      obu_forbidden_bit                                                        u(1)
      CHECK(obu_forbidden_bit == 0, "obu_forbidden_bit must be zero")
      obu_type                                                                 u(4)
      if ( obu_type == OBU_TEMPORAL_DELIMITER ) {
        SeenSeqHeaderBeforeFirstFrameHeader = 0
        for (i=0; i<=MAX_SPATIAL_ID; i++) {
          TemporalIds[ i ] = -1
        }
      }
      //:pdb.set_trace()
      obu_extension_flag                                                       u(1)
    if ( HasEnhancement && ( obu_type == OBU_FRAME_HEADER || obu_type == OBU_TILE_GROUP ) ) {
        CHECK( obu_extension_flag == 1, "Enhancement layer sequences must always have OBU extensions in frame headers and tile groups")
    }
      obu_has_size_field                                                       u(1)
      obu_reserved_1bit                                                        u(1)
    if ( obu_extension_flag == 1 )
          obu_extension_header()
    else if ( obu_type == OBU_SEQUENCE_HEADER )
          HasEnhancement = 0

    if ( obu_extension_flag != 1) {
      temporal_id = 0
      spatial_id = 0
    }
}

// OBU Extension Header Syntax
obu_extension_header() {
      temporal_id                                                              u(3)
      spatial_id                                                               u(2)
      // Check temporal ids are the same if spatial id is the same
      if ( TemporalIds[ spatial_id ] != -1 ) {
        CHECK( TemporalIds[ spatial_id ] == temporal_id, "All OBU extension headers that are contained in the same temporal unit and have the same spatial_id value must have the same temporal_id value." )
      } else {
        TemporalIds[ spatial_id ] = temporal_id
      }
      // Check that if temporal_ids differ, then temporal_group_description_present_flag is always 0 in this CVS
      temporalIdsDiffer = 0
      for (i=0; i<=MAX_SPATIAL_ID; i++) {
        for (j=i+1; j<=MAX_SPATIAL_ID; j++) {
          if ( TemporalIds[ i ] != -1 &&
               TemporalIds[ j ] != -1 &&
               TemporalIds[ i ] != TemporalIds[ j ] ) {
            temporalIdsDiffer = 1
          }
        }
      }
      if ( temporalIdsDiffer ) {
        CHECK( CVSTemporalGroupDescriptionPresentFlag == 0, "When any temporal unit in a coded video sequence contains OBU extension headers that have temporal_id values that are not equal to each other, temporal_group_description_present_flag must be equal to 0 (obu_extension_header())." )
      }
      if ( spatial_id > 0 ) {
          CHECK( spatial_id >= LastSpatialId, "Spatial IDs must increase" )
          LastSpatialId = spatial_id
      }

      HasEnhancement = ( spatial_id > 0 || temporal_id > 0 )
      extension_header_reserved_3bits                                          u(3)
}

// Trailing Bits Syntax
trailing_bits( nbBits ) {
      trailing_one_bit                                      u(1)
    CHECK( trailing_one_bit == 1, "trailing_one_bit must be 1")
    CHECK( nbBits > 0, "When the syntax element trailing_one_bit is read, it is a requirement that nbBits is greater than zero")
    nbBits--
    while ( nbBits > 0 ) {
          trailing_zero_bit                                 u(1)
        CHECK( trailing_zero_bit == 0, "trailing_zero_bit must be 0")
        nbBits--
    }
}

// Byte alignment Syntax

byte_alignment( ) {
    while ( get_position( ) & 7 )
          zero_bit                                          u(1)
}

// Reserved OBU Syntax
reserved_obu( ) {
}

// Sequence Header OBU Syntax
sequence_header_obu( ) {
    uint_0_2 prev_seq_profile
    uint_0_2 curr_seq_profile
    prev_seq_profile = seq_profile
    seq_profile                                                              u(3)
    curr_seq_profile = seq_profile
    if (SeenSeqHeader) {
      COVERCROSS(CROSS_PROFILE_SWITCH, prev_seq_profile, curr_seq_profile)
    }
    SeenSeqHeader = 1
    SeenSeqHeaderBeforeFirstFrameHeader = 1
    CHECK( seq_profile <= 2, "Profile must be less than or equal to 2")
    still_picture                                                            u(1)
    reduced_still_picture_hdr                                                u(1)
    if ( reduced_still_picture_hdr ) {
        timing_info_present_flag = 0
        CHECK( still_picture == 1, "If reduced_still_picture_hdr is equal to 1, it is a requirement of bitstream conformance that still_picture is equal to 1")
        decoder_model_info_present_flag = 0
        initial_display_delay_present_flag = 0
        operating_points_cnt_minus_1 = 0
        operating_point_idc[ 0 ] = 0
        seq_level_idx[ 0 ]                                                   u(5)
        seq_tier[0] = 0
        decoder_model_present_for_this_op[ 0 ] = 0
        initial_display_delay_present_for_this_op[ 0 ] = 0
    } else {
        timing_info_present_flag                                             u(1)
        if ( timing_info_present_flag ) {
          timing_info( )
          decoder_model_info_present_flag                                    u(1)
          if ( decoder_model_info_present_flag ) {
              decoder_model_info( )
          }
        } else {
          decoder_model_info_present_flag = 0
        }
          initial_display_delay_present_flag                                 u(1)
          operating_points_cnt_minus_1                                       u(5)
        for ( i = 0; i <= operating_points_cnt_minus_1; i++ ) {
              operating_point_idc[ i ]                                       u(12)
            for (j = 0; j < i; j++)
                CHECK( operating_point_idc[ i ] != operating_point_idc[ j ], "It is a requirement of bitstream conformance that operating_point_idc[ i ] is not equal to operating_point_idc[ j ] for j = 0..(i - 1)")
            seq_level_idx[ i ]                                               u(5)
            if (COVERCLASS(LEVEL5,1,seq_level_idx[ i ] > 7)) {
                seq_tier[ i ]                                                u(1)
            } else {
                seq_tier[ i ] = 0
            }
            if ( decoder_model_info_present_flag ) {
                decoder_model_present_for_this_op[ i ]                       u(1)
                if ( decoder_model_present_for_this_op[ i ] )
                    operating_parameters_info( i )
            } else {
                decoder_model_present_for_this_op[ i ] = 0
            }
            if ( initial_display_delay_present_flag ) {
                initial_display_delay_present_for_this_op[ i ]               u(1)
                if ( initial_display_delay_present_for_this_op[ i ] ) {
                    initial_display_delay_minus1                             u(4)
                    // Adrian asked for this check to be removed from the semantics, but should still be in the decoder model somewhere
                    CHECK(initial_display_delay_minus1 < 10, "It is a requirement of bitstream conformance that initial_display_delay_minus1 < 10")
                }
            } else {
                initial_display_delay_present_for_this_op[ i ] = 0
            }
        }
    }
    operatingPoint = choose_operating_point( )
    COVERCROSS(CROSS_LEVEL_TIER, seq_tier[ operatingPoint ], seq_level_idx[ operatingPoint ])
    OperatingPointIdc = operating_point_idc[ operatingPoint ]
    // TODO this check should also appy to all following OBUs
    if ( OperatingPointIdc == 0 )
        CHECK( obu_extension_flag == 0, "It is a requirement of bitstream conformance that if OperatingPointIdc is equal to 0, then obu_extension_flag is equal to 0.")
      frame_width_bits_minus_1                                                 u(4)
      frame_height_bits_minus_1                                                u(4)
    n = frame_width_bits_minus_1 + 1
      max_frame_width_minus_1                                                  u(n)
    n = frame_height_bits_minus_1 + 1
      max_frame_height_minus_1                                                 u(n)
    if ( reduced_still_picture_hdr )
        frame_id_numbers_present_flag = 0
    else
          frame_id_numbers_present_flag                                        u(1)
    if ( frame_id_numbers_present_flag ) {
          delta_frame_id_length_minus_2                                         u(4)
          additional_frame_id_length_minus_1                                    u(3)
    }
    use_128x128_superblock                                                     u(1)
    enable_filter_intra                                                        u(1)
    enable_intra_edge_filter                                                   u(1)
    if ( reduced_still_picture_hdr ) {
        enable_interintra_compound = 0
        enable_masked_compound = 0
        enable_warped_motion = 0
        enable_dual_filter = 0
        enable_order_hint = 0
        enable_jnt_comp = 0
        enable_ref_frame_mvs = 0
        seq_force_screen_content_tools = SELECT_SCREEN_CONTENT_TOOLS
        seq_force_integer_mv = SELECT_INTEGER_MV
        OrderHintBits = 0
    } else {
        enable_interintra_compound                                             u(1)
        enable_masked_compound                                                 u(1)
        enable_warped_motion                                                   u(1)
        enable_dual_filter                                                     u(1)
        enable_order_hint                                                      u(1)
        if ( enable_order_hint ) {
            enable_jnt_comp                                                    u(1)
            enable_ref_frame_mvs                                               u(1)
        } else {
            enable_jnt_comp = 0
            enable_ref_frame_mvs = 0
        }
        seq_choose_screen_content_tools                                        u(1)
        if ( seq_choose_screen_content_tools ) {
            seq_force_screen_content_tools = SELECT_SCREEN_CONTENT_TOOLS
        } else {
            seq_force_screen_content_tools                                     u(1)
        }

        if ( seq_force_screen_content_tools > 0 ) {
            seq_choose_integer_mv                                              u(1)
            if (seq_choose_integer_mv) {
                seq_force_integer_mv = SELECT_INTEGER_MV
            } else {
                seq_force_integer_mv                                           u(1)
            }
        } else {
            seq_force_integer_mv = SELECT_INTEGER_MV
        }
        if ( enable_order_hint ) {
              order_hint_bits_minus_1                                           u(3)
            OrderHintBits = order_hint_bits_minus_1 + 1
        } else {
            OrderHintBits = 0
        }
    }
      enable_superres                                                          u(1)
      enable_cdef                                                              u(1)
      enable_restoration                                                       u(1)
    color_config( )
      film_grain_params_present                                                u(1)
}

choose_operating_point( ) {
    opPoint = DesiredOpPoint
    COVERCLASS(0) {
        if (opPoint < 0 || opPoint > operating_points_cnt_minus_1) {
            opPoint = 0
        }
    }
    return opPoint
}

// Color Config Syntax
color_config( ) {
      high_bitdepth                                                            u(1)
    if ( COVERCLASS(PROFILE2, 1, COVERCLASS(PROFILE2, (PROFILE0 || PROFILE1), seq_profile == 2) && COVERCLASS(PROFILE2, high_bitdepth)) ) {
          twelve_bit                                                           u(1)
        BitDepth = twelve_bit ? 12 : 10
    } else if (COVERCLASS(1, IMPOSSIBLE, seq_profile <= 2 )) {
        BitDepth = COVERCLASS((PROFILE0 || PROFILE1), 1, high_bitdepth) ? 10 : 8
    }
    if ( COVERCLASS(PROFILE1, (PROFILE0 || PROFILE2), seq_profile == 1) ) {
        mono_chrome = 0
    } else {
        mono_chrome                                                            u(1)
    }
    NumPlanes = COVERCLASS((PROFILE0 || PROFILE2), 1, mono_chrome) ? 1 : 3
    color_description_present_flag                                             u(1)
    if ( color_description_present_flag ) {
        color_primaries                                                        u(8)
        transfer_characteristics                                               u(8)
        matrix_coefficients                                                    u(8)
    } else {
        color_primaries = CP_UNSPECIFIED
        transfer_characteristics = TC_UNSPECIFIED
        matrix_coefficients = MC_UNSPECIFIED
    }
    if ( COVERCLASS((PROFILE0 || PROFILE2), 1, mono_chrome) ) {
        color_range                                                               u(1)
        subsampling_x = 1
        subsampling_y = 1
        chroma_sample_position = CSP_UNKNOWN
        separate_uv_delta_q = 0
    } else {
        if ( COVERCLASS((PROFILE1 || PROFILE2), 1, color_primaries == CP_BT_709 &&
                    transfer_characteristics == TC_SRGB &&
                    COVERCLASS((PROFILE1 || PROFILE2), 1, matrix_coefficients == MC_IDENTITY)) ) {
            color_range = 1
            subsampling_x = 0
            subsampling_y = 0
        } else {
            color_range                                                            u(1)
            if ( COVERCLASS(PROFILE0, (PROFILE1 || PROFILE2), seq_profile == 0) ) {
                subsampling_x = 1
                subsampling_y = 1
            } else {
                if ( COVERCLASS(PROFILE1, PROFILE2, seq_profile == 1) ) {
                    subsampling_x = 0
                    subsampling_y = 0
                } else {
                    if ( BitDepth == 12 ) {
                        subsampling_x                                                  u(1)
                        if ( subsampling_x )
                            subsampling_y                                                  u(1)
                        else
                            subsampling_y = 0
                    } else {
                        subsampling_x = 1
                        subsampling_y = 0
                    }
                }
            }
            if (COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) && COVERCLASS((PROFILE0 || PROFILE2), PROFILE2, subsampling_y))) {
                chroma_sample_position                                             u(2)
            }
        }
        separate_uv_delta_q                                                        u(1)
    }
    if ( COVERCLASS((PROFILE1 || PROFILE2), 1, matrix_coefficients == MC_IDENTITY) )
        CHECK( subsampling_x == 0 && subsampling_y == 0, "If matrix_coefficients is equal to MC_IDENTITY, it is a requirement of bitstream conformance that subsampling_x is equal to 0 and subsampling_y is equal to 0." )
}

// Timing Info Syntax

timing_info( ) {
    num_units_in_display_tick                                                  u(32)
    CHECK(num_units_in_display_tick > 0, "num_units_in_display_tick must be greater than 0")
    time_scale                                                                 u(32)
    CHECK(time_scale > 0, "time_scale must be greater than 0")
    equal_picture_interval                                                     u(1)
    if (equal_picture_interval) {
       num_ticks_per_picture_minus_1                                            ae(v)
       CHECK(num_ticks_per_picture_minus_1 >= 0, "num_ticks_per_picture_minus_1 must be in the range 0 to (1 << 32) - 2, inclusive.")
       CHECK(num_ticks_per_picture_minus_1 <= (1 << 32) - 2, "num_ticks_per_picture_minus_1 must be in the range 0 to (1 << 32) - 2, inclusive.")
    }
}

// Decoder Model Info Syntax

decoder_model_info( ) {
      buffer_delay_length_minus_1                               u(5)
      num_units_in_decoding_tick                                               u(32)
      buffer_removal_time_length_minus_1                                       u(5)
      frame_presentation_time_length_minus_1                                   u(5)
}

// Operating Parameters Info Syntax

operating_parameters_info( op ) {
    n = buffer_delay_length_minus_1 + 1
      decoder_buffer_delay                                                     u(n)
      encoder_buffer_delay                                                     u(n)
      low_delay_mode_flag                                                      u(1)
}

// This runs at the end of each frame and snaffles away the
// compression ratio of the frame: we'll check that ratio at the end
// of the temporal unit, but it depends on SpeedAdj, which we don't
// know until then.
#if !COVER
check_frame_level_rates () {
   // Store the compressed size of the frame into
   // frame_byte_counters. It will be checked at the end of the TU.
   if (count_frame_size && ! show_existing_frame) {
       ASSERT (frame_size_counter < MAX_SCALABILITY_STRUCTURE_SIZE,
               "Too many frames in TU?")
       frame_compressed_sizes [frame_size_counter] = BytesInFrame
       frame_luma_sample_counts [frame_size_counter] = UpscaledWidth * FrameHeight
       frame_size_counter ++
   }
}

// This runs at the end of each TU and does some bitstream conformance
// checks
check_tu_level_rates () {
    uint64 MaxHeaderRate
    uint64 MaxDisplayRate
    uint64 MaxDecodeRate
    uint64 TotalDisplayLumaSampleRate
    uint64 TotalDecodedLumaSampleRate

    MaxHeaderRate = Profiles_Levels[LevelIdx][PROFILES_AND_LEVELS_MaxHeaderRate]
    MaxDisplayRate = Profiles_Levels[LevelIdx][PROFILES_AND_LEVELS_MaxDisplayRate]
    MaxDecodeRate = Profiles_Levels[LevelIdx][PROFILES_AND_LEVELS_MaxDecodeRate]

    CHECK (TargetDecodeFrameRate < MaxHeaderRate,
           "Target frame rate (%d) is higher than level max frame rate (%d)",
           TargetDecodeFrameRate, MaxHeaderRate)

    // Check maximum display sample rate. Note that this check
    // trivially passes when TargetDisplayFrameRate is zero.
    TotalDisplayLumaSampleRate = DisplayedLumaSamplesInTU * TargetDisplayFrameRate
    CHECK (TotalDisplayLumaSampleRate <= MaxDisplayRate,
           "Display frame rate (%d) gives TotalDisplayLumaSampleRate = %d > %d.",
           TargetDisplayFrameRate, TotalDisplayLumaSampleRate, MaxDisplayRate)

    // Check maximum decode sample rate. Again, this check trivially
    // passes when TargetDecodeFrameRate is zero.
    TotalDecodedLumaSampleRate = LumaSamplesInTU * TargetDecodeFrameRate
    CHECK (TotalDecodedLumaSampleRate <= MaxDecodeRate,
           "Decode frame rate (%d) gives TotalDecodedLumaSampleRate = %d > %d.",
           TargetDecodeFrameRate, TotalDecodedLumaSampleRate, MaxDecodeRate)

    // We run all ratios multiplied by 10,000 (using a power of 10
    // because we don't care about efficiency and this makes it much
    // easier to read).
    uint64 ratio_scale
    ratio_scale = 10000

    uint64 UnCompressedSize
    uint64 CompressedSize
    uint64 CompressedRatio
    uint64 SpeedAdj
    uint64 MinPicCompressRatio

    if (seq_profile == 0) {
        PicSizeProfileFactor = 15
    } else if (seq_profile == 1) {
        PicSizeProfileFactor = 30
    } else {
        PicSizeProfileFactor = 36
    }

    // Calculating SpeedAdj
    uint64 luma_samples_in_tu
    luma_samples_in_tu = 0
    for (idx = 0; idx < frame_size_counter; idx++) {
        luma_samples_in_tu += frame_luma_sample_counts[idx]
    }
    ASSERT (luma_samples_in_tu == LumaSamplesInTU,
            "TU luma sample tracking has got out of whack (%llu != %llu)",
            luma_samples_in_tu, LumaSamplesInTU)

    MinPicCompressRatio = ratio_scale * 4 / 5
    if (! still_picture) {
        idx = (seq_tier[operatingPoint] ?
               PROFILES_AND_LEVELS_MinCompBasisHigh :
               PROFILES_AND_LEVELS_MinCompBasisMain)
        MinCompBasis = Profiles_Levels[LevelIdx][idx]

        // SpeedAdj is TotalDecodedLumaSampleRate / MaxDisplayRate.
        // TotalDecodedLumaSampleRate is computed for this TU by dividing
        // by the length of the TU (which we do by multiplying by the
        // frame rate).
        //
        // If the frame rate isn't given then TargetDisplayFrameRate will
        // be zero, SpeedAdj will be zero and MinPicCompressRatio will
        // come out as 0.8 * ratio_scale.
        //
        // Note that we use luma_samples_in_tu rather than
        // LumaSamplesInTU to force the calculation to be done at 64
        // bits. (ratio_scale isn't enough because constant folding
        // just replaces it with its value).
        SpeedAdj = ((luma_samples_in_tu * TargetDisplayFrameRate * ratio_scale) /
                    MaxDisplayRate)

        // I'm a little twitchy about this multiplication, so let's check
        // it doesn't overflow.
        ASSERT ((TargetDisplayFrameRate == 0) ||
                (luma_samples_in_tu * TargetDisplayFrameRate >= luma_samples_in_tu),
                "Overflow in multiplication (1)")
        ASSERT ((TargetDisplayFrameRate == 0) ||
                (luma_samples_in_tu * TargetDisplayFrameRate * ratio_scale >=
                 luma_samples_in_tu * TargetDisplayFrameRate),
                "Overflow in multiplication (2)")

        MinPicCompressRatio = Max (MinPicCompressRatio, MinCompBasis * SpeedAdj)
    }

    // Check the compression ratio for each frame in the TU
    for (idx = 0; idx < frame_size_counter; idx++) {
        CompressedSize = frame_compressed_sizes [idx]
        UnCompressedSize = (frame_luma_sample_counts [idx] * PicSizeProfileFactor) / 8

        if (CompressedSize > 128) {
            CompressedRatio = (ratio_scale * UnCompressedSize) / (CompressedSize - 128)
            CHECK (CompressedRatio >= MinPicCompressRatio,
                   "[COMPRATIO] CompressedRatio is %llu.%04llu (minimum: %llu.%04llu)",
                   CompressedRatio / ratio_scale,
                   CompressedRatio % ratio_scale,
                   MinPicCompressRatio / ratio_scale,
                   MinPicCompressRatio % ratio_scale)
        }
    }
}

#endif // !COVER

// Temporal Delimiter OBU Syntax
temporal_delimiter_obu( ) {
    SeenFrameHeader = 0
    LastSpatialId = 0
    LumaSamplesInTU = 0
    FramesDecodedInTU = 0
    FramesDisplayedInTU = 0
    DisplayedLumaSamplesInTU = 0
    SeenTemporalDelimiter = 1
    SeenTemporalDelimiterBeforeSequenceHeader = 1
    frame_size_counter = 0
}

// Padding OBU Syntax
padding_obu( obu_padding_length ) {
    // Section 5.7 of the spec is a bit irritating here: we should
    // read some bits, then there should be a trailing bits sequence.
    // Except if sz is zero, in which case we don't read anything.
    //
    // To implement the checks properly, we read the whole caboodle
    // and then rewind back to the last nonzero byte, which hopefully
    // should be 0x80.
    //
    // After we return, we'll call trailing_bits to read that stuff
    // again.
    COVERCLASS (0) {
        if (obu_padding_length == 0) {
            return 0
        }
    }

    int64 pos
    int max_i

    pos = current_position ()
    max_i = -1

    for (i = 0; i < obu_padding_length; i++) {
        obu_padding_byte                         u(8)

        COVERCLASS (0) {
            if (obu_padding_byte) {
                max_i = i
            }
        }
    }

    CHECK (max_i >= 0,
           "nonempty padding_obu should have some nonzero bytes")

    // Rewind to just before the last nonzero byte, to be read by
    // trailing_bits.
    set_pos (pos + max_i)
}

// ignore_obu is called for OBU data that isn't defined in the spec
// (but isn't part of padding_obu). This is a similar situation to
// padding_obu, but this is called after reading some of a metadata
// OBU so the empty OBU case won't apply.
ignore_obu (sz) {
    int64 pos
    int max_i

    COVERCLASS (0) {
        CHECK (sz > 0, "no space for trailing bits after ignore_obu")

        pos = current_position ()
        max_i = -1

        for (i = 0; i < sz; i++) {
            obu_ignored_byte                         u(8)
            if (obu_ignored_byte) {
                max_i = i
            }
        }

        CHECK (max_i >= 0,
               "nonempty ignored_obu should have some nonzero bytes")

        // Rewind to just before the last nonzero byte, to be read by
        // trailing_bits.
        set_pos (pos + max_i)
    }
}

// Metadata OBU Syntax
metadata_obu( sz ) {
    uint32 metadata_type
    metadata_type                                                            ae(v)
    if ( metadata_type == METADATA_TYPE_ITUT_T35 )
        metadata_itut_t35( )
    else if ( metadata_type == METADATA_TYPE_HDR_CLL )
        metadata_hdr_cll( )
    else if ( metadata_type == METADATA_TYPE_HDR_MDCV )
        metadata_hdr_mdcv( )
    else if ( metadata_type == METADATA_TYPE_SCALABILITY )
        metadata_scalability( )
    else if ( metadata_type == METADATA_TYPE_TIMECODE )
        metadata_timecode( )
    else if ( COVERCLASS(LARGE_SCALE_TILES, 1, metadata_type == METADATA_TYPE_ARGON_DESIGN_LST) )
        metadata_argon_design_lst( )
    else {
        ignore_obu( sz - Leb128Bytes )
    }
}

// Metadata Private Data Syntax
metadata_itut_t35( ) {
    itu_t_t35_country_code                     u(8)

    int bytes_read
    bytes_read = 1

    if (itu_t_t35_country_code == 0xff) {
        itu_t_t35_country_code_extension_byte  u(8)
        bytes_read++
    }

    ignore_obu (obu_size - (Leb128Bytes + bytes_read))
}

// Metadata High Dynamic Range Content Light Level Syntax
metadata_hdr_cll( ) {
    max_cll                                                                    u(16)
    max_fall                                                                   u(16)
}

// Metadata High Dynamic Range Mastering Display Color Volume Syntax
metadata_hdr_mdcv( ) {
    for ( i = 0; i < 3; i++ ) {
        primary_chromaticity_x[ i ]                                            u(16)
        primary_chromaticity_y[ i ]                                            u(16)
    }
    white_point_chromaticity_x                                                 u(16)
    white_point_chromaticity_y                                                 u(16)
    luminance_max                                                              u(32)
    luminance_min                                                              u(32)
}

// Metadata Scalability Syntax
metadata_scalability( ) {
    int64 scalabilityStartPosition
    int64 scalabilityCurrentPosition
    int64 scalabilitySize
    int64 i
    COVERCLASS(0) {
      scalabilityStartPosition = current_bitposition( )
    }

    uint_0_28 scalability_mode_idc
      scalability_mode_idc                                                     u(8)
    COVERCROSS(VALUE_SCALABILITY_MODE, scalability_mode_idc)
    CVSTemporalGroupDescriptionPresentFlag = 0
    if (scalability_mode_idc == SCALABILITY_SS)
        scalability_structure( )

    COVERCLASS(0) {
        // see whether the scalability structures have changed and store this one
        ASSERT(byte_aligned(), "Expected scalability structure to be byte aligned at start")
        scalabilityCurrentPosition = current_bitposition( )
        scalabilitySize = (scalabilityCurrentPosition - scalabilityStartPosition + 7) / 8
        ASSERT(scalabilitySize <= MAX_SCALABILITY_STRUCTURE_SIZE, "Need to increase MAX_SCALABILITY_STRUCTURE_SIZE, scalabilitySize=%d", scalabilitySize)

        unused = peek_last_bytes(scalabilitySize, curScalabilityStructure)

        if (!byte_aligned()) { // zero out additional bytes
            extraBits = 8 - ((scalabilityCurrentPosition - scalabilityStartPosition) % 8)
            mask = ~((1<<extraBits)-1)
            curScalabilityStructure[scalabilitySize - 1] &= mask
        }

        if (prevScalabilityStructureSize == -1) {
            // treat this as unchanged, so we can start streams without a shown key frame
            scalabilityStructureChanged = 0
        } else if (prevScalabilityStructureSize != scalabilitySize) {
            scalabilityStructureChanged = 1
        } else {
            scalabilityStructureChanged = 0
            for (i=0; i<scalabilitySize; i++) {
              if (prevScalabilityStructure[i] != curScalabilityStructure[i]) {
                scalabilityStructureChanged = 1
                break
              }
            }
        }
        prevScalabilityStructureSize = scalabilitySize
        for (i=0; i<scalabilitySize; i++) {
          prevScalabilityStructure[i] = curScalabilityStructure[i]
        }
    }
}


// Scalability structure syntax
scalability_structure( ) {
      spatial_layers_cnt_minus_1                                                       u(2)
      spatial_layer_dimensions_present_flag                                    u(1)
      spatial_layer_description_present_flag                                   u(1)
      temporal_group_description_present_flag                                  u(1)
      if ( temporal_group_description_present_flag ) {
        CVSTemporalGroupDescriptionPresentFlag = 1
      }
      scalability_structure_reserved_3bits                                     u(3)
    if ( spatial_layer_dimensions_present_flag ) {
        for ( i = 0; i <= spatial_layers_cnt_minus_1 ; i++ ) {
              spatial_layer_max_width[ i ]                                     u(16)
              spatial_layer_max_height[ i ]                                    u(16)
        }
    }
    if ( spatial_layer_description_present_flag ) {
        for ( i = 0; i <= spatial_layers_cnt_minus_1; i++ )
              spatial_layer_ref_id[ i ]                                        u(8)
    }
    if (temporal_group_description_present_flag) {
          temporal_group_size                                                  u(8)
        for ( i = 0; i < temporal_group_size; i++ ) {
              temporal_group_temporal_id[ i ]                                  u(3)
              temporal_group_temporal_switching_up_point_flag[ i ]             u(1)
              temporal_group_spatial_switching_up_point_flag[ i ]              u(1)
              temporal_group_ref_cnt[ i ]                                      u(3)
            for ( j = 0; j < temporal_group_ref_cnt[ i ]; j++ ) {
                  temporal_group_ref_pic_diff[ i ][ j ]                        u(8)
            }
        }
    }
}

// Metadata Timecode Syntax
metadata_timecode( ) {
    counting_type                                                              u(5)
    full_timestamp_flag                                                        u(1)
    discontinuity_flag                                                         u(1)
    cnt_dropped_flag                                                           u(1)
    n_frames                                                                   u(9)
    if ( full_timestamp_flag ) {
        seconds_value                                                          u(6)
        minutes_value                                                          u(6)
        hours_value                                                            u(5)
    } else {
        seconds_flag                                                           u(1)
        if ( seconds_flag ) {
            seconds_value                                                      u(6)
            minutes_flag                                                       u(1)
            if ( minutes_flag ) {
                minutes_value                            u(6)
                hours_flag                               u(1)
                if ( hours_flag ) {
                    hours_value                          u(5)
                }
            }
        }
    }
    time_offset_length                                                         u(5)
    if ( time_offset_length > 0 ) {
        time_offset_value                                                      u(time_offset_length)
    }
}

metadata_argon_design_lst( ) COVERCLASS(LARGE_SCALE_TILES) {
    num_large_scale_tile_anchor_frames                  u(8)
    CHECK(num_large_scale_tile_anchor_frames <= 128, "num_large_scale_tile_anchor_frames must be <= 128")
    COVERCROSS(VALUE_NUM_ANCHOR_FRAMES, num_large_scale_tile_anchor_frames)
    num_large_scale_tile_tile_list_obus                 u(16)
#if COVER
    if ( COVERCLASS(LARGE_SCALE_TILES, IMPOSSIBLE, num_large_scale_tile_anchor_frames != 0) )
        profile_disable()
    else
        profile_enable()
#endif
}

// Frame Header OBU Syntax
frame_header_obu( ) {
    CHECK(SeenSeqHeader == 1, "It is a requirement of bitstream conformance that a sequence header OBU has been received before a frame header OBU")
    if ( obu_type == OBU_FRAME_HEADER )
        CHECK( SeenFrameHeader == 0, "If obu_type is equal to OBU_FRAME_HEADER, it is a requirement of bitstream conformance that SeenFrameHeader is equal to 0")
    if ( obu_type == OBU_REDUNDANT_FRAME_HEADER )
        CHECK( SeenFrameHeader == 1, "If obu_type is equal to OBU_REDUNDANT_FRAME_HEADER, it is a requirement of bitstream conformance that SeenFrameHeader is equal to 1")
    if ( SeenFrameHeader == 1 ) {
        frame_header_copy                                                      u(FrameHeaderSize)
    } else {
        SeenFrameHeader = 1
        TileGroupCount = 0
        frameHeaderStart = current_bitposition()
        uncompressed_header( )

        check_frame_header_cvs ()

        if (show_existing_frame == 0) {
            LumaSamplesInTU += UpscaledWidth * FrameHeight
            FramesDecodedInTU++
        }
#if COVER
	// Clear the current frame before starting a new one - this
	// isn't really necessary, but intrabc can sometimes pick up
	// old values which get zeroed out later. This messes up our
	// range coverage.
        //
        // SetArray (defined in av1_pre.c) is essentially just memset
        // and sets every element to the given value.
        if (! show_existing_frame) {
          :Cbuiltin_SetArray (b->global_data->CurrFrame, 42)
        }
#endif
#if OUTPUT_TRANSFORM_BLOCKS
        jsh_FrameWidth = FrameWidth
        jsh_FrameHeight = FrameHeight
        jsh_subX = subsampling_x
        jsh_subY = subsampling_y
        jsh_128 = use_128x128_superblock
        :print "transform_block: frame {} {} {} {} {}".format(p.jsh_FrameWidth, p.jsh_FrameHeight, p.jsh_subX, p.jsh_subY, p.jsh_128)
        :C printf("transform_block: frame %d %d %d %d %d\n", jsh_FrameWidth, jsh_FrameHeight, jsh_subX, jsh_subY, jsh_128);
#endif
        if ( show_existing_frame ) {
            decode_frame_wrapup( )
            SeenFrameHeader = 0
        } else {
            TileNum = 0
            SeenFrameHeader = 1
        }
        FrameHeaderSize = current_bitposition() - frameHeaderStart
    }
    IsFirstFrame = 0
    if ( COVERCLASS(LARGE_SCALE_TILES, LARGE_SCALE_TILES, FrameNumber == num_large_scale_tile_anchor_frames) ) {
        FrameNumber = FrameNumber + 1
#if COVER
        profile_enable()
#endif
    }
}

// Quantization Params Syntax
quantization_params( ) {
      base_q_idx                                                               u(8)
    DeltaQYDc = read_delta_q( )
    COVERCROSS(VALUE_CABAC_DELTAQYDc, DeltaQYDc)
    if ( COVERCLASS(1, (PROFILE0 || PROFILE2), NumPlanes > 1) ) {
        if ( separate_uv_delta_q )
            diff_uv_delta                                                      u(1)
        else
          diff_uv_delta = 0
        DeltaQUDc = read_delta_q( )
        DeltaQUAc = read_delta_q( )
        COVERCROSS(VALUE_CABAC_DELTAQUDc, DeltaQUDc)
        COVERCROSS(VALUE_CABAC_DELTAQUAc, DeltaQUAc)
        if ( diff_uv_delta ) {
          DeltaQVDc = read_delta_q( )
          DeltaQVAc = read_delta_q( )
        } else {
          DeltaQVDc = DeltaQUDc
          DeltaQVAc = DeltaQUAc
        }
        COVERCROSS(VALUE_CABAC_DELTAQVDc, DeltaQVDc)
        COVERCROSS(VALUE_CABAC_DELTAQVAc, DeltaQVAc)
    } else {
        DeltaQUDc = 0
        DeltaQUAc = 0
        DeltaQVDc = 0
        DeltaQVAc = 0
    }
      using_qmatrix                                                            u(1)
    if (using_qmatrix) {
          qm_y                                                                 u(4)
          qm_u                                                                 u(4)
        if ( !separate_uv_delta_q )
            qm_v = qm_u
        else
              qm_v                                                             u(4)
    }
}

check_valid_profile_level( ) {
  levelMajor = 2 + (seq_level_idx[operatingPoint] >> 2)
  levelMinor = seq_level_idx[operatingPoint] & 3
  LevelIdx = -1
  for (i=0;
       COVERCLASS(1, MAXLEVEL, i < PROFILES_AND_LEVELS_Num_Valid_Levels);
       COVERCLASS(LEVEL5, i++)) {
    if (COVERCLASS(1, LEVEL5,(COVERCLASS(1, LEVEL5,Profiles_Levels[i][PROFILES_AND_LEVELS_Level_Major] == levelMajor)) &&
        (COVERCLASS(1, LEVEL5, Profiles_Levels[i][PROFILES_AND_LEVELS_Level_Minor] == levelMinor)))) {
      LevelIdx = i
      break
    }
  }

  // Mapped level contained in table, so conformance tests below apply
  if (COVERCLASS((!MAXLEVEL), MAXLEVEL, LevelIdx != -1)) {
    CHECK(UpscaledWidth * FrameHeight <= Profiles_Levels[LevelIdx][PROFILES_AND_LEVELS_MaxPicSize], "UpscaledWidth * FrameHeight is less than or equal to MaxPicSize")
    CHECK(UpscaledWidth <= Profiles_Levels[LevelIdx][PROFILES_AND_LEVELS_MaxHSize], "UpscaledWidth is less than or equal to MaxHSize")
    CHECK(FrameHeight <= Profiles_Levels[LevelIdx][PROFILES_AND_LEVELS_MaxVSize], "FrameHeight is less than or equal to MaxVSize")
    // Depends on frame rate: TotalDisplayLumaSampleRate is less than or equal to MaxDisplayRate
    // Depends on frame rate: TotalDecodedLumaSampleRate is less than or equal to MaxDecodeRate
    // Depends on frame rate: NumFrameHeadersSec is less than or equal to MaxHeaderRate
    // Depends on frame rate: The number of tiles per second is less than or equal to MaxTiles * 120
    CHECK(TileCols * TileRows <= Profiles_Levels[LevelIdx][PROFILES_AND_LEVELS_MaxTiles], "NumTiles (%d) is less than or equal to MaxTiles (%d)",TileCols * TileRows,Profiles_Levels[LevelIdx][PROFILES_AND_LEVELS_MaxTiles])
    CHECK(TileCols <= Profiles_Levels[LevelIdx][PROFILES_AND_LEVELS_MaxTileCols], "TileCols (%d) is less than or equal to MaxTileCols (%d)",TileCols,Profiles_Levels[LevelIdx][PROFILES_AND_LEVELS_MaxTileCols])
    // Depends on frame rate: CompressedRatio is greater than or equal to MinPicCompressRatio
    CHECK(FrameWidth >= 16, "FrameWidth is greater than or equal to 16")
    CHECK(FrameHeight >= 16, "FrameHeight is greater than or equal to 16")

    //CHECK(FrameWidth >= 384, "FrameWidth is greater than or equal to 64") //Client specific constrain - ClientB
    //CHECK(FrameHeight >= 384, "FrameHeight is greater than or equal to 64") //Client specific constrain - ClientB
    //CHECK(FrameWidth >= 384, "[ILLEGAL_RESOLUTION] Client specific size constraint, Width (%d) too small", FrameWidth) //Client specific constrain - ClientC
    //CHECK(FrameHeight >= 384, "[ILLEGAL_RESOLUTION] Client specific size constraint, Width (%d) too small", FrameHeight) //Client specific constrain - ClientC
    //CHECK(UpscaledWidth <= 8192, "UpscaledWidth is less than or equal to 8192") //Client specific constrain - ClientD
    //CHECK(Height <= 8192, "Height is less than or equal to 8192") //Client specific constrain - ClientD
    //CHECK(Width > Height || Width <= 4352, "Shorter dimension less than 4352") //Client specific constrain - ClientD
    //CHECK(Height > Width || Height <= 4352, "Shorter dimension less than 4352") //Client specific constrain - ClientD
    // Depends on frame rate: MaxTileSizeInLumaSamples * NumFrameHeadersSec * TemporalParallelDen/TemporalParallelNum is less than or equal to 588,251,136 (where this number is the decode luma sample rate of 4096x2176 * 60fps * 1.1)
  }
}

check_valid_profile_level_per_tile( ) {
  // Mapped level contained in table, so conformance tests below apply
  if (COVERCLASS((!MAXLEVEL),MAXLEVEL,LevelIdx != -1)) {
    CHECK(( TileWidth * SuperresDenom / SUPERRES_NUM ) <= 4096, "Upscaled TileWidth is less than or equal to 4096 for each tile")
    if ( use_superres == 0 && RightMostTile == 0 ) {
      // not actually possible to hit this, as the minium superblock size is 64x64
      CHECK(TileWidth >= 64, "If use_superres is equal to 0 and RightMostTile is equal to 0, TileWidth must be greater than or equal to 64")
    }
    if ( use_superres == 1 && RightMostTile == 0 ) {
      CHECK(TileWidth >= 128, "If use_superres is equal to 1 and RightMostTile is equal to 0, TileWidth must be greater than or equal to 128")
    }
    CHECK(TileWidth * TileHeight <= 4096 * 2304, "TileWidth * TileHeight is less than or equal to 4096 * 2304 for each tile")
    CHECK(FrameWidth - (MiColStart * MI_SIZE) >= 8, "CroppedTileWidth is greater than or equal to 8 for each tile")
    CHECK(FrameHeight - (MiRowStart * MI_SIZE) >= 8, "CroppedTileHeight is greater than or equal to 8 for each tile")
  }
}

// Allocate any variable-size buffers
aloc_buffers () COVERCLASS(0) {
    // Align dimensions to superblock size
    mask = MAX_SB_SIZE - 1
    stride = ((max_frame_width_minus_1 + 1) + mask) & ~mask
    height = ((max_frame_height_minus_1 + 1) + mask) & ~mask

    area = stride * height
    mi_width = stride / MI_SIZE
    mi_height = height / MI_SIZE

    // If the max frame width and/or height has changed, this must be
    // the start of a CVS, so we can safely trash anything that was in
    // the arrays before.

    // This if() statement guards stuff that depends on the
    // frame width and height independently.
    if ((aloc_frame_height < height) || (aloc_frame_stride < stride)) {

        alloc LrFrame           [3] [height] [stride]
        alloc CdefFrame         [3] [height] [stride]
        alloc UpscaledCdefFrame [3] [height] [stride]
        alloc UpscaledCurrFrame [3] [height] [stride]
        alloc MotionFieldMvs    [8] [height >> 3] [stride >> 3] [2]
        alloc FrameStore        [NUM_REF_FRAMES] [3] [height] [stride]

        alloc OutY              [height] [stride]
        alloc OutU              [height] [stride]
        alloc OutV              [height] [stride]
        alloc OutputFrameY      [height] [stride]
        alloc OutputFrameU      [height] [stride]
        alloc OutputFrameV      [height] [stride]

        alloc noiseStripe       [(height + 33)/32] [3] [34] [stride + 34]
        alloc noiseImage        [3] [height] [stride]

        aloc_frame_stride = stride
        aloc_frame_height = height
    }

    // And, finally, stuff that depends on mi_width and mi_height.
    if (mi_width > aloc_mi_width || mi_height > aloc_mi_height) {

        mi_stride = mi_width + (MAX_SB_SIZE / MI_SIZE)

        alloc MfRefFrames     [mi_height] [mi_stride]
        alloc MfMvs           [mi_height] [mi_stride] [2]
        alloc Skips           [mi_height] [mi_stride]
        alloc InterTxSizes    [mi_height] [mi_stride]
        alloc TxSizes         [mi_height] [mi_stride]
        alloc TxTypes         [mi_height] [mi_stride]
        alloc MiSizes         [mi_height] [mi_stride]
        alloc LoopfilterTxSizes [3] [mi_height] [mi_stride]
        alloc MiColStartGrid  [mi_height] [mi_stride]
        alloc MiRowStartGrid  [mi_height] [mi_stride]
        alloc MiColEndGrid    [mi_height] [mi_stride]
        alloc MiRowEndGrid    [mi_height] [mi_stride]
        alloc TileStarts      [mi_height] [mi_stride]
        alloc SegmentIds      [mi_height] [mi_stride]
        alloc PrevSegmentIds  [mi_height] [mi_stride]

        alloc PaletteSizes    [2] [mi_height] [mi_stride]
        alloc PaletteColors   [2] [mi_height] [mi_stride] [9]
        alloc DeltaLFs            [mi_height] [mi_stride] [FRAME_LF_COUNT]
        alloc YModes              [mi_height] [mi_stride]
        alloc UVModes             [mi_height] [mi_stride]
        alloc CompGroupIdxs       [mi_height] [mi_stride]
        alloc CompoundIdxs        [mi_height] [mi_stride]
        alloc IsInters            [mi_height] [mi_stride]
        alloc SkipModes           [mi_height] [mi_stride]
        alloc RefFrames           [mi_height] [mi_stride] [2]
        alloc RefFramesWritten    [mi_height] [mi_stride]
        alloc InterpFilters       [mi_height] [mi_stride] [2]
        alloc Mvs                 [mi_height] [mi_stride] [2] [2]
        alloc PredMvs             [mi_height] [mi_stride] [2] [2]

        alloc SavedRefFrames      [NUM_REF_FRAMES] [mi_height] [mi_stride]
        alloc SavedMvs            [NUM_REF_FRAMES] [mi_height] [mi_stride] [2]
        alloc SavedSegmentIds     [NUM_REF_FRAMES] [mi_height] [mi_stride]

        alloc cdef_idx            [mi_height] [mi_stride]

        aloc_mi_width = mi_width
        aloc_mi_height = mi_height
    }
}

// Uncompressed Header Syntax
uncompressed_header( ) {
    int ref_order_hint[ NUM_REF_FRAMES ]
    uint1 cross_base_q_idx_is_zero

    // Re-allocate buffers if necessary. By the time we get here,
    // we've read the current sequence header, which defines things
    // like the maximum frame size, so we know how big the buffers
    // need to be.
    COVERCLASS(0) {
      aloc_buffers ()
    }

    if ( frame_id_numbers_present_flag ) {
        IdLen = ( additional_frame_id_length_minus_1 +
                  delta_frame_id_length_minus_2 + 3 )
        CHECK( IdLen <= 16, "IdLen must be less than or equal to 16" )
    }
    if ( reduced_still_picture_hdr ) {
        show_existing_frame = 0
        frame_type = KEY_FRAME
        FrameIsIntra = 1
        show_frame = 1
        showable_frame = 0
    } else {
          show_existing_frame                                                  u(1)
        if ( show_existing_frame == 1 ) {
              frame_to_show_map_idx                                            u(3)
            COVERCROSS(VALUE_FRAME_TO_SHOW_MAP_IDX, frame_to_show_map_idx)
            Stride = RefStride[ frame_to_show_map_idx ]
            if (RefFrameType[ frame_to_show_map_idx ] == KEY_FRAME) {
              CHECK( RefShowableFrame[ frame_to_show_map_idx ] == 1, "Existing frame to show must have had showable_frame == 1")
              CHECK( RefShownCount[ frame_to_show_map_idx ] == 0, "Existing frame to show must not have been shown before")
            }
            // Need to increase shown count for all copies
            for (i = 0; i < NUM_REF_FRAMES; i++) {
                if ( RefValid[i] && RefFrameUID[ i ] == RefFrameUID[ frame_to_show_map_idx ] ) {
                    RefShownCount[ i ] ++
                }
            }
            if ( decoder_model_info_present_flag && !equal_picture_interval ) {
                temporal_point_info( )
            }
            refresh_frame_flags = 0
            if (frame_id_numbers_present_flag) {
                  display_frame_id                                             u(IdLen)
                  CHECK(RefValid[ frame_to_show_map_idx ] == 1, "Frame ID numbers present, but reference frame not valid")
                  CHECK(RefFrameId[ frame_to_show_map_idx ] == display_frame_id, "display_frame_id doesn't match reference frame")
            }
            frame_type = RefFrameType[ frame_to_show_map_idx ]
            if ( frame_type == KEY_FRAME ) {
                refresh_frame_flags = (1 << NUM_REF_FRAMES) - 1
            }
            if ( film_grain_params_present ) {
                load_grain_params( frame_to_show_map_idx )
            }
            return 0
        }
        frame_type                                                             u(2)
        if ( IsFirstFrame ) {
            CHECK( frame_type == KEY_FRAME, "The first frame header in a coded video sequence must have frame_type equal to KEY_FRAME" )
        }
        FrameIsIntra = (frame_type == INTRA_ONLY_FRAME ||
                        frame_type == KEY_FRAME)
        show_frame                                                             u(1)
        // Check show_frame==1 removed because conformant decoders also need to be able to start
        // decoding from part way through a valid bitstream
        //if ( IsFirstFrame ) {
        //    CHECK( show_frame == 1, "The first frame header in a coded video sequence must have show_frame equal to 1" )
        //}
        if ( show_frame && decoder_model_info_present_flag && !equal_picture_interval ) {
            temporal_point_info( )
        }
        if ( show_frame ) {
            showable_frame = 0
        } else {
              showable_frame                                                   u(1)
        }
        if ( frame_type == SWITCH_FRAME ||
             ( frame_type == KEY_FRAME && show_frame ) )
            error_resilient_mode = 1
        else
              error_resilient_mode                                             u(1)
    }
    if ( frame_type == KEY_FRAME && show_frame) {
        for ( i = 0; i < NUM_REF_FRAMES; i++ ) {
            //if (show_frame)
                RefValid[ i ] = 0
            RefOrderHint[ i ] = 57 // Random number which should never get used!
            RefFrameId[ i ] = 89 // Random number which should never get used!
        }
        // Place some dummy values in OrderHints
        for ( i = 0; i < REFS_PER_FRAME; i++ ) {
            OrderHints[ LAST_FRAME + i ] = 57 // Random number which should never get used!
        }
        current_frame_id = 57 // Dummy value to stop complaints with PrevFrameId
    }
    disable_cdf_update                                                         u(1)
    if ( seq_force_screen_content_tools == SELECT_SCREEN_CONTENT_TOOLS ) {
        allow_screen_content_tools                                             u(1)
    } else {
        allow_screen_content_tools = seq_force_screen_content_tools
    }
    if ( allow_screen_content_tools ) {
        if ( seq_force_integer_mv == SELECT_INTEGER_MV ) {
            force_integer_mv                                                   u(1)
        } else {
            force_integer_mv = seq_force_integer_mv
        }
    } else {
        force_integer_mv = 0
    }
    if ( FrameIsIntra ) {
        force_integer_mv = 1
    }
    if ( frame_id_numbers_present_flag ) {
        PrevFrameID = IsFirstFrame ? -1 : current_frame_id
          current_frame_id                                                     u(IdLen)
        if (!IsFirstFrame) {
          if ( frame_type != KEY_FRAME || show_frame == 0 ) {
              CHECK( current_frame_id != PrevFrameID , "current_frame_id must not be equal to PrevFrameID" )
              diffFrameID = ( current_frame_id > PrevFrameID ) ? ( current_frame_id - PrevFrameID ) :
                                                                 ( ( 1 << IdLen ) + current_frame_id - PrevFrameID )
              CHECK( diffFrameID < ( 1 << ( IdLen - 1 ) ), "diffFrameID out of range" )
          }
          mark_ref_frames( )
        }
    } else {
      current_frame_id = 0
    }
    if ( frame_type == SWITCH_FRAME )
        frame_size_override_flag = 1
    else if ( reduced_still_picture_hdr )
        frame_size_override_flag = 0
    else
          frame_size_override_flag                                             u(1)
      order_hint                                                               u(OrderHintBits)
    OrderHint = order_hint
    if ( FrameIsIntra || error_resilient_mode ) {
        primary_ref_frame = PRIMARY_REF_NONE
    } else {
          primary_ref_frame                                                    u(3)
    }
    if ( decoder_model_info_present_flag ) {
          buffer_removal_time_present                                         u(1)
        if ( COVERCLASS(1, IMPOSSIBLE, buffer_removal_time_present) ) {
            for ( opNum = 0; opNum <= operating_points_cnt_minus_1; opNum++ ) {
                opPtIdc = operating_point_idc[ opNum ]
                if (opPtIdc != 0)
                    CHECK(obu_extension_flag==1, "If opPtIdc != 0, it is a requirement of bitstream conformance that obu_extension_flag==1")
                if ( decoder_model_present_for_this_op[ opNum ] ) {
                    inTemporalLayer = 0
                    inSpatialLayer = 0
                    if ( opPtIdc != 0) {
                      inTemporalLayer = ( opPtIdc >> temporal_id ) & 1
                      inSpatialLayer = ( opPtIdc >> ( spatial_id + 8 ) ) & 1
                    }
                    if ( opPtIdc == 0 || ( inTemporalLayer && inSpatialLayer ) ) {
                        n = buffer_removal_time_length_minus_1 + 1
                          buffer_removal_delay                                     u(n)
                    }
                }
            }
        }
    }
    allow_high_precision_mv = 0
    use_ref_frame_mvs = 0
    allow_intrabc = 0
    allFrames = (1 << NUM_REF_FRAMES) - 1
    if ( frame_type == SWITCH_FRAME ||
         ( frame_type == KEY_FRAME && show_frame ) ) {
        refresh_frame_flags = allFrames
    } else {
          refresh_frame_flags                               u(8)
        if ( frame_type == INTRA_ONLY_FRAME )
            CHECK( refresh_frame_flags != 0xFF, "If frame_type is equal to INTRA_ONLY_FRAME, it is a requirement of bitstream conformance that refresh_frame_flags is not equal to 0xff")
    }

    if ( !FrameIsIntra || refresh_frame_flags != allFrames ) {
        if ( error_resilient_mode && enable_order_hint ) {
            for ( i = 0; i < NUM_REF_FRAMES; i++) {
                  ref_order_hint[ i ]                       u(OrderHintBits)
                if ( ref_order_hint[ i ] != RefOrderHint[ i ] ) {
                     RefValid[ i ] = 0
                }
            }
        }
    }
    if ( FrameIsIntra ) {
        frame_size( )
        render_size( )
        if ( allow_screen_content_tools && UpscaledWidth == FrameWidth ) {
              allow_intrabc                                 u(1)
        }
    } else {
        if ( !enable_order_hint ) {
          frame_refs_short_signaling = 0
        } else {
          frame_refs_short_signaling                        u(1)
          if ( frame_refs_short_signaling ) {
            last_frame_idx                                  u(3)
            gold_frame_idx                                  u(3)
            COVERCROSS(VALUE_LAST_FRAME_IDX, last_frame_idx)
            COVERCROSS(VALUE_GOLD_FRAME_IDX, gold_frame_idx)
            set_frame_refs()
          }
        }
        for ( i = 0; i < REFS_PER_FRAME; i++ ) {
            if ( !frame_refs_short_signaling ) {
              ref_frame_idx[ i ]                            u(3)
              uint3 RefFrameIdx
              RefFrameIdx = ref_frame_idx[i]
              if (i==0) {
                COVERCROSS(VALUE_REF_FRAME_IDX_0, RefFrameIdx)
              } else if (i==1) {
                COVERCROSS(VALUE_REF_FRAME_IDX_1, RefFrameIdx)
              } else if (i==2) {
                COVERCROSS(VALUE_REF_FRAME_IDX_2, RefFrameIdx)
              } else if (i==3) {
                COVERCROSS(VALUE_REF_FRAME_IDX_3, RefFrameIdx)
              } else if (i==4) {
                COVERCROSS(VALUE_REF_FRAME_IDX_4, RefFrameIdx)
              } else if (i==5) {
                COVERCROSS(VALUE_REF_FRAME_IDX_5, RefFrameIdx)
              } else {
                ASSERT(i==6,"Impossible value for i (RefFrameIdx)")
                COVERCROSS(VALUE_REF_FRAME_IDX_6, RefFrameIdx)
              }
            }
            CHECK(RefBitDepth[ ref_frame_idx[ i ] ] == BitDepth, "Reference frame must match bit depth")
            CHECK(RefSeqProfile[ ref_frame_idx[ i ] ] == seq_profile, "Reference frame must match profile")
            CHECK(RefSubsamplingX[ ref_frame_idx[ i ] ] == subsampling_x, "Reference frame must match subsampling")
            CHECK(RefSubsamplingY[ ref_frame_idx[ i ] ] == subsampling_y, "Reference frame must match subsampling")
            CHECK(RefValid[ ref_frame_idx[ i ] ] == 1, "Reference frame must be valid")
            CHECK(RefColorPrimaries[ ref_frame_idx[ i ] ] == color_primaries && RefTransferCharacteristics[ ref_frame_idx[ i ] ] == transfer_characteristics && RefMatrixCoefficients[ ref_frame_idx[ i ] ] == matrix_coefficients, "Reference frame color space must match")

            if ( frame_id_numbers_present_flag ) {
                n = delta_frame_id_length_minus_2 + 2
                delta_frame_id_minus_1                      u(n)
                DeltaFrameId = delta_frame_id_minus_1 + 1
                expectedFrameId[ i ] = ((current_frame_id + (1 << IdLen) -
                                        DeltaFrameId ) % (1 << IdLen))
            }
        }
        if ( frame_size_override_flag && !error_resilient_mode ) {
            frame_size_with_refs( )
        } else {
            frame_size( )
            render_size( )
        }
        if ( force_integer_mv ) {
            allow_high_precision_mv = 0
        } else {
            allow_high_precision_mv                        u(1)
        }
        read_interpolation_filter( )
        is_motion_mode_switchable                          u(1)
        if ( error_resilient_mode || !enable_ref_frame_mvs ) {
            use_ref_frame_mvs = 0
        } else {
            use_ref_frame_mvs                              u(1)
        }
        for( i = 0; i < REFS_PER_FRAME; i++ ) {
            refFrame = LAST_FRAME + i
            hint = RefOrderHint[ ref_frame_idx[ i ] ]
            OrderHints[ refFrame ] = hint
            if ( !enable_order_hint ) {
                RefFrameSignBias[ refFrame ] = 0
            } else {
                RefFrameSignBias[ refFrame ] = get_relative_dist( hint, OrderHint) > 0
            }
        }
    }
    if ( reduced_still_picture_hdr || disable_cdf_update )
        disable_frame_end_update_cdf = 1
    else
          disable_frame_end_update_cdf                                         u(1)
    if ( primary_ref_frame == PRIMARY_REF_NONE ) {
        init_non_coeff_cdfs( )
        setup_past_independence( )
    } else {
        load_cdfs( ref_frame_idx[ primary_ref_frame ] )
        load_previous( )
    }
    if ( use_ref_frame_mvs == 1 )
        motion_field_estimation( )

    level_coverage_0( )
    tile_info( )
    level_coverage_1( )

    quantization_params( )
    segmentation_params( )
    delta_q_params( )
    delta_lf_params( )
    if ( primary_ref_frame == PRIMARY_REF_NONE ) {
        init_coeff_cdfs( )
    } else {
        load_previous_segment_ids( )
    }
    CodedLossless = 1
    for ( segmentId = 0; segmentId < MAX_SEGMENTS; segmentId++ ) {
        qindex = get_qindex( 1, segmentId )
        LosslessArray[ segmentId ] = qindex == 0 && DeltaQYDc == 0 &&
                                     DeltaQUAc == 0 && DeltaQUDc == 0 &&
                                     DeltaQVAc == 0 && DeltaQVDc == 0
        if ( !LosslessArray[ segmentId ] )
            CodedLossless = 0
        if ( using_qmatrix ) {
            if ( LosslessArray[ segmentId ] ) {
                SegQMLevel[ 0 ][ segmentId ] = 15
                SegQMLevel[ 1 ][ segmentId ] = 15
                SegQMLevel[ 2 ][ segmentId ] = 15
            } else {
                SegQMLevel[ 0 ][ segmentId ] = qm_y
                SegQMLevel[ 1 ][ segmentId ] = qm_u
                SegQMLevel[ 2 ][ segmentId ] = qm_v
            }
        }
    }
    AllLossless = CodedLossless && ( FrameWidth == UpscaledWidth )
    if ( CodedLossless == 1 ) {
      CHECK( delta_q_present == 0, "It is a requirement of bitstream conformance that delta_q_present is equal to 0 when CodedLossless is equal to 1")
    }
    cross_base_q_idx_is_zero = (base_q_idx == 0)
    COVERCROSS(CROSS_LOSSLESS, CodedLossless, AllLossless, cross_base_q_idx_is_zero)
    loop_filter_params( )
    cdef_params( )
    lr_params( )
    read_tx_mode( )
    frame_reference_mode( )
    skip_mode_params( )
    if ( FrameIsIntra ||
         error_resilient_mode ||
         !enable_warped_motion )
        allow_warped_motion = 0
    else
          allow_warped_motion                                                      u(1)
      reduced_tx_set                                                           u(1)
    global_motion_params( )
    film_grain_params( )
    check_valid_profile_level( )
}

get_relative_dist( xa, xb ) {
    if ( !enable_order_hint )
        return 0
    diff = xa - xb
    m = 1 << (OrderHintBits - 1)
    diff = (diff & (m - 1)) - (diff & m)
    return diff
}

load_previous( ) {
    prevFrame = ref_frame_idx[ primary_ref_frame ]
    for (i = LAST_FRAME; i <= ALTREF_FRAME; i++)
        for (j = 0; j < 6; j++)
            PrevGmParams[ i ][ j ] = SavedGmParams[ prevFrame ][ i ][ j ]
    load_loop_filter_params( prevFrame )
    load_segmentation_params( prevFrame )
}

load_previous_segment_ids( ) {
    prevFrame = ref_frame_idx[ primary_ref_frame ]
    if ( segmentation_enabled == 1 &&
         RefMiCols[ prevFrame ] == MiCols &&
         RefMiRows[ prevFrame ] == MiRows )
        for ( row = 0; row < MiRows; row++ )
            for ( col = 0; col < MiCols; col++ )
                PrevSegmentIds[ row ][ col ] = SavedSegmentIds[ prevFrame ][ row ][ col ]
    else
        for ( row = 0; row < MiRows; row++ )
            for ( col = 0; col < MiCols; col++ )
                PrevSegmentIds[ row ][ col ] = 0
}


// Reference Frame Marking Function
mark_ref_frames( ) {
    diffLen = delta_frame_id_length_minus_2 + 2
    for ( i = 0; i < NUM_REF_FRAMES; i++ ) {
        if ( current_frame_id > ( 1 << diffLen ) ) {
            if ( RefFrameId[ i ] > current_frame_id ||
                 RefFrameId[ i ] < ( current_frame_id - ( 1 << diffLen ) ) )
                RefValid[ i ] = 0
        } else {
            if ( RefFrameId[ i ] > current_frame_id &&
                 RefFrameId[ i ] < ( ( 1 << IdLen ) +
                                     current_frame_id -
                                     ( 1 << diffLen ) ) )
                RefValid[ i ] = 0
        }
    }
}

// Frame Size Syntax
frame_size( ) {
    if (frame_size_override_flag) {
        n = frame_width_bits_minus_1 + 1
          frame_width_minus_1                                                  u(n)
        CHECK(frame_width_minus_1 <= max_frame_width_minus_1, "frame_width_minus_1 must be less than or equal to max_frame_width_minus_1")
        n = frame_height_bits_minus_1 + 1
          frame_height_minus_1                                                 u(n)
        CHECK(frame_height_minus_1 <= max_frame_height_minus_1, "frame_height_minus_1 must be less than or equal to max_frame_height_minus_1")
        FrameWidth = frame_width_minus_1 + 1
        FrameHeight = frame_height_minus_1 + 1
    } else {
        FrameWidth = max_frame_width_minus_1 + 1
        FrameHeight = max_frame_height_minus_1 + 1
    }
    ASSERT(FrameWidth <= MAX_WIDTH, "FrameWidth out of range")
    ASSERT(FrameHeight <= MAX_HEIGHT, "FrameHeight out of range")
    if ( FrameIsIntra == 0 ) {
        for (i = 0; i < REFS_PER_FRAME; i++) {
            CHECK( ( 2 * FrameWidth >= RefUpscaledWidth[ ref_frame_idx[ i ] ] &&
                     2 * FrameHeight >= RefFrameHeight[ ref_frame_idx[ i ] ] &&
                         FrameWidth <= 16 * RefUpscaledWidth[ ref_frame_idx[ i ] ] &&
                         FrameHeight <= 16 * RefFrameHeight[ ref_frame_idx[ i ] ] ), "All reference frames must have a valid size" )
        }
    }
    superres_params( )
    compute_image_size( )
}

// Render Size Syntax
render_size( ) {
      render_and_frame_size_different                                          u(1)
    if ( render_and_frame_size_different == 1 ) {
          render_width_minus_1                                                 u(16)
          render_height_minus_1                                                u(16)
        RenderWidth = render_width_minus_1 + 1
        RenderHeight = render_height_minus_1 + 1
    } else {
        RenderWidth = UpscaledWidth
        RenderHeight = FrameHeight
    }
}

// Frame Size with Refs Syntax
frame_size_with_refs( ) {
    for ( i = 0; i < REFS_PER_FRAME; i++ ) {
          found_ref                                                            u(1)
        if ( found_ref == 1 ) {
            UpscaledWidth = RefUpscaledWidth[ ref_frame_idx[ i ] ]
            FrameWidth = UpscaledWidth
            FrameHeight = RefFrameHeight[ ref_frame_idx[ i ] ]
            RenderWidth = RefRenderWidth[ ref_frame_idx[ i ] ]
            RenderHeight = RefRenderHeight[ ref_frame_idx[ i ] ]
            break
        }
    }
    if ( found_ref == 0 ) {
        frame_size( )
        render_size( )
    } else {
        superres_params( )
        compute_image_size( )
    }
    for (i = 0; i < REFS_PER_FRAME; i++) {
        CHECK( ( 2 * FrameWidth >= RefUpscaledWidth[ ref_frame_idx[ i ] ] &&
                 2 * FrameHeight >= RefFrameHeight[ ref_frame_idx[ i ] ] &&
                     FrameWidth <= 16 * RefUpscaledWidth[ ref_frame_idx[ i ] ] &&
                     FrameHeight <= 16 * RefFrameHeight[ ref_frame_idx[ i ] ] ), "All reference frames must have a valid size" )
    }
}

superres_params() {
    if ( enable_superres )
        use_superres                                                           u(1)
    else
        use_superres = 0
    if (use_superres) {
        coded_denom                                                            u(SUPERRES_DENOM_BITS)
        SuperresDenom = coded_denom + SUPERRES_DENOM_MIN
    } else {
        SuperresDenom = SUPERRES_NUM
    }
    COVERCROSS(VALUE_SUPERRES_DENOM, SuperresDenom)
    UpscaledWidth = FrameWidth
    FrameWidth = (UpscaledWidth * SUPERRES_NUM +
                  (SuperresDenom / 2)) / SuperresDenom
    ASSERT(UpscaledWidth <= MAX_WIDTH, "UpscaledWidth out of range")
    ASSERT(FrameWidth <= MAX_WIDTH, "FrameWidth out of range")
}

level_coverage_0 () {
    // level_coverage_0 tracks the coverage that should be collected
    // about a frame before reading tile information. In particular,
    // it must set up maxParamsSmall, which gets read by
    // tile_coverage(), called by tile_info().
#if COVER
    frameWidthIsMin = COVERCLASS((!CLIENT_A && !CLIENT_B && !CLIENT_C), 1, ( FrameWidth == 16 ))
    frameHeightIsMin = COVERCLASS((!CLIENT_A && !CLIENT_B && !CLIENT_C), 1, ( FrameHeight == 16 ))
    // ClientA specific coverage
    frameWidthIs144  = COVERCLASS((!CLIENT_C), 1, FrameWidth == 144)
    frameHeightIs128 = COVERCLASS((!CLIENT_C), 1, FrameHeight == 128)
    frameWidthIs3840  = COVERCLASS((CLIENT_A_LEVEL5 || LEVEL5), 1, FrameWidth == 3840)
    frameHeightIs2160 = COVERCLASS((CLIENT_A_LEVEL5 || LEVEL5), 1, FrameHeight == 2160)

    // Coverage points for level 5.x:
    frameWidthIs8192 = COVERCLASS((LEVEL5 && !CLIENT_A), 1, FrameWidth == 8192)
    frameHeightIs4352 = COVERCLASS((LEVEL5 && !CLIENT_A), 1, FrameHeight == 4352)

    // Coverage that is specifically for non-annex B streams. The
    // weird if/else formulation avoids having to propagate the
    // coverclass stuff explicitly across a !.
    if (COVERCLASS (1, NON_ANNEX_B, isAnnexB)) {
        :pass
    } else {
        // ClientA and ClientB streams are constrained to be strictly
        // more than 16 pixels across.
        NotAnnexBMinW = COVERCLASS ((NON_ANNEX_B && !(CLIENT_A || CLIENT_B || CLIENT_C)),
                                    NON_ANNEX_B,
                                    frameWidthIsMin)
        NotAnnexBMinH = COVERCLASS ((NON_ANNEX_B && !(CLIENT_A || CLIENT_B || CLIENT_C)), NON_ANNEX_B, frameHeightIsMin)

        NotAnnexBLargeW = COVERCLASS ((LEVEL5 && NON_ANNEX_B && !CLIENT_A),
                                      NON_ANNEX_B,
                                      frameWidthIs8192)
        NotAnnexBLargeH = COVERCLASS ((LEVEL5 && NON_ANNEX_B && !CLIENT_A),
                                      NON_ANNEX_B,
                                      frameHeightIs4352)
    }

    // Coverage points for level 6.x:
    // Note that, while width=8K and height=4K are independently possible
    // in level 5.x, 8Kx4K frames require level 6.x
    // Note also that there appear to be several different definitions of "8Kx4K".
    // To line up with the levels in the spec, we pick the largest definition.
    //
    // The frameWidthIs16K and frameHeightIs8K targets aren't visible
    // for ClientB streams because they are both bigger than 8192
    // pixels ("8K" here means 8704, which is more than 8192).
    frameIs8Kx4K =
        COVERCLASS(LEVEL6, 1,
                   COVERCLASS((LEVEL5 && !CLIENT_A), 1,
                              frameWidthIs8192) &&
                   COVERCLASS((LEVEL6 && !CLIENT_A), (LEVEL5 && !CLIENT_A),
                              frameHeightIs4352))

    frameWidthIs16K = COVERCLASS((LEVEL6 && !(CLIENT_B || CLIENT_A || CLIENT_D)), 1,
                                 FrameWidth == 16384)

    frameHeightIs8K = COVERCLASS((LEVEL6 && !(CLIENT_B || CLIENT_A || CLIENT_D)), 1,
                                 FrameHeight == 8704)

    // We can't use the max width and max height simultaneously,
    // so we don't have a coverage point for 16Kx8K.

    // Some coverage points which are relevant to the "max parameters" level.
    // These aren't really spec coverage per se, they're just here so that
    // autoproduct keeps at least one stream hitting each interesting case.
    //
    // Neither the big width nor height are visible for CLIENT_B streams
    // (both are bigger than 8192, the ClientB maximum), but the large area is
    // (because 8192^2 > 16384 * 2176).

    // Largest sizes we'll generate
    frameWidthBeyondLevel63  = COVERCLASS((MAXLEVEL && !(CLIENT_B || CLIENT_D)), 1, FrameWidth > 16384)
    frameHeightBeyondLevel63 = COVERCLASS((MAXLEVEL && !(CLIENT_B || CLIENT_D)), 1, FrameHeight > 8704)
    frameAreaBeyondLevel63 =
        COVERCLASS((MAXLEVEL && !CLIENT_D), 1, FrameWidth*FrameHeight > 16384*2176)

    // Check for streams which use the "max params" level but whose size fits
    // into level 6.3.
    // We save this information so that we can add "many tiles" coverage later
    if (COVERCLASS(MAXLEVEL, (! MAXLEVEL),
                   seq_level_idx[operatingPoint] == 31)) {
      maxWidth = max_frame_width_minus_1 + 1
      maxHeight = max_frame_height_minus_1 + 1

      maxParamsSmall =
      COVERCLASS (MAXLEVEL, (MAXLEVEL && !(CLIENT_B || CLIENT_D)),
                  COVERCLASS (MAXLEVEL, (MAXLEVEL && !(CLIENT_B || CLIENT_D)),
                              maxWidth <= 16384) &&
                  COVERCLASS (MAXLEVEL, (MAXLEVEL && !(CLIENT_B || CLIENT_D)),
                              maxHeight <= 8704) &&
                  maxWidth*maxHeight <= 16384*2176)
    } else {
      maxParamsSmall = 0
    }
#endif
}

level_coverage_1 () {
    // level_coverage_1 tracks the coverage that should be collected
    // about a frame after reading tile information (because it needs
    // TileRows and TileCols)
#if COVER
    // Max # of tiles
    tileRowsIs64 = COVERCLASS(MAXLEVEL, 1, TileRows == 64)
    tileColsIs64 = COVERCLASS(MAXLEVEL, 1, TileCols == 64)
#endif
}

#if COVER
tile_coverage() {
  // Add coverage points so that we keep at least:
  // * One stream where the max size fits into level 6.3, and each frame
  //   fits within the level 6.3 restrictions on #tiles and #tile cols
  // * One stream where the max size fits into level 6.3, but at least
  //   one frame does *not* fit within the level 6.3 restrictions.
  if (COVERCLASS(MAXLEVEL, 1, maxParamsSmall)) {
      manyTileCols = COVERCLASS(MAXLEVEL, MAXLEVEL, TileCols > 16)
      manyTiles    = COVERCLASS(MAXLEVEL, MAXLEVEL, TileRows*TileCols > 128)
  }
}

// Extra coverage point - check that we keep at least one stream which
// demonstrates https://bugs.chromium.org/p/aomedia/issues/detail?id=2074
// This bug requires:
// * The intrabc MV is an odd number of pixels
// * Subsampling is enabled in the relevant direction
// Let the chroma prediction be of size W x H. Then we also need:
// * The (W+1)x(H+1) source pixels used for BILINEAR prediction do *not* overlap
//   the destination block (this is guaranteed by the code above)
// * But the (W+7)x(H+7) source region which would be used by the 8-tap convolve
//   filter *does* overlap the destination block
// This helps to catch cases where the bilinear filter is implemented using the
// regular 8-tap convolve path; in that case, it is possible for the source
// and destination blocks to overlap. This shouldn't cause a bad prediction,
// since the overlap pixels cannot influence the output, but it may cause
// other issues (eg, an assertion failure, in the bug report above)
intrabc_coverage() {
  bw = Block_Width[ MiSize ]
  bh = Block_Height[ MiSize ]
  deltaRow = Mv[ 0 ][ 0 ] >> 3
  deltaCol = Mv[ 0 ][ 1 ] >> 3
  srcTopEdge = MiRow * MI_SIZE + deltaRow
  srcLeftEdge = MiCol * MI_SIZE + deltaCol
  srcBottomEdge = srcTopEdge + bh
  srcRightEdge = srcLeftEdge + bw

  if (COVERCLASS((PROFILE0||PROFILE2), (PROFILE1||PROFILE2),
        COVERCLASS((PROFILE0||PROFILE2), (PROFILE1||PROFILE2), subsampling_x) &&
        COVERCLASS((PROFILE0||PROFILE2), PROFILE2, subsampling_y))) {
    if ((deltaCol & 1) && (deltaRow & 1)) {
      // Bounds of the (W+1)x(H+1) region used for bilinear interpolation
      // These pixels *can* influence the output, so the intrabc sampling
      // constraints should ensure that they can't overlap with the destination.
      chromaRight = Round2(srcRightEdge, 1)
      chromaBottom = Round2(srcBottomEdge, 1)
      ASSERT(chromaRight <= (MiCol*MI_SIZE >> 1) ||
             chromaBottom <= (MiRow*MI_SIZE >> 1),
             "Unexpected intrabc sampling region")
      // Bounds of the (W+7)x(H+7) region used for the generic 8-tap convolve
      // filter - this is an extra 3 pixels on each side compared to the bilinear
      // source region.
      // These extra pixels *cannot* influence the output, so it's okay for
      // them to overlap the destination.
      extChromaRight = chromaRight + 3
      extChromaBottom = chromaBottom + 3
      if (extChromaRight <= (MiCol*MI_SIZE >> 1) ||
          extChromaBottom <= (MiRow*MI_SIZE >> 1)) {
        // Dummy branch for coverage.
        // The interesting case is the "false" branch, which indicates that the
        // extended chroma sampling region overlaps the destination block.
        :pass
      }
    }
  }
}
#endif

// Compute Image Size Function
compute_image_size( ) {
    COVERCLASS (0) {
        MiCols = 2 * ( ( FrameWidth + 7 ) >> 3 )
        MiRows = 2 * ( ( FrameHeight + 7 ) >> 3 )

        // Align stride to superblock size
        mask = MAX_SB_SIZE - 1
        Stride = (UpscaledWidth + mask) & ~mask

        alloc CurrFrame[3][(MiRows*MI_SIZE + mask) & ~mask][Stride]
    }
}

// Interpolation Filter Syntax
read_interpolation_filter( ) {
      is_filter_switchable                                                     u(1)
    if ( is_filter_switchable == 1 ) {
        interpolation_filter = SWITCHABLE
    } else {
          interpolation_filter                                                 u(2)
    }
}

// Loop Filter Params Syntax
loop_filter_params( ) {
      if ( CodedLossless || allow_intrabc ) {
          loop_filter_level[ 0 ] = 0
          loop_filter_level[ 1 ] = 0
          loop_filter_ref_deltas[ INTRA_FRAME ] = 1
          loop_filter_ref_deltas[ LAST_FRAME ] = 0
          loop_filter_ref_deltas[ LAST2_FRAME ] = 0
          loop_filter_ref_deltas[ LAST3_FRAME ] = 0
          loop_filter_ref_deltas[ BWDREF_FRAME ] = 0
          loop_filter_ref_deltas[ GOLDEN_FRAME ] = -1
          loop_filter_ref_deltas[ ALTREF_FRAME ] = -1
          loop_filter_ref_deltas[ ALTREF2_FRAME ] = -1
          for ( i = 0; i < 2; i++ ) {
              loop_filter_mode_deltas[ i ] = 0
          }
      }
      else {
          loop_filter_level[ 0 ]                                                   u(6)
          loop_filter_level[ 1 ]                                                   u(6)
          COVERCROSS(VALUE_LOOP_FILTER_LEVEL0, loop_filter_level[ 0 ])
          COVERCROSS(VALUE_LOOP_FILTER_LEVEL2, loop_filter_level[ 1 ])
          if ( COVERCLASS(1, (PROFILE0 || PROFILE2), NumPlanes > 1) ) {
              if ( loop_filter_level[ 0 ] || loop_filter_level[ 1 ] ) {
                    loop_filter_level[ 2 ]                                             u(6)
                    loop_filter_level[ 3 ]                                             u(6)
                    COVERCROSS(VALUE_LOOP_FILTER_LEVEL2, loop_filter_level[ 2 ])
                    COVERCROSS(VALUE_LOOP_FILTER_LEVEL3, loop_filter_level[ 3 ])
              }
          }
          loop_filter_sharpness                                                    u(3)
          COVERCROSS(VALUE_LOOP_FILTER_SHARPNESS, loop_filter_sharpness)
          loop_filter_delta_enabled                                                u(1)
        if ( loop_filter_delta_enabled == 1 ) {
              loop_filter_delta_update                                             u(1)
            if ( loop_filter_delta_update == 1 ) {
                for ( i = 0; i < TOTAL_REFS_PER_FRAME; i++ ) {
                    update_ref_delta                                               u(1)
                    if ( update_ref_delta == 1 ) {
                        loop_filter_ref_deltas[ i ]                                ae(v)
#if COVER
                        if (i==INTRA_FRAME) {
                            COVERCROSS(VALUE_CABAC_LOOP_FILTER_REF_DELTAS_INTRA, loop_filter_ref_deltas[ i ])
                        } else if (i==LAST_FRAME) {
                            COVERCROSS(VALUE_CABAC_LOOP_FILTER_REF_DELTAS_LAST, loop_filter_ref_deltas[ i ])
                        } else if (i==LAST2_FRAME) {
                            COVERCROSS(VALUE_CABAC_LOOP_FILTER_REF_DELTAS_LAST2, loop_filter_ref_deltas[ i ])
                        } else if (i==LAST3_FRAME) {
                            COVERCROSS(VALUE_CABAC_LOOP_FILTER_REF_DELTAS_LAST3, loop_filter_ref_deltas[ i ])
                        } else if (i==GOLDEN_FRAME) {
                            COVERCROSS(VALUE_CABAC_LOOP_FILTER_REF_DELTAS_GOLDEN, loop_filter_ref_deltas[ i ])
                        } else if (i==BWDREF_FRAME) {
                            COVERCROSS(VALUE_CABAC_LOOP_FILTER_REF_DELTAS_BWDREF, loop_filter_ref_deltas[ i ])
                        } else if (i==ALTREF2_FRAME) {
                            COVERCROSS(VALUE_CABAC_LOOP_FILTER_REF_DELTAS_ALTREF2, loop_filter_ref_deltas[ i ])
                        } else {
                            ASSERT(i==ALTREF_FRAME,"Impossible value for i (update_ref_delta)")
                            COVERCROSS(VALUE_CABAC_LOOP_FILTER_REF_DELTAS_ALTREF, loop_filter_ref_deltas[ i ])
                        }
#endif // COVER
                    }
                }
                for ( i = 0; i < 2; i++ ) {
                    update_mode_delta                                              u(1)
                    if ( update_mode_delta == 1 ) {
                        loop_filter_mode_deltas[ i ]                               ae(v)
#if COVER
                        if (i==0) {
                            COVERCROSS(VALUE_CABAC_LOOP_FILTER_MODE_DELTAS_MODE0, loop_filter_mode_deltas[ i ])
                        } else (i==1) {
                            COVERCROSS(VALUE_CABAC_LOOP_FILTER_MODE_DELTAS_MODE1, loop_filter_mode_deltas[ i ])
                        }
#endif // COVER
                    }
                }
            }
        }
    }
}

// Delta Quantizer Syntax
read_delta_q( ) {
      delta_coded                                                              u(1)
    if ( delta_coded ) {
          delta_q                                                              ae(v)
    } else {
        delta_q = 0
    }
    return delta_q
}

int Segmentation_Feature_Bits[ SEG_LVL_MAX ] = { 8, 6, 6, 6, 6, 3, 0, 0 };
int Segmentation_Feature_Signed[ SEG_LVL_MAX ] = { 1, 1, 1, 1, 1, 0, 0, 0 };
int Segmentation_Feature_Max[ SEG_LVL_MAX ] = {
  255, MAX_LOOP_FILTER, MAX_LOOP_FILTER,
  MAX_LOOP_FILTER, MAX_LOOP_FILTER, 7,
  0, 0 };

// Segmentation Params Syntax
segmentation_params( ) {
    uint1 feature_enabled
    uint3 cross_seg_feature
      segmentation_enabled                                                     u(1)
    if ( segmentation_enabled == 1 ) {
        if ( primary_ref_frame == PRIMARY_REF_NONE ) {
            segmentation_update_map = 1
            segmentation_temporal_update = 0
            segmentation_update_data = 1
        } else {
              segmentation_update_map                                          u(1)
            if ( segmentation_update_map == 1 )
                  segmentation_temporal_update                                 u(1)
              segmentation_update_data                                         u(1)
        }
        if ( segmentation_update_data == 1 ) {
            for ( i = 0; i < MAX_SEGMENTS; i++ ) {
                for ( j = 0; j < SEG_LVL_MAX; j++ ) {
                      feature_enabled                                          u(1)
                    cross_seg_feature = j
                    COVERCROSS(CROSS_SEG_FEATURES, cross_seg_feature, feature_enabled)
                    FeatureEnabled[ i ][ j ] = feature_enabled
                    clippedValue = 0
                    if ( feature_enabled == 1 ) {
                        bitsToRead = Segmentation_Feature_Bits[ j ]
                        limit = Segmentation_Feature_Max[ j ]
                        COVERCROSS(VALUE_SEGMENTATION_FEATURE_BITS, cross_seg_feature)
                        COVERCROSS(VALUE_SEGMENTATION_FEATURE_MAX, cross_seg_feature)
                        COVERCROSS(VALUE_SEGMENTATION_FEATURE_SIGNED, cross_seg_feature)
                        if ( Segmentation_Feature_Signed[ j ] == 1 ) {
                            feature_value_signed                               ae(v)
                            clippedValue = Clip3( -limit, limit, feature_value_signed)
                            // I believe this is the only possible clamping condition
                            hitLowerLimitClamped = (feature_value_signed < clippedValue)
                        } else {
                            feature_value_unsigned                             u(bitsToRead)
                            clippedValue = Clip3( 0, limit, feature_value_unsigned)
                        }
                        // Track value coverage for each feature which has associated data
#if COVER
                        if (j == SEG_LVL_ALT_Q) {
                          int9 v0
                          v0 = clippedValue
                          COVERCROSS(VALUE_SEG_LVL_ALT_Q, v0)
                        }
                        if (j == SEG_LVL_ALT_LF_Y_V) {
                          int7 v1
                          v1 = clippedValue
                          COVERCROSS(VALUE_SEG_LVL_ALT_LF_Y_V, v1)
                        }
                        if (j == SEG_LVL_ALT_LF_Y_H) {
                          int7 v2
                          v2 = clippedValue
                          COVERCROSS(VALUE_SEG_LVL_ALT_LF_Y_H, v2)
                        }
                        if (j == SEG_LVL_ALT_LF_U) {
                          int7 v3
                          v3 = clippedValue
                          COVERCROSS(VALUE_SEG_LVL_ALT_LF_U, v3)
                        }
                        if (j == SEG_LVL_ALT_LF_V) {
                          int7 v4
                          v4 = clippedValue
                          COVERCROSS(VALUE_SEG_LVL_ALT_LF_V, v4)
                        }
                        if (j == SEG_LVL_REF_FRAME) {
                          uint_0_7 v5
                          v5 = clippedValue
                          COVERCROSS(VALUE_SEG_LVL_REF_FRAME, v5)
                        }
#endif // COVER
                    }
                    FeatureData[ i ][ j ] = clippedValue
                }
            }
        }
    } else {
        for ( i = 0; i < MAX_SEGMENTS; i++ ) {
            for ( j = 0; j < SEG_LVL_MAX; j++ ) {
                FeatureEnabled[ i ][ j ] = 0
                FeatureData[ i ][ j ] = 0
            }
        }
    }
    SegIdPreSkip = 0
    LastActiveSegId = 0
    for ( i = 0; i < MAX_SEGMENTS; i++ ) {
        for ( j = 0; j < SEG_LVL_MAX; j++ ) {
            if ( FeatureEnabled[ i ][ j ] ) {
                LastActiveSegId = i
                if ( j >= SEG_LVL_REF_FRAME ) {
                    SegIdPreSkip = 1
                }
            }
        }
    }
}

// Tile Info Syntax
tile_info ( ) {
    sizeSb = 0
    sbCols = use_128x128_superblock ? ( ( MiCols + 31 ) >> 5 ) : ( ( MiCols + 15 ) >> 4 )
    sbRows = use_128x128_superblock ? ( ( MiRows + 31 ) >> 5 ) : ( ( MiRows + 15 ) >> 4 )
    sbShift = use_128x128_superblock ? 5 : 4
    sbSize = sbShift + 2
    maxTileWidthSb = MAX_TILE_WIDTH >> sbSize
    maxTileAreaSb = MAX_TILE_AREA >> ( 2 * sbSize )
    minLog2TileCols = tile_log2(maxTileWidthSb, sbCols)
    maxLog2TileCols = tile_log2(1, Min(sbCols, MAX_TILE_COLS))
    maxLog2TileRows = tile_log2(1, Min(sbRows, MAX_TILE_ROWS))
    minLog2Tiles = Max(minLog2TileCols,
                       tile_log2(maxTileAreaSb, sbRows * sbCols))

      uniform_tile_spacing_flag                                                u(1)
#if VALIDATE_SPEC_TILES
    validate(4444)
#endif
    if ( uniform_tile_spacing_flag ) {
#if VALIDATE_SPEC_TILES
        validate(4445)
#endif
        TileColsLog2 = minLog2TileCols
        while ( TileColsLog2 < maxLog2TileCols ) {
              increment_tile_cols_log2                                         u(1)
            if ( increment_tile_cols_log2 == 1 )
                TileColsLog2++
            else
                break
        }
        tileWidthSb = (sbCols + (1 << TileColsLog2) - 1) >> TileColsLog2
        i = 0
        for (startSb = 0; startSb < sbCols; startSb += tileWidthSb) {
          MiColStarts[ i ] = startSb << sbShift
#if VALIDATE_SPEC_TILES
          validate(MiColStarts[ i ])
#endif
          i += 1
        }
        MiColStarts[i] = MiCols
        TileCols = i

        minLog2TileRows = Max( minLog2Tiles - TileColsLog2, 0)
        TileRowsLog2 = minLog2TileRows
        while ( TileRowsLog2 < maxLog2TileRows ) {
              increment_tile_rows_log2                                         u(1)
            if ( increment_tile_rows_log2 == 1 )
                TileRowsLog2++
            else
                break
        }
        tileHeightSb = (sbRows + (1 << TileRowsLog2) - 1) >> TileRowsLog2
        i = 0
        for (startSb = 0; startSb < sbRows; startSb += tileHeightSb) {
          MiRowStarts[ i ] = startSb << sbShift
#if VALIDATE_SPEC_TILES
          validate(MiRowStarts[ i ])
#endif
          i += 1
        }
        MiRowStarts[i] = MiRows
        TileRows = i

        tileAreaSb = tileWidthSb * tileHeightSb

        CHECK (tileWidthSb <= maxTileWidthSb,
               "tileWidthSb (%d) must be at most maxTileWidthSb (%d)",
               tileWidthSb, maxTileWidthSb)
        CHECK (tileAreaSb <= maxTileAreaSb,
               "tileWidthSb (%d) * tileHeightSb (%d) must be at most maxTileAreaSb (%d)",
               tileWidthSb, tileHeightSb, maxTileAreaSb)

        // Track whether we've seen maximum tile width and tile area for each
        // sbShift.
        COVERCROSS (CROSS_TILE_WIDTH_SB,
                    tileWidthSb == maxTileWidthSb,
                    use_128x128_superblock)
        COVERCROSS (CROSS_TILE_AREA_SB,
                    tileAreaSb == maxTileAreaSb,
                    use_128x128_superblock)

    } else {
#if VALIDATE_SPEC_TILES
        validate(4446)
#endif
        widestTileSb = 0
        startSb = 0
        for ( i = 0; startSb < sbCols; i++ ) {
            MiColStarts[ i ] = startSb << sbShift
#if VALIDATE_SPEC_TILES
            validate(MiColStarts[ i ])
#endif
            maxWidth = Min(sbCols - startSb, maxTileWidthSb)
              width_in_sbs_minus_1                                             ae(v)
            sizeSb = width_in_sbs_minus_1 + 1
            widestTileSb = Max( sizeSb, widestTileSb )
            startSb += sizeSb
        }
        CHECK(startSb == sbCols, "startSb must be equal to sbCols on exit from the loop")
        MiColStarts[i] = MiCols
        TileCols = i
        TileColsLog2 = tile_log2(1, TileCols)

        // Since a tile may be up to 4k x 4k, CLIENT_A streams (which
        // are smaller than that) will never see minLog2Tiles > 0.
        if (COVERCLASS((LEVEL5 && !CLIENT_A), 1, minLog2Tiles > 0))
            maxTileAreaSb = (sbRows * sbCols) >> (minLog2Tiles + 1)
        else
            maxTileAreaSb = sbRows * sbCols

        maxTileHeightSb = Max( maxTileAreaSb / widestTileSb, 1 )

        startSb = 0
        for ( i = 0; startSb < sbRows; i++ ) {
            MiRowStarts[ i ] = startSb << sbShift
#if VALIDATE_SPEC_TILES
            validate(MiRowStarts[ i ])
#endif
            maxHeight = Min(sbRows - startSb, maxTileHeightSb)
              height_in_sbs_minus_1                                            ae(v)
            sizeSb = height_in_sbs_minus_1 + 1
            startSb += sizeSb
        }
        CHECK(startSb == sbRows, "startSb must be equal to sbRows on exit from the loop")
        MiRowStarts[ i ] = MiRows
        TileRows = i
        TileRowsLog2 = tile_log2(1, TileRows)
    }
    CHECK(TileCols <= MAX_TILE_COLS, "TileCols must be less than or equal to MAX_TILE_COLS")
    CHECK(TileRows <= MAX_TILE_ROWS, "TileRows must be less than or equal to MAX_TILE_ROWS")
    if ( TileColsLog2 > 0 || TileRowsLog2 > 0 ) {
          context_update_tile_id                                                  u(TileRowsLog2 + TileColsLog2)
          tile_size_bytes_minus_1                                                  u(2)
        TileSizeBytes = tile_size_bytes_minus_1 + 1
    } else {
        context_update_tile_id = 0
    }
    CHECK(context_update_tile_id < TileCols * TileRows, "context_update_tile_id must be less than TileCols * TileRows.")

#if COVER
    tile_coverage()
#endif
}

// Tile Size Calculation Function
// tile_log2 returns the smallest value for k such that blkSize << k is greater than or equal to target.

tile_log2( blkSize, target) {
  for (k = 0; (blkSize << k) < target; k++) {
  }
  return k
}

// Quantizer Index Delta Parameters Syntax
delta_q_params( ) {
    delta_q_res = 0
    delta_q_present = 0
    if ( base_q_idx > 0 ) {
          delta_q_present                                                      u(1)
    }
    if ( delta_q_present ) {
          delta_q_res                                                          u(2)
    }
}

// Loop Filter Delta Parameters Syntax
delta_lf_params( ) {
    delta_lf_present = 0
    delta_lf_res = 0
    delta_lf_multi = 0
    if ( delta_q_present ) {
        if ( !allow_intrabc )
          delta_lf_present                                                     u(1)
        if ( delta_lf_present ) {
              delta_lf_res                                                     u(2)
              delta_lf_multi                                                   u(1)
        }
    }
}

// CDEF Params Syntax
cdef_params( ) {
    if ( CodedLossless || allow_intrabc ||
         !enable_cdef) {
        cdef_bits = 0
        cdef_y_pri_strength[0] = 0
        cdef_y_sec_strength[0] = 0
        cdef_uv_pri_strength[0] = 0
        cdef_uv_sec_strength[0] = 0
        CdefDamping = 3
    } else {
        cdef_damping_minus_3                                                 f(2)
        CdefDamping = cdef_damping_minus_3 + 3
        cdef_bits                                                            f(2)
        for (i = 0; i < (1 << cdef_bits); i++) {
            cdef_y_pri_strength[i]                                           f(4)
            cdef_y_sec_strength[i]                                           f(2)
            if (cdef_y_sec_strength[i] == 3)
                cdef_y_sec_strength[i] += 1
            if ( COVERCLASS(1, (PROFILE0 || PROFILE2), NumPlanes > 1) ) {
                cdef_uv_pri_strength[i]                                      f(4)
                cdef_uv_sec_strength[i]                                      f(2)
                if (cdef_uv_sec_strength[i] == 3)
                    cdef_uv_sec_strength[i] += 1
            }
        }
    }
}

// TX Mode Syntax
read_tx_mode( ) {
    if ( CodedLossless == 1 ) {
        TxMode = ONLY_4X4
    } else {
          tx_mode_select                                                       u(1)
        if ( tx_mode_select ) {
            TxMode = TX_MODE_SELECT
        } else {
            TxMode = TX_MODE_LARGEST
        }
    }
}

// Skip Mode Params Syntax

skip_mode_params( ) {
    if ( FrameIsIntra || !reference_select || !enable_order_hint ) {
        skipModeAllowed = 0
    } else {
        forwardIdx = -1
        backwardIdx = -1
        forwardHint = -1
        backwardHint = -1
        for( i = 0; i < REFS_PER_FRAME; i++ ) {
            refHint = RefOrderHint[ ref_frame_idx[ i ] ]
            if ( get_relative_dist( refHint, OrderHint ) < 0 ) {
                if ( forwardIdx < 0 || get_relative_dist( refHint, forwardHint) > 0 ) {
                    forwardIdx = i
                    forwardHint = refHint
                }
            } else if ( get_relative_dist( refHint, OrderHint) > 0 ) {
                if ( backwardIdx < 0 || get_relative_dist( refHint, backwardHint) < 0 ) {
                    backwardIdx = i
                    backwardHint = refHint
                }
            }
        }
        if ( forwardIdx < 0 ) {
            skipModeAllowed = 0
        } else if ( backwardIdx >= 0 ) {
            skipModeAllowed = 1
            SkipModeFrame[ 0 ] = LAST_FRAME + Min(forwardIdx, backwardIdx)
            SkipModeFrame[ 1 ] = LAST_FRAME + Max(forwardIdx, backwardIdx)
        } else {
            secondForwardIdx = -1
            secondForwardHint = -1
            for( i = 0; i < REFS_PER_FRAME; i++ ) {
                refHint = RefOrderHint[ ref_frame_idx[ i ] ]
                if ( get_relative_dist( refHint, forwardHint ) < 0 ) {
                    if ( secondForwardIdx < 0 ||
                         get_relative_dist( refHint, secondForwardHint ) > 0 ) {
                        secondForwardIdx = i
                        secondForwardHint = refHint
                    }
                }
            }
            if ( secondForwardIdx < 0 ) {
                skipModeAllowed = 0
            } else {
                skipModeAllowed = 1
                SkipModeFrame[ 0 ] = LAST_FRAME + Min(forwardIdx, secondForwardIdx)
                SkipModeFrame[ 1 ] = LAST_FRAME + Max(forwardIdx, secondForwardIdx)
            }
        }
    }
    if ( skipModeAllowed ) {
        skip_mode_present                                                    u(1)
    } else {
        skip_mode_present = 0
    }
}

// Frame Reference Mode Function
frame_reference_mode( ) {
    if ( FrameIsIntra ) {
        reference_select = 0
    } else {
          reference_select                                                     u(1)
    }
}

// Global Motion Params Syntax
global_motion_params( ) {
    for ( ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++ ) {
        GmType[ ref ] = IDENTITY
        for ( i = 0; i < 6; i++ ) {
            gm_params[ ref ][ i ] = ( ( i % 3 == 2 ) ? 1 << WARPEDMODEL_PREC_BITS : 0 )
        }
    }
    if ( !FrameIsIntra ) {
        for (ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++) {
              is_global                                                            u(1)
            if (is_global) {
                  is_rot_zoom                                                      u(1)
                if (is_rot_zoom) {
                    type = ROTZOOM
                } else {
                      is_translation                                               u(1)
                    type = is_translation ? TRANSLATION : AFFINE
                }
            } else {
                type = IDENTITY
            }
            GmType[ref] = type

            if (type >= ROTZOOM) {
                read_global_param(type,ref,2)
                read_global_param(type,ref,3)
                if (type == AFFINE) {
                    read_global_param(type,ref,4)
                    read_global_param(type,ref,5)
                } else {
                    gm_params[ref][4] = -gm_params[ref][3]
                    gm_params[ref][5] = gm_params[ref][2]
                }
            }
            if (type >= TRANSLATION) {
#if VALIDATE_SPEC_GM_PARAM
                validate(222000)
#endif
                read_global_param(type,ref,0)
                read_global_param(type,ref,1)
            }
        }
    }
}

// Global Param Syntax
read_global_param( type, ref, idx ) {
    absBits = GM_ABS_ALPHA_BITS
    precBits = GM_ALPHA_PREC_BITS
    if (idx < 2) {
        if (type == TRANSLATION) {
            absBits = GM_ABS_TRANS_ONLY_BITS - !allow_high_precision_mv
            precBits = GM_TRANS_ONLY_PREC_BITS - !allow_high_precision_mv
        } else {
            absBits = GM_ABS_TRANS_BITS
            precBits = GM_TRANS_PREC_BITS
        }
    }
    precDiff = WARPEDMODEL_PREC_BITS - precBits
    round = (idx % 3) == 2 ? (1 << WARPEDMODEL_PREC_BITS) : 0
    sub = (idx % 3) == 2 ? (1 << precBits) : 0
    mx = (1 << absBits)
    r = (PrevGmParams[ref][idx] >> precDiff) - sub
    gmParamsPreShift = decode_signed_subexp_with_ref( -mx, mx + 1, r )
    gm_params[ref][idx] = (gmParamsPreShift << precDiff) + round
    COVERCROSS(VALUE_GM_PARAMS_PRE_SHIFT, gmParamsPreShift)
#if VALIDATE_SPEC_GM_PARAM
    if (idx<2) {
      validate(precDiff)
      validate(r)
      validate(gm_params[ref][idx])
    }
#endif
}

// Decode Signed Subexp With Ref Syntax
decode_signed_subexp_with_ref( low, high, r ) {
    x = decode_unsigned_subexp_with_ref(high - low, r - low)
    return x + low
}

// Decode Unsigned Subexp With Ref Syntax
decode_unsigned_subexp_with_ref( mx, r ) {
    v = decode_subexp( mx )
    if ((r << 1) <= mx) {
        return inverse_recenter(r, v)
    } else {
        return mx - 1 - inverse_recenter(mx - 1 - r, v)
    }
}

// Decode Subexp Syntax
decode_subexp( numSyms ) {
    i = 0
    mk = 0
    k = 3
    while (1) {
        b2 = i ? k + i - 1 : k
        a = 1 << b2
        if (numSyms <= mk + 3 * a) {
              subexp_final_bits                                                ae(v)
            return subexp_final_bits + mk
        } else {
              subexp_more_bits                                                 u(1)
            if (subexp_more_bits) {
               i++
               mk += a
            } else {
                 subexp_bits                                                   u(b2)
               return subexp_bits + mk
            }
        }
    }
}

// Inverse Recenter Syntax
inverse_recenter( r, v ) {
  if (v > 2 * r)
    return v
  else if (v & 1)
    return r - ((v + 1) >> 1)
  else
    return r + (v >> 1)
}

// Film Grain Params Syntax
film_grain_params( ) {
    reset_grain_params() // TODO should this be in spec?  Placed here so saving uses initialized values
    if ( !film_grain_params_present ||
         ( !show_frame && !showable_frame ) ) {
        reset_grain_params()
        return 0
    }
      apply_grain                                                              u(1)
    if ( !apply_grain ) {
        reset_grain_params()
        return 0
    }
      grain_seed                                                               u(16)
    if ( frame_type == INTER_FRAME )
          update_grain                                                         u(1)
    else
        update_grain = 1
    if ( !update_grain ) {
          film_grain_params_ref_idx                                            u(3)
        j = 0
        while (1) {
            CHECK( j < REFS_PER_FRAME, "It is a requirement of bitstream conformance that film_grain_params_ref_idx is equal to ref_frame_idx[ j ] for some value of j in the range 0 to REFS_PER_FRAME - 1")
            ASSERT( j < REFS_PER_FRAME, "Break infinite loop" )
            if ( film_grain_params_ref_idx == ref_frame_idx[ j ] ) {
                break
            }
            j++
        }
        tempGrainSeed = grain_seed
        load_grain_params( film_grain_params_ref_idx )
        grain_seed = tempGrainSeed
        return 0
    }
      num_y_points                                                             u(4)
      CHECK( num_y_points <= 14, "It is a requirement of bitstream conformance that num_y_points is less than or equal to 14")
    for ( i = 0; i < num_y_points; i++ ) {
          point_y_value[ i ]                                                   u(8)
          if ( i > 0 ) {
              CHECK( point_y_value[ i ] > point_y_value[ i - 1 ], "It is a requirement of bitstream conformance that point_y_value[ i ] is greater than point_y_value[ i - 1 ]")
          }
          point_y_scaling[ i ]                                                 u(8)
    }
    if ( COVERCLASS((PROFILE0 || PROFILE2), 1, mono_chrome) ) {
        chroma_scaling_from_luma = 0
    } else {
        chroma_scaling_from_luma                                               u(1)
    }
    if ( COVERCLASS((PROFILE0 || PROFILE2), 1, mono_chrome) || chroma_scaling_from_luma ||
         ( COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2),
           COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2),
           COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) == 1) &&
           COVERCLASS((PROFILE0 || PROFILE2), PROFILE2, COVERCLASS((PROFILE0 || PROFILE2), PROFILE2, subsampling_y) == 1)) &&
           COVERCLASS((PROFILE0 || PROFILE2), num_y_points == 0)) )
       ) {
        num_cb_points = 0
        num_cr_points = 0
    } else {
        num_cb_points                                                          u(4)
        CHECK( num_cb_points <= 10, "It is a requirement of bitstream conformance that num_cb_points is less than or equal to 10")
        for ( i = 0; i < num_cb_points; i++ ) {
            point_cb_value[ i ]                                                u(8)
            if ( i > 0 ) {
                CHECK( point_cb_value[ i ] > point_cb_value[ i - 1 ], "It is a requirement of bitstream conformance that point_cb_value[ i ] is greater than point_cb_value[ i - 1 ]")
            }
            point_cb_scaling[ i ]                                              u(8)
        }
        num_cr_points                                                          u(4)
        CHECK( num_cr_points <= 10, "It is a requirement of bitstream conformance that num_cr_points is less than or equal to 10")
        for ( i = 0; i < num_cr_points; i++ ) {
            point_cr_value[ i ]                                                u(8)
            if ( i > 0 ) {
                CHECK( point_cr_value[ i ] > point_cr_value[ i - 1 ], "It is a requirement of bitstream conformance that point_cr_value[ i ] is greater than point_cr_value[ i - 1 ]")
            }
            point_cr_scaling[ i ]                                              u(8)
        }
    }
    if ( COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x == 1) && COVERCLASS((PROFILE0 || PROFILE2), PROFILE2, subsampling_y == 1)) && COVERCLASS((PROFILE0 || PROFILE2), num_cb_points == 0 ) ) )
        CHECK( num_cr_points == 0, "If subsampling_x is equal to 1 and subsampling_y is equal to 1 and num_cb_points is equal to 0, it is a requirement of bitstream conformance that num_cr_points is equal to 0.")

    if ( COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x == 1) && COVERCLASS((PROFILE0 || PROFILE2), PROFILE2, subsampling_y == 1)) && COVERCLASS((PROFILE0 || PROFILE2), num_cb_points != 0 ) ) )
        CHECK( num_cr_points != 0, "If subsampling_x is equal to 1 and subsampling_y is equal to 1 and num_cb_points is not equal to 0, it is a requirement of bitstream conformance that num_cr_points is not equal to 0.")

    uint1 num_y_points_nz
    uint1 num_cb_points_nz
    uint1 num_cr_points_nz
    num_y_points_nz = (num_y_points != 0)
    num_cb_points_nz = (num_cb_points != 0)
    num_cr_points_nz = (num_cr_points != 0)
    COVERCROSS(CROSS_FILM_GRAIN_POINTS, num_cb_points_nz, num_cr_points_nz, num_y_points_nz)

    grain_scaling_minus_8                                                      u(2)
    ar_coeff_lag                                                               u(2)
    numPosLuma = 2 * ar_coeff_lag * ( ar_coeff_lag + 1 )
    if (num_y_points) {
        numPosChroma = numPosLuma + 1
        for ( i = 0; i < numPosLuma; i++ )
            ar_coeffs_y_plus_128[ i ]                                          u(8)
    } else {
        numPosChroma = numPosLuma
    }
    if ( chroma_scaling_from_luma || num_cb_points ) {
        for ( i = 0; i < numPosChroma; i++ )
              ar_coeffs_cb_plus_128[ i ]                                       u(8)
    }
    if ( chroma_scaling_from_luma || num_cr_points ) {
        for ( i = 0; i < numPosChroma; i++ )
              ar_coeffs_cr_plus_128[ i ]                                       u(8)
    }
      ar_coeff_shift_minus_6                                                   u(2)
      grain_scale_shift                                                        u(2)

    if ( num_cb_points ) {
          cb_mult                                                              u(8)
          cb_luma_mult                                                         u(8)
          cb_offset                                                            u(9)
    }
    if ( num_cr_points ) {
          cr_mult                                                              u(8)
          cr_luma_mult                                                         u(8)
          cr_offset                                                            u(9)
    }
      overlap_flag                                                             u(1)
      clip_to_restricted_range                                                 u(1)
}

reset_grain_params( ) {
      apply_grain = 0
      grain_seed = 0
          update_grain = 0
          film_grain_params_ref_idx = 0
      num_y_points = 0
    for ( i = 0; i < 16; i++ ) {
          point_y_value[ i ] = 0
          point_y_scaling[ i ] = 0
    }
        chroma_scaling_from_luma = 0
        num_cb_points = 0
        for ( i = 0; i < 16; i++ ) {
            point_cb_value[ i ] = 0
            point_cb_scaling[ i ] = 0
        }
        num_cr_points = 0
        for ( i = 0; i < 16; i++ ) {
            point_cr_value[ i ] = 0
            point_cr_scaling[ i ] = 0
        }
    grain_scaling_minus_8 = 0
    ar_coeff_lag = 0
        for ( i = 0; i < 24; i++ )
            ar_coeffs_y_plus_128[ i ] = 0
        for ( i = 0; i < 25; i++ )
              ar_coeffs_cb_plus_128[ i ] = 0
        for ( i = 0; i < 25; i++ )
              ar_coeffs_cr_plus_128[ i ] = 0
      ar_coeff_shift_minus_6 = 0
      grain_scale_shift = 0

          cb_mult = 0
          cb_luma_mult = 0
          cb_offset = 0
          cr_mult = 0
          cr_luma_mult = 0
          cr_offset = 0
      overlap_flag = 0
      clip_to_restricted_range = 0
}

save_grain_params(idx) {
    SavedApplyGrain[ idx ] = apply_grain
    SavedGrainSeed[ idx ] = grain_seed
    SavedNumYPoints[ idx ] = num_y_points
    for ( i = 0; i < num_y_points; i++ ) {
          SavedPointYValue[ idx ][ i ] = point_y_value[ i ]
          SavedPointYScaling[ idx ][ i ] = point_y_scaling[ i ]
    }
        SavedChromaScalingFromLuma[ idx ] = chroma_scaling_from_luma
        SavedNumCbPoints          [ idx ] = num_cb_points
        for ( i = 0; i < num_cb_points; i++ ) {
            SavedPointCbValue  [ idx ][ i ] = point_cb_value[ i ]
            SavedPointCbScaling[ idx ][ i ] = point_cb_scaling[ i ]
        }
        SavedNumCrPoints[ idx ] = num_cr_points
        for ( i = 0; i < num_cr_points; i++ ) {
            SavedPointCrValue  [ idx ][ i ] = point_cr_value[ i ]
            SavedPointCrScaling[ idx ][ i ] = point_cr_scaling[ i ]
        }
    SavedGrainScalingMinus8[ idx ] = grain_scaling_minus_8
    SavedArCoeffLag[ idx ] = ar_coeff_lag
        for ( i = 0; i < 24; i++ )
            SavedArCoeffsYPlus128[ idx ][ i ] = ar_coeffs_y_plus_128[ i ]
        for ( i = 0; i < 25; i++ )
              SavedArCoeffsCbPlus128[ idx ][ i ] = ar_coeffs_cb_plus_128[ i ]
        for ( i = 0; i < 25; i++ )
              SavedArCoeffsCrPlus128[ idx ][ i ] = ar_coeffs_cr_plus_128[ i ]
      SavedArCoeffShiftMinus6[ idx ] = ar_coeff_shift_minus_6
      SavedGrainScaleShift[ idx ] = grain_scale_shift

          SavedCbMult[ idx ]     = cb_mult
          SavedCbLumaMult[ idx ] = cb_luma_mult
          SavedCbOffset[ idx ]   = cb_offset
          SavedCrMult[ idx ]     = cr_mult
          SavedCrLumaMult[ idx ] = cr_luma_mult
          SavedCrOffset[ idx ]   = cr_offset
      SavedOverlapFlag[ idx ]    = overlap_flag
      SavedClipToRestrictedRange[ idx ] = clip_to_restricted_range
}
load_grain_params(idx) {
    apply_grain = SavedApplyGrain[ idx ]
    grain_seed = SavedGrainSeed[ idx ]
    num_y_points = SavedNumYPoints[ idx ]
    for ( i = 0; i < num_y_points; i++ ) {
          point_y_value[ i ] = SavedPointYValue[ idx ][ i ]
          point_y_scaling[ i ] = SavedPointYScaling[ idx ][ i ]
    }
        chroma_scaling_from_luma = SavedChromaScalingFromLuma[ idx ]
        num_cb_points = SavedNumCbPoints          [ idx ]
        for ( i = 0; i < num_cb_points; i++ ) {
            point_cb_value[ i ] = SavedPointCbValue  [ idx ][ i ]
            point_cb_scaling[ i ] = SavedPointCbScaling[ idx ][ i ]
        }
        num_cr_points = SavedNumCrPoints[ idx ]
        for ( i = 0; i < num_cr_points; i++ ) {
            point_cr_value[ i ] = SavedPointCrValue  [ idx ][ i ]
            point_cr_scaling[ i ] = SavedPointCrScaling[ idx ][ i ]
        }
    grain_scaling_minus_8 = SavedGrainScalingMinus8[ idx ]
    ar_coeff_lag = SavedArCoeffLag[ idx ]
        for ( i = 0; i < 24; i++ )
            ar_coeffs_y_plus_128[ i ] = SavedArCoeffsYPlus128[ idx ][ i ]
        for ( i = 0; i < 25; i++ )
              ar_coeffs_cb_plus_128[ i ] = SavedArCoeffsCbPlus128[ idx ][ i ]
        for ( i = 0; i < 25; i++ )
              ar_coeffs_cr_plus_128[ i ] = SavedArCoeffsCrPlus128[ idx ][ i ]
      ar_coeff_shift_minus_6 = SavedArCoeffShiftMinus6[ idx ]
      grain_scale_shift = SavedGrainScaleShift[ idx ]

          cb_mult = SavedCbMult[ idx ]
          cb_luma_mult = SavedCbLumaMult[ idx ]
          cb_offset = SavedCbOffset[ idx ]
          cr_mult = SavedCrMult[ idx ]
          cr_luma_mult = SavedCrLumaMult[ idx ]
          cr_offset = SavedCrOffset[ idx ]
      overlap_flag = SavedOverlapFlag[ idx ]
      clip_to_restricted_range = SavedClipToRestrictedRange[ idx ]
}

// Temporal Point Info Syntax
temporal_point_info( ) {
    n = frame_presentation_time_length_minus_1 + 1
      frame_presentation_time                                                    u(n)
}

// Frame OBU Syntax
frame_obu( sz ) {
    startBitPos = get_position( )
    frame_header_obu( )
    CHECK( show_existing_frame == 0, "If obu_type is equal to OBU_FRAME, it is a requirement of bitstream conformance that show_existing_frame is equal to 0.")

    byte_alignment( )
    endBitPos = get_position( )
    headerBytes = (endBitPos - startBitPos) / 8
    sz -= headerBytes
    tile_group_obu( sz )
}

// General tile list OBU syntax
tile_list_obu( ) COVERCLASS(LARGE_SCALE_TILES) {

#if !COVER
    // This frame shouldn't contribute to compressed size or luma
    // sample counts for this TU, because large scale tile frames have
    // different rules.
    count_frame_size = 0
#endif

    prevTileWidth = -1
    output_frame_width_in_tiles_minus_1                                        u(8)
    output_frame_height_in_tiles_minus_1                                       u(8)
    tile_count_minus_1                                                         u(16)
    CHECK( tile_count_minus_1 <= 511, "It is a requirement of bitstream conformance that tile_count_minus_1 is less than or equal to 511")
    for ( tile = 0; tile <= tile_count_minus_1; tile++ ) {
        tile_list_entry( )
        if ( COVERCLASS(LARGE_SCALE_TILES, IMPOSSIBLE, num_large_scale_tile_anchor_frames > 0) ) {
            last = ref_frame_idx[ 0 ]
            RefStride[ last ] = Stride

            for (plane = 0; plane < NumPlanes; plane++) {
                if (plane == 0) {
                    for (y = 0; y < AnchorHeight[ anchor_frame_idx ]; y++) {
                        for (x = 0; x < AnchorWidth[ anchor_frame_idx ]; x++) {
                            FrameStore[ last ][ 0 ][ y ][ x ] = AnchorY[ anchor_frame_idx ][ y ][ x ]
                        }
                    }
                } else {
                    for (y = 0; y < ((AnchorHeight[ anchor_frame_idx ] + COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_y)) >> COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_y)); y++) {
                        for (x = 0; x < ((AnchorWidth[ anchor_frame_idx ] + COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_x)) >> COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_x)); x++) {
                            if ( plane == 1 ) {
                                FrameStore[ last ][ 1 ][ y ][ x ] = AnchorU[ anchor_frame_idx ][ y ][ x ]
                            } else {
                                FrameStore[ last ][ 2 ][ y ][ x ] = AnchorV[ anchor_frame_idx ][ y ][ x ]
                            }
                        }
                    }
                }
            }
            RefValid[ last ] = 1
            RefUpscaledWidth[ last ] = UpscaledWidth
            RefFrameWidth[ last ] = FrameWidth
            RefFrameHeight[ last ] = FrameHeight
            RefMiCols[ last ] = MiCols
            RefMiRows[ last ] = MiRows
            RefSubsamplingX[ last ] = COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_x)
            RefSubsamplingY[ last ] = COVERCLASS((LARGE_SCALE_TILES && (PROFILE0 || PROFILE2)), (LARGE_SCALE_TILES && (PROFILE1 || PROFILE2)), subsampling_y)
            RefBitDepth[ last ] = BitDepth

            decode_tile_wrapper( tile_data_size_minus_1 + 1, anchor_tile_row, anchor_tile_col )

            output_tile_list( tile )

            CHECK( enable_superres == 0,                              "large_scale_tile: enable_superres must be equal to 0")
            CHECK( enable_order_hint == 0,                            "large_scale_tile: enable_order_hint must be equal to 0")
            CHECK( still_picture == 0,                                "large_scale_tile: still_picture must be equal to 0")
            CHECK( film_grain_params_present == 0,                    "large_scale_tile: film_grain_params_present must be equal to 0")
            CHECK( timing_info_present_flag == 0,                     "large_scale_tile: timing_info_present_flag must be equal to 0")
            CHECK( decoder_model_info_present_flag == 0,              "large_scale_tile: decoder_model_info_present_flag must be equal to 0")
            CHECK( initial_display_delay_present_flag == 0,           "large_scale_tile: initial_display_delay_present_flag must be equal to 0")
            CHECK( enable_restoration == 0,                           "large_scale_tile: enable_restoration must be equal to 0")
            CHECK( enable_cdef == 0,                                  "large_scale_tile: enable_cdef must be equal to 0")
            CHECK( mono_chrome == 0,                                  "large_scale_tile: mono_chrome must be equal to 0")
            CHECK( FrameWidth == MiCols * MI_SIZE,                    "large_scale_tile: FrameWidth must be equal to MiCols * MI_SIZE")
            CHECK( FrameHeight == MiRows * MI_SIZE,                   "large_scale_tile: FrameHeight must be equal to MiRows * MI_SIZE")
            CHECK( show_existing_frame == 0,                          "large_scale_tile: show_existing_frame must be equal to 0")
            CHECK( frame_type == INTER_FRAME,                         "large_scale_tile: frame_type must be equal to INTER_FRAME")
            CHECK( show_frame == 1,                                   "large_scale_tile: show_frame must be equal to 1")
            CHECK( error_resilient_mode == 0,                         "large_scale_tile: error_resilient_mode must be equal to 0")
            CHECK( disable_cdf_update == 1,                           "large_scale_tile: disable_cdf_update must be equal to 1")
            CHECK( disable_frame_end_update_cdf == 1,                 "large_scale_tile: disable_frame_end_update_cdf must be equal to 1")
            CHECK( delta_lf_present == 0,                             "large_scale_tile: delta_lf_present must be equal to 0")
            CHECK( delta_q_present == 0,                              "large_scale_tile: delta_q_present must be equal to 0")
            CHECK( frame_size_override_flag == 0,                     "large_scale_tile: frame_size_override_flag must be equal to 0")
            CHECK( refresh_frame_flags == 0,                          "large_scale_tile: refresh_frame_flags must be equal to 0")
            CHECK( use_ref_frame_mvs == 0,                            "large_scale_tile: use_ref_frame_mvs must be equal to 0")
            CHECK(! (segmentation_enabled && segmentation_update_map &&
                     segmentation_temporal_update != 0),
                  "large_scale_tile: segmentation_temporal_update must be equal to 0")
            CHECK( reference_select == 0,                             "large_scale_tile: reference_select must be equal to 0")
            CHECK( loop_filter_level[ 0 ] == 0,                       "large_scale_tile: loop_filter_level[ 0 ] must be equal to 0")
            CHECK( loop_filter_level[ 1 ] == 0,                       "large_scale_tile: loop_filter_level[ 1 ] must be equal to 0")
            CHECK( TileHeight == (use_128x128_superblock ? 128 : 64), "large_scale_tile: TileHeight must be equal to (use_128x128_superblock ? 128 : 64)")
            CHECK( TileWidth % TileHeight == 0,                       "large_scale_tile: TileWidth must be an integer multiple of TileHeight")
            if ( tile > 0 )
                CHECK( TileWidth == prevTileWidth,                    "large_scale_tile: TileWidth must be the same for all tiles")
            CHECK( tile_count_minus_1 + 1 <= (output_frame_width_in_tiles_minus_1 + 1) * (output_frame_height_in_tiles_minus_1 + 1), "large_scale_tile: tile_count_minus_1 + 1 must be less than or equal to (output_frame_width_in_tiles_minus_1 + 1) * (output_frame_height_in_tiles_minus_1 + 1)")
        }
        prevTileWidth = TileWidth
    }
}

// Tile list entry syntax
tile_list_entry( ) {
    anchor_frame_idx                                                           u(8)
    CHECK( anchor_frame_idx <= 127, "It is a requirement of bitstream conformance that anchor_frame_idx is less than or equal to 127")
    anchor_tile_row                                                            u(8)
    CHECK( anchor_tile_row < TileRows, "It is a requirement of bitstream conformance that anchor_tile_row is less than TileRows")
    anchor_tile_col                                                            u(8)
    CHECK( anchor_tile_col < TileCols, "It is a requirement of bitstream conformance that anchor_tile_col is less than TileCols")
    tile_data_size_minus_1                                                     u(16)
    //N = 8 * (tile_data_size_minus_1 + 1)
    //coded_tile_data                                                            u(N)
}

// Tile Group OBU Syntax
tile_group_obu( sz ) {
    NumTiles = TileCols * TileRows
    startBitPos = get_position( )
    tile_start_and_end_present_flag = 0
    if ( NumTiles > 1 ) {
          tile_start_and_end_present_flag                                      u(1)
          if ( obu_type == OBU_FRAME )
              CHECK( tile_start_and_end_present_flag == 0, "If obu_type is equal to OBU_FRAME, it is a requirement of bitstream conformance that the value of tile_start_and_end_present_flag is equal to 0")
    }
    if ( NumTiles == 1 || !tile_start_and_end_present_flag ) {
        tg_start = 0
        tg_end = NumTiles - 1
    } else {
        tileBits = TileColsLog2 + TileRowsLog2
          tg_start                                                             u(tileBits)
        CHECK(tg_start == TileNum, "tg_start must be equal to TileNum at the beginning of a tile group")
          tg_end                                                               u(tileBits)
        CHECK(tg_end >= tg_start, "tg_end must be greater than or equal to tg_start")
    }
    byte_alignment( )
    endBitPos = get_position( )
    headerBytes = (endBitPos - startBitPos) / 8
    sz -= headerBytes

    for ( TileNum = tg_start; TileNum <= tg_end; TileNum++ ) {
        tileRow = TileNum / TileCols
        tileCol = TileNum % TileCols
        lastTile = TileNum == tg_end
        if ( lastTile ) {
            tileSize = sz
        } else {
              tile_size_minus_1                                                  ae(v)
            tileSize = tile_size_minus_1 + 1
            sz -= tileSize + TileSizeBytes
        }
        decode_tile_wrapper( tileSize, tileRow, tileCol )
    }
    TileGroupCount++
    if (tg_end == NumTiles - 1) {
        if ( !disable_frame_end_update_cdf ) {
            frame_end_update_cdf( )
        }
        decode_frame_wrapup( )
        SeenFrameHeader = 0
    }
}

// Decode Partition Function
decode_partition( r, c, bSize ) {
    uint_0_9 partition
    if ( r >= MiRows || c >= MiCols )
        return 0
    AvailU = is_inside( r - 1, c )
    AvailL = is_inside( r, c - 1 )
    num4x4 = Num_4x4_Blocks_Wide[ bSize ]
    halfBlock4x4 = num4x4 >> 1
    quarterBlock4x4 = halfBlock4x4 >> 1
    hasRows = ( r + halfBlock4x4 ) < MiRows
    hasCols = ( c + halfBlock4x4 ) < MiCols
    if (bSize < BLOCK_8X8) {
        partition = PARTITION_NONE
      } else if ( COVERCLASS(1, (LEVEL5 || !CLIENT_C), COVERCLASS(1, (LEVEL5 || !CLIENT_C), hasRows) && COVERCLASS(1, (LEVEL5 || !CLIENT_C),hasCols) ) ) {
          partition                                                            ae(v)
    } else if ( COVERCLASS((LEVEL5 || !CLIENT_C), (LEVEL5 || !CLIENT_C), hasCols) ) {
        uint1 split_or_horz
          split_or_horz                                                        ae(v)
        COVERCROSS(VALUE_CABAC_SPLIT_OR_HORZ, split_or_horz)
        partition = split_or_horz ? PARTITION_SPLIT : PARTITION_HORZ
    } else if ( COVERCLASS((LEVEL5 || !CLIENT_C), (LEVEL5 || !CLIENT_C), hasRows ) ) {
        uint1 split_or_vert
          split_or_vert                                                        ae(v)
        COVERCROSS(VALUE_CABAC_SPLIT_OR_VERT, split_or_vert)
        partition = split_or_vert ? PARTITION_SPLIT : PARTITION_VERT
    } else {
        partition = PARTITION_SPLIT
    }
#if VALIDATE_SPEC_STRUCTURE
    validate(40000)
    validate(partition)
#endif
    subSize = Partition_Subsize[ partition ][ bSize ]
    CHECK(get_plane_residual_size( subSize, 1 ) != BLOCK_INVALID, "Invalid subSize value")
    splitSize = Partition_Subsize[ PARTITION_SPLIT ][ bSize ]
#if COVER
    uint_0_21 partition_bSize
    partition_bSize = bSize
    COVERCROSS(CROSS_PARTITION_SUBSIZE, partition, partition_bSize)
#endif // COVER
    MiddleVertPartition = 0
    if ( partition == PARTITION_NONE ) {
        decode_block( r, c, subSize )
    } else if ( partition == PARTITION_HORZ ) {
        decode_block( r, c, subSize )
        if ( hasRows )
            decode_block( r + halfBlock4x4, c, subSize )
    } else if ( partition == PARTITION_VERT ) {
        decode_block( r, c, subSize )
        if ( hasCols )
            decode_block( r, c + halfBlock4x4, subSize )
    } else if ( partition == PARTITION_SPLIT ) {
        decode_partition( r, c, subSize )
        decode_partition( r, c + halfBlock4x4, subSize )
        decode_partition( r + halfBlock4x4, c, subSize )
        decode_partition( r + halfBlock4x4, c + halfBlock4x4, subSize )
    } else if ( partition == PARTITION_HORZ_A ) {
        decode_block( r, c, splitSize )
        decode_block( r, c + halfBlock4x4, splitSize )
        decode_block( r + halfBlock4x4, c, subSize )
    } else if ( partition == PARTITION_HORZ_B ) {
        decode_block( r, c, subSize )
        decode_block( r + halfBlock4x4, c, splitSize )
        decode_block( r + halfBlock4x4, c + halfBlock4x4, splitSize )
    } else if ( partition == PARTITION_VERT_A ) {
        decode_block( r, c, splitSize )
        decode_block( r + halfBlock4x4, c, splitSize )
        decode_block( r, c + halfBlock4x4, subSize )
    } else if ( partition == PARTITION_VERT_B ) {
        decode_block( r, c, subSize )
        decode_block( r, c + halfBlock4x4, splitSize )
        decode_block( r + halfBlock4x4, c + halfBlock4x4, splitSize )
    } else if ( partition == PARTITION_HORZ_4 ) {
        decode_block( r + quarterBlock4x4 * 0, c, subSize )
        decode_block( r + quarterBlock4x4 * 1, c, subSize )
        decode_block( r + quarterBlock4x4 * 2, c, subSize )
        if ( r + quarterBlock4x4 * 3 < MiRows )
            decode_block( r + quarterBlock4x4 * 3, c, subSize )
    } else {
        decode_block( r, c + quarterBlock4x4 * 0, subSize )
        MiddleVertPartition = 0
        decode_block( r, c + quarterBlock4x4 * 1, subSize )
        decode_block( r, c + quarterBlock4x4 * 2, subSize )
        MiddleVertPartition = 0
        if ( c + quarterBlock4x4 * 3 < MiCols )
            decode_block( r, c + quarterBlock4x4 * 3, subSize )
    }
}

decode_tile_wrapper( tileSize, tileRow, tileCol ) {
    MiRowStart = MiRowStarts[ tileRow ]
    MiRowEnd = MiRowStarts[ tileRow + 1 ]
    MiColStart = MiColStarts[ tileCol ]
    MiColEnd = MiColStarts[ tileCol + 1 ]
#if OUTPUT_TILES
    jsh_MiRowStart = MiRowStart
    jsh_MiRowEnd   = MiRowEnd
    jsh_MiColStart = MiColStart
    jsh_MiColEnd   = MiColEnd
    :C printf("tile: %d %d %d %d\n", jsh_MiRowStart, jsh_MiRowEnd, jsh_MiColStart, jsh_MiColEnd);
#endif
    TileWidth = ( MiColEnd - MiColStart ) * MI_SIZE
    TileHeight = ( MiRowEnd - MiRowStart ) * MI_SIZE
    RightMostTile = (MiColEnd == MiCols)
    check_valid_profile_level_per_tile( )
    for (r = MiRowStart; r < MiRowEnd; r++) {
        for (c = MiColStart; c < MiColEnd; c++) {
            RefFramesWritten[ r ][ c ] = 0
        }
    }
    CurrentQIndex = base_q_idx
    init_symbol( tileSize )
    decode_tile( )
    exit_symbol( )
}

int Wiener_Taps_Mid[3] = {  3,  -7,  15 };
int Sgrproj_Xqd_Mid[2] = { -32,  31 };

// Decode Tile Function
decode_tile( ) {
    clear_above_context( )
    for ( i = 0; i < FRAME_LF_COUNT; i++ )
        DeltaLF[ i ] = 0

    for ( plane = 0; plane < NumPlanes; plane++ ) {
        for ( ppass = 0; ppass < 2; ppass++ ) {
            RefSgrXqd[ plane ][ ppass ] = Sgrproj_Xqd_Mid[ ppass ]
#if COVER
            uint1 pPass
            pPass = ppass
            COVERCROSS(VALUE_SGRPROJ_XQD_MID, pPass)
#endif // COVER
            for ( i = 0; i < WIENER_COEFFS; i++ ) {
                RefLrWiener[ plane ][ ppass ][ i ] = Wiener_Taps_Mid[ i ]
#if COVER
                uint_0_2 I
                I = i
                COVERCROSS(VALUE_WIENER_TAPS_MID, I)
#endif // COVER
            }
        }
    }

    sbSize = use_128x128_superblock ? BLOCK_128X128 : BLOCK_64X64
    sbSize4 = Num_4x4_Blocks_Wide[ sbSize ]
    for ( r = MiRowStart; r < MiRowEnd; r += sbSize4 ) {
        clear_left_context( )
        for ( c = MiColStart; c < MiColEnd; c += sbSize4 ) {
            ReadDeltas = delta_q_present
            clear_cdef( r, c )
            clear_block_decoded_flags( r, c, sbSize4 )
            read_lr( r, c, sbSize )
            decode_partition( r, c, sbSize )
        }
    }
}

clear_left_context( ) {
    for (i = 0; i < MiRows; i++) {
        for (plane = 0; plane < 3; plane++) {
            LeftDcContext[ plane ][ i ] = 0
            LeftLevelContext[ plane ][ i ] = 0
        }
        LeftSegPredContext[ i ] = 0
    }
}

clear_above_context( ) {
    for (i = 0; i < MiCols; i++) {
        for (plane = 0; plane < 3; plane++) {
            AboveDcContext[ plane ][ i ] = 0
            AboveLevelContext[ plane ][ i ] = 0
        }
        AboveSegPredContext[ i ] = 0
    }
}

// Clear Block Decoded Flags Function
clear_block_decoded_flags( r, c, sbSize4 ) {
    for ( plane = 0; plane < NumPlanes; plane++ ) {
        subX = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) : 0)
        subY = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) : 0)
        sbWidth4 = ( MiColEnd - c ) >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX)
        sbHeight4 = ( MiRowEnd - r ) >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY)
        for ( y = -1; y <= ( sbSize4 >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY) ); y++ )
            for ( x = -1; x <= ( sbSize4 >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX) ); x++ ) {
                if ( y < 0 && x < sbWidth4 )
                    BlockDecoded[ plane ][ y ][ x ] = 1
                else if ( x < 0 && y < sbHeight4 )
                    BlockDecoded[ plane ][ y ][ x ] = 1
                else
                    BlockDecoded[ plane ][ y ][ x ] = 0
            }
        //Below-left
        BlockDecoded[ plane ][ sbSize4 >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY) ][ -1 ] = 0
    }
}

reset_block_context( bw4, bh4 ) {
    for ( plane = 0; plane < 1 + 2 * COVERCLASS(1, (PROFILE0 || PROFILE2), HasChroma ); plane++ ) {

        subX = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) : 0)
        subY = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) : 0)
        for ( i = MiCol >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX); i < ( ( MiCol + bw4 ) >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX) ); i++) {
            AboveLevelContext[ plane ][ i ] = 0
            AboveDcContext[ plane ][ i ] = 0
        }
        for ( i = MiRow >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY); i < ( ( MiRow + bh4 ) >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY) ); i++) {
            LeftLevelContext[ plane ][ i ] = 0
            LeftDcContext[ plane ][ i ] = 0
        }
    }
}

// Decode Block Syntax
decode_block( r, c, subSize ) {
#if VALIDATE_SPEC_STRUCTURE
    validate(40001)
    validate(r)
    validate(c)
    validate(subSize)
#endif
    MiRow = r
    MiCol = c
    MiSize = subSize
    AvailU = is_inside( r - 1, c )
    AvailL = is_inside( r, c - 1 )
    bw4 = Num_4x4_Blocks_Wide[ subSize ]
    bh4 = Num_4x4_Blocks_High[ subSize ]
    if ( COVERCLASS((PROFILE0 || PROFILE2), 1, COVERCLASS((PROFILE0 || PROFILE2), 1, bh4 == 1 && COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)) && COVERCLASS((PROFILE0 || PROFILE2), (MiRow & 1) == 0 ) ) )
        HasChroma = 0
    else if ( COVERCLASS((PROFILE0 || PROFILE2), 1, COVERCLASS((PROFILE0 || PROFILE2), 1, bw4 == 1 && COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)) && COVERCLASS((PROFILE0 || PROFILE2), (MiCol & 1) == 0 ) ) )
        HasChroma = 0
    else
        HasChroma = COVERCLASS(1, (PROFILE0 || PROFILE2), NumPlanes > 1)
    AvailU = is_inside( r - 1, c )
    AvailL = is_inside( r, c - 1 )
    AvailUChroma = AvailU
    AvailLChroma = AvailL
    if ( COVERCLASS(1, (PROFILE0 || PROFILE2), HasChroma ) ) {
        if ( COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) && COVERCLASS((PROFILE0 || PROFILE2), bh4 == 1) ) )
            AvailUChroma = is_inside( r - 2, c )
        if ( COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) && COVERCLASS((PROFILE0 || PROFILE2), bw4 == 1) ) )
            AvailLChroma = is_inside( r, c - 2 )
    } else {
        AvailUChroma = 0
        AvailLChroma = 0
    }
    mode_info( )
    palette_tokens( )
    read_block_tx_size( )

    if ( skip )
        reset_block_context( bw4, bh4 )
    isCompound = RefFrame[ 1 ] > INTRA_FRAME
    for ( y = 0; y < bh4; y++ ) {
        for ( x = 0; x < bw4; x++ ) {
            YModes [ r + y ][ c + x ] = YMode
            if ( RefFrame[ 0 ] == INTRA_FRAME && COVERCLASS(1, (PROFILE0 || PROFILE2), HasChroma ) )
                UVModes [ r + y ][ c + x ] = UVMode
            for( refList = 0; refList < 2; refList++ )
                RefFrames[ r + y ][ c + x ][ refList ] = RefFrame[ refList ]
            RefFramesWritten[ r + y ][ c + x ] = 1
            if ( is_inter ) {
                if ( !use_intrabc ) {
                    CompGroupIdxs[ r + y ][ c + x ] = comp_group_idx
                    CompoundIdxs[ r + y ][ c + x ] = compound_idx
                }
                for ( dir = 0; dir < 2; dir++ ) {
                    InterpFilters[ r + y ][ c + x ][ dir ] = interp_filter[ dir ]
                }
                for( refList = 0; refList < 1 + isCompound; refList++ ) {
                    Mvs[ r + y ][ c + x ][ refList ][ 0 ] = Mv[ refList ][ 0 ]
                    Mvs[ r + y ][ c + x ][ refList ][ 1 ] = Mv[ refList ][ 1 ]
                }
            }
        }
    }
    compute_prediction()
    residual( )
    for ( y = 0; y < bh4; y++ ) {
        for ( x = 0; x < bw4; x++ ) {
            IsInters[ r + y ][ c + x ] = is_inter
            SkipModes[ r + y ][ c + x ] = skip_mode
            Skips[ r + y ][ c + x ] = skip
            TxSizes[ r + y ][ c + x ] = TxSize
            MiSizes[ r + y ][ c + x ] = MiSize
            MiColStartGrid[ r + y ][ c + x ] = MiColStart
            MiRowStartGrid[ r + y ][ c + x ] = MiRowStart
            MiColEndGrid[ r + y ][ c + x ] = MiColEnd
            MiRowEndGrid[ r + y ][ c + x ] = MiRowEnd

            SegmentIds[ r + y ][ c + x ] = segment_id
            PaletteSizes[ 0 ][ r + y ][ c + x ] = PaletteSizeY
            PaletteSizes[ 1 ][ r + y ][ c + x ] = PaletteSizeUV
            for ( i = 0; i < PaletteSizeY; i++ )
                PaletteColors[ 0 ][ r + y ][ c + x ][ i ] = palette_colors_y[ i ]
            for ( i = 0; i < PaletteSizeUV; i++ )
                PaletteColors[ 1 ][ r + y ][ c + x ][ i ] = palette_colors_u[ i ]
            for ( i = 0; i < FRAME_LF_COUNT; i++ )
                DeltaLFs[ r + y ][ c + x ][ i ] = DeltaLF[ i ]
        }
    }
}

// Mode Info Syntax
mode_info( ) {
    if ( FrameIsIntra )
        intra_frame_mode_info( )
    else
        inter_frame_mode_info( )
}

// Intra Frame Mode Info Syntax
intra_frame_mode_info( ) {
    uint_0_12 intra_frame_y_mode
    skip = 0
    if ( SegIdPreSkip )
        intra_segment_id( )
    skip_mode = 0
    read_skip( )
    if ( !SegIdPreSkip )
        intra_segment_id( )
    read_cdef( )
    read_delta_qindex( )
    read_delta_lf( )
    ReadDeltas = 0
    RefFrame[ 0 ] = INTRA_FRAME
    RefFrame[ 1 ] = NONE
    if ( allow_intrabc ) {
        use_intrabc                                                          ae(v)
    } else {
        use_intrabc = 0
    }
    if ( use_intrabc ) {
        is_inter = 1
        YMode = DC_PRED
        UVMode = DC_PRED
        motion_mode = SIMPLE
        compound_type = COMPOUND_AVERAGE
        PaletteSizeY = 0
        PaletteSizeUV = 0
        interp_filter[ 0 ] = BILINEAR
        interp_filter[ 1 ] = BILINEAR
        find_mv_stack( 0 )
        assign_mv( 0 )
    } else {
        is_inter = 0
        intra_frame_y_mode                                                   ae(v)
        COVERCROSS(VALUE_CABAC_INTRA_FRAME_Y_MODE, intra_frame_y_mode)
        YMode = intra_frame_y_mode
        intra_angle_info_y( )
        if (COVERCLASS(1, (PROFILE0 || PROFILE2), HasChroma )) {
            uv_mode                                                          ae(v)
            COVERCROSS(VALUE_CABAC_UV_MODE, uv_mode)
            UVMode = uv_mode
            if (UVMode == UV_CFL_PRED) {
                read_cfl_alphas( )
            }
            intra_angle_info_uv( )
        }
        PaletteSizeY = 0
        PaletteSizeUV = 0
        if ( MiSize >= BLOCK_8X8 &&
             Block_Width[ MiSize ] <= 64  &&
             Block_Height[ MiSize ] <= 64 &&
             allow_screen_content_tools ) {
            palette_mode_info( )
        }
        filter_intra_mode_info( )
    }
}

// Intra Segment ID Syntax
intra_segment_id( ) {
    if ( segmentation_enabled )
        read_segment_id( )
    else
        segment_id = 0
    Lossless = LosslessArray[ segment_id ]
}

// Read Segment ID Syntax
read_segment_id( ) {
    if ( AvailU && AvailL )
        prevUL = SegmentIds[ MiRow - 1 ][ MiCol - 1 ]
    else
        prevUL = -1
    if ( AvailU )
        prevU = SegmentIds[ MiRow - 1 ][ MiCol ]
    else
        prevU = -1
    if ( AvailL )
        prevL = SegmentIds[ MiRow ][ MiCol - 1 ]
    else
        prevL = -1
    if (prevU == -1)
        pred = ( prevL == -1 ) ? 0 : prevL
    else if (prevL == -1)
        pred = prevU
    else
        pred = (prevUL == prevU) ? prevU : prevL
    if ( skip ) {
        segment_id = pred
    } else {
          segment_id                                                           ae(v)
        COVERCROSS(VALUE_CABAC_SEGMENT_ID, segment_id)
        segment_id = neg_deinterleave( segment_id, pred,
                                       LastActiveSegId + 1 )
        CHECK(segment_id >= 0, "segment_id must be in range 0 to LastActiveSegId inclusive")
        CHECK(segment_id <= LastActiveSegId, "segment_id must be in range 0 to LastActiveSegId inclusive")
    }
}

neg_deinterleave(diff, ref, max) {
  if (!ref)
    return diff
  if (ref >= (max - 1))
    return max - diff - 1
  if (2 * ref < max) {
    if (diff <= 2 * ref) {
      if (diff & 1)
        return ref + ((diff + 1) >> 1)
      else
        return ref - (diff >> 1)
    }
    return diff
  } else {
    if (diff <= 2 * (max - ref - 1)) {
      if (diff & 1)
        return ref + ((diff + 1) >> 1)
      else
        return ref - (diff >> 1)
    }
    return max - (diff + 1)
  }
}

// Skip Mode Syntax
read_skip_mode() {
    if ( seg_feature_active( SEG_LVL_SKIP ) ||
         seg_feature_active( SEG_LVL_REF_FRAME ) ||
         seg_feature_active( SEG_LVL_GLOBALMV ) ||
         !skip_mode_present ||
         Block_Width[ MiSize ] < 8 ||
         Block_Height[ MiSize ] < 8 ) {
        skip_mode = 0
    } else {
        skip_mode                                                            ae(v)
        COVERCROSS(VALUE_CABAC_SKIP_MODE, skip_mode)
    }
}

// Skip Syntax
read_skip() {
    if ( SegIdPreSkip && seg_feature_active( SEG_LVL_SKIP ) ) {
        skip = 1
    } else {
          skip                                                                 ae(v)
        COVERCROSS(VALUE_CABAC_SKIP_MODE, skip)
    }
}

// Quantizer Index Delta Syntax
read_delta_qindex( ) {
    uint2 delta_q_abs
    uint3 delta_q_rem_bits
    uint8 delta_q_abs_bits
    uint1 delta_q_sign_bit
    sbSize = use_128x128_superblock ? BLOCK_128X128 : BLOCK_64X64
    if ( !( MiSize == sbSize && skip ) ) {
        if ( ReadDeltas ) {
              delta_q_abs                                                          ae(v)
              COVERCROSS(VALUE_CABAC_DELTA_Q_ABS, delta_q_abs)
            if ( delta_q_abs == DELTA_Q_SMALL ) {
                  delta_q_rem_bits                                                 ae(v)
                COVERCROSS(VALUE_CABAC_DELTA_Q_REM_BITS, delta_q_rem_bits)
                n = delta_q_rem_bits + 1
                  delta_q_abs_bits                                                 ae(v)
                COVERCROSS(VALUE_CABAC_DELTA_Q_ABS_BITS, delta_q_abs_bits)
                deltaQAbs = delta_q_abs_bits + (1 << n) + 1
            } else {
                deltaQAbs = delta_q_abs
            }
            if (deltaQAbs) {
                  delta_q_sign_bit                                                 ae(v)
                COVERCROSS(VALUE_CABAC_DELTA_Q_SIGN_BIT, delta_q_sign_bit)
                reducedDeltaQIndex = delta_q_sign_bit ? -deltaQAbs : deltaQAbs
                CurrentQIndex = Clip3(1, 255, CurrentQIndex + (reducedDeltaQIndex << delta_q_res))
            }
        }
    }
}

// Loop Filter Delta Syntax
read_delta_lf( ) {
    uint2 delta_lf_abs
    uint3 delta_lf_rem_bits
    uint8 delta_lf_abs_bits
    uint1 delta_lf_sign_bit
    sbSize = use_128x128_superblock ? BLOCK_128X128 : BLOCK_64X64
    if ( !( MiSize == sbSize && skip ) ) {
        if ( ReadDeltas && delta_lf_present ) {
            frameLfCount = 1
            if ( delta_lf_multi ) {
                frameLfCount = COVERCLASS(1, (PROFILE0 || PROFILE2), NumPlanes > 1) ? FRAME_LF_COUNT : ( FRAME_LF_COUNT - 2 )
            }
            for ( i = 0; i < frameLfCount; i++ ) {
                  delta_lf_abs                                                     ae(v)
                COVERCROSS(VALUE_CABAC_DELTA_LF_ABS, delta_lf_abs)
                if ( delta_lf_abs == DELTA_LF_SMALL ) {
                      delta_lf_rem_bits                                            ae(v)
                    COVERCROSS(VALUE_CABAC_DELTA_LF_REM_BITS, delta_lf_rem_bits)
                    n = delta_lf_rem_bits + 1
                      delta_lf_abs_bits                                            ae(v)
                    COVERCROSS(VALUE_CABAC_DELTA_LF_ABS_BITS, delta_lf_abs_bits)
                    deltaLfAbs = delta_lf_abs_bits +
                                   ( 1 << n ) + 1
                } else {
                    deltaLfAbs = delta_lf_abs
                }
                if ( deltaLfAbs ) {
                      delta_lf_sign_bit                                            ae(v)
                    COVERCROSS(VALUE_CABAC_DELTA_LF_SIGN_BIT, delta_lf_sign_bit)
                    reducedDeltaLfLevel = delta_lf_sign_bit ?
                                          -deltaLfAbs :
                                           deltaLfAbs
                    DeltaLF[ i ] = Clip3( -MAX_LOOP_FILTER, MAX_LOOP_FILTER, DeltaLF[ i ] +
                                      (reducedDeltaLfLevel << delta_lf_res) )
                }
            }
        }
    }
}

// Segmentation Feature Active Function
seg_feature_active_idx( idx, feature ) {
    return segmentation_enabled && FeatureEnabled[ idx ][ feature ]
}

// TX Size Syntax
read_tx_size( allowSelect ) {
    uint_0_2 tx_depth
    uint_0_4 maxTxDepth
    if ( Lossless ) {
        TxSize = TX_4X4
    } else {
        maxRectTxSize = Max_Tx_Size_Rect[ MiSize ]
        maxTxDepth = Max_Tx_Depth[ MiSize ]
#if COVER
        uint_0_21 read_tx_size_mi_size
        read_tx_size_mi_size = MiSize
        COVERCROSS(VALUE_MAX_TX_SIZE_RECT, read_tx_size_mi_size)
        COVERCROSS(VALUE_MAX_TX_DEPTH, read_tx_size_mi_size)
#endif // COVER
        TxSize = maxRectTxSize
        if ( MiSize > BLOCK_4X4 && allowSelect && TxMode == TX_MODE_SELECT ) {
                  tx_depth                                                         ae(v)
                COVERCROSS(VALUE_CABAC_TX_DEPTH, tx_depth)
                for ( i = 0; i < tx_depth; i++ ) {
#if COVER
                    uint_0_18 read_tx_size_TxSize
                    read_tx_size_TxSize = TxSize
                    COVERCROSS(VALUE_SPLIT_TX_SIZE, read_tx_size_TxSize)
#endif // COVER
                    TxSize = Split_Tx_Size[ TxSize ]
                }
        }
    }
}

// The Rect_Tx_Allowed table specifies which block sizes are allowed to use a rectangular transform; the table is defined as:

int Rect_Tx_Allowed[ BLOCK_SIZES ] = {
    0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 0, 0, 0, 0
};

// Inter TX Size Syntax
// read_block_tx_size is used in inter frames to read the transform sizes.
// Either a single transform size is used for the whole block, or a tree of transform sizes is generated.
// As the maximum transform size is 32 samples, a block may require several transform trees to be coded.

read_block_tx_size( ) {
    bw4 = Num_4x4_Blocks_Wide[ MiSize ]
    bh4 = Num_4x4_Blocks_High[ MiSize ]
    if (TxMode == TX_MODE_SELECT &&
          MiSize > BLOCK_4X4 && is_inter &&
          !skip && !Lossless) {
        uint_0_18 maxTxSz
        maxTxSz = Max_Tx_Size_Rect[ MiSize ]
        txW4 = Tx_Width[ maxTxSz ] / MI_SIZE
        txH4 = Tx_Height[ maxTxSz ] / MI_SIZE
        COVERCROSS(VALUE_TX_WIDTH, maxTxSz)
        COVERCROSS(VALUE_TX_HEIGHT, maxTxSz)
        for ( row = MiRow; row < MiRow + bh4; row += txH4 )
            for ( col = MiCol; col < MiCol + bw4; col += txW4 )
                read_var_tx_size( row, col, maxTxSz, 0 )
    } else {
        read_tx_size(!skip || !is_inter)
#if VALIDATE_SPEC_VARTX
        validate(130000)
        validate(Tx_Width[ TxSize ])
        validate(Tx_Height[ TxSize ])
#endif
        for ( row = MiRow; row < MiRow + bh4; row++ )
            for ( col = MiCol; col < MiCol + bw4; col++ )
                InterTxSizes[ row ][ col ] = TxSize
    }
}

// Var TX Size Syntax
// read_var_tx_size is used to read a transform size tree.
read_var_tx_size( row, col, uint_0_18 txSz, uint8 depth) {
    uint1 txfm_split
    if ( row < MiRows && col < MiCols ) {
        if (txSz == TX_4X4 || depth == MAX_VARTX_DEPTH) {
            txfm_split = 0
        } else {
              txfm_split                                                           ae(v)
            COVERCROSS(VALUE_CABAC_TXFM_SPLIT, txfm_split)
        }
        w4 = Tx_Width[ txSz ] / MI_SIZE
        h4 = Tx_Height[ txSz ] / MI_SIZE
        COVERCROSS(VALUE_TX_WIDTH, txSz)
        COVERCROSS(VALUE_TX_HEIGHT, txSz)
        if (txfm_split) {
#if COVER
            uint_0_18 read_var_tx_size_txSz
            read_var_tx_size_txSz = txSz
            COVERCROSS(VALUE_SPLIT_TX_SIZE, read_var_tx_size_txSz)
#endif // COVER
            subTxSz = Split_Tx_Size[ txSz ]
            stepW = Tx_Width[ subTxSz ] / MI_SIZE
            stepH = Tx_Height[ subTxSz ] / MI_SIZE
            for ( i = 0; i < h4; i += stepH )
                for ( j = 0; j < w4; j += stepW )
                    read_var_tx_size( row + i, col + j, subTxSz, depth+1)
        } else {
            for (i = 0; i < h4; i++ )
                for (j = 0; j < w4; j++ )
                    InterTxSizes[ row + i ][ col + j ] = txSz
            TxSize = txSz
        }
    }
}

// where Split_Tx_Size defines the transform size to use if txfm_split is equal to 1, and is defined as:

int Split_Tx_Size[TX_SIZES_ALL] = {
  TX_4X4,
  TX_4X4,
  TX_8X8,
  TX_16X16,
  TX_32X32,
  TX_4X4,
  TX_4X4,
  TX_8X8,
  TX_8X8,
  TX_16X16,
  TX_16X16,
  TX_32X32,
  TX_32X32
  TX_4X8,
  TX_8X4,
  TX_8X16,
  TX_16X8,
  TX_16X32,
  TX_32X16
};

// Inter Frame Mode Info Function
inter_frame_mode_info( ) {
    use_intrabc = 0
    LeftRefFrame[ 0 ] = AvailL ? RefFrames[ MiRow ][ MiCol-1 ][ 0 ] : INTRA_FRAME
    AboveRefFrame[ 0 ] = AvailU ? RefFrames[ MiRow-1 ][ MiCol ][ 0 ] : INTRA_FRAME
    LeftRefFrame[ 1 ] = AvailL ? RefFrames[ MiRow ][ MiCol-1 ][ 1 ] : NONE
    AboveRefFrame[ 1 ] = AvailU ? RefFrames[ MiRow-1 ][ MiCol ][ 1 ] : NONE
    LeftIntra = LeftRefFrame[ 0 ] <= INTRA_FRAME
    AboveIntra = AboveRefFrame[ 0 ] <= INTRA_FRAME
    LeftSingle = LeftRefFrame[ 1 ] <= INTRA_FRAME
    AboveSingle = AboveRefFrame[ 1 ] <= INTRA_FRAME
    skip = 0
    inter_segment_id( 1 )
    read_skip_mode( )
    if ( skip_mode )
      skip = 1
    else
      read_skip( )
    if ( !SegIdPreSkip )
        inter_segment_id( 0 )
    Lossless = LosslessArray[ segment_id ]
    read_cdef( )
    read_delta_qindex( )
    read_delta_lf( )
    ReadDeltas = 0
    read_is_inter( )
    if ( is_inter )
        inter_block_mode_info( )
    else
        intra_block_mode_info( )
}

// Inter Segment ID Syntax
inter_segment_id( preSkip ) {
    uint1 seg_id_predicted
    if ( segmentation_enabled ) {
        predictedSegmentId = get_segment_id( )
        if ( segmentation_update_map ) {
            if ( preSkip && !SegIdPreSkip ) {
                segment_id = 0
                return 0
            }
            if ( !preSkip ) {
                if ( skip ) {
                    seg_id_predicted = 0
                    for ( i = 0; i < Num_4x4_Blocks_Wide[ MiSize ]; i++ )
                        AboveSegPredContext[ MiCol + i ] = seg_id_predicted
                    for ( i = 0; i < Num_4x4_Blocks_High[ MiSize ]; i++ )
                        LeftSegPredContext[ MiRow + i ] = seg_id_predicted
                    read_segment_id( )
                    return 0
                }
            }
            if ( segmentation_temporal_update == 1 ) {
                  seg_id_predicted                                             ae(v)
                COVERCROSS(VALUE_CABAC_SEG_ID_PREDICTED, seg_id_predicted)
                if ( seg_id_predicted )
                    segment_id = predictedSegmentId
                else
                    read_segment_id( )
                for ( i = 0; i < Num_4x4_Blocks_Wide[ MiSize ]; i++ )
                    AboveSegPredContext[ MiCol + i ] = seg_id_predicted
                for ( i = 0; i < Num_4x4_Blocks_High[ MiSize ]; i++ )
                    LeftSegPredContext[ MiRow + i ] = seg_id_predicted
            } else {
                    read_segment_id( )
            }
        } else {
            segment_id = predictedSegmentId
        }
    } else {
        segment_id = 0
    }
}

// Is Inter Syntax
read_is_inter( ) {
    if ( skip_mode ) {
        is_inter = 1
    } else if ( seg_feature_active ( SEG_LVL_REF_FRAME ) ) {
        is_inter = FeatureData[ segment_id ][ SEG_LVL_REF_FRAME ] != INTRA_FRAME
    } else if ( seg_feature_active ( SEG_LVL_GLOBALMV ) ) {
        is_inter = 1
    } else {
        is_inter                                                             ae(v)
        COVERCROSS(VALUE_CABAC_IS_INTER, is_inter)
    }
}

// Get Segment ID Syntax
// The predicted segment id is the smallest value found in the on-screen region of the segmentation map covered by the current block.
get_segment_id( ) {
    bw4 = Num_4x4_Blocks_Wide[ MiSize ]
    bh4 = Num_4x4_Blocks_High[ MiSize ]
    xMis = Min( MiCols - MiCol, bw4 )
    yMis = Min( MiRows - MiRow, bh4 )
    seg = 7
    for ( y = 0; y < yMis; y++ )
        for ( x = 0; x < xMis; x++ )
            seg = Min( seg, PrevSegmentIds[ MiRow + y ][ MiCol + x ] )
    return seg
}

// Intra Block Mode Info Syntax
// The predicted segment id is the smallest value found in the on-screen region of the segmentation map covered by the current block.
intra_block_mode_info( ) {
    uint_0_12 y_mode
    RefFrame[ 0 ] = INTRA_FRAME
    RefFrame[ 1 ] = NONE
      y_mode                                                                   ae(v)
    COVERCROSS(VALUE_CABAC_Y_MODE, y_mode)
    YMode = y_mode
    intra_angle_info_y( )
    if (COVERCLASS(1, (PROFILE0 || PROFILE2), HasChroma )) {
          uv_mode                                                              ae(v)
        COVERCROSS(VALUE_CABAC_UV_MODE, uv_mode)
        UVMode = uv_mode
        if (UVMode == UV_CFL_PRED) {
            read_cfl_alphas( )
        }
        intra_angle_info_uv( )
    }
    PaletteSizeY = 0
    PaletteSizeUV = 0
    if ( MiSize >= BLOCK_8X8 &&
         Block_Width[ MiSize ] <= 64  &&
         Block_Height[ MiSize ] <= 64 &&
          allow_screen_content_tools )
        palette_mode_info( )
    filter_intra_mode_info( )
}

int Compound_Mode_Ctx_Map[ 3 ][ COMP_NEWMV_CTXS ] = {
    { 0, 1, 1, 1, 1 },
    { 1, 2, 3, 4, 4 },
    { 4, 4, 5, 6, 7 }
};

// Inter Block Mode Info Syntax
inter_block_mode_info( ) {
    uint3 compound_mode
    uint1 new_mv
    uint1 zero_mv
    uint1 ref_mv
    uint_0_2 drl_mode_ctx
    uint1 drl_mode
    uint1 isCompound
#if COVER
    uint_0_1 syntax_drl_mode
#endif // COVER
    PaletteSizeY = 0
    PaletteSizeUV = 0
    read_ref_frames( )
    if ( COVERCLASS( LARGE_SCALE_TILES, 1, COVERCLASS(LARGE_SCALE_TILES, 1, num_large_scale_tile_anchor_frames > 0) && COVERCLASS(LARGE_SCALE_TILES, IMPOSSIBLE, FrameNumber > num_large_scale_tile_anchor_frames)) ) {
        CHECK( RefFrame[ 0 ] == LAST_FRAME, "large_scale_tile: RefFrame[ 0 ] must be equal to LAST_FRAME" )
        CHECK( RefFrame[ 1 ] == NONE, "large_scale_tile: RefFrame[ 1 ] must be equal to NONE" )
    }
    isCompound = RefFrame[ 1 ] > INTRA_FRAME
    find_mv_stack( isCompound )
    if ( skip_mode ) {
        YMode = NEAREST_NEARESTMV
    } else if ( seg_feature_active( SEG_LVL_SKIP ) ||
         seg_feature_active( SEG_LVL_GLOBALMV ) ) {
        YMode = GLOBALMV
    } else if ( isCompound ) {
          compound_mode                                                        ae(v)
        COVERCROSS(VALUE_CABAC_COMPOUND_MODE, compound_mode)
        YMode = NEAREST_NEARESTMV + compound_mode
    } else {
          new_mv                                                               ae(v)
        COVERCROSS(VALUE_CABAC_NEW_MV, new_mv)
        if ( new_mv == 0 ) {
            YMode = NEWMV
        } else {
              zero_mv                                                          ae(v)
            COVERCROSS(VALUE_CABAC_ZERO_MV, zero_mv)
            if ( zero_mv == 0 ) {
                YMode = GLOBALMV
            } else {
                  ref_mv                                                       ae(v)
                COVERCROSS(VALUE_CABAC_REF_MV, ref_mv)
                YMode = (ref_mv == 0) ? NEARESTMV : NEARMV
            }
        }
    }
    RefMvIdx = 0
    COVERCROSS(CROSS_NUM_MV_FOUND, NumMvFound, isCompound)
    if (YMode == NEWMV || YMode == NEW_NEWMV) {
        for (idx = 0; idx < 2; idx++) {
            if (NumMvFound > idx + 1) {
                  drl_mode                                                     ae(v)
                COVERCROSS(VALUE_CABAC_DRL_MODE, drl_mode)
                if (drl_mode == 0) {
                  RefMvIdx = idx
                  break
                }
                RefMvIdx = idx + 1
            }
        }
    } else if ( has_nearmv( ) ) {
        RefMvIdx = 1
        for (idx = 1; idx < 3; idx++) {
            if (NumMvFound > idx + 1) {
                  drl_mode                                                     ae(v)
                COVERCROSS(VALUE_CABAC_DRL_MODE, drl_mode)
                if ( drl_mode == 0 ) {
                    RefMvIdx = idx
                    break
                }
                RefMvIdx = idx + 1
            }
        }
    }
    assign_mv( isCompound )
    read_interintra_mode( isCompound )
    read_motion_mode( isCompound )
    read_compound_type( isCompound )
    if ( interpolation_filter == SWITCHABLE ) {
        for ( dir = 0; dir < ( enable_dual_filter ? 2 : 1 ); dir++ ) {
            if ( needs_interp_filter( ) ) {
                  interp_filter[ dir ]                                         ae(v)
                COVERCROSS(VALUE_CABAC_INTERP_FILTER, interp_filter[ dir ])
            } else {
                interp_filter[ dir ] = EIGHTTAP
            }
        }
        if ( !enable_dual_filter )
            interp_filter[ 1 ] = interp_filter[ 0 ]
    } else {
        for ( dir = 0; dir < 2; dir++ )
            interp_filter[ dir ] = interpolation_filter
    }
}

// The function has_nearmv is defined as:
has_nearmv( ) {
    return (YMode == NEARMV || YMode == NEAR_NEARMV
            || YMode == NEAR_NEWMV || YMode == NEW_NEARMV)
}

// The function needs_interp_filter is defined as:
needs_interp_filter( ) {
    large = (Min(Block_Width[MiSize], Block_Height[MiSize]) >= 8)
    if ( skip_mode || motion_mode == LOCALWARP ) {
        return 0
    } else if (large && YMode == GLOBALMV) {
        return GmType[ RefFrame[ 0 ] ] == TRANSLATION
    } else if (large && YMode == GLOBAL_GLOBALMV ) {
        return GmType[ RefFrame[ 0 ] ] == TRANSLATION || GmType[ RefFrame[ 1 ] ] == TRANSLATION
    } else {
        return 1
    }
}

// Filter Intra Mode Info Syntax
filter_intra_mode_info( ) {
    use_filter_intra = 0
    if ( enable_filter_intra &&
         YMode == DC_PRED && PaletteSizeY == 0 &&
         Max( Block_Width[ MiSize ], Block_Height[ MiSize ] ) <= 32 ) {
          use_filter_intra                                                     ae(v)
        COVERCROSS(VALUE_CABAC_USE_FILTER_INTRA, use_filter_intra)
        if ( use_filter_intra ) {
              filter_intra_mode                                                ae(v)
            COVERCROSS(VALUE_CABAC_FILTER_INTRA_MODE, filter_intra_mode)
        }
    }
}

// Ref Frames Syntax
read_ref_frames( ) {
    uint1 r_comp_mode
    uint1 comp_ref_type
    uint1 uni_comp_ref
    uint1 uni_comp_ref_p1
    uint1 uni_comp_ref_p2
    uint1 comp_ref
    uint1 comp_ref_p1
    uint1 comp_ref_p2
    uint1 comp_bwdref
    uint1 comp_bwdref_p1
    uint1 single_ref_p1
    uint1 single_ref_p2
    uint1 single_ref_p3
    uint1 single_ref_p4
    uint1 single_ref_p5
    uint1 single_ref_p6
    // Extra variables used for coverage
    uint_1_7 rf0
    uint_1_7 rf1
    if ( skip_mode ) {
        RefFrame[ 0 ] = SkipModeFrame[ 0 ]
        RefFrame[ 1 ] = SkipModeFrame[ 1 ]
        rf0 = RefFrame[0]
        rf1 = RefFrame[1]
    } else if ( seg_feature_active( SEG_LVL_REF_FRAME ) ) {
        RefFrame[ 0 ] = FeatureData[ segment_id ][ SEG_LVL_REF_FRAME ]
        RefFrame[ 1 ] = NONE
    } else if ( seg_feature_active( SEG_LVL_SKIP ) ||
                seg_feature_active( SEG_LVL_GLOBALMV ) ) {
        RefFrame[ 0 ] = LAST_FRAME
        RefFrame[ 1 ] = NONE
    } else {
        bw4 = Num_4x4_Blocks_Wide[ MiSize ]
        bh4 = Num_4x4_Blocks_High[ MiSize ]
        if ( reference_select && ( Min( bw4, bh4 ) >= 2 ) ) {
              r_comp_mode                                                        ae(v)
            COVERCROSS(VALUE_CABAC_COMP_MODE, r_comp_mode)
            comp_mode = r_comp_mode
        } else {
            comp_mode = SINGLE_REFERENCE
        }
        if ( comp_mode == COMPOUND_REFERENCE ) {
              comp_ref_type                                                    ae(v)
            COVERCROSS(VALUE_CABAC_COMP_REF_TYPE, comp_ref_type)
            if (comp_ref_type == UNIDIR_COMP_REFERENCE) {
                  uni_comp_ref                                                 ae(v)
                COVERCROSS(VALUE_CABAC_UNI_COMP_REF, uni_comp_ref)
                if (uni_comp_ref) {
                    RefFrame[0] = BWDREF_FRAME
                    RefFrame[1] = ALTREF_FRAME
                } else {
                      uni_comp_ref_p1                                          ae(v)
                    COVERCROSS(VALUE_CABAC_UNI_COMP_REF_P1, uni_comp_ref_p1)
                    if (uni_comp_ref_p1) {
                          uni_comp_ref_p2                                      ae(v)
                        COVERCROSS(VALUE_CABAC_UNI_COMP_REF_P2, uni_comp_ref_p2)
                        if (uni_comp_ref_p2) {
                          RefFrame[0] = LAST_FRAME
                          RefFrame[1] = GOLDEN_FRAME
                        } else {
                          RefFrame[0] = LAST_FRAME
                          RefFrame[1] = LAST3_FRAME
                        }
                    } else {
                        RefFrame[0] = LAST_FRAME
                        RefFrame[1] = LAST2_FRAME
                    }
                }
            } else {
                  comp_ref                                                     ae(v)
                COVERCROSS(VALUE_CABAC_COMP_REF, comp_ref)
                if ( comp_ref == 0 ) {
                      comp_ref_p1                                              ae(v)
                    COVERCROSS(VALUE_CABAC_COMP_REF_P1, comp_ref_p1)
                    RefFrame[ 0 ] = comp_ref_p1 ?
                                    LAST2_FRAME : LAST_FRAME
                } else {
                      comp_ref_p2                                              ae(v)
                    COVERCROSS(VALUE_CABAC_COMP_REF_P2, comp_ref_p2)
                    RefFrame[ 0 ] = comp_ref_p2 ?
                                    GOLDEN_FRAME : LAST3_FRAME
                }
                  comp_bwdref                                                  ae(v)
                COVERCROSS(VALUE_CABAC_COMP_BWDREF, comp_bwdref)
                if ( comp_bwdref == 0 ) {
                      comp_bwdref_p1                                           ae(v)
                    COVERCROSS(VALUE_CABAC_COMP_BWDREF_P1, comp_bwdref_p1)
                    RefFrame[ 1 ] = comp_bwdref_p1 ?
                                     ALTREF2_FRAME : BWDREF_FRAME
                } else {
                    RefFrame[ 1 ] = ALTREF_FRAME
                }
            }
            rf0 = RefFrame[0]
            rf1 = RefFrame[1]
            COVERCROSS(CROSS_COMP_REF_FRAMES, rf0, rf1)
        } else {
              single_ref_p1                                                    ae(v)
            COVERCROSS(VALUE_CABAC_SINGLE_REF_P1, single_ref_p1)
            if ( single_ref_p1 ) {
                  single_ref_p2                                                ae(v)
                COVERCROSS(VALUE_CABAC_SINGLE_REF_P2, single_ref_p2)
                if ( single_ref_p2 == 0 ) {
                      single_ref_p6                                            ae(v)
                    COVERCROSS(VALUE_CABAC_SINGLE_REF_P6, single_ref_p6)
                    RefFrame[ 0 ] = single_ref_p6 ?
                                     ALTREF2_FRAME : BWDREF_FRAME
                } else {
                    RefFrame[ 0 ] = ALTREF_FRAME
                }
            } else {
                  single_ref_p3                                                ae(v)
                COVERCROSS(VALUE_CABAC_SINGLE_REF_P3, single_ref_p3)
                if ( single_ref_p3 ) {
                      single_ref_p5                                            ae(v)
                    COVERCROSS(VALUE_CABAC_SINGLE_REF_P5, single_ref_p5)
                    RefFrame[ 0 ] = single_ref_p5 ?
                                     GOLDEN_FRAME : LAST3_FRAME
                } else {
                      single_ref_p4                                            ae(v)
                    COVERCROSS(VALUE_CABAC_SINGLE_REF_P4, single_ref_p4)
                    RefFrame[ 0 ] = single_ref_p4 ?
                                     LAST2_FRAME : LAST_FRAME
                }
            }
            RefFrame[ 1 ] = NONE
        }
    }
}

is_samedir_ref_pair(ref0, ref1) {
  return (ref0 >= BWDREF_FRAME) == (ref1 >= BWDREF_FRAME)
}

count_refs(frameType) {
    c = 0
    if ( AvailU ) {
        if ( AboveRefFrame[ 0 ] == frameType ) c++
        if ( AboveRefFrame[ 1 ] == frameType ) c++
    }
    if ( AvailL ) {
        if ( LeftRefFrame[ 0 ] == frameType ) c++
        if ( LeftRefFrame[ 1 ] == frameType ) c++
    }
    return c
}

ref_count_ctx(counts0, counts1) {
  if (counts0 < counts1)
    return 0
  else if (counts0 == counts1)
    return 1
  else
    return 2
}

// Assign MV Function
assign_mv( isCompound ) {
    for ( i = 0; i < 1 + isCompound; i++ ) {
        if ( use_intrabc ) {
            compMode = NEWMV
        } else {
            compMode = get_mode( i )
        }
        if ( use_intrabc ) {
            PredMv[ 0 ][ 0 ] = RefStackMv[ 0 ][ 0 ][ 0 ]
            PredMv[ 0 ][ 1 ] = RefStackMv[ 0 ][ 0 ][ 1 ]
            if ( PredMv[ 0 ][ 0 ] == 0 && PredMv[ 0 ][ 1 ] == 0 ) {
                PredMv[ 0 ][ 0 ] = RefStackMv[ 1 ][ 0 ][ 0 ]
                PredMv[ 0 ][ 1 ] = RefStackMv[ 1 ][ 0 ][ 1 ]
            }
            if ( PredMv[ 0 ][ 0 ] == 0 && PredMv[ 0 ][ 1 ] == 0 ) {
                sbSize = use_128x128_superblock ? BLOCK_128X128 : BLOCK_64X64
                sbSize4 = Num_4x4_Blocks_High[ sbSize ]
                if ( MiRow - sbSize4 < MiRowStart ) {
                    PredMv[ 0 ][ 0 ] = 0
                    PredMv[ 0 ][ 1 ] = -(sbSize4 * MI_SIZE + INTRABC_DELAY_PIXELS) * 8
                } else {
                    PredMv[ 0 ][ 0 ] = -(sbSize4 * MI_SIZE * 8)
                    PredMv[ 0 ][ 1 ] = 0
                }
            }
        } else if ( compMode == GLOBALMV ) {
            PredMv[ i ][ 0 ] = GlobalMvs[ i ][ 0 ]
            PredMv[ i ][ 1 ] = GlobalMvs[ i ][ 1 ]
        } else {
            pos = ( compMode == NEARESTMV ) ? 0 : RefMvIdx
            if ( compMode == NEWMV && NumMvFound <= 1)
                pos = 0
            PredMv[ i ][ 0 ] = RefStackMv[ pos ][ i ][ 0 ]
            PredMv[ i ][ 1 ] = RefStackMv[ pos ][ i ][ 1 ]
        }
        if ( compMode == NEWMV ) {
            read_mv( i )
        } else {
            Mv[ i ][ 0 ] = PredMv[ i ][ 0 ]
            Mv[ i ][ 1 ] = PredMv[ i ][ 1 ]
        }
        :C g_cd_info->log_mv(b->global_data->Mv[i][1], b->global_data->Mv[i][0], i, isCompound, compMode);
    }
    CHECK(is_mv_valid( isCompound ), "is_mv_valid( ) must be equal to 1")
#if VALIDATE_MVS
  for ( i = 0; i < 1 + isCompound; i++ ) {
    validate(600)
    validate(Mv[ i ][0])
    validate(Mv[ i ][1])
  }
#endif
}

// Read Motion Mode Syntax
read_motion_mode( isCompound ) {
    uint1 use_obmc
    if ( skip_mode ) {
        motion_mode = SIMPLE
        return 0
    }
    if ( !is_motion_mode_switchable ) {
        motion_mode = SIMPLE
        return 0
    }
    if ( Min( Block_Width[ MiSize ],
              Block_Height[ MiSize ] ) < 8 ) {
        motion_mode = SIMPLE
        return 0
    }
    if ( !force_integer_mv &&
         ( YMode == GLOBALMV || YMode == GLOBAL_GLOBALMV ) ) {
        if (GmType[ RefFrame[ 0 ] ] > TRANSLATION) {
            motion_mode = SIMPLE
            return 0
        }
    }
    if ( isCompound || RefFrame[ 1 ] == INTRA_FRAME || !has_overlappable_candidates( ) ) {
        motion_mode = SIMPLE
        return 0
    }
    find_warp_samples()
    if ( force_integer_mv || NumSamples == 0 ||
         !allow_warped_motion || is_scaled( RefFrame[0] ) ) {
          use_obmc                                                             ae(v)
        COVERCROSS(VALUE_CABAC_USE_OBMC, use_obmc)
        motion_mode = use_obmc ? OBMC : SIMPLE
    } else {
          motion_mode                                                          ae(v)
        COVERCROSS(VALUE_CABAC_MOTION_MODE, motion_mode)
    }
}

is_scaled( refFrame ) {
  refIdx = ref_frame_idx[ refFrame - LAST_FRAME ]
  xScale = ( ( RefUpscaledWidth[ refIdx ] << REF_SCALE_SHIFT ) + ( FrameWidth / 2 ) ) / FrameWidth
  yScale = ( ( RefFrameHeight[ refIdx ] << REF_SCALE_SHIFT ) + ( FrameHeight / 2 ) ) / FrameHeight
  noScale = 1 << REF_SCALE_SHIFT
  hitLibaomBug2191 =
      COVERCLASS((LEVEL5 && ! CLIENT_A), 1,
                 (COVERCLASS((LEVEL5 && !CLIENT_A), 1, RefFrameHeight[ refIdx ] == 4352) &&
                  COVERCLASS((LEVEL5 && !CLIENT_A), (LEVEL5 && !CLIENT_A),
                             COVERCLASS((LEVEL5 && !CLIENT_A), (LEVEL5 && !CLIENT_A),
                                        FrameHeight == 4351) &&
                             COVERCLASS((LEVEL5 && !CLIENT_A), IMPOSSIBLE,
                                        yScale == 16388))))

  return xScale != noScale || yScale != noScale
}

// Read Inter Intra Syntax
read_interintra_mode( isCompound ) {
    if ( !skip_mode && enable_interintra_compound && !isCompound &&
         MiSize >= BLOCK_8X8 && MiSize <= BLOCK_32X32) {
          interintra                                                           ae(v)
        COVERCROSS(VALUE_CABAC_INTERINTRA, interintra)
        if (interintra) {
              interintra_mode                                                  ae(v)
            COVERCROSS(VALUE_CABAC_INTERINTRA_MODE, interintra_mode)
            RefFrame[1] = INTRA_FRAME
            AngleDeltaY = 0
            AngleDeltaUV = 0
            use_filter_intra = 0
              wedge_interintra                                             ae(v)
            COVERCROSS(VALUE_CABAC_WEDGE_INTERINTRA, wedge_interintra)
            if (wedge_interintra) {
                  wedge_index                                              ae(v)
                COVERCROSS(VALUE_CABAC_WEDGE_INDEX, wedge_index)
                wedge_sign = 0
            }
        }
    } else {
        interintra = 0
    }
}

// Read Compound Type Syntax
read_compound_type( isCompound ) {
    uint1 r_compound_type
    comp_group_idx = 0
    compound_idx = 1
    if ( skip_mode ) {
        compound_type = COMPOUND_AVERAGE
        return 0
    }
    if ( isCompound ) {
        n = Wedge_Bits[ MiSize ]
        COVERCROSS(VALUE_WEDGE_BITS, MiSize)
        if ( enable_masked_compound ) {
              comp_group_idx                                                   ae(v)
              COVERCROSS(VALUE_CABAC_COMP_GROUP_IDX, comp_group_idx)
        }
        if ( comp_group_idx == 0 ) {
            if ( enable_jnt_comp ) {
                compound_idx                                                    ae(v)
                COVERCROSS(VALUE_CABAC_COMPOUND_IDX, compound_idx)
                compound_type = compound_idx ? COMPOUND_AVERAGE :
                                               COMPOUND_DISTANCE
            } else {
                compound_type = COMPOUND_AVERAGE
            }
        } else {
            if ( n == 0 ) {
                compound_type = COMPOUND_DIFFWTD
            } else {
                r_compound_type                                                ae(v)
                COVERCROSS(VALUE_CABAC_COMPOUND_TYPE, r_compound_type)
                compound_type = r_compound_type
            }
        }
        if ( compound_type == COMPOUND_WEDGE ) {
            wedge_index                                                          ae(v)
            COVERCROSS(VALUE_CABAC_WEDGE_INDEX, wedge_index)
            wedge_sign                                                           ae(v)
            COVERCROSS(VALUE_CABAC_WEDGE_SIGN, wedge_sign)
        } else if ( compound_type == COMPOUND_DIFFWTD ) {
            mask_type                                                            ae(v)
            COVERCROSS(VALUE_CABAC_MASK_TYPE, mask_type)
        }
    } else {
        if ( interintra ) {
            compound_type = wedge_interintra ? COMPOUND_WEDGE : COMPOUND_INTRA
        } else {
            compound_type = COMPOUND_AVERAGE
        }
    }
}

// Get Mode Function
get_mode( refList ) {
    if ( refList == 0 ) {
        if (YMode < NEAREST_NEARESTMV)
            compMode = YMode
        else if (YMode == NEW_NEWMV || YMode == NEW_NEARESTMV || YMode == NEW_NEARMV)
            compMode = NEWMV
        else if (YMode == NEAREST_NEARESTMV || YMode == NEAREST_NEWMV)
            compMode = NEARESTMV
        else if (YMode == NEAR_NEARMV || YMode == NEAR_NEWMV)
            compMode = NEARMV
        else
            compMode = GLOBALMV
    } else {
        if (YMode == NEW_NEWMV || YMode == NEAREST_NEWMV || YMode == NEAR_NEWMV)
            compMode = NEWMV
        else if (YMode == NEAREST_NEARESTMV || YMode == NEW_NEARESTMV)
            compMode = NEARESTMV
        else if (YMode == NEAR_NEARMV || YMode == NEW_NEARMV)
            compMode = NEARMV
        else
            compMode = GLOBALMV
    }
    return compMode
}

is_mv_valid( isCompound ) {
  for ( i = 0; i < 1 + isCompound; i++ ) {
        for ( comp = 0; comp < 2; comp++ ) {
            if ( COVERCLASS(IMPOSSIBLE,1,Abs( Mv[ i ][ comp ] ) >= ( 1 << 14 )) )
                return 0
        }
    }
    if ( !use_intrabc ) {
        return 1
    }
    bw = Block_Width[ MiSize ]
    bh = Block_Height[ MiSize ]
    if ( COVERCLASS(IMPOSSIBLE,1,(Mv[ 0 ][ 0 ] & 7) || (Mv[ 0 ][ 1 ] & 7)) ) {
        return 0
    }
    deltaRow = Mv[ 0 ][ 0 ] >> 3
    deltaCol = Mv[ 0 ][ 1 ] >> 3
    srcTopEdge = MiRow * MI_SIZE + deltaRow
    srcLeftEdge = MiCol * MI_SIZE + deltaCol
    srcBottomEdge = srcTopEdge + bh
    srcRightEdge = srcLeftEdge + bw
    if ( COVERCLASS(1, (PROFILE0 || PROFILE2), HasChroma ) ) {
        if ( COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), bw < 8 && COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x ) ) )
            srcLeftEdge -= 4
        if ( COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), bh < 8 && COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y ) ) )
            srcTopEdge -= 4
    }
    if ( COVERCLASS(IMPOSSIBLE,1,
            srcTopEdge < MiRowStart * MI_SIZE ||
            srcLeftEdge < MiColStart * MI_SIZE ||
            srcBottomEdge > MiRowEnd * MI_SIZE ||
            srcRightEdge > MiColEnd * MI_SIZE) ) {
        return 0
    }
    sbSize = use_128x128_superblock ? BLOCK_128X128 : BLOCK_64X64
    sbH = Block_Height[ sbSize ]
    activeSbRow = (MiRow * MI_SIZE) / sbH
    activeSb64Col = (MiCol * MI_SIZE) >> 6
    srcSbRow = (srcBottomEdge - 1) / sbH
    srcSb64Col = (srcRightEdge - 1) >> 6
    totalSb64PerRow = ((MiColEnd - MiColStart - 1) >> 4) + 1
    activeSb64 = activeSbRow * totalSb64PerRow + activeSb64Col
    srcSb64 = srcSbRow * totalSb64PerRow + srcSb64Col
    if ( COVERCLASS(IMPOSSIBLE,1,srcSb64 >= activeSb64 - INTRABC_DELAY_SB64) ) {
        return 0
    }
    gradient = 1 + INTRABC_DELAY_SB64 + use_128x128_superblock
    wfOffset = gradient * (activeSbRow - srcSbRow)
    if (COVERCLASS(IMPOSSIBLE,1,
          srcSbRow > activeSbRow ||
          srcSb64Col >= activeSb64Col - INTRABC_DELAY_SB64 + wfOffset) ) {
        return 0
    }

#if COVER
    intrabc_coverage()
#endif

    return 1
}

// MV Syntax
read_mv( ref ) {
    int diffMv[ 2 ]

    diffMv[ 0 ] = 0
    diffMv[ 1 ] = 0
    if ( use_intrabc ) {
        MvCtx = MV_INTRABC_CONTEXT
    } else {
        MvCtx = 0
    }
      mv_joint                                                                 ae(v)
    COVERCROSS(VALUE_CABAC_MV_JOINT, mv_joint)
    if ( mv_joint == MV_JOINT_HZVNZ || mv_joint == MV_JOINT_HNZVNZ )
        diffMv[ 0 ] = read_mv_component( 0 )
    if ( mv_joint == MV_JOINT_HNZVZ || mv_joint == MV_JOINT_HNZVNZ )
        diffMv[ 1 ] = read_mv_component( 1 )
#if VALIDATE_SPEC_NEWMV
  validate(22000)
  validate(PredMv[ ref ][ 0 ])
  validate(PredMv[ ref ][ 1 ])
  validate(diffMv[ 0 ])
  validate(diffMv[ 1 ])
#endif
    Mv[ ref ][ 0 ] = PredMv[ ref ][ 0 ] + diffMv[ 0 ]
    Mv[ ref ][ 1 ] = PredMv[ ref ][ 1 ] + diffMv[ 1 ]
    //CHECK(-(1 << 14) < Mv[ ref ][ 0 ], "Mv[ ref ][ 0 ] must be larger than -(1 << 14)")
    //CHECK(Mv[ ref ][ 0 ] < (1 << 14) - 1, "Mv[ ref ][ 0 ] must be smaller than (1 << 14)")
    //CHECK(-(1 << 14) < Mv[ ref ][ 1 ], "Mv[ ref ][ 1 ] must be larger than -(1 << 14)")
    //CHECK(Mv[ ref ][ 1 ] < (1 << 14) - 1, "Mv[ ref ][ 1 ] must be smaller than (1 << 14)")
    //Moved to assign_mv CHECK(is_dv_valid( ), "is_dv_valid( ) must be equal to 1")
}

// Note: When has_nearmv() is true, the candidate motion vector to be used is at index RefMvIdx, but the CDF for the motion vectors is associated with the previous candidate.

// MV Component Syntax
read_mv_component( uint1 comp ) {
    uint1 mv_sign
    uint_0_10 mv_class
    uint1 mv_class0_bit
    uint2 mv_class0_fr
    uint1 mv_class0_hp
    uint1 mv_bit
    uint2 mv_fr
    uint1 mv_hp
      mv_sign                                                                  ae(v)
    COVERCROSS(VALUE_CABAC_MV_SIGN, mv_sign)
      mv_class                                                                 ae(v)
    COVERCROSS(VALUE_CABAC_MV_CLASS, mv_class)
    if ( mv_class == MV_CLASS_0 ) {
          mv_class0_bit                                                        ae(v)
        COVERCROSS(VALUE_CABAC_MV_CLASS0_BIT, mv_class0_bit)
        if ( force_integer_mv )
            mv_class0_fr = 3
        else {
            mv_class0_fr                                                       ae(v)
            COVERCROSS(VALUE_CABAC_MV_CLASS0_FR, mv_class0_fr)
        }
        if ( allow_high_precision_mv ) {
            mv_class0_hp                                                       ae(v)
            COVERCROSS(VALUE_CABAC_MV_CLASS0_HP, mv_class0_hp)
        }
        else
            mv_class0_hp = 1
        mag = ( ( mv_class0_bit << 3 ) |
                ( mv_class0_fr << 1 ) |
                  mv_class0_hp ) + 1
    } else {
        d = 0
        for ( i = 0; i < mv_class; i++ ) {
              mv_bit                                                           ae(v)
            COVERCROSS(VALUE_CABAC_MV_BIT, mv_bit)
            d |= mv_bit << i
        }
        mag = CLASS0_SIZE << ( mv_class + 2 )
        if ( force_integer_mv )
            mv_fr = 3
        else {
            mv_fr                                                              ae(v)
            COVERCROSS(VALUE_CABAC_MV_FR, mv_fr)
        }
        if ( allow_high_precision_mv ) {
            mv_hp                                                              ae(v)
            COVERCROSS(VALUE_CABAC_MV_HP, mv_hp)
        }
        else
            mv_hp = 1
        mag += ( ( d << 3 ) | ( mv_fr << 1 ) | mv_hp ) + 1
    }
    return mv_sign ? -mag : mag
}

// Residual Syntax
residual( ) {
    sbMask = use_128x128_superblock ? 31 : 15

    widthChunks = Max( 1, Block_Width[ MiSize ] >> 6 )
    heightChunks = Max( 1, Block_Height[ MiSize ] >> 6 )

    miSizeChunk = ( widthChunks > 1 || heightChunks > 1 ) ? BLOCK_64X64 : MiSize

    for ( chunkY = 0; chunkY < heightChunks; chunkY++ ) {
        for ( chunkX = 0; chunkX < widthChunks; chunkX++ ) {
            miRowChunk = MiRow + ( chunkY << 4 )
            miColChunk = MiCol + ( chunkX << 4 )
            subBlockMiRow = miRowChunk & sbMask
            subBlockMiCol = miColChunk & sbMask

            for ( plane = 0; plane < 1 + COVERCLASS(1, (PROFILE0 || PROFILE2), HasChroma ) * 2; plane++ ) {
#if VALIDATE_SPEC_STRUCTURE
                validate(60000)
                validate(plane)
                validate(chunkX)
                validate(chunkY)
#endif
                uint_0_18 txSz
                txSz = Lossless ? TX_4X4 : get_tx_size( plane, TxSize )
                stepX = Tx_Width[ txSz ] >> 2
                stepY = Tx_Height[ txSz ] >> 2
                COVERCROSS(VALUE_TX_WIDTH, txSz)
                COVERCROSS(VALUE_TX_HEIGHT, txSz)
                uint_0_21 planeSz
                planeSz = get_plane_residual_size( miSizeChunk, plane )
                num4x4W = Num_4x4_Blocks_Wide[ planeSz ]
                num4x4H = Num_4x4_Blocks_High[ planeSz ]
                COVERCROSS(VALUE_NUM_4X4_BLOCKS_WIDE, planeSz)
                COVERCROSS(VALUE_NUM_4X4_BLOCKS_HIGH, planeSz)
                subX = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) : 0)
                subY = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) : 0)
                baseX = (miColChunk >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX)) * MI_SIZE
                baseY = (miRowChunk >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY)) * MI_SIZE
                log2W = MI_SIZE_LOG2 + Mi_Width_Log2[ planeSz ]
                log2H = MI_SIZE_LOG2 + Mi_Height_Log2[ planeSz ]
                candRow = (MiRow >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY)) << COVERCLASS((PROFILE0 || PROFILE2), 1, subY)
                candCol = (MiCol >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX)) << COVERCLASS((PROFILE0 || PROFILE2), 1, subX)
                if ( is_inter && !Lossless && !plane ) {
                    transform_tree( baseX, baseY, num4x4W * 4, num4x4H * 4 )
                } else {
                    baseXBlock = (MiCol >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX)) * MI_SIZE
                    baseYBlock = (MiRow >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY)) * MI_SIZE
                    for( y = 0; y < num4x4H; y += stepY )
                        for( x = 0; x < num4x4W; x += stepX )
                            transform_block( plane, baseXBlock, baseYBlock, txSz,
                                             x + ( ( chunkX << 4 ) >> subX ),
                                             y + ( ( chunkY << 4 ) >> subY ) )
                }
            }
        }
    }
}

// Transform Block Syntax
transform_block(plane, baseX, baseY, uint_0_18 txSz, x, y) {
    startX = baseX + 4 * x
    startY = baseY + 4 * y
//#if VALIDATE_SPEC_STRUCTURE
//    validate(30001)
//    validate(plane)
//    validate(startX)
//    validate(startY)
//#endif
    subX = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) : 0)
    subY = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) : 0)
    row = ( startY << COVERCLASS((PROFILE0 || PROFILE2), 1, subY ) ) >> MI_SIZE_LOG2
    col = ( startX << COVERCLASS((PROFILE0 || PROFILE2), 1, subX ) ) >> MI_SIZE_LOG2
    sbMask = use_128x128_superblock ? 31 : 15
    subBlockMiRow = row & sbMask
    subBlockMiCol = col & sbMask
    stepX = Tx_Width[ txSz ] >> MI_SIZE_LOG2
    stepY = Tx_Height[ txSz ] >> MI_SIZE_LOG2
    COVERCROSS(VALUE_TX_WIDTH, txSz)
    COVERCROSS(VALUE_TX_HEIGHT, txSz)
#if OUTPUT_TRANSFORM_BLOCKS
    :print "transform_block: {} {} {} {} {}".format(plane, startX, startY, stepX, stepY)
    :C printf("transform_block: %d %d %d %d %d\n", plane, startX, startY, stepX, stepY);
#endif

    maxX = (MiCols * MI_SIZE) >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX)
    maxY = (MiRows * MI_SIZE) >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY)
    if ( startX < maxX && startY < maxY ) {
        if ( !is_inter ) {
            if ( ( ( plane == 0 ) && PaletteSizeY ) ||
                 ( ( plane != 0 ) && PaletteSizeUV ) ) {
                predict_palette( plane, startX, startY, x, y, txSz )
            } else {
                isCfl = ( plane > 0 && UVMode == UV_CFL_PRED )
                if ( plane == 0 ) {
                    mode = YMode
                } else {
                    mode = ( isCfl ) ? DC_PRED : UVMode
                }
                log2W = Tx_Width_Log2[ txSz ]
                log2H = Tx_Height_Log2[ txSz ]
                COVERCROSS(VALUE_TX_WIDTH_LOG2, txSz)
                COVERCROSS(VALUE_TX_HEIGHT_LOG2, txSz)
                predict_intra( plane, startX, startY,
                               ( plane == 0 ? AvailL : AvailLChroma ) || x > 0,
                               ( plane == 0 ? AvailU : AvailUChroma ) || y > 0,
                               BlockDecoded[ plane ]
                                           [ ( subBlockMiRow >> subY ) - 1 ]
                                           [ ( subBlockMiCol >> subX ) + stepX ],
                               BlockDecoded[ plane ]
                                           [ ( subBlockMiRow >> subY ) + stepY ]
                                           [ ( subBlockMiCol >> subX ) - 1 ],
                               mode,
                               log2W, log2H )
                if ( isCfl ) {
                    predict_chroma_from_luma( plane, startX, startY, txSz )
                }
            }
            if (plane == 0) {
                MaxLumaW = startX + stepX * 4
                MaxLumaH = startY + stepY * 4
            }
        }
        if ( !skip ) {
            eob = coeffs( plane, startX, startY, txSz )
            if ( eob > 0 )
                reconstruct( plane, startX, startY, txSz )
#if VALIDATE_SPEC_SKIP
            else {
                for (i = 0; i < Tx_Height[ txSz ]; i++)
                    for (j = 0; j < Tx_Width[ txSz ]; j++) {
                        validate(20012)
                        validate(plane)
                        validate(i)
                        validate(j)
                        validate(startY+i)
                        validate(startX+j)
                        validate(CurrFrame[ plane ][ startY + i ][ startX + j ])
                    }
            }
#endif
        }
#if VALIDATE_SPEC_SKIP
        else {
            for (i = 0; i < Tx_Height[ txSz ]; i++)
                for (j = 0; j < Tx_Width[ txSz ]; j++) {
                    validate(20011)
                    validate(plane)
                    validate(i)
                    validate(j)
                    validate(startY+i)
                    validate(startX+j)
                    validate(CurrFrame[ plane ][ startY + i ][ startX + j ])
                }
        }
#endif
        for ( i = 0; i < stepY; i++ ) {
            for ( j = 0; j < stepX; j++ ) {
                LoopfilterTxSizes[ plane ]
                                 [ (row >> subY) + i ]
                                 [ (col >> subX) + j ] = txSz
                BlockDecoded[ plane ]
                            [ ( subBlockMiRow >> subY ) + i ]
                            [ ( subBlockMiCol >> subX ) + j ] = 1
            }
        }
    }
}

// Transform Tree Syntax
// transform_tree is used to read a number of transform blocks arranged in a transform tree.
transform_tree( startX, startY, w, h ) {
//#if VALIDATE_SPEC_STRUCTURE
//    validate(30001)
//    validate(plane)
//    validate(startX)
//    validate(startY)
//#endif
    maxX = (MiCols * MI_SIZE)
    maxY = (MiRows * MI_SIZE)
    if ( startX < maxX && startY < maxY ) {
        row = startY >> MI_SIZE_LOG2
        col = startX >> MI_SIZE_LOG2
        uint_0_18 lumaTxSz
        lumaTxSz = InterTxSizes[ row ][ col ]
        lumaW = Tx_Width[ lumaTxSz ]
        lumaH = Tx_Height[ lumaTxSz ]
        COVERCROSS(VALUE_TX_WIDTH, lumaTxSz)
        COVERCROSS(VALUE_TX_HEIGHT, lumaTxSz)
        uses64 = ( w == 64 || h == 64 )
        if ( w <= lumaW && h <= lumaH ) {
            txSz = find_tx_size( w, h )
            transform_block( 0, startX, startY, txSz, 0, 0 )
        } else {
            if ( w > h ) {
                transform_tree( startX, startY, w/2, h )
                transform_tree( startX + w / 2, startY, w/2, h )
            } else if ( w < h ) {
                transform_tree( startX, startY, w, h/2 )
                transform_tree( startX, startY + h/2, w, h/2 )
            } else {
                transform_tree( startX, startY, w/2, h/2 )
                transform_tree( startX + w/2, startY, w/2, h/2 )
                transform_tree( startX, startY + h/2, w/2, h/2 )
                transform_tree( startX + w/2, startY + h/2, w/2, h/2 )
            }
        }
    }
}

// where find_tx_size finds the transform size matching the given dimensions and is defined as:

find_tx_size( w, h ) {
    uint_0_18 txSz
    txSz = 0
    while (1) {
        ASSERT( txSz < TX_SIZES_ALL, "Invalid tx size")
#if COVER
        COVERCROSS(VALUE_TX_WIDTH, txSz)
        if ( Tx_Width[ txSz ] == w ) {
          COVERCROSS(VALUE_TX_HEIGHT, txSz)
        }
#endif // COVER
        if ( Tx_Width[ txSz ] == w && Tx_Height[ txSz ] == h )
            break
        txSz++
    }
    return txSz
}

// Get TX Size Function
get_tx_size( plane, txSz ) {
    if ( plane == 0 )
        return txSz
    uint_0_18 uvTx
    uvTx = Max_Tx_Size_Rect[ get_plane_residual_size( MiSize, plane ) ]
    COVERCROSS(VALUE_TX_WIDTH, uvTx)
    if ( Tx_Width[ uvTx ] != 64 ) {
      COVERCROSS(VALUE_TX_HEIGHT, uvTx)
    }
    if ( Tx_Width[ uvTx ] == 64 || Tx_Height[ uvTx ] == 64 ){
        if ( COVERCLASS((PROFILE1 || PROFILE2), 1, Tx_Width[ uvTx ] == 16) ) {
            return TX_16X32
        }
        if ( COVERCLASS((PROFILE1 || PROFILE2), 1, Tx_Height[ uvTx ] == 16) ) {
            return TX_32X16
        }
        return TX_32X32
    }
    return uvTx
}

// Get Plane Residual Size Function
// The get_plane_residual_size returns the size of a residual block for the specified plane. (The residual block will always have width and height at least equal to 4.)
get_plane_residual_size( subsize, plane ) {
#if COVER
    uint1 subx
    uint1 suby
#endif // COVER
    subx = COVERCLASS((PROFILE0 || PROFILE2), 1, plane > 0 ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) : 0 )
    suby = COVERCLASS((PROFILE0 || PROFILE2), 1, plane > 0 ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) : 0 )
#if COVER
    uint_0_21 SubSize
    SubSize = subsize
    COVERCROSS(CROSS_SUBSAMPLED_SIZE, SubSize, subx, suby)
#endif // COVER
    return Subsampled_Size[ subsize ][ subx ][ suby ]
}

// The Subsampled_Size table is defined as:

int Subsampled_Size[ BLOCK_SIZES ][ 2 ][ 2 ] = {
  { { BLOCK_4X4,    BLOCK_4X4},      {BLOCK_4X4,     BLOCK_4X4} },
  { { BLOCK_4X8,    BLOCK_4X4},      {BLOCK_INVALID, BLOCK_4X4} },
  { { BLOCK_8X4,    BLOCK_INVALID},  {BLOCK_4X4,     BLOCK_4X4} },
  { { BLOCK_8X8,    BLOCK_8X4},      {BLOCK_4X8,     BLOCK_4X4} },
  { {BLOCK_8X16,    BLOCK_8X8},      {BLOCK_INVALID, BLOCK_4X8} },
  { {BLOCK_16X8,    BLOCK_INVALID},  {BLOCK_8X8,     BLOCK_8X4} },
  { {BLOCK_16X16,   BLOCK_16X8},     {BLOCK_8X16,    BLOCK_8X8} },
  { {BLOCK_16X32,   BLOCK_16X16},    {BLOCK_INVALID, BLOCK_8X16} },
  { {BLOCK_32X16,   BLOCK_INVALID},  {BLOCK_16X16,   BLOCK_16X8} },
  { {BLOCK_32X32,   BLOCK_32X16},    {BLOCK_16X32,   BLOCK_16X16} },
  { {BLOCK_32X64,   BLOCK_32X32},    {BLOCK_INVALID, BLOCK_16X32} },
  { {BLOCK_64X32,   BLOCK_INVALID},  {BLOCK_32X32,   BLOCK_32X16} },
  { {BLOCK_64X64,   BLOCK_64X32},    {BLOCK_32X64,   BLOCK_32X32} },
  { {BLOCK_64X128,  BLOCK_64X64},    {BLOCK_INVALID, BLOCK_32X64} },
  { {BLOCK_128X64,  BLOCK_INVALID},  {BLOCK_64X64,   BLOCK_64X32} },
  { {BLOCK_128X128, BLOCK_128X64},   {BLOCK_64X128,  BLOCK_64X64} },
  { {BLOCK_4X16,    BLOCK_4X8},      {BLOCK_INVALID, BLOCK_4X8} },
  { {BLOCK_16X4,    BLOCK_INVALID},  {BLOCK_8X4,     BLOCK_8X4} },
  { {BLOCK_8X32,    BLOCK_8X16},     {BLOCK_INVALID, BLOCK_4X16} },
  { {BLOCK_32X8,    BLOCK_INVALID},  {BLOCK_16X8,    BLOCK_16X4} },
  { {BLOCK_16X64,   BLOCK_16X32},    {BLOCK_INVALID, BLOCK_8X32} },
  { {BLOCK_64X16,   BLOCK_INVALID},  {BLOCK_32X16,   BLOCK_32X8} }
};

int Mag_Ref_Offset_With_Tx_Class[ 3 ][ 3 ][ 2 ] = {
  { { 0, 1 }, { 1, 0 }, { 1, 1 } },
  { { 0, 1 }, { 1, 0 }, { 0, 2 } },
  { { 0, 1 }, { 1, 0 }, { 2, 0 } }
};

// Coefficients Syntax
coeffs( plane, startX, startY, uint_0_18 txSz ) {
    uint1 all_zero
    uint_0_4 eob_pt_16
    uint_0_5 eob_pt_32
    uint_0_6 eob_pt_64
    uint3 eob_pt_128
    uint_0_8 eob_pt_256
    uint_0_9 eob_pt_512
    uint_0_10 eob_pt_1024
    uint1 eob_extra
    uint_0_2 coeff_base_eob
    uint2 coeff_base
    uint2 coeff_br
    uint1 dc_sign
    uint_0_4 txSzCtx
    uint1 ptype
    uint1 sign_bit
    uint1 eob_extra_bit
    uint1 golomb_length_bit
    uint1 golomb_data_bit
#if VALIDATE_SPEC_STRUCTURE
    validate(30000)
    validate(plane)
    validate(startX)
    validate(startY)
    validate(txSz)
#endif
    int scan[ 1024 ]

    x4 = startX >> 2
    y4 = startY >> 2
    w4 = Tx_Width[ txSz ] >> 2
    h4 = Tx_Height[ txSz ] >> 2

    COVERCROSS(VALUE_TX_SIZE_SQR, txSz)
    COVERCROSS(VALUE_TX_SIZE_SQR_UP, txSz)
    txSzCtx = ( Tx_Size_Sqr[txSz] + Tx_Size_Sqr_Up[txSz] + 1 ) >> 1
    ptype = plane > 0
    segEob = ( txSz == TX_16X64 || txSz == TX_64X16 ) ? 512 :
                Min( 1024, Tx_Width[ txSz ] * Tx_Height[ txSz ] )
#if COVER
    if (!( txSz == TX_16X64 || txSz == TX_64X16 )) {
      COVERCROSS(VALUE_TX_WIDTH, txSz)
      COVERCROSS(VALUE_TX_HEIGHT, txSz)
    }
#endif // COVER

    for ( c = 0; c < segEob; c++ ) {
        Quant[ c ] = 0

    }
    for ( i = 0; i < 64; i++ )
        for ( j = 0; j < 64; j++ )
            Dequant[ i ][ j ] = 0

    eob = 0
    culLevel = 0
    dcCategory = 0

    all_zero                                                                 ae(v)
    COVERCROSS(VALUE_CABAC_ALL_ZERO, all_zero)
    if ( all_zero ) {
        c = 0
        if ( plane == 0 ) {
            for ( i = 0; i < w4; i++ ) {
                for ( j = 0; j < h4; j++ ) {
                    TxTypes[ y4 + j ][ x4 + i ] = DCT_DCT
                }
            }
        }
    } else {
        if ( plane == 0 )
            transform_type( x4, y4, txSz )
        PlaneTxType = compute_tx_type( plane, txSz, x4, y4 )
        get_scan( plane, txSz, scan )

        eobMultisize = Min( Tx_Width_Log2[ txSz ], 5) + Min( Tx_Height_Log2[ txSz ], 5) - 4
        COVERCROSS(VALUE_TX_WIDTH_LOG2, txSz)
        COVERCROSS(VALUE_TX_HEIGHT_LOG2, txSz)
        if ( eobMultisize == 0 ) {
              eob_pt_16                                                        ae(v)
              COVERCROSS(VALUE_CABAC_EOB_PT_16, eob_pt_16)
            eobPt = eob_pt_16 + 1
        } else if ( eobMultisize == 1 ) {
              eob_pt_32                                                        ae(v)
              COVERCROSS(VALUE_CABAC_EOB_PT_32, eob_pt_32)
            eobPt = eob_pt_32 + 1
        } else if ( eobMultisize == 2 ) {
              eob_pt_64                                                        ae(v)
              COVERCROSS(VALUE_CABAC_EOB_PT_64, eob_pt_64)
            eobPt = eob_pt_64 + 1
        } else if ( eobMultisize == 3 ) {
              eob_pt_128                                                       ae(v)
              COVERCROSS(VALUE_CABAC_EOB_PT_128, eob_pt_128)
            eobPt = eob_pt_128 + 1
        } else if ( eobMultisize == 4 ) {
              eob_pt_256                                                       ae(v)
              COVERCROSS(VALUE_CABAC_EOB_PT_256, eob_pt_256)
            eobPt = eob_pt_256 + 1
        } else if ( eobMultisize == 5 ) {
              eob_pt_512                                                       ae(v)
              COVERCROSS(VALUE_CABAC_EOB_PT_512, eob_pt_512)
            eobPt = eob_pt_512 + 1
        } else {
              eob_pt_1024                                                      ae(v)
              COVERCROSS(VALUE_CABAC_EOB_PT_1024, eob_pt_1024)
            eobPt = eob_pt_1024 + 1
        }

        eob = ( eobPt < 2 ) ? eobPt : ( ( 1 << ( eobPt - 2 ) ) + 1 )
        eobShift = Max( -1, eobPt - 3 )
        if ( eobShift >= 0 ) {
            eob_extra                                                        ae(v)
            COVERCROSS(VALUE_CABAC_EOB_EXTRA, eob_extra)
            if ( eob_extra ) {
                eob += ( 1 << eobShift )
            }

            for ( i = 1; i < Max( 0, eobPt - 2 ); i++ ) {
                eobShift = Max( 0, eobPt - 2 ) - 1 - i
                eob_extra_bit                                                ae(v)
                COVERCROSS(VALUE_CABAC_EOB_EXTRA_BIT, eob_extra_bit)
                if ( eob_extra_bit ) {
                    eob += ( 1 << eobShift )
                }
            }
        }
        for ( c = eob - 1; c >= 0; c-- ) {
            pos = scan[ c ]
            if ( c == ( eob - 1 ) ) {
                coeff_base_eob                                               ae(v)
                COVERCROSS(VALUE_CABAC_COEFF_BASE_EOB, coeff_base_eob)
                llevel = coeff_base_eob + 1
            } else {
                coeff_base                                                   ae(v)
                COVERCROSS(VALUE_CABAC_COEFF_BASE, coeff_base)
                llevel = coeff_base
            }

            if ( llevel > NUM_BASE_LEVELS ) {
                for ( idx = 0;
                      idx < COEFF_BASE_RANGE / ( BR_CDF_SIZE - 1 );
                      idx++ ) {
                    coeff_br                                                 ae(v)
                    COVERCROSS(VALUE_CABAC_COEFF_BR, coeff_br)
                    llevel += coeff_br
                    if ( coeff_br < ( BR_CDF_SIZE - 1 ) )
                        break
                }
            }
            Quant[ pos ] = llevel
        }

        for ( c = 0; c < eob; c++ ) {
            pos = scan[ c ]
            if ( Quant[ pos ] != 0 ) {
                if ( c == 0 ) {
                      dc_sign                                                  ae(v)
                    COVERCROSS(VALUE_CABAC_DC_SIGN, dc_sign)
                    sign = dc_sign
                } else {
                      sign_bit                                                 ae(v)
                    COVERCROSS(VALUE_CABAC_SIGN_BIT, sign_bit)
                    sign = sign_bit
                }
            } else {
                sign = 0
            }
            if ( Quant[ pos ] >
                ( NUM_BASE_LEVELS + COEFF_BASE_RANGE ) ) {
                length = 0
                do {
                    length++
                    golomb_length_bit                                          ae(v)
                    COVERCROSS(VALUE_CABAC_GOLOMB_LENGTH_BIT, golomb_length_bit)
                    CHECK( length<20 || golomb_length_bit==1, "If length is equal to 20, it is a requirement of bitstream conformance that golomb_length_bit is equal to 1")
                } while ( !golomb_length_bit )
                x = 1
                for ( i = length - 2; i >= 0; i-- ) {
                    golomb_data_bit                                            ae(v)
                    COVERCROSS(VALUE_CABAC_GOLOMB_DATA_BIT, golomb_data_bit)
                    x = ( x << 1 ) | golomb_data_bit
                }
               Quant[ pos ] = x + COEFF_BASE_RANGE + NUM_BASE_LEVELS
            }
            if ( pos == 0 && Quant[ pos ] > 0 ) {
                dcCategory = sign ? 1 : 2
            }
            Quant[ pos ] = Quant[ pos ] & 0xFFFFF

            culLevel += Quant[ pos ]

            if ( sign )
                Quant[ pos ] = - Quant[ pos ]
#if VALIDATE_SPEC_QUANT
            if ( txSz == TX_32X32 || txSz == TX_16X32 || txSz == TX_32X16 || txSz == TX_16X64 || txSz == TX_64X16 )
                jdqDenom = 2
            else if ( txSz == TX_64X64 || txSz == TX_32X64 || txSz == TX_64X32 )
                jdqDenom = 4
            else
                jdqDenom = 1
            validate(20000)
            jq = (c == 0) ? get_dc_quant( plane ) : get_ac_quant( plane )
            validate(jq)
            jq2 = jq
            if (using_qmatrix == 1 && PlaneTxType < IDTX && SegQMLevel[ plane ][ segment_id ] < 15) {
                validate(999)
                validate(SegQMLevel[ plane ][ segment_id ])
                validate(get_qm_offset(txSz))
                validate(pos)
                jq2 = Round2( jq * get_quantizer_matrix( SegQMLevel[ plane ][ segment_id ], plane > 0, get_qm_offset( txSz ) + pos ), 5 )
            }
            validate(jq2)
            validate(Quant[ pos ])
            validate( culLevel )
            jdq = Abs( Quant[ pos ] * jq2 ) & 0xFFFFFF

            validate(jdq)
            jdq = jdq/jdqDenom
            validate(jdq)
            if (sign) jdq = -jdq
            validate(Clip3( - ( 1 << ( 7 + BitDepth ) ), ( 1 << ( 7 + BitDepth ) ) - 1, jdq ))
#endif

        }
        culLevel = Min( 63, culLevel )

    }

    for( i = 0; i < w4; i++ ) {
        AboveLevelContext[ plane ][ x4 + i ] = culLevel
        AboveDcContext[ plane ][ x4 + i ] = dcCategory
    }
    for( i = 0; i < h4; i++ ) {
        LeftLevelContext[ plane ][ y4 + i ] = culLevel
        LeftDcContext[ plane ][ y4 + i ] = dcCategory
    }
    return eob
}

get_tx_class( txType ) {
    if ( ( txType == V_DCT ) ||
         ( txType == V_ADST ) ||
         ( txType == V_FLIPADST ) ) {
        return TX_CLASS_VERT
    } else if ( ( txType == H_DCT ) ||
                ( txType == H_ADST ) ||
                ( txType == H_FLIPADST ) ) {
        return TX_CLASS_HORIZ
    } else
        return TX_CLASS_2D
}

is_tx_type_in_set( txSet, txType ) {
#if COVER
    uint_0_15 txTypeInSet
    txTypeInSet = txType
    if (is_inter) {
      uint_0_3 txSetInter
      txSetInter = txSet
      COVERCROSS(CROSS_TX_TYPE_IN_SET_INTER, txSetInter, txTypeInSet)
    } else {
      uint_0_2 txSetIntra
      txSetIntra = txSet
      COVERCROSS(CROSS_TX_TYPE_IN_SET_INTRA, txSetIntra, txTypeInSet)
    }
#endif //COVER
    return is_inter ? Tx_Type_In_Set_Inter[ txSet ][ txType ] :
                      Tx_Type_In_Set_Intra[ txSet ][ txType ]
}

// Compute Transform Type Function
compute_tx_type( plane, txSz, blockX, blockY ) {
    txSzSqrUp = Tx_Size_Sqr_Up[ txSz ]

    if ( Lossless || txSzSqrUp > TX_32X32 )
        return DCT_DCT

    txSet = get_tx_set( txSz )

    if ( plane == 0 ) {
        return TxTypes[ blockY ][ blockX ]
    }

    if ( is_inter ) {
        x4 = Max( MiCol, blockX << COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) )
        y4 = Max( MiRow, blockY << COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) )
        txType = TxTypes[ y4 ][ x4 ]
        if ( !is_tx_type_in_set( txSet, txType ) )
            return DCT_DCT
        return txType
    }

    txType = Mode_To_Txfm[ UVMode ]
#if COVER
    uint_0_13 txType_UVMode
    txType_UVMode = UVMode
    COVERCROSS(VALUE_MODE_TO_TXFM, txType_UVMode)
#endif // COVER
    if ( !is_tx_type_in_set( txSet, txType ) )
        return DCT_DCT
    return txType
}

// Intra Angle Info Luma Syntax
intra_angle_info_y( ) {
    uint_0_5 angle_delta_y
    AngleDeltaY = 0
    if ( MiSize >= BLOCK_8X8 ) {
        if ( is_directional_mode( YMode ) ) {
            angle_delta_y                                                    ae(v)
            COVERCROSS(VALUE_CABAC_ANGLE_DELTA_Y, angle_delta_y)
            AngleDeltaY = angle_delta_y - MAX_ANGLE_DELTA
        }
    }
}

// Intra Angle Info Chroma Syntax
intra_angle_info_uv( ) {
    uint_0_5 angle_delta_uv
    AngleDeltaUV = 0
    if ( MiSize >= BLOCK_8X8 ) {
        if ( is_directional_mode( UVMode ) ) {
            angle_delta_uv                                                   ae(v)
            COVERCROSS(VALUE_CABAC_ANGLE_DELTA_UV, angle_delta_uv)
            AngleDeltaUV = angle_delta_uv - MAX_ANGLE_DELTA
        }
    }
}

// Is Directional Mode Function
is_directional_mode( mode ) {
    if ( ( mode >= V_PRED ) && ( mode <= D67_PRED ) ) {
        return 1
    }
    return 0
}

// Read CFL Alphas Syntax
read_cfl_alphas() {
    uint3 cfl_alpha_signs
    uint4 cfl_alpha_u
    uint4 cfl_alpha_v
      cfl_alpha_signs                                                          ae(v)
    COVERCROSS(VALUE_CABAC_CFL_ALPHA_SIGNS, cfl_alpha_signs)
    signU = (cfl_alpha_signs + 1 ) / 3
    signV = (cfl_alpha_signs + 1 ) % 3
    CHECK( !( signU == CFL_SIGN_ZERO && signV == CFL_SIGN_ZERO ), "The combination of two zero signs is prohibited as it is redundant with DC Intra prediction")
    if (signU != CFL_SIGN_ZERO) {
          cfl_alpha_u                                                          ae(v)
        COVERCROSS(VALUE_CABAC_CFL_ALPHA_U, cfl_alpha_u)
        CflAlphaU = 1 + cfl_alpha_u
        if (signU == CFL_SIGN_NEG)
            CflAlphaU = -CflAlphaU
    } else {
      CflAlphaU = 0
    }
    if (signV != CFL_SIGN_ZERO) {
          cfl_alpha_v                                                          ae(v)
        COVERCROSS(VALUE_CABAC_CFL_ALPHA_V, cfl_alpha_v)
        CflAlphaV = 1 + cfl_alpha_v
        if (signV == CFL_SIGN_NEG)
            CflAlphaV = -CflAlphaV
    } else {
      CflAlphaV = 0
    }
}

// Palette Mode Info Syntax
palette_mode_info( ) {
    uint1 has_palette_y
    uint_0_6 palette_size_y_minus_2
    uint1 has_palette_uv
    uint_0_6 palette_size_uv_minus_2
    uint_0_6 bsizeCtx
    uint1 use_palette_color_cache_y
    uint2 palette_num_extra_bits_y
    uint2 palette_num_extra_bits_u
    uint2 palette_num_extra_bits_v
    uint12 r_palette_delta_y
    uint12 r_palette_delta_u
    uint11 r_palette_delta_v
    uint1 use_palette_color_cache_u
    uint1 delta_encode_palette_colors_v
    uint1 palette_delta_sign_bit_v
    paletteBits = 0
    bsizeCtx = Mi_Width_Log2[ MiSize ] + Mi_Height_Log2[ MiSize ] - 2
    if ( YMode == DC_PRED ) {
          has_palette_y                                                        ae(v)
        COVERCROSS(VALUE_CABAC_HAS_PALETTE_Y, has_palette_y)
        if ( has_palette_y ) {
              palette_size_y_minus_2                                           ae(v)
            COVERCROSS(VALUE_CABAC_PALETTE_SIZE_Y_MINUS_2, palette_size_y_minus_2)
            PaletteSizeY = palette_size_y_minus_2 + 2
            cacheN = get_palette_cache( 0 )
            idx = 0
            for ( i = 0; i < cacheN && idx < PaletteSizeY; i++ ) {
                  use_palette_color_cache_y                                    ae(v)
                COVERCROSS(VALUE_CABAC_USE_PALETTE_COLOR_CACHE_Y, use_palette_color_cache_y)
                if ( use_palette_color_cache_y ) {
                    palette_colors_y[ idx ] = PaletteCache[ i ]
                    idx++
                }
            }
            if ( idx < PaletteSizeY ) {
                  palette_colors_y[ idx ]                                      ae(v)
                COVERCROSS(VALUE_CABAC_PALETTE_COLORS_Y, palette_colors_y[ idx ])
                idx++
            }
            if ( idx < PaletteSizeY ) {
                minBits = BitDepth - 3
                  palette_num_extra_bits_y                                     ae(v)
                COVERCROSS(VALUE_CABAC_PALETTE_NUM_EXTRA_BITS_Y, palette_num_extra_bits_y)
                paletteBits = minBits + palette_num_extra_bits_y
            }
            while ( idx < PaletteSizeY ) {
                  r_palette_delta_y                                            ae(v)
                COVERCROSS(VALUE_CABAC_PALETTE_DELTA_Y, r_palette_delta_y)
                palette_delta_y = r_palette_delta_y
                palette_delta_y++
                palette_colors_y[ idx ] =
                          Clip1( palette_colors_y[ idx - 1 ] +
                                 palette_delta_y )
                range = ( 1 << BitDepth ) - palette_colors_y[ idx ] - 1
                paletteBits = Min( paletteBits, CeilLog2( range ) )
                idx++
            }
            sort( palette_colors_y, 0, PaletteSizeY - 1 )
        }
    }
    if ( COVERCLASS(1, (PROFILE0 || PROFILE2), HasChroma ) && UVMode == DC_PRED ) {
          has_palette_uv                                                       ae(v)
        COVERCROSS(VALUE_CABAC_HAS_PALETTE_UV, has_palette_uv)
        if ( has_palette_uv ) {
              palette_size_uv_minus_2                                          ae(v)
            COVERCROSS(VALUE_CABAC_PALETTE_SIZE_UV_MINUS_2, palette_size_uv_minus_2)
            PaletteSizeUV = palette_size_uv_minus_2 + 2
            cacheN = get_palette_cache( 1 )
            idx = 0
            for ( i = 0; i < cacheN && idx < PaletteSizeUV; i++ ) {
                  use_palette_color_cache_u                                    ae(v)
                COVERCROSS(VALUE_CABAC_USE_PALETTE_COLOR_CACHE_U, use_palette_color_cache_u)
                if ( use_palette_color_cache_u ) {
                    palette_colors_u[ idx ] = PaletteCache[ i ]
                    idx++
                }
            }
            if ( idx < PaletteSizeUV ) {
                  palette_colors_u[ idx ]                                      ae(v)
                COVERCROSS(VALUE_CABAC_PALETTE_COLORS_U, palette_colors_u[ idx ])
                idx++
            }
            if ( idx < PaletteSizeUV ) {
                minBits = BitDepth - 3
                  palette_num_extra_bits_u                                     ae(v)
                COVERCROSS(VALUE_CABAC_PALETTE_NUM_EXTRA_BITS_U, palette_num_extra_bits_u)
                paletteBits = minBits + palette_num_extra_bits_u
            }
            while ( idx < PaletteSizeUV ) {
                  r_palette_delta_u                                            ae(v)
                COVERCROSS(VALUE_CABAC_PALETTE_DELTA_U, r_palette_delta_u)
                palette_delta_u = r_palette_delta_u
                palette_colors_u[ idx ] =
                          Clip1( palette_colors_u[ idx - 1 ] +
                                 palette_delta_u )
                range = ( 1 << BitDepth ) - palette_colors_u[ idx ]
                paletteBits = Min( paletteBits, CeilLog2( range ) )
                idx++
            }
            sort( palette_colors_u, 0, PaletteSizeUV - 1 )

              delta_encode_palette_colors_v                                    ae(v)
            COVERCROSS(VALUE_CABAC_DELTA_ENCODE_PALETTE_COLORS_V, delta_encode_palette_colors_v)
            if ( delta_encode_palette_colors_v ) {
                minBits = BitDepth - 4
                maxVal = 1 << BitDepth
                  palette_num_extra_bits_v                                     ae(v)
                COVERCROSS(VALUE_CABAC_PALETTE_NUM_EXTRA_BITS_V, palette_num_extra_bits_v)
                paletteBits = minBits + palette_num_extra_bits_v
                  palette_colors_v[ 0 ]                                        ae(v)
                COVERCROSS(VALUE_CABAC_PALETTE_COLORS_V, palette_colors_v[ 0 ])
                for ( idx = 1; idx < PaletteSizeUV; idx++ ) {
                      r_palette_delta_v                                        ae(v)
                    COVERCROSS(VALUE_CABAC_PALETTE_DELTA_V, r_palette_delta_v)
                    palette_delta_v = r_palette_delta_v
                    if ( palette_delta_v ) {
                          palette_delta_sign_bit_v                             ae(v)
                        COVERCROSS(VALUE_CABAC_PALETTE_DELTA_SIGN_BIT_V, palette_delta_sign_bit_v)
                        if ( palette_delta_sign_bit_v ) {
                            palette_delta_v = -palette_delta_v
                        }
                    }
                    val = palette_colors_v[ idx - 1 ] + palette_delta_v
                    if ( val < 0 ) val += maxVal
                    if ( val >= maxVal ) val -= maxVal
                    palette_colors_v[ idx ] = Clip1( val )
                }
            } else {
                for ( idx = 0; idx < PaletteSizeUV; idx++ ) {
                      palette_colors_v[ idx ]                                  ae(v)
                    COVERCROSS(VALUE_CABAC_PALETTE_COLORS_V, palette_colors_v[ idx ])
                }
            }
        }
    }
}

// where the function get_palette_cache, which merges the above and left palettes to form a cache, is specified as follows:

get_palette_cache( plane ) {
    aboveN = 0
    if ( ( MiRow * MI_SIZE ) % 64 ) {
        aboveN = PaletteSizes[ plane ][ MiRow - 1 ][ MiCol ]
    }
    leftN = 0
    if ( AvailL ) {
        leftN = PaletteSizes[ plane ][ MiRow ][ MiCol - 1 ]
    }
    aboveIdx = 0
    leftIdx = 0
    n = 0
    while ( aboveIdx < aboveN  && leftIdx < leftN ) {
        aboveC = PaletteColors[ plane ][ MiRow - 1 ][ MiCol ][ aboveIdx ]
        leftC = PaletteColors[ plane ][ MiRow ][ MiCol - 1 ][ leftIdx ]
        if ( leftC < aboveC ) {
            // Note: The "if (leftC == aboveC)" block in the other branch means that
            // we can never end up with leftC != aboveC but leftC == PaletteCache[ n - 1 ].
            if ( n == 0 || COVERCLASS(1,IMPOSSIBLE,leftC != PaletteCache[ n - 1 ]) ) {
                PaletteCache[ n ] = leftC
                n++
            }
            leftIdx++
        } else {
            // Note: In contrast to above, this branch is coverable: we *can* have
            // aboveC == PaletteCache[ n - 1 ]. However, this can only happen if
            // leftC == aboveC == PaletteCache[ n - 1 ] == ((1 << bit_depth) - 1).
            //
            // This is because the delta encoding prevents us from including duplicate
            // values within a single palette, *except* that we can have multiple entries
            // of the maximum pixel value.
            if ( n == 0 || aboveC != PaletteCache[ n - 1 ] ) {
                PaletteCache[ n ] = aboveC
                n++
            }
            aboveIdx++
            if ( leftC == aboveC ) {
                leftIdx++
            }
        }
    }
    while ( aboveIdx < aboveN ) {
        val = PaletteColors[ plane ][ MiRow - 1 ][ MiCol ][ aboveIdx ]
        aboveIdx++
        if ( n == 0 || val != PaletteCache[ n - 1 ] ) {
            PaletteCache[ n ] = val
            n++
        }
    }
    while ( leftIdx < leftN ) {
        val = PaletteColors[ plane ][ MiRow ][ MiCol - 1 ][ leftIdx ]
        leftIdx++
        if ( n == 0 || val != PaletteCache[ n - 1 ] ) {
            PaletteCache[ n ] = val
            n++
        }
    }
    return n
}

// The function sort( arr, i1, i2 ) sorts a subarray of the array arr in-place into ascending order. The subarray to be sorted is between indices i1 and i2 inclusive.

sort( uint12pointer arr, int16 i1, int16 i2 ) {
    if (i1 < i2) {
        pt = qs_partition( arr, i1, i2 )
        sort( arr, i1, pt - 1 )
        sort( arr, pt + 1, i2 )
    }
}

qs_partition( uint12pointer arr, i1, i2 ) {
    uint12 pivot
    uint12 tmp
    pivot = arr[ i2 ]
    i = i1 - 1
    for (j = i1; j < i2; j++) {
        if ( arr[ j ] < pivot ) {
            i++
            tmp = arr[ i ]
            arr[ i ] = arr[ j ]
            arr[ j ] = tmp
        }
    }
    if ( arr[ i2 ] < arr[ i + 1 ] ) {
        tmp = arr[ i + 1 ]
        arr[ i + 1 ] = arr[ i2 ]
        arr[ i2 ] = tmp
    }
    return i + 1
}

int Tx_Type_Intra_Inv_Set1[ 7 ]  = { IDTX, DCT_DCT, V_DCT, H_DCT, ADST_ADST, ADST_DCT, DCT_ADST };
int Tx_Type_Intra_Inv_Set2[ 5 ]  = { IDTX, DCT_DCT, ADST_ADST, ADST_DCT, DCT_ADST };
int Tx_Type_Inter_Inv_Set1[ 16 ] = { IDTX, V_DCT, H_DCT, V_ADST, H_ADST, V_FLIPADST, H_FLIPADST,
                                     DCT_DCT, ADST_DCT, DCT_ADST, FLIPADST_DCT, DCT_FLIPADST, ADST_ADST,
                                     FLIPADST_FLIPADST, ADST_FLIPADST, FLIPADST_ADST };
int Tx_Type_Inter_Inv_Set2[ 12 ] = { IDTX, V_DCT, H_DCT, DCT_DCT, ADST_DCT, DCT_ADST, FLIPADST_DCT,
                                     DCT_FLIPADST, ADST_ADST, FLIPADST_FLIPADST, ADST_FLIPADST,
                                     FLIPADST_ADST };
int Tx_Type_Inter_Inv_Set3[ 2 ]  = { IDTX, DCT_DCT };

int Filter_Intra_Mode_To_Intra_Dir[ INTRA_FILTER_MODES ] = {
  DC_PRED, V_PRED, H_PRED, D157_PRED, DC_PRED
};

// Transform Type Syntax
transform_type( blockX, blockY, uint_0_18 txSz ) {
    uint4 inter_tx_type
    uint_0_6 intra_tx_type
    set = get_tx_set( txSz )

    if ( set > 0 &&
         ( segmentation_enabled ? get_qindex( 1, segment_id ) : base_q_idx ) > 0 ) {
        if ( is_inter ) {
            inter_tx_type                                                    ae(v)
            COVERCROSS(VALUE_CABAC_INTER_TX_TYPE, inter_tx_type)
            if ( set == TX_SET_INTER_1 ) {
#if COVER
                uint_0_15 interSet1TxType
                interSet1TxType = inter_tx_type
                COVERCROSS(VALUE_TX_TYPE_INTER_INV_SET1, interSet1TxType)
#endif // COVER
                TxType = Tx_Type_Inter_Inv_Set1[ inter_tx_type ]
            } else if ( set == TX_SET_INTER_2 ) {
#if COVER
                uint_0_11 interSet2TxType
                interSet2TxType = inter_tx_type
                COVERCROSS(VALUE_TX_TYPE_INTER_INV_SET2, interSet2TxType)
#endif // COVER
                TxType = Tx_Type_Inter_Inv_Set2[ inter_tx_type ]
            } else {
#if COVER
                uint1 interSet3TxType
                interSet3TxType = inter_tx_type
                COVERCROSS(VALUE_TX_TYPE_INTER_INV_SET3, interSet3TxType)
#endif // COVER
                TxType = Tx_Type_Inter_Inv_Set3[ inter_tx_type ]
            }
        } else {
            intra_tx_type                                                    ae(v)
            COVERCROSS(VALUE_CABAC_INTRA_TX_TYPE, intra_tx_type)
            if ( set == TX_SET_INTRA_1 ) {
#if COVER
                uint_0_6 intraSet1TxType
                intraSet1TxType = intra_tx_type
                COVERCROSS(VALUE_TX_TYPE_INTRA_INV_SET1, intraSet1TxType)
#endif // COVER
                TxType = Tx_Type_Intra_Inv_Set1[ intra_tx_type ]
            } else {
#if COVER
                uint_0_4 intraSet2TxType
                intraSet2TxType = intra_tx_type
                COVERCROSS(VALUE_TX_TYPE_INTRA_INV_SET2, intraSet2TxType)
#endif // COVER
                TxType = Tx_Type_Intra_Inv_Set2[ intra_tx_type ]
            }
        }
    } else {
        TxType = DCT_DCT
    }
    COVERCROSS(VALUE_TX_WIDTH, txSz)
    COVERCROSS(VALUE_TX_HEIGHT, txSz)
    for ( i = 0; i < ( Tx_Width[ txSz ] >> 2 ); i++ ) {
        for ( j = 0; j < ( Tx_Height[ txSz ] >> 2 ); j++ ) {
            TxTypes[ blockY + j ][ blockX + i ] = TxType
        }
    }
}

seg_feature_active( feature ) {
    return seg_feature_active_idx( segment_id, feature )
}

// Get Transform Set Function
get_tx_set( uint_0_18 txSz ) {
    COVERCROSS(VALUE_TX_SIZE_SQR, txSz)
    COVERCROSS(VALUE_TX_SIZE_SQR_UP, txSz)
    txSzSqr = Tx_Size_Sqr[ txSz ]
    txSzSqrUp = Tx_Size_Sqr_Up[ txSz ]
    if ( txSzSqrUp > TX_32X32 )
        return TX_SET_DCTONLY
    if ( is_inter ) {
        if ( reduced_tx_set || txSzSqrUp == TX_32X32 ) return TX_SET_INTER_3
        else if ( txSzSqr == TX_16X16 ) return TX_SET_INTER_2
        return TX_SET_INTER_1
    } else {
        if ( txSzSqrUp == TX_32X32 ) return TX_SET_DCTONLY
        else if ( reduced_tx_set ) return TX_SET_INTRA_2
        else if ( txSzSqr == TX_16X16 ) return TX_SET_INTRA_2
        return TX_SET_INTRA_1
    }
}

// Palette Tokens Syntax
palette_tokens( ) {
    uint3 palette_color_idx_y
    uint3 palette_color_idx_uv
    uint3 color_index_map_y
    uint3 color_index_map_uv
    blockHeight = Block_Height[ MiSize ]
    blockWidth = Block_Width[ MiSize ]
    onscreenHeight = Min( blockHeight, (MiRows - MiRow) * MI_SIZE )
    onscreenWidth = Min( blockWidth, (MiCols - MiCol) * MI_SIZE )

    if ( PaletteSizeY ) {
          color_index_map_y                                                    ae(v)
        COVERCROSS(VALUE_CABAC_COLOR_INDEX_MAP_Y, color_index_map_y)
        ColorMapY[0][0] = color_index_map_y
        for ( i = 1; i < onscreenHeight + onscreenWidth - 1; i++ ) {
            for ( j = Min( i, onscreenWidth - 1 ); j >= Max( 0, i - onscreenHeight + 1 ); j-- ) {
                get_palette_color_context( 1, ( i - j ), j, PaletteSizeY )
                  palette_color_idx_y                                   ae(v)
                COVERCROSS(VALUE_CABAC_PALETTE_COLOR_IDX_Y, palette_color_idx_y)
                ColorMapY[ i - j ][ j ] = ColorOrder[ palette_color_idx_y ]
            }
        }
        for ( i = 0; i < onscreenHeight; i++ ) {
            for ( j = onscreenWidth; j < blockWidth; j++ ) {
                ColorMapY[ i ][ j ] = ColorMapY[ i ][ onscreenWidth - 1 ]
            }
        }
        for ( i = onscreenHeight; i < blockHeight; i++ ) {
            for ( j = 0; j < blockWidth; j++ ) {
                ColorMapY[ i ][ j ] = ColorMapY[ onscreenHeight - 1 ][ j ]
            }
        }
    }

    if ( PaletteSizeUV ) {
          color_index_map_uv                                                   ae(v)
        COVERCROSS(VALUE_CABAC_COLOR_INDEX_MAP_UV, color_index_map_uv)
        ColorMapUV[0][0] = color_index_map_uv
        blockHeight = blockHeight >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
        blockWidth = blockWidth >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
        onscreenHeight = onscreenHeight >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
        onscreenWidth = onscreenWidth >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
        if ( COVERCLASS((PROFILE0 || PROFILE2), 1, blockWidth < 4) ) {
            blockWidth += 2
            onscreenWidth += 2
        }
        if ( COVERCLASS((PROFILE0 || PROFILE2), 1, blockHeight < 4) ) {
            blockHeight += 2
            onscreenHeight += 2
        }

        for ( i = 1; i < onscreenHeight + onscreenWidth - 1; i++ ) {
            for ( j = Min( i, onscreenWidth - 1 ); j >= Max( 0, i - onscreenHeight + 1 ); j-- ) {
                get_palette_color_context( 0, ( i - j ), j, PaletteSizeUV )
                  palette_color_idx_uv                                  ae(v)
                COVERCROSS(VALUE_CABAC_PALETTE_COLOR_IDX_UV, palette_color_idx_uv)
                ColorMapUV[ i - j ][ j ] = ColorOrder[ palette_color_idx_uv ]
            }
        }
        for ( i = 0; i < onscreenHeight; i++ ) {
            for ( j = onscreenWidth; j < blockWidth; j++ ) {
                ColorMapUV[ i ][ j ] = ColorMapUV[ i ][ onscreenWidth - 1 ]
            }
        }
        for ( i = onscreenHeight; i < blockHeight; i++ ) {
            for ( j = 0; j < blockWidth; j++ ) {
                ColorMapUV[ i ][ j ] = ColorMapUV[ onscreenHeight - 1 ][ j ]
            }
        }
    }
}

// Palette Colors Syntax
get_palette_color_context( useYColorMap, r, c, n ) {
    int scores[ PALETTE_COLORS ]

    for( i = 0; i < PALETTE_COLORS; i++ ) {
        scores[ i ] = 0
        ColorOrder[i] = i
    }
    if ( c > 0 ) {
        neighbor = useYColorMap ? ColorMapY[ r ][ c - 1 ] : ColorMapUV[ r ][ c - 1 ]
        scores[ neighbor ] += 2
    }
    if ( ( r > 0 ) && ( c > 0 ) ) {
        neighbor = useYColorMap ? ColorMapY[ r - 1 ][ c - 1 ] : ColorMapUV[ r - 1 ][ c - 1 ]
        scores[ neighbor ] += 1
    }
    if ( r > 0 ) {
        neighbor = useYColorMap ? ColorMapY[ r - 1 ][ c ] : ColorMapUV[ r - 1 ][ c ]
        scores[ neighbor ] += 2
    }
    for ( i = 0; i < PALETTE_NUM_NEIGHBORS; i++ ) {
        maxScore = scores[ i ]
        maxIdx = i
        for ( j = i + 1; j < n; j++ ) {
            if (scores[ j ] > maxScore) {
                maxScore = scores[ j ]
                maxIdx = j
            }
        }
        if ( maxIdx != i ) {
            maxScore = scores[ maxIdx ]
            maxColorOrder = ColorOrder[ maxIdx ]
            for ( k = maxIdx; k > i; k-- ) {
                scores[ k ] = scores[ k - 1 ]
                ColorOrder[ k ] = ColorOrder[ k - 1 ]
            }
            scores[ i ] = maxScore
            ColorOrder[ i ] = maxColorOrder
        }
    }
    ColorContextHash = 0
    for( i = 0; i < PALETTE_NUM_NEIGHBORS; i++ ) {
        ColorContextHash += scores[ i ] * Palette_Color_Hash_Multipliers[ i ]
#if COVER
        uint_0_2 get_palette_color_context_i
        get_palette_color_context_i = i
        COVERCROSS(VALUE_PALETTE_COLOR_HASH_MULTIPLIERS, get_palette_color_context_i)
#endif // COVER
    }
}

// Is Inside Function
// is_inside determines whether a candidate position is inside the current tile.
is_inside( candidateR, candidateC ) {
    return ( candidateC >= MiColStart &&
             candidateC < MiColEnd &&
             candidateR >= MiRowStart &&
             candidateR < MiRowEnd )
}

// Is Inside Filter Region Function

// is_inside_filter_region determines whether a candidate position is inside the region that is being used for CDEF filtering.

is_inside_filter_region( candidateR, candidateC ) {
    colStart = 0
    colEnd = MiCols
    rowStart = 0
    rowEnd = MiRows
    return (candidateC >= colStart &&
            candidateC < colEnd &&
            candidateR >= rowStart &&
            candidateR < rowEnd)
}

// Check References Functions
// This section defines some simple helper functions used in other parts of the specification:
check_backward(refFrame) {
  return ( (refFrame >= BWDREF_FRAME) && (refFrame <= ALTREF_FRAME) )
}


// Clamp MV Row Function
clamp_mv_row( mvec, border ) {
    bh4 = Num_4x4_Blocks_High[ MiSize ]
    mbToTopEdge = -((MiRow * MI_SIZE) * 8)
    mbToBottomEdge = ((MiRows - bh4 - MiRow) * MI_SIZE) * 8
    return Clip3( mbToTopEdge - border, mbToBottomEdge + border, mvec )
}

// Clamp MV Col Function
clamp_mv_col( mvec, border ) {
    bw4 = Num_4x4_Blocks_Wide[ MiSize ]
    mbToLeftEdge = -((MiCol * MI_SIZE) * 8)
    mbToRightEdge = ((MiCols - bw4 - MiCol) * MI_SIZE) * 8
    return Clip3( mbToLeftEdge - border, mbToRightEdge + border, mvec )
}

// Clear CDEF Function
clear_cdef( r, c ) {
    cdef_idx[ r ][ c ] = -1
    if ( use_128x128_superblock ) {
        cdefSize4 = Num_4x4_Blocks_Wide[ BLOCK_64X64 ]
        cdef_idx[ r ][ c + cdefSize4 ] = -1
        cdef_idx[ r + cdefSize4][ c ] = -1
        cdef_idx[ r + cdefSize4][ c + cdefSize4 ] = -1
    }
}

// Read CDEF Syntax
read_cdef( ) {
    uint3 r_cdef_idx
    if ( !skip && !CodedLossless && enable_cdef && !allow_intrabc ) {
        cdefSize4 = Num_4x4_Blocks_Wide[ BLOCK_64X64 ]
        cdefMask4 = ~(cdefSize4 - 1)
        r = MiRow & cdefMask4
        c = MiCol & cdefMask4
        if ( cdef_idx[ r ][ c ] == -1 ) {
            r_cdef_idx                                                       ae(v)
            COVERCROSS(VALUE_CABAC_CDEF_IDX, r_cdef_idx)
            cdef_idx[ r ][ c ] = r_cdef_idx
            w4 = Num_4x4_Blocks_Wide[ MiSize ]
            h4 = Num_4x4_Blocks_High[ MiSize ]
            for (i = r; i < r + h4 ; i += cdefSize4) {
                for (j = c; j < c + w4 ; j += cdefSize4) {
                    cdef_idx[ i ][ j ] = cdef_idx[ r ][ c ]
                }
            }
        }
    }
}

read_lr( r, c, bSize ) {
    uint_0_2 numLrParams
    if ( !allow_intrabc ) {
        w = Num_4x4_Blocks_Wide[ bSize ]
        h = Num_4x4_Blocks_High[ bSize ]
        for (plane = 0; plane < NumPlanes; plane++) {
            if (FrameRestorationType[ plane ] != RESTORE_NONE) {
                // continue does not work in Argon Streams code
                subX = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane == 0) ? 0 : COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x))
                subY = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane == 0) ? 0 : COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y))
                unitSize = LoopRestorationSize[ plane ]
                unitRows = count_units_in_frame( unitSize, Round2( FrameHeight, COVERCLASS((PROFILE0 || PROFILE2), 1, subY)) )
                unitCols = count_units_in_frame( unitSize, Round2( UpscaledWidth, COVERCLASS((PROFILE0 || PROFILE2), 1, subX)) )
                unitRowStart = ( r * ( MI_SIZE >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY)) + unitSize - 1 ) / unitSize
                unitRowEnd = Min( unitRows, ( (r + h) * ( MI_SIZE >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY)) + unitSize - 1 ) / unitSize)
                if ( use_superres ) {
                    numerator = (MI_SIZE >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX)) * SuperresDenom
                    denominator = unitSize * SUPERRES_NUM
                } else {
                    numerator = MI_SIZE >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX)
                    denominator = unitSize
                }
                unitColStart = ( c * numerator + denominator - 1 ) / denominator
                unitColEnd = Min( unitCols, ( (c + w) * numerator + denominator - 1 ) / denominator)
                numLrParams = Max(0, (unitRowEnd - unitRowStart) * (unitColEnd - unitColStart))
                COVERCROSS(VALUE_NUM_LR_PARAMS_IN_SB, numLrParams)
                if (FrameRestorationType[ plane ] != RESTORE_NONE) {
                    for ( unitRow = unitRowStart; unitRow < unitRowEnd; unitRow++ ) {
                        for ( unitCol = unitColStart; unitCol < unitColEnd; unitCol++ ) {
                            read_lr_unit( plane, unitRow, unitCol )
                        }
                    }
                }
            }
        }
    }
}

count_units_in_frame(unitSize, frameSize) {
    return Max((frameSize + (unitSize >> 1)) / unitSize, 1)
}

int Wiener_Taps_Min[3] = { -5, -23, -17 };
int Wiener_Taps_Max[3] = { 10,   8,  46 };
int Wiener_Taps_K[3] = { 1, 2, 3 };

int Sgrproj_Xqd_Min[2] = { -96, -32 };
int Sgrproj_Xqd_Max[2] = {  31,  95 };

read_lr_unit(plane, unitRow, unitCol) {
    uint1 use_wiener
    uint1 use_sgrproj
    uint_0_2 restoration_type
    uint4 lr_sgr_set
    if (FrameRestorationType[ plane ] == RESTORE_WIENER) {
        use_wiener                                                           ae(v)
        COVERCROSS(VALUE_CABAC_USE_WIENER, use_wiener)
        restoration_type = use_wiener ? RESTORE_WIENER : RESTORE_NONE
      } else if (FrameRestorationType[ plane ] == RESTORE_SGRPROJ) {
        use_sgrproj                                                          ae(v)
        COVERCROSS(VALUE_CABAC_USE_SGRPROJ, use_sgrproj)
        restoration_type = use_sgrproj ? RESTORE_SGRPROJ : RESTORE_NONE
    } else {
        restoration_type                                                     ae(v)
        COVERCROSS(VALUE_CABAC_RESTORATION_TYPE, restoration_type)
    }
    LrType[ plane ][ unitRow ][ unitCol ] = restoration_type
    if (restoration_type == RESTORE_WIENER) {
        for ( ppass = 0; ppass < 2; ppass++ ) {
            if (plane) {
                firstCoeff = 1
                LrWiener[ plane ]
                        [ unitRow ][ unitCol ][ ppass ][0] = 0
            } else {
                firstCoeff = 0
            }
            for (j = firstCoeff; j < 3; j++) {
                min = Wiener_Taps_Min[ j ]
                max = Wiener_Taps_Max[ j ]
                k = Wiener_Taps_K[ j ]
#if COVER
                uint_0_2 J
                J = j
                COVERCROSS(VALUE_WIENER_TAPS_MIN, J)
                COVERCROSS(VALUE_WIENER_TAPS_MAX, J)
                COVERCROSS(VALUE_WIENER_TAPS_K, J)
#endif // COVER
                v = decode_signed_subexp_with_ref_bool(
                        min, max + 1, k, RefLrWiener[ plane ][ ppass ][ j ] )
                LrWiener[ plane ]
                        [ unitRow ][ unitCol ][ ppass ][ j ] = v
                COVERCROSS(VALUE_LR_WIENER, LrWiener[ plane ][ unitRow ][ unitCol ][ ppass ][ j ])
                RefLrWiener[ plane ][ ppass ][ j ] = v
            }
        }
    } else if (restoration_type == RESTORE_SGRPROJ) {
        lr_sgr_set                                                           ae(v)
        COVERCROSS(VALUE_CABAC_LR_SGR_SET, lr_sgr_set)
        LrSgrSet[ plane ][ unitRow ][ unitCol ] = lr_sgr_set
        for (i = 0; i < 2; i++) {
            radius = get_sgr_params( lr_sgr_set, i * 2 )
#if COVER
            uint1 I
            I = i
            COVERCROSS(VALUE_SGRPROJ_XQD_MIN, I)
            COVERCROSS(VALUE_SGRPROJ_XQD_MAX, I)
#endif // COVER
            min = Sgrproj_Xqd_Min[i]
            max = Sgrproj_Xqd_Max[i]
            if ( radius ) {
                v = decode_signed_subexp_with_ref_bool(
                       min, max + 1, SGRPROJ_PRJ_SUBEXP_K, RefSgrXqd[ plane ][ i ])
            } else {
                v = 0
                if ( i == 1 ) {
                    v = Clip3( min, max, (1 << SGRPROJ_PRJ_BITS) - RefSgrXqd[ plane ][ 0 ] )
                }
            }
            LrSgrXqd[ plane ][ unitRow ][ unitCol ][ i ] = v
            COVERCROSS(VALUE_LR_SGR_XQD, LrSgrXqd[ plane ][ unitRow ][ unitCol ][ i ])
            RefSgrXqd[ plane ][ i ] = v
        }
    }
}

decode_signed_subexp_with_ref_bool( low, high, k, r ) {
    x = decode_unsigned_subexp_with_ref_bool(high - low, k, r - low)
    return x + low
}

decode_unsigned_subexp_with_ref_bool( mx, k, r ) {
    v = decode_subexp_bool( mx, k )
    if ((r << 1) <= mx) {
        return inverse_recenter(r, v)
    } else {
        return mx - 1 - inverse_recenter(mx - 1 - r, v)
    }
}

decode_subexp_bool( numSyms, k ) {
    uint_0_95 subexp_unif_bools
    uint1 subexp_more_bools
    uint4 subexp_bools
    i = 0
    mk = 0
    while (1) {
        b2 = i ? k + i - 1 : k
        a = 1 << b2
        if (numSyms <= mk + 3 * a) {
            subexp_unif_bools                                                ae(v)
            COVERCROSS(VALUE_CABAC_SUBEXP_UNIF_BOOLS, subexp_unif_bools)
            return subexp_unif_bools + mk
        } else {
            subexp_more_bools                                                ae(v)
            COVERCROSS(VALUE_CABAC_SUBEXP_MORE_BOOLS, subexp_more_bools)
            if (subexp_more_bools) {
               i++
               mk += a
            } else {
               subexp_bools                                                  ae(v)
               COVERCROSS(VALUE_CABAC_SUBEXP_BOOLS, subexp_bools)
               return subexp_bools + mk
            }
        }
    }
}

int Remap_Lr_Type[4] = {
  RESTORE_NONE, RESTORE_SWITCHABLE, RESTORE_WIENER, RESTORE_SGRPROJ
};

lr_params( ) {
    if ( AllLossless || allow_intrabc ||
         !enable_restoration ) {
        FrameRestorationType[0] = RESTORE_NONE
        FrameRestorationType[1] = RESTORE_NONE
        FrameRestorationType[2] = RESTORE_NONE
        UsesLr = 0
    } else {
        UsesLr = 0
        usesChromaLr = 0
        for (i = 0; i < NumPlanes; i++) {
            lr_type                                                              u(2)
            FrameRestorationType[i] = Remap_Lr_Type[lr_type]
#if COVER
            uint2 lrType
            lrType = lr_type
            COVERCROSS(VALUE_REMAP_LR_TYPE, lrType)
#endif // COVER
            if (FrameRestorationType[i] != RESTORE_NONE) {
                UsesLr = 1
                if ( i > 0 ) {
                    usesChromaLr = 1
                }
            }
        }
        if ( UsesLr ) {
            if ( use_128x128_superblock ) {
                lr_unit_shift                                                    u(1)
                lr_unit_shift++
            } else {
                lr_unit_shift                                                    u(1)
                if ( lr_unit_shift ) {
                    lr_unit_extra_shift                                          u(1)
                    lr_unit_shift += lr_unit_extra_shift
                }
            }
            LoopRestorationSize[ 0 ] = RESTORATION_TILESIZE_MAX >> (2 - lr_unit_shift)
            if (COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) && COVERCLASS((PROFILE0 || PROFILE2), PROFILE2, subsampling_y)) && COVERCLASS((PROFILE0 || PROFILE2), usesChromaLr)) ) {
                lr_uv_shift                                                      u(1)
            } else {
                lr_uv_shift = 0
            }
            LoopRestorationSize[ 1 ] = LoopRestorationSize[0] >> COVERCLASS((PROFILE0 || PROFILE2), 1, lr_uv_shift)
            LoopRestorationSize[ 2 ] = LoopRestorationSize[0] >> COVERCLASS((PROFILE0 || PROFILE2), 1, lr_uv_shift)
        }
    }
}

get_above_tx_width( row, col ) {
    if ( row == MiRow ) {
        if ( !AvailU ) {
            return 64
        } else if ( Skips[ row - 1 ][ col ] && IsInters[ row - 1 ][ col ] ) {
            return Block_Width[ MiSizes[ row - 1 ][ col ] ]
        }
    }

#if COVER
    uint_0_18 get_above_tx_width_txSz
    get_above_tx_width_txSz = InterTxSizes[ row - 1 ][ col ]
    COVERCROSS(VALUE_TX_WIDTH, get_above_tx_width_txSz)
#endif // COVER
    return Tx_Width[ InterTxSizes[ row - 1 ][ col ] ]
}

get_left_tx_height( row, col ) {
    if ( col == MiCol ) {
        if ( !AvailL ) {
            return 64
        } else if ( Skips[ row ][ col - 1 ] && IsInters[ row ][ col - 1 ] ) {
            return Block_Height[ MiSizes[ row ][ col - 1 ] ]
        }
    }
    return Tx_Height[ InterTxSizes[ row ][ col - 1 ] ]
}
