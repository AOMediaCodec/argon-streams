/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

// This file is currently unused by VP9
static const UChar
INIT_UNUSED_INIT[3][1] =
{
  { 153,  },
  { 153,  },
  { 153,  },
};
