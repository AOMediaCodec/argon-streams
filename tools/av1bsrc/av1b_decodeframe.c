/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

decode_frame_wrapup( ) {
    if ( show_existing_frame == 0 ) {
        if ( loop_filter_level[ 0 ] != 0 || loop_filter_level[ 1 ] != 0 )
            loop_filter( )
        cdef( )
        upscaling( 0 )
        upscaling( 1 )
        loop_restoration( )
        motion_field_motion_vector_storage( )
        if (segmentation_enabled && !segmentation_update_map) {
            for (row = 0; row < MiRows; row++) {
                for (col = 0; col < MiCols; col++) {
                    SegmentIds[ row ][ col ] = PrevSegmentIds[ row ][ col ]
                }
            }
        }
    } else if ( frame_type == KEY_FRAME ) {
        reference_frame_loading( )
    }
    reference_frame_update( )
    if ( show_frame == 1 || show_existing_frame == 1 )
        output( )
    // Check for full coverage of the allowable number of tile groups.
    // Note: Level 6.3 only allows up to 128 tiles, while the "max parameters"
    // level allows up to 4096. We provide individual value coverage for
    // level 6.3 and below, but for the max parameters level we just have a
    // single point of whether we hit 4096 tile groups.
    // This avoids needing tons of max-parameters streams.
    if (COVERCLASS(1, (MAXLEVEL && !CLIENT_A),
                   seq_level_idx[operatingPoint] < 31 )) {
        TileGroupCountNonLevelMax = TileGroupCount
        COVERCROSS(VALUE_TILE_GROUP_COUNT, TileGroupCountNonLevelMax)
    } else {
        numTileGroupsIs4096 = TileGroupCount == 4096
    }
}
