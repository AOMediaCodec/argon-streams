/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_SPEC_MV 0
#define VALIDATE_SPEC_MV_REF_CANDIDATE 0
#define VALIDATE_SPEC_MFMV 0
#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_WARP 1
#define VALIDATE_SPEC_INTER2 1
#else
#define VALIDATE_SPEC_WARP 0
#define VALIDATE_SPEC_INTER2 0
#endif
#define VALIDATE_SPEC_INTER2_PLANE 0

#define OUTPUT_MVS 0

int Div_Mult[32] = {
  0,    16384, 8192, 5461, 4096, 3276, 2730, 2340, 2048, 1820, 1638,
  1489, 1365,  1260, 1170, 1092, 1024, 963,  910,  862,  819,  780,
  744,  712,   682,  655,  630,  606,  585,  564,  546,  528
};

compute_prediction() {
    sbMask = use_128x128_superblock ? 31 : 15
    subBlockMiRow = MiRow & sbMask
    subBlockMiCol = MiCol & sbMask

    for ( plane = 0; plane < 1 + COVERCLASS(1, (PROFILE0 || PROFILE2), HasChroma) * 2; plane++ ) {
        txSz = Lossless ? TX_4X4 : get_tx_size( plane, TxSize )
        planeSz = get_plane_residual_size( MiSize, plane )
        num4x4W = Num_4x4_Blocks_Wide[ planeSz ]
        num4x4H = Num_4x4_Blocks_High[ planeSz ]
        log2W = MI_SIZE_LOG2 + Mi_Width_Log2[ planeSz ]
        log2H = MI_SIZE_LOG2 + Mi_Height_Log2[ planeSz ]
#if COVER
        uint_0_21 cross_planeSz
        cross_planeSz = planeSz
        COVERCROSS(VALUE_NUM_4X4_BLOCKS_WIDE, cross_planeSz)
        COVERCROSS(VALUE_NUM_4X4_BLOCKS_HIGH, cross_planeSz)
        COVERCROSS(VALUE_MI_WIDTH_LOG2, cross_planeSz)
        COVERCROSS(VALUE_MI_HEIGHT_LOG2, cross_planeSz)
#endif //COVER
        subX = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) : 0)
        subY = COVERCLASS((PROFILE0 || PROFILE2), 1, (plane > 0) ? COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) : 0)
        baseX = (MiCol >> subX) * MI_SIZE
        baseY = (MiRow >> subY) * MI_SIZE
        candRow = (MiRow >> subY) << subY
        candCol = (MiCol >> subX) << subX

        IsInterIntra = ( is_inter && RefFrame[ 1 ] == INTRA_FRAME )

        if ( IsInterIntra ) {
            if ( interintra_mode == II_DC_PRED ) mode = DC_PRED
            else if ( interintra_mode == II_V_PRED ) mode = V_PRED
            else if ( interintra_mode == II_H_PRED ) mode = H_PRED
            else mode = SMOOTH_PRED
            predict_intra( plane, baseX, baseY,
                           plane == 0 ? AvailL : AvailLChroma,
                           plane == 0 ? AvailU : AvailUChroma,
                           BlockDecoded[ plane ]
                                       [ ( subBlockMiRow >> subY ) - 1 ]
                                       [ ( subBlockMiCol >> subX ) + num4x4W ],
                           BlockDecoded[ plane ]
                                       [ ( subBlockMiRow >> subY ) + num4x4H ]
                                       [ ( subBlockMiCol >> subX ) - 1 ],
                           mode,
                           log2W, log2H )
        }
        if ( is_inter ) {
            predW = Block_Width[ MiSize ] >> subX
            predH = Block_Height[ MiSize ] >> subY
            someUseIntra = 0
            for( r = 0; r < num4x4H << subY; r++ )
                for( c = 0; c < num4x4W << subX; c++ )
                    if ( RefFrames[ candRow + r ][ candCol + c ][ 0 ] == INTRA_FRAME )
                        someUseIntra = 1
            if ( someUseIntra ) {
                predW = num4x4W * 4
                predH = num4x4H * 4
                candRow = MiRow
                candCol = MiCol
            }
            r = 0
            for( y = 0; y < num4x4H * 4; y += predH ) {
                c = 0
                for( x = 0; x < num4x4W * 4; x += predW ) {
                    predict_inter( plane, baseX + x, baseY + y,
                                   predW, predH,
                                   candRow + r, candCol + c)
                    c++
                }
                r++
            }
        }
    }
}

predict_inter( plane, x, y, w, h, candRow, candCol ) {
    int mv[ 2 ]
    int mvsout[ 4 ]
    int ssout[ 5 ]

    isCompound = RefFrames[ candRow ][ candCol ][ 1 ] > INTRA_FRAME

    rounding_variables_derivation( isCompound )

    if ( plane == 0 && motion_mode == LOCALWARP )
        warp_estimation( )

    if ( plane == 0 && motion_mode == LOCALWARP && LocalValid == 1 ) {
#if VALIDATE_SPEC_WARP
        validate(55004)
        for(i=0;i<6;i++)
          validate(LocalWarpParams[i])
#endif
        setup_shear( LocalWarpParams, ssout )
        LocalValid = ssout[ 0 ]
#if VALIDATE_SPEC_WARP
        validate(LocalValid)
#endif
    }

    for ( refList = 0; refList < ( isCompound ? 2 : 1 ); refList++ ) {
        refFrame = RefFrames[ candRow ][ candCol ][ refList ]
        globalValid = 1 // Set value to avoid complaints about unused variable.
                        // Choose value of 1 as this is most likely to cause problems if actually used
        if (( YMode == GLOBALMV || YMode == GLOBAL_GLOBALMV ) && GmType[ refFrame ] > TRANSLATION ) {
            setup_shear( gm_params[ refFrame ], ssout )
            globalValid = ssout[ 0 ]
        }

        if ( w < 8 || h < 8 )
            useWarp = 0
        else if ( force_integer_mv == 1 )
            useWarp = 0
        else if ( motion_mode == LOCALWARP && LocalValid == 1 )
            useWarp = 1
        else if ( ( YMode == GLOBALMV || YMode == GLOBAL_GLOBALMV ) &&
                   GmType[ refFrame ] > TRANSLATION &&
                   !is_scaled( refFrame ) &&
                   globalValid == 1 )
            useWarp = 2
        else
            useWarp = 0

        mv[ 0 ] = Mvs[ candRow ][ candCol ][ refList ][ 0 ]
        mv[ 1 ] = Mvs[ candRow ][ candCol ][ refList ][ 1 ]


        if ( use_intrabc == 0 )
            refIdx = ref_frame_idx[ refFrame - LAST_FRAME ]
        else {
            refIdx = -1
            RefFrameWidth[ -1 ] = FrameWidth
            RefFrameHeight[ -1 ] = FrameHeight
            RefUpscaledWidth[ -1 ] = UpscaledWidth
        }

        motion_vector_scaling( plane, refIdx, x, y, mv, mvsout )

        if ( use_intrabc ) {
            RefFrameWidth[ -1 ] = MiCols * MI_SIZE // FrameWidth
            RefFrameHeight[ -1 ] = MiRows * MI_SIZE // FrameHeight
            RefUpscaledWidth[ -1 ] = MiCols * MI_SIZE // UpscaledWidth
        }

        startX = mvsout[ 0 ]
        startY = mvsout[ 1 ]
        stepX  = mvsout[ 2 ]
        stepY  = mvsout[ 3 ]

        ValidationInterIsObmc = 0
        if ( useWarp != 0 )
            for ( i8 = 0; i8 <= ((h-1) >> 3); i8++ )
                for ( j8 = 0; j8 <= ((w-1) >> 3); j8++ ) {
                    block_warp( useWarp, plane, refList, x, y, i8, j8, w, h )
                }
        else {
#if OUTPUT_MVS
            :C printf ("MV: %d %d %d %d %d %d %d\n", plane, x, y, startX, startY, stepX, stepY);
#endif
            block_inter_prediction( plane, refList, refIdx, startX, startY, stepX, stepY, w, h, candRow, candCol )
        }

    }

#if VALIDATE_SPEC_INTER2
    if (plane == VALIDATE_SPEC_INTER2_PLANE && !use_intrabc) {
        for (i = 0; i < h; i++) {
            for (j = 0; j < w; j++) {
                for (refList = 0; refList < ( isCompound ? 2 : 1 ); refList++) {
                    validate(81000)
                    validate(j)
                    validate(i)
                    validate(refList)
                    validate(preds[ refList ][ i ][ j ])
                }
            }
        }
    }
#endif

    if ( compound_type == COMPOUND_WEDGE && plane == 0 ) wedge_mask( w, h )
    else if ( compound_type == COMPOUND_INTRA ) intra_mode_variant_mask( plane, x, y, w, h )
    else if ( compound_type == COMPOUND_DIFFWTD && plane == 0 ) difference_weight_mask( w, h )

    if ( compound_type == COMPOUND_DISTANCE ) distance_weights( candRow, candCol )

    if ( isCompound == 0 && IsInterIntra == 0 ) {
        for (i = 0; i < h; i++) {
            for (j = 0; j < w; j++) {
                CurrFrame[ plane ][ y + i ][ x + j ] = Clip1( preds[ 0 ][ i ][ j ] ) // E-48 [RNG-Blend1]
            }
        }
    }
    else if ( compound_type == COMPOUND_AVERAGE ) {
        for (i = 0; i < h; i++) {
            for (j = 0; j < w; j++) {
                CurrFrame[ plane ][ y + i ][ x + j ] = Clip1( Round2( preds[ 0 ][ i ][ j ] + preds[ 1 ][ i ][ j ], 1 + InterPostRound ) ) // E-49 [RNG-Blend2]
            }
        }
    }
    else if ( compound_type == COMPOUND_DISTANCE ) {
        for (i = 0; i < h; i++) {
            for (j = 0; j < w; j++) {
                CurrFrame[ plane ][ y + i ][ x + j ] = Clip1( Round2( FwdWeight * preds[ 0 ][ i ][ j ] + BckWeight * preds[ 1 ][ i ][ j ], 4 + InterPostRound ) ) // E-50 [RNG-Blend3]
            }
        }
    }
    else mask_blend( plane, x, y, w, h )

#if VALIDATE_SPEC_INTER2
    if (plane == VALIDATE_SPEC_INTER2_PLANE && !use_intrabc) {
        for (i = 0; i < h; i++) {
            for (j = 0; j < w; j++) {
                validate(81001)
                validate(x+j)
                validate(y+i)
                validate(CurrFrame[ plane ][ y + i ][ x + j ])
            }
        }
    }
#endif
    if ( motion_mode == OBMC ) {
        overlapped_motion_compensation( plane, x, y, w, h )
#if VALIDATE_SPEC_INTER2
        if (plane == VALIDATE_SPEC_INTER2_PLANE) {
            for (i = 0; i < h; i++) {
                for (j = 0; j < w; j++) {
                    validate(81002)
                    validate(x+j)
                    validate(y+i)
                    validate(CurrFrame[ plane ][ y + i ][ x + j ])
                }
            }
        }
#endif
    }
}

rounding_variables_derivation( isCompound ) {
    InterRound0 = 3
    InterRound1 = ( isCompound ? 7 : 11)
    if ( COVERCLASS(PROFILE2, 1, BitDepth == 12) ) {
        InterRound0 = InterRound0 + 2
        if ( isCompound == 0 )
            InterRound1 = InterRound1 - 2
    }
    InterPostRound = 2 * FILTER_BITS - ( InterRound0 + InterRound1 )
}

int Quant_Dist_Weight[ 4 ][ 2 ] = {
  { 2, 3 }, { 2, 5 }, { 2, 7 }, { 1, MAX_FRAME_DISTANCE }
};

int Quant_Dist_Lookup[ 4 ][ 2 ] = {
  { 9, 7 }, { 11, 5 }, { 12, 4 }, { 13, 3 }
};

distance_weights( candRow, candCol ) {
    int dist[ 2 ]
    for ( refList = 0; refList < 2; refList++ ) {
        h = OrderHints[ RefFrames[ candRow ][ candCol ][ refList ] ]
        dist[ refList ] = Clip3( 0, MAX_FRAME_DISTANCE, Abs( get_relative_dist( h, OrderHint ) ) )
    }
    d0 = dist[ 1 ]
    d1 = dist[ 0 ]
    order = d0 <= d1
#if COVER
    uint_0_3 idx_1
    uint1 order_1
#endif // COVER
    if (d0 == 0 || d1 == 0) {
#if COVER
        idx_1 = 3
        order_1 = 0
        COVERCROSS(CROSS_QUANT_DIST_LOOKUP, idx_1, order_1)
        order_1 = 1
        COVERCROSS(CROSS_QUANT_DIST_LOOKUP, idx_1, order_1)
#endif // COVER
        FwdWeight = Quant_Dist_Lookup[ 3 ][ order ]
        BckWeight = Quant_Dist_Lookup[ 3 ][ 1 - order ]
    } else {
        for (i = 0; i < 3; i++) {
#if COVER
            idx_1 = i
            order_1 = 0
            COVERCROSS(CROSS_QUANT_DIST_WEIGHT, idx_1, order_1)
            order_1 = 1
            COVERCROSS(CROSS_QUANT_DIST_WEIGHT, idx_1, order_1)
#endif // COVER
            c0 = Quant_Dist_Weight[ i ][ order ]
            c1 = Quant_Dist_Weight[ i ][ 1 - order ]
            if ( order ) {
              if ( d0 * c0 > d1 * c1 )
                break
            } else {
              if ( d0 * c0 < d1 * c1 )
                break
            }
        }
#if COVER
        idx_1 = i
        order_1 = 0
        COVERCROSS(CROSS_QUANT_DIST_LOOKUP, idx_1, order_1)
        order_1 = 1
        COVERCROSS(CROSS_QUANT_DIST_LOOKUP, idx_1, order_1)
#endif // COVER
        FwdWeight = Quant_Dist_Lookup[ i ][ order ]
        BckWeight = Quant_Dist_Lookup[ i ][ 1 - order ]
    }
}

find_latest_backward() {
    ref = -1
    latestOrderHint = -1
    for ( i = 0; i < NUM_REF_FRAMES; i++ ) {
        hint = shiftedOrderHints[ i ]
        if ( !usedFrame[ i ] &&
             hint >= curFrameHint &&
             ( ref < 0 || hint >= latestOrderHint ) ) {
            ref = i
            latestOrderHint = hint
        }
    }
    return ref
}

find_earliest_backward() {
    ref = -1
    earliestOrderHint = -1
    for ( i = 0; i < NUM_REF_FRAMES; i++ ) {
        hint = shiftedOrderHints[ i ]
        if ( !usedFrame[ i ] &&
             hint >= curFrameHint &&
             ( ref < 0 || hint < earliestOrderHint ) ) {
            ref = i
            earliestOrderHint = hint
        }
    }
    return ref
}

find_latest_forward() {
    ref = -1
    latestOrderHint = -1
    for ( i = 0; i < NUM_REF_FRAMES; i++ ) {
        hint = shiftedOrderHints[ i ]
        if ( !usedFrame[ i ] &&
             hint < curFrameHint &&
             ( ref < 0 || hint >= latestOrderHint ) ) {
            ref = i
            latestOrderHint = hint
        }
    }
    return ref
}

int Ref_Frame_List[ REFS_PER_FRAME - 2 ] = {
    LAST2_FRAME, LAST3_FRAME, BWDREF_FRAME, ALTREF2_FRAME, ALTREF_FRAME
};

set_frame_refs( ) {
    for ( i = 0; i < REFS_PER_FRAME; i++ )
        ref_frame_idx[ i ] = -1
    ref_frame_idx[ LAST_FRAME - LAST_FRAME ] = last_frame_idx
    ref_frame_idx[ GOLDEN_FRAME - LAST_FRAME ] = gold_frame_idx
    for ( i = 0; i < NUM_REF_FRAMES; i++ )
        usedFrame[ i ] = 0
    usedFrame[ last_frame_idx ] = 1
    usedFrame[ gold_frame_idx ] = 1

    curFrameHint = 1 << (OrderHintBits - 1)

    for ( i = 0; i < NUM_REF_FRAMES; i++ )
        shiftedOrderHints[ i ] = curFrameHint + get_relative_dist( RefOrderHint[ i ], OrderHint )

    lastOrderHint = shiftedOrderHints[ last_frame_idx ]
    CHECK( lastOrderHint < curFrameHint, "lastOrderHint must be less than curFrameHint")

    goldOrderHint = shiftedOrderHints[ gold_frame_idx ]
    CHECK( goldOrderHint < curFrameHint, "goldOrderHint must be less than curFrameHint")

    ref = find_latest_backward()
    if ( ref >= 0 ) {
        ref_frame_idx[ ALTREF_FRAME - LAST_FRAME ] = ref
        usedFrame[ ref ] = 1
    }

    ref = find_earliest_backward()
    if ( ref >= 0 ) {
        ref_frame_idx[ BWDREF_FRAME - LAST_FRAME ] = ref
        usedFrame[ ref ] = 1
    }

    ref = find_earliest_backward()
    if ( ref >= 0 ) {
        ref_frame_idx[ ALTREF2_FRAME - LAST_FRAME ] = ref
        usedFrame[ ref ] = 1
    }

    for ( i = 0; i < REFS_PER_FRAME - 2; i++ ) {
        refFrame = Ref_Frame_List[ i ]
#if COVER
        uint_0_4 I
        I = i
        COVERCROSS(VALUE_REF_FRAME_LIST, I)
#endif // COVER
        if ( ref_frame_idx[ refFrame - LAST_FRAME ] < 0 ) {
            ref = find_latest_forward()
            if ( ref >= 0 ) {
                ref_frame_idx[ refFrame - LAST_FRAME ] = ref
                usedFrame[ ref ] = 1
            }
        }
    }

    ref = -1
    earliestOrderHint = -1
    for ( i = 0; i < NUM_REF_FRAMES; i++ ) {
        hint = shiftedOrderHints[ i ]
        if ( ref < 0 || hint < earliestOrderHint ) {
            ref = i
            earliestOrderHint = hint
        }
    }
    for ( i = 0; i < REFS_PER_FRAME; i++ ) {
        if ( ref_frame_idx[ i ] < 0 ) {
            ref_frame_idx[ i ] = ref
        }
    }
}

motion_field_estimation( ) {
    w8 = MiCols >> 1
    h8 = MiRows >> 1
    for ( ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++ )
        for ( y = 0; y < h8 ; y++ )
            for ( x = 0; x < w8; x++ )
                for ( j = 0; j < 2; j++ )
                    MotionFieldMvs[ ref ][ y ][ x ][ j ] = -1 << 15
    lastIdx = ref_frame_idx[ 0 ]
    curGoldOrderHint = OrderHints[ GOLDEN_FRAME ]
    lastAltOrderHint = SavedOrderHints[ lastIdx ][ ALTREF_FRAME ]
    useLast = ( lastAltOrderHint != curGoldOrderHint )
    if ( useLast == 1 )
        projection( LAST_FRAME, -1 )
    refStamp = MFMV_STACK_SIZE - 2
    useBwd = get_relative_dist( OrderHints[ BWDREF_FRAME ], OrderHint ) > 0
    if ( useBwd == 1 ) {
        projOutput = projection( BWDREF_FRAME, 1 )
        if (projOutput) refStamp--
    }
    useAlt2 = get_relative_dist( OrderHints[ ALTREF2_FRAME ], OrderHint ) > 0
    if ( useAlt2 == 1 ) {
        projOutput = projection( ALTREF2_FRAME, 1 )
        if (projOutput) refStamp--
    }
    useAlt = get_relative_dist( OrderHints[ ALTREF_FRAME ], OrderHint ) > 0
    if ( useAlt == 1 && refStamp >= 0 ) {
        projOutput = projection( ALTREF_FRAME, 1 )
        if (projOutput) refStamp--
    }
    if ( refStamp >= 0 ) {
        projection( LAST2_FRAME, -1 )
    }
}

projection( src, dstSign ) {
    int mv[ 2 ]
    int projMv[ 2 ]
    srcIdx = ref_frame_idx[ src - LAST_FRAME ]

#if VALIDATE_SPEC_MFMV
  validate(42011)
  validate(src)
  validate(srcIdx)
#endif

    if ( RefMiRows[ srcIdx ] != MiRows || RefMiCols[ srcIdx ] != MiCols ||
         RefFrameType[ srcIdx ] == INTRA_ONLY_FRAME || RefFrameType[ srcIdx ] == KEY_FRAME )
        return 0

#if VALIDATE_SPEC_MFMV
  validate(42000)
  validate(src)
  validate(srcIdx)
#endif

    w8 = MiCols >> 1
    h8 = MiRows >> 1

    for ( y8 = 0; y8 < h8; y8++ ) {
        for ( x8 = 0; x8 < w8; x8++ ) {
            row = 2 * y8 + 1
            col = 2 * x8 + 1
            srcRef = SavedRefFrames[ srcIdx ][ row ][ col ]
            if ( srcRef > INTRA_FRAME ) {
                refToCur = get_relative_dist( OrderHints[ src ], OrderHint )
                refOffset = get_relative_dist( OrderHints[ src ], SavedOrderHints[ srcIdx ][ srcRef ] )
                posValid = Abs( refToCur ) <= MAX_FRAME_DISTANCE &&
                           Abs( refOffset ) <= MAX_FRAME_DISTANCE &&
                           ( refOffset > 0 )
                if ( posValid ) {
                    mv[ 0 ] = SavedMvs[ srcIdx ][ row ][ col ][ 0 ]
                    mv[ 1 ] = SavedMvs[ srcIdx ][ row ][ col ][ 1 ]
                    get_mv_projection( mv, projMv, refToCur * dstSign, refOffset )
                    posValid = get_block_position( x8, y8, dstSign, projMv )
                    if ( posValid ) {
                        for ( dst = LAST_FRAME; dst <= ALTREF_FRAME; dst++ ) {
                            refToDst = get_relative_dist( OrderHint, OrderHints[ dst ] )
                            get_mv_projection( mv, projMv, refToDst, refOffset )
                            MotionFieldMvs[ dst ][ PosY8 ][ PosX8 ][ 0 ] = projMv[ 0 ]
                            MotionFieldMvs[ dst ][ PosY8 ][ PosX8 ][ 1 ] = projMv[ 1 ]
                        }
                    }
                }
            }
        }
    }
    return 1
}

// TODO: is int32pointer correct?
get_mv_projection( int32pointer mv, int32pointer projMv, num, den ) {
    uint_0_31 clippedDenominator
    clippedDenominator = Min( den, MAX_FRAME_DISTANCE )
    clippedNumerator = Clip3( -MAX_FRAME_DISTANCE, MAX_FRAME_DISTANCE, num )
    for ( i = 0; i < 2; i++ ) {
        COVERCROSS(VALUE_DIV_MULT, clippedDenominator)
        scaled = Round2Signed( mv[ i ] * clippedNumerator * Div_Mult[ clippedDenominator ], 14 )
        projMv[ i ] = Clip3( -(1 << 14) + 1, (1 << 14) - 1, scaled )
    }
#if VALIDATE_SPEC_MFMV
  validate(42001)
  validate(mv[ 0 ])
  validate(mv[ 1 ])
  validate(clippedNumerator)
  validate(clippedDenominator)
  validate(projMv[0])
  validate(projMv[1])
#endif
}

get_block_position( x8, y8, dstSign, int32pointer projMv ) {
    posValid = 1

    for (ord = 0; ord < 2; ord++) {
        v8 = ord ? x8 : y8
        delta = projMv[ ord ]
        max8 = ( ord ? MiCols : MiRows ) >> 1
        maxOff8 = ord ? MAX_OFFSET_WIDTH : MAX_OFFSET_HEIGHT

        base8 = (v8 >> 3) << 3
        if (delta >= 0) {
            offset8 = delta >> ( 3 + 1 + MI_SIZE_LOG2 )
        } else {
            offset8 = -( ( -delta ) >> ( 3 + 1 + MI_SIZE_LOG2 ) )
        }
        v8 += dstSign * offset8
        if ( v8 < 0 ||
             v8 >= max8 ||
             v8 < base8 - maxOff8 ||
             v8 >= base8 + 8 + maxOff8 ) {
            posValid = 0
        }

        if (ord == 0)
            PosY8 = v8
        else
            PosX8 = v8
    }
    return posValid
}

find_mv_stack( isCompound ) {
    bw4 = Num_4x4_Blocks_Wide[ MiSize ]
    bh4 = Num_4x4_Blocks_High[ MiSize ]

    NumMvFound = 0
    NewMvCount = 0
    setup_global_mv( 0, GlobalMvs[ 0 ] )
    if ( isCompound == 1 )
        setup_global_mv( 1, GlobalMvs[ 1 ] )

#if VALIDATE_SPEC_MV
    validate(700010)
#endif

    FoundMatch = 0
    scan_row( -1, isCompound )
    foundAboveMatch = FoundMatch

#if VALIDATE_SPEC_MV
    validate(700011)
    validate(foundAboveMatch)
#endif

    FoundMatch = 0
    scan_col( -1, isCompound )
    foundLeftMatch = FoundMatch

#if VALIDATE_SPEC_MV
  validate(700012)
    validate(foundAboveMatch)
    validate(foundLeftMatch)
#endif

#if VALIDATE_SPEC_MV_REF_CANDIDATE
  validate(70020)
  validate(MiRow)
  validate(MiCol)
  validate(bw4)
  validate(bh4)
#endif
    FoundMatch = 0
    if ( Max( bw4, bh4 ) <= 16 )
        scan_point( -1, bw4, isCompound )
    if ( FoundMatch == 1 )
        foundAboveMatch = 1

#if VALIDATE_SPEC_MV
    validate(700013)
    validate(foundAboveMatch)
    validate(foundLeftMatch)
#endif

    CloseMatches = foundAboveMatch + foundLeftMatch
    numNearest = NumMvFound
    numNew = NewMvCount
    if ( numNearest > 0 ) {
        for ( idx = 0; idx < numNearest; idx++ )
            WeightStack[ idx ] += REF_CAT_LEVEL
    }
    ZeroMvContext = 0
    if ( use_ref_frame_mvs == 1 )
        temporal_scan( isCompound )

    scan_point( -1, -1, isCompound )

    if ( FoundMatch == 1 )
        foundAboveMatch = 1

    FoundMatch = 0
    scan_row( -3, isCompound )
    if ( FoundMatch == 1 )
        foundAboveMatch = 1

    FoundMatch = 0
    scan_col( -3, isCompound )
    if ( FoundMatch == 1 )
        foundLeftMatch = 1

    FoundMatch = 0
    if ( bh4 > 1 )
        scan_row( -5, isCompound )
    if ( FoundMatch == 1 )
        foundAboveMatch = 1

    FoundMatch = 0
    if ( bw4 > 1 )
        scan_col( -5, isCompound )
    if ( FoundMatch == 1 )
        foundLeftMatch = 1

#if VALIDATE_SPEC_MV
    validate(70002)
    validate(foundAboveMatch)
    validate(foundLeftMatch)
    for(i=0;i<NumMvFound;i++) {
      validate(i)
      validate(WeightStack[i])
      validate(RefStackMv[ i ][ 0 ][ 0 ])
      validate(RefStackMv[ i ][ 0 ][ 1 ])
    }
#endif

    TotalMatches = foundAboveMatch + foundLeftMatch

    sort_mv_stack( 0, numNearest, isCompound )
    sort_mv_stack( numNearest, NumMvFound, isCompound )

#if VALIDATE_SPEC_MV
    validate(70000)
    validate(CloseMatches)
    validate(TotalMatches)
    validate(numNew)
#endif

    if ( NumMvFound < 2 )
        extra_search( isCompound )

    context_and_clamping( isCompound, numNew )
}

setup_global_mv( refList, int32pointer mv ) {
    typ = 0
    ref = RefFrame[ refList ]
    if (ref != INTRA_FRAME)
        typ = GmType[ ref ]
    bw = Block_Width[ MiSize ]
    bh = Block_Height[ MiSize ]
#if VALIDATE_SPEC_MV_REF_CANDIDATE
    validate(24000)
#endif
    if (ref == INTRA_FRAME || typ == IDENTITY) {
        mv[0] = 0
        mv[1] = 0
    } else if (typ == TRANSLATION) {
        mv[0] = gm_params[ref][0] >> (WARPEDMODEL_PREC_BITS - 3)
        mv[1] = gm_params[ref][1] >> (WARPEDMODEL_PREC_BITS - 3)
    } else {
        x = MiCol * MI_SIZE + bw / 2 - 1
        y = MiRow * MI_SIZE + bh / 2 - 1
        xc = (gm_params[ref][2] - (1 << WARPEDMODEL_PREC_BITS)) * x +
              gm_params[ref][3] * y +
              gm_params[ref][0]
        yc =  gm_params[ref][4] * x +
             (gm_params[ref][5] - (1 << WARPEDMODEL_PREC_BITS)) * y +
              gm_params[ref][1]
        if (allow_high_precision_mv) {
            mv[0] = Round2Signed(yc, WARPEDMODEL_PREC_BITS - 3)
            mv[1] = Round2Signed(xc, WARPEDMODEL_PREC_BITS - 3)
        } else {
            mv[0] = Round2Signed(yc, WARPEDMODEL_PREC_BITS - 2) * 2
            mv[1] = Round2Signed(xc, WARPEDMODEL_PREC_BITS - 2) * 2
        }
    }
    lower_mv_precision( mv )
#if VALIDATE_SPEC_MV_REF_CANDIDATE
    validate(mv[0])
    validate(mv[1])
#endif
}

scan_row( deltaRow, isCompound ) {
    bw4 = Num_4x4_Blocks_Wide[ MiSize ]
    end4 = Min( Min( bw4, MiCols - MiCol ), 16 )
    deltaCol = 0
    useStep16 = ( bw4 >= 16 )
    if ( Abs( deltaRow ) > 1 ) {
        deltaRow += MiRow & 1
        deltaCol = 1 - (MiCol & 1)
    }
    i = 0
    while ( i < end4 ) {
        mvRow = MiRow + deltaRow
        mvCol = MiCol + deltaCol + i
        if (!is_inside(mvRow,mvCol))
            break
        len = Min(bw4, Num_4x4_Blocks_Wide[ MiSizes[ mvRow ][ mvCol ] ])
        if (Abs(deltaRow) > 1)
            len = Max(2, len)
        if (useStep16)
            len = Max(4, len)

        weight = len * 2
        add_reference_motion_vector( mvRow, mvCol, isCompound, weight)
        i += len
    }
}

scan_col( deltaCol, isCompound ) {
    bh4 = Num_4x4_Blocks_High[ MiSize ]
    end4 = Min( Min( bh4, MiRows - MiRow ), 16 )
    deltaRow = 0
    useStep16 = ( bh4 >= 16 )
    if ( Abs( deltaCol ) > 1 ) {
        deltaRow = 1 - (MiRow & 1)
        deltaCol += MiCol & 1
    }
    i = 0
    while ( i < end4 ) {
        mvRow = MiRow + deltaRow + i
        mvCol = MiCol + deltaCol
        if (!is_inside(mvRow,mvCol))
            break
        len = Min(bh4, Num_4x4_Blocks_High[ MiSizes[ mvRow ][ mvCol ] ])
        if (Abs(deltaCol) > 1)
            len = Max(2, len)
        if (useStep16)
            len = Max(4, len)

        weight = len * 2
        add_reference_motion_vector( mvRow, mvCol, isCompound, weight)
        i += len
    }
}

scan_point( deltaRow, deltaCol, isCompound ) {
    mvRow = MiRow + deltaRow
    mvCol = MiCol + deltaCol
    weight = 4
    if ( is_inside( mvRow, mvCol ) == 1 && RefFramesWritten[ mvRow ][ mvCol ] ) {
        if ( COVERCLASS(IMPOSSIBLE, 1, COVERCLASS(IMPOSSIBLE, 1, MiddleVertPartition) && COVERCLASS(IMPOSSIBLE, IMPOSSIBLE, deltaCol > 0)) ) {
            sbMask = use_128x128_superblock ? 31 : 15
            maxSize = use_128x128_superblock ? 32 : 16
            maskRow = MiRow & sbMask
            maskCol = MiCol & sbMask
            bw4 = Num_4x4_Blocks_Wide[ MiSize ]
            bh4 = Num_4x4_Blocks_High[ MiSize ]
            bs = Max( bw4, bh4 )
            hastr = !((maskRow & bs) && (maskCol & bs))
            while (bs < maxSize) {
                if (maskCol & bs) {
                    if ((maskCol & (2 * bs)) && (maskRow & (2 * bs))) {
                      hastr = 0
                      break
                    }
                } else {
                    break
                }
                bs = bs << 1
            }
            if ( !hastr ) {
                return 0
            }
        }
#if VALIDATE_SPEC_MV_REF_CANDIDATE
        validate(70021)
        validate(mvRow)
        validate(mvCol)
#endif
        add_reference_motion_vector( mvRow, mvCol, isCompound, weight )
    }
}

temporal_scan( isCompound ) {
    int32 tplSamplePos[3][2]

    bw4 = Num_4x4_Blocks_Wide[ MiSize ]
    bh4 = Num_4x4_Blocks_High[ MiSize ]
    stepW4 = ( bw4 >= 16 ) ? 4 : 2
    stepH4 = ( bh4 >= 16 ) ? 4 : 2

    for ( deltaRow = 0; deltaRow < Min( bh4, 16 ) ; deltaRow += stepH4 ) {
        for ( deltaCol = 0; deltaCol < Min( bw4, 16 ) ; deltaCol += stepW4 ) {
            add_tpl_ref_mv( deltaRow, deltaCol, isCompound )
        }
    }
    allowExtension = ((bh4 >= Num_4x4_Blocks_High[BLOCK_8X8]) &&
                      (bh4 < Num_4x4_Blocks_High[BLOCK_64X64]) &&
                      (bw4 >= Num_4x4_Blocks_Wide[BLOCK_8X8]) &&
                      (bw4 < Num_4x4_Blocks_Wide[BLOCK_64X64]))
    if ( allowExtension ) {
        tplSamplePos[0][0] = bh4
        tplSamplePos[0][1] = -2
        tplSamplePos[1][0] = bh4
        tplSamplePos[1][1] = bw4
        tplSamplePos[2][0] = bh4 - 2
        tplSamplePos[2][1] = bw4
        for ( i = 0; i < 3; i++ ) {
            deltaRow = tplSamplePos[ i ][ 0 ]
            deltaCol = tplSamplePos[ i ][ 1 ]
            if ( check_sb_border( deltaRow, deltaCol ) ) {
                add_tpl_ref_mv( deltaRow, deltaCol, isCompound )
            }
        }
    }
}

check_sb_border( int5 deltaRow, int5 deltaCol ) {
    row = (MiRow & 15) + deltaRow
    col = (MiCol & 15) + deltaCol

    return ( row >= 0 && row < 16 && col >= 0 && col < 16 )
}

add_tpl_ref_mv( int5 deltaRow, int5 deltaCol, isCompound ) {
    int candMv[ 2 ]
    int candMv0[ 2 ]
    int candMv1[ 2 ]

    mvRow = ( MiRow + deltaRow ) | 1
    mvCol = ( MiCol + deltaCol ) | 1
    if ( is_inside( mvRow, mvCol ) != 0 ) {

#if VALIDATE_SPEC_MV_REF_CANDIDATE
        validate(70016)
        validate(mvRow)
        validate(mvCol)
#endif

        x8 = mvCol >> 1
        y8 = mvRow >> 1

        if ( deltaRow == 0 && deltaCol == 0 ) {
            ZeroMvContext = 1
        }
        if ( !isCompound ) {
            candMv[ 0 ] = MotionFieldMvs[ RefFrame[ 0 ] ][ y8 ][ x8 ][ 0 ]
            candMv[ 1 ] = MotionFieldMvs[ RefFrame[ 0 ] ][ y8 ][ x8 ][ 1 ]
            if ( candMv[ 0 ] == -1 << 15 )
                return 0
            lower_mv_precision( candMv )
            if ( deltaRow == 0 && deltaCol == 0 ) {
                if ( Abs( candMv[ 0 ] - GlobalMvs[ 0 ][ 0 ] ) >= 16 ||
                     Abs( candMv[ 1 ] - GlobalMvs[ 0 ][ 1 ] ) >= 16 )
                    ZeroMvContext = 1
                else
                    ZeroMvContext = 0
            }
            for ( idx = 0; idx < NumMvFound; idx++ ) {
                if ( candMv[ 0 ] == RefStackMv[ idx ][ 0 ][ 0 ] &&
                     candMv[ 1 ] == RefStackMv[ idx ][ 0 ][ 1 ] )
                     break
            }
            if ( idx < NumMvFound ) {
                WeightStack[ idx ] += 2
            } else if ( NumMvFound < MAX_REF_MV_STACK_SIZE ) {
                RefStackMv[ NumMvFound ][ 0 ][ 0 ] = candMv[ 0 ]
                RefStackMv[ NumMvFound ][ 0 ][ 1 ] = candMv[ 1 ]
                WeightStack[ NumMvFound ] = 2
#if VALIDATE_SPEC_MV_REF_CANDIDATE
                validate(70005)
                validate(NumMvFound)
                validate(candMv[ 0 ])
                validate(candMv[ 1 ])
#endif
                NumMvFound += 1
            }
        } else {
            candMv0[ 0 ] = MotionFieldMvs[ RefFrame[ 0 ] ][ y8 ][ x8 ][ 0 ]
            candMv0[ 1 ] = MotionFieldMvs[ RefFrame[ 0 ] ][ y8 ][ x8 ][ 1 ]
            if ( candMv0[ 0 ] == -1 << 15 )
                return 0
            candMv1[ 0 ] = MotionFieldMvs[ RefFrame[ 1 ] ][ y8 ][ x8 ][ 0 ]
            candMv1[ 1 ] = MotionFieldMvs[ RefFrame[ 1 ] ][ y8 ][ x8 ][ 1 ]
            // Due to the way the motion field is set up, for any particular (x8, y8),
            // we either have *no* valid MFMVs, or one valid MFMV for every ref frame.
            // If we get here, then candMv0 must be valid, so candMv1 must be valid too.
            if ( COVERCLASS(IMPOSSIBLE,1,candMv1[ 0 ] == -1 << 15) )
                return 0
            lower_mv_precision( candMv0 )
            lower_mv_precision( candMv1 )
            if ( deltaRow == 0 && deltaCol == 0 ) {
                if ( Abs( candMv0[ 0 ] - GlobalMvs[ 0 ][ 0 ] ) >= 16 ||
                     Abs( candMv0[ 1 ] - GlobalMvs[ 0 ][ 1 ] ) >= 16 ||
                     Abs( candMv1[ 0 ] - GlobalMvs[ 1 ][ 0 ] ) >= 16 ||
                     Abs( candMv1[ 1 ] - GlobalMvs[ 1 ][ 1 ] ) >= 16 )
                    ZeroMvContext = 1
                else
                    ZeroMvContext = 0
            }
            for ( idx = 0; idx < NumMvFound; idx++ ) {
                if ( candMv0[ 0 ] == RefStackMv[ idx ][ 0 ][ 0 ]  &&
                     candMv0[ 1 ] == RefStackMv[ idx ][ 0 ][ 1 ]  &&
                     candMv1[ 0 ] == RefStackMv[ idx ][ 1 ][ 0 ]  &&
                     candMv1[ 1 ] == RefStackMv[ idx ][ 1 ][ 1 ] )
                     break
            }
            if ( idx < NumMvFound ) {
                WeightStack[ idx ] += 2
            } else if ( NumMvFound < MAX_REF_MV_STACK_SIZE ) {
                RefStackMv[ NumMvFound ][ 0 ][ 0 ] = candMv0[ 0 ]
                RefStackMv[ NumMvFound ][ 0 ][ 1 ] = candMv0[ 1 ]
                RefStackMv[ NumMvFound ][ 1 ][ 0 ] = candMv1[ 0 ]
                RefStackMv[ NumMvFound ][ 1 ][ 1 ] = candMv1[ 1 ]
                WeightStack[ NumMvFound ] = 2
#if VALIDATE_SPEC_MV_REF_CANDIDATE
                validate(70006)
                validate(NumMvFound)
                validate(candMv0[ 0 ])
                validate(candMv0[ 1 ])
                validate(candMv1[ 0 ])
                validate(candMv1[ 1 ])
#endif
                NumMvFound += 1
            }
        }
    }
}

add_reference_motion_vector( mvRow, mvCol, isCompound, weight ) {
    if ( IsInters[ mvRow ][ mvCol ] != 0 ) {
        if ( isCompound == 0 ) {
            for (candList = 0; candList < 2; candList++) {
                if ( RefFrames[ mvRow ][ mvCol ][ candList ] == RefFrame[ 0 ] )
                    search_stack( mvRow, mvCol, weight, candList )
            }
        } else {
            if ( RefFrames[ mvRow ][ mvCol ][ 0 ] == RefFrame[ 0 ] &&
                 RefFrames[ mvRow ][ mvCol ][ 1 ] == RefFrame[ 1 ] )
                compound_search_stack( mvRow, mvCol, weight )
        }
    }
}

search_stack( mvRow, mvCol, weight, candList ) {
    int candMv[ 2 ]

    candMode = YModes[ mvRow ][ mvCol ]
    candSize = MiSizes[ mvRow ][ mvCol ]
    large = ( Min( Block_Width[ candSize ],Block_Height[ candSize ] ) >= 8 )
    if ( ( candMode == GLOBALMV || candMode == GLOBAL_GLOBALMV ) &&
         GmType[ RefFrame[ 0 ] ] > TRANSLATION &&
         large == 1 ) {
        candMv[ 0 ] = GlobalMvs[ 0 ][ 0 ]
        candMv[ 1 ] = GlobalMvs[ 0 ][ 1 ]
    }
    else
    {
        candMv[ 0 ] = Mvs[ mvRow ][ mvCol ][ candList ][ 0 ]
        candMv[ 1 ] = Mvs[ mvRow ][ mvCol ][ candList ][ 1 ]
    }
    lower_mv_precision( candMv )
    if ( has_newmv( candMode ) )
        NewMvCount = NewMvCount + 1
    FoundMatch = 1
    candMvFound = 0
    for ( idx = 0; idx < NumMvFound; idx++ ) {
        if (candMv[ 0 ] == RefStackMv[ idx ][ 0 ][ 0 ] && candMv[ 1 ] == RefStackMv[ idx ][ 0 ][ 1 ]) {
            WeightStack[ idx ] += weight
            candMvFound = 1
            break
        }
    }
    if (!candMvFound) {
        if (NumMvFound < MAX_REF_MV_STACK_SIZE) {
            RefStackMv[ NumMvFound ][ 0 ][ 0 ] = candMv[ 0 ]
            RefStackMv[ NumMvFound ][ 0 ][ 1 ] = candMv[ 1 ]
#if VALIDATE_SPEC_MV_REF_CANDIDATE
            validate(70003)
            validate(NumMvFound)
            validate(candMv[ 0 ])
            validate(candMv[ 1 ])
#endif
            WeightStack[ NumMvFound ] = weight
            NumMvFound++
        }
    }
}

has_newmv( mode ) {
    ret = (mode == NEWMV ||
           mode == NEW_NEWMV ||
           mode == NEAR_NEWMV ||
           mode == NEW_NEARMV ||
           mode == NEAREST_NEWMV ||
           mode == NEW_NEARESTMV)
    return ret
}

compound_search_stack( mvRow, mvCol, weight ) {
    int candMvs[ 2 ][ 2 ]

    numMvFoundIncreased = 0

    candMvs[ 0 ][ 0 ] = Mvs[ mvRow ][ mvCol ][ 0 ][ 0 ]
    candMvs[ 0 ][ 1 ] = Mvs[ mvRow ][ mvCol ][ 0 ][ 1 ]
    candMvs[ 1 ][ 0 ] = Mvs[ mvRow ][ mvCol ][ 1 ][ 0 ]
    candMvs[ 1 ][ 1 ] = Mvs[ mvRow ][ mvCol ][ 1 ][ 1 ]

    candMode = YModes[ mvRow ][ mvCol ]
    candSize = MiSizes[ mvRow ][ mvCol ]
    ASSERT(( Min( Block_Width[ candSize ],Block_Height[ candSize ] ) >= 8 ), "Compound block size should be 8x8 or bigger")
    if ( candMode == GLOBAL_GLOBALMV ) {
        for (refList = 0; refList < 2; refList++)
            if (GmType[ RefFrame[ refList ] ] > TRANSLATION) {
                candMvs[ refList ][ 0 ] = GlobalMvs[ refList ][ 0 ]
                candMvs[ refList ][ 1 ] = GlobalMvs[ refList ][ 1 ]
            }
    }
    for (i = 0; i < 2; i++)
        lower_mv_precision( candMvs[ i ] )
    FoundMatch = 1
    candMvFound = 0
    for ( idx = 0; idx < NumMvFound; idx++ ) {
        if ( candMvs[ 0 ][ 0 ] == RefStackMv[ idx ][ 0 ][ 0 ] &&
             candMvs[ 0 ][ 1 ] == RefStackMv[ idx ][ 0 ][ 1 ] &&
             candMvs[ 1 ][ 0 ] == RefStackMv[ idx ][ 1 ][ 0 ] &&
             candMvs[ 1 ][ 1 ] == RefStackMv[ idx ][ 1 ][ 1 ] ) {
            WeightStack[ idx ] += weight
            candMvFound = 1
            break
        }
    }
    if (!candMvFound) {
        if (NumMvFound < MAX_REF_MV_STACK_SIZE) {
            for (i = 0; i < 2; i++) {
                RefStackMv[ NumMvFound ][ i ][ 0 ] = candMvs[ i ][ 0 ]
                RefStackMv[ NumMvFound ][ i ][ 1 ] = candMvs[ i ][ 1 ]
#if VALIDATE_SPEC_MV_REF_CANDIDATE
                validate(70004)
                validate(NumMvFound)
                validate(i)
                validate(candMvs[ i ][ 0 ])
                validate(candMvs[ i ][ 1 ])
#endif
            }
            WeightStack[ NumMvFound ] = weight
            NumMvFound++
            numMvFoundIncreased = 1
        }
    }
    if ( has_newmv( candMode ) == 1 )
        NewMvCount = NewMvCount + 1
}

lower_mv_precision( int32pointer candMv ) {
    if ( allow_high_precision_mv != 1 ) {
        for (i = 0; i < 2; i++) {
            if (force_integer_mv) {
                a = Abs( candMv[ i ] )
                aInt = (a + 3) >> 3
                if (candMv[ i ] > 0)
                    candMv[ i ] = aInt << 3
                else
                    candMv[ i ] = -( aInt << 3 )
            } else {
                if (candMv[ i ] & 1) {
                    if (candMv[ i ] > 0)
                        candMv[ i ]--
                    else
                        candMv[ i ]++
                }
            }
        }
    }
}

swap_stack( i, j, isCompound ) {
    temp = WeightStack[ i ]
    WeightStack[ i ] = WeightStack[ j ]
    WeightStack[ j ] = temp
    for ( list = 0; list < isCompound + 1; list++ ) {
        for ( comp = 0; comp < 2; comp++ ) {
            temp = RefStackMv[ i ][ list ][ comp ]
            RefStackMv[ i ][ list ][ comp ] = RefStackMv[ j ][ list ][ comp ]
            RefStackMv[ j ][ list ][ comp ] = temp
        }
    }
}

sort_mv_stack( start, end, isCompound ) {
    while ( end > start ) {
        newEnd = start
        for ( idx = start + 1; idx < end; idx++ ) {
            if ( WeightStack[ idx - 1 ] < WeightStack[ idx ] ) {
                swap_stack( idx - 1, idx, isCompound )
                newEnd = idx
            }
        }
        end = newEnd
    }
}

extra_search( isCompound ) {
    int combinedMvs[ 2 ][ 2 ][ 2 ]
    for ( list = 0; list < 2; list++ ) {
        RefIdCount[ list ] = 0
        RefDiffCount[ list ] = 0
    }
    w4 = Min( 16, Num_4x4_Blocks_Wide[ MiSize ] )
    h4 = Min( 16, Num_4x4_Blocks_High[ MiSize ] )
    w4 = Min( w4, MiCols - MiCol )
    h4 = Min( h4, MiRows - MiRow )
    num4x4 = Min( w4, h4 )
    for ( ppass = 0; ppass < 2; ppass++ ) {
        idx = 0
        while ( idx < num4x4 && NumMvFound < 2 ) {
            if ( ppass == 0 ) {
                mvRow = MiRow - 1
                mvCol = MiCol + idx
            } else {
                mvRow = MiRow + idx
                mvCol = MiCol - 1
            }
            if ( !is_inside( mvRow, mvCol ) )
                break
            add_extra_mv_candidate( mvRow, mvCol, isCompound )
            if ( ppass == 0 ) {
                idx += Num_4x4_Blocks_Wide[ MiSizes[ mvRow ][ mvCol ] ]
            } else {
                idx += Num_4x4_Blocks_High[ MiSizes[ mvRow ][ mvCol ] ]
            }
        }
    }
    if ( isCompound == 1 ) {
        for ( list = 0; list < 2; list++ ) {
            compCount = 0
            for ( idx = 0; idx < RefIdCount[ list ]; idx++ ) {
                combinedMvs[ compCount ][ list ][ 0 ] = RefIdMvs[ list ][ idx ][ 0 ]
                combinedMvs[ compCount ][ list ][ 1 ] = RefIdMvs[ list ][ idx ][ 1 ]
                compCount++
            }
            for ( idx = 0; idx < RefDiffCount[ list ] && compCount < 2; idx++ ) {
                combinedMvs[ compCount ][ list ][ 0 ] = RefDiffMvs[ list ][ idx ][ 0 ]
                combinedMvs[ compCount ][ list ][ 1 ] = RefDiffMvs[ list ][ idx ][ 1 ]
                compCount++
            }
            while ( compCount < 2 ) {
                combinedMvs[ compCount ][ list ][ 0 ] = GlobalMvs[ list ][ 0 ]
                combinedMvs[ compCount ][ list ][ 1 ] = GlobalMvs[ list ][ 1 ]
                compCount++
            }
        }
        if ( NumMvFound == 1 ) {
            if ( combinedMvs[ 0 ][ 0 ][ 0 ] == RefStackMv[ 0 ][ 0 ][ 0 ] &&
                 combinedMvs[ 0 ][ 0 ][ 1 ] == RefStackMv[ 0 ][ 0 ][ 1 ] &&
                 combinedMvs[ 0 ][ 1 ][ 0 ] == RefStackMv[ 0 ][ 1 ][ 0 ] &&
                 combinedMvs[ 0 ][ 1 ][ 1 ] == RefStackMv[ 0 ][ 1 ][ 1 ] ) {
                RefStackMv[ NumMvFound ][ 0 ][ 0 ] = combinedMvs[ 1 ][ 0 ][ 0 ]
                RefStackMv[ NumMvFound ][ 0 ][ 1 ] = combinedMvs[ 1 ][ 0 ][ 1 ]
                RefStackMv[ NumMvFound ][ 1 ][ 0 ] = combinedMvs[ 1 ][ 1 ][ 0 ]
                RefStackMv[ NumMvFound ][ 1 ][ 1 ] = combinedMvs[ 1 ][ 1 ][ 1 ]
            } else {
                RefStackMv[ NumMvFound ][ 0 ][ 0 ] = combinedMvs[ 0 ][ 0 ][ 0 ]
                RefStackMv[ NumMvFound ][ 0 ][ 1 ] = combinedMvs[ 0 ][ 0 ][ 1 ]
                RefStackMv[ NumMvFound ][ 1 ][ 0 ] = combinedMvs[ 0 ][ 1 ][ 0 ]
                RefStackMv[ NumMvFound ][ 1 ][ 1 ] = combinedMvs[ 0 ][ 1 ][ 1 ]
            }
            WeightStack[ NumMvFound ] = 2
#if VALIDATE_SPEC_MV_REF_CANDIDATE
            validate(70007)
            validate(NumMvFound)
            validate(RefStackMv[ NumMvFound ][ 0 ][ 0 ])
            validate(RefStackMv[ NumMvFound ][ 0 ][ 1 ])
            validate(RefStackMv[ NumMvFound ][ 1 ][ 0 ])
            validate(RefStackMv[ NumMvFound ][ 1 ][ 1 ])
#endif
            NumMvFound++
        } else {
            for ( idx = 0; idx < 2; idx++ ) {
                RefStackMv[ NumMvFound ][ 0 ][ 0 ] = combinedMvs[ idx ][ 0 ][ 0 ]
                RefStackMv[ NumMvFound ][ 0 ][ 1 ] = combinedMvs[ idx ][ 0 ][ 1 ]
                RefStackMv[ NumMvFound ][ 1 ][ 0 ] = combinedMvs[ idx ][ 1 ][ 0 ]
                RefStackMv[ NumMvFound ][ 1 ][ 1 ] = combinedMvs[ idx ][ 1 ][ 1 ]
                WeightStack[ NumMvFound ] = 2
#if VALIDATE_SPEC_MV_REF_CANDIDATE
                validate(70008)
                //validate(NumMvFound)
                validate(RefStackMv[ NumMvFound ][ 0 ][ 0 ])
                validate(RefStackMv[ NumMvFound ][ 0 ][ 1 ])
                validate(RefStackMv[ NumMvFound ][ 1 ][ 0 ])
                validate(RefStackMv[ NumMvFound ][ 1 ][ 1 ])
#endif
                NumMvFound++
            }
        }
    }
    if ( isCompound == 0 ) {
        for ( idx = NumMvFound; idx < 2; idx++ ) {
            RefStackMv[ idx ][ 0 ][ 0 ] = GlobalMvs[ 0 ][ 0 ]
            RefStackMv[ idx ][ 0 ][ 1 ] = GlobalMvs[ 0 ][ 1 ]
        }
    }
}

add_extra_mv_candidate( mvRow, mvCol, isCompound ) {
    int candMv[ 2 ]
#if VALIDATE_SPEC_MV_REF_CANDIDATE
    validate(70030)
    validate(mvRow)
    validate(mvCol)
#endif
    if ( isCompound ) {
        for ( candList = 0; candList < 2; candList++ ) {
            candRef = RefFrames[ mvRow ][ mvCol ][ candList ]
            if ( candRef > INTRA_FRAME ) {
                for ( list = 0; list < 2; list++ ) {
                    candMv[ 0 ] = Mvs[ mvRow ][ mvCol ][ candList ][ 0 ]
                    candMv[ 1 ] = Mvs[ mvRow ][ mvCol ][ candList ][ 1 ]
                    if ( candRef == RefFrame[ list ] && RefIdCount[ list ] < 2 ) {
                        RefIdMvs[ list ][ RefIdCount[ list ] ][ 0 ] = candMv[ 0 ]
                        RefIdMvs[ list ][ RefIdCount[ list ] ][ 1 ] = candMv[ 1 ]
#if VALIDATE_SPEC_MV_REF_CANDIDATE
                        validate(70020)
                        validate(RefIdCount[list])
                        validate(RefIdMvs[list][RefIdCount[list]][0])
                        validate(RefIdMvs[list][RefIdCount[list]][1])
#endif
                        RefIdCount[ list ]++
                    } else if ( RefDiffCount[ list ] < 2 ) {
                        if ( RefFrameSignBias[ candRef ] != RefFrameSignBias[ RefFrame[list] ] ) {
                            candMv[ 0 ] *= -1
                            candMv[ 1 ] *= -1
                        }
                        RefDiffMvs[ list ][ RefDiffCount[ list ] ][ 0 ] = candMv[ 0 ]
                        RefDiffMvs[ list ][ RefDiffCount[ list ] ][ 1 ] = candMv[ 1 ]
#if VALIDATE_SPEC_MV_REF_CANDIDATE
                        validate(70021)
                        validate(RefDiffCount[list])
                        validate(RefDiffMvs[list][RefDiffCount[list]][0])
                        validate(RefDiffMvs[list][RefDiffCount[list]][1])
#endif
                        RefDiffCount[ list ]++
                    }
                }
            }
        }
    } else {
        for ( candList = 0; candList < 2; candList++ ) {
            candRef = RefFrames[ mvRow ][ mvCol ][ candList ]
            if ( candRef > INTRA_FRAME ) {
                candMv[ 0 ] = Mvs[ mvRow ][ mvCol ][ candList ][ 0 ]
                candMv[ 1 ] = Mvs[ mvRow ][ mvCol ][ candList ][ 1 ]
                if ( RefFrameSignBias[ candRef ] != RefFrameSignBias[ RefFrame[ 0 ] ] ) {
                    candMv[ 0 ] *= -1
                    candMv[ 1 ] *= -1
                }
                for ( idx = 0; idx < NumMvFound; idx++ ) {
                    if ( candMv[ 0 ] == RefStackMv[ idx ][ 0 ][ 0 ] && candMv[ 1 ] == RefStackMv[ idx ][ 0 ][ 1 ] )
                        break
                }
                if ( idx == NumMvFound ) {
                    RefStackMv[ idx ][ 0 ][ 0 ] = candMv[ 0 ]
                    RefStackMv[ idx ][ 0 ][ 1 ] = candMv[ 1 ]
                    WeightStack[ idx ] = 2
#if VALIDATE_SPEC_MV_REF_CANDIDATE
                    validate(70009)
                    validate(NumMvFound)
                    validate(RefStackMv[ NumMvFound ][ 0 ][ 0 ])
                    validate(RefStackMv[ NumMvFound ][ 0 ][ 1 ])
#endif
                    NumMvFound++
                }
            }
        }
    }
}

context_and_clamping( isCompound, numNew ) {
    int refMv[ 2 ]
    bw = Block_Width[ MiSize ]
    bh = Block_Height[ MiSize ]
    numLists = ( isCompound ? 2 : 1 )
    for ( idx = 0; idx < NumMvFound ; idx++ ) {
        z = 0
        if ( idx + 1 < NumMvFound ) {
            w0 = WeightStack[ idx ]
            w1 = WeightStack[ idx + 1 ]
            if (w0 >= REF_CAT_LEVEL) {
                if (w1 < REF_CAT_LEVEL) {
                    z = 1
                }
            } else {
                z = 2
            }
        }
        DrlCtxStack[ idx ] = z
    }
    for ( list = 0; list < numLists; list++ ) {
        for ( idx = 0; idx < NumMvFound ; idx++ ) {
            refMv[ 0 ] = RefStackMv[ idx ][ list ][ 0 ]
            refMv[ 1 ] = RefStackMv[ idx ][ list ][ 1 ]
            refMv[ 0 ] = clamp_mv_row( refMv[ 0 ], MV_BORDER + bh * 8)
            refMv[ 1 ] = clamp_mv_col( refMv[ 1 ], MV_BORDER + bw * 8)
            RefStackMv[ idx ][ list ][ 0 ] = refMv[ 0 ]
            RefStackMv[ idx ][ list ][ 1 ] = refMv[ 1 ]
        }
    }
    if ( CloseMatches == 0 ) {
        NewMvContext = Min( TotalMatches, 1 )       // 0,1
        RefMvContext = TotalMatches
    } else if ( CloseMatches == 1 ) {
        NewMvContext = 3 - Min( numNew, 1 )       // 2,3
        RefMvContext = 2 + TotalMatches
    } else {
        NewMvContext = 5 - Min( numNew, 1 )       // 4,5
        RefMvContext = 5
    }
}

has_overlappable_candidates( ) {
    if ( AvailU ) {
        w4 = Num_4x4_Blocks_Wide[ MiSize ]
        for ( x4 = MiCol; x4 < Min( MiCols, MiCol + w4 ); x4 += 2 ) {
            if ( RefFrames[ MiRow - 1 ][ x4 | 1 ][ 0 ] > INTRA_FRAME )
                return 1
        }
    }
    if ( AvailL ) {
        h4 = Num_4x4_Blocks_High[ MiSize ]
        for ( y4 = MiRow; y4 < Min( MiRows, MiRow + h4 ); y4 += 2 ) {
            if ( RefFrames[ y4 | 1 ][ MiCol - 1 ][ 0 ] > INTRA_FRAME )
                return 1
        }
    }
    return 0
}

#define OLD_WARP 0
// It turns out the OLD_WARP process was subtly flawed
// When we have lots of invalid candidates, the search process
// does not discard them, so the number of samples scanned is increased.
// Could also search amongst the discarded samples, but no-one will ever
// want to implement that way, so change to the reference code approach.
//
// Note that the doTopRight condition for larger than 64x64 being dropped
// was not found by the first 3000 test streams, this is a tricky case to cover!

find_warp_samples( ) {
  NumSamples = 0
  NumSamplesScanned = 0
  w4 = Num_4x4_Blocks_Wide[ MiSize ]
  h4 = Num_4x4_Blocks_High[ MiSize ]
#if OLD_WARP
    for ( deltaCol = 0; deltaCol < w4; deltaCol++ )
        add_sample( -1, deltaCol )
    for ( deltaRow = 0; deltaRow < h4; deltaRow++ )
        add_sample( deltaRow, -1 )
    add_sample( -1, -1 )
    add_sample( -1, w4 )
#else
  doTopLeft = 1
  doTopRight = 1
  if (AvailU) {
    srcSize = MiSizes[ MiRow - 1 ][ MiCol ]
    srcW = Num_4x4_Blocks_Wide[ srcSize ]
    if (w4 <= srcW) {
      colOffset = -(MiCol & (srcW - 1))
      if (colOffset < 0)
        doTopLeft = 0
      if (colOffset + srcW > w4)
        doTopRight = 0
      add_sample( -1, 0 )
    } else {
      for (i = 0; i < Min( w4, MiCols - MiCol ); i += miStep) {
        srcSize = MiSizes[ MiRow - 1 ][ MiCol + i ]
        srcW = Num_4x4_Blocks_Wide[ srcSize ]
        miStep = Min(w4, srcW)
        add_sample( -1, i )
      }
    }
  }
  if (AvailL) {
    srcSize = MiSizes[ MiRow ][ MiCol - 1 ]
    srcH = Num_4x4_Blocks_High[ srcSize ]
    if (h4 <= srcH) {
      rowOffset = -(MiRow & (srcH - 1))
      if (rowOffset < 0)
        doTopLeft = 0
      add_sample( 0, -1 )
    } else {
      for (i = 0; i < Min( h4, MiRows - MiRow); i += miStep) {
        srcSize = MiSizes[ MiRow + i ][ MiCol - 1 ]
        srcH = Num_4x4_Blocks_High[ srcSize ]
        miStep = Min(h4, srcH)
        add_sample( i, -1 )
      }
    }
  }
  if ( doTopLeft ) {
    add_sample( -1, -1 )
  }
  if ( doTopRight ) {
    if ( Max( w4, h4 ) <= 16 ) {
      add_sample( -1, w4 )
    }
  }
#endif
    if ( NumSamples == 0 && NumSamplesScanned > 0 )
        NumSamples = 1
}

add_sample( deltaRow, deltaCol ) {
    int cand[ 4 ]

    if ( NumSamplesScanned >= LEAST_SQUARES_SAMPLES_MAX ) { return 0 }
    mvRow = MiRow + deltaRow
    mvCol = MiCol + deltaCol
    if ( is_inside( mvRow, mvCol ) == 0 ) { return 0 }
    if ( !RefFramesWritten[ mvRow ][ mvCol ] ) { return 0 }
    if ( RefFrames[ mvRow ][ mvCol ][ 0 ] != RefFrame[ 0 ] ) { return 0 }
    if ( RefFrames[ mvRow ][ mvCol ][ 1 ] != NONE ) { return 0 }
    candSz = MiSizes[ mvRow ][ mvCol ]
    candW4 = Num_4x4_Blocks_Wide[ candSz ]
    candH4 = Num_4x4_Blocks_High[ candSz ]
    candRow = mvRow & ( ~(candH4 - 1) )
    candCol = mvCol & ( ~(candW4 - 1) )
    midY = candRow * 4 + candH4 * 2 - 1
    midX = candCol * 4 + candW4 * 2 - 1
    threshold = Clip3( 16, 112, Max( Block_Width[ MiSize ], Block_Height[ MiSize ] ) )
    mvDiffRow = Abs( Mvs[ candRow ][ candCol ][ 0 ][ 0 ] - Mv[ 0 ][ 0 ] )
    mvDiffCol = Abs( Mvs[ candRow ][ candCol ][ 0 ][ 1 ] - Mv[ 0 ][ 1 ] )
    valid = ( ( mvDiffRow + mvDiffCol ) <= threshold )
    cand[ 0 ] = midY * 8
    cand[ 1 ] = midX * 8
    cand[ 2 ] = midY * 8 + Mvs[ candRow ][ candCol ][ 0 ][ 0 ]
    cand[ 3 ] = midX * 8 + Mvs[ candRow ][ candCol ][ 0 ][ 1 ]

#if OLD_WARP
    for ( j = 0; j < NumSamples; j++ ) {
        for ( i = 0; i < 4; i++ ) {
            if (cand[ i ] != CandList[ j ][ i ]) {
                break
            }
        }
        if (i == 4) { return 0 }
    }
#endif

    NumSamplesScanned++
    if (valid == 0 && NumSamplesScanned > 1) { return 0 }
    for ( i = 0; i < 4; i++ )
        CandList[ NumSamples ][ i ] = cand[ i ]
#if VALIDATE_SPEC_WARP
    // Comment out because argon streams calls this more often than needed
    //if (valid) {
    //validate(55005)
    //validate(midX)
    //validate(midY)
    //validate(Mvs[ candRow ][ candCol ][ 0 ][ 1 ])
    //validate(Mvs[ candRow ][ candCol ][ 0 ][ 0 ])
    //}
#endif
    if (valid == 1) NumSamples++
}

motion_vector_scaling( plane, refIdx, x, y, int32pointer mv, int32pointer out ) {
    int64 origX
    int64 origY
    int64 baseX
    int64 baseY
    int64 xScale
    int64 yScale
    CHECK( 2 * FrameWidth >= RefUpscaledWidth[ refIdx ], "2 * FrameWidth must be greater than or equal to RefUpscaledWidth[ refIdx ]" )
    CHECK( 2 * FrameHeight >= RefFrameHeight[ refIdx ], "2 * FrameHeight must be greater than or equal to RefFrameHeight[ refIdx ]" )
    CHECK( FrameWidth <= 16 * RefUpscaledWidth[ refIdx ], "FrameWidth must be less than or equal to 16 * RefUpscaledWidth[ refIdx ]" )
    CHECK( FrameHeight <= 16 * RefFrameHeight[ refIdx ], "FrameHeight must be less than or equal to 16 * RefFrameHeight[ refIdx ]" )

    xScale = ( ( RefUpscaledWidth[ refIdx ] << REF_SCALE_SHIFT ) + ( FrameWidth / 2 ) ) / FrameWidth
    yScale = ( ( RefFrameHeight[ refIdx ] << REF_SCALE_SHIFT ) + ( FrameHeight / 2 ) ) / FrameHeight

    if ( plane == 0 ) {
        subX = 0
        subY = 0
    } else {
        subX = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
        subY = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
    }

    halfSample = ( 1 << ( SUBPEL_BITS - 1 ) )
    origX = ( (x << SUBPEL_BITS) + ( ( 2 * mv[1] ) >> subX ) + halfSample )
    origY = ( (y << SUBPEL_BITS) + ( ( 2 * mv[0] ) >> subY ) + halfSample )
    baseX = (origX * xScale - ( halfSample << REF_SCALE_SHIFT ) )
    baseY = (origY * yScale - ( halfSample << REF_SCALE_SHIFT ) )
    off = ( ( 1 << (SCALE_SUBPEL_BITS - SUBPEL_BITS) ) / 2 )
    out[ 0 ] = (Round2Signed( baseX, REF_SCALE_SHIFT + SUBPEL_BITS - SCALE_SUBPEL_BITS) + off)
    out[ 1 ] = (Round2Signed( baseY, REF_SCALE_SHIFT + SUBPEL_BITS - SCALE_SUBPEL_BITS) + off)
    out[ 2 ] = Round2Signed( xScale, REF_SCALE_SHIFT - SCALE_SUBPEL_BITS)
    out[ 3 ] = Round2Signed( yScale, REF_SCALE_SHIFT - SCALE_SUBPEL_BITS)
}

int Subpel_Filters[ 6 ][ 16 ][ 8 ] = {
  {
    { 0, 0, 0, 128, 0, 0, 0, 0 },
    { 0, 2, -6, 126, 8, -2, 0, 0 },
    { 0, 2, -10, 122, 18, -4, 0, 0 },
    { 0, 2, -12, 116, 28, -8, 2, 0 },
    { 0, 2, -14, 110, 38, -10, 2, 0 },
    { 0, 2, -14, 102, 48, -12, 2, 0 },
    { 0, 2, -16, 94, 58, -12, 2, 0 },
    { 0, 2, -14, 84, 66, -12, 2, 0 },
    { 0, 2, -14, 76, 76, -14, 2, 0 },
    { 0, 2, -12, 66, 84, -14, 2, 0 },
    { 0, 2, -12, 58, 94, -16, 2, 0 },
    { 0, 2, -12, 48, 102, -14, 2, 0 },
    { 0, 2, -10, 38, 110, -14, 2, 0 },
    { 0, 2, -8, 28, 116, -12, 2, 0 },
    { 0, 0, -4, 18, 122, -10, 2, 0 },
    { 0, 0, -2, 8, 126, -6, 2, 0 }
  },
  {
    { 0, 0, 0, 128, 0, 0, 0, 0 },
    { 0, 2, 28, 62, 34, 2, 0, 0 },
    { 0, 0, 26, 62, 36, 4, 0, 0 },
    { 0, 0, 22, 62, 40, 4, 0, 0 },
    { 0, 0, 20, 60, 42, 6, 0, 0 },
    { 0, 0, 18, 58, 44, 8, 0, 0 },
    { 0, 0, 16, 56, 46, 10, 0, 0 },
    { 0, -2, 16, 54, 48, 12, 0, 0 },
    { 0, -2, 14, 52, 52, 14, -2, 0 },
    { 0, 0, 12, 48, 54, 16, -2, 0 },
    { 0, 0, 10, 46, 56, 16, 0, 0 },
    { 0, 0, 8, 44, 58, 18, 0, 0 },
    { 0, 0, 6, 42, 60, 20, 0, 0 },
    { 0, 0, 4, 40, 62, 22, 0, 0 },
    { 0, 0, 4, 36, 62, 26, 0, 0 },
    { 0, 0, 2, 34, 62, 28, 2, 0 }
  },
  {
    { 0, 0, 0, 128, 0, 0, 0, 0 },
    { -2, 2, -6, 126, 8, -2, 2, 0 },
    { -2, 6, -12, 124, 16, -6, 4, -2 },
    { -2, 8, -18, 120, 26, -10, 6, -2 },
    { -4, 10, -22, 116, 38, -14, 6, -2 },
    { -4, 10, -22, 108, 48, -18, 8, -2 },
    { -4, 10, -24, 100, 60, -20, 8, -2 },
    { -4, 10, -24, 90, 70, -22, 10, -2 },
    { -4, 12, -24, 80, 80, -24, 12, -4 },
    { -2, 10, -22, 70, 90, -24, 10, -4 },
    { -2, 8, -20, 60, 100, -24, 10, -4 },
    { -2, 8, -18, 48, 108, -22, 10, -4 },
    { -2, 6, -14, 38, 116, -22, 10, -4 },
    { -2, 6, -10, 26, 120, -18, 8, -2 },
    { -2, 4, -6, 16, 124, -12, 6, -2 },
    { 0, 2, -2, 8, 126, -6, 2, -2 }
  },
  {
    { 0, 0, 0, 128, 0, 0, 0, 0 },
    { 0, 0, 0, 120, 8, 0, 0, 0 },
    { 0, 0, 0, 112, 16, 0, 0, 0 },
    { 0, 0, 0, 104, 24, 0, 0, 0 },
    { 0, 0, 0, 96, 32, 0, 0, 0 },
    { 0, 0, 0, 88, 40, 0, 0, 0 },
    { 0, 0, 0, 80, 48, 0, 0, 0 },
    { 0, 0, 0, 72, 56, 0, 0, 0 },
    { 0, 0, 0, 64, 64, 0, 0, 0 },
    { 0, 0, 0, 56, 72, 0, 0, 0 },
    { 0, 0, 0, 48, 80, 0, 0, 0 },
    { 0, 0, 0, 40, 88, 0, 0, 0 },
    { 0, 0, 0, 32, 96, 0, 0, 0 },
    { 0, 0, 0, 24, 104, 0, 0, 0 },
    { 0, 0, 0, 16, 112, 0, 0, 0 },
    { 0, 0, 0, 8, 120, 0, 0, 0 }
  },
  {
    { 0, 0, 0, 128, 0, 0, 0, 0 },
    { 0, 0, -4, 126, 8, -2, 0, 0 },
    { 0, 0, -8, 122, 18, -4, 0, 0 },
    { 0, 0, -10, 116, 28, -6, 0, 0 },
    { 0, 0, -12, 110, 38, -8, 0, 0 },
    { 0, 0, -12, 102, 48, -10, 0, 0 },
    { 0, 0, -14, 94, 58, -10, 0, 0 },
    { 0, 0, -12, 84, 66, -10, 0, 0 },
    { 0, 0, -12, 76, 76, -12, 0, 0 },
    { 0, 0, -10, 66, 84, -12, 0, 0 },
    { 0, 0, -10, 58, 94, -14, 0, 0 },
    { 0, 0, -10, 48, 102, -12, 0, 0 },
    { 0, 0, -8, 38, 110, -12, 0, 0 },
    { 0, 0, -6, 28, 116, -10, 0, 0 },
    { 0, 0, -4, 18, 122, -8, 0, 0 },
    { 0, 0, -2, 8, 126, -4, 0, 0 }
  },
  {
    { 0, 0, 0, 128, 0, 0, 0, 0 },
    { 0, 0, 30, 62, 34, 2, 0, 0 },
    { 0, 0, 26, 62, 36, 4, 0, 0 },
    { 0, 0, 22, 62, 40, 4, 0, 0 },
    { 0, 0, 20, 60, 42, 6, 0, 0 },
    { 0, 0, 18, 58, 44, 8, 0, 0 },
    { 0, 0, 16, 56, 46, 10, 0, 0 },
    { 0, 0, 14, 54, 48, 12, 0, 0 },
    { 0, 0, 12, 52, 52, 12, 0, 0 },
    { 0, 0, 12, 48, 54, 14, 0, 0 },
    { 0, 0, 10, 46, 56, 16, 0, 0 },
    { 0, 0, 8, 44, 58, 18, 0, 0 },
    { 0, 0, 6, 42, 60, 20, 0, 0 },
    { 0, 0, 4, 40, 62, 22, 0, 0 },
    { 0, 0, 4, 36, 62, 26, 0, 0 },
    { 0, 0, 2, 34, 62, 30, 0, 0 }
  }
};

block_inter_prediction( plane, refList, refIdx, startX, startY, xStep, yStep, w, h, candRow, candCol ) {
    int intermediate[ 134*2 ][ 64*2 ] // TODO where did the 134 come from?

    if ( plane == 0 ) {
        subX = 0
        subY = 0
    } else {
        subX = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
        subY = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
    }
    lastX = ( (RefUpscaledWidth[ refIdx ] + subX) >> subX) - 1
    lastY = ( (RefFrameHeight[ refIdx ] + subY) >> subY) - 1
    intermediateHeight = (((h - 1) * yStep + (1 << SCALE_SUBPEL_BITS) - 1) >> SCALE_SUBPEL_BITS) + 8
    interpFilter = InterpFilters[ candRow ][ candCol ][ 1 ]
    if ( w <= 4 ) {
        if (interpFilter == EIGHTTAP || interpFilter == EIGHTTAP_SHARP) {
            interpFilter = 4
        } else if (interpFilter == EIGHTTAP_SMOOTH) {
            interpFilter = 5
        }
    }
#if VALIDATE_SPEC_INTER2
    if (plane == VALIDATE_SPEC_INTER2_PLANE && ValidationInterIsObmc == 0 && !use_intrabc) {
        validate(81006)
        validate(interpFilter)
    }
#endif
    for ( r = 0; r < intermediateHeight; r++ ) {
        for ( c = 0; c < w; c++ ) {
            s = 0
            pp = startX + xStep * c
            for ( t = 0; t < 8; t++ ) {
                if ( refIdx == -1 ) {
                    v = CurrFrame[ plane ][ Clip3( 0, lastY, (startY >> 10) + r - 3 ) ]
                               [ Clip3( 0, lastX, (pp >> 10) + t - 3 ) ]
                }
                else {
                    v = FrameStore[ refIdx ][ plane ][ Clip3( 0, lastY, (startY >> 10) + r - 3 ) ]
                               [ Clip3( 0, lastX, (pp >> 10) + t - 3 ) ]
                }
#if COVER
                uint_0_5 interpFilter_1
                interpFilter_1 = interpFilter
                uint_0_15 offset_3
                offset_3 = (pp >> 6) & SUBPEL_MASK
                uint_0_7 T_1
                T_1 = t
                COVERCROSS(CROSS_SUBPEL_FILTERS, interpFilter_1, offset_3, T_1)
#endif // COVER
                s += Subpel_Filters[ interpFilter ][ (pp >> 6) & SUBPEL_MASK ][ t ] * v // E-25 [RNG-Inter1]
            }

            intermediate[ r ][ c ] = Round2(s, InterRound0) // E-26 [RNG-Inter2]
#if VALIDATE_SPEC_INTER2
            // av1bsrc doesn't change the fetch window size for intrabc so these values
            // are not expected to match
            // Also the scaling is slightly different in av1src, but this should be equivalent.
            // TODO may want to recode this to avoid sampling non-existent pixels on the Python side
            //if (plane == VALIDATE_SPEC_INTER2_PLANE && use_intrabc) {
            //    validate(81009)
            //    validate(r)
            //    validate(c)
            //    validate(intermediate[ r ][ c ])
            //}
#endif
        }
    }

    interpFilter = InterpFilters[ candRow ][ candCol ][ 0 ]
    if ( h <= 4 ) {
        if (interpFilter == EIGHTTAP || interpFilter == EIGHTTAP_SHARP) {
            interpFilter = 4
        } else if (interpFilter == EIGHTTAP_SMOOTH) {
            interpFilter = 5
        }
    }
#if VALIDATE_SPEC_INTER2
    if (plane == VALIDATE_SPEC_INTER2_PLANE && ValidationInterIsObmc == 0 && !use_intrabc) {
        validate(81007)
        validate(interpFilter)
    }
#endif
    for ( r = 0; r < h; r++ ) {
        for ( c = 0; c < w; c++ ) {
            s = 0
            pp = (startY & 1023) + yStep * r
            for ( t = 0; t < 8; t++ ) {
#if COVER
                uint_0_5 interpFilter_2
                interpFilter_2 = interpFilter
                uint_0_15 offset_4
                offset_4 = (pp >> 6) & SUBPEL_MASK
                uint_0_7 T_2
                T_2 = t
                COVERCROSS(CROSS_SUBPEL_FILTERS, interpFilter_2, offset_4, T_2)
#endif // COVER
                s += Subpel_Filters[ interpFilter ][ (pp >> 6) & SUBPEL_MASK ][ t ] * intermediate[ (pp >> 10) + t ][ c ] // E-27 [RNG-Inter3]
            }
            preds[ refList ][ r ][ c ] = Round2( s, InterRound1 ) // E-28 [RNG-Inter4]
#if VALIDATE_SPEC_INTER2
            if (plane == VALIDATE_SPEC_INTER2_PLANE && use_intrabc) {
                validate(81008)
                validate(r)
                validate(c)
                validate(preds[ refList ][ r ][ c ])
            }
#endif
        }
    }
}

int Warped_Filters[WARPEDPIXEL_PREC_SHIFTS * 3 + 1][8] = {
    { 0,   0, 127,   1,   0, 0, 0, 0 }, { 0, - 1, 127,   2,   0, 0, 0, 0 },
    { 1, - 3, 127,   4, - 1, 0, 0, 0 }, { 1, - 4, 126,   6, - 2, 1, 0, 0 },
    { 1, - 5, 126,   8, - 3, 1, 0, 0 }, { 1, - 6, 125,  11, - 4, 1, 0, 0 },
    { 1, - 7, 124,  13, - 4, 1, 0, 0 }, { 2, - 8, 123,  15, - 5, 1, 0, 0 },
    { 2, - 9, 122,  18, - 6, 1, 0, 0 }, { 2, -10, 121,  20, - 6, 1, 0, 0 },
    { 2, -11, 120,  22, - 7, 2, 0, 0 }, { 2, -12, 119,  25, - 8, 2, 0, 0 },
    { 3, -13, 117,  27, - 8, 2, 0, 0 }, { 3, -13, 116,  29, - 9, 2, 0, 0 },
    { 3, -14, 114,  32, -10, 3, 0, 0 }, { 3, -15, 113,  35, -10, 2, 0, 0 },
    { 3, -15, 111,  37, -11, 3, 0, 0 }, { 3, -16, 109,  40, -11, 3, 0, 0 },
    { 3, -16, 108,  42, -12, 3, 0, 0 }, { 4, -17, 106,  45, -13, 3, 0, 0 },
    { 4, -17, 104,  47, -13, 3, 0, 0 }, { 4, -17, 102,  50, -14, 3, 0, 0 },
    { 4, -17, 100,  52, -14, 3, 0, 0 }, { 4, -18,  98,  55, -15, 4, 0, 0 },
    { 4, -18,  96,  58, -15, 3, 0, 0 }, { 4, -18,  94,  60, -16, 4, 0, 0 },
    { 4, -18,  91,  63, -16, 4, 0, 0 }, { 4, -18,  89,  65, -16, 4, 0, 0 },
    { 4, -18,  87,  68, -17, 4, 0, 0 }, { 4, -18,  85,  70, -17, 4, 0, 0 },
    { 4, -18,  82,  73, -17, 4, 0, 0 }, { 4, -18,  80,  75, -17, 4, 0, 0 },
    { 4, -18,  78,  78, -18, 4, 0, 0 }, { 4, -17,  75,  80, -18, 4, 0, 0 },
    { 4, -17,  73,  82, -18, 4, 0, 0 }, { 4, -17,  70,  85, -18, 4, 0, 0 },
    { 4, -17,  68,  87, -18, 4, 0, 0 }, { 4, -16,  65,  89, -18, 4, 0, 0 },
    { 4, -16,  63,  91, -18, 4, 0, 0 }, { 4, -16,  60,  94, -18, 4, 0, 0 },
    { 3, -15,  58,  96, -18, 4, 0, 0 }, { 4, -15,  55,  98, -18, 4, 0, 0 },
    { 3, -14,  52, 100, -17, 4, 0, 0 }, { 3, -14,  50, 102, -17, 4, 0, 0 },
    { 3, -13,  47, 104, -17, 4, 0, 0 }, { 3, -13,  45, 106, -17, 4, 0, 0 },
    { 3, -12,  42, 108, -16, 3, 0, 0 }, { 3, -11,  40, 109, -16, 3, 0, 0 },
    { 3, -11,  37, 111, -15, 3, 0, 0 }, { 2, -10,  35, 113, -15, 3, 0, 0 },
    { 3, -10,  32, 114, -14, 3, 0, 0 }, { 2, - 9,  29, 116, -13, 3, 0, 0 },
    { 2, - 8,  27, 117, -13, 3, 0, 0 }, { 2, - 8,  25, 119, -12, 2, 0, 0 },
    { 2, - 7,  22, 120, -11, 2, 0, 0 }, { 1, - 6,  20, 121, -10, 2, 0, 0 },
    { 1, - 6,  18, 122, - 9, 2, 0, 0 }, { 1, - 5,  15, 123, - 8, 2, 0, 0 },
    { 1, - 4,  13, 124, - 7, 1, 0, 0 }, { 1, - 4,  11, 125, - 6, 1, 0, 0 },
    { 1, - 3,   8, 126, - 5, 1, 0, 0 }, { 1, - 2,   6, 126, - 4, 1, 0, 0 },
    { 0, - 1,   4, 127, - 3, 1, 0, 0 }, { 0,   0,   2, 127, - 1, 0, 0, 0 },

    { 0,  0,   0, 127,   1,   0,  0,  0}, { 0,  0,  -1, 127,   2,   0,  0,  0},
    { 0,  1,  -3, 127,   4,  -2,  1,  0}, { 0,  1,  -5, 127,   6,  -2,  1,  0},
    { 0,  2,  -6, 126,   8,  -3,  1,  0}, {-1,  2,  -7, 126,  11,  -4,  2, -1},
    {-1,  3,  -8, 125,  13,  -5,  2, -1}, {-1,  3, -10, 124,  16,  -6,  3, -1},
    {-1,  4, -11, 123,  18,  -7,  3, -1}, {-1,  4, -12, 122,  20,  -7,  3, -1},
    {-1,  4, -13, 121,  23,  -8,  3, -1}, {-2,  5, -14, 120,  25,  -9,  4, -1},
    {-1,  5, -15, 119,  27, -10,  4, -1}, {-1,  5, -16, 118,  30, -11,  4, -1},
    {-2,  6, -17, 116,  33, -12,  5, -1}, {-2,  6, -17, 114,  35, -12,  5, -1},
    {-2,  6, -18, 113,  38, -13,  5, -1}, {-2,  7, -19, 111,  41, -14,  6, -2},
    {-2,  7, -19, 110,  43, -15,  6, -2}, {-2,  7, -20, 108,  46, -15,  6, -2},
    {-2,  7, -20, 106,  49, -16,  6, -2}, {-2,  7, -21, 104,  51, -16,  7, -2},
    {-2,  7, -21, 102,  54, -17,  7, -2}, {-2,  8, -21, 100,  56, -18,  7, -2},
    {-2,  8, -22,  98,  59, -18,  7, -2}, {-2,  8, -22,  96,  62, -19,  7, -2},
    {-2,  8, -22,  94,  64, -19,  7, -2}, {-2,  8, -22,  91,  67, -20,  8, -2},
    {-2,  8, -22,  89,  69, -20,  8, -2}, {-2,  8, -22,  87,  72, -21,  8, -2},
    {-2,  8, -21,  84,  74, -21,  8, -2}, {-2,  8, -22,  82,  77, -21,  8, -2},
    {-2,  8, -21,  79,  79, -21,  8, -2}, {-2,  8, -21,  77,  82, -22,  8, -2},
    {-2,  8, -21,  74,  84, -21,  8, -2}, {-2,  8, -21,  72,  87, -22,  8, -2},
    {-2,  8, -20,  69,  89, -22,  8, -2}, {-2,  8, -20,  67,  91, -22,  8, -2},
    {-2,  7, -19,  64,  94, -22,  8, -2}, {-2,  7, -19,  62,  96, -22,  8, -2},
    {-2,  7, -18,  59,  98, -22,  8, -2}, {-2,  7, -18,  56, 100, -21,  8, -2},
    {-2,  7, -17,  54, 102, -21,  7, -2}, {-2,  7, -16,  51, 104, -21,  7, -2},
    {-2,  6, -16,  49, 106, -20,  7, -2}, {-2,  6, -15,  46, 108, -20,  7, -2},
    {-2,  6, -15,  43, 110, -19,  7, -2}, {-2,  6, -14,  41, 111, -19,  7, -2},
    {-1,  5, -13,  38, 113, -18,  6, -2}, {-1,  5, -12,  35, 114, -17,  6, -2},
    {-1,  5, -12,  33, 116, -17,  6, -2}, {-1,  4, -11,  30, 118, -16,  5, -1},
    {-1,  4, -10,  27, 119, -15,  5, -1}, {-1,  4,  -9,  25, 120, -14,  5, -2},
    {-1,  3,  -8,  23, 121, -13,  4, -1}, {-1,  3,  -7,  20, 122, -12,  4, -1},
    {-1,  3,  -7,  18, 123, -11,  4, -1}, {-1,  3,  -6,  16, 124, -10,  3, -1},
    {-1,  2,  -5,  13, 125,  -8,  3, -1}, {-1,  2,  -4,  11, 126,  -7,  2, -1},
    { 0,  1,  -3,   8, 126,  -6,  2,  0}, { 0,  1,  -2,   6, 127,  -5,  1,  0},
    { 0,  1,  -2,   4, 127,  -3,  1,  0}, { 0,  0,   0,   2, 127,  -1,  0,  0},

    { 0, 0, 0,   1, 127,   0,   0, 0 }, { 0, 0, 0, - 1, 127,   2,   0, 0 },
    { 0, 0, 1, - 3, 127,   4, - 1, 0 }, { 0, 0, 1, - 4, 126,   6, - 2, 1 },
    { 0, 0, 1, - 5, 126,   8, - 3, 1 }, { 0, 0, 1, - 6, 125,  11, - 4, 1 },
    { 0, 0, 1, - 7, 124,  13, - 4, 1 }, { 0, 0, 2, - 8, 123,  15, - 5, 1 },
    { 0, 0, 2, - 9, 122,  18, - 6, 1 }, { 0, 0, 2, -10, 121,  20, - 6, 1 },
    { 0, 0, 2, -11, 120,  22, - 7, 2 }, { 0, 0, 2, -12, 119,  25, - 8, 2 },
    { 0, 0, 3, -13, 117,  27, - 8, 2 }, { 0, 0, 3, -13, 116,  29, - 9, 2 },
    { 0, 0, 3, -14, 114,  32, -10, 3 }, { 0, 0, 3, -15, 113,  35, -10, 2 },
    { 0, 0, 3, -15, 111,  37, -11, 3 }, { 0, 0, 3, -16, 109,  40, -11, 3 },
    { 0, 0, 3, -16, 108,  42, -12, 3 }, { 0, 0, 4, -17, 106,  45, -13, 3 },
    { 0, 0, 4, -17, 104,  47, -13, 3 }, { 0, 0, 4, -17, 102,  50, -14, 3 },
    { 0, 0, 4, -17, 100,  52, -14, 3 }, { 0, 0, 4, -18,  98,  55, -15, 4 },
    { 0, 0, 4, -18,  96,  58, -15, 3 }, { 0, 0, 4, -18,  94,  60, -16, 4 },
    { 0, 0, 4, -18,  91,  63, -16, 4 }, { 0, 0, 4, -18,  89,  65, -16, 4 },
    { 0, 0, 4, -18,  87,  68, -17, 4 }, { 0, 0, 4, -18,  85,  70, -17, 4 },
    { 0, 0, 4, -18,  82,  73, -17, 4 }, { 0, 0, 4, -18,  80,  75, -17, 4 },
    { 0, 0, 4, -18,  78,  78, -18, 4 }, { 0, 0, 4, -17,  75,  80, -18, 4 },
    { 0, 0, 4, -17,  73,  82, -18, 4 }, { 0, 0, 4, -17,  70,  85, -18, 4 },
    { 0, 0, 4, -17,  68,  87, -18, 4 }, { 0, 0, 4, -16,  65,  89, -18, 4 },
    { 0, 0, 4, -16,  63,  91, -18, 4 }, { 0, 0, 4, -16,  60,  94, -18, 4 },
    { 0, 0, 3, -15,  58,  96, -18, 4 }, { 0, 0, 4, -15,  55,  98, -18, 4 },
    { 0, 0, 3, -14,  52, 100, -17, 4 }, { 0, 0, 3, -14,  50, 102, -17, 4 },
    { 0, 0, 3, -13,  47, 104, -17, 4 }, { 0, 0, 3, -13,  45, 106, -17, 4 },
    { 0, 0, 3, -12,  42, 108, -16, 3 }, { 0, 0, 3, -11,  40, 109, -16, 3 },
    { 0, 0, 3, -11,  37, 111, -15, 3 }, { 0, 0, 2, -10,  35, 113, -15, 3 },
    { 0, 0, 3, -10,  32, 114, -14, 3 }, { 0, 0, 2, - 9,  29, 116, -13, 3 },
    { 0, 0, 2, - 8,  27, 117, -13, 3 }, { 0, 0, 2, - 8,  25, 119, -12, 2 },
    { 0, 0, 2, - 7,  22, 120, -11, 2 }, { 0, 0, 1, - 6,  20, 121, -10, 2 },
    { 0, 0, 1, - 6,  18, 122, - 9, 2 }, { 0, 0, 1, - 5,  15, 123, - 8, 2 },
    { 0, 0, 1, - 4,  13, 124, - 7, 1 }, { 0, 0, 1, - 4,  11, 125, - 6, 1 },
    { 0, 0, 1, - 3,   8, 126, - 5, 1 }, { 0, 0, 1, - 2,   6, 126, - 4, 1 },
    { 0, 0, 0, - 1,   4, 127, - 3, 1 }, { 0, 0, 0,   0,   2, 127, - 1, 0 },

    { 0, 0, 0,   0,   2, 127, - 1, 0 }
};

block_warp( useWarp, plane, refList, x, y, i8, j8, w, h ) {
    int warpParams[ 6 ]
    int intermediate[ 15 ][ 8 ]
    int ssout[ 5 ]
#if VALIDATE_SPEC_INTER2
    int validate_spec_inter2_aux_tmp[ 5 ]
#endif
    refIdx = ref_frame_idx[ RefFrame[ refList ] - LAST_FRAME ]
    if ( plane == 0 ) {
        subX = 0
        subY = 0
    } else {
        subX = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
        subY = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
    }
    lastX = ( (RefUpscaledWidth[ refIdx ] + subX) >> subX ) - 1
    lastY = ( (RefFrameHeight[ refIdx ] + subY) >> subY ) - 1
    srcX = (x + j8 * 8 + 4) << subX
    srcY = (y + i8 * 8 + 4) << subY
    if ( useWarp == 1 ) {
        for ( i = 0; i < 6; i++ )
            warpParams[ i ] = LocalWarpParams[ i ]
    }
    else {
        for ( i = 0; i < 6; i++ )
            warpParams[ i ] = gm_params[ RefFrame[ refList ] ][ i ]
    }
    dstX = warpParams[2] * srcX + warpParams[3] * srcY + warpParams[0]
    dstY = warpParams[4] * srcX + warpParams[5] * srcY + warpParams[1]

    setup_shear( warpParams, ssout )
    warpValid = ssout[ 0 ]
    alpha = ssout[ 1 ]
    beta = ssout[ 2 ]
    gamma = ssout[ 3 ]
    delta = ssout[ 4 ]

    x4 = dstX >> subX
    y4 = dstY >> subY
    ix4 = x4 >> WARPEDMODEL_PREC_BITS
    sx4 = x4 & ((1 << WARPEDMODEL_PREC_BITS) - 1)
    iy4 = y4 >> WARPEDMODEL_PREC_BITS
    sy4 = y4 & ((1 << WARPEDMODEL_PREC_BITS) - 1)
#if VALIDATE_SPEC_INTER2
    if ( plane == VALIDATE_SPEC_INTER2_PLANE && i8==0 && j8==0) {
        validate(81014)
        validate(warpParams[0])
        validate(warpParams[1])
        validate(warpParams[2])
        validate(warpParams[3])
        validate(warpParams[4])
        validate(warpParams[5])
        validate(alpha)
        validate(beta)
        validate(gamma)
        validate(delta)
        setup_shear(warpParams, validate_spec_inter2_aux_tmp)
    }
#endif
    for (k = -7; k < 8; k++) {
        for (l = -4; l < 4; l++) {
            sx = sx4 + alpha * l + beta * k
            offs = Round2(sx, WARPEDDIFF_PREC_BITS) + WARPEDPIXEL_PREC_SHIFTS
            s = 0
            for (m = 0; m < 8; m++) {
#if COVER
                uint_0_192 offset_1
                offset_1 = offs
                uint_0_7 M_1
                M_1 = m
                COVERCROSS(CROSS_WARPED_FILTERS, offset_1, M_1)
#endif // COVER
                s += Warped_Filters[ offs ][ m ] * FrameStore[ refIdx ][ plane ][ Clip3( 0, lastY, iy4 + k ) ][ Clip3( 0, lastX, ix4 + l - 3 + m ) ] // E-29 [RNG-Warp1]
#if VALIDATE_SPEC_INTER2
                  if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
                      validate(81012)
                      validate(k)
                      validate(l)
                      validate(m)
                      validate(offs)
                      validate(Warped_Filters[ offs ][ m ])
                      validate(FrameStore[ refIdx ][ plane ][ Clip3( 0, lastY, iy4 + k ) ]
                              [ Clip3( 0, lastX, ix4 + l - 3 + m ) ])
                      validate(s)
                  }
#endif
            }
            intermediate[(k + 7)][(l + 4)] = Round2( s, InterRound0 ) // E-30 [RNG-Warp2]
#if VALIDATE_SPEC_INTER2
            if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
                validate(81010)
                validate(k)
                validate(l)
                validate(intermediate[(k + 7)][(l + 4)])
            }
#endif
        }
    }

    for (k = -4; k < Min(4, h - i8 * 8 - 4); k++) {
        for (l = -4; l < Min(4, w - j8 * 8 - 4); l++) {
            sy = sy4 + gamma * l + delta * k
            offs = Round2(sy, WARPEDDIFF_PREC_BITS) + WARPEDPIXEL_PREC_SHIFTS
            s = 0
            for (m = 0; m < 8; m++) {
#if COVER
                uint_0_192 offset_2
                offset_2 = offs
                uint_0_7 M_2
                M_2 = m
                COVERCROSS(CROSS_WARPED_FILTERS, offset_2, M_2)
#endif // COVER
                s += Warped_Filters[offs][m] * intermediate[(k + m + 4)][(l + 4)] // E-31 [RNG-Warp3]
#if VALIDATE_SPEC_INTER2
                if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
                    validate(81013)
                    validate(k)
                    validate(l)
                    validate(m)
                    validate(offs)
                    validate(Warped_Filters[ offs ][ m ])
                    validate(intermediate[(k + m + 4)][(l + 4)])
                    validate(s)
                }
#endif
            }
            preds[ refList ][ i8 * 8 + k + 4 ][ j8 * 8 + l + 4 ] = Round2( s, InterRound1 ) // E-32 [RNG-Warp4]
#if VALIDATE_SPEC_INTER2
          if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
              validate(81011)
              validate(k)
              validate(l)
              validate(preds[ refList ][ i8 * 8 + k + 4 ][ j8 * 8 + l + 4 ])
          }
#endif
        }
    }
    return 1
}

setup_shear( int32pointer warpParams, int32pointer out ) {
    int rdout[ 2 ]
    int64 v
    int64 w
    alpha0 = Clip3( -32768, 32767, warpParams[ 2 ] - (1 << WARPEDMODEL_PREC_BITS) ) // E-33 [RNG-Shear1]
    beta0 = Clip3( -32768, 32767, warpParams[ 3 ] ) // E-34 [RNG-Shear2]
    resolve_divisor( warpParams[ 2 ], rdout )
    divShift = rdout[ 0 ]
    divFactor = rdout[ 1 ]
    v = ( warpParams[ 4 ] << WARPEDMODEL_PREC_BITS ) // E-35 [RNG-Shear3]
    gamma0 = Clip3( -32768, 32767, Round2Signed( v * divFactor, divShift ) ) // E-36 [RNG-Shear4]
    w = ( warpParams[ 3 ] * warpParams[ 4 ] ) // E-37 [RNG-Shear5]
    delta0 = Clip3( -32768, 32767, warpParams[ 5 ] - Round2Signed( w * divFactor, divShift ) - (1 << WARPEDMODEL_PREC_BITS) ) // E-38 [RNG-Shear6]

    alpha = Round2Signed( alpha0, WARP_PARAM_REDUCE_BITS) << WARP_PARAM_REDUCE_BITS // E-39 [RNG-Shear7]
    beta  = Round2Signed(  beta0, WARP_PARAM_REDUCE_BITS) << WARP_PARAM_REDUCE_BITS // E-40 [RNG-Shear8]
    gamma = Round2Signed( gamma0, WARP_PARAM_REDUCE_BITS) << WARP_PARAM_REDUCE_BITS // E-41 [RNG-Shear9]
    delta = Round2Signed( delta0, WARP_PARAM_REDUCE_BITS) << WARP_PARAM_REDUCE_BITS // E-42 [RNG-Shear10]

    out[ 1 ] = alpha
    out[ 2 ] = beta
    out[ 3 ] = gamma
    out[ 4 ] = delta

    t = ( 4 * Abs( alpha ) + 7 * Abs( beta ) ) // E-43 [RNG-Shear11]
    u = ( 4 * Abs( gamma ) + 4 * Abs( delta ) ) // E-44 [RNG-Shear12]

    if (t >= (1 << WARPEDMODEL_PREC_BITS))
        out[ 0 ] = 0
    else if (u >= (1 << WARPEDMODEL_PREC_BITS))
        out[ 0 ] = 0
    else
        out[ 0 ] = 1

    return 1
}

int Div_Lut[DIV_LUT_NUM] = {
  16384, 16320, 16257, 16194, 16132, 16070, 16009, 15948, 15888, 15828, 15768,
  15709, 15650, 15592, 15534, 15477, 15420, 15364, 15308, 15252, 15197, 15142,
  15087, 15033, 14980, 14926, 14873, 14821, 14769, 14717, 14665, 14614, 14564,
  14513, 14463, 14413, 14364, 14315, 14266, 14218, 14170, 14122, 14075, 14028,
  13981, 13935, 13888, 13843, 13797, 13752, 13707, 13662, 13618, 13574, 13530,
  13487, 13443, 13400, 13358, 13315, 13273, 13231, 13190, 13148, 13107, 13066,
  13026, 12985, 12945, 12906, 12866, 12827, 12788, 12749, 12710, 12672, 12633,
  12596, 12558, 12520, 12483, 12446, 12409, 12373, 12336, 12300, 12264, 12228,
  12193, 12157, 12122, 12087, 12053, 12018, 11984, 11950, 11916, 11882, 11848,
  11815, 11782, 11749, 11716, 11683, 11651, 11619, 11586, 11555, 11523, 11491,
  11460, 11429, 11398, 11367, 11336, 11305, 11275, 11245, 11215, 11185, 11155,
  11125, 11096, 11067, 11038, 11009, 10980, 10951, 10923, 10894, 10866, 10838,
  10810, 10782, 10755, 10727, 10700, 10673, 10645, 10618, 10592, 10565, 10538,
  10512, 10486, 10460, 10434, 10408, 10382, 10356, 10331, 10305, 10280, 10255,
  10230, 10205, 10180, 10156, 10131, 10107, 10082, 10058, 10034, 10010, 9986,
  9963,  9939,  9916,  9892,  9869,  9846,  9823,  9800,  9777,  9754,  9732,
  9709,  9687,  9664,  9642,  9620,  9598,  9576,  9554,  9533,  9511,  9489,
  9468,  9447,  9425,  9404,  9383,  9362,  9341,  9321,  9300,  9279,  9259,
  9239,  9218,  9198,  9178,  9158,  9138,  9118,  9098,  9079,  9059,  9039,
  9020,  9001,  8981,  8962,  8943,  8924,  8905,  8886,  8867,  8849,  8830,
  8812,  8793,  8775,  8756,  8738,  8720,  8702,  8684,  8666,  8648,  8630,
  8613,  8595,  8577,  8560,  8542,  8525,  8508,  8490,  8473,  8456,  8439,
  8422,  8405,  8389,  8372,  8355,  8339,  8322,  8306,  8289,  8273,  8257,
  8240,  8224,  8208,  8192
};

resolve_divisor( int64 d, int32pointer out ) {
    // NOTE:
    // * When called from setup_shear(), d is always in the range
    //   [2^16 - 2^13, 2^16 + 2^13]
    // * When called from warp_estimation(), d is always at least 1968
    // In particular, d is always positive and >= 2^(DIV_LUT_BITS+1).
    // Thus some of the branches in this function are impossible,
    // and the 'Abs' calls are redundant (but left in to match the spec)
    int64 e
    uint_0_256 f
    n = FloorLog2( Abs(d) )
    e = Abs( d ) - ( convert_to_int64(1) << n )
    if ( COVERCLASS(1,IMPOSSIBLE,n > DIV_LUT_BITS) ) f = Round2( e, n - DIV_LUT_BITS )
    else f = e << ( DIV_LUT_BITS - n )
    out[ 0 ] = (n + DIV_LUT_PREC_BITS)
    COVERCROSS(VALUE_DIV_LUT, f)
    if ( COVERCLASS(IMPOSSIBLE,1,d < 0) ) out[ 1 ] = -Div_Lut[ f ]
    else out[ 1 ] = Div_Lut[ f ]
}

ls_product( xa, xb ) {
    ret = ( (xa * xb) >> 2) + (xa + xb)
    return ret
}

warp_estimation( ) {
    int64 A[ 2 ][ 2 ]
    int64 Bx[ 2 ]
    int64 By[ 2 ]
    int rdout[ 2 ]
    int64 det

#if VALIDATE_SPEC_WARP
    validate(55000)
#endif

    for (i = 0; i < 2; i++) {
        for (j = 0; j < 2; j++) {
            A[i][j] = 0
        }
        Bx[i] = 0
        By[i] = 0
    }
    w4 = Num_4x4_Blocks_Wide[MiSize]
    h4 = Num_4x4_Blocks_High[MiSize]
    midY = MiRow * 4 + h4 * 2 - 1
    midX = MiCol * 4 + w4 * 2 - 1
    suy = midY * 8
    sux = midX * 8
    duy = suy + Mv[0][0]
    dux = sux + Mv[0][1]
    for (i = 0; i < NumSamples; i++) {
        sy = CandList[i][0] - suy
        sx = CandList[i][1] - sux
        dy = CandList[i][2] - duy
        dx = CandList[i][3] - dux
        if (Abs(sx - dx) < LS_MV_MAX && Abs(sy - dy) < LS_MV_MAX) {
            A[0][0] += ls_product(sx, sx) + 8
            A[0][1] += ls_product(sx, sy) + 4
            A[1][1] += ls_product(sy, sy) + 8
            Bx[0] += ls_product(sx, dx) + 8
            Bx[1] += ls_product(sy, dx) + 4
            By[0] += ls_product(sx, dy) + 4
            By[1] += ls_product(sy, dy) + 8
        }
    }
    det = A[0][0] * A[1][1] - A[0][1] * A[0][1]
    LocalValid = ( det == 0 ) ? 0 : 1
#if VALIDATE_SPEC_WARP
    validate(55001)
    validate(det)
#endif
    if ( det == 0 ) {
        return 0
    }
    resolve_divisor( det, rdout )
    divShift = rdout[ 0 ]
    divFactor = rdout[ 1 ]
    divShift -= WARPEDMODEL_PREC_BITS
    // Note: At this point, det is always at least 15, so rdout[0]
    // is always at least 17. Thus divShift is always at least 1.
    if (COVERCLASS(IMPOSSIBLE,1,divShift < 0)) {
        divFactor = divFactor << (-divShift)
        divShift = 0
    }
    LocalWarpParams[2] = diag(     A[1][1] * Bx[0] - A[0][1] * Bx[1], divFactor, divShift)
    LocalWarpParams[3] = nondiag( -A[0][1] * Bx[0] + A[0][0] * Bx[1], divFactor, divShift)
    LocalWarpParams[4] = nondiag(  A[1][1] * By[0] - A[0][1] * By[1], divFactor, divShift)
    LocalWarpParams[5] = diag(    -A[0][1] * By[0] + A[0][0] * By[1], divFactor, divShift)

    mvx = Mv[ 0 ][ 1 ]
    mvy = Mv[ 0 ][ 0 ]
    vx = mvx * (1 << (WARPEDMODEL_PREC_BITS - 3)) -
         (midX * (LocalWarpParams[2] - (1 << WARPEDMODEL_PREC_BITS)) + midY * LocalWarpParams[3])
    vy = mvy * (1 << (WARPEDMODEL_PREC_BITS - 3)) -
         (midX * LocalWarpParams[4] + midY * (LocalWarpParams[5] - (1 << WARPEDMODEL_PREC_BITS)))
    LocalWarpParams[0] = Clip3(-WARPEDMODEL_TRANS_CLAMP, WARPEDMODEL_TRANS_CLAMP - 1, vx)
    LocalWarpParams[1] = Clip3(-WARPEDMODEL_TRANS_CLAMP, WARPEDMODEL_TRANS_CLAMP - 1, vy)
    return 1
}

nondiag(int64 v, int64 divFactor, divShift) {
    ret = Clip3(-WARPEDMODEL_NONDIAGAFFINE_CLAMP + 1,
                 WARPEDMODEL_NONDIAGAFFINE_CLAMP - 1,
                 Round2Signed(v * divFactor, divShift))
    return ret
}

diag(int64 v, int64 divFactor, divShift) {
    ret = Clip3((1 << WARPEDMODEL_PREC_BITS) - WARPEDMODEL_NONDIAGAFFINE_CLAMP + 1,
                (1 << WARPEDMODEL_PREC_BITS) + WARPEDMODEL_NONDIAGAFFINE_CLAMP - 1,
                Round2Signed(v * divFactor, divShift))
    return ret
}

overlapped_motion_compensation( plane, x, y, w, h ) {
    if ( plane == 0 ) {
        subX = 0
        subY = 0
    } else {
        subX = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
        subY = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
    }

    if ( AvailU ) {
        if ( COVERCLASS(1, (PROFILE0 || PROFILE2), get_plane_residual_size( MiSize, plane ) >= BLOCK_8X8) ) {
            ppass = 0
            w4 = Num_4x4_Blocks_Wide[ MiSize ]
            x4 = MiCol
            y4 = MiRow
            nCount = 0
            nLimit = Min(4, Mi_Width_Log2[ MiSize ])
            while ( nCount < nLimit && x4 < Min( MiCols, MiCol + w4 ) ) {
                candRow = MiRow - 1
                candCol = x4 | 1
                candSz = MiSizes[ candRow ][ candCol ]
                step4 = Clip3( 2, 16, Num_4x4_Blocks_Wide[ candSz ] )
                if ( RefFrames[ candRow ][ candCol ][ 0 ] > INTRA_FRAME ) {
                    nCount += 1
                    predW = Min( w, ( step4 * MI_SIZE ) >> subX )
                    predH = Min( h >> 1, 32 >> subY )
#if VALIDATE_SPEC_INTER2
                    if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
                        validate(81003)
                    }
#endif
                    predict_overlap( predH, predW, predH, candRow, candCol, x4, y4, subX, subY, plane, ppass )
                }
                x4 += step4
            }
        }
    }
    if ( AvailL ) {
        ppass = 1
        h4 = Num_4x4_Blocks_High[ MiSize ]
        x4 = MiCol
        y4 = MiRow
        nCount = 0
        nLimit = Min(4, Mi_Height_Log2[ MiSize ])
        while ( nCount < nLimit && y4 < Min( MiRows, MiRow + h4 ) ) {
            candCol = MiCol - 1
            candRow = y4 | 1
            candSz = MiSizes[ candRow ][ candCol ]
            step4 = Clip3( 2, 16, Num_4x4_Blocks_High[ candSz ] )
            if ( RefFrames[ candRow ][ candCol ][ 0 ] > INTRA_FRAME ) {
                nCount += 1
                predW = Min( w >> 1, 32 >> subX )
                predH = Min( h, ( step4 * MI_SIZE ) >> subY )
#if VALIDATE_SPEC_INTER2
                if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
                    validate(81004)
                }
#endif
                predict_overlap( predW, predW, predH, candRow, candCol, x4, y4, subX, subY, plane, ppass )
            }
            y4 += step4
        }
    }
}

predict_overlap( length, predW, predH, candRow, candCol, x4, y4, subX, subY, plane, ppass ) {
    int mask[ 32 ]
    int mv[ 2 ]
    int mvsout[ 4 ]

    get_obmc_mask( length, mask )
    mv[ 0 ] = Mvs[ candRow ][ candCol ][ 0 ][ 0 ]
    mv[ 1 ] = Mvs[ candRow ][ candCol ][ 0 ][ 1 ]
    refIdx = ref_frame_idx[ RefFrames[ candRow ][ candCol ][ 0 ] - LAST_FRAME ]
    predX = (x4 * 4) >> subX
    predY = (y4 * 4) >> subY
    motion_vector_scaling( plane, refIdx, predX, predY, mv, mvsout )
    startX = mvsout[ 0 ]
    startY = mvsout[ 1 ]
    stepX  = mvsout[ 2 ]
    stepY  = mvsout[ 3 ]
    ValidationInterIsObmc = 1
    block_inter_prediction( plane, 0, refIdx, startX, startY, stepX, stepY, predW, predH, candRow, candCol )
    overlap_blending( plane, predX, predY, predW, predH, ppass, mask )
}

int Obmc_Mask_2[2] = { 45, 64 };

int Obmc_Mask_4[4] = { 39, 50, 59, 64 };

int Obmc_Mask_8[8] = { 36, 42, 48, 53, 57, 61, 64, 64 };

int Obmc_Mask_16[16] = { 34, 37, 40, 43, 46, 49, 52, 54,
                     56, 58, 60, 61, 64, 64, 64, 64 };

int Obmc_Mask_32[32] = { 33, 35, 36, 38, 40, 41, 43, 44,
                     45, 47, 48, 50, 51, 52, 53, 55,
                     56, 57, 58, 59, 60, 60, 61, 62,
                     64, 64, 64, 64, 64, 64, 64, 64 };

get_obmc_mask( length, int32pointer mask ) {
    if (COVERCLASS((PROFILE0 || PROFILE2), 1, length == 2)) {
        for (i = 0; i < length; i++) {
#if COVER
            uint_0_1 I_2
            I_2 = i
            COVERCROSS(VALUE_OBMC_MASK_2, I_2)
#endif // COVER
            mask[ i ] = Obmc_Mask_2[ i ]
        }
    } else if (length == 4) {
        for (i = 0; i < length; i++) {
#if COVER
            uint_0_3 I_4
            I_4 = i
            COVERCROSS(VALUE_OBMC_MASK_4, I_4)
#endif // COVER
            mask[ i ] = Obmc_Mask_4[ i ]
        }
    } else if (length == 8) {
        for (i = 0; i < length; i++) {
#if COVER
            uint_0_7 I_8
            I_8 = i
            COVERCROSS(VALUE_OBMC_MASK_8, I_8)
#endif // COVER
            mask[ i ] = Obmc_Mask_8[ i ]
        }
    } else if (length == 16) {
        for (i = 0; i < length; i++) {
#if COVER
            uint_0_15 I_16
            I_16 = i
            COVERCROSS(VALUE_OBMC_MASK_16, I_16)
#endif // COVER
            mask[ i ] = Obmc_Mask_16[ i ]
        }
    } else {
        for (i = 0; i < length; i++) {
#if COVER
            uint_0_31 I_32
            I_32 = i
            COVERCROSS(VALUE_OBMC_MASK_32, I_32)
#endif // COVER
            mask[ i ] = Obmc_Mask_32[ i ]
        }
    }
}

overlap_blending( plane, predX, predY, predW, predH, ppass, int32pointer mask ) {
    for (i = 0; i < predH; i++) {
        for (j = 0; j < predW; j++) {
            if ( ppass == 0 ) m = mask[ i ]
            else m = mask[ j ]
#if VALIDATE_SPEC_INTER2
            if (plane == VALIDATE_SPEC_INTER2_PLANE) {
                validate(81005)
                validate(m)
                validate(predX + j)
                validate(predY + i)
                validate(CurrFrame[ plane ][ predY + i ][ predX + j ])
                validate(Clip1(preds[ 0 ][ i ][ j ]))
            }
#endif
            CurrFrame[ plane ][ predY + i ][ predX + j ] = Round2( m * CurrFrame[ plane ][ predY + i ][ predX + j ] + (64 - m) * Clip1( preds[ 0 ][ i ][ j ] ), 6) // E-51 [RNG-Blend4]
#if VALIDATE_SPEC_INTER2
            if (plane == VALIDATE_SPEC_INTER2_PLANE) {
                validate(CurrFrame[ plane ][ predY + i ][ predX + j ])
            }
#endif
        }
    }
}

wedge_mask( w, h ) {
    if ( !WedgeMaskTableInitialised ) {
        initialise_wedge_mask_table( )
        WedgeMaskTableInitialised = 1
    }
    for (i = 0; i < h; i++) {
        for (j = 0; j < w; j++) {
            Mask[ i ][ j ] = WedgeMasks[ MiSize ][ wedge_sign ][ wedge_index ][ i ][ j ]
        }
    }
}

int Wedge_Master_Oblique_Odd[MASK_MASTER_SIZE] = {
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  6,  18,
  37, 53, 60, 63, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
};

int Wedge_Master_Oblique_Even[MASK_MASTER_SIZE] = {
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  4,  11, 27,
  46, 58, 62, 63, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
};

int Wedge_Master_Vertical[MASK_MASTER_SIZE] = {
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  2,  7,  21,
  43, 57, 62, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
};

initialise_wedge_mask_table( ) {
    w = MASK_MASTER_SIZE
    h = MASK_MASTER_SIZE
    for (j = 0; j < w; j ++) {
        shift = MASK_MASTER_SIZE / 4
        for (i = 0; i < h; i += 2) {
#if COVER
            uint_0_63 mask_master
            mask_master = Clip3( 0, MASK_MASTER_SIZE - 1, j - shift )
            COVERCROSS(VALUE_WEDGE_MASTER_OBLIQUE_EVEN, mask_master)
            COVERCROSS(VALUE_WEDGE_MASTER_OBLIQUE_ODD, mask_master)
            mask_master = j
            COVERCROSS(VALUE_WEDGE_MASTER_OBLIQUE_VERTICAL, mask_master)
#endif // COVER
            MasterMask[ WEDGE_OBLIQUE63 ][ i ][ j ] = Wedge_Master_Oblique_Even[ Clip3( 0, MASK_MASTER_SIZE - 1, j - shift ) ]
            shift -= 1
            MasterMask[ WEDGE_OBLIQUE63 ][ i + 1][ j ] = Wedge_Master_Oblique_Odd[ Clip3( 0, MASK_MASTER_SIZE - 1, j - shift ) ]
            MasterMask[ WEDGE_VERTICAL ][ i ][ j ] = Wedge_Master_Vertical[ j ]
            MasterMask[ WEDGE_VERTICAL ][ i + 1 ][ j ] = Wedge_Master_Vertical[ j ]
        }
    }
    for (i = 0; i < h; i++) {
        for (j = 0; j < w; j++) {
          msk = MasterMask[WEDGE_OBLIQUE63][ i ][ j ]
          MasterMask[WEDGE_OBLIQUE27][ j ][ i ] = msk
          MasterMask[WEDGE_OBLIQUE117][ i ][ w - 1 - j ] = 64 - msk
          MasterMask[WEDGE_OBLIQUE153][ w - 1 - j ][ i ] = 64 - msk
          MasterMask[WEDGE_HORIZONTAL][ j ][ i ] = MasterMask[WEDGE_VERTICAL][ i ][ j ]
        }
    }
    uint_0_21 bsize
    for (bsize = BLOCK_8X8; bsize < BLOCK_SIZES; bsize++) {
        if ( Wedge_Bits[ bsize ] > 0 ) {
            w = Block_Width[ bsize ]
            h = Block_Height[ bsize ]
            for (wedge = 0; wedge < WEDGE_TYPES; wedge++) {
                dir = get_wedge_direction(bsize, wedge)
                xoff = MASK_MASTER_SIZE / 2 - ((get_wedge_xoff(bsize, wedge) * w) >> 3)
                yoff = MASK_MASTER_SIZE / 2 - ((get_wedge_yoff(bsize, wedge) * h) >> 3)
                sum = 0
                for (i = 0; i < w; i++)
                  sum += MasterMask[dir][ yoff ][ xoff+i ]
                for (i = 1; i < h; i++)
                  sum += MasterMask[dir][ yoff+i ][ xoff ]
                avg = (sum + (w + h - 1) / 2) / (w + h - 1)
                flipSign = (avg < 32)
                for (i = 0; i < h; i++) {
                    for (j = 0; j < w; j++) {
                      WedgeMasks[bsize][flipSign][wedge][ i ][ j ] = MasterMask[ dir ][ yoff+i ][ xoff+j ]
                      WedgeMasks[bsize][!flipSign][wedge][ i ][ j ] = 64 - MasterMask[ dir ][ yoff+i ][ xoff+j ]
                    }
                }
            }
        }
    }
}

block_shape(bsize) {
  w4 = Num_4x4_Blocks_Wide[bsize]
  h4 = Num_4x4_Blocks_High[bsize]
  if (h4 > w4)
      return 0
  else if (h4 < w4)
      return 1
  else
      return 2
}

int Wedge_Codebook[3][16][3] = {
    {
        { WEDGE_OBLIQUE27, 4, 4 },  { WEDGE_OBLIQUE63, 4, 4 },
        { WEDGE_OBLIQUE117, 4, 4 }, { WEDGE_OBLIQUE153, 4, 4 },
        { WEDGE_HORIZONTAL, 4, 2 }, { WEDGE_HORIZONTAL, 4, 4 },
        { WEDGE_HORIZONTAL, 4, 6 }, { WEDGE_VERTICAL, 4, 4 },
        { WEDGE_OBLIQUE27, 4, 2 },  { WEDGE_OBLIQUE27, 4, 6 },
        { WEDGE_OBLIQUE153, 4, 2 }, { WEDGE_OBLIQUE153, 4, 6 },
        { WEDGE_OBLIQUE63, 2, 4 },  { WEDGE_OBLIQUE63, 6, 4 },
        { WEDGE_OBLIQUE117, 2, 4 }, { WEDGE_OBLIQUE117, 6, 4 },
    },
    {
        { WEDGE_OBLIQUE27, 4, 4 },  { WEDGE_OBLIQUE63, 4, 4 },
        { WEDGE_OBLIQUE117, 4, 4 }, { WEDGE_OBLIQUE153, 4, 4 },
        { WEDGE_VERTICAL, 2, 4 },   { WEDGE_VERTICAL, 4, 4 },
        { WEDGE_VERTICAL, 6, 4 },   { WEDGE_HORIZONTAL, 4, 4 },
        { WEDGE_OBLIQUE27, 4, 2 },  { WEDGE_OBLIQUE27, 4, 6 },
        { WEDGE_OBLIQUE153, 4, 2 }, { WEDGE_OBLIQUE153, 4, 6 },
        { WEDGE_OBLIQUE63, 2, 4 },  { WEDGE_OBLIQUE63, 6, 4 },
        { WEDGE_OBLIQUE117, 2, 4 }, { WEDGE_OBLIQUE117, 6, 4 },
    },
    {
        { WEDGE_OBLIQUE27, 4, 4 },  { WEDGE_OBLIQUE63, 4, 4 },
        { WEDGE_OBLIQUE117, 4, 4 }, { WEDGE_OBLIQUE153, 4, 4 },
        { WEDGE_HORIZONTAL, 4, 2 }, { WEDGE_HORIZONTAL, 4, 6 },
        { WEDGE_VERTICAL, 2, 4 },   { WEDGE_VERTICAL, 6, 4 },
        { WEDGE_OBLIQUE27, 4, 2 },  { WEDGE_OBLIQUE27, 4, 6 },
        { WEDGE_OBLIQUE153, 4, 2 }, { WEDGE_OBLIQUE153, 4, 6 },
        { WEDGE_OBLIQUE63, 2, 4 },  { WEDGE_OBLIQUE63, 6, 4 },
        { WEDGE_OBLIQUE117, 2, 4 }, { WEDGE_OBLIQUE117, 6, 4 },
    }
};

get_wedge_direction(bsize, index) {
#if COVER
  uint_0_2 shape
  shape = block_shape(bsize)
  uint_0_15 Index
  Index = index
  uint_0_2 pos
  pos = 0
  COVERCROSS(CROSS_WEDGE_CODEBOOK, shape, Index, pos)
#endif // COVER
  ret = Wedge_Codebook[block_shape(bsize)][index][0]
  return ret
}

get_wedge_xoff(bsize, index) {
#if COVER
  uint_0_2 shape
  shape = block_shape(bsize)
  uint_0_15 Index
  Index = index
  uint_0_2 pos
  pos = 1
  COVERCROSS(CROSS_WEDGE_CODEBOOK, shape, Index, pos)
#endif // COVER
  ret = Wedge_Codebook[block_shape(bsize)][index][1]
  return ret
}

get_wedge_yoff(bsize, index) {
#if COVER
  uint_0_2 shape
  shape = block_shape(bsize)
  uint_0_15 Index
  Index = index
  uint_0_2 pos
  pos = 2
  COVERCROSS(CROSS_WEDGE_CODEBOOK, shape, Index, pos)
#endif // COVER
  ret = Wedge_Codebook[block_shape(bsize)][index][2]
  return ret
}

difference_weight_mask( w, h ) {
    for (i = 0; i < h; i++) {
        for (j = 0; j < w; j++) {
            diff = Abs(preds[ 0 ][ i ][ j ] - preds[ 1 ][ i ][ j ]) // E-45 [RNG-DiffWtd1]
            diff = Round2(diff, (BitDepth - 8) + InterPostRound) // E-46 [RNG-DiffWtd2]
            m = Clip3(0, 64, 38 + diff / 16) // E-47 [RNG-DiffWtd3]
            if (mask_type)
                Mask[ i ][ j ] = 64 - m
            else
                Mask[ i ][ j ] = m
        }
    }
}

int Ii_Weights_1d[MAX_SB_SIZE] = {
  60, 58, 56, 54, 52, 50, 48, 47, 45, 44, 42, 41, 39, 38, 37, 35, 34, 33, 32,
  31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 22, 21, 20, 19, 19, 18, 18, 17, 16,
  16, 15, 15, 14, 14, 13, 13, 12, 12, 12, 11, 11, 10, 10, 10,  9,  9,  9,  8,
  8,  8,  8,  7,  7,  7,  7,  6,  6,  6,  6,  6,  5,  5,  5,  5,  5,  4,  4,
  4,  4,  4,  4,  4,  4,  3,  3,  3,  3,  3,  3,  3,  3,  3,  2,  2,  2,  2,
  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  1,  1,  1,  1,  1,  1,  1,  1,
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1
};

intra_mode_variant_mask( plane, x, y, w, h ) {
#if COVER
    uint_0_127 SbSize
#endif // COVER
    sizeScale = MAX_SB_SIZE / Max( h, w )
    for (i = 0; i < h; i++) {
        for (j = 0; j < w; j++) {
            if (interintra_mode == II_V_PRED) {
#if COVER
                SbSize = i * sizeScale
                COVERCROSS(VALUE_II_WEIGHTS_1D, SbSize)
#endif // COVER
                Mask[ i ][ j ] = Ii_Weights_1d[ i * sizeScale ]
            } else if (interintra_mode == II_H_PRED) {
#if COVER
                SbSize = j * sizeScale
                COVERCROSS(VALUE_II_WEIGHTS_1D, SbSize)
#endif // COVER
                Mask[ i ][ j ] = Ii_Weights_1d[ j * sizeScale ]
            } else if (interintra_mode == II_SMOOTH_PRED) {
#if COVER
                SbSize = Min(i, j) * sizeScale
                COVERCROSS(VALUE_II_WEIGHTS_1D, SbSize)
#endif // COVER
                Mask[ i ][ j ] = Ii_Weights_1d[ Min(i, j) * sizeScale ]
            } else {
                Mask[ i ][ j ] = 32
            }
        }
    }
}

mask_blend( plane, dstX, dstY, w, h ) {
    if ( plane == 0 ) {
        subX = 0
        subY = 0
    } else {
        subX = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
        subY = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
    }
    for (y = 0; y < h; y++) {
        for (x = 0; x < w; x++) {
            // These coverclasses are because 4:4:0 is never used currently
            // If it ever is, these will no longer be valid.
            if ( COVERCLASS(1, (PROFILE0 || PROFILE2),
                  (COVERCLASS(1, (PROFILE0 || PROFILE2), !COVERCLASS((PROFILE0 || PROFILE2), 1, subX)) && COVERCLASS(1,IMPOSSIBLE,!COVERCLASS(IMPOSSIBLE,1,subY))) ||
                   COVERCLASS((PROFILE0 || PROFILE2), (interintra && !wedge_interintra)) ) ) {
                m = Mask[ y ][ x ]
            } else {
                if (COVERCLASS(PROFILE2, (PROFILE0 || PROFILE2), subX && COVERCLASS(PROFILE2, (PROFILE0 || PROFILE2), !subY))) {
                    m = Round2( Mask[ y ][ 2*x ] + Mask[ y ][ 2*x+1 ], 1 )
                }/* else if (!subX && COVERCLASS(subY) {
                    // This is the 4:4:0 case
                    m = Round2( Mask[ 2*y ][ x ] + Mask[ 2*y+1 ][ x ], 1 )
                }*/ else {
                    m = Round2( Mask[ 2*y ][ 2*x ] + Mask[ 2*y ][ 2*x+1 ] +
                                Mask[ 2*y+1 ][ 2*x ] + Mask[ 2*y+1 ][ 2*x+1 ], 2 )
                }
            }
            if (interintra) {
                pred0 = Clip1(Round2( preds[ 0 ][ y ][ x ], InterPostRound ))
                pred1 = CurrFrame[plane][y+dstY][x+dstX]
                CurrFrame[plane][y+dstY][x+dstX] = Round2( m * pred1 + (64 - m) * pred0, 6 ) // E-52 [RNG-Blend5]
            } else {
                pred0 = preds[ 0 ][ y ][ x ]
                pred1 = preds[ 1 ][ y ][ x ]
                CurrFrame[plane][y+dstY][x+dstX] = Clip1( Round2( m * pred0 + (64 - m) * pred1, 6 + InterPostRound ) ) // E-53 [RNG-Blend6]
            }
        }
    }
}
