/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_LOOPFILTER 1
#else
#define VALIDATE_SPEC_LOOPFILTER 0
#endif
#define VALIDATE_SPEC_LOOPFILTER_INTERNALS 0
#define VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS 0

#define OUTPUT_LOOP_FILTER 0

loop_filter( ) {
#if VALIDATE_SPEC_LOOPFILTER
    for ( plane = 0; plane < NumPlanes; plane++ ) {
        subX = ( plane == 0 ) ? 0 : subsampling_x
        subY = ( plane == 0 ) ? 0 : subsampling_y
        for ( i = 0; i < FrameHeight >> subY; i++) {
            for ( j = 0; j < FrameWidth >> subX; j++ ) {
                validate(180000)
                validate(plane)
                validate(j)
                validate(i)
                validate( CurrFrame[ plane ][ i ][ j ] )
            }
        }
    }
#endif
    for ( plane = 0; plane < NumPlanes; plane++ ) {
        if ( plane == 0 ||
             loop_filter_level[ 1 + plane ] ) {
            for ( ppass = 0; ppass < 2; ppass++ ) {
                rowStep = ( plane == 0 ) ? 1 : ( 1 << COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) )
                colStep = ( plane == 0 ) ? 1 : ( 1 << COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) )
#if VALIDATE_SPEC_LOOPFILTER_INTERNALS
                for (blkY=0; blkY < MiRows; blkY+=32)
                    for (blkX=0; blkX < MiCols; blkX+=32)
                        for ( row = blkY; row < blkY+32; row += rowStep )
                            for ( col = blkX; col < blkX+32; col += colStep )
#else
                for ( row = 0; row < MiRows; row += rowStep )
                    for ( col = 0; col < MiCols; col += colStep )
#endif
                                edge_loop_filter( plane, ppass, row, col )
            }
        }
    }
#if VALIDATE_SPEC_LOOPFILTER
    for ( plane = 0; plane < NumPlanes; plane++ ) {
        subX = ( plane == 0 ) ? 0 : subsampling_x
        subY = ( plane == 0 ) ? 0 : subsampling_y
        for ( i = 0; i < FrameHeight >> subY; i++) {
            for ( j = 0; j < FrameWidth >> subX; j++ ) {
                validate(180001)
                validate(plane)
                validate(j)
                validate(i)
                validate( CurrFrame[ plane ][ i ][ j ] )
            }
        }
    }
#endif
}

edge_loop_filter( plane, ppass, row, col ) {
    int afsout[ 4 ]
#if VALIDATE_SPEC_LOOPFILTER_INTERNALS
    int afsout2[ 4 ]
    validate(180002)
    validate(plane)
    validate(row)
    validate(col)
    validate(ppass)
#endif
    if ( plane == 0 ) {
        subX = 0
        subY = 0
    } else {
        subX = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
        subY = COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
    }
    if ( ppass == 0 ) {
        dx = 1
        dy = 0
    } else {
        dx = 0
        dy = 1
    }

    x = col * MI_SIZE
    y = row * MI_SIZE
    col |= subX
    row |= subY

    if ( x >= FrameWidth ) onScreen = 0
    else if ( y >= FrameHeight ) onScreen = 0
    else if ( ppass == 0 && x == 0 ) onScreen = 0
    else if ( ppass == 1 && y == 0 ) onScreen = 0
    else onScreen = 1

    if ( onScreen == 0 ) { return 0 }

    xP = x >> subX
    yP = y >> subY

    prevRow = row - ( dy << subY )
    prevCol = col - ( dx << subX )
    MiSize = MiSizes[ row ][ col ]
    uint_0_18 txSz
    txSz = LoopfilterTxSizes[ plane ][ row >> subY ][ col >> subX ]
    planeSize = get_plane_residual_size( MiSize, plane )
    skip = Skips[ row ][ col ]
    isIntra = RefFrames[ row ][ col ][ 0 ] <= INTRA_FRAME
    prevTxSz = LoopfilterTxSizes[ plane ][ prevRow >> subY ][ prevCol >> subX ]

    if ( ppass == 0 && ( xP % Block_Width[ planeSize ] ) == 0 ) isBlockEdge = 1
    else if ( ppass == 1 && ( yP % Block_Height[ planeSize ] ) == 0 ) isBlockEdge = 1
    else isBlockEdge = 0

#if COVER
    if ( ppass == 0 ) {
      COVERCROSS(VALUE_TX_WIDTH, txSz)
    } else {
      ASSERT( ppass == 1, "Unexpected value of ppass")
      COVERCROSS(VALUE_TX_HEIGHT, txSz)
    }
#endif // COVER
    if ( ppass == 0 && ( xP % Tx_Width[ txSz ] ) == 0 ) isTxEdge = 1
    else if ( ppass == 1 && ( yP % Tx_Height[ txSz ] ) == 0 ) isTxEdge = 1
    else isTxEdge = 0

    if ( isTxEdge != 0 && isBlockEdge == 0 ) {
        ASSERT( skip == Skips[ prevRow ][ prevCol ], "skip should be equal to prevSkip within a block")
        ASSERT( isIntra == (RefFrames[ prevRow ][ prevCol ][ 0 ] <= INTRA_FRAME), "isIntra should be equal to prevIsIntra within a block")
    }
    if ( isTxEdge == 0 ) applyFilter = 0
    else if ( isBlockEdge == 1 || skip == 0 || isIntra == 1 ) applyFilter = 1
    else applyFilter = 0

    filterSize = filter_size( txSz, prevTxSz, ppass, subX, subY, plane )
    adaptive_filter_strength( row, col, plane, ppass, afsout )
#if VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS
    //Call this again even though we might not use the results, because av1src does.
    adaptive_filter_strength( prevRow, prevCol, plane, ppass, afsout2 )
    if ( afsout[ 0 ] == 0 )
        for (i = 0; i < 4; i++)
            afsout[ i ] = afsout2[ i ]
#else
    if ( afsout[ 0 ] == 0 ) adaptive_filter_strength( prevRow, prevCol, plane, ppass, afsout )
#endif
    lvl = afsout[ 0 ]
    limit = afsout[ 1 ]
    blimit = afsout[ 2 ]
    thresh = afsout[ 3 ]

#if VALIDATE_SPEC_LOOPFILTER_INTERNALS
    if ( applyFilter == 1 && lvl > 0 ) {
        validate(180003)
        validate(lvl)
        validate(limit)
        validate(blimit)
        validate(thresh)
    }
#endif
    for (i = 0; i < MI_SIZE; i++) {
        if ( applyFilter == 1 && lvl > 0 )
            sample_filtering( xP + dy * i, yP + dx * i, plane, limit, blimit, thresh, dx, dy, filterSize )
    }
}

filter_size( txSz, uint_0_18 prevTxSz, ppass, subX, subY, plane ) {
    COVERCROSS(VALUE_TX_WIDTH, prevTxSz)
    COVERCROSS(VALUE_TX_HEIGHT, prevTxSz)
    if ( ppass == 0 )
        baseSize = Min( Tx_Width[ prevTxSz ], Tx_Width[ txSz ] )
    else
        baseSize = Min( Tx_Height[ prevTxSz ], Tx_Height[ txSz ] )
    return ( plane == 0 ) ? Min( 16, baseSize ) : Min( 8, baseSize )
}

adaptive_filter_strength( row, col, plane, ppass, int32pointer out ) {
    segment = SegmentIds[ row ][ col ]
    ref = RefFrames[ row ][ col ][ 0 ]
    mode = YModes[ row ][ col ]
    if ( mode >= NEARESTMV && mode != GLOBALMV && mode != GLOBAL_GLOBALMV ) modeType = 1
    else modeType = 0
    if ( delta_lf_multi == 0 ) deltaLF = DeltaLFs[ row ][ col ][ 0 ]
    else deltaLF = DeltaLFs[ row ][ col ][ ( plane == 0 ) ? ppass : ( plane + 1 ) ]
    uint6 lvl
    lvl = adaptive_filter_strength_selection( segment, ref, modeType, deltaLF, plane, ppass )
    COVERCROSS(CROSS_LF_STRENGTH, loop_filter_sharpness, lvl)
    if ( loop_filter_sharpness > 4 ) shift = 2
    else if ( loop_filter_sharpness > 0 ) shift = 1
    else shift = 0
    if ( loop_filter_sharpness > 0 ) limit = Clip3( 1, 9 - loop_filter_sharpness, lvl >> shift )
    else limit = Max( 1, lvl >> shift )
    blimit = 2 * (lvl + 2) + limit
    thresh = lvl >> 4
    out[ 0 ] = lvl
    out[ 1 ] = limit
    out[ 2 ] = blimit
    out[ 3 ] = thresh
}

adaptive_filter_strength_selection( segment, ref, modeType, deltaLF, plane, ppass ) {
    i = ( plane == 0 ) ? ppass : ( plane + 1 )
    baseFilterLevel = Clip3( 0, MAX_LOOP_FILTER, deltaLF + loop_filter_level[ i ] )
#if VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS
      validate(180004)
    if (delta_lf_present) {
      validate(baseFilterLevel)
    }
#endif
    lvlSeg = baseFilterLevel
    feature = SEG_LVL_ALT_LF_Y_V + i
    if ( seg_feature_active_idx( segment, feature ) == 1 ) {
        lvlSeg = FeatureData[ segment ][ feature ] + lvlSeg
        lvlSeg = Clip3( 0, MAX_LOOP_FILTER, lvlSeg )
    }
#if VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS
    if (delta_lf_present) {
      validate(lvlSeg)
    }
#endif
    if ( loop_filter_delta_enabled == 1 ) {
        nShift = lvlSeg >> 5
        if ( ref == INTRA_FRAME ) lvlSeg += ( loop_filter_ref_deltas[ INTRA_FRAME ] << nShift )
        else lvlSeg += ( loop_filter_ref_deltas[ ref ] << nShift ) + ( loop_filter_mode_deltas[ modeType ] << nShift )
        lvlSeg = Clip3(0, MAX_LOOP_FILTER, lvlSeg)
    }
#if VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS
    validate(lvlSeg)
#endif
    return lvlSeg
}

sample_filtering( x, y, plane, limit, blimit, thresh, dx, dy, filterSize ) {
    int fm[ 4 ]
    for ( i = 0; i < 4; i++)
        fm[ i ] = 0
    filter_mask( x, y, plane, limit, blimit, thresh, dx, dy, filterSize, fm )
    hevMask = fm[ 0 ]
    filterMask = fm[ 1 ]
    flatMask = fm[ 2 ]
    flatMask2 = fm [ 3 ]
    if ( filterMask != 0 ) {
#if OUTPUT_LOOP_FILTER
        :C printf("loop_filter: %d %d %d %d %d\n", plane, x, y, dx, dy);
#endif
        if ( filterSize == 4 || flatMask == 0 ) narrow_filter( hevMask, x, y, plane, limit, blimit, thresh, dx, dy )
        else if ( filterSize == 8 || flatMask2 == 0 ) wide_filter( x, y, plane, dx, dy, 3 )
        else wide_filter( x, y, plane, dx, dy, 4 )
    }
}

filter_mask( x, y, plane, limit, blimit, thresh, dx, dy, filterSize, int32pointer fm ) {
    pq4Defined = 0 // Extra code to check that q4/q5/q6/p4/p5/p6 are only used if defined
    p4 = 0
    p5 = 0
    p6 = 0
    q4 = 0
    q5 = 0
    q6 = 0
    q0 = CurrFrame[ plane ][ y ][ x ]
    q1 = CurrFrame[ plane ][ y + dy     ][ x + dx     ]
    q2 = CurrFrame[ plane ][ y + dy * 2 ][ x + dx * 2 ]
    q3 = CurrFrame[ plane ][ y + dy * 3 ][ x + dx * 3 ]
    if ( filterSize == 16 ) {
        pq4Defined = 1
        q4 = CurrFrame[ plane ][ y + dy * 4 ][ x + dx * 4 ]
        q5 = CurrFrame[ plane ][ y + dy * 5 ][ x + dx * 5 ]
        q6 = CurrFrame[ plane ][ y + dy * 6 ][ x + dx * 6 ]
    }
    p0 = CurrFrame[ plane ][ y - dy     ][ x - dx     ]
    p1 = CurrFrame[ plane ][ y - dy * 2 ][ x - dx * 2 ]
    p2 = CurrFrame[ plane ][ y - dy * 3 ][ x - dx * 3 ]
    p3 = CurrFrame[ plane ][ y - dy * 4 ][ x - dx * 4 ]
    if ( filterSize == 16 ) {
        p4 = CurrFrame[ plane ][ y - dy * 5 ][ x - dx * 5 ]
        p5 = CurrFrame[ plane ][ y - dy * 6 ][ x - dx * 6 ]
        p6 = CurrFrame[ plane ][ y - dy * 7 ][ x - dx * 7 ]
    }

    fm[ 0 ] = 0
    threshBd = thresh << (BitDepth - 8)
    fm[ 0 ] |= (Abs( p1 - p0 ) > threshBd)
    fm[ 0 ] |= (Abs( q1 - q0 ) > threshBd)

    if ( filterSize == 4 ) filterLen = 4
    else if ( plane != 0 ) filterLen = 6
    else if ( filterSize == 8 ) filterLen = 8
    else filterLen = 16

    limitBd = limit << (BitDepth - 8)
    blimitBd = blimit << (BitDepth - 8)
    mask = 0
    mask |= (Abs( p1 - p0 ) > limitBd)
    mask |= (Abs( q1 - q0 ) > limitBd)
    mask |= (Abs( p0 - q0 ) * 2 + Abs( p1 - q1 ) / 2 > blimitBd)
    if ( filterLen >= 6 ) {
        mask |= (Abs( p2 - p1 ) > limitBd)
        mask |= (Abs( q2 - q1 ) > limitBd)
    }
    if ( filterLen >= 8 ) {
        mask |= (Abs( p3 - p2 ) > limitBd)
        mask |= (Abs( q3 - q2 ) > limitBd)
    }
    fm[ 1 ] = (mask == 0)

    thresholdBd = 1 << (BitDepth - 8)
    if ( filterSize >= 8 ) {
        mask = 0
        mask |= (Abs( p1 - p0 ) > thresholdBd)
        mask |= (Abs( q1 - q0 ) > thresholdBd)
        mask |= (Abs( p2 - p0 ) > thresholdBd)
        mask |= (Abs( q2 - q0 ) > thresholdBd)
        if ( filterLen >= 8 ) {
            mask |= (Abs( p3 - p0 ) > thresholdBd)
            mask |= (Abs( q3 - q0 ) > thresholdBd)
        }
        fm[ 2 ] = (mask == 0)
    }

    thresholdBd = 1 << (BitDepth - 8)
    if ( filterSize >= 16 ) {
        ASSERT( pq4Defined, "Must define p4 before being used")
        mask = 0
        mask |= (Abs( p6 - p0 ) > thresholdBd)
        mask |= (Abs( q6 - q0 ) > thresholdBd)
        mask |= (Abs( p5 - p0 ) > thresholdBd)
        mask |= (Abs( q5 - q0 ) > thresholdBd)
        mask |= (Abs( p4 - p0 ) > thresholdBd)
        mask |= (Abs( q4 - q0 ) > thresholdBd)
        fm[ 3 ] = (mask == 0)
    }
}

filter4_clamp( value ) {
    ret = Clip3( -(1 << (BitDepth - 1)), (1 << (BitDepth - 1)) - 1, value )
    return ret
}

narrow_filter( hevMask, x, y, plane, limit, blimit, thresh, dx, dy ) {
    COVERCROSS(CROSS_NARROW_FILTER, (hevMask != 0), (plane > 0))

    q0 = CurrFrame[ plane ][ y ][ x ]
    q1 = CurrFrame[ plane ][ y + dy ][ x + dx ]
    p0 = CurrFrame[ plane ][ y - dy ][ x - dx ]
    p1 = CurrFrame[ plane ][ y - dy * 2 ][ x - dx * 2 ]
    ps1 = p1 - (0x80 << (BitDepth - 8))
    ps0 = p0 - (0x80 << (BitDepth - 8))
    qs0 = q0 - (0x80 << (BitDepth - 8))
    qs1 = q1 - (0x80 << (BitDepth - 8))
    filter = hevMask ? filter4_clamp( ps1 - qs1 ) : 0
    filter = filter4_clamp( filter + 3 * (qs0 - ps0) )
    filter1 = filter4_clamp( filter + 4 ) >> 3
    filter2 = filter4_clamp( filter + 3 ) >> 3
    oq0 = filter4_clamp( qs0 - filter1 ) + (0x80 << (BitDepth - 8)) // E-61 [RNG-Narrow1]
    op0 = filter4_clamp( ps0 + filter2 ) + (0x80 << (BitDepth - 8)) // E-62 [RNG-Narrow2]
    CurrFrame[ plane ][ y ][ x ] = oq0
    CurrFrame[ plane ][ y - dy ][ x - dx ] = op0
    if ( !hevMask ) {
        filter = Round2( filter1, 1 )
        oq1 = filter4_clamp( qs1 - filter ) + (0x80 << (BitDepth - 8)) // E-63 [RNG-Narrow3]
        op1 = filter4_clamp( ps1 + filter ) + (0x80 << (BitDepth - 8)) // E-64 [RNG-Narrow4]
        CurrFrame[ plane ][ y + dy ][ x + dx ] = oq1
        CurrFrame[ plane ][ y - dy * 2 ][ x - dx * 2 ] = op1
    }
}

wide_filter( x, y, plane, dx, dy, uint_3_4 log2Size ) {
    COVERCROSS(CROSS_WIDE_FILTER, log2Size, (plane > 0))
    int F[ 12 ]

    if ( log2Size == 4 ) n = 6
    else if ( plane == 0 ) n = 3
    else n = 2
    if ( log2Size == 3 && plane == 0 ) n2 = 0
    else n2 = 1

    for( i = -n; i < n; i++ ) {
        t = 0
        for( j = -n; j <= n; j++ ) {
            pp = Clip3( -( n + 1 ), n, i + j )
            tap = ( Abs( j ) <= n2 ) ? 2 : 1
            t += CurrFrame[ plane ][ y + pp * dy ][ x + pp * dx ] * tap // E-65 [RNG-Wide1]
        }
        F[ i + 6 ] = Round2( t, log2Size ) // E-66 [RNG-Wide2]
    }
    for( i = -n; i < n; i++ )
        CurrFrame[ plane ][ y+i * dy ][ x+i * dx ] = F[ i + 6 ]
}
