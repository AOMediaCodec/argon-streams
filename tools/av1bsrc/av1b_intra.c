/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_SPEC_INTRA 0

int Intra_Filter_Taps[ INTRA_FILTER_MODES ][ 8 ][ 7 ] = {
  {
      { -6, 10, 0, 0, 0, 12, 0 },
      { -5, 2, 10, 0, 0, 9, 0 },
      { -3, 1, 1, 10, 0, 7, 0 },
      { -3, 1, 1, 2, 10, 5, 0 },
      { -4, 6, 0, 0, 0, 2, 12 },
      { -3, 2, 6, 0, 0, 2, 9 },
      { -3, 2, 2, 6, 0, 2, 7 },
      { -3, 1, 2, 2, 6, 3, 5 },
  },
  {
      { -10, 16, 0, 0, 0, 10, 0 },
      { -6, 0, 16, 0, 0, 6, 0 },
      { -4, 0, 0, 16, 0, 4, 0 },
      { -2, 0, 0, 0, 16, 2, 0 },
      { -10, 16, 0, 0, 0, 0, 10 },
      { -6, 0, 16, 0, 0, 0, 6 },
      { -4, 0, 0, 16, 0, 0, 4 },
      { -2, 0, 0, 0, 16, 0, 2 },
  },
  {
      { -8, 8, 0, 0, 0, 16, 0 },
      { -8, 0, 8, 0, 0, 16, 0 },
      { -8, 0, 0, 8, 0, 16, 0 },
      { -8, 0, 0, 0, 8, 16, 0 },
      { -4, 4, 0, 0, 0, 0, 16 },
      { -4, 0, 4, 0, 0, 0, 16 },
      { -4, 0, 0, 4, 0, 0, 16 },
      { -4, 0, 0, 0, 4, 0, 16 },
  },
  {
      { -2, 8, 0, 0, 0, 10, 0 },
      { -1, 3, 8, 0, 0, 6, 0 },
      { -1, 2, 3, 8, 0, 4, 0 },
      { 0, 1, 2, 3, 8, 2, 0 },
      { -1, 4, 0, 0, 0, 3, 10 },
      { -1, 3, 4, 0, 0, 4, 6 },
      { -1, 2, 3, 4, 0, 4, 4 },
      { -1, 2, 2, 3, 4, 3, 3 },
  },
  {
      { -12, 14, 0, 0, 0, 14, 0 },
      { -10, 0, 14, 0, 0, 12, 0 },
      { -9, 0, 0, 14, 0, 11, 0 },
      { -8, 0, 0, 0, 14, 10, 0 },
      { -10, 12, 0, 0, 0, 0, 14 },
      { -9, 1, 12, 0, 0, 0, 12 },
      { -8, 0, 0, 12, 0, 1, 11 },
      { -7, 0, 0, 1, 12, 1, 9 },
  }
};

predict_intra( plane, x, y, haveLeft, haveAbove, haveAboveRight, haveBelowLeft, mode, log2W, log2H ) {
    w = 1 << log2W
    h = 1 << log2H
    maxX = (MiCols * MI_SIZE) - 1
    maxY = (MiRows * MI_SIZE) - 1
    if ( plane > 0 ) {
        maxX = ( (MiCols * MI_SIZE) >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)) - 1
        maxY = ( (MiRows * MI_SIZE) >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)) - 1
    }

    :C g_cd_info->predict_intra(plane, x, y, haveLeft, haveAbove, haveAboveRight, haveBelowLeft, mode, log2W, log2H, maxX, maxY);

#if VALIDATE_SPEC_INTRA
    validate(50005)
    validate(plane)
    validate(w)
    validate(h)
    validate(haveLeft?1:0)
    validate(haveAbove?1:0)
    if (haveLeft)
        validate(haveBelowLeft?1:0)
    if (haveAbove)
        validate(haveAboveRight?1:0)
#endif
    for ( i = 0; i < w + h; i++ ) {
        if ( haveLeft == 0 && haveAbove == 1 ) {
            LeftCol[ i ] = CurrFrame[ plane ][ y - 1 ][ x ]
#if VALIDATE_SPEC_INTRA
            validate(51004)
            validate(i)
            validate(LeftCol[i])
#endif
        } else if ( haveLeft == 0 ) {
            LeftCol[ i ] = ( 1 << ( BitDepth - 1 ) ) + 1
#if VALIDATE_SPEC_INTRA
            validate(51005)
            validate(i)
            validate(LeftCol[i])
#endif
        } else {
            leftLimit = Min( maxY, y + ( haveBelowLeft ? 2 * h : h ) - 1 )
            LeftCol[ i ] = CurrFrame[ plane ][ Min(leftLimit, y+i) ][ x-1 ]
#if VALIDATE_SPEC_INTRA
            validate(haveBelowLeft ? 51000 : 51002)
            validate(i)
            validate(plane)
            validate(Min(leftLimit, y+i))
            validate(x-1)
            validate(LeftCol[i])
#endif
        }
    }
    for ( i = 0; i < w + h; i++ ) {
        if ( haveAbove == 0 && haveLeft == 1 ) {
            AboveRow[ i ] = CurrFrame[ plane ][ y ][ x - 1 ]
#if VALIDATE_SPEC_INTRA
            validate(51010)
            validate(i)
            validate(AboveRow[i])
#endif
        } else if ( haveAbove == 0 ) {
            AboveRow[ i ] = ( 1 << ( BitDepth - 1 ) ) - 1
#if VALIDATE_SPEC_INTRA
            validate(51011)
            validate(i)
            validate(AboveRow[i])
#endif
        } else {
            aboveLimit = Min( maxX, x + ( haveAboveRight ? 2 * w : w ) - 1 )
            AboveRow[ i ] = CurrFrame[ plane ][ y-1 ][ Min(aboveLimit, x+i) ]
#if VALIDATE_SPEC_INTRA
            validate( haveAboveRight ? 51006 : 51008 )
            validate(i)
            validate(AboveRow[i])
#endif
        }
    }
    if ( haveAbove == 1 && haveLeft == 1 ) {
        AboveRow[ -1 ] = CurrFrame[ plane ][ y - 1 ][ x - 1 ]
#if VALIDATE_SPEC_INTRA
        validate(51012)
        validate(AboveRow[-1])
#endif
    } else if ( haveAbove == 1 ) {
        AboveRow[ -1 ] = CurrFrame[ plane ][ y - 1 ][ x ]
#if VALIDATE_SPEC_INTRA
        validate(51013)
        validate(AboveRow[-1])
#endif
    } else if ( haveLeft == 1 ) {
        AboveRow[ -1 ] = CurrFrame[ plane ][ y ][ x - 1 ]
#if VALIDATE_SPEC_INTRA
        validate(51014)
        validate(AboveRow[-1])
#endif
    } else {
        AboveRow[ -1 ] = 1 << ( BitDepth - 1 )
#if VALIDATE_SPEC_INTRA
        validate(51015)
        validate(AboveRow[-1])
#endif
    }
    LeftCol[ -1 ] = AboveRow[ -1 ]
    if ( plane == 0 && use_filter_intra ) recursive_intra_prediction( w, h )
    else if ( is_directional_mode( mode ) ) directional_intra_prediction( plane, x, y, haveLeft, haveAbove, mode, w, h, maxX, maxY )
    else if ( mode == SMOOTH_PRED || mode == SMOOTH_V_PRED || mode == SMOOTH_H_PRED ) smooth_intra_prediction( mode, log2W, log2H, w, h )
    else if ( mode == DC_PRED ) dc_intra_prediction( haveLeft, haveAbove, log2W, log2H, w, h )
    else basic_intra_prediction( mode, w, h )

#if VALIDATE_SPEC_INTRA
    validate(52000)
#endif
    for ( i = 0; i < h; i++ )
        for ( j = 0; j < w; j++ ) {
#if VALIDATE_SPEC_INTRA
            validate(ReturnPred[ i ][ j ])
#endif
            CurrFrame[ plane ][ y + i ][ x + j ] = ReturnPred[ i ][ j ]
        }
}

basic_intra_prediction( mode, w, h ) {
#if VALIDATE_SPEC_INTRA
    validate(50004)
#endif
    for ( i = 0; i < h; i++ )
        for ( j = 0; j < w; j++ ) {
            base = AboveRow[ j ] + LeftCol[ i ] - AboveRow[ -1 ] // E-151 [RNG-BasicPred1]
            pLeft = Abs( base - LeftCol[ i ]) // E-152 [RNG-BasicPred2]
            pTop = Abs( base - AboveRow[ j ]) // E-153 [RNG-BasicPred3]
            pTopLeft = Abs( base - AboveRow[ -1 ] ) // E-154 [RNG-BasicPred4]
            if (pLeft <= pTop && pLeft <= pTopLeft)
                ReturnPred[ i ][ j ] = LeftCol[ i ] // E-155 [RNG-BasicPred5]
            else if ( pTop <= pTopLeft )
                ReturnPred[ i ][ j ] = AboveRow[ j ] // E-156 [RNG-BasicPred6]
            else
                ReturnPred[ i ][ j ] = AboveRow[ -1 ] // E-157 [RNG-BasicPred7]
        }
}

recursive_intra_prediction( w, h ) {
    int pp[ 7 ]
#if VALIDATE_SPEC_INTRA
    validate(50000)
#endif
    w4 = w >> 2
    h2 = h >> 1

    for ( i2 = 0; i2 < h2; i2++ ) {
        for ( j4 = 0; j4 < w4; j4++ ) {
            for ( i = 0; i < 7; i++ ) {
                if ( i < 5 ) {
                    if ( i2 == 0 ) pp[ i ] = AboveRow[ ( j4 << 2 ) + i - 1 ]
                    else if ( j4 == 0 && i == 0 ) pp[ i ] = LeftCol[ ( i2 << 1 ) - 1 ]
                    else pp[ i ] = ReturnPred[ ( i2 << 1 ) - 1 ][ ( j4 << 2 ) + i - 1 ]
                } else {
                    if ( j4 == 0 ) pp[ i ] = LeftCol[ ( i2 << 1 ) + i - 5 ]
                    else pp[ i ] = ReturnPred[ ( i2 << 1 ) + i - 5 ][ ( j4 << 2 ) - 1 ]
                }
            }
            for ( i1 = 0; i1 < 2; i1++ ) {
                for ( j1 = 0; j1 < 4; j1++ ) {
                    pr = 0
                    for ( i = 0; i < 7; i++ ) {
#if COVER
                        uint_0_7 rec_intra_i1_j1
                        rec_intra_i1_j1 = ( i1 << 2 ) + j1
                        uint_0_6 rec_intra_i
                        rec_intra_i = i
                        COVERCROSS(CROSS_INTRA_FILTER_TAPS, filter_intra_mode, rec_intra_i1_j1, rec_intra_i)
#endif // COVER
                        pr += Intra_Filter_Taps[ filter_intra_mode ][ ( i1 << 2 ) + j1 ][ i ] * pp[ i ] // E-4 [RNG-RecursivePred1]
                    }
                    ReturnPred[ ( i2 << 1 ) + i1 ][ ( j4 << 2 ) + j1 ] = Clip1( Round2Signed( pr, INTRA_FILTER_SCALE_BITS ) ) // E-5 [RNG-RecursivePred2]
                }
            }
        }
    }
}

directional_intra_prediction( plane, x, y, haveLeft, haveAbove, mode, w, h, maxX, maxY ) {
    uint_0_212 pAngle
    angleDelta = ( plane == 0 ) ? AngleDeltaY : AngleDeltaUV
#if COVER
    uint_0_12 dir_intra_mode
    dir_intra_mode = mode
    COVERCROSS(VALUE_MODE_TO_ANGLE, dir_intra_mode)
#endif // COVER
    pAngle = Mode_To_Angle[ mode ] + angleDelta * ANGLE_STEP
    COVERCROSS(VALUE_INTRA_DIRECTIONAL_PANGLE, pAngle)
#if VALIDATE_SPEC_INTRA
    validate(50001)
    validate(angleDelta)
    validate(mode)
    validate(pAngle)
    validate(haveAbove?1:0)
    validate(haveLeft?1:0)
#endif

    upsampleAbove = 0
    upsampleLeft = 0
    if ( enable_intra_edge_filter == 1 ) {
        filterType = 0 // Not really needed, but keeps the compiler happy
        if ( pAngle != 90 && pAngle != 180 ) {
            if ( pAngle > 90 && pAngle < 180 && ( w + h ) >= 24 ) {
                LeftCol[ -1 ] = filter_corner( )
                AboveRow[ -1 ] = LeftCol[ -1 ]
            }
            filterType = get_filter_type( plane )
            if ( haveAbove == 1 ) {
                strength = intra_edge_filter_strength_selection( w, h, filterType, pAngle - 90 )
                numPx = Min( w, ( maxX - x + 1 ) ) + ( pAngle < 90 ? h : 0 ) + 1
                intra_edge_filter( numPx, strength, 0 )
            }
            if ( haveLeft == 1 ) {
                strength = intra_edge_filter_strength_selection( w, h, filterType, pAngle - 180 )
                numPx = Min( h, ( maxY - y + 1 ) ) + ( pAngle > 180 ? w : 0 ) + 1
                intra_edge_filter( numPx, strength, 1 )
            }
        }
        upsampleAbove = intra_edge_upsample_selection( w, h, filterType, pAngle - 90 )
        numPx = ( w + (pAngle < 90 ? h : 0) )
        if ( upsampleAbove == 1 )
            intra_edge_upsample( numPx, 0 )
        upsampleLeft = intra_edge_upsample_selection( w, h, filterType, pAngle - 180 )
        numPx = ( h + (pAngle > 180 ? w : 0) )
        if ( upsampleLeft == 1 )
            intra_edge_upsample( numPx, 1 )
    }

    dx = -1
    dy = -1
#if COVER
    uint_0_89 dir_pAngle
#endif // COVER
    if ( pAngle < 90 ) {
#if COVER
        dir_pAngle = pAngle
        COVERCROSS(VALUE_DR_INTRA_DERIVATIVE, dir_pAngle)
#endif // COVER
        dx = Dr_Intra_Derivative[ pAngle ]
    } else if ( pAngle > 90 && pAngle < 180 ) {
#if COVER
        dir_pAngle = 180 - pAngle
        COVERCROSS(VALUE_DR_INTRA_DERIVATIVE, dir_pAngle)
#endif // COVER
        dx = Dr_Intra_Derivative[ 180 - pAngle ]
    }
    if ( pAngle > 90 && pAngle < 180 ) {
#if COVER
        dir_pAngle = pAngle - 90
        COVERCROSS(VALUE_DR_INTRA_DERIVATIVE, dir_pAngle)
#endif // COVER
        dy = Dr_Intra_Derivative[ pAngle - 90 ]
    } else if ( pAngle > 180 ) {
#if COVER
        dir_pAngle = 270 - pAngle
        COVERCROSS(VALUE_DR_INTRA_DERIVATIVE, dir_pAngle)
#endif // COVER
        dy = Dr_Intra_Derivative[ 270 - pAngle ]
    }

#if VALIDATE_SPEC_INTRA
    validate(50006)
    validate(dx)
    validate(dy)
    validate(upsampleAbove?1:0)
    validate(upsampleLeft?1:0)
#endif

    if ( pAngle < 90 ) {
#if VALIDATE_SPEC_INTRA
        validate(500010)
#endif
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ ) {
                idx = ( i + 1 ) * dx
                base = ( idx >> ( 6 - upsampleAbove ) ) + ( j << upsampleAbove )
                shift = ( (idx << upsampleAbove) >> 1 ) & 0x1F
                maxBaseX = (w + h - 1) << upsampleAbove
#if VALIDATE_SPEC_INTRA
                validate(base)
                validate(shift)
                validate(maxBaseX)
#endif

                if ( base < maxBaseX ) {
#if VALIDATE_SPEC_INTRA
                    validate(1)
                    validate(idx)
                    validate(AboveRow[ base ])
                    validate(AboveRow[ base + 1 ])
#endif
                    ReturnPred[ i ][ j ] = Clip1( Round2( AboveRow[ base ] * ( 32 - shift ) + AboveRow[ base + 1 ] * shift, 5 ) ) // E-6 [RNG-DirPred1]
#if VALIDATE_SPEC_INTRA
                    validate(ReturnPred[ i ][ j ])
#endif
                } else {
#if VALIDATE_SPEC_INTRA
                    validate(2)
                    validate(idx)
#endif
                    ReturnPred[ i ][ j ] = AboveRow[ maxBaseX ] // E-7 [RNG-DirPred2]
#if VALIDATE_SPEC_INTRA
                    validate(ReturnPred[ i ][ j ])
#endif
                }
            }
    }
    if ( pAngle > 90 && pAngle < 180 ) {
#if VALIDATE_SPEC_INTRA
        validate(500011)
#endif
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ ) {
                idx = ( j << 6 ) - ( i + 1 ) * dx
                base = idx >> ( 6 - upsampleAbove )
                if ( base >= -(1 << upsampleAbove) ) {
                    shift = ( ( idx << upsampleAbove ) >> 1 ) & 0x1F
#if VALIDATE_SPEC_INTRA
                    validate(1)
                    validate(idx)
                    validate(base)
                    validate(shift)
                    validate(AboveRow[ base ])
                    validate(AboveRow[ base + 1 ])
#endif
                    ReturnPred[ i ][ j ] = Clip1( Round2( AboveRow[ base ] * ( 32 - shift ) + AboveRow[ base + 1 ] * shift, 5 ) ) // E-8 [RNG-DirPred3]
#if VALIDATE_SPEC_INTRA
                    validate(ReturnPred[ i ][ j ])
#endif
                } else {
                    idx = ( i << 6 ) - ( j + 1 ) * dy
                    base = idx >> ( 6 - upsampleLeft )
                    shift = ( ( idx << upsampleLeft ) >> 1 ) & 0x1F
#if VALIDATE_SPEC_INTRA
                    validate(2)
                    validate(idx)
                    validate(base)
                    validate(shift)
                    validate(LeftCol[ base ])
                    validate(LeftCol[ base + 1 ])
#endif
                    ReturnPred[ i ][ j ] = Clip1( Round2( LeftCol[ base ] * ( 32 - shift ) + LeftCol[ base + 1 ] * shift, 5 ) ) // E-9 [RNG-DirPred4]
#if VALIDATE_SPEC_INTRA
                    validate(ReturnPred[ i ][ j ])
#endif
                }
            }
    } else if ( pAngle > 180 ) {
#if VALIDATE_SPEC_INTRA
        validate(500012)
#endif
        for ( j = 0; j < w; j++ )
            for ( i = 0; i < h; i++ ) {
                idx = ( j + 1 ) * dy
                base = ( idx >> ( 6 - upsampleLeft ) ) + ( i << upsampleLeft )
                shift = ( ( idx << upsampleLeft ) >> 1 ) & 0x1F
#if VALIDATE_SPEC_INTRA
                validate(idx)
                validate(base)
                validate(shift)
#endif
#if VALIDATE_SPEC_INTRA
                validate(1)
                validate(LeftCol[ base ])
                validate(LeftCol[ base + 1 ])
#endif
                ReturnPred[ i ][ j ] = Clip1( Round2( LeftCol[ base ] * ( 32 - shift ) + LeftCol[ base + 1 ] * shift, 5 ) ) // E-10 [RNG-DirPred5]
#if VALIDATE_SPEC_INTRA
                validate(ReturnPred[ i ][ j ])
#endif

            }
    } else if ( pAngle == 90 ) {
#if VALIDATE_SPEC_INTRA
        validate(500013)
#endif
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ ) {
                ReturnPred[ i ][ j ] = AboveRow[ j ] // E-11 [RNG-DirPred6]
#if VALIDATE_SPEC_INTRA
                validate(ReturnPred[ i ][ j ])
#endif
            }
    } else if ( pAngle == 180 ) {
#if VALIDATE_SPEC_INTRA
        validate(500014)
#endif
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ ) {
                ReturnPred[ i ][ j ] = LeftCol[ i ] // E-12 [RNG-DirPred7]
#if VALIDATE_SPEC_INTRA
                validate(ReturnPred[ i ][ j ])
#endif
            }
    }

    :C g_cd_info->predict_intra_directional(pAngle, b->global_data->ReturnPred, h, w);
}

dc_intra_prediction( haveLeft, haveAbove, log2W, log2H, w, h ) {
#if VALIDATE_SPEC_INTRA
    validate(50003)
#endif
    if ( haveLeft == 1 && haveAbove == 1 ) {
        sum = 0
        for ( k = 0; k < h; k++ )
            sum += LeftCol[ k ] // E-160 [RNG-DCPred1]
        for ( k = 0; k < w; k++ )
            sum += AboveRow[ k ] // E-161 [RNG-DCPred2]

        sum += ( w + h ) >> 1 // E-162 [RNG-DCPred3]
        avg = sum / ( w + h ) // E-163 [RNG-DCPred4]
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ )
                ReturnPred[ i ][ j ] = avg
    } else if ( haveLeft == 1 ) {
      sum = 0
      for ( k = 0; k < h; k++ ) {
          sum += LeftCol[ k ] // E-164 [RNG-DCPred5]
      }
      leftAvg = Clip1( ( sum + ( h >> 1 ) ) >> log2H ) // E-165 [RNG-DCPred6]
      for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ )
                ReturnPred[ i ][ j ] = leftAvg
    } else if ( haveAbove == 1 ) {
        sum = 0
        for ( k = 0; k < w; k++ ) {
            sum += AboveRow[ k ] // E-166 [RNG-DCPred7]
        }
        aboveAvg = Clip1( ( sum + ( w >> 1 ) ) >> log2W ) // E-167 [RNG-DCPred8]
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ )
                ReturnPred[ i ][ j ] = aboveAvg
    } else {
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ )
                ReturnPred[ i ][ j ] = 1 << ( BitDepth - 1 ) // No range eqn needed for this one
    }
}

smooth_intra_prediction( mode, log2W, log2H, w, h ) {
#if VALIDATE_SPEC_INTRA
    validate(50002)
#endif
    if ( mode == SMOOTH_PRED ) {
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ ) {
                smoothPred = get_sm_weight(log2H, i) * AboveRow[ j ] + ( 256 - get_sm_weight(log2H, i) ) * LeftCol[ h - 1 ] + get_sm_weight(log2W, j) * LeftCol[ i ] + ( 256 - get_sm_weight(log2W, j) ) * AboveRow[ w - 1 ] // E-170 [RNG-SmoothPred1]
                ReturnPred[ i ][ j ] = Round2( smoothPred, 9 ) // E-171 [RNG-SmoothPred2]
            }
    }
    else if ( mode == SMOOTH_V_PRED ) {
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ ) {
                smoothPred = get_sm_weight(log2H, i) * AboveRow[ j ] + ( 256 - get_sm_weight(log2H, i) ) * LeftCol[ h - 1 ] // E-173 [RNG-SmoothVPred1]
                ReturnPred[ i ][ j ] = Round2( smoothPred, 8 ) // E-174 [RNG-SmoothVPred2]
            }
    }
    else {
        for ( i = 0; i < h; i++ )
            for ( j = 0; j < w; j++ ) {
                smoothPred = get_sm_weight(log2W, j) * LeftCol[ i ] + ( 256 - get_sm_weight(log2W, j) ) * AboveRow[ w - 1 ] // E-175 [RNG-SmoothHPred1]
                ReturnPred[ i ][ j ] = Round2( smoothPred, 8 ) // E-176 [RNG-SmoothHPred2]
            }
    }
}

get_sm_weight(log2W, i) {
    if ( log2W == 2 ) {
#if COVER
      uint_0_3 sm_weight_4x4_i
      sm_weight_4x4_i = i
      COVERCROSS(VALUE_SM_WEIGHTS_TX_4X4, sm_weight_4x4_i)
#endif // COVER
      return Sm_Weights_Tx_4x4[ i ]
    } else if ( log2W == 3 ) {
#if COVER
      uint_0_7 sm_weight_8x8_i
      sm_weight_8x8_i = i
      COVERCROSS(VALUE_SM_WEIGHTS_TX_8X8, sm_weight_8x8_i)
#endif // COVER
      return Sm_Weights_Tx_8x8[ i ]
    } else if ( log2W == 4 ) {
#if COVER
      uint_0_15 sm_weight_16x16_i
      sm_weight_16x16_i = i
      COVERCROSS(VALUE_SM_WEIGHTS_TX_16X16, sm_weight_16x16_i)
#endif // COVER
      return Sm_Weights_Tx_16x16[ i ]
    } else if ( log2W == 5 ) {
#if COVER
      uint_0_31 sm_weight_32x32_i
      sm_weight_32x32_i = i
      COVERCROSS(VALUE_SM_WEIGHTS_TX_32X32, sm_weight_32x32_i)
#endif // COVER
      return Sm_Weights_Tx_32x32[ i ]
    } else {
        ASSERT( log2W == 6, "Invalid log2W value in get_sm_weight()" )
#if COVER
      uint_0_63 sm_weight_64x64_i
      sm_weight_64x64_i = i
      COVERCROSS(VALUE_SM_WEIGHTS_TX_64X64, sm_weight_64x64_i)
#endif // COVER
        return Sm_Weights_Tx_64x64[ i ]
    }
}

intra_edge_filter_strength_selection( w, h, filterType, delta ) {
    blkWh = w + h
    strength = 0
    d = Abs(delta)
    if (filterType == 0) {
        if (blkWh <= 8) {
            if (d >= 56) strength = 1
        } else if (blkWh <= 12) {
            if (d >= 40) strength = 1
        } else if (blkWh <= 16) {
            if (d >= 40) strength = 1
        } else if (blkWh <= 24) {
            if (d >= 8) strength = 1
            if (d >= 16) strength = 2
            if (d >= 32) strength = 3
        } else if (blkWh <= 32) {
            strength = 1
            if (d >= 4) strength = 2
            if (d >= 32) strength = 3
        } else {
          strength = 3
        }
    } else {
        if (blkWh <= 8) {
            if (d >= 40) strength = 1
            if (d >= 64) strength = 2
        } else if (blkWh <= 16) {
            if (d >= 20) strength = 1
            if (d >= 48) strength = 2
        } else if (blkWh <= 24) {
            if (d >= 4) strength = 3
        } else {
            strength = 3
        }
    }
    return strength
}

int Intra_Edge_Kernel[INTRA_EDGE_KERNELS][INTRA_EDGE_TAPS] = {
    { 0, 4, 8, 4, 0 },
    { 0, 5, 6, 5, 0 },
    { 2, 4, 4, 4, 2 }
};

intra_edge_filter( sz, strength, left ) {
    int edge[ 129 ]
    for (i = 0; i < sz; i++)
        edge[ i ] = left ? LeftCol[ i - 1 ] : AboveRow[ i - 1 ]

    if ( strength != 0 ) {
        for (i = 1; i < sz; i++) {
            s = 0
            for (j = 0; j < INTRA_EDGE_TAPS; j++) {
                k = Clip3( 0, sz - 1, i - 2 + j )
#if COVER
                uint_0_2 kernel_1
                kernel_1 = strength - 1
                uint_0_4 edge_1
                edge_1 = j
                COVERCROSS(CROSS_INTRA_EDGE_KERNEL, kernel_1, edge_1)
#endif // COVER
                s += Intra_Edge_Kernel[ strength - 1 ][ j ] * edge[ k ] // E-22 [RNG-IntraEdgeFilter1]
#if VALIDATE_SPEC_INTRA
                validate(60005)
                validate(k)
                validate(Intra_Edge_Kernel[ strength - 1 ][ j ])
                validate( left ? 1 : 0 )
                validate( edge[ k ] )
                validate(s)
#endif
            }
            if ( left == 1 ) {
              LeftCol[ i - 1 ] = ( s + 8 ) >> 4 // E-23 [RNG-IntraEdgeFilter2]
#if VALIDATE_SPEC_INTRA
              validate(60003)
              validate(LeftCol[ i - 1 ])
#endif
            }
            else {
              AboveRow[ i - 1 ] = ( s + 8 ) >> 4 // E-24 [RNG-IntraEdgeFilter3]
#if VALIDATE_SPEC_INTRA
              validate(60004)
              validate(AboveRow[ i - 1 ])
#endif
            }
        }
    }
}

filter_corner( ) {
    s = LeftCol[ 0 ] * 5 + AboveRow[ -1 ] * 6 + AboveRow[ 0 ] * 5 // E-19 [RNG-FilterCorner]
    return Round2(s, 4)
}

get_filter_type( plane ) {
  aboveSmooth = 0
  leftSmooth = 0
  if ( ( plane == 0 ) ? AvailU : AvailUChroma ) {
    r = MiRow - 1
    c = MiCol
    if ( plane > 0 ) {
        if ( COVERCLASS((PROFILE0 || PROFILE2), 1, COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) && COVERCLASS((PROFILE0 || PROFILE2), !( MiCol & 1 )) ) )
            c++
        if ( COVERCLASS((PROFILE0 || PROFILE2), 1, COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) && COVERCLASS((PROFILE0 || PROFILE2), ( MiRow & 1 )) ) )
            r--
    }
    aboveSmooth = is_smooth( r, c, plane )
  }
  if ( ( plane == 0 ) ? AvailL : AvailLChroma ) {
    r = MiRow
    c = MiCol - 1
    if ( plane > 0 ) {
        if ( COVERCLASS((PROFILE0 || PROFILE2), 1, COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x) && COVERCLASS((PROFILE0 || PROFILE2), ( MiCol & 1 )) ) )
            c--
        if ( COVERCLASS((PROFILE0 || PROFILE2), 1, COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y) && COVERCLASS((PROFILE0 || PROFILE2), !( MiRow & 1 )) ) )
            r++
    }
    leftSmooth = is_smooth( r, c, plane )
  }
  return aboveSmooth || leftSmooth
}

is_smooth( row, col, plane ) {
  if ( plane == 0 ) {
    mode = YModes[ row ][ col ]
  } else {
    if ( RefFrames[ row ][ col ][ 0 ] > INTRA_FRAME )
      return 0
    mode = UVModes[ row ][ col ]
  }
  return (mode == SMOOTH_PRED || mode == SMOOTH_V_PRED || mode == SMOOTH_H_PRED)
}

intra_edge_upsample_selection( w, h, filterType, delta ) {
    d = Abs( delta )
    blkWh = w + h

    if (d <= 0 || d >= 40) {
        useUpsample = 0
    } else if ( filterType == 0 ) {
        useUpsample = (blkWh <= 16)
    } else {
        useUpsample = (blkWh <= 8)
    }

    return useUpsample
}

intra_edge_upsample( numPx, dir ) {
    int dup[ 32 ]

    dup[ 0 ] = ( dir == 0 ) ? AboveRow[ -1 ] : LeftCol[ -1 ]
    for (i = -1; i < numPx; i++) {
        dup[ i + 2 ] = ( dir == 0 ) ? AboveRow[ i ] : LeftCol[ i ]
    }
    dup[ numPx + 2 ] = ( dir == 0 ) ? AboveRow[ numPx - 1 ] : LeftCol[ numPx - 1 ]

    if ( dir == 0 )
        AboveRow[-2] = dup[0]
    else
        LeftCol[-2] = dup[0]
    for (i = 0; i < numPx; i++) {
        s = -dup[i] + (9 * dup[i + 1]) + (9 * dup[i + 2]) - dup[i + 3] // E-20 [RNG-IntraEdgeUpsample1]
        s = Clip1( Round2(s, 4) ) // E-21 [RNG-IntraEdgeUpsample2]
        if ( dir == 0 ) {
            AboveRow[ 2 * i - 1 ] = s
            AboveRow[ 2 * i ] = dup[i + 2]
        } else {
            LeftCol[ 2 * i - 1 ] = s
            LeftCol[ 2 * i ] = dup[i + 2]
        }
    }
}
