/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

le(n) {
    t = 0
    for( i = 0; i < n; i++) {
          byte                                                                 u(8)
        t += ( byte << ( i * 8 ) )
    }
    syntax = t
}

leb128() {
    int64 value
    value = 0
    Leb128Bytes = 0
    i = 0
    while (1) {
        ASSERT( i < 8, "Invalid leb128 state" )
          leb128_byte                                                          u(8)
        if ( i == 7 )
            CHECK( ( leb128_byte & 0x80 ) == 0, "leb128_byte's MSB must be equal to 0 when i is 7")
        value |= ( (leb128_byte & 0x7f) << (i*7) )
        Leb128Bytes += 1
        if ( !(leb128_byte & 0x80) ) {
            break
        }
        i++
    }
    CHECK( value <= 0xffffffff,   "leb128 return value must fit in unsigned 32bit value")
    syntax = value
}

su(n) {
    value                                                                      u(n)
    signMask = 1 << (n - 1)
    if (value & signMask)
        value = value - 2 * signMask
    syntax = value
}

L(n) {
    syntax = read_literal( n )
}

big_ns( n ) {
    w = FloorLog2(n) + 1
    m = (1 << w) - n
    v = read_literal( w - 1 )
    if (v < m) {
        syntax = v
    }
    else {
        extra_bit = read_literal( 1 )
        syntax = (v << 1) - m + extra_bit
    }
}

uvlc_num_ticks_per_picture_minus_1() {
    leadingZeros = 0
    while( 1 ) {
        done                                                                   u(1)
        if ( done )
            break
        leadingZeros++
    }
    if ( COVERCLASS(IMPOSSIBLE, 1, leadingZeros >= 32) ) {
        syntax = ( 1 << 32 ) - 1
    } else {
      value                                                                      u(leadingZeros)
      syntax = value + ( 1 << leadingZeros ) - 1
    }
}

uvlc() {
    leadingZeros = 0
    while( 1 ) {
        done                                                                   u(1)
        if ( done )
            break
        leadingZeros++
    }
    if ( COVERCLASS(IMPOSSIBLE, 1, leadingZeros >= 32) ) {
        syntax = ( 1 << 32 ) - 1
    } else {
      value                                                                      u(leadingZeros)
      syntax = value + ( 1 << leadingZeros ) - 1
    }
}

ns( n ) {
    w = FloorLog2(n) + 1
    m = (1 << w) - n
      v                                                                        u(w - 1)
    if (v < m)
        syntax = v
    else {
      extra_bit                                                                u(1)
      syntax = (v << 1) - m + extra_bit
    }
}

b_partition() {
    uint_1_5 bsl
    uint2 partition_ctx
    context_fn()
    partition_ctx = ctxIdxOffset
    if ( bsl == 1 ) syntax = read_symbol( TilePartitionW8Cdf[ ctxIdxOffset ], 4, 1 )
    else if ( bsl == 2 ) syntax = read_symbol( TilePartitionW16Cdf[ ctxIdxOffset ], 10, 1 )
    else if ( bsl == 3 ) syntax = read_symbol( TilePartitionW32Cdf[ ctxIdxOffset ], 10, 1 )
    else if ( bsl == 4 ) syntax = read_symbol( TilePartitionW64Cdf[ ctxIdxOffset ], 10, 1 )
    else syntax = read_symbol( TilePartitionW128Cdf[ ctxIdxOffset ], 8, 1 )
#if COVER
    uint_0_9 syntax_partition
    syntax_partition = syntax
    COVERCROSS(CROSS_PARTITION_CDF, bsl, partition_ctx, syntax_partition)
#endif // COVER
}

b_split_or_horz() {
    uint2 split_or_horz_ctx
    context_fn()
    split_or_horz_ctx = ctxIdxOffset
    uint16 cdfSOH[ 3 ]
    psum = ( get_partition_cdf( ctxIdxOffset, PARTITION_VERT, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_VERT - 1, bsl ) +
             get_partition_cdf( ctxIdxOffset, PARTITION_SPLIT, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_SPLIT - 1, bsl ) +
             get_partition_cdf( ctxIdxOffset, PARTITION_HORZ_A, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_HORZ_A - 1, bsl ) +
             get_partition_cdf( ctxIdxOffset, PARTITION_VERT_A, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_VERT_A - 1, bsl ) +
             get_partition_cdf( ctxIdxOffset, PARTITION_VERT_B, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_VERT_B - 1, bsl ) )
    if ( bSize != BLOCK_128X128 )
        psum += get_partition_cdf( ctxIdxOffset, PARTITION_VERT_4, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_VERT_4 - 1, bsl )
    cdfSOH[0] = ( 1 << 15 ) - psum
    cdfSOH[1] = 1 << 15
    cdfSOH[2] = 0
    syntax = read_symbol (cdfSOH, 2, 0)
#if COVER
    uint_0_1 syntax_split_or_horz
    syntax_split_or_horz = syntax
    COVERCROSS(CROSS_SPLIT_OR_HORZ_CDF, split_or_horz_ctx, syntax_split_or_horz)
#endif // COVER
}

b_split_or_vert() {
    uint2 split_or_vert_ctx
    context_fn()
    split_or_vert_ctx = ctxIdxOffset
    uint16 cdfSOV[ 3 ]
    psum = ( get_partition_cdf( ctxIdxOffset, PARTITION_HORZ, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_HORZ - 1, bsl ) +
             get_partition_cdf( ctxIdxOffset, PARTITION_SPLIT, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_SPLIT - 1, bsl ) +
             get_partition_cdf( ctxIdxOffset, PARTITION_HORZ_A, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_HORZ_A - 1, bsl ) +
             get_partition_cdf( ctxIdxOffset, PARTITION_HORZ_B, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_HORZ_B - 1, bsl ) +
             get_partition_cdf( ctxIdxOffset, PARTITION_VERT_A, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_VERT_A - 1, bsl ) )
    if ( bSize != BLOCK_128X128 )
        psum += get_partition_cdf( ctxIdxOffset, PARTITION_HORZ_4, bsl ) - get_partition_cdf( ctxIdxOffset, PARTITION_HORZ_4 - 1, bsl )
    cdfSOV[0] = ( 1 << 15 ) - psum
    cdfSOV[1] = 1 << 15
    cdfSOV[2] = 0
    syntax = read_symbol (cdfSOV, 2, 0)
#if COVER
    uint_0_1 syntax_split_or_vert
    syntax_split_or_vert = syntax
    COVERCROSS(CROSS_SPLIT_OR_VERT_CDF, split_or_vert_ctx, syntax_split_or_vert)
#endif // COVER
}

b_use_intrabc() {
    syntax = read_symbol( TileIntrabcCdf, 2, 1 )
}

b_intra_frame_y_mode() {
    uint_0_4 abovemode
    uint_0_4 leftmode
    abovemode = Intra_Mode_Context[ AvailU ? YModes[ MiRow - 1 ][ MiCol ] : DC_PRED ]
    leftmode = Intra_Mode_Context[ AvailL ? YModes[ MiRow ][ MiCol - 1] : DC_PRED ]
    syntax = read_symbol( TileIntraFrameYModeCdf[ abovemode ][ leftmode ], INTRA_MODES, 1  )
#if COVER
    uint_0_12 syntax_intra_frame_y_mode
    syntax_intra_frame_y_mode = AvailU ? YModes[ MiRow - 1 ][ MiCol ] : DC_PRED
    COVERCROSS(VALUE_INTRA_MODE_CONTEXT, syntax_intra_frame_y_mode)
    syntax_intra_frame_y_mode = AvailL ? YModes[ MiRow ][ MiCol - 1] : DC_PRED
    COVERCROSS(VALUE_INTRA_MODE_CONTEXT, syntax_intra_frame_y_mode)
    syntax_intra_frame_y_mode = syntax
    COVERCROSS(CROSS_INTRA_FRAME_Y_MODE_CDF, abovemode, leftmode, syntax_intra_frame_y_mode)
#endif // COVER
}

b_uv_mode() {
    uint_0_12 uv_mode_cfl_allowed_ctx
    uint_0_12 uv_mode_cfl_not_allowed_ctx
#if COVER
    uint_0_13 syntax_cfl_allowed
    uint_0_12 syntax_cfl_not_allowed
#endif // COVER

    context_fn()
    if ( Lossless == 1 && get_plane_residual_size( MiSize, 1 ) == BLOCK_4X4 ) {
        uv_mode_cfl_allowed_ctx = ctxIdxOffset
        syntax = read_symbol( TileUVModeCflAllowedCdf[ ctxIdxOffset ], UV_INTRA_MODES_CFL_ALLOWED, 1 )
#if COVER
        syntax_cfl_allowed = syntax
        COVERCROSS(CROSS_UV_MODE_CFL_ALLOWED_CDF, uv_mode_cfl_allowed_ctx, syntax_cfl_allowed)
#endif // COVER
    }
    else if ( Lossless == 0 && Max( Block_Width[ MiSize ], Block_Height[ MiSize ] ) <= 32 ) {
        uv_mode_cfl_allowed_ctx = ctxIdxOffset
        syntax = read_symbol( TileUVModeCflAllowedCdf[ ctxIdxOffset ], UV_INTRA_MODES_CFL_ALLOWED, 1 )
#if COVER
        syntax_cfl_allowed = syntax
        COVERCROSS(CROSS_UV_MODE_CFL_ALLOWED_CDF, uv_mode_cfl_allowed_ctx, syntax_cfl_allowed)
#endif // COVER
    }
    else {
        uv_mode_cfl_not_allowed_ctx = ctxIdxOffset
        syntax = read_symbol( TileUVModeCflNotAllowedCdf[ ctxIdxOffset ], UV_INTRA_MODES_CFL_NOT_ALLOWED, 1 )
#if COVER
        syntax_cfl_not_allowed = syntax
        COVERCROSS(CROSS_UV_MODE_CFL_NOT_ALLOWED_CDF, uv_mode_cfl_not_allowed_ctx, syntax_cfl_not_allowed)
#endif // COVER
    }
}

b_segment_id() {
    uint_0_2 segment_id_ctx
    context_fn()
    segment_id_ctx = ctxIdxOffset
    syntax = read_symbol( TileSegmentIdCdf[ ctxIdxOffset ], MAX_SEGMENTS, 1 )
#if COVER
    uint_0_7 syntax_segment_id
    syntax_segment_id = syntax
    COVERCROSS(CROSS_TILE_SEGMENT_ID_CDF, segment_id_ctx, syntax_segment_id)
#endif // COVER
}

b_skip_mode() {
    uint_0_2 skip_mode_ctx
    context_fn()
    skip_mode_ctx = ctxIdxOffset
    syntax = read_symbol( TileSkipModeCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_skip_mode
    syntax_skip_mode = syntax
    COVERCROSS(CROSS_TILE_SKIP_MODE_CDF, skip_mode_ctx, syntax_skip_mode)
#endif // COVER
}

b_skip() {
    uint_0_2 skip_ctx
    context_fn()
    skip_ctx = ctxIdxOffset
    syntax = read_symbol( TileSkipCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_skip
    syntax_skip = syntax
    COVERCROSS(CROSS_TILE_SKIP_CDF, skip_ctx, syntax_skip)
#endif // COVER
}

b_delta_q_abs() {
    syntax = read_symbol( TileDeltaQCdf, DELTA_Q_SMALL + 1, 1 )
#if COVER
    uint_0_3 syntax_delta_q_abs
    syntax_delta_q_abs = syntax
    COVERCROSS(VALUE_TILE_DELTA_Q_ABS, syntax_delta_q_abs)
#endif // COVER
}

b_delta_lf_abs() {
    uint2 delta_lf_multi_ctx
#if COVER
    uint_0_3 syntax_delta_lf_abs
#endif // COVER
    if ( delta_lf_multi == 0 ) {
        syntax = read_symbol( TileDeltaLFCdf, DELTA_LF_SMALL + 1, 1)
#if COVER
        syntax_delta_lf_abs = syntax
        COVERCROSS(VALUE_TILE_DELTA_LF_SINGLE_CDF, syntax_delta_lf_abs)
#endif // COVER
     } else {
        delta_lf_multi_ctx = i
        syntax = read_symbol( TileDeltaLFMultiCdf[ i ], DELTA_LF_SMALL + 1, 1 )
#if COVER
        syntax_delta_lf_abs = syntax
        COVERCROSS(CROSS_TILE_DELTA_LF_MULTI_CDF, delta_lf_multi_ctx, syntax_delta_lf_abs)
#endif // COVER
    }
}

b_tx_depth() {
    uint_0_2 tx_depth_ctx
    uint_1_4 cross_maxTxDepth
    context_fn()
    tx_depth_ctx = ctxIdxOffset
    cross_maxTxDepth = maxTxDepth
    if ( maxTxDepth == 4 ) syntax = read_symbol( TileTx64x64Cdf[ ctxIdxOffset ], MAX_TX_DEPTH + 1, 1 )
    else if ( maxTxDepth == 3 ) syntax = read_symbol( TileTx32x32Cdf[ ctxIdxOffset ], MAX_TX_DEPTH + 1, 1 )
    else if ( maxTxDepth == 2 ) syntax = read_symbol( TileTx16x16Cdf[ ctxIdxOffset ], MAX_TX_DEPTH + 1, 1 )
    else syntax = read_symbol( TileTx8x8Cdf[ ctxIdxOffset ], MAX_TX_DEPTH, 1 )
#if COVER
    uint_0_2 syntax_tx_depth
    syntax_tx_depth = syntax
    COVERCROSS(CROSS_MAX_TX_DEPTH_CDF, cross_maxTxDepth, tx_depth_ctx, syntax_tx_depth)
#endif // COVER
}

b_txfm_split() {
    uint_0_20 txfm_split_ctx
    context_fn()
    txfm_split_ctx = ctxIdxOffset
    syntax = read_symbol( TileTxfmSplitCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_txfm_split
    syntax_txfm_split = syntax
    COVERCROSS(CROSS_TXFM_SPLIT_CDF, txfm_split_ctx, syntax_txfm_split)
#endif // COVER
}

b_seg_id_predicted() {
    uint_0_2 segment_id_predicted_ctx
    context_fn()
    segment_id_predicted_ctx = ctxIdxOffset
    syntax = read_symbol( TileSegmentIdPredictedCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_seg_id_predicted
    syntax_seg_id_predicted = syntax
    COVERCROSS(CROSS_SEGMENT_ID_PREDICTED_CDF, segment_id_predicted_ctx, syntax_seg_id_predicted)
#endif // COVER
}

b_is_inter() {
    uint2 is_inter_contexts_ctx
    context_fn()
    is_inter_contexts_ctx = ctxIdxOffset
    syntax = read_symbol( TileIsInterCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_is_inter
    syntax_is_inter = syntax
    COVERCROSS(CROSS_IS_INTER_CDF, is_inter_contexts_ctx, syntax_is_inter)
#endif // COVER
}

b_use_filter_intra() {
    uint_0_21 filter_intra_ctx
    context_fn()
    filter_intra_ctx = ctxIdxOffset
    syntax = read_symbol( TileFilterIntraCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_use_filter_intra
    syntax_use_filter_intra = syntax
    COVERCROSS(CROSS_FILTER_INTRA_CDF, filter_intra_ctx, syntax_use_filter_intra)
#endif // COVER
}

b_filter_intra_mode() {
    syntax = read_symbol( TileFilterIntraModeCdf, 5, 1 )
#if COVER
    uint_0_4 syntax_filter_intra_mode
    syntax_filter_intra_mode = syntax
    COVERCROSS(VALUE_FILTER_INTRA_MODE, syntax_filter_intra_mode)
#endif // COVER
}

b_y_mode() {
    uint2 y_mode_ctx
    context_fn()
    y_mode_ctx = ctxIdxOffset
    syntax = read_symbol( TileYModeCdf[ ctxIdxOffset ], INTRA_MODES, 1 )
#if COVER
    uint_0_12 syntax_y_mode
    syntax_y_mode = syntax
    COVERCROSS(CROSS_Y_MODE_CDF, y_mode_ctx, syntax_y_mode)
#endif // COVER
}

b_compound_mode() {
    uint3 compound_mode_ctx
    context_fn()
    compound_mode_ctx = ctxIdxOffset
    syntax = read_symbol( TileCompoundModeCdf[ ctxIdxOffset ], COMPOUND_MODES, 1 )
#if COVER
    uint_0_7 syntax_compound_mode
    syntax_compound_mode = syntax
    COVERCROSS(CROSS_COMPOUND_MODE_CDF, compound_mode_ctx, syntax_compound_mode)
#endif // COVER
}

b_new_mv() {
    syntax = read_symbol( TileNewMvCdf[ NewMvContext ] , 2, 1 )
#if COVER
    uint_0_1 syntax_new_mv
    syntax_new_mv = syntax
    COVERCROSS(CROSS_NEW_MV_CDF, NewMvContext, syntax_new_mv)
#endif // COVER
}

b_zero_mv() {
    syntax = read_symbol( TileZeroMvCdf[ ZeroMvContext ], 2, 1 )
#if COVER
    uint_0_1 syntax_zero_mv
    syntax_zero_mv = syntax
    COVERCROSS(CROSS_ZERO_MV_CDF, ZeroMvContext, syntax_zero_mv)
#endif // COVER
}

b_ref_mv() {
    syntax = read_symbol( TileRefMvCdf[ RefMvContext ], 2, 1 )
#if COVER
    uint_0_1 syntax_ref_mv
    syntax_ref_mv = syntax
    COVERCROSS(CROSS_REF_MV_CDF, RefMvContext, syntax_ref_mv)
#endif // COVER
}

b_drl_mode() {
    context_fn()
    drl_mode_ctx = ctxIdxOffset
    syntax = read_symbol( TileDrlModeCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    // syntax_drl_mode is defiend in inter_block_mode_info
    syntax_drl_mode = syntax
    COVERCROSS(CROSS_DRL_MODE_CDF, drl_mode_ctx, syntax_drl_mode)
#endif // COVER
}

b_interp_filter() {
    uint4 interp_filter_ctx
    context_fn()
    interp_filter_ctx = ctxIdxOffset
    syntax = read_symbol( TileInterpFilterCdf[ ctxIdxOffset ], INTERP_FILTERS, 1 )
#if COVER
    uint_0_2 syntax_interp_filter
    syntax_interp_filter = syntax
    COVERCROSS(CROSS_INTERP_FILTER_CDF, interp_filter_ctx, syntax_interp_filter)
#endif // COVER
}

b_comp_mode() {
    uint_0_4 comp_mode_ctx
    context_fn()
    comp_mode_ctx = ctxIdxOffset
    syntax = read_symbol( TileCompModeCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_comp_mode
    syntax_comp_mode = syntax
    COVERCROSS(CROSS_COMP_MODE_CDF, comp_mode_ctx, syntax_comp_mode)
#endif // COVER
}

b_comp_ref_type() {
    uint_0_4 comp_ref_type_ctx
    context_fn()
    comp_ref_type_ctx = ctxIdxOffset
    syntax = read_symbol( TileCompRefTypeCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_comp_ref_type
    syntax_comp_ref_type = syntax
    COVERCROSS(CROSS_COMP_REF_TYPE_CDF, comp_ref_type_ctx, syntax_comp_ref_type)
#endif // COVER
}

b_uni_comp_ref() {
    uint_0_2 uni_comp_ref_0_ctx
    context_fn()
    uni_comp_ref_0_ctx = ctxIdxOffset
    syntax = read_symbol( TileUniCompRefCdf[ ctxIdxOffset ][ 0 ], 2, 1 )
#if COVER
    uint_0_1 syntax_uni_comp_ref
    syntax_uni_comp_ref = syntax
    COVERCROSS(CROSS_UNI_COMP_REF_0_CDF, uni_comp_ref_0_ctx, syntax_uni_comp_ref)
#endif // COVER
}

b_uni_comp_ref_p1() {
    uint_0_2 uni_comp_ref_1_ctx
    context_fn()
    uni_comp_ref_1_ctx = ctxIdxOffset
    syntax = read_symbol( TileUniCompRefCdf[ ctxIdxOffset ][ 1 ], 2, 1 )
#if COVER
    uint_0_1 syntax_uni_comp_ref_p1
    syntax_uni_comp_ref_p1 = syntax
    COVERCROSS(CROSS_UNI_COMP_REF_1_CDF, uni_comp_ref_1_ctx, syntax_uni_comp_ref_p1)
#endif // COVER
}

b_uni_comp_ref_p2() {
    uint_0_2 uni_comp_ref_2_ctx
    context_fn()
    uni_comp_ref_2_ctx = ctxIdxOffset
    syntax = read_symbol( TileUniCompRefCdf[ ctxIdxOffset ][ 2 ], 2, 1 )
#if COVER
    uint_0_1 syntax_uni_comp_ref_p2
    syntax_uni_comp_ref_p2 = syntax
    COVERCROSS(CROSS_UNI_COMP_REF_2_CDF, uni_comp_ref_2_ctx, syntax_uni_comp_ref_p2)
#endif // COVER
}

b_comp_ref() {
    uint_0_2 comp_ref_0_ctx
    context_fn()
    comp_ref_0_ctx = ctxIdxOffset
    syntax = read_symbol( TileCompRefCdf[ ctxIdxOffset ][ 0 ], 2, 1 )
#if COVER
    uint_0_1 syntax_comp_ref
    syntax_comp_ref = syntax
    COVERCROSS(CROSS_COMP_REF_0_CDF, comp_ref_0_ctx, syntax_comp_ref)
#endif // COVER
}

b_comp_ref_p1() {
    uint_0_2 comp_ref_1_ctx
    context_fn()
    comp_ref_1_ctx = ctxIdxOffset
    syntax = read_symbol( TileCompRefCdf[ ctxIdxOffset ][ 1 ], 2, 1 )
#if COVER
    uint_0_1 syntax_comp_ref_p1
    syntax_comp_ref_p1 = syntax
    COVERCROSS(CROSS_COMP_REF_1_CDF, comp_ref_1_ctx, syntax_comp_ref_p1)
#endif // COVER
}

b_comp_ref_p2() {
    uint_0_2 comp_ref_2_ctx
    context_fn()
    comp_ref_2_ctx = ctxIdxOffset
    syntax = read_symbol( TileCompRefCdf[ ctxIdxOffset ][ 2 ], 2, 1 )
#if COVER
    uint_0_1 syntax_comp_ref_p2
    syntax_comp_ref_p2 = syntax
    COVERCROSS(CROSS_COMP_REF_2_CDF, comp_ref_2_ctx, syntax_comp_ref_p2)
#endif // COVER
}

b_comp_bwdref() {
    uint_0_2 comp_bwdref_0_ctx
    context_fn()
    comp_bwdref_0_ctx = ctxIdxOffset
    syntax = read_symbol( TileCompBwdRefCdf[ ctxIdxOffset ][ 0 ], 2, 1 )
#if COVER
    uint_0_1 syntax_comp_bwdref
    syntax_comp_bwdref = syntax
    COVERCROSS(CROSS_COMP_BWDREF_0_CDF, comp_bwdref_0_ctx, syntax_comp_bwdref)
#endif // COVER
}

b_comp_bwdref_p1() {
    uint_0_2 comp_bwdref_1_ctx
    context_fn()
    comp_bwdref_1_ctx = ctxIdxOffset
    syntax = read_symbol( TileCompBwdRefCdf[ ctxIdxOffset ][ 1 ], 2, 1 )
#if COVER
    uint_0_1 syntax_comp_bwdref_p1
    syntax_comp_bwdref_p1 = syntax
    COVERCROSS(CROSS_COMP_BWDREF_1_CDF, comp_bwdref_1_ctx, syntax_comp_bwdref_p1)
#endif // COVER
}

b_single_ref_p1() {
    uint_0_2 comp_single_ref_0_ctx
    context_fn()
    comp_single_ref_0_ctx = ctxIdxOffset
    syntax = read_symbol( TileSingleRefCdf[ ctxIdxOffset ][ 0 ], 2, 1 )
#if COVER
    uint_0_1 syntax_single_ref_p1
    syntax_single_ref_p1 = syntax
    COVERCROSS(CROSS_COMP_SINGLE_REF_0_CDF, comp_single_ref_0_ctx, syntax_single_ref_p1)
#endif // COVER
}

b_single_ref_p2() {
    uint_0_2 comp_single_ref_1_ctx
    context_fn()
    comp_single_ref_1_ctx = ctxIdxOffset
    syntax = read_symbol( TileSingleRefCdf[ ctxIdxOffset ][ 1 ], 2, 1 )
#if COVER
    uint_0_1 syntax_single_ref_p2
    syntax_single_ref_p2 = syntax
    COVERCROSS(CROSS_COMP_SINGLE_REF_1_CDF, comp_single_ref_1_ctx, syntax_single_ref_p2)
#endif // COVER
}

b_single_ref_p3() {
    uint_0_2 comp_single_ref_2_ctx
    context_fn()
    comp_single_ref_2_ctx = ctxIdxOffset
    syntax = read_symbol( TileSingleRefCdf[ ctxIdxOffset ][ 2 ], 2, 1 )
#if COVER
    uint_0_1 syntax_single_ref_p3
    syntax_single_ref_p3 = syntax
    COVERCROSS(CROSS_COMP_SINGLE_REF_2_CDF, comp_single_ref_2_ctx, syntax_single_ref_p3)
#endif // COVER
}

b_single_ref_p4() {
    uint_0_2 comp_single_ref_3_ctx
    context_fn()
    comp_single_ref_3_ctx = ctxIdxOffset
    syntax = read_symbol( TileSingleRefCdf[ ctxIdxOffset ][ 3 ], 2, 1 )
#if COVER
    uint_0_1 syntax_single_ref_p4
    syntax_single_ref_p4 = syntax
    COVERCROSS(CROSS_COMP_SINGLE_REF_3_CDF, comp_single_ref_3_ctx, syntax_single_ref_p4)
#endif // COVER
}

b_single_ref_p5() {
    uint_0_2 comp_single_ref_4_ctx
    context_fn()
    comp_single_ref_4_ctx = ctxIdxOffset
    syntax = read_symbol( TileSingleRefCdf[ ctxIdxOffset ][ 4 ], 2, 1 )
#if COVER
    uint_0_1 syntax_single_ref_p5
    syntax_single_ref_p5 = syntax
    COVERCROSS(CROSS_COMP_SINGLE_REF_4_CDF, comp_single_ref_4_ctx, syntax_single_ref_p5)
#endif // COVER
}

b_single_ref_p6() {
    uint_0_2 comp_single_ref_5_ctx
    context_fn()
    comp_single_ref_5_ctx = ctxIdxOffset
    syntax = read_symbol( TileSingleRefCdf[ ctxIdxOffset ][ 5 ], 2, 1 )
#if COVER
    uint_0_1 syntax_single_ref_p6
    syntax_single_ref_p6 = syntax
    COVERCROSS(CROSS_COMP_SINGLE_REF_5_CDF, comp_single_ref_5_ctx, syntax_single_ref_p6)
#endif // COVER
}

b_use_obmc() {
    syntax = read_symbol( TileUseObmcCdf[ MiSize ], 2, 1 )
#if COVER
    uint_0_1 syntax_use_obmc
    syntax_use_obmc = syntax
    COVERCROSS(CROSS_USE_OBMC_CDF, MiSize, syntax_use_obmc)
#endif // COVER
}

b_motion_mode() {
    syntax = read_symbol( TileMotionModeCdf[ MiSize ], MOTION_MODES, 1 )
#if COVER
    uint_0_2 syntax_motion_mode
    syntax_motion_mode = syntax
    COVERCROSS(CROSS_MOTION_MODE_CDF, MiSize, syntax_motion_mode)
#endif // COVER
}

b_interintra() {
    uint_0_2 inter_intra_ctx
    context_fn()
    inter_intra_ctx = ctxIdxOffset
    syntax = read_symbol( TileInterIntraCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_interintra
    syntax_interintra = syntax
    COVERCROSS(CROSS_INTER_INTRA_CDF, inter_intra_ctx, syntax_interintra)
#endif // COVER
}

b_interintra_mode() {
    uint_0_2 inter_intra_mode_ctx
    context_fn()
    inter_intra_mode_ctx = ctxIdxOffset
    syntax = read_symbol( TileInterIntraModeCdf[ ctxIdxOffset ], INTERINTRA_MODES, 1 )
#if COVER
    uint_0_3 syntax_interintra_mode
    syntax_interintra_mode = syntax
    COVERCROSS(CROSS_INTER_INTRA_MODE_CDF, inter_intra_mode_ctx, syntax_interintra_mode)
#endif // COVER
}

b_wedge_index() {
    syntax = read_symbol( TileWedgeIndexCdf[ MiSize ], 16, 1 )
#if COVER
    uint_0_15 syntax_wedge_index
    syntax_wedge_index = syntax
    COVERCROSS(CROSS_WEDGE_INDEX_CDF, MiSize, syntax_wedge_index)
#endif // COVER
}

b_wedge_interintra() {
    syntax = read_symbol( TileWedgeInterIntraCdf[ MiSize ], 2, 1 )
#if COVER
    uint_0_1 syntax_wedge_interintra
    syntax_wedge_interintra = syntax
    COVERCROSS(CROSS_WEDGE_INTERINTRA_CDF, MiSize, syntax_wedge_interintra)
#endif // COVER
}

b_comp_group_idx() {
    uint_0_5 comp_group_idx_ctx
    context_fn()
    comp_group_idx_ctx = ctxIdxOffset
    syntax = read_symbol( TileCompGroupIdxCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_comp_group_idx
    syntax_comp_group_idx = syntax
    COVERCROSS(CROSS_COMP_GROUP_IDX_CDF, comp_group_idx_ctx, syntax_comp_group_idx)
#endif // COVER
}

b_compound_idx() {
    uint_0_5 compound_idx_ctx
    context_fn()
    compound_idx_ctx = ctxIdxOffset
    syntax = read_symbol( TileCompoundIdxCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_compound_idx
    syntax_compound_idx = syntax
    COVERCROSS(CROSS_COMPOUND_IDX_CDF, compound_idx_ctx, syntax_compound_idx)
#endif // COVER
}

b_compound_type() {
    syntax = read_symbol( TileCompoundTypeCdf[ MiSize ], COMPOUND_TYPES, 1 )
#if COVER
    uint_0_1 syntax_compound_type
    syntax_compound_type = syntax
    COVERCROSS(CROSS_COMPOUND_TYPE_CDF, MiSize, syntax_compound_type)
#endif // COVER
}

b_mv_joint() {
    syntax = read_symbol( TileMvJointCdf[ MvCtx ], MV_JOINTS, 1 )
#if COVER
    uint_0_3 syntax_mv_joint
    syntax_mv_joint = syntax
    COVERCROSS(CROSS_MV_JOINT_CDF, MvCtx, syntax_mv_joint)
#endif // COVER
}

b_mv_sign() {
    syntax = read_symbol( TileMvSignCdf[ MvCtx ][ comp ], 2, 1 )
#if COVER
    uint_0_1 syntax_mv_sign
    syntax_mv_sign = syntax
    COVERCROSS(CROSS_MV_SIGN_CDF, MvCtx, comp, syntax_mv_sign)
#endif // COVER
}

b_mv_class() {
    syntax = read_symbol( TileMvClassCdf[ MvCtx ][ comp ], MV_CLASSES, 1 )
#if COVER
    uint_0_10 syntax_mv_class
    syntax_mv_class = syntax
    COVERCROSS(CROSS_MV_CLASS_CDF, MvCtx, comp, syntax_mv_class)
#endif // COVER
}

b_mv_class0_bit() {
    syntax = read_symbol( TileMvClass0BitCdf[ MvCtx ][ comp ], 2, 1 )
#if COVER
    uint_0_1 syntax_mv_class0_bit
    syntax_mv_class0_bit = syntax
    COVERCROSS(CROSS_MV_CLASS0_BIT_CDF, MvCtx, comp, syntax_mv_class0_bit)
#endif // COVER
}

b_mv_class0_fr() {
    syntax = read_symbol( TileMvClass0FrCdf[ MvCtx ][ comp ][ mv_class0_bit ], MV_JOINTS, 1 )
#if COVER
    uint_0_3 syntax_mv_class0_fr
    syntax_mv_class0_fr = syntax
    COVERCROSS(CROSS_MV_CLASS0_FR_CDF, MvCtx, comp, mv_class0_bit, syntax_mv_class0_fr)
#endif // COVER
}

b_mv_class0_hp() {
    syntax = read_symbol( TileMvClass0HpCdf[ MvCtx ][ comp ], 2, 1 )
#if COVER
    uint_0_1 syntax_mv_class0_hp
    syntax_mv_class0_hp = syntax
    COVERCROSS(CROSS_MV_CLASS0_HP_CDF, MvCtx, comp, syntax_mv_class0_hp)
#endif // COVER
}

b_mv_bit() {
    uint_0_9 mv_bit_i
    mv_bit_i = i
    syntax = read_symbol( TileMvBitCdf[ MvCtx ][ comp ][ i ], 2, 1 )
#if COVER
    uint_0_1 syntax_mv_bit
    syntax_mv_bit = syntax
    COVERCROSS(CROSS_MV_BIT_CDF, MvCtx, comp, mv_bit_i, syntax_mv_bit)
#endif // COVER
}

b_mv_fr() {
    syntax = read_symbol( TileMvFrCdf[ MvCtx ][ comp ], MV_JOINTS, 1 )
#if COVER
    uint_0_3 syntax_mv_fr
    syntax_mv_fr = syntax
    COVERCROSS(CROSS_MV_FR_CDF, MvCtx, comp, syntax_mv_fr)
#endif // COVER
}

b_mv_hp() {
    syntax = read_symbol( TileMvHpCdf[ MvCtx ][ comp ], 2, 1 )
#if COVER
    uint_0_1 syntax_mv_hp
    syntax_mv_hp = syntax
    COVERCROSS(CROSS_MV_HP_CDF, MvCtx, comp, syntax_mv_hp)
#endif // COVER
}

b_all_zero() {
    uint_0_12 txb_skip_ctx
    context_fn()
    txb_skip_ctx = ctxIdxOffset
    syntax = read_symbol( TileTxbSkipCdf[ txSzCtx ][ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_all_zero
    syntax_all_zero = syntax
    COVERCROSS(CROSS_TXB_SKIP_CDF, txSzCtx, txb_skip_ctx, syntax_all_zero)
#endif // COVER
}

b_eob_pt_16() {
    uint1 eob_pt_16_ctx
    context_fn()
    eob_pt_16_ctx = ctxIdxOffset
    syntax = read_symbol( TileEobPt16Cdf[ ptype ][ ctxIdxOffset ], 5, 1 )
#if COVER
    uint_0_4 syntax_eob_pt_16
    syntax_eob_pt_16 = syntax
    COVERCROSS(CROSS_EOB_PT_16_CDF, ptype, eob_pt_16_ctx, syntax_eob_pt_16)
#endif // COVER
}

b_eob_pt_32() {
    uint1 eob_pt_32_ctx
    context_fn()
    eob_pt_32_ctx = ctxIdxOffset
    syntax = read_symbol( TileEobPt32Cdf[ ptype ][ ctxIdxOffset ], 6, 1 )
#if COVER
    uint_0_5 syntax_eob_pt_32
    syntax_eob_pt_32 = syntax
    COVERCROSS(CROSS_EOB_PT_32_CDF, ptype, eob_pt_32_ctx, syntax_eob_pt_32)
#endif // COVER
}

b_eob_pt_64() {
    uint1 eob_pt_64_ctx
    context_fn()
    eob_pt_64_ctx = ctxIdxOffset
    syntax = read_symbol( TileEobPt64Cdf[ ptype ][ ctxIdxOffset ], 7, 1 )
#if COVER
    uint_0_6 syntax_eob_pt_64
    syntax_eob_pt_64 = syntax
    COVERCROSS(CROSS_EOB_PT_64_CDF, ptype, eob_pt_64_ctx, syntax_eob_pt_64)
#endif // COVER
}

b_eob_pt_128() {
    uint1 eob_pt_128_ctx
    context_fn()
    eob_pt_128_ctx = ctxIdxOffset
    syntax = read_symbol( TileEobPt128Cdf[ ptype ][ ctxIdxOffset ], 8, 1 )
#if COVER
    uint_0_7 syntax_eob_pt_128
    syntax_eob_pt_128 = syntax
    COVERCROSS(CROSS_EOB_PT_128_CDF, ptype, eob_pt_128_ctx, syntax_eob_pt_128)
#endif // COVER
}

b_eob_pt_256() {
    uint1 eob_pt_256_ctx
    context_fn()
    eob_pt_256_ctx = ctxIdxOffset
    syntax = read_symbol( TileEobPt256Cdf[ ptype ][ ctxIdxOffset ], 9, 1 )
#if COVER
    uint_0_8 syntax_eob_pt_256
    syntax_eob_pt_256 = syntax
    COVERCROSS(CROSS_EOB_PT_256_CDF, ptype, eob_pt_256_ctx, syntax_eob_pt_256)
#endif // COVER
}

b_eob_pt_512() {
    syntax = read_symbol( TileEobPt512Cdf[ ptype ], 10, 1 )
#if COVER
    uint_0_9 syntax_eob_pt_512
    syntax_eob_pt_512 = syntax
    COVERCROSS(CROSS_EOB_PT_512_CDF, ptype, syntax_eob_pt_512)
#endif // COVER
}

b_eob_pt_1024() {
    syntax = read_symbol( TileEobPt1024Cdf[ ptype ], 11, 1 )
#if COVER
    uint_0_10 syntax_eob_pt_1024
    syntax_eob_pt_1024 = syntax
    COVERCROSS(CROSS_EOB_PT_1024_CDF, ptype, syntax_eob_pt_1024)
#endif // COVER
}

b_eob_extra() {
    uint_0_8 eob_extra_ctx
    eob_extra_ctx = eobPt - 3
    syntax = read_symbol( TileEobExtraCdf[ txSzCtx ][ ptype ][ eobPt - 3 ] , 2, 1 )
#if COVER
    uint_0_1 syntax_eob_extra
    syntax_eob_extra = syntax
    COVERCROSS(CROSS_EOB_EXTRA_CDF, txSzCtx, ptype, eob_extra_ctx, syntax_eob_extra)
#endif // COVER
}

b_coeff_base() {
    uint_0_41 coeff_base_ctx
    context_fn()
    coeff_base_ctx = ctxIdxOffset
    syntax = read_symbol( TileCoeffBaseCdf[ txSzCtx ][ ptype ][ ctxIdxOffset ], 4, 1 )
#if COVER
    uint_0_3 syntax_coeff_base
    syntax_coeff_base = syntax
    COVERCROSS(CROSS_COEFF_BASE_CDF, txSzCtx, ptype, coeff_base_ctx, syntax_coeff_base)
#endif // COVER
}

b_coeff_base_eob() {
    uint2 coeff_base_eob_ctx
    context_fn()
    coeff_base_eob_ctx = ctxIdxOffset
    syntax = read_symbol( TileCoeffBaseEobCdf[ txSzCtx ][ ptype ][ ctxIdxOffset ], 3, 1 )
#if COVER
    uint_0_2 syntax_coeff_base_eob
    syntax_coeff_base_eob = syntax
    COVERCROSS(CROSS_COEFF_BASE_EOB_CDF, txSzCtx, ptype, coeff_base_eob_ctx, syntax_coeff_base_eob)
#endif // COVER
}

b_dc_sign() {
    uint_0_2 dc_sign_ctx
    context_fn()
    dc_sign_ctx = ctxIdxOffset
    syntax = read_symbol( TileDcSignCdf[ ptype ][ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_dc_sign
    syntax_dc_sign = syntax
    COVERCROSS(CROSS_DC_SIGN_CDF, ptype, dc_sign_ctx, syntax_dc_sign)
#endif // COVER
}

b_coeff_br() {
    uint_0_3 coeff_br_txSzCtx
    uint_0_20 coeff_br_ctx
    context_fn()
    coeff_br_txSzCtx = Min( txSzCtx, TX_32X32 )
    coeff_br_ctx = ctxIdxOffset
    syntax = read_symbol( TileCoeffBrCdf[ Min( txSzCtx, TX_32X32 ) ][ ptype ][ ctxIdxOffset ], BR_CDF_SIZE, 1 )
#if COVER
    uint_0_3 syntax_coeff_br
    syntax_coeff_br = syntax
    COVERCROSS(CROSS_COEFF_BR_CDF, coeff_br_txSzCtx, ptype, coeff_br_ctx, syntax_coeff_br)
#endif // COVER
}

b_cfl_alpha_signs() {
    syntax = read_symbol( TileCflSignCdf, CFL_JOINT_SIGNS, 1 )
#if COVER
    uint_0_7 syntax_cfl_alpha_signs
    syntax_cfl_alpha_signs = syntax
    COVERCROSS(VALUE_CFL_ALPHA_SIGNS, syntax_cfl_alpha_signs)
#endif // COVER
}

b_cfl_alpha_u() {
    uint_0_5 cfl_alpha_u_ctx
    context_fn()
    cfl_alpha_u_ctx = ctxIdxOffset
    syntax = read_symbol( TileCflAlphaCdf[ ctxIdxOffset ], CFL_ALPHABET_SIZE, 1 )
#if COVER
    uint_0_15 syntax_cfl_alpha_u
    syntax_cfl_alpha_u = syntax
    COVERCROSS(CROSS_CFL_ALPHA_U_CDF, cfl_alpha_u_ctx, syntax_cfl_alpha_u)
#endif // COVER
}

b_cfl_alpha_v() {
    uint_0_5 cfl_alpha_v_ctx
    context_fn()
    cfl_alpha_v_ctx = ctxIdxOffset
    syntax = read_symbol( TileCflAlphaCdf[ ctxIdxOffset ], CFL_ALPHABET_SIZE, 1 )
#if COVER
    uint_0_15 syntax_cfl_alpha_v
    syntax_cfl_alpha_v = syntax
    COVERCROSS(CROSS_CFL_ALPHA_V_CDF, cfl_alpha_v_ctx, syntax_cfl_alpha_v)
#endif // COVER
}

b_has_palette_y() {
    uint_0_2 palette_y_mode_ctx
    context_fn()
    palette_y_mode_ctx = ctxIdxOffset
    syntax = read_symbol( TilePaletteYModeCdf[ bsizeCtx ][ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_has_palette_y
    syntax_has_palette_y = syntax
    COVERCROSS(CROSS_PALETTE_Y_MODE_CDF, bsizeCtx, palette_y_mode_ctx, syntax_has_palette_y)
#endif // COVER
}

b_palette_size_y_minus_2() {
    syntax = read_symbol( TilePaletteYSizeCdf[ bsizeCtx ], PALETTE_SIZES, 1 )
#if COVER
    uint_0_6 syntax_palette_size_y_minus2
    syntax_palette_size_y_minus2 = syntax
    COVERCROSS(CROSS_PALETTE_Y_SIZE_CDF, bsizeCtx, syntax_palette_size_y_minus2)
#endif // COVER
}

b_has_palette_uv() {
    uint1 palette_uv_mode_ctx
    context_fn()
    palette_uv_mode_ctx = ctxIdxOffset
    syntax = read_symbol( TilePaletteUVModeCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
    uint_0_1 syntax_has_palette_uv
    syntax_has_palette_uv = syntax
    COVERCROSS(CROSS_PALETTE_UV_MODE_CDF, palette_uv_mode_ctx, syntax_has_palette_uv)
#endif // COVER
}

b_palette_size_uv_minus_2() {
    syntax = read_symbol( TilePaletteUVSizeCdf[ bsizeCtx ], PALETTE_SIZES, 1 )
#if COVER
    uint_0_6 syntax_palette_size_uv_minus2
    syntax_palette_size_uv_minus2 = syntax
    COVERCROSS(CROSS_PALETTE_UV_SIZE_CDF, bsizeCtx, syntax_palette_size_uv_minus2)
#endif // COVER
}

// FIXME: What are the sizes of these CDFs (for inter_tx_type and intra_tx_type)
b_inter_tx_type() {
    uint1 inter_tx_type_set1_ctx
    uint2 inter_tx_type_set_ctx
    inter_tx_type_set_ctx = Tx_Size_Sqr[ txSz ]
#if COVER
    uint_0_18 inter_tx_type_txSz
    inter_tx_type_txSz = txSz
    COVERCROSS(VALUE_TX_SIZE_SQR, inter_tx_type_txSz)
#endif // COVER
    if ( set == TX_SET_INTER_1 ) {
        inter_tx_type_set1_ctx = Tx_Size_Sqr[ txSz ]
        syntax = read_symbol( TileInterTxTypeSet1Cdf[ Tx_Size_Sqr[ txSz ] ], 16, 1 )
#if COVER
        uint_0_15 syntax_inter_tx_type_set1
        syntax_inter_tx_type_set1 = syntax
        COVERCROSS(CROSS_INTER_TX_TYPE_SET1_CDF, inter_tx_type_set1_ctx, syntax_inter_tx_type_set1)
#endif // COVER
    }
    else if ( set == TX_SET_INTER_2 ) {
        syntax = read_symbol( TileInterTxTypeSet2Cdf, 12, 1 )
#if COVER
        uint_0_11 syntax_inter_tx_type_set2
        syntax_inter_tx_type_set2 = syntax
        COVERCROSS(VALUE_INTER_TX_TYPE_SET2_CDF, syntax_inter_tx_type_set2)
#endif // COVER
    }
    else {// if ( set == TX_SET_INTER_3 )
        syntax = read_symbol( TileInterTxTypeSet3Cdf[ Tx_Size_Sqr[ txSz ] ], 2, 1 )
#if COVER
        uint_0_1 syntax_inter_tx_type_set3
        syntax_inter_tx_type_set3 = syntax
        COVERCROSS(CROSS_INTER_TX_TYPE_SET3_CDF, inter_tx_type_set_ctx, syntax_inter_tx_type_set3)
#endif // COVER
    }
}

b_intra_tx_type() {
    uint1 intra_tx_type_set1_ctx
    uint_0_2 intra_tx_type_set2_ctx
    uint_0_12 intraDir

    if ( use_filter_intra ) {
#if COVER
      uint_0_4 intra_tx_type_filter_intra_mode
      intra_tx_type_filter_intra_mode = filter_intra_mode
      COVERCROSS(VALUE_FILTER_INTRA_MODE_TO_INTRA_DIR, intra_tx_type_filter_intra_mode)
#endif // COVER
      intraDir = Filter_Intra_Mode_To_Intra_Dir[ filter_intra_mode ]
    } else intraDir = YMode

    if ( set == TX_SET_INTRA_1 ) {
        intra_tx_type_set1_ctx = Tx_Size_Sqr[ txSz ]
        syntax = read_symbol( TileIntraTxTypeSet1Cdf[ Tx_Size_Sqr[ txSz ] ][ intraDir ], 7, 1 )
#if COVER
        uint_0_6 syntax_intra_tx_type_set1
        syntax_intra_tx_type_set1 = syntax
        COVERCROSS(CROSS_INTRA_TX_TYPE_SET1_CDF, intra_tx_type_set1_ctx, intraDir, syntax_intra_tx_type_set1)
#endif // COVER
    }
    else { // if ( set == TX_SET_INTRA_2 )
        intra_tx_type_set2_ctx = Tx_Size_Sqr[ txSz ]
        syntax = read_symbol( TileIntraTxTypeSet2Cdf[ Tx_Size_Sqr[ txSz ] ][ intraDir ], 5, 1 )
#if COVER
        uint_0_4 syntax_intra_tx_type_set2
        syntax_intra_tx_type_set2 = syntax
        COVERCROSS(CROSS_INTRA_TX_TYPE_SET2_CDF, intra_tx_type_set2_ctx, intraDir, syntax_intra_tx_type_set2)
#endif // COVER
    }
}

b_palette_color_idx_y() {
    uint_0_4 palette_color_idx_y_ctx
    context_fn()
    palette_color_idx_y_ctx = ctxIdxOffset
    if ( PaletteSizeY == 2 ) {
        syntax = read_symbol( TilePaletteSize2YColorCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
        uint_0_1 syntax_paletter_color_idx_y_size2
        syntax_paletter_color_idx_y_size2 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_2_Y_COLOR_CDF, palette_color_idx_y_ctx, syntax_paletter_color_idx_y_size2)
#endif // COVER
    }
    else if ( PaletteSizeY == 3 ) {
        syntax = read_symbol( TilePaletteSize3YColorCdf[ ctxIdxOffset ], 3, 1 )
#if COVER
        uint_0_2 syntax_paletter_color_idx_y_size3
        syntax_paletter_color_idx_y_size3 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_3_Y_COLOR_CDF, palette_color_idx_y_ctx, syntax_paletter_color_idx_y_size3)
#endif // COVER
    }
    else if ( PaletteSizeY == 4 ) {
        syntax = read_symbol( TilePaletteSize4YColorCdf[ ctxIdxOffset ], 4, 1 )
#if COVER
        uint_0_3 syntax_paletter_color_idx_y_size4
        syntax_paletter_color_idx_y_size4 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_4_Y_COLOR_CDF, palette_color_idx_y_ctx, syntax_paletter_color_idx_y_size4)
#endif // COVER
    }
    else if ( PaletteSizeY == 5 ) {
        syntax = read_symbol( TilePaletteSize5YColorCdf[ ctxIdxOffset ], 5, 1 )
#if COVER
        uint_0_4 syntax_paletter_color_idx_y_size5
        syntax_paletter_color_idx_y_size5 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_5_Y_COLOR_CDF, palette_color_idx_y_ctx, syntax_paletter_color_idx_y_size5)
#endif // COVER
    }
    else if ( PaletteSizeY == 6 ) {
        syntax = read_symbol( TilePaletteSize6YColorCdf[ ctxIdxOffset ], 6, 1 )
#if COVER
        uint_0_5 syntax_paletter_color_idx_y_size6
        syntax_paletter_color_idx_y_size6 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_6_Y_COLOR_CDF, palette_color_idx_y_ctx, syntax_paletter_color_idx_y_size6)
#endif // COVER
    }
    else if ( PaletteSizeY == 7 ) {
        syntax = read_symbol( TilePaletteSize7YColorCdf[ ctxIdxOffset ], 7, 1 )
#if COVER
        uint_0_6 syntax_paletter_color_idx_y_size7
        syntax_paletter_color_idx_y_size7 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_7_Y_COLOR_CDF, palette_color_idx_y_ctx, syntax_paletter_color_idx_y_size7)
#endif // COVER
    }
    else { // if ( PaletteSizeY == 8 )
        syntax = read_symbol( TilePaletteSize8YColorCdf[ ctxIdxOffset ], 8, 1 )
#if COVER
        uint_0_7 syntax_paletter_color_idx_y_size8
        syntax_paletter_color_idx_y_size8 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_8_Y_COLOR_CDF, palette_color_idx_y_ctx, syntax_paletter_color_idx_y_size8)
#endif // COVER
    }
}

b_palette_color_idx_uv() {
    uint_0_4 palette_color_idx_uv_ctx
    context_fn()
    palette_color_idx_uv_ctx = ctxIdxOffset
    if ( PaletteSizeUV == 2 ) {
        syntax = read_symbol( TilePaletteSize2UVColorCdf[ ctxIdxOffset ], 2, 1 )
#if COVER
        uint_0_1 syntax_paletter_color_idx_uv_size2
        syntax_paletter_color_idx_uv_size2 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_2_UV_COLOR_CDF, palette_color_idx_uv_ctx, syntax_paletter_color_idx_uv_size2)
#endif // COVER
    }
    else if ( PaletteSizeUV == 3 ) {
        syntax = read_symbol( TilePaletteSize3UVColorCdf[ ctxIdxOffset ], 3, 1 )
#if COVER
        uint_0_2 syntax_paletter_color_idx_uv_size3
        syntax_paletter_color_idx_uv_size3 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_3_UV_COLOR_CDF, palette_color_idx_uv_ctx, syntax_paletter_color_idx_uv_size3)
#endif // COVER
    }
    else if ( PaletteSizeUV == 4 ) {
        syntax = read_symbol( TilePaletteSize4UVColorCdf[ ctxIdxOffset ], 4, 1 )
#if COVER
        uint_0_3 syntax_paletter_color_idx_uv_size4
        syntax_paletter_color_idx_uv_size4 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_4_UV_COLOR_CDF, palette_color_idx_uv_ctx, syntax_paletter_color_idx_uv_size4)
#endif // COVER
    }
    else if ( PaletteSizeUV == 5 ) {
        syntax = read_symbol( TilePaletteSize5UVColorCdf[ ctxIdxOffset ], 5, 1 )
#if COVER
        uint_0_4 syntax_paletter_color_idx_uv_size5
        syntax_paletter_color_idx_uv_size5 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_5_UV_COLOR_CDF, palette_color_idx_uv_ctx, syntax_paletter_color_idx_uv_size5)
#endif // COVER
    }
    else if ( PaletteSizeUV == 6 ) {
        syntax = read_symbol( TilePaletteSize6UVColorCdf[ ctxIdxOffset ], 6, 1 )
#if COVER
        uint_0_5 syntax_paletter_color_idx_uv_size6
        syntax_paletter_color_idx_uv_size6 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_6_UV_COLOR_CDF, palette_color_idx_uv_ctx, syntax_paletter_color_idx_uv_size6)
#endif // COVER
    }
    else if ( PaletteSizeUV == 7 ) {
        syntax = read_symbol( TilePaletteSize7UVColorCdf[ ctxIdxOffset ], 7, 1 )
#if COVER
        uint_0_6 syntax_paletter_color_idx_uv_size7
        syntax_paletter_color_idx_uv_size7 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_7_UV_COLOR_CDF, palette_color_idx_uv_ctx, syntax_paletter_color_idx_uv_size7)
#endif // COVER
    }
    else { // if ( PaletteSizeUV == 8 )
        syntax = read_symbol( TilePaletteSize8UVColorCdf[ ctxIdxOffset ], 8, 1 )
#if COVER
        uint_0_7 syntax_paletter_color_idx_uv_size8
        syntax_paletter_color_idx_uv_size8 = syntax
        COVERCROSS(CROSS_PALETTE_SIZE_8_UV_COLOR_CDF, palette_color_idx_uv_ctx, syntax_paletter_color_idx_uv_size8)
#endif // COVER
    }
}

b_use_wiener() {
    syntax = read_symbol( TileUseWienerCdf, 2, 1 )
}

b_use_sgrproj() {
    syntax = read_symbol( TileUseSgrprojCdf, 2, 1 )
}

b_restoration_type() {
    syntax = read_symbol( TileRestorationTypeCdf, RESTORE_SWITCHABLE, 1 )
#if COVER
    uint_0_2 syntax_restoration_type
    syntax_restoration_type = syntax
    COVERCROSS(VALUE_RESTORATION_TYPE, syntax_restoration_type)
#endif // COVER
}

b_angle_delta_y() {
    uint3 angle_delta_y_ctx
    angle_delta_y_ctx = YMode - V_PRED
    syntax = read_symbol( TileAngleDeltaCdf[YMode - V_PRED], 2 * MAX_ANGLE_DELTA + 1, 1 )
#if COVER
    uint_0_6 syntax_angle_delta_y
    syntax_angle_delta_y = syntax
    COVERCROSS(CROSS_ANGLE_DELTA_Y_CDF, angle_delta_y_ctx, syntax_angle_delta_y)
#endif // COVER
}

b_angle_delta_uv() {
    uint3 angle_delta_uv_ctx
    angle_delta_uv_ctx = UVMode - V_PRED
    syntax = read_symbol( TileAngleDeltaCdf[UVMode - V_PRED], 2 * MAX_ANGLE_DELTA + 1, 1 )
#if COVER
    uint_0_6 syntax_angle_delta_uv
    syntax_angle_delta_uv = syntax
    COVERCROSS(CROSS_ANGLE_DELTA_UV_CDF, angle_delta_uv_ctx, syntax_angle_delta_uv)
#endif // COVER
}
