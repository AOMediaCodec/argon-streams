/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

ivf_version: unused_init, le(2) = -1;
headersize: unused_init, le(2) = -1;
g_w: unused_init, le(2) = -1;
g_h: unused_init, le(2) = -1;
g_timebase_den: unused_init, le(4) = -1;
g_timebase_num: unused_init, le(4) = -1;
frame_cnt: unused_init, le(4) = -1;

frame_sz: unused_init, le(4) = -1;
ivf_timebase_low: unused_init, le(4) = -1;
ivf_timebase_high: unused_init, le(4) = -1;

num_ticks_per_picture_minus_1: unused_init, uvlc_num_ticks_per_picture_minus_1() = -1;

obu_payload_size: unused_init, leb128() = -1;
obu_size: unused_init, leb128() = -1;
obu_length: unused_init, leb128() = -1;
frame_unit_size: unused_init, leb128() = -1;
temporal_unit_size: unused_init, leb128() = -1;
tile_size_minus_1: unused_init, le(TileSizeBytes) = -1;
metadata_type: unused_init, leb128() = -1;

loop_filter_ref_deltas: unused_init, su(6+1) = -1;
loop_filter_mode_deltas: unused_init, su(6+1) = -1;
delta_q: unused_init, su(6+1) = -1;
feature_value_signed: unused_init, su(bitsToRead+1) = -1;

delta_q_rem_bits: unused_init, L(3) = -1;
delta_q_abs_bits: unused_init, L(n) = -1;
delta_q_sign_bit: unused_init, L(1) = -1;
delta_lf_rem_bits: unused_init, L(3) = -1;
delta_lf_abs_bits: unused_init, L(n) = -1;
delta_lf_sign_bit: unused_init, L(1) = -1;
wedge_index: unused_init, b_wedge_index() = -1;
wedge_sign: unused_init, L(1) = -1;
mask_type: unused_init, L(1) = -1;
sign_bit: unused_init, L(1) = -1;
use_palette_color_cache_y: unused_init, L(1) = -1;
palette_colors_y: unused_init, L(BitDepth) = -1;
palette_num_extra_bits_y: unused_init, L(2) = -1;
r_palette_delta_y: unused_init, L(paletteBits) = -1;
use_palette_color_cache_u: unused_init, L(1) = -1;
palette_colors_u: unused_init, L(BitDepth) = -1;
palette_num_extra_bits_u: unused_init, L(2) = -1;
r_palette_delta_u: unused_init, L(paletteBits) = -1;
delta_encode_palette_colors_v: unused_init, L(1) = -1;
palette_num_extra_bits_v: unused_init, L(2) = -1;
r_palette_delta_v: unused_init, L(paletteBits) = -1;
palette_delta_sign_bit_v: unused_init, L(1) = -1;
palette_colors_v: unused_init, L(BitDepth) = -1;
r_cdef_idx: unused_init, L(cdef_bits) = -1;

all_zero: unused_init, b_all_zero() = {
    maxX4 = MiCols
    maxY4 = MiRows
    if ( plane > 0 ) {
        maxY4 = maxY4 >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
        maxX4 = maxX4 >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
    }
#if COVER
    uint_0_18 all_zero_txSz
    all_zero_txSz = txSz
    COVERCROSS(VALUE_TX_WIDTH, all_zero_txSz)
    COVERCROSS(VALUE_TX_HEIGHT, all_zero_txSz)
#endif // COVER
    w = Tx_Width[txSz]
    h = Tx_Height[txSz]

    bsize = get_plane_residual_size( MiSize, plane )
    bw = Block_Width[ bsize ]
    bh = Block_Height[ bsize ]

    if (plane == 0) {
        top = 0
        left = 0
        for (k = 0; k < w4; k++) {
            if ( x4 + k < maxX4 )
                top = Max( top, AboveLevelContext[ plane ][ x4 + k ] )
        }
        for (k = 0; k < h4; k++) {
            if ( y4 + k < maxY4 )
                left = Max( left, LeftLevelContext[ plane ][ ( y4 + k ) ] )
        }
        top = Min( top, 255 )
        left = Min( left, 255 )
        if ( bw == w && bh == h ) {
            ctxIdxOffset = 0
        } else if ( top == 0 && left == 0 ) {
            ctxIdxOffset = 1
        } else if ( top == 0 || left == 0 ) {
            ctxIdxOffset = 2 + ( Max( top, left ) > 3 )
        } else if ( Max( top, left ) <= 3 ) {
            ctxIdxOffset = 4
        } else if ( Min( top, left ) <= 3 ) {
            ctxIdxOffset = 5
        } else {
            ctxIdxOffset = 6
        }
    } else {
        above = 0
        left = 0
        for( i = 0; i < w4; i++ ) {
            if ( x4 + i < maxX4 ) {
                alc = AboveLevelContext[ plane ][ x4 + i ]
                adc = AboveDcContext[ plane ][ x4 + i ]
                above |= alc
                above |= adc
            }
        }
        for( i = 0; i < h4; i++ ) {
            if ( y4 + i < maxY4 ) {
                llc = LeftLevelContext[ plane ][ ( y4 + i ) ]
                ldc = LeftDcContext[ plane ][ ( y4 + i ) ]
                left |= llc
                left |= ldc
            }
        }
        ctxIdxOffset = ( above != 0 ) + ( left != 0 )
        ctxIdxOffset += 7
        if ( bw * bh > w * h )
            ctxIdxOffset += 3
    }
}

eob_pt_16: unused_init, b_eob_pt_16() = {
    txType = compute_tx_type( plane, txSz, x4, y4 )
    ctxIdxOffset = ( get_tx_class( txType ) == TX_CLASS_2D ) ? 0 : 1
}

eob_pt_32: unused_init, b_eob_pt_32() = {
    txType = compute_tx_type( plane, txSz, x4, y4 )
    ctxIdxOffset = ( get_tx_class( txType ) == TX_CLASS_2D ) ? 0 : 1
}

eob_pt_64: unused_init, b_eob_pt_64() = {
    txType = compute_tx_type( plane, txSz, x4, y4 )
    ctxIdxOffset = ( get_tx_class( txType ) == TX_CLASS_2D ) ? 0 : 1
}

eob_pt_128: unused_init, b_eob_pt_128() = {
    txType = compute_tx_type( plane, txSz, x4, y4 )
    ctxIdxOffset = ( get_tx_class( txType ) == TX_CLASS_2D ) ? 0 : 1
}

eob_pt_256: unused_init, b_eob_pt_256() = {
    txType = compute_tx_type( plane, txSz, x4, y4 )
    ctxIdxOffset = ( get_tx_class( txType ) == TX_CLASS_2D ) ? 0 : 1
}

eob_pt_512: unused_init, b_eob_pt_512() = -1;
eob_pt_1024: unused_init, b_eob_pt_1024() = -1;

eob_extra: unused_init, b_eob_extra() = -1;
eob_extra_bit: unused_init, L(1) = -1;
coeff_base: unused_init, b_coeff_base() = get_coeff_base_ctx(txSz, plane, x4, y4, scan[c], c, 0);
coeff_base_eob: unused_init, b_coeff_base_eob() = get_coeff_base_ctx(txSz, plane, x4, y4, scan[c], c, 1) - SIG_COEF_CONTEXTS + SIG_COEF_CONTEXTS_EOB;

dc_sign: unused_init, b_dc_sign() = {
    maxX4 = MiCols
    maxY4 = MiRows
    if ( plane > 0 ) {
        maxX4 = maxX4 >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)
        maxY4 = maxY4 >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)
    }

    dcSign = 0
    for ( k = 0; k < w4; k++ ) {
        if ( x4 + k < maxX4 ) {
            sign = AboveDcContext[ plane ][ x4 + k ]
            if ( sign == 1 ) {
                dcSign--
            } else if ( sign == 2 ) {
                dcSign++
            }
        }
    }
    mask = use_128x128_superblock ? 255 : 127
    for ( k = 0; k < h4; k++ ) {
        if ( y4 + k < maxY4 ) {
            sign = LeftDcContext[ plane ][ ( y4 + k ) ]
            if ( sign == 1 ) {
                dcSign--
            } else if ( sign == 2 ) {
                dcSign++
            }
        }
    }
    if ( dcSign < 0 ) {
        ctxIdxOffset = 1
    } else if ( dcSign > 0 ) {
        ctxIdxOffset = 2
    } else {
        ctxIdxOffset = 0
    }
}

coeff_br: unused_init, b_coeff_br() = {
    int nbMag[ 3 ]
#if COVER
    uint_0_18 coeff_br_txSz
    coeff_br_txSz = txSz
    COVERCROSS(VALUE_ADJUSTED_TX_SIZE, coeff_br_txSz)
#endif // COVER
    adjTxSz = get_Adjusted_Tx_Size( txSz )
    bwl = Tx_Width_Log2[ adjTxSz ]
    txw = Tx_Width[ adjTxSz ]
    txh = Tx_Height[ adjTxSz ]
#if COVER
    uint_0_18 coeff_br_adjTxSz
    coeff_br_adjTxSz = adjTxSz
    COVERCROSS(VALUE_TX_WIDTH_LOG2, coeff_br_adjTxSz)
    COVERCROSS(VALUE_TX_WIDTH, coeff_br_adjTxSz)
    COVERCROSS(VALUE_TX_HEIGHT, coeff_br_adjTxSz)
#endif // COVER
    row = pos >> bwl
    col = pos - (row << bwl)

    nbMag[ 0 ] = 0
    nbMag[ 1 ] = 0
    nbMag[ 2 ] = 0
    txType = compute_tx_type( plane, txSz, x4, y4 )
    txClass = get_tx_class( txType )

    for ( cbrIdx = 0; cbrIdx < 3; cbrIdx++ ) {
#if COVER
        uint_0_2 coeff_br_txClass
        coeff_br_txClass = txClass
        uint_0_2 coeff_br_cbrIdx
        coeff_br_cbrIdx = cbrIdx
        uint1 coeff_br_ref
        coeff_br_ref = 0
        COVERCROSS(CROSS_MAG_REF_OFFSET_WITH_TX_CLASS, coeff_br_txClass, coeff_br_cbrIdx, coeff_br_ref)
        coeff_br_ref = 1
        COVERCROSS(CROSS_MAG_REF_OFFSET_WITH_TX_CLASS, coeff_br_txClass, coeff_br_cbrIdx, coeff_br_ref)
#endif // COVER
        refRow = row + Mag_Ref_Offset_With_Tx_Class[ txClass ][ cbrIdx ][ 0 ]
        refCol = col + Mag_Ref_Offset_With_Tx_Class[ txClass ][ cbrIdx ][ 1 ]
        if ( refRow < txh &&
             refCol < (1 << bwl) ) {
            nbMag[ cbrIdx ] = Quant[ refRow * txw + refCol ]
        }
    }

    mag = Min( nbMag[0], COEFF_BASE_RANGE + NUM_BASE_LEVELS + 1 ) +
          Min( nbMag[1], COEFF_BASE_RANGE + NUM_BASE_LEVELS + 1 ) +
          Min( nbMag[2], COEFF_BASE_RANGE + NUM_BASE_LEVELS + 1 )
    mag = Min( ( mag + 1 ) >> 1, 6 )
    if ( pos == 0 ) {
        ctxIdxOffset = mag
    } else if (txClass == 0) {
        if ( ( row < 2 ) && ( col < 2 ) ) {
            ctxIdxOffset = mag + 7
        } else {
            ctxIdxOffset = mag + 14
        }
    } else {
        if ( txClass == 1 ) {
            if ( col == 0 ) {
                ctxIdxOffset = mag + 7
            } else {
                ctxIdxOffset = mag + 14
            }
        } else {
            if ( row == 0 ) {
                ctxIdxOffset = mag + 7
            } else {
                ctxIdxOffset = mag + 14
            }
        }
    }
}

golomb_length_bit: unused_init, L(1) = -1;
golomb_data_bit: unused_init, L(1) = -1;
angle_delta_y : unused_init, b_angle_delta_y() = -1;
angle_delta_uv : unused_init, b_angle_delta_uv() = -1;
color_index_map_y : unused_init, big_ns(PaletteSizeY) = -1;
color_index_map_uv : unused_init, big_ns(PaletteSizeUV) = -1;

partition : unused_init, b_partition() = {
    bsl = Mi_Width_Log2[ bSize ]
    above = AvailU && ( Mi_Width_Log2[ MiSizes[ r - 1 ][ c ] ] < bsl )
    left = AvailL && ( Mi_Height_Log2[ MiSizes[ r ][ c - 1 ] ] < bsl )
    ctxIdxOffset = left * 2 + above
}

split_or_horz : unused_init, b_split_or_horz() = {
    bsl = Mi_Width_Log2[ bSize ]
    above = AvailU && ( Mi_Width_Log2[ MiSizes[ r - 1 ][ c ] ] < bsl )
    left = AvailL && ( Mi_Height_Log2[ MiSizes[ r ][ c - 1 ] ] < bsl )
    ctxIdxOffset = left * 2 + above
}

split_or_vert : unused_init, b_split_or_vert() = {
    bsl = Mi_Width_Log2[ bSize ]
    above = AvailU && ( Mi_Width_Log2[ MiSizes[ r - 1 ][ c ] ] < bsl )
    left = AvailL && ( Mi_Height_Log2[ MiSizes[ r ][ c - 1 ] ] < bsl )
    ctxIdxOffset = left * 2 + above
}

use_intrabc : unused_init, b_use_intrabc() = -1;

intra_frame_y_mode : unused_init, b_intra_frame_y_mode() = -1;
uv_mode : unused_init, b_uv_mode() = YMode;
segment_id : unused_init, b_segment_id() = {
    if (prevUL < 0)
        ctxIdxOffset = 0
    else if ((prevUL == prevU) && (prevUL == prevL))
        ctxIdxOffset = 2
    else if ((prevUL == prevU) || (prevUL == prevL) || (prevU == prevL))
        ctxIdxOffset = 1
    else
        ctxIdxOffset = 0
}

skip_mode : unused_init, b_skip_mode() = {
    ctxIdxOffset = 0
    if ( AvailU )
        ctxIdxOffset += SkipModes[ MiRow - 1 ][ MiCol ]
    if ( AvailL )
        ctxIdxOffset += SkipModes[ MiRow ][ MiCol - 1 ]
}

skip : unused_init, b_skip() = {
    ctxIdxOffset = 0
    if ( AvailU )
        ctxIdxOffset += Skips[ MiRow - 1 ][ MiCol ]
    if ( AvailL )
        ctxIdxOffset += Skips[ MiRow ][ MiCol - 1 ]
}

delta_q_abs : unused_init, b_delta_q_abs() = -1;
delta_lf_abs : unused_init, b_delta_lf_abs() = -1;
tx_depth : unused_init, b_tx_depth() = {
    maxTxWidth = Tx_Width[ maxRectTxSize ]
    maxTxHeight = Tx_Height[ maxRectTxSize ]
#if COVER
    uint_0_18 tx_depth_maxRectTxSize
    tx_depth_maxRectTxSize = maxRectTxSize
    COVERCROSS(VALUE_TX_WIDTH, tx_depth_maxRectTxSize)
    COVERCROSS(VALUE_TX_HEIGHT, tx_depth_maxRectTxSize)
#endif // COVER

    if ( AvailU && IsInters[ MiRow - 1 ][ MiCol ] ) {
        aboveW = Block_Width[ MiSizes[ MiRow - 1 ][ MiCol ] ]
    } else if ( AvailU ) {
        aboveW = get_above_tx_width( MiRow, MiCol )
    } else {
        aboveW = 0
    }

    if ( AvailL && IsInters[ MiRow ][ MiCol - 1 ] ) {
        leftH = Block_Height[ MiSizes[ MiRow ][ MiCol - 1 ] ]
    } else if ( AvailL ) {
        leftH = get_left_tx_height( MiRow, MiCol )
    } else {
        leftH = 0
    }

    ctxIdxOffset = ( aboveW >= maxTxWidth ) + ( leftH >= maxTxHeight )
}

txfm_split : unused_init, b_txfm_split() = {
    above = get_above_tx_width( row, col ) < Tx_Width[ txSz ]
    left = get_left_tx_height( row, col ) < Tx_Height[ txSz ]
    size = Min(64, Max( Block_Width[ MiSize ], Block_Height[ MiSize ] ) )
    maxTxSz = find_tx_size( size, size )
#if COVER
    uint_0_18 txfm_split_txSz
    txfm_split_txSz = txSz
    COVERCROSS(VALUE_TX_SIZE_SQR_UP, txfm_split_txSz)
    COVERCROSS(VALUE_TX_WIDTH, txfm_split_txSz)
    COVERCROSS(VALUE_TX_HEIGHT, txfm_split_txSz)
#endif // COVER
    txSzSqrUp = Tx_Size_Sqr_Up[ txSz ]
    ctxIdxOffset = (txSzSqrUp != maxTxSz) * 3 +
                   (TX_SIZES - 1 - maxTxSz) * 6 + above + left
}

seg_id_predicted : unused_init, b_seg_id_predicted() = LeftSegPredContext[ MiRow ] + AboveSegPredContext[ MiCol ];
is_inter : unused_init, b_is_inter() = {
    if ( AvailU && AvailL )
        ctxIdxOffset = (LeftIntra && AboveIntra) ? 3 : LeftIntra || AboveIntra
    else if ( AvailU || AvailL )
        ctxIdxOffset = 2 * (AvailU ? AboveIntra : LeftIntra)
    else
        ctxIdxOffset = 0
}

use_filter_intra : unused_init, b_use_filter_intra() = MiSize;
filter_intra_mode : unused_init, b_filter_intra_mode() = -1;
y_mode : unused_init, b_y_mode() = {
  ctxIdxOffset = Size_Group[ MiSize ]
#if COVER
  uint_0_21 size_group_mi_size
  size_group_mi_size = MiSize
  COVERCROSS(VALUE_SIZE_GROUP, size_group_mi_size)
#endif // COVER
}

compound_mode : unused_init, b_compound_mode() = {
#if COVER
    uint_0_2 refMvCtx
    refMvCtx = RefMvContext >> 1
    uint_0_4 newMvCtx
    newMvCtx = Min(NewMvContext, COMP_NEWMV_CTXS - 1)
    COVERCROSS(CROSS_COMPOUND_MODE_CTX_MAP, refMvCtx, newMvCtx)
#endif // COVER
    ctxIdxOffset = Compound_Mode_Ctx_Map[ RefMvContext >> 1 ][ Min(NewMvContext, COMP_NEWMV_CTXS - 1) ]
}
new_mv : unused_init, b_new_mv() = -1;
zero_mv : unused_init, b_zero_mv() = -1;
ref_mv : unused_init, b_ref_mv() = -1;
drl_mode : unused_init, b_drl_mode() = DrlCtxStack[ idx ];
interp_filter : unused_init, b_interp_filter() = {
    ctxIdxOffset = ( ( dir & 1 ) * 2 + ( RefFrame[ 1 ] > INTRA_FRAME ) ) * 4
    leftType = 3
    aboveType = 3

    if ( AvailL ) {
        if ( RefFrames[ MiRow ][ MiCol - 1 ][ 0 ] == RefFrame[ 0 ] ||
             RefFrames[ MiRow ][ MiCol - 1 ][ 1 ] == RefFrame[ 0 ] )
            leftType = InterpFilters[ MiRow ] [ MiCol - 1 ][ dir ]
    }

    if ( AvailU ) {
        if ( RefFrames[ MiRow - 1 ][ MiCol ][ 0 ] == RefFrame[ 0 ] ||
             RefFrames[ MiRow - 1 ][ MiCol ][ 1 ] == RefFrame[ 0 ] )
            aboveType = InterpFilters[ MiRow - 1 ] [ MiCol ][ dir ]
    }

    if ( leftType == aboveType )
        ctxIdxOffset += leftType
    else if ( leftType == 3 )
        ctxIdxOffset += aboveType
    else if ( aboveType == 3 )
        ctxIdxOffset += leftType
    else
        ctxIdxOffset += 3
}

r_comp_mode : unused_init, b_comp_mode() = {
    if ( AvailU && AvailL ) {
        if ( AboveSingle && LeftSingle )
        ctxIdxOffset = check_backward( AboveRefFrame[ 0 ] )
            ^ check_backward( LeftRefFrame[ 0 ] )
        else if ( AboveSingle )
            ctxIdxOffset = 2 + ( check_backward( AboveRefFrame[ 0 ] ) || AboveIntra)
        else if ( LeftSingle )
            ctxIdxOffset = 2 + ( check_backward( LeftRefFrame[ 0 ] ) || LeftIntra)
        else
            ctxIdxOffset = 4
    } else if ( AvailU ) {
        if ( AboveSingle )
            ctxIdxOffset = check_backward( AboveRefFrame[ 0 ] )
        else
            ctxIdxOffset = 3
    } else if ( AvailL ) {
        if ( LeftSingle )
            ctxIdxOffset= check_backward( LeftRefFrame[ 0 ] )
        else
            ctxIdxOffset = 3
    } else {
        ctxIdxOffset = 1
    }
}

comp_ref_type : unused_init, b_comp_ref_type() = {
    above0 = AboveRefFrame[ 0 ]
    above1 = AboveRefFrame[ 1 ]
    left0 = LeftRefFrame[ 0 ]
    left1 = LeftRefFrame[ 1 ]
    aboveCompInter = AvailU && !AboveIntra && !AboveSingle
    leftCompInter = AvailL && !LeftIntra && !LeftSingle
    aboveUniComp = aboveCompInter && is_samedir_ref_pair(above0, above1)
    leftUniComp = leftCompInter && is_samedir_ref_pair(left0, left1)

    if (AvailU && !AboveIntra && AvailL && !LeftIntra) {
        samedir = is_samedir_ref_pair(above0, left0)

        if (!aboveCompInter && !leftCompInter) {
            ctxIdxOffset = 1 + 2 * samedir
        } else if (!aboveCompInter) {
            if (!leftUniComp)
                ctxIdxOffset = 1
            else
                ctxIdxOffset = 3 + samedir
        } else if (!leftCompInter) {
            if (!aboveUniComp)
                ctxIdxOffset = 1
            else
                ctxIdxOffset = 3 + samedir
        } else {
            if (!aboveUniComp && !leftUniComp)
                ctxIdxOffset = 0
            else if (!aboveUniComp || !leftUniComp)
                ctxIdxOffset = 2
            else
                ctxIdxOffset = 3 + ((above0 == BWDREF_FRAME) == (left0 == BWDREF_FRAME))
        }
    } else if (AvailU && AvailL) {
        if (aboveCompInter)
            ctxIdxOffset = 1 + 2 * aboveUniComp
        else if (leftCompInter)
            ctxIdxOffset = 1 + 2 * leftUniComp
        else
            ctxIdxOffset = 2
    } else if (aboveCompInter) {
        ctxIdxOffset = 4 * aboveUniComp
    } else if (leftCompInter) {
        ctxIdxOffset = 4 * leftUniComp
    } else {
        ctxIdxOffset = 2
    }
}

uni_comp_ref : unused_init, b_uni_comp_ref() = {
    fwdCount = count_refs( LAST_FRAME )
    fwdCount += count_refs( LAST2_FRAME )
    fwdCount += count_refs( LAST3_FRAME )
    fwdCount += count_refs( GOLDEN_FRAME )
    bwdCount = count_refs( BWDREF_FRAME )
    bwdCount += count_refs( ALTREF2_FRAME )
    bwdCount += count_refs( ALTREF_FRAME )
    ctxIdxOffset = ref_count_ctx( fwdCount, bwdCount )
}

uni_comp_ref_p1 : unused_init, b_uni_comp_ref_p1() = {
    last2Count = count_refs( LAST2_FRAME )
    last3GoldCount = count_refs( LAST3_FRAME ) + count_refs( GOLDEN_FRAME )
    ctxIdxOffset = ref_count_ctx( last2Count, last3GoldCount )
}

uni_comp_ref_p2 : unused_init, b_uni_comp_ref_p2() = {
    last3Count = count_refs( LAST3_FRAME )
    goldCount = count_refs( GOLDEN_FRAME )
    ctxIdxOffset = ref_count_ctx( last3Count, goldCount )
}

comp_ref : unused_init, b_comp_ref() = {
    last12Count = count_refs( LAST_FRAME ) + count_refs( LAST2_FRAME )
    last3GoldCount = count_refs( LAST3_FRAME ) + count_refs( GOLDEN_FRAME )
    ctxIdxOffset = ref_count_ctx( last12Count, last3GoldCount )
}

comp_ref_p1 : unused_init, b_comp_ref_p1() = {
    lastCount = count_refs( LAST_FRAME )
    last2Count = count_refs( LAST2_FRAME )
    ctxIdxOffset = ref_count_ctx( lastCount, last2Count )
}

comp_ref_p2 : unused_init, b_comp_ref_p2() = {
    last3Count = count_refs( LAST3_FRAME )
    goldCount = count_refs( GOLDEN_FRAME )
    ctxIdxOffset = ref_count_ctx( last3Count, goldCount )
}

comp_bwdref : unused_init, b_comp_bwdref() = {
    brfarf2Count = count_refs( BWDREF_FRAME ) + count_refs( ALTREF2_FRAME )
    arfCount = count_refs( ALTREF_FRAME )
    ctxIdxOffset = ref_count_ctx( brfarf2Count, arfCount )
}

comp_bwdref_p1 : unused_init, b_comp_bwdref_p1() = {
    brfCount = count_refs( BWDREF_FRAME )
    arf2Count = count_refs( ALTREF2_FRAME )
    ctxIdxOffset = ref_count_ctx( brfCount, arf2Count )
}

single_ref_p1 : unused_init, b_single_ref_p1() = {
    fwdCount = count_refs( LAST_FRAME )
    fwdCount += count_refs( LAST2_FRAME )
    fwdCount += count_refs( LAST3_FRAME )
    fwdCount += count_refs( GOLDEN_FRAME )
    bwdCount = count_refs( BWDREF_FRAME )
    bwdCount += count_refs( ALTREF2_FRAME )
    bwdCount += count_refs( ALTREF_FRAME )
    ctxIdxOffset = ref_count_ctx( fwdCount, bwdCount )
}

single_ref_p2 : unused_init, b_single_ref_p2() = {
    brfarf2Count = count_refs( BWDREF_FRAME ) + count_refs( ALTREF2_FRAME )
    arfCount = count_refs( ALTREF_FRAME )
    ctxIdxOffset = ref_count_ctx( brfarf2Count, arfCount )
}

single_ref_p3 : unused_init, b_single_ref_p3() = {
    last12Count = count_refs( LAST_FRAME ) + count_refs( LAST2_FRAME )
    last3GoldCount = count_refs( LAST3_FRAME ) + count_refs( GOLDEN_FRAME )
    ctxIdxOffset = ref_count_ctx( last12Count, last3GoldCount )
}

single_ref_p4 : unused_init, b_single_ref_p4() = {
    lastCount = count_refs( LAST_FRAME )
    last2Count = count_refs( LAST2_FRAME )
    ctxIdxOffset = ref_count_ctx( lastCount, last2Count )
}

single_ref_p5 : unused_init, b_single_ref_p5() = {
    last3Count = count_refs( LAST3_FRAME )
    goldCount = count_refs( GOLDEN_FRAME )
    ctxIdxOffset = ref_count_ctx( last3Count, goldCount )
}

single_ref_p6 : unused_init, b_single_ref_p6() = {
    brfCount = count_refs( BWDREF_FRAME )
    arf2Count = count_refs( ALTREF2_FRAME )
    ctxIdxOffset = ref_count_ctx( brfCount, arf2Count )
}

use_obmc : unused_init, b_use_obmc() = -1;
motion_mode : unused_init, b_motion_mode() = -1;
interintra : unused_init, b_interintra() = Size_Group[ MiSize ]-1;
interintra_mode : unused_init, b_interintra_mode() = Size_Group[ MiSize ]-1;
wedge_interintra : unused_init, b_wedge_interintra() = -1;

comp_group_idx : unused_init, b_comp_group_idx() = {
    ctxIdxOffset = 0
    if ( AvailU ) {
        if ( !AboveSingle )
            ctxIdxOffset += CompGroupIdxs[ MiRow - 1 ][ MiCol ]
        else if (AboveRefFrame[ 0 ] == ALTREF_FRAME)
            ctxIdxOffset += 3
    }
    if ( AvailL ) {
        if ( !LeftSingle )
            ctxIdxOffset += CompGroupIdxs[ MiRow ][ MiCol - 1 ]
        else if (LeftRefFrame[ 0 ] == ALTREF_FRAME)
            ctxIdxOffset += 3
    }
    ctxIdxOffset = Min( 5, ctxIdxOffset )
}

compound_idx : unused_init, b_compound_idx() = {
    fwd = Abs( get_relative_dist( OrderHints[ RefFrame[ 0 ] ], OrderHint ) )
    bck = Abs( get_relative_dist( OrderHints[ RefFrame[ 1 ] ], OrderHint ) )
    ctxIdxOffset = ( fwd == bck ) ? 3 : 0
    if ( AvailU ) {
        if ( !AboveSingle )
            ctxIdxOffset += CompoundIdxs[ MiRow - 1 ][ MiCol ]
        else if (AboveRefFrame[ 0 ] == ALTREF_FRAME)
            ctxIdxOffset++
    }
    if ( AvailL ) {
        if ( !LeftSingle )
            ctxIdxOffset += CompoundIdxs[ MiRow ][ MiCol - 1 ]
        else if (LeftRefFrame[ 0 ] == ALTREF_FRAME)
            ctxIdxOffset++
    }
}

r_compound_type : unused_init, b_compound_type() = -1;
mv_joint : unused_init, b_mv_joint() = -1;
mv_sign : unused_init, b_mv_sign() = -1;
mv_class : unused_init, b_mv_class() = -1;
mv_class0_bit : unused_init, b_mv_class0_bit() = -1;
mv_class0_fr : unused_init, b_mv_class0_fr() = -1;
mv_class0_hp : unused_init, b_mv_class0_hp() = -1;
mv_bit : unused_init, b_mv_bit() = -1;
mv_fr : unused_init, b_mv_fr() = -1;
mv_hp : unused_init, b_mv_hp() = -1;

cfl_alpha_signs : unused_init, b_cfl_alpha_signs() = -1;
cfl_alpha_u : unused_init, b_cfl_alpha_u() = (signU - 1) * 3 + signV;
cfl_alpha_v : unused_init, b_cfl_alpha_v() = (signV - 1) * 3 + signU;

has_palette_y : unused_init, b_has_palette_y() = {
    ctxIdxOffset = 0
    if ( AvailU && PaletteSizes[ 0 ][ MiRow - 1 ][ MiCol ] > 0 )
        ctxIdxOffset += 1
    if ( AvailL && PaletteSizes[ 0 ][ MiRow ][ MiCol - 1 ] > 0 )
        ctxIdxOffset += 1
}

palette_size_y_minus_2 : unused_init, b_palette_size_y_minus_2() = -1;

has_palette_uv : unused_init, b_has_palette_uv() = ( PaletteSizeY > 0 ) ? 1 : 0;

palette_size_uv_minus_2 : unused_init, b_palette_size_uv_minus_2() = -1;
inter_tx_type : unused_init, b_inter_tx_type() = -1;
intra_tx_type : unused_init, b_intra_tx_type() = -1;
palette_color_idx_y : unused_init, b_palette_color_idx_y() = {
  ctxIdxOffset = Palette_Color_Context[ ColorContextHash ]
#if COVER
  uint_0_8 palette_y_ColorContextHash
  palette_y_ColorContextHash = ColorContextHash
  COVERCROSS(VALUE_PALETTE_COLOR_CONTEXT, palette_y_ColorContextHash)
#endif // COVER
}
palette_color_idx_uv : unused_init, b_palette_color_idx_uv() = {
  ctxIdxOffset = Palette_Color_Context[ ColorContextHash ]
#if COVER
  uint_0_8 palette_uv_ColorContextHash
  palette_uv_ColorContextHash = ColorContextHash
  COVERCROSS(VALUE_PALETTE_COLOR_CONTEXT, palette_uv_ColorContextHash)
#endif // COVER
}

use_wiener : unused_init, b_use_wiener() = -1;
use_sgrproj : unused_init, b_use_sgrproj() = -1;
restoration_type : unused_init, b_restoration_type() = -1;
lr_sgr_set: unused_init, L(SGRPROJ_PARAMS_BITS) = -1;
subexp_unif_bools: unused_init, big_ns(numSyms - mk) = -1;
subexp_more_bools: unused_init, L(1) = -1;
subexp_bools: unused_init, L(b2) = -1;
width_in_sbs_minus_1: unused_init, ns(maxWidth) = -1;
height_in_sbs_minus_1: unused_init, ns(maxHeight) = -1;
subexp_final_bits: unused_init, ns(numSyms - mk) = -1;
