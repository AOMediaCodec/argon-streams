################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

from bitstream import *
from collections import defaultdict
import pdb,cPickle
import os,md5,copy

class Object(object):
    pass

def cdivide(a,b):
  """a/b with rounding towards zero"""
  r=abs(a)/abs(b);
  if (a<0 and b>=0) or (b<0 and a>=0):
    return -r
  return r

def D2():
    return defaultdict(dict)
def D3():
    return defaultdict(D2)
def D4():
    return defaultdict(D3)
def D5():
    return defaultdict(D4)
def D6():
    return defaultdict(D5)
def D7():
    return defaultdict(D6)

class checked_dict(dict):
  """Same as a dictionary, except we check the bounds of the array when items are written"""
  def __init__(self,dim):
    self.dim = dim
    dict.__init__(self)

  def __setitem__(self, key, val):
    assert 0<=key<self.dim
    return dict.__setitem__(self, key, val)

class checked_defaultdict(defaultdict):
  """Produces a hierarchy of dictionaries that check bounds access."""
  def __init__(self,dims):
    N=len(dims)
    assert N>=2
    self.dim = dims[0]
    self.dims = dims
    def factory():
      if dims[1] is None:
        return None
      elif N==2:
        return checked_dict(dims[1])
      else:
        return checked_defaultdict(dims[1:])
    defaultdict.__init__(self,factory)

  def __getitem__(self, key):
    assert 0<=key<self.dim
    return defaultdict.__getitem__(self, key)

  def __setitem__(self, key, val):
    assert 0<=key<self.dim
    return defaultdict.__setitem__(self, key, val)

  def __deepcopy__(self,memo):
    C = checked_defaultdict(self.dims)
    for key,value in self.items():
      C[key] = copy.deepcopy(value)
    return C
