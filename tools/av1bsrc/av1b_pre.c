/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "arrays.h"

#define PRINT_BIT_READS 0

#include <x86intrin.h>
#define ALIGN16 __attribute__((aligned (16)))

typedef unsigned char uint1;
typedef unsigned char uint2;
typedef unsigned char uint3;
typedef unsigned char uint4;
typedef unsigned char uint5;
typedef unsigned char uint6;
typedef unsigned char uint7;
typedef unsigned char uint8;
typedef unsigned short uint9;
typedef unsigned short uint10;
typedef unsigned short uint11;
typedef unsigned short uint12;
typedef unsigned short uint13;
typedef unsigned short uint14;
typedef unsigned short uint15;
typedef unsigned short uint16;
typedef unsigned int uint17;
typedef unsigned int uint19;
typedef unsigned int uint22;
typedef unsigned int uint32;
typedef signed char int1;
typedef signed char int2;
typedef signed char int3;
typedef signed char int4;
typedef signed char int5;
typedef signed char int6;
typedef signed char int7;
typedef signed char int8;
typedef short int9;
typedef short int10;
typedef short int12;
typedef short int16;
typedef int int13;
typedef int int14;
typedef int int20;
typedef int int24;
typedef int int32;
typedef long long int64;
typedef unsigned long long uint64;
typedef unsigned long long uint33;
typedef __m128i int16x4;  // We usse 128bit registers for all internal parallel activites at the moment
typedef __m128i int16x8;
typedef __m128i int32x4;
typedef uint3 uint_4_6;
typedef uint1 uint_0_1;
typedef uint2 uint_0_2;
typedef uint2 uint_0_3;
typedef uint3 uint_0_4;
typedef uint3 uint_1_4;
typedef uint3 uint_3_4;
typedef uint3 uint_0_5;
typedef uint3 uint_1_5;
typedef uint3 uint_0_6;
typedef uint3 uint_0_7;
typedef uint3 uint_1_7;
typedef uint4 uint_0_8;
typedef uint4 uint_0_9;
typedef uint4 uint_0_10;
typedef uint4 uint_0_11;
typedef uint4 uint_0_12;
typedef uint4 uint_0_13;
typedef uint4 uint_0_14;
typedef uint4 uint_10_13;
typedef uint5 uint_8_16;
typedef uint5 uint_0_28;
typedef uint6 uint_0_41;
typedef uint6 uint_0_42;
typedef uint7 uint_0_63;
typedef uint7 uint_0_64;
typedef uint7 uint_2_64;
typedef uint8 uint_0_127;
typedef uint8 uint_1_128;
typedef uint8 uint_1_255;
typedef uint8 uint_0_89;
typedef uint8 uint_0_95;
typedef uint8 uint_0_192;
typedef uint8 uint_0_212;
typedef uint8 uint_0_253;
typedef uint8 uint_0_254;
typedef uint8 uint_0_255;
typedef uint9 uint_0_256;
typedef uint11 uint_0_1024;
typedef uint11 uint_0_2047;
typedef uint12 uint_0_3343;
typedef uint15 uint_0_32640;
typedef uint6 uint_0_32;
typedef uint5 uint_0_31;
typedef uint15 uint_0_32766;
typedef uint15 uint_0_16450;
typedef uint15 uint_0_16384;
typedef uint19 uint_0_262144;
typedef uint8 uint_3_255;
typedef uint9 uint_0_261;
typedef uint8 uint_2_128;
typedef uint5 uint_0_26;
typedef uint5 uint_0_20;
typedef uint4 uint_4_12;
typedef uint4 uint_0_12;
typedef uint4 uint_0_15;
typedef uint5 uint_0_16;
typedef uint5 uint_0_17;
typedef uint5 uint_0_18;
typedef uint5 uint_0_21;
typedef uint5 uint_0_24;
typedef uint5 uint_0_27;
typedef uint4 uint_1_8;
typedef uint4 uint_0_9;
typedef uint4 uint_0_8;
typedef uint4 uint_2_8;
typedef uint5 uint_1_16;
typedef uint6 uint_1_32;
typedef uint3 uint_1_6;
typedef uint2 uint_1_3;
typedef uint32 uint_1_25;
typedef uint32 uint_2_17;

typedef int *intpointer;
typedef int3 *int3pointer;
typedef int8 *int8pointer;
typedef int7 *int7pointer;
typedef uint8 *uint8pointer;
typedef uint32 *uint32pointer;
typedef int32 *int32pointer;
typedef int64 *int64pointer;
typedef int16 *int16pointer;
typedef uint16 *uint16pointer;
typedef uint12 *uint12pointer;

typedef struct toplevel_s TOPLEVEL_T;

static inline int64 builtin_Max(int64 a, int64 b) {
    return a<b?b:a;
}
static inline int64 builtin_Abs(int64 a) {
    return a<0?-a:a;
}
static inline int builtin_Sign(int64 a) {
    return a<=0?a<0?-1:0:1;
}
static inline int64 builtin_Min(int64 a, int64 b) {
    return a<b?a:b;
}
static inline int64 builtin_CeilDiv(int64 a, int64 b) {
    return (a + b - 1)/b;
}
#define builtin_Clip3(x,y,z) builtin_Max(builtin_Min(y,z),x)
//#define builtin_Clip1Y(x) builtin_Clip3(0,255,x)
//#define builtin_Clip1C(x) builtin_Clip3(0,255,x)
static inline int builtin_Log2(int a) {
  int b=0;
  while ((1<<b)<a)
      b+=1;
  return b;
}

static inline int builtin_CeilLog2( int x ) {
  if (x < 2)
    return 0;
  int i = 1;
  int p = 2;
  while (p < x) {
    i++;
    p = p << 1;
  }
  return i;
}

static inline int builtin_FloorLog2( int64 x ) {
    int s = 0;
    while (x != 0) {
        x = x >> 1;
        s++;
    }
    return s - 1;
}

#define builtin_more_rbsp_trailing_data() bitstream_more_rbsp_trailing_data(b)
#define builtin_needs_byte_extensions() bitstream_needs_byte_extensions(b)
#define builtin_next_bits(n) bitstream_next_bits(b,n)
#define builtin_count_data_decode(pos) bitstream_count_data_decode(b,pos)
#define builtin_current_position() bitstream_current_position(b)
#define builtin_current_bitposition() bitstream_current_bitposition(b)
#define builtin_more_rbsp_data() bitstream_more_rbsp_data(b)
#define builtin_payload_extension_present(sp,ps) bitstream_payload_extension_present(b,sp,ps)
#define builtin_payload_extension_size(sp,ps) bitstream_payload_extension_size(b,sp,ps)
#define builtin_byte_aligned() bitstream_byte_aligned(b)
#define builtin_set_pos(pos) bitstream_set_pos(b,pos)
#define builtin_set_chunk_size(size) bitstream_set_chunk_size(b,size)
#define builtin_move_to_next_chunk()  bitstream_move_to_next_chunk(b)
#define builtin_peek_last_bytes(size,out) bitstream_peek_last_bytes(b,size,out)
#if CONFIG_RAWBITS
# define builtin_readbits_from_chunk_end(num) bitstream_readbits_from_chunk_end(b,num)
#endif // CONFIG_RAWBITS

#define builtin_profile(a,b) profile_func(a,b)
#define builtin_profile_disable() profile_disable_func()
#define builtin_profile_enable() profile_enable_func()
#define hevc_parse_assert(a,b,x) assert(x)
//#define hevc_parse_assert(a,b,x)
#define hevc_parse_RangeStep(a,b,x,low,high,step)
#define hevc_parse_Range(a,b,x,low,high)

void decode_log(int r,int off,int s,int v);
void validate(int x);
void video_output (TOPLEVEL_T *b, int bps, int w, int h,
                   int subw, int subh, int mono);
void profile_output(const char *profXmlFile,
                    const char *profPyFile,
                    const char *firstStreamPath,
#if USE_DB
                    const char *dbURL, int subset, int seed,
#endif
                    size_t size, int outputAllCoveringStreams);
void profile_report(void);
long long profile_func(unsigned idx, long long val);
void profile_disable_func();
void profile_enable_func();

int debugMode=0;

int cabac_bins_count = 0;
void daala_stop()
{
   int iii=0;
   iii++;
   //   printf("stop\n");
   (void)iii;
}

void debug_cabac_daala_bits(int bit)
{
  if (cabac_bins_count>=20) daala_stop();
  cabac_bins_count++;
}

void debug_cabac_daala_symbol(int symbol,int nsyms,int rng,int bits,int Fh,int Fl,int buf)
{
  if (cabac_bins_count>=20) daala_stop();
  cabac_bins_count++;
}

void debug_cabac_bins(int binVal, int startRange, int startVal, int prob)
{
  if (cabac_bins_count>=20) daala_stop();
  cabac_bins_count++;
}

int mv_count = 0;
void mv_stop()
{
  int jjj=0;
  jjj++;
  (void)jjj;
}

void debug_mv(int i, int is_compound)
{
  if (i && !is_compound)
    return;
  if (mv_count>=6) mv_stop();
  mv_count++;
}

void recon_stop()
{
  int kkk=0;
  kkk++;
  (void)kkk;
}

enum {
  PREDICTION = 0,
  TRANSFORM = 1,
  ABOVE = 2,
  LEFT = 3,
  OBMC_PRE = 4,
  OBMC_POST = 5,
};


static unsigned int recon_count = 0;
void log_prediction(int phase) {
  if (recon_count >= 208) recon_stop();

  if (phase==TRANSFORM) {
    recon_count+=1;
  }
}

void log_intra_neighbours() {
  //  if (recon_count >= 252) recon_stop();
}

static unsigned int loop_count = 0;
void loop_stop() {
  int aaa=0;
  aaa++;
  (void)aaa;
}

void log_loop() {
  if (loop_count >= 244) loop_stop();
  loop_count+=1; // once for each plane
}

static unsigned int cdef_log_count = 0;
void cdef_stop() {
  int aaa=0;
  aaa++;
  (void)aaa;
}

void log_cdef(int nplanes) {
  if (cdef_log_count >= 244) cdef_stop();
  cdef_log_count+=nplanes; // once for each plane
}

int builtin_CheckStride (int x, int y, int stride, int size) {
  int offset = y * stride + x;

  assert (x < stride);
  assert (offset < size);

  return offset;
}

template <typename T, size_t D>
static void builtin_SetArray (flat_array<T, D> &array, int value)
{
    array.set (static_cast<T> (value));
}

template <typename T>
static void builtin_Copy2D (flat_subarray <T, 2>        dst,
                            const flat_subarray <T, 2>  src,
                            int                         width,
                            int                         height)
{
    assert (width >= 0 && height >= 0);
    const std::array <size_t, 2> sizes = { (size_t) height, (size_t) width };
    dst.take (src.const_box (sizes));
}

// Local Variables:
// mode: c++
// End:
