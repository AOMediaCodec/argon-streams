/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#if USE_PROFILE

#include "customers.h"

// Using ifdefs for PROF_SHOW_* is a bit painful. Here are macros to
// make it a bit nicer.
#ifdef PROF_SHOW_CLIENT_A
#  define CLIENT_A_LIST_ENTRY(x) x,
#else
#  define CLIENT_A_LIST_ENTRY(x)
#endif
#ifdef PROF_SHOW_CLIENT_B
#  define CLIENT_B_LIST_ENTRY(x) x,
#else
#  define CLIENT_B_LIST_ENTRY(x)
#endif
#ifdef PROF_SHOW_CLIENT_D
# define CLIENT_D_LIST_ENTRY(x) x,
#else
#  define CLIENT_D_LIST_ENTRY(x)
#endif
#ifdef PROF_SHOW_CLIENT_C
#  define CLIENT_C_LIST_ENTRY(x) x,
#else
#  define CLIENT_C_LIST_ENTRY(x)
#endif


enum {
  VC_PROFILE0,
  VC_PROFILE1,
  VC_PROFILE2,

  CLIENT_A_LIST_ENTRY(VC_CLIENT_A)
  CLIENT_A_LIST_ENTRY(VC_CLIENT_A_LEVEL5)

  CLIENT_B_LIST_ENTRY(VC_CLIENT_B)
  CLIENT_C_LIST_ENTRY(VC_CLIENT_C)
  CLIENT_D_LIST_ENTRY(VC_CLIENT_D)

  VC_LEVEL5,   // Things supported in level 5.3 but not level 2.0
  VC_LEVEL6,   // Things not supported below level 6.0
  VC_MAXLEVEL, // All Core items, level 6, and seq_level==31

  VC_LARGE_SCALE_TILES,
  VC_NON_ANNEX_B,

  VC_IMPOSSIBLE,

  //Insert new entries here
  VC_ENTRY_COUNT
  //Don't insert anything here!
};

// We don't want to compile in customer-specific visibility categories
// unless required. Unfortunately, this would make a right mess of the
// profile_views table below. Our cunning trick is to define any
// unused VC_<WHATEVER> as VC_IMPOSSIBLE. That way, instead of saying
// "VC_CLIENT_A is invisible", we are saying "VC_IMPOSSIBLE is
// invisible". This is, of course, true for everything except the
// allopt view.
#ifndef PROF_SHOW_CLIENT_A
#  define VC_CLIENT_A        VC_IMPOSSIBLE
#  define VC_CLIENT_A_LEVEL5 VC_IMPOSSIBLE
#endif
#ifndef PROF_SHOW_CLIENT_B
#  define VC_CLIENT_B         VC_IMPOSSIBLE
#endif
#ifndef PROF_SHOW_CLIENT_C
#  define VC_CLIENT_C         VC_IMPOSSIBLE
#endif
#ifndef PROF_SHOW_CLIENT_D
#  define VC_CLIENT_D         VC_IMPOSSIBLE
#endif

const char *profile_visibility_category_names[VC_ENTRY_COUNT] = {
  "PROFILE0",
  "PROFILE1",
  "PROFILE2",
  CLIENT_A_LIST_ENTRY("CLIENT_A")
  CLIENT_A_LIST_ENTRY("CLIENT_A_LEVEL5")
  CLIENT_B_LIST_ENTRY("CLIENT_B")
  CLIENT_C_LIST_ENTRY("CLIENT_C")
  CLIENT_D_LIST_ENTRY("CLIENT_D")
  "LEVEL5",
  "LEVEL6",
  "MAXLEVEL",
  "LARGE_SCALE_TILES",
  "NON_ANNEX_B",
  "IMPOSSIBLE"
};

enum {
  VIEW_PROFILE0_LEVEL2,
  CLIENT_A_LIST_ENTRY(VIEW_PROFILE0_CLIENT_A_LEVEL2)

  VIEW_PROFILE1_LEVEL2,
  VIEW_PROFILE2_LEVEL2,

  VIEW_PROFILE0_LEVEL5,
  CLIENT_A_LIST_ENTRY(VIEW_PROFILE0_CLIENT_A_LEVEL5)
  CLIENT_C_LIST_ENTRY(VIEW_PROFILE0_CLIENT_C_LEVEL5)
  VIEW_PROFILE1_LEVEL5,
  VIEW_PROFILE2_LEVEL5,

  VIEW_PROFILE0_LEVEL6,
  VIEW_PROFILE1_LEVEL6,
  VIEW_PROFILE2_LEVEL6,

  VIEW_PROFILE0_CORE,
  CLIENT_A_LIST_ENTRY(VIEW_PROFILE0_CLIENT_A_CORE)
  CLIENT_B_LIST_ENTRY(VIEW_PROFILE0_CLIENT_B_CORE)
  CLIENT_C_LIST_ENTRY(VIEW_PROFILE0_CLIENT_C_CORE)
  CLIENT_D_LIST_ENTRY(VIEW_PROFILE0_CLIENT_D_CORE)

  VIEW_PROFILE1_CORE,
  VIEW_PROFILE2_CORE,

  VIEW_PROFILE0_LARGE_SCALE_TILES,
  CLIENT_C_LIST_ENTRY(VIEW_PROFILE0_CLIENT_C_LARGE_SCALE_TILES)
  VIEW_PROFILE1_LARGE_SCALE_TILES,
  VIEW_PROFILE2_LARGE_SCALE_TILES,

  VIEW_PROFILE0_NON_ANNEX_B,
  CLIENT_A_LIST_ENTRY(VIEW_PROFILE0_CLIENT_A_NON_ANNEX_B)
  CLIENT_C_LIST_ENTRY(VIEW_PROFILE0_CLIENT_C_NON_ANNEX_B)
  VIEW_PROFILE1_NON_ANNEX_B,
  VIEW_PROFILE2_NON_ANNEX_B,

  VIEW_PROFILE0,
  CLIENT_A_LIST_ENTRY(VIEW_PROFILE0_CLIENT_A)
  CLIENT_B_LIST_ENTRY(VIEW_PROFILE0_CLIENT_B)
  CLIENT_C_LIST_ENTRY(VIEW_PROFILE0_CLIENT_C)
  CLIENT_D_LIST_ENTRY(VIEW_PROFILE0_CLIENT_D)

  VIEW_PROFILE1,
  VIEW_PROFILE2,
  VIEW_POSSIBLE,
  CLIENT_A_LIST_ENTRY(VIEW_POSSIBLE_CLIENT_A)
  VIEW_ALL,

  //Insert new entries here
  VIEW_ENTRY_COUNT
  //Don't insert anything here!
};

static int excluded_views[] = {
#if RELEASE
  VIEW_PROFILE0_LEVEL2,
  VIEW_PROFILE1_LEVEL2,
  VIEW_PROFILE2_LEVEL2,
  VIEW_PROFILE0_LEVEL2,
  VIEW_PROFILE1_LEVEL2,
  VIEW_PROFILE2_LEVEL2,
  VIEW_PROFILE0_LEVEL5,
  VIEW_PROFILE1_LEVEL5,
  VIEW_PROFILE2_LEVEL5,
  VIEW_PROFILE0_LEVEL6,
  VIEW_PROFILE1_LEVEL6,
  VIEW_PROFILE2_LEVEL6,
  CLIENT_A_LIST_ENTRY(VIEW_PROFILE0_CLIENT_A_LEVEL2)
  CLIENT_A_LIST_ENTRY(VIEW_PROFILE0_CLIENT_A_LEVEL5)
  CLIENT_A_LIST_ENTRY(VIEW_PROFILE0_CLIENT_A_CORE)
  CLIENT_A_LIST_ENTRY(VIEW_PROFILE0_CLIENT_A_NON_ANNEX_B)
#endif
  -1
};

struct CoverageReportView {
  const char   *id;
  const char   *displayName;
  const int8_t  invisibleCategories[VC_ENTRY_COUNT+6];
  const char   *rangeVisibilityType;
};

const static struct CoverageReportView profile_views[VIEW_ENTRY_COUNT] = {
  { "profile0_level2opt",         "Profile 0 (up to level 2.0, core only)",   {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D, VC_LEVEL5, VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0" },
#ifdef PROF_SHOW_CLIENT_A
  { "profile0_level2opt_client_a","ClientA - Profile 0 (up to level 2.0, core only)", {     VC_PROFILE1, VC_PROFILE2,              VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D, VC_LEVEL5, VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_A" },
#endif
  { "profile1_level2opt",         "Profile 1 (up to level 2.0, core only)",   { VC_PROFILE0,              VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D, VC_LEVEL5, VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE1" },
  { "profile2_level2opt",         "Profile 2 (up to level 2.0, core only)",   { VC_PROFILE0, VC_PROFILE1,              VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D, VC_LEVEL5, VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "ALL" },
  { "profile0_level5opt",         "Profile 0 (up to level 5.3, core only)",   {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,            VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0" },
#ifdef PROF_SHOW_CLIENT_A
  { "profile0_level5opt_client_a","ClientA - Profile 0 (up to mod. level 5.3, core only)", {VC_PROFILE1, VC_PROFILE2,                                  VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D, VC_LEVEL5, VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_A" },
#endif
#ifdef PROF_SHOW_CLIENT_C
  { "profile0_level5opt_client_c", "ClientC - Profile 0 (up to level 5.3, core only)", {      VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B,             VC_CLIENT_D, VC_LEVEL5, VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_C" },
#endif
  { "profile1_level5opt",         "Profile 1 (up to level 5.3, core only)",   { VC_PROFILE0,              VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,            VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE1" },
  { "profile2_level5opt",         "Profile 2 (up to level 5.3, core only)",   { VC_PROFILE0, VC_PROFILE1,              VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,            VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "ALL" },
  { "profile0_level6opt",         "Profile 0 (up to level 6.3, core only)",   {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                       VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0" },
  { "profile1_level6opt",         "Profile 1 (up to level 6.3, core only)",   { VC_PROFILE0,              VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                       VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE1" },
  { "profile2_level6opt",         "Profile 2 (up to level 6.3, core only)",   { VC_PROFILE0, VC_PROFILE1,              VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                       VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "ALL" },
  { "profile0_coreopt",           "Profile 0 (core only)",                    {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                    VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0" },
#ifdef PROF_SHOW_CLIENT_A
  { "profile0_coreopt_client_a",  "ClientA - Profile 0 (core only)",         {              VC_PROFILE1, VC_PROFILE2,                                  VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D, VC_LEVEL5, VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_A" },
#endif
#ifdef PROF_SHOW_CLIENT_B
  { "profile0_coreopt_client_b",   "ClientB - Profile 0 (core only)",          {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5,             VC_CLIENT_C, VC_CLIENT_D,                                    VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_B" },
#endif
#ifdef PROF_SHOW_CLIENT_C
  { "profile0_coreopt_client_c",   "ClientC - Profile 0 (core only)",          {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B,             VC_CLIENT_D,                                    VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_C" },
#endif
#ifdef PROF_SHOW_CLIENT_D
  { "profile0_coreopt_client_d",  "ClientD - Profile 0 (core only)",         {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C,                                    VC_MAXLEVEL, VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0" },
#endif
  { "profile1_coreopt",           "Profile 1 (core only)",                    { VC_PROFILE0,              VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                    VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE1" },
  { "profile2_coreopt",           "Profile 2 (core only)",                    { VC_PROFILE0, VC_PROFILE1,              VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                    VC_LARGE_SCALE_TILES, VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "ALL" },
  { "profile0_lstopt",            "Profile 0 (core + large scale tiles)",     {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                                          VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0" },
#ifdef PROF_SHOW_CLIENT_C
{ "profile0_lstopt_client_c",      "ClientC - Profile 0 (core + large scale tiles)", {        VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B,             VC_CLIENT_D,                                                          VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_C" },
#endif
  { "profile1_lstopt",            "Profile 1 (core + large scale tiles)",     { VC_PROFILE0,              VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                                          VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "PROFILE1" },
  { "profile2_lstopt",            "Profile 2 (core + large scale tiles)",     { VC_PROFILE0, VC_PROFILE1,              VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                                          VC_NON_ANNEX_B, VC_IMPOSSIBLE, -1 }, "ALL" },
  { "profile0_non_annexbopt",     "Profile 0 (core + non-Annex B)",           {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                    VC_LARGE_SCALE_TILES,                 VC_IMPOSSIBLE, -1 }, "PROFILE0" },
#ifdef PROF_SHOW_CLIENT_A
  { "profile0_non_annexbopt_client_a","ClientA - Profile 0 (core + non-Annex B)",  {        VC_PROFILE1, VC_PROFILE2,              VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D, VC_LEVEL5, VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES,                 VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_A" },
#endif
#ifdef PROF_SHOW_CLIENT_C
  { "profile0_non_annexbopt_client_c", "ClientC - Profile 0 (core + non-Annex B)", {          VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B,             VC_CLIENT_D,                                    VC_LARGE_SCALE_TILES,                 VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_C" },
#endif
  { "profile1_non_annexbopt",     "Profile 1 (core + non-Annex B)",           { VC_PROFILE0,              VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                    VC_LARGE_SCALE_TILES,                 VC_IMPOSSIBLE, -1 }, "PROFILE1" },
  { "profile2_non_annexbopt",     "Profile 2 (core + non-Annex B)",           { VC_PROFILE0, VC_PROFILE1,              VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                    VC_LARGE_SCALE_TILES,                 VC_IMPOSSIBLE, -1 }, "ALL" },
  { "profile0opt",                "Profile 0 (all)",                          {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                                                          VC_IMPOSSIBLE, -1 }, "PROFILE0" },
#ifdef PROF_SHOW_CLIENT_A
  { "profile0opt_client_a",       "ClientA - Profile 0 (all)",               {              VC_PROFILE1, VC_PROFILE2,                                  VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D, VC_LEVEL5, VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES,                 VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_A" },
#endif
#ifdef PROF_SHOW_CLIENT_B
  { "profile0opt_client_b",        "ClientB - Profile 0 (all)",                {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5,             VC_CLIENT_C, VC_CLIENT_D,                                    VC_LARGE_SCALE_TILES,                 VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_B" },
#endif
#ifdef PROF_SHOW_CLIENT_C
  { "profile0opt_client_c",        "ClientC - Profile 0 (all)",                {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B,             VC_CLIENT_D,                                    VC_LARGE_SCALE_TILES,                 VC_IMPOSSIBLE, -1 }, "PROFILE0_CLIENT_C" },
#endif
#ifdef PROF_SHOW_CLIENT_D
  { "profile0opt_client_d",       "ClientD - Profile 0 (all)",               {              VC_PROFILE1, VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C,                                    VC_MAXLEVEL, VC_LARGE_SCALE_TILES,                 VC_IMPOSSIBLE, -1 }, "PROFILE0" },
#endif
  { "profile1opt",                "Profile 1 (all)",                          { VC_PROFILE0,              VC_PROFILE2, VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                                                          VC_IMPOSSIBLE, -1 }, "PROFILE1" },
  { "profile2opt",                "Profile 2 (all)",                          { VC_PROFILE0, VC_PROFILE1,              VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                                                          VC_IMPOSSIBLE, -1 }, "ALL" },
  { "possopt",                    "All possible cases",                       {                                        VC_CLIENT_A, VC_CLIENT_A_LEVEL5, VC_CLIENT_B, VC_CLIENT_C, VC_CLIENT_D,                                                                          VC_IMPOSSIBLE, -1 }, "ALL" },
#ifdef PROF_SHOW_CLIENT_A
  { "possopt_client_a",            "ClientA - All possible cases",                         {VC_PROFILE1, VC_PROFILE2,                                                                       VC_LEVEL5, VC_LEVEL6, VC_MAXLEVEL, VC_LARGE_SCALE_TILES,                 VC_IMPOSSIBLE, -1 }, "ALL" },
#endif
  { "allopt" ,                    "All cases (including impossible cases)",   {                                                                                                                                                                                                      -1 }, "ALL" },
};

void findCoveredValues(int count, const char *names[], int *results_out);

const char *getInitialView() {
  const char *valuesToFind [] = {
    "seq_profile"
  };
  int valuesFound[1*3];
  findCoveredValues(1, valuesToFind, valuesFound);

  // Check we've found the value we're after. If not, return "all possible cases"
  if (! valuesFound[0])
      return "possopt";

  int max_profile = valuesFound[2];
  if (max_profile == 0) return "profile0opt";
  if (max_profile == 1) return "profile1opt";

  assert (max_profile == 2);
  return "profile2opt";
}
#endif // USE_PROFILE
