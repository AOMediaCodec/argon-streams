/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

//#line 2 "av1b_mid.c"

struct toplevel_s {
  uint8 *data;  // Source of encoded data
  int len; // Number of bytes of encoded data
  int bitpos; // Position within a byte (starts at 7 to indicate most significant bit read first)
  uint8 num; // Current byte
  int64 pos; // Index into encoded data
  int next_chunk_pos;
  int pos_from_chunk_end;
  int bitpos_from_chunk_end_pos;
  GLOBAL_DATA_T *global_data;
  //FRAME_PROBS_T frame_contexts[NUM_FRAME_CONTEXTS];   // Note that there are also unused frame_contexts inside global_data.  (Those ones are used in Python.)
  //FRAME_PROBS2_T frame_contexts2[NUM_FRAME_CONTEXTS];
  //FRAME_CDFS_T frame_contexts_cdfs[NUM_FRAME_CONTEXTS];
}
;
extern FILE *val_fid;

#include <stdarg.h>
/* Something is fatally wrong with this stream */
void ASSERT(int cond,const char *msg, ...) {
  if (cond)
    return;
  va_list argptr;
  va_start(argptr, msg);
  vprintf(msg, argptr);
  va_end(argptr);
  printf("\n");
  printf("Unable to continue decoding\nEXITING\n");
  if (val_fid) {
    fflush(val_fid);
    fclose(val_fid);
    val_fid=NULL;
  }
  exit(-1);
  assert(0);
}

// this bool is set if a check failed in debugmode and will cause us
// to exit with an error status at the end.
bool some_check_failed;

// Something is wrong according to the specification, but we think we
// can recover safely
void CHECK (int cond, const char *msg, ...) {
  if (cond)
    return;

  if (debugMode) {
    printf("Check Failed: ");
    va_list argptr;
    va_start(argptr, msg);
    vprintf(msg, argptr);
    va_end(argptr);
    printf("\n");

    some_check_failed = true;
  }
}

/*
OPT convert to packaging in integers
OPT optimise read_f
*/

void bitstream_init(TOPLEVEL_T *b,uint8 *data, int len) {
    b->data=data;
    b->len=len;
    b->pos=0;
    b->bitpos=7;
    b->num=*data;
    b->next_chunk_pos = 0;
    b->pos_from_chunk_end = 0;
    b->bitpos_from_chunk_end_pos = 0;
    b->global_data->isAnnexB = 0;
}

// Set position (in bytes)
int bitstream_set_pos (TOPLEVEL_T *b, int pos) {
    assert (pos <= b->len);
    b->bitpos = 7;
    b->pos = pos;
    b->num = (pos == b->len) ? 0 : b->data [b->pos];
    return 0;
}

// Count the number of bytes (including emulation prevention bytes) from pos to here
int bitstream_count_data_decode(TOPLEVEL_T *b,int pos) {
  ASSERT(b->bitpos == 7,"Entry points must be byte aligned");
  int numzeros = 0;
  int num = 0;
  int di;
  for(di=pos;di<b->pos;di++) {
    uint8 d=b->data[di];
    if (numzeros==2 && d<=3) {
      num+=1;
      numzeros=0;
    }
    if (d==0)
      numzeros += 1;
    else
      numzeros = 0;
    num +=1;
  }
  return num;
}

int bitstream_numbytes(TOPLEVEL_T *b) {
  return b->len;
}

int bitstream_current_position(TOPLEVEL_T *b) {
  return b->pos;
}

int64 bitstream_current_bitposition(TOPLEVEL_T *b) {
  int64 r = (b->pos<<3)+(7-b->bitpos);
  assert(r >= 0);
  return r;
}

#if PRINT_BIT_READS
int jsh_outID = 0;
int jsh_dont_print = 0;

int get_jsh_outID() {
  return jsh_outID++;
}
#endif

int bitstream_peek_last_bytes(TOPLEVEL_T *b, int size, uint8_t*out) {
  for (int i=0; i<size; i++) {
    out[i] = b->data[b->pos - (size-1) + i];
  }
  return 0;
}

int bitstream_readbit(TOPLEVEL_T *b) {
  int bp = b->bitpos;
  int x=(b->num>>bp)&1;
  if (bp>0) {
      b->bitpos=bp-1;
  } else {
    b->bitpos=7;
    b->pos+=1;
    if (b->pos<b->len) {
        b->num=b->data[b->pos];
    } else {
       b->num = 0; // This data should never actually be used
    }
  }
#if PRINT_BIT_READS
  if (!jsh_dont_print)
    printf("[%d] %lld %d Read 1: 0x%x\n",get_jsh_outID(), b->pos, b->bitpos, x);
#endif
  return x;
}

int bitstream_set_chunk_size(TOPLEVEL_T *b, int size) {
   ASSERT((b->next_chunk_pos == 0), "Set chunk size initilisation order wrong");
   b->next_chunk_pos = b->pos + size;
   b->pos_from_chunk_end = b->next_chunk_pos - 1;
   b->bitpos_from_chunk_end_pos = 0;
   return 0;
}

int bitstream_move_to_next_chunk(TOPLEVEL_T *b) {
   b->pos = b->next_chunk_pos;
   b->bitpos=7;
   if (b->pos<b->len) {
     b->num=b->data[b->pos];
   } else {
     b->num = 0; // This data should never actually be used
   }
   b->next_chunk_pos = 0;
   return 0;
}

int bitstream_readbit_from_chunk_end(TOPLEVEL_T *b) {
   if (b->bitpos_from_chunk_end_pos == 8) {
      b->pos_from_chunk_end -= 1;
      b->bitpos_from_chunk_end_pos = 0;
   }
   ASSERT((b->pos_from_chunk_end + 2 > b->pos), "Daala bit chunk overflow");
   int bit = (b->data[b->pos_from_chunk_end] >> b->bitpos_from_chunk_end_pos) & 0x1;
   b->bitpos_from_chunk_end_pos += 1;
   return bit;
}

int bitstream_more_rbsp_trailing_data(TOPLEVEL_T *b) {
  return b->pos<b->len-1;
}

int bitstream_more_rbsp_data(TOPLEVEL_T *b) {
  // Return 1 unless got to the stop bit in rbsp_trailing_bits
  if (b->pos<b->len-1)
      return 1;
  int t=0;
  for (int i=0;i<=b->bitpos;i++)
  {
      if (b->num&(1<<i))
          t+=1;
  }
  return t>1; // i.e. if only spot trailing bit, then say that we have finished
}

int bitstream_payload_extension_present(TOPLEVEL_T *b, int startpos, int payloadSize) {
  // Return 1 unless got to the stop bit in sei_payload
  if (b->pos-startpos<payloadSize-1)
      return 1;
  int t=0;
  for (int i=0;i<=b->bitpos;i++)
  {
      if (b->num&(1<<i))
          t+=1;
  }
  return t>1; // i.e. if only spot trailing bit, then say that we have finished
}

int bitstream_payload_extension_size(TOPLEVEL_T *b, int startpos, int payloadSize) {
  int extSize=0;
  //First add up everything before the last byte
  if (b->pos-startpos<payloadSize-1) {
      //Do the partial byte first
      extSize=b->bitpos+1;
      //Then do any full bytes
      if (b->pos-startpos<payloadSize-2) {
          extSize+=(payloadSize+startpos-b->pos-2)*8;
      }
  }
  //Now handle the last byte
  int lastByte=b->data[startpos+payloadSize-1];
  int lastOneBit;
  for (int x=0; x<8; x++) {
      lastOneBit=x;
      if ((lastByte>>x)&1)
          break;
  }
  //If we're IN the last byte we need to take our bitpos into account
  if ((b->pos-startpos)==(payloadSize-1)) {
      extSize=b->bitpos-lastOneBit;
  }
  //Otherwise we need to add all the available bits in the last byte
  else {
      extSize+=7-lastOneBit;
  }
  return extSize;
}

uint32 bitstream_read_f(TOPLEVEL_T *b,int num) {
  if (num==0)
      return 0;
  uint32 t=0;
#if PRINT_BIT_READS
  jsh_dont_print = 1;
#endif
  for (int i=0;i<num;i++)
      t=(t<<1)+bitstream_readbit(b);
#if PRINT_BIT_READS
  jsh_dont_print = 0;
  printf("[%d] %lld %d Read %d: 0x%x\n",get_jsh_outID(), b->pos, b->bitpos,num,t);
#endif
  return t;
}

#if CONFIG_RAWBITS
int bitstream_readbits_from_chunk_end(TOPLEVEL_T *b,int num) {
  if (num==0)
      return 0;
  int t=0;
  for (int i=0;i<num;i++)
      t=(t<<1)+bitstream_readbit_from_chunk_end(b);
  return t;
}
#endif // CONFIG_RAWBITS

int bitstream_next_bits(TOPLEVEL_T *b,int num) {
  int a=b->pos;
  int n=b->num;
  int c=b->bitpos;
  int x=bitstream_read_f(b,num);
  b->pos=a;
  b->num=n;
  b->bitpos=c;
  return x;
}

int bitstream_read_u(TOPLEVEL_T *b,int num) {
  return bitstream_read_f(b,num);
}

uint8 bitstream_read_b(TOPLEVEL_T *b,int num) {
  uint8 x=b->num;
  if (b->pos<b->len-1) {
      b->pos+=1;
      b->num=b->data[b->pos];
  }
  return x;
}

uint32 bitstream_read_ue(TOPLEVEL_T *b,int v) {
  // Unsigned Exp-Golomb as in 9.2
  int numzeros = 0;
  while (!bitstream_readbit(b) && numzeros<32)
      numzeros += 1;
  uint32 x = (1<<numzeros)+bitstream_read_f(b,numzeros)-1;
  return x;
}

int32 bitstream_read_se(TOPLEVEL_T *b,int v) {
  // Signed Exp-Golomb as in 9.2
  int x = bitstream_read_ue(b,v);
  if (x==0)
      return x;
  if (x&1)
      v = (x+1)>>1;
  else
      v = -(x>>1);
  return v;
}

int32 bitstream_read_le(TOPLEVEL_T *b,int num) {
  assert(num==16 || num==32 || num==24 || num==8);
  // Signed Exp-Golomb as in 9.2
  int v = bitstream_read_f(b,num);
  int  a = v&255;
  int  b2 = (v>>8)&255;
  int  c = (v>>16)&255;
  int  d = (v>>24)&255;
  if (num==16) {
    return b2+(a<<8);
  } else if (num==24) {
    return c+(b2<<8)+(a<<16);
  } else if (num==8) {
    return a;
  } else {
    return d+(c<<8)+(b2<<16)+(a<<24);
  }
}

int32 bitstream_read_s(TOPLEVEL_T *b,int num) {
  int x = bitstream_read_f(b,num);
  if (bitstream_readbit(b))
      return -x;
  return x;
}

int32 bitstream_read_su(TOPLEVEL_T *b,int num) {
  int v = bitstream_read_f(b,num+1);
  int sign_bit = 1 << num;
  return (v & (sign_bit - 1)) - (v & sign_bit);
}

// Read 111110, to give value 0->cMax
int32 bitstream_read_tu(TOPLEVEL_T *b,int cMax) {
  int x;
  for(x=0;x<cMax;x++) {
    if (!bitstream_readbit(b))
      break;
  }
  return x;
}

int bitstream_needs_byte_extensions(TOPLEVEL_T *b) {
  if (b->pos-1<b->len) {
    return (b->data[b->pos-1]&0xe0) == 0xc0;
  } else {
    printf("Overflowed bit buffer\n");
    return 0;
  }
}

int bitstream_byte_aligned(TOPLEVEL_T *b) {
  // Return if stream is aligned
  return (b->bitpos==7);
}

int64 builtin_syntax_elem(int64 v) {
  return v;
}
