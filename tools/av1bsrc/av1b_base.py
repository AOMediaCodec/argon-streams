################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

from optparse import OptionParser
import struct,os

# set this to False to always output the bpp in the first frame
outputBppPerFrame=True

check_bins  = 0  # auto checking: (In dkboolreader.c LOG_CABAC)
check_mvs   = 0  # auto checking: (In decodemv.c )
check_recon = 0  # auto checking: (In decodeframe.c LOG_PRED and reconintra ARGON_LOG_ALL)
check_inter = 0  # auto checking: Use log_recon_cabac.patch  (Ref code clips motion vectors before filtering so this is not always passing)
check_lf    = 0  # auto checking: (need to change define in loopfilter.c to stop edges being merged) Ref code adds extra deblocking off edge
check_loop  = 0  # auto checking: (in loopfilter.c LOG_LOOP), superblock level loopfilter checking
check_cdef  = 0  # auto checking: 64x64 level cdef checking
skip_allowed = 0 # If we have an odd multiple of 8 size, then ref code filters some offscreen edges which we should skip

check_yuv = 0

CONFIG_MISC_FIXES = 1

validate_critical = None

validate_fname = "validate_critical.txt"
if os.path.exists(validate_fname):
    with open(validate_fname, "r") as f:
        validate_critical = int(f.readline())

path_to_ref = r"aom/build/"

if check_bins:
  dut_cabac_log=open(path_to_ref+'dut_cabac.txt','wb')
  ref_cabac_log=open(path_to_ref+'ref_cabac2.txt','rb')
  cabac_bins_count = 0
  def debug_cabac(s):
      global cabac_bins_count,check_bins
      print >>dut_cabac_log,s
      if not check_bins:
        cabac_bins_count+=1
        return
      if cabac_bins_count == -122958:
        pdb.set_trace()
      while 1:
        s2=ref_cabac_log.readline().rstrip()
        if s2=='':
          print 'Reached end of reference file'
          break
        if 'bin=' in s2:
          break
      if s2!=s and check_bins:
          # try stripping the buf= part off the end (the ref encoder zero fills, we don't)
          s2BufEnd = s2.find(", buf=")
          sBufEnd = s.find(", buf=")
          s2Strip = s2
          sStrip = s
          if sBufEnd != -1 and s2BufEnd != -1:
            s2Strip = s2[0:s2BufEnd]
            sStrip = s[0:sBufEnd]
          if s2Strip != sStrip:
            dut_cabac_log.flush()
            print 'good',s2
            print 'bad',s
            pdb.set_trace()
            check_bins = 0
      #if cabac_bins_count>=6-1:
      #  pdb.set_trace()
      cabac_bins_count+=1

  def debug_cabac_daala_bits(binVal):
      s='%d bin=%d'%(cabac_bins_count,binVal)
      debug_cabac(s)

  def debug_cabac_daala_symbol(symbol,nsyms,rng,bits,Fh,Fl,buf):
      s='%d bin=%d/%d, rng=0x%x, bits=%d, Fh=%d, Fl=%d, buf=%d'%(cabac_bins_count,symbol,nsyms,rng,bits,Fh,Fl,buf)
      debug_cabac(s)

  def debug_cabac_bins(binVal,startRange,startVal,prob):
      if startRange==255:
        return # Skip marker bits as they appear out of order
      s='%d bin=%d sr=%d, sv=%d p=%d'%(cabac_bins_count,binVal,startRange,startVal,prob)
      debug_cabac(s)
else:
  def debug_cabac_bins(binVal,startRange,startVal,prob):
    pass
  def debug_cabac_daala_bits(binVal):
    pass
  def debug_cabac_daala_symbol(symbol,nsyms,rng,bits,Fh,Fl,buf):
    pass

if check_mvs:
  dut_mv_log=open(path_to_ref+'dut_mv.txt','wb')
  ref_mv_log=open(path_to_ref+'ref_mv2.txt','rb')
  mv_count = 0
  def debug_mv(b,i,mode,compound):
      global mv_count
      if i and not compound:
        return
      s="%d i=%d mode=%d dx=%d dy=%d"%(mv_count,i,mode,b.video_stream.frame_data.mv[i][1],b.video_stream.frame_data.mv[i][0])
      print >>dut_mv_log,s
      while 1:
        s2=ref_mv_log.readline().rstrip()
        if s2=='':
          print 'Reached end of reference file'
          break
        if 'mode=' in s2:
          break
      if s2!=s:
          dut_mv_log.flush()
          print 'good',s2
          print 'bad',s
          pdb.set_trace()
      #if mv_count==1082:
      #  pdb.set_trace()
      mv_count+=1
else:
  def debug_mv(b,i,mode,compound):
    pass

if check_lf:
  dut_lf_log=open(path_to_ref+'dut_lf.txt','wb')
  ref_lf_log=open(path_to_ref+'ref_lf2.txt','rb')
  lf_count = 0
  def debug_lf(b,phase,sz,x,y,dx,dy,plane):
      global lf_count, check_lf
      I=b.video_stream.frame_data.rframe
      pitch = b.video_stream.frame_data.stride
      a="%d phase=%d sz=%d x=%d y=%d plane=%d dx=%d dy=%d"%(lf_count,phase,sz,x,y,plane,dx,dy)
      a2=ref_lf_log.readline().rstrip()
      print >>dut_lf_log,a
      s=' '.join( ('%.2x' % I[plane][x+dx*i+(y+dy*i)*pitch]) for i in range(-(sz>>1),(sz>>1)))
      s2=ref_lf_log.readline().rstrip()
      print >>dut_lf_log,s
      if skip_allowed:
        a2_orig=a2
        s2_orig=s2
        num_skips=0
        while s!=s2:
          # Skip in batches of 4
          for i in range(4):
            a2=ref_lf_log.readline().rstrip()
            s2=ref_lf_log.readline().rstrip()
          num_skips+=1
          if num_skips>256:
            print 'skip failed to find match'
            a2=a2_orig
            s2=s2_orig
            break
      if s2!=s and check_lf:
          dut_lf_log.flush()
          print 'good',a2
          print 'good samples',s2
          print 'bad ',a
          print 'bad samples ',s
          pdb.set_trace()
          check_lf = False
      #if lf_count>=1343:
      #  pdb.set_trace()
      lf_count+=1
else:
  lf_count = 0
  def debug_lf(b,phase,sz,x,y,dx,dy,plane):
    #global lf_count
    #I=b.video_stream.frame_data.rframe
    #pitch = b.video_stream.frame_data.stride
    #a="%d phase=%d sz=%d x=%d y=%d plane=%d dx=%d dy=%d"%(lf_count,phase,sz,x,y,plane,dx,dy)
    #print a
    #s=' '.join( ('%.2x' % I[plane][x+dx*i+(y+dy*i)*pitch]) for i in range(-(sz>>1),(sz>>1)))
    #print s
    pass

if check_loop:
  dut_loop_log=open(path_to_ref+'dut_loop.txt','wb')
  ref_loop_log=open(path_to_ref+'ref_loop2.txt','rb')
  loop_count = 0
  def log_loop(b,mi_row,mi_col,step,plane,mi_size):
      global loop_count, check_loop
      I = b.video_stream.frame_data.rframe
      pitch = b.video_stream.frame_data.stride
      #if loop_count>=1343:
      #  pdb.set_trace()
      print >>dut_loop_log,loop_count,plane,step
      a2=ref_loop_log.readline().rstrip()
      w = min(64,(b.video_stream.frame_data.mi_cols - mi_col)*mi_size) >> b.global_data.plane_subsampling_x[plane]
      h = min(64,(b.video_stream.frame_data.mi_rows - mi_row)*mi_size) >> b.global_data.plane_subsampling_y[plane]
      basex = (mi_col)*mi_size >> b.global_data.plane_subsampling_x[plane]
      basey = (mi_row)*mi_size >> b.global_data.plane_subsampling_y[plane]

      for y in range(h):
        s = ' '.join('%.2x'%I[plane][x+basex+(y+basey)*pitch] for x in range(w))
        print >>dut_loop_log,s
        s2 = ref_loop_log.readline().rstrip()
        if s2!=s and check_loop:
          dut_loop_log.flush()
          print 'good',a2
          print 'good loop',s2
          print 'bad ',loop_count,plane,step,basex,basey,y,pitch
          print 'bad loop ',s
          pdb.set_trace()
          check_loop = False
      loop_count += 1
else:
  def log_loop(b,mi_row,mi_col,step,plane,mi_size):
    pass

if check_cdef:
  dut_cdef_log=open(path_to_ref+'dut_cdef.txt','wb')
  ref_cdef_log=open(path_to_ref+'ref_cdef2.txt','rb')
  cdef_count = 0
  def log_cdef(b,mi_row,mi_col,step,mi_size):
      global cdef_count, check_cdef
      if step == 0:
        I = b.video_stream.frame_data.rframe
      else:
        I = b.video_stream.frame_data.rcdef_frame
      pitch = b.video_stream.frame_data.stride
      nplanes = 3 if b.video_stream.frame_data.cdef_filter_chroma else 1
      for plane in range(nplanes):
        #if cdef_count>=1343:
        #  pdb.set_trace()
        print >>dut_cdef_log,cdef_count,plane,step
        a2=ref_cdef_log.readline().rstrip()
        w = min(64,(b.video_stream.frame_data.mi_cols - mi_col)*mi_size) >> b.global_data.plane_subsampling_x[plane]
        h = min(64,(b.video_stream.frame_data.mi_rows - mi_row)*mi_size) >> b.global_data.plane_subsampling_y[plane]
        x0 = ((mi_col)*mi_size >> b.global_data.plane_subsampling_x[plane])
        y0 = ((mi_row)*mi_size >> b.global_data.plane_subsampling_y[plane])

        for y in range(h):
          s = ' '.join('%.2x'%I[plane][x+x0+(y+y0)*pitch] for x in range(w))
          print >>dut_cdef_log,s
          s2 = ref_cdef_log.readline().rstrip()
          if s2!=s and check_cdef:
            dut_cdef_log.flush()
            print 'good',a2
            print 'good cdef',s2
            print 'bad ',cdef_count,plane,step,x0,y0,y,pitch
            print 'bad cdef ',s
            pdb.set_trace()
            check_cdef = False
        cdef_count += 1
else:
  def log_cdef(b,mi_row,mi_col,step,mi_size):
    pass

if check_inter:
  dut_inter_log=open(path_to_ref+'dut_inter.txt','wb')
  ref_inter_log=open(path_to_ref+'ref_inter2.txt','rb')
  inter_count = 0
  def debug_inter(subpel_x,subpel_y,w,h,mvrow,mvcol):
      global inter_count

      s="%d subx=%d suby=%d w=%d h=%d mvrow=%d mvcol=%d"%(inter_count,subpel_x, subpel_y, w, h, mvrow, mvcol)
      s2=ref_inter_log.readline().rstrip()
      print >>dut_inter_log,s
      if s2!=s:
          dut_inter_log.flush()
          print 'good',s2
          print 'bad ',s
          pdb.set_trace()
      #if inter_count==103:
      #  pdb.set_trace()
      inter_count+=1
else:
  def debug_inter(subpel_x,subpel_y,w,h,mvrow,mvcol):
    pass

PREDICTION = 0
TRANSFORM = 1
ABOVE = 2
LEFT = 3
OBMC_PRE = 4
OBMC_POST = 5
if check_recon:
  dut_recon_log=open(path_to_ref+'dut_recon.txt','wb')
  ref_recon_log=open(path_to_ref+'ref_recon2.txt','rb')
  recon_count = 0
  def log_prediction(b,tx_sz,x,y,plane,phase):
      bw = tx_size_wide[tx_sz]
      bh = tx_size_high[tx_sz]
      I=b.video_stream.frame_data.rframe[plane]
      pitch = b.video_stream.frame_data.stride
      log_prediction_rect(b,bw,bh,x,y,plane,phase,I,pitch)

  def log_prediction_rect_pred(b,bw,bh,plane,ref,pitch,phase):
      I=b.video_stream.frame_data.rpredSamples[ref]
      log_prediction_rect(b,bw,bh,0,0,plane,phase,I,pitch)

  def log_prediction_rect_frame(b,bw,bh,x,y,plane,phase):
      I=b.video_stream.frame_data.rframe[plane]
      pitch = b.video_stream.frame_data.stride
      log_prediction_rect(b,bw,bh,x,y,plane,phase,I,pitch)

  def log_prediction_rect(b,bw,bh,x,y,plane,phase,I,pitch):
      global recon_count,check_recon
      if not check_recon:
        return

      #if plane==0 and x<=40<x+bs and y<=1<y+bs:
      #  for dy in range(4):
      #    print dy,' '.join(('%.2x' % I[plane][x+dx+(y+dy)*pitch]) for dx in range(bs))
      #  pdb.set_trace()

      for dy in range(bh):
        s=' '.join(('%.2x' % I[x+dx+(y+dy)*pitch]) for dx in range(bw))
        print >>dut_recon_log,s
        while 1:
          s2=ref_recon_log.readline().rstrip()
          if s2=='':
            print 'Reached end of reference file'
            break
          if s2[0]=='P':
            s3="Prediction %d %d %d"%(recon_count, plane if phase>=0 else -1, phase)
            if s3!=s2:
              print "Mismatch in prediction marker"
              print 'good',s2
              print 'bad',s3
              pdb.set_trace()
              check_recon = 0
            else:
              continue
          if s2[0]=='R':
            continue
          break
        if s2!=s:
            if phase==PREDICTION:
              print 'Mismatch in prediction'
            elif phase==TRANSFORM:
              print 'Mismatch in transform'
            elif phase==ABOVE:
              print 'Mismatch in above'
            elif phase==LEFT:
              print 'Mismatch in left'
            elif phase==OBMC_PRE:
              print 'Mismatch in obmc pre'
            elif phase==OBMC_POST:
              print 'Mismatch in obmc post'
            else:
              print 'Mismatch in ???'
            dut_recon_log.flush()
            print 'x=',x,'y=',y,'dy=',dy
            print 'good',s2
            print 'bad ',s
            pdb.set_trace()
            check_recon = 0
      #if recon_count==5997 and not phase:
      #  pdb.set_trace()
      if phase==TRANSFORM:
        recon_count+=1

  track_intra_errors = 0
  def log_intra_neighbours(b,txw,txh,mode):
    global track_intra_errors
    s=('Intra %d left:'%mode)+' '.join(('%.2x' % b.video_stream.frame_data.left_data[x+2]) for x in range(txw+txh))
    print >>dut_recon_log,s
    s2=ref_recon_log.readline().rstrip()
    if track_intra_errors and s2!=s:
      dut_recon_log.flush()
      print 'good',s2
      print 'bad',s
      pdb.set_trace()
      track_intra_errors = 0
    s='Intra above:'+' '.join(('%.2x' % b.video_stream.frame_data.above_data[x+16]) for x in range(-1,txw+txh))
    print >>dut_recon_log,s
    s2=ref_recon_log.readline().rstrip()
    if track_intra_errors and s2!=s:
      dut_recon_log.flush()
      print 'good',s2
      print 'bad',s
      pdb.set_trace()
      track_intra_errors = 0

else:
  recon_count = 0
  def log_prediction(b,tx_sz,x,y,plane,phase):
    global recon_count
    if phase:
        recon_count+=1
  def log_prediction_rect_pred(b,bw,bh,plane,ref,pitch,phase):
    pass
  def log_prediction_rect_frame(b,bw,bh,x,y,plane,phase):
    pass
  def log_prediction_rect(b,bw,bh,x,y,plane,phase,I,pitch):
    pass
  def log_intra_neighbours(b,txw,txh,mode):
    pass

val_fid = open('validate_decode_python_spec.txt','wb')
val_count = 0
def validate(x):
  global val_count
  print >>val_fid,val_count,x
  if val_count==validate_critical:
    print 'critical'
    pdb.set_trace()
  val_count+=1


if check_yuv:
  yuv0 = open(path_to_ref+'ref_before_loop.yuv','rb')
  yuv1 = open(path_to_ref+'ref_after_loop.yuv','rb')
  yuv2 = open(path_to_ref+'ref_after_sao.yuv','rb')

  frame_num = 0
  def dump_yuv(b,stage):
    global frame_num
    w=b.seq_parameter_set_rbsp.pic_width_in_luma_samples
    h=b.seq_parameter_set_rbsp.pic_height_in_luma_samples
    R = [yuv0,yuv1,yuv2][stage]
    I = [b.global_data.picture_level.rCurrPic,b.global_data.picture_level.rCurrPic,b.global_data.picture_level.rsaoPicture][stage]
    if stage==2 and not b.seq_parameter_set_rbsp.sample_adaptive_offset_enabled_flag:
      I = b.global_data.picture_level.rCurrPic # If SAO is not used, then rsaoPicture is empty
    for y in xrange(h):
      for x in xrange(w):
        ref = ord(R.read(1))
        our = I[0][y][x]&255
        if ref!=our:
          print x,y,'luma ref=',ref,'ours=',our,'frame',frame_num,'stage',stage
          pdb.set_trace()
    for c in [1,2]:
      for y in xrange(h>>1):
        for x in xrange(w>>1):
          ref = ord(R.read(1))
          our = I[c][y][x]&255
          if ref!=our:
            print x,y,c,'chroma ref=',ref,'ours=',our,'frame',frame_num,'stage',stage
            pdb.set_trace()
    if stage==2:
      frame_num+=1
else:
  def dump_yuv(b,stage):
    pass

def ASSERT(cond,msg):
  """Check that cond is True, print msg otherwise"""
  if cond:
    return
  print msg
  assert False

def CHECK(cond,msg, *args):
  """Check that cond is True, print msg otherwise"""
  if cond:
    return
  print "Check Failed: {}".format(msg % tuple(args))
  pdb.set_trace()

def CSV(id, val):
  pass

if 1:
  write_yuv_bits = 0
  def video_output(b,bitdepthy,bitdepthc, w, h,crop_left,crop_right,crop_top,crop_bottom,pitch,extend,subw,subh,mono):
    global write_yuv_bits
    print "Output frame",w,h
    # TODO for show bit we need to output a framestore, using different pitch
    #assert crop_left==crop_right==crop_top==crop_bottom==0
    # The lines below would set the output bit-depth to be constant for the entire stream
    if outputBppPerFrame or write_yuv_bits==0:
        write_yuv_bits = bitdepthy
    upshift=bitdepthy<write_yuv_bits
    if (upshift):
      shift=write_yuv_bits-bitdepthy
      offset=1<<(shift-1)
      def transform(x):
        return (x<<shift)+offset
    else:
      shift=bitdepthy-write_yuv_bits
      def transform(x):
        return x>>shift

    subw=subw-1
    subh=subh-1
    #if m<0:
    #  I=b.video_stream.frame_data.rframe
    #else:
    #  I=b.global_data.rframestore[m]
    #  pitch=b.global_data.ref_width[m]
    I = b.global_data.rOutY, b.global_data.rOutU, b.global_data.rOutV
    w2=w-crop_left-crop_right
    h2=h-crop_top-crop_bottom
    baseY=(extend+crop_top)*(pitch)+extend+crop_left
    baseC=(extend+(crop_top>>1))*(pitch)+extend+(crop_left>>1)

    if write_yuv_bits>8:
      for y in xrange(h2):
        out_python.write(''.join(struct.pack('<H',transform(I[0][y*pitch + x])) for x in xrange(w2)))
      if not mono:
       for c in [1,2]:
        for y in xrange((h2+subh)>>subh):
          out_python.write(''.join(struct.pack('<H',transform(I[c][y*pitch + x])) for x in xrange((w2+subw)>>subw)))
    else:
      for y in xrange(h2):
        out_python.write(''.join(chr(transform(I[0][y*pitch + x])) for x in xrange(w2)))
      if not mono:
       for c in [1,2]:
        for y in xrange((h2+subh)>>subh):
          out_python.write(''.join(chr(transform(I[c][y*pitch + x])) for x in xrange((w2+subw)>>subw)))
    out_python.flush()

v='error'
startcode=('\0\0\1')

parser = OptionParser()
parser.add_option("-i","--input",dest="filename",default='../data/out.ivf',help="bitstream file to decode")
parser.add_option("-o","--output",dest="outFilename",default='out_python.yuv',help="YUV file to output")
parser.add_option("--not-annexb",dest="isAnnexB",action="store_const",default=1,const=0,help="use annex-b format")
parser.add_option("--no-film-grain",dest="overrideNoFilmGrain",action="store_const",default=0,const=1,help="Don't apply the film grain noise process")
(options,args) = parser.parse_args()
out_python=open(options.outFilename,'wb')

global_data=hevc_parse_global_data(Bitstream([]), 0, 0, options.overrideNoFilmGrain)

with open(options.filename,'rb') as fd:
    data=fd.read()

file_length = len(data)
raw_bytes = [ord(a) for a in data]
raw_bytes.append(0) # Add a zero at the end so that current_position does not stop early
b = Bitstream(raw_bytes,logging=True)
b.global_data = global_data
b.global_data.TargetDisplayFrameRate = -1
b.global_data.TargetDecodeFrameRate = -1
b.frame_contexts = checked_dict(8) # NUM_FRAME_CONTEXTS
b.frame_contexts2 = checked_dict(8) # NUM_FRAME_CONTEXTS
b.frame_contexts_cdfs = checked_dict(8) # NUM_FRAME_CONTEXTS

isObu = options.filename.endswith(".obu")
if isObu:
    if options.isAnnexB:
        hevc_parse_bitstream(b, file_length, -1)
    else:
        while b.current_position()<file_length:
            hevc_parse_open_bitstream_unit(b, 0)
else:
  file_hdr = hevc_parse_ivf_file_header(b)
  for frame in xrange(file_hdr.frame_cnt):
      frame_hdr = hevc_parse_ivf_frame_header(b)
      data_sz = frame_hdr.frame_sz
      hevc_parse_raw_bitstream(b, data_sz)
      #for i in xrange(data_sz):
      #    b.read_u(8, 'data_byte')

print "Read {} bytes out of {}".format(b.current_position(), b.numbytes())
