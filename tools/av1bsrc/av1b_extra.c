/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

global_data( ) {
    autostruct int Stride
    autostruct int NumTiles
    autostruct int TileCols
    autostruct int TileRows
    autostruct int TileColsLog2
    autostruct int TileRowsLog2
    autostruct int TileNum
    autostruct int FrameHeaderSize
    autostruct uint16 SymbolValue
    autostruct uint16 SymbolRange
    autostruct int SymbolMaxBits
    autostruct int BitDepth
    autostruct int TxMode
    autostruct int TxType
    autostruct int TxSize
    autostruct int ReferenceMode
    autostruct int NewMvCount
    autostruct uint1 ReadDeltas
    autostruct uint1 FrameIsIntra
    autostruct uint1 AllLossless
    autostruct uint1 CodedLossless
    autostruct uint1 Lossless
    autostruct uint1 CdefFilterChroma
    autostruct uint1 AvailU
    autostruct uint1 AvailL
    autostruct uint1 AvailUChroma
    autostruct uint1 AvailLChroma
    autostruct uint1 HasChroma
    autostruct uint1 SeenTemporalDelimiter
    autostruct uint1 SeenTemporalDelimiterBeforeSequenceHeader
    autostruct uint1 SeenSeqHeader
    autostruct uint1 SeenSeqHeaderBeforeFirstFrameHeader
    autostruct uint1 SeenFrameHeader
    autostruct uint1 is_inter
    autostruct int FoundMatch
    autostruct int RandomRegister
    autostruct int YMode
    autostruct int UVMode
    autostruct int PaletteSizeY
    autostruct int PaletteSizeUV
    autostruct uint_3_6 CdefDamping
    autostruct int MiCol
    autostruct int MiRow
    autostruct int MiCols
    autostruct int MiRows
    autostruct int MiColStart
    autostruct int MiRowStart
    autostruct int MiColEnd
    autostruct int MiRowEnd
    autostruct int TileWidth
    autostruct int TileHeight
    autostruct uint1 RightMostTile
    autostruct uint_0_21 MiSize
    autostruct int PosX8
    autostruct int PosY8
    autostruct uint_0_8 NumMvFound
    autostruct int NumNewMvFound
    autostruct int CompoundModeContext
    autostruct uint1 ZeroMvContext
    autostruct uint_0_5 NewMvContext
    autostruct uint_0_5 RefMvContext
    autostruct int NumSamples
    autostruct int NumSamplesScanned
    autostruct int FrameWidth
    autostruct int FrameHeight
    autostruct int RenderWidth
    autostruct int RenderHeight
    autostruct int AngleDeltaY
    autostruct int AngleDeltaUV
    autostruct int ColorMapY[ 128 ][ 128 ]
    autostruct int ColorMapUV[ 128 ][ 128 ]
    autostruct int ColorOrder[ PALETTE_COLORS ]
    autostruct int ColorContextHash
    autostruct uint4 PlaneTxType
    autostruct int CurrentQIndex
    autostruct int ReceivedSequenceHeader
    autostruct int WedgeMaskTableInitialised
    autostruct int TileSizeBytes
    autostruct int64 T[ 64 ]
    autostruct uint3 segment_id
    autostruct int LeftIntra
    autostruct int AboveIntra
    autostruct int LeftSingle
    autostruct int AboveSingle
    autostruct int RefMvIdx
    autostruct int RefFrameId[ NUM_REF_FRAMES ]
    autostruct int expectedFrameId[ NUM_REF_FRAMES ]
    autostruct uint1 MvCtx
    autostruct int MaxLumaW
    autostruct int MaxLumaH
    autostruct int CflAlphaU
    autostruct int CflAlphaV
    autostruct int Profile
    autostruct int OrderHint
    autostruct int TileStartX
    autostruct int TileStartY
    autostruct int TileEndX
    autostruct int TileEndY
    autostruct int StripeStartY
    autostruct int StripeEndY
    autostruct int CompFixedRef
    autostruct int AllZero
    autostruct int IsInterIntra
    autostruct int IsCFL
    autostruct int ReturnPred[ 128 ][ 128 ]
    autostruct int LastSpatialId
    autostruct int HasEnhancement
    autostruct int Leb128Bytes
    autostruct int SkipModeFrame[ 2 ]

    autostruct int MfRefFrames [][]
    autostruct int MfMvs[][][]

    autostruct int usedFrame[ NUM_REF_FRAMES ]
    autostruct int RefIdCount[ 2 ]
    autostruct int RefDiffCount[ 2 ]
    autostruct int RefIdMvs[ 2 ][ 2 ][ 2 ]
    autostruct int RefDiffMvs[ 2 ][ 2 ][ 2 ]
    autostruct int LumaGrain[ 73 ][ 82 ]
    autostruct int CbGrain[ 73 ][ 82 ]
    autostruct int CrGrain[ 73 ][ 82 ]
    autostruct int ScalingLut[ 3 ][ 256 ]
    autostruct int CloseMatches
    autostruct int InterRound0
    autostruct int InterRound1
    autostruct int InterPostRound
    autostruct int IdLen
    autostruct int TotalMatches
    autostruct int FwdWeight
    autostruct int BckWeight
    autostruct int GrainMin
    autostruct int GrainMax
    autostruct uint1 IsFirstFrame
    autostruct uint1 CdefAvailable
    autostruct int shiftedOrderHints[ NUM_REF_FRAMES ]
    autostruct int curFrameHint
    autostruct int OrderHintBits
    autostruct int ProjDenominator
    autostruct int operatingPoint
    autostruct int OperatingPointIdc
    autostruct int PlaneEndX
    autostruct int PlaneEndY
    autostruct int FrameUID
    autostruct int FrameNumber
    autostruct uint1 ValidationInterIsObmc
    autostruct int SymbolRingBuffer
    autostruct int SymbolRingPtr
    autostruct int OutputFrameCount

#if COVER
    // Special value used for coverage: Tracks whether we're using
    // the max params level but the sequence-level max size would fit
    // into level 6.3
    autostruct uint1 maxParamsSmall
#endif

    autostruct int DeltaLF[ FRAME_LF_COUNT ]
    autostruct int RefFrame[ 2 ]
    autostruct int Mv[ 2 ][ 2 ]
    autostruct int PredMv[ 2 ][ 2 ]
    autostruct int MvCtxStack[ 8 ][ 2 ]

    autostruct int PaletteCache[ 16 ]

    autostruct int16 LrFrame[][][]
    autostruct int16 CdefFrame[][][]
    autostruct int16 UpscaledCdefFrame[][][]

    autostruct int16 CurrFrame[][][]
    autostruct int16 UpscaledCurrFrame[][][]

    autostruct uint1 FeatureEnabled[ MAX_SEGMENTS ][ SEG_LVL_MAX ]

    autostruct uint1 Skips[][]

    autostruct int   InterTxSizes[][]
    autostruct int   TxSizes[][]
    autostruct int   TxTypes[][]
    autostruct int   MiSizes[][]

    autostruct int   LoopfilterTxSizes[][][]

    autostruct int MiColStarts[ MAX_TILE_COLS ]
    autostruct int MiRowStarts[ MAX_TILE_ROWS ]
    autostruct int MiColEnds[ MAX_TILE_COLS ]
    autostruct int MiRowEnds[ MAX_TILE_ROWS ]

    autostruct int MiColStartGrid [][]
    autostruct int MiRowStartGrid [][]
    autostruct int MiColEndGrid   [][]
    autostruct int MiRowEndGrid   [][]
    autostruct int TileStarts     [][]
    autostruct int SegmentIds     [][]
    autostruct int PrevSegmentIds [][]

    autostruct int PaletteSizes   [][][]
    autostruct int PaletteColors  [][][][]
    autostruct int DeltaLFs         [][][]
    autostruct int YModes           [][]
    autostruct int UVModes          [][]
    autostruct int CompGroupIdxs    [][]
    autostruct int CompoundIdxs     [][]
    autostruct int IsInters         [][]
    autostruct int SkipModes        [][]
    autostruct int RefFrames        [][][]
    autostruct int RefFramesWritten [][]
    autostruct int InterpFilters    [][][]
    autostruct int Mvs              [][][][]
    autostruct int PredMvs          [][][][]

    autostruct int MotionFieldMvs   [][][][]

    autostruct int OrderHints[ 8 ]
    autostruct int RefListMv[ 2 ][ 2 ][ 2 ]
    autostruct int GlobalMvs[ 2 ][ 2 ]
    autostruct int CandList[ 66 ][ 5 ]
    autostruct int preds[ 2 ][ 128 ][ 128 ]
    autostruct int LocalValid
    autostruct int LocalWarpParams[ 6 ]
    autostruct int WeightStack[ 8 ]
    autostruct int RefStackMv[ 8 ][ 2 ][ 2 ]
    autostruct int PredStackMv[ 8 ][ 2 ][ 2 ]
    autostruct int WedgeMasks[ BLOCK_SIZES ][ 2 ][ WEDGE_TYPES ][ 32 ][ 32 ]
    autostruct int MasterMask[ 6 ][ 64 ][ 64 ]
    autostruct int Mask[ 128 ][ 128 ]
    autostruct int SegQMLevel[ 3 ][ MAX_SEGMENTS ]
    autostruct int LosslessArray[ MAX_SEGMENTS ]
    autostruct int FeatureData[ MAX_SEGMENTS ][ SEG_LVL_MAX ]
    autostruct int Quant[ 64 * 64 ]
    autostruct int Dequant[ 64 ][ 64 ]
    autostruct int Residual[ 64 ][ 64 ]
    autostruct int rBlockDecoded[ 3 ][ ( 128 >> MI_SIZE_LOG2 ) + 2 ][ ( 128 >> MI_SIZE_LOG2 ) + 2 ]
    autostruct int LeftRefFrame[ 2 ]
    autostruct int AboveRefFrame[ 2 ]
    autostruct int FrameRestorationType[ 3 ]
    autostruct uint1 UsesLr
    autostruct int LoopRestorationSize[ 3 ]
    autostruct int rLrType[ 3 ][ MAX_LR_UNITS_IN_FRAME ]
    rewrite LrType[ i ][ r ][ c ] rLrType[ i ][ CheckStride(c, r, ( Stride + 63 ) / 64, MAX_LR_UNITS_IN_FRAME) ]
    autostruct int DrlCtxStack[ 8 ]

    rewrite BlockDecoded[ p ][ y ][ x ] rBlockDecoded[ p ][ y + 1 ][ x + 1 ]

    autostruct int rAboveRow[ 130 ]
    autostruct int rLeftCol[ 130 ]

    autostruct int LeftLevelContext[ 3 ][ MAX_HEIGHT >> MI_SIZE_LOG2 ]
    autostruct int AboveLevelContext[ 3 ][ MAX_WIDTH >> MI_SIZE_LOG2 ]
    autostruct int LeftDcContext[ 3 ][ MAX_HEIGHT >> MI_SIZE_LOG2 ]
    autostruct int AboveDcContext[ 3 ][ MAX_WIDTH >> MI_SIZE_LOG2 ]
    autostruct int LeftSegPredContext[ MAX_HEIGHT >> MI_SIZE_LOG2 ]
    autostruct int AboveSegPredContext[ MAX_WIDTH >> MI_SIZE_LOG2 ]

    rewrite AboveRow[ x ] rAboveRow[ x + 2 ]
    rewrite LeftCol[ x ] rLeftCol[ x + 2 ]

    autostruct int RefStride[ NUM_REF_FRAMES ]

    // Helper variables to track the size of variable-size allocated
    // arrays.
    autostruct int aloc_frame_stride
    autostruct int aloc_frame_height
    autostruct int aloc_mi_width
    autostruct int aloc_mi_height

    autostruct int16 FrameStore [][][][]

    autostruct uint16 OutY [][]
    autostruct uint16 OutU [][]
    autostruct uint16 OutV [][]
    autostruct uint16 OutputFrameY [][]
    autostruct uint16 OutputFrameU [][]
    autostruct uint16 OutputFrameV [][]

    autostruct uint16 rAnchorY[ 128 ][ ]
    rewrite AnchorY[ i ][ y ][ x ] rAnchorY[ i ][ CheckStride(x, y, AnchorWidth[i], MAX_PX) ]
    autostruct uint16 rAnchorU[ 128 ][ ]
    rewrite AnchorU[ i ][ y ][ x ] rAnchorU[ i ][ CheckStride(x, y, AnchorWidthC[i], MAX_PX) ]
    autostruct uint16 rAnchorV[ 128 ][ ]
    rewrite AnchorV[ i ][ y ][ x ] rAnchorV[ i ][ CheckStride(x, y, AnchorWidthC[i], MAX_PX) ]

    autostruct int AnchorWidth[ 128 ]
    autostruct int AnchorWidthC[ 128 ]
    autostruct int AnchorHeight[ 128 ]
    autostruct int RefValid[ NUM_REF_FRAMES ]
    autostruct int rRefUpscaledWidth[ NUM_REF_FRAMES + 1 ]
    autostruct int rRefFrameWidth[ NUM_REF_FRAMES + 1 ]
    autostruct int rRefFrameHeight[ NUM_REF_FRAMES + 1 ]
    rewrite RefUpscaledWidth[ x ] rRefUpscaledWidth[ x + 1 ]
    rewrite RefFrameWidth[ x ] rRefFrameWidth[ x + 1 ]
    rewrite RefFrameHeight[ x ] rRefFrameHeight[ x + 1 ]
    autostruct int RefRenderWidth[ NUM_REF_FRAMES ]
    autostruct int RefRenderHeight[ NUM_REF_FRAMES ]
    autostruct int RefSubsamplingX[ NUM_REF_FRAMES ]
    autostruct int RefSubsamplingY[ NUM_REF_FRAMES ]
    autostruct int RefBitDepth[ NUM_REF_FRAMES ]
    autostruct int RefOrderHint[ NUM_REF_FRAMES ]
    autostruct int RefMiCols[ NUM_REF_FRAMES ]
    autostruct int RefMiRows[ NUM_REF_FRAMES ]
    autostruct int RefFrameType[ NUM_REF_FRAMES ]
    autostruct int RefShowableFrame[ NUM_REF_FRAMES ]
    autostruct int RefShownCount[ NUM_REF_FRAMES ]
    autostruct int RefFrameUID[ NUM_REF_FRAMES ]
    autostruct int RefRefreshFrameFlags[ NUM_REF_FRAMES ]
    autostruct uint2 RefSeqProfile[ NUM_REF_FRAMES ]
    autostruct uint8 RefMatrixCoefficients[ NUM_REF_FRAMES ]
    autostruct uint8 RefColorPrimaries[ NUM_REF_FRAMES ]
    autostruct uint8 RefTransferCharacteristics[ NUM_REF_FRAMES ]
    autostruct int SavedOrderHints[ NUM_REF_FRAMES ][ 8 ]

    autostruct int SavedRefFrames  [][][]
    autostruct int SavedMvs        [][][][]
    autostruct int SavedSegmentIds [][][]

    autostruct int SavedGmParams[ NUM_REF_FRAMES ][ 8 ][ 6 ]

    autostruct int SavedApplyGrain[ NUM_REF_FRAMES ]
    autostruct int SavedGrainSeed[ NUM_REF_FRAMES ]
    autostruct int SavedNumYPoints[ NUM_REF_FRAMES ]
    autostruct int SavedPointYValue[ NUM_REF_FRAMES ][ 16 ]
    autostruct int SavedPointYScaling[ NUM_REF_FRAMES ][ 16 ]
    autostruct int SavedChromaScalingFromLuma[ NUM_REF_FRAMES ]
    autostruct int SavedNumCbPoints[ NUM_REF_FRAMES ]
    autostruct int SavedPointCbValue[ NUM_REF_FRAMES ][ 16 ]
    autostruct int SavedPointCbScaling[ NUM_REF_FRAMES ][ 16 ]
    autostruct int SavedNumCrPoints[ NUM_REF_FRAMES ]
    autostruct int SavedPointCrValue[ NUM_REF_FRAMES ][ 16 ]
    autostruct int SavedPointCrScaling[ NUM_REF_FRAMES ][ 16 ]
    autostruct int SavedGrainScalingMinus8[ NUM_REF_FRAMES ]
    autostruct int SavedArCoeffLag[ NUM_REF_FRAMES ]
    autostruct int SavedArCoeffsYPlus128[ NUM_REF_FRAMES ][ 24 ]
    autostruct int SavedArCoeffsCbPlus128[ NUM_REF_FRAMES ][ 25 ]
    autostruct int SavedArCoeffsCrPlus128[ NUM_REF_FRAMES ][ 25 ]
    autostruct int SavedArCoeffShiftMinus6[ NUM_REF_FRAMES ]
    autostruct int SavedGrainScaleShift[ NUM_REF_FRAMES ]
    autostruct int SavedCbMult[ NUM_REF_FRAMES ]
    autostruct int SavedCbLumaMult[ NUM_REF_FRAMES ]
    autostruct int SavedCbOffset[ NUM_REF_FRAMES ]
    autostruct int SavedCrMult[ NUM_REF_FRAMES ]
    autostruct int SavedCrLumaMult[ NUM_REF_FRAMES ]
    autostruct int SavedCrOffset[ NUM_REF_FRAMES ]
    autostruct int SavedOverlapFlag[ NUM_REF_FRAMES ]
    autostruct int SavedClipToRestrictedRange[ NUM_REF_FRAMES ]
    autostruct int SavedLoopFilterRefDeltas[ NUM_REF_FRAMES ][ TOTAL_REFS_PER_FRAME ]
    autostruct int SavedLoopFilterModeDeltas[ NUM_REF_FRAMES ][ 2 ]
    autostruct int SavedFeatureEnabled[ NUM_REF_FRAMES ][ MAX_SEGMENTS ][ SEG_LVL_MAX ]
    autostruct int SavedFeatureData[ NUM_REF_FRAMES ][ MAX_SEGMENTS ][ SEG_LVL_MAX ]



    autostruct uint16 YModeCdf[ BLOCK_SIZE_GROUPS ][ INTRA_MODES + 1 ]
    autostruct uint16 UVModeCflNotAllowedCdf[ INTRA_MODES ][ UV_INTRA_MODES_CFL_NOT_ALLOWED + 1 ]
    autostruct uint16 UVModeCflAllowedCdf[ INTRA_MODES ][ UV_INTRA_MODES_CFL_ALLOWED + 1 ]
    autostruct uint16 IntrabcCdf[ 2 + 1 ]
    autostruct uint16 PartitionW8Cdf[ PARTITION_CONTEXTS ][ 4 + 1 ]
    autostruct uint16 PartitionW16Cdf[ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 PartitionW32Cdf[ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 PartitionW64Cdf[ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 PartitionW128Cdf[ PARTITION_CONTEXTS ][ 8 + 1 ]
    autostruct uint16 SegmentIdCdf[ SEGMENT_ID_CONTEXTS ][ MAX_SEGMENTS + 1 ]
    autostruct uint16 SegmentIdPredictedCdf[ SEGMENT_ID_PREDICTED_CONTEXTS ][ 3 ]
    autostruct uint16 Tx8x8Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 1 ]
    autostruct uint16 Tx16x16Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 Tx32x32Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 Tx64x64Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 TxfmSplitCdf[ TXFM_PARTITION_CONTEXTS ][ 3 ]
    autostruct uint16 FilterIntraModeCdf[ 6 ]
    autostruct uint16 FilterIntraCdf[ BLOCK_SIZES ][ 3 ]
    autostruct uint16 InterpFilterCdf[ INTERP_FILTER_CONTEXTS ][ INTERP_FILTERS + 1 ]
    autostruct uint16 MotionModeCdf[ BLOCK_SIZES ][ MOTION_MODES + 1 ]
    autostruct uint16 NewMvCdf[ NEW_MV_CONTEXTS ][ 3 ]
    autostruct uint16 ZeroMvCdf[ ZERO_MV_CONTEXTS ][ 3 ]
    autostruct uint16 RefMvCdf[ REF_MV_CONTEXTS ][ 3 ]
    autostruct uint16 CompoundModeCdf[ COMPOUND_MODE_CONTEXTS ][ COMPOUND_MODES + 1 ]
    autostruct uint16 DrlModeCdf[ DRL_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 IsInterCdf[ IS_INTER_CONTEXTS ][ 3 ]
    autostruct uint16 CompModeCdf[ COMP_INTER_CONTEXTS ][ 3 ]
    autostruct uint16 SkipModeCdf[ SKIP_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 SkipCdf[ SKIP_CONTEXTS ][ 3 ]
    autostruct uint16 CompRefCdf[ REF_CONTEXTS ][ FWD_REFS - 1 ][ 3 ]
    autostruct uint16 CompBwdRefCdf[ REF_CONTEXTS ][ BWD_REFS - 1 ][ 3 ]
    autostruct uint16 SingleRefCdf[ REF_CONTEXTS ][ SINGLE_REFS - 1 ][ 3 ]
    autostruct uint16 MvJointCdf[ MV_CONTEXTS ][ MV_JOINTS + 1 ]
    autostruct uint16 MvClassCdf[ MV_CONTEXTS ][ 2 ][ MV_CLASSES + 1 ]
    autostruct uint16 MvClass0BitCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 MvFrCdf[ MV_CONTEXTS ][ 2 ][ MV_JOINTS + 1 ]
    autostruct uint16 MvClass0FrCdf[ MV_CONTEXTS ][ 2 ][ CLASS0_SIZE ][ MV_JOINTS + 1 ]
    autostruct uint16 MvClass0HpCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 MvBitCdf[ MV_CONTEXTS ][ 2 ][ MV_OFFSET_BITS ][ 3 ]
    autostruct uint16 MvHpCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 MvSignCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 PaletteYModeCdf[ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_Y_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 PaletteYSizeCdf[ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_SIZES + 1 ]
    autostruct uint16 PaletteUVSizeCdf[ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_SIZES + 1 ]
    autostruct uint16 PaletteUVModeCdf[ PALETTE_UV_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 PaletteSize2YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 3 ]
    autostruct uint16 PaletteSize2UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 3 ]
    autostruct uint16 PaletteSize3YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 4 ]
    autostruct uint16 PaletteSize3UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 4 ]
    autostruct uint16 PaletteSize4YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 5 ]
    autostruct uint16 PaletteSize4UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 5 ]
    autostruct uint16 PaletteSize5YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 6 ]
    autostruct uint16 PaletteSize5UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 6 ]
    autostruct uint16 PaletteSize6YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 7 ]
    autostruct uint16 PaletteSize6UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 7 ]
    autostruct uint16 PaletteSize7YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 8 ]
    autostruct uint16 PaletteSize7UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 8 ]
    autostruct uint16 PaletteSize8YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 9 ]
    autostruct uint16 PaletteSize8UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 9 ]
    autostruct uint16 DeltaQCdf[ DELTA_Q_SMALL + 2 ]
    autostruct uint16 DeltaLFCdf[ DELTA_LF_SMALL + 2 ]
    autostruct uint16 IntraTxTypeSet1Cdf[ 2 ][ INTRA_MODES ][ 8 ]
    autostruct uint16 IntraTxTypeSet2Cdf[ 3 ][ INTRA_MODES ][ 8 ]
    autostruct uint16 InterTxTypeSet1Cdf[ 2 ][ 17 ]
    autostruct uint16 InterTxTypeSet2Cdf[ 13 ]
    autostruct uint16 InterTxTypeSet3Cdf[ 4 ][ 3 ]
    autostruct uint16 UseObmcCdf[ BLOCK_SIZES ][ 3 ]
    autostruct uint16 InterIntraCdf[ BLOCK_SIZE_GROUPS - 1 ][ 3 ]
    autostruct uint16 CompRefTypeCdf[ COMP_REF_TYPE_CONTEXTS ][ 3 ]
    autostruct uint16 CflSignCdf[ CFL_JOINT_SIGNS + 1 ]
    autostruct uint16 UniCompRefCdf[ REF_CONTEXTS ][ UNIDIR_COMP_REFS - 1 ][ 3 ]
    autostruct uint16 WedgeInterIntraCdf[ BLOCK_SIZES ][ 3 ]
    autostruct uint16 CompoundIdxCdf[ COMPOUND_IDX_CONTEXTS ][ 3 ]
    autostruct uint16 CompGroupIdxCdf[ COMP_GROUP_IDX_CONTEXTS ][ 3 ]
    autostruct uint16 CompoundTypeCdf[ BLOCK_SIZES ][ COMPOUND_TYPES + 1 ]
    autostruct uint16 InterIntraModeCdf[ BLOCK_SIZE_GROUPS - 1 ][ INTERINTRA_MODES + 1 ]
    autostruct uint16 WedgeIndexCdf[ BLOCK_SIZES ][ 16 + 1 ]
    autostruct uint16 CflAlphaCdf[ CFL_ALPHA_CONTEXTS ][ CFL_ALPHABET_SIZE + 1 ]
    autostruct uint16 DeltaLFMultiCdf[ FRAME_LF_COUNT ][ DELTA_LF_SMALL + 2 ]
    autostruct uint16 UseWienerCdf[ 3 ]
    autostruct uint16 UseSgrprojCdf[ 3 ]
    autostruct uint16 RestorationTypeCdf[ RESTORE_SWITCHABLE + 1 ]
    autostruct uint16 EobPt16Cdf[ PLANE_TYPES ][ 2 ][ 6 ]
    autostruct uint16 EobPt32Cdf[ PLANE_TYPES ][ 2 ][ 7 ]
    autostruct uint16 EobPt64Cdf[ PLANE_TYPES ][ 2 ][ 8 ]
    autostruct uint16 EobPt128Cdf[ PLANE_TYPES ][ 2 ][ 9 ]
    autostruct uint16 EobPt256Cdf[ PLANE_TYPES ][ 2 ][ 10 ]
    autostruct uint16 EobPt512Cdf[ PLANE_TYPES ][ 11 ]
    autostruct uint16 EobPt1024Cdf[ PLANE_TYPES ][ 12 ]
    autostruct uint16 TxbSkipCdf[ TX_SIZES ][ TXB_SKIP_CONTEXTS ][ 3 ]
    autostruct uint16 EobExtraCdf[ TX_SIZES ][ PLANE_TYPES ][ EOB_COEF_CONTEXTS ][ 3 ]
    autostruct uint16 DcSignCdf[ PLANE_TYPES ][ DC_SIGN_CONTEXTS ][ 3 ]
    autostruct uint16 CoeffBaseEobCdf[ TX_SIZES ][ PLANE_TYPES ][ SIG_COEF_CONTEXTS_EOB ][ 4 ]
    autostruct uint16 CoeffBaseCdf[ TX_SIZES ][ PLANE_TYPES ][ SIG_COEF_CONTEXTS ][ 5 ]
    autostruct uint16 CoeffBrCdf[ TX_SIZES ][ PLANE_TYPES ][ LEVEL_CONTEXTS ][ BR_CDF_SIZE + 1 ]
    autostruct uint16 AngleDeltaCdf[ DIRECTIONAL_MODES ][ (2 * MAX_ANGLE_DELTA + 1) + 1 ]

    autostruct uint16 TileIntraFrameYModeCdf[ INTRA_MODE_CONTEXTS ][ INTRA_MODE_CONTEXTS ][ INTRA_MODES + 1 ]
    autostruct uint16 TileYModeCdf[ BLOCK_SIZE_GROUPS ][ INTRA_MODES + 1 ]
    autostruct uint16 TileUVModeCflNotAllowedCdf[ INTRA_MODES ][ UV_INTRA_MODES_CFL_NOT_ALLOWED + 1 ]
    autostruct uint16 TileUVModeCflAllowedCdf[ INTRA_MODES ][ UV_INTRA_MODES_CFL_ALLOWED + 1 ]
    autostruct uint16 TileIntrabcCdf[ 2 + 1 ]
    autostruct uint16 TilePartitionW8Cdf[ PARTITION_CONTEXTS ][ 4 + 1 ]
    autostruct uint16 TilePartitionW16Cdf[ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 TilePartitionW32Cdf[ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 TilePartitionW64Cdf[ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 TilePartitionW128Cdf[ PARTITION_CONTEXTS ][ 8 + 1 ]
    autostruct uint16 TileSegmentIdCdf[ SEGMENT_ID_CONTEXTS ][ MAX_SEGMENTS + 1 ]
    autostruct uint16 TileSegmentIdPredictedCdf[ SEGMENT_ID_PREDICTED_CONTEXTS ][ 3 ]
    autostruct uint16 TileTx8x8Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 1 ]
    autostruct uint16 TileTx16x16Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 TileTx32x32Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 TileTx64x64Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 TileTxfmSplitCdf[ TXFM_PARTITION_CONTEXTS ][ 3 ]
    autostruct uint16 TileFilterIntraModeCdf[ 6 ]
    autostruct uint16 TileFilterIntraCdf[ BLOCK_SIZES ][ 3 ]
    autostruct uint16 TileInterpFilterCdf[ INTERP_FILTER_CONTEXTS ][ INTERP_FILTERS + 1 ]
    autostruct uint16 TileMotionModeCdf[ BLOCK_SIZES ][ MOTION_MODES + 1 ]
    autostruct uint16 TileNewMvCdf[ NEW_MV_CONTEXTS ][ 3 ]
    autostruct uint16 TileZeroMvCdf[ ZERO_MV_CONTEXTS ][ 3 ]
    autostruct uint16 TileRefMvCdf[ REF_MV_CONTEXTS ][ 3 ]
    autostruct uint16 TileCompoundModeCdf[ COMPOUND_MODE_CONTEXTS ][ COMPOUND_MODES + 1 ]
    autostruct uint16 TileDrlModeCdf[ DRL_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 TileIsInterCdf[ IS_INTER_CONTEXTS ][ 3 ]
    autostruct uint16 TileCompModeCdf[ COMP_INTER_CONTEXTS ][ 3 ]
    autostruct uint16 TileSkipModeCdf[ SKIP_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 TileSkipCdf[ SKIP_CONTEXTS ][ 3 ]
    autostruct uint16 TileCompRefCdf[ REF_CONTEXTS ][ FWD_REFS - 1 ][ 3 ]
    autostruct uint16 TileCompBwdRefCdf[ REF_CONTEXTS ][ BWD_REFS - 1 ][ 3 ]
    autostruct uint16 TileSingleRefCdf[ REF_CONTEXTS ][ SINGLE_REFS - 1 ][ 3 ]
    autostruct uint16 TileMvJointCdf[ MV_CONTEXTS ][ MV_JOINTS + 1 ]
    autostruct uint16 TileMvClassCdf[ MV_CONTEXTS ][ 2 ][ MV_CLASSES + 1 ]
    autostruct uint16 TileMvClass0BitCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 TileMvFrCdf[ MV_CONTEXTS ][ 2 ][ MV_JOINTS + 1 ]
    autostruct uint16 TileMvClass0FrCdf[ MV_CONTEXTS ][ 2 ][ CLASS0_SIZE ][ MV_JOINTS + 1 ]
    autostruct uint16 TileMvClass0HpCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 TileMvBitCdf[ MV_CONTEXTS ][ 2 ][ MV_OFFSET_BITS ][ 3 ]
    autostruct uint16 TileMvHpCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 TileMvSignCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 TilePaletteYModeCdf[ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_Y_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 TilePaletteYSizeCdf[ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_SIZES + 1 ]
    autostruct uint16 TilePaletteUVSizeCdf[ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_SIZES + 1 ]
    autostruct uint16 TilePaletteUVModeCdf[ PALETTE_UV_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 TilePaletteSize2YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 3 ]
    autostruct uint16 TilePaletteSize2UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 3 ]
    autostruct uint16 TilePaletteSize3YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 4 ]
    autostruct uint16 TilePaletteSize3UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 4 ]
    autostruct uint16 TilePaletteSize4YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 5 ]
    autostruct uint16 TilePaletteSize4UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 5 ]
    autostruct uint16 TilePaletteSize5YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 6 ]
    autostruct uint16 TilePaletteSize5UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 6 ]
    autostruct uint16 TilePaletteSize6YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 7 ]
    autostruct uint16 TilePaletteSize6UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 7 ]
    autostruct uint16 TilePaletteSize7YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 8 ]
    autostruct uint16 TilePaletteSize7UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 8 ]
    autostruct uint16 TilePaletteSize8YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 9 ]
    autostruct uint16 TilePaletteSize8UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 9 ]
    autostruct uint16 TileDeltaQCdf[ DELTA_Q_SMALL + 2 ]
    autostruct uint16 TileDeltaLFCdf[ DELTA_LF_SMALL + 2 ]
    autostruct uint16 TileIntraTxTypeSet1Cdf[ 2 ][ INTRA_MODES ][ 8 ]
    autostruct uint16 TileIntraTxTypeSet2Cdf[ 3 ][ INTRA_MODES ][ 8 ]
    autostruct uint16 TileInterTxTypeSet1Cdf[ 2 ][ 17 ]
    autostruct uint16 TileInterTxTypeSet2Cdf[ 13 ]
    autostruct uint16 TileInterTxTypeSet3Cdf[ 4 ][ 3 ]
    autostruct uint16 TileUseObmcCdf[ BLOCK_SIZES ][ 3 ]
    autostruct uint16 TileInterIntraCdf[ BLOCK_SIZE_GROUPS - 1 ][ 3 ]
    autostruct uint16 TileCompRefTypeCdf[ COMP_REF_TYPE_CONTEXTS ][ 3 ]
    autostruct uint16 TileCflSignCdf[ CFL_JOINT_SIGNS + 1 ]
    autostruct uint16 TileUniCompRefCdf[ REF_CONTEXTS ][ UNIDIR_COMP_REFS - 1 ][ 3 ]
    autostruct uint16 TileWedgeInterIntraCdf[ BLOCK_SIZES ][ 3 ]
    autostruct uint16 TileCompoundIdxCdf[ COMPOUND_IDX_CONTEXTS ][ 3 ]
    autostruct uint16 TileCompGroupIdxCdf[ COMP_GROUP_IDX_CONTEXTS ][ 3 ]
    autostruct uint16 TileCompoundTypeCdf[ BLOCK_SIZES ][ COMPOUND_TYPES + 1 ]
    autostruct uint16 TileInterIntraModeCdf[ BLOCK_SIZE_GROUPS - 1 ][ INTERINTRA_MODES + 1 ]
    autostruct uint16 TileWedgeIndexCdf[ BLOCK_SIZES ][ 16 + 1 ]
    autostruct uint16 TileCflAlphaCdf[ CFL_ALPHA_CONTEXTS ][ CFL_ALPHABET_SIZE + 1 ]
    autostruct uint16 TileDeltaLFMultiCdf[ FRAME_LF_COUNT ][ DELTA_LF_SMALL + 2 ]
    autostruct uint16 TileUseWienerCdf[ 3 ]
    autostruct uint16 TileUseSgrprojCdf[ 3 ]
    autostruct uint16 TileRestorationTypeCdf[ RESTORE_SWITCHABLE + 1 ]
    autostruct uint16 TileEobPt16Cdf[ PLANE_TYPES ][ 2 ][ 6 ]
    autostruct uint16 TileEobPt32Cdf[ PLANE_TYPES ][ 2 ][ 7 ]
    autostruct uint16 TileEobPt64Cdf[ PLANE_TYPES ][ 2 ][ 8 ]
    autostruct uint16 TileEobPt128Cdf[ PLANE_TYPES ][ 2 ][ 9 ]
    autostruct uint16 TileEobPt256Cdf[ PLANE_TYPES ][ 2 ][ 10 ]
    autostruct uint16 TileEobPt512Cdf[ PLANE_TYPES ][ 11 ]
    autostruct uint16 TileEobPt1024Cdf[ PLANE_TYPES ][ 12 ]
    autostruct uint16 TileTxbSkipCdf[ TX_SIZES ][ TXB_SKIP_CONTEXTS ][ 3 ]
    autostruct uint16 TileEobExtraCdf[ TX_SIZES ][ PLANE_TYPES ][ EOB_COEF_CONTEXTS ][ 3 ]
    autostruct uint16 TileDcSignCdf[ PLANE_TYPES ][ DC_SIGN_CONTEXTS ][ 3 ]
    autostruct uint16 TileCoeffBaseEobCdf[ TX_SIZES ][ PLANE_TYPES ][ SIG_COEF_CONTEXTS_EOB ][ 4 ]
    autostruct uint16 TileCoeffBaseCdf[ TX_SIZES ][ PLANE_TYPES ][ SIG_COEF_CONTEXTS ][ 5 ]
    autostruct uint16 TileCoeffBrCdf[ TX_SIZES ][ PLANE_TYPES ][ LEVEL_CONTEXTS ][ BR_CDF_SIZE + 1 ]
    autostruct uint16 TileAngleDeltaCdf[ DIRECTIONAL_MODES ][ (2 * MAX_ANGLE_DELTA + 1) + 1 ]

    autostruct uint16 SavedYModeCdf[ BLOCK_SIZE_GROUPS ][ INTRA_MODES + 1 ]
    autostruct uint16 SavedUVModeCflNotAllowedCdf[ INTRA_MODES ][ UV_INTRA_MODES_CFL_NOT_ALLOWED + 1 ]
    autostruct uint16 SavedUVModeCflAllowedCdf[ INTRA_MODES ][ UV_INTRA_MODES_CFL_ALLOWED + 1 ]
    autostruct uint16 SavedIntrabcCdf[ 2 + 1 ]
    autostruct uint16 SavedPartitionW8Cdf[ PARTITION_CONTEXTS ][ 4 + 1 ]
    autostruct uint16 SavedPartitionW16Cdf[ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 SavedPartitionW32Cdf[ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 SavedPartitionW64Cdf[ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 SavedPartitionW128Cdf[ PARTITION_CONTEXTS ][ 8 + 1 ]
    autostruct uint16 SavedSegmentIdCdf[ SEGMENT_ID_CONTEXTS ][ MAX_SEGMENTS + 1 ]
    autostruct uint16 SavedSegmentIdPredictedCdf[ SEGMENT_ID_PREDICTED_CONTEXTS ][ 3 ]
    autostruct uint16 SavedTx8x8Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 1 ]
    autostruct uint16 SavedTx16x16Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 SavedTx32x32Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 SavedTx64x64Cdf[ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 SavedTxfmSplitCdf[ TXFM_PARTITION_CONTEXTS ][ 3 ]
    autostruct uint16 SavedFilterIntraModeCdf[ 6 ]
    autostruct uint16 SavedFilterIntraCdf[ BLOCK_SIZES ][ 3 ]
    autostruct uint16 SavedInterpFilterCdf[ INTERP_FILTER_CONTEXTS ][ INTERP_FILTERS + 1 ]
    autostruct uint16 SavedMotionModeCdf[ BLOCK_SIZES ][ MOTION_MODES + 1 ]
    autostruct uint16 SavedNewMvCdf[ NEW_MV_CONTEXTS ][ 3 ]
    autostruct uint16 SavedZeroMvCdf[ ZERO_MV_CONTEXTS ][ 3 ]
    autostruct uint16 SavedRefMvCdf[ REF_MV_CONTEXTS ][ 3 ]
    autostruct uint16 SavedCompoundModeCdf[ COMPOUND_MODE_CONTEXTS ][ COMPOUND_MODES + 1 ]
    autostruct uint16 SavedDrlModeCdf[ DRL_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 SavedIsInterCdf[ IS_INTER_CONTEXTS ][ 3 ]
    autostruct uint16 SavedCompModeCdf[ COMP_INTER_CONTEXTS ][ 3 ]
    autostruct uint16 SavedSkipModeCdf[ SKIP_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 SavedSkipCdf[ SKIP_CONTEXTS ][ 3 ]
    autostruct uint16 SavedCompRefCdf[ REF_CONTEXTS ][ FWD_REFS - 1 ][ 3 ]
    autostruct uint16 SavedCompBwdRefCdf[ REF_CONTEXTS ][ BWD_REFS - 1 ][ 3 ]
    autostruct uint16 SavedSingleRefCdf[ REF_CONTEXTS ][ SINGLE_REFS - 1 ][ 3 ]
    autostruct uint16 SavedMvJointCdf[ MV_CONTEXTS ][ MV_JOINTS + 1 ]
    autostruct uint16 SavedMvClassCdf[ MV_CONTEXTS ][ 2 ][ MV_CLASSES + 1 ]
    autostruct uint16 SavedMvClass0BitCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 SavedMvFrCdf[ MV_CONTEXTS ][ 2 ][ MV_JOINTS + 1 ]
    autostruct uint16 SavedMvClass0FrCdf[ MV_CONTEXTS ][ 2 ][ CLASS0_SIZE ][ MV_JOINTS + 1 ]
    autostruct uint16 SavedMvClass0HpCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 SavedMvBitCdf[ MV_CONTEXTS ][ 2 ][ MV_OFFSET_BITS ][ 3 ]
    autostruct uint16 SavedMvHpCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 SavedMvSignCdf[ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 SavedPaletteYModeCdf[ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_Y_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 SavedPaletteYSizeCdf[ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_SIZES + 1 ]
    autostruct uint16 SavedPaletteUVSizeCdf[ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_SIZES + 1 ]
    autostruct uint16 SavedPaletteUVModeCdf[ PALETTE_UV_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 SavedPaletteSize2YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 3 ]
    autostruct uint16 SavedPaletteSize2UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 3 ]
    autostruct uint16 SavedPaletteSize3YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 4 ]
    autostruct uint16 SavedPaletteSize3UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 4 ]
    autostruct uint16 SavedPaletteSize4YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 5 ]
    autostruct uint16 SavedPaletteSize4UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 5 ]
    autostruct uint16 SavedPaletteSize5YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 6 ]
    autostruct uint16 SavedPaletteSize5UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 6 ]
    autostruct uint16 SavedPaletteSize6YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 7 ]
    autostruct uint16 SavedPaletteSize6UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 7 ]
    autostruct uint16 SavedPaletteSize7YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 8 ]
    autostruct uint16 SavedPaletteSize7UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 8 ]
    autostruct uint16 SavedPaletteSize8YColorCdf[ PALETTE_COLOR_CONTEXTS ][ 9 ]
    autostruct uint16 SavedPaletteSize8UVColorCdf[ PALETTE_COLOR_CONTEXTS ][ 9 ]
    autostruct uint16 SavedDeltaQCdf[ DELTA_Q_SMALL + 2 ]
    autostruct uint16 SavedDeltaLFCdf[ DELTA_LF_SMALL + 2 ]
    autostruct uint16 SavedIntraTxTypeSet1Cdf[ 2 ][ INTRA_MODES ][ 8 ]
    autostruct uint16 SavedIntraTxTypeSet2Cdf[ 3 ][ INTRA_MODES ][ 8 ]
    autostruct uint16 SavedInterTxTypeSet1Cdf[ 2 ][ 17 ]
    autostruct uint16 SavedInterTxTypeSet2Cdf[ 13 ]
    autostruct uint16 SavedInterTxTypeSet3Cdf[ 4 ][ 3 ]
    autostruct uint16 SavedUseObmcCdf[ BLOCK_SIZES ][ 3 ]
    autostruct uint16 SavedInterIntraCdf[ BLOCK_SIZE_GROUPS - 1 ][ 3 ]
    autostruct uint16 SavedCompRefTypeCdf[ COMP_REF_TYPE_CONTEXTS ][ 3 ]
    autostruct uint16 SavedCflSignCdf[ CFL_JOINT_SIGNS + 1 ]
    autostruct uint16 SavedUniCompRefCdf[ REF_CONTEXTS ][ UNIDIR_COMP_REFS - 1 ][ 3 ]
    autostruct uint16 SavedWedgeInterIntraCdf[ BLOCK_SIZES ][ 3 ]
    autostruct uint16 SavedCompoundIdxCdf[ COMPOUND_IDX_CONTEXTS ][ 3 ]
    autostruct uint16 SavedCompGroupIdxCdf[ COMP_GROUP_IDX_CONTEXTS ][ 3 ]
    autostruct uint16 SavedCompoundTypeCdf[ BLOCK_SIZES ][ COMPOUND_TYPES + 1 ]
    autostruct uint16 SavedInterIntraModeCdf[ BLOCK_SIZE_GROUPS - 1 ][ INTERINTRA_MODES + 1 ]
    autostruct uint16 SavedWedgeIndexCdf[ BLOCK_SIZES ][ 16 + 1 ]
    autostruct uint16 SavedCflAlphaCdf[ CFL_ALPHA_CONTEXTS ][ CFL_ALPHABET_SIZE + 1 ]
    autostruct uint16 SavedDeltaLFMultiCdf[ FRAME_LF_COUNT ][ DELTA_LF_SMALL + 2 ]
    autostruct uint16 SavedUseWienerCdf[ 3 ]
    autostruct uint16 SavedUseSgrprojCdf[ 3 ]
    autostruct uint16 SavedRestorationTypeCdf[ RESTORE_SWITCHABLE + 1 ]
    autostruct uint16 SavedEobPt16Cdf[ PLANE_TYPES ][ 2 ][ 6 ]
    autostruct uint16 SavedEobPt32Cdf[ PLANE_TYPES ][ 2 ][ 7 ]
    autostruct uint16 SavedEobPt64Cdf[ PLANE_TYPES ][ 2 ][ 8 ]
    autostruct uint16 SavedEobPt128Cdf[ PLANE_TYPES ][ 2 ][ 9 ]
    autostruct uint16 SavedEobPt256Cdf[ PLANE_TYPES ][ 2 ][ 10 ]
    autostruct uint16 SavedEobPt512Cdf[ PLANE_TYPES ][ 11 ]
    autostruct uint16 SavedEobPt1024Cdf[ PLANE_TYPES ][ 12 ]
    autostruct uint16 SavedTxbSkipCdf[ TX_SIZES ][ TXB_SKIP_CONTEXTS ][ 3 ]
    autostruct uint16 SavedEobExtraCdf[ TX_SIZES ][ PLANE_TYPES ][ EOB_COEF_CONTEXTS ][ 3 ]
    autostruct uint16 SavedDcSignCdf[ PLANE_TYPES ][ DC_SIGN_CONTEXTS ][ 3 ]
    autostruct uint16 SavedCoeffBaseEobCdf[ TX_SIZES ][ PLANE_TYPES ][ SIG_COEF_CONTEXTS_EOB ][ 4 ]
    autostruct uint16 SavedCoeffBaseCdf[ TX_SIZES ][ PLANE_TYPES ][ SIG_COEF_CONTEXTS ][ 5 ]
    autostruct uint16 SavedCoeffBrCdf[ TX_SIZES ][ PLANE_TYPES ][ LEVEL_CONTEXTS ][ BR_CDF_SIZE + 1 ]
    autostruct uint16 SavedAngleDeltaCdf[ DIRECTIONAL_MODES ][ (2 * MAX_ANGLE_DELTA + 1) + 1 ]

    autostruct uint1 StoredCdfInitialised[ NUM_REF_FRAMES ]
    autostruct uint16 StoredYModeCdf[ NUM_REF_FRAMES ][ BLOCK_SIZE_GROUPS ][ INTRA_MODES + 1 ]
    autostruct uint16 StoredUVModeCflNotAllowedCdf[ NUM_REF_FRAMES ][ INTRA_MODES ][ UV_INTRA_MODES_CFL_NOT_ALLOWED + 1 ]
    autostruct uint16 StoredUVModeCflAllowedCdf[ NUM_REF_FRAMES ][ INTRA_MODES ][ UV_INTRA_MODES_CFL_ALLOWED + 1 ]
    autostruct uint16 StoredIntrabcCdf[ NUM_REF_FRAMES ][ 2 + 1 ]
    autostruct uint16 StoredPartitionW8Cdf[ NUM_REF_FRAMES ][ PARTITION_CONTEXTS ][ 4 + 1 ]
    autostruct uint16 StoredPartitionW16Cdf[ NUM_REF_FRAMES ][ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 StoredPartitionW32Cdf[ NUM_REF_FRAMES ][ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 StoredPartitionW64Cdf[ NUM_REF_FRAMES ][ PARTITION_CONTEXTS ][ 10 + 1 ]
    autostruct uint16 StoredPartitionW128Cdf[ NUM_REF_FRAMES ][ PARTITION_CONTEXTS ][ 8 + 1 ]
    autostruct uint16 StoredSegmentIdCdf[ NUM_REF_FRAMES ][ SEGMENT_ID_CONTEXTS ][ MAX_SEGMENTS + 1 ]
    autostruct uint16 StoredSegmentIdPredictedCdf[ NUM_REF_FRAMES ][ SEGMENT_ID_PREDICTED_CONTEXTS ][ 3 ]
    autostruct uint16 StoredTx8x8Cdf[ NUM_REF_FRAMES ][ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 1 ]
    autostruct uint16 StoredTx16x16Cdf[ NUM_REF_FRAMES ][ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 StoredTx32x32Cdf[ NUM_REF_FRAMES ][ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 StoredTx64x64Cdf[ NUM_REF_FRAMES ][ TX_SIZE_CONTEXTS ][ MAX_TX_DEPTH + 2 ]
    autostruct uint16 StoredTxfmSplitCdf[ NUM_REF_FRAMES ][ TXFM_PARTITION_CONTEXTS ][ 3 ]
    autostruct uint16 StoredFilterIntraModeCdf[ NUM_REF_FRAMES ][ 6 ]
    autostruct uint16 StoredFilterIntraCdf[ NUM_REF_FRAMES ][ BLOCK_SIZES ][ 3 ]
    autostruct uint16 StoredInterpFilterCdf[ NUM_REF_FRAMES ][ INTERP_FILTER_CONTEXTS ][ INTERP_FILTERS + 1 ]
    autostruct uint16 StoredMotionModeCdf[ NUM_REF_FRAMES ][ BLOCK_SIZES ][ MOTION_MODES + 1 ]
    autostruct uint16 StoredNewMvCdf[ NUM_REF_FRAMES ][ NEW_MV_CONTEXTS ][ 3 ]
    autostruct uint16 StoredZeroMvCdf[ NUM_REF_FRAMES ][ ZERO_MV_CONTEXTS ][ 3 ]
    autostruct uint16 StoredRefMvCdf[ NUM_REF_FRAMES ][ REF_MV_CONTEXTS ][ 3 ]
    autostruct uint16 StoredCompoundModeCdf[ NUM_REF_FRAMES ][ COMPOUND_MODE_CONTEXTS ][ COMPOUND_MODES + 1 ]
    autostruct uint16 StoredDrlModeCdf[ NUM_REF_FRAMES ][ DRL_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 StoredIsInterCdf[ NUM_REF_FRAMES ][ IS_INTER_CONTEXTS ][ 3 ]
    autostruct uint16 StoredCompModeCdf[ NUM_REF_FRAMES ][ COMP_INTER_CONTEXTS ][ 3 ]
    autostruct uint16 StoredSkipModeCdf[ NUM_REF_FRAMES ][ SKIP_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 StoredSkipCdf[ NUM_REF_FRAMES ][ SKIP_CONTEXTS ][ 3 ]
    autostruct uint16 StoredCompRefCdf[ NUM_REF_FRAMES ][ REF_CONTEXTS ][ FWD_REFS - 1 ][ 3 ]
    autostruct uint16 StoredCompBwdRefCdf[ NUM_REF_FRAMES ][ REF_CONTEXTS ][ BWD_REFS - 1 ][ 3 ]
    autostruct uint16 StoredSingleRefCdf[ NUM_REF_FRAMES ][ REF_CONTEXTS ][ SINGLE_REFS - 1 ][ 3 ]
    autostruct uint16 StoredMvJointCdf[ NUM_REF_FRAMES ][ MV_CONTEXTS ][ MV_JOINTS + 1 ]
    autostruct uint16 StoredMvClassCdf[ NUM_REF_FRAMES ][ MV_CONTEXTS ][ 2 ][ MV_CLASSES + 1 ]
    autostruct uint16 StoredMvClass0BitCdf[ NUM_REF_FRAMES ][ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 StoredMvFrCdf[ NUM_REF_FRAMES ][ MV_CONTEXTS ][ 2 ][ MV_JOINTS + 1 ]
    autostruct uint16 StoredMvClass0FrCdf[ NUM_REF_FRAMES ][ MV_CONTEXTS ][ 2 ][ CLASS0_SIZE ][ MV_JOINTS + 1 ]
    autostruct uint16 StoredMvClass0HpCdf[ NUM_REF_FRAMES ][ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 StoredMvBitCdf[ NUM_REF_FRAMES ][ MV_CONTEXTS ][ 2 ][ MV_OFFSET_BITS ][ 3 ]
    autostruct uint16 StoredMvHpCdf[ NUM_REF_FRAMES ][ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 StoredMvSignCdf[ NUM_REF_FRAMES ][ MV_CONTEXTS ][ 2 ][ 3 ]
    autostruct uint16 StoredPaletteYModeCdf[ NUM_REF_FRAMES ][ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_Y_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 StoredPaletteYSizeCdf[ NUM_REF_FRAMES ][ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_SIZES + 1 ]
    autostruct uint16 StoredPaletteUVSizeCdf[ NUM_REF_FRAMES ][ PALETTE_BLOCK_SIZE_CONTEXTS ][ PALETTE_SIZES + 1 ]
    autostruct uint16 StoredPaletteUVModeCdf[ NUM_REF_FRAMES ][ PALETTE_UV_MODE_CONTEXTS ][ 3 ]
    autostruct uint16 StoredPaletteSize2YColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 3 ]
    autostruct uint16 StoredPaletteSize2UVColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 3 ]
    autostruct uint16 StoredPaletteSize3YColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 4 ]
    autostruct uint16 StoredPaletteSize3UVColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 4 ]
    autostruct uint16 StoredPaletteSize4YColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 5 ]
    autostruct uint16 StoredPaletteSize4UVColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 5 ]
    autostruct uint16 StoredPaletteSize5YColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 6 ]
    autostruct uint16 StoredPaletteSize5UVColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 6 ]
    autostruct uint16 StoredPaletteSize6YColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 7 ]
    autostruct uint16 StoredPaletteSize6UVColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 7 ]
    autostruct uint16 StoredPaletteSize7YColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 8 ]
    autostruct uint16 StoredPaletteSize7UVColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 8 ]
    autostruct uint16 StoredPaletteSize8YColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 9 ]
    autostruct uint16 StoredPaletteSize8UVColorCdf[ NUM_REF_FRAMES ][ PALETTE_COLOR_CONTEXTS ][ 9 ]
    autostruct uint16 StoredDeltaQCdf[ NUM_REF_FRAMES ][ DELTA_Q_SMALL + 2 ]
    autostruct uint16 StoredDeltaLFCdf[ NUM_REF_FRAMES ][ DELTA_LF_SMALL + 2 ]
    autostruct uint16 StoredIntraTxTypeSet1Cdf[ NUM_REF_FRAMES ][ 2 ][ INTRA_MODES ][ 8 ]
    autostruct uint16 StoredIntraTxTypeSet2Cdf[ NUM_REF_FRAMES ][ 3 ][ INTRA_MODES ][ 8 ]
    autostruct uint16 StoredInterTxTypeSet1Cdf[ NUM_REF_FRAMES ][ 2 ][ 17 ]
    autostruct uint16 StoredInterTxTypeSet2Cdf[ NUM_REF_FRAMES ][ 13 ]
    autostruct uint16 StoredInterTxTypeSet3Cdf[ NUM_REF_FRAMES ][ 4 ][ 3 ]
    autostruct uint16 StoredUseObmcCdf[ NUM_REF_FRAMES ][ BLOCK_SIZES ][ 3 ]
    autostruct uint16 StoredInterIntraCdf[ NUM_REF_FRAMES ][ BLOCK_SIZE_GROUPS - 1 ][ 3 ]
    autostruct uint16 StoredCompRefTypeCdf[ NUM_REF_FRAMES ][ COMP_REF_TYPE_CONTEXTS ][ 3 ]
    autostruct uint16 StoredCflSignCdf[ NUM_REF_FRAMES ][ CFL_JOINT_SIGNS + 1 ]
    autostruct uint16 StoredUniCompRefCdf[ NUM_REF_FRAMES ][ REF_CONTEXTS ][ UNIDIR_COMP_REFS - 1 ][ 3 ]
    autostruct uint16 StoredWedgeInterIntraCdf[ NUM_REF_FRAMES ][ BLOCK_SIZES ][ 3 ]
    autostruct uint16 StoredCompoundIdxCdf[ NUM_REF_FRAMES ][ COMPOUND_IDX_CONTEXTS ][ 3 ]
    autostruct uint16 StoredCompGroupIdxCdf[ NUM_REF_FRAMES ][ COMP_GROUP_IDX_CONTEXTS ][ 3 ]
    autostruct uint16 StoredCompoundTypeCdf[ NUM_REF_FRAMES ][ BLOCK_SIZES ][ COMPOUND_TYPES + 1 ]
    autostruct uint16 StoredInterIntraModeCdf[ NUM_REF_FRAMES ][ BLOCK_SIZE_GROUPS - 1 ][ INTERINTRA_MODES + 1 ]
    autostruct uint16 StoredWedgeIndexCdf[ NUM_REF_FRAMES ][ BLOCK_SIZES ][ 16 + 1 ]
    autostruct uint16 StoredCflAlphaCdf[ NUM_REF_FRAMES ][ CFL_ALPHA_CONTEXTS ][ CFL_ALPHABET_SIZE + 1 ]
    autostruct uint16 StoredDeltaLFMultiCdf[ NUM_REF_FRAMES ][ FRAME_LF_COUNT ][ DELTA_LF_SMALL + 2 ]
    autostruct uint16 StoredUseWienerCdf[ NUM_REF_FRAMES ][ 3 ]
    autostruct uint16 StoredUseSgrprojCdf[ NUM_REF_FRAMES ][ 3 ]
    autostruct uint16 StoredRestorationTypeCdf[ NUM_REF_FRAMES ][ RESTORE_SWITCHABLE + 1 ]
    autostruct uint16 StoredEobPt16Cdf[ NUM_REF_FRAMES ][ PLANE_TYPES ][ 2 ][ 6 ]
    autostruct uint16 StoredEobPt32Cdf[ NUM_REF_FRAMES ][ PLANE_TYPES ][ 2 ][ 7 ]
    autostruct uint16 StoredEobPt64Cdf[ NUM_REF_FRAMES ][ PLANE_TYPES ][ 2 ][ 8 ]
    autostruct uint16 StoredEobPt128Cdf[ NUM_REF_FRAMES ][ PLANE_TYPES ][ 2 ][ 9 ]
    autostruct uint16 StoredEobPt256Cdf[ NUM_REF_FRAMES ][ PLANE_TYPES ][ 2 ][ 10 ]
    autostruct uint16 StoredEobPt512Cdf[ NUM_REF_FRAMES ][ PLANE_TYPES ][ 11 ]
    autostruct uint16 StoredEobPt1024Cdf[ NUM_REF_FRAMES ][ PLANE_TYPES ][ 12 ]
    autostruct uint16 StoredTxbSkipCdf[ NUM_REF_FRAMES ][ TX_SIZES ][ TXB_SKIP_CONTEXTS ][ 3 ]
    autostruct uint16 StoredEobExtraCdf[ NUM_REF_FRAMES ][ TX_SIZES ][ PLANE_TYPES ][ EOB_COEF_CONTEXTS ][ 3 ]
    autostruct uint16 StoredDcSignCdf[ NUM_REF_FRAMES ][ PLANE_TYPES ][ DC_SIGN_CONTEXTS ][ 3 ]
    autostruct uint16 StoredCoeffBaseEobCdf[ NUM_REF_FRAMES ][ TX_SIZES ][ PLANE_TYPES ][ SIG_COEF_CONTEXTS_EOB ][ 4 ]
    autostruct uint16 StoredCoeffBaseCdf[ NUM_REF_FRAMES ][ TX_SIZES ][ PLANE_TYPES ][ SIG_COEF_CONTEXTS ][ 5 ]
    autostruct uint16 StoredCoeffBrCdf[ NUM_REF_FRAMES ][ TX_SIZES ][ PLANE_TYPES ][ LEVEL_CONTEXTS ][ BR_CDF_SIZE + 1 ]
    autostruct uint16 StoredAngleDeltaCdf[ NUM_REF_FRAMES ][ DIRECTIONAL_MODES ][ (2 * MAX_ANGLE_DELTA + 1) + 1 ]
    autostruct int extra_bit
    autostruct uint16 buf
    autostruct uint13 TileGroupCount
    autostruct uint_0_128 TileGroupCountNonLevelMax
//}
    autostruct uint1 isAnnexB
    autostruct int obu_size
//obu_header( ) {
    autostruct uint1 obu_forbidden_bit
    autostruct uint4 obu_type
    autostruct uint2 obu_reserved_2bits
    autostruct uint1 obu_extension_flag
    autostruct uint1 obu_has_size_field
//}
//
//obu_extension_header( ) {
    autostruct uint3 temporal_id
    autostruct int_(-1)_7 TemporalIds[MAX_SPATIAL_ID+1]
    autostruct uint2 spatial_id
    autostruct uint2 quality_id
    autostruct uint1 reserved_flag
//}
//
//sequence_header_obu( ) {
    autostruct uint2 seq_profile
    autostruct uint1 still_picture
    autostruct uint1 reduced_still_picture_hdr
    autostruct uint1 timing_info_present_flag
    autostruct uint1 decoder_model_info_present_flag
    autostruct uint1 decoder_model_present_for_this_op[ 32 ]
    autostruct uint1 initial_display_delay_present_flag
    autostruct uint1 initial_display_delay_present_for_this_op[ 32 ]
    autostruct uint1 equal_picture_interval
    autostruct uint2 max_spatial_layers_cnt
    autostruct uint1 profile_low_bit
    autostruct uint1 profile_high_bit
    autostruct uint1 enable_order_hint
    autostruct uint1 enable_jnt_comp
    autostruct uint1 enable_ref_frame_mvs
    autostruct uint1 seq_force_screen_content_tools
    autostruct uint5 seq_level_idx[ 32 ]
    autostruct uint1 seq_tier[ 32 ]
    autostruct uint4 frame_width_bits_minus_1
    autostruct uint4 frame_height_bits_minus_1
    autostruct int max_frame_width_minus_1
    autostruct int max_frame_height_minus_1
    autostruct uint1 frame_id_numbers_present_flag
    autostruct uint4 additional_frame_id_length_minus_1
    autostruct uint4 delta_frame_id_length_minus_2
    autostruct uint1 enable_superres
    autostruct uint1 enable_cdef
    autostruct uint1 enable_restoration
    autostruct uint1 film_grain_params_present
    autostruct int operating_point_idc[ 32 ]
    autostruct int decode_to_display_rate_ratio[ 32 ]
    autostruct int initial_display_delay[ 32 ]
    autostruct int extra_frame_buffers[ 32 ]
    autostruct uint1 decoder_rate_model_param_present_flag[ 32 ]
    autostruct int operating_points_cnt_minus_1  // Note this can take the value -1
//}
// decoder_model( ) {
    autostruct uint5 buffer_delay_length_minus_1
    autostruct uint32 num_units_in_decoding_tick
    autostruct uint5 buffer_removal_time_length_minus_1
    autostruct uint5 frame_presentation_time_length_minus_1
//}
//
//color_config( ) {
    autostruct uint1 ten_or_twelve_bit
    autostruct uint1 mono_chrome
    autostruct int NumPlanes
    autostruct uint5 color_space
    autostruct uint5 transfer_function
    autostruct uint1 color_range
    autostruct uint1 subsampling_x
    autostruct uint1 subsampling_y
    autostruct uint2 chroma_sample_position
    autostruct uint1 separate_uv_delta_q
    autostruct uint8 matrix_coefficients
    autostruct uint8 color_primaries
    autostruct uint8 transfer_characteristics
//}
//
//metadata_hdr_cll( ) {
    autostruct uint16 max_cll
    autostruct uint16 max_fall
//}
//
//metadata_hdr_mdcv( ) {
    autostruct uint16 primary_chromaticity_x[ 3 ]
    autostruct uint16 primary_chromaticity_y[ 3 ]
    autostruct uint16 white_point_chromaticity_x
    autostruct uint16 white_point_chromaticity_y
    autostruct uint32 luminance_max
    autostruct uint32 luminance_min
//}
//
//scalability_structure( ) {
    autostruct uint1 CVSTemporalGroupDescriptionPresentFlag
    autostruct int spatial_layer_max_width[ 4 ]
    autostruct int spatial_layer_max_height[ 4 ]
    autostruct int spatial_layer_ref_id[ 4 ]
    autostruct int temporal_group_temporal_id[ 255 ]
    autostruct int temporal_group_temporal_switching_up_point_flag[ 255 ]
    autostruct int temporal_group_spatial_switching_up_point_flag[ 255 ]
    autostruct int temporal_group_ref_cnt[ 255 ]
    autostruct int temporal_group_ref_pic_diff[ 255 ][ 7 ]
//}
//
//mode_info( ) {
    autostruct uint_0_13 uv_mode
    autostruct uint4 wedge_index
    autostruct int use_intrabc
//}
//
//filter_intra_mode_info( ) {
    autostruct uint1 use_filter_intra
    autostruct uint_0_4 filter_intra_mode
//}
//
//uncompressed_header( ) {
    autostruct uint1 show_existing_frame
    autostruct uint3 frame_to_show_map_idx
    autostruct int display_frame_id
    autostruct uint2 frame_type
    autostruct uint1 show_frame
    autostruct uint1 error_resilient_mode
    autostruct uint1 enable_intra_edge_filter
    autostruct uint1 enable_filter_intra
    autostruct uint1 disable_cdf_update
    autostruct uint1 allow_intrabc
    autostruct int current_frame_id
    autostruct uint1 frame_size_override_flag
    autostruct uint1 use_128x128_superblock
    autostruct uint1 enable_dual_filter
    autostruct uint1 allow_screen_content_tools
    autostruct uint8 refresh_frame_flags
    autostruct uint3 last_frame_idx
    autostruct uint3 gold_frame_idx
    autostruct int_(-1)_7 ref_frame_idx[ REFS_PER_FRAME ] // Note this can take values from -1 to 7 (-1 used in short signalling)
    autostruct uint1 RefFrameSignBias[ REFS_PER_FRAME + LAST_FRAME ]
    autostruct int delta_frame_id_minus_1
    autostruct uint1 allow_high_precision_mv
    autostruct uint1 is_motion_mode_switchable
    autostruct uint1 use_ref_frame_mvs
    autostruct uint3 primary_ref_frame
    autostruct uint1 disable_frame_end_update_cdf
    autostruct uint3 frame_context_idx
    autostruct uint1 allow_warped_motion
    autostruct uint1 reduced_tx_set
    autostruct uint1 showable_frame
    autostruct int seq_force_integer_mv
    autostruct int7 DeltaQYDc
    autostruct int7 DeltaQUDc
    autostruct int7 DeltaQUAc
    autostruct int7 DeltaQVDc
    autostruct int7 DeltaQVAc
//}
//
//frame_size( ) {
    autostruct int frame_width_minus_1
    autostruct int frame_height_minus_1
//}
//
//render_size( ) {
    autostruct uint1 render_and_frame_size_different
    autostruct uint16 render_width_minus_1
    autostruct uint16 render_height_minus_1
//}
//
//frame_size_with_refs( ) {
    autostruct uint1 found_ref
//}
//
//superres_params() {
    autostruct uint1 use_superres
    autostruct uint_8_16 SuperresDenom
    autostruct int UpscaledWidth
//}
//
//read_interpolation_filter( ) {
    autostruct uint1 is_filter_switchable
    autostruct uint2 interpolation_filter
//}
//
//loop_filter_params( ) {
    autostruct uint6 loop_filter_level[ 4 ]
    autostruct uint3 loop_filter_sharpness
    autostruct uint1 loop_filter_delta_enabled
    autostruct uint1 loop_filter_delta_update
    autostruct int7 loop_filter_ref_deltas[ TOTAL_REFS_PER_FRAME ]
    autostruct int7 loop_filter_mode_deltas[ TOTAL_REFS_PER_FRAME ]
//}
//
//metadata_argon_design_lst( ) {
    autostruct uint_1_128 num_large_scale_tile_anchor_frames
    autostruct int num_large_scale_tile_tile_list_obus
//}
//
//quantization_params( ) {
    autostruct uint8 base_q_idx
    autostruct uint1 using_qmatrix
    autostruct uint4 min_qmlevel
    autostruct uint4 max_qmlevel
    autostruct uint4 qm_y
    autostruct uint4 qm_u
    autostruct uint4 qm_v
//}
//
//segmentation_params( ) {
    autostruct int SegIdPreSkip
    autostruct int LastActiveSegId
    autostruct uint1 segmentation_enabled
    autostruct uint1 segmentation_update_map
    autostruct uint1 segmentation_temporal_update
    autostruct uint1 segmentation_update_data
//}
//
//tile_info ( ) {
    autostruct uint1 uniform_tile_spacing_flag
    autostruct uint2 tile_size_bytes_minus_1
    autostruct int context_update_tile_id
//}
//
//delta_q_params( ) {
    autostruct uint1 delta_q_present
    autostruct uint2 delta_q_res
//}
//
//delta_lf_params( ) {
    autostruct uint1 delta_lf_present
    autostruct uint2 delta_lf_res
    autostruct uint1 delta_lf_multi
//}
//
//cdef_params( ) {
    autostruct uint2 cdef_damping_minus_3
    autostruct uint2 cdef_bits
    autostruct uint4 cdef_y_pri_strength[ 8 ]
    autostruct uint_0_4 cdef_y_sec_strength[ 8 ]
    autostruct uint4 cdef_uv_pri_strength[ 8 ]
    autostruct uint_0_4 cdef_uv_sec_strength[ 8 ]

    autostruct int4 cdef_idx [][]
//}
//
//skip_mode_params( ) {
    autostruct uint1 skip_mode_present
//}
//
//frame_reference_mode( ) {
    autostruct uint1 reference_select
//}
//
//compound_tools( ) {
    autostruct uint1 enable_interintra_compound
    autostruct uint1 enable_masked_compound
    autostruct uint1 enable_warped_motion
//}
//
//global_motion_params( ) {
    autostruct int PrevGmParams[ 8 ][ 6 ]
    autostruct int gm_params[ 8 ][ 6 ]
    autostruct int GmType[ 8 ]
//}
//
//film_grain_params( ) {
    autostruct uint1 override_no_film_grain
    autostruct int apply_grain
    autostruct int grain_seed
    autostruct int update_grain
    autostruct int film_grain_params_ref_idx
    autostruct uint_0_14 num_y_points
    autostruct uint_0_10 num_cb_points
    autostruct uint_0_10 num_cr_points
    autostruct int chroma_scaling_from_luma
    autostruct int grain_scaling_minus_8
    autostruct int ar_coeff_lag
    autostruct int ar_coeff_shift_minus_6
    autostruct int grain_scale_shift
    autostruct int cb_mult
    autostruct int cb_luma_mult
    autostruct int cb_offset
    autostruct int cr_mult
    autostruct int cr_luma_mult
    autostruct int cr_offset
    autostruct int overlap_flag
    autostruct int clip_to_restricted_range
    autostruct int point_y_value[ 16 ]
    autostruct int point_y_scaling[ 16 ]
    autostruct int point_cb_value[ 16 ]
    autostruct int point_cb_scaling[ 16 ]
    autostruct int point_cr_value[ 16 ]
    autostruct int point_cr_scaling[ 16 ]
    autostruct int ar_coeffs_y_plus_128[ 24 ]
    autostruct int ar_coeffs_cb_plus_128[ 25 ]
    autostruct int ar_coeffs_cr_plus_128[ 25 ]
//}
//tile_list_obu( ) {
    autostruct uint8 output_frame_width_in_tiles_minus_1
    autostruct uint8 output_frame_height_in_tiles_minus_1
    autostruct uint16 tile_count_minus_1
//}
//
//tile_list_entry( ) {
    autostruct uint8 anchor_frame_idx
    autostruct uint8 anchor_tile_row
    autostruct uint8 anchor_tile_col
    autostruct uint16 tile_data_size_minus_1
    autostruct int coded_tile_data
//}
//
//tile_group_obu( ) {
    autostruct int tg_start
    autostruct int tg_end
//}
//
//read_skip_mode( ) {
    autostruct uint1 skip_mode
//}
//
//read_skip( ) {
    autostruct uint1 skip
//}
//
//read_tx_size( ) {
    autostruct int tx_size
//}
//
//inter_block_mode_info( ) {
    autostruct uint_0_2 interp_filter[ 2 ]
//}
//
//read_interintra_mode( ) {
    autostruct uint1 interintra
    autostruct uint_0_2 interintra_mode
    autostruct uint1 wedge_interintra
//}
//
//read_compound_type( ) {
    autostruct uint_0_4 compound_type
    autostruct uint1 wedge_sign
    autostruct uint1 mask_type
    autostruct uint1 comp_group_idx
    autostruct uint1 compound_idx
//}
//
//read_mv( ) {
    autostruct uint2 mv_joint
//}
//
//palette_mode_info( ) {
    autostruct uint12 palette_colors_y[ 8 ]
    autostruct uint12 palette_colors_u[ 8 ]
    autostruct uint12 palette_colors_v[ 8 ]
//}

    autostruct uint_0_2 motion_mode
    autostruct int comp_pred_mode
    autostruct int force_integer_mv
    autostruct int min_tx_size
    autostruct int inter_mode

    autostruct int_(-23)_46 RefLrWiener[ 3 ][ 2 ][ WIENER_COEFFS ]
    autostruct int_(-96)_95 RefSgrXqd[ 3 ][ 2 ]
    // TODO LrWiener harder to resize, as it's passed to wiener_coefficient
    autostruct int_(-23)_46 LrWiener[ 3 ][ MAX_HEIGHT >> 6 ][ MAX_WIDTH >> 6 ][ 2 ][ WIENER_COEFFS ]
    autostruct int rLrSgrSet[ 3 ][ MAX_LR_UNITS_IN_FRAME ]
    rewrite LrSgrSet[ i ][ r ][ c ] rLrSgrSet[ i ][ CheckStride(c, r, ( Stride + 63 ) / 64, MAX_LR_UNITS_IN_FRAME) ]
    autostruct int_(-96)_95 rLrSgrXqd[ 3 ][ MAX_LR_UNITS_IN_FRAME ][ 2 ]
    rewrite LrSgrXqd[ i ][ r ][ c ][ j ] rLrSgrXqd[ i ][ CheckStride(c, r, ( Stride + 63 ) / 64, MAX_LR_UNITS_IN_FRAME) ][ j ]

    autostruct int MiddleVertPartition

    // noiseStripe and noiseImage should be local, but give stack overflow
    autostruct int    noiseStripe [][][][]
    autostruct int16  noiseImage  [][][]

    autostruct int LevelIdx

    autostruct uint32 LumaSamplesInTU
    autostruct uint32 FramesDecodedInTU
    autostruct uint32 DisplayedLumaSamplesInTU
    autostruct uint32 FramesDisplayedInTU
    autostruct uint32 TargetDisplayFrameRate
    autostruct uint32 TargetDecodeFrameRate

    autostruct uint32 BytesInFrame
#if !COVER
    // Set if this frame should contribute to frame_compressed_sizes
    // and frame_luma_sample_counts for this TU. False for dropped
    // frames or large-scale-tile frames.
    autostruct uint1  count_frame_size

    // Per-frame byte counters that will be used to compute
    // compression ratios and check bitstream conformance at the end
    // of the TU.
    autostruct uint64 frame_compressed_sizes   [MAX_SCALABILITY_STRUCTURE_SIZE]
    autostruct uint64 frame_luma_sample_counts [MAX_SCALABILITY_STRUCTURE_SIZE]
    autostruct int    frame_size_counter
#endif // #!COVER

    // WRITE_YUV
    autostruct int outputBytes

    // coverage helpers
    autostruct uint1 doingTxfmRow
    autostruct uint1 doingTxfmDCT
    autostruct int_(-4096)_4096 gmParamsPreShift

    // used to check validity of sequence header and scalability changes
    autostruct uint1 sequenceHeaderChanged
    autostruct int64 prevSequenceHeaderSize
    autostruct uint8 prevSequenceHeader[MAX_SEQUENCE_HEADER_SIZE]
    autostruct uint8 curSequenceHeader[MAX_SEQUENCE_HEADER_SIZE]
    autostruct uint1 scalabilityStructureChanged
    autostruct int64 prevScalabilityStructureSize
    autostruct uint8 prevScalabilityStructure[MAX_SCALABILITY_STRUCTURE_SIZE]
    autostruct uint8 curScalabilityStructure[MAX_SCALABILITY_STRUCTURE_SIZE]

    // implementation of choose_operating_point()
    autostruct int DesiredOpPoint

    // Used to find the maximum number of operating points in this stream
    //    -1 means not active, >=0 active
    autostruct int max_num_oppoints

#if !COVER
    autostruct int estimateCmd
#endif // !COVER
}

ivf_file_header( ) {
    autostruct int header
    autostruct int ivf_version
    autostruct int headersize
    autostruct int fourcc
    autostruct int g_w
    autostruct int g_h
    autostruct int g_timebase_den
    autostruct int g_timebase_num
    autostruct int frame_cnt
    autostruct int ivf_unused
}

ivf_frame_header( ) {
    autostruct int frame_sz
    autostruct int ivf_timebase_low
    autostruct int ivf_timebase_high
}
