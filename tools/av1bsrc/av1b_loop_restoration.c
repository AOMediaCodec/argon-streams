/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS 0
#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_LOOP_RESTORATION_OUTPUT 1
#else
#define VALIDATE_SPEC_LOOP_RESTORATION_OUTPUT 0
#endif

loop_restoration( ) {
    for (x = 0; x < UpscaledWidth; x++) {
        for (y = 0; y < FrameHeight; y++) {
            LrFrame[ 0 ][ y ][ x ] = UpscaledCdefFrame[ 0 ][ y ][ x ]
        }
    }
    for (plane = 1; plane < NumPlanes; plane++) {
        for (x = 0; x < ((UpscaledWidth + COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)) >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x)); x++) {
            for (y = 0; y < ((FrameHeight + COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)) >> COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y)); y++) {
                LrFrame[ plane ][ y ][ x ] = UpscaledCdefFrame[ plane ][ y ][ x ]
            }
        }
    }
    if ( UsesLr ) {
        for ( plane = 0; plane < NumPlanes; plane++ ) {
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
          subX = (plane == 0) ? 0 : subsampling_x
          subY = (plane == 0) ? 0 : subsampling_y
          unitSize = LoopRestorationSize[ plane ]
          unitRows = count_units_in_frame( unitSize, Round2( FrameHeight, subY) )
          unitCols = count_units_in_frame( unitSize, Round2( UpscaledWidth, subX ) )
          planeW = Round2(UpscaledWidth, subX)
          planeH = Round2(FrameHeight, subY)
          for (unitRow = 0; unitRow < unitRows; unitRow++) {
            for (unitCol = 0; unitCol < unitCols; unitCol++) {
              offset = 8 >> subY
              y0 = unitRow * unitSize - offset
              y1 = (unitRow == unitRows - 1) ? planeH : y0 + unitSize
              x0 = unitCol * unitSize
              x1 = (unitCol == unitCols - 1) ? planeW : x0 + unitSize
              y0 = y0 << subY
              x0 = x0 << subX
              y1 = y1 << subY
              x1 = x1 << subX
              for (y = y0; y < y1; y += 64) {
                for (x = x0; x < x1; x += 64) {
                  if ( FrameRestorationType[ plane ] != RESTORE_NONE ) {
                    row = y >> MI_SIZE_LOG2
                    col = x >> MI_SIZE_LOG2
                    loop_restore_block( plane, row, col )
                  }
                }
              }
            }
          }
#else
          for ( y = 0; y < FrameHeight; y += MI_SIZE ) {
            for ( x = 0; x < UpscaledWidth; x += MI_SIZE ) {
              if ( FrameRestorationType[ plane ] != RESTORE_NONE ) {
                row = y >> MI_SIZE_LOG2
                col = x >> MI_SIZE_LOG2
                loop_restore_block( plane, row, col )
              }
            }
          }
#endif
        }
#if VALIDATE_SPEC_LOOP_RESTORATION_OUTPUT
        for ( plane = 0; plane < NumPlanes; plane++ ) {
            subX = (plane == 0) ? 0 : subsampling_x
            subY = (plane == 0) ? 0 : subsampling_y
            for ( i = 0; i < (FrameHeight + subY) >> subY; i++) {
                for ( j = 0; j < (UpscaledWidth + subX) >> subX; j++ ) {
                    validate(120000)
                    validate(plane)
                    validate(j)
                    validate(i)
                    validate( LrFrame[ plane ][ i ][ j ] )
                }
            }
        }
#endif
    }
}

loop_restore_block( plane, row, col ) {
    lumaY = row * MI_SIZE
    stripeNum = ( lumaY + 8 ) / 64
    subX = COVERCLASS((PROFILE0 || PROFILE2), 1, ( plane == 0 ) ? 0 : COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_x))
    subY = COVERCLASS((PROFILE0 || PROFILE2), 1, ( plane == 0 ) ? 0 : COVERCLASS((PROFILE0 || PROFILE2), (PROFILE1 || PROFILE2), subsampling_y))
    StripeStartY =  ( (-8 + stripeNum * 64) >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY) )
    StripeEndY = StripeStartY + (64 >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY)) - 1
    unitSize = LoopRestorationSize[ plane ]
    unitRows = count_units_in_frame( unitSize, Round2( FrameHeight, COVERCLASS((PROFILE0 || PROFILE2), 1, subY) ) )
    unitCols = count_units_in_frame( unitSize, Round2( UpscaledWidth, COVERCLASS((PROFILE0 || PROFILE2), 1, subX) ) )
    unitRow = Min( unitRows - 1, ( ( row * MI_SIZE + 8) >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY) ) / unitSize )
    unitCol = Min( unitCols - 1, ( col * MI_SIZE >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX) ) / unitSize )
    PlaneEndX = Round2( UpscaledWidth, COVERCLASS((PROFILE0 || PROFILE2), 1, subX) ) - 1
    PlaneEndY = Round2( FrameHeight, COVERCLASS((PROFILE0 || PROFILE2), 1, subY) ) - 1
    x = ( col * MI_SIZE >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX) )
    y = ( row * MI_SIZE >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY) )
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
    w = ( 64 >> subX )
    h = ( 64 >> subY )
    if (y<0) {
      h+=y  // Don't produce pixels off top of screen
      y=0
    }
#else
    w = ( MI_SIZE >> COVERCLASS((PROFILE0 || PROFILE2), 1, subX) )
    h = ( MI_SIZE >> COVERCLASS((PROFILE0 || PROFILE2), 1, subY) )
#endif
    w = Min( w, PlaneEndX - x + 1 )
    h = Min( h, PlaneEndY - y + 1 )
    rType = LrType[ plane ][ unitRow ][ unitCol ]
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
    if (rType != RESTORE_NONE) {
      validate(120006)
      validate(plane)
      validate(rType)
    }
#endif
    if ( rType == RESTORE_WIENER ) wiener_filter( plane, unitRow, unitCol, x, y, w, h )
    else if ( rType == RESTORE_SGRPROJ ) self_guided_filter( plane, unitRow, unitCol, x, y, w, h )
}

wiener_filter( plane, unitRow, unitCol, x, y, w, h ) {
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
    int intermediate[ 64+7 ][ 64 ]
#else
    int intermediate[ 11 ][ 4 ]
#endif
    int vfilter[ 7 ]
    int hfilter[ 7 ]
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
    validate(120001)
    validate(plane)
    validate(x)
    validate(y)
    validate(w)
    validate(h)
#endif
    rounding_variables_derivation( 0 )
    wiener_coefficient( LrWiener[ plane ][ unitRow ][ unitCol ][ 0 ], vfilter )
    wiener_coefficient( LrWiener[ plane ][ unitRow ][ unitCol ][ 1 ], hfilter )
    // TODO: Value coverage for each coefficient?
    offset = (1 << (BitDepth + FILTER_BITS - InterRound0 - 1))
    limit = (1 << (BitDepth + 1 + FILTER_BITS - InterRound0)) - 1
    for ( r = 0; r < h + 6; r++ ) {
        for ( c = 0; c < w; c++ ) {
            s = 0
            for ( t = 0; t < 7; t++ )
                s += hfilter[ t ] * get_source_sample( plane, x + c + t - 3, y + r - 3 ) // E-140 [RNG-Wiener1]
            v = Round2(s, InterRound0) // E-141 [RNG-Wiener2]
            intermediate[ r ][ c ] = Clip3( -offset, limit - offset, v)
        }
    }
    for ( r = 0; r < h; r++ ) {
        for ( c = 0; c < w; c++ ) {
            s = 0
            for ( t = 0; t < 7; t++ )
                s += vfilter[ t ] * intermediate[ r + t ][ c ] // E-142 [RNG-Wiener3]
            v = Round2( s, InterRound1 ) // E-143 [RNG-Wiener4]
            LrFrame[ plane ][ y + r ][ x + c ] = Clip1( v )
        }
    }
}

wiener_coefficient( int7pointer coeff, int32pointer filter ) {
    filter[ 3 ] = 128
    for ( i = 0; i < 3; i++ ) {
        c = coeff[ i ]
        filter[ i ] = c
        filter[ 6 - i ] = c
        filter[ 3 ] -= 2 * c
    }
}

int Sgr_Params[ (1 << SGRPROJ_PARAMS_BITS) ][ 4 ] = {
  { 2, 12, 1, 4 },  { 2, 15, 1, 6 },  { 2, 18, 1, 8 },  { 2, 21, 1, 9 },
  { 2, 24, 1, 10 }, { 2, 29, 1, 11 }, { 2, 36, 1, 12 }, { 2, 45, 1, 13 },
  { 2, 56, 1, 14 }, { 2, 68, 1, 15 }, { 0, 0, 1, 5 },   { 0, 0, 1, 8 },
  { 0, 0, 1, 11 },  { 0, 0, 1, 14 },  { 2, 30, 0, 0 },  { 2, 75, 0, 0 }
};

get_sgr_params(uint_0_15 xa, uint_0_3 xb) {
    COVERCROSS(CROSS_SGR_PARAMS, xa, xb)
    return Sgr_Params[ xa ][ xb ]
}

self_guided_filter( plane, unitRow, unitCol, x, y, w, h ) {
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
    int flt0[ 64*64 ]
    int flt1[ 64*64 ]
#else
    int flt0[ 16 ]
    int flt1[ 16 ]
#endif
    uint_0_15 set
    set = LrSgrSet[ plane ][ unitRow ][ unitCol ]
    COVERCROSS(VALUE_SGR_PARAM_SET, set)
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
    r0 = Sgr_Params[ set ][ 0 ]
    r1 = Sgr_Params[ set ][ 2 ]
	validate(120005)
    validate(r0)
    validate(r1)
#endif
    box_filter( plane, x, y, w, h, set, 0, flt0 )
    box_filter( plane, x, y, w, h, set, 1, flt1 )

    w0 = LrSgrXqd[ plane ][ unitRow ][ unitCol ][ 0 ]
    w1 = LrSgrXqd[ plane ][ unitRow ][ unitCol ][ 1 ]
    w2 = (1 << SGRPROJ_PRJ_BITS) - w0 - w1
#if COVER
    uint_0_3 xb
    xb = 0
    COVERCROSS(CROSS_SGR_PARAMS, set, xb)
    xb = 2
    COVERCROSS(CROSS_SGR_PARAMS, set, xb)
#endif // COVER
    r0 = Sgr_Params[ set ][ 0 ]
    r1 = Sgr_Params[ set ][ 2 ]
    for ( i = 0; i < h; i++ ) {
        for ( j = 0; j < w; j++ ) {
            u = UpscaledCdefFrame[ plane ][ y + i ][ x + j ] << SGRPROJ_RST_BITS
            v = w1 * u // E-363 [RNG-SelfGuided14]
            v1 = 0
            if ( r0 ) {
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
                v1 = w0 * flt0[ i * 64 + j ]
#else
                v1 = w0 * flt0[ i * 4 + j ]
#endif
            } else {
                v1 = w0 * u
            }
            v += v1 // E-364 [RNG-SelfGuided15]
            v2 = 0
            if ( r1 ) {
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
                v2 = w2 * flt1[ i * 64 + j ]
#else
                v2 = w2 * flt1[ i * 4 + j ]
#endif
            } else {
                v2 = w2 * u
            }
            v += v2
            sign = (v < 0) ? -1 : 1
            offset = (v < 0) ? 0 : 1
            vBits = sign * (CeilLog2(Abs(v + offset)) + 1) // E-365 [RNG-SelfGuided16]
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
            validate(120002)
            validate(plane)
            validate(x+j)
            validate(y+i)
            validate(w1)
            validate(w0)
            validate(u)
            if ( r0 ) {
                validate(0)
                validate(flt0[ i * 64 + j ])
            }
            if ( r1 ) {
                validate(1)
                validate(flt1[ i * 64 + j ])
            }
            validate(v)
#endif
            s = Round2( v, SGRPROJ_RST_BITS + SGRPROJ_PRJ_BITS )
            sign = (s < 0) ? -1 : 1
            offset = (s < 0) ? 0 : 1
            sBits = sign * (CeilLog2(Abs(s + offset)) + 1) // E-366 [RNG-SelfGuided17]
            LrFrame[ plane ][ y + i ][ x + j ] = Clip1( s )
        }
    }
}

box_filter( plane, x, y, w, h, uint_0_15 set, ppass, int32pointer out ) {
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
    int A[ 64+2 ][ 64+2 ]
    int B[ 64+2 ][ 64+2 ]
#else
    int A[ 6 ][ 6 ]
    int B[ 6 ][ 6 ]
#endif
#if COVER
    uint_0_3 xb
    xb = ppass * 2 + 0
    COVERCROSS(CROSS_SGR_PARAMS, set, xb)
#endif // COVER
    r = Sgr_Params[ set ][ ppass * 2 + 0 ]
    if ( r != 0 ) {
#if COVER
        xb = ppass * 2 + 1
        COVERCROSS(CROSS_SGR_PARAMS, set, xb)
#endif // COVER
        eps = Sgr_Params[ set ][ ppass * 2 + 1 ]
        n = ( 2 * r + 1 ) * ( 2 * r + 1 )
        oneOverN = ((1 << SGRPROJ_RECIP_BITS) + (n/2)) / n
        n2e = n * n * eps
        s = (((1 << SGRPROJ_MTABLE_BITS) + n2e / 2) / n2e)
        for ( i = -1; i < h + 1; i++ ) {
            for ( j = -1; j < w + 1; j++ ) {
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
                if ( ppass != 0 || (i&1) ) {
                  validate(120003)
                  validate(i)
                  validate(j)
                  valueUsed = 1
                } else {
                  valueUsed = 0
                }
#endif
                a = 0
                bb = 0
                for ( dy = -r ; dy <= r; dy++ ) {
                    for ( dx = -r; dx <= r; dx++ ) {
                        c = get_source_sample( plane, x + j + dx, y + i + dy )
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
                        if (valueUsed) {
                          validate(c)
                        }
#endif
                        a += c * c // E-350 [RNG-SelfGuided1]
                        bb += c // E-351 [RNG-SelfGuided2]
                    }
                }
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
                if (valueUsed) {
                  validate(a)
                  validate(bb)
                }
#endif
                a = Round2( a, 2 * (BitDepth - 8) ) // E-352 [RNG-SelfGuided3]
                d = Round2( bb, BitDepth - 8 ) // E-353 [RNG-SelfGuided4]
                int64 pp
                pp = Max( 0, a * n - d * d ) // E-354 [RNG-SelfGuided5]
                z = Round2( pp * s, SGRPROJ_MTABLE_BITS ) // E-355 [RNG-SelfGuided6]
                if ( z >= 255 )
                    a2 = 256
                else if ( z == 0 )
                    a2 = 1
                else
                    a2 = ((z << SGRPROJ_SGR_BITS) + (z/2)) / (z + 1)
                uint32 b2
                b2 = ( (1 << SGRPROJ_SGR_BITS) - a2 ) * bb * oneOverN // E-356 [RNG-SelfGuided7]
                A[ i+1 ][ j+1 ] = a2 // E-357 [RNG-SelfGuided8]
                B[ i+1 ][ j+1 ] = Round2( b2, SGRPROJ_RECIP_BITS ) // E-358 [RNG-SelfGuided9]
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
                if (valueUsed) {
                  validate(z)
                  validate(A[ i+1 ][ j+1 ])
                  validate(r)
                  validate(n)
                  validate(oneOverN)
                  validate(B[ i+1 ][ j+1 ])
                }
#endif
            }
        }
        for ( i = 0; i < h; i++ ) {
            shift = 5
            if ( ppass == 0 && ( i & 1 ) ) {
                shift = 4
            }
            for ( j = 0; j < w; j++ ) {
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
            validate(120004)
            validate(i)
            validate(j)
#endif
                a = 0
                bb = 0
                for ( dy = -1 ; dy <= 1; dy++ ) {
                    for ( dx = -1; dx <= 1; dx++ ) {
                        if ( ppass == 0 ) {
                            if ( (i + dy) & 1 ) {
                                weight = (dx == 0) ? 6 : 5
                            } else {
                                weight = 0
                            }
                        } else {
                            weight = (dx == 0 || dy == 0) ? 4 : 3
                        }
                        a += weight * A[ i + dy + 1 ][ j + dx + 1 ] // E-359 [RNG-SelfGuided10]
                        bb += weight * B[ i + dy + 1 ][ j + dx + 1 ] // E-360 [RNG-SelfGuided11]
                    }
                }
                v = a * UpscaledCdefFrame[ plane ][ y + i ][ x + j ] + bb // E-361 [RNG-SelfGuided12]
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
                out[ i * 64 + j ] = Round2( v, SGRPROJ_SGR_BITS + shift - SGRPROJ_RST_BITS)
#else
                out[ i * 4 + j ] = Round2( v, SGRPROJ_SGR_BITS + shift - SGRPROJ_RST_BITS) // E-362 [RNG-SelfGuided13]
#endif
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
                validate(a)
                validate(bb)
                validate(v)
                validate(shift)
                validate(out[ i * 64 + j ])
#endif
            }
        }
    }
}

get_source_sample( plane, x, y ) {
    x = Min(PlaneEndX, x)
    x = Max(0, x)
    y = Min(PlaneEndY, y)
    y = Max(0, y)
    if (y < StripeStartY) {
        y = Max(StripeStartY - 2,y)
        return UpscaledCurrFrame[ plane ][ y ][ x ]
    } else if (y > StripeEndY) {
        y = Min(StripeEndY + 2,y)
        return UpscaledCurrFrame[ plane ][ y ][ x ]
    } else {
        return UpscaledCdefFrame[ plane ][ y ][ x ]
    }
}
