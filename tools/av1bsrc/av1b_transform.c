/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_SPEC_TRANSFORM 0
#define IGNORE_TRANSFORM_RANGE 0

int Transform_Row_Shift[ TX_SIZES_ALL ] = {
  0, 1, 2, 2, 2, 0, 0, 1, 1,
  1, 1, 1, 1, 1, 1, 2, 2, 2, 2
};

inverse_transform_2d( uint_0_18 txSz ) {
    log2W = Tx_Width_Log2[ txSz ]
    log2H = Tx_Height_Log2[ txSz ]
    COVERCROSS(VALUE_TX_WIDTH_LOG2, txSz)
    COVERCROSS(VALUE_TX_HEIGHT_LOG2, txSz)
    w = 1 << log2W
    h = 1 << log2H
#if VALIDATE_SPEC_TRANSFORM
    validate(20002)
    validate(txSz)
    validate(PlaneTxType)
#endif
    rowShift = Lossless ? 0 : Transform_Row_Shift[ txSz ]
#if COVER
    if (!Lossless) {
      COVERCROSS(VALUE_TRANSFORM_ROW_SHIFT, txSz)
    }
#endif // COVER
    colShift = Lossless ? 0 : 4
    rowClampRange = BitDepth + 8
    colClampRange = Max( BitDepth + 6, 16 )

    // Row transforms
    doingTxfmRow = 1
    for (i = 0; i < h; i++) {
        for (j = 0; j < w; j++) {
            if ( i < 32 && j < 32 ) {
                T[ j ] = Dequant[ i ][ j ]
#if VALIDATE_SPEC_TRANSFORM
                validate(20001)
                validate(i)
                validate(j)
                validate( T[ j ] )
#endif
            }
            else
                T[ j ] = 0
            if ( Abs( log2W - log2H ) == 1 )
                T[ j ] = Round2( T[ j ] * 2896, 12 ) // E-301 [RNG-TxfmRow1]
        }
        if ( Lossless == 1 ) inverse_wht( 2 )
        else if ( PlaneTxType == DCT_DCT ||
                  PlaneTxType == ADST_DCT ||
                  PlaneTxType == FLIPADST_DCT ||
                  PlaneTxType == H_DCT ) {
            doingTxfmDCT = 1
            inverse_dct( log2W, rowClampRange )
        }
        else if ( PlaneTxType == DCT_ADST ||
                  PlaneTxType == ADST_ADST ||
                  PlaneTxType == DCT_FLIPADST ||
                  PlaneTxType == FLIPADST_FLIPADST ||
                  PlaneTxType == ADST_FLIPADST ||
                  PlaneTxType == FLIPADST_ADST ||
                  PlaneTxType == H_ADST ||
                  PlaneTxType == H_FLIPADST ) {
            doingTxfmDCT = 0
            inverse_adst( log2W, rowClampRange )
        }
        else inverse_identity_transform( log2W )
#if VALIDATE_SPEC_TRANSFORM
        validate(20006)
        for (j = 0; j < w; j++) {
            validate(T[ j ])
        }
        validate(20004)
#endif
        for (j = 0; j < w; j++) {
            Residual[ i ][ j ] = Round2( T[ j ], rowShift ) // E-302 [RNG-TxfmRow2]
#if VALIDATE_SPEC_TRANSFORM
            validate(Residual[ i ][ j ])
#endif
        }
    }
    for (j = 0; j < w; j++) {
        for (i = 0; i < h; i++) {
            Residual[ i ][ j ] = Clip3( - ( 1 << ( colClampRange - 1 ) ),
                                          ( 1 << ( colClampRange - 1 ) ) - 1,
                                          Residual[ i ][ j ] )
        }
    }
    // Column transforms
    doingTxfmRow = 0
    for (j = 0; j < w; j++) {
        for (i = 0; i < h; i++) {
              T[ i ] = Residual[ i ][ j ]
        }
        if ( Lossless == 1 ) inverse_wht( 0 )
        else if ( PlaneTxType == DCT_DCT ||
                  PlaneTxType == DCT_ADST ||
                  PlaneTxType == DCT_FLIPADST ||
                  PlaneTxType == V_DCT ) {
            doingTxfmDCT = 1
            inverse_dct( log2H, colClampRange )
        }
        else if ( PlaneTxType == ADST_DCT ||
                  PlaneTxType == ADST_ADST ||
                  PlaneTxType == FLIPADST_DCT ||
                  PlaneTxType == FLIPADST_FLIPADST ||
                  PlaneTxType == ADST_FLIPADST ||
                  PlaneTxType == FLIPADST_ADST ||
                  PlaneTxType == V_ADST ||
                  PlaneTxType == V_FLIPADST ) {
            doingTxfmDCT = 0
            inverse_adst( log2H, colClampRange )
        } else inverse_identity_transform( log2H )
#if VALIDATE_SPEC_TRANSFORM
        validate(20007)
        for (i = 0; i < h; i++) {
            validate(T[ i ])
        }
        validate(20005)
#endif
        for (i = 0; i < h; i++) {
            Residual[ i ][ j ] = Round2( T[ i ], colShift ) // E-303 [RNG-TxfmCol3]
#if VALIDATE_SPEC_TRANSFORM
            validate(Residual[ i ][ j ])
#endif
            if ( Lossless ) {
              CHECK( Residual[ i ][ j ] >= -( 1 << ( BitDepth ) ), "Inverse transform writes must be representable by an 1 + BitDepth signed integer" )
              CHECK( Residual[ i ][ j ] < ( 1 << ( BitDepth ) ), "Inverse transform writes must be representable by an 1 + BitDepth signed integer" )
            } else {
              numbits = Max( BitDepth + 5, 15 )
              ASSERT( Residual[ i ][ j ] >= -( 1 << ( numbits - 1 ) ), "Inverse transform writes always representable by Max( BitDepth + 5, 15 ) bits" )
              ASSERT( Residual[ i ][ j ] < ( 1 << ( numbits - 1 ) ), "Inverse transform writes always representable by Max( BitDepth + 5, 15 ) bits" )
            }
        }
    }
}

brev( numBits, x ) {
    t = 0
    for ( i = 0; i < numBits; i++ ) {
        bit = (x >> i) & 1
        t += bit << (numBits - 1 - i)
    }
    return t
}

int Cos128_Lookup[ 65 ] = {
    4096, 4095, 4091, 4085, 4076, 4065, 4052, 4036,
    4017, 3996, 3973, 3948, 3920, 3889, 3857, 3822,
    3784, 3745, 3703, 3659, 3612, 3564, 3513, 3461,
    3406, 3349, 3290, 3229, 3166, 3102, 3035, 2967,
    2896, 2824, 2751, 2675, 2598, 2520, 2440, 2359,
    2276, 2191, 2106, 2019, 1931, 1842, 1751, 1660,
    1567, 1474, 1380, 1285, 1189, 1092, 995, 897,
    799, 700, 601, 501, 401, 301, 201, 101, 0
};

cos128( angle ) {
#if COVER
    uint_0_64 angleIdx
#endif
    angle2 = angle & 255
    if ( angle2 <= 64 ) {
#if COVER
        angleIdx = angle2
        COVERCROSS(VALUE_COS128_LOOKUP, angleIdx)
#endif // COVER
        return Cos128_Lookup[ angle2 ]
    }
    if ( angle2 <= 128 ) {
#if COVER
        angleIdx = 128 - angle2
        COVERCROSS(VALUE_COS128_LOOKUP, angleIdx)
#endif // COVER
      return Cos128_Lookup[ 128 - angle2 ] * -1
    }
    if ( COVERCLASS( IMPOSSIBLE, 1, angle2 <= 192 ) ) {
#if COVER
        angleIdx = angle2 - 128
        COVERCROSS(VALUE_COS128_LOOKUP, angleIdx)
#endif // COVER
      return Cos128_Lookup[ angle2 - 128 ] * -1
    }
#if COVER
    angleIdx = 256 - angle2
    COVERCROSS(VALUE_COS128_LOOKUP, angleIdx)
#endif // COVER
    return Cos128_Lookup[ 256 - angle2 ]
}

sin128( angle ) {
    ret = cos128( angle - 64 )
    return ret
}

B( xa, xb, angle, rev, r ) {
    uint8 tmpAngle
    int64 tmp
    tmpAngle = angle & 255
    COVERCROSS(VALUE_BUTTERFLY_ANGLE, tmpAngle)

    int64 x
    int64 y
    x = T[ xa ] * cos128( angle ) - T[ xb ] * sin128( angle )
    y = T[ xa ] * sin128( angle ) + T[ xb ] * cos128( angle )

    // A note about these ranges:
    //
    // You might reasonably worry that these ranges need specialising for DCT
    // vs. ADST and/or for row vs. column. Indeed, the ranges are different and
    // if the customer's µarch uses different B instantiations for the different
    // cases, they might get it wrong without us spotting it.
    //
    // Unfortunately, that wouldn't be enough: the ranges are different for
    // (say) the two different butterflies along the 4x4 transform and they are
    // also different for different stages and sizes of transform, so you'd
    // really need a range for each possible butterfly in each possible B()
    // instantiation in case the customer had fully expanded their design. This
    // would be a huge number of ranges and *really* hard to hit.
    //
    // This version has just one range for each of the two plausible lines of
    // code in the spec. We do it after rounding because it turns out that a
    // 16x16 transform can very slightly bigger numbers, because of the
    // different angles compared with a 4x4 transform. Calculating the absolute
    // maximum possible value seems really hard, so we're punting on it
    // slightly.
    T[ xa ] = Round2( x, 12 ) // E-201 [RNG-Butterfly1]
    T[ xb ] = Round2( y, 12 ) // E-202 [RNG-Butterfly2]

#if COVER
    // Add coverage points for https://bugs.chromium.org/p/aomedia/issues/detail?id=2151
    // In short: At their widest, T[ xa ] and T[ xb ] must fit into an int(bd+8).
    // This implies that (x + (1<<11)) and (y + (1<<11)) must fit into an int(bd+20).
    // The reference code had a check that x and y fit into an int(bd+20), without the
    // offset, but this is incorrect - it is valid for x and y to lie slightly below
    // this range.
    // NOTE: We don't add 8-bit coverage points here, to keep things in line with
    // our range equations which only cover 10bit and 12bit content. These could be added
    // later if desired.
    if (COVERCLASS(1, 1, BitDepth == 10)) {
        if (x < -(1<<29)) {
            :pass
        }
        if (y < -(1<<29)) {
            :pass
        }
    }
    if (COVERCLASS(PROFILE2, 1, BitDepth == 12)) {
        if (x < -(1<<31)) {
            :pass
        }
        if (y < -(1<<31)) {
            :pass
        }
    }
#endif

    if (rev) {
        tmp = T[ xa ]
        T[ xa ] = T[ xb ]
        T[ xb ] = tmp
    }
#if IGNORE_TRANSFORM_RANGE
#else
    CHECK( T[ xa ] >= -( 1 << ( r - 1 ) ), "Value out of range in B()")
    CHECK( T[ xa ] <   ( 1 << ( r - 1 ) ), "Value out of range in B()")
    CHECK( T[ xb ] >= -( 1 << ( r - 1 ) ), "Value out of range in B()")
    CHECK( T[ xb ] <   ( 1 << ( r - 1 ) ), "Value out of range in B()")
#endif
}

H( xa, xb, rev, r ) {
    if (rev) {
        tmp = xa
        xa = xb
        xb = tmp
    }
    x = T[ xa ]
    y = T[ xb ]

    // If you're contemplating splitting these ranges up for DCT vs. ADST or row
    // vs. column, see the long comment in B() above.
    T[ xa ] = Clip3( - ( 1 << ( r - 1 ) ), ( 1 << ( r - 1 ) ) - 1, x + y ) // E-210 [RNG-Hadamard1]
    T[ xb ] = Clip3( - ( 1 << ( r - 1 ) ), ( 1 << ( r - 1 ) ) - 1, x - y ) // E-211 [RNG-Hadamard2]
}

inverse_dct_array_permutation( n ) {
    int copyT[ 64 ]
    n0 = 1 << n
    for (i=0; i < n0; i++)
        copyT[ i ] = T[ i ]
    for (i=0; i < n0; i++)
        T[ i ] = copyT[ brev( n, i ) ]
}

inverse_dct( n, r ) {
    inverse_dct_array_permutation( n )
    if ( n == 6 )
        for (i = 0; i < 16; i++)
            B( 32 + i, 63 - i, 63 - 4 * brev( 4, i ), 0, r )
    if ( n >= 5 )
        for (i = 0; i < 8; i++)
            B( 16 + i, 31 - i, 6 + ( brev( 3, 7-i ) << 3 ), 0, r )
    if ( n == 6 )
        for (i = 0; i < 16; i++)
            H( 32 + i * 2, 33 + i * 2, i & 1, r )
    if ( n >= 4 )
        for (i = 0; i < 4; i++)
            B( 8 + i, 15 - i, 12 + ( brev( 2, 3 - i ) << 4 ), 0, r)
    if ( n >= 5 )
        for (i = 0; i < 8; i++)
            H( 16 + 2 * i, 17 + 2 * i, i & 1, r )
    if ( n == 6 )
        for ( i = 0; i < 4; i++ )
            for ( j = 0; j < 2; j++ )
                B(62 - i * 4 - j, 33 + i * 4 + j, 60 - 16 * brev( 2, i ) + 64 * j, 1, r)
    if ( n >= 3 )
        for (i = 0; i < 2; i++)
            B( 4 + i, 7 - i, 56 - 32 * i, 0, r )
    if ( n >= 4 )
        for (i = 0; i < 4; i++)
            H( 8 + 2 * i, 9 + 2 * i, i & 1, r )
    if ( n >= 5 )
        for (i = 0; i < 2; i++)
            for (j = 0; j < 2; j++)
                B( 30 - 4 * i - j, 17 + 4 * i + j, 24 + (j << 6) + ( ( 1 - i ) << 5 ), 1, r )
    if ( n == 6 )
        for ( i = 0; i < 8; i++ )
            for ( j = 0; j < 2; j++ )
                H( 32 + i * 4 + j, 35 + i * 4 - j, i & 1, r )
    for (i = 0; i < 2; i++)
        B( 2 * i, 2 * i + 1, 32 + 16 * i, 1 - i, r )
    if ( n >= 3 )
        for (i = 0; i < 2; i++)
            H( 4 + 2 * i, 5 + 2 * i, i, r )
    if ( n >= 4 )
        for ( i = 0; i < 2; i++ )
            B( 14 - i, 9 + i, 48 + 64 * i, 1, r )
    if ( n >= 5 )
        for ( i = 0; i < 4; i++ )
            for ( j = 0; j < 2; j++ )
                H( 16 + 4 * i + j, 19 + 4 * i - j, i & 1, r )
    if ( n == 6 )
        for ( i = 0; i < 2; i++ )
            for ( j = 0; j < 4; j++ )
                B( 61 - i * 8 - j, 34 + i * 8 + j, 56 - i * 32 + ( j >> 1 ) * 64, 1, r )
    for (i = 0; i < 2; i++)
        H( i, 3 - i, 0, r )
    if ( n >= 3 )
        B( 6, 5, 32, 1, r )
    if ( n >= 4 )
        for ( i = 0; i < 2; i++ )
            for ( j = 0; j < 2; j++ )
                H( 8 + 4 * i + j, 11 + 4 * i - j, i, r )
    if ( n >= 5 )
        for ( i = 0; i < 4; i++ )
            B( 29 - i, 18 + i, 48 + ( i >> 1 ) * 64, 1, r )
    if ( n == 6 )
        for ( i = 0; i < 4; i++ )
            for ( j = 0; j < 4; j++ )
                H( 32 + 8 * i + j, 39 + 8 * i - j, i & 1, r )
    if ( n >= 3 )
        for ( i = 0; i < 4; i++ )
            H( i, 7 - i, 0, r )
    if ( n >= 4 )
        for (i = 0; i < 2; i++)
            B( 13 - i, 10 + i, 32, 1, r )
    if ( n >= 5 )
        for ( i = 0; i < 2; i++ )
            for ( j = 0; j < 4; j++ )
                H( 16 + i * 8 + j, 23 + i * 8 - j, i, r )
    if ( n == 6 )
        for ( i = 0; i < 8; i++ )
            B( 59 - i, 36 + i, i < 4 ? 48 : 112, 1, r )
    if ( n >= 4 )
        for ( i = 0; i < 8; i++ )
            H( i, 15 - i, 0, r )
    if ( n >= 5 )
        for ( i = 0; i < 4; i++ )
            B( 27 - i, 20 + i, 32, 1, r )
    if ( n == 6 )
        for ( i = 0; i < 8; i++ ) {
            H( 32 + i, 47 - i, 0, r )
            H( 48 + i, 63 - i, 1, r )
        }
    if ( n >= 5 )
        for ( i = 0; i < 16; i++ )
            H( i, 31 - i, 0, r )
    if ( n == 6 ) {
        for ( i = 0; i < 8; i++ )
            B( 55 - i, 40 + i, 32, 1, r )
    }
    if ( n == 6 ) {
        for ( i = 0; i < 32; i++ )
            H( i, 63 - i, 0, r )
    }
}

inverse_adst_input_array_permutation( n ) {
    int copyT[ 64 ]
    n0 = 1 << n
    for (i=0; i < n0; i++)
        copyT[ i ] = T[ i ]
    for (i=0; i < n0; i++) {
        idx = ( i & 1 ) ? ( i - 1 ) : ( n0 - i - 1 )
        T[ i ] = copyT[ idx ]
    }
}

inverse_adst_output_array_permutation( n ) {
    int copyT[ 64 ]
    n0 = 1 << n
    for (i=0; i < n0; i++)
        copyT[ i ] = T[ i ]
    for (i=0; i < n0; i++) {
        xa = ( ( i >> 3 ) & 1 )
        xb = ( ( i >> 2 ) & 1 ) ^ ( ( i >> 3 ) & 1 )
        xc = ( ( i >> 1 ) & 1 ) ^ ( ( i >> 2 ) & 1 )
        xd = ( i & 1 ) ^ ( ( i >> 1 ) & 1 )
        idx = ( ( xd << 3 ) | ( xc << 2 ) | ( xb << 1 ) | xa ) >> ( 4 - n )
        T[ i ] = ( i & 1 ) ? ( - copyT[ idx ] ) : copyT[ idx ]
    }
}

#define SINPI_1_9 1321
#define SINPI_2_9 2482
#define SINPI_3_9 3344
#define SINPI_4_9 3803

iadst4_check( int64 v, r ) {
#if IGNORE_TRANSFORM_RANGE
#else
    CHECK( v >= - ( 1 << ( r + 11 ) ), "iadst4_check - value out of range" )
    CHECK( v <=    ( 1 << ( r + 11 ) ) - 1, "iadst4_check - value out of range" )
#endif
    return v
}

inverse_adst4( r ) {
    int s[ 7 ]
    int x[ 4 ]
    if (doingTxfmRow) {
      s[ 0 ] = iadst4_check( SINPI_1_9 * T[ 0 ], r ) // E-220 [RNG-IADSTRow1]
      s[ 1 ] = iadst4_check( SINPI_2_9 * T[ 0 ], r ) // E-221 [RNG-IADSTRow2]
      s[ 2 ] = iadst4_check( SINPI_3_9 * T[ 1 ], r ) // E-222 [RNG-IADSTRow3]
      s[ 3 ] = iadst4_check( SINPI_4_9 * T[ 2 ], r ) // E-223 [RNG-IADSTRow4]
      s[ 4 ] = iadst4_check( SINPI_1_9 * T[ 2 ], r ) // E-224 [RNG-IADSTRow5]
      s[ 5 ] = iadst4_check( SINPI_2_9 * T[ 3 ], r ) // E-225 [RNG-IADSTRow6]
      s[ 6 ] = iadst4_check( SINPI_4_9 * T[ 3 ], r ) // E-226 [RNG-IADSTRow7]
      a7 = T[ 0 ] - T[ 2 ] // E-227 [RNG-IADSTRow8]
#if IGNORE_TRANSFORM_RANGE
#else
      CHECK( a7 >= - ( 1 << r ), "iadst4 a7 - value out of range" )
      CHECK( a7 <=    ( 1 << r ) - 1, "iadst4 a7 - value out of range" )
#endif
      b7 = a7 + T[ 3 ] // E-228 [RNG-IADSTRow9]
#if IGNORE_TRANSFORM_RANGE
#else
      CHECK( b7 >= - ( 1 << ( r - 1 ) ), "iadst4 b7 - value out of range" )
      CHECK( b7 <=    ( 1 << ( r - 1 ) ) - 1, "iadst4 b7 - value out of range" )
#endif
      s[ 0 ] = iadst4_check( s[ 0 ] + s[ 3 ], r ) // E-229 [RNG-IADSTRow10]
      s[ 1 ] = iadst4_check( s[ 1 ] - s[ 4 ], r ) // E-230 [RNG-IADSTRow11]
      s[ 3 ] = iadst4_check( s[ 2 ]         , r )
      s[ 2 ] = iadst4_check( SINPI_3_9 * b7 , r ) // E-231 [RNG-IADSTRow12]

      s[ 0 ] = iadst4_check( s[ 0 ] + s[ 5 ], r ) // E-232 [RNG-IADSTRow13]
      s[ 1 ] = iadst4_check( s[ 1 ] - s[ 6 ], r ) // E-233 [RNG-IADSTRow14]

      x[ 0 ] = iadst4_check( s[ 0 ] + s[ 3 ], r ) // E-234 [RNG-IADSTRow15]
      x[ 1 ] = iadst4_check( s[ 1 ] + s[ 3 ], r ) // E-235 [RNG-IADSTRow16]
      x[ 2 ] = iadst4_check( s[ 2 ]         , r )
      x[ 3 ] = iadst4_check( s[ 0 ] + s[ 1 ], r ) // E-236 [RNG-IADSTRow17]

      x[ 3 ] = iadst4_check( x[ 3 ] - s[ 3 ], r ) // E-237 [RNG-IADSTRow18]

      T[ 0 ] = Round2( x[ 0 ], 12 ) // E-238 [RNG-IADSTRow19]
      T[ 1 ] = Round2( x[ 1 ], 12 ) // E-239 [RNG-IADSTRow20]
      T[ 2 ] = Round2( x[ 2 ], 12 ) // E-240 [RNG-IADSTRow21]
      T[ 3 ] = Round2( x[ 3 ], 12 ) // E-241 [RNG-IADSTRow22]
    } else {
      s[ 0 ] = iadst4_check( SINPI_1_9 * T[ 0 ], r ) // E-242 [RNG-IADSTCol1]
      s[ 1 ] = iadst4_check( SINPI_2_9 * T[ 0 ], r ) // E-243 [RNG-IADSTCol2]
      s[ 2 ] = iadst4_check( SINPI_3_9 * T[ 1 ], r ) // E-244 [RNG-IADSTCol3]
      s[ 3 ] = iadst4_check( SINPI_4_9 * T[ 2 ], r ) // E-245 [RNG-IADSTCol4]
      s[ 4 ] = iadst4_check( SINPI_1_9 * T[ 2 ], r ) // E-246 [RNG-IADSTCol5]
      s[ 5 ] = iadst4_check( SINPI_2_9 * T[ 3 ], r ) // E-247 [RNG-IADSTCol6]
      s[ 6 ] = iadst4_check( SINPI_4_9 * T[ 3 ], r ) // E-248 [RNG-IADSTCol7]
      a7 = T[ 0 ] - T[ 2 ] // E-249 [RNG-IADSTCol8]
#if IGNORE_TRANSFORM_RANGE
#else
      CHECK( a7 >= - ( 1 << r ), "iadst4 a7 - value out of range" )
      CHECK( a7 <=    ( 1 << r ) - 1, "iadst4 a7 - value out of range" )
#endif
      b7 = a7 + T[ 3 ] // E-250 [RNG-IADSTCol9]
#if IGNORE_TRANSFORM_RANGE
#else
      CHECK( b7 >= - ( 1 << ( r - 1 ) ), "iadst4 b7 - value out of range" )
      CHECK( b7 <=    ( 1 << ( r - 1 ) ) - 1, "iadst4 b7 - value out of range" )
#endif
      s[ 0 ] = iadst4_check( s[ 0 ] + s[ 3 ], r ) // E-251 [RNG-IADSTCol10]
      s[ 1 ] = iadst4_check( s[ 1 ] - s[ 4 ], r ) // E-252 [RNG-IADSTCol11]
      s[ 3 ] = iadst4_check( s[ 2 ]         , r )
      s[ 2 ] = iadst4_check( SINPI_3_9 * b7 , r ) // E-253 [RNG-IADSTCol12]

      s[ 0 ] = iadst4_check( s[ 0 ] + s[ 5 ], r ) // E-254 [RNG-IADSTCol13]
      s[ 1 ] = iadst4_check( s[ 1 ] - s[ 6 ], r ) // E-255 [RNG-IADSTCol14]

      x[ 0 ] = iadst4_check( s[ 0 ] + s[ 3 ], r ) // E-256 [RNG-IADSTCol15]
      x[ 1 ] = iadst4_check( s[ 1 ] + s[ 3 ], r ) // E-257 [RNG-IADSTCol16]
      x[ 2 ] = iadst4_check( s[ 2 ]         , r )
      x[ 3 ] = iadst4_check( s[ 0 ] + s[ 1 ], r ) // E-258 [RNG-IADSTCol17]

      x[ 3 ] = iadst4_check( x[ 3 ] - s[ 3 ], r ) // E-259 [RNG-IADSTCol18]

      T[ 0 ] = Round2( x[ 0 ], 12 ) // E-260 [RNG-IADSTCol19]
      T[ 1 ] = Round2( x[ 1 ], 12 ) // E-261 [RNG-IADSTCol20]
      T[ 2 ] = Round2( x[ 2 ], 12 ) // E-262 [RNG-IADSTCol21]
      T[ 3 ] = Round2( x[ 3 ], 12 ) // E-263 [RNG-IADSTCol22]
    }
}

inverse_adst8( r ) {
    inverse_adst_input_array_permutation( 3 )

    for (i = 0; i < 4; i++)
        B( 2 * i, 2 * i + 1, 60 - 16 * i, 1, r )

    for (i = 0; i < 4; i++)
        H( i, 4 + i, 0, r )

    for (i = 0; i < 2; i++)
        B( 4 + 3 * i, 5 + i, 48 - 32 * i, 1, r )

    for (i = 0; i < 2; i++)
        for (j = 0; j < 2; j++)
            H( 4 * j + i, 2 + 4 * j + i, 0, r )

    for (i = 0; i < 2; i++)
        B( 2 + 4 * i, 3 + 4 * i, 32, 1, r )

    inverse_adst_output_array_permutation( 3 )
}

inverse_adst16( r ) {
    inverse_adst_input_array_permutation( 4 )

    for (i = 0; i < 8; i++)
        B( 2 * i, 2 * i + 1, 62 - 8 * i, 1, r )

    for (i = 0; i < 8; i++)
        H( i, 8 + i, 0, r )

    for (i = 0; i < 2; i++) {
        B( 8 + 2 * i, 9 + 2 * i, 56 - 32 * i, 1, r )
        B( 13 + 2 * i, 12 + 2 * i, 8 + 32 * i, 1, r )
    }

    for (i = 0; i < 4; i++)
        for (j = 0; j < 2; j++)
            H( 8 * j + i, 4 + 8 * j + i, 0, r )

    for (i = 0; i < 2; i++)
        for (j = 0; j < 2; j++) {
            B( 4 + 8 * j + 3 * i, 5 + 8 * j + i, 48 - 32 * i, 1, r )
            //B( 12 + 3 * i, 13 + i, 48 - 32 * i, 1, r )
        }

    for (i = 0; i < 2; i++)
        for (j = 0; j < 4; j++)
            H( 4 * j + i, 2 + 4 * j + i, 0, r )

    for (i = 0; i < 4; i++)
        B( 2 + 4 * i, 3 + 4 * i, 32, 1, r )

    inverse_adst_output_array_permutation( 4 )
}

inverse_adst( n, r ) {
    if ( n == 2 ) {
        inverse_adst4( r )
    } else if ( n == 3 ) {
        inverse_adst8( r )
    } else {
        inverse_adst16( r )
    }
}

inverse_wht( shift ) {
    xa = T[ 0 ] >> shift // E-270 [RNG-IWHT1]
    xc = T[ 1 ] >> shift // E-271 [RNG-IWHT2]
    xd = T[ 2 ] >> shift // E-272 [RNG-IWHT3]
    xb = T[ 3 ] >> shift // E-273 [RNG-IWHT4]
    xa += xc // E-274 [RNG-IWHT5]
    xd -= xb // E-275 [RNG-IWHT6]
    xe = (xa - xd) >> 1 // E-276 [RNG-IWHT7]
    xb = xe - xb // E-277 [RNG-IWHT8]
    xc = xe - xc // E-278 [RNG-IWHT9]
    xa -= xb // E-279 [RNG-IWHT10]
    xd += xc // E-280 [RNG-IWHT11]
    T[ 0 ] = xa
    T[ 1 ] = xb
    T[ 2 ] = xc
    T[ 3 ] = xd
}

inverse_identity_transform_4( ) {
    for (i = 0; i < 4; i++) {
      if (doingTxfmRow) {
        T[ i ] = Round2( T[ i ] * 5793, 12 ) // E-281 [RNG-IIDTXRow4]
      } else {
        T[ i ] = Round2( T[ i ] * 5793, 12 ) // E-282 [RNG-IIDTXCol4]
      }
    }
}

inverse_identity_transform_8( ) {
    for (i = 0; i < 8; i++) {
      if (doingTxfmRow) {
        T[ i ] = T[ i ] * 2 // E-283 [RNG-IIDTXRow8]
      } else {
        T[ i ] = T[ i ] * 2 // E-284 [RNG-IIDTXCol8]
      }
    }
}

inverse_identity_transform_16( ) {
    for (i = 0; i < 16; i++) {
      if (doingTxfmRow) {
        T[ i ] = Round2( T[ i ] * 11586, 12 ) // E-285 [RNG-IIDTXRow16]
      } else {
        T[ i ] = Round2( T[ i ] * 11586, 12 ) // E-286 [RNG-IIDTXCol16]
      }
    }
}

inverse_identity_transform_32( ) {
    for (i = 0; i < 32; i++) {
      if (doingTxfmRow) {
        T[ i ] = T[ i ] * 4 // E-287 [RNG-IIDTXRow32]
      } else {
        T[ i ] = T[ i ] * 4 // E-288 [RNG-IIDTXCol32]
      }
    }
}

inverse_identity_transform( n ) {
    if ( n == 2 ) inverse_identity_transform_4( )
    else if ( n == 3 ) inverse_identity_transform_8( )
    else if ( n == 4 ) inverse_identity_transform_16( )
    else inverse_identity_transform_32( )
}
