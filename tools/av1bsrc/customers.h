/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#ifndef CUSTOMERS_HH
#define CUSTOMERS_HH

// A note about release and non-release mode:
//
//   - For normal release builds (which we'll give to customers), we
//     don't want even a whiff of our magic custom sets. They will be
//     compiled out completely.
//
//   - When generating coverage for one of our custom sets, however,
//     we want to mention that set and no other. To make that work,
//     build with RELEASE=1 and also -DRELEASE_<SETNAME> in CFLAGS
//     (which you can control with the CUSTOMER argument in the
//     Makefile). For example, -DRELEASE_CLIENT_A. This will turn off
//     the other sets but enable the one you need.
//
// Custom set names:
//
//     -DRELEASE_CLIENT_A
//     -DRELEASE_CLIENT_B
//     -DRELEASE_CLIENT_C

#if !RELEASE
# define PROF_SHOW_CLIENT_A
# define PROF_SHOW_CLIENT_B
# define PROF_SHOW_CLIENT_C
# define PROF_SHOW_CLIENT_D
#else
# ifdef RELEASE_CLIENT_A
#  define PROF_SHOW_CLIENT_A
# endif
# ifdef RELEASE_CLIENT_B
#  define PROF_SHOW_CLIENT_B
# endif
# ifdef RELEASE_CLIENT_C
#  define PROF_SHOW_CLIENT_C
# endif
# ifdef RELEASE_CLIENT_D
#  define PROF_SHOW_CLIENT_D
# endif
#endif

#endif // CUSTOMERS_HH
