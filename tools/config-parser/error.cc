/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "error.hh"
#include <cassert>
#include <cstdio>
#include <cstring>
#include <memory>

typedef std::unique_ptr<FILE, int(*)(FILE *)> file_handle_t;

static bool print_file_line (const char *filename, unsigned to_print)
{
    if (! filename)
        return false;

    file_handle_t input (fopen (filename, "r"), fclose);
    if (! input)
        return false;

    char buf [128];
    unsigned line_counter = 1;
    bool printed = false;

    while (fgets (buf, sizeof buf, input.get ())) {
        if (line_counter == to_print) {
            fputs (buf, stderr);
            printed = true;
        }

        // If this is the end of a line, increment the line counter
        size_t len = strlen (buf);
        if (buf [len - 1] == '\n')
            ++ line_counter;

        // If we're past the error, we can stop reading
        if (line_counter > to_print)
            break;
    }

    return printed;
}

static void annotate_line (unsigned col0, unsigned col1)
{
    assert (col0);
    assert (col1 > col0);

    unsigned num_spaces = col0 - 1;
    unsigned num_tildes = col1 - col0 - 1;

    for (unsigned i = 0; i < num_spaces; ++ i)
        fputc (' ', stderr);
    fputc ('^', stderr);
    for (unsigned i = 0; i < num_tildes; ++ i)
        fputc ('~', stderr);
    fputc ('\n', stderr);
}

void print_error (const char    *filename,
                  const place_t &place,
                  const char    *msg,
                  const char    *post_msg)
{
    assert (filename);
    fprintf(stderr, "%s:%d:%d: error: %s\n",
            filename, place.line0, place.col0, msg);
    if (print_file_line (filename, place.line0))
        annotate_line (place.col0,
                       ((place.line0 == place.line1) ?
                        place.col1 :
                        place.col0 + 1));

    if (post_msg) fprintf(stderr, "%s\n", post_msg);
}

void print_error (const char    *filename,
                  const char    *msg)
{
    assert (filename);
    fprintf(stderr, "%s: error: %s\n", filename, msg);
}

void print_error (const char *msg)
{
    fprintf(stderr, "error: %s\n", msg);
}
