/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "error.hh"
#include "swizzle.hh"
#include "collections.hh"
#include "inserts.hh"

#include <algorithm>
#include <iomanip>
#include <sstream>

using namespace swizzle;

bool
block_slice_t::reverse ()
{
    // Start by reversing the vector.
    std::reverse (std::begin (slices), std::end (slices));

    // Now walk through it, reversing children and keeping track of
    // whether anything unconditionally writes to x.
    for (size_t i = 0; i < slices.size (); ++ i) {
        if (slices [i]->reverse ()) {
            // Ahah! We can discard everything after this point.
            slices.erase (slices.begin () + i + 1,
                          slices.end ());
            return true;
        }
    }

    return false;
}

void block_slice_t::display (std::ostream   &os,
                             unsigned        indent,
                             const symtab_t &symbol_table,
                             const char     *element) const
{
    if (indent) os << std::setw(2 * indent) << ' ';
    os << "{\n";
    for (const std::unique_ptr<slice_t> &slice : slices) {
        slice->display (os, indent + 1, symbol_table, element);
    }
    if (indent) os << std::setw(2 * indent) << ' ';
    os << "}\n";
}

bool
block_slice_t::eval (rnd_t&                          rnd,
                     const std::vector<std::string> &paths,
                     int64_t                        *result)
{
    for (const std::unique_ptr<slice_t> &slice : slices) {
        if (slice->eval (rnd, paths, result)) return true;
    }

    return false;
}

void
block_slice_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);

    for (const std::unique_ptr<slice_t> &slice : slices) {
        slice->fill_sensitivity_list (dest);
    }
}

void ret_slice_t::display (std::ostream   &os,
                           unsigned        indent,
                           const symtab_t &symbol_table,
                           const char     *element) const
{
    os << std::setw(2 * indent) << ' ' << "return ";
    underlying->display (os, symbol_table, element, -1);
    os << ";\n";
}

bool
ret_slice_t::eval (rnd_t&                          rnd,
                   const std::vector<std::string> &paths,
                   int64_t                        *result)
{
    assert (result);

    *result = underlying->eval (rnd, paths.at (path_index));
    return true;
}

void
ret_slice_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    underlying->fill_sensitivity_list (dest);
}

bool
if_slice_t::reverse ()
{
    bool clean_t = if_true.reverse ();
    bool clean_f = if_false.reverse ();

    return clean_t && clean_f;
}

void
if_slice_t::display (std::ostream   &os,
                     unsigned        indent,
                     const symtab_t &symbol_table,
                     const char     *element) const
{
    os << std::setw (2 * indent) << ' ';
    os << "if (";
    test->display (os, symbol_table, nullptr, -1);
    os << ")\n";

    if_true.display (os, indent, symbol_table, element);
    os << std::setw (2 * indent) << ' ' << "else\n";
    if_false.display(os, indent, symbol_table, element);
}

bool
if_slice_t::eval (rnd_t&                          rnd,
                  const std::vector<std::string> &paths,
                  int64_t                        *result)
{
    assert (result);

    if (test->eval (rnd, paths.at (path_index)))
        return if_true.eval (rnd, paths, result);
    return if_false.eval (rnd, paths, result);
}

void
if_slice_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    test->fill_sensitivity_list (dest);
    if_true.fill_sensitivity_list (dest);
    if_false.fill_sensitivity_list (dest);
}

void
map_t::display (std::ostream &os, const symtab_t &symbols) const
{
    // Walk the map in order, printing the slice for each element
    bool first = true;
    for (size_t i = 0; i < symbols.num_elements (); ++ i) {
        const block_slice_t *bs = vec [i].get ();
        if (! bs)
            continue;

        const std::string &elt_name = symbols.element_name (i);

        if (! first) os << '\n';

        os << elt_name << ":\n";
        bs->display (os, 0, symbols, elt_name.c_str ());

        first = false;
    }
}

int64_t
map_t::eval (rnd_t &rnd, size_t element_index, symtab_t &st)
{
    assert (element_index < vec.size ());

    if (!vec.at (element_index).get ()) {
        std::ostringstream oss;
        oss << "Attempt to evaluate the element `" << st.element_name (element_index)
                << "' failed, as it is not overridden in this configuration.";
        print_error (oss.str ().c_str ());
        throw runtime_error ();
    }

    int64_t result;
    if (!vec.at (element_index)->eval (rnd, paths, &result)) {
        std::ostringstream oss;
        oss << "Evaluation of the element `" << st.element_name (element_index)
                << "' failed.";

        print_error (oss.str ().c_str ());
        throw runtime_error ();
    }

    return result;
}

std::unique_ptr<map_t>
swizzle::pass (std::unique_ptr<inserts::config_t> &&cfg,
               std::vector<std::string>           &&paths,
               size_t                               num_elements)
{
    // The first thing we have to do in the swizzle pass is get a list
    // of all the elements that are assigned to in the config. We'll
    // basically run through the pass once per element.
    std::vector<size_t> elements;
    cfg->elements (& elements);

    std::unique_ptr<map_t> ret (new map_t (std::move (cfg),
                                           std::move (paths),
                                           num_elements));

    for (size_t element : elements) {
        std::unique_ptr<block_slice_t>
            slice (ret->config->slice_config (element));

        slice->reverse ();

        ret->vec [element] = std::move (slice);
    }

    return ret;
}
