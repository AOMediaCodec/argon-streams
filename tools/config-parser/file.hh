/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <cstdint>
#include <string>

// This is a unique key for a file (device id and inode)
typedef std::pair<uint64_t, uint64_t> file_unique_id;

// Resolve an inclusion to a path
std::string resolve_include (const char *includer, const char *includee);

// Get the key for the file at path, writing it to id. Return true on
// success; false on failure.
bool get_file_data (file_unique_id *id, const char *path);
