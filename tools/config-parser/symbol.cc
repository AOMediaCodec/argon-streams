/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "symbol.hh"
#include <cassert>

void
symtab_t::add_element (const std::string &element)
{
    // No duplicates allowed.
    assert (element_indices.find (element) == element_indices.end ());
    assert (elements.size () == element_indices.size ());

    element_indices [element] = elements.size ();
    elements.push_back (element);
}

size_t
symtab_t::element_index (const std::string &element) const
{
    std::map<std::string, size_t>::const_iterator it =
        element_indices.find (element);

    if (it == element_indices.end ()) {
        return SIZE_MAX;
    }

    return it->second;
}

const std::string &
symtab_t::element_name (size_t idx) const
{
    return elements.at (idx);
}
