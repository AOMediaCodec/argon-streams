/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "collections.hh"
#include "error.hh"
#include "inserts.hh"
#include "mem-ref.hh"

#include <cassert>
#include <algorithm>
#include <limits>
#include <iomanip>
#include <memory>
#include <random>
#include <sstream>

using namespace collections;

// Instantiate the two types of conditional expression
template struct collections::cond_expr_t<int_expr_t>;
template struct collections::cond_expr_t<coll_expr_t>;

struct op_info_t {
    const char *name;
    int         precedence;
};

static const op_info_t unop_data [UNOP_NUM_ENTRIES] = {
    { "-", 3 }, // UNOP_MINUS
    { "!", 3 }, // UNOP_LOG_NOT
    { "~", 3 }  // UNOP_BIT_NOT
};

static const op_info_t binop_data [IBINOP_NUM_ENTRIES] = {
    { "+",       6 }, // BINOP_PLUS
    { "-",       6 }, // BINOP_MINUS
    { "*",       5 }, // BINOP_TIMES
    { "/",       5 }, // BINOP_DIVIDE
    { "%",       5 }, // BINOP_MOD
    { "<",       9 }, // BINOP_LT
    { "<=",      9 }, // BINOP_LE
    { ">",       9 }, // BINOP_GT
    { ">=",      9 }, // BINOP_GE
    { "==",     10 }, // BINOP_EQ
    { "!=",     10 }, // BINOP_NEQ
    { "&&",     14 }, // BINOP_LOG_AND
    { "||",     15 }, // BINOP_LOG_OR
    { "&",      11 }, // BINOP_BIT_AND
    { "^",      12 }, // BINOP_BIT_XOR
    { "|",      13 }, // BINOP_BIT_OR
    { "<<",      7 }, // BINOP_LSHIFT
    { ">>",      7 }, // BINOP_RSHIFT
};

void
int_expr_t::display (std::ostream   &os,
                     const symtab_t &symbol_table,
                     const char     *element,
                     unsigned        parent_precedence) const
{
    bool needs_brackets = (get_precedence() >= parent_precedence);

    if (needs_brackets) os << '(';
    display_ (os, symbol_table, element);
    if (needs_brackets) os << ')';
}

void
coll_expr_t::display (std::ostream   &os,
                      const symtab_t &symbol_table,
                      const char     *element,
                      unsigned        parent_precedence) const
{
    bool needs_brackets = (get_precedence() >= parent_precedence);

    if (needs_brackets) os << '(';
    display_ (os, symbol_table, element);
    if (needs_brackets) os << ')';
}

void
int_literal_t::display_ (std::ostream   &os,
                         const symtab_t &symbol_table,
                         const char     *element) const
{
    (void) symbol_table;
    (void) element;

    os << value;
}

int_expr_t::ptr_t
int_literal_t::clone() const
{
    return ptr_t(new int_literal_t(place, value));
}

int64_t
int_literal_t::eval (rnd_t& rnd, const std::string& filename)
{
    (void) rnd;
    (void) filename;
    return value;
}

void
int_literal_t::fill_sensitivity_list (svds_t *dest) const
{
    (void) dest;
}

void
variable_t::display_ (std::ostream   &os,
                      const symtab_t &symbol_table,
                      const char     *element) const
{
    const char *name = symbol_table.sym_invert (element, desc.svd.addr);
    assert (name);

    os << name;

    for (const auto &index : indices) {
        os << '[';
        index->display (os, symbol_table, element, 1000);
        os << ']';
    }
}

int_expr_t::ptr_t
variable_t::clone() const
{
    auto clone = std::unique_ptr<variable_t>(
            new variable_t(place, desc));
    clone->indices.reserve(indices.size());

    for (const ptr_t &index : indices) {
        clone->indices.push_back(ptr_t(index->clone()));
    }

    return std::move (clone);
}

int64_t
variable_t::eval (rnd_t& rnd, const std::string& filename)
{
    // This should be guaranteed at parse time
    assert (indices.size () == desc.dim);

    // If there are no indices, take a fast path.
    if (! desc.dim)
        return desc.svd.read ();

    // Otherwise, evaluate each index and check that it is valid
    std::vector<uint64_t> eval_indices;
    eval_indices.reserve (desc.dim);

    for (size_t i = 0; i < desc.dim; i++) {
        int64_t index = indices[i]->eval (rnd, filename);
        if (index < 0 || (uint64_t) index >= desc.lengths [i]) {
            std::ostringstream oss;
            oss << "Index " << i << " (value = " << index
                << ") is out of bounds: expected between 0 and "
                << desc.lengths [i] - 1 << " (inclusive).";
            print_error (filename.c_str (), place, oss.str ().c_str ());
            throw runtime_error ();
        }

        eval_indices.push_back ((uint64_t) index);
    }

    return desc.read (eval_indices);
}

void
variable_t::fill_sensitivity_list (std::vector<simple_var_desc_t> *dest) const
{
    assert (dest);

    // TODO: Sensitivity for arrays??
    if (desc.dim)
        return;

    if (dest->end () == std::find (dest->begin (), dest->end (), desc.svd)) {
        dest->push_back (desc.svd);
    }
}

template <typename F>
static void
show_fun_name (std::ostream                                      &os,
               F                                                 *fun,
               const std::map<std::string, std::pair<F *, bool>> &map)
{
    // Invert the map to find the function that points at this
    // address.
    for (const auto &pr : map) {
        if (pr.second.first == fun) {
            os << pr.first;
            return;
        }
    }

    // LCOV_EXCL_START
    assert (0);
    __builtin_unreachable ();
    // LCOV_EXCL_STOP
}

template <typename ARG, typename F>
static void
print_funcall (F                      print,
               std::ostream          &os,
               const symtab_t        &symbol_table,
               const char            *element,
               const char            *name,
               const std::vector<ARG> &args)
{
    assert (name);
    os << name << " (";
    bool first = true;
    for (const auto &arg : args) {
        if (! first) os << ", ";
        print (os, symbol_table, element, arg);
        first = false;
    }
    os << ")";
}

void
funcall_0_t::display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const
{
    (void) element;

    show_fun_name (os, fun, symbol_table.fun0s);
    os << " ()";
}

int_expr_t::ptr_t
funcall_0_t::clone() const
{
    return ptr_t(new funcall_0_t(place, fun));
}

int64_t
funcall_0_t::eval (rnd_t& rnd, const std::string& filename)
{
    (void) rnd;
    (void) filename;
    return fun ();
}

void
funcall_0_t::fill_sensitivity_list (svds_t *dest) const
{
    (void) dest;
}

void
funcall_1_t::display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const
{
    show_fun_name (os, fun, symbol_table.fun1s);
    os << " (";
    arg0->display (os, symbol_table, element, 1000);
    os << ")";
}

int_expr_t::ptr_t
funcall_1_t::clone() const
{
    return ptr_t(new funcall_1_t(place, fun, arg0->clone().release()));
}

int64_t
funcall_1_t::eval (rnd_t& rnd, const std::string& filename)
{
    return fun (arg0->eval (rnd, filename));
}

void
funcall_1_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    arg0->fill_sensitivity_list (dest);
}

void
funcall_2_t::display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const
{
    show_fun_name (os, fun, symbol_table.fun2s);
    os << " (";
    arg0->display (os, symbol_table, element, 1000);
    os << ", ";
    arg1->display (os, symbol_table, element, 1000);
    os << ")";
}

int_expr_t::ptr_t
funcall_2_t::clone() const
{
    return ptr_t(new funcall_2_t(place,
                                 fun,
                                 arg0->clone().release(),
                                 arg1->clone().release()));
}

int64_t
funcall_2_t::eval (rnd_t& rnd, const std::string& filename)
{
    return fun (arg0->eval (rnd, filename), arg1->eval (rnd, filename));
}

void
funcall_2_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    arg0->fill_sensitivity_list (dest);
    arg1->fill_sensitivity_list (dest);
}

void
funcall_3_t::display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const
{
    show_fun_name (os, fun, symbol_table.fun3s);
    os << " (";
    arg0->display (os, symbol_table, element, 1000);
    os << ", ";
    arg1->display (os, symbol_table, element, 1000);
    os << ", ";
    arg2->display (os, symbol_table, element, 1000);
    os << ")";
}

int_expr_t::ptr_t
funcall_3_t::clone() const
{
    return ptr_t(new funcall_3_t(place,
                                 fun,
                                 arg0->clone().release(),
                                 arg1->clone().release(),
                                 arg2->clone().release()));
}

int64_t
funcall_3_t::eval (rnd_t& rnd, const std::string& filename)
{
    return fun (arg0->eval (rnd, filename),
                arg1->eval (rnd, filename),
                arg2->eval (rnd, filename));
}

void
funcall_3_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    arg0->fill_sensitivity_list (dest);
    arg1->fill_sensitivity_list (dest);
    arg2->fill_sensitivity_list (dest);
}

void
choose_t::display_ (std::ostream   &os,
                    const symtab_t &symbol_table,
                    const char     *element) const
{
    os << "choose (";
    arg->display (os, symbol_table, element, 1000);
    os << ")";
}

int_expr_t::ptr_t
choose_t::clone() const
{
    return ptr_t(new choose_t(place, arg->clone().release()));
}

int64_t
choose_t::eval (rnd_t& rnd, const std::string& filename)
{
    return arg->eval (rnd, filename);
}

void
choose_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    arg->fill_sensitivity_list (dest);
}

unsigned
unop_expr_t::get_precedence () const
{
    return unop_data [op].precedence;
}

void
unop_expr_t::display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const
{
    os << unop_data [op].name;
    arg->display (os, symbol_table, element, get_precedence ());
}

int_expr_t::ptr_t
unop_expr_t::clone() const
{
    return ptr_t(new unop_expr_t(place, op, arg->clone().release()));
}

int64_t
unop_expr_t::eval (rnd_t& rnd, const std::string& filename)
{
    int64_t val = arg->eval (rnd, filename);

    switch (op) {
    case UNOP_MINUS:
        val = - val;
        break;
    case UNOP_LOG_NOT:
        val = ! val;
        break;
    case UNOP_BIT_NOT:
        val = ~ val;
        break;

        // LCOV_EXCL_START
    default:
        // Unreachable case
        assert (0);
        __builtin_unreachable ();
        // LCOV_EXCL_STOP
    }

    return val;
}

void
unop_expr_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    arg->fill_sensitivity_list (dest);
}

unsigned
binop_expr_t::get_precedence () const
{
    return binop_data [op].precedence;
}

void
binop_expr_t::display_ (std::ostream   &os,
                        const symtab_t &symbol_table,
                        const char     *element) const
{
    unsigned prec = get_precedence ();
    l->display (os, symbol_table, element, prec);
    os << ' ' << binop_data [op].name << ' ';
    r->display (os, symbol_table, element, prec);
}

int_expr_t::ptr_t
binop_expr_t::clone() const
{
    return ptr_t(new binop_expr_t(place,
                                  l->clone().release(),
                                  op,
                                  r->clone().release()));
}

int64_t
binop_expr_t::eval (rnd_t& rnd, const std::string& filename)
{
    int64_t val0 = l->eval (rnd, filename);
    int64_t val1 = r->eval (rnd, filename);

    int64_t val;

    switch (op) {
    case IBINOP_PLUS:
        val = (val0 + val1);
        break;
    case IBINOP_MINUS:
        val = (val0 - val1);
        break;
    case IBINOP_TIMES:
        val = (val0 * val1);
        break;
    case IBINOP_DIVIDE:
        val = (val0 / val1);
        break;
    case IBINOP_MOD:
        val = (val0 % val1);
        break;
    case IBINOP_LT:
        val = (val0 < val1);
        break;
    case IBINOP_LE:
        val = (val0 <= val1);
        break;
    case IBINOP_GT:
        val = (val0 > val1);
        break;
    case IBINOP_GE:
        val = (val0 >= val1);
        break;
    case IBINOP_EQ:
        val = (val0 == val1);
        break;
    case IBINOP_NEQ:
        val = (val0 != val1);
        break;
    case IBINOP_LOG_AND:
        val = (val0 && val1);
        break;
    case IBINOP_LOG_OR:
        val = (val0 || val1);
        break;
    case IBINOP_BIT_AND:
        val = (val0 & val1);
        break;
    case IBINOP_BIT_XOR:
        val = (val0 ^ val1);
        break;
    case IBINOP_BIT_OR:
        val = (val0 | val1);
        break;
    case IBINOP_LSHIFT:
        val = (val0 << val1);
        break;
    case IBINOP_RSHIFT:
        val = (val0 >> val1);
        break;

        // LCOV_EXCL_START
    default:
        // This should not be possible.
        assert(0);
        __builtin_unreachable();
        // LCOV_EXCL_STOP
    }

    return val;
}

void
binop_expr_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    l->fill_sensitivity_list (dest);
    r->fill_sensitivity_list (dest);
}

template <typename T>
void cond_expr_t<T>::display_ (std::ostream   &os,
                               const symtab_t &symbol_table,
                               const char     *element) const
{
    unsigned prec = get_precedence ();
    test->display (os, symbol_table, element, prec);
    os << " ? ";
    if_true->display (os, symbol_table, element, -1);
    os << " : ";
    if_false->display (os, symbol_table, element, prec);
}

namespace collections {
    template <>
    int_expr_t::ptr_t
    cond_expr_t<int_expr_t>::clone() const
    {
	return int_expr_t::ptr_t(
		new cond_expr_t<int_expr_t>(place,
					    test->clone().release(),
					    if_true->clone().release(),
					    if_false->clone().release()));
    }

    template <>
    coll_expr_t::ptr_t
    cond_expr_t<coll_expr_t>::clone() const
    {
	return coll_expr_t::ptr_t(
		new cond_expr_t<coll_expr_t>(place,
					     test->clone().release(),
					     if_true->clone().release(),
					     if_false->clone().release()));
    }
}

template <typename T>
int64_t cond_expr_t<T>::eval (rnd_t& rnd, const std::string& filename)
{
    if (test->eval (rnd, filename)) return if_true->eval (rnd, filename);
    else return if_false->eval (rnd, filename);
}

template <typename T>
void
cond_expr_t<T>::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    test->fill_sensitivity_list (dest);
    if_true->fill_sensitivity_list (dest);
    if_false->fill_sensitivity_list (dest);
}

void
ivl_t::display (std::ostream   &os,
                const symtab_t &symbol_table,
                const char     *element) const
{
    lo->display (os, symbol_table, element, hi ? 16 : 1000);
    if (hi) {
        os << " .. ";
        hi->display (os, symbol_table, element, 16);
    }
}

ivl_t
ivl_t::clone() const
{
    return ivl_t(place,
                 lo->clone().release(),
                 hi.get() ? hi->clone().release() : nullptr);
}

void
ivl_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    lo->fill_sensitivity_list (dest);
    if (hi)
        hi->fill_sensitivity_list (dest);
}

void
set_t::add_ivl (int64_t lo, int64_t hi)
{
    // To find the insertion point, search for the first interval
    // whose range starts at or after address. If there is a previous
    // interval and that interval has a top that is at least lo - 1,
    // that interval is the insertion point. Otherwise, the original
    // iterator gives the insertion point.
    iter_t insert_pt = intervals.lower_bound (lo);
    if (insert_pt != intervals.begin ()) {
        iter_t prev = std::prev (insert_pt);

        // If lo was zero, we would have insert_pt == intervals.begin().
        assert (lo > 0);
        if (prev->second >= lo - 1)
            insert_pt = prev;
    }

    int64_t u64_max = std::numeric_limits<int64_t>::max ();

    // At this point, we need to figure out whether we overlap with
    // anything. If insert_pt == intervals.end(), we start strictly
    // after the highest range, so don't overlap with anything.
    // Otherwise, we are free of overlaps if insert_pt->first is
    // strictly greater than 1 + hi.
    if ((insert_pt == intervals.end ()) ||
        ((hi < u64_max) && (insert_pt->first > 1 + hi))) {

        // We don't overlap with anything. Just insert and be done with it!
        std::pair<iter_t, bool> pr =
            intervals.insert (std::make_pair (lo, hi));

        // Check that we did indeed manage to insert something.
        assert (pr.second);
        (void) pr;

        add_ivl_length (lo, hi);
        return;
    }

    iter_t first_overlap = insert_pt;

    // This is the more complicated case: we overlap with at least one
    // other interval. The first interval that we overlap with is
    // first_overlap. We also need to find the last interval with
    // which we overlap. To do so, find the first record that starts
    // strictly later than 1 + hi (which is the first record that is
    // too high to overlap), then step back by one.
    iter_t after_overlap =
        (hi < u64_max) ? intervals.upper_bound (1 + hi) : intervals.end ();

    // We know that after_overlap isn't intervals.begin () because we
    // know that intervals is nonempty (otherwise insert_pt would have
    // been intervals.end()) and upper_bound doesn't return begin()
    // unless the map is empty.
    assert (after_overlap != intervals.begin ());
    iter_t last_overlap = std::prev (after_overlap);

    // Now calculate the interval that the new merged interval will
    // cover. The low address is the minimum of lo and
    // first_overlap->first. The high address is the maximum of hi
    // and last_overlap->second.
    int64_t merged_lo = std::min (lo, first_overlap->first);
    int64_t merged_hi = std::max (hi, last_overlap->second);

    if (merged_lo != first_overlap->first) {
        // If merged_lo != first_overlap->first, we actually have to
        // insert a new element into the map. Note that this doesn't
        // invalidate any iterators.
        std::pair<iter_t, bool> pr =
            intervals.insert (std::make_pair (merged_lo, merged_hi));

        // Check that we did indeed manage to insert something.
        assert (pr.second);

        insert_pt = pr.first;

    } else {
        // We already have the correct starting address, so don't need
        // to insert a new element into the map and instead should
        // just update the top of insert_pt.

        insert_pt = first_overlap;
        insert_pt->second = merged_hi;

        ++ first_overlap;
    }

    // At this point, first_overlap points at a dummy entry that
    // is the first element of the range [first_overlap,
    // after_overlap) that needs deleting.
    intervals.erase (first_overlap, after_overlap);

    // Rather than carefully working out how many elements we added,
    // let's be lame and just count them again.
    num_elements = 0;
    for (const std::pair<const int64_t, int64_t> &pr : intervals) {
        add_ivl_length (pr.first, pr.second);
    }
}

void
set_t::remove_point (int64_t val)
{
    // Find the interval containing the point. To find it, look for
    // the upper bound of val, which gives the first interval strictly
    // above val and then go back one.
    iter_t it = intervals.upper_bound (val);

    // it can only be intervals.begin() if either the set is empty or
    // val is less than the start of the first interval. Either way,
    // this means that val wasn't an element of the set, which ain't
    // allowed.
    assert (it != intervals.begin ());

    // Skip back so that the interval might contain val.
    -- it;

    // Indeed, this had better actually contain val.
    assert (it != intervals.end ());
    assert (val <= it->second);

    // Add any interval needed above
    if (val < it->second) {
        std::pair<iter_t, bool> pr =
            intervals.insert (std::make_pair (val + 1, it->second));

        (void) pr;
        // Check that we did indeed manage to insert something.
        assert (pr.second);
    }

    // Is there an interval below?
    if (it->first < val) {
        // If so, we can just update it.second to be one less than
        // val.
        it->second = val - 1;
    } else {
        // Otherwise, we just erase this entry
        intervals.erase (it);
    }

    // Record that we've removed exactly one element.
    -- num_elements;
}

void
set_t::display (std::ostream &os) const
{
    bool first = true;
    for (const_iter_t it = intervals.begin (); it != intervals.end (); ++ it) {
        if (! first)
            os << ", ";
        first = false;
        os << it->first << " .. " << it->second;
    }
}

int64_t
set_t::choose_uniformly (rnd_t& rnd) const
{
    std::uniform_int_distribution<uint64_t> distribution (0, num_elements - 1);

    uint64_t index = distribution (rnd);

    for (const std::pair<const int64_t, int64_t> &ivl : intervals) {
        // Note that the result of len_minus_one cannot overflow, just
        // doing "hi - lo" might because each is a signed int64 and
        // the subtraction is done at the wrong width.
        uint64_t len_minus_one;
        if (ivl.second < 0) {
            // If both endpoints are negative, we know that we're safe
            // (the max possible value is -1 - INT64_MIN = INT64_MAX).
            len_minus_one = ivl.second - ivl.first;
        } else if (ivl.first >= 0) {
            // If both endpoints are non-negative, we're also fine
            // (the max possible value is INT64_MAX - 0 = INT64_MAX).
            len_minus_one = ivl.second - ivl.first;
        } else {
            // If hi is positive and lo is negative, we want to do the
            // subtraction as an addition of uint64_t's. Note that we
            // have to shift both ends up by 1, because -INT64_MIN
            // isn't defined.
            //
            // The largest possible result is when lo is INT64_MIN and
            // hi is INT64_MAX. In that case, abs_lo is INT64_MAX and
            // hi is INT64_MAX + 1 and the sum is UINT64_MAX.
            uint64_t abs_lo = - (ivl.first + 1);
            uint64_t hi = (uint64_t) ivl.second + 1;
            len_minus_one =  hi + abs_lo;
        }

        // If index is less than or equal to len_minus_one, we've got
        // the right interval.
        if (index <= len_minus_one) {
            // The actual value is ivl.first + index. As above, we
            // have to be a little careful when calculating this in
            // order to avoid overflow.
            int64_t val;
            if (0 <= ivl.first) {
                // This is the easy case: since the bottom of the
                // interval is non-negative, its length is small
                // enough that index must fit in a signed int64_t.
                val = ivl.first + index;
            }
            else {
                uint64_t abs_lo = - (ivl.first + 1);
                if (index > abs_lo) {
                    // Another case is when ivl.first is negative, but
                    // ivl.first + index is not. This happens iff index >
                    // -(ivl.first + 1). Note that the +1 means we don't
                    // overflow an int64_t, even if ivl.first = INT64_MIN.
                    val = index - abs_lo - 1;
                } else if (index == 0) {
                    // Finally, there is the case when the result is also
                    // negative. If index is zero, return ivl.first.
                    val = ivl.first;
                } else {
                    // Otherwise, we can write lo + idx as
                    //
                    //    -((1 + abs_lo) - (1 + (idx - 1)))
                    //  = -(abs_lo - (idx - 1))
                    //
                    // and we know this calculation is safe (abs_lo <
                    // INT64_MAX so abs_lo-(index-1) is too).
                    val = - (int64_t) (abs_lo - (index - 1));
                }
            }
            return val;
        }

        // If we get here then index > len_minus_one, which means we
        // need to subtract the length of the interval from index and
        // keep going. (Adding 1 to len_minus_one can't overflow
        // because it's at most equal to index).
        index -= 1 + len_minus_one;
    }

    // LCOV_EXCL_START
    assert (0);
    __builtin_unreachable ();
    // LCOV_EXCL_STOP
}

void
set_t::add_ivl_length (int64_t lo, int64_t hi)
{
    assert (lo <= hi);
    uint64_t len_minus_one = hi - lo;

    // Deal with unsigned overflow by explicitly saturating. The only
    // time this can happen is when the set is the entire int64 range.
    // We'll end up one too short which means that sampling will
    // technically be wrong (you'll never get INT64_MAX). However,
    // you'd have to sample O(2^64) times from the distribution to
    // notice, so I claim that we'll get away with it!
    uint64_t new_val = num_elements + len_minus_one;
    num_elements = ((new_val < num_elements) ?
                    std::numeric_limits<uint64_t>::max () :
                    new_val);

    if (num_elements != std::numeric_limits<uint64_t>::max ())
        ++ num_elements;
}

void
arith_prog_t::display (std::ostream &os) const
{
    os << first << " .. " << last << " step " << step;
}

int64_t
arith_prog_t::initial_val () const
{
    return first;
}

bool
arith_prog_t::step_value (int64_t *val) const
{
    assert (val);
    * val += step;
    return ! ((step == 0) ||
              ((step > 0) && (* val > last)) ||
              ((step < 0) && (* val < last)));
}

void
dyn_uniform_t::display_ (std::ostream   &os,
                         const symtab_t &symbol_table,
                         const char     *element) const
{
    print_funcall ([] (std::ostream &os, const symtab_t &st,
                       const char *e, const ivl_t &ivl)
                   { ivl.display (os, st, e); },
                   os, symbol_table, element, "uniform", intervals);
}

coll_expr_t::ptr_t
dyn_uniform_t::clone() const
{
    auto clone = std::unique_ptr<dyn_uniform_t>(new dyn_uniform_t(place));
    clone->intervals.reserve(intervals.size());

    for (const ivl_t &ivl : intervals) {
        clone->intervals.push_back(ivl.clone());
    }

    return std::move (clone);
}

int64_t
dyn_uniform_t::eval (rnd_t& rnd, const std::string& filename)
{
    set_t set;

    for (const ivl_t& ivl : intervals) {
        int64_t lo = ivl.lo->eval (rnd, filename);
        int64_t hi = ivl.hi ? ivl.hi->eval (rnd, filename) : lo;

        if (hi < lo) {
            std::ostringstream oss;
            oss << "The upper bound of an interval must be >= the lower bound.";
            print_error(filename.c_str(), ivl.place, oss.str().c_str());
            throw runtime_error();
        }

        set.add_ivl (lo, hi);
    }

    return set.choose_uniformly (rnd);
}

void
dyn_uniform_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    for (const ivl_t &ivl : intervals) {
        ivl.fill_sensitivity_list (dest);
    }
}

void
const_uniform_t::display_ (std::ostream   &os,
                           const symtab_t &symbol_table,
                           const char     *element) const
{
    (void) symbol_table;
    (void) element;

    os << "uniform (";
    set.display (os);
    os << ")";
}

coll_expr_t::ptr_t
const_uniform_t::clone() const
{
    auto clone = std::unique_ptr<const_uniform_t>(new const_uniform_t(place));
    clone->set = set;
    return std::move (clone);
}

int64_t
const_uniform_t::eval (rnd_t& rnd, const std::string& filename)
{
    (void) filename;
    return set.choose_uniformly (rnd);
}

void
const_uniform_t::fill_sensitivity_list (svds_t *dest) const
{
    (void) dest;
}

void
all_t::display_ (std::ostream   &os,
                 const symtab_t &symbol_table,
                 const char     *element) const
{
    (void) symbol_table;
    (void) element;

    os << "all (";
    set.display (os);
    os << ")";
}

coll_expr_t::ptr_t
all_t::clone () const
{
    return std::unique_ptr<all_t> (new all_t (* this));
}

int64_t
all_t::eval (rnd_t& rnd, const std::string& filename)
{
    (void) filename;

    // Have we emptied set? If so, swap in a copy of fresh_set.
    if (set.empty ()) {
        set = fresh_set;
    }

    assert (! set.empty ());

    int64_t val = set.choose_uniformly (rnd);
    set.remove_point (val);

    return val;
}

void
all_t::fill_sensitivity_list (svds_t *dest) const
{
    (void) dest;
}

void
seq_t::display_ (std::ostream   &os,
                 const symtab_t &symbol_table,
                 const char     *element) const
{
    (void) symbol_table;
    (void) element;

    os << "seq (";
    bool first = true;
    for (const arith_prog_t &entry : entries) {
        if (! first)
            os << ", ";
        first = false;
        entry.display (os);
    }
    os << ")";
}

void
seq_t::add_progression (const arith_prog_t &prog)
{
    if (entries.empty ()) {
        next_idx = 0;
        next_val = prog.initial_val ();
    }

    entries.push_back (prog);
}

coll_expr_t::ptr_t
seq_t::clone() const
{
    return ptr_t (new seq_t (* this));
}

int64_t
seq_t::eval (rnd_t& rnd, const std::string& filename)
{
    (void) rnd;
    (void) filename;

    int64_t val = next_val;
    bool same_prog = entries.at (next_idx).step_value (& next_val);
    if (! same_prog) {
        ++ next_idx;
        if (next_idx >= entries.size ())
            next_idx = 0;
        next_val = entries.at (next_idx).initial_val ();
    }

    return val;
}

void
seq_t::fill_sensitivity_list (svds_t *dest) const
{
    (void) dest;
}

void
weighted_t::display_ (std::ostream   &os,
                      const symtab_t &symbol_table,
                      const char     *element) const
{
    print_funcall ([] (std::ostream &os, const symtab_t &st,
                       const char *e, const pair_t &pr)
                   {
                       pr.first->display (os, st, e, 1000);
                       if (pr.second) {
                           os << " weight ";
                           pr.second->display (os, st, e, 1000);
                       }
                   },
                   os, symbol_table, element, "weighted", entries);
}

coll_expr_t::ptr_t
weighted_t::clone() const
{
    auto clone = std::unique_ptr<weighted_t>(new weighted_t(place));
    clone->entries.reserve(entries.size());

    for (const pair_t &entry : entries) {
	coll_ptr coll (entry.first->clone ());
	int_ptr weight (entry.second ? entry.second->clone () : nullptr);

        clone->entries.push_back (pair_t (std::move (coll), std::move (weight)));
    }

    return std::move (clone);
}

int64_t
weighted_t::eval (rnd_t& rnd, const std::string& filename)
{
    assert(entries.size());

    std::vector<int64_t> weights;
    weights.reserve (entries.size ());

    int64_t total_weight = 0;
    for (const pair_t& entry : entries) {
        int64_t weight = entry.second ? entry.second->eval (rnd, filename) : 1;

        if (weight < 0) {
            std::ostringstream oss;
            oss << "The weight of an element must be non-negative.";
            print_error (filename.c_str (),
                         entry.second->place,
                         oss.str ().c_str ());
            throw runtime_error ();
        }

        weights.push_back (weight);
        total_weight += weight;
    }

    if (!total_weight) {
        std::ostringstream oss;
        oss << "The total weight of all elements must be positive.";
        print_error (filename.c_str (), place, oss.str ().c_str ());
        throw runtime_error ();
    }

    std::discrete_distribution<int> distribution (weights.begin (), weights.end ());

    int index = distribution (rnd);

    return entries[index].first->eval (rnd, filename);
}

void
weighted_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    for (const pair_t &pr : entries) {
        pr.first->fill_sensitivity_list (dest);
        if (pr.second)
            pr.second->fill_sensitivity_list (dest);
    }
}

coll_expr_t::ptr_t
int_coll_expr_t::clone() const
{
    return ptr_t (new int_coll_expr_t (place, expr->clone ()));
}

int64_t
int_coll_expr_t::eval (rnd_t& rnd, const std::string& filename)
{
    return expr->eval (rnd, filename);
}

void
int_coll_expr_t::fill_sensitivity_list (svds_t *dest) const
{
    assert (dest);
    expr->fill_sensitivity_list (dest);
}

void
int_coll_expr_t::display_ (std::ostream   &os,
                           const symtab_t &symbol_table,
                           const char     *element) const
{
    expr->display (os, symbol_table, element, 1000);
}

stmt_t::stmt_ptr_t
block_stmt_t::
clone_replace_insert (config_map_t&                   config_map,
                      insert_stack_t&                 insert_stack,
                      const std::vector<std::string> &paths,
                      int                             current_path_index,
                      bool                           *error) const
{
    auto clone = std::unique_ptr<inserts::block_stmt_t>(
            new inserts::block_stmt_t(current_path_index));

    clone->stmts.reserve(stmts.size());

    for (const std::unique_ptr<stmt_t> &stmt : stmts) {
        clone->stmts.emplace_back
            (stmt->clone_replace_insert(config_map,
                                        insert_stack,
                                        paths,
                                        current_path_index,
                                        error).release());
    }

    return std::move (clone);
}

stmt_t::stmt_ptr_t
if_stmt_t::clone_replace_insert (config_map_t& config_map,
                                 insert_stack_t& insert_stack,
                                 const std::vector<std::string> &paths,
                                 int current_path_index,
                                 bool *error) const
{
    auto clone = std::unique_ptr<inserts::if_stmt_t> (
            new inserts::if_stmt_t (current_path_index,
                                    test->clone ().release (),
                                    nullptr,
                                    nullptr));

    // Clone the "if true" statement, replacing 'insert's as necessary.
    clone->if_true.reset (
            if_true->clone_replace_insert(config_map,
                                          insert_stack,
                                          paths,
                                          current_path_index,
                                          error).release());

    // Clone the "if false" statement, replacing 'insert's as necessary.
    if (if_false)
        clone->if_false.reset (
                if_false->clone_replace_insert(config_map,
                                               insert_stack,
                                               paths,
                                               current_path_index,
                                               error).release());

    return std::move (clone);
}

stmt_t::stmt_ptr_t
assign_stmt_t::
clone_replace_insert (config_map_t&                   config_map,
                      insert_stack_t&                 insert_stack,
                      const std::vector<std::string> &paths,
                      int                             current_path_index,
                      bool                           *error) const
{
    (void) config_map;
    (void) insert_stack;
    (void) paths;
    (void) error;

    return (std::unique_ptr<inserts::assign_stmt_t>
            (new inserts::assign_stmt_t (current_path_index, dest, expr->clone().release())));
}

static bool already_inserted (std::string name, insert_stack_t &insert_stack)
{
    for (size_t i = 0; i < insert_stack.size(); i++) {
        if (std::get<0>(insert_stack[i]) == name) return true;
    }

    return false;
}

stmt_t::stmt_ptr_t
insert_stmt_t::
clone_replace_insert (config_map_t&                  config_map,
                     insert_stack_t&                 insert_stack,
                     const std::vector<std::string> &paths,
                     int                             current_path_index,
                     bool                           *error) const
{
    // Are we going to end up in an infinite loop? (i.e. is this
    // configuration already in the stack?)
    if (already_inserted(sym.name, insert_stack)) {
        // Display message to show insert stack to help the user.
        *error = true;

        std::ostringstream oss;
        oss << "Infinite insert recursion detected while inserting `"
            << sym.name << "'.";

        std::ostringstream oss_post;
        oss_post << "\nInsert stack:\n\n";

        size_t i;
        for (i = 0; i < insert_stack.size(); i++) {
            std::string name = std::get<0>(insert_stack[i]);
            int path_index = std::get<1>(insert_stack[i]);
            place_t place = std::get<2>(insert_stack[i]);

            oss_post << "(" << i << ") " << paths[path_index] << ":"
                     << place.line0 << ":" << place.col0 << ": ";

            if (i) oss_post << "`" << name << "' inserted here;\n";
            else oss_post << "started on `" << name << "' configuration;\n";
        }

        oss_post << "(" << i << ") " << paths[current_path_index] << ":"
                 << sym.place.line0 << ":" << sym.place.col0 << ":" << "`"
                 << sym.name << "' inserted here.";

        print_error (paths[current_path_index].c_str(),
                     sym.place, oss.str().c_str(), oss_post.str().c_str());

        return nullptr;
    }

    // Does the configuration that we're attempting to insert actually exist?
    auto it = config_map.find(sym.name);
    if (it == config_map.end()) {
        // Couldn't find the configuration.

        std::ostringstream oss;
        oss << "Unable to find the configuration `" << sym.name << "'.";
        print_error(paths[current_path_index].c_str(),
                    sym.place, oss.str().c_str());

        *error = true;
        return nullptr;
    }

    // Add the inserted configuration to the insert stack
    insert_stack.push_back(std::make_tuple(sym.name,
                                           current_path_index,
                                           sym.place));

    collections::config_t& config = it->second.first;

    auto block = std::unique_ptr<inserts::block_stmt_t> (
            new inserts::block_stmt_t (it->second.second));

    size_t num_stmts = config.stmts.size();
    block->stmts.reserve(num_stmts);

    for (size_t i = 0; i < num_stmts; i++) {
        block->stmts.emplace_back(
                config.stmts[i]->clone_replace_insert(config_map,
                                                      insert_stack,
                                                      paths,
                                                      it->second.second,
                                                      error).release());
    }

    // Remove the inserted configuration from the insert stack
    insert_stack.pop_back();

    return std::move (block);
}

std::unique_ptr<inserts::config_t>
config_t::
clone_replace_insert (config_map_t&                   config_map,
                      const std::vector<std::string> &paths,
                      int                             current_path_index,
                      bool                           *error) const
{
    auto clone = std::unique_ptr<inserts::config_t>(new inserts::config_t(name));
    clone->stmts.reserve(stmts.size());

    insert_stack_t insert_stack;
    insert_stack.push_back(std::make_tuple(name.name,
                                           current_path_index,
                                           name.place));

    for (const std::unique_ptr<stmt_t> &stmt : stmts) {
        clone->stmts.emplace_back(
                stmt->clone_replace_insert(config_map,
                                           insert_stack,
                                           paths,
                                           current_path_index,
                                           error).release());
    }

    insert_stack.pop_back();

    return clone;
}

static bool
coll_file (files_t          *dest,
           mem_ref::file_t &&configs,
           const char       *filename)
{
    assert (filename);

    bool good = true;
    file_t ret;

    for (mem_ref::config_t &cfg : configs) {
        good &= cfg.to_collections (filename, & ret);
    }

    if (good)
        dest->emplace_back (std::move (ret));

    return good;
}

files_t
collections::pass (mem_ref::files_t               &&src,
                   const std::vector<std::string>  &paths)
{
    assert (src.size () == paths.size ());

    std::vector<file_t> dest;

    bool good = true;
    for (size_t i = 0; i < src.size (); ++ i) {
        good &= coll_file (& dest,
                           std::move (src [i]),
                           paths [i].c_str ());
    }

    src.clear ();

    if (! good)
        throw parse_error ();

    return dest;
}
