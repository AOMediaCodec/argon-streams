/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "typing.hh"

#include "ast.hh"
#include "error.hh"
#include "mem-ref.hh"
#include "parse.hh"

#include <map>
#include <sstream>

using namespace typing;

// Instantiate the two types of conditional expression
template struct typing::cond_expr_t<int_expr_t, mem_ref::int_expr_t>;
template struct typing::cond_expr_t<coll_expr_t, mem_ref::coll_expr_t>;

typedef std::unique_ptr<mem_ref::int_expr_t>  mem_int_ptr;
typedef std::unique_ptr<mem_ref::coll_expr_t> mem_coll_ptr;

template <typename S, typename T>
static bool
exprs_to_mem_ref (const char                       *filename,
                  const char                       *element,
                  const symtab_t                   &symbol_table,
                  std::vector<std::unique_ptr<T>>  *dest,
                  std::vector<std::unique_ptr<S>> &&src)
{
    assert (filename && dest);

    bool good = true;

    for (const std::unique_ptr<S> &s : src) {
        std::unique_ptr<T> p (s->to_mem_ref (filename, element, symbol_table));
        if (! p) {
            good = false;
        } else {
            dest->emplace_back (std::move (p));
        }
    }

    src.clear ();
    return good;
}

static bool
insert_constants_at (const symtab_t              &symbol_table,
                     const char                  *filename,
                     std::unique_ptr<int_expr_t> *expr)
{
    assert (filename && expr);
    if (! * expr) return true;

    // Recurse through any children of expr. This line won't do
    // anything special if expr is itself a variable reference.
    (* expr)->insert_constants (symbol_table, filename);

    // Is *expr a variable reference?
    const variable_t *var = dynamic_cast <const variable_t *> (expr->get ());

    if (! var) return true;

    // If so, does it name a constant?
    if (! symbol_table.constants)
        return true;
    auto it = symbol_table.constants->find (var->sym.name);
    if (it == symbol_table.constants->end ())
        return true;

    // Yes it does! If there are any indices, this is not allowed
    // (2[0] doesn't make much sense!)
    if (! var->indices.empty ()) {
        std::ostringstream oss;
        oss << "The codec constant `" << var->sym.name << "' is not an array.";
        print_error (filename, var->sym.place, oss.str ().c_str ());
        return false;
    }

    // Otherwise, we can replace *expr with an integer literal.
    expr->reset (new int_literal_t ((* expr)->place, it->second));
    return true;
}

mem_int_ptr
int_literal_t::to_mem_ref (const char     *filename,
                           const char     *element,
                           const symtab_t &symbol_table)
{
    assert (filename);
    (void) filename;
    (void) element;
    (void) symbol_table;

    return mem_int_ptr (new mem_ref::int_literal_t (place, value));
}

bool
int_literal_t::insert_constants (const symtab_t &symbol_table,
                                 const char     *filename)
{
    assert (filename);
    (void) filename;
    (void) symbol_table;
    return true;
}

mem_int_ptr
variable_t::to_mem_ref (const char *filename,
                        const char *element,
                        const symtab_t &symbol_table)
{
    bool good = true;

    var_desc_t desc;
    if (! symbol_table.sym_lookup (& desc, element, sym.name.c_str ())) {
        std::ostringstream oss;
        oss << "Unknown variable: `" << sym.name << "'.";
        print_error (filename, sym.place, oss.str ().c_str ());
        good = false;
    }

    std::vector<mem_int_ptr> mem_indices;
    good &= exprs_to_mem_ref (filename, element, symbol_table,
                              & mem_indices, std::move (indices));

    return good ? mk_var (filename, place, desc, std::move (mem_indices)) : nullptr;
}

bool
variable_t::insert_constants (const symtab_t &symbol_table,
                              const char     *filename)
{
    // Note that this doesn't deal with the possible constant in the
    // variable itself. That is done by insert_constants_at, called by
    // the object that owns this one.

    assert (filename);

    bool good = true;
    for (ptr_t &index : indices) {
        good &= insert_constants_at (symbol_table, filename, & index);
    }
    return good;
}

mem_int_ptr
funcall_t::to_mem_ref (const char     *filename,
                       const char     *element,
                       const symtab_t &symbol_table)
{
    assert (filename);

    // How many arguments do we have? If more than 3, we've failed
    // already.
    size_t num_args = args.size ();
    bool good = true;
    if (num_args > 3) {
        std::ostringstream oss;
        oss << "Unknown function of " << num_args << " arguments: `"
            << sym.name << "'.";
        print_error (filename, sym.place, oss.str ().c_str ());
        good = false;
    }

    std::vector<mem_int_ptr> mem_args;
    good &= exprs_to_mem_ref (filename, element, symbol_table,
                              & mem_args, std::move (args));

    if (! good)
        return nullptr;

    mem_int_ptr ret;

    if (num_args == 0) {
        auto it = symbol_table.fun0s.find (sym.name);
        if (it == symbol_table.fun0s.end ()) {
            std::ostringstream oss;
            oss << "Unknown function of no arguments: `" << sym.name << "'.";
            print_error (filename, sym.place, oss.str ().c_str ());
        } else if (good) {
            assert (mem_args.size () == 0);
            ret.reset (new mem_ref::funcall_0_t (place,
                                                 it->second.first,
                                                 it->second.second));
        }
    } else if (num_args == 1) {
        auto it = symbol_table.fun1s.find (sym.name);
        if (it == symbol_table.fun1s.end ()) {
            std::ostringstream oss;
            oss << "Unknown function of 1 argument: `" << sym.name << "'.";
            print_error (filename, sym.place, oss.str ().c_str ());
        } else if (good) {
            assert (mem_args.size () == 1);
            ret.reset (new
                       mem_ref::funcall_1_t (place, it->second.first,
                                             it->second.second,
                                             mem_args [0].release ()));
        }
    } else if (num_args == 2) {
        auto it = symbol_table.fun2s.find (sym.name);
        if (it == symbol_table.fun2s.end ()) {
            std::ostringstream oss;
            oss << "Unknown function of 2 arguments: `" << sym.name << "'.";
            print_error (filename, sym.place, oss.str ().c_str ());
        } else if (good) {
            assert (mem_args.size () == 2);
            ret.reset (new mem_ref::funcall_2_t (place, it->second.first,
                                                 it->second.second,
                                                 mem_args [0].release (),
                                                 mem_args [1].release ()));
        }
    } else {
        assert (num_args == 3);
        auto it = symbol_table.fun3s.find (sym.name);
        if (it == symbol_table.fun3s.end ()) {
            std::ostringstream oss;
            oss << "Unknown function of 3 arguments: `" << sym.name << "'.";
            print_error (filename, sym.place, oss.str ().c_str ());
            return nullptr;
        } else if (good) {
            assert (mem_args.size () == 3);
            ret.reset (new mem_ref::funcall_3_t (place, it->second.first,
                                                 it->second.second,
                                                 mem_args [0].release (),
                                                 mem_args [1].release (),
                                                 mem_args [2].release ()));
        }
    }

    return ret;
}

bool
funcall_t::insert_constants (const symtab_t &symbol_table,
                             const char     *filename)
{
    bool good = true;
    for (ptr_t &arg : args) {
        good &= insert_constants_at (symbol_table, filename, & arg);
    }
    return good;
}

mem_int_ptr
choose_t::to_mem_ref (const char     *filename,
                      const char     *element,
                      const symtab_t &symbol_table)
{
    assert (filename);

    mem_coll_ptr m_arg (arg->to_mem_ref (filename, element, symbol_table));

    return (m_arg ?
            mem_int_ptr (new mem_ref::choose_t (place, m_arg.release ())) :
            nullptr);
}

bool
choose_t::insert_constants (const symtab_t &symbol_table,
                            const char     *filename)
{
    return arg->insert_constants (symbol_table, filename);
}

bool
ivl_t::to_mem_ref (const char     *filename,
                   const char     *element,
                   const symtab_t &symbol_table,
                   std::vector<mem_ref::ivl_t> *dest)
{
    assert (filename && dest);
    assert (lo);

    mem_int_ptr mem_lo (lo->to_mem_ref (filename, element, symbol_table));
    mem_int_ptr mem_hi (hi ?
                        hi->to_mem_ref (filename, element, symbol_table) :
                        nullptr);

    if (mem_lo && (mem_hi || ! hi)) {
        dest->push_back (mem_ref::ivl_t (place,
                                         mem_lo.release (),
                                         mem_hi.release ()));
        return true;
    }

    return false;
}

bool
ivl_t::insert_constants (const symtab_t &symbol_table,
                         const char     *filename)
{
    bool good = true;
    good &= insert_constants_at (symbol_table, filename, & lo);
    good &= insert_constants_at (symbol_table, filename, & hi);
    return good;
}

mem_coll_ptr
uniform_t::to_mem_ref (const char     *filename,
                       const char     *element,
                       const symtab_t &symbol_table)
{
    std::unique_ptr<mem_ref::uniform_t>
        ret (new mem_ref::uniform_t (place, choose_all));

    bool good = true;
    for (ivl_t &ivl : intervals) {
        good &= ivl.to_mem_ref (filename, element,
                                symbol_table, & ret->intervals);
    }

    if (! good)
        ret.reset ();

    return std::move (ret);
}

bool
uniform_t::insert_constants (const symtab_t &symbol_table,
                             const char     *filename)
{
    assert (filename);

    bool good = true;
    for (ivl_t &ivl : intervals) {
        good &= ivl.insert_constants (symbol_table, filename);
    }
    return good;
}

bool
weighted_pair_t::to_mem_ref (const char     *filename,
                             const char     *element,
                             const symtab_t &symbol_table,
                             std::vector<mem_ref::weighted_pair_t> *dest)
{
    assert (filename && dest);
    assert (coll);

    mem_coll_ptr mem_c (coll->to_mem_ref (filename, element, symbol_table));
    mem_int_ptr mem_w (weight ?
                       weight->to_mem_ref (filename, element, symbol_table) :
                       nullptr);

    if (mem_c && (mem_w || ! weight)) {
        dest->push_back (mem_ref::weighted_pair_t (mem_c.release (),
                                                   mem_w.release ()));
        return true;
    }

    return false;

}

bool
weighted_pair_t::insert_constants (const symtab_t &symbol_table,
                                   const char     *filename)
{
    assert (filename);

    bool good = true;
    good &= coll->insert_constants (symbol_table, filename);
    good &= insert_constants_at (symbol_table, filename, & weight);
    return good;
}

mem_coll_ptr
weighted_t::to_mem_ref (const char     *filename,
                        const char     *element,
                        const symtab_t &symbol_table)
{
    assert (filename);

    std::unique_ptr<mem_ref::weighted_t> ret (new mem_ref::weighted_t (place));

    bool good = true;
    for (weighted_pair_t &pr : pairs) {
        good &= pr.to_mem_ref (filename, element, symbol_table, & ret->pairs);
    }

    if (! good)
        ret.reset ();

    return std::move (ret);
}

bool
weighted_t::insert_constants (const symtab_t &symbol_table,
                              const char     *filename)
{
    assert (filename);

    bool good = true;
    for (weighted_pair_t &pair : pairs) {
        good &= pair.insert_constants (symbol_table, filename);
    }
    return good;
}

mem_int_ptr
unop_expr_t::to_mem_ref (const char     *filename,
                         const char     *element,
                         const symtab_t &symbol_table)
{
    assert (filename && arg);
    mem_int_ptr mem_arg (arg->to_mem_ref (filename, element, symbol_table));

    return (mem_arg ?
            mem_int_ptr (new mem_ref::unop_expr_t (place, op,
                                                   mem_arg.release ())) :
            nullptr);
}

bool
unop_expr_t::insert_constants (const symtab_t &symbol_table,
                               const char     *filename)
{
    assert (filename);
    return insert_constants_at (symbol_table, filename, & arg);
}

mem_int_ptr
int_binop_expr_t::to_mem_ref (const char     *filename,
                              const char     *element,
                              const symtab_t &symbol_table)
{
    assert (filename && l && r);
    mem_int_ptr mem_l (l->to_mem_ref (filename, element, symbol_table));
    mem_int_ptr mem_r (r->to_mem_ref (filename, element, symbol_table));

    return ((mem_l && mem_r) ?
            mem_int_ptr (new mem_ref::binop_expr_t (place,
                                                    mem_l.release (),
                                                    op,
                                                    mem_r.release ())) :
            nullptr);
}

bool
int_binop_expr_t::insert_constants (const symtab_t &symbol_table,
                                    const char     *filename)
{
    assert (filename);
    bool good = true;
    good &= insert_constants_at (symbol_table, filename, & l);
    good &= insert_constants_at (symbol_table, filename, & r);
    return good;
}

template <typename T1, typename T2, typename T3>
static std::unique_ptr<T2>
mk_cond_expr (const char          *filename,
              const char          *element,
              const symtab_t      &symbol_table,
              cond_expr_t<T1, T2> &in)
{
    assert (filename && in.test && in.if_true);
    mem_int_ptr test (in.test->to_mem_ref (filename, element, symbol_table));
    std::unique_ptr<T2> t (in.if_true->to_mem_ref (filename, element, symbol_table));
    std::unique_ptr<T2> f (in.if_false ?
                           in.if_false->to_mem_ref (filename, element, symbol_table) :
                           nullptr);

    return ((test && t && (f || ! in.if_false)) ?
            (std::unique_ptr<T2> (new T3 (in.place,
                                          test.release (),
                                          t.release (), f.release ()))) :
            nullptr);
}

namespace typing {
    template <>
    mem_int_ptr
    cond_expr_t<int_expr_t, mem_ref::int_expr_t>::
    to_mem_ref (const char     *filename,
                const char     *element,
                const symtab_t &symbol_table)
    {
        return mk_cond_expr<int_expr_t,
                            mem_ref::int_expr_t,
                            mem_ref::int_cond_expr_t> (filename, element,
                                                       symbol_table, *this);
    }

    template<>
    bool
    cond_expr_t<int_expr_t, mem_ref::int_expr_t>::
    insert_constants (const symtab_t &symbol_table,
                      const char     *filename)
    {
        assert (filename);

        bool good = true;
        good &= insert_constants_at (symbol_table, filename, & test);
        good &= insert_constants_at (symbol_table, filename, & if_true);
        good &= insert_constants_at (symbol_table, filename, & if_false);
        return good;
    }

    template <>
    mem_coll_ptr
    cond_expr_t<coll_expr_t, mem_ref::coll_expr_t>::
    to_mem_ref (const char     *filename,
                const char     *element,
                const symtab_t &symbol_table)
    {
        return mk_cond_expr<coll_expr_t,
                            mem_ref::coll_expr_t,
                            mem_ref::coll_cond_expr_t> (filename, element,
                                                        symbol_table, *this);
    }

    template<>
    bool
    cond_expr_t<coll_expr_t, mem_ref::coll_expr_t>::
    insert_constants (const symtab_t &symbol_table,
                      const char     *filename)
    {
        assert (filename);

        bool good = true;
        good &= insert_constants_at (symbol_table, filename, & test);
        good &= if_true->insert_constants (symbol_table, filename);
        good &= if_false->insert_constants (symbol_table, filename);
        return good;
    }
}

bool
arith_prog_t::to_mem_ref (const char     *filename,
                          const char     *element,
                          const symtab_t &symbol_table,
                          std::vector<mem_ref::arith_prog_t> *dest)
{
    assert (filename && dest);
    assert (ivl.lo);

    mem_int_ptr mem_lo (ivl.lo->to_mem_ref (filename, element, symbol_table));
    mem_int_ptr mem_hi (ivl.hi ?
                        ivl.hi->to_mem_ref (filename, element, symbol_table) :
                        nullptr);
    mem_int_ptr mem_step (step ?
                          step->to_mem_ref (filename, element, symbol_table) :
                          nullptr);

    if (mem_lo && (mem_hi || ! ivl.hi) && (mem_step || ! step)) {
        dest->push_back (mem_ref::
                         arith_prog_t (place,
                                       mem_ref::ivl_t (ivl.place,
                                                       mem_lo.release (),
                                                       mem_hi.release ()),
                                       mem_step.release ()));
        return true;
    }

    return false;
}

bool
arith_prog_t::insert_constants (const symtab_t &symbol_table,
                                const char     *filename)
{
    assert (filename);

    bool good = true;
    good &= ivl.insert_constants (symbol_table, filename);
    good &= insert_constants_at (symbol_table, filename, & step);
    return good;
}

mem_coll_ptr
seq_t::to_mem_ref (const char     *filename,
                   const char     *element,
                   const symtab_t &symbol_table)
{
    assert (filename);

    std::unique_ptr<mem_ref::seq_t> seq (new mem_ref::seq_t (place));
    bool good = true;

    for (arith_prog_t &aprog : progs) {
        good &= aprog.to_mem_ref (filename, element,
                                  symbol_table, & seq->progs);
    }

    if (! good) seq.reset ();

    return std::move (seq);
}

bool
seq_t::insert_constants (const symtab_t &symbol_table,
                         const char     *filename)
{
    assert (filename);

    bool good = true;
    for (arith_prog_t &prog : progs) {
        good &= prog.insert_constants (symbol_table, filename);
    }
    return good;
}

mem_coll_ptr
int_coll_expr_t::to_mem_ref (const char     *filename,
                             const char     *element,
                             const symtab_t &symbol_table)
{
    assert (filename);

    mem_int_ptr mem_expr (expr->to_mem_ref (filename, element, symbol_table));
    if (! mem_expr)
        return nullptr;

    return mem_coll_ptr (new mem_ref::int_coll_expr_t (place,
                                                       std::move (mem_expr)));
}

bool
int_coll_expr_t::insert_constants (const symtab_t &symbol_table,
                                   const char     *filename)
{
    assert (filename);
    return insert_constants_at (symbol_table, filename, & expr);
}

static bool
stmts_to_mem_ref (const char               *filename,
                  const symtab_t           &symbol_table,
                  mem_ref::stmt_t::ptrs_t  *dest,
                  stmt_t::ptrs_t          &&src)
{
    assert (filename && dest);

    bool good = true;

    for (const stmt_t::ptr_t &s : src) {
        mem_ref::stmt_t::ptr_t p (s->to_mem_ref (filename, symbol_table));
        if (! p) {
            good = false;
        } else {
            dest->emplace_back (std::move (p));
        }
    }

    src.clear ();
    return good;
}

std::unique_ptr<mem_ref::stmt_t>
block_stmt_t::to_mem_ref (const char *filename, const symtab_t &symbol_table)
{
    std::unique_ptr<mem_ref::block_stmt_t>
        stmt (new mem_ref::block_stmt_t);

    if (! stmts_to_mem_ref (filename, symbol_table,
                            & stmt->stmts, std::move (stmts))) {
        stmt.reset ();
    }

    return std::move (stmt);
}

bool block_stmt_t::insert_constants (const symtab_t &symbol_table,
                                     const char     *filename)
{
    assert (filename);

    bool good = true;
    for (std::unique_ptr<stmt_t> &stmt : stmts) {
        good &= stmt->insert_constants (symbol_table, filename);
    }
    return good;
}

std::unique_ptr<mem_ref::stmt_t>
if_stmt_t::to_mem_ref (const char *filename, const symtab_t &symbol_table)
{
    assert (filename && test && if_true);

    std::unique_ptr<mem_ref::int_expr_t>
        c_test (test->to_mem_ref (filename, nullptr, symbol_table));

    std::unique_ptr<mem_ref::stmt_t>
        c_if_t (if_true->to_mem_ref (filename, symbol_table));

    std::unique_ptr<mem_ref::stmt_t>
        c_if_f (if_false ?
                if_false->to_mem_ref (filename, symbol_table) :
                nullptr);

    if (! (c_test && c_if_t && ! (if_false && ! c_if_f))) {
        return nullptr;
    }

    return (std::unique_ptr<mem_ref::if_stmt_t>
            (new mem_ref::if_stmt_t (c_test.release (),
                                     c_if_t.release (),
                                     c_if_f.release ())));
}

bool
if_stmt_t::insert_constants (const symtab_t &symbol_table,
                             const char     *filename)
{
    assert (filename);

    bool good = true;
    good &= insert_constants_at (symbol_table, filename, & test);
    good &= if_true->insert_constants (symbol_table, filename);
    if (if_false)
        good &= if_false->insert_constants (symbol_table, filename);

    return good;
}

std::unique_ptr<mem_ref::stmt_t>
assign_stmt_t::to_mem_ref (const char *filename, const symtab_t &symbol_table)
{
    assert (filename);

    size_t idx = symbol_table.element_index (sym.name);
    if (idx == SIZE_MAX) {
        // Oh dear. That means that the left hand side isn't a known bitstream
        // element.
        std::ostringstream oss;
        oss << "Cannot assign to `" << sym.name
            << "': this is not a known bitstream element.";

        print_error (filename, sym.place, oss.str ().c_str ());
    }

    // Replace variable references in the right hand side.
    mem_int_ptr mem_expr (expr->to_mem_ref (filename,
                                            sym.name.c_str (),
                                            symbol_table));

    return (((idx != SIZE_MAX) && mem_expr) ?
            (std::unique_ptr<mem_ref::stmt_t>
             (new mem_ref::assign_stmt_t (idx, mem_expr.release ()))) :
            nullptr);
}

bool
assign_stmt_t::insert_constants (const symtab_t &symbol_table,
                                 const char     *filename)
{
    assert (filename);

    bool good = true;

    if (symbol_table.constants) {
        auto it = symbol_table.constants->find (sym.name);
        if (it != symbol_table.constants->end ()) {
            std::ostringstream oss;
            oss << "The codec constant `" << sym.name
                << "' cannot be assigned a value.";
            print_error (filename, sym.place, oss.str ().c_str ());
            good = false;
        }
    }

    good &= insert_constants_at (symbol_table, filename, & expr);

    return good;
}

std::unique_ptr<mem_ref::stmt_t>
insert_stmt_t::to_mem_ref (const char *filename, const symtab_t &symbol_table)
{
    assert (filename);
    (void) filename;
    (void) symbol_table;

    return (std::unique_ptr<mem_ref::stmt_t>
            (new mem_ref::insert_stmt_t (std::move (sym))));
}

bool
insert_stmt_t::insert_constants (const symtab_t &symbol_table,
                                 const char     *filename)
{
    assert (filename);
    (void) filename;
    (void) symbol_table;
    return true;
}

bool
config_t::to_mem_ref (const char                     *filename,
                      const symtab_t                 &symbol_table,
                      std::vector<mem_ref::config_t> *dest)
{
    assert (filename && dest);

    mem_ref::config_t cfg (std::move (name));

    if (stmts_to_mem_ref (filename, symbol_table,
                          & cfg.stmts, std::move (stmts))) {
        dest->emplace_back (std::move (cfg));
        return true;
    }

    return false;
}

bool
config_t::insert_constants (const symtab_t &symbol_table,
                            const char     *filename)
{
    assert (filename);

    bool good = true;
    for (std::unique_ptr<stmt_t> &stmt : stmts) {
        good &= stmt->insert_constants (symbol_table, filename);
    }
    return good;
}

static bool type_file (std::vector<file_t> *dest,
                       const tl_list       *stmts,
                       const char          *filename)
{
    assert (dest && filename);

    file_t ret;
    bool good = true;
    while (stmts) {
        assert (stmts->element);
        good &= stmts->element->add_cfg (filename, & ret);
        stmts = stmts->next.get ();
    }

    if (good)
        dest->emplace_back (std::move (ret));

    return good;
}

files_t
typing::pass (std::vector<std::unique_ptr<tl_list>> &&src,
              const std::vector<std::string>         &paths)
{
    assert (src.size () == paths.size ());

    files_t dest;

    bool good = true;
    for (size_t i = 0; i < src.size (); ++ i) {
        good &= type_file (& dest,
                           src [i].get (),
                           paths [i].c_str ());
    }

    src.clear ();

    if (! good)
        throw parse_error ();

    return dest;
}

void
typing::insert_constants (const symtab_t                 &symbol_table,
                          files_t                        *files,
                          const std::vector<std::string> &paths)
{
    assert (files && files->size () == paths.size ());
    bool good = true;
    for (size_t i = 0; i < files->size(); i++) {
        for (typing::config_t &config : files->at (i)) {
            good &= config.insert_constants (symbol_table, paths [i].c_str ());
        }
    }

    if (! good)
        throw parse_error ();
}
