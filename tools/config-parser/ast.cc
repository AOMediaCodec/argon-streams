/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "ast.hh"
#include "typing.hh"
#include "config.tab.h"
#include "error.hh"

#include <inttypes.h>
#include <limits>
#include <sstream>

// Run some form of typing function (map_fun) on each element of elts.
// If dest is non-null, store the results into it, otherwise discard
// them. Return true if all elements were converted successfully.
template <typename S, typename T, typename F, typename G>
static bool
add_typed_general (F                  map_fun,
                   G                  set_fun,
                   const char        *filename,
                   std::vector<T>    *dest,
                   const elt_list<S> *elts)
{
  assert (filename);

  bool good = true;
  while (elts) {
    assert (elts->element);
    auto typed = map_fun (filename, * elts->element);
    if (typed) {
      if (dest) {
        dest->emplace_back (set_fun (std::move (typed)));
      }
    } else {
      good = false;
    }
    elts = elts->next.get ();
  }

  return good;
}


template <typename S, typename T, typename F>
static bool
add_typed_indirect (F                                map_fun,
                    const char                      *filename,
                    std::vector<std::unique_ptr<T>> *dest,
                    const elt_list<S>               *elts)
{
  return add_typed_general (map_fun,
                            [] (std::unique_ptr<T> &&p)
                            { return std::move (p); },
                            filename, dest, elts);
}

template <typename S, typename T, typename F>
static bool
add_typed_direct (F                  map_fun,
                  const char        *filename,
                  std::vector<T>    *dest,
                  const elt_list<S> *elts)
{
  return add_typed_general (map_fun,
                            [] (std::unique_ptr<T> &&p)
                            { return std::move (* p); },
                            filename, dest, elts);
}

static bool
add_typed_stmts (const char                        *filename,
                 std::vector<statement::typed_ptr> *dest,
                 const statement_list              *stmts)
{
  return add_typed_indirect ([] (const char *filename, const statement &s)
                             { return s.mk_typed (filename); },
                             filename,
                             dest,
                             stmts);
}

static bool
add_int_exprs (const char                       *filename,
               std::vector<expression::int_ptr> *dest,
               const expression_list            *exprs)
{
  return add_typed_indirect ([] (const char *filename, const expression &e)
                             { return e.mk_int (filename, "integer"); },
                             filename,
                             dest,
                             exprs);
}

static bool
add_coll_exprs (const char                        *filename,
                std::vector<expression::coll_ptr> *dest,
                const expression_list             *exprs)
{
  return add_typed_indirect ([] (const char *filename, const expression &e)
                             { return e.mk_coll (filename, "collection"); },
                             filename,
                             dest,
                             exprs);
}

static bool
add_ivl_exprs (const char                 *filename,
               std::vector<typing::ivl_t> *dest,
               const expression_list      *exprs)
{
  return add_typed_direct ([] (const char *filename, const expression &e)
                           { return e.mk_ivl (filename); },
                           filename,
                           dest,
                           exprs);
}

static bool
add_wpair_exprs (const char                           *filename,
                 std::vector<typing::weighted_pair_t> *dest,
                 const expression_list                *exprs)
{
  return add_typed_direct ([] (const char *filename, const expression &e)
                           { return e.mk_weighted (filename); },
                           filename,
                           dest,
                           exprs);
}

static bool
add_aprog_exprs (const char                        *filename,
                 std::vector<typing::arith_prog_t> *dest,
                 const expression_list             *exprs)
{
  return add_typed_direct ([] (const char *filename, const expression &e)
                           { return e.mk_aprog (filename); },
                           filename,
                           dest,
                           exprs);
}

// This is the base implementation of mk_int_uminus, which just
// ignores the fact that this might be inside a unary minus.
expression::int_ptr
expression::mk_int_uminus (const char    *filename,
                           const char    *context,
                           const place_t *unop_place,
                           bool          *is_unop_lit) const
{
  (void) unop_place;
  (void) is_unop_lit;

  return mk_int (filename, context);
}

// This is the base implementation of mk_ivl. The point is that an
// integer expression is a valid interval.
expression::ivl_ptr
expression::mk_ivl (const char *filename) const
{
  assert (filename);
  int_ptr int_val (mk_int (filename, "interval"));
  return (int_val ?
          ivl_ptr (new typing::ivl_t (place, int_val.release (), nullptr)) :
          nullptr);
}

expression::coll_ptr
expression::mk_coll (const char *filename,
                     const char *context) const
{
  assert (filename && context);
  (void) context;

  int_ptr as_int (mk_int (filename, "integer (as degenerate collection)"));
  if (! as_int)
    return nullptr;

  return coll_ptr (new typing::int_coll_expr_t (place, std::move (as_int)));
}

// This is the base implementation of mk_weighted. The point is that a
// collection is a valid weighted expression.
expression::wpair_ptr
expression::mk_weighted (const char *filename) const
{
  assert (filename);
  coll_ptr coll (mk_coll (filename, "weighted expression"));

  return (coll ?
          wpair_ptr (new typing::weighted_pair_t (coll.release (), nullptr)) :
          nullptr);
}

// This is the base implementation of mk_aprog. The point is that
// an interval is a valid arithmetic progression.
expression::aprog_ptr
expression::mk_aprog (const char *filename) const
{
  assert (filename);
  ivl_ptr ivl (mk_ivl (filename));

  return (ivl ?
          aprog_ptr (new typing::arith_prog_t (place,
                                               std::move (* ivl),
                                               nullptr)) :
          nullptr);
}

expression::int_ptr
unary_operator_node::mk_int (const char *filename,
                             const char *context) const
{
  assert (filename && context);
  (void) context;

  int_ptr typed_arg;

  // Unary minus is special because we squeeze it into integer literals.
  if (op == UNOP_MINUS) {
    bool unop_lit = false;
    typed_arg = arg->mk_int_uminus (filename, "integer", & place, & unop_lit);
    if (typed_arg && unop_lit) {
      // The argument was an integer literal and has eaten the unary minus.
      return typed_arg;
    }
  } else {
    typed_arg = arg->mk_int (filename, "integer");
  }

  return (typed_arg ?
          int_ptr (new typing::unop_expr_t (place, op, typed_arg.release ())) :
          nullptr);
}

expression::int_ptr
binary_operator_node::mk_int (const char *filename,
                              const char *context) const
{
  assert (filename && context);

  if (op == BINOP_RANGE) {
    // Range expects integers as arguments
    l->mk_int (filename, "integer");
    r->mk_int (filename, "integer");

    std::ostringstream oss;
    oss << "The `..' operator yields an interval, not an expression of type "
        << context << ".";

    print_error (filename, place, oss.str ().c_str ());
    return nullptr;
  }

  if (op == BINOP_WEIGHT) {
    // Weight expects a collection and an integer
    l->mk_coll (filename, "collection");
    r->mk_int (filename, "integer");

    std::ostringstream oss;
    oss << "The `weight' operator yields a weighted expression, "
        << "not one of type " << context << ".";
    print_error (filename, place, oss.str ().c_str ());
    return nullptr;
  }

  // The remaining operators should be at most BINOP_LAST_INT_OP and
  // are all "int -> int -> int".
  assert (op <= BINOP_LAST_INT_OP);

  assert (l && r);
  int_ptr typed_l (l->mk_int (filename, "integer"));
  int_ptr typed_r (r->mk_int (filename, "integer"));
  int_binop_t typed_op = static_cast<int_binop_t> (op);

  return ((typed_l && typed_r) ?
          int_ptr (new typing::int_binop_expr_t (place,
                                                 typed_l.release (),
                                                 typed_op,
                                                 typed_r.release ())) :
          nullptr);
}

expression::coll_ptr
binary_operator_node::mk_coll (const char *filename, const char *context) const
{
  assert (filename && context);

  if (op == BINOP_RANGE) {
    // Range expects integers as arguments
    l->mk_int (filename, "integer");
    r->mk_int (filename, "integer");

    std::ostringstream oss;
    oss << "The `..' operator yields an interval, not a "
        << context << ".";
    print_error (filename, place, oss.str ().c_str ());
    return nullptr;
  }

  if (op == BINOP_WEIGHT) {
    // Weight expects a collection and an integer
    l->mk_coll (filename, "collection");
    r->mk_int (filename, "integer");

    std::ostringstream oss;
    oss << "The `weight' operator yields a weighted expression, not a "
        << context << ".";
    print_error (filename, place, oss.str ().c_str ());
    return nullptr;
  }

  // The remaining operators should be at most BINOP_LAST_INT_OP and
  // are all "int -> int -> int". Fall back on the default
  // implementation to wrap up as a degenerate collection.
  return expression::mk_coll (filename, context);
}

expression::ivl_ptr
binary_operator_node::mk_ivl (const char *filename) const
{
  // If the top-level operator is "..", we can build a proper interval
  // out of the arguments.
  if (op == BINOP_RANGE) {
    int_ptr typed_l (l->mk_int (filename, "integer"));
    int_ptr typed_r (r->mk_int (filename, "integer"));
    return ((typed_l && typed_r) ?
            ivl_ptr (new typing::ivl_t (place,
                                        typed_l.release (),
                                        typed_r.release ())) :
            nullptr);
  }

  // Otherwise, fall back to parsing this as an integer expression.
  return expression::mk_ivl (filename);
}

expression::wpair_ptr
binary_operator_node::mk_weighted (const char *filename) const
{
  assert (filename);

  // If the top-level operator is "weight", we can build an actual
  // weighted expression
  if (op == BINOP_WEIGHT) {
    coll_ptr typed_l (l->mk_coll (filename, "collection"));
    int_ptr typed_r (r->mk_int (filename, "integer"));

    return ((typed_l && typed_r) ?
            wpair_ptr (new typing::weighted_pair_t (typed_l.release (),
                                                    typed_r.release ())) :
            nullptr);
  }

  // Otherwise, fall back to parsing this as an collection expression.
  // This will actually always fail (because no binary operator
  // results in a collection).
  return expression::mk_weighted (filename);
}

expression::int_ptr
cond_op_node::mk_int (const char *filename, const char *context) const
{
  assert (filename && context);

  std::ostringstream oss;
  oss << "integer (ternary operand in " << context << " context)";

  assert (p && t && f);
  int_ptr typed_p (p->mk_int (filename, "integer"));
  int_ptr typed_t (t->mk_int (filename, oss.str ().c_str ()));
  int_ptr typed_f (f->mk_int (filename, oss.str ().c_str ()));

  typedef typing::cond_expr_t<typing::int_expr_t,
                              mem_ref::int_expr_t> cond_t;

  return ((typed_p && typed_t && typed_f) ?
          int_ptr (new cond_t (place,
                               typed_p.release (),
                               typed_t.release (),
                               typed_f.release ())) :
          nullptr);
}

expression::coll_ptr
cond_op_node::mk_coll (const char *filename, const char *context) const
{
  assert (filename && context);

  std::ostringstream oss;
  oss << "collection (ternary operand in " << context << " context)";

  assert (p && t && f);
  int_ptr typed_p (p->mk_int (filename, "integer"));
  coll_ptr typed_t (t->mk_coll (filename, oss.str ().c_str ()));
  coll_ptr typed_f (f->mk_coll (filename, oss.str ().c_str ()));

  typedef typing::cond_expr_t<typing::coll_expr_t,
                              mem_ref::coll_expr_t> cond_t;

  return ((typed_p && typed_t && typed_f) ?
          coll_ptr (new cond_t (place,
                                typed_p.release (),
                                typed_t.release (),
                                typed_f.release ())) :
          nullptr);
}

expression::int_ptr
arith_prog_node::mk_int (const char *filename,
                         const char *context) const
{
  assert (filename && context);

  // This is going to fail because we're not an integer, but we should
  // try to type the constituent parts too.
  assert (a && b && step);
  a->mk_int (filename, "integer");
  b->mk_int (filename, "integer");
  step->mk_int (filename, "integer");

  std::ostringstream oss;
  oss << "An arithmetic progression is not an expression of type "
      << context << ".";
  print_error (filename, place, oss.str ().c_str ());
  return nullptr;
}

expression::coll_ptr
arith_prog_node::mk_coll (const char *filename, const char *context) const
{
  assert (filename && context);

  // This is going to fail because we're not a collection, but we should
  // try to type the constituent parts too.
  assert (a && b && step);
  a->mk_int (filename, "integer");
  b->mk_int (filename, "integer");
  step->mk_int (filename, "integer");

  std::ostringstream oss;
  oss << "An arithmetic progression is not a " << context << ".";

  print_error (filename, place, oss.str ().c_str ());
  return nullptr;
}

expression::aprog_ptr
arith_prog_node::mk_aprog (const char *filename) const
{
  assert (filename);

  assert (a && b && step);
  int_ptr typed_a (a->mk_int (filename, "integer"));
  int_ptr typed_b (b->mk_int (filename, "integer"));
  int_ptr typed_step (step->mk_int (filename, "integer"));

  if (! (typed_a && typed_b && typed_step))
    return nullptr;

  typing::ivl_t ivl (a->place + b->place,
                     typed_a.release (), typed_b.release ());
  return aprog_ptr (new typing::arith_prog_t (place, std::move (ivl),
                                              typed_step.release ()));
}

expression::int_ptr
lit_node::mk_int_uminus (const char    *filename,
                         const char    *context,
                         const place_t *unop_place,
                         bool          *is_unop_lit) const
{
  assert (filename && context && (is_unop_lit || ! unop_place));
  (void) context;

  // Try to parse the string as a uintmax_t. This type is definitely at least as
  // large as a uint64_t (and thus will be able to hold the absolute value of
  // anything that an int64_t can hold). Skipping two characters when base is 2
  // means we skip the '0b' prefix.
  const char *str = str_value.c_str () + ((base == 2) ? 2 : 0);
  char *end;
  errno = 0;
  uintmax_t wide_val = strtoumax (str, & end, base);

  if (errno || * end) {
    // We didn't manage to parse the entire literal as a string in the given
    // base.
    std::ostringstream oss;
    oss << "Failed to parse `" << str_value << "' as a base "
        << base << " integer literal.";
    print_error (filename, place, oss.str ().c_str ());
    return nullptr;
  }

  bool minus = false;
  if (unop_place) {
    * is_unop_lit = true;
    minus = true;
  }

  int64_t val;

  if (minus) {
    uintmax_t abs_min = 1 + (uintmax_t) std::numeric_limits<int64_t>::max ();
    if (abs_min < wide_val) {
      std::ostringstream oss;
      oss << "The value -" << wide_val
          << " is too negative to fit in a 64-bit two's complement integer.";
      place_t whole_place = * unop_place + place;
      print_error (filename, whole_place, oss.str ().c_str ());
      return nullptr;
    }

    if (wide_val == 0) {
      val = 0;
    } else {
      // Since wide_val <= abs_min, we know that wide_val - 1 is safely
      // representable as an int64_t. We can negate that and then subtract one
      // again to get the correct value.
      //
      // I suspect this will compile away to nothing...
      val = (- ((int64_t) (wide_val - 1))) - 1;
    }
  } else {
    uintmax_t abs_max = (uintmax_t) std::numeric_limits<int64_t>::max ();
    if (abs_max < wide_val) {
      std::ostringstream oss;
      oss << "The value " << wide_val
          << " is too large to fit in a 64-bit two's complement integer.";
      print_error (filename, place, oss.str ().c_str ());
      return nullptr;
    }

    val = wide_val;
  }

  return int_ptr (new typing::int_literal_t (place, val));
}

expression::int_ptr
lit_node::mk_int (const char *filename, const char *context) const
{
  return mk_int_uminus (filename, context, nullptr, nullptr);
}

expression::int_ptr
variable_node::mk_int (const char *filename, const char *context) const
{
  assert (filename && context);
  (void) context;

  std::unique_ptr<typing::variable_t>
    var (new typing::variable_t (place, * sym));

  if (! add_int_exprs (filename, & var->indices, indices.get ())) {
    var.reset ();
  }

  return std::move (var);
}

expression::int_ptr
fn_node::mk_int (const char *filename, const char *context) const
{
  assert (filename);

  if ((id == -1) ||
      ((id == FN_MIN) || (id == FN_MAX) || (id == FN_CLAMP) ||
       (id == FN_CHOOSE_BITS) || (id == FN_CHOOSE_BIT) ||
       (id == FN_CHOOSE_BERNOULLI))) {
      // This is either a non-builtin function call (which we assume
      // always has integer arguments and result) or it's one of the
      // built-ins with integer inputs and output.

      if (id == -1) {
          assert (! name.empty ());
      } else {
          assert (name.empty ());
      }

      std::unique_ptr<typing::funcall_t>
          fun (new typing::funcall_t (place, symbol (name_place,
                                                     get_function_name ())));

      if (! add_int_exprs (filename, & fun->args, args.get ())) {
          fun.reset ();
      }

      return std::move (fun);
  }

  // FN_CHOOSE is the choose() function, which takes one collection and
  // returns an integer.
  if (id == FN_CHOOSE) {
    if ((! args) || (args && args->next)) {
      add_coll_exprs (filename, nullptr, args.get ());
      print_error (filename, place,
                   "The choose function expects exactly one argument.");
      return nullptr;
    }

    assert (args && args->element);

    coll_ptr coll (args->element->mk_coll (filename, "collection"));

    return (coll ?
            expression::int_ptr (new typing::choose_t (place,
                                                       name_place,
                                                       coll.release ())) :
            nullptr);
  }

  // All the functions below return collections. We'll still try to
  // parse them (to get errors from their arguments), but we'll then
  // raise an error about the return type.
  bool known_fun = false;

  // COL_UNIFORM is the uniform() function, which takes a list of
  // intervals and returns a collection. COL_ALL has the same types
  // (but later on we'll require the intervals to be constant)
  if ((id == COL_UNIFORM) || (id == COL_ALL)) {
    add_ivl_exprs (filename, nullptr, args.get ());
    known_fun = true;
  }

  // COL_SEQ is the seq() function, which takes a list of arithmetic
  // progressions and returns a collection.
  if (id == COL_SEQ) {
    add_aprog_exprs (filename, nullptr, args.get ());
    known_fun = true;
  }

  // COL_WEIGHTED is the weighted() function, which takes a weighted
  // list of collections and returns a collection.
  if (id == COL_WEIGHTED) {
    add_wpair_exprs (filename, nullptr, args.get ());
    known_fun = true;
  }

  // We don't know any other builtin functions: if known_fun is false,
  // something has gone horribly wrong.
  assert (known_fun);
  (void) known_fun;

  // If we get here, this means we had a builtin function with the
  // wrong return type. Complain about it.
  std::ostringstream oss;
  oss << "The built-in function `" << get_function_name ()
      << "' results in a collection, not an expression of type "
      << context << ".";

  print_error (filename, place, oss.str ().c_str ());
  return nullptr;
}

expression::coll_ptr
fn_node::mk_coll (const char *filename, const char *context) const
{
  assert (filename && context);

  if (id == -1) {
    // We assume that a non-builtin function call always has integer
    // arguments and result. Fall back on mk_int.
    return expression::mk_coll (filename, context);
  }

  // This is a builtin function. Some of these functions are also just
  // int* -> int. Let's deal with them first, again falling back on
  // mk_int.
  assert (name.empty ());
  if ((id == FN_MIN) || (id == FN_MAX) || (id == FN_CLAMP) ||
      (id == FN_CHOOSE_BITS) || (id == FN_CHOOSE_BIT) ||
      (id == FN_CHOOSE_BERNOULLI)) {
    return expression::mk_coll (filename, context);
  }

  // FN_CHOOSE is the choose() function, which takes one collection and
  // returns an integer. Yet again, we can just use mk_int here.
  if (id == FN_CHOOSE) {
    return expression::mk_coll (filename, context);
  }

  // All the functions below return collections.

  // COL_UNIFORM is the uniform() function, which takes a list of
  // intervals and returns a collection. COL_ALL has the same types
  // (but later on we'll require the intervals to be constant)
  if ((id == COL_UNIFORM) || (id == COL_ALL)) {
    std::unique_ptr<typing::uniform_t>
      uniform (new typing::uniform_t (place, id == COL_ALL));

    if (! add_ivl_exprs (filename, & uniform->intervals, args.get ()))
      uniform.reset ();

    return std::move (uniform);
  }

  // COL_SEQ is the seq() function, which takes a list of arithmetic
  // progressions and returns a collection.
  if (id == COL_SEQ) {
    std::unique_ptr<typing::seq_t> seq (new typing::seq_t (place));

    if (! add_aprog_exprs (filename, & seq->progs, args.get ()))
      seq.reset ();

    return std::move (seq);
  }

  // COL_WEIGHTED is the weighted() function, which takes a weighted
  // list of collections and returns a collection.
  if (id == COL_WEIGHTED) {
    std::unique_ptr<typing::weighted_t>
      weighted (new typing::weighted_t (place));

    if (! add_wpair_exprs (filename, & weighted->pairs, args.get ()))
      weighted.reset ();

    return std::move (weighted);
  }

  // We don't know any other builtin functions so should never get
  // here. Mark it unused for coverage too: we don't expect to hit it!

  assert (0); // LCOV_EXCL_LINE
  __builtin_unreachable (); // LCOV_EXCL_LINE
}

const char *fn_node::get_function_name() const {
  if (id == -1) {
    return name.c_str ();
  }

  switch (id) {
    case FN_MIN: return "min";
    case FN_MAX: return "max";
    case FN_CLAMP: return "clamp";
    case FN_CHOOSE_BITS: return "choose_bits";
    case FN_CHOOSE_BIT: return "choose_bit";
    case FN_CHOOSE_BERNOULLI: return "choose_bernoulli";

    case COL_UNIFORM: return "uniform";
    case COL_ALL: return "all";
    case COL_SEQ: return "seq";
    case COL_WEIGHTED: return "weighted";

    // LCOV_EXCL_START

    // This case shouldn't be reachable. We only call get_function_name() when
    // building integer -> integer functions (not choose()) or to generate error
    // messages for functions that return collections (also not choose()).
    case FN_CHOOSE:
      // return "choose";
      assert (0);
      __builtin_unreachable ();

    default:
      // This should not be possible: if id is not -1, it came from
      // the builtin constructor to fn_node.
      assert (0);
      __builtin_unreachable ();
    // LCOV_EXCL_STOP
  }
}

statement::typed_ptr
assignment::mk_typed (const char *filename) const
{
  assert (filename);

  assert (expr);
  expression::int_ptr rhs (expr->mk_int (filename, "integer"));

  return (rhs ?
          typed_ptr (new typing::assign_stmt_t (* sym, rhs.release ())) :
          nullptr);
}

statement::typed_ptr
statement_block::mk_typed (const char *filename) const
{
  assert (filename);

  std::unique_ptr<typing::block_stmt_t> block (new typing::block_stmt_t ());

  if (! add_typed_stmts (filename, & block->stmts, statements.get ()))
    block.reset ();

  return std::move (block);
}

statement::typed_ptr
if_else_statement::mk_typed (const char *filename) const
{
  assert (filename);

  assert (test && if_true);
  expression::int_ptr typed_test (test->mk_int (filename, "integer"));
  typed_ptr typed_true (if_true->mk_typed (filename));

  typed_ptr typed_false;
  if (if_false) {
    typed_false = if_false->mk_typed (filename);
  }

  return typed_ptr ((test && typed_true && (typed_false || ! if_false)) ?
                    new typing::if_stmt_t (typed_test.release (),
                                           typed_true.release (),
                                           typed_false.release ()) :
                    nullptr);
}

statement::typed_ptr
insert_statement::mk_typed (const char *filename) const
{
  assert (filename);
  (void) filename;

  return typed_ptr (new typing::insert_stmt_t (* sym));
}

bool
import_statement::add_cfg (const char *filename,
                           std::vector<config_t> *cfgs) const
{
  assert (filename && cfgs);
  (void) filename;
  (void) cfgs;

  // These statements are ignored: we've already dealt with them and
  // can just skip over them.

  return true;
}

bool
configuration_statement::add_cfg (const char *filename,
                                  std::vector<config_t> *cfgs) const
{
  assert (filename && cfgs);

  typing::config_t cfg (* name);
  if (add_typed_stmts (filename, & cfg.stmts, statements.get ())) {
    cfgs->emplace_back (std::move (cfg));
    return true;
  }

  return false;
}
