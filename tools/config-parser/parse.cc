/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "parse.hh"

#include "config.tab.h"
#include "config.lex.h"
#include "error.hh"
#include "file.hh"

#include <cassert>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <deque>

// Globals for communicating with the bison parser
tl_list           *parser_tl_list;
static const char *parser_file_name;
static bool        parser_saw_error;

typedef std::unique_ptr<FILE, int(*)(FILE *)> file_handle_t;

struct global_parse_context_t
{
    file_handle_t file_handle;

    global_parse_context_t (const char *filename, bool debug)
        : file_handle (fopen (filename, "r"), fclose)
    {
        assert (! yyin);
        assert (! parser_tl_list);
        assert (! parser_file_name);
        assert (! parser_saw_error);

        if (! file_handle) {
            const char *err = strerror (errno);
            std::cerr << "Failed to open config file at `" << filename << "': "
                      << err << ".\n";
            throw parse_error ();
        }

        yyin = file_handle.get ();
        yydebug = debug ? 1 : 0;
        parser_file_name = filename;
    }

    ~ global_parse_context_t ()
    {
        yyin = nullptr;
        delete parser_tl_list;
        parser_tl_list = nullptr;
        parser_file_name = nullptr;
        parser_saw_error = false;
    }
};

// seen_file_ids is the set of file IDs that we've seen already,
// either that we've parsed already or that we've added to our work
// list. work_list is a list of files that we still have to parse.
// Returns true on success or false if something went wrong.
static bool
add_new_file_ids (const std::string           &path,
                  std::vector<std::string>    *paths,
                  std::vector<file_unique_id> *seen_file_ids,
                  const tl_statement          &tl)
{
    assert (paths && seen_file_ids);

    const import_statement *import =
        dynamic_cast<const import_statement *> (& tl);

    if (! import) return true;

    // We have an import statement. Resolve the include name to a
    // path.
    std::string inc_path = resolve_include (path.c_str (),
                                            import->file_name.c_str ());

    file_unique_id id;
    if (! get_file_data (& id, inc_path.c_str ())) {
        std::ostringstream oss;
        oss << "Cannot find file at path `" << inc_path << "'.";
        print_error (path.c_str (), import->place, oss.str ().c_str ());
        return false;
    }

    if (std::find (seen_file_ids->begin (), seen_file_ids->end (), id)
        !=
        seen_file_ids->end ()) {
        // We've seen this file before. Skip it.
        return true;
    }

    // This is a new file. Add its id to seen_file_ids and its name to
    // the list of paths to read.
    seen_file_ids->push_back (id);
    paths->emplace_back (std::move (inc_path));
    return true;
}

static bool
add_new_file_ids (size_t                       next_path,
                  std::vector<std::string>    *paths,
                  std::vector<file_unique_id> *seen_file_ids,
                  const tl_list               *tls)
{
    assert (paths && seen_file_ids && (next_path < paths->size ()));

    bool good = true;
    while (tls) {
        assert (tls->element);
        // Note that we recompute the reference to paths[next_path]
        // each time in case it is invalidated by pushing a path onto
        // the end inside the function call.
        good &= add_new_file_ids (paths->at (next_path),
                                  paths, seen_file_ids,
                                  * tls->element);
        tls = tls->next.get ();
    }

    return good;
}

typedef std::unique_ptr<tl_list> ast_t;

static bool
parse_file (bool                         debug,
            size_t                       next_path,
            std::vector<std::string>    *paths,
            std::vector<ast_t>          *asts,
            std::vector<file_unique_id> *seen_file_ids)
{
    assert (paths && asts && seen_file_ids && (next_path < paths->size ()));

    const char *path = paths->at (next_path).c_str ();

    global_parse_context_t gpc (path, debug);
    if (yyparse ()) throw parse_error ();
    if (parser_saw_error) throw parse_error ();

    assert (parser_tl_list);
    asts->push_back (ast_t (parser_tl_list));
    parser_tl_list = nullptr;

    return add_new_file_ids (next_path, paths, seen_file_ids,
                             asts->back ().get ());
}

std::pair <std::vector<std::string>, std::vector<ast_t>>
parse (const char *path, bool debug)
{
    assert (path);

    size_t                      next_path;
    std::vector<std::string>    paths;
    std::vector<ast_t>          asts;
    std::vector<file_unique_id> seen_file_ids;

    file_unique_id top_id;
    if (! get_file_data (& top_id, path)) {
        std::cerr << path << ":" << "error: Cannot stat file.\n";
        throw parse_error ();
    }
    seen_file_ids.push_back (top_id);

    paths.push_back (path);
    next_path = 0;

    bool good = true;
    while (next_path < paths.size ()) {
        good &= parse_file (debug,
                            next_path, & paths,
                            & asts, & seen_file_ids);
        ++ next_path;
    }

    if (! good)
        throw parse_error ();

    return std::make_pair (std::move (paths), std::move (asts));
}

void yyerror (const char *msg)
{
    parser_saw_error = true;
    print_error (parser_file_name, yylloc, msg);
}
