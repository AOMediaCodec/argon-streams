/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include "ast-ops.hh"
#include "place.hh"
#include "symbol.hh"

#include <cassert>
#include <cstdint>
#include <string>
#include <memory>
#include <vector>
#include <list>

template <typename T>
struct elt_list
{
    std::unique_ptr<T>           element;
    std::unique_ptr<elt_list<T>> next;

    elt_list (elt_list<T> *next, T *element)
        : element (element), next (next)
    {
        assert (element);
        // next may be null (if we're at the end of the list)
    }
};

// Forward declarations
namespace typing {
    struct int_expr_t;
    struct coll_expr_t;
    struct ivl_t;
    struct weighted_pair_t;
    struct arith_prog_t;
    struct stmt_t;
    struct config_t;
}

class expression
{
public:
    expression (const place_t &place)
        : place (place)
    {}

    virtual ~expression() {}

    typedef std::unique_ptr <typing::int_expr_t>      int_ptr;
    typedef std::unique_ptr <typing::coll_expr_t>     coll_ptr;
    typedef std::unique_ptr <typing::ivl_t>           ivl_ptr;
    typedef std::unique_ptr <typing::weighted_pair_t> wpair_ptr;
    typedef std::unique_ptr <typing::arith_prog_t>    aprog_ptr;

    // mk_int interprets this expression in a context where we expect
    // an integer. If successful, it returns the typed expression.
    // Otherwise, it prints one or more errors and returns nullptr.
    //
    // We want to special case the situation where we apply a unary
    // minus to an integer. In this case, we call mk_int_uminus,
    // passing in the non-null location of the uminus (for error
    // messages) and a non-null pointer as is_unop_lit. If this is a
    // (valid) literal 123, it will parse to -123 and set *is_unop_lit
    // to true. If unop_place is null, this should be treated as a
    // call to mk_int (not uminus).
    //
    // In both cases, context is the actual type we're trying to make.
    // Most of the time it will be "integer", but not always. For
    // example, if we're actually parsing an interval and we've fallen
    // back to trying to parse an integer, we want to say that XYZ is
    // not an interval, rather than that XYZ is not an integer.
    //
    // The default implementation of mk_int_uminus falls back on
    // mk_int (only integer literals need to override this).
    virtual int_ptr mk_int_uminus (const char    *filename,
                                   const char    *context,
                                   const place_t *unop_place,
                                   bool          *is_unop_lit) const;

    virtual int_ptr mk_int (const char *filename,
                            const char *context) const = 0;

    // Interpret this expression in a context where we expect an
    // interval. If successful, return it. Otherwise, print one or
    // more errors and return nullptr.
    virtual ivl_ptr mk_ivl (const char *filename) const;

    // Interpret this expression in a context where we expect a
    // collection. If successful, return it. Otherwise, print one or
    // more errors and return nullptr.
    //
    // Since an expression that should really be an integer can be
    // evaluated as a collection (via typing::int_coll_expr_t), there
    // is a base implementation of this method which interprets the
    // expression as an integer and wraps it up in that.
    virtual coll_ptr mk_coll (const char *filename,
                              const char *context) const;

    // Interpret this expression in a context where we expect a
    // weighted collection. If successful, return it. Otherwise, print
    // one or more errors and return nullptr.
    virtual wpair_ptr mk_weighted (const char *filename) const;

    // Interpret this expression in a context where we expect an
    // arithmetic progression. If successful, return it. Otherwise,
    // print one or more errors and return nullptr.
    virtual aprog_ptr mk_aprog (const char *filename) const;

    place_t place;
};

typedef elt_list<expression> expression_list;

class unary_operator_node : public expression
{
public:
    unary_operator_node (const place_t &place, unop_t op, expression *arg)
        : expression (place), op (op), arg (arg)
    {}

    int_ptr mk_int (const char *filename, const char *context) const override;

protected:
    unop_t                      op;
    std::unique_ptr<expression> arg;
};

class binary_operator_node : public expression
{
public:
    binary_operator_node (const place_t &place,
                          expression    *l,
                          binop_t        op,
                          expression    *r)
        : expression (place), op (op), l (l), r (r)
    {}

    int_ptr mk_int (const char *filename, const char *context) const override;
    ivl_ptr mk_ivl (const char *filename) const override;
    coll_ptr mk_coll (const char *filename, const char *context) const override;
    wpair_ptr mk_weighted (const char *filename) const override;

protected:
    binop_t                     op;
    std::unique_ptr<expression> l, r;
};

// Conditional (ternary) operator
class cond_op_node : public expression
{
public:
    cond_op_node (const place_t &place,
                  expression *p, expression *t, expression *f)
        : expression (place), p (p), t (t), f (f)
    {}

    int_ptr mk_int (const char *filename, const char *context) const override;
    coll_ptr mk_coll (const char *filename, const char *context) const override;

protected:
    std::unique_ptr<expression> p, t, f;
};

class arith_prog_node : public expression
{
public:
    arith_prog_node (const place_t &place,
                     expression *a, expression *b, expression *step)
        : expression (place), a (a), b (b), step (step)
    {}

    int_ptr mk_int (const char *filename, const char *context) const override;
    coll_ptr mk_coll (const char *filename, const char *context) const override;
    aprog_ptr mk_aprog (const char *filename) const override;

protected:
    std::unique_ptr<expression> a, b, step;
};

// This is an integer literal. We actually store it as a string here, because
// the grammar doesn't glue on a unary minus properly. For example, "-3" would
// be the unary minus applied to 3. We want to correctly spot that
// -99999999999999 is overly large, but if we explode in the parsing pass, we'll
// complain about the absolute value rather than the negative number. Boo! We'll
// sort this out properly in the typing pass.
class lit_node : public expression
{
public:
    lit_node (const place_t &place, const char *value, unsigned base)
        : expression (place), base (base), str_value (value)
    {}

    int_ptr mk_int_uminus (const char    *filename,
                           const char    *context,
                           const place_t *unop_place,
                           bool          *is_unop_lit) const override;
    int_ptr mk_int (const char *filename, const char *context) const override;

protected:
    unsigned    base;
    std::string str_value;
};

class variable_node : public expression
{
public:
    variable_node (const place_t   &place,
                   symbol          *sym,
                   expression_list *indices)
        : expression (place), sym (sym), indices (indices)
    {}

    int_ptr mk_int (const char *filename, const char *context) const override;

protected:
    std::unique_ptr<symbol>          sym;
    std::unique_ptr<expression_list> indices;
};

// Functions

class fn_node : public expression
{
public:
    // Builtin function
    fn_node (const place_t   &place,
             const place_t   &name_place,
             int              id,
             expression_list *args)
        : expression (place),
          name_place (name_place), id (id), name (""), args (args)
    {}

    // Codec-specific function
    fn_node (const place_t   &place,
             const place_t   &name_place,
             const char      *name,
             expression_list *args)
        : expression (place),
          name_place (name_place), id (-1), name (name), args (args)
    {}

    int_ptr mk_int (const char *filename, const char *context) const override;
    coll_ptr mk_coll (const char *filename, const char *context) const override;

protected:
    // The function is either a builtin (in which case id is
    // non-negative and is one of the function_id's defined in
    // config.y) or it's a codec-specific function (in which case id
    // is -1 and the actual name is stored in name).
    place_t     name_place;
    int         id;
    std::string name;

    std::unique_ptr<expression_list> args;

    const char *get_function_name() const;
};

class statement {
  public:
    statement() { }
    virtual ~statement() { }

    typedef std::unique_ptr <typing::stmt_t> typed_ptr;

    // Interpret this statement as a statement inside a configuration.
    // On success, return the typed version. On failure, print one or
    // more errors and return nullptr.
    virtual typed_ptr mk_typed (const char *filename) const = 0;
};

typedef elt_list<statement> statement_list;

class assignment : public statement
{
public:
    assignment (symbol *sym, expression *expr)
        : sym (sym), expr (expr)
    {}

    typed_ptr mk_typed (const char *filename) const override;

private:
    std::unique_ptr<symbol>      sym;
    std::unique_ptr<expression>  expr;
};

class statement_block : public statement
{
public:
    statement_block(statement_list *statements) : statements (statements) {}

    typed_ptr mk_typed (const char *filename) const override;

private:
    std::unique_ptr<statement_list> statements;
};

class if_else_statement : public statement
{
public:
    if_else_statement (expression *test,
                       statement  *if_true,
                       statement  *if_false)
        : test (test), if_true (if_true), if_false (if_false)
    {}

    typed_ptr mk_typed (const char *filename) const override;

private:
    std::unique_ptr<expression> test;
    std::unique_ptr<statement>  if_true, if_false;
};

class insert_statement : public statement
{
public:
    insert_statement (symbol *sym)
        : sym (sym)
    {}

    typed_ptr mk_typed (const char *filename) const override;

private:
    std::unique_ptr<symbol> sym;
};

struct tl_statement
{
  public:
    virtual ~tl_statement() { }

    typedef typing::config_t config_t;

    // Try to type the top-level statement. If this is a configuration
    // and it gets typed successfully, add it to cfgs. If there is any
    // problem, print one or more errors and return false. Otherwise
    // return true.
    virtual bool add_cfg (const char            *filename,
                          std::vector<config_t> *cfgs) const = 0;
};

typedef elt_list<tl_statement> tl_list;

struct import_statement : public tl_statement
{
    import_statement (const place_t &place, const std::string &filename)
        : place (place), file_name (filename)
    {}

    bool add_cfg (const char *filename,
                  std::vector<config_t> *cfgs) const override;

    place_t     place;
    std::string file_name;
};

struct configuration_statement : public tl_statement
{
    configuration_statement (symbol *name, statement_list *statements)
        : name (name), statements (statements)
    {}

    bool add_cfg (const char *filename,
                  std::vector<config_t> *cfgs) const override;

    std::unique_ptr<symbol>         name;
    std::unique_ptr<statement_list> statements;
};

// Local Variables:
// mode: c++
// End:
