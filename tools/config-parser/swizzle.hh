/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <cassert>
#include <map>
#include <memory>
#include <random>
#include <vector>

#include "inserts.hh"

// The "swizzle" pass gets rid of all the statements from earlier
// passes and replaces them with a map from bitstream element to a
// tree of guarded expressions.

// Expressions come from the collections pass
namespace collections {
    struct int_expr_t;
}

struct symtab_t;

namespace swizzle {

    typedef std::mt19937 rnd_t;
    typedef std::vector<simple_var_desc_t> svds_t;

    // This is a "sliced" view on statements in the inserts pass. The
    // point is that it will only hold statements of interest for a
    // given element.
    struct slice_t;

    struct block_slice_t
    {
        typedef std::unique_ptr<block_slice_t> ptr_t;

        std::vector<std::unique_ptr<slice_t>> slices;

        // See documentation in slice_t
        bool reverse ();

        void display (std::ostream   &os,
                      unsigned        indent,
                      const symtab_t &symbol_table,
                      const char     *element) const;

        bool eval (rnd_t&                          rnd,
                   const std::vector<std::string> &paths,
                   int64_t                        *result);

        // Walk the slice searching for memory locations that are
        // referenced by expressions in the slice. Add each such
        // expression to dest.
        void fill_sensitivity_list (svds_t *dest) const;
    };

    struct slice_t
    {
        int path_index;

        slice_t (int path_index) : path_index (path_index) { }
        virtual ~slice_t() {}

        // Reverse and clean up the slice. This might simplify the
        // current slice and then returns true if this slice
        // unconditionally writes to the element. So the slice for
        //
        //     if (A) { x = 1; } else { x = 2; }
        //
        // should return true and the slice for
        //
        //     if (A) { x = 1; }
        //
        // should return false.
        //
        // A block slice that looks like { A; B; } will be reversed to
        // { B; A; } so that we can evaluate in order later. If
        // reversing A returns true, it will actually reverse to just
        // { A; }.
        //
        // Note that the checks for unconditional writes are just for
        // efficiency: there's no problem if they don't manage to spot
        // things. For example, we won't throw away the first
        // statement in this script:
        //
        //    x = 1;
        //    if (A) x = 2;
        //    if (! A) x = 3;
        //
        // We'll just end up with code that is essentially:
        //
        //    if (! A) { return 3; }
        //    if (A) { return 2; }
        //    return 1;
        virtual bool reverse () = 0;

        virtual void display (std::ostream   &os,
                              unsigned        indent,
                              const symtab_t &symbol_table,
                              const char     *element) const = 0;

        virtual bool eval (rnd_t&                          rnd,
                           const std::vector<std::string> &paths,
                           int64_t                        *result) = 0;

        // See block_slice_t for documentation
        virtual void fill_sensitivity_list (svds_t *dest) const = 0;
    };

    // This comes from an assignment, but should be viewed as a return
    // statement ("return EXPR") because if we execute it, we'll stop
    // immediately.
    struct ret_slice_t : public slice_t
    {
        collections::int_expr_t *underlying;

        ret_slice_t (int path_index, collections::int_expr_t &underlying)
            : slice_t (path_index), underlying (& underlying)
        {}

        // There's no cleanup possible for a return, and reversing it
        // has no effect. It does unconditionally return, so we should
        // return true.
        bool reverse () override { return true; }

        void display (std::ostream   &os,
                      unsigned        indent,
                      const symtab_t &symbol_table,
                      const char     *element) const override;

        bool eval (rnd_t&                          rnd,
                   const std::vector<std::string> &paths,
                   int64_t                        *result) override;

        void fill_sensitivity_list (svds_t *dest) const override;
    };

    struct if_slice_t : public slice_t
    {
        collections::int_expr_t *test;
        block_slice_t            if_true;
        block_slice_t            if_false;

        if_slice_t (int                        path_index,
                    collections::int_expr_t   &test,
                    block_slice_t            &&if_true,
                    block_slice_t            &&if_false)
            : slice_t (path_index),
              test (& test),
              if_true (std::move (if_true)),
              if_false (std::move (if_false))
        {}

        bool reverse () override;

        void display (std::ostream   &os,
                      unsigned        indent,
                      const symtab_t &symbol_table,
                      const char     *element) const override;

        bool eval (rnd_t&                          rnd,
                   const std::vector<std::string> &paths,
                   int64_t                        *result) override;

        void fill_sensitivity_list (svds_t *dest) const override;
    };

    // This is a thin wrapper around the map from element name to code
    // to evaluate it. However, it also takes ownership of the
    // config_t contents from the previous pass.
    struct map_t
    {
        typedef std::vector<block_slice_t::ptr_t> vec_t;

        std::unique_ptr<inserts::config_t> config;
        std::vector<std::string>           paths;
        vec_t                              vec;

        map_t (std::unique_ptr<inserts::config_t> &&config,
               std::vector<std::string>           &&paths,
               size_t                               num_elements)
            : config (std::move (config)), paths (paths), vec (num_elements)
        {}

        // Draw some representation of the map to os.
        void display (std::ostream &os, const symtab_t &symbols) const;

        int64_t eval (rnd_t &rnd, size_t element_index, symtab_t &st);
    };

    std::unique_ptr<map_t>
    pass (std::unique_ptr<inserts::config_t> &&cfg,
          std::vector<std::string>           &&paths,
          size_t                               num_elements);
}
