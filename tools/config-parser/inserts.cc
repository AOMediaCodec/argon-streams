/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "inserts.hh"

#include "error.hh"
#include "collections.hh"
#include "swizzle.hh"

#include <cassert>
#include <sstream>
#include <algorithm>

using namespace inserts;

void block_stmt_t::elements (std::vector<size_t> *dests) const
{
  assert (dests);
  for (const std::unique_ptr<stmt_t> &stmt : stmts) {
    stmt->elements (dests);
  }
}

void
block_stmt_t::slice_into (size_t element, swizzle::block_slice_t *dest) const
{
  // Flatten blocks: If I had something like { A; { B; C; } }, this
  // will mean we end up with { a; b; c; } where a, b, c are the
  // sliced versions of A, B, C.
  assert (dest);
  for (const std::unique_ptr<stmt_t> &stmt : stmts) {
    stmt->slice_into (element, dest);
  }
}

// We define the constructor and destructor out-of-line because it
// means the header doesn't need to include collections.hh (otherwise
// the unique_ptr constructor would need to know how to reset itself).
if_stmt_t::if_stmt_t (int path_index, expr_t *test, stmt_t *if_true, stmt_t *if_false)
    : stmt_t (path_index), test (test), if_true (if_true), if_false (if_false)
{}

if_stmt_t::~ if_stmt_t () {}

void if_stmt_t::elements (std::vector<size_t> *dests) const
{
  assert (dests);
  if_true->elements (dests);
  if (if_false)
    if_false->elements (dests);
}

void
if_stmt_t::slice_into (size_t element, swizzle::block_slice_t *dest) const
{
  assert (dest);

  // Generate a block slice for each leg of the if statement and slice
  // into each. If both end up empty, we can skip the statement
  // completely.
  swizzle::block_slice_t slice_t, slice_f;

  if_true->slice_into (element, & slice_t);
  if (if_false)
    if_false->slice_into (element, & slice_f);

  if (slice_t.slices.empty () && slice_f.slices.empty ())
    return;

  dest->slices.emplace_back (std::unique_ptr<swizzle::slice_t> (
                               new swizzle::if_slice_t (path_index,
                                                        * test,
                                                        std::move (slice_t),
                                                        std::move (slice_f))));
}

// We define the constructor and destructor out-of-line because it
// means the header doesn't need to include collections.hh (otherwise
// the unique_ptr constructor would need to know how to reset itself).
assign_stmt_t::assign_stmt_t (int path_index, size_t dest, expr_t *expr)
    : stmt_t (path_index), dest (dest), expr (expr)
{}

assign_stmt_t::~ assign_stmt_t () {}

void assign_stmt_t::elements (std::vector<size_t> *dests) const
{
  assert (dests);
  // Yes, this is a linear search. But I reckon the list will be short
  // enough that this is quicker than something cleverer.
  auto it = std::find (std::begin (* dests), std::end (* dests), dest);
  if (it == std::end (* dests)) {
    dests->push_back (dest);
  }
}

void
assign_stmt_t::slice_into (size_t element, swizzle::block_slice_t *bs) const
{
  assert (bs);

  if (dest == element) {
    bs->slices.emplace_back (
      std::unique_ptr<swizzle::slice_t> (
        new swizzle::ret_slice_t (path_index, * expr)));
  }
}

void config_t::elements (std::vector<size_t> *dest) const
{
  assert (dest);
  for (const std::unique_ptr<stmt_t> &stmt : stmts) {
    stmt->elements (dest);
  }
}

std::unique_ptr<swizzle::block_slice_t>
config_t::slice_config (size_t element) const
{
  std::unique_ptr<swizzle::block_slice_t> bs (new swizzle::block_slice_t ());
  for (const std::unique_ptr<stmt_t> &stmt : stmts) {
    stmt->slice_into (element, bs.get ());
  }
  return bs;
}

std::unique_ptr<config_t>
inserts::pass (std::vector<collections::file_t> &&src,
               const std::vector<std::string>    &paths,
               const char                        *selected_config)
{
  assert(src.size() && src.size() == paths.size()
         && selected_config);

  collections::config_map_t config_map;

  int num_files = src.size();
  for (int i = 0; i < num_files; i++) {
    for (collections::config_t &config : src[i]) {
      symbol sym = config.name;

      auto elem = std::make_pair(sym.name,
                      std::make_pair(std::move(config), i));
      auto ret = config_map.insert(std::move(elem));

      if (!ret.second) {
        // We're trying to add a configuration that's already in the map.
        // Display a helpful error showing the conflict.

        std::ostringstream oss;
        oss << "The configuration `" << sym.name
            << "' has already been defined.";

        print_error(paths[i].c_str(), sym.place, oss.str().c_str());

        print_error(paths[ret.first->second.second].c_str(),
                    ret.first->second.first.name.place,
                    "Previous definition is here");

        throw parse_error();
      }
    }
  }

  src.clear();

  auto it = config_map.find(std::string(selected_config));
  if (it == config_map.end()) {
    // Couldn't find the configuration.

    std::ostringstream oss;
    oss << "Unable to find the selected configuration `"
        << selected_config << "'.";

    print_error (paths [0].c_str (), oss.str ().c_str ());

    throw parse_error();
  }

  collections::config_t& config = it->second.first;

  bool error = false;
  std::unique_ptr<inserts::config_t> ret (
          config.clone_replace_insert (config_map,
                                       paths,
                                       it->second.second,
                                      &error));

  if (error) throw parse_error();

  return ret;
}
