/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <cassert>
#include <cstdint>
#include <iosfwd>
#include <map>
#include <memory>
#include <random>
#include <vector>

#include "ast-ops.hh"
#include "place.hh"
#include "symbol.hh"

// Forward declarations (from mem-ref.hh)
namespace mem_ref {
    struct config_t;
    typedef std::vector<config_t> file_t;
    typedef std::vector<file_t>   files_t;
}

// Forward declarations (from inserts.hh)
namespace inserts {
    struct stmt_t;
    struct block_stmt_t;
    struct if_stmt_t;
    struct assign_stmt_t;
    struct config_t;
}

// These collections are the ones that can be used at run-time. Note
// that collections like seq() and all() can only be constructed with
// actual numbers rather than arbitrary expressions, as seen in
// earlier passes.

namespace collections {

    struct config_t; // Forward declaration

    // < name, < ptr, path index > >
    typedef std::map<std::string, std::pair<config_t, int>> config_map_t;

    // < name, path index, position >
    typedef std::vector<std::tuple<std::string, int, place_t>> insert_stack_t;

    typedef std::mt19937 rnd_t;

    typedef std::vector<simple_var_desc_t> svds_t;

    // The base class for expressions representing integers
    struct int_expr_t
    {
        typedef std::unique_ptr<int_expr_t> ptr_t;
        typedef std::vector<ptr_t>          ptrs_t;

        int_expr_t (place_t place) : place (place) {}
        virtual ~ int_expr_t () {}

        place_t place;

        // Write a representation of the expression to os.
        void display (std::ostream   &os,
                      const symtab_t &symbol_table,
                      const char     *element,
                      unsigned        parent_precedence) const;

        virtual ptr_t clone() const = 0;
        virtual int64_t eval (rnd_t& rnd, const std::string& filename) = 0;

        // Walk the expression searching for memory locations that are
        // referenced. Add each such expression to dest.
        virtual void fill_sensitivity_list (svds_t *dest) const = 0;

    protected:
        virtual unsigned get_precedence () const = 0;
        virtual void display_ (std::ostream   &os,
                               const symtab_t &symbol_table,
                               const char     *element) const = 0;
    };

    // The base class for expressions representing collections.
    struct coll_expr_t
    {
        typedef std::unique_ptr<coll_expr_t> ptr_t;
        typedef std::vector<ptr_t>           ptrs_t;

        coll_expr_t (place_t place) : place (place) {}
        virtual ~ coll_expr_t () {}

        place_t place;

        // Write a representation of the expression to os.
        void display (std::ostream   &os,
                      const symtab_t &symbol_table,
                      const char     *element,
                      unsigned        parent_precedence) const;

        virtual ptr_t clone() const = 0;
        virtual int64_t eval (rnd_t& rnd, const std::string& filename) = 0;

        virtual void fill_sensitivity_list (svds_t *dest) const = 0;

    protected:
        virtual unsigned get_precedence () const = 0;
        virtual void display_ (std::ostream   &os,
                               const symtab_t &symbol_table,
                               const char     *element) const = 0;
    };

    // A literal integer expression.
    struct int_literal_t : public int_expr_t
    {
        int64_t value;

        int_literal_t (place_t place, int64_t value)
            : int_expr_t (place), value (value)
        {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 0; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    // A variable, possibly with array indices
    struct variable_t : public int_expr_t
    {
        const var_desc_t desc;
        ptrs_t           indices;

        variable_t (place_t place, const var_desc_t& desc)
            : int_expr_t (place), desc (desc)
        {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 0; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    struct funcall_0_t : public int_expr_t
    {
        symtab_t::fun0_t *fun;

        funcall_0_t (place_t place, symtab_t::fun0_t *fun)

            : int_expr_t (place), fun (fun)
        {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 2; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    struct funcall_1_t : public int_expr_t
    {
        symtab_t::fun1_t *fun;
        ptr_t             arg0;

        funcall_1_t (place_t place, symtab_t::fun1_t *fun, int_expr_t *arg0)
            : int_expr_t (place), fun (fun), arg0 (arg0)
        {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 2; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    struct funcall_2_t : public int_expr_t
    {
        symtab_t::fun2_t *fun;
        ptr_t             arg0, arg1;

        funcall_2_t (place_t place, symtab_t::fun2_t *fun,
                     int_expr_t *arg0, int_expr_t *arg1)
            : int_expr_t (place), fun (fun),
              arg0 (arg0), arg1 (arg1)
        {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 2; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    struct funcall_3_t : public int_expr_t
    {
        symtab_t::fun3_t *fun;
        ptr_t             arg0, arg1, arg2;

        funcall_3_t (place_t place, symtab_t::fun3_t *fun,
                     int_expr_t *arg0, int_expr_t *arg1, int_expr_t *arg2)
            : int_expr_t (place), fun (fun),
              arg0 (arg0), arg1 (arg1), arg2 (arg2)
        {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 2; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    struct choose_t : public int_expr_t
    {
        coll_expr_t::ptr_t arg;

        choose_t (place_t place, coll_expr_t *arg)
            : int_expr_t (place), arg (arg)
        {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 2; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    struct unop_expr_t : public int_expr_t
    {
        unop_t                      op;
        std::unique_ptr<int_expr_t> arg;

        unop_expr_t (place_t place, unop_t op, int_expr_t *arg)
            : int_expr_t (place), op (op), arg (arg)
        {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override;
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    struct binop_expr_t : public int_expr_t
    {
        int_binop_t                 op;
        std::unique_ptr<int_expr_t> l, r;

        binop_expr_t (place_t place,
                      int_expr_t *l, int_binop_t op, int_expr_t *r)
            : int_expr_t (place), op (op), l (l), r (r)
        {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override;
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    // The conditional operator (can be used for integer expressions
    // or collection expressions)
    template <typename T>
    struct cond_expr_t : public T
    {
        std::unique_ptr<int_expr_t> test;
        std::unique_ptr<T>          if_true;
        std::unique_ptr<T>          if_false;

        cond_expr_t (place_t place, int_expr_t *test, T *if_true, T *if_false)
            : T (place), test (test), if_true (if_true), if_false (if_false)
        {}

        std::unique_ptr<T> clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 17; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    struct ivl_t
    {
        place_t                     place;
        std::unique_ptr<int_expr_t> lo;
        std::unique_ptr<int_expr_t> hi; // May be null

        ivl_t (place_t place, int_expr_t *lo, int_expr_t *hi)
            : place (place), lo (lo), hi (hi)
        {}

        void display (std::ostream   &os,
                      const symtab_t &symbol_table,
                      const char     *element) const;

        ivl_t clone () const;

        void fill_sensitivity_list (svds_t *dest) const;
    };

    // This type represents a set of integers, all of whose values are
    // known.
    struct set_t
    {
        set_t ()
            : num_elements ()
        {}

        // Add the interval lo..hi to the set. The caller is
        // responsible for checking that lo <= hi.
        void add_ivl (int64_t lo, int64_t hi);

        // Remove a point from the set.
        void remove_point (int64_t val);

        bool empty () const { return intervals.empty (); }
        void display (std::ostream &os) const;

        int64_t choose_uniformly (rnd_t& rnd) const;

    private:
        // The underlying representation stores an interval lo..hi as
        // the mapping lo |-> hi.
        typedef std::map<int64_t, int64_t> map_t;
        typedef map_t::iterator            iter_t;
        typedef map_t::const_iterator      const_iter_t;

        // Add the length of the interval [lo, hi] to num_elements.
        // Assumes that lo <= hi and that [lo, hi] is disjoint from
        // the intervals counted so far.
        void add_ivl_length (int64_t lo, int64_t hi);

        map_t    intervals;
        uint64_t num_elements;
    };

    struct arith_prog_t
    {
        int64_t first;
        int64_t last;
        int64_t step;

        arith_prog_t (int64_t first, int64_t last, int64_t step)
            : first (first), last (last), step (step)
        {
            // These invariants should be checked by the function that
            // constructs the object.
            assert (step != 0);
            if (first < last) assert (step > 0);
            if (last < first) assert (step < 0);
        }

        void display (std::ostream &os) const;

        // Return the first value that should appear when sampling
        // from this arithmetic progression.
        int64_t initial_val () const;

        // Try to step through this arithmetic progression. *val is
        // the current value and will be updated to *val + step.
        // Returns true if the result is still an element of the
        // progression and false otherwise.
        bool step_value (int64_t *val) const;
    };

    // This type represents a uniform set as a list of intervals. In
    // general, some of the endpoints of the intervals might be
    // unknown when the uniform_t is constructed (so they will need
    // evaluating as part of choosing from the uniform_t).
    struct dyn_uniform_t : public coll_expr_t
    {
        std::vector<ivl_t> intervals;

        dyn_uniform_t (place_t place) : coll_expr_t (place) {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 2; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    // This type represents a uniform set as an actual set of integers
    // (which is more efficient than the list of intervals
    // representation if the endpoints are all known)
    struct const_uniform_t : public coll_expr_t
    {
        set_t set;

        const_uniform_t (place_t place) : coll_expr_t (place) {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 2; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    // This type represents an all() statement, which has a backing
    // set_t from which it samples without replacement.
    struct all_t : public coll_expr_t
    {
        all_t (place_t place, const set_t &set)
            : coll_expr_t (place), set (set), fresh_set (set)
        {
            assert (! set.empty ());
        }

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    private:
        set_t set;
        set_t fresh_set;

        unsigned get_precedence () const override { return 2; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    // This type represents a sequence, which is a list of arithmetic
    // progressions (with constant entries).
    struct seq_t : public coll_expr_t
    {
        seq_t (place_t place)
            : coll_expr_t (place),
              next_idx (std::numeric_limits<size_t>::max ()),
              next_val (0)
        {}

        void add_progression (const arith_prog_t &prog);

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    private:
        unsigned get_precedence () const override { return 2; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;

        std::vector <arith_prog_t> entries;
        size_t                     next_idx;
        int64_t                    next_val;
    };

    // This type represents a weighted collection with a list of
    // underlying collections, each with a weight (which might not be
    // known until runtime).
    struct weighted_t : public coll_expr_t
    {
        typedef std::unique_ptr<coll_expr_t> coll_ptr;
        typedef std::unique_ptr<int_expr_t>  int_ptr; // May be nullptr

        typedef std::pair <coll_ptr, int_ptr>  pair_t;

        std::vector <pair_t> entries;

        weighted_t (place_t place) : coll_expr_t (place) {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 2; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    // This type is the degenerate collection that is actually just an
    // integer expression
    struct int_coll_expr_t : public coll_expr_t
    {
        int_expr_t::ptr_t expr;

        int_coll_expr_t (place_t place, int_expr_t::ptr_t &&expr)
            : coll_expr_t (place), expr (std::move (expr))
        {}

        ptr_t clone() const override;
        int64_t eval (rnd_t& rnd, const std::string& filename) override;

        void fill_sensitivity_list (svds_t *dest) const override;

    protected:
        unsigned get_precedence () const override { return 0; }
        void display_ (std::ostream   &os,
                       const symtab_t &symbol_table,
                       const char     *element) const override;
    };

    // The base statement type.
    struct stmt_t
    {
        typedef std::unique_ptr<inserts::stmt_t> stmt_ptr_t;

        virtual ~ stmt_t () {}

        virtual stmt_ptr_t clone_replace_insert (
                config_map_t&                   config_map,
                insert_stack_t&                 insert_stack,
                const std::vector<std::string> &paths,
                int                             current_path_index,
                bool                           *error) const = 0;
    };

    struct block_stmt_t : public stmt_t
    {
        std::vector<std::unique_ptr <stmt_t>> stmts;

        stmt_ptr_t clone_replace_insert (
                config_map_t&                   config_map,
                insert_stack_t&                 insert_stack,
                const std::vector<std::string> &paths,
                int                             current_path_index,
                bool                           *error) const override;
    };

    struct if_stmt_t : public stmt_t
    {
        std::unique_ptr<int_expr_t> test;
        std::unique_ptr<stmt_t>     if_true;
        std::unique_ptr<stmt_t>     if_false; // may be null

        if_stmt_t (int_expr_t *test,
                   stmt_t     *if_true,
                   stmt_t     *if_false)
            : test (test), if_true (if_true), if_false (if_false)
        {}

        stmt_ptr_t clone_replace_insert (
                config_map_t&                   config_map,
                insert_stack_t&                 insert_stack,
                const std::vector<std::string> &paths,
                int                             current_path_index,
                bool                           *error) const override;
    };

    struct assign_stmt_t : public stmt_t
    {
        size_t                      dest;
        std::unique_ptr<int_expr_t> expr;

        assign_stmt_t (size_t dest, int_expr_t *expr)
            : dest (dest), expr (expr)
        {}

        stmt_ptr_t clone_replace_insert (
                config_map_t&                   config_map,
                insert_stack_t&                 insert_stack,
                const std::vector<std::string> &paths,
                int                             current_path_index,
                bool                           *error) const override;
    };

    struct insert_stmt_t : public stmt_t
    {
        symbol sym;

        insert_stmt_t (symbol sym)
            : sym (sym)
        {}

        stmt_ptr_t clone_replace_insert (
                config_map_t&                   config_map,
                insert_stack_t&                 insert_stack,
                const std::vector<std::string> &paths,
                int                             current_path_index,
                bool                           *error) const override;
    };

    struct config_t
    {
        symbol                               name;
        std::vector<std::unique_ptr<stmt_t>> stmts;

        config_t (symbol name)
            : name (name)
        {}

        std::unique_ptr<inserts::config_t> clone_replace_insert (
                config_map_t&                   config_map,
                const std::vector<std::string> &paths,
                int                             current_path_index,
                bool                           *error) const;
    };

    typedef std::vector<config_t> file_t;
    typedef std::vector<file_t>   files_t;

    // The pass that builds collections
    files_t pass (mem_ref::files_t               &&src,
                  const std::vector<std::string>  &paths);

    // Display the statements of a config file
    void display_statements (std::ostream   &os,
                             const symtab_t &symbol_table,
                             const file_t   &configs);
}
