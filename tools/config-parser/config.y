/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

%code requires {
#include "ast.hh"

#define YYLTYPE place_t
}

%initial-action
{
  yylloc = { 1, 1, 1, 1 };
};

%code top {
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include "config.lex.h"
#include "parse.hh"

// Error callback defined in parser.cc
extern void yyerror (const char *msg);

// This is defined in parse.cc and is used to pass the top-level list
// of statements to the caller.
extern tl_list *parser_tl_list;

#define YYLLOC_DEFAULT(Cur, Rhs, N)                                     \
    do {                                                                \
        if (N) {                                                        \
            (Cur).line0 = YYRHSLOC (Rhs, 1).line0;                      \
            (Cur).col0  = YYRHSLOC (Rhs, 1).col0;                       \
            (Cur).line1 = YYRHSLOC (Rhs, N).line1;                      \
            (Cur).col1  = YYRHSLOC (Rhs, N).col1;                       \
        } else {                                                        \
            (Cur).line0 = (Cur).line1 = YYRHSLOC (Rhs, 0).line1;        \
            (Cur).col0 = (Cur).col1 = YYRHSLOC (Rhs, 0).col1;           \
        }                                                               \
    } while (0)

// Destructively reverses an element list, returning the reversed
// version.
template <typename T>
static elt_list<T> *reverse_elt_list (elt_list<T> *cur)
{
    elt_list<T> *ret = nullptr;

    while (cur) {
        elt_list<T> *next = cur->next.release ();
        cur->next.reset (ret);

        ret = cur;
        cur = next;
    }

    return ret;
}

}

// This switches on a "lookahead correction" feature, which is
// supposed to result in more sane error messages.
%define parse.lac full

// Fill out parser errors a bit
%define parse.error verbose

// Switch on location tracking
%locations

%union {
  tl_statement    *tl_stmt;
  tl_list         *tl_stmts;

  statement       *stmt;
  statement_list  *stmts;

  expression      *expr;
  expression_list *exprs;

  char             text [1024];
  int              function_id;
}

// Establish precedence and associativity

// 18
%left KEY_WEIGHT
%precedence NO_STEP
%precedence KEY_STEP
// 17
%nonassoc ':'
%nonassoc '?'
// 16
%left RANGE
// 15
%left LOG_OR
// 14
%left LOG_AND
// 13
%left '|'
// 12
%left '^'
// 11
%left '&'
// 10
%left REL_EQ REL_NEQ
// 9
%left '<' REL_LT_EQ '>' REL_GT_EQ
// 7
%left BIT_LSHIFT BIT_RSHIFT
// 6
%left '+' '-'
// 5
%left '*' '/' '%'
// 3
%right UMINUS UPLUS '!' '~'

%precedence IF_NO_ELSE
%precedence KEY_ELSE

// Literals
%token <text> BIN_LIT OCT_LIT HEX_LIT DEC_LIT STR_LIT
%token <text> SYMBOL

%token EOL
// Relational operators
%token REL_LT_EQ REL_GT_EQ REL_EQ REL_NEQ
// Logical operators
%token LOG_AND LOG_OR
// Bitwise operators
%token BIT_LSHIFT BIT_RSHIFT

%token KEY_IF KEY_ELSE KEY_IMPORT KEY_INSERT KEY_DEFAULT KEY_STEP KEY_WEIGHT
%token <function_id> FN_MIN FN_MAX FN_CLAMP FN_CHOOSE_BITS FN_CHOOSE_BIT FN_CHOOSE_BERNOULLI FN_CHOOSE
%token <function_id> COL_UNIFORM COL_ALL COL_SEQ COL_WEIGHTED
%token RANGE
%token UNKNOWN_CHARACTER INVALID_ESCAPED_CHARACTER_IN_STRING_LITERAL

%type <function_id> function_id

%type <expr>        expression
%type <expr>        conditional_expression arithmetic_expression
%type <expr>        relational_expression logical_expression
%type <expr>        bitwise_expression arith_prog_expression
%type <expr>        weighted_expression literal parentheses
%type <expr>        function_call element variable
%type <exprs>       element_list argument_list non_empty_argument_list

%type <stmt>        statement if_else_statement assignment statement_block
%type <stmt>        insert_statement
%type <stmts>       statement_list

%type <tl_stmt>     top_level_statement
%type <tl_stmt>     configuration_statement import_statement
%type <tl_stmts>    top_level top_level_statement_list

%destructor { delete $$; } <expr>
%destructor { delete $$; } <exprs>
%destructor { delete $$; } <stmt>
%destructor { delete $$; } statement_list
%destructor { delete $$; } top_level_statement_list

%%

top_level:
    top_level_statement_list                      { parser_tl_list = reverse_elt_list ($1); }
  ;

top_level_statement_list:
    top_level_statement                           { $$ = new tl_list(NULL, $1); }
  | top_level_statement_list top_level_statement  { $$ = new tl_list($1, $2); }
  ;

top_level_statement:
    configuration_statement
  | import_statement
  ;

configuration_statement:
    SYMBOL '{' statement_list '}'        { $$ = new configuration_statement (new symbol (@1, $1),
                                                                             reverse_elt_list ($3)); }
  | KEY_DEFAULT '{' statement_list '}'   { $$ = new configuration_statement(new symbol(@1, "default"),
                                                                            reverse_elt_list ($3)); }
  ;

statement_list:
    statement                            { $$ = new statement_list(NULL, $1); }
  | statement_list statement             { $$ = new statement_list($1, $2); }
  ;

statement:
    assignment ';'
  | statement_block
  | if_else_statement
  | insert_statement
    // Error recovery: If something has gone wrong, reset on the next semicolon.
  | error ';'                            { $$ = new statement_block(NULL); }
  ;

statement_block:
    '{' '}'                              { $$ = new statement_block(NULL); }
  | '{' statement_list '}'               { $$ = new statement_block(reverse_elt_list ($2)); }
  ;

assignment:
    SYMBOL '=' expression                { $$ = new assignment(new symbol(@1, $1), $3); }
  ;

if_else_statement:
    KEY_IF '(' expression ')' statement %prec IF_NO_ELSE    { $$ = new if_else_statement($3, $5, NULL); }
  | KEY_IF '(' expression ')' statement KEY_ELSE statement  { $$ = new if_else_statement($3, $5, $7); }
  ;

insert_statement:
    KEY_INSERT KEY_DEFAULT ';'           { $$ = new insert_statement (new symbol (@2, "default")); }
  | KEY_INSERT SYMBOL ';'                { $$ = new insert_statement (new symbol (@2, $2)); }
  ;

import_statement:
    KEY_IMPORT STR_LIT ';'               { $$ = new import_statement (@2, $2); }
  ;

expression:
    conditional_expression
  | arithmetic_expression
  | relational_expression
  | logical_expression
  | bitwise_expression
  | literal
  | variable
  | parentheses
  | function_call
  | arith_prog_expression
  | weighted_expression
  ;

conditional_expression:
    expression '?' expression ':' expression  { $$ = new cond_op_node(@$, $1, $3, $5); }
  ;

arithmetic_expression:
    expression '+' expression           { $$ = new binary_operator_node (@$, $1, BINOP_PLUS, $3); }
  | expression '-' expression           { $$ = new binary_operator_node (@$, $1, BINOP_MINUS, $3); }
  | expression '*' expression           { $$ = new binary_operator_node (@$, $1, BINOP_TIMES, $3); }
  | expression '/' expression           { $$ = new binary_operator_node (@$, $1, BINOP_DIVIDE, $3); }
  | expression '%' expression           { $$ = new binary_operator_node (@$, $1, BINOP_MOD, $3); }
  | '-' expression %prec UMINUS         { $$ = new unary_operator_node (@$, UNOP_MINUS, $2); }
  | '+' expression %prec UPLUS          { $$ = $2; }
  ;

relational_expression:
    expression REL_LT_EQ expression     { $$ = new binary_operator_node (@$, $1, BINOP_LE, $3); }
  | expression REL_GT_EQ expression     { $$ = new binary_operator_node (@$, $1, BINOP_GE, $3); }
  | expression REL_EQ expression        { $$ = new binary_operator_node (@$, $1, BINOP_EQ, $3); }
  | expression REL_NEQ expression       { $$ = new binary_operator_node (@$, $1, BINOP_NEQ, $3); }
  | expression '<' expression           { $$ = new binary_operator_node (@$, $1, BINOP_LT, $3); }
  | expression '>' expression           { $$ = new binary_operator_node (@$, $1, BINOP_GT, $3); }
  ;

logical_expression:
    expression LOG_AND expression       { $$ = new binary_operator_node (@$, $1, BINOP_LOG_AND, $3); }
  | expression LOG_OR expression        { $$ = new binary_operator_node (@$, $1, BINOP_LOG_OR, $3); }
  | '!' expression                      { $$ = new unary_operator_node (@$, UNOP_LOG_NOT, $2); }
  ;

bitwise_expression:
    '~' expression                      { $$ = new unary_operator_node (@$, UNOP_BIT_NOT, $2); }
  | expression '&' expression           { $$ = new binary_operator_node (@$, $1, BINOP_BIT_AND, $3); }
  | expression '^' expression           { $$ = new binary_operator_node (@$, $1, BINOP_BIT_XOR, $3); }
  | expression '|' expression           { $$ = new binary_operator_node (@$, $1, BINOP_BIT_OR, $3); }
  | expression BIT_LSHIFT expression    { $$ = new binary_operator_node (@$, $1, BINOP_LSHIFT, $3); }
  | expression BIT_RSHIFT expression    { $$ = new binary_operator_node (@$, $1, BINOP_RSHIFT, $3); }
  ;

literal:
    BIN_LIT                             { $$ = new lit_node(@$, $1, 2); }
  | OCT_LIT                             { $$ = new lit_node(@$, $1, 8); }
  | HEX_LIT                             { $$ = new lit_node(@$, $1, 16); }
  | DEC_LIT                             { $$ = new lit_node(@$, $1, 10); }
  ;

variable:
    SYMBOL                              { $$ = new variable_node (@$, new symbol (@1, $1), NULL); }
  | SYMBOL element_list                 { $$ = new variable_node (@$, new symbol (@1, $1),
                                                                  reverse_elt_list ($2)); }
  ;

parentheses:
    '(' expression ')'                  { $$ = $2; }
  ;

function_call:
    function_id '(' argument_list ')'   { $$ = new fn_node (@$, @1, $1, reverse_elt_list ($3)); }
  | SYMBOL '(' argument_list ')'        { $$ = new fn_node (@$, @1, $1, reverse_elt_list ($3)); }
  ;

function_id:
    FN_MIN
  | FN_MAX
  | FN_CLAMP
  | FN_CHOOSE_BITS
  | FN_CHOOSE_BIT
  | FN_CHOOSE_BERNOULLI
  | FN_CHOOSE
  | COL_UNIFORM
  | COL_ALL
  | COL_SEQ
  | COL_WEIGHTED
  ;

argument_list:
                                                    { $$ = NULL; }
  | non_empty_argument_list
  ;

non_empty_argument_list:
    expression                                      { $$ = new expression_list(NULL, $1); }
  | argument_list ',' expression                    { $$ = new expression_list($1, $3); }
  ;

arith_prog_expression:
    expression RANGE expression KEY_STEP expression { $$ = new arith_prog_node(@$, $1, $3, $5); }
  | expression RANGE expression %prec NO_STEP       { $$ = new binary_operator_node (@$, $1, BINOP_RANGE, $3); }
  ;

weighted_expression:
    expression KEY_WEIGHT expression                { $$ = new binary_operator_node (@$, $1, BINOP_WEIGHT, $3); }
  ;

element:
    '[' expression ']'                              { $$ = $2; }
  ;

element_list:
    element                                         { $$ = new expression_list(NULL, $1); }
  | element_list element                            { $$ = new expression_list($1, $2); }
  ;

%%

// Local Variables:
// mode: c++
// End:
