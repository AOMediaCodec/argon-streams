/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include "swizzle.hh"

struct symtab_t;

// Run through the compiler passes. On success, return the "compiled"
// code. On failure, print one or more messages to stderr and return
// nullptr.
std::unique_ptr<swizzle::map_t>
run_passes (const char               *filename,
            const char               *selected_config,
            const symtab_t           &symbol_table,
            bool                      debug_parser);
