/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include "ast-ops.hh"
#include "place.hh"
#include "symbol.hh"

#include <cstdint>
#include <memory>
#include <vector>

// Forward declarations (from typing.hh)
namespace typing {
    struct config_t;
    typedef std::vector<config_t> file_t;
    typedef std::vector<file_t>   files_t;
}

// Forward declarations (from collections.hh)
namespace collections {
    struct config_t;
    struct stmt_t;
    struct int_expr_t;
    struct coll_expr_t;
}

namespace mem_ref {
    // This is the base type for expressions that should be evaluated
    // to an integer.
    struct int_expr_t
    {
        place_t place;

        int_expr_t (place_t place) : place (place) {}
        virtual ~ int_expr_t () {}

        typedef std::unique_ptr<int_expr_t> ptr_t;
        typedef std::vector<ptr_t>          ptrs_t;

        // Try to interpret the object as a constant integer. On
        // success, write that constant to dest and return true. On
        // failure return false.
        virtual bool to_constant (int64_t *dest) const;

        // Build an object for the collections pass. On success,
        // returns the new object. On failure, prints out one or more
        // error messages and returns nullptr. Destructive (may modify
        // internals of this object).
        virtual std::unique_ptr<collections::int_expr_t>
        to_collections (const char *filename) = 0;

        // Partially evaluate the expression. Operates in place. If
        // the expression should be replaced by a new one, returns
        // that. Otherwise returns nullptr.
        virtual ptr_t partial_eval () = 0;
    };

    // This is the base type for expressions that should be evaluated
    // to a collection.
    struct coll_expr_t
    {
        place_t place;

        coll_expr_t (place_t place) : place (place) {}
        virtual ~ coll_expr_t () {}

        typedef std::unique_ptr<coll_expr_t> ptr_t;

        // Build an object for the collections pass. On success,
        // returns the new object. On failure, prints out one or more
        // error messages and returns nullptr. Destructive (may modify
        // internals of this object).
        virtual std::unique_ptr<collections::coll_expr_t>
        to_collections (const char *filename) = 0;

        // Partially evaluate the expression. Operates in place.
        virtual void partial_eval () = 0;
    };

    // A literal integer expression.
    struct int_literal_t : public int_expr_t
    {
        int64_t value;

        int_literal_t (place_t place, int64_t value)
            : int_expr_t (place), value (value)
        {}

        bool to_constant (int64_t *dest) const override;

        std::unique_ptr<collections::int_expr_t>
        to_collections (const char *filename) override;

        ptr_t partial_eval () override;
    };

    // A variable, possibly with array indices
    //
    // Variables are represented by a variable description structure
    // and a vector of array indices.
    struct variable_t : public int_expr_t
    {
        const var_desc_t desc;
        ptrs_t           indices;

        variable_t (place_t place, const var_desc_t& desc, ptrs_t &&indices)
            : int_expr_t (place), desc (desc), indices (std::move (indices))
        {}

        std::unique_ptr<collections::int_expr_t>
        to_collections (const char *filename) override;

        ptr_t partial_eval () override;
    };

    // Factory method for building variables
    int_expr_t::ptr_t
    mk_var (const char          *filename,
            place_t              place,
            const var_desc_t    &var_desc,
            int_expr_t::ptrs_t &&indices);

    struct funcall_0_t : public int_expr_t
    {
        symtab_t::fun0_t *fun;
        bool              pure;

        funcall_0_t (place_t place, symtab_t::fun0_t *fun, bool pure)

            : int_expr_t (place), fun (fun), pure (pure)
        {}

        std::unique_ptr<collections::int_expr_t>
        to_collections (const char *filename) override;

        ptr_t partial_eval () override;
    };

    struct funcall_1_t : public int_expr_t
    {
        symtab_t::fun1_t *fun;
        bool              pure;
        ptr_t             arg0;

        funcall_1_t (place_t place, symtab_t::fun1_t *fun,
                     bool pure, int_expr_t *arg0)
            : int_expr_t (place), fun (fun), pure (pure), arg0 (arg0)
        {}

        std::unique_ptr<collections::int_expr_t>
        to_collections (const char *filename) override;

        ptr_t partial_eval () override;
    };

    struct funcall_2_t : public int_expr_t
    {
        symtab_t::fun2_t *fun;
        bool              pure;
        ptr_t             arg0, arg1;

        funcall_2_t (place_t place, symtab_t::fun2_t *fun,
                     bool pure, int_expr_t *arg0, int_expr_t *arg1)
            : int_expr_t (place), fun (fun), pure (pure),
              arg0 (arg0), arg1 (arg1)
        {}

        std::unique_ptr<collections::int_expr_t>
        to_collections (const char *filename) override;

        ptr_t partial_eval () override;
    };

    struct funcall_3_t : public int_expr_t
    {
        symtab_t::fun3_t *fun;
        bool              pure;
        ptr_t             arg0, arg1, arg2;

        funcall_3_t (place_t place, symtab_t::fun3_t *fun, bool pure,
                     int_expr_t *arg0, int_expr_t *arg1, int_expr_t *arg2)
            : int_expr_t (place), fun (fun), pure (pure),
              arg0 (arg0), arg1 (arg1), arg2 (arg2)
        {}

        std::unique_ptr<collections::int_expr_t>
        to_collections (const char *filename) override;

        ptr_t partial_eval () override;
    };

    // The only function that takes a collection is choose().
    struct choose_t : public int_expr_t
    {
        coll_expr_t::ptr_t arg;

        choose_t (place_t place, coll_expr_t *arg)
            : int_expr_t (place), arg (arg)
        {}

        std::unique_ptr<collections::int_expr_t>
        to_collections (const char *filename) override;

        ptr_t partial_eval () override;
    };

    // The only unary operations are on integer expressions
    struct unop_expr_t : public int_expr_t
    {
        unop_t op;
        ptr_t  arg;

        unop_expr_t (place_t place, unop_t op, int_expr_t *arg)
            : int_expr_t (place), op (op), arg (arg)
        {}

        std::unique_ptr<collections::int_expr_t>
        to_collections (const char *filename) override;

        ptr_t partial_eval () override;
    };

    // Integer expressions that are binary operations
    struct binop_expr_t : public int_expr_t
    {
        int_binop_t op;
        ptr_t       l, r;

        binop_expr_t (place_t place,
                      int_expr_t *l, int_binop_t op, int_expr_t *r)
            : int_expr_t (place), op (op), l (l), r (r)
        {}

        std::unique_ptr<collections::int_expr_t>
        to_collections (const char *filename) override;

        ptr_t partial_eval () override;
    };

    struct int_cond_expr_t : public int_expr_t
    {
        ptr_t test;
        ptr_t if_true, if_false;

        int_cond_expr_t (place_t place, int_expr_t *test,
                         int_expr_t *if_true, int_expr_t *if_false)
            : int_expr_t (place), test (test),
              if_true (if_true), if_false (if_false)
        {}

        std::unique_ptr<collections::int_expr_t>
        to_collections (const char *filename) override;

        ptr_t partial_eval () override;
    };

    struct coll_cond_expr_t : public coll_expr_t
    {
        int_expr_t::ptr_t test;
        ptr_t             if_true, if_false;

        coll_cond_expr_t (place_t place, int_expr_t *test,
                          coll_expr_t *if_true, coll_expr_t *if_false)
            : coll_expr_t (place), test (test),
              if_true (if_true), if_false (if_false)
        {}

        std::unique_ptr<collections::coll_expr_t>
        to_collections (const char *filename) override;

        void partial_eval () override;
    };

    struct ivl_t
    {
        place_t           place;
        int_expr_t::ptr_t lo;
        int_expr_t::ptr_t hi; // May be null

        ivl_t (place_t place, int_expr_t *lo, int_expr_t *hi)
            : place (place), lo (lo), hi (hi)
        {}

        void partial_eval ();
    };

    struct uniform_t : public coll_expr_t
    {
        bool               choose_all;
        std::vector<ivl_t> intervals;

        uniform_t (place_t place, bool choose_all)
            : coll_expr_t (place), choose_all (choose_all)
        {}

        std::unique_ptr<collections::coll_expr_t>
        to_collections (const char *filename) override;

        void partial_eval () override;
    };

    struct weighted_pair_t
    {
        coll_expr_t::ptr_t coll;
        int_expr_t::ptr_t  weight; // May be null

        weighted_pair_t (coll_expr_t *coll, int_expr_t *weight)
            : coll (coll), weight (weight)
        {}

        void partial_eval ();
    };

    struct weighted_t : public coll_expr_t
    {
        std::vector<weighted_pair_t> pairs;

        weighted_t (place_t place)
            : coll_expr_t (place)
        {}

        std::unique_ptr<collections::coll_expr_t>
        to_collections (const char *filename) override;

        void partial_eval () override;
    };

    // An arithmetical progression. This isn't an expression on its
    // own (it can only appear in a seq expression).
    struct arith_prog_t
    {
        place_t           place;
        ivl_t             ivl;
        int_expr_t::ptr_t step; // May be null

        arith_prog_t (place_t place, ivl_t &&ivl, int_expr_t *step)
            : place (place), ivl (std::move (ivl)), step (step)
        {}

        void partial_eval ();
    };

    // This is the expression that comes from a call to seq()
    struct seq_t : public coll_expr_t
    {
        std::vector<arith_prog_t> progs;

        seq_t (place_t place)
            : coll_expr_t (place)
        {}

        std::unique_ptr<collections::coll_expr_t>
        to_collections (const char *filename) override;

        void partial_eval () override;
    };

    // This is a degenerate collection that just contains an integer
    // expression
    struct int_coll_expr_t : public coll_expr_t
    {
        int_expr_t::ptr_t expr;

        int_coll_expr_t (place_t place, int_expr_t::ptr_t &&expr)
            : coll_expr_t (place), expr (std::move (expr))
        {}

        std::unique_ptr<collections::coll_expr_t>
        to_collections (const char *filename) override;

        void partial_eval () override;
    };

    // The base statement type. This is for statements that occur
    // within configuration blocks.
    struct stmt_t
    {
        virtual ~ stmt_t () {}

        typedef std::unique_ptr<stmt_t> ptr_t;
        typedef std::vector<ptr_t>      ptrs_t;

        // Build an object for the collections pass. On success,
        // returns the new object. On failure, prints out one or more
        // error messages and returns nullptr. Destructive (may modify
        // internals of this object).
        virtual std::unique_ptr<collections::stmt_t>
        to_collections (const char *filename) = 0;

        // Partially evaluate statements inside the expression.
        // Operates in place.
        virtual void partial_eval () = 0;
    };

    struct block_stmt_t : public stmt_t
    {
        ptrs_t stmts;

        std::unique_ptr<collections::stmt_t>
        to_collections (const char *filename) override;

        void partial_eval () override;
    };

    struct if_stmt_t : public stmt_t
    {
        int_expr_t::ptr_t test;
        ptr_t             if_true;
        ptr_t             if_false; // may be null

        if_stmt_t (int_expr_t *test,
                   stmt_t     *if_true,
                   stmt_t     *if_false = nullptr)
            : test (test), if_true (if_true), if_false (if_false)
        {}

        std::unique_ptr<collections::stmt_t>
        to_collections (const char *filename) override;

        void partial_eval () override;
    };

    struct assign_stmt_t : public stmt_t
    {
        size_t            dest;
        int_expr_t::ptr_t expr;

        assign_stmt_t (size_t dest, int_expr_t *expr)
            : dest (dest), expr (expr)
        {}

        std::unique_ptr<collections::stmt_t>
        to_collections (const char *filename) override;

        void partial_eval () override;
    };

    struct insert_stmt_t : public stmt_t
    {
        symbol sym;

        insert_stmt_t (symbol sym)
            : sym (sym)
        {}

        std::unique_ptr<collections::stmt_t>
        to_collections (const char *filename) override;

        void partial_eval () override;
    };

    struct config_t
    {
        symbol         name;
        stmt_t::ptrs_t stmts;

        config_t (symbol name)
            : name (name)
        {}

        // Build an object for the collections pass. Destructive.
        bool to_collections (const char *filename,
                             std::vector<collections::config_t> *dest);

        void partial_eval ();
    };

    typedef std::vector<config_t> file_t;
    typedef std::vector<file_t>   files_t;

    // The actual pass
    files_t pass (const symtab_t                  &syms,
                  typing::files_t                &&src,
                  const std::vector<std::string>  &paths);

    // Partially evaluate the file in place
    void partial_eval (files_t *files);
}
