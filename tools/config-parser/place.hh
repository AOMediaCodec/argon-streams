/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

struct place_t
{
    int line0, line1, col0, col1;
};

// Compute the "union" of two places. This is the range between the
// minimum start position and the maximum end position.
inline place_t operator+ (const place_t &a, const place_t &b)
{
    bool start_a = ((a.line0 < b.line0) ||
                    ((a.line0 == b.line0) && (a.col0 <= b.col0)));
    bool end_b = ((a.line1 < b.line1) ||
                  ((a.line1 == b.line1) && (a.col1 <= b.col1)));

    place_t ret;
    ret.line0 = start_a ? a.line0 : b.line0;
    ret.col0  = start_a ? a.col0  : b.col0;
    ret.line1 = end_b   ? b.line1 : a.line1;
    ret.col1  = end_b   ? b.col1  : a.col1;

    return ret;
}
