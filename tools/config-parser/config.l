/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

%x MULTILINE_COMMENT
%x STRING_LITERAL

%{
#include "config.tab.h"
#include "error.hh"
#include "parse.hh"

static void buffer_overflow_error(const char *type) {
  fprintf (stderr, "\n\nError: buffer overflow: %s is too long (maximum %lu bytes)."
                   "\n\n  %s = \"%s\"\n\n",
                   type,
                   (unsigned long) sizeof(yylval.text),
                   type,
                   yytext);
  throw parse_error();
}

// Grab the text that's currently being parsed

static void get_text(const char *type)
{
  size_t len = yyleng;
  if (len >= sizeof(yylval.text)) buffer_overflow_error(type);
  memcpy (yylval.text, yytext, yyleng + 1);
}

// Deal with string literals

int string_literal_pos;

static void add_char_to_string_literal(char c) {
  if (string_literal_pos >= sizeof(yylval.text)) buffer_overflow_error("string literal");
  yylval.text[string_literal_pos++] = c;
}

static void add_string_to_string_literal(char *s, size_t len) {
  if (string_literal_pos + len - 1 >= sizeof(yylval.text)) buffer_overflow_error("string literal");
  memcpy (yylval.text + string_literal_pos, s, len);
  string_literal_pos += len;
}

static void terminate_string_literal() {
  add_char_to_string_literal('\0');
}

// Step over something. Afterwards, the start of the range points just
// after the end of the previous range
#define STEP_OVER                               \
    do {                                        \
        yylloc.col0 = yylloc.col1;              \
        yylloc.line0 = yylloc.line1;            \
    } while (0)

// Update for one or more newlines. Reset the column to 1 and
// increment line counter.
#define STEP_LINES(n)                           \
    do {                                        \
        yylloc.col1 = 1;                        \
        yylloc.line1 += (n);                    \
    } while (0)

// Each time we match a string, move the end cursor to its end.
#define YY_USER_ACTION  yylloc.col1 += yyleng;

%}

%option yylineno
%option noyywrap
%option nounput

%%

%{
    // At each yylex call, step the current position
    STEP_OVER;
%}

  /* Keywords */

"if"                    { return KEY_IF; }
"else"                  { return KEY_ELSE; }
"import"                { return KEY_IMPORT; }
"insert"                { return KEY_INSERT; }
"default"               { return KEY_DEFAULT; }
"step"                  { return KEY_STEP; }
"weight"                { return KEY_WEIGHT; }

  /* Built-in function names */

"min"                   { yylval.function_id = FN_MIN;                return FN_MIN; }
"max"                   { yylval.function_id = FN_MAX;                return FN_MAX; }
"clamp"                 { yylval.function_id = FN_CLAMP;              return FN_CLAMP; }
"choose_bits"           { yylval.function_id = FN_CHOOSE_BITS;        return FN_CHOOSE_BITS; }
"choose_bit"            { yylval.function_id = FN_CHOOSE_BIT;         return FN_CHOOSE_BIT; }
"choose_bernoulli"      { yylval.function_id = FN_CHOOSE_BERNOULLI;   return FN_CHOOSE_BERNOULLI; }
"choose"                { yylval.function_id = FN_CHOOSE;             return FN_CHOOSE; }

  /* Types of collection */

"uniform"               { yylval.function_id = COL_UNIFORM;           return COL_UNIFORM; }
"all"                   { yylval.function_id = COL_ALL;               return COL_ALL; }
"seq"                   { yylval.function_id = COL_SEQ;               return COL_SEQ; }
"weighted"              { yylval.function_id = COL_WEIGHTED;          return COL_WEIGHTED; }

  /* Arithmetic operators */

"+"                     |
"-"                     |
"*"                     |
"/"                     |
"%"                     { return yytext[0]; }

  /* Relational operators */

"<="                    { return REL_LT_EQ; }
">="                    { return REL_GT_EQ; }
"=="                    { return REL_EQ; }
"!="                    { return REL_NEQ; }
"<"                     |
">"                     { return yytext[0]; }

  /* Logical operators */

"&&"                    { return LOG_AND; }
"||"                    { return LOG_OR; }
"!"                     { return yytext[0]; }

  /* Bitwise operators */

"~"                     |
"&"                     |
"^"                     |
"|"                     { return yytext[0]; }
"<<"                    { return BIT_LSHIFT; }
">>"                    { return BIT_RSHIFT; }


  /* Operators for intervals and arithmetic progressions */

".."                    { return RANGE; }

  /* Literals */

"0b"[01]+               { get_text("binary literal"); return BIN_LIT; }
"0"[0-7]*               { get_text("octal literal"); return OCT_LIT; }
"0x"[0-9a-f]+           { get_text("hexadecimal literal"); return HEX_LIT; }
[1-9][0-9]*             { get_text("decimal literal"); return DEC_LIT; }

  /* Quoted strings */

"\""                    { string_literal_pos = 0; BEGIN (STRING_LITERAL); }
<STRING_LITERAL>{
"\\\""                  { add_char_to_string_literal('\"'); } /* Replace \" with " */
"\\n"                   { add_char_to_string_literal('\n'); } /* Replace \n with newline */
"\\\\"                  { add_char_to_string_literal('\\'); } /* Replace \\ with \ */
"\\".                   { return INVALID_ESCAPED_CHARACTER_IN_STRING_LITERAL; } /* Do not allow any other escaped characters */
[^\"\\\n]+              { add_string_to_string_literal(yytext, yyleng); } /* Add valid characters to the output */
"\""                    { terminate_string_literal(); BEGIN (INITIAL); return STR_LIT; } /* Terminate when a non-escaped " is reached */
}

  /* Conditional (ternary) operator */

"?"                     |
":"                     { return yytext[0]; }

  /* Assignment */

"="                     { return yytext[0]; }

  /* Punctuation */

"("                     |
")"                     |
","                     |
"{"                     |
"}"                     |
"["                     |
"]"                     |
";"                     { return yytext[0]; }

  /* Symbols */

[a-zA-Z_][a-zA-Z_0-9]*  { get_text("symbol"); return SYMBOL; }

  /* Comments and whitespace NOT WORKING PROPERLY */

"//".*                  { }

"/*"                    { BEGIN (MULTILINE_COMMENT); }
<MULTILINE_COMMENT>{
"*/"                      { STEP_OVER; BEGIN (INITIAL); }
[^*\n]+                   { STEP_OVER; }
"*"                       { STEP_OVER; }
\n+                       { STEP_LINES(yyleng);
STEP_OVER; }
}

[ \t\r]+                { STEP_OVER; }
\n+                     { STEP_LINES(yyleng);
STEP_OVER;}

. { return UNKNOWN_CHARACTER; }

%%

/* Local Variables: */
/* mode: c */
/* End: */
