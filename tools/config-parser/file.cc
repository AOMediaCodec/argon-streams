/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "file.hh"

#include <cassert>
#include <cstring>
#include <sys/stat.h>

std::string resolve_include (const char *includer, const char *includee)
{
    assert (includer && includee);

    if (includee [0] == '/') {
        return includee;
    }

    // The dirname of includer is everything up to the last '/'. There
    // might not be one.
    const char *last_slash = strrchr (includer, '/');
    size_t dir_len = last_slash ? (1 + (last_slash - includer)) : 0;

    // dirname grabs characters from includer up to the last slash (or
    // is empty if there was no slash).
    std::string path (std::string (includer, dir_len) + includee);


    // Strip any "./" from the front of path.
    while (path.size () >= 2 && path [0] == '.' && path [1] == '/') {
        path = path.substr (2);
    }

    // Replace any "/./" with "/"
    for (;;) {
        size_t idx = path.find ("/./");
        if (idx == std::string::npos)
            break;

        path.replace (idx, 3, "/");
    }

    // Replace any "//" with "/"
    for (;;) {
        size_t idx = path.find ("//");
        if (idx == std::string::npos)
            break;

        path.replace (idx, 2, "/");
    }

    // At this point, path might still contain things like
    // foo/bar/../../baz. leading_dots is the number of characters
    // that shouldn't be stripped because they are of the form
    // "../../.." (followed by a slash).
    size_t leading_dots = 0;
    for (;;) {
        size_t idx = path.find ("/../", leading_dots);
        if (idx == std::string::npos)
            break;

        if (idx == leading_dots) {
            if (idx == 0) {
                // The path starts "/../foo/bar". Strip the silly top-level "/..".
                path = path.substr (3);
            } else {
                // We should add /.. to our initial segment.
                leading_dots += 3;
                continue;
            }
        }

        // If leading_dots is zero, it's possible that we've just got
        // to the first /../ of a path of the form "../../foo".
        if (leading_dots == 0 && idx == 2 &&
            path [0] == '.' && path [1] == '.') {
            leading_dots += 2;
            continue;
        }

        // Ahah. We have "blahblah/../". Search backwards for the
        // previous slash.
        size_t last_slash = path.find_last_of ('/', idx - 1);

        if (last_slash == std::string::npos) {
            // If there is no preceding slash, the path looks like
            // "x/../Y" and should be converted to "Y" by deleting
            // from the start to idx + 3 inclusive.
            path = path.erase (0, idx + 4);
        } else {
            // There was a preceding slash so the path looks like
            // "A/b/../D" and should be converted to "A/D". We know
            // that this won't hit the leading dots.
            assert (last_slash >= leading_dots);
            path = path.erase (last_slash + 1, 3 + idx - last_slash);
        }

    }

    return path;
}

bool get_file_data (file_unique_id *id, const char *path)
{
    assert (id && path);

    struct stat sb;
    if (stat (path, & sb) != 0)
        return false;

    id->first = (uint64_t) sb.st_dev;
    id->second = (uint64_t) sb.st_ino;
    return true;
}
