/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <memory>
#include <string>
#include "ast.hh"

// Entry point for config file parsing.
//
// This reads the file at path and any files included from there. On a
// syntax error, it writes a message to stderr and throws a
// parse_error. Otherwise, it returns the statements from the files it
// reads.
//
// If debug is true, yydebug is set to 1 so we print shift/reduce
// steps from the grammar.
std::pair <std::vector<std::string>,
           std::vector<std::unique_ptr<tl_list>>>
parse (const char *path, bool debug);
