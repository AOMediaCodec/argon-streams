/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

enum unop_t
{
    UNOP_MINUS,
    UNOP_LOG_NOT,
    UNOP_BIT_NOT,

    UNOP_NUM_ENTRIES
};

// NOTE: We assume that all binary operators less than or equal to
//       BINOP_LAST_INT_OP are "int -> int -> int". We also assume
//       that the names in int_binop_t match the initial segment of
//       binop_t up to BINOP_LAST_INT_OP. This makes the typing code a
//       bit easier to write.
enum binop_t
{
    BINOP_PLUS,
    BINOP_MINUS,
    BINOP_TIMES,
    BINOP_DIVIDE,
    BINOP_MOD,
    BINOP_LT,
    BINOP_LE,
    BINOP_GT,
    BINOP_GE,
    BINOP_EQ,
    BINOP_NEQ,
    BINOP_LOG_AND,
    BINOP_LOG_OR,
    BINOP_BIT_AND,
    BINOP_BIT_XOR,
    BINOP_BIT_OR,
    BINOP_LSHIFT,
    BINOP_RSHIFT,
    BINOP_RANGE,
    BINOP_WEIGHT,

    BINOP_NUM_ENTRIES,
    BINOP_LAST_INT_OP = BINOP_RSHIFT
};

// Binary operators between integers
//
// NOTE: See note above binop_t definition.
enum int_binop_t {
    IBINOP_PLUS,
    IBINOP_MINUS,
    IBINOP_TIMES,
    IBINOP_DIVIDE,
    IBINOP_MOD,
    IBINOP_LT,
    IBINOP_LE,
    IBINOP_GT,
    IBINOP_GE,
    IBINOP_EQ,
    IBINOP_NEQ,
    IBINOP_LOG_AND,
    IBINOP_LOG_OR,
    IBINOP_BIT_AND,
    IBINOP_BIT_XOR,
    IBINOP_BIT_OR,
    IBINOP_LSHIFT,
    IBINOP_RSHIFT,

    IBINOP_NUM_ENTRIES
};

static_assert ((int) IBINOP_NUM_ENTRIES == 1 + (int) BINOP_LAST_INT_OP,
               "int_binop_t should match initial segment of binop_t.");
