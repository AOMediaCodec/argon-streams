/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "mem-ref.hh"

#include "collections.hh"
#include "error.hh"
#include "typing.hh"

#include <cassert>
#include <sstream>

#define IMPLIES(p, q) (!(p) || (q))

using namespace mem_ref;

typedef std::unique_ptr<collections::int_expr_t>  coll_int_ptr;
typedef std::unique_ptr<collections::coll_expr_t> coll_coll_ptr;

template <typename S, typename T>
static bool
map_and_fill (const char                       *filename,
              std::vector<std::unique_ptr<T>>  *dest,
              std::vector<std::unique_ptr<S>> &&src)
{
    assert (filename && dest);

    bool good = true;

    for (const std::unique_ptr<S> &s : src) {
        std::unique_ptr<T> p (s->to_collections (filename));
        if (! p) {
            good = false;
        } else {
            dest->emplace_back (std::move (p));
        }
    }

    src.clear ();
    return good;
}

// Resolve the endpoints of an interval to actual integers. Write to
// the out pointers on success and return true. Print one or more
// errors (if not quiet) and return false on failure.
static bool
convert_ivl (const char  *filename,
             int64_t     *dest_lo,
             int64_t     *dest_hi,
             bool         quiet,
             const ivl_t &ivl)
{
    assert (filename && dest_lo && dest_hi);

    bool good = true;

    assert (ivl.lo);

    // Make sure that something gets written to *dest_lo and *dest_hi,
    // even if the calls to to_constant fail. This is a C++ legalese
    // thing really: we know that we don't really care what ends up in
    // these variables, but they should probably be assigned with
    // something.
    * dest_lo = 0;
    * dest_hi = 0;

    if (! ivl.lo->to_constant (dest_lo)) {
        good = false;
        if (! quiet) {
            print_error (filename, ivl.lo->place,
                         "Interval endpoint is not a constant.");
        }
    }

    if (! ivl.hi) {
        * dest_hi = * dest_lo;
    } else if (! ivl.hi->to_constant (dest_hi)) {
        good = false;
        if (! quiet) {
            print_error (filename, ivl.hi->place,
                         "Interval endpoint is not a constant.");
        }
    }

    return good;
}

// Resolve the terms of the arithmetical progression to actual
// integers. Add it to dest on success and return true. On
// failure, print out one or more errors and return false.
static bool
convert_aprog (const char         *filename,
               collections::seq_t *dest,
               const arith_prog_t &aprog)
{
    assert (filename && dest);

    int64_t lo, hi, step;
    bool good = true;

    good = convert_ivl (filename, & lo, & hi, false, aprog.ivl);
    if (! aprog.step) {
        step = (lo <= hi) ? 1 : -1;
    } else if (! aprog.step->to_constant (& step)) {
        print_error (filename, aprog.step->place,
                     "Arithmetic progression step is not a constant.");
        good = false;
    } else if ((lo < hi) && (step <= 0)) {
        std::ostringstream oss;
        oss << "Arithmetic progression over interval of "
            << lo << ".." << hi << " must have positive step, not "
            << step << ".";
        print_error (filename, aprog.place, oss.str ().c_str ());
        good = false;
    } else if ((hi < lo) && (step >= 0)) {
        std::ostringstream oss;
        oss << "Arithmetic progression over interval of "
            << lo << ".." << hi << " must have negative step, not "
            << step << ".";
        print_error (filename, aprog.place, oss.str ().c_str ());
        good = false;
    }

    if (good) {
        dest->add_progression (collections::arith_prog_t (lo, hi, step));
    }

    return good;
}

static void
partial_eval_at_pointer (int_expr_t::ptr_t *expr)
{
    assert (expr);
    if (* expr) {
        int_expr_t::ptr_t new_expr = (* expr)->partial_eval ();
        if (new_expr) (* expr) = std::move (new_expr);
    }
}

// The integer expressions aren't usually constants, so the base class
// implementation just returns false. Overridden by int_literal_t.
bool
int_expr_t::to_constant (int64_t *dest) const
{
    assert (dest);
    (void) dest;
    return false;
}

bool
int_literal_t::to_constant (int64_t *dest) const
{
    assert (dest);
    * dest = value;
    return true;
}

coll_int_ptr
int_literal_t::to_collections (const char *filename)
{
    assert (filename);
    (void) filename;
    return coll_int_ptr (new collections::int_literal_t (place, value));
}

int_expr_t::ptr_t
int_literal_t::partial_eval ()
{
    // Nothing to do here - we're already an integer!
    return nullptr;
}

coll_int_ptr
variable_t::to_collections (const char *filename)
{
    std::unique_ptr<collections::variable_t>
        v (new collections::variable_t (place, desc));

    if (! map_and_fill (filename, & v->indices, std::move (indices))) {
        v.reset ();
    }

    return std::move (v);
}

int_expr_t::ptr_t
variable_t::partial_eval ()
{
    // We can't replace the variable, but might be able to do
    // something useful with its indices.
    for (ptr_t &index : indices) {
        partial_eval_at_pointer (& index);
    }

    return nullptr;
}

std::unique_ptr<int_expr_t>
mem_ref::mk_var (const char          *filename,
                 place_t              place,
                 const var_desc_t    &var_desc,
                 int_expr_t::ptrs_t &&inds)
{
    // CHECK ARRAY INDICES ARE VALID //

    // Get number of supplied and required indices
    size_t num_supplied_indices = inds.size ();
    size_t num_required_indices = var_desc.dim;

    // Do I have too many indices?
    if (num_supplied_indices > num_required_indices) {
        std::ostringstream oss;
        oss << "Too many array indices have been specified: expecting "
            << num_required_indices << " indices.";
        print_error (filename, place, oss.str ().c_str ());
        return nullptr;
    }

    // Do I have too few indices?
    if (num_supplied_indices < num_required_indices) {
        std::ostringstream oss;
        oss << "Too few array indices have been specified: expecting "
            << num_required_indices << " indices.";
        print_error (filename, place, oss.str ().c_str ());
        return nullptr;
    }

    return std::unique_ptr<int_expr_t>(new variable_t (place, var_desc, std::move (inds)));
}

coll_int_ptr
funcall_0_t::to_collections (const char *filename)
{
    (void) filename;
    return coll_int_ptr (new collections::funcall_0_t (place, fun));
}

int_expr_t::ptr_t
funcall_0_t::partial_eval ()
{
    if (pure)
        return ptr_t (new int_literal_t (place, fun ()));

    return nullptr;
}

coll_int_ptr
funcall_1_t::to_collections (const char *filename)
{
    coll_int_ptr c0 (arg0->to_collections (filename));

    return (c0 ?
            coll_int_ptr (new collections::funcall_1_t (place, fun,
                                                        c0.release ())) :
            nullptr);
}

int_expr_t::ptr_t
funcall_1_t::partial_eval ()
{
    partial_eval_at_pointer (& arg0);

    if (! pure)
        return nullptr;

    int64_t iarg0;
    bool all_const = arg0->to_constant (& iarg0);

    return (all_const ?
            ptr_t (new int_literal_t (place, fun (iarg0))) :
            nullptr);
}

coll_int_ptr
funcall_2_t::to_collections (const char *filename)
{
    coll_int_ptr c0 (arg0->to_collections (filename));
    coll_int_ptr c1 (arg1->to_collections (filename));

    return ((c0 && c1) ?
            coll_int_ptr (new collections::funcall_2_t (place, fun,
                                                        c0.release (),
                                                        c1.release ())) :
            nullptr);
}

int_expr_t::ptr_t
funcall_2_t::partial_eval ()
{
    partial_eval_at_pointer (& arg0);
    partial_eval_at_pointer (& arg1);

    if (! pure)
        return nullptr;

    int64_t iarg0, iarg1;
    bool all_const = (arg0->to_constant (& iarg0) &&
                      arg1->to_constant (& iarg1));

    return (all_const ?
            ptr_t (new int_literal_t (place, fun (iarg0, iarg1))) :
            nullptr);
}

coll_int_ptr
funcall_3_t::to_collections (const char *filename)
{
    coll_int_ptr c0 (arg0->to_collections (filename));
    coll_int_ptr c1 (arg1->to_collections (filename));
    coll_int_ptr c2 (arg2->to_collections (filename));

    return ((c0 && c1 && c2) ?
            coll_int_ptr (new collections::funcall_3_t (place, fun,
                                                        c0.release (),
                                                        c1.release (),
                                                        c2.release ())) :
            nullptr);
}

int_expr_t::ptr_t
funcall_3_t::partial_eval ()
{
    partial_eval_at_pointer (& arg0);
    partial_eval_at_pointer (& arg1);
    partial_eval_at_pointer (& arg2);

    if (! pure)
        return nullptr;

    int64_t iarg0, iarg1, iarg2;
    bool all_const = (arg0->to_constant (& iarg0) &&
                      arg1->to_constant (& iarg1) &&
                      arg2->to_constant (& iarg2));

    return (all_const ?
            ptr_t (new int_literal_t (place, fun (iarg0, iarg1, iarg2))) :
            nullptr);
}

coll_int_ptr
choose_t::to_collections (const char *filename)
{
    coll_coll_ptr c0 (arg->to_collections (filename));

    return (c0 ?
            coll_int_ptr (new collections::choose_t (place, c0.release ())) :
            nullptr);
}

int_expr_t::ptr_t
choose_t::partial_eval ()
{
    arg->partial_eval ();
    return nullptr;
}

coll_int_ptr
unop_expr_t::to_collections (const char *filename)
{
    assert (filename && arg);
    coll_int_ptr c_arg (arg->to_collections (filename));

    return (c_arg ?
            coll_int_ptr (new collections::unop_expr_t (place, op,
                                                        c_arg.release ())) :
            nullptr);
}

int_expr_t::ptr_t
unop_expr_t::partial_eval ()
{
    partial_eval_at_pointer (& arg);
    const int_literal_t *lit =
        dynamic_cast <const int_literal_t *> (arg.get ());

    if (! lit)
        return nullptr;

    int64_t val = lit->value;

    switch (op) {
    case UNOP_MINUS:
        val = - val;
        break;
    case UNOP_LOG_NOT:
        val = ! val;
        break;
    case UNOP_BIT_NOT:
        val = ~ val;
        break;

        // LCOV_EXCL_START
    default:
        // Unreachable case
        assert (0);
        __builtin_unreachable ();
        // LCOV_EXCL_STOP
    }

    return ptr_t (new int_literal_t (place, val));
}

coll_int_ptr
binop_expr_t::to_collections (const char *filename)
{
    assert (filename && l && r);
    coll_int_ptr c_l (l->to_collections (filename));
    coll_int_ptr c_r (r->to_collections (filename));

    return ((c_l && c_r) ?
            coll_int_ptr (new collections::binop_expr_t (place,
                                                         c_l.release (),
                                                         op,
                                                         c_r.release ())) :
            nullptr);
}

int_expr_t::ptr_t
binop_expr_t::partial_eval ()
{
    partial_eval_at_pointer (& l);
    partial_eval_at_pointer (& r);

    const int_literal_t *lit0 =
        dynamic_cast <const int_literal_t *> (l.get ());
    const int_literal_t *lit1 =
        dynamic_cast <const int_literal_t *> (r.get ());

    if (! (lit0 && lit1))
        return nullptr;

    int64_t val0 = lit0->value;
    int64_t val1 = lit1->value;

    int64_t val;

    switch (op) {
    case IBINOP_PLUS:
        val = (val0 + val1);
        break;
    case IBINOP_MINUS:
        val = (val0 - val1);
        break;
    case IBINOP_TIMES:
        val = (val0 * val1);
        break;
    case IBINOP_DIVIDE:
        val = (val0 / val1);
        break;
    case IBINOP_MOD:
        val = (val0 % val1);
        break;
    case IBINOP_LT:
        val = (val0 < val1);
        break;
    case IBINOP_LE:
        val = (val0 <= val1);
        break;
    case IBINOP_GT:
        val = (val0 > val1);
        break;
    case IBINOP_GE:
        val = (val0 >= val1);
        break;
    case IBINOP_EQ:
        val = (val0 == val1);
        break;
    case IBINOP_NEQ:
        val = (val0 != val1);
        break;
    case IBINOP_LOG_AND:
        val = (val0 && val1);
        break;
    case IBINOP_LOG_OR:
        val = (val0 || val1);
        break;
    case IBINOP_BIT_AND:
        val = (val0 & val1);
        break;
    case IBINOP_BIT_XOR:
        val = (val0 ^ val1);
        break;
    case IBINOP_BIT_OR:
        val = (val0 | val1);
        break;
    case IBINOP_LSHIFT:
        val = (val0 << val1);
        break;
    case IBINOP_RSHIFT:
        val = (val0 >> val1);
        break;

        // LCOV_EXCL_START
    default:
        // This should not be possible.
        assert(0);
        __builtin_unreachable();
        // LCOV_EXCL_STOP
    }

    return ptr_t (new int_literal_t (place, val));
}

template <typename S, typename T>
static
std::unique_ptr<T>
cond_to_collections (const char    *filename,
                     const place_t &place,
                     int_expr_t    *test,
                     S             *if_true,
                     S             *if_false)
{
    assert (filename && test && if_true);
    coll_int_ptr c_test (test->to_collections (filename));
    std::unique_ptr<T> c_t (if_true->to_collections (filename));
    std::unique_ptr<T> c_f (if_false ?
                             if_false->to_collections (filename) :
                             nullptr);

    return ((c_test && c_t && (c_f || ! if_false)) ?
            (std::unique_ptr<T>
             (new collections::cond_expr_t<T> (place,
                                               c_test.release (),
                                               c_t.release (),
                                               c_f.release ()))) :
            nullptr);
}

coll_int_ptr
int_cond_expr_t::to_collections (const char *filename)
{
    return cond_to_collections<int_expr_t,
                               collections::int_expr_t> (filename,
                                                         place,
                                                         test.get (),
                                                         if_true.get (),
                                                         if_false.get ());
}

int_expr_t::ptr_t
int_cond_expr_t::partial_eval ()
{
    partial_eval_at_pointer (& test);
    partial_eval_at_pointer (& if_true);
    partial_eval_at_pointer (& if_false);
    return nullptr;
}

coll_coll_ptr
coll_cond_expr_t::to_collections (const char *filename)
{
    return cond_to_collections<coll_expr_t,
                               collections::coll_expr_t> (filename,
                                                          place,
                                                          test.get (),
                                                          if_true.get (),
                                                          if_false.get ());
}

void
coll_cond_expr_t::partial_eval ()
{
    assert (test && if_true);

    partial_eval_at_pointer (& test);
    if_true->partial_eval ();
    if (if_false)
        if_false->partial_eval ();
}

void
ivl_t::partial_eval ()
{
    partial_eval_at_pointer (& lo);
    partial_eval_at_pointer (& hi);
}

static bool
mk_set_t (const char                *filename,
          collections::set_t        *dest,
          bool                      *hard_error, // may be null
          bool                       quiet,
          const std::vector <ivl_t> &ivls)
{
    assert (filename && dest);
    assert (hard_error || ! quiet);

    bool good = true;

    for (const ivl_t &ivl : ivls) {
        int64_t lo, hi;
        good &= convert_ivl (filename, & lo, & hi, quiet, ivl);
        if (quiet && ! good)
            break;
        if (! good)
            continue;

        if (lo > hi) {
            if (hard_error)
                * hard_error = true;

            good = false;
            std::ostringstream oss;
            oss << "Interval endpoints are out of order ("
                << lo << " > " << hi << ").";
            print_error (filename, ivl.place, oss.str ().c_str ());
            continue;
        }

        dest->add_ivl (lo, hi);
    }

    return good;
}

static std::unique_ptr<collections::all_t>
mk_all_t (const char                *filename,
          const place_t             &place,
          const std::vector <ivl_t> &ivls)
{
    assert (filename);

    collections::set_t set;
    if (! mk_set_t (filename, & set, nullptr, false, ivls))
        return nullptr;

    if (set.empty ()) {
        print_error (filename, place,
                     "all () cannot choose from an empty set.");
        return nullptr;
    }

    return (std::unique_ptr<collections::all_t>
            (new collections::all_t (place, set)));
}

coll_coll_ptr
uniform_t::to_collections (const char *filename)
{
    assert (filename);

    if (choose_all) {
        return mk_all_t (filename, place, intervals);
    }

    // Try to build a const uniform if possible
    {
        std::unique_ptr<collections::const_uniform_t>
            cu (new collections::const_uniform_t (place));

        bool error = false;
        if (! mk_set_t (filename, & cu->set, & error, true, intervals))
            cu.reset ();

        if (error) return nullptr;

        if (cu) {
            if (cu->set.empty ()) {
                print_error (filename, place,
                             "uniform () cannot choose from an empty set.");
                return nullptr;
            }

            return std::move (cu);
        }
    }

    // If we couldn't build a const uniform, no worries. Build a
    // dynamic one instead.
    std::unique_ptr<collections::dyn_uniform_t>
        du (new collections::dyn_uniform_t (place));

    bool good = true;
    for (ivl_t &ivl : intervals) {
        assert (ivl.lo);

        coll_int_ptr lo (ivl.lo->to_collections (filename));
        coll_int_ptr hi (ivl.hi ?
                         ivl.hi->to_collections (filename) :
                         nullptr);

        good &= lo && (hi || ! ivl.hi);
        if (good)
            du->intervals.push_back (collections::ivl_t (ivl.place,
                                                         lo.release (),
                                                         hi.release ()));
    }

    if (! good) du.reset ();

    return std::move (du);
}

void
uniform_t::partial_eval ()
{
    for (ivl_t &ivl : intervals) {
        ivl.partial_eval ();
    }
}

coll_coll_ptr
weighted_t::to_collections (const char *filename)
{
    assert (filename);

    std::unique_ptr<collections::weighted_t>
        w (new collections::weighted_t (place));

    bool good = true;

    for (weighted_pair_t &pair : pairs) {
        assert (pair.coll);
        coll_coll_ptr cp (pair.coll->to_collections (filename));
        coll_int_ptr wp (pair.weight ?
                         pair.weight->to_collections (filename) :
                         nullptr);
        good &= (cp && (wp || ! pair.weight));

        if (good) {
            w->entries.push_back (std::make_pair (std::move (cp),
                                                  std::move (wp)));
        }
    }
    pairs.clear ();

    if (w->entries.empty ()) {
        print_error (filename,
                     place,
                     "weighted () cannot choose from an empty list.");
        good = false;
    }

    if (! good)
        w.reset ();

    return std::move (w);
}

void
weighted_t::partial_eval ()
{
    for (weighted_pair_t &pair : pairs) {
        pair.coll->partial_eval ();
        partial_eval_at_pointer (& pair.weight);
    }
}

void
arith_prog_t::partial_eval ()
{
    ivl.partial_eval ();
    partial_eval_at_pointer (& step);
}

coll_coll_ptr
seq_t::to_collections (const char *filename)
{
    assert (filename);

    if (progs.empty ()) {
        print_error (filename, place,
                     "seq () cannot choose from an empty list.");
        return nullptr;
    }

    std::unique_ptr<collections::seq_t> seq (new collections::seq_t (place));

    bool good = true;
    for (const arith_prog_t &aprog : progs) {
        good &= convert_aprog (filename, seq.get (), aprog);
    }

    if (! good)
        seq.reset ();

    return std::move (seq);
}

void
seq_t::partial_eval ()
{
    for (arith_prog_t &ap : progs)
        ap.partial_eval ();
}

coll_coll_ptr
int_coll_expr_t::to_collections (const char *filename)
{
    assert (filename);

    coll_int_ptr coll_expr (expr->to_collections (filename));
    if (! coll_expr)
        return nullptr;

    return (coll_coll_ptr
            (new collections::int_coll_expr_t (place, std::move (coll_expr))));
}

void
int_coll_expr_t::partial_eval ()
{
    partial_eval_at_pointer (& expr);
}

std::unique_ptr<collections::stmt_t>
block_stmt_t::to_collections (const char *filename)
{
    std::unique_ptr<collections::block_stmt_t>
        stmt (new collections::block_stmt_t);

    if (! map_and_fill (filename, & stmt->stmts, std::move (stmts))) {
        stmt.reset ();
    }

    return std::move (stmt);
}

void
block_stmt_t::partial_eval ()
{
    for (ptr_t &stmt : stmts)
        stmt->partial_eval ();
}

std::unique_ptr<collections::stmt_t>
if_stmt_t::to_collections (const char *filename)
{
    assert (filename && test && if_true);

    std::unique_ptr<collections::int_expr_t>
        c_test (test->to_collections (filename));

    std::unique_ptr<collections::stmt_t>
        c_if_t (if_true->to_collections (filename));

    std::unique_ptr<collections::stmt_t>
        c_if_f (if_false ?
                if_false->to_collections (filename) :
                nullptr);

    if (! (c_test && c_if_t && ! (if_false && ! c_if_f))) {
        return nullptr;
    }

    return (std::unique_ptr<collections::if_stmt_t>
            (new collections::if_stmt_t (c_test.release (),
                                         c_if_t.release (),
                                         c_if_f.release ())));
}

void
if_stmt_t::partial_eval ()
{
    partial_eval_at_pointer (& test);
    if_true->partial_eval ();
    if (if_false)
        if_false->partial_eval ();
}

std::unique_ptr<collections::stmt_t>
assign_stmt_t::to_collections (const char *filename)
{
    assert (filename);

    std::unique_ptr<collections::int_expr_t>
        c_expr (expr->to_collections (filename));

    return (c_expr ?
            (std::unique_ptr<collections::stmt_t>
             (new collections::assign_stmt_t (dest,
                                              c_expr.release ()))) :
            nullptr);
}

void
assign_stmt_t::partial_eval ()
{
    partial_eval_at_pointer (& expr);
}

std::unique_ptr<collections::stmt_t>
insert_stmt_t::to_collections (const char *filename)
{
    assert (filename);
    (void) filename;
    return (std::unique_ptr<collections::stmt_t>
            (new collections::insert_stmt_t (std::move (sym))));
}

void
insert_stmt_t::partial_eval ()
{
    return;
}

bool
config_t::to_collections (const char *filename,
                          std::vector<collections::config_t> *dest)
{
    assert (filename && dest);

    collections::config_t cfg (std::move (name));

    if (map_and_fill (filename, & cfg.stmts, std::move (stmts))) {
        dest->emplace_back (std::move (cfg));
        return true;
    }

    return false;
}

void
config_t::partial_eval ()
{
    for (stmt_t::ptr_t &stmt : stmts)
        stmt->partial_eval ();
}

static bool
ref_file (const symtab_t  &syms,
          files_t         *dest,
          typing::file_t &&input,
          const char      *filename)
{
    assert (dest && filename);

    file_t ret;
    bool good = true;
    for (typing::config_t &cfg : input) {
        good &= cfg.to_mem_ref (filename, syms, & ret);
    }

    if (good)
        dest->emplace_back (std::move (ret));

    return good;
}

files_t
mem_ref::pass (const symtab_t                  &syms,
               typing::files_t                &&src,
               const std::vector<std::string>  &paths)
{
    assert (src.size () == paths.size ());

    files_t dest;

    bool good = true;
    for (size_t i = 0; i < src.size (); ++ i) {
        good &= ref_file (syms, & dest,
                          std::move (src [i]),
                          paths [i].c_str ());
    }

    src.clear ();

    if (! good)
        throw parse_error ();

    return dest;
}

void
mem_ref::partial_eval (files_t *files)
{
    assert (files);
    for (file_t &file : * files) {
        for (config_t & cfg : file) {
            cfg.partial_eval ();
        }
    }
}
