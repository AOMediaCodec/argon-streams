/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include <random>

#include "error.hh"
#include "passes.hh"
#include "symbol.hh"
#include "glue/var-desc.hh"

#define ABS(x) ((x) < 0 ? -(x) : (x))
#define IMPLIES(p, q) (!(p) || (q))

static void usage () {
    std::cerr << "Usage: config-test [-d] <config file> [<script file>]\n";
    exit (1);
}

static std::mt19937 rnd;

static int8_t sym_i8s [88];
static uint8_t sym_u8s [12];
static int16_t sym_i16;
static uint16_t sym_u16;
static int32_t sym_i32;
static uint32_t sym_u32;
static int64_t sym_i64;
static uint64_t sym_u64;

static int64_t min64 (int64_t a, int64_t b)
{
    return a < b ? a : b;
}

static int64_t max64 (int64_t a, int64_t b)
{
    return a < b ? b : a;
}

static int64_t clamp64 (int64_t x, int64_t lo, int64_t hi)
{
    return max64 (lo, min64 (x, hi));
}

static int64_t ret_one ()
{
    return 1;
}

static int64_t abs64 (int64_t a)
{
    return a < 0 ? -a : a;
}

static int64_t choose_bit ()
{
    std::uniform_int_distribution<int64_t> distribution (0, 1);
    return distribution(rnd);
}

static int64_t choose_bits (int64_t nbits)
{
    if (nbits <= 0)
        return 0;

    int64_t max_value = std::numeric_limits<int64_t>::max ();
    if (nbits <= 63) {
        max_value >>= 63 - nbits;
    }

    // At this point, max_value is 2^k-1 where k=min(nbits, 63).
    std::uniform_int_distribution<int64_t> distribution (0, max_value);

    return distribution (rnd);
}

static int64_t choose_bernoulli_2 (int64_t p, int64_t n)
{
    n = max64 (n, 1);
    p = clamp64 (p, 0, n);

    std::uniform_int_distribution<int64_t> distribution (0, n - 1);
    return (distribution(rnd) < p);
}

static int64_t choose_bernoulli_1 (int64_t p)
{
    return choose_bernoulli_2 (p, 100);
}

// A codec-defined function? This returns one of its three inputs at random.
static int64_t cabbage_fun (int64_t a, int64_t b, int64_t c)
{
    int idx = rand () % 3;
    if (idx == 0) return a;
    if (idx == 1) return b;
    return c;
}

static const size_t g_array_lens[2] = { 5, 2 };
static const size_t h_array_lens[4] = { 4, 3, 3, 2 };
static const size_t w_array_lens[1] = { 9 };

static const std::map<std::string, var_desc_t>
symbol_pairs = {
    // a through f are elements 0-5
    { "a", var_desc_t ((uintptr_t) (sym_i8s +  0), -1, 0, nullptr) },
    { "b", var_desc_t ((uintptr_t) (sym_i8s +  1), -1, 0, nullptr) },
    { "c", var_desc_t ((uintptr_t) (sym_i8s +  2), -1, 0, nullptr) },
    { "d", var_desc_t ((uintptr_t) (sym_i8s +  3), -1, 0, nullptr) },
    { "e", var_desc_t ((uintptr_t) (sym_i8s +  4), -1, 0, nullptr) },
    { "f", var_desc_t ((uintptr_t) (sym_i8s +  5), -1, 0, nullptr) },
    // g is an array with 10 elements (6-15)
    { "g", var_desc_t ((uintptr_t) (sym_i8s +  6), -1, 2, g_array_lens) },
    // h is an array with 4*3*3*2 = 72 elements (16-87)
    { "h", var_desc_t ((uintptr_t) (sym_i8s + 16), -1, 4, h_array_lens) },

    // x, y, z and w are "local" and only available when writing to "x".
    { "x:x", var_desc_t ((uintptr_t) (sym_u8s + 0), 1, 0, nullptr) },
    { "x:y", var_desc_t ((uintptr_t) (sym_u8s + 1), 1, 0, nullptr) },
    { "x:z", var_desc_t ((uintptr_t) (sym_u8s + 2), 1, 0, nullptr) },
    { "x:w", var_desc_t ((uintptr_t) (sym_u8s + 3), 1, 1, w_array_lens) },

    { "foo", var_desc_t ((uintptr_t) (& sym_i16), -2, 0, nullptr) },
    { "bar", var_desc_t ((uintptr_t) (& sym_u16),  2, 0, nullptr) },

    { "cabbage",      var_desc_t ((uintptr_t) (& sym_i32), -4, 0, nullptr) },
    { "broccoli",     var_desc_t ((uintptr_t) (& sym_u32),  4, 0, nullptr) },

    { "lettuce",      var_desc_t ((uintptr_t) (& sym_i64), -8, 0, nullptr) },
    { "other_greens", var_desc_t ((uintptr_t) (& sym_u64),  8, 0, nullptr) },
};

static const std::map<std::string, int64_t>
constants = {
    { "ONE",   1 },
    { "TWO",   2 },
    { "THREE", 3 },
    { "FOUR",  4 }
};

static bool
symbol_lookup (var_desc_t *dest, const char *element, const char *symbol)
{
    assert (dest && symbol);

    // Try a global first.
    auto it = symbol_pairs.find (symbol);
    if (it != symbol_pairs.end ()) {
        * dest = it->second;
        return true;
    }

    // No luck? Try a local if we have an element
    if (element) {
        std::string full_name (element);
        full_name += ':';
        full_name += symbol;

        it = symbol_pairs.find (full_name);
        if (it != symbol_pairs.end ()) {
            * dest = it->second;
            return true;
        }
    }

    // Still no luck? Oh well...
    return false;
}

static const char *
symbol_invert (const char *element,
               uintptr_t   address)
{
    for (const auto &pr : symbol_pairs) {
        if (pr.second.svd.addr != address)
            continue;

        // This is the right address. Make sure that any prefix matches element.
        size_t colon_idx = pr.first.find_first_of (':');

        if (colon_idx == std::string::npos) {
            // No colon: this isn't a local variable, so we can just
            // use it.
            return pr.first.c_str ();
        }

        // There is a colon. If element is null, we definitely don't
        // match the context.
        if (! element)
            continue;

        // Otherwise, check that the first colon_idx characters of
        // pr.first is the same string as element.
        if (std::string (pr.first, 0, colon_idx) != element)
            continue;

        // Success!
        return pr.first.c_str () + colon_idx + 1;
    }

    // Oh dear. This should never happen.
    assert (0);
    abort ();
}

// Build an example symbol table. This will be done by the codec in
// the real system.
static symtab_t
mk_symbol_table ()
{
    symtab_t st (symbol_lookup, symbol_invert);
    st.constants = & constants;

    st.fun0s ["choose_bit"] = std::make_pair (& choose_bit, false);
    st.fun0s ["ret_one"] = std::make_pair (& ret_one, true);

    st.fun1s ["choose_bits"] = std::make_pair (& choose_bits, false);
    st.fun1s ["choose_bernoulli"] =
        std::make_pair (& choose_bernoulli_1, false);
    st.fun1s ["abs"] = std::make_pair (& abs64, true);

    st.fun2s ["min"] = std::make_pair (& min64, true);
    st.fun2s ["max"] = std::make_pair (& max64, true);
    st.fun2s ["choose_bernoulli"] =
        std::make_pair (& choose_bernoulli_2, false);

    st.fun3s ["clamp"] = std::make_pair (& clamp64, true);
    st.fun3s ["cabbage"] = std::make_pair (& cabbage_fun, false);

    for (char c = 'a'; c <= 'z'; ++ c) {
        const char str [2] = { c, '\0' };
        st.add_element (str);
    }
    st.add_element ("aa");
    st.add_element ("biggles");

    return st;
}

static
uint64_t generate_seed ()
{
    return static_cast<uint64_t>(
            std::chrono::high_resolution_clock::now()
                 .time_since_epoch()
                 .count());
}

static
bool is_whitespace (char c)
{
    if (c == ' ') return true;
    if (c == '\t') return true;
    if (c == '\r') return true;
    if (c == '\n') return true;
    return false;
}

static
std::vector<std::pair<std::string, bool>> delimiters = {
    { " ", false },
    { "*", true  },
    { "=", true  },
    { ":", true  },
    { "[", true  },
    { "]", true  }
};

static
void
print_script_error (int         line_number,
                    const char *msg,
                    const char *usage = NULL)
{
    fprintf (stderr, "script error: line %d: %s.\n", line_number, msg);
    if (usage) fprintf (stderr, "\n  Expecting `%s'.\n", usage);
}

static void
parse_split (std::vector<std::string>& tokens,
             std::string token,
             size_t depth)
{
    if (depth == delimiters.size ()) {
        tokens.push_back (token);
        return;
    }

    std::string& delimeter = delimiters[depth].first;
    bool include_delimeter = delimiters[depth].second;

    while (token != "") {
        std::string sub_token;

        size_t pos = token.find_first_of (delimeter);
        if (pos == std::string::npos) {
            sub_token = token;
            token = "";
        } else {
            sub_token = token.substr (0, pos);
            token = token.substr (pos + delimeter.size ());
        }

        if (sub_token != "") parse_split (tokens, sub_token, depth + 1);

        if (include_delimeter && pos != std::string::npos)
            tokens.push_back (delimeter);
    }
}

static std::vector<std::string>
parse_script_line (std::string& line)
{
    std::vector<std::string> tokens;

    if (is_whitespace (line.front ())) return tokens;

    // Remove '\r's from the line
    line.erase (std::remove (line.begin (), line.end (), '\r'), line.end ());

    std::istringstream iss_line (line);
    std::string token;

    while (getline (iss_line, token, '#')) {
        parse_split(tokens, token, 0);
        break;
    }

    return tokens;
}

static std::string
get_config_name (const char *script_file_name)
{
    if(script_file_name)
    {
        std::ifstream script (script_file_name);

        if (!script.is_open ()) {
            std::ostringstream oss;
            oss << "Unable to open script `" << script_file_name
                    << "'. Setting configuration to `default'.";
            print_error (oss.str ().c_str ());

            return "default";
        }

        std::string script_line;
        if (!std::getline (script, script_line)) {
            std::ostringstream oss;
            oss << "Script `" << script_file_name << "' is empty. "
                    << "Setting configuration to `default'.";
            print_error (oss.str ().c_str ());

            return "default";
        }

        std::vector<std::string> tokens = parse_script_line (script_line);

        if (tokens.size() > 0 && tokens[0] == "config") {
            if (tokens.size () < 2) {
                print_script_error (1, "missing config name", "config <config-name>");
                throw parse_error ();
            }

            return tokens[1];
        }
    }

    return "default";
}

static bool
string_to_int (const char *str, int64_t *value)
{
    assert (value);

    int base = 0;
    if(str[0] == '0' && (str[1] == 'b' || str[1] == 'B')) {
        str += 2;
        base = 2;
    }

    char *end;
    int64_t res = std::strtoll (str, &end, base);

    if (*end == '\0') {
        *value = res;
        return true;
    }

    return false;
}

template<typename TTo, typename TFrom>
static bool
int_cast(TFrom value, TTo *dest)
{
    assert (dest);

    TTo result = static_cast<TTo> (value);
    if (static_cast<TFrom> (result) != value) return false;

    *dest = result;
    return true;
}


template<typename T>
static void
set_variable (uintptr_t addr, size_t offset,
              int64_t value, const char *type, int line_number)
{
    assert (type);

    T result;
    if (!int_cast<T, int64_t> (value, &result)) {
        std::ostringstream oss;
        oss << "`" << value << "' is not a valid " << type;
        print_script_error (line_number, oss.str ().c_str ());
        throw parse_error ();
    }

    reinterpret_cast<T *> (addr) [offset] = result;
}

static void
set_variable_from_var_desc (const var_desc_t            &vdesc,
                            const std::vector<uint64_t> &array_indices,
                            int64_t                      value,
                            int                          line_number)
{
    // CHECK ARRAY INDICES ARE VALID //

    // Get number of supplied and required indices
    size_t num_supplied_indices = array_indices.size ();
    size_t num_required_indices = vdesc.dim;

    // Do I have too many indices?
    if (num_supplied_indices > num_required_indices) {
        std::ostringstream oss;
        oss << "Too many array indices have been specified: expecting "
            << num_required_indices << " indices";
        print_script_error (line_number, oss.str ().c_str ());
        throw runtime_error();
    }

    // Do I have too few indices?
    if (num_supplied_indices < num_required_indices) {
        std::ostringstream oss;
        oss << "Too few array indices have been specified: expecting "
            << num_required_indices << " indices";
        print_script_error (line_number, oss.str ().c_str ());
        throw runtime_error();
    }

    // Are indices valid?
    for (size_t i = 0; i < num_supplied_indices; i++) {
        if (array_indices[i] >= vdesc.lengths[i]) {
            std::ostringstream oss;
            oss << "Index " << i << " (value = " << array_indices[i]
                << ") is out of bounds: expected strictly less than "
                << vdesc.lengths[i];
            print_script_error (line_number, oss.str ().c_str ());
            throw runtime_error ();
        }
    }

    // Calculate location of element
    size_t offset = vdesc.indices_to_offset (array_indices);

    switch (vdesc.svd.type)
    {
        case -1:
            set_variable<int8_t> (vdesc.svd.addr, offset,
                                  value, "int8_t", line_number);
            break;
        case -2:
            set_variable<int16_t> (vdesc.svd.addr, offset,
                                   value, "int16_t", line_number);
            break;
        case -4:
            set_variable<int32_t> (vdesc.svd.addr, offset,
                                   value, "int32_t", line_number);
            break;
        case -8:
            set_variable<int64_t> (vdesc.svd.addr, offset,
                                   value, "int64_t", line_number);
            break;
        case 1:
            set_variable<uint8_t> (vdesc.svd.addr, offset,
                                   value, "uint8_t", line_number);
            break;
        case 2:
            set_variable<uint16_t> (vdesc.svd.addr, offset,
                                    value, "uint16_t", line_number);
            break;
        case 4:
            set_variable<uint32_t> (vdesc.svd.addr, offset,
                                    value, "uint32_t", line_number);
            break;
        case 8:
            set_variable<uint64_t> (vdesc.svd.addr, offset,
                                    value, "uint64_t", line_number);
            // This line may cause a problem. If value_str represents uint64_t_max,
            // then the int64_t and uint64_t casts will not match up. However, this
            // is unlikely, and so we don't bother dealing with that here.
            break;
        default:
            // We shouldn't reach here!
            assert (0);
    }
}

static void
set_variable_from_var_desc (const var_desc_t            &vdesc,
                            const std::vector<uint64_t> &array_indices,
                            const std::string           &value_str,
                            int                          line_number)
{
    int64_t value;
    bool parsed_value = string_to_int (value_str.c_str (), &value);
    if (!parsed_value) {
        std::ostringstream oss;
        oss << "`" << value_str << "' is not a valid number";
        print_script_error (line_number, oss.str ().c_str ());
        throw parse_error ();
    }

    set_variable_from_var_desc (vdesc, array_indices, value, line_number);
}

static void
throw_script_set_error (int line_number)
{
    print_script_error (line_number,
                        "incorrect syntax for set command",
                        "set <variable>[:<local-variable>]"
                        "[<indices>] = <value>");

    throw parse_error ();
}

static void
run_script(const char                      *script_file_name,
           symtab_t                        &symbol_table,
           std::unique_ptr<swizzle::map_t> &swizzled)
{
    assert (script_file_name);

    std::ifstream script (script_file_name);

    if (!script.is_open ()) {
        std::ostringstream oss;
        oss << "Unable to open script `" << script_file_name
                << "'. Setting configuration to `default'.";
        print_error (oss.str ().c_str ());

        return;
    }

    std::string script_line;
    int line_number = 0;
    while (std::getline (script, script_line)) {
        line_number++;

        std::vector<std::string> tokens = parse_script_line (script_line);
        if (tokens.empty ()) continue;

        if (line_number == 1 && tokens[0] == "config") continue; // We've already set the selected config

        if (tokens[0] == "config") {
            print_script_error (line_number,
                                "config can only be specified on the first line");
            throw parse_error ();
        }

        if (tokens[0] == "set") {
            int set_type = 0;

            // Of the form "set <variable>[<indices>] = <value>"
            if (tokens.size () >= 4 && tokens[tokens.size () - 2] == "=")
                set_type = 1;

            // Of the form "set <variable>[<indices>]:<local-variable>[<indices>] = <value>"
            if (tokens.size () >= 6 && tokens[tokens.size () - 2] == "="
                    && tokens[2] == ":")
                set_type = 2;

            if (!set_type) throw_script_set_error (line_number);

            std::vector<uint64_t> indices;
            std::string& value_str = tokens[tokens.size () - 1];
            var_desc_t vdesc;

            std::string blank_str = "";

            std::string& parent_name = (set_type == 1) ? blank_str : tokens[1];
            std::string& variable_name = (set_type == 1) ? tokens[1] : tokens[3];
            size_t i_start = (set_type == 1) ? 2 : 4;

            // Detemine indices
            bool in_bracket = false;
            for (size_t i = i_start; i < tokens.size () - 2; i++) {
                if (tokens[i] == "[") {
                    if (in_bracket) throw_script_set_error (line_number);
                    else in_bracket = true;
                } else if (tokens[i] == "]") {
                    if (tokens[i - 2] != "[") throw_script_set_error (line_number);
                    if (!in_bracket) throw_script_set_error (line_number);
                    else in_bracket = false;
                } else if (in_bracket) {
                    if (indices.size () == 4) throw_script_set_error (line_number);

                    int64_t value;
                    if (!string_to_int (tokens[i].c_str (), &value)) {
                        std::ostringstream oss;
                        oss << "`" << tokens[i] << "' is not a valid number.";
                        print_script_error (line_number, oss.str ().c_str ());
                        throw parse_error ();
                    } else if (value < 0) {
                        std::ostringstream oss;
                        oss << "`" << tokens[i] << "' is negative.";
                        print_script_error (line_number, oss.str ().c_str ());
                        throw parse_error ();
                    }
                    indices.push_back ((uint64_t) value);
                } else {
                    throw_script_set_error (line_number);
                }

            }

            if (in_bracket) throw_script_set_error (line_number);

            if (!symbol_lookup (&vdesc,
                                parent_name.c_str (),
                                variable_name.c_str ())) {
                std::ostringstream oss;
                oss << "`";
                if (parent_name != "") oss << parent_name << ":";
                oss << variable_name << "' is not a valid variable";
                print_script_error (line_number, oss.str ().c_str ());
                throw parse_error ();
            }

            set_variable_from_var_desc (vdesc, indices, value_str, line_number);
        }

        if (tokens[0] == "eval") {
            bool valid = (tokens.size () == 2)
                    || (tokens.size () == 4 && tokens[1] == "*");

            if (!valid) {
                print_script_error (line_number, "incorrect syntax for eval command",
                        "eval [* <repetitions>] <element>");
                throw parse_error ();
            }

            std::string& element_name =
                    (tokens.size () == 2) ? tokens[1] : tokens[3];
            int64_t iterations = 1;

            if (tokens.size () == 4) {
                bool parsed_iterations = string_to_int (tokens[2].c_str (),
                        &iterations);
                if (!parsed_iterations) {
                    std::ostringstream oss;
                    oss << "`" << tokens[2] << "' is not a valid number";
                    print_script_error (line_number, oss.str ().c_str ());
                    throw parse_error ();
                }
            }

            size_t element_index = symbol_table.element_index (element_name);
            if (element_index == SIZE_MAX) {
                std::ostringstream oss;
                oss << "`" << tokens[3] << "' is not a valid element";
                print_script_error (line_number, oss.str ().c_str ());
                throw parse_error ();
            }

            std::cout << element_name << ":\n";
            for (int64_t i = 0; i < iterations; i++) {
                int64_t result = swizzled->eval (rnd, element_index, symbol_table);
                std::cout << result << "\n";
            }
        }
    }
}


int main(int argc, char **argv) {

  const char *config_file_name = NULL;
  const char *script_file_name = NULL;
  bool debug_mode = false;

  for (int i = 1; i < argc; ++ i) {
    const char *arg = argv[i];

    if (arg[0] == '-') {
      if (strcmp (arg, "-d") == 0) {
        debug_mode = true;
      } else {
        std::cerr << "Error: Unknown flag `" << arg << "'.\n";
        usage ();
      }

      continue;
    }

    if (config_file_name) {
      if(script_file_name) {
        std::cerr << "Error: Too many arguments.\n";
        usage ();
      } else {
        script_file_name = arg;
      }
    } else {
      config_file_name = arg;
    }
  }

  if (! config_file_name) {
    std::cerr << "Error: No config file specified.\n";
    usage ();
  }

  symtab_t symbol_table (mk_symbol_table ());

  rnd = std::mt19937 (generate_seed ());

  try {
      std::string selected_config = get_config_name (script_file_name);

      std::unique_ptr<swizzle::map_t>
          swizzled (run_passes (config_file_name,
                                selected_config.c_str (),
                                symbol_table,
                                debug_mode));
      if (! swizzled) {
          // The parse failed. We've already printed an error, so can
          // just return 1.
          return 1;
      }

      swizzled->display (std::cout, symbol_table);

      if (script_file_name) {
        std::cout << "---\n";
        run_script (script_file_name, symbol_table, swizzled);
      }

  } catch (const parse_error &) {
    // We printed something before throwing the error, so can just return 1 here.
    return 1;
  } catch (const runtime_error &) {
    // We printed something before throwing the error, so can just return 2 here.
    return 2;
  }

  return 0;
}
