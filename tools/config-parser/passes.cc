/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "passes.hh"

#include "error.hh"

#include "config.tab.h"
#include "parse.hh"
#include "typing.hh"
#include "mem-ref.hh"
#include "collections.hh"
#include "inserts.hh"
#include "swizzle.hh"

std::unique_ptr<swizzle::map_t>
run_passes (const char               *filename,
            const char               *selected_config,
            const symtab_t           &symbol_table,
            bool                      debug_parser)
{
    assert (filename);

    if (! selected_config)
        selected_config = "default";

    typedef std::vector<std::unique_ptr<tl_list>> tl_list_vec_t;

    try {
        std::pair <std::vector<std::string>, tl_list_vec_t>
            parsed (parse (filename, debug_parser));

        assert (parsed.first.size () == parsed.second.size ());

        // Type all the parsed files
        typing::files_t typed (typing::pass (std::move (parsed.second),
                                             parsed.first));

        // Insert constant values (in place)
        typing::insert_constants (symbol_table, & typed, parsed.first);

        // Insert memory references
        mem_ref::files_t reffed (mem_ref::pass (symbol_table,
                                                std::move (typed),
                                                parsed.first));

        // Partially evaluate (in place)
        mem_ref::partial_eval (& reffed);

        // Convert to collections
        collections::files_t collected (collections::pass (std::move (reffed),
                                                           parsed.first));

        // Remove 'insert' statements
        std::unique_ptr<inserts::config_t>
            config (inserts::pass (std::move (collected),
                                   parsed.first, selected_config));

        // Swizzle
        return swizzle::pass (std::move (config),
                              std::move (parsed.first),
                              symbol_table.num_elements ());
    }
    catch (const parse_error &) {
        // If something went wrong, that's fine. We've printed out an
        // error message already and can just return null.
        return nullptr;
    }
}
