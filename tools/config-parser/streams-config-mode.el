;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Project P8005 HEVC
; (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
; All rights reserved.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; A simple major mode for bitstream configuration files
;;
;; To install it in your emacs, add the directory containing it to
;; your load-path:
;;
;;   (add-to-list 'load-path "/directory/containing/this/file")
;;
;; and add it as a default handler for files ending in .cfg:
;;
;;   (add-to-list 'auto-mode-alist '("\\.cfg\\'" . streams-config-mode))
;;
;; If you've just opened this file and want things to work now,
;; dammit, you can do the following. This won't persist next time you
;; restart emacs.
;;
;;   M-x eval-buffer
;;   M-x streams-config-mode-setup

(defun streams-config-mode-setup ()
  (interactive)
  "Set up the streams config mode.

You should probably do this properly by editing your
~/.emacs.d/init.el, but this will make stuff just work for the
time being, by adding the mode to your auto-mode-alist for files
ending in .cfg."
  (add-to-list 'auto-mode-alist '("\\.cfg\\'" . streams-config-mode)))

(defvar streams-config--keywords-re
  (regexp-opt '("default" "import" "insert" "if" "else")
              'symbols))

(defvar streams-config--font-lock-defaults
  `((("\"\\.\\*\\?" . font-lock-string-face)
     (,streams-config--keywords-re . font-lock-builtin-face))))

(defvar streams-config--syntax-table
  (let ((st (make-syntax-table)))
    (modify-syntax-entry ?/ ". 124b" st)
    (modify-syntax-entry ?* ". 23" st)
    (modify-syntax-entry ?\n "> b" st)

    ;; Turn '=' into punctuation
    (modify-syntax-entry ?= "." st)
    st))

(defvar streams-config--grammar
  (smie-prec2->grammar
   (smie-bnf->prec2
    '((stmt ("{" stmts "}")
            ("if" "(" expr ")" stmt)
            ("if" "(" expr ")" stmt "else" stmt))
      (stmts (stmts ";" stmts) (stmt))
      (expr (expr "+" expr)
            (expr "*" expr)))
    '((assoc "+") (assoc "*"))
    '((assoc ";"))
    '((assoc ")" "else")))
   ))

(defvar streams-config-indent-basic 4)

(defun streams-config--smie-rules (kind token)
  (let ((pr (cons kind token)))
    (cond ((equal pr '(:elem . basic)) streams-config-indent-basic)
          ((equal pr '(:after . ";")) 0)
          ((equal pr '(:before . "{"))
           (when (smie-rule-hanging-p) (smie-rule-parent)))
          ((equal pr '(:after . "}")) 0))))

(define-derived-mode streams-config-mode fundamental-mode "Streams-cfg"
  (setq font-lock-defaults streams-config--font-lock-defaults)

  (setq comment-start "//")
  (setq comment-end "")

  (set-syntax-table streams-config--syntax-table)
  (smie-setup streams-config--grammar 'streams-config--smie-rules)
  (setq require-final-newline mode-require-final-newline))

(provide 'streams-config-mode)
