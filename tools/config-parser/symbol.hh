/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include "place.hh"
#include "glue/var-desc.hh"

#include <map>
#include <string>
#include <vector>

struct symbol
{
    place_t     place;
    std::string name;

    symbol (const place_t &place, const char *name)
        : place (place), name (name)
    {}
};

// Symbol -> pointer mapping used in mem_ref pass
struct symtab_t
{
    // The symbol lookup function takes the symbol in question,
    // together with the bitstream element we're evaluating (this
    // might be null).
    typedef bool (sym_lookup_t) (var_desc_t *dest,
                                 const char *element,
                                 const char *symbol);

    // The symbol invert function takes a memory address and name of
    // the current bitstream element (might be null) and should return
    // the corresponding symbol name.
    typedef const char *(sym_invert_t) (const char *element,
                                        uintptr_t   address);

    typedef int64_t (fun0_t ());
    typedef int64_t (fun1_t (int64_t));
    typedef int64_t (fun2_t (int64_t, int64_t));
    typedef int64_t (fun3_t (int64_t, int64_t, int64_t));

    template <typename T> using str_map_t = std::map<std::string, T>;
    template <typename T> using str_fun_map_t = str_map_t<std::pair <T, bool>>;

    symtab_t (sym_lookup_t *sym_lookup,
              sym_invert_t *sym_invert)
        : sym_lookup (sym_lookup),
          sym_invert (sym_invert),
          constants (nullptr)
    {}

    sym_lookup_t                         *sym_lookup;
    sym_invert_t                         *sym_invert;

    const std::map<std::string, int64_t> *constants;

    // The bool is a flag saying whether the function is pure.
    str_fun_map_t <fun0_t *>              fun0s;
    str_fun_map_t <fun1_t *>              fun1s;
    str_fun_map_t <fun2_t *>              fun2s;
    str_fun_map_t <fun3_t *>              fun3s;

    // Add an element to the elements table. This element must not have been
    // added already.
    void add_element (const std::string &element);

    // Get the index of an element by name. If the name can't be found, return
    // SIZE_MAX.
    size_t element_index (const std::string &element) const;

    // Return the number of elements known
    size_t num_elements () const { return elements.size (); }

    // Return the name of an element by index
    const std::string &element_name (size_t idx) const;

private:
    // These are the overrideable bitstream elements. This should be a list
    // without duplicates. The positions have meaning: the end result of parsing
    // a configuration file will be a vector of slices of the same length as
    // elements where entry i will correspond to element i.
    //
    // This is hidden because we keep it in sync with a std::map from name to
    // index.
    std::vector<std::string>      elements;
    std::map<std::string, size_t> element_indices;
};
