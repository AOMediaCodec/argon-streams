/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include "ast-ops.hh"
#include "place.hh"
#include "symbol.hh"

#include <memory>
#include <vector>

// Forward declaration (type is in ast.hh)
struct tl_statement;
template <typename T> struct elt_list;
typedef elt_list<tl_statement> tl_list;

// Forward declarations (from mem-ref.hh)
namespace mem_ref {
    struct config_t;
    struct stmt_t;
    struct int_expr_t;
    struct coll_expr_t;
    struct ivl_t;
    struct arith_prog_t;
    struct weighted_pair_t;
}

// This is the "typing pass", where everything gets a sensible type
// and we spot silly errors like adding a number to a collection etc.

namespace typing
{
    // This is the base type for expressions that should be evaluated
    // to an integer.
    struct int_expr_t
    {
        typedef std::unique_ptr<int_expr_t> ptr_t;
        typedef std::vector<ptr_t>          ptrs_t;

        place_t place;

        int_expr_t (place_t place) : place (place) {}
        virtual ~ int_expr_t () {}

        // Build an object for the mem_ref pass. On success, returns
        // the new object. On failure, prints out one or more error
        // messages and returns nullptr. Destructive (may modify
        // internals of this object). The element argument is the name
        // of the bitstream element that is being set.
        virtual std::unique_ptr<mem_ref::int_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) = 0;

        // Insert constants into the expression. On success returns
        // true (whether or not any constants were inserted). On
        // error, prints out one or more error messages and returns
        // false.
        virtual bool insert_constants (const symtab_t &symbol_table,
                                       const char     *filename) = 0;
    };

    // This is the base type for expressions that should be evaluated
    // to a collection.
    struct coll_expr_t
    {
        typedef std::unique_ptr<coll_expr_t> ptr_t;
        typedef std::vector<ptr_t>           ptrs_t;

        place_t place;

        coll_expr_t (place_t place) : place (place) {}
        virtual ~ coll_expr_t () {}

        // Build an object for the mem_ref pass. On success, returns
        // the new object. On failure, prints out one or more error
        // messages and returns nullptr. Destructive (may modify
        // internals of this object). The element argument is the name
        // of the bitstream element that is being set.
        virtual std::unique_ptr<mem_ref::coll_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) = 0;

        // Insert constants into the expression. On success returns
        // true (whether or not any constants were inserted). On
        // error, prints out one or more error messages and returns
        // false.
        virtual bool insert_constants (const symtab_t &symbol_table,
                                       const char     *filename) = 0;
    };

    // A literal integer expression.
    struct int_literal_t : public int_expr_t
    {
        int64_t value;

        int_literal_t (place_t place, int64_t value)
            : int_expr_t (place), value (value)
        {}

        std::unique_ptr<mem_ref::int_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    // A variable, possibly with array indices
    struct variable_t : public int_expr_t
    {
        symbol sym;
        ptrs_t indices;

        variable_t (place_t place, symbol sym)
            : int_expr_t (place), sym (sym)
        {}

        std::unique_ptr<mem_ref::int_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    // A function call with integer expression arguments.
    struct funcall_t : public int_expr_t
    {
        symbol sym;
        ptrs_t args;

        funcall_t (place_t place, symbol sym)
            : int_expr_t (place), sym (sym)
        {}

        std::unique_ptr<mem_ref::int_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    // A call to the choose() function
    struct choose_t : public int_expr_t
    {
        place_t            sym_place;
        coll_expr_t::ptr_t arg;

        choose_t (place_t place, place_t sym_place, coll_expr_t *arg)
            : int_expr_t (place), sym_place (sym_place), arg (arg)
        {}

        std::unique_ptr<mem_ref::int_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    struct ivl_t
    {
        place_t           place;
        int_expr_t::ptr_t lo;
        int_expr_t::ptr_t hi; // May be null

        ivl_t (place_t place, int_expr_t *lo, int_expr_t *hi)
            : place (place), lo (lo), hi (hi)
        {}

        bool to_mem_ref (const char                  *filename,
                         const char                  *element,
                         const symtab_t              &symbol_table,
                         std::vector<mem_ref::ivl_t> *dest);

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename);
    };

    struct uniform_t : public coll_expr_t
    {
        bool               choose_all;
        std::vector<ivl_t> intervals;

        uniform_t (place_t place, bool choose_all)
            : coll_expr_t (place), choose_all (choose_all)
        {}

        std::unique_ptr<mem_ref::coll_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    struct weighted_pair_t
    {
        coll_expr_t::ptr_t coll;
        int_expr_t::ptr_t  weight; // May be null

        weighted_pair_t (coll_expr_t *coll, int_expr_t *weight)
            : coll (coll), weight (weight)
        {}

        bool to_mem_ref (const char     *filename,
                         const char     *element,
                         const symtab_t &symbol_table,
                         std::vector<mem_ref::weighted_pair_t> *dest);

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename);
    };

    struct weighted_t : public coll_expr_t
    {
        std::vector<weighted_pair_t> pairs;

        weighted_t (place_t place)
            : coll_expr_t (place)
        {}

        std::unique_ptr<mem_ref::coll_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    // The only unary operations are on integer expressions
    struct unop_expr_t : public int_expr_t
    {
        unop_t op;
        ptr_t  arg;

        unop_expr_t (place_t place, unop_t op, int_expr_t *arg)
            : int_expr_t (place), op (op), arg (arg)
        {}

        std::unique_ptr<mem_ref::int_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    // Integer expressions that are binary operations
    struct int_binop_expr_t : public int_expr_t
    {
        int_binop_t op;
        ptr_t       l, r;

        int_binop_expr_t (place_t place,
                          int_expr_t *l, int_binop_t op, int_expr_t *r)
            : int_expr_t (place), op (op), l (l), r (r)
        {}

        std::unique_ptr<mem_ref::int_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    // The conditional operator (can be used for integer expressions
    // or collection expressions)
    template <typename T, typename T1>
    struct cond_expr_t : public T
    {
        int_expr_t::ptr_t test;
        typename T::ptr_t if_true, if_false;

        cond_expr_t (place_t place, int_expr_t *test, T *if_true, T *if_false)
            : T (place), test (test), if_true (if_true), if_false (if_false)
        {}

        std::unique_ptr<T1>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    // An arithmetical progression. This isn't an expression on its
    // own (it can only appear in a seq expression).
    struct arith_prog_t
    {
        place_t           place;
        ivl_t             ivl;
        int_expr_t::ptr_t step; // May be null

        arith_prog_t (place_t place, ivl_t &&ivl, int_expr_t *step)
            : place (place), ivl (std::move (ivl)), step (step)
        {}

        bool to_mem_ref (const char     *filename,
                         const char     *element,
                         const symtab_t &symbol_table,
                         std::vector<mem_ref::arith_prog_t> *dest);

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename);
    };

    // This is the expression that comes from a call to seq()
    struct seq_t : public coll_expr_t
    {
        std::vector<arith_prog_t> progs;

        seq_t (place_t place)
            : coll_expr_t (place)
        {}

        std::unique_ptr<mem_ref::coll_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    // This is an integer expression thought of as a degenerate
    // collection (sampling from it always yields the same value).
    struct int_coll_expr_t : public coll_expr_t
    {
        int_expr_t::ptr_t expr;

        int_coll_expr_t (place_t place, int_expr_t::ptr_t &&expr)
            : coll_expr_t (place), expr (std::move (expr))
        {}

        std::unique_ptr<mem_ref::coll_expr_t>
        to_mem_ref (const char     *filename,
                    const char     *element,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    // The base statement type. This is for statements that occur
    // within configuration blocks.
    struct stmt_t
    {
        typedef std::unique_ptr<stmt_t> ptr_t;
        typedef std::vector<ptr_t>      ptrs_t;

        virtual ~ stmt_t () {}

        // Build an object for the mem_ref pass. On success, returns
        // the new object. On failure, prints out one or more error
        // messages and returns nullptr. Destructive (may modify
        // internals of this object).
        virtual std::unique_ptr<mem_ref::stmt_t>
        to_mem_ref (const char     *filename,
                    const symtab_t &symbol_table) = 0;

        // Insert constants into the statement. On success returns
        // true (whether or not any constants were inserted). On
        // error, prints out one or more error messages and returns
        // false.
        virtual bool insert_constants (const symtab_t &symbol_table,
                                       const char     *filename) = 0;
    };

    struct block_stmt_t : public stmt_t
    {
        ptrs_t stmts;

        std::unique_ptr<mem_ref::stmt_t>
        to_mem_ref (const char     *filename,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    struct if_stmt_t : public stmt_t
    {
        int_expr_t::ptr_t test;
        ptr_t             if_true;
        ptr_t             if_false; // May be null

        if_stmt_t (int_expr_t *test,
                   stmt_t     *if_true,
                   stmt_t     *if_false = nullptr)
            : test (test), if_true (if_true), if_false (if_false)
        {}

        std::unique_ptr<mem_ref::stmt_t>
        to_mem_ref (const char     *filename,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    struct assign_stmt_t : public stmt_t
    {
        symbol            sym;
        int_expr_t::ptr_t expr;

        assign_stmt_t (symbol sym, int_expr_t *expr)
            : sym (sym), expr (expr)
        {}

        std::unique_ptr<mem_ref::stmt_t>
        to_mem_ref (const char     *filename,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    struct insert_stmt_t : public stmt_t
    {
        symbol sym;

        insert_stmt_t (symbol sym)
            : sym (sym)
        {}

        std::unique_ptr<mem_ref::stmt_t>
        to_mem_ref (const char     *filename,
                    const symtab_t &symbol_table) override;

        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename) override;
    };

    struct config_t
    {
        symbol         name;
        stmt_t::ptrs_t stmts;

        config_t (symbol name)
            : name (name)
        {}

        // Build an object for the mem_ref pass. Destructive.
        bool to_mem_ref (const char                     *filename,
                         const symtab_t                 &symbol_table,
                         std::vector<mem_ref::config_t> *dest);

        // Insert constants into the configuration. On success returns
        // true (whether or not any constants were inserted). On
        // error, prints out one or more error messages and returns
        // false.
        bool insert_constants (const symtab_t &symbol_table,
                               const char     *filename);
    };

    typedef std::vector<config_t> file_t;
    typedef std::vector<file_t>   files_t;

    // The actual pass
    files_t pass (std::vector<std::unique_ptr<tl_list>> &&src,
                  const std::vector<std::string>         &paths);

    // The partial evaluation pass (operates in place on the data)
    void insert_constants (const symtab_t                 &symbol_table,
                           files_t                        *files,
                           const std::vector<std::string> &paths);
}
