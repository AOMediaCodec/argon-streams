#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

set -e
set -x

# Get the absolute path of the current directory; we're going to build
# to a coverage-build directory in there.
ABD=$(readlink -f .)/coverage-build
mkdir -p coverage-build

# We're going to need to run Make in the directory that contains this
# script.
MD=$(dirname "${BASH_SOURCE[0]}")/..

# Clean out any existing gcda's in coverage-build
find coverage-build -name '*.gcda' | xargs -r rm
# And any .good files (these are the results from running the tests
# and we want to force them to run again).
find coverage-build -name '*.good' | xargs -r rm

# Build and run the config parser with coverage collection enabled
# with outputs going in BD. The -k flag means we'll actually get a
# result.
#
# Defining NDEBUG turns off assertions. The point is that we
# (hopefully) already know the test suite passes with assertions
# turned on and this means we don't see bogus branch coverage ("Hey,
# that assertion never failed!").
make -k -C "$MD" \
  BUILD_CFG_PARSER=1 CHECK_COMPILER_VERSION=0 \
  CXXFLAGS='-O0 --coverage -DNDEBUG' \
  P8005_BUILD_PREFIX="$ABD" -j4 \
  config-test

gcovr --html --html-details \
      -r "$MD" \
      --object-directory coverage-build/build/config-parser \
      --exclude coverage-build \
      -o coverage-build/coverage.html \
      -s
