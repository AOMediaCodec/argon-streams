/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <cassert>
#include <cstdint>
#include <iosfwd>
#include <map>
#include <memory>
#include <vector>

#include "ast-ops.hh"
#include "place.hh"
#include "symbol.hh"

namespace collections
{
  struct int_expr_t;
  struct config_t;
  typedef std::vector<config_t> file_t;
  typedef std::vector<file_t>   files_t;
};

namespace swizzle
{
  struct slice_t;
  struct block_slice_t;
}

// These collections are the ones that can be used at run-time. Note
// that collections like seq() and all() can only be constructed with
// actual numbers rather than arbitrary expressions, as seen in
// earlier passes.

namespace inserts
{
  // The base statement type.
  struct stmt_t
  {
      typedef collections::int_expr_t expr_t;
      int path_index;

      stmt_t (int path_index) : path_index (path_index) { }
      virtual ~ stmt_t() {}

      // Fill dest with a list of all the element indices that are assigned
      // to in this configuration (once each).
      virtual void elements (std::vector<size_t> *dests) const = 0;

      // Add slices into dest to represent this statement
      virtual void slice_into (size_t                  element,
                               swizzle::block_slice_t *dest) const = 0;
  };

  struct block_stmt_t: public stmt_t
  {
      std::vector<std::unique_ptr<stmt_t>> stmts;

      block_stmt_t (int path_index) : stmt_t (path_index) { }

      void elements (std::vector<size_t> *dests) const override;
      void slice_into (size_t                  element,
                       swizzle::block_slice_t *dest) const override;
  };

  struct if_stmt_t: public stmt_t
  {
      std::unique_ptr<expr_t> test;
      std::unique_ptr<stmt_t> if_true;
      std::unique_ptr<stmt_t> if_false; // may be null

      if_stmt_t (int path_index, expr_t *test, stmt_t *if_true, stmt_t *if_false);
      ~ if_stmt_t ();

      void elements (std::vector<size_t> *dests) const override;
      void slice_into (size_t                  element,
                       swizzle::block_slice_t *dest) const override;
  };

  struct assign_stmt_t: public stmt_t
  {
      size_t                  dest;
      std::unique_ptr<expr_t> expr;

      assign_stmt_t (int path_index, size_t dest, expr_t *expr);
      ~ assign_stmt_t ();

      void elements (std::vector<size_t> *dests) const override;
      void slice_into (size_t                  element,
                       swizzle::block_slice_t *dest) const override;
  };

  struct config_t
  {
      symbol name;
      std::vector<std::unique_ptr<stmt_t>> stmts;

      config_t(symbol name) :
          name(name)
      { }

      // Fill dest with a list of all the element indices that are assigned to
      // in this configuration (once each).
      void elements (std::vector<size_t> *dest) const;

      // Return a sliced view of this config for just the element with the given
      // index.
      std::unique_ptr<swizzle::block_slice_t>
      slice_config (size_t element) const;
  };

  // The pass that builds a single configuration
  std::unique_ptr<config_t>
  pass (collections::files_t           &&src,
        const std::vector<std::string>  &paths,
        const char                      *selected_config);
}
