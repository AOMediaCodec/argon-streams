/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <cassert>
#include <cstdint>
#include <cstring>
#include <vector>

// A simple variable descriptor, containing just an address and a signed size.
struct simple_var_desc_t
{
    // The address of the variable
    uintptr_t addr;

    // The type of the variable. This is a signed number whose
    // absolute value is the width of the variable in bytes and the
    // sign is positive if the variable is unsigned and negative
    // otherwise. A uint8_t would be represented by 1. An int64_t
    // would be represented by -8.
    //
    // As a special case, an invalid descriptor has type 0.
    int8_t    type;

    simple_var_desc_t (uintptr_t addr, int8_t type)
        : addr (addr), type (type)
    {}

    // Read the value in the variable
    int64_t read (int64_t offset = 0) const
    {
        switch (type)
        {
            case -1:
                return *(((const int8_t *) addr) + offset);
            case 1:
                return *(((const uint8_t *) addr) + offset);
            case -2:
                return *(((const int16_t *) addr) + offset);
            case 2:
                return *(((const uint16_t *) addr) + offset);
            case -4:
                return *(((const int32_t *) addr) + offset);
            case 4:
                return *(((const uint32_t *) addr) + offset);
            case -8:
                return *(((const int64_t *) addr) + offset);
            case 8:
                return *(((const uint64_t *) addr) + offset);
            // LCOV_EXCL_START
            default:
                assert (0);
                __builtin_unreachable ();
            // LCOV_EXCL_STOP
        }
    }
};

inline bool
operator== (const simple_var_desc_t &svd0, const simple_var_desc_t &svd1)
{
    // We assume there is no pointer aliasing, so don't bother
    // checking that the types match.
    return svd0.addr == svd1.addr;
}

struct var_desc_t
{
    // The descriptor for the variable. If this is an array, it points
    // to the first element.
    simple_var_desc_t svd;

    // The dimensionality of the array named by this variable
    // descriptor. If it doesn't name an array, dim is 0.
    uint8_t dim;

    // The lengths of the dimensions of the array. This points to an
    // array of lengths. If dim is non-zero, the pointer must be valid
    // and must point to an array of length at least equal to dim.
    const size_t *lengths;

    var_desc_t ()
        : svd (0, 0), dim (0), lengths (nullptr)
    {}

    var_desc_t (uintptr_t     addr,
                int8_t        type,
                uint8_t       dim,
                const size_t *lengths)
        : svd (addr, type), dim (dim), lengths (lengths)
    {
        if (dim)
            assert (lengths);
    }

    size_t indices_to_offset (std::vector <uint64_t> indices) const
    {
        // This should be statically checked before evaluation time,
        // so we just use an assertion here.
        assert (indices.size () == dim);

        // To calculate the offset to the element, we work through the
        // indices from right to left. The stride for the right-most
        // index is 1.
        size_t offset = 0;
        size_t stride = 1;

        for (uint8_t d = dim; d; -- d) {

            size_t idx = indices [d - 1];

            // Indices also should be less than the relevant length
            assert ((uint64_t) idx <= lengths [d - 1]);

            offset += idx * stride;
            stride *= lengths [d - 1];
        }

        return offset;
    }

    int64_t read (std::vector<uint64_t> indices) const
    {
        return svd.read (indices_to_offset (indices));
    }
};
