/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <stdexcept>

struct runtime_error : public std::runtime_error
{
    runtime_error ()
        : std::runtime_error ("runtime error")
    {}
};
