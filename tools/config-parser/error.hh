/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <stdexcept>
#include "glue/runtime-error.hh"
#include "place.hh"

struct parse_error : public std::runtime_error
{
    parse_error ()
        : std::runtime_error ("parse error")
    {}
};

// Print an error to stderr.
//
// filename and place give the position where the error happened and
// msg is the error message.
void print_error (const char    *filename,
                  const place_t &place,
                  const char    *msg,
                  const char    *post_msg = NULL);

void print_error (const char    *filename,
                  const char    *msg);

void print_error (const char    *msg);
