#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A script for testing the config parser.'''

import argparse
import os
import subprocess
import sys
from collections.abc import Sequence


# Creates a set from a list of args
def set_def(*args):
    S = set({})

    for arg in args:
        sequ_arg = arg
        if not isinstance(arg, Sequence):
            sequ_arg = {arg}

        S.update(sequ_arg)

    return S


# Creates a sequence from a list of args
def seq_def(*args):
    L = list({})

    for arg in args:
        sequ_arg = arg
        if not isinstance(arg, Sequence):
            sequ_arg = {arg}

        L = L + list(sequ_arg)

    return L


# Creates an interval
def ivl(lo, hi):
    return range(lo, hi + 1)


# Creats an arithmetic progression
def aprog(lo, hi, step=None):
    if step is None:
        if lo > hi:
            return range (lo, hi - 1, -1)
        else:
            return range (lo, hi + 1, 1)
    else:
        if step > 0:
            return range (lo, hi + 1, step)
        else: return range(lo, hi - 1, step)


# Clamps a value between lo and hi
def clamp(value, lo, hi):
    return max (lo, min (value, hi))


def throw_error(path, line_number, msg, usage):
    if len(path):
        sys.stderr.write('error: {}:{}: {}.\n'.format(path, line_number, msg));
    else:
        sys.stderr.write('error: line {}: {}.\n'.format(line_number, msg));

    if len(usage):
        sys.stderr.write('\n  Expecting `{}\'.\n'.format(usage));

    sys.exit(1)


# Attempts to generate a set of values from the text in the script
def mk_set(path, line_number, set_defn):
    if not set_defn:
        throw_error(path, line_number, 'expected definition of a set', '<CONDITION> <set>')

    try:
        S = set(eval(set_defn))
    except:
        throw_error(path, line_number, 'expected definition of a set', '<CONDITION> <set>')

    return S


# Attempts to generate an ordered list of values from the text in the script
def mk_list(path, line_number, seq_defn):
    if not seq_defn:
        throw_error(path, line_number, 'expected definition of a list', 'SEQ <list>')

    try:
        S = eval(seq_defn)
    except:
        throw_error(path, line_number, 'expected definition of a list', 'SEQ <list>')

    return S


def check_result(in_config_path,
                 in_script_path,
                 is_script_test,
                 retcode,
                 config_stdout_path,
                 script_stdout_path,
                 stderr_path):
    good = True

    set_def(1, 2)

    expected_err_path = in_config_path + '.err'
    expected_err_exists = os.path.exists(expected_err_path)

    # We require that a non-zero exit code <=> existence of expected error file

    if retcode == 0 and expected_err_exists:
        print('Found expected error file ({}). Unexpected retcode: 0.'
              .format(expected_err_path))
        good = False

    if retcode != 0 and not expected_err_exists:
        print('retcode ({}) was non-zero. Expected error file not found: {}'
              .format(retcode, expected_err_path))
        good = False;

    if retcode == 0:

        # # Firstly, check that the "config" part gave the correct output

        # Is there an expected config_stdout_path? In which case, diff the
        # output with it. Otherwise, diff with the input file.
        expected_out = in_config_path + '.out'
        if not os.path.exists(expected_out):
            expected_out = in_config_path

        has_diff = subprocess.call(['diff', '-q', expected_out,
                                    config_stdout_path])

        if has_diff:
            print('Output differs from expected (diff {} {}).'
                  .format(expected_out, config_stdout_path))
            good = False

        # # Now check that we got the correct output from the "script" part
        if is_script_test:

            with open(script_stdout_path) as script_stdout_file:
                std_line_number = 0;

                with open(in_script_path) as in_script_file:
                    # We can assume that the syntax for the lines read by
                    # config-test C++ code is correct, or we would have
                    # produced an error already

                    # in_eval_block tells us if we're on or inside an eval block
                    in_eval_block = False
                    line_number = 0
                    for raw_line in in_script_file:
                        line_number = line_number + 1
                        # Remove comments from the line
                        line = raw_line.split('#')[0]

                        if len(line.strip()) == 0:
                            # If the line is blank, move to next line
                            continue
                        else:
                            # If a line is not indented, it means we are no longer in the
                            # eval block
                            if in_eval_block and not line[0].isspace():
                                in_eval_block = False

                            # If a line is indented and we're not in an eval block, throw
                            # an error
                            if not in_eval_block and line[0].isspace():
                                throw_error(in_script_path, line_number,
                                            'lines cannot be indented '
                                            'unless in an eval block', '')

                        if in_eval_block:
                            # Get line number of the eval line (at the top of the eval block)
                            std_block_line_number = std_line_number - iterations

                            # Now let's read in the instructions for how to test these
                            # values, and perform the tests
                            stripped_line = line.strip()

                            # Check if all values are equal to a specific value
                            if stripped_line[:6] == 'EQUALS':
                                val_defn = stripped_line[6:].strip()
                                ref_value = eval(val_defn)

                                for value in values:
                                    std_block_line_number = std_block_line_number + 1
                                    if value != ref_value:
                                        throw_error(script_stdout_path, std_block_line_number,
                                                    'the value `{}\' is not equal to `{}\' = `{}\''
                                                    .format(value, val_defn, ref_value), '')

                            # Check if all values are within a set
                            elif stripped_line[:2] == 'IN':
                                set_defn = stripped_line[2:].strip()
                                S = mk_set(in_script_path, line_number, set_defn)

                                for value in values:
                                    std_block_line_number = std_block_line_number + 1
                                    if value not in S:
                                        throw_error(script_stdout_path, std_block_line_number,
                                                    'the value `{}\' is not in the set `{}\''
                                                    .format(value, set_defn), '')

                            # Check if the values fit the 'all' collection defined by the specified set
                            elif stripped_line[:3] == 'ALL':
                                set_defn = stripped_line[3:].strip()
                                S = mk_set(in_script_path, line_number, set_defn)

                                S_size = len(S)

                                cur_value_pos = 0
                                while cur_value_pos + S_size < iterations:
                                    std_block_line_number = std_block_line_number + S_size

                                    cur_values = values[cur_value_pos:cur_value_pos + S_size]
                                    cur_values_set = set(cur_values);

                                    if cur_values_set != S:
                                        throw_error(script_stdout_path, std_block_line_number,
                                                    'the values `{}\' do not choose all from the set `{}\''
                                                    .format(values[cur_value_pos:cur_value_pos + S_size],
                                                            set_defn), '')

                                    cur_value_pos = cur_value_pos + S_size

                                cur_values = values[cur_value_pos:]
                                cur_values_set = set(cur_values);

                                if not cur_values_set <= S or len(cur_values_set) != len(cur_values):
                                    throw_error(script_stdout_path, std_block_line_number,
                                                'the values `{}\' do not choose all from the set `{}\''
                                                .format(values[cur_value_pos:cur_value_pos + S_size],
                                                        set_defn), '')

                            # Check if the values fit the 'seq' collection defined by the specified list
                            elif stripped_line[:3] == 'SEQ':
                                seq_defn = stripped_line[3:].strip()
                                S = mk_list(in_script_path, line_number, seq_defn)

                                for i in range(len(values)):
                                    std_block_line_number = std_block_line_number + 1
                                    ref_i = i % len(S)

                                    if values[i] != S[ref_i] :
                                        throw_error(script_stdout_path, std_block_line_number,
                                            'the value `{}\' does not match the value `{}\' in the sequence `{}\''
                                            .format(values[i], S[ref_i], seq_defn), '')

                            # Check if the values cover all of the values in the defined set. Values
                            # are permitted to lie outside the set.
                            elif stripped_line[:5] == 'COVER':
                                set_defn = stripped_line[5:].strip()
                                S = mk_set(in_script_path, line_number, set_defn)

                                cur_values_set = set(values)
                                if not S <= cur_values_set:
                                    throw_error(script_stdout_path, std_block_line_number,
                                                'the values `{}\' do not cover the set `{}\''
                                                .format(values, set_defn), '')

                            # Run a custom function. The list of values can be accessed by using the
                            # list 'values'.
                            elif stripped_line[:8] == 'FUNCTION':
                                fn_defn = stripped_line[8:].strip()

                                # Attempt to evaluate function. Throw an error if this fails.
                                try:
                                    if not eval(fn_defn):
                                        throw_error(script_stdout_path, std_block_line_number,
                                                'the values `{}\' do not satisfy the function `{}\''
                                                .format(values, fn_defn), '')
                                except:
                                    throw_error(in_script_path, line_number, 'expected function or expression',
                                                'FUNCTION <function/expression>')

                            # Couldn't find anything we expect, so throw an error
                            else:
                                throw_error(in_script_path, line_number, 'expected condition', '')

                        else:
                            # Check if line starts with 'eval'
                            if line[:4] == 'eval':
                                # Remove 'eval' from the front of the line
                                eval_line_0 = line[4:].strip()

                                if eval_line_0[0] == '*':
                                    # Read values from syntax 'eval * <repetitions> <element>'
                                    eval_line_1 = eval_line_0[1:].strip()
                                    eval_line_1_split = eval_line_1.split()

                                    iterations = int(eval_line_1_split[0])
                                    element = eval_line_1_split[1].strip();
                                else:
                                    # Read values from syntax 'eval <element>'
                                    element = eval_line_0
                                    iterations = 1

                                # We now know what the element is called, and how many
                                # iterations to read. Let's read these from the script
                                # stdout.

                                # We expect the first line to be an element followed by
                                # a colon, and the subsequent lines to be the evaluated
                                # values of that element.

                                std_line_number = std_line_number + 1
                                std_line = script_stdout_file.readline().strip()

                                if std_line != '{}:'.format(element):
                                    throw_error(script_stdout_path, std_line_number,
                                                'element name is expected here', '{}:'
                                                .format(element))

                                values = []

                                for i in range(iterations):
                                    std_line_number = std_line_number + 1
                                    std_line = script_stdout_file.readline().strip()

                                    try:
                                        value = int(std_line)
                                        values.append(value)
                                    except ValueError:
                                        throw_error(script_stdout_path, std_line_number,
                                                    'an integer is expected here', '')

                                in_eval_block = True

    else:

        # # We are expecting this error. Let's check that the error we got
        # # matches the one we're expecting.

        has_diff = subprocess.call(['diff', '-q', expected_err_path, stderr_path])
        if has_diff:
            print('Error output differs from expected (diff {} {}).'
                  .format(expected_err_path, stderr_path))
            good = False

    return good


def main():
    '''Main test entry point'''
    parser = argparse.ArgumentParser()
    parser.add_argument('binary', help='The config-test binary')
    parser.add_argument('in_path', help='The input file')
    parser.add_argument('obase', help='The base file name for output')

    args = parser.parse_args()

    cfg_stdout_path = args.obase + '.cfg.stdout'
    script_stdout_path = args.obase + '.script.stdout'
    stderr_path = args.obase + '.stderr'

    # Determine if the corresponding script file exists
    in_config_path = args.in_path
    in_path_base = os.path.splitext(in_config_path)[0]
    in_script_path = in_path_base + '.script'
    in_script_exists = os.path.exists(in_script_path)

    arg_list = [args.binary, in_config_path]
    if in_script_exists:
        arg_list.append(in_script_path);

    # Run test and grab stdout, stderr and retcode
    p = subprocess.Popen(arg_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    stdout_str = stdout.decode('utf-8')
    stderr_str = stderr.decode('utf-8')
    retcode = p.returncode

    with open(stderr_path, 'w') as stderr_file:
        stderr_file.write(stderr_str)

    stdout_split = stdout_str.split('---\n', 1);

    # Verify that splitting occurs => existence of script file
    if len(stdout_split) == 2 and not in_script_exists:
        print('The output suggests that a script was executed, '
              'but the script file ({}) was not found.'
              .format(in_script_path))

    # Store the config part of the stdout
    with open(cfg_stdout_path, 'w') as cfg_stdout_file:
        cfg_stdout_file.write(stdout_split[0])

    # Store the script part of the stdout, providing it exists
    if len(stdout_split) == 2:
        with open(script_stdout_path, 'w') as script_stdout_file:
            script_stdout_file.write(stdout_split[1])

    if not check_result(in_config_path,
                        in_script_path,
                        in_script_exists,
                        retcode,
                        cfg_stdout_path,
                        script_stdout_path,
                        stderr_path):
        print('Test failed for in_path file {}.'.format(args.in_path))
        return 1

    return 0


if __name__ == '__main__':
    exit(main())
