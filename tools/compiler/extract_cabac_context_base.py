################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import cPickle,sys

# Go through the special list of cabac definitions and binarisation functions and place into a dictionary
CABAC_CONTEXT={}
def cabac_context_fn(name,b,a,init):
    """Syntax is called name, uses binarization function b, and implemented via a context function described in a.
    Uses context table name as init"""
    if not init:
        init = name
    init='init_'+init
    CABAC_CONTEXT[name]=b,a,init

CABAC_BINARY={}
def cabac_binary_fn(name,a,decls):
    """For binarisation name, record syntax tree a and declarations"""
    CABAC_BINARY[name]=a,[d[1] for d in decls] # Extract the name part of each declaration

assert sys.argv[-2]=='-o'
with open(sys.argv[-3],'rb') as fd:
    A=cPickle.load(fd)

extract_cabac_context(A).apply("start")[0]

with open(sys.argv[-1],'wb') as out:
    cPickle.dump((CABAC_CONTEXT,CABAC_BINARY),out,2)
