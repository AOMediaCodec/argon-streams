################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import cPickle,sys
from bitstream import *
from Node import *

pure_functions=['Abs','Max','Min','CeilLog2','Log2','CeilDiv']
myb=Bitstream('1')
def pure(fun,args):
    """Apply pure function fun to arglist args and return answer as integer"""
    return myb.__getattribute__(fun)(*args)

def add_to_dict(D,i,rhs):
    if not i[0].isupper():
        D[i]=rhs

def remove_from_dict(D,i):
    if i in D:
        del D[i]

def find_in_dict(D,i):
    return ('num',[],[D[i]]) if i in D else ('id',[],[i])

def disp(x):
    print x

assert sys.argv[-2]=='-o'
with open(sys.argv[-3],'rb') as fd:
    A=cPickle.load(fd)

B=fold_constants(A).apply("start")[0]

with open(sys.argv[-1],'wb') as out:
    cPickle.dump(B,out,2)
