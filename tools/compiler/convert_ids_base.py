################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import cPickle,sys

def access_field(myfn,field):
    """Constructs accessor for this field based on extracted structure of where field is defined"""
    if field in field_to_fn:
        fn = field_to_fn[field]
        if myfn!=fn:
            pass
    return field

def is_autostruct(x):
    """Return True if x is in a automatically generated structure"""
    is_g = '_' in x or not x[0].islower() # Default choice based on case and underscores
    if style=='C':
      is_g = False # Require us to define all autostructs when doing C
    if x in defined_fields:
      is_g = defined_fields[x]==1
    return is_g

def joinFieldElements(a, b):
    if a=='global_data':
        return a+join+b
    return a+'.'+b

def id_name(myfn,x):
    if x in defined_fields and defined_fields[x]==3:
      # A global access
      return 'id',[],[x]
    if not is_autostruct(x) and len(x)>1 and x.isupper() and style=='C':
      # A global access if using all capitals
      return 'id',[],[x]
    if is_autostruct(x):
        # A parsed syntax element, store on the local object
        # If accessed from a foreign function, access via structures on global object
        if x in field_to_fn:
            fn = field_to_fn[x]
            if myfn!=fn:
                x=joinFieldElements(fn, x)
                while fn in call_to_fn:
                    fn = call_to_fn[fn]
                    x=joinFieldElements(fn, x)
                return 'id',[],['b'+join+x]
        return 'id',[],['p'+join+x]
    else:
        # A local variable only used in this routine
        return 'localid',[],[x]
    #else:
    # AbcDffg are variables that can be used by later parts so should be global
    #   return 'b.'+x

def def_name(myfn,x):
    """Return name mangled string suitable for use in definitions"""
    # For C this is unaltered, while Python wants the mangled version
    if style=='C':
        return ('id' if is_autostruct(x) else 'localid'),[],[x]
    return id_name(myfn,x)

sys.setrecursionlimit(15000)

style = sys.argv[-4]
join={'P':'.','C':'->'}[style]


assert sys.argv[-2]=='-o'
with open(sys.argv[-3],'rb') as fd:
    A=cPickle.load(fd)

s = sys.argv[-3]
t = 'marked'
assert s[-len(t):]==t
with open(s[:-len(t)]+'struct','rb') as fd:
    field_to_fn,defined_fields,call_to_fn,id_to_type,id_to_array_dimensionality = cPickle.load(fd)

B=default_parser(A).apply("start")[0]

with open(sys.argv[-1],'wb') as out:
    cPickle.dump(B,out,2)

