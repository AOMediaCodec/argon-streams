################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> ''.join(e)

function = ['fn' [statement(''):stmts] [:name :decls :ret] ] -> name+'('+str(decls)+')'+'\n'+stmts
         | ['max' [] [:n :v]] -> 'max '+n+' = '+str(v)+'\n'
         | ['def' [expr:name expr:val] :b] -> 'def '+str(b)+name+'='+val+'\n'
         | ['cabac_fn' [expr:bin statement('  '):s] [:name :init]] -> 'cabac_fn '+name+':'+str(init)+','+bin+'='+s+'\n'
         | :s -> 'unknown_fn('+str(s)+')'

statement :indent = ['COVERCLASS' [statement(indent+'  '):s] [:visT :visF] ] ?(visF==None) -> "{}COVERCLASS({})\n{}".format(indent,visT,s)
          | ['COVERCLASS' [statement(indent+'  '):s] [:visT :visF] ] ?(visF!=None) -> "{}COVERCLASS({},{})\n{}".format(indent,visT,visF,s)
          | ['block' [statement(indent+'  ')*:s] [] ] -> indent+'{\n'+''.join(s)+indent+'}\n'
          | ['if' [expr:cond statement(indent+'  '):yes] [] ] -> indent+'if ('+cond+')\n'+yes
          | ['ifelse' [expr:cond statement(indent+'  '):yes statement(indent+'  '):no] [] ] -> indent+'if '+cond+'\n'+yes+indent+'else\n'+no
          | ['for' [expr:start expr:cond expr:iter statement(indent):s] :side ] -> indent+str(side)+'for('+start+';'+cond+';'+iter+')\n'+s
          | ['while' [expr:cond statement(indent+'  '):s] [] ] -> indent+'while('+cond+')\n'+s
          | ['do' [expr:cond statement(indent+'  '):s] [] ] -> indent+'do\n'+s+indent+'while('+cond+')\n'
          | expr:e -> indent+e+'\n'

expr = [('id'|'num'|'string') [] [anything:x]] -> str(x)
     | ['=' [expr:lhs expr:rhs] [] ] -> lhs+' = '+rhs
     | [':' [] [:raw] ] -> ':'+raw
     | ['call' [expr:a expr:ind] [] ] -> a+'('+ind+')'
     | ['arglist' [expr*:a] [] ] -> ', '.join(a)
     | ['array' [expr:a expr:ind] [] ] -> a+'['+ind+']'
     | ['init' [expr*:a] [] ] -> '{'+','.join(a)+'}'
     | ['COVERCLASS' [expr:e] [:visT :visF] ] ?(visF==None) -> "COVERCLASS({}, {})".format(visT, e)
     | ['COVERCLASS' [expr:e] [:visT :visF] ] ?(visF!=None) -> "COVERCLASS({}, {}, {})".format(visT, visF, e)
     | [:op [expr:a expr:b] [] ] -> '('+a+str(op)+b+')'
     | [:op [expr*:e] [anything*:side] ] -> '('+str(op)+' '+' '.join(e)+')'+('Extra{'+str(side)+'}' if len(side) else '')
     | :s -> 'unknown('+str(s)+')'

""","pprint")

