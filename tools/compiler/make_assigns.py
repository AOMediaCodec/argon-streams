################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
#
# DESCRIPTION:
#   Transform the AST into a dictionary of names->AST for RHS of assigns.
#   This is used to insert code to generate syntax elements.
#
#   We pass in a name which specifies the toplevel function to explore.
#   Function calls made within this function will override existing assings.
#
#   The intention is to allow a hierarchy of parameter settings.
#
# IMPLEMENTATION:
#   First scan functions into a dictionary
#   Then execute using the toplevel name
################################################################################

import pymeta3.grammar
pymeta3.grammar.OMeta.makeCode(r"""

start = function+:e

function = ['fn' [:body] [:name :side :ret]] -> save_fn(name,body)

execute_fnname :fn = execute_ast(fn_body[fn])

extract_id = ['id' [] [:name] ] -> name

execute_ast = ['=' [extract_id:name :rhs] :side] -> save_assign(name,rhs)
            | ['call' [extract_id:fn :a] :side] execute_fnname(fn)
            | [:op [execute_ast*] :side]

""","assigns")

