################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import argparse
import os
import re
import sys
import cPickle


remaps={
  'h265': {
       'bipredictive_merging_8_5_3_2_3':'Derivation_process_for_combined_bipredictive_merging_candidates_8_5_3_2_4',
       'h265_deblock.gen':'In_loop_filter_8_7',
       'h265_inter.gen':'Decoding_inter_prediction_mode_8_5',
       'h265_intra.gen':'Decoding_intra_prediction_mode_8_4',
       'h265_refpic.gen':'Slice_decoding_8_3',
       'h265_sao.gen':'Sample_adaptive_offset_8_7_3',
       'h265_sei.gen':'SEI payload_D_3',
       'h265_syntax_9.gen':'Syntax_7_3',
       'DPB_bump':'DPB_bump_C_5_2_4',
       'dpb_removal':'Output_and_removal_of_pictures_from_the_DPB_C_5_2_2',
       'picture_marking':'picture_marking_C_5_2_3',
       'arithmetic_decode_init':'Initialization_for_arithmetic_decode_9_3_2_5',
       'arithmetic_storage':'Storage_for_context_variables_9_3_2_3',
       'arithmetic_memorize':'Synchronization_for_context_variables_9_3_2_4',
       'compute_highest_tid_8_1':'CVSG_decoding_process_8_1_2',
       'coding_quadtree':'Coding_quadtree_7_3_8_4',
       'coding_tree_unit':'coding_tree_unit_7_3_8_2',
       'coding_unit':'coding_unit_7_3_8_5',
       'compute_qp':'compute_qp_8_6_1',
       'decode_bin':'Arithmetic_decoding_binary_decision_9_3_4_3_2',
       'decode_bypass':'Bypass_decoding_9_3_4_3_4',
       'decode_picture':'Decoding_process_for_a_coded_picture_8_1_3',
       'decode_terminate':'Decoding_before_termination_9_3_4_3_5',
       'default_sao':'set_default_values_for_sao',
       'derive_chroma_mv_8_5_3_2_9':'Derivation_process_for_chroma_motion_vectors_8_5_3_2_10',
       'derive_collocate_8_5_3_2_8':'Derivation_process_for_collocated_motion_vectors_8_5_3_2_9',
       'derive_luma_mv_merge_8_5_3_2_1':'Derivation_process_for_luma_motion_vectors_for_merge_mode_8_5_3_2_2',
       'derive_mvp_candidates_8_5_3_2_6':'Derivation_process_for_motion_vector_predictor_candidates_8_5_3_2_7',
       'derive_qp':'Derivation_quantization_parameters_8_6_1',
       'compute_qp':'Derivation_quantization_parameters_8_6_1',
       'derive_temporal_prediction_8_5_3_2_7':'Derivation_process_for_temporal_luma_motion_vector_prediction_8_5_3_2_8',
       'end_of_seq_rbsp':'End_of_sequence_RBSP_7_3_2_6',
       'entry_points':'entry_points_7_3_6_1',
       'hrd_parameters':'hrd_parameters_E_1_2',
       'mvd_coding':'Motion_vector_difference_7_3_8_9',
       'nal_unit':'General_NAL_unit_7_3_1_1',
       'nal_unit_header':'NAL_unit_header_7_3_1_2',
       'pcm_sample':'PCM_sample_7_3_8_7',
       'pic_parameter_set_rbsp':'Picture_parameter_set_RBSP_7_3_2_3_1',
       'picture_reconstruction':'picture_construction_8_6_7',
       'decode_pps_range_extensions':'Picture_parameter_set_range_extension_syntax_7_3_2_3_2',
       'pred_weight_table':'Weighted_prediction_parameters_7_3_6_3',
       'predict_mv_8_5_3_2_5':'Derivation_process_for_luma_motion_vector_prediction_8_5_3_2_6',
       'prediction_unit':'Prediction_unit_7_3_8_6',
       'profile_tier_level':'Profile_tier_level_7_3_3',
       'ref_pic_lists_modification':'Reference_picture_list_modification_7_3_6_2',
       'residual_coding':'Residual_coding_7_3_8_11',
       'sao':'Sample_adaptive_offset_7_3_8_3',
       'scaling_list_data':'Scaling_list_data_7_3_4',
       'sei_rbsp':'Supplemental_enhancement_information_RBSP_syntax_7_3_2_4',
       'access_unit_delimiter_rbsp':'Access_unit_delimiter_RBSP_syntax_7_3_2_5',
       'sei_message':'Supplemental_enhancement_information_message_7_3_5',
       'end_of_bitstream_rbsp':'End_of_bitstream_RBSP_syntax_7_3_2_7',
       'filler_data_rbsp':'Filler_data_RBSP_syntax_7_3_2_8',
       'rbsp_slice_segment_trailing_bits':'RBSP_slice_segment_trailing_bits_syntax_7_3_2_10',
       'rbsp_trailing_bits':'RBSP_trailing_bits_syntax_7_3_2_11',
       'seq_parameter_set_rbsp':'Sequence_parameter_set_RBSP_7_3_2_2_1',
       'byte_alignment':'Byte_alignment_syntax_7_3_2_12',
       'decode_sps_range_extensions':'sps_range_extension_7_3_2_2_2',
       'short_term_ref_pic_set':'Short_term_reference_picture_set_7_3_7',
       'slice_segment_data':'General_slice_segment_data_7_3_8_1',
       'slice_segment_header':'General_slice_segment_header_7_3_6_1',
       'slice_segment_layer_rbsp':'Slice_segment_layer_RBSP_7_3_2_9',
       'sub_layer_hrd_parameters':'Sub_layer_hrd_parameters_E_1_3',
       'transform_tree':'Transform_tree_7_3_8_8',
       'transform_unit':'Transform_unit_7_3_8_10',
       'video_parameter_set_rbsp':'Video_parameter_set_RBSP_7_3_2_1',
       'vui_parameters':'VUI_parameters_E_2_1',
       'cross_comp_pred':'Cross_component_prediction_syntax_7_3_8_12',
       'zero_merging_8_5_3_2_4':'Derivation_process_for_zero_motion_vector_merging_candidates_8_5_3_2_5'
       },
  'vp9': {
       'vp9_adapt_probs.gen': 'Entropy_decoding_update',
       'vp9_compressed_header.gen': 'Compressed_header',
       'vp9_dct.gen': 'Discrete_cosine_transform',
       'vp9_default_coef_probs.gen': 'Default_coefficient_probabilities',
       'vp9_dsubexp.gen': 'Subexponential_decoding',
       'vp9_entropy.gen': 'Entropy_decoding',
       'vp9_frame.gen': 'Frame',
       'vp9_init_probs.gen': 'Default_probabilities',
       'vp9_inter.gen': 'Inter_blocks',
       'vp9_inter_filters.gen': 'Interpolation_filters',
       'vp9_intra.gen': 'Intra_blocks',
       'vp9_loopfilter.gen': 'Loop_filter',
       'vp9_loopfilter_filts.gen': 'Adaptive_filtering_operations',
       'vp9_mv_predict.gen': 'Motion_vector_predictions',
       'vp9_plane.gen': 'Image_planes',
       'vp9_quant.gen': 'Dequantization',
       'vp9_syntax_9.gen': 'Syntax',
       'vp9_transform.gen': 'Inverse_transform',
       'vp9_uncompressed_header.gen': 'Uncompressed_header',
       'decode_subexp': 'decode_term_subexp',
       'swap_frame_buffers': 'update_ref_slots',
       'read_sync_code': 'vertify_sync_code', # Deliberate spelling mistake which is present in the Google spec, arrgh
       'frame_size_with_refs': 'read_frame_size_from_refs',
       'read_display_size': 'read_display_frame_size',
       'read_tiles': 'get_tile_columns',
       'read_inter_mode_probs': 'forward_update_inter_mode',
       'read_switchable_interp_probs': 'forward_update_switchable',
       'read_intra_inter_probs': 'forward_update_intra_inter',
       'read_y_mode_probs': 'forward_update_intra_modes',
       'read_comp_pred': 'forward_update_compound',
       'read_partition_probs': 'forward_update_partition',
       'read_mv_probs': 'forward_update_mv',
       'backward_update_inter_intra': 'backward_update_inter_inter',
       'adapt_mv_probs': 'backward_update_mv',
       'decode_modes_sb': 'decode_partition',
       'read_skip_coeff': 'read_block_skip',
       'read_skip_probs': 'forward_update_skip',
       'read_coef_probs': 'forward_update_coeff',
       'read_is_inter_block': 'read_block_is_inter',
       'read_block_references': 'read_ref_frames',
       'read_comp_pred': 'forward_update_reference_mode',
       'update_mv_prob': 'update_prob',
       'use_mv_hp': 'vp9_use_mv_hp',
       'scaled_x': 'scale_x',
       'scaled_y': 'scale_y',
       'convolve_horiz_c': 'convolve_horiz',
       'convolve_vert_c': 'convolve_vert',
       'convolve_c': 'convolve',
       'append_sub8x8_mvs_for_idx': 'vp9_append_sub8x8_mvs_for_idx',
       'read_intra_frame_mode_info': 'read_intra_frame_block_header',
       'read_inter_frame_mode_info': 'read_inter_frame_block_header',
       'read_inter_block_mode_info': 'read_inter_frame_inter_block_header',
       'read_intra_block_mode_info': 'read_inter_frame_intra_block_header',
       'adapt_coef_probs': 'backward_update_coeff',
       'adapt_transform_coef_probs': 'backward_update_coeff_impl',
       'read_tx_probs': 'forward_update_tx',
       'idct4_1d': 'idct4',
       'idct8_1d': 'idct8',
       'idct16_1d': 'idct16',
       'idct32_1d': 'idct32',
       'iadst4_1d': 'iadst4',
       'iadst8_1d': 'iadst8',
       'iadst16_1d': 'iadst16',
       'read_quantization': 'read_quantization_params',
       'get_qindex': 'get_block_quant_base',
       'decode_coefs': 'dequantize_tx_block',
       'inverse_transform_block': 'inverse_transform_tx_block',
       'predict_and_reconstruct_intra_block': 'predict_and_reconstruct_intra_tx_block',
       'predict_and_reconstruct_inter_block': 'reconstruct_inter_tx_block',
       'decode_modes_b': 'decode_block',
       'make_filter_mask_filter': 'filter_mask',
       'make_filter_mask_hev': 'hev_mask',
       'read_loopfilter': 'read_loop_filter',
       'diff_update_prob': 'DIFF',
       },
  'vp8': {},
  'av1b': {
          'av1b_arithmetic.gen':       'Arithmetic_decoding',
          'av1b_cdef.gen':             'CDEF',
          'av1b_cfl.gen':              'Chroma_from_luma',
          'av1b_decodeframe.gen':      'Frame_decoding',
          'av1b_inter.gen':            'Inter_decoding',
          'av1b_intra.gen':            'Intra_decoding',
          'av1b_loop_restoration.gen': 'Loop_restoration',
          'av1b_loopfilter.gen':       'Loop_filter',
          'av1b_mathfunctions.gen':    'Math_functions',
          'av1b_output.gen':           'Output',
          'av1b_palette.gen':          'Palette',
          'av1b_recon.gen':            'Reconstruction',
          'av1b_syntax.gen':           'Syntax',
          'av1b_tables.gen':           'Tables',
          'av1b_transform.gen':        'Transform',
          'av1b_upscaling.gen':        'Upscaling'
       }
}

# This is a list of customer names for whom we've generated a release with
# special coverage behaviour. When we're generating a release version of the
# coverage tool, we want to get rid of these names, otherwise they'll
# eventually appear in the visibilityConditionTrue and visibilityConditionFalse
# entries in the XML. Eugh.
#
# The 'CLIENT' name is to make stuff work after someone has run
# strip-clients.sh on the source tree.
CUSTOMERS = ['CLIENT', 'CLIENT_A', 'CLIENT_B', 'CLIENT_C', 'CLIENT_D']

# CUSTOMER_REGEX will match any disallowed customer names (set up in main())
CUSTOMER_REGEX = None

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Code to construct equations
#
eqn = 0
current_eqn_description=['']
def new_equation_number(d):
  global eqn,part
  eqn+=1
  part=0
  current_eqn_description[0]=d

current_eqn=[('', None)]
def set_eqn(e):
  """Called when we have found an explicit definition of an equation"""
  global eqn
  current_eqn[0]=(e)
  #eqn=0

def set_multiple_eqn(e):
  current_eqn[0]=(e[0], e[2][2][0], e[3][2][0], e[4])

part = 0
def new_part_number():
  global part
  part+=1

START_SYMBOL = None

def make_str(s):
  return ['string',[],[s]]

def make_num(s):
  return ['num',[],[s]]

def make_idx(n):
  '''Make an index to use for a call to profile_func

  This index will be PROFILE_POINTS_<THIS_FILE> + n'''
  assert START_SYMBOL
  return ['+', [START_SYMBOL, make_num(n)], []]


def splat_visibility(visT, visF):
    if visT is None:
        assert visF is None
        return (None, None)

    # If CUSTOMER_REGEX is not None, it matches any customer names. So stuff
    # like CLIENT_A_LEVEL5 will get mapped here to '0'.
    if CUSTOMER_REGEX is not None:
        visT = CUSTOMER_REGEX.sub('0', visT)
        if visF is not None:
            visF = CUSTOMER_REGEX.sub('0', visF)

    return (visT, visF)


coverclass_stack = [("1","1")];
def coverclass_push(visT, visF):
    """Register the expressions below should be identified with the given category.

    Categories are pushed onto a stack so only the deepest category is
    reported.

    """
    if visT is None:
        return

    coverclass_stack.append(splat_visibility(visT, visF))


def coverclass_pop():
  """This is called when we have finished profiling a category and goes back up the stack."""
  coverclass_stack.pop()

class BranchPoint:
  def __init__(self, name, low, high, step, visT, visF):
    self.name=name
    self.low=low
    self.high=high
    self.step=step
    self.visT=visT
    self.visF=visF


def isInRange(rangeStr, v):
  if rangeStr==None:
    return True
  for rp in rangeStr.split(","):
    if "-" in rp:
      rpBits=rp.partition("-")
      rpLow=int(rpBits[0].strip())
      rpHigh=int(rpBits[2].strip())
      if rpLow<=v and v<=rpHigh:
        return True
    else:
      if v==int(rp):
        return True
  return False

point_idx=0
pointList=[]
rangePointIdxes={}


def is_invisible(visT, visF, stack):
  '''Check if visT, visF are both '0' or there's such a pair on the stack.'''
  zeros = ('0', '0')
  zero_none = ('0', None)
  return (visT, visF) == zeros or zeros in stack or zero_none in stack


# s,f are defined at the bottom of this file and s[:f] is the name of the source file.
def make_profile(expr,r,t,force=False,visT=None,visF=None):
  """force can be used to make sure that this is profiled even if it only contains a fixed value.
  This is used for the toplevel of equations to ensure they appear in the report."""

  (visT, visF) = splat_visibility(visT, visF)

  # If anything on the coverclass stack is has zero for visibility on both
  # sides, we know that we're never going to care about this, so we don't
  # actually need to generate profiling code.
  if is_invisible(visT, visF, coverclass_stack):
    return expr

  if expr[0]=='id':
    if expr[2][0] in id_to_array_dimensionality and id_to_array_dimensionality[expr[2][0]]>0:
      return expr
    if expr[2][0] in id_to_type:
      typeStr=id_to_type[expr[2][0]]
      if 'pointer' in typeStr:
        return expr
  if (r[0]==r[1] and not force):
    return expr # This value only ever takes a single value so no point profiling it.
  new_part_number()
  file_name = filename_base
  if file_name in remap:
    file_name = remap[file_name]
  fn_name = current_fn[0]
  if fn_name in remap:
    fn_name = remap[fn_name]
  p = ['id',[],['profile']]

  if not visT:
    (visT,visF) = coverclass_stack[-1]

  visFStr='"{}"'.format(visF) if visF!=None else "NULL"

  if type(current_eqn[0]) is str:
    description = file_name+';'+fn_name+';'+current_eqn[0]+';'+str(eqn)+':     '+current_eqn_description[0]+';'+str(part)+';'+t
    point_idx = len(pointList)
    alist = ['arglist',[make_idx(len(pointList)),expr],[]]

    pointList.append(BranchPoint(description,r[0],r[1],r[2],visT,visFStr))
  else:
    rangeKey=(current_eqn[0][0].rpartition('[')[2][:-1], part)
    pointPtr=rangePointIdxes[rangeKey] if rangeKey in rangePointIdxes else len(pointList)
    description = file_name+';'+fn_name+';REQN_PLACEHOLDER'+str(pointPtr)+';'+str(eqn)+':     '+current_eqn_description[0]+';'+str(part)+';'+t
    alist = ['arglist',[make_idx(pointPtr),expr],[]]
    if pointPtr==len(pointList):
      pointPtr=None
      rangePointIdxes[rangeKey]=len(pointList)
    for k in xrange(current_eqn[0][1], current_eqn[0][2]+1):
      if pointPtr!=None:
        if isInRange(current_eqn[0][3], k):
          pointList[pointPtr+k-current_eqn[0][1]].visT=visT
          pointList[pointPtr+k-current_eqn[0][1]].visF=visFStr
      else:
        name=current_eqn[0][0][:-1]+str(k)+']'
        pointList.append(BranchPoint(re.sub("REQN_PLACEHOLDER[0-9]*", name, description), r[0],r[1],r[2],visT, visFStr))
  #alist = ['arglist',[make_str(s[:f]+'.'+str(eqn)+'.'+str(part)),expr],[]]

  return ['call',[p,alist],[]]

def replace_placeholder(orig, e, start):
  l=None
  if type(orig) is list:
    l=orig
  elif type(orig) is tuple:
    l=list(orig)
  if l==None:
    return orig
  if len(l)<3 or l[0]!='string' or not l[2][0].startswith("REQN_PLACEHOLDER"):
      if type(orig) is list:
        return [replace_placeholder(x, e, start) for x in orig]
      else:
        return tuple(replace_placeholder(x, e, start) for x in orig)

  baseIdx=int(l[2][0][len("REQN_PLACEHOLDER"):])-start
  return ['array', [ ('id', [], ['allProfilerPoints_'+points_name]), ['+', [ ['num', [], [baseIdx]], e ], []] ], []]

def make_eqn_withexpr(s, e):
  rangeEqnExpr=e[1]
  rangeEqnStart=e[2][2][0]
  return replace_placeholder(s,rangeEqnExpr, rangeEqnStart)

def make_covercross(xs):
  """Return a list of expressions containing expr,low,high,step for each argument

  xs is a list of expr,range for each argument.
  """
  A=[]
  for expr,r in xs:
    A.append(expr)
    for a in r:
      A.append(make_num(a))

  return A

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Code to work out the range of expressions
#
# Ranges are min,max,step where min and max are inclusive
# step of -1 means the number is a power of 2

def error(str):
  print str
  assert(False)


def new_function():
  global local_id_to_type,ind,local_id_explicit_range
  local_id_to_type = {}
  local_id_explicit_range = set() # Mark which ids have an explicit Range command meaning we ignore the definitions in parameters
  ind = -1

def element(name,style,num):
  if name=='dummy':
    return 0,0,1
  if style=='u' or style=='f' or style=='b':
    if name not in id_to_type:
      r = range_type('uint'+str(num))
      if name in local_id_to_type and local_id_to_type[name]!=r:
        print name,r,local_id_to_type[name]
        assert name not in local_id_to_type
      local_id_to_type[name] = r
  elif style=='ue':
    return
  elif style=='se':
    return
  else:
    print 'Unknown style: '+style

def next_element(name,style,num):
  if style=='u' or style=='f' or style=='b':
    r = range_type('uint'+str(num))
    save_next_range(name,r)

def add_range(name,rs):
  """Define the range for the given id.

  The range is expanded to be able to hold the first two arguments, and has the smallest value of the third argument as the step size"""
  r = list(range_expand(rs[0],rs[1]))
  if len(rs)==3:
    r[2] = rs[2][0] # minimum value for the step
  else:
    r[2] = 1
  local_id_to_type[name] = r
  local_id_explicit_range.add(name)

def save_range(name,r):
  if name in local_id_explicit_range:
    return
  local_id_to_type[name] = r

def make_assert(name):
  r = local_id_to_type[name]
  r_step=r[2]
  art = ['id',[],['assert']]
  x = ['id',[],[name]]
  low=['>=',[x,make_num(r[0])],[]]
  high=['<=',[x,make_num(r[1])],[]]
  cond=['&&',[low,high],[]]
  if (r_step==-1):
    a = ['-',[x,make_num(1)],[]]
    b = ['&',[x,a],[]]
    c = ['==',[b,make_num(0)],[]]
    cond = ['&&',[cond,c],[]]
  args='arglist',[cond],[]
  return ['call',[art,args],[]]

def make_assert_nonzero(name):
  art = ['id',[],['assert']]
  x = ['id',[],[name]]
  args='arglist',[x],[]
  return ['call',[art,args],[]]

fn_to_range = {}
def save_call(fname,args):
  """Record how this function is used.

  @param args contains a list of ranges for each argument used"""
  global fn_to_range
  if fname in logged_functions:
    if len(args)>0:  # Don't care about order for functions with no arguments
      print "WARNING: Function %s called out of order.  Move the function below the call in order to be able to deduce ranges for parameters."%fname
    logged_functions.remove(fname)
  if fname not in fn_to_range:
    fn_to_range[fname] = args
  else:
    fn_to_range[fname] = [range_expand(a,b) for a,b in zip(args,fn_to_range[fname])]
  #print fname,args,local_id_to_type

logged_functions=set()
current_fn=['']
def log_function(fname):
  """Keep track of which functions are called so we can spot if invocations are out of order"""
  logged_functions.add(fname)
  current_fn[0] = fname
  set_eqn('start...')

def range_expand(a,b):
  """Returns worst case out of two ranges"""
  stp=min(a[2],b[2])
  if stp<0 and (a[2]>0 or b[2]>0):
    stp=1 # adding power of 2 to something else, TODO make this even more accurate?
  return min(a[0],b[0]),max(a[1],b[1]),stp

def get_ind():
  """Return index of next parameter in current function"""
  global ind
  ind +=1
  return ind

def range_id(name):
  if name in local_id_to_type:
    return local_id_to_type[name]
  if name not in id_to_type:
    #print 'Unknown id: '+name
    t = 'int32'
  else:
    t=id_to_type[name]
  return range_type(t)

def range_type(t):
  unsigned=t.startswith('u')
  if unsigned:
    t=t[1:]
  if '_' in t:
    T=t.split('_')
    if len(T)==4:
      return [int(t) for t in T[1:]]
    return int(T[1]),int(T[2]),1
  if t=='int': t='int32'
  if t.endswith('pointer'):
    bottom=0
    top=(1<<64)-1
    step=1
  elif t.startswith('int'):
    num_bits=int(t[3:])
    top=(1<<num_bits)
    bottom=0
    if not unsigned:
      half=top>>1
      bottom-=half
      top-=half
    top-=1
    step=1
  else:
    error('Unknown type:'+t)
  return bottom,top,step

def range_op(op,a,b):
  """Combine the ranges for a binary operator"""
  a_min,a_max,a_step=a
  b_min,b_max,b_step=b
  if a_step<=0: a_step = 1
  if b_step<=0: b_step = 1
  if op=='+':
    r_min = a_min+b_min
    r_max = a_max+b_max
    r_step = min(a_step,b_step)
  elif op=='-':
    r_min = a_min-b_max
    r_max = a_max-b_min
    r_step = min(a_step,b_step)
  elif op=='?':
    r_min = min(a_min,b_min)
    r_max = max(a_max,b_max)
    r_step = min(a_step,b_step)
  elif op=='*':
    L=[a_min,a_max]
    if a_min<=0<=a_max:
      L.append(0)
    R=[b_min,b_max]
    if b_min<=0<=b_max:
      R.append(0)
    r_min = min(x*y for x in L for y in R)
    r_max = max(x*y for x in L for y in R)
    r_step = a_step*b_step
  elif op=='<<':
    b_min = min(31,max(0,b_min))
    b_max = min(31,max(0,b_max)) # Avoid getting enormous numbers!
    r_min = min(x<<y for x in [a_min,a_max] for y in [b_min,b_max])
    r_max = max(x<<y for x in [a_min,a_max] for y in [b_min,b_max])
    r_step = a_step<<b_min
    if a_min==a_max==1:
      r_step = -1 # Mark step as being a power of 2
  elif op=='>>':
    b_min = max(0,b_min)
    r_min = min(x>>y for x in [a_min,a_max] for y in [b_min,b_max])
    r_max = max(x>>y for x in [a_min,a_max] for y in [b_min,b_max])
    r_step = max(1,a_step>>b_max)
  elif op=='/':
    L=[a_min,a_max]
    if a_min<=1<=a_max:
      L.append(1)
    if a_min<=-1<=a_max:
      L.append(-1)
    R=[b_min,b_max]
    if b_min<=1<=b_max:
      R.append(1)
    if b_min<=-1<=b_max:
      R.append(-1)
    r_min = min(x/y for x in L for y in R if y)
    r_max = max(x/y for x in L for y in R if y)
    r_step = 1 # May be able to improve this, but probably not helpful
  elif op=='%':
    r_min = 0
    r_max = b_max-1
    r_step= 1
  elif op=='|':
    # Assume that the numbers are unsigned, and find number of bits needed to express both sides
    n=max(compute_bits(x) for x in [a_max,b_max])
    return range_type('uint'+str(n))
  elif op=='&':
    # After an &, the answer must be <= each operand
    assert(a_max>=0)
    assert(b_max>=0)
    r_max=min(a_max,b_max)
    return 0,r_max,1
  elif op in ['<','<=','==','!=','>=','>','&&','||']:
    return 0,1,1
  elif op == '^':
    n=compute_bits(max(a_max,b_max))
    return 0,2**n-1,1
  else:
    error('Unknown operator: '+op)
  high=1<<40
  r_min=max(r_min,-high)
  r_max=min(r_max,high-1)
  r_step=min(high,r_step)
  return r_min,r_max,r_step

def compute_bits(m):
  """Return number of bits needed to express value m"""
  b=0
  while (1<<b)<=m:
    b+=1
  return b

def range_call(op,args):
  if op=='Abs':
    assert len(args)==1
    r_min,r_max,r_step=args[0]
    return 0,max(abs(r_min),abs(r_max)),1
  if op=='CeilLog2':
    assert len(args)==1
    r_min,r_max,r_step=args[0]
    return compute_bits(r_min),compute_bits(r_max),1
  if op=='Sign':
    return -1,1,1
  elif op=='Min':
    assert len(args)==2
    a_min,a_max,a_step = args[0]
    b_min,b_max,b_step = args[1]
    if a_step==-1: a_step=1
    if b_step==-1: b_step=1
    return min(a_min,b_min),min(a_max,b_max),min(a_step,b_step)
  elif op=='Max':
    assert len(args)==2
    a_min,a_max,a_step = args[0]
    b_min,b_max,b_step = args[1]
    if a_step==-1: a_step=1
    if b_step==-1: b_step=1
    return max(a_min,b_min),max(a_max,b_max),min(a_step,b_step)
  elif op=='Log2':
    r_min,r_max,r_step=args[0]
    return 0,compute_bits(r_max),1
  elif op=='Clip3':
    a_min,a_max,a_step = args[0]
    b_min,b_max,b_step = args[1]
    return a_min,b_max,1
  elif op=='Clip1Y':
    return 0,255,1 # TODO fix if bitdepth changes!
  elif op=='Clip1C':
    return 0,255,1 # TODO fix if bitdepth changes!
  elif op=='next_bits':
    assert len(args)==1
    a_min,a_max,a_step = args[0]
    return 0,2**a_max-1,1
  elif op=='CeilDiv':
    assert len(args)==2
    # (a+b-1)//b
    # For the moment, we assume that b!=0
    a_min,a_max,a_step = args[0]
    b_min,b_max,b_step = args[1]
    assert b_min>0
    return (a_min+b_max-1)//b_max,(a_max+b_min-1)//b_min,1
  elif op=='current_position':
    return range_type('int32')
  elif op in ['isBLA','isCRA','isShortTerm','isIRAP','GetOutput','deblock_mv_check','isIDR','isRASL','more_data_in_payload','payload_extension_present']:
    return range_type('uint1')
  elif op=='compute_longterm':
    return range_type('uint8') # Actually 0->14 plus 128->128+14
  elif op=='LongTermRefPic':
    return 0,128,128 # Either 0 or 128
  elif op=='DiffPicOrderCnt':
    return range_type('int16')
  elif op=='GetMark':
    return 0,3,1
  elif op=='GetPicLatencyCount':
    return 0,2**31-1,1
  elif op=='PicOrderCnt':
    return range_type('int32')
  elif op=='PicOrderCntLsb':
    return range_type('uint16')
  elif op in ['AllocatePicture','generate_picture_8_3_3_2']:
    return 0,16,1

  return range_type('int32')

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Code to handle the ranges of local variables
#

def is_local(name):
  """Returns true if the name corresponds to a local identifier or a parameter"""
  if name in local_id_to_type:
    return True
  return name not in id_to_type

def save_next_range(name,r):
  """Called for every assignment to name with range r"""
  #print name,r,is_local(name)
  if not is_local(name):
    return
  if name in local_id_explicit_range:
    return
  if name not in next_local_id_to_type:
    next_local_id_to_type[name] = r
  else:
    next_local_id_to_type[name] = range_expand(next_local_id_to_type[name],r)
  #print name,r,next_local_id_to_type[name]

def clear_locals():
  global ind,next_local_id_to_type
  ind = -1
  next_local_id_to_type={}
  for id in local_id_explicit_range:
    next_local_id_to_type[id] = local_id_to_type[id]
  #print local_id_to_type

def prepare_locals():
  """Update the local dictionary of ranges"""
  global local_id_to_type
  local_id_to_type = next_local_id_to_type
  #print local_id_to_type


def main():
    # Reads <base>.gen.simd and <base>.gen.struct. Writes <base>.gen.profile
    # and <base>.gen.points.

    parser = argparse.ArgumentParser()
    parser.add_argument('--release', action='store_true')
    parser.add_argument('--customer')
    parser.add_argument('codec')
    parser.add_argument('base')

    args = parser.parse_args()

    if args.customer is not None:
        if args.customer not in CUSTOMERS:
            raise RuntimeError('Unknown customer: {}'.format(args.customer))
        if not args.release:
            raise RuntimeError('Setting customer without --release.')

    if args.release:
        bad_customers = [c for c in CUSTOMERS if c != args.customer]

        global CUSTOMER_REGEX
        CUSTOMER_REGEX = re.compile(r'[_A-Z0-9]*(?:' +
                                    '|'.join(bad_customers) +
                                    r')[_A-Z0-9]*')

    global remap
    remap = remaps[args.codec]

    simd_path = args.base + '.gen.simd'
    struct_path = args.base + '.gen.struct'

    with open(simd_path, 'rb') as in_fd:
        A = cPickle.load(in_fd)

    global id_to_type
    global id_to_array_dimensionality

    with open(struct_path, 'rb') as in_fd:
        struct_data = cPickle.load(in_fd)
        assert len(struct_data) == 5

        id_to_type = struct_data[3]
        id_to_array_dimensionality = struct_data[4]

    global filename_base
    filename_base = os.path.basename(args.base)

    # This is the symbol that we'll use for the start of the coverage points
    # for this file.
    points_name = 'PROFILE_POINTS_' + filename_base.upper()

    global START_SYMBOL
    START_SYMBOL = ('id', [], [points_name])

    B = profile(A).apply("start")[0]

    points_path = args.base + '.gen.points'
    with open(points_path, 'wb') as points_fd:
        for i, p in enumerate(pointList):
            point_format_string = []
            for point_limit in [p.low, p.high, p.step]:
                if point_limit == -(1 << 63):
                    point_format_string.append('(%dLL-1),' % (point_limit+1))
                elif point_limit >= 1 << 63:
                    point_format_string.append('%dLL,' % ((1 << 63)-1))
                else:
                    point_format_string.append('%dLL,' % point_limit)
            point_format_string = ' '.join(point_format_string)
            print >>points_fd, ('  { "%s", %s "%s", %s }, // %d' %
                                (p.name, point_format_string,
                                 p.visT, p.visF, i))

    profile_path = args.base + '.gen.profile'
    with open(profile_path, 'wb') as profile_fd:
        cPickle.dump(B, profile_fd, 2)

    return 0


if __name__ == '__main__':
    sys.exit(main())
