################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

"""
Python to be appended to generated code
"""
import cPickle,sys

def add_to_dict(D,name,val):
  D[name]=val
  return D

def disp(a):
  print a

assert sys.argv[-2]=='-o'
with open(sys.argv[-3],'rb') as fd:
    A=cPickle.load(fd)

assert sys.argv[-5]=='--builddir'
builddir=sys.argv[-4]

assert sys.argv[-7]=='--codec'
codec = '/'+sys.argv[-6]

with open(builddir+codec+'_extra.gen.extra','rb') as fd:
    extra_fns,limits,rewrites=cPickle.load(fd)

B=merge_extra(A).apply("start")[0]

with open(sys.argv[-1],'wb') as out:
    cPickle.dump(B,out,2)
