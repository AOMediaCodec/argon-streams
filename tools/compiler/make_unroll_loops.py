################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> e

function = ['fn' [expr:xs] :side ] -> 'fn',[xs],side
         | anything

id = ['id' [] [:x]] -> x

num = ['num' [] [:x]] -> x

basic_cond :i = ['<' [id:a num:n] []] ?(a==i and n==1)

basic_init = ['=' [id:i num:n] []] ?(n==0) -> i

basic_sum :i = ['+' [id:a num:n] []] ?(n==1 and a==i)

basic_inc :i = ['=' [id:a basic_sum(i)] []] ?(a==i)

expr    = ['for' [~~expr:init basic_init:i ~~expr:cond basic_cond(i) ~~expr:inc basic_inc(i) :body] [] ] -> 'block',[init,body,inc],[]
        | [:op [expr*:a] :side ] -> op,a,side
        | :x -> disp('unroll_loops error'+str(x))

""","unroll_loops","pure_ast")

