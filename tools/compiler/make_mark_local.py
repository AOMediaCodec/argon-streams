################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> e

decl = [:t :name] -> name

getdecls = ['fn' [:xs] [:name [decl*:decls] :ret] ] -> decls

function =  ~~getdecls:D ['fn' [expr2(D):xs] :side ] -> 'fn',[xs],side
         | :notfn -> notfn

expr2 :D = ['id' [] [:name] ?(name in D) ] -> 'paramid',[],[name]
         | [:op [expr2(D)*:xs] :side] -> (op,xs,side)


""","mark_local","pure_ast")

