################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

# This simply removes any SIMD constructs from the AST

pymeta3.grammar.OMeta.makeCode(r"""

start = statement+:t -> t

statement = ['for' [statement*:xs] :v] -> 'for',xs,[]
          | [:op [statement*:xs] :side] -> op,xs,side

""","remove_simd","pure_ast")

