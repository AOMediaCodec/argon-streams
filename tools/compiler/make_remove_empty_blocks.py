################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar
# For Python we need to make ++ and -- into separate statements if they live within assignments

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> e

function = ['fn' [expr1:xs] [:name :side :ret] ] -> 'fn',[xs],[name,side,ret]
         | :notfn -> notfn

empty_block = [ 'block' [empty_block*] [] ]

nonempty_block = empty_block* expr1:a empty_block* -> a

expr_list = [nonempty_block*:xs] -> xs

expr1 = ['block' expr_list:xs []] -> xs[0] if (len(xs)==1 and xs[0][0][:2]!='if') else ('block',xs,[])
     | [:op [expr1*:xs] anything:side] -> op,xs,side

""","remove_empty_blocks","pure_ast")

