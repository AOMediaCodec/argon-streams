################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

"""
make_store_extra.py

Generates a parser that takes an abstract syntax tree and extracts max and def statements.
"""
import pymeta3.grammar

# Most elements are returned verbatim except:
#   defines of arrays have some identifiers substituted with constants
#   rewrite rules are removed, and stored separately

# rewrite returns list of [name of array,id1,id2] which specifes which ids should be replaced.

store_extra = r"""
start = function+:e

function = ['fn' [expr:xs] [:name anything*] ] -> store_fn(name,xs)
         | ['max' [] [:n :x]] -> store_max(n,x)

idname = ['id' [] [:x]] -> x

rewrite = ['array' [rewrite:a idname:x] [] ] -> a+[x]
        | idname:x -> [x]

expr = ['def' [def:d] :side ] -> 'def',[d],side
     | ['rewrite' [rewrite:a expr:b] []] -> store_rewrite(a,b)
     | [:op [expr*:xs] :side] -> op,xs,side

def = ['array' [def:a index:ind] [] ] -> 'array',[a,ind],[]
    | [:op [def*:xs] :side] -> op,xs,side

index = ['num' [] [anything:x]]:a -> a
      | ['id' [] [anything:x]]:a -> ['num', [], [limits[x]]] if x in limits else a
      | ['ind_noexpr' [] []]
      | [:op [index*:xs] :side] -> op,xs,side

"""

pymeta3.grammar.OMeta.makeCode(store_extra,"store_extra")

