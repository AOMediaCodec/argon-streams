################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

"""
make_syntax.py

Generates a parser that takes a preprocessed text file and produces an abstract syntax tree.

 Section 5.9
   Syntax elements a_b_c
   Derived variables aBc (mixture of lower and upper, no underscores)
   Abc variable can be used in later syntax elements
   aBC variable is local
   a_b are reuse of syntax elements (always possesses an _)

"""
import pymeta3.grammar

# If not parsing, try sticking debug after "statement =" to log the start of each line as it goes past

# Try spotting the 0 first in order to get more helpful parse errorss
syntax_grammar="""
start2 = function+:fns '\0' -> fns

start = functions

functions = '\0' -> []
          | function:f functions:fns -> [f]+fns


opt_init = name:init #, -> init
         | ( #, )? -> None

fn_arg = name:type name:n -> type,n
       | name:n -> 'int',n

function = call_qualifier*:q name:n #( list('fn_arg')?:args #) statement:s spaces -> 'fn',[s],[n,args,'int']
         | call_qualifier*:q name:ret name:n #( list('fn_arg')?:args #) statement:s spaces -> 'fn',[s],[n,args,ret]
         | ( #max | #min ) name:n spaces num:d spaces -> 'max',[],[n,int(d)]
         | ( #max | #min ) name:n name:d spaces -> 'max',[],[n,d]
         | name:n #: opt_init:init postfix_expression:bin #= ~~#{ statement:s spaces -> 'cabac_fn',[bin,s],[n,init]
         | name:n #: opt_init:init postfix_expression:bin #= expr:e #; spaces -> 'cabac_fn',[bin, ['=',[ ['id',[],['ctxIdxOffset']] , e  ],[]]  ],[n,init]
         | definition:e spaces -> e

name = spaces letter:a letterOrDigit*:xs -> a+''.join(xs)

coverclass_expr_seg = #( coverclass_expr_seg:x coverclass_expr_seg*:y #) -> '('+x+''.join(y)+')'
                    | :x ?(x!=')') -> x

coverclass_expression = spaces #( spaces coverclass_expr_seg:x coverclass_expr_seg*:y #) -> x+''.join(y)
                      | name:x -> x
                      | spaces '1' -> '1'
                      | spaces '0' -> '0'

list:p = apply(p):a (#, list(p))*:ps -> [a]+(ps[0] if len(ps)>0 else [])
       | -> []

debug = (~~(spaces name:a))? -> disp(str(a))

statement = #COVERCLASS #( coverclass_expression:vis #) statement:s -> 'COVERCLASS',[s],[vis,None]
          | #COVERCLASS #( coverclass_expression:visT #, coverclass_expression:visF #) statement:s -> 'COVERCLASS',[s],[visT,visF]
          | #{ statement*:s #} -> 'block',s,[]
          | rewrite
          | definition
          | control
          | element
          | assignment
          | raw_line
          | rule_invocation


digit = :d ?('0'<=d<='9') -> d

num = digit+:d -> ''.join(d)

string = #" (~'"' anything)*:a '"' -> ''.join(a)

not_bracket = (~')' ~'(' anything)*:a -> ''.join(a)

my_python_expr = not_bracket:a '(' my_python_expr:p ')' not_bracket:b -> a+'('+p+')'+b
               | not_bracket:a -> a

num_or_eval = num
            | '(' my_python_expr:p ')' -> str(eval(p))

known_types = ~~name:typ ( (#int num) | #int | (#uint num) | #UChar ) ~letterOrDigit -> typ
            | (#uint | #int ):base '_' num_or_eval:a '_' num_or_eval:b '_' num_or_eval:c -> base+'_'+a+'_'+b+'_'+c
            | (#uint | #int ):base '_' num_or_eval:a '_' num_or_eval:b  -> base+'_'+a+'_'+b

qualified_type = (#autostruct )?:autos (#static )? (#const )?:const known_types:type -> type,autos,const
               | (#addstruct ) (#static )? (#const )?:const name:type ~letterOrDigit -> type,'autostruct',const

init_expression = #{ init_expression*:e #} ( #, )?  -> 'init',e,[]
                | expr:e ( #, )?  -> e


definition = qualified_type:type spaces postfix_expression:e #= init_expression:init #; -> 'def',[e,init],[type]
           | qualified_type:type spaces postfix_expression:e -> 'def',[e],[type]

rewrite = #rewrite postfix_expression:e postfix_expression:r -> 'rewrite',[e,r],[]

element = #autostruct ~~unary_expression:n element:e -> 'block',[ ('def',[n],[['int','autostruct']]),e],[]
        | unary_expression:n variable_op:op #( expr:e #) -> 'element',[n,['num',[],[0]]],[op]
        | unary_expression:n descriptor_op:op #( expr:e #) -> 'element',[n,e],[op]

variable_op =  #ue | #se

descriptor_op =  #u | #b | #f | #tu | #i | #ae | #su | #s | #le | #B

style = 'P' | 'C' | -> 'P'

raw_line = #: style:style (~'\r' ~'\n' anything)*:xs -> ':',[],[''.join(xs),style]

rule_invocation = postfix_expression

assignment_list = list("assignment"):a -> a[0] if len(a)==1 else ('block',a,[])

hint = #SIMD #( name:v #) -> [v]
     | -> []

replace = [:e :v replace_expr(e,v):x] -> x

replace_expr [:e :v] = anything:x ?(x==e) -> v
                     | [:op [replace_expr(e,v)*:xs] :side] -> op,xs,side

bake [:e :v :s] = ?(len(v)==1) replace(e,v[0],s)
                | replace(e,v[0],s):a bake(e,v[1:],s):b -> 'ifelse',[ ('==',[e,v[0]],[]) , a, b] ,[]

control = #if #( expr:e #) statement:s #else statement:s2 -> 'ifelse',[e,s,s2],[]
        | #if #( expr:e #) statement:s -> 'if',[e,s],[]
        | hint:h #for #( assignment_list:a #; expr:cond #; assignment_list:b #) statement:body -> 'for',[a,cond,b,body],h
	| #while #( expr:cond #) statement:s -> 'while',[cond,s],[]
	| #do statement:s #while #( expr:cond #) -> 'do',[cond,s],[]
  | #BAKE #( expr:e #, list("expr"):v #) statement:s bake(e,v,s)
  | #EQN #( not_bracket:a #) #( CONSTANT:start #, CONSTANT:end #, not_bracket:rng #) #( expr:e #) statement:s -> ['EQN',[s],[a,e,start,end,rng]]
  | #EQN #( not_bracket:a #) #( CONSTANT:start #, CONSTANT:end #) #( expr:e #) statement:s -> ['EQN',[s],[a,e,start,end,None]]
  | #EQN #( not_bracket:a #) statement:s -> 'EQN',[s],[a, None, None, None, None]
  | #ASSERT #( expr:e #, string:s ( #, )? argument_expression_list:args #) -> 'ASSERT',[e,args],[s]
  | #CHECK #( expr:e #, string:s ( #, )? argument_expression_list:args #) -> 'CHECK',[e,args],[s]
  | #CSV #( name:n #, list("expr"):v #) -> 'CSV',v,[n]
  | #COVERCROSS #( name:n #, list("expr"):args #) -> 'COVERCROSS',args,[n]
	| #return expr:e -> 'return',[e],[]
	| #return -> 'return',[],[]
  | #break -> 'break',[],[]
  | #continue -> 'continue',[],[]
  | #alloc postfix_expression:e -> 'alloc',[e],[]
  | #free primary_expression:e -> 'free',[e],[]

assignment = unary_expression:n assignment_operator:op expr:e -> op,[n,e],[]
           | postfix_expression


assignment_operator = #= | #*= | #/= | #%= | #+= | #-= | #<<= | #>>= | #&= | #^= | #|=

expr = conditional_expression
conditional_expression =
              logical_or_expression:test #? expr:res1 #: conditional_expression:res2 -> '?',[test,res1,res2],[]
            | logical_or_expression
cast_expression = unary_expression
unary_expression = unary_operator:op cast_expression:e -> "unary"+op,[e],[]
	             | postfix_expression

unary_operator = #* | #+ | #- | #~ | #!

argument_expression_list = list("expr"):e -> 'arglist',e,[]

call_qualifier = #autostruct

postfix_expression =
          postfix_expression:e #[ expr:ind #] -> "array",[e,ind],[]
        | postfix_expression:e #[ spaces #] -> "array",[e,['ind_noexpr', [], []]],[]
        | call_qualifier+:q primary_expression:e #( argument_expression_list:a #) -> funcall(e, a, q)
        | postfix_expression:e #( argument_expression_list:a #) -> funcall(e, a, None)
        | postfix_expression:e #++ ->  "post++",[e],[]
	| postfix_expression:e #-- ->  "post--",[e],[]
	| primary_expression

primary_expression = CONSTANT
	| #( expr:e #) -> e
  | #COVERCLASS #( coverclass_expression:visT #, spaces coverclass_expression:visF #, spaces expr:e #) ->  'COVERCLASS',[e], [visT,visF]
  | #COVERCLASS #( coverclass_expression:vis #, spaces expr:e #) ->  'COVERCLASS',[e], [vis,None]
  | #alloc -> 'alloc',[],[]
	| id

id = name:n -> 'id',[],[n]

CONSTANT = spaces digit:a letterOrDigit*:n -> 'num',[],[eval(a+''.join(n))]

"""
binary_operators = [ ('multiplicative_expression','*','/','%'),
                     ('additive_expression','+','-'),
                     ('shift_expression','<<','>>'),
                     ('relational_expression','<','>','<=','>='),
                     ('equality_expression','==','!='),
                     ('and_expression','&'),
                     ('exclusive_or_expression','^'),
                     ('inclusive_or_expression','|'),
                     ('logical_and_expression','&&'),
                     ('logical_or_expression','||') ]
s=''
for i,cmd in enumerate(binary_operators):
    name=cmd[0]
    ops=cmd[1:]
    s+=name+' = '
    base = 'cast_expression' if i==0 else binary_operators[i-1][0]
    s+='\n | '.join(name+":left token('"+op+"'):op "+base+":right -> op,[left,right],[]" for op in ops)
    s+='\n | '+base+'\n'
syntax_grammar+=s

pymeta3.grammar.OMeta.makeCode(syntax_grammar,"syntax")

