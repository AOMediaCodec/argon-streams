################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import cPickle
import sys
from pymeta3.runtime import _MaybeParseError


def disp(a):
    print(a)
    return a


# These are things that you might generate a function call to if you got your
# syntax wrong, but which should have been parsed as a specialised expression
# or statement.
_BAD_FUNS = {'COVERCLASS'}


def funcall(to_call, args, qualifier):
    '''Make a function call'''
    if to_call[0] == 'id':
        name = to_call[2][0]
        if name in _BAD_FUNS:
            raise RuntimeError('Call to function with name `{}\': '
                               'this is almost certainly a syntax error.'
                               .format(name))

    return ('call',
            [to_call, args],
            [qualifier] if qualifier is not None else [])


assert sys.argv[-2] == '-o'
with open(sys.argv[-3]) as fd:
    A = fd.read()
try:
    S = syntax(A+'\0')
    B, Berr = S.apply("start")
except _MaybeParseError as e:
    print(e)
    e = S.currentError
    print(e)
    pos = e[0]
    extra = 200
    start = max(0, pos - extra)
    end = min(len(A), pos + extra)
    line = A.count('\n', 0, pos)
    print(A[start:pos]+'\x1b[1;31m Parse error here! \x1b[0m'+A[pos:end])
    print()
    print("Error on line {} of {}".format(line + 1, sys.argv[-3]))
    print()
    raise e


with open(sys.argv[-1], 'wb') as out:
    cPickle.dump(B, out, 2)
