################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import cPickle
import argparse
import cfg
import re

C_SYM_RE = re.compile('^[a-zA-Z_][a-zA-Z_0-9]*$')


# Replace calls to ae with arithmetic decode logic. Note that function calls
# with multiple arguments place all arguments in a list.
def make_id(i):
    return 'id', [], [i]


def make_num(i):
    return 'num', [], [i]


def mk_assignments(name, a, via_syntax):
    '''Generate the list of assignments for a bitstream element

    These assignments set `a' (the syntax tree element representing the left
    hand side of the assignment) to the result of the expression in
    CABAC_ENCODE[name]. If via_syntax is true, the result looks something like:

        syntax = some_complicated_formula;
        a = syntax;

    As a special case, if name has no entry in CABAC_ENCODE, we just end up
    with:

        syntax = a;

    If via_syntax is false, we assume that name has an entry and we just write

        a = some_complicated_formula;

    In either case, it's possible that the given bitstream element can be
    overridden at runtime. In this case, the "some_complicated_formula
    assignment" looks more like:

       syntax = get_cfg_syntax(123, cfg_syntax) ?
                  cfg_syntax[0] :
                  some_complicated_formula;

    and this might be preceded by some lines setting entries in cfg_syntax to
    local variable values.

    '''
    if not via_syntax:
        assert name in CABAC_ENCODE

    # rhs is the value that will be assigned to syntax for name if not
    # overridden by the config file.
    rhs = CABAC_ENCODE.get(name, a)

    cfg_idx, group = CFG_ELEMENTS.get(name, (None, None))
    if cfg_idx is not None:
        # This means that the value can be overridden by the config file. We
        # want to update RHS to be
        #
        #    get_cfg_syntax(123, cfg_syntax) ? cfg_syntax[0] : old_rhs
        rhs = ('?',
               [('call',
                 [make_id('get_cfg_syntax'),
                  ['arglist', [make_num(cfg_idx), make_id('cfg_syntax')], []]],
                 []),
                ('array', [make_id('cfg_syntax'), make_num(0)], []),
                rhs],
               [])

    # It's also possible that there is a list of local variables that need
    # storing into cfg_syntax. This flat_array acts as an inout parameter
    # for get_cfg_syntax: on the way in, it holds locals; on the way out,
    # it's first element holds the value.
    set_locals = []
    if group is not None:
        local_vars = CFG_GROUPS.get(group, [])
        for idx, var in enumerate(local_vars):
            # As a belt and braces check, make sure that each var is a valid C
            # symbol.
            if not C_SYM_RE.match(var[1]):
                raise RuntimeError("Local variable `{}' not a valid C symbol."
                                   .format(var[1]))
            set_locals.append(('=',
                               [('array',
                                 [make_id('cfg_syntax'), make_num(idx)], []),
                                make_id(var[1])],
                               []))

    assignment_tgt = make_id('syntax') if via_syntax else a
    main_assignment = ('=', [assignment_tgt, rhs], [])
    late_assignments = []
    if via_syntax and name in CABAC_ENCODE:
        late_assignments = [('=', [a, make_id('syntax')], [])]

    return set_locals + [main_assignment] + late_assignments


def insert_cabac_fn(name, a, x):
    """For decode, return AST to call tree x and then assign a with the resulting value of syntax.  Also sets base to the appropriate value.

    For encode, return AST to set syntax to current contents of a, then set base and call tree x.
    If the id is present in the ENCODE dictionary, then it will also prepend code to set a to a generated value.
    """
    ctxName = CABAC_CONTEXT[name][2]
    base = '=',[make_id('ctxIdxTable'),make_num(CABAC_TO_CONTEXT[ctxName])],[]
    if is_encode:
        return ('block', mk_assignments(name, a, True) + [base, x], [])
    else:
        # Generate code like:
        #
        #    ctxIdxTable = 0;
        #    <contents of x>
        #    foo = syntax;
        #    <logging code>
        log = ':',[],['b.log("{}", "ae(v)", syntax)'.format(name),'P']
        syn = 'call',[make_id('syntax_elem'), ['arglist',[make_id('syntax')],[]]],[]
        z = '=',[a,syn],[]
        return 'block',[base,x,z,log],[]


def insert_noncabac_fn(name,a):
    """Return a AST that sets "a" to the value in the CABAC_ENCODE dictionary"""
    assignments = mk_assignments(name, a, False)
    return (assignments[0]
            if len(assignments) == 1
            else ('block', assignments, []))


def make_assign(params,args):
    """Return a block that assigns each parameter with associated argument"""
    if len(params)!=len(args):
        print params
        print args
    assert len(params)==len(args)
    return 'block',[['=',[make_id(p),a],[]] for p,a in zip(params,args)],[]

def check(name):
  assert name!='diff_update'

def debug(b):
  print repr(b)
  assert False


def slurp(filename):
    '''Load a file with cPickle'''
    with open(filename, 'rb') as handle:
        return cPickle.load(handle)


parser = argparse.ArgumentParser()
parser.add_argument('--codec', required=True)
parser.add_argument('--builddir', required=True)
parser.add_argument('--output', '-o', required=True)
parser.add_argument('--cfg-elements', dest='cfg_elts', default=None)
parser.add_argument('--cfg-groups', dest='cfg_groups', default=None)
parser.add_argument('--release', default=False, type=int)
parser.add_argument('--assigns')
parser.add_argument('infile')

args = parser.parse_args()


builddir = args.builddir
is_encode = args.assigns is not None
codec_pfx = builddir + '/' + args.codec

A = slurp(args.infile)
CABAC_CONTEXT, _ = slurp(codec_pfx + '_cabac.gen.cabac')

bin_name = codec_pfx + ('_encode_binarization.gen.cabac'
                        if is_encode else '_binarization.gen.cabac')

_, CABAC_BINARY = slurp(bin_name)

CABAC_ENCODE = slurp(args.assigns) if args.assigns else {}
CABAC_TO_CONTEXT, CABAC_INIT, NUM_CONTEXTS = \
    slurp(builddir + '/ContextTables.gen.cabacinit')

CFG_GROUPS, elements, _, _ = \
    cfg.read(args.cfg_groups, args.cfg_elts, None, None, args.release)

# elements is keyed by the codec name, with values (idx, sname, grp). We
# actually need a dictionary keyed by the source name with values (idx, grp).
CFG_ELEMENTS = {}
for idx, sname, grp in elements.values():
    CFG_ELEMENTS[sname] = (idx, grp)

# Run everything through pymeta3
B = insert_cabac(A).apply("start")[0]

# Dump the main output
with open(args.output, 'wb') as out:
    cPickle.dump(B, out, 2)
