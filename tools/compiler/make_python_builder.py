################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> ''.join(e)

decl = [:t :name] -> name
function = ['fn' [statement('  '):xs] [:name [decl*:decls] :ret] ] -> make_fn(name,xs,decls)
         | statement(''):s -> s

arrayname = justname
          | ['array' [arrayname:a :ind] []] -> a

arraydims = justname -> []
           | ['alloc' [] [] ] -> ['alloc']
           | ['array' [ arraydims:a expr:dim] [] ] -> a+[dim]

statement :indent = ['COVERCLASS' [statement(indent):s] :side] -> s
          | ['block' [statement(indent)*:s] [] ] -> (''.join(s) if len(s) > 0 else indent+'pass\n')
          | ['if' [expr:cond statement(indent+'  '):yes] [] ] -> indent+'if ('+cond+'):\n'+yes
          | ['ifelse' [expr:cond statement(indent+'  '):yes statement(indent+'  '):no] [] ] -> indent+'if '+cond+':\n'+yes+indent+'else:\n'+no
          | ['for' [statement(indent):start expr:cond statement(indent+'  '):iter statement(indent+'  '):s] [] ] -> start+indent+'while('+cond+'):\n'+s+iter+'\n'
          | ['while' [expr:cond statement(indent+'  '):s] [] ] -> indent+'while('+cond+'):\n'+s
          | ['EQN' [statement(indent):s] :side ] -> s
          | ['ASSERT' [expr:e expr:args] ?(not args) [:msg] ]  -> indent+'ASSERT('+e+',"'+msg+'")\n'
          | ['ASSERT' [expr:e expr:args] [:msg] ]  -> indent+'ASSERT('+e+',"'+msg+'",' + args + ')\n'
          | ['CHECK' [expr:e expr:args] ?(not args) [:msg] ]  -> indent+'CHECK('+e+',"'+msg+'")\n'
          | ['CHECK' [expr:e expr:args] [:msg] ]  -> indent+'CHECK('+e+',"'+msg+'",' + args + ')\n'
          | ['COVERCROSS' :e :n ] -> indent+'pass\n'
          | ['CSV' :e [:n] ] -> indent+'pass\n'
          | ['do' [expr:cond statement(indent+'  '):s] [] ] -> indent+'while(1):\n'+s+indent+'  if not ('+cond+'): break\n'
          | ['def' [ arrayname:a expr:init ] [:typ]] -> indent+a+' = '+init+'\n'
          | ['def' [ ~~arraydims:d ?(len(d)) ?(all(x=='' for x in d)) arrayname:a ] [:typ]] -> indent+a+' = None\n'
          | ['def' [ ~~arraydims:d ?(len(d)) arrayname:a ] [:typ]] -> indent+a+' = '+make_dict(d)+'\n'
          | ['def' [expr:e] [:typ]] -> ''
          | ?(is_encode) ['element' [expr:a expr:w] [:style]] -> indent+'b.write_'+style+'('+a+','+w+',"'+str(a)+'")\n'
          | ?(not is_encode) ['element' [expr:a expr:w] [:style]] -> indent+a+' = b.read_'+style+'('+w+',"'+str(a)+'")\n'
          | ['break' [] [] ] -> indent+'break\n'
          | ['continue' [] [] ] -> indent+'continue\n'
          | ['return' [expr:a] []] -> indent+'return '+a+'\n'
          | ['return' [] []] -> indent+'return\n'
          | ['call' [justname:a ?(a in builtin_statements) expr:ind] :side ] -> a+'('+ind+')\n'
          | ['call' [~~dataname:d fnname:a expr:ind] :side ] -> indent+d+' = '+a+'( b, '+ind+')\n'
          | ['alloc' [~~arraydims:d arrayname:a] [] ] -> indent+a+' = ' + make_dict(d) + '\n'
          | ['free' [justname:a] [] ] -> indent+a+' = None\n'
          | expr:e -> indent+e+'\n'

anyid = 'id' | 'paramid' | 'localid'

fnname = [anyid [] [anything:x]] -> 'hevc_parse_'+x
dataname = [anyid [] [anything:x]] -> 'p.'+x
justname = [anyid [] [anything:x]] -> x

expr = ['num' [] [anything:x]] -> str(x)
     | ['id' [] [anything:x]] -> x
     | ['localid' [] [anything:x]] -> x
     | ['paramid' [] [anything:x]] -> x
     | ['ind_noexpr' [] []] -> ''
     | ['=' [expr:lhs arraydims:a ?(len(a)>1 and a[0]=='alloc')] [] ] -> lhs+' = ' + make_dict(a[1:])
     | [('=' | '+=' | '-=' | '/=' | '*=' | '%=' | '&=' | '^=' | '|=' ):op [expr:lhs expr:rhs] [] ] -> lhs+' '+op+' '+rhs
     | [':' [] [:raw :style] ] -> raw if style=='P' else ''
     | ['call' [ justname:a ?(a=='ChooseAll') expr:ind] :side ]  -> make_ChooseAll(ind)
     | ['call' [ justname:a ?(a in builtin_functions) expr:ind] :side ] -> 'b.'+a+'('+ind+')'
     | ['call' [fnname:a expr:ind] :side ] -> a+'( b, '+ind+')'
     | ['array' [expr:a expr:ind] [] ] -> a+'['+ind+']'
     | ['init' [expr*:a] [] ] -> '['+','.join(a)+']'
     | ['arglist' [expr*:a] [] ] -> ', '.join(a)
     | [('post++' | 'post--'):op [expr:a] [] ] -> a+op[4]+'='+'1'
     | ['unary-' [expr:a] [] ] -> '(-'+a+')'
     | ['unary~' [expr:a] [] ] -> '(~'+a+')'
     | ['unary!' [expr:a] [] ] -> '( not '+a+')'
     | ['||' [expr:a expr:b] [] ] -> '('+a+' or '+b+')'
     | ['&&' [expr:a expr:b] [] ] -> '('+a+' and '+b+')'
     | ['?' [expr:test expr:res1 expr:res2] [] ] -> '('+res1+' if '+test+' else '+res2+')'
     | ['/' [expr:a expr:b] [] ] -> 'cdivide('+a+','+b+')'
     | [:op [expr:a expr:b] [] ] -> '('+a+str(op)+b+')'
     | ['COVERCLASS' [expr:e] :side] -> e
     | [:op [expr*:e] [anything*:side] ] -> '('+str(op)+' '+' '.join(e)+')'+('Extra{'+str(side)+'}' if len(side) else '')
     | :s -> 'unknown('+str(s)+')'

""","python_builder")
# Note the division needs to round towards zero, not -infinity
