################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
#
# DESCRIPTION:
# Base file for make_assigns.py
################################################################################

import cPickle,sys

fn_body={}
def save_fn(name,body):
  """Store a pointer to the AST for the named function in the dictionary fn_body"""
  fn_body[name] = body

assigns_dict={}
def save_assign(name,rhs):
  """Store a pointer to this assignment in the dictionary assigns_dict."""
  assigns_dict[name] = rhs

assert sys.argv[-2]=='-o'

assert sys.argv[-4]=='-p'
toplevel_fnlist = sys.argv[-3].split(",")

with open(sys.argv[-5],'rb') as fd:
    A=cPickle.load(fd)

assignsInstance=assigns(A)
assignsInstance.apply("start")[0]
for fn in sys.argv[-3].split(","):
  assignsInstance.apply("execute_fnname", fn)

with open(sys.argv[-1],'wb') as out:
    cPickle.dump(assigns_dict,out,2)
