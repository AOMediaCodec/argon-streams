################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e

decl = [:t :name]
function = ['fn' [~~defined_ids([defined_scalars,defined_arrays]):defined_ids ~~local_ids(set()):local_ids statement] [:name [decl*:decls] :ret] ] -> make_fn(name,local_ids,decls,ret)
         | defines:s

control_op = 'block' | 'for' | 'do' | 'while' | 'if' | 'ifelse' | 'EQN'

defines = ['def' [ ~~arraydims:d arrayname:a def_expr:init ] [:typ] ?('autostruct' not in typ) ]
          | ['def' [ ~~arraydims:d arrayname:a ] [:typ] ?('autostruct' not in typ)]
          | [control_op [defines*:e] :side]
          | [:op :e :side]

def_local_name :S = ['localid' [] [anything:x]] !((S[1] if S[2] else S[0]).add((x, S[3]) if S[2] else x))
                               | ['array' [def_local_name(S[:2]+[1, S[3]+1]):n :dim] :side]

debug = ~~anything:a !(disp(a))

defined_ids :S = ['def' [ def_local_name(S+[0, 0]):nm def_expr?] :side ]:a -> S
                  | [:op [defined_ids(S)*] :side] -> S

loc_local_name :S = ['localid' [] [anything:x]] !(S.add(x))
                  | ['array' [loc_local_name(S):n :dim] :side]

local_ids :S = loc_local_name(S) -> S
             | [:op [local_ids(S)*] :side] -> S

arrayname = justname
          | ['array' [arrayname:a :ind] []]

arraydims = justname
           | ['array' [ arraydims:a def_expr:dim] [] ]

sub_statement = 'block' [statement*:s] []
          | 'if' [expr:cond statement:yes] []
          | 'ifelse' [expr:cond statement:yes statement:no] []
          | 'for' [statement:start expr:cond statement:iter statement:s] []
          | 'while' [expr:cond statement:s] []
          | 'do' [expr:cond statement:s] []
          | 'EQN' [statement:s] :side
          | 'ASSERT' [expr:e] [:msg]
          | 'CHECK' [expr:e expr:args] ?(not args) [:msg]
          | 'CHECK' [expr:e expr:args] [:msg]
          | 'CSV' [expr*:e] [:n]
          | 'COVERCROSS' :a :b
          | 'def' def_expr:a :side
          | 'def' :a :side
          | 'element' [assign_lhs_expr:a expr:w] [:style]
          | 'break' [] []
          | 'continue' [] []
          | 'return' [] []
          | 'return' [expr:a] []
          | 'call' [~~dataname:d fnname:a func_arglist:ind] [:q] ?('autostruct' in q)
          | 'call' [fnname:a func_arglist:ind] :side
          | 'COVERCROSSCALL' [expr*:xs] [:n]
          | 'alloc' [expr:e] []
          | 'free' [expr:e] []

statement = ['COVERCLASS' [statement:s] :side]
          | [sub_statement:s]
          | expr:e

anyid = 'id' | 'paramid' | 'localid'
fnname = [anyid [] [anything:x]]
dataname = [anyid [] [anything:x]]
justname = [anyid [] [anything:x]]

int16s = 'int16x4' | 'int16x8'

int32s = 'int32x2' | 'int32x4'

expr = xexpr([0,0])

assign_lhs_expr = xexpr([1,0])

readassign_lhs_expr = xexpr([2,0])

func_arg = xexpr([3,0])

def_expr = xexpr([4,0])

xexpr :expr_param = [sub_expr(expr_param):e]
                              | :s

check_simd  = ~~( :op :exprs [ 'int32x4' | 'int32x2' | 'int16x8' | 'int16x4' | 'uint8x16' | 'uint8' | 'int32' | 'int32x4' | 'int' | 'int16'] )

func_arglist = ['arglist' [func_arg*:a] []] -> a

sub_expr :expr_param = 'num' [] [anything:x]
                        | 'id' [] [anything:x]
                        | 'string' [] [anything:x]
                        | 'localid' [] [anything:x] !(add_local_read_write(expr_param, x))
                        | 'paramid' [] [anything:x]
                        | '=' [assign_lhs_expr:lhs expr:rhs] []
                        | ('+=' | '-=' | '/=' | '*=' | '%=' | '&=' | '^=' | '|=' ):op [readassign_lhs_expr:lhs expr:rhs] []
                        | ':' [] [:raw :style]
                        | 'call_unused' [ justname:a ?(a=='profile') func_arglist:ind] :side
                        | 'call' [ justname:a ?(a=='profile') func_arglist:ind] :side
                        | 'call' [ justname:a ?(a=='ChooseAll') func_arglist:ind] :side
                        | 'call' [ justname:a ?(a in builtin_functions) func_arglist:ind] :side
                        | 'call' [fnname:a func_arglist:ind] :side
                        | 'array' [xexpr([expr_param[0], expr_param[1]+1]):a expr:ind] []
                        | 'init' [expr*:a] []
                        | 'arglist' [expr*:a] []
                        | ('post++' | 'post--'):op [expr:a] []
                        | 'unary-' [expr:a] []
                        | 'unary~' [expr:a] []
                        | 'unary!' [expr:a] []
                        | '?' [expr:test expr:res1 expr:res2] []
                        | check_simd '<<scalar' [expr:a expr:b] [int16s]
                        | check_simd '>>scalar' [expr:a expr:b] [int32s]
                        | check_simd '>>scalar' [expr:a expr:b] [int16s]
                        | check_simd '*' [expr:a expr:b] [int16s]
                        | check_simd '*' [expr:a expr:b] [int32s]
                        | check_simd '+' [expr:a expr:b] [int16s]
                        | check_simd '+' [expr:a expr:b] [int32s]
                        | 'vector' [expr:a] [int16s]
                        | 'vector' [expr:a] [int32s]
                        | 'vector_store' [expr:lhs expr:rhs] ['uint8x16' 'uint8']
                        | 'vector_store' [expr:lhs expr:rhs] ['int32x4' 'int32']
                        | 'vector_store' [expr:lhs expr:rhs] ['int32x4' 'int']
                        | 'vector_store' [expr:lhs expr:rhs] ['int16x4' 'int16']
                        | 'vector_store' [expr:lhs expr:rhs] ['int16x8' 'int16']
                        | 'vector_store' [expr:lhs expr:rhs] ['int32x4' 'int16']
                        | 'vector_store' [expr:lhs expr:rhs] ['int16x8' 'uint8']
                        | 'vector_load' [expr:e] ['uint8x16' 'uint8']
                        | 'vector_load' [expr:e] ['int16x8' 'int16']
                        | 'vector_load' [expr:e] ['int32x4' 'int']
                        | 'vector_load' [expr:e] ['int16x8' 'int32']
                        | 'vector_load' [expr:e] ['int16x4' 'int16']
                        | 'vector_load' [expr:e] ['int16x4' 'uint8']
                        | 'vector_load' [expr:e] ['int16x8' 'uint8']
                        | 'vector_load' [expr:e] ['int32x4' 'int16']
                        | :op [expr:a expr:b] []
                        | 'COVERCLASS' [expr:e] :side
                        | :op [expr*:e] [anything*:side]


""","check_locals")

