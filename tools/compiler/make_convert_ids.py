################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

# This code converts ids based on the target language
#
# It uses id_name to work out the correct way of accessing objects within a given function.
# This works based on the loaded structure file which has information about the hierarchy.

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> e

decl = [:t :name] -> name

getfnname = ['fn' [:xs] [:name :side :ret] ] -> name

function =  ~~getfnname:D ['fn' [expr(D):xs] :side ] -> 'fn',[xs],side
         | :notfn -> notfn

def_expr :D = ['id' [] [anything:x]] -> def_name(D,x)
        | [:op [def_expr(D)*:xs] :side] -> (op,xs,side)

expr :D = ['id' [] [anything:x]] -> id_name(D,x)
        | ['call' [ :a  expr(D):ind] :side ] -> 'call',[a,ind],side
        | ['def' [def_expr(D)*:a] :side] -> 'def',a,side
        | [:op [expr(D)*:xs] :side] -> (op,xs,side)

""","convert_ids")

