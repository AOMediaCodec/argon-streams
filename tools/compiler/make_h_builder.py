################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = ~~(prototype+:p) function+:e -> ''.join(e[::-1])+''.join(p)

prototype = ['fn' :xs [:name [decl*:decls] :ret] ] -> make_fn(name,decls,ret)
          | :s -> ''

decl = [:t :name] -> t,name
function = ['fn' [statement:xs] [:name [decl*:decls] :ret] ] -> make_fn2(name,xs,decls,ret)
         | ['def' [ ~~arraydims:d ?(d=='') arrayname:a anything? ] [:typ] ] -> 'extern '+typ[0]+' '+a+';\n'
         | ['def' [ ~~arraydims:d arrayname:a anything? ] [:typ] ] -> 'extern flat_array<'+typ[0]+','+str(d.count('['))+'> '+a+';\n'
         | statement:s -> s

arrayname = justname
          | ['array' [arrayname:a :ind] []] -> a

arraydims = justname -> ''
           | ['array' [ arraydims:a expr:dim] [] ] -> a+'['+dim+']'

control_op = 'block' | 'for' | 'do' | 'while' | 'if' | 'ifelse' | 'EQN'

statement = ['COVERCLASS' [statement:s] :side] -> s
          | ['def' [ ~~arraydims:d arrayname:a expr:init ] [:typ] ?('autostruct' in typ) ] -> '  '+convert_type(typ[0])+' ALIGN16 '+a+d+' = '+init+';\n'
          | ['def' [ ~~arraydims:d arrayname:a ] [:typ] ?('autostruct' in typ)] -> '  '+convert_type(typ[0])+' ALIGN16 '+a+d+';\n'
          | ['def' :a :b] -> ''
          | ['call' [justname:j expr:ind] [:q] ?('autostruct' in q)] -> '  '+j.upper()+'_T '+j+';\n'
          | [control_op [statement*:e] :side] -> ''.join(e)
          | [:op :e :side] -> ''
          | expr:e -> ''

fnname = ['id' [] [anything:x]] -> 'hevc_parse_'+x
dataname = ['id' [] [anything:x]] -> '&p->'+x
justname = ['id' [] [anything:x]] -> x

expr = ['num' [] [anything:x]] -> str(x)
     | ['id' [] [anything:x]] -> x
     | ['ind_noexpr' [] []] -> ''
     | ['localid' [] [anything:x]] -> x
     | ['paramid' [] [anything:x]] -> x
     | ['array' [expr:a expr:ind] [] ] -> a+'['+ind+']'
     | ['init' [expr*:a] [] ] -> '{'+','.join(a)+'}'
     | ['unary-' [expr:a] [] ] -> '(-'+a+')'
     | ['unary!' [expr:a] [] ] -> '(!'+a+')'
     | ['?' [expr:test expr:res1 expr:res2] [] ] -> '('+'('+test+')?'+res1+' : '+res2+')'
     | [:op [expr:a expr:b] [] ] -> '('+a+str(op)+b+')'
     | ['COVERCLASS' [expr:e] :side] -> e
     | [:op [expr*:e] [anything*:side] ] -> '('+str(op)+' '+' '.join(e)+')'+('Extra{'+str(side)+'}' if len(side) else '')
     | :s -> 'unknown('+str(s)+')'

""","h_builder")

