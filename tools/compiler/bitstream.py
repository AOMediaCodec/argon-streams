################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This class contains the builtin functions and tables for the final parser
# It is also used during constant folding
import random
import math
import pdb

builtin_functions = ('record profile_cross option_level option_force_tiles '
                     'option_tier option_framecnt option_targetausize '
                     'option_maxbitrate_fudgefactor option_width '
                     'option_height count_data_decode count_data '
                     'switch_to_data switch_to_header combine_header_and_data '
                     'ChooseSpecial ChoosePowerLaw ChooseBiasHigh ChooseBit '
                     'ChooseSignedBits ChooseSignedBits2 Choose ChooseBits '
                     'ChooseAll Sign Abs Clip1C Clip1Y CeilLog2 Log2 Ceil '
                     'Clip3 CeilDiv InverseRasterScan current_position '
                     'overwrite overwritebit addcarry needs_byte_extensions '
                     'current_bitposition more_rbsp_trailing_data next_bits '
                     'more_rbsp_data payload_extension_present '
                     'payload_extension_size byte_aligned Min Max profile '
                     'Sqrt FloorLog2 set_pos start_nest copy_from_nest '
                     'end_nest copy_to_buffer copy_from_buffer unread_bits '
                     'set_bool_length syntax_elem check_md5s get_cfg_syntax '
                     'peek_last_bytes '
                     'CheckStride SetArray get_msb'
                     .split())

builtin_statements = ('profile_disable profile_enable start_md5 end_md5 '
                      'set_pos Copy2D'
                      .split())
# We only use myrand when checking that C and Python can generate the same streams
use_myrand = False
def start_md5():
  pass
def end_md5():
  pass
def set_use_myrand(val):
  global use_myrand
  use_myrand = val

class Bitstream(object):
    count=0

    def __init__(self,data,logging=False):
        self.data=data
        self.pos=0  # Current byte being read offset from start of NAL
        if len(self.data)>0:
          self.num=self.data[self.pos]
        self.bitpos=7 # Current bit position in the byte (starts at 7 and goes down to 0)
        self.logging = logging
        self.log_count = 20 if logging else 0
        self.chunk_end = -1

    def set_pos(self, pos):
      assert pos <= len(self.data)
      self.bitpos = 7
      self.pos = pos
      if pos < len(self.data):
        self.num = self.data[self.pos]
      else:
        self.num = 0
      return 0

    def set_bool_length(self, sz):
      self.chunk_end = (self.pos + sz) if (sz > 0) else -1
      assert ((self.chunk_end == -1) or (self.chunk_end <= len(self.data)))
      return 0

    def count_data_decode(self,pos):
        """Count the number of bytes (including emulation prevention bytes) from pos to here"""
        assert self.bitpos == 7
        numzeros = 0
        num = 0
        for di in range(pos,self.pos):
          d=self.data[di]
          if (numzeros==2 and d<=3):
            num+=1
            numzeros=0
          if d==0:
            numzeros += 1
          else:
            numzeros = 0
          num +=1
        return num

    def numbytes(self):
        return len(self.data)

    def current_position(self):
        return self.pos

    # VP9 extends streams if the last byte has has 110x_xxxx pattern
    def needs_byte_extensions(self):
        return self.pos>0 and self.data[self.pos-1]&0xe0 == 0xc0

    def current_bitposition(self):
        return (self.pos<<3)+(7-self.bitpos)

    def unread_bits(self, n):
        """Move the bitstream pointer back n bits (or to the start of the stream, if n > current_bitposition())"""
        full_bitpos = self.current_bitposition()
        adj_bitpos = max(0, full_bitpos - n)
        self.bitpos = 7 - (adj_bitpos & 7)
        self.pos = adj_bitpos >> 3
        self.num = self.data[self.pos]

    def bitstream_peek_last_bytes(self, size, out):
      for i in range(size):
        out[i] = self.data[self.pos - (size-1) + i]
      return 0

    def readbit(self):
        x=(self.num>>self.bitpos)&1
        if self.bitpos>0:
            self.bitpos-=1
        else:
            self.bitpos=7
            if self.pos<len(self.data)-1:
                self.pos+=1
                assert ((self.chunk_end == -1) or (self.chunk_end <= len(self.data)))
                self.num=self.data[self.pos]
            else:
                print 'byte out of range'
                pdb.set_trace()
                pass
        return x

    def next_bits(self,num):
        a=self.pos
        b=self.num
        c=self.bitpos
        x=self.read_f(num)
        self.pos=a
        self.num=b
        self.bitpos=c
        return x

    def more_rbsp_trailing_data(self):
        return self.pos<len(self.data)-1

    def more_rbsp_data(self):
        """Return True unless got to the stop bit in rbsp_trailing_bits"""
        if self.pos<len(self.data)-1:
            return True
        t=0
        for i in xrange(self.bitpos+1):
            if self.num&(1<<i):
                t+=1
        return t>1 # i.e. if only spot trailing bit, then say that we have finished

    def payload_extension_present(self, startpos, payloadSize):
        """Return True unless got to the stop bit in sei_payload"""
        if self.pos-startpos<payloadSize-1:
            return True
        t=0
        for i in xrange(self.bitpos+1):
            if self.num&(1<<i):
                t+=1
        return t>1 # i.e. if only spot trailing bit, then say that we have finished

    def payload_extension_size(self, startpos, payloadSize):
        extSize=0
        #First add up everything before the last byte
        if (self.pos-startpos)<(payloadSize-1):
            #Do the partial byte first
            extSize=self.bitpos+1
            #Then do any full bytes
            if (self.pos-startpos)<(payloadSize-2):
                extSize+=(payloadSize+startpos-self.pos-2)*8
        #Now handle the last byte
        lastByte=self.data[startpos+payloadSize-1]
        for x in xrange(8):
            lastOneBit=x
            if (lastByte>>x)&1:
                break
        #If we're IN the last byte we need to take our bitpos into account
        if (self.pos-startpos)==(payloadSize-1):
            extSize=self.bitpos-lastOneBit
        #Otherwise we need to add all the available bits in the last byte
        else:
            extSize+=7-lastOneBit
        return extSize

    def log(self,sym,code,val):
        if not sym:
            return
        if sym=="bit":
            return
        if not self.logging:
            return
        #if self.log_count==0: # Only log the headers
        #    return
        self.log_count-=1
        self.count+=1
        print '%8d  %-47s %-4s : %d (%d,%d)' %(Bitstream.count,sym,code,val,self.pos,self.bitpos)
        #print '%8d  %-47s %-4s : %d' %(Bitstream.count,sym,code,val)
        #print '%-47s %-4s : %d (%d)' %(sym,code,val,self.current_bitposition())
        Bitstream.count += 1

    def read_f(self,num,sym=None):
        if num==0:
            self.log(sym,'f'+str(num),0)
            return 0
        t=0
        for i in xrange(num):
            t<<=1
            t+=self.readbit()
        self.log(sym,'f'+str(num),t)
        return t

    def read_u(self,num,sym):
        v = self.read_f(num)
        self.log(sym,'u'+str(num),v)
        return v

    def read_le(self,num,sym):
        assert(num==8 or num==16 or num==32 or num==24)
        v = self.read_f(num)
        a = v&255
        b = (v>>8)&255
        c = (v>>16)&255
        d = (v>>24)&255
        if num==16:
          v = b+(a<<8)
        elif num==24:
          v = c+(b<<8)+(a<<16)
        elif num==8:
          v = a
        else:
          v = d+(c<<8)+(b<<16)+(a<<24)
        self.log(sym,'le'+str(num),v)
        return v

    # This reads num+1 bits to give a signed quantity
    def read_s(self,num,sym):
        v = self.read_f(num)
        if self.readbit():
          v=-v
        self.log(sym,'s'+str(num),v)
        return v

    def read_su(self,num,sym):
        v = self.read_f(num+1)
        sign_bit = 1 << num
        return (v & (sign_bit - 1)) - (v & sign_bit)

    def read_b(self,num,sym):
        assert num==8
        assert self.bitpos==7
        x=self.num
        if self.pos<len(self.data)-1:
            self.pos+=1
            assert ((self.chunk_end == -1) or (self.chunk_end <= len(self.data)))
            self.num=self.data[self.pos]
        #self.log(sym,'b'+str(num),x) # This triggers rbsp logging which we want to skip
        return x

    def read_ue(self,v,sym=None):
        """Unsigned Exp-Golomb as in 9.2"""
        numzeros = 0
        while not self.read_f(1) and numzeros<32:
            numzeros += 1
        assert numzeros<32
        x = (1<<numzeros)+self.read_f(numzeros)-1
        self.log(sym,'ue',x)
        return x

    def read_se(self,v,sym):
        """Signed Exp-Golomb as in 9.2"""
        x = self.read_ue(v)
        if x==0:
            self.log(sym,'se',0)
            return x
        if x&1:
            v = (x+1)>>1
        else:
            v = -(x>>1)
        self.log(sym,'se',v)
        return v

    def read_tu(self,cMax,sym):
        """Read 111110, to give value 0->cMax"""
        x = 0
        while x<cMax:
          if self.readbit()==0:
            break
          x+=1
        self.log(sym,'tu',x)
        return x

    def byte_aligned(self):
        """True if stream is aligned"""
        return self.bitpos==7

    def CeilDiv(self,a,b):
        """Return Ceil(a/b)"""
        return (a+b-1)//b

    def Clip3(self,x,y,z):
        """Clip z to range x->y"""
        return max(min(y,z),x)

    def Clip1Y(self,x):
        return self.Clip3(0,255,x)

    def Clip1C(self,x):
        return self.Clip3(0,255,x)

    def Log2(self,a):
        b=0
        while (1<<b)<a:
            b+=1
        assert 1<<b==a
        return b

    def CeilLog2(self,a):
        b=0
        while (1<<b)<a:
            b+=1
        return b

    def Min(self,a,b):
        return min(a,b)

    def Max(self,a,b):
        return max(a,b)

    def Abs(self,a):
        return abs(a)

    def Sign(self,a):
        if a<0:
            return -1
        if a>0:
            return 1
        return 0

    def FloorLog2(self, x):
        return int(math.floor(math.log(x, 2)))

    def syntax_elem(self, v):
        return v

next_rand = 14
def myrand():
  global next_rand
  next_rand = (next_rand * 1103515245 + 12345) % 2**32
  val = (next_rand & 0x7FFFFFFF) # Extract bits 30..0
  #print val
  return val

def mysrand(seed):
  global next_rand
  next_rand = seed

class dataStruct:
    def __init__(self):
        self.num = 0 # Current data value being built up
        self.bitpos = 7 # Current bit position in the byte (starts at 7 and goes down to 0)
        self.data = []

class EncodeBitstream(Bitstream):
    count=0

    def __init__(self, glob, opts, logging=False):
        self.MAX_NESTS = 1
        self.data={}
        for i in range(self.MAX_NESTS + 1):
          self.data[i] = dataStruct()
        self.nest = 0
        self.global_data = glob
        self.logging = logging
        self.log_count = 20 if logging else 0
        self.opts = opts
        self.ChooseAllSequences={}

    def count_data(self,pos):
        """Count the number of bytes (including emulation prevention bytes) from pos to here"""
        assert self.data[self.nest].bitpos == 7
        D = self.data[self.nest].data[pos:]
        numzeros = 0
        num = 0
        for d in D:
          if (numzeros==2 and d<=3):
            num+=1
            numzeros=0
          if d==0:
            numzeros += 1
          else:
            numzeros = 0
          num +=1

        return num

    def switch_to_data(self):
        """Called when we have encoded the first part of a slice header and want to now start emitting the data.

           We use two parts in order to be able to place entry points in the correct locations."""

        self.header=self.data[self.nest].data
        self.headerbitpos=self.data[self.nest].bitpos
        self.headernum=self.data[self.nest].num
        self.data[self.nest].data = []
        self.data[self.nest].bitpos = 7
        self.data[self.nest].num = 0

    def switch_to_header(self):
        """Called when we have encoded the data and wish to finish off the header"""
        assert self.data[self.nest].bitpos==7
        self.payload = self.data[self.nest].data
        self.data[self.nest].data = self.header
        self.data[self.nest].bitpos = self.headerbitpos
        self.data[self.nest].num = self.headernum
        del self.header # prevent us from doing this twice

    def combine_header_and_data(self):
        """Called when we want to stick the data at the end of the header"""
        assert self.data[self.nest].bitpos==7
        self.data[self.nest].data += self.payload
        del self.payload # prevent us from using the payload again

    def numbytes(self):
        return len(self.data[self.nest].data)

    def current_position(self):
        return len(self.data[self.nest].data)

    def set_pos(self, pos):
        assert pos <= len(self.data[self.nest].data) and pos >= 0
        self.data[self.nest].data = self.data[self.nest].data[:pos]
        return 0

    def current_bitposition(self):
        return (len(self.data[self.nest].data)<<3)+(7-self.data[self.nest].bitpos)

    def unread_bits(self, n):
        """Move the bitstream pointer back n bits (or to the start of the stream, if n > current_bitposition())"""
        full_bitpos = self.current_bitposition()
        adj_bitpos = max(0, full_bitpos - n)

        # Save the current partial byte, in case we un-read to an earlier
        # point in the same byte
        self.data[self.nest].data.append(self.data[self.nest].num)

        self.data[self.nest].bitpos = 7 - (adj_bitpos & 7)
        new_len = adj_bitpos >> 3
        num = self.data[self.nest].data[new_len]
        self.data[self.nest].data = self.data[self.nest].data[:new_len]
        # Reload the appropriate value of 'num'.
        # Note that we need to clear out any bits which have been "un-written"
        mask = (1 << (self.data[self.nest].bitpos+1)) - 1
        self.data[self.nest].num = num & ~mask

    def byte_aligned(self):
        """True if stream is aligned"""
        return self.data[self.nest].bitpos==7

    def start_nest(self):
      assert self.data[self.nest].bitpos==7
      self.nest += 1
      assert self.nest <= self.MAX_NESTS
      self.data[self.nest] = None
      self.data[self.nest] = dataStruct()
      return 0

    def copy_from_nest(self, from_lvl, pos, sz):
      assert self.nest >= 0
      assert self.nest < from_lvl
      assert len(self.data[from_lvl].data) >= (sz + pos)
      assert self.data[self.nest].bitpos == 7
      assert self.data[self.nest].num == 0
      self.data[self.nest].data.extend(self.data[from_lvl].data[pos:pos+sz])
      return 0

    def end_nest(self):
      self.nest -= 1
      assert self.nest >= 0
      return 0

    def copy_to_buffer(self, buf, pos, sz):
      assert len(self.data[self.nest].data) >= (sz + pos)
      for i in range(sz):
        buf[i] = self.data[self.nest].data[pos+i]
      return 0

    def copy_from_buffer(self, buf, sz):
      assert self.data[self.nest].bitpos == 7
      assert self.data[self.nest].num == 0
      for i in range(sz):
        self.data[self.nest].data.append(buf[i])
      return 0

    def writebit(self,bit):
        """Bit must be 1 or 0"""
        assert 0<=bit<=1
        self.data[self.nest].num |= bit<<self.data[self.nest].bitpos
        if self.data[self.nest].bitpos>0:
            self.data[self.nest].bitpos-=1
        else:
            self.data[self.nest].bitpos=7
            self.data[self.nest].data.append(self.data[self.nest].num)
            self.data[self.nest].num=0

    def getbit(self,loc):
        """Read a bit from a given bitposition"""
        pos = loc>>3
        bitpos = (loc&7)
        bitshift = (7-bitpos)
        if pos==len(self.data[self.nest].data):
          w = self.data[self.nest].num
        else:
          w = self.data[self.nest].data[pos]
        return (w>>bitshift)&1

    def overwritebit(self,loc,bit):
        """Write <bit> at bit location <loc>"""
        pos = loc>>3
        bitpos = (loc&7)
        bitmask = 1<<(7-bitpos)
        if bit:
          if pos==len(self.data[self.nest].data):
            self.data[self.nest].num |= bitmask
          else:
            self.data[self.nest].data[pos] |= bitmask
        else:
          if pos==len(self.data[self.nest].data):
            self.data[self.nest].num &= ~bitmask
          else:
            self.data[self.nest].data[pos] &= ~bitmask

    def overwrite(self,loc,numbits,value):
        """Write numbits at bit location loc"""
        for b in range(numbits):
          self.overwritebit(loc+b,(value>>(numbits-1-b))&1)

    def addcarry(self):
        """Add a 1 to the last emitted bit, may need to propogate the carry"""
        loc = self.current_bitposition()-1
        while self.getbit(loc)==1:
          self.overwritebit(loc,0)
          loc-=1
        self.overwritebit(loc,1)

    def next_bits(self,num):
        assert False

    def more_rbsp_trailing_data(self):
        """Called at end of a slice segment to add cabac zero words to pad nal units"""
        return False # TODO should pad nal units to minimum size

    def more_rbsp_data(self):
        if self.global_data.encode_mode!=2 or not self.global_data.enc_produce_nonconformant_tests:
            return 0
        returnVal=0
        #If we're in an SEI NUT, we want to produce a maximum of 3 messages, otherwise
        #we produce a maximum of 256 bytes of extension data
        limit=256
        if self.global_data.encode_nut==39 or self.global_data.encode_nut==40:
            limit=2
        if self.global_data.enc_nonconformant_extension_count<limit:
            returnVal=random.choice([1,0])
        #Keep count of the number of times we've returned true
        if returnVal:
            self.global_data.enc_nonconformant_extension_count+=1
        else:
            self.global_data.enc_nonconformant_extension_count=0
        return returnVal

    def payload_extension_present(self, startpos, payloadSize):
        #If we're before the last byte, we must always produce a payload extension
        if (len(self.data[self.nest].data)-startpos)<(payloadSize-1):
            return 1
        #If we've only got one bit left, we mustn't produce a payload extension
        if (self.data[self.nest].bitpos==0):
            return 0
        #Otherwise we can choose
        return random.choice([True, False])

    def payload_extension_size(self, startpos, payloadSize):
        #If we're before the last byte, we must produce at least enough bits to fill up to the last byte
        minBits=0
        if (len(self.data[self.nest].data)-startpos)<(payloadSize-1):
            #Do the partial byte first
            minBits=self.data[self.nest].bitpos+1
            #Then do any full bytes
            if (len(self.data[self.nest].data)-startpos)<(payloadSize-2):
                minBits+=(payloadSize+startpos-len(self.data[self.nest].data)-2)*8
        #We need to leave at least the last bit free so the payload trailing bit can be added
        maxBits=minBits+7
        #If we're in the last byte, we can't generate all 7 bits
        if (len(self.data[self.nest].data)-startpos)==(payloadSize-1):
            maxBits-=(7-self.data[self.nest].bitpos)
        result=random.randint(minBits,maxBits)
        print "payload_extension_size: {}".format(result)
        return result

    def needs_byte_extensions(self):
        return self.data[self.nest].data[-1]&0xe0 == 0xc0

    # Legacy behaviour is signed (difference is the check)
    def write_f(self,val,num,sign=1,sym=None):
        if num==0:
            self.log(sym,'f'+str(num),0)
            return
        t=0
        assert(sign or (val <= ((1<<num)-1)))
        for i in xrange(num):
            t<<=1
            self.writebit((val>>(num-1-i))&1)
        self.log(sym,'f'+str(num),val)
        return t

    def write_u(self,val,num,sym):
        self.write_f(val,num,0)
        self.log(sym,'u'+str(num),val)

    def write_s(self,val,num,sym):
        self.write_f(abs(val),num,0)
        if val>=0:
          self.write_f(0,1,0)
        else:
          self.write_f(1,1,0) # TODO decide whether to include negative zeros?
        self.log(sym,'s'+str(num),val)

    def write_su(self,val,num,sym):
        self.write_f(val,num+1,1)
        self.log(sym,'su'+str(num),val)

    def write_le(self,v,num,sym):
        assert(num==16 or num==32 or num==24 or num==8)
        a = v&255
        b = (v>>8)&255
        c = (v>>16)&255
        d = (v>>24)&255
        if num==8:
          v = a
        elif num==16:
          v = b+(a<<8)
        elif num==24:
          v = c+(b<<8)+(a<<16)
        else: # num==32
          v = d+(c<<8)+(b<<16)+(a<<24)
        self.write_f(v,num,0)

        self.log(sym,'le'+str(num),v)
        return v

    def write_b(self,val,num,sym):
        assert num==8
        assert self.data[self.nest].bitpos==7
        self.data[self.nest].data.append(val)

    def write_ue(self,val,v,sym=None):
        """Unsigned Exp-Golomb as in 9.2"""
        # 0 zeros if <1
        # 1 zero  if <3
        # 2 zeros if <7
        # 3 zeros if <15
        # k zeros if <2**(k+1)-1
        k=0
        while val>=2**(k+1)-1:
          k+=1
        self.write_f(1,k+1,0)
        self.write_f(val-(2**k-1),k,0)
        self.log(sym,'ue',val)

    def write_se(self,val,v,sym):
        """Signed Exp-Golomb as in 9.2"""
        if val==0:
          x=0
        elif val>0:
          x=2*val-1
        else:
          x=-2*val
        self.write_ue(x,v)
        self.log(sym,'se',val)

    def write_tu(self,val,cMax,sym):
        """write 111110, to give value 0->cMax"""
        x = 0
        while x<cMax:
          if x==val:
            self.writebit(0)
            break
          self.writebit(1)
          x+=1
        self.log(sym,'tu',val)

    def rand(self,n):
        if use_myrand:
            x = myrand() & ((1<<n)-1)
            print 'b %d - %d' %(n,x)
            return x
        else:
            return random.randint(0,(1<<n)-1)

    def randfloat(self):
        "Generate a random number in [0, 1)"
        if use_myrand:
            x = myrand() / 2147483648.
            return x
        else:
            return random.random()

    def ChooseBits(self,n):
        y = 0
        shift = 0
        bits_per_rand = 31

        while n>0:
          choose = min(n, bits_per_rand)
          x = self.rand(choose)
          y |= (x << shift)
          n -= choose
          shift += choose
        return y

    def Choose(self,low,high):
        if use_myrand:
            if (low==high):
                return low
            x = low+(myrand() % (1+high-low))
            print 'c %d %d - %d'%(low,high,x)
            return x
        else:
            return random.randint(low,high)

    def ChooseSignedBits(self,n):
        if use_myrand:
            x = self.ChooseBits(n)
            if (myrand()&1):
                x = -x
            print 's %d - %d'%(n,x)
            return x
        else:
            return random.randint(-((1<<n)-1),(1<<n)-1)

    def ChooseSignedBits2(self,n):
        if use_myrand:
            return self.Choose(-((1<<n)),(1<<n)-1)
        else:
            return random.randint(-((1<<n)),(1<<n)-1)

    def ChooseBit(self):
        if use_myrand:
            x = myrand()&1
            print 'bit - %d'%(x)
            return x
        else:
            return random.randint(0,1)

    def ChooseSpecial(self,low,high):
      """Choose a number in the given range with special emphasis on values near interesting cases"""
      p=random.randint(0,99)
      tlow,thigh=low,high # Aim uniform by default
      if p<=94:
        thigh=low+128 # Aim low
      elif p<=95:
        tlow=high-10 # Aim high
      elif p<=96:
        # Choose a bitrange to target
        b=random.randint(1,self.CeilLog2(high))
        tlow=(1<<b)-10
        thigh=(1<<b)+10
      # Clip target values to allowed levels
      thigh=min(high,thigh)
      tlow=min(high,tlow)
      thigh=max(low,thigh)
      tlow=max(low,tlow)
      return random.randint(tlow,thigh)

    def ChoosePowerLaw(self,high):
        """Choose a random number in [1, high], using a power-law distribution p(x) ~ x^-2"""
        z = self.randfloat() # Uniformly distributed over [0, 1)
        x = 1. / (1. - z)    # Power-law distributed over [1, infinity)
        return int(min(x, high))

    class ChooseAllSequence:
      def __init__(self,low,high):
        self.low=low
        self.high=high
        self.seq=[]

    # Similar to std::shuffle / random.shuffle, except using a known algorithm
    # and calling myrand(). This allows us to have exactly matching
    # C and Python implementations.
    def Shuffle(self, array):
        n = len(array)
        # Use the Fisher-Yates shuffle algorithm:
        # For each i = 0, ..., n-2 (inclusive), select j in i, ..., n-1 (inclusive)
        # and exchange elements i and j.
        for i in range(n-1):
            j = self.Choose(i, n-1)
            array[i], array[j] = array[j], array[i]

    def ChooseAll(self,low,high,name):
      """Choose a number in the given range from a random permutation of all possible values, eventually returning all of them."""
      if not name in self.ChooseAllSequences:
        self.ChooseAllSequences[name]=self.ChooseAllSequence(low,high)
      seq=self.ChooseAllSequences[name]
      if len(seq.seq)==0:
        seq.seq.extend(xrange(seq.low,seq.high+1))
        self.Shuffle(seq.seq)
      if seq.low>low:
        extra=range(low,seq.low)
        seq.seq.extend(extra)
        self.Shuffle(seq.seq)
        seq.low=low
      if seq.high<high:
        extra=range(seq.high+1,high+1)
        seq.seq.extend(extra)
        self.Shuffle(seq.seq)
        seq.high=high

      myelem=next((x for x in xrange(len(seq.seq)) if seq.seq[x]>=low and seq.seq[x]<=high), None)
      if myelem==None:
        return random.randint(low,high)
      return seq.seq.pop(myelem)

    def Sqrt(self,operand):
      return int(math.sqrt(operand))

    def option_level(self):
      return self.opts.level*3 # input parameters are more convient in *10 notation, but require *30 for encoding the level

    def option_tier(self):
      return self.opts.tier

    def option_maxbitrate_fudgefactor(self):
      return self.opts.maxbitrate_fudgefactor

    def option_targetausize(self):
      return self.opts.targetausize

    def option_width(self,rounding=8):
      return (self.opts.width+rounding-1)&~(rounding-1)

    def option_height(self,rounding=8):
      return (self.opts.height+rounding-1)&~(rounding-1)

    def option_force_tiles(self):
      return self.opts.force_tiles

    def option_framecnt(self):
      return self.opts.option_framecnt

def compute_bits(m):
  """Return number of bits needed to express value m"""
  b=0
  while (1<<b)<=m:
    b+=1
  return b

def convert_type(name):
  if '_' not in name:
    return name
  T=name.split('_')
  if T[0]=='int' or T[0]=='uint':
    r_min=int(T[1])
    r_max=int(T[2])
    if r_min>=0:
      return 'uint'+str(compute_bits(r_max))
    r_max=max(-r_min-1,r_max)
    return 'int'+str(1+compute_bits(r_max))
  return name

