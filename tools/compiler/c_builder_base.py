################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import cPickle,sys
from bitstream import builtin_functions
from bitstream import builtin_statements
from bitstream import convert_type

trace_calls = False

def make_fn(name,local_ids,defined_ids,d,xs,decls,ret):
    # undef is a set of undefined local identifiers
    undef = local_ids-defined_ids
    undef = ''.join('  int '+u+'; // auto \n' for u in undef if not u.startswith('allProfilerPoints_'))
    s_decls = ','.join(convert_type(t)+' '+x for t,x in decls)
    if s_decls: s_decls=','+s_decls
    if trace_calls:
        lines = xs.splitlines(True)
        line = 0
        return_count = 0
        for i in range(len(lines)):
          if 'return' in lines[i]:
            return_count += 1
        returns_processed = 0
        while i < len(lines):
          if 'return' in lines[i] and returns_processed < return_count:
            lines = lines[:i] + ['  printf(\" << ' + name + '\\n\");\n'] + lines[i:]
            i = 0
            returns_processed += 1
          i += 1
        xs = ''.join(lines)
        return (convert_type(ret)+' hevc_parse_'+name+' ( TOPLEVEL_T *b,' + name.upper()+'_T *p' + s_decls+' ) \n{\n'
            + '  printf(\">>  ' + name + '\\n\");\n'
            +    undef
            +    d
            +    xs
            + '  printf(\" << ' + name + '\\n\");\n'
            + '  return 0;'
            + '\n}\n\n')
    else:
        return (convert_type(ret)+' hevc_parse_'+name+' ( TOPLEVEL_T *b,' + name.upper()+'_T *p' + s_decls+' ) \n{\n'
                +    undef
                +    d
                +    xs
                + '  return 0;'
                + '\n}\n\n')

def make_alloc(a):
    id = a[0].rpartition(".")[2]
    id = id.rpartition("->")[2]
    typ = id_to_type[id]
    dims = id_to_array_dimensionality[id]
    return a[0] + '.alloc({' + (','.join("static_cast<size_t>({})".format(x) for x in a[1:]))+'});\n'

def make_local_array_def(typ, a, d):
    dims = d.count('[')
    #definition = '  '+typ+' ALIGN16 '+a+d+';\n'
    definition = '  flat_array<'+typ+','+str(dims)+'> '+a+';\n'
    definition += '  '+a+'.alloc({'+(', '.join(d[1:-1].split('][')))+'});\n'
    return definition

def make_covercross_call(n, xs):
    global covercrossDefs
    covercrossDefs += "{{ \"{}\", {} }},\n".format(n, ", ".join("{}, {}, {}".format(xs[i+1], xs[i+2], xs[i+3]) for i in xrange(0, len(xs), 4)))
    return 'profile_cross('+n+','+  ",".join("(int)({})".format(x) for x in xs) +');\n'

def make_ChooseAll(ind):
  indbits=ind.rpartition(',')
  return 'builtin_ChooseAll({},"{}")'.format(indbits[0],indbits[2].strip())

def disp(a):
  print a

sys.setrecursionlimit(15000)
i=1
inputFile = None
outputFile = None
ccFile = None
is_encode = 0
while i < len(sys.argv):
    if sys.argv[i] == "--encode":
        is_encode = int(sys.argv[i+1])
        i += 1
    elif sys.argv[i] == "-o":
        outputFile = sys.argv[i+1]
        i += 1
    elif sys.argv[i] == "-cc":
        ccFile = sys.argv[i+1]
        i += 1
    else:
        inputFile = sys.argv[i]
    i += 1

assert outputFile is not None
assert inputFile is not None

with open(inputFile,'rb') as fd:
    A=cPickle.load(fd)

with open(inputFile[:-len('c_ids')]+'struct','rb') as fd:
    field_to_fn,defined_fields,call_to_fn,id_to_type,id_to_array_dimensionality = cPickle.load(fd)

covercrossDefs = ""

B=default_parser(A).apply("start")[0]

with open(outputFile,'wb') as out:
    print >>out,B

if ccFile is not None:
    with open(ccFile, 'w') as out:
        out.write(covercrossDefs)
