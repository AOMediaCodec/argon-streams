#!/usr/bin/env python2

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import sys,errno,os,shutil,cgi
from collections import defaultdict

if len(sys.argv)!=4 or sys.argv[2]!='-o':
  print """Usage: make_report.py prof.txt -o report

Creates a directory called report containing a report.html file that displays the coverage information.
"""
  sys.exit(1)
fname = sys.argv[1]
dname = sys.argv[3]

def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

make_sure_path_exists(dname)

def insert(D,A,value):
  """Recursively insert list A into dictionary D"""
  a=A[0]
  if value==0:
    if len(A)==1:
      # Reached bottom
      D[a]=0
      return
    if a not in D:
      D[a]={}
    insert(D[a],A[1:],value)
  else:
    v2=D[a]
    if v2==0:
      # Need to populate this entry
      # If we have a binary range we split, otherwise we just store range
      low,high,rlow,rhigh=map(int,A[1:])
      if rlow==0 and rhigh==1:
        E={a+'==0':1 if low==0 else 0,
           a+'==1':1 if high>=1 else 0,  # Currently some of our ranges are too narrow (often in while loops), so high can be > rhigh
        }
        D[a]=E
      else:
        # Change name of key to include range
        del D[a]
        D[a+' min=%d max=%d'%(low,high)]=1
    else:
      insert(v2,A[1:],value)

def insert_file(D,fname,value):
  """Insert all seen items into hierachical dictionary"""
  with open(fname,'rb') as fd:
    for line in fd:
      if 'failsafe' in line:
        continue
      A=line.rstrip().split(';')
      insert(top,A,value)

top={}
insert_file(top,'all_points.txt',0)
insert_file(top,fname,1)

cover_cache={}
def coverage(D):
  """Returns hits,total for dictionary"""
  if D==0:
    return 0,1
  elif D==1:
    return 1,1
  else:
    if id(D) not in cover_cache:
      cover_cache[id(D)] = [sum(coverage(E)[i] for E in D.values()) for i in [0,1]]
    return cover_cache[id(D)]

def recurse(out,D,indent):
  """Output hierarchical lists for the given profile."""
  sp=' '*indent*4
  for i,(key,value) in enumerate(sorted(D.items())):
    last= ' class="lastChild"' if (i==len(D)-1) else ''
    key = cgi.escape(key)
    # outline:#FF0000 dotted;
    if value==0:
      print >>out,sp+'<li style="color:red;font-weight:bold;font-size:medium"'+last+'>'+key+"</li>"
    elif value==1:
      print >>out,sp+'<li style="color:green;font-weight:normal;font-size:medium"'+last+'>'+key+"</li>"
    else:
      h,t = coverage(value)
      if h==t:
        print >>out,sp+'<li style="color:green;font-weight:normal;font-size:medium"'+last+'>',
      elif h==0:
        print >>out,sp+'<li style="color:red;font-weight:bold;font-size:medium"'+last+'>',
      else:
        print >>out,sp+'<li style="color:orange;font-weight:bold;font-size:medium"'+last+'>',
      print >>out,('%g%% (%d/%d) covered in '%(h*100.0/t,h,t))+key
      if h==t:
        print >>out,sp+'<ul class="collapsibleList" style="color:green">' # It is useful to store color here in order to be able to work out which uls to expand
      else:
        print >>out,sp+'<ul class="collapsibleList" style="color:red">'
      recurse(out,value,indent+1)
      print >>out,sp+"</ul></li>"

with open(os.path.join(dname,'report.html'),'wb') as out:
  # HEADER
  print >>out,"""<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>HEVC profile report</title>

<script type="text/javascript" src="CollapsibleLists.js"></script>

<link href="treeview.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
function load()
{
  CollapsibleLists.apply();
}
function showBad()
{
  CollapsibleLists.expandBad();
}
</script>

</head>
<body onload="load()">
  <img src="logo.png">
  <button type="button" onClick="CollapsibleLists.hideAll()" >Collapse all</button>
  <button type="button" onClick="showBad()" >Show test escapes</button>"""
  # GENERATE DATA
  h,t=coverage(top)
  x=('  (%g%% of specification covered with %d points hit out of a total of %d points.)'%(h*100.0/t,h,t))
  print >>out,"""<ul class="treeView"> <li> HEVC Profile report for """+fname+x+'<ul class="collapsibleList">'
  recurse(out,top,1)
  print >>out,"</ul></li></ul>"

  # TAIL
  print >>out,"""</body>
</html>
"""

# Now copy helper files
for a in """button-closed.png               list-item-last-open.png  logo.png
button-open.png                 list-item-last.png
button.png                      list-item-open.png
CollapsibleLists.js  list-item.png
list-item-contents.png          list-item-root.png       treeview.css""".split():
  shutil.copy(os.path.join('html',a),dname)



