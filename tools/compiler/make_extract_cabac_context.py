################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> e

function = ['cabac_fn' [:b :a] [:name :init] ] -> cabac_context_fn(name,b,a,init)
         | ['fn' [:s] [:name :decls :ret]] -> cabac_binary_fn(name,s,decls)
         | anything:a -> disp('unknown'+str(a))

""","extract_cabac_context")

