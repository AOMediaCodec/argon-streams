################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import sys
import os

outFile=sys.argv[1]
inFiles=sys.argv[2:]

with open(outFile, "w") as outf:
  outf.write("typedef struct {\n")
  outf.write("  const char *name;\n")
  outf.write("  const long long low;\n")
  outf.write("  const long long high;\n")
  outf.write("  const int step;\n")
  outf.write("  const char *visibilityConditionTrue;\n")
  outf.write("  const char *visibilityConditionFalse;\n")
  outf.write("} PROFILER_POINT_T;\n")
  outf.write("\n")
  outf.write("const static PROFILER_POINT_T allProfilerPoints[]={\n")

  idx=0
  fileIdxs={}

  for f in inFiles:
    with open(f, "r") as inf:
      fileIdxs[os.path.basename(f)[:-len(".gen.points")]]=idx
      lines=inf.readlines()
      for l in lines:
        if len(l.strip())>0:
          outf.write(l)
          idx+=1
  outf.write("  { NULL, 0, 0, 0, NULL, NULL }\n")
  outf.write("};\n")
  outf.write("\n")

  for f in fileIdxs:
    outf.write("const static PROFILER_POINT_T *allProfilerPoints_{} = "
               "allProfilerPoints + {};\n"
               .format(f, fileIdxs[f]))

  outf.write("#define ALL_PROFILER_POINTS_UNUSED do { ")
  for f in fileIdxs:
    outf.write("(void)(allProfilerPoints_{}); ".format(f))
  outf.write("} while (0)\n");
  outf.write("\n")

  for f in fileIdxs:
    outf.write('#define PROFILE_POINTS_{} {}\n'
               .format(f.upper(), fileIdxs[f]))
  outf.write('\n')

  outf.write('#define PROFILE_POINTS_TABLE_SIZE {}\n\n'.format(idx))
