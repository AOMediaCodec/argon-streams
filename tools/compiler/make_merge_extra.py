################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

# collect_ids returns a dictionary of AST replacements for each id in order
# insert_ast takes a dictionary of id->argument and a replacement AST
#   It replaces all ids in the replacement AST with the corresponding arguments

# If the compile fails complaining about a KeyError in ids, it probably means someone has used an array with a rewrite rule with too many array indices.

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> e

function = ['fn' [expr:xs] [:name :side :ret] ] -> ('fn',[xs],[name,side,ret]) if name not in extra_fns else ('fn',[ ['block',[extra_fns[name],xs],[]] ],[name,side,ret])
         | :notfn -> notfn

justname = ['array' [justname:a :x] [] ] -> a
         | ['id' [] [:x]] -> x

collect_ids :ids = ['array' [collect_ids(ids[:-1]):a expr:x] [] ] -> add_to_dict(a,ids[-1],x)
                 | ['id' [] [:x]] -> {}

insert_ast_wrap [:D insert_ast(D):a] -> a

insert_ast :D = ['id' [] [:n]] ?(n in D) -> D[n]
              | [:op [insert_ast(D)*:xs] :side] -> op,xs,side

expr = ~~justname:n ?(n in rewrites) collect_ids(rewrites[n][0]):c insert_ast_wrap(c,rewrites[n][1])
     | [:op [expr*:xs] :side] -> op,xs,side

""","merge_extra")

