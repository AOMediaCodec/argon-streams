################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

# Compute any constant expressions that we can spot
# We spot assignment to localids and use a dictionary to track possible simplifications.
# Care needs to be taken for looping constructs that the ids are removed from the dictionary
# remove_ids goes through expressions and removes any assignments (via = or element)
# other elements return the new syntax tree, with the dictionary updated to contain new constant values

# Probably want to remove the [special calls and += type operations and element] and convert them into normal calls and assignments
# Makes subsequent stages simpler...
# Want to avoid backtracking so call rules on extracted simplified expressions

# expr returns fully simplified expressions, always matches.
# simplify_expr tries pattern matching to do a single level of constant folding (doesn't need the dictionary anymore)
# If nothing matches, it returns the original AST

# Be careful with lvalues that they may include rvalues as part of array index
# constant fails to match unless a constant expression is found

# OPT simplify constant array lookup?

# remove_ids(D) removes all localids present as lvalues in the following single expression from dictionary D

# Currently only folds localids that are simple values (not arrays)

# WARNING Assumes that if we write an identifier, we can be sure that no one will change it in a different function!
# This may not always be valid?
# May want to switch back to only working on localids (need to mark more as being localids though)
# Would then make it easier to remove localids that are never read, only written

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> e

function = ['fn' [expr({}):xs] :side ] -> 'fn',[xs],side
         | anything

justname = ['id' [] [anything:x]] -> x

constant = ['num' [] [:x]] -> x

localid = ['id' [] [anything:x]] -> x

simplify_expr = ['call' [ justname:a ?(a in pure_functions) ['arglist' [constant*:x] []] ] :side ] -> make_num(pure(a,x))
        | ['unary-' [constant:a] [] ] -> make_num(-a)
        | ['unary!' [constant:a] [] ] -> make_num(0 if a else 1)
        | ['||' [constant:a ?(a) :b] [] ] -> make_num(1)
        | ['||' [constant:a ?(not a) :b] [] ] -> b
        | ['||' [:a constant:b ?(b)] [] ] -> make_num(1)
        | ['&&' [constant:a ?(not a) :b] [] ] -> make_num(0)
        | ['&&' [:a constant:b ?(not b)] [] ] -> make_num(0)
        | ['&&' [constant:a ?(a) constant:b ?(b)] [] ] -> make_num(1)
        | ['&&' [constant:a ?(a) :b ] [] ] -> b
        | ['?' [constant:test :res1 :res2] [] ] -> res1 if test else res2
        | [('arglist') :a :b]:x -> x
        | [:op [constant:a constant:b] [] ] -> make_num(eval(str(a)+op+str(b)))
        | anything

apply_assign = [:D '=' [ localid:i constant:rhs ] []] !(add_to_dict(D,i,rhs))
             | [:D '=' [ remove_lvalue(D) :rhs ] []]
             | anything

simplify_if = [ constant:x :yes ] -> yes if x else ['block',[],[]]
            | [:cond :yes] -> 'if',[cond, yes],[]

simplify_ifelse = [ constant:x :yes :no] -> yes if x else no
            | [:cond :yes :no] -> 'ifelse',[cond, yes, no],[]

launch_remove_ids  = [:D remove_ids(D)]

launch_expr  = [:D expr(D):e] -> e

copy_dict = :D -> dict(D)

remove_lvalue :D = ['id' [] [anything:x]] !(remove_from_dict(D,x))
                 | ['array' :a :b]
                 | :x -> disp('remove lvalue error '+str(x))

remove_ids :D = ['=' [remove_lvalue(D):lhs :rhs] [] ]
              | ['element' [remove_lvalue(D):lhs :rhs] [:style] ]
              | [:op [remove_ids(D)*] :side ]

lvalue :D = ['id' [] [anything:x]]:a -> a
          | ['array' [lvalue(D):lhs expr(D):ind] []] -> 'array',[lhs,ind],[]
          | :x -> disp('lvalue error '+str(x))

debug = ~~anything:a !(disp(str(a)))

merge_cond [:cond :D :val] = :test ?(cond==test) -> val
                      | [:op [merge_cond(cond,D,val)*:a] :side] -> [op,a,side]

cond_op = '==' | '!=' | '&&' | '||'

conditional_expr = [cond_op :a :b]

expr :D = ['id' [] [anything:x]] -> find_in_dict(D,x)
        | ['block' [expr(D)*:s] [] ] -> 'block',s,[]
        | ['if' [~~conditional_expr:mcond expr(D):cond copy_dict(D):B ~~remove_ids(D) merge_cond(mcond,B,make_num(1)):R launch_expr(B,R):yes] [] ] simplify_if(cond,yes)
        | ['if' [expr(D):cond copy_dict(D):B ~~remove_ids(D) expr(B):yes] [] ] simplify_if(cond,yes)
        | ['ifelse' [~~conditional_expr:mcond expr(D):cond copy_dict(D):B copy_dict(D):C ~~remove_ids(D) merge_cond(mcond,B,make_num(1)):R launch_expr(B,R):yes ~~remove_ids(D) merge_cond(mcond,B,make_num(0)):S launch_expr(C,S):no ] [] ] simplify_ifelse(cond,yes,no)
        | ['ifelse' [expr(D):cond copy_dict(D):B copy_dict(D):C ~~remove_ids(D) expr(B):yes ~~remove_ids(D) expr(C):no ] [] ] simplify_ifelse(cond,yes,no)
        | ['for' [expr(D):start :cond :iter :body] :side ]
                launch_remove_ids(D,body) launch_remove_ids(D,iter) copy_dict(D):B
                launch_expr(B,cond):cond2 copy_dict(D):B
                launch_expr(B,body):body2 launch_expr(B,iter):iter2 -> 'for',[start, cond2, iter2, body2],side
        | [('while'|'do'):op [:cond :body] [] ]
                launch_remove_ids(D,body) copy_dict(D):B
                launch_expr(B,cond):cond2 copy_dict(D):B
                launch_expr(B,body):body2 -> op,[cond2, body2],[]
        | ['=' [lvalue(D):lhs expr(D):rhs] [] ] apply_assign([D,'=',[lhs,rhs],[]]) -> '=',[lhs,rhs],[]
        | ['element' [lvalue(D):lhs expr(D):w] [:style]]  apply_assign([D,'=',[lhs,[]],[]]) -> 'element',[lhs,w],[style]
        | ['COVERCROSS' :args :side]
        | [:op [expr(D)*:a] :side ] simplify_expr([op,a,side])
        | :x -> disp('expr error'+str(x))

""","fold_constants")

