################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

"""
Python to be appended to generated code
"""
import cPickle,sys

# This runs through the functions and fills in a dictionary from function name to AST for that function
# Also extracts range statements and fills in array limits
extra_fns={}
def store_fn(name,xs):
    assert name not in extra_fns
    extra_fns[name]=xs

limits={}
def store_max(name,val):
    assert name not in limits
    if isinstance(val,str):
        val = limits[val]
    limits[name] = val

rewrites={}    # dictionary from name of array to list of ids and final AST
def store_rewrite(start,ast):
    # Rewrite rules are removed from the resulting syntax tree
    rewrites[start[0]]=start[1:],ast
    return 'block',[],[]

assert sys.argv[-2]=='-o'
with open(sys.argv[-3],'rb') as fd:
    A=cPickle.load(fd)

store_extra(A).apply("start")[0]

with open(sys.argv[-1],'wb') as out:
    cPickle.dump((extra_fns,limits,rewrites),out,2)
