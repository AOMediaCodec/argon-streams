################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar
# USER GUIDE
#
# Profiling adds calls for each part of each assignment
#
# Functions that return a value should be defined in profile_base (or get warning about Unknown Function)
# It is best to define functions at the bottom of the syntax.  This allows the profile to deduce ranges for each argument.
#

# INTERNALS
# This inserts call to profile for each part of each assignment.
# We use the extracted structure to determine the allowed range of values.
# Use "100.5" to mean part 5 of equation 100.
# We use global variables to keep track of equations and parts so we need to be careful not to call the rules multiple times.

# profile(rhs) returns an expression that calls the profiler for the current equation on the given expression and all subexpressions.
#
# profile0(rhs) is the same as profile except that constant expressions are still profiled at the top level.  (Used to make sure that whole equations don't disappear.)
#
# range of an expression returns a tuple of (min,max,step) for the allowed values (or a list of these for arglist)
#
# Relies on effective memoisation to work efficiently.  Perhaps this is not happening?
#
# prepare_function extracts all ranges that can be deduced from elements/function parameters and returns a list of assert statements to check ranges of input parameters
#
# prepare_locals tries a iterative relaxation for the types of local variables (nothing is returned)
#
# TODO seem to have quite a lot of repetition here, perhaps can be refactored somehow?  e.g. declaration could be skipped?
# May have problems if a loop variable is reused?

pymeta3.grammar.OMeta.makeCode(r"""

start = function+

next_ind -> get_ind()

decl :fnname = next_ind:ind [:t :name] ?(fnname in fn_to_range and t=='int')  !(save_range(name,fn_to_range[fnname][ind])) -> make_assert(name)
             | [:t :name] ?('pointer' in t) -> make_assert_nonzero(name)
             | [:t :name] !(save_range(name,range_type(t))) -> make_assert(name)

prepare_function = ['fn' !(new_function()) [extract_elements] [:name !(log_function(name)) [decl(name)*:d] :ret]] -> 'block',d,[]

prepare_decl :fnname = next_ind:ind [:t :name] ?(fnname in fn_to_range and t=='int') -> save_next_range(name,fn_to_range[fnname][ind])
                     | [:t :name] ?('pointer' in t)
                     | [:t :name] -> save_next_range(name,range_type(t))

prepare_statement = ['=' [justname:lhs range:rhs] :side] -> save_next_range(lhs,rhs)
                  | ['for' [assign:start cond('>'):cond ?(start[0]==cond[0]) !(save_range(start[0],[cond[1][0]+1,start[1][1],1])) ?(False) ] ]
                  | ['for' [assign:start cond('>='):cond ?(start[0]==cond[0]) !(save_range(start[0],[cond[1][0],start[1][1],1])) ?(False) ] ]
                  | ['for' [assign:start cond('<'):cond ?(start[0]==cond[0]) !(save_range(start[0],[start[1][0],cond[1][1]-1,1])) ?(False) ] ]
                  | ['for' [assign:start cond('<='):cond ?(start[0]==cond[0]) !(save_range(start[0],[start[1][0],cond[1][1],1])) ?(False) ] ]
                  | ['element' [justname:name num:x] [:style]] -> next_element(name,style,x)
                  | [:op [prepare_statement*:xs] :side]

prepare_locals = !(clear_locals()) ['fn' [prepare_statement] [:name [prepare_decl(name)*] :ret ] ] !(prepare_locals())

function = ~~anything:f prepare_function(f):A prepare_locals(f) prepare_locals(f) prepare_locals(f) prepare_locals(f) prepare_locals(f) ['fn' [statement:x] :side] -> 'fn',[ ['block',[A,x],[]] ],side
         | anything

num = ['num' [] [:x]] -> x

range_args = ['arglist' [justname:n range*:rs] :side] -> n,rs

extract_elements = ['element' [justname:name num:x] [:style]] -> element(name,style,x)
                 | ['call' [justname:n ?(n=='Range' or n=='RangeStep') range_args:x ] :side] -> add_range(*x)
                 | [:op [extract_elements*] :side]

justname = ['id' [] [:name]] -> name
         | ['array' [justname:name anything] :side] -> name

range = ['id' [] [:name]] -> range_id(name)
      | ['num' [] [:x]] -> x,x,1
      | ['array' [range:a anything] :side] -> a
      | ['unary~' [range:a] anything] -> a[0],a[1],a[2]
      | ['unary!' anything anything] -> range_type('uint1')
      | ['unary-' [range:a] anything] -> -a[1],-a[0],a[2]
      | ['call' [justname:fn range:a] :side] -> range_call(fn,a)
      | ['arglist' [range*:rs] :side] -> rs
      | ['?' [:cond range:a range:b] :side] -> range_op('?',a,b)
      | [:op [range:a range:b] :side] -> range_op(op,a,b)
      | ['COVERCLASS' [range:a] :side] -> a
      | ['alloc' [] []] -> 0,0,1
      | ~~anything:x !(error('Cannot compute range for '+str(x)))

describe = [('id'|'num'|'string') [] [anything:x]] -> str(x)
     | ['call' [describe:a describe:ind] [] ] -> a+'('+ind+')'
     | ['arglist' [describe*:a] [] ] -> ', '.join(a)
     | ['array' [describe:a describe:ind] [] ] -> a+'['+ind+']'
     | ['=' [describe:a describe:b] :side ] -> a+'='+b
     | [:op [describe:a describe:b] :side ] -> '('+a+str(op)+b+')'
     | ['?' [describe:a describe:b describe:c] :side] -> a+'?'+b+':'+c
     | ['unary~' [describe:a] [] ] -> '(~'+a+')'
     | ['unary-' [describe:a] [] ] -> '(-'+a+')'
     | ['unary!' [describe:a] [] ] -> '(!'+a+')'
     | ['COVERCLASS' [describe:a] :side] -> a
     | ['alloc' [] []] -> 'alloc'
     | :s -> 'unknown('+str(s)+')'

rangearg =  ~~range:r :e -> e,r

coverclass = ~~[ 'COVERCLASS' :xs [ :visT :visF ] ] !(coverclass_push(visT,visF)) [ 'COVERCLASS' [profile:e] :side ] !(coverclass_pop()) -> e
           |   [ 'COVERCLASS' [~~range:r ~~describe:d ['call' [~~justid:name :fn ~~range:args profile:a] :side] ] [ :visT :visF ] ] !(save_call(name,args)) -> make_profile(['call',[fn,a],side], r, d, visT=visT, visF=visF)
           |   [ 'COVERCLASS' [~~range:r ~~describe:d [:op [profile*:xs] :side] ] [ :visT :visF ] ] -> make_profile( [op,xs,side], r, d, visT=visT, visF=visF )

categorized_profile :vis = !(coverclass_push(vis[0],vis[1])) profile:p !(coverclass_pop()) -> p

ternary_profile = ~~range:r ~~describe:d ~~[ '?' [ [ 'COVERCLASS' :xs [ :visT :visF ] ] :b :c ] :side] [ '?' [ profile:a categorized_profile(visT,None):b categorized_profile(visF,None):c ] :side ] -> make_profile(['?',[a,b,c],side], r, d)

clip3_arglist = ['arglist' [profile:minarg profile:maxarg profile0:inarg] :side] -> 'arglist',[minarg,maxarg,inarg],side

profile = ['array' [:name profile:ind] :side] -> 'array',[name,ind],side
        | ['arglist' [profile*:xs] :side] -> 'arglist',xs,side
        | ~~range:r ~~describe:d ['call' [~~justid:name ?(name=="Clip3") :fn ~~range:args clip3_arglist:a] :side] !(save_call(name,args)) -> make_profile(['call',[fn,a],side], r, d)
        | ~~range:r ~~describe:d ['call' [~~justid:name :fn ~~range:args profile:a] :side] !(save_call(name,args)) -> make_profile(['call',[fn,a],side], r, d)
        | coverclass
        | ternary_profile
        | ~~range:r ~~describe:d [:op [profile*:xs] :side] -> make_profile( [op,xs,side], r, d )

profile0 = ~~range:r ~~describe:d ['array' [:name profile:ind] :side] -> make_profile( ['array',[name,ind],side], r , d, 1)
         | ~~range:r ~~describe:d ['call' [~~justid:name ?(name=="Clip3") :fn ~~range:args clip3_arglist:a] :side] !(save_call(name,args)) -> make_profile(['call',[fn,a],side], r, d, 1)
         | ~~range:r ~~describe:d ['call' [~~justid:name :fn ~~range:args profile:a] :side] !(save_call(name,args)) -> make_profile(['call',[fn,a],side], r, d, 1)
         | coverclass
         | ternary_profile
         | ~~range:r ~~describe:d [:op [profile*:xs] :side] -> make_profile( [op,xs,side], r, d, 1)
         | profile

justid = ['id' [] [:name]] -> name

assign = ['=' [justid:name range:rhs] :side] -> name,rhs

cond :target_op = [:op ?(op==target_op) [justid:name range:rhs] :side] -> name,rhs

new_eqn = ['EQN' [:s] :side ?(side[1]==None)]  -> side[0]

new_eqn_withexpr = ['EQN' [:s] :side ?(side[1]!=None)] -> side

categorized_statement :vis = !(coverclass_push(vis[0],vis[1])) statement:s !(coverclass_pop()) -> s

alloc_expr = ['array' [alloc_expr anything] :side]:a -> a
           | ['alloc' [] []]:a -> a

statement = ~~[ 'COVERCLASS' :xs [:visT :visF] ] !(coverclass_push(visT, visF)) [ 'COVERCLASS' [statement:s] :side ] !(coverclass_pop()) -> s
          | ['=' [anything alloc_expr] :side]:s -> s
          | ~~describe:d ['=' !(new_equation_number(d)) [:lhs profile0:rhs] :side] -> ['=',[lhs,rhs],side]
          | ~~new_eqn_withexpr:e !(set_multiple_eqn(e)) ['EQN' [statement:s] :side] !(set_eqn(e[0]+'...')) -> make_eqn_withexpr(s, e)
          | ~~new_eqn:e !(set_eqn(e)) ['EQN' [statement:s] :side] !(set_eqn(e+'...')) -> s
          | ~~['if' [ [ 'COVERCLASS' :xs [ :visT :visF ] ] :a ] :side] ['if' [~~describe:d !(new_equation_number('if ('+d+')...')) profile:cond categorized_statement(visT,None):a] :side]   -> 'if',[cond,a],side
          | ~~['ifelse' [ [ 'COVERCLASS' :xs [ :visT :visF ] ] :a :b ] :side] ['ifelse' [~~describe:d !(new_equation_number('if ('+d+')...')) profile:cond categorized_statement(visT,None):a categorized_statement(visF,None):b] :side] -> 'ifelse',[cond,a,b],side
          | ['if' [~~describe:d !(new_equation_number('if ('+d+')...')) profile:cond statement:a] :side] -> 'if',[cond,a],side
          | ['ifelse' [~~describe:d !(new_equation_number('if ('+d+')...')) profile:cond statement:a statement:b] :side] -> 'ifelse',[cond,a,b],side
          | ['def' :a :b] -> 'def',a,b
          | ['for' [assign:start cond('>'):cond ?(start[0]==cond[0]) !(save_range(start[0],[cond[1][0]+1,start[1][1],1])) ?(False) ] ]
          | ['for' [assign:start cond('>='):cond ?(start[0]==cond[0]) !(save_range(start[0],[cond[1][0],start[1][1],1])) ?(False) ] ]
          | ['for' [assign:start cond('<'):cond ?(start[0]==cond[0]) !(save_range(start[0],[start[1][0],cond[1][1]-1,1])) ?(False) ] ]
          | ['for' [assign:start cond('<='):cond ?(start[0]==cond[0]) !(save_range(start[0],[start[1][0],cond[1][1],1])) ?(False) ] ]
          | ['for' [statement:start ~~describe:d !(new_equation_number('while ('+d+')...')) profile:cond statement:iter statement:body] :side] -> 'for',[start,cond,iter,body],side
          | ['while' [profile:cond statement:body] :side] -> 'while',[cond,body],side
          | ['call' [justid:name range:args !(save_call(name,args))] :side] ?(False)
          | ['COVERCROSS' [rangearg*:xs] :side] -> 'COVERCROSSCALL', make_covercross(xs) , side
          | 'do not bother profiling arguments to functions' ['call' [:name profile:args] :side] -> 'call',[name,args],side
          | [:op [statement*:xs] :side] -> op,xs,side

""","profile")

