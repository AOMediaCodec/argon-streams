################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

"""
Generate a preprocessor that strips off // and /* comments
and applies simple "#define word value" replacements
"""

import pymeta3.grammar

process_equation_comments_grammar="""
start = line+:cs -> ''.join(cs)

word = letterOrDigit+:xs -> ''.join(xs)

digit = :d ?('0'<=d<='9') -> d

number = spaces digit+:xs -> ''.join(xs)

eqnFirstPart = number
             | spaces :l ?('A'<=l<='Z') -> l

line = normal_line:a eqn_comment:b '\n':c -> make_eqn_line(a,b)
     | normal_line:a comment? '\n':b -> a+b

normal_line = ( ~'\n' ~"//" char)*:xs -> ''.join(xs)

normal_eqn_comment = #( eqnFirstPart:a number:b #) -> ['N', a+'.'+b]
                   | #( eqnFirstPart:a #- number:b #) -> ['N', a+'.'+b]
                   | spaces eqnFirstPart:a #- number:b -> ['N', a+'.'+b]

range_eqn_comment = spaces #[RNG- ( ~']' ~'{' anything)*:name #{ ( ~'}' char)*:e #} #{ ( ~'}' anything)*:r #} #] -> ['R',(''.join(name))+'{'+(''.join(e))+'}{'+(''.join(r))+'}']
                  | spaces #[RNG- ( ~']' anything)*:name #] -> ['R',''.join(name)]

all_eqn_comment = normal_eqn_comment
                | range_eqn_comment

eqn_comment = "//" all_eqn_comment:x all_eqn_comment*:y ( ~'\n' anything)* -> [x]+y

comment = "//" ( ~'\n' anything)* -> ''

char =       "/*" ( ~"*/" anything)* "*/" -> ''
           | '\"' (~'\n' ~'\"' anything)*:xs '\"' -> '"'+''.join(xs)+'"'
           | anything
"""

pymeta3.grammar.OMeta.makeCode(process_equation_comments_grammar,"process_equation_comments")
