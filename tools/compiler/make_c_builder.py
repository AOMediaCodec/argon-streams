################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> ''.join(e)

decl = [:t :name] -> t,name
function = ['fn' [~~defined_ids(set()):defined_ids ~~local_ids(set()):local_ids ~~defines:d statement('  '):xs] [:name [decl*:decls] :ret] ] -> make_fn(name,local_ids,defined_ids,d,xs,decls,ret)
         | defines:s -> s

control_op = 'block' | 'for' | 'do' | 'while' | 'if' | 'ifelse' | 'EQN'

defines = ['def' [ ~~arraydims:d ?(d=='') arrayname:a expr:init ] [:typ] ?('autostruct' not in typ) ] -> '  '+typ[0]+' ALIGN16 '+a+d+' = '+init+';\n'
          | ['def' [ ~~arraydims:d ?(d=='') arrayname:a ] [:typ] ?('autostruct' not in typ)] -> '  '+typ[0]+' ALIGN16 '+a+';\n'
          | ['def' [ ~~arraydims:d arrayname:a expr:init ] [:typ] ?('autostruct' not in typ) ] -> '  flat_array<'+typ[0]+','+str(d.count('['))+'> '+a+' = '+init+';\n'
          | ['def' [ ~~arraydims:d arrayname:a ] [:typ] ?('autostruct' not in typ)] -> make_local_array_def(typ[0], a, d)
          | [control_op [defines*:e] :side] -> ''.join(e)
          | [:op :e :side] -> ''

local_name :S =   ['localid' [] [anything:x]] !(S.add(x))
                | ['array' [local_name(S) :dim] :side]

debug = ~~anything:a !(disp(a))

defined_ids :S =   ['def' [ local_name(S) expr?] :side ] -> S
                 | [:op [defined_ids(S)*] :side] -> S

local_ids :S = local_name(S) -> S
             | [:op [local_ids(S)*] :side] -> S

arrayname = justname
          | ['array' [arrayname:a :ind] []] -> a

arraydims = justname -> ''
           | ['array' [ arraydims:a expr:dim] [] ] -> a+'['+dim+']'

add_comma_expr = expr:e -> ','+e if len(e) else ''

sub_statement :indent = 'block' [statement(indent+'  ')*:s] []  -> indent+'{\n'+''.join(s)+indent+'}\n'
          | 'if' [expr:cond statement(indent+'  '):yes] []  -> indent+'if ('+cond+')\n'+yes
          | 'ifelse' [expr:cond statement(indent+'  '):yes statement(indent+'  '):no] []  -> indent+'if ('+cond+')\n'+yes+indent+'else\n'+no
          | 'for' [statement(indent):start expr:cond statement(indent+'  '):iter statement(indent+'  '):s] []  -> indent+'{\n'+start+indent+'while('+cond+'){\n'+s+iter+indent+'}\n'+indent+'}\n'
          | 'while' [expr:cond statement(indent+'  '):s] []  -> indent+'while('+cond+')\n'+s
          | 'do' [expr:cond statement(indent+'  '):s] []  -> indent+'do\n'+s+indent+'while ('+cond+');\n'
          | 'EQN' [statement(indent):s] :side  -> s
          | 'ASSERT' [expr:e expr:args] ?(not args) [:msg]  -> indent+'ASSERT('+e+',"'+msg+'");\n'
          | 'ASSERT' [expr:e expr:args] [:msg]  -> indent+'ASSERT('+e+',"'+msg+'",' + args + ');\n'
          | 'CHECK' [expr:e expr:args] ?(not args) [:msg]  -> indent+'CHECK('+e+',"'+msg+'");\n'
          | 'CHECK' [expr:e expr:args] [:msg]  -> indent+'CHECK('+e+',"'+msg+'",' + args + ');\n'
          | 'CSV' [expr*:e] [:n] -> indent+'CSV('+n+','+(','.join(e))+');\n'
          | 'COVERCROSS' :a :b -> indent+';\n'
          | 'def' :a :side -> ''
          | ?(not is_encode) 'element' [expr:a add_comma_expr:w] [:style] -> indent+a+' = builtin_syntax_elem(bitstream_read_'+style+'(b'+w+'));\n'
          | ?(is_encode) 'element' [expr:a add_comma_expr:w] [:style] -> indent+'encode_bitstream_write_'+style+'(b,'+a+w+',"'+str(a)+'");\n'
          | 'break' [] [] -> indent+'break;\n'
          | 'continue' [] [] -> indent+'continue;\n'
          | 'return' [] [] -> indent+'return;\n'
          | 'return' [expr:a] [] -> indent+'return '+a+';\n'
          | 'call' [~~dataname:d fnname:a add_comma_expr:ind] [:q] ?('autostruct' in q)  -> indent+a+'( b, '+d+ind+');\n'
          | 'call' [justname:a ?(a in builtin_statements) expr:ind] :side  -> indent+'builtin_'+a+'('+ind+');\n'
          | 'call' [fnname:a add_comma_expr:ind] :side  -> indent+a+'( b, NULL'+ind+');\n'
          | 'COVERCROSSCALL' [expr*:xs] [:n] -> indent+make_covercross_call(n,xs)
          | 'alloc' [[arrayindices:a]] [] -> indent+make_alloc(a)
          | 'free' [expr:e] [] -> indent+e+'.free();\n'

statement :indent = ['COVERCLASS' [statement(indent):s] :side] -> s
          | [sub_statement(indent):s] -> s
          | expr:e -> indent+e+';\n'

anyid = 'id' | 'paramid' | 'localid'
fnname = [anyid [] [anything:x]] -> 'hevc_parse_'+x
dataname = [anyid [] [anything:x]] -> '&p->'+x
justname = [anyid [] [anything:x]] -> x

int16s = 'int16x4' | 'int16x8'

int32s = 'int32x2' | 'int32x4'

expr = [sub_expr:e] -> e
     | :s -> 'unknown('+str(s)+')'

check_simd  = ~~( :op :exprs [ 'int32x4' | 'int32x2' | 'int16x8' | 'int16x4' | 'uint8x16' | 'uint8' | 'int32' | 'int32x4' | 'int' | 'int16'] )

sub_arrayindices = 'array' [[sub_arrayindices:a] expr:ind] [] -> a+[ind]
                 | 'id' [] [anything:x] -> [x]
                 | 'alloc' [] [] -> ['alloc']

arrayindices = 'array' [[sub_arrayindices:a] expr:ind] [] -> a+[ind]

sub_expr = 'num' [] [anything:x] -> str(x) if (0 <= x and x >> 31 == 0) else '((int)'+str(x)+')' if -1<=(x>>31)<=0 else ('('+str(x+1)+'LL'+'-1)' if x == -(1<<63) else str(x)+'LL')
     | 'id' [] [anything:x] -> x
     | 'string' [] [anything:x] -> '"'+x+'"'
     | 'localid' [] [anything:x] -> x
     | 'paramid' [] [anything:x] -> x
     | 'ind_noexpr' [] [] -> ''
     | '=' [expr:lhs [arrayindices:a ?(a[0]=='alloc')]] [] -> lhs+'.alloc({'+(','.join('static_cast<size_t>('+x+')' for x in a[1:]))+'})'
     | ('=' | '+=' | '-=' | '/=' | '*=' | '%=' | '&=' | '^=' | '|=' ):op [expr:lhs expr:rhs] []  -> lhs+' '+op+' '+rhs
     | ':' [] [:raw :style]  -> raw if style=='C' else ''
     | 'call_unused' [ justname:a ?(a=='profile') expr:ind] :side  -> '(__typeof__('+ind[ind.find(",")+1:]+'))builtin_'+a+'('+ind+')'
     | 'call' [ justname:a ?(a=='profile') expr:ind] :side  -> 'builtin_'+a+'('+ind+')'
     | 'call' [ justname:a ?(a=='ChooseAll') expr:ind] :side  -> make_ChooseAll(ind)
     | 'call' [ justname:a ?(a in builtin_functions) expr:ind] :side  -> 'builtin_'+a+'('+ind+')'
     | 'call' [fnname:a add_comma_expr:ind] :side  -> a+'( b, NULL'+ind+')'
     | 'array' [expr:a expr:ind] [] -> a+'[(size_t)('+ind+')]'+(".name" if a.startswith("allProfilerPoints_") else "")
     | 'init' [expr*:a] []  -> '{'+','.join(a)+'}'
     | 'arglist' [expr*:a] []  -> ', '.join(a)
     | ('post++' | 'post--'):op [expr:a] []  -> a+op[4]+'='+'1'
     | 'unary-' [expr:a] []  -> '(-'+a+')'
     | 'unary~' [expr:a] []  -> '(~'+a+')'
     | 'unary!' [expr:a] []  -> '(!'+a+')'
     | '?' [expr:test expr:res1 expr:res2] []  -> '('+'('+test+')?'+res1+' : '+res2+')'
     | check_simd '<<scalar' [expr:a expr:b] [int16s] -> '_mm_sll_epi16('+a+',_mm_cvtsi32_si128('+b+'))'
     | check_simd '>>scalar' [expr:a expr:b] [int32s] -> '_mm_srai_epi32 ('+a+','+b+')'
     | check_simd '>>scalar' [expr:a expr:b] [int16s] -> '_mm_srai_epi16 ('+a+','+b+')'
     | check_simd '*' [expr:a expr:b] [int16s] -> '_mm_mullo_epi16('+a+','+b+')'
     | check_simd '*' [expr:a expr:b] [int32s] -> '_mm_mullo_epi32('+a+','+b+')'
     | check_simd '+' [expr:a expr:b] [int16s] -> '_mm_add_epi16('+a+','+b+')'
     | check_simd '+' [expr:a expr:b] [int32s] -> '_mm_add_epi32('+a+','+b+')'
     | 'vector' [expr:a] [int16s] -> '_mm_set1_epi16('+a+')'
     | 'vector' [expr:a] [int32s] -> '_mm_set1_epi32('+a+')'
     | 'vector_store' [expr:lhs expr:rhs] ['uint8x16' 'uint8'] -> '_mm_store_si128((__m128i*)&('+lhs+'),'+rhs+')'
     | 'vector_store' [expr:lhs expr:rhs] ['int32x4' 'int32'] -> '_mm_store_si128((__m128i*)&('+lhs+'),'+rhs+')'
     | 'vector_store' [expr:lhs expr:rhs] ['int32x4' 'int'] -> '_mm_store_si128((__m128i*)&('+lhs+'),'+rhs+')'
     | 'vector_store' [expr:lhs expr:rhs] ['int16x4' 'int16'] -> '_mm_storel_epi64((__m128i*)&('+lhs+'),'+rhs+')'
     | 'vector_store' [expr:lhs expr:rhs] ['int16x8' 'int16'] -> '_mm_storeu_si128((__m128i*)&('+lhs+'),'+rhs+')'
     | 'vector_store' [expr:lhs expr:rhs] ['int32x4' 'int16'] -> '_mm_storel_epi64((__m128i*)&('+lhs+'),_mm_packs_epi32 ('+rhs+',_mm_setzero_si128()))'
     | 'vector_store' [expr:lhs expr:rhs] ['int16x8' 'uint8'] -> '_mm_storel_epi64((__m128i*)&('+lhs+'),_mm_packus_epi16 ('+rhs+',_mm_setzero_si128()))'
     | 'vector_load' [expr:e] ['uint8x16' 'uint8'] -> '_mm_loadu_si128((__m128i*)&('+e+'))'
     | 'vector_load' [expr:e] ['int16x8' 'int16'] -> '_mm_loadu_si128((__m128i*)&('+e+'))'
     | 'vector_load' [expr:e] ['int32x4' 'int'] -> '_mm_loadu_si128((__m128i*)&('+e+'))'
     | 'vector_load' [expr:e] ['int16x8' 'int32'] -> '_mm_packs_epi32(_mm_loadu_si128((__m128i*)&('+e+')),_mm_loadu_si128(1+(__m128i*)&('+e+')))'
     | 'vector_load' [expr:e] ['int16x4' 'int16'] -> '_mm_loadl_epi64((__m128i*)&('+e+'))'
     | 'vector_load' [expr:e] ['int16x4' 'uint8'] -> '_mm_unpacklo_epi8(_mm_cvtsi32_si128(*(int*)&('+e+')),_mm_setzero_si128())'
     | 'vector_load' [expr:e] ['int16x8' 'uint8'] -> '_mm_unpacklo_epi8(_mm_loadl_epi64((__m128i*)&('+e+')),_mm_setzero_si128())'
     | 'vector_load' [expr:e] ['int32x4' 'int16'] -> '_mm_unpacklo_epi16(_mm_loadu_si128((__m128i*)&('+e+')),  _mm_cmpgt_epi16( _mm_setzero_si128(),_mm_loadu_si128((__m128i*)&('+e+')))  )'
     | :op [expr:a expr:b] []  -> '('+a+' '+str(op)+' '+b+')'
     | 'COVERCLASS' [expr:e] :side -> e
     | :op [expr*:e] [anything*:side]  -> '('+str(op)+' '+' '.join(e)+')'+('Extra{'+str(side)+'}' if len(side) else '')


""","c_builder")

