################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> e

function = ['fn' [expr:xs] :side ] -> 'fn',[xs],side
         | anything

expr    = [('+=' | '-=' | '/=' | '*=' | '%=' | '&=' | '^=' | '|='):op [expr:lhs expr:rhs] [] ] -> '=',[lhs, [op[0],[lhs,rhs],[]] ],[]
        | [('post++' | 'post--'):op [expr:a] [] ] -> '=',[a,(op[4],[a,make_num(1)],[]) ],[]
        | [:op [expr*:a] :side ] -> op,a,side
        | :x -> disp('complex expr error'+str(x))

""","remove_complex_operations","pure_ast")

