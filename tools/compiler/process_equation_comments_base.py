################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

"""
Python to be appended to generated code
"""
import re,cPickle,sys

def make_eqn_line(line, eqns):
    normalEqns=[x[1] for x in eqns if x[0]=='N']
    if len(normalEqns)!=1:
        if len(normalEqns)==0:
            print "Range equation supplied without normal equation!"
        else:
            print "Too many normal equations!"
        print normalEqns
        print line
        assert(False)
    rangeEqns=[x[1] for x in eqns if x[0]=='R']
    if len(rangeEqns)==0:
      return 'EQN('+normalEqns[0]+') {'+line+'}\n'
    elif len(rangeEqns)==1:
      m = re.match(r"([^\{]*)\{([^\}]*)\}\{([^\}]*)\}$", rangeEqns[0])
      if m!=None:
        return 'EQN('+normalEqns[0]+'['+m.group(1)+'])('+m.group(3)+')('+m.group(2)+') {'+line+'}\n'
      else:
        return 'EQN('+normalEqns[0]+'['+rangeEqns[0]+']) {'+line+'}\n'
    else:
      print "Too many range equations!"
      print rangeEqns
      print line
      assert(False)

i=1
outfile=None
infiles=[]
while i<len(sys.argv):
  if sys.argv[i]=="-o":
    i+=1
    outfile=sys.argv[i]
  else:
    infiles.append(sys.argv[i])
  i+=1

assert outfile!=None
assert len(infiles)==1

with open(infiles[-1]) as fd:
    A=fd.read()
    A=re.sub('\xe2\x88\x92','-',A)
    A=re.sub('\x96','-',A)
    B,Berr=process_equation_comments(A).apply("start")

with open(outfile,'wb') as out:
    out.write(B)
