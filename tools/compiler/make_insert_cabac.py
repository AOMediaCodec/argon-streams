################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
#
# DESCRIPTION:
# Search the abstract syntax tree for element nodes and replace with code.
# It replaces the element with the binarisation code.
# The binarisation consists of assigns to set the parameters, followed by the
# actual binarisation with calls to context_fn replace with the corresponding
# cabac context function.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = expr+:e -> e

extract_id = ['id' [] [:name] ] -> name
   | ['array' [extract_id:name :ind] []] -> name

lookup_binary_call = :name -> CABAC_CONTEXT[name][0]

lookup_binary_function = :name -> CABAC_BINARY[name][0]

lookup_binary_params = :name -> CABAC_BINARY[name][1]

lookup_context_fn = :name -> CABAC_CONTEXT[name][1]

justname = ['id' [] [:x]] -> x

function_name = ['call' [justname:n :args] :side] -> n

arg_list = ['call' [justname:n ['arglist' :args :side]] :s] -> args

replace_calls :c_f = function_name:n ?(n=='context_fn') -> c_f
                   | [:op [replace_calls(c_f)*:xs] :side]  -> (op,xs,side)

replace_calls2 = [:c_f replace_calls(c_f):x] -> x

insert_macro =  [:name :a test(a) :c_f  :b_f ] replace_calls2(c_f,b_f):x -> insert_cabac_fn(name,a,x)
             | anything:x !(disp('insert_macro cannot parse'+str(x))) -> x

debug = ~~anything:a !(disp(str(a)))

test = :x

assign_args [:params :args] -> make_assign(params,args)

check = :name -> check(name)

expr = ['element'  [~~extract_id:name expr:a expr:w] ['ae'] ]  lookup_binary_call(name):b lookup_context_fn(name):c_f
                                                               function_name(b):b_name  lookup_binary_function(b_name):b_f
                                                              lookup_binary_params(b_name):params  arg_list(b):args  assign_args(params,args):assigns
                                                              insert_macro(name,a,c_f,b_f):res -> ['block',[assigns,res],[]]
     | ['element'  [~~extract_id:name expr:a expr:w] :side ]:x ?(name in CABAC_ENCODE) -> ['block',[insert_noncabac_fn(name,a),x],[]]
     | [:op [expr*:xs] :side] -> (op,xs,side)

""","insert_cabac")

