################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

"""
Drop-in replacement for 'preprocessor.gen.py' which delegates to CPP
instead of doing the processing itself
"""

import re,sys,subprocess

extra_macros=[]
i=1
outfile=None
infiles=[]
while i<len(sys.argv):
  if sys.argv[i]=="-o":
    i+=1
    outfile=sys.argv[i]
  elif sys.argv[i].startswith("-D"):
    # Pass extra macro definitions straight through to CPP
    extra_macros.append(sys.argv[i])
  else:
    infiles.append(sys.argv[i])
  i+=1

# Include all input files except the last as headers (as if we used #include "...")
headers = []
for header in infiles[:-1]:
  headers.append("-include")
  headers.append(header)

args = (["cpp",
        "-nostdinc", # Don't allow inclusion of standard C headers!
        "-P",        # Don't emit line markers
        "-Werror",
        "-Wall",
        "-Wundef"] +
        extra_macros +
        headers +
        [infiles[-1],
        "-o", outfile])

result = subprocess.call(args)
if result != 0:
    # There was an error, but CPP should have printed a useful message to stderr,
    # so we can just exit without a message rather than raising an exception
    sys.exit(result)
