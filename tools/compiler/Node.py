################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

def make_id(i):
    return 'id',[],[i]

def make_num(i):
    if isinstance(i,bool):
        return 'num',[],[1 if i else 0]
    return 'num',[],[i]

