################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import cPickle,sys

# Extract cabac initializers
# Prepares NUM_CONTEXTS = number of contexts
# and CABAC_INIT[3][val]
# and CABAC_TO_CONTEXT dictionary from symbol (in lower case) to context

NUM_CONTEXTS = 0
CABAC_INIT = [ [],[],[] ]
CABAC_TO_CONTEXT={}
def cabac_field(n,I):
    global NUM_CONTEXTS
    name,dim1,dim2 = n
    name=name.lower()

    assert dim1==3
    assert len(I)==3
    assert name not in CABAC_TO_CONTEXT

    CABAC_TO_CONTEXT[name] = NUM_CONTEXTS
    N=len(I[0]) # number of extra contexts
    NUM_CONTEXTS += N
    for a in range(3):
        CABAC_INIT[a].extend(I[a])
        assert len(CABAC_INIT[a])==NUM_CONTEXTS

def disp(a):
  print(a)
  assert 0

assert sys.argv[-2]=='-o'
with open(sys.argv[-3],'rb') as fd:
    A=cPickle.load(fd)

extract_cabac_init(A).apply("start")[0]

with open(sys.argv[-1],'wb') as out:
    cPickle.dump((CABAC_TO_CONTEXT,CABAC_INIT,NUM_CONTEXTS),out,2)
