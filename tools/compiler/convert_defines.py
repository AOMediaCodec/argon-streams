################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

"""
Script to convert the '*_defines.txt' files used by the OMeta preprocessor
into '*_defines.h' files used by the CPP-based preprocessor
"""

import re,sys,os

i=1
infiles=[]
while i<len(sys.argv):
  infiles.append(sys.argv[i])
  i+=1

for fname in infiles:
  out_fname = os.path.splitext(fname)[0] + ".h"
  print "Converting %s -> %s" % (fname, out_fname)
  with open(fname) as infile:
    with open(out_fname, "w") as outfile:
        for a in infile.readlines():
            a=a.rstrip()
            if a.startswith('#') or len(a) == 0:
                outfile.write(a + "\n")
            else:
                l = a.split(None, 2) # Split at whitespace at most two times
                if len(l) == 3:
                    val,pat,comment = l
                    outfile.write("#define %s %s // %s\n"  % (pat, val, comment))
                else:
                    val,pat = l
                    outfile.write("#define %s %s\n"  % (pat, val))
