################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import cPickle,sys
# Pull out implicit hierarchy of where elements are defined

# TODO place these into the syntax somehow?

# builtin_functions is a list of built in functions
from bitstream import builtin_functions

# multiple_called_functions is a list of syntax functions that are called from multiple places (so prevent us from inferring structure)
# TODO perhaps we should simply remove multiply called functions?
multiple_called_functions = 'convert_distribution directional_residual_modification_8_6_5 mvd_coding compute_highest_tid_8_1 lookup_limits_A_4_1 profile_assert derive_temporal_prediction_8_5_3_2_7 generate_picture_8_3_3_2 arithmetic_startup_9_3_2_5 compute_longterm get_poc sao_copy fill_qp compute_available_6_4_1 DPB_bump hrd_parameters scaling_transform_8_6_2 decode_chroma_residual_8_5_4_3 decode_luma_residual_8_5_4_2 LongTermRefPic DiffPicOrderCnt derive_available_prediction_6_4_2 filter_chroma_8_7_2_5_5 filter_luma_8_7_2_5_4 decision_8_7_2_5_3 transform_block_boundary_8_7_2_2 deblock_quadtree_8_7_2 derive_qp decode_intra_8_4_4_1 arithmetic_decode_init EG decode_bypass decode_bypass_lowlevel decode_bypass_mult decode_bin encode_bin encode_bypass decode_terminate byte_alignment rbsp_trailing_bits scaling_list_data short_term_ref_pic_set coding_quadtree coding_unit pcm_sample transform_tree encode_tiles_enabled_flag get_arm_above_context setup_arm_registers_arith arm_write_context_ram arm_write_bubble'.split()
# ignored_fields is a list of fields defined at multiple places (so prevent us from inferring structure)
# TODO most of these can be removed now we distinguish based on case
ignored_fields = set('nCSL nCSC bitDepth delta xP yP tC2 Q QPQ QPP yDm xDk nD nS str c xC yC xS yS nT t matrixId sizeId bdShift k q idx v syntax binIdx fixedLength cIdx cMax bit m n x y x1 y1 i j video_parameter_set_id seq_parameter_set_id pic_parameter_set_id ff_byte beta_offset_div2 tc_offset_div2 arm_xCtb arm_yCtb arm_coeff_x arm_coeff_y arm_coeff_blksize arm_coeff_value arm_coeff_cIdx arm_val arm_codIOffset arm_codIRange arm_x0 arm_y0 arm_alignment_count arm_data enhancement_layers_cnt leb128_byte'.split())

ignored_functions = set(builtin_functions+multiple_called_functions)

field_to_fn={} # Mapping from field to function that extracts the element
defined_fields={} # Variables that have a def to explicitly note where they are defined:
                  # 1 means autostruct
                  # 2 means localid
                  # 3 means global structure (these are always constant arrays in HEVC)
id_to_type={} # Mapping from name of array to its base type (e.g. int16)
id_to_array_dimensionality={} # Mapping from id to bool stating whether it is an array type

def extract_field(fn,f,is_def,side,arrayIndices):
    """is_def = 1 means this is a define in the body of a function.
       is_def = 2 means that this is a define outside the functions."""
    #if f == 'tx_sz':  # Note a new type of variable is allowed once per compiled file without problems.
    #  print f,fn,is_def,side
    #  return
    if f in ignored_fields:
        return
    if is_def:
        id_to_type[f] = side[0][0]
        id_to_array_dimensionality[f] = arrayIndices
        if f in defined_fields and defined_fields[f]==1 and fn!=field_to_fn[f]:
          print f,'defined in both',field_to_fn[f],'and',fn
          assert False
        if f in defined_fields and defined_fields[f]==2 and 'autostruct' in side[0]:
          print f,'defined as both local and autostruct'
          assert False
        if 'autostruct' in side[0]:
          field_to_fn[f]=fn
          defined_fields[f]=1
        elif is_def==2:
          defined_fields[f]=3
        else:
          # This defines a local structure only used in the current function
          defined_fields[f]=2
    if f in defined_fields:
        return
    if f[0].islower() and '_' not in f:
      # lower case marks local ids, however syntax elements containing _ do not necessarily follow this rule.
      return
    if f in field_to_fn:
        if fn!=field_to_fn[f]:
            print f,'appears in both',fn,'and',field_to_fn[f]
        assert field_to_fn[f]==fn
    field_to_fn[f]=fn


# Mapping from name of function to the function that called it
call_to_fn = {}


def extract_call(fn, call):
    if call in ignored_functions:
        return
    if '_' not in call:
        return  # These are local functions
    if call in call_to_fn and call_to_fn[call] != fn:
        ignored_functions.add(call)
        del call_to_fn[call]
    call_to_fn[call] = fn


def extract_fn_args(D, args):
  for t, n in args:
    if n not in id_to_type:
      id_to_type[n]=t
      id_to_array_dimensionality[n] = 0

def disp(a):
  print a

assert sys.argv[-2]=='-o'
with open(sys.argv[-3],'rb') as fd:
    A=cPickle.load(fd)

# We can take an option to build our structure with another structure as the base
# This allows the subroutines to access data members in the sps and pps
field_to_fn,defined_fields,call_to_fn,id_to_type,id_to_array_dimensionality = {}, {}, {}, {}, {}
for i in range(len(sys.argv)):
    if sys.argv[i] == "-b":
        with open(sys.argv[i+1],"rb") as fd:
            a,b,c,d,e = cPickle.load(fd)
            field_to_fn.update(a)
            defined_fields.update(b)
            call_to_fn.update(c)
            id_to_type.update(d)
            id_to_array_dimensionality.update(e)

_,err=extract_structure(A).apply("start")

with open(sys.argv[-1],'wb') as out:
    cPickle.dump((field_to_fn,defined_fields,call_to_fn,id_to_type,id_to_array_dimensionality),out,2)

