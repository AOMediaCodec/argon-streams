################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> e

decl = [:t :name] -> name

getfnname = ['fn' [:xs] [:name :side :ret] ] -> name

function =  ~~getfnname:D ['fn' [expr(D):xs] :side ] !(extract_fn_args(D, side[1]))
         | ['def' [['id' [] [anything:a]] anything?] :side] -> extract_field('top level',a,2,side,0)
         | ['def' [['array' [justname_extractdepth(1):a :b] []] anything?] :side] -> extract_field('top level',a[0],2,side,a[1])
         | anything

justname = ['id' [] [anything:x]] -> x
         | ['array' [justname:x :b] []] -> x

justname_extractdepth :D = ['id' [] [anything:x]] -> (x,D)
                         | ['array' [justname_extractdepth(D+1):x :b] []] -> x

expr :D = ['element' [['id' [] [anything:a]] expr(D):w] [:style]] -> extract_field(D,a,0,None,0)
        | ['element' [['array' [justname_extractdepth(1):a :b] []] expr(D):w] [:style]] -> extract_field(D,a[0],0,None,a[1])
        | ['def' [['id' [] [anything:a]] anything?] :side] -> extract_field(D,a,1,side,0)
        | ['def' [['array' [justname_extractdepth(1):a :b] []] anything?] :side] -> extract_field(D,a[0],1,side,a[1])
        | ['=' [justname:a expr(D)] []] -> extract_field(D,a,0,None,0)
        | ['call' [justname:a :ind] :side ] -> extract_call(D,a)
        | [:op [expr(D)*:xs] :side] -> (op,xs,side)

""","extract_structure")

