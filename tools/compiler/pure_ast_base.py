################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

"""
Python to be appended to generated code
This is a generic base that loads an abstract syntax tree,
processes it, and returns a new tree.

It contains some common helper routines
"""
import cPickle,sys
from Node import *

sys.setrecursionlimit(15000)

assert sys.argv[-2]=='-o'
with open(sys.argv[-3],'rb') as fd:
    A=cPickle.load(fd)

B=default_parser(A).apply("start")[0]

with open(sys.argv[-1],'wb') as out:
    cPickle.dump(B,out,2)
