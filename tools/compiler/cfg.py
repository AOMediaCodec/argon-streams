################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Code for reading config language symbol definitions'''

# Use new-style open() with Python2, which means we can pass an encoding
# parameter for Python 2 code as well as Python 3 code.
import io
import re


C_SYM_RE = re.compile('^[a-zA-Z_][a-zA-Z_0-9]*$')


def read_groups(path):
    '''Read config language groups at the given path

    A group lists a set of local variables that should be available in some
    context. Each line of the input should be of the form

      GROUP: VAR VAR:VAR VAR

    If two variables appear separated by a colon, that means that the first
    variable is the name used by the config language and the second variable is
    the variable name in the streams code.

    You can use '\' as a line continuation character and insert existing groups
    with $group_name.

    '''
    ret = {}
    with io.open(path, 'r', encoding='utf-8') as infile:

        group = None
        group_vars = []

        for linum, line in enumerate(infile):
            line = line.split('#', 1)[0].strip()
            if not line:
                continue

            if group is None:
                # This is the start of a line. We should expect "group_name:
                # var+".
                if ':' not in line:
                    raise RuntimeError("{}:{}: No colon between "
                                       "group name and vars."
                                       .format(path, linum + 1))

                group, line = line.split(':', 1)
                group = group.strip()
                line = line.strip()
                if group in ret:
                    raise RuntimeError("{}:{}: Duplicate group definition."
                                       .format(path, linum + 1))

            # Spot whether the line ends in a '\' (which will mean we continue
            # the group on the next line)
            continue_line = (line and line[-1] == '\\')
            if continue_line:
                line = line[:-1]

            # Split the rest of the line into words (or maybe VAR:VAR pairs)
            words = [x.strip() for x in line.split()]

            for word in words:
                assert word

                # If the name starts with a $, it should be a group we already
                # know and we should copy that group into this one.
                if word[0] == '$':
                    ins_vars = ret.get(word[1:], None)
                    if ins_vars is None:
                        raise RuntimeError("{}:{}: Unknown group: `{}'."
                                           .format(path, linum + 1, word))
                    group_vars += ins_vars
                    continue

                # If the word has a ':', it gives the cname and sname
                # separately. Otherwise, they are equal.
                codec_name, impl_name = (word.split(':', 1)
                                         if ':' in word else [word, word])
                if not C_SYM_RE.match(impl_name):
                    raise RuntimeError("Local variable `{}' "
                                       "is not a valid C symbol."
                                       .format(impl_name))
                group_vars.append([codec_name, impl_name])

            # Do we close the group at this point?
            if not continue_line:
                if not group_vars:
                    raise RuntimeError("{}:{}: Empty variable list "
                                       "for group `{}'."
                                       .format(path, linum + 1, group))

                ret[group] = sorted(group_vars)
                group = None
                group_vars = []

        if group:
            raise RuntimeError("{}: File ends with backslash.")

    return ret


class EltDesc:
    def __init__(self):
        self.structure = ''
        self.codec_name = ''
        self.impl_name = ''
        self.dimensions = []
        self.instead = ''
        self.description = ''


def read_elements(path, groups, release_mode):
    '''Read bit-stream elements at the given path

    The result is a dictionary keyed by the codec name for the bit-stream
    element. Values are triples (index, impl_name, group_name). `index'
    is the index that will be used for this element. `impl_name' is the
    name used for this element in the source code. 'group_name' is either
    None or the name of a group (which should appear in groups).

    Each element block has the following form:

    codec_name
      USE:        alt_codec_name_0 alt_codec_name_1 ...
      IMPL:       impl_name
      GROUP:      group_name
      DESC:       description of codec_name
      ARGON-ONLY: true

    Any of the sub-block lines may be omitted.

    USE:   "You can't actually override codec_name. You should override
            alt_codec_name_i instead." (docs only)

    IMPL:  name used for the element in the source code

    GROUP: either None or the name of a group (from cfg_groups.txt)

    DESC:  a description of the element (docs only). Can spill onto
           several lines by ending a line with a \.

    ARGON-ONLY: Must be 'true' or 'false'. If true, this will be
                omitted from release builds.

    In the code below, any blocks containing a USE entry are ignored. In
    addition, all descriptions are ignored.

    '''
    symbol_re = re.compile('[a-zA-Z][a-zA-Z0-9_]*$')

    elts = dict()
    with io.open(path, 'r', encoding='utf-8') as elts_file:

        codec_name = None
        impl_name = None
        group_name = None
        ignore_elt = False

        desc_spilled_over = False
        condition = None

        for line_num, line in enumerate(elts_file):
            # Remove comments
            line = line.split('#', 1)[0]

            # Remove section numbers/headings
            line = line.split('--', 1)[0]

            # If there's nothing on this line, skip it
            if len(line.strip()) == 0:
                desc_spilled_over = False
                continue

            if line[0].isspace() or desc_spilled_over:
                # We're inside a definition block
                if codec_name is None:
                    raise RuntimeError('{}:{}: Expecting an element name.'
                                       .format(path, line_num + 1))

                if desc_spilled_over:
                    # This is a description continuing from the previous line
                    assert condition == 'DESC'
                    value = line.strip()
                else:
                    condition, value = [x.strip() for x in line.strip().split(':', 1)]

                if condition == 'DESC':
                    # ignore this condition
                    # the next line is also a DESC line <==> the current line ends on a '\'
                    desc_spilled_over = (value[-1] == '\\')
                elif condition == 'IMPL':
                    impl_name = value
                    if not symbol_re.match(codec_name):
                        raise RuntimeError('{}:{}: Implementation name `{}\' '
                                           'is not a valid symbol.'
                                           .format(path, line_num + 1,
                                                   impl_name))
                elif condition == 'GROUP':
                    if value not in groups:
                        raise RuntimeError("{}:{}: Element `{}' belongs to group "
                                           "`{}', which isn't one of the {} groups we "
                                           "have read."
                                           .format(path, line_num + 1,
                                                   codec_name, value, len(elts)))
                    group_name = value
                elif condition == 'USE':
                    ignore_elt = True
                elif condition == 'DFLT':
                    # ignore this condition
                    pass
                elif condition == 'ARGON-ONLY':
                    if value == 'true':
                        if release_mode:
                            ignore_elt = True
                    elif value == 'false':
                        pass
                    else:
                        raise RuntimeError('{}:{}: Bad value for ARGON-ONLY '
                                           '(should be \'true\' or \'false\').'
                                           .format(path, line_num + 1))
                else:
                    raise RuntimeError('{}:{}: unknown condition: `{}\'.'
                                       .format(path, line_num + 1, condition))
            else:
                # We're at the top level
                if not ignore_elt and codec_name is not None:
                    elts[codec_name] = (len(elts), impl_name, group_name)

                codec_name = line.split('[')[0].strip()
                # Check that codec_name is a valid symbol name
                if not symbol_re.match(codec_name):
                    raise RuntimeError('{}:{}: Elt name `{}\' '
                                       'is not a valid symbol.'
                                       .format(path, line_num + 1, codec_name))
                if codec_name in elts:
                    raise RuntimeError('{}:{}: Duplicate element name `{}\'.'
                                       .format(path, line_num + 1, codec_name))


                impl_name = codec_name
                group_name = None
                ignore_elt = False

        if not ignore_elt and codec_name is not None:
            elts[codec_name] = (len(elts), impl_name, group_name)

    return elts

    # Temporary debug code below
    #
    # for cname in sorted(elts):
    #     print('"{}", "{}", "{}", "{}"'.format(cname, elts[cname][0], elts[cname][1], elts[cname][2]))


class VarDesc:
    # Note that num_entries is a string (not an integer) because it might
    # actually be some enum which the C compiler will turn into an integer
    # later.
    def __init__(self):
        self.structure = ''
        self.codec_name = ''
        self.impl_name = ''
        self.dimensions = []


def add_var(var, vars, vars_path, line_num):
    if var.structure == '':
        raise RuntimeError('{}:{}: Variable is missing a structure.'
                           .format(vars_path, line_num + 1))

    if var.codec_name == '':
        raise RuntimeError('{}:{}: Variable is missing a codec name.'
                           .format(vars_path, line_num + 1))

    vars.append(var)


def read_vars(vars_path, release_mode):
    '''Read the cfg_vars from the given path.

    The result is a list of variable descriptions (in the same order as
    the input file)

    Each variable block has the following form:

    codec_var_name [NUM_ENTRIES_0][NUM_ENTRIES_1]...
      USE:   alt_codec_var_name_0 alt_codec_var_name_1 ...
      STRUC: G
      IMPL:  impl_var_name
      DESC:  description of codec_var_name

    The array indices and any of the sub-block lines may be omitted.

    USE:   "You can't actually override codec_var_name. You should override
            alt_codec_var_name_i instead." (docs only)

    STRUC: indicates the global structure in which the variable resides

    IMPL:  a C++ expression for the implementation 'name'

    DESC:  a description of the element (docs only). Can spill onto
           several lines by ending a line with a \.

    NUM_ENTRIES_i: C++ expressions for the number of elements

    ARGON-ONLY: Must be 'true' or 'false'. If true, this will be
                omitted from release builds.

    In the code below, any blocks containing a USE entry are ignored. In
    addition, all descriptions are ignored.

    '''

    desc_spilled_over = False
    ignore_var = False

    vars = []
    with io.open(vars_path, 'r', encoding='utf-8') as vars_file:
        var = None
        var_start_line = 0
        for line_num, line in enumerate(vars_file):
            line = line.split('#', 1)[0]

            # If there's nothing on this line, skip it
            if len(line.strip()) == 0:
                desc_spilled_over = False
                continue

            if line[0].isspace() or desc_spilled_over:
                # We're inside a definition block
                if var is None:
                    raise RuntimeError('{}:{}: Expecting a variable name.'
                                       .format(vars_path, line_num + 1))

                if desc_spilled_over:
                    condition = 'DESC'
                    value = line.strip()
                else:
                    condition, value = [x.strip()
                                        for x in line.strip().split(':', 1)]

                if condition == 'DESC':
                    # Ignore this condition. The next line is also a
                    # DESC line iff the current line ends on a '\'
                    desc_spilled_over = (value[-1] == '\\')
                elif condition == 'STRUC':
                    if value not in ['G', 'F', '_']:
                        raise RuntimeError('{}:{}: Unknown structure: `{}\'.'
                                           .format(vars_path, line_num + 1,
                                                   value))
                    var.structure = value
                elif condition == 'IMPL':
                    var.impl_name = value
                elif condition == 'USE':
                    ignore_var = True
                elif condition == 'ARGON-ONLY':
                    if value == 'true':
                        if release_mode:
                            ignore_var = True
                    elif value == 'false':
                        pass
                    else:
                        raise RuntimeError('{}:{}: Bad value for ARGON-ONLY '
                                           '(should be \'true\' or \'false\').'
                                           .format(vars_path, line_num + 1))
                else:
                    raise RuntimeError('{}:{}: unknown condition: `{}\'.'
                                       .format(vars_path,
                                               line_num + 1, condition))
            else:
                # We're at the top level
                if not ignore_var and var is not None:
                    add_var(var, vars, vars_path, var_start_line)

                var = VarDesc()
                var.codec_name = line.split('[')[0].strip()
                var.dimensions = [x.strip()
                                  for x in re.findall(r'\[([^\]]*)\]',
                                                      line.strip())]
                var.impl_name = var.codec_name
                ignore_var = False
                var_start_line = line_num

        if not ignore_var and var is not None:
            add_var(var, vars, vars_path, var_start_line)

    return vars

    # Temporary debug code below
    #
    # for var in vars:
    #     print('"{}", "{}"'.format(var.codec_name, var.impl_name))
    #     for dim in var.dimensions:
    #         print('    "{}"'.format(dim))


def read_constants(constants_path):
    '''Read the constants from the given path.

    The result is a dict keyed by name with elements that are numerical values
    (in the same order as the input file).

    '''
    min_value = - (1 << 63)
    max_value = (1 << 63) - 1

    ret = {}
    with io.open(constants_path, 'r', encoding='utf-8') as constants_file:
        for linum, line in enumerate(constants_file):
            line = line.split('#', 1)[0].strip()
            if not line:
                continue

            fields = [part.strip() for part in line.split(':')]
            if len(fields) != 2:
                raise RuntimeError('{}:{}: Line not of form NAME: CONSTANT.'
                                   .format(constants_path, linum + 1))

            name = fields[0]
            try:
                value = int(fields[1])
            except ValueError:
                raise RuntimeError("{}:{}: Value for `{}' is `{}' "
                                   "which doesn't parse as an integer."
                                   .format(constants_path, linum + 1,
                                           name, fields[1]))

            if name in ret:
                raise RuntimeError("{}:{}: Duplicate value for `{}'."
                                   .format(constants_path, linum + 1, name))

            if value < min_value or max_value < value:
                raise RuntimeError("{}:{}: Value for `{}' is {}, "
                                   "which doesn't fit in a 64 bit integer."
                                   .format(constants_path, linum + 1,
                                           name, value))

            ret[name] = value
    return ret


def read(groups_path, elements_path, vars_path, constants_path, release_mode):
    '''Read groups and elements

    Either groups_path can be None or both groups_path and elements_path can be
    None. vars_path may be none.

    '''
    groups = (read_groups(groups_path)
              if groups_path is not None
              else dict())
    elts = (read_elements(elements_path, groups, release_mode)
            if elements_path is not None
            else dict())
    variables = (read_vars(vars_path, release_mode)
                 if vars_path is not None
                 else [])
    constants = (read_constants(constants_path)
                 if constants_path is not None
                 else [])

    return (groups, elts, variables, constants)
