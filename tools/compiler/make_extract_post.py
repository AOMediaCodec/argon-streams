################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar
# For Python we need to make ++ and -- into separate statements if they live within assignments

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> e

function = ['fn' [expr1:xs] [:name :side :ret] ] -> 'fn',[xs],[name,side,ret]
         | :notfn -> notfn

expr1 =  [('=' | '+='):op [~~expr2:lhs2 expr0:lhs ~~expr2:rhs2 expr0:rhs] [] ] -> 'block',[ [op,[lhs,rhs],[] ] , lhs2, rhs2 ],[]
     | ['element' [~~expr2:post expr0:a expr1:w] [:style] ] -> 'block',[ ['element',[a,w],[style]] , post],[]
     | [:op [expr1*:xs] anything:side] -> (op,xs,side)

expr2 = [('post++' | 'post--'):op [expr1:a] []] -> op,[a],[]
     | [:op [expr2*:xs] anything:side] -> 'block',xs,[]

expr0 = [('post++' | 'post--'):op [expr1:a] []] -> a
     | [:op [expr0*:xs] anything:side] -> op,xs,side

""","extract_post","pure_ast")

