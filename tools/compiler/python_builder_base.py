################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import cPickle,sys
from bitstream import builtin_functions
from bitstream import builtin_statements

def make_fn(name,xs,decls):
    return ('def hevc_parse_'+name+' ( b,' + ', '.join(decls)+' ):\n'+
            '  p = Object()\n'+
            xs+
            '  return p'+
            '\n\n')

def make_dict_unused(dims):
    """Generate a multi dimensional dictionary with the given number of dimensions"""
    N=len(dims)
    if N==0:
        return '0'
    if N==1:
        return 'dict()'
    if N==2:
        return 'defaultdict(dict)'
    if N==3:
        return 'defaultdict(D2)'
    if N==4:
        return 'defaultdict(D3)'
    if N==5:
        return 'defaultdict(D4)'
    assert N==6
    return 'defaultdict(D5)'

def make_dict(dims):
    """Generate a multi dimensional dictionary with the given number of dimensions"""
    N=len(dims)
    if N==0:
        return '0'
    if N==1:
      return 'checked_dict('+dims[0]+')'
    if all(x=='' for x in dims[1:]):
      return 'checked_defaultdict(['+dims[0]+','+(','.join('None' for d in dims[1:]))+'])'
    return 'checked_defaultdict(['+','.join(dims)+'])'

def make_ChooseAll(ind):
  indbits=ind.rpartition(',')
  return 'b.ChooseAll({},"{}")'.format(indbits[0],indbits[2].strip())

assert sys.argv[-2]=='-o'
with open(sys.argv[-5],'rb') as fd:
    A=cPickle.load(fd)

with open(sys.argv[-5][:-len('py_ids')]+'struct','rb') as fd:
    field_to_fn,defined_fields,call_to_fn,id_to_type,id_to_array_dimensionality = cPickle.load(fd)

assert sys.argv[-4]=='--encode'
is_encode = int(sys.argv[-3])

B=default_parser(A).apply("start")[0]

with open(sys.argv[-1],'wb') as out:
    print >>out,B
