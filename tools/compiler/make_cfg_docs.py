#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Code for auto-generating config language documentation'''

import argparse
import cfg
import csv
import io
import os
import re

html_header = '<html>' \
              '    <head>' \
              '        <link href="https://fonts.googleapis.com/css?family=Inconsolata:400,700" rel="stylesheet">' \
              '        <link rel="stylesheet" type="text/css" href="cfg_docs.css">' \
              '    </head>' \
              '    <body>' \
              '        <table>'

html_footer = '        </table>' \
              '    </body>' \
              '</html>'

elts_table_header = '<thead>' \
                    '    <td class="nowrap">Section number</td>' \
                    '    <td class="nowrap">Overridable Element Name</td>' \
                    '    <td>Details</td>' \
                    '</thead>'

vars_table_header = '<thead>' \
                    '    <td class="nowrap">Global Variable</td>' \
                    '    <td class="nowrap">Details</td>' \
                    '</thead>'

consts_table_header = '<thead>' \
                      '    <td class="nowrap">Symbol Name</td>' \
                      '    <td class="nowrap" style="text-align: center;">Value</td>' \
                      '</thead>'


def gen_consts_docs(consts_path, consts_docs_path):
    '''Generate an HTML table of configuration constants from
    the constants in the consts_path.

    The result is an HTML table with columns:
        <constant_name>, <value>

    Each 'constant' line has the following form:

    <constant_name>: <value>
    '''

    consts = []
    with io.open(consts_path, 'r', encoding='utf-8') as consts_file:
        for line_num, line in enumerate(consts_file):
            line = line.split('#', 1)[0]

            # If there's nothing on this line, skip it
            if len(line.strip()) == 0:
                continue

            const_pair = [x.strip() for x in line.strip().split(':')]
            if len(const_pair) != 2:
                raise RuntimeError('{}:{}: Expecting "<constant_name>: <value>".'
                                   .format(consts_path, line_num + 1))

            consts.append(const_pair)

    with io.open(consts_docs_path, 'w', newline='') as consts_docs_file:

        consts_docs_file.write(html_header)
        consts_docs_file.write(consts_table_header)

        for const_ in consts:
            name = '<code class="green">{}</code>'.format(const_[0])
            value = '<code>{}</code>'.format(const_[1])

            consts_table_row = '<tr>' \
                               '    <td class="nowrap">{}</td>' \
                               '    <td style="text-align: center;">{}</td>' \
                               '</tr>'.format(name, value)

            consts_docs_file.write(consts_table_row)


class VarDesc:
    def __init__(self, codec_name, dimensions):
        self.codec_name = codec_name
        self.dimensions = dimensions
        self.use = ''
        self.description = ''
        self.invisible = False


def gen_vars_docs(vars_path, vars_docs_path):
    '''Generate an HTML table of configuration variables from
    the variables in vars_path.

    The result is an HTML table with columns:
        <variable_name>, <details>

    Each variable block has the following form:

    codec_var_name [NUM_ENTRIES_0][NUM_ENTRIES_1]...
      USE:   alt_codec_var_name_0 alt_codec_var_name_1 ...
      STRUC: G
      IMPL:  impl_var_name
      DESC:  description of codec_var_name

    The array indices and any of the sub-block lines may be omitted.

    USE:   "You can't actually override codec_var_name. You should override
            alt_codec_var_name_i instead."

    STRUC: ignore here

    IMPL:  ignore here

    DESC:  a description of the element. Can spill onto
           several lines by ending a line with a \.

    NUM_ENTRIES_i: C++ expressions for the number of elements

    We expect that blocks containing a USE entry contain no DESC entry.

    The final <details> contains either the DESC information or the USE information.

    '''

    desc_spilled_over = False

    vars = []
    with io.open(vars_path, 'r', encoding='utf-8') as vars_file:
        var = None
        for line_num, line in enumerate(vars_file):
            line = line.split('#', 1)[0]

            # If there's nothing on this line, skip it
            if len(line.strip()) == 0:
                desc_spilled_over = False
                continue

            if line[0].isspace() or desc_spilled_over:
                # We're inside a definition block
                if var is None:
                    raise RuntimeError('{}:{}: Expecting a variable name.'
                                       .format(vars_path, line_num + 1))

                if desc_spilled_over:
                    condition = 'DESC'
                    value = line.strip()
                else:
                    condition, value = [x.strip() for x in line.strip().split(':', 1)]

                if condition == 'DESC':
                    if len(value) == 0:
                        desc_spilled_over = False
                    else:
                        desc_spilled_over = (value[-1] == '\\')
                        if desc_spilled_over:
                            var.description += value[:-1]
                        else:
                            var.description += value

                elif condition == 'STRUC':
                    # ignore this condition
                    pass
                elif condition == 'IMPL':
                    # ignore this condition
                    pass
                elif condition == 'USE':
                    var.use = value
                elif condition == 'ARGON-ONLY':
                    var.invisible = True
                else:
                    raise RuntimeError('{}:{}: unknown condition: `{}\'.'
                                       .format(vars_path,
                                               line_num + 1, condition))
            else:
                # We're at the top level
                if var is not None and not var.invisible:
                    vars.append(var)

                var = VarDesc(line.split('[')[0].strip(),
                              [x.strip() for x in re.findall(r'\[([^\]]*)\]',
                                                             line.strip())])

        if var is not None and not var.invisible:
            vars.append(var)

    vars.sort(key=lambda v: v.codec_name)

    with io.open(vars_docs_path, 'w', newline='') as vars_docs_file:

        vars_docs_file.write(html_header)
        vars_docs_file.write(vars_table_header)

        for var in vars:
            cname = '{} '.format(var.codec_name)
            for dim in var.dimensions:
                cname += '[ {} ]'.format(dim)
            cname = cname.strip()

            details = var.description

            # Convert <...> to text in a <code> tag
            details = re.sub(
                r'<([A-Za-z0-9_\(\)\[\]\{\}]([-+/\*A-Za-z0-9_\(\)\[\]\{\} ]*[A-Za-z0-9_\(\)\[\]\{\}])?)>',
                r'<code>\1</code>', details)

            if var.use != '':
                cname = '({})'.format(cname)

                if var.use.strip() != 'N/A':
                    details = '<strong>See instead: </strong>'

                    alts = var.use.split()
                    for alt in alts:
                        if "," in alt:
                            raise RuntimeError('{}: "{}" is not a valid C-style alternative variable.'
                                               .format(vars_path, alt))
                        details += '<code>{}</code>, '.format(alt)

                    details = details[:-2]

            cname = '<code class="green">{}</code>'.format(cname)

            vars_table_row = '<tr>' \
                             '    <td class="nowrap">{}</td>' \
                             '    <td>{}</td>' \
                             '</tr>'.format(cname, details)

            vars_docs_file.write(vars_table_row)


class EltDesc:
    def __init__(self, section_number, codec_name):
        self.section_number = section_number
        self.codec_name = codec_name
        self.use = ''
        self.description = ''
        self.default = ''
        self.locals = []
        self.invisible = False


def gen_elts_docs(elts_path, groups, elts_docs_path):
    '''Generate an HTML table of bit-stream elements from
    the elements in elts_path.

    The result is an HTML table with the following columns:
        - <spec_section_number>, <overridable_element_name>, <details>

    Each element block has the following form:

    codec_name
      USE:   alt_codec_name_0 alt_codec_name_1 ...
      IMPL:  impl_name
      GROUP: group_name
      DESC:  description of codec_name
      DFLT:  description of default value

    Any of the sub-block lines may be omitted.

    USE:   "You can't actually override codec_name. You should override
            alt_codec_name_i instead."

    IMPL:  ignore here

    GROUP: either None or the name of a group (from cfg_groups.txt)

    DESC:  a description of the element. Can spill onto
    several lines by ending a line with a \.

    DFLT:  a single-line description of the element's default value.

    Blocks containing a USE entry may also contain a DESC entry.

    The final <details> contains the information from the DESC, USE and GROUP entries.

    <available_local_variables> is populated using the read_groups () function from cfg.py.

    '''

    desc_spilled_over = False

    section_number = None

    elts = []
    with io.open(elts_path, 'r', encoding='utf-8') as elts_file:
        elt = None
        for line_num, line in enumerate(elts_file):
            # Remove comments
            line = line.split('#', 1)[0]

            # Get section_number
            if line[:3] == '-- ':
                section_number = line[3:].split(':')[0]
                line = ''

            # If there's nothing on this line, skip it
            if len(line.strip()) == 0:
                desc_spilled_over = False
                continue

            if line[0].isspace() or desc_spilled_over:
                # We're inside a definition block
                if elt is None:
                    raise RuntimeError('{}:{}: Expecting an element name.'
                                       .format(elts_path, line_num + 1))

                if desc_spilled_over:
                    condition = 'DESC'
                    value = line.strip()
                else:
                    condition, value = [x.strip() for x in line.strip().split(':', 1)]

                if condition == 'DESC':
                    if len(value) == 0:
                        desc_spilled_over = False
                    else:
                        desc_spilled_over = (value[-1] == '\\')
                        if desc_spilled_over:
                            elt.description += value[:-1]
                        else:
                            elt.description += value
                elif condition == 'IMPL':
                    # ignore this condition
                    pass
                elif condition == 'GROUP':
                    if value not in groups:
                        raise RuntimeError("{}:{}: Element `{}' belongs to group "
                                           "`{}', which isn't one of the {} groups we "
                                           "have read."
                                           .format(elts_path, line_num + 1,
                                                   elt.codec_name, value, len(elts)))

                    elt.locals = []
                    for cname, iname in groups[value]:
                        elt.locals.append(cname)
                elif condition == 'USE':
                    elt.use = value
                elif condition == 'DFLT':
                    elt.default = value
                elif condition == 'ARGON-ONLY':
                    elt.invisible = True
                else:
                    raise RuntimeError('{}:{}: unknown condition: `{}\'.'
                                       .format(elts_path,
                                               line_num + 1, condition))
            else:
                # We're at the top level
                if elt is not None and not elt.invisible:
                    elts.append(elt)

                elt = EltDesc(section_number,
                              line.split('[')[0].strip())

        if elt is not None and not elt.invisible:
            elts.append(elt)

    with io.open(elts_docs_path, 'w', newline='') as elt_docs_file:
        elt_docs_file.write(html_header)

        elt_docs_file.write(elts_table_header)

        for elt in elts:

            details = elt.description
            cname = elt.codec_name

            # Convert <...> to text in a <code> tag
            details = re.sub(
                r'<([A-Za-z0-9_\(\)\[\]\{\}]([^>]*))>',
                r'<code>\1</code>', details).strip()

            if details != '':
                details = '<p>{}</p>'.format(details)

            if elt.use != '':
                cname = '({})'.format(cname)

                if elt.use.strip() != 'N/A':
                    alts_text = ''
                    alts = elt.use.split()
                    for alt in alts:
                        if "," in alt:
                            raise RuntimeError('{}: "{}" is not a valid C-style alternative element.'
                                               .format(elts_path, alt))
                        alts_text += ' <code>{}</code>,'.format(alt)
                    alts_text = alts_text[:-1]

                    details += '<p><strong>See instead:</strong> {}</p>'.format(alts_text)

            if elt.default != '':
                dflt = elt.default

                # Change keywords to bold
                dflt = re.sub(
                    r'(choose|min|max|clamp|choose_bits|choose_bit|choose_bernoulli|uniform|all|seq|weighted) *\(',
                    r'<strong>\1</strong> (', dflt)
                dflt = re.sub(r' weight ', r' <strong>weight</strong> ', dflt)
                dflt = re.sub(r' step ', r' <strong>weight</strong> ', dflt)

                details += '<p><strong>Default:</strong> <code>{}</code></p>'.format(dflt)

            # Section Number
            sec_num = elt.section_number

            # Bit-stream element (Codec Name)
            cname = '<code class="green">{}</code>'.format(cname)

            # Available local variables
            locals_ = ''
            for local in elt.locals:
                locals_ += '<code>{}</code>, '.format(local)
            if len(locals_) >= 2:
                locals_ = locals_[:-2]

            if locals_ != '':
                details += '<p><strong>Local variables:</strong> {}</p>'.format(locals_)

            elts_table_row = '<tr>' \
                             '    <td class="nowrap">{}</td>' \
                             '    <td class="nowrap">{}</td>' \
                             '    <td>{}</td>' \
                             '</tr>'.format(sec_num, cname, details)

            elt_docs_file.write(elts_table_row)

            if elt.use != '' and locals_ != '':
                raise RuntimeError("{}: Element `{}' has a USE entry and available local variables!"
                                   .format(elts_path, elt.codec_name))

        elt_docs_file.write(html_footer)


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('cfg_groups_path', help='Path for the file containing the list of configuration groups')
    parser.add_argument('cfg_elts_path', help='Path for the file containing the list of bit-stream elements')
    parser.add_argument('cfg_vars_path', help='Path for the file containing the list of global variables')
    parser.add_argument('cfg_consts_path', help='Path for the file containing the list of constants')

    parser.add_argument('output_dir', help='Output directory for CSVs')

    args = parser.parse_args()

    # make output directory if it doesn't yet exist
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    groups = cfg.read_groups(args.cfg_groups_path)
    gen_elts_docs(args.cfg_elts_path, groups, os.path.join(args.output_dir, 'cfg_elements.docs.html'))
    gen_vars_docs(args.cfg_vars_path, os.path.join(args.output_dir, 'cfg_vars.docs.html'))
    gen_consts_docs(args.cfg_consts_path, os.path.join(args.output_dir, 'cfg_constants.docs.html'))


if __name__ == '__main__':
    exit(main())
