################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import sys
import os
import cPickle

from bitstream import builtin_functions
from bitstream import convert_type

def error_out(s):
    print s
    assert False

def disp(x):
    print "DISP: {}".format(x)
    return x

def make_fn(name,decls,ret):
    s_decls = ','.join(convert_type(t)+' '+x for t,x in decls)
    if s_decls: s_decls=','+s_decls
    return (convert_type(ret)+' hevc_parse_'+name+' ( TOPLEVEL_T *b,' + name.upper()+'_T *p' + s_decls +' ); \n\n')

def make_fn2(name,xs,decls,ret):
    # TODO make this only work when a define/env variable is set...
    defines = ''
    declares = ''
    constructor = ''

    if len(xs):
        for x in xs.split("\n"):
            if len(x):
                sp = x
                sp = sp.lstrip(" ").rstrip(";").split(" ")
                typ = sp[0]
                var = sp[-1].split("[")[0]
                dims = sp[-1].lstrip(var)

                numDims = dims.count('[')

                if numDims > 0:
                    isLateAlloc = all(x in '[]' for x in dims)
                    isArrayOfArrays = (not isLateAlloc) and numDims > 1 and all(x in '[]' for x in dims.partition(']')[2])
                    if isArrayOfArrays:
                        declares += '   flat_array<flat_array<' + typ + ', ' + str(numDims-1) + '>, 1> ' + var + ';\n'
                    #elif not isLateAlloc:
                    #    declares += '   ' + typ + ' ALIGN16 ' + var + dims + ';\n'
                    else:
                        declares += '   flat_array<' + typ + ', ' + str(numDims) + '> ' + var + ';\n'


                    if not isLateAlloc:
                        if isArrayOfArrays:
                            constructor += '      ' + var + '.alloc({'+dims[1:].partition(']')[0]+'});'+'\n'
                        else:
                            constructor += '      ' + var + '.alloc({'+dims[1:-1].replace("][", ", ")+'});'+'\n'
                else:
                    declares += '   '+typ+' '+var+';\n'

    output = 'typedef struct hevc_parse_'+name+'_s {\n'
    output += declares

    output += '\n   hevc_parse_'+name+'_s() {\n'
    output += constructor
    output += '   }\n'
    output += '} '+name.upper()+'_T; \n\n'
    return (output)

assert sys.argv[-2]=='-o'
with open(sys.argv[-3],'rb') as fd:
    A=cPickle.load(fd)

B=default_parser(A).apply("start")[0]

# Turn my-output.h to MY_OUTPUT_H
output_base = os.path.basename(sys.argv[-1])
output_base = output_base.replace('-', '_').replace('.', '_').upper()

with open(sys.argv[-1], 'wb') as out:
    out.write('#ifndef {}\n'.format(output_base))
    out.write('#define {}\n\n'.format(output_base))
    print >>out, B
    out.write('#endif //{}\n'.format(output_base))
