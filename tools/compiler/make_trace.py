################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar
# Trace
#
# This adds a function call trace(name,x=2+4) for each assignment.
# The purpose is to allow us to generate a trace file for all variable assignments to help with debugging.

pymeta3.grammar.OMeta.makeCode(r"""

start = function+

function = ['fn' [statement:x] :side] -> 'fn',[ x ],side
         | anything

num = ['num' [] [:x]] -> x

justname = ['id' [] [:name]] -> name
         | ['array' [justname:name anything] :side] -> name

describe = [('id'|'num'|'string') [] [anything:x]] -> str(x)
     | ['call' [describe:a describe:ind] [] ] -> a+'('+ind+')'
     | ['arglist' [describe*:a] [] ] -> ', '.join(a)
     | ['array' [describe:a describe:ind] [] ] -> a+'['+ind+']'
     | ['=' [describe:a describe:b] :side ] -> a+'='+b
     | [:op [describe:a describe:b] :side ] -> '('+a+str(op)+b+')'
     | ['?' [describe:a describe:b describe:c] :side] -> a+'?'+b+':'+c
     | ['unary-' [describe:a] [] ] -> '(-'+a+')'
     | ['unary!' [describe:a] [] ] -> '(!'+a+')'
     | ['COVERCLASS' [describe:a] :side] -> a
     | :s -> 'unknown('+str(s)+')'

statement = ~~anything:e ['=' [describe:lhs :rhs] :side] -> 'call',[('id',[],['trace']),('arglist',[('string',[],[lhs]),e],[])],[]
          | [:op [statement*:xs] :side] -> op,xs,side

""","trace","pure_ast")

