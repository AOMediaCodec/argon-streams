################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import pymeta3.grammar

pymeta3.grammar.OMeta.makeCode(r"""
start = function+:e -> e

decl = :name -> name

id = ['id' [] [:name] ] -> name

expr = ['num' [] [:val] ] -> val
     | ['*' [expr:a expr:b] []] -> a*b

array1 = ['array' [id:name expr:index] []] -> name,index

array2 = ['array' [array1:n expr:index] [] ] -> n+(index,)

init1 = ['init' [expr*:vals] []] -> vals

init2 = ['init' [init1*:V] []] -> V

function = ['def' [array2:n init2:i] :type ] -> cabac_field(n,i)
         | anything:a -> disp('unknown'+str(a))

""","extract_cabac_init")

