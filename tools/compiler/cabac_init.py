################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import cPickle,sys

assert sys.argv[-2]=='--builddir'
builddir=sys.argv[-1]

with open(builddir+'/ContextTables.gen.cabacinit','rb') as fd:
    CABAC_TO_CONTEXT,CABAC_INIT,NUM_CONTEXTS = cPickle.load(fd)

print 'int NUM_CONTEXTS = %d;' % NUM_CONTEXTS
print 'unsigned short CABAC_INIT[3][%d] = {'%NUM_CONTEXTS
for i in range(3):
  print '{',','.join(str(d) for d in CABAC_INIT[i]),'},'
print '};'
