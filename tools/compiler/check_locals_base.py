################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import cPickle,sys
from bitstream import builtin_functions
from cStringIO import StringIO
import os

def make_fn(name,local_ids,decls,ret):
    global writes
    global reads
    global badReads
    global errorsFound
    global defined_scalars
    global defined_arrays

    unread = writes - reads
    unused = local_ids - writes
    unused -= reads

    for br in badReads:
        print "CHECK_LOCALS: {}:{}(): variable {} read before being written".format(currentFile, name, br)
        errorsFound = True
    for u in unread:
        print "CHECK_LOCALS: {}:{}(): variable {} written but never read".format(currentFile, name, u)
        errorsFound = True
    for u in unused:
        print "CHECK_LOCALS: {}:{}(): local id {} never written or read".format(currentFile, name, u)
        errorsFound = True

    writes = set()
    reads = set()
    badReads = set()
    defined_scalars = set()
    defined_arrays = set()

    return ""

import pdb

def add_local_read_write(expr_param, x):
    expr_cat = expr_param[0]
    array_dims = expr_param[1]
    is_array = any(nm == x and dims > array_dims for nm, dims in defined_arrays)
    if expr_cat == 0 or expr_cat == 2 or expr_cat == 3:
        reads.add(x)
    if expr_cat == 0 or expr_cat == 2 or (expr_cat == 3 and not is_array):
        if x not in writes:
            badReads.add(x)
    if expr_cat == 1 or expr_cat == 2 or (expr_cat == 3 and is_array):
        writes.add(x)

def disp(a):
  print a

fname = sys.argv[-1][:-6] + ".check_locals"

reads = set()
writes = set()
badReads = set()
defined_scalars = set()
defined_arrays = set()

currentFile = "{}.c".format(os.path.basename(sys.argv[-1])[:-11])
errorsFound = False

sys.setrecursionlimit(15000)
with open(sys.argv[-1],'rb') as fd:
    A=cPickle.load(fd)

default_parser(A).apply("start")[0]

#if errorsFound:
#    sys.exit(1)

