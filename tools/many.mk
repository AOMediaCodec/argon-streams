################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This Makefile should be used when you want to build more than one
# tool at a time and exploit parallelism between builds.

# We expect the user to specify this on the command line.
TARGETS :=

ifeq ($(TARGETS),)
$(error You must specify TARGETS on the command line)
endif

# These flags get passed through to the sub-Makes.
passed-flags := \
  P8005_BUILD_PREFIX \
  RELEASE \
  CFLAGS \
  LDFLAGS

passed-flag-args := \
  $(foreach f,$(passed-flags),$(if $($(f)),$(f)="$($(f))",))

c-build-targets := $(addprefix c-build-,$(TARGETS))

.PHONY: c-build
c-build: $(c-build-targets)

# mf is the "real" tools/Makefile. The last word of MAKEFILE_LIST is
# the path from the current directory to this Makefile, so using that
# directory and adding Makefile gives tools/Makefile no matter where
# CWD is at the moment.
mf := $(dir $(lastword $(MAKEFILE_LIST)))Makefile

# This phony "compiler" job makes sure that we've built each
# "foo.gen.py" before we start actually building anything important.
# This avoids a nasty race, because all the c-build-targets depend on
# building those files in the same directory.
.PHONY: compiler-build
compiler-build:
	$(MAKE) -f $(mf) $(passed-flag-args) compiler-build

.PHONY: $(c-build-targets)
$(c-build-targets): c-build-%: compiler-build
	$(MAKE) -f $(mf) TARGET=$* $(passed-flag-args) c-build
