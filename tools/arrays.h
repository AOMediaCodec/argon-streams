/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <cstddef>
#include <array>
#include <memory>
#include <cassert>

#define ARRAY_BOUNDS_CHECK 0

#if ARRAY_BOUNDS_CHECK
# define CHECK_BOUNDS(i, len)       assert ((i) < (len))
# define CHECK_BOUNDS_EQ(len0, len) assert ((len0) = (len))
#else
# define CHECK_BOUNDS(i, len)
# define CHECK_BOUNDS_EQ(len0, len)
#endif

template <typename T, size_t D>
class flat_subarray
{
private:
    T            *data_;
    const size_t *strides;
    const size_t *sizes;

public:
    flat_subarray(T* data, const size_t *strides, const size_t *sizes)
        : data_ (data), strides (strides), sizes (sizes)
    {}

    flat_subarray<T, D-1> operator[](size_t i) {
        CHECK_BOUNDS (i, sizes[0]);
        return flat_subarray<T, D-1>(&(data_[i*strides[0]]), &(strides[1]), &(sizes[1]));
    }

    flat_subarray<T, D> &operator=(flat_subarray<T, D> o) {
        CHECK_BOUNDS_EQ (sizes[0], o.sizes[0]);
        for (size_t i=0; i<sizes[0]; i++) {
            (*this)[i] = o[i];
        }
        return *this;
    }

    T *data ()             { return data_; }
    const T *data () const { return data_; }

    const size_t *lengths () const { return sizes; }

    // Copy data from src
    void take (flat_subarray<const T, D> src) {
        CHECK_BOUNDS(src.lengths ()[0], sizes[0] + 1);
        for (size_t i = 0; i < sizes[0]; ++ i) {
            (* this)[i].take (src [i]);
        }
    }

    // Make a constant view of a "box" subset of this array or subarray
    flat_subarray<const T, D>
    const_box (const std::array<size_t, D> &lens) const {
        // Check that the box lengths are actually a subset of this
        // subarray.
        for (size_t d = 0; d < D; ++ d) {
            CHECK_BOUNDS (lens [d], sizes [d] + 1);
        }

        return flat_subarray<const T, D> (data_, strides, & lens [0]);
    }
};

template <typename T>
class flat_subarray<T, 1>
{
private:
    T*     data_;
    size_t size;

public:
    flat_subarray(T* data, const size_t *strides, const size_t *sizes)
        : data_ (data), size (*sizes)
    {}

    T& operator[](size_t i) {
        CHECK_BOUNDS (i, size);
        return data_[i];
    }
    operator T*() { return data_; }

    flat_subarray<T, 1> &operator=(flat_subarray<T, 1> o) {
        CHECK_BOUNDS_EQ (size, o.size);
        for (size_t i=0; i<size; i++) {
            data_[i] = o[i];
        }
        return *this;
    }

    T *data ()             { return data_; }
    const T *data () const { return data_; }

    const size_t *lengths () const { return & size; }

    // Copy data from src
    void take (const flat_subarray<const T, 1> &src) {
        CHECK_BOUNDS (src.lengths ()[0], size + 1);
        memcpy (data_, src.data (), sizeof (T) * src.lengths()[0]);
    }
};

template <typename T, size_t D>
struct flat_array_initializer_list {
    using t = std::initializer_list<typename flat_array_initializer_list<T, D-1>::t>;

    static void get_extents(const t &l, size_t *out_s) {
        out_s[0] = std::max(out_s[0], l.size());
        for (auto p = l.begin(); p != l.end(); p++)
            flat_array_initializer_list<T, D-1>::get_extents(*p, out_s+1);
    }

    static void get_contents(const t &l, T *out_data, size_t *strides) {
        auto p = l.begin();
        for (size_t i=0; i < l.size(); i++) {
            flat_array_initializer_list<T, D-1>::get_contents(p[i], out_data + (i*strides[0]), strides+1);
        }
    }
};

template <typename T>
struct flat_array_initializer_list<T, 1> {
    using t = std::initializer_list<T>;

    static void get_extents(const t &l, size_t *out_s) {
        out_s[0] = std::max(out_s[0], l.size());
    }

    static void get_contents(const t &l, T *out_data, size_t *strides) {
        auto p = l.begin();
        for (size_t i=0; i < l.size(); i++) {
            out_data[i] = p[i];
        }
    }
};

template <typename T, size_t D>
class flat_array
{
private:
    std::array<size_t, D-1> strides;
    std::array<size_t, D>   sizes;
    std::unique_ptr<T[]>    data_;

public:
    flat_array() {}

    flat_array(typename flat_array_initializer_list<T, D>::t l) {
        std::array<size_t, D> il_extents;
        il_extents.fill(0);
        flat_array_initializer_list<T, D>::get_extents(l, &(il_extents[0]));

        alloc(il_extents);

        flat_array_initializer_list<T, D>::get_contents(l, data_.get(), &(strides[0]));
    }

    void alloc(std::array<size_t, D> extents) {
        sizes = extents;
        strides[D-2] = extents[D-1];
        for (size_t i = 0; i < D-2; i++) {
            size_t j = D-3-i;
            strides[j] = extents[j+1]*strides[j+1];
        }
        data_.reset(new T[strides[0]*extents[0]]);
    }

    void free() {
        data_.reset();
    }

    flat_subarray<T, D-1> operator[](size_t i) {
        CHECK_BOUNDS (i, sizes[0]);
        return flat_subarray<T, D-1>(&(data_[i*strides[0]]), &(strides[1]), &(sizes[1]));
    }

    flat_array<T, D> &operator=(flat_array<T, D> &o) {
        CHECK_BOUNDS_EQ (sizes [0], o.sizes [0]);
        for (size_t i=0; i<sizes[0]; i++) {
            (*this)[i] = o[i];
        }
        return *this;
    }

    T *data ()             { return data_.get (); }
    const T *data () const { return data_.get (); }

    const size_t *lengths () const { return sizes.data (); }

    void set (T value) {
        size_t total_size = strides[0] * sizes[0];
        for (size_t i = 0; i < total_size; ++ i) {
            data_.get()[i] = value;
        }
    }
};

template <typename T>
class flat_array<T, 1>
{
private:
    size_t               size;
    std::unique_ptr<T[]> data_;

public:
    flat_array() {}

    flat_array(typename flat_array_initializer_list<T, 1>::t l) {
        alloc({l.size()});
        const T* il = l.begin();
        for (size_t i=0; i<l.size(); i++) {
            data_[i] = il[i];
        }
    }

    void alloc(std::array<size_t, 1> extents) {
        size = extents[0];
        data_.reset(new T[extents[0]]);
    }

    void free() {
        data_.reset();
    }

    T& operator[](size_t i) {
        CHECK_BOUNDS (i, size);
        return data_[i];
    }

    const T& operator[](size_t i) const {
        CHECK_BOUNDS (i, size);
        return data_[i];
    }

    operator T*() { return data_.get(); }

    flat_array<T, 1> &operator=(flat_array<T, 1> &o) {
        CHECK_BOUNDS_EQ (size, o.size);
        for (size_t i=0; i<size; i++) {
            data_[i] = o[i];
        }
        return *this;
    }

    T *data ()             { return data_.get (); }
    const T *data () const { return data_.get (); }

    const size_t *lengths () const { return & size; }

    void set (T value) {
        for (size_t i = 0; i < size; ++ i) {
            data_.get()[i] = value;
        }
    }
};

// Local Variables:
// mode: c++
// c-basic-offset: 2
// End:
