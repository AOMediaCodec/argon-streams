################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import av1_process
import argparse
import os

## Class just analyzes the contents of AV1 stream and prints out the result
class Av1NonAnnexBAnalyse(av1_process.Av1NonAnnexBParse):

  ## Function initializes Av1NonAnnexBAnalyse object
  # @param fname - name of the file containing AV1 stream to be converted
  # @param args - object with command line arguments parsed by argparse
  def __init__(self, fname, args):
    self.fname = fname
    super(Av1NonAnnexBAnalyse, self).__init__(fname, args) # open the stream so it can be parsed
    self.analyse()  # perform actual analysis

  ## Function analyses contents of input AV1 stream
  def analyse(self):
    self.dprint('=========================================')
    self.dprint('Analysing {:s} file'.format(self.fname))
    self.dprint('=========================================')
    self.dprint(' ')
    while True:
      d = self.parse_obu()
      end_of_stream = (self.stream.pos >= self.stream.length)
      if end_of_stream:
      # gracefully finish off processing
        break

    self.dprint(' ')

## Function removes OBU types that are not compatible with MP4 container
# Temporal delimiter, padding and redundant frame header OBUs will be removed
# @param l - the list of the contents of AV1 stream
def remove_non_mp4_obus(l):
  # first find which OBUs are not MP4 compatible ones
  i_list= []
  for i in range(len(l)):
    obu_type = l[i]['obu_type']
    if (  (obu_type == 'TEMPORAL_DELIMITER') or
          (obu_type == 'PADDING') or
          (obu_type == 'REDUNDANT_FRAME_HEADER')):
      i_list.append(i)

  # and delete them from the list
  for i in range(len(i_list)):
    i_to_del = i_list[i]
    del l[i_to_del - i]

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description = 'Script takes input AV1 stream and analyzes it. It can also compare two AV1 streams')
  parser.add_argument('av1_file', type = str, help = 'The name of the input AV1 file')
  parser.add_argument('-v', '--verbose', action = 'store_true', help = 'Enables verbose debugging output')
  parser.add_argument('-b', '--annex_b', action = 'store_true', help = 'Specifies that input AV1 stream is based on Annex B Length delimited bitstream format')
  parser.add_argument('-c', '--compare', type = str, default ='', help = 'The name of second AV1 file which will get compared to the first one')
  parser.add_argument('-s', '--skip_non_mp4', action = 'store_true', help = 'Enables skipping temporal delimiters, padding and redundant frame headers during comparison')
  parser.add_argument('-n', '--no_delete', action = 'store_true', help = 'Don\'t delete or rename intermediate files')
  args = parser.parse_args()

  # define file types suffixes
  NON_ANNEX_B_SUFFIX = '.nab'

  av1_1_convert_to_annexb = av1_process.Av1ConvertToNonAnnexB(args.av1_file, args, suffix = NON_ANNEX_B_SUFFIX)
  av1_1_non_annexb_analyse = Av1NonAnnexBAnalyse(av1_1_convert_to_annexb.fname_out, args)

  if not args.no_delete:
    av1_1_convert_to_annexb.cleanup_files()

  if (args.compare != ''):
  # user wants to compare two AV1 files
    av1_2_convert_to_annexb = av1_process.Av1ConvertToNonAnnexB(args.compare, args, suffix = NON_ANNEX_B_SUFFIX)
    av1_2_non_annexb_analyse = Av1NonAnnexBAnalyse(av1_2_convert_to_annexb.fname_out, args)
    if not args.no_delete:
      av1_2_convert_to_annexb.cleanup_files()

    # prepare everything for comparison
    av1_1_list = av1_1_non_annexb_analyse.av1_contents
    av1_2_list = av1_2_non_annexb_analyse.av1_contents

    if args.skip_non_mp4:
    # remove temporal delimiter, padding and redundant frame header OBUs from the lists
      remove_non_mp4_obus(av1_1_list)
      remove_non_mp4_obus(av1_2_list)

    av1_1_len = len(av1_1_list)
    av1_2_len = len(av1_2_list)

    # now compare AV1 files contents
    for i in range(min(av1_1_len, av1_2_len)):
      av1_1 = av1_1_list[i]
      type_1 = av1_1['obu_type']
      len_1 = av1_1['obu_len']

      av1_2 = av1_2_list[i]
      type_2 = av1_2['obu_type']
      len_2 = av1_2['obu_len']

      type_match = (type_1 == type_2)
      len_match = (len_1 == len_2)

      if (('temporal_id' in av1_1) != ('temporal_id' in av1_2)):
      # one AV1 stream has header extension, but the other doesn't
        raise ValueError('One AV1 stream has header extesion, but not the other')

      if ('temporal_id' in av1_1) and ('temporal_id' in av1_2):
        temporal_spatial_id_present = True
      else:
        temporal_spatial_id_present = False

      if (temporal_spatial_id_present == True):
        temporal_id_1 = av1_1['temporal_id']
        spatial_id_1 = av1_1['spatial_id']

        temporal_id_2 = av1_2['temporal_id']
        spatial_id_2 = av1_2['spatial_id']

        temporal_id_match = (temporal_id_1 == temporal_id_2)
        spatial_id_match = (spatial_id_1 == spatial_id_2)

      # everything is ready, so perform the comparison
      same = True
      if (temporal_spatial_id_present == True):
        if (type_match and len_match and temporal_id_match and spatial_id_match):
          if args.verbose:
            print ('OK   {:s}, len=0x{:X}, t={:d}, s={:d}'.format(type_1, len_1, temporal_id_1, spatial_id_1))
        else:
          same = False
          if args.verbose:
            print ('DIFF {:s}/{:s}, len=0x{:X}/0x{:X}, t={:d}/{:d}, s={:d}/{:d}'.format(type_1, type_2, len_1, len_2, temporal_id_1, temporal_id_2, spatial_id_1, spatial_id_2))
      else:
        if (type_match and len_match):
          if args.verbose:
            print ('OK   {:s}, len=0x{:X}'.format(type_1, len_1))
        else:
          same = False
          if args.verbose:
            print ('DIFF {:s}/{:s}, len=0x{:X}/0x{:X}'.format(type_1, type_2, len_1, len_2))

    print(' ')
    if (av1_1_len != av1_2_len):
      print('DIFF The streams contain different amount on OBUs !!!!')
    elif (same == False):
      print('DIFF The streams are different !!!')
    else:
      print('OK The streams are the same !!!')

