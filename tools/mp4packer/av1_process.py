################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

from bitstring import ConstBitStream, BitArray, Bits, pack, BitStream
from enum import Enum
import os
import shutil

# definitions of AV1 obu_type field
OBU_TYPE_SEQUENCE_HEADER =          1
OBU_TYPE_TEMPORAL_DELIMITER =       2
OBU_TYPE_FRAME_HEADER =             3
OBU_TYPE_TILE_GROUP =               4
OBU_TYPE_METADATA =                 5
OBU_TYPE_FRAME =                    6
OBU_TYPE_REDUNDANT_FRAME_HEADER =   7
OBU_TYPE_TILE_LIST =                8
OBU_TYPE_PADDING =                  15

# color primaries enum
CP_BT_709 =                         1
CP_UNSPECIFIED =                    2
CP_BT_470_M =                       4
CP_BT_470_B_G =                     5
CP_BT_601 =                         6
CP_SMPTE_240 =                      7
CP_GENERIC_FILM =                   8
CP_BT_2020 =                        9
CP_XYZ =                            10
CP_SMPTE_431 =                      11
CP_SMPTE_432 =                      12
CP_EBU_3213 =                       22

# transfer characteristic enum
TC_UNSPECIFIED =                    2
TC_SRGB =                           13

# matrix_coefficients enum
MC_IDENTITY =                       0
MC_UNSPECIFIED =                    2

# chroma sample position enum
CSP_UNKNOWN =                       0
CSP_VERTICAL =                      1
CSP_COLOCATED =                     2
CSP_RESERVED =                      3

# frame types
KEY_FRAME =                         0
INTER_FRAME =                       1
INTRA_ONLY_FRAME =                  2
SWITCH_FRAME =                      3

# other miscellaneous values
SELECT_SCREEN_CONTENT_TOOLS =       2
SELECT_INTEGER_MV =                 2
NUM_REF_FRAMES =                    8

## Class containting different utilities that allow for parsing Non Annex B AV1 streams
class Av1NonAnnexBParse(object):
  ## Function initializes Av1NonAnnexBParse object
  # @param fname - name of AV1 file which will be parsed
  # @param args - object with command line arguments parsed by argparse
  def __init__(self, fname, args):
    self.verbose = args.verbose
    self.state = {}
    self.stream = ConstBitStream(filename = fname)
    self.av1_contents = []

  ## Function prints string if verbose mode is set in command line arguments
  # @param string - string to be printed
  def dprint(self, string):
    if self.verbose:
      print(string)

  ## Function extracts show_existing_frame and show_frame flags from frame header
  # @param d - dictionary with parsed frame header fields
  # @returns show_existing_frame - the value of show_existing_frame flag
  # @returns show_frame - the value of show_frame flag
  def prepare_show(self, d):
    frame_header = d['frame_header']
    if 'show_existing_frame' in frame_header:
      show_existing_frame = frame_header['show_existing_frame']
    else:
      show_existing_frame = False

    if 'show_frame' in frame_header:
      show_frame = frame_header['show_frame']
    else:
      show_frame = False

    return show_existing_frame, show_frame

  ## Function prints show_existing_frame and show_frame flags if verbose mode is set in command line arguments
  # @param d - dictionary with parsed frame header fields
  def dprint_show(self, d):
    if self.verbose:
      show_existing_frame, show_frame = self.prepare_show(d)
      print('show_existing_frame={:s} show_frame={:s}'.format(str(show_existing_frame), str(show_frame)))

  ## Function converts integer OBU type to name text
  # @param type - OBU type value as extracted from OBU header
  # @returns result - string containting OBU type name text
  def str_obu_type(self, type):
    if (type == OBU_TYPE_SEQUENCE_HEADER):
      result = 'SEQUENCE_HEADER'
    elif (type == OBU_TYPE_TEMPORAL_DELIMITER):
      result = 'TEMPORAL_DELIMITER'
    elif (type == OBU_TYPE_FRAME_HEADER):
      result = 'FRAME_HEADER'
    elif (type == OBU_TYPE_TILE_GROUP):
      result = 'TILE_GROUP'
    elif (type == OBU_TYPE_METADATA):
      result = 'METADATA'
    elif (type == OBU_TYPE_FRAME):
      result = 'FRAME'
    elif (type == OBU_TYPE_REDUNDANT_FRAME_HEADER):
      result = 'REDUNDANT_FRAME_HEADER'
    elif (type == OBU_TYPE_TILE_LIST):
      result = 'TILE_LIST'
    elif (type == OBU_TYPE_PADDING):
      result = 'PADDING'
    else:
      raise ValueError('OBU type = {:d} unknown'.format(type))

    return result

  ## Function prints the description og OBU type if verbose mode is set in command line arguments
  # @param type - OBU type value as extracted from OBU header
  def dprint_obu_type(self, type):
    self.dprint(self.str_obu_type(type))

  ## Function parses unsigned integer value in LEB128 format from the stream
  # @returns result - parsed value
  # @returns length - the length in bytes of LEB128 value in the stream
  def read_leb128(self):
    result = 0
    shift = 0
    length = 0
    while True:
      val_byte = self.stream.read('uint:8')
      result |= (val_byte & 0x7F) << shift
      shift += 7
      length += 1
      if ((val_byte & 0x80) == 0x00):
        break
    return result, length

  ## Function creates a bit string which encodes value given as argument in LEB128 format
  # @returns result - resulting bitstring
  # @returns length - the length in bytes of the bitstring
  def encode_leb128(self, value):
    result = BitStream(0)
    length = 0
    while True:
      bitstring_bottom = pack('uint:7', value & 0x7F)
      value = value >> 7
      length += 1
      if (value != 0):
      # more bytes to come
        result.append('0b1')
        result += bitstring_bottom
      else:
      # we are done
        result.append('0b0')
        result += bitstring_bottom
        break

    return result, length

  ## Function parses unsigned value encoded in uvlc format from the stream
  # @returns result - parsed value
  def read_uvlc(self):
    leading_zeros = 0
    while True:
      done = self.stream.read('bool')
      if done:
        break
      leading_zeros += 1
    if (leading_zeros >= 32):
      return (1 << 32) - 1
    if (leading_zeros > 0):
      value = self.stream.read('uint:' + str(leading_zeros))
    else:
    # leading_zeros == 0, so there is nothing to read from the bitstream
      value = 0
    return value + (1 << leading_zeros) - 1

  ## Function parses AV1 OBU header
  # @returns dictionary with values of the fields parsed out of OBU header
  def parse_obu_header(self):
    stream = self.stream
    obu_header_start_pos = stream.pos
    obu_header_start_bytepos = stream.bytepos

    d = {}
    d['obu_forbidden_bit'] = stream.read('bool')
    if d['obu_forbidden_bit']:
      raise ValueError('obu_forbidden_bit set in obu_header at OBU starting at pos={0:d} bytepos=0x{1:x}'.format(obu_header_start_pos, obu_header_start_bytepos))
    d['obu_type'] = stream.read('uint:4')
    d['obu_extension_flag'] = stream.read('bool')
    d['obu_has_size_field'] = stream.read('bool')
    d['obu_reserved_1bit'] = stream.read('bool')
    if d['obu_reserved_1bit']:
      raise ValueError('obu_reserved_1bit set in obu_header at OBU starting at pos={0:d} bytepos=0x{1:x}'.format(obu_header_start_pos, obu_header_start_bytepos))
    if d['obu_extension_flag']:
      d['temporal_id'] = stream.read('uint:3')
      d['spatial_id'] = stream.read('uint:2')
      d['extension_header_reserved_3bits'] = stream.read('uint:3')
      if (d['extension_header_reserved_3bits'] != 0):
        raise ValueError('extension_header_reserved_3bits set to {:d} in obu_extension_header at OBU starting at pos={:d} bytepos=0x{:x}'.format(extension_header_reserved_3bits, obu_header_start_pos, obu_header_start_bytepos))
      d['obu_header_len'] = 2
    else:
      d['obu_header_len'] = 1

    return d

  ## Function parses uncompressed header in frame header
  # The function doesn't read the whole uncompressed header from the stream, so the stream
  # position needs to be adjusted by the calling function
  # @returns d - the dictionary containing the values of parsed fields
  def parse_uncompressed_header(self):
    stream = self.stream
    sequence_header = self.state['sequence_header']

    d = {}
    if sequence_header['frame_id_numbers_present_flag']:
      self.state['idLen'] = sequence_header['additional_frame_id_length_minus_1'] + sequence_header['delta_frame_id_length_minus_2'] + 3
    self.state['allFrames'] = (1 << NUM_REF_FRAMES) - 1
    if sequence_header['reduced_still_picture_header']:
      d['show_existing_frame'] = False
      d['frame_type'] = KEY_FRAME
      FrameIsIntra = True
      d['show_frame'] = True
      d['showable_frame'] = False
    else:
      d['show_existing_frame'] = stream.read('bool')
      if d['show_existing_frame']:
        d['frame_to_show_map_idx'] = stream.read('uint:3')
        if (sequence_header['decoder_model_info_present_flag'] and not sequence_header['timing_info']['equal_picture_interval']):
        # parse temporal_point_info structure
          n = sequence_header['decoder_model_info']['frame_presentation_time_length_minus_1'] + 1
          d['frame_presentation_time'] = stream.read('uint:' + str(n))
        d['refresh_frame_flags'] = 0
        if sequence_header['frame_id_numbers_present_flag']:
          d['display_frame_id'] = stream.read('uint:' + str(self.state['idLen']))
        # there is no stream content to be left to read here
        # the rest of processing is skipped as not required for MP4 AV1 parser
        return d

      d['frame_type'] = stream.read('uint:2')
      FrameIsIntra = (d['frame_type'] == INTRA_ONLY_FRAME) or (d['frame_type'] == KEY_FRAME)
      d['show_frame'] = stream.read('bool')
    # the rest of reading the stream and processing is skipped  as not required for MP4 AVI parser
    # the uncompressed_header is not completely read from the stream, so its position needs to be
    # adjusted by caller function

    return d

  ## Function parses frame header
  # @returns d - the dictionary containing the values of parsed fields
  def parse_frame_header_obu(self):
    d = self.parse_uncompressed_header()
    return d

  ## Function parses AV1 color_config structure that is present in AV1 sequence header
  # @param sequence_header - the dictionary with sequence header values parsed to date
  # @returns dictionary with values of the fields parsed out of AV1 color_config structure
  def parse_color_config(self, sequence_header):
    stream = self.stream
    d = {}
    d['high_bitdepth'] = stream.read('bool')
    d['twelve_bit'] = False # Default value of the bit if it is not present in the stream.
                            # It is required to create AV1CodecConfigurationBox for MP4 container
    if (sequence_header['seq_profile'] == 2) and d['high_bitdepth']:
      d['twelve_bit'] = stream.read('bool')
      if d['twelve_bit']:
        self.state['BitDepth'] = 12
      else:
        self.state['BitDepth'] = 10
    elif (sequence_header['seq_profile'] <= 2):
      if d['high_bitdepth']:
        self.state['BitDepth'] = 10
      else:
        self.state['BitDepth'] = 8
    if (sequence_header['seq_profile'] == 1):
      d['mono_chrome'] = False
    else:
      d['mono_chrome'] = stream.read('bool')

    if d['mono_chrome']:
      self.state['NumPlanes'] = 1
    else:
      self.state['NumPlanes'] = 3

    d['color_description_present_flag'] = stream.read('bool')
    if d['color_description_present_flag']:
      d['color_primaries'] = stream.read('uint:8')
      d['transfer_characteristics'] = stream.read('uint:8')
      d['matrix_coefficients'] = stream.read('uint:8')
    else:
    # color_description_present_flag is False
      d['color_primaries'] = CP_UNSPECIFIED
      d['transfer_characteristics'] = TC_UNSPECIFIED
      d['matrix_coefficients'] = MC_UNSPECIFIED

    if d['mono_chrome']:
      d['color_range'] = stream.read('bool')
      d['subsampling_x'] = True
      d['subsampling_y'] = True
      d['chroma_sample_position'] = CSP_UNKNOWN
      d['separate_uv_delta_q'] = False
      return d
    elif (  (d['color_primaries'] == CP_BT_709) and
            (d['transfer_characteristics'] == TC_SRGB) and
            (d['matrix_coefficients'] == MC_IDENTITY)):
      d['color_range'] = True
      d['subsampling_x'] = False
      d['subsampling_y'] = False
    else:
      d['color_range'] = stream.read('bool')
      if (sequence_header['seq_profile'] == 0):
        d['subsampling_x'] = True
        d['subsampling_y'] = True
      elif (sequence_header['seq_profile'] == 1):
        d['subsampling_x'] = False
        d['subsampling_y'] = False
      else:
      # profiles other than 0 and 1
        if (self.state['BitDepth'] == 12):
          d['subsampling_x'] = stream.read('bool')
          if d['subsampling_x']:
            d['subsampling_y'] = stream.read('bool')
          else:
            d['subsampling_y'] = False
        else:
        # BitDepth different than 12
          d['subsampling_x'] = True
          d['subsampling_y'] = False
      if (d['subsampling_x'] and d['subsampling_y']):
        d['chroma_sample_position'] = stream.read('uint:2')
    d['separate_uv_delta_q'] = stream.read('bool')
    return d

  ## Function parses AV1 sequence header OBU
  # @returns dictionary with values of the fields parsed out of AV1 sequence header OBU
  def parse_obu_sequence_header(self):
    stream = self.stream
    d = {}
    d['seq_profile'] = stream.read('uint:3')
    d['still_picture'] = stream.read('bool')
    d['reduced_still_picture_header'] = stream.read('bool')
    if (d['reduced_still_picture_header']):
      d['timing_info_present_flag'] = False
      d['decoder_model_info_present_flag'] = False
      d['initial_display_delay_present_flag'] = False
      d['operating_points_cnt_minus_1'] = 0
      d['operating_point_idc'] = [0]
      seq_level_idx_0 = stream.read('uint:5')
      d['seq_level_idx'] = [seq_level_idx_0]
      d['seq_tier'] = [0]
      d['decoder_model_present_for_this_op'] = [0]
      d['initial_display_delay_present_for_this_op'] = [0]
    else:
      d['timing_info_present_flag'] = stream.read('bool')
      if d['timing_info_present_flag']:
        d['timing_info'] = {}
        d['timing_info']['num_units_in_display_tick'] = stream.read('uintbe:32')
        d['timing_info']['time_scale'] = stream.read('uintbe:32')
        d['timing_info']['equal_picture_interval'] = stream.read('bool')
        if d['timing_info']['equal_picture_interval']:
          d['timing_info']['num_ticks_per_picture_minus_1'] = self.read_uvlc()
        d['decoder_model_info_present_flag'] = stream.read('bool')
        if (d['decoder_model_info_present_flag']):
          d['decoder_model_info'] = {}
          d['decoder_model_info']['buffer_delay_length_minus_1'] = stream.read('uint:5')
          d['decoder_model_info']['num_units_in_decoding_tick'] = stream.read('uintbe:32')
          d['decoder_model_info']['buffer_removal_time_length_minus_1'] = stream.read('uint:5')
          d['decoder_model_info']['frame_presentation_time_length_minus_1'] = stream.read('uint:5')
      else:
      # timing_info_present_flag is False
        d['decoder_model_info_present_flag'] = False
      d['initial_display_delay_present_flag'] = stream.read('bool')
      if d['initial_display_delay_present_flag']:
        d['initial_display_delay_present_for_this_op'] = []
      d['operating_points_cnt_minus_1'] = stream.read('uint:5')
      d['operating_point_idc'] = []
      d['seq_level_idx'] =[]
      d['seq_tier'] = []
      d['decoder_model_present_for_this_op'] = []
      for i in range(d['operating_points_cnt_minus_1'] + 1):
        d['operating_point_idc'].append(stream.read('uint:12'))
        seq_level_idx = stream.read('uint:5')
        d['seq_level_idx'].append(seq_level_idx)
        if (seq_level_idx > 7):
          d['seq_tier'].append(stream.read('bool'))
        else:
          d['seq_tier'].append(False)
        if d['decoder_model_info_present_flag']:
          decoder_model_present_for_this_op = stream.read('bool')
          d['decoder_model_present_for_this_op'].append(decoder_model_present_for_this_op)
          if decoder_model_present_for_this_op:
          # need to read the structure to advance the stream even if we don't care about the content
            n = d['decoder_model_info']['buffer_delay_length_minus_1'] + 1
            stream.read('uint:' + str(n + n + 1))
        else:
        # decoder_model_info_present_flag is False
          d['decoder_model_present_for_this_op'].append(False)
        if d['initial_display_delay_present_flag']:
          initial_display_delay_present_for_this_op = stream.read('bool')
          d['initial_display_delay_present_for_this_op'].append(initial_display_delay_present_for_this_op)
          if initial_display_delay_present_for_this_op:
          # need to read the structure to advance the stream even if we don't care about the content
            initial_display_delay_minus_1 = stream.read('uint:4')
    # skip choosing operating point and operation point IDC as we don't care about that
    d['frame_width_bits_minus_1'] = stream.read('uint:4')
    d['frame_height_bits_minus_1'] = stream.read('uint:4')
    n = d['frame_width_bits_minus_1'] + 1
    d['max_frame_width_minus_1'] = stream.read('uint:' + str(n))
    n = d['frame_height_bits_minus_1'] + 1
    d['max_frame_height_minus_1'] = stream.read('uint:' + str(n))
    if d['reduced_still_picture_header']:
      d['frame_id_numbers_present_flag'] = False
    else:
      d['frame_id_numbers_present_flag'] = stream.read('bool')
    if d['frame_id_numbers_present_flag']:
      d['delta_frame_id_length_minus_2'] = stream.read('uint:4')
      d['additional_frame_id_length_minus_1'] = stream.read('uint:3')
    d['use_128x128_superblock'] = stream.read('bool')
    d['enable_filter_intra'] = stream.read('bool')
    d['enable_intra_edge_filter'] = stream.read('bool')
    if d['reduced_still_picture_header']:
      d['enable_interintra_compound'] = False
      d['enable_masked_compound'] = False
      d['enable_warped_motion'] = False
      d['enable_dual_filter'] = False
      d['enable_order_hint'] = False
      d['enable_jnt_comp'] = False
      d['enable_ref_frame_mvs'] = False
      d['seq_force_screen_content_tools'] = SELECT_SCREEN_CONTENT_TOOLS
      d['seq_force_integer_mv'] = SELECT_INTEGER_MV
      self.state['OrderHintBits'] = 0
    else:
    # reduced_still_picture_header flag is False
      d['enable_interintra_compound'] = stream.read('bool')
      d['enable_masked_compound'] = stream.read('bool')
      d['enable_warped_motion'] = stream.read('bool')
      d['enable_dual_filter'] = stream.read('bool')
      d['enable_order_hint'] = stream.read('bool')
      if d['enable_order_hint']:
        d['enable_jnt_comp'] = stream.read('bool')
        d['enable_ref_frame_mvs'] = stream.read('bool')
      else:
        d['enable_jnt_comp'] = False
        d['enable_ref_frame_mvs'] = False
      d['seq_choose_screen_content_tools'] = stream.read('bool')
      if d['seq_choose_screen_content_tools']:
        d['seq_force_screen_content_tools'] = SELECT_SCREEN_CONTENT_TOOLS
      else:
        d['seq_force_screen_content_tools'] = int(stream.read('bool')) # have to convert to int as this field has to present two bit wide values
      if (d['seq_force_screen_content_tools'] > 0):
        d['seq_choose_integer_mv'] = stream.read('bool')
        if d['seq_choose_integer_mv']:
          d['seq_force_integer_mv'] = SELECT_INTEGER_MV
        else:
          d['seq_force_integer_mv'] = int(stream.read('bool')) # have to convert to int as this field has to present two bit wide values
      else:
      # d['seq_force_screen_content_tools'] == 0
        d['seq_force_integer_mv'] = SELECT_INTEGER_MV
      if d['enable_order_hint']:
        d['order_hint_bits_minus_1'] = stream.read('uint:3')
        self.state['OrderHintBits'] = d['order_hint_bits_minus_1'] + 1
      else:
        self.state['OrderHintBits'] = 0
    d['enable_superres'] = stream.read('bool')
    d['enable_cdef'] = stream.read('bool')
    d['enable_restoration'] = stream.read('bool')

    d['color_config'] = self.parse_color_config(d)
    d['film_grain_params_present'] = stream.read('bool')

    self.state['sequence_header'] = d
    return d

  ## Function parses signle OBU in AV1 non Annex B stream
  # @returns d - the dictionary containing the values of parsed fields
  def parse_obu(self):
    stream = self.stream
    d = {}
    obu_start_bytepos = stream.bytepos

    # parse OBU header
    d['obu_header'] = self.parse_obu_header()
    if (d['obu_header']['obu_has_size_field']):
      d['obu_size'], d['obu_size_len'] = self.read_leb128()

    d['obu_len'] = d['obu_header']['obu_header_len'] + d['obu_size_len'] + d['obu_size']
    obu_type = d['obu_header']['obu_type']
    startPosition = stream.pos

    # print OBU type, start offset and length
    if (d['obu_header']['obu_extension_flag'] == True):
    # header has extension, so print temporal_id and spatial_id
      self.av1_contents.append({  'obu_type' : self.str_obu_type(obu_type),                         \
                                  'obu_start' : obu_start_bytepos,                                  \
                                  'obu_len' : d['obu_len'],                                         \
                                  'temporal_id' : d['obu_header']['temporal_id'],                   \
                                  'spatial_id' : d['obu_header']['spatial_id'] })
      self.dprint('{:s} obuStart=0x{:X} len={:d} payload_size={:d} temporal_id={:d}, spatial_id={:d}'.format(self.str_obu_type(obu_type), obu_start_bytepos, d['obu_len'], d['obu_size'], d['obu_header']['temporal_id'], d['obu_header']['spatial_id']))
    else:
      self.av1_contents.append({  'obu_type' : self.str_obu_type(obu_type),                         \
                                  'obu_start' : obu_start_bytepos,                                  \
                                  'obu_len' : d['obu_len'] })
      self.dprint('{:s} obuStart=0x{:X} len={:d}, payload_size={:d}'.format(self.str_obu_type(obu_type), obu_start_bytepos, d['obu_len'], d['obu_size']))

    # perform processing specific to particular OBU type
    if (obu_type == OBU_TYPE_SEQUENCE_HEADER):
      self.state['sequence_header'] = self.parse_obu_sequence_header()
      d['sequence_header'] = self.state['sequence_header']

    elif (obu_type == OBU_TYPE_TEMPORAL_DELIMITER):
      # seek to the end of this OBU
      stream.bytepos += d['obu_size']

    elif (obu_type == OBU_TYPE_FRAME_HEADER):
      d['frame_header'] = self.parse_frame_header_obu()
      # we have to manually seek to the end of this OBU because parser didn't read complete OBU contents
      stream.bytepos = obu_start_bytepos + d['obu_len']

    elif (obu_type == OBU_TYPE_TILE_GROUP):
      # seek to the end of this OBU
      stream.bytepos += d['obu_size']

    elif (obu_type == OBU_TYPE_METADATA):
      # seek to the end of this OBU
      stream.bytepos += d['obu_size']

    elif (obu_type == OBU_TYPE_FRAME):
      d['frame_header'] = self.parse_frame_header_obu()
      # we have to manually seek to the end of this OBU because parser didn't read complete OBU contents
      stream.bytepos = obu_start_bytepos + d['obu_len']

    elif (obu_type == OBU_TYPE_TILE_LIST):
      # seek to the end of this OBU
      stream.bytepos += d['obu_size']

    elif (obu_type == OBU_TYPE_PADDING):
      # seek to the end of this OBU
      stream.bytepos += d['obu_size']

    elif (obu_type == OBU_TYPE_REDUNDANT_FRAME_HEADER):
      # seek to the end of this OBU
      stream.bytepos += d['obu_size']

    else:
      raise ValueError('Undefined obu_type={:d} in OBU starting at offset=0x{:X}'.format(obu_type, obu_start_bytepos))

    # sort out trailing bits if necessary
    currentPosition = stream.pos
    payloadBits = currentPosition - startPosition
    # self.dprint('endPosition = {:d} payloadBits = {:d} trailing = {:d}'.format(currentPosition, payloadBits, d['obu_size'] * 8 - payloadBits))
    if (  (d['obu_size'] > 0) and
          (obu_type != OBU_TYPE_TILE_GROUP) and
          (obu_type != OBU_TYPE_TILE_LIST) and
          (obu_type != OBU_TYPE_FRAME)):
      # skip trailing bits to get byte alignment on the next OBU
      stream.pos += d['obu_size'] * 8 - payloadBits

    return d

  ## Function deletes output file(s)
  def cleanup_files(self):
    self.dprint('Deleting {:s} file'.format(self.fname_out))
    os.remove(self.fname_out)

## Class checks the structure of non Annex B AV1 stream and fixes it if required
class Av1NonAnnexBCheckAndFix(Av1NonAnnexBParse):

  ## Function initializes Av1NonAnnexBCheckAndFix object
  # @param fname - name of the file containing AV1 stream to be checked
  # @param args - object with command line arguments parsed by argparse
  def __init__(self, fname, args, suffix = '.checked'):
    self.fname = fname
    self.fix_missing_temp_del = args.fix_missing_temp_del
    self.fname_out = fname + suffix
    super(Av1NonAnnexBCheckAndFix, self).__init__(fname, args) # open the stream so it can be parsed
    self.check_and_fix()  # perform actual analysis and optional fixing

  ## Function analyses contents of input AV1 stream and fixes it if required
  # - checks if every sequence header is prepended with temporal delimiter as per spec requirement
  def check_and_fix(self):
    self.dprint('=========================================')
    self.dprint('Checking {:s} file'.format(self.fname))
    self.dprint('=========================================')
    self.dprint(' ')

    with open(self.fname_out, 'wb') as f_out:
      # initialize things to check for missing temporal delimiters in front of sequence headers
      last_td_index = -2
      index = 0

      while True:
        d = self.parse_obu()
        obu_start_bytepos = self.stream.bytepos - d['obu_len']
        obu_type = d['obu_header']['obu_type']

        if (obu_type == OBU_TYPE_TEMPORAL_DELIMITER):
        # temporal delimiter
          last_td_index = index

        elif (obu_type == OBU_TYPE_SEQUENCE_HEADER):
        # sequence header
          if ((index - last_td_index) > 1):
          # there were OBUs other than temporal delimiter just before sequence header
            if self.fix_missing_temp_del:
            # add missing temporal delimiter to the output file
              self.dprint('Inserting missing temporal delimiter at offset 0x{:X}'.format(obu_start_bytepos))
              format =  'bool=obu_forbidden_bit, '
              format += 'uint:4=obu_type, '
              format += 'bool=obu_extension_flag, '
              format += 'bool=obu_has_size_field, '
              format += 'bool=obu_reserved_1bit, '
              format += 'uint:8=leb128_zero'
              f_out.write(  pack(format,
                            obu_forbidden_bit = False,
                            obu_type = OBU_TYPE_TEMPORAL_DELIMITER,
                            obu_extension_flag = False,
                            obu_has_size_field = True,
                            obu_reserved_1bit = False,
                            leb128_zero = 0).read('bytes:2'))

            else:
            # report missing temporal delimiter
              raise ValueError('Missing temporal delimiter in front of sequence header at offset 0x{:X}'.format(obu_start_bytepos))

        elif (  (obu_type == OBU_TYPE_FRAME_HEADER) or
                (obu_type == OBU_TYPE_TILE_GROUP) or
                (obu_type == OBU_TYPE_METADATA) or
                (obu_type == OBU_TYPE_FRAME) or
                (obu_type == OBU_TYPE_REDUNDANT_FRAME_HEADER) or
                (obu_type == OBU_TYPE_TILE_LIST) or
                (obu_type == OBU_TYPE_PADDING)):
        # copy other allowed OBU types to the output file
          pass

        else:
        # unknown OBU type
          raise ValueError('Unknown obu_type = {:d} at offset 0x{:X}'.format(obu_type, obu_start_bytepos))

        # copy OBU to the output file
        self.stream.bytepos = obu_start_bytepos
        f_out.write(self.stream.read('bytes:' + str(d['obu_len'])))
        index += 1

        end_of_stream = (self.stream.pos >= self.stream.length)
        if end_of_stream:
        # gracefully finish off processing
          break

    self.dprint(' ')

## Class converts AV1 Annex B compatible streams to AV1 Non Annex B compatible ones
class Av1ConvertToNonAnnexB(Av1NonAnnexBParse):

  ## Function initializes Av1ConvertToNonAnnexB object
  # @param fname - name of the file containing AV1 stream to be converted
  # @param args - object with command line arguments parsed by argparse
  # @param suffix - suffix added to the output file
  def __init__(self, fname, args, suffix = '.nab'):
    self.annex_b = args.annex_b
    self.fname = fname
    self.fname_out = fname + suffix
    super(Av1ConvertToNonAnnexB, self).__init__(fname, args) # open the stream so it can be parsed
    self.convert()  # perform actual conversion

  ## Function parses AV1 Annex B frame unit and converts it to AV1 Non Annex B format
  # @param size - the size of the frame unit in bytes
  # @param f_out - object that allows to write to output file
  def parse_frame_unit(self, size, f_out):
    while (size > 0):
      obu_length, leb128_bytes = self.read_leb128()
      self.dprint('Frame unit obu_length={:d}'.format(obu_length))
      size -= leb128_bytes
      obu_start_bytepos = self.stream.bytepos
      d = self.parse_obu_header()
      self.dprint_obu_type(d['obu_type'])
      if (d['obu_has_size_field'] == True):
      # OBU has size field, so check if the value there is consistent with obu_length in Annex B frame_unit
        obu_size, obu_size_len = self.read_leb128()
        calc_obu_length = obu_size + obu_size_len + 1
        if (d['obu_extension_flag'] == True):
        # OBU has an extension byte in the header
          calc_obu_length += 1

        if (obu_length != calc_obu_length):
          raise ValueError('Annex B frame unit obu_length={:d} is insonsistent with OBU size = {:d} at stream byte position 0x{:x}'.format(obu_length, obu_size, obu_start_bytepos))
        # everything seems to be fine, so we can extract OBU and save it into output file
        self.dprint('OBU has correct size field')
        self.stream.bytepos = obu_start_bytepos
        f_out.write(self.stream.read('bytes:' + str(obu_length)))
      else:
      # OBU header doesn't have size field, so we have to create one
        bytes_written = 0
        d['obu_has_size_field'] = True
        format =  'bool=obu_forbidden_bit, '
        format += 'uint:4=obu_type, '
        format += 'bool=obu_extension_flag, '
        format += 'bool=obu_has_size_field, '
        format += 'bool=obu_reserved_1bit'
        f_out.write(pack(format, **d).read('bytes:1'))
        bytes_written += 1
        if (d['obu_extension_flag'] == True):
          format = 'uint:3=temporal_id, '
          format += 'uint:2=spatial_id, '
          format += 'uint:3=extension_header_reserved_3bits'
          f_out.write(pack(format, **d).read('bytes:1'))
          bytes_written += 1

        # calculate obu_size assuming that it will get encoded in 5 bytes and the header is 1 byte long
        obu_size = obu_length - bytes_written

        self.dprint('Created obu_size field = 0x{:X}'.format(obu_size))

        # now encode obu_size in the output stream
        obu_size_leb128, obu_size_leb128_len = self.encode_leb128(obu_size)
        obu_size_leb128.bytepos = 0
        f_out.write(obu_size_leb128.read('bytes:' + str(obu_size_leb128_len)))

        # copy the rest of the OBU as is into the ouput file
        f_out.write(self.stream.read('bytes:' + str(obu_size)))
      size -= obu_length

  ## Function parses AV1 Annex B temporal unit and converts it to AV1 Non Annex B format
  # @param size - the size of the temporal unit in bytes
  # @param f_out - object that allows to write to output file
  def parse_temporal_unit(self, size, f_out):
    while (size > 0):
      frame_unit_size, leb128_bytes = self.read_leb128()
      self.dprint('Temporal unit frame_unit_size = {:d}'.format(frame_unit_size))
      size -= leb128_bytes
      self.parse_frame_unit(frame_unit_size, f_out)
      size -= frame_unit_size

    self.dprint(' ')

  ## Function parses AV1 Annex B stream and converts it to AV1 Non Annex B format if not already in the right format
  def convert(self):
    self.dprint('=========================================')
    self.dprint('Opening {:s} output file'.format(self.fname_out))
    self.dprint('=========================================')
    if (self.annex_b == True):
    # we really have to convert the file to non Annex B one
      with open(self.fname_out, 'wb') as f_out:
        while True:
        # first AV1 Annex B while loop that splits temporal units
          temporal_unit_size, temporal_unit_size_len = self.read_leb128()
          self.dprint('Temporal_unit_size = {:d}'.format(temporal_unit_size))
          self.parse_temporal_unit(temporal_unit_size, f_out)
          end_of_stream = (self.stream.pos >= self.stream.length)
          if end_of_stream:
          # gracefully finish off processing
            break
    else:
    # just copy the file as it is already in non Annex B format
      shutil.copyfile(self.fname, self.fname_out)

    self.dprint(' ')

## Class splits AV1 Non Annex B compatible stream on different sequence headers
# This is to make them compatible with MP4 container that does only allow single sequence headers
class Av1SplitOnDiffSeqHeaders(Av1NonAnnexBParse):

  ## Function initializes Av1AnnexBConvert object
  # @param fname - name of the file containing AV1 stream to be converted
  # @param args - object with command line arguments parsed by argparse
  def __init__(self, fname, args):
    self.fname = fname
    super(Av1SplitOnDiffSeqHeaders, self).__init__(fname, args) # open the stream so it can be parsed

  ## Function checks if two sequence headers are the same
  # Please note that some fields (for example in LEB128 format) can be binary different, but
  # still encode the same value. That is why function compares actual encoded values
  # rather than performing binary diff
  # @param dict1 - dictionary representing first sequence header
  # @param dict2 - dictionary representing second sequence header
  # @returns True if sequence headers are different, False otherwise
  def is_seq_header_different(self, dict1, dict2):
    if (set(dict1.keys()) != set(dict2.keys())):
    # dict1 and dict2 dictionaries have different keys, so they differ
      return True

    # here both dictionaries have exactly the same keys
    for key in dict1:
      if (key != 'obu_header'):
      # compare everything apart OBU header, as this may differ even if the content is the same
        if (dict1[key] != dict2[key]):
          return True

    return False

  ## Function splits input stream on different sequence headers
  # @returns - list containing the names of the split files
  def split(self):
    # open first output file
    out_file_no = 0
    curr_filename = self.fname + '.' + str(out_file_no)
    out_file_no += 1

    self.dprint('=========================================')
    self.dprint('Opening {:s} output file'.format(curr_filename))
    self.dprint('=========================================')
    f_out = open(curr_filename, 'wb')

    self.split_list = [] # prepare an empty list for split file names
    deferred_temporal_delimiter = False
    seq_header_seen = False

    while True:
      d = self.parse_obu()
      obu_type = d['obu_header']['obu_type']

      if (obu_type == OBU_TYPE_TEMPORAL_DELIMITER):
      # temporal delimiter
        # mark it for saving to the output stream, but don't save it yet
        deferred_temporal_delimiter = True
        deferred_temporal_delimiter_len = d['obu_len']

      elif (obu_type == OBU_TYPE_SEQUENCE_HEADER):
      # sequence header
        if seq_header_seen:
        # this is not first sequence_header
          new_seq_header = d['sequence_header']
          if self.is_seq_header_different(seq_header, new_seq_header):
          # the sequence header is different than the previous one, so switch to new output file
            f_out.close()
            self.split_list.append(curr_filename)  # and add current file to split files list

            # open a new file
            curr_filename = self.fname + '.' + str(out_file_no)
            out_file_no += 1
            f_out = open(curr_filename, 'wb')

            # and new sequence_header for later
            seq_header = new_seq_header

            self.dprint('Different sequence header than the previous one detected, , so have to switch to new output file')
            self.dprint(' ')
            self.dprint('=========================================')
            self.dprint('Opening {:s} output file'.format(curr_filename))
            self.dprint('=========================================')
        else:
        # this is the first sequence header
          self.dprint('Found first sequence header')
          seq_header = d['sequence_header']
          seq_header_seen = True

      # now copy  what is required to the output file
        len_to_copy = d['obu_len']
        if deferred_temporal_delimiter:
        # we also have to save deferred temporal delimiter
          len_to_copy += deferred_temporal_delimiter_len
          deferred_temporal_delimiter = False
        self.stream.bytepos -= len_to_copy
        f_out.write(self.stream.read('bytes:' + str(len_to_copy)))

      elif (  (obu_type == OBU_TYPE_FRAME_HEADER) or
              (obu_type == OBU_TYPE_TILE_GROUP) or
              (obu_type == OBU_TYPE_METADATA) or
              (obu_type == OBU_TYPE_FRAME) or
              (obu_type == OBU_TYPE_REDUNDANT_FRAME_HEADER) or
              (obu_type == OBU_TYPE_TILE_LIST) or
              (obu_type == OBU_TYPE_PADDING)):
      # copy other allowed OBU types to the output file
        len_to_copy = d['obu_len']
        if deferred_temporal_delimiter:
        # we also have to copy deferred temporal delimiter
          len_to_copy += deferred_temporal_delimiter_len
          deferred_temporal_delimiter = False

        self.stream.bytepos -= len_to_copy
        f_out.write(self.stream.read('bytes:' + str(len_to_copy)))

      else:
      # unknown OBU type
        raise ValueError('Unknown obu_type = {:d} at offset 0x{:X}'.format(obu_type, self.stream.bytepos - d['obu_len']))

      end_of_stream = (self.stream.pos >= self.stream.length)
      if end_of_stream:
      # gracefully finish off processing
        f_out.close()
        self.split_list.append(curr_filename)  # and add current file to split files list
        break

    self.dprint(' ')
    return self.split_list

  ## Function deletes output file(s)
  def cleanup_files(self):
    for fname in self.split_list:
      self.dprint('Deleting {:s} file'.format(fname))
      os.remove(fname)

## Class prepares AV1 Non Annex B compatible stream to be put into MP4 container
# To do so it performs the following:
# - strips repeated SEQUENCE_HEADER OBUs
# - strips PADDING, REDUNDANT_FRAME_HEADER and TEMPORAL_DELIMITER OBUs
# - calculates the sizes of MP4 compatible chunks each containing one frame that will get shown
# - parses out config_obus
# - parses out SEQUENCE_HEADER
class Av1ForMp4(Av1NonAnnexBParse):

  ## Function initializes Av1ForMp4 object
  # @param fname - name of the file containing AV1 stream to be converted
  # @param args - object with command line arguments parsed by argparse
  # @param suffix - suffix added to the output file
  def __init__(self, fname, args, suffix = '.s'):
    self.fname = fname
    self.fname_out = fname + suffix
    self.dont_check_frame_show = args.dont_check_frame_show
    super(Av1ForMp4, self).__init__(fname, args) # open the stream so it can be parsed
    self.scalability_check()
    if self.scalability_present:
    # input AV1 stream implements scalability
      self.prepare_with_scalability()
    else:
    # input AV1 stream doesn't implement scalability
      self.prepare_no_scalability()

  ## Function parses input AV1 to check if there is implements scalability
  def scalability_check(self):
    self.dprint('=========================================')
    self.dprint('Checking scalability implementation in {:s} file'.format(self.fname))
    self.dprint('=========================================')

    # first check if extension flag is present in any OBU headers in the stream
    extension_flag_present = False
    while True:
      d = self.parse_obu()
      obu_type = d['obu_header']['obu_type']
      if (  (obu_type == OBU_TYPE_FRAME) or
            (obu_type == OBU_TYPE_FRAME_HEADER) or
            (obu_type == OBU_TYPE_TILE_GROUP)):
      # the presence of extension_flag in a OBU header only matters for frame, frame header or tile group
        if d['obu_header']['obu_extension_flag']:
        # AV1 specification requires that obu_extension_flag is on if the stream allows for scalability
          extension_flag_present = True
      end_of_stream = (self.stream.pos >= self.stream.length)
      if end_of_stream:
      # gracefully finish off processing
        break

    if extension_flag_present:
    # extension flag is present, so work out which spatial and temporal layers are supposed to be displayed
      operating_point_idc = self.state['sequence_header']['operating_point_idc'][0] # this is the operating point that is being used by Argon Streams
      temporal_layer_list = []
      for i in range(8):
      # bottom 8 bits in the mask specify temporal layers
        if ((operating_point_idc & (1 << i)) != 0):
          temporal_layer_list.append(i)

      spatial_layer_list = []
      for i in range(4):
      # bits 11-8 in the mask specify spatial layers
        if ((operating_point_idc & (1 << (i+8))) != 0):
          spatial_layer_list.append(i)

      # create list with all enabled temporal/spatial combinations
      self.scalability_combinations = []
      self.dprint('Enabled temporal_id / spatial_id combinations:')
      for temporal_id in temporal_layer_list:
        for spatial_id in spatial_layer_list:
          self.scalability_combinations.append({'temporal_id' : temporal_id, 'spatial_id' : spatial_id})
          self.dprint('temporal_id={:d}, spatial_id={:d}'.format(temporal_id, spatial_id))

      enabled_operating_points_no = len(self.scalability_combinations)
      if (enabled_operating_points_no == 0):
        raise ValueError('Stream contains OBU header extensions, but no operating points are enabled')
      elif (enabled_operating_points_no == 1):
      # only one operating point is enabled, so basically there is no scalability at all in the stream
        self.scalability_present = False
      else:
      # more than one operating point is enabled, so the stream has scalability implemented
        self.scalability_present = True
    else:
    # scalability is disabled in that stream
      self.scalability_present = False

    if self.scalability_present:
      self.dprint('=========================================')
      self.dprint('Stream in {:s} file, implements scalability'.format(self.fname))
      self.dprint('=========================================')
    else:
      self.dprint('=========================================')
      self.dprint('Stream in {:s} file, does not implement scalability'.format(self.fname))
      self.dprint('=========================================')

    self.dprint(' ')

    # wind the stream back to be beginning
    self.stream.bytepos = 0

  ## Function checks if given scalability setting should be decoded or not
  # @param scalability - the dictionary with the scalability settings that need to be checked
  # @returns True if this scalability should be decoded, False otherwise
  def is_scalability_decoded(self, scalability):
    scalability_decoded = False
    for s in self.scalability_combinations:
      if (s == scalability):
        scalability_decoded = True
        break

    return scalability_decoded

  ## Function prepares config_obus for AV1C box in MP4 container
  # It does it by extracting the bits from AV1 stream as specified in dictionary
  # provided as an input argument
  # @param config_obus_positions - the dictionary specifying which parts of AV1 stream should be extracted
  def prepare_config_obus(self, config_obus_positions):
    self.config_obus = []
    for i in config_obus_positions:
      self.stream.bytepos = i['offset']
      self.config_obus += self.stream.read('bytes:' + str(i['len']))

  ## Function prepares input AV1 Non Annex B compatible stream which doesn't implement scalability
  ## to be put into MP4 container
  # To do so it performs the following:
  # - strips repeated SEQUENCE_HEADER OBUs
  # - strips PADDING, REDUNDANT_FRAME_HEADER and TEMPORAL_DELIMITER OBUs
  # - calculates the sizes of MP4 compatible chunks each containing one frame that will get shown
  def prepare_no_scalability(self):
    self.dprint('=========================================')
    self.dprint('Opening {:s} output file'.format(self.fname_out))
    self.dprint('=========================================')
    f_out = open(self.fname_out, 'wb')

    self.chunks = []
    chunk_len = 0

    config_obus_positions =[]

    first_temp_delimiter = True
    seq_header_seen = False
    frame_shown_in_chunk = False

    while True:
      d = self.parse_obu()
      obu_type = d['obu_header']['obu_type']

      if (obu_type == OBU_TYPE_TEMPORAL_DELIMITER):
      # TEMPORAL_DELIMITER marks the start of a new MP4 chunk only when the stream doesn't support scalability
        if first_temp_delimiter:
        # this is the beginning of the first chunk in current output file
          first_temp_delimiter = False
        else:
        # this is the end of chunk if the frame was shown in it
          if frame_shown_in_chunk:
            self.dprint('Frame was shown in this chunk, so we can end it')
            self.chunks.append(chunk_len)
            chunk_len = 0
            frame_shown_in_chunk = False

        # we don't write OBU_TYPE_TEMPORAL_DELIMITER to the ouput file as the spec requires that it SHOULD NOT be used in MP4 container
        self.dprint('Dropped TEMPORAL_DELIMITER')

      elif (obu_type == OBU_TYPE_PADDING):
      # skip OBU_TYPE_PADDING as according to the spec it SHOULD NOT be used in MP4 container
        self.dprint('Dropped PADDING')

      elif (obu_type == OBU_TYPE_REDUNDANT_FRAME_HEADER):
      # skip OBU_TYPE_REDUNDANT_FRAME_HEADER as according to the spec it SHOULD NOT be used in MP4 container
        self.dprint('Dropped REDUNDANT_FRAME_HEADER')

      elif (obu_type == OBU_TYPE_SEQUENCE_HEADER):
      # deal with SEQUENCE_HEADER
        if seq_header_seen:
        # this is repeated SEQUENCE_HEADER, so drop it
          self.dprint('Dropped repeated SEQUENCE_HEADER')
        else:
        # this is the first sequence header
          self.dprint('Found first sequence header')
          seq_header_seen = True

        # mark OBU position to be copied to config_obus later
          config_obus_positions.append({'offset' : self.stream.bytepos - d['obu_len'], 'len' : d['obu_len']})

        # copy it as it is over to the output file
          self.stream.bytepos -= d['obu_len']
          f_out.write(self.stream.read('bytes:' + str(d['obu_len'])))
          chunk_len += d['obu_len']

      elif ((obu_type == OBU_TYPE_FRAME) or (obu_type == OBU_TYPE_FRAME_HEADER)):
      # deal with FRAME and FRAME_HEADER
        show_existing_frame, show_frame = self.prepare_show(d)
        self.dprint('show_existing_frame = {:s}, show_frame = {:s}'.format(str(show_existing_frame), str(show_frame)))
        if not frame_shown_in_chunk:
          if show_existing_frame or show_frame:
            frame_shown_in_chunk = True

        # copy it as it is over to the output file
        self.stream.bytepos -= d['obu_len']
        f_out.write(self.stream.read('bytes:' + str(d['obu_len'])))
        chunk_len += d['obu_len']

      elif (obu_type == OBU_TYPE_METADATA):
      # deal with METADATA
      # mark OBU position to be copied to config_obus later
        config_obus_positions.append({'offset' : self.stream.bytepos - d['obu_len'], 'len' : d['obu_len']})

      # copy OBU as it is over to the output file
        self.stream.bytepos -= d['obu_len']
        f_out.write(self.stream.read('bytes:' + str(d['obu_len'])))
        chunk_len += d['obu_len']

      else:
      # copy the rest of the OBUs as they are
        self.stream.bytepos -= d['obu_len']
        f_out.write(self.stream.read('bytes:' + str(d['obu_len'])))
        chunk_len += d['obu_len']

      self.dprint(' ')
      end_of_stream = (self.stream.pos >= self.stream.length)
      if end_of_stream:
      # gracefully finish off processing
        f_out.close()
        if (chunk_len > 0):
        # end the chunk only when there is something in it
          self.chunks.append(chunk_len)
        break

    if not self.dont_check_frame_show:
      if (len(self.chunks) == 0):
        raise ValueError('File {:s} contains no frames that will get shown'.format(self.fname))

    # prepare config_obus
    self.prepare_config_obus(config_obus_positions)

    self.dprint(' ')

  ## Function prepares input AV1 Non Annex B compatible stream which implement scalability
  ## to be put into MP4 container
  # To do so it performs the following:
  # - strips repeated SEQUENCE_HEADER OBUs
  # - strips PADDING, REDUNDANT_FRAME_HEADER and TEMPORAL_DELIMITER OBUs
  # - calculates the sizes of MP4 compatible chunks each containing one frame that will get shown
  #   in temporal and spatial IDs enabled in operating_point_idc[0], which by default is used
  #   for checking AV1 Argon Streams
  def prepare_with_scalability(self):
    self.dprint('=========================================')
    self.dprint('Opening {:s} output file'.format(self.fname_out))
    self.dprint('=========================================')
    f_out = open(self.fname_out, 'wb')

    self.chunks = []
    chunk_len = 0

    config_obus_positions =[]

    first_temp_delimiter = True
    seq_header_seen = False
    frame_shown_in_chunk = False
    first_frame = True

    while True:
      d = self.parse_obu()
      obu_type = d['obu_header']['obu_type']

      if (obu_type == OBU_TYPE_TEMPORAL_DELIMITER):
      # skip OBU_TYPE_PADDING as according to the spec it SHOULD NOT be used in MP4 container
        if first_temp_delimiter:
        # this is the beginning of the first chunk in current output file
          first_temp_delimiter = False
        else:
        # this is the end of chunk if the frame was shown in it
          if frame_shown_in_chunk:
            self.dprint('Frame was shown in this chunk, so we can end it')
            self.chunks.append(chunk_len)
            chunk_len = 0
            frame_shown_in_chunk = False

        # we don't write OBU_TYPE_TEMPORAL_DELIMITER to the ouput file as the spec requires that it SHOULD NOT be used in MP4 container
        self.dprint('Dropped TEMPORAL_DELIMITER')

      elif (obu_type == OBU_TYPE_PADDING):
      # skip OBU_TYPE_PADDING as according to the spec it SHOULD NOT be used in MP4 container
        self.dprint('Dropped PADDING')

      elif (obu_type == OBU_TYPE_REDUNDANT_FRAME_HEADER):
      # skip OBU_TYPE_REDUNDANT_FRAME_HEADER as according to the spec it SHOULD NOT be used in MP4 container
        self.dprint('Dropped REDUNDANT_FRAME_HEADER')

      elif (obu_type == OBU_TYPE_SEQUENCE_HEADER):
      # deal with SEQUENCE_HEADER
        if seq_header_seen:
        # this is repeated SEQUENCE_HEADER, so drop it
          self.dprint('Dropped repeated SEQUENCE_HEADER')
        else:
        # this is the first sequence header
          self.dprint('Found first sequence header')
          seq_header_seen = True

        # mark OBU position to be copied to config_obus later
          config_obus_positions.append({'offset' : self.stream.bytepos - d['obu_len'], 'len' : d['obu_len']})

        # copy it as it is over to the output file
          self.stream.bytepos -= d['obu_len']
          f_out.write(self.stream.read('bytes:' + str(d['obu_len'])))
          chunk_len += d['obu_len']

      elif ((obu_type == OBU_TYPE_FRAME) or (obu_type == OBU_TYPE_FRAME_HEADER)):
      # deal with FRAME and FRAME_HEADER
        if first_frame:
        # this is the first frame in the streams
          first_frame = False
        else:
        # this is the consecutive frame in the stream
          new_scalability = {'temporal_id' : d['obu_header']['temporal_id'], 'spatial_id' : d['obu_header']['spatial_id']}
          if (new_scalability != scalability):
          # scalability combination different from the previous one detected
            if frame_shown_in_chunk:
            # frame with decoded scalability was shown in the chunk, so end it and start a new one
              self.dprint('Frame with decoded scalability was shown, so end the chunk and start a new one')
              self.chunks.append(chunk_len)
              chunk_len = 0
              frame_shown_in_chunk = False

        scalability = {'temporal_id' : d['obu_header']['temporal_id'], 'spatial_id' : d['obu_header']['spatial_id']}
        show_existing_frame, show_frame = self.prepare_show(d)
        self.dprint('show_existing_frame = {:s}, show_frame = {:s}'.format(str(show_existing_frame), str(show_frame)))
        if self.is_scalability_decoded(scalability):
        # the scalability for this frame should be decoded
          if not frame_shown_in_chunk:
            if show_existing_frame or show_frame:
              frame_shown_in_chunk = True

        # copy it as it is over to the output file
        self.stream.bytepos -= d['obu_len']
        f_out.write(self.stream.read('bytes:' + str(d['obu_len'])))
        chunk_len += d['obu_len']

      elif (obu_type == OBU_TYPE_METADATA):
      # deal with METADATA
      # mark OBU position to be copied to config_obus later
        config_obus_positions.append({'offset' : self.stream.bytepos - d['obu_len'], 'len' : d['obu_len']})

      # copy OBU as it is over to the output file
        self.stream.bytepos -= d['obu_len']
        f_out.write(self.stream.read('bytes:' + str(d['obu_len'])))
        chunk_len += d['obu_len']

      else:
      # copy the rest of the OBUs as they are
        self.stream.bytepos -= d['obu_len']
        f_out.write(self.stream.read('bytes:' + str(d['obu_len'])))
        chunk_len += d['obu_len']

      self.dprint(' ')
      end_of_stream = (self.stream.pos >= self.stream.length)
      if end_of_stream:
      # gracefully finish off processing
        f_out.close()
        if (chunk_len > 0):
        # end the chunk only when there is something in it
          self.chunks.append(chunk_len)
        break

    if not self.dont_check_frame_show:
      if (len(self.chunks) == 0):
        raise ValueError('File {:s} contains no frames that will get shown'.format(self.fname))

    # prepare config_obus
    self.prepare_config_obus(config_obus_positions)

    self.dprint(' ')


  ## Function returns AV1 parameters required for putting it into MP4 container
  # @returns dictionary with MP4 compatible chunks sizes, AV1 sequence header and config OBUs
  def get_av1_params(self):
    av1_params = {}
    av1_params['chunks'] = self.chunks
    av1_params['sequence_header'] = self.state['sequence_header']
    av1_params['config_obus'] = self.config_obus
    return av1_params

  ## Function returns the raw video data format that is required by FFMPEG command line -pix_fmt argument
  # @ return video data description format required by FFMPEG -pix_fmt argument
  def get_ffmpeg_format(self):
    bit_depth = self.state['BitDepth']
    mono_chrome = self.state['sequence_header']['color_config']['mono_chrome']
    subsampling_x = self.state['sequence_header']['color_config']['subsampling_x']
    subsampling_y = self.state['sequence_header']['color_config']['subsampling_y']
    if (mono_chrome):
      if (bit_depth == 8):
        return 'gray8'
      elif (bit_depth == 10):
        return 'gray10le'
      elif (bit_depth == 12):
        return 'gray12le'
      else:
        raise ValueError('Unsupported combination of bit_depth={:d} mono_chrome={:s}, subsampling_x={:s}, subsampling_y={:s}'.format(bit_depth, mono_chrome, subsampling_x, subsampling_y))
    else:
    # colour video stream
      if (subsampling_x == True) and (subsampling_y == True):
      # profile 0
        if (bit_depth == 8):
          return 'yuv420p'
        elif (bit_depth == 10):
          return 'yuv420p10le'
        elif (bit_depth == 12):
          return 'yuv420p12le'
        else:
          raise ValueError('Unsupported combination of bit_depth={:d} mono_chrome={:s}, subsampling_x={:s}, subsampling_y={:s}'.format(bit_depth, mono_chrome, subsampling_x, subsampling_y))
      elif (subsampling_x == False) and (subsampling_y == False):
      # profile 1
        if (bit_depth == 8):
          return 'yuv444p'
        elif (bit_depth == 10):
          return 'yuv444p10le'
        elif (bit_depth == 12):
          return 'yuv444p12le'
        else:
          raise ValueError('Unsupported combination of bit_depth={:d} mono_chrome={:s}, subsampling_x={:s}, subsampling_y={:s}'.format(bit_depth, mono_chrome, subsampling_x, subsampling_y))
      elif (subsampling_x == True) and (subsampling_y == False):
      # other profiles
        if (bit_depth == 8):
          return 'yuv422p'
        elif (bit_depth == 10):
          return 'yuv422p10le'
        elif (bit_depth == 12):
          return 'yuv422p12le'
        else:
          raise ValueError('Unsupported combination of bit_depth={:d} mono_chrome={:s}, subsampling_x={:s}, subsampling_y={:s}'.format(bit_depth, mono_chrome, subsampling_x, subsampling_y))

      else:
      # unsuported combinations of mono_chrome, subsampling_x and subsampling_y
        raise ValueError('Unsupported combination of bit_depth={:d} mono_chrome={:s}, subsampling_x={:s}, subsampling_y={:s}'.format(bit_depth, mono_chrome, subsampling_x, subsampling_y))

if __name__ == '__main__':
  print ('This script is not supposed to be run from command line')
