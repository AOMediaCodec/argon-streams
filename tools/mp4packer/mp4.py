################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

from pkg_resources import parse_version
from kaitaistruct import __version__ as ks_version, KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if parse_version(ks_version) < parse_version('0.7'):
    raise Exception("Incompatible Kaitai Struct Python API: 0.7 or later is required, but you have %s" % (ks_version))

from vlq_base128_be import VlqBase128Be
class Mp4(KaitaiStruct):
    """
    .. seealso::
       ISO/IEC 14496-12:2005(E)
    """

    class DateEntryType(Enum):
        url = 1970433056

    class StreamType(Enum):
        forbidden = 0
        objectdescriptorstream = 1
        clockreferencestream = 2
        scenedescriptionstream = 3
        visualstream = 4
        audiostream = 5
        mpeg7stream = 6
        ipmpstream = 7
        objectcontentinfostream = 8
        mpegjstream = 9
        interactionstream = 10
        ipmptoolstream = 11

    class ClassTag(Enum):
        forbidden = 0
        objectdescrtag = 1
        initialobjectdescrtag = 2
        es_descrtag = 3
        decoderconfigdescrtag = 4
        decspecificinfotag = 5
        slconfigdescrtag = 6

    class ObjectType(Enum):
        forbidden = 0
        systems_iso_iec_14496_1_a = 1
        systems_iso_iec_14496_1_b = 2
        interaction_stream = 3
        systems_iso_iec_14496_1_extended_bifs_configuration_c = 4
        systems_iso_iec_14496_1_afx_d = 5
        font_data_stream = 6
        synthesized_texture_stream = 7
        streaming_text_stream = 8
        visual_iso_iec_14496_2_e = 32
        visual_itu_t_recommendation_h_264_or_iso_iec_14496_10_f = 33
        parameter_sets_for_itu_t_recommendation_h_264_or_iso_iec_14496_10_f = 34
        audio_iso_iec_14496_3_g = 64
        visual_iso_iec_13818_2_simple_profile = 96
        visual_iso_iec_13818_2_main_profile = 97
        visual_iso_iec_13818_2_snr_profile = 98
        visual_iso_iec_13818_2_spatial_profile = 99
        visual_iso_iec_13818_2_high_profile = 100
        visual_iso_iec_13818_2_422_profile = 101
        audio_iso_iec_13818_7_main_profile = 102
        audio_iso_iec_13818_7_lowcomplexity_profile = 103
        audio_iso_iec_13818_7_scaleable_sampling_rate_profile = 104
        audio_iso_iec_13818_3 = 105
        visual_iso_iec_11172_2 = 106
        audio_iso_iec_11172_3 = 107
        visual_iso_iec_10918_1 = 108
        no_object_type_specified = 255

    class Brand(Enum):
        x_3g2a = 862401121
        x_3ge6 = 862414134
        x_3ge9 = 862414137
        x_3gf9 = 862414393
        x_3gg6 = 862414646
        x_3gg9 = 862414649
        x_3gh9 = 862414905
        x_3gm9 = 862416185
        x_3gp4 = 862416948
        x_3gp5 = 862416949
        x_3gp6 = 862416950
        x_3gp7 = 862416951
        x_3gp8 = 862416952
        x_3gp9 = 862416953
        x_3gr6 = 862417462
        x_3gr9 = 862417465
        x_3gs6 = 862417718
        x_3gs9 = 862417721
        x_3gt9 = 862417977
        arri = 1095914057
        caep = 1128351056
        cdes = 1128555891
        j2p0 = 1244811312
        j2p1 = 1244811313
        lcag = 1279476039
        m4a = 1295270176
        m4b = 1295270432
        m4p = 1295274016
        m4v = 1295275552
        mfsm = 1296454477
        mgsv = 1296520022
        mppi = 1297109065
        msnv = 1297305174
        ross = 1380930387
        seau = 1397047637
        sebk = 1397047883
        xavc = 1480676931
        av01 = 1635135537
        avc1 = 1635148593
        bbxm = 1650620525
        caqv = 1667330422
        ccff = 1667458662
        da0a = 1684090977
        da0b = 1684090978
        da1a = 1684091233
        da1b = 1684091234
        da2a = 1684091489
        da2b = 1684091490
        da3a = 1684091745
        da3b = 1684091746
        dash = 1684108136
        dby1 = 1684175153
        dmb1 = 1684890161
        dsms = 1685286259
        dv1a = 1685467489
        dv1b = 1685467490
        dv2a = 1685467745
        dv2b = 1685467746
        dv3a = 1685468001
        dv3b = 1685468002
        dvr1 = 1685484081
        dvt1 = 1685484593
        dxo = 1685614368
        emsg = 1701671783
        ifrm = 1768321645
        isc2 = 1769169714
        iso2 = 1769172786
        iso3 = 1769172787
        iso4 = 1769172788
        iso5 = 1769172789
        iso6 = 1769172790
        isom = 1769172845
        jp2 = 1785737760
        jpm = 1785752864
        jpsi = 1785754473
        jpx = 1785755680
        jpxb = 1785755746
        lmsg = 1819112295
        mj2s = 1835676275
        mjp2 = 1835692082
        mp21 = 1836069425
        mp41 = 1836069937
        mp42 = 1836069938
        mp71 = 1836070705
        msdh = 1836278888
        msix = 1836280184
        niko = 1852402543
        odcf = 1868850022
        opf2 = 1869637170
        opx2 = 1869641778
        pana = 1885433441
        piff = 1885955686
        pnvi = 1886287465
        qt = 1903435808
        risx = 1919513464
        sdv = 1935963680
        senv = 1936027254
        sims = 1936289139
        sisx = 1936290680
        ssss = 1936946035
        uvvu = 1970697845

    class FieldOrdering(Enum):
        only_one_field = 0
        t_disp_earliest_t_stored_first = 1
        b_disp_earliest_b_stored_first = 6
        b_disp_earliest_t_stored_first = 9
        t_disp_earliest_b_stored_first = 14

    class GroupingType(Enum):
        roll = 1919904876

    class BoxType(Enum):
        xtra = 1484026465
        av01 = 1635135537
        av1c = 1635135811
        avc1 = 1635148593
        co64 = 1668232756
        data = 1684108385
        dinf = 1684631142
        dref = 1685218662
        edts = 1701082227
        elst = 1701606260
        esds = 1702061171
        fiel = 1718183276
        free = 1718773093
        ftyp = 1718909296
        hdlr = 1751411826
        hmhd = 1752000612
        ilst = 1768715124
        iods = 1768907891
        mdat = 1835295092
        mdhd = 1835296868
        mdia = 1835297121
        meta = 1835365473
        minf = 1835626086
        moof = 1836019558
        moov = 1836019574
        mp4a = 1836069985
        mvhd = 1836476516
        nmhd = 1852663908
        pasp = 1885434736
        sbgp = 1935828848
        sgpd = 1936158820
        smhd = 1936549988
        stbl = 1937007212
        stco = 1937007471
        stsc = 1937011555
        stsd = 1937011556
        stss = 1937011571
        stsz = 1937011578
        stts = 1937011827
        tkhd = 1953196132
        traf = 1953653094
        trak = 1953653099
        tref = 1953654118
        udta = 1969517665
        url = 1970433056
        urn = 1970433568
        vmhd = 1986881636
        vp09 = 1987063865
        vpcc = 1987076931
        cart = 2839630420
        cday = 2841928057
        cgen = 2842125678
        cnam = 2842583405
        ctoo = 2842980207

    class SlConfigDescriptorPredefined(Enum):
        custom = 0
        null_sl_packet_header = 1
        reserved_for_mp4 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.boxes = self._root.BoxList(self._io, self, self._root)

    class SgpdBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.40.3.3.2,  SampleGroupDescriptionBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.grouping_type = self._root.GroupingType(self._io.read_u4be())
            self.entry_count = self._io.read_u4be()
            if self.grouping_type == self._root.GroupingType.roll:
                self.roll_entries = [None] * (self.entry_count)
                for i in range(self.entry_count):
                    self.roll_entries[i] = self._root.RollEntry(self._io, self, self._root)




    class EsDescriptor(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-1:2004(E), section 7.2.6.5.1, ES_Descriptor class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.es_id = self._io.read_u2be()
            self.stream_dependence_flag = self._io.read_bits_int(1) != 0
            self.url_flag = self._io.read_bits_int(1) != 0
            self.ocr_stream_flag = self._io.read_bits_int(1) != 0
            self.stream_priority = self._io.read_bits_int(5)
            self._io.align_to_byte()
            if self.stream_dependence_flag == True:
                self.depends_on_es_id = self._io.read_u2be()

            if self.url_flag == True:
                self.url_length = self._io.read_u1()

            if self.url_flag == True:
                self.url_string = (self._io.read_bytes(self.url_length)).decode(u"UTF-8")

            if self.ocr_stream_flag == True:
                self.ocr_es_id = self._io.read_u2be()

            self.decoder_config_descriptor_tag = self._io.ensure_fixed_contents(b"\x04")
            self.decoder_config_descriptor_len = VlqBase128Be(self._io)
            self._raw_decoder_config_descriptor = self._io.read_bytes(self.decoder_config_descriptor_len.value)
            io = KaitaiStream(BytesIO(self._raw_decoder_config_descriptor))
            self.decoder_config_descriptor = self._root.DecoderConfigDescriptor(io, self, self._root)
            self.sl_config_descriptor_tag = self._io.ensure_fixed_contents(b"\x06")
            self.sl_config_descriptor_len = VlqBase128Be(self._io)
            self._raw_sl_config_descriptor = self._io.read_bytes(self.sl_config_descriptor_len.value)
            io = KaitaiStream(BytesIO(self._raw_sl_config_descriptor))
            self.sl_config_descriptor = self._root.SlConfigDescriptor(io, self, self._root)


    class RollEntry(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.40.4.2, VisualRollRecoveryEntry and AudioRollRecoveryEntry classes
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.roll_distance = self._io.read_s2be()


    class UrnBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.13, URN entry in DREF Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.name = (self._io.read_bytes_term(0, False, True, True)).decode(u"UTF-8")
            self.location = (KaitaiStream.bytes_terminate(self._io.read_bytes_full(), 0, False)).decode(u"UTF-8")


    class MvhdBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.4, MVHD (Movie Header) Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            if self.version_flags.version == 1:
                self.creation_time_v1 = self._io.read_u8be()

            if self.version_flags.version == 1:
                self.modification_time_v1 = self._io.read_u8be()

            if self.version_flags.version == 1:
                self.time_scale_v1 = self._io.read_u4be()

            if self.version_flags.version == 1:
                self.duration_v1 = self._io.read_u8be()

            if self.version_flags.version == 0:
                self.creation_time_v0 = self._io.read_u4be()

            if self.version_flags.version == 0:
                self.modification_time_v0 = self._io.read_u4be()

            if self.version_flags.version == 0:
                self.time_scale_v0 = self._io.read_u4be()

            if self.version_flags.version == 0:
                self.duration_v0 = self._io.read_u4be()

            self.rate = self._root.FixedS2U2(self._io, self, self._root)
            self.volume = self._root.FixedS1U1(self._io, self, self._root)
            self.reserved1 = self._io.read_bytes(10)
            self.matrix = self._io.read_bytes(36)
            self.pre_defined = self._io.read_bytes(24)
            self.next_track_id = self._io.read_u4be()


    class IlstBody(KaitaiStruct):
        """
        .. seealso::
           "Known iTunes Metadata Atoms" - http://atomicparsley.sourceforge.net/mpeg-4files.html
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entry = []
            i = 0
            while not self._io.is_eof():
                self.entry.append(self._root.Box(self._io, self, self._root))
                i += 1



    class ElstBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.26.2, EditListBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.entry_count = self._io.read_u4be()
            self.entries = [None] * (self.entry_count)
            for i in range(self.entry_count):
                self.entries[i] = self._root.ElstEntry(self._io, self, self._root)



    class StsdBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.16, Sample Descripion (STSD) Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.entry_count = self._io.read_u4be()
            self.entry = [None] * (self.entry_count)
            for i in range(self.entry_count):
                self.entry[i] = self._root.Box(self._io, self, self._root)



    class HintSampleEntry(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.16.2, HintSampleEntry class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sample_entry = self._root.SampleEntry(self._io, self, self._root)
            self.data = self._io.read_u1()


    class StcoBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.19.2, ChunkOffsetBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.entry_count = self._io.read_u4be()
            self.entries = [None] * (self.entry_count)
            for i in range(self.entry_count):
                self.entries[i] = self._io.read_u4be()



    class FtypBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 4.3, FTYP (File Type) Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.major_brand = self._root.Brand(self._io.read_u4be())
            self.minor_version = self._io.read_u4be()
            self.compatible_brands = []
            i = 0
            while not self._io.is_eof():
                self.compatible_brands.append(self._root.Brand(self._io.read_u4be()))
                i += 1



    class EsdsBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-1:2004(E), section 7.2.6.5.1, ES_Descriptor class
           ISO/IEC 14496-14:2003(E), section 5.6.1, class ESDBox
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.es_descriptor_tag = self._io.ensure_fixed_contents(b"\x03")
            self.es_descriptor_len = VlqBase128Be(self._io)
            self._raw_es_descriptor = self._io.read_bytes(self.es_descriptor_len.value)
            io = KaitaiStream(BytesIO(self._raw_es_descriptor))
            self.es_descriptor = self._root.EsDescriptor(io, self, self._root)


    class PaspBody(KaitaiStruct):
        """
        .. seealso::
           Source - https://developer.apple.com/library/archive/documentation/QuickTime/QTFF/QTFFChap3/qtff3.html#//apple_ref/doc/uid/TP40000939-CH205-124550
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.h_spacing = self._io.read_u4be()
            self.v_spacing = self._io.read_u4be()


    class FullBox(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 4.2, Full Box Type Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version = self._io.read_u1()
            self.flags = self._io.read_bytes(3)


    class UrlBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.13, URL entry in DREF Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.location = (KaitaiStream.bytes_terminate(self._io.read_bytes_full(), 0, False)).decode(u"UTF-8")


    class StszEntry(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.17.2, SampleSizeBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entry_size = self._io.read_u4be()


    class DataBody(KaitaiStruct):
        """
        .. seealso::
           "Known iTunes Metadata Atoms" - http://atomicparsley.sourceforge.net/mpeg-4files.html
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.unknown = self._io.read_bytes(8)
            self.data = (self._io.read_bytes((self._io.size() - 8))).decode(u"UTF-8")


    class DecoderSpecificInfo(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-1:2004(E), section 7.2.6.7.1, DecoderSpecific info class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.specific = self._io.read_bytes(self._parent.decoder_specific_info_len.value)


    class SampleEntry(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.16.2, SampleEntry abstract class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.reserved = self._io.read_bytes(6)
            self.data_reference_index = self._io.read_u2be()


    class AudioSampleEntry(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.16.2, AudioSampleEntry class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sample_entry = self._root.SampleEntry(self._io, self, self._root)
            self.reserved1 = self._io.read_bytes(8)
            self.channel_count = self._io.read_u2be()
            self.sample_size = self._io.read_u2be()
            self.pre_defined = self._io.read_u2be()
            self.reserved2 = self._io.read_bytes(2)
            self.samplerate = self._root.FixedU2U2(self._io, self, self._root)


    class HdlrBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.9, HDLR (Handler) Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.pre_defined = self._io.read_u4be()
            self.handler_type = (self._io.read_bytes(4)).decode(u"ASCII")
            self.reserved = self._io.read_bytes(12)
            self.name = (self._io.read_bytes_term(0, False, True, True)).decode(u"UTF-8")


    class Avc1Body(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.16.2, VideoSampleEntry class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.visual_sample_entry = self._root.VisualSampleEntry(self._io, self, self._root)


    class MdhdBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.8, MDHR (Media Header) Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            if self.version_flags.version == 1:
                self.creation_time_v1 = self._io.read_u8be()

            if self.version_flags.version == 1:
                self.modification_time_v1 = self._io.read_u8be()

            if self.version_flags.version == 1:
                self.timescale_v1 = self._io.read_u4be()

            if self.version_flags.version == 1:
                self.duration_v1 = self._io.read_u8be()

            if self.version_flags.version == 0:
                self.creation_time_v0 = self._io.read_u4be()

            if self.version_flags.version == 0:
                self.modification_time_v0 = self._io.read_u4be()

            if self.version_flags.version == 0:
                self.timescale_v0 = self._io.read_u4be()

            if self.version_flags.version == 0:
                self.duration_v0 = self._io.read_u4be()

            self.pad = self._io.read_bits_int(1) != 0
            self.lang_0 = self._io.read_bits_int(5)
            self.lang_1 = self._io.read_bits_int(5)
            self.lang_2 = self._io.read_bits_int(5)
            self._io.align_to_byte()
            self.pre_defined = self._io.read_u2be()


    class NmhdBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.11.5, NMHD (Null Media Header) Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)


    class SttsBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.15.2.2, TimeToSampleBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.entry_count = self._io.read_u4be()
            self.entries = [None] * (self.entry_count)
            for i in range(self.entry_count):
                self.entries[i] = self._root.SampleDeltaEntry(self._io, self, self._root)



    class FixedS2U2(KaitaiStruct):
        """Fixed-point 32-bit number containing signed integer 16 bit and unsigned fractional 16 bit part."""
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int_part = self._io.read_s2be()
            self.frac_part = self._io.read_u2be()


    class SlConfigDescriptor(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-1:2004(E), section 7.2.6.7.1, DecoderSpecific info class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.predefined = self._root.SlConfigDescriptorPredefined(self._io.read_u1())
            if self.predefined == self._root.SlConfigDescriptorPredefined.custom:
                self.use_access_unit_start_flag = self._io.read_bits_int(1) != 0



    class StszBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.17.2, SampleSizeBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.sample_size = self._io.read_u4be()
            self.sample_count = self._io.read_u4be()
            if self.sample_size == 0:
                self.entries = [None] * (self.sample_count)
                for i in range(self.sample_count):
                    self.entries[i] = self._root.StszEntry(self._io, self, self._root)




    class FielBody(KaitaiStruct):
        """
        .. seealso::
           , Table 4-2, video sample description extension - https://developer.apple.com/library/archive/documentation/QuickTime/QTFF/QTFFChap3/qtff3.html
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.field_count = self._io.read_u1()
            self.field_ordering = self._root.FieldOrdering(self._io.read_u1())


    class CnamBody(KaitaiStruct):
        """
        .. seealso::
           "Known iTunes Metadata Atoms" - http://atomicparsley.sourceforge.net/mpeg-4files.html
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entry = []
            i = 0
            while not self._io.is_eof():
                self.entry.append(self._root.Box(self._io, self, self._root))
                i += 1



    class SbgpBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.40.3.2.2, SampleToGroupBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.grouping_type = self._root.GroupingType(self._io.read_u4be())
            self.entry_count = self._io.read_u4be()
            if self.grouping_type == self._root.GroupingType.roll:
                self.entries = [None] * (self.entry_count)
                for i in range(self.entry_count):
                    self.entries[i] = self._root.SbgpEntry(self._io, self, self._root)




    class FixedS1U1(KaitaiStruct):
        """Fixed-point 16-bit number containing signed integer 8 bit and unsigned fractional 8 bit part."""
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int_part = self._io.read_s1()
            self.frac_part = self._io.read_u1()


    class VmhdBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.11.2, VMHD (Video Media Header) Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.graphicsmode = self._io.read_u2be()
            self.opcolor_red = self._io.read_u2be()
            self.opcolor_green = self._io.read_u2be()
            self.opcolor_blue = self._io.read_u2be()


    class Av01Body(KaitaiStruct):
        """
        .. seealso::
           ,section 2.2.3, AV1SampleEntry class - https://aomediacodec.github.io/av1-isobmff/
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.visual_sample_entry = self._root.VisualSampleEntry(self._io, self, self._root)
            self.av1c_box = self._root.Box(self._io, self, self._root)
            self.fiel_box = self._root.Box(self._io, self, self._root)


    class SbgpEntry(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.40.3.2.2, SampleToGroupBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sample_count = self._io.read_u4be()
            self.group_description_index = self._io.read_u4be()


    class DrefBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.13, DREF (Data Reference) Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.entry_count = self._io.read_u4be()
            self.data_entry = [None] * (self.entry_count)
            for i in range(self.entry_count):
                self.data_entry[i] = self._root.Box(self._io, self, self._root)



    class StssBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.20.2, SyncSampleBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.entry_count = self._io.read_u4be()
            self.sample_number = [None] * (self.entry_count)
            for i in range(self.entry_count):
                self.sample_number[i] = self._io.read_u4be()



    class CgenBody(KaitaiStruct):
        """
        .. seealso::
           "Known iTunes Metadata Atoms" - http://atomicparsley.sourceforge.net/mpeg-4files.html
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entry = []
            i = 0
            while not self._io.is_eof():
                self.entry.append(self._root.Box(self._io, self, self._root))
                i += 1



    class StscBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.18.2, SampleToChunkBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.entry_count = self._io.read_u4be()
            self.entries = [None] * (self.entry_count)
            for i in range(self.entry_count):
                self.entries[i] = self._root.StscEntry(self._io, self, self._root)



    class CdayBody(KaitaiStruct):
        """
        .. seealso::
           "Known iTunes Metadata Atoms" - http://atomicparsley.sourceforge.net/mpeg-4files.html
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entry = []
            i = 0
            while not self._io.is_eof():
                self.entry.append(self._root.Box(self._io, self, self._root))
                i += 1



    class FixedU2U2(KaitaiStruct):
        """Fixed-point 32-bit number containing unsigned integer 16 bit and unsigned fractional 16 bit part."""
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int_part = self._io.read_u2be()
            self.frac_part = self._io.read_u2be()


    class VpccBody(KaitaiStruct):
        """
        .. seealso::
           VPCodecConfigurationBox class - https://www.webmproject.org/vp9/mp4/
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.profile = self._io.read_u1()
            self.level = self._io.read_u1()
            self.bit_depth = self._io.read_bits_int(4)
            self.chroma_subsampling = self._io.read_bits_int(3)
            self.video_full_range_flag = self._io.read_bits_int(1) != 0
            self._io.align_to_byte()
            self.colour_primaries = self._io.read_u1()
            self.transfer_characteristics = self._io.read_u1()
            self.matrix_coefficients = self._io.read_u1()
            self.codec_intialization_data_size = self._io.read_u2be()


    class ElstEntry(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.26.2, EditListBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            if self._parent.version_flags.version == 1:
                self.segment_duration_v1 = self._io.read_u8be()

            if self._parent.version_flags.version == 1:
                self.media_time_v1 = self._io.read_s8be()

            if self._parent.version_flags.version == 0:
                self.segment_duration_v0 = self._io.read_u4be()

            if self._parent.version_flags.version == 0:
                self.media_time_v0 = self._io.read_s4be()

            self.media_rate = self._root.FixedS2S2(self._io, self, self._root)


    class CartBody(KaitaiStruct):
        """
        .. seealso::
           "Known iTunes Metadata Atoms" - http://atomicparsley.sourceforge.net/mpeg-4files.html
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entry = []
            i = 0
            while not self._io.is_eof():
                self.entry.append(self._root.Box(self._io, self, self._root))
                i += 1



    class Box(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len32 = self._io.read_u4be()
            self.box_type = self._root.BoxType(self._io.read_u4be())
            if self.len32 == 1:
                self.len64 = self._io.read_u8be()

            _on = self.box_type
            if _on == self._root.BoxType.tkhd:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.TkhdBody(io, self, self._root)
            elif _on == self._root.BoxType.sgpd:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.SgpdBody(io, self, self._root)
            elif _on == self._root.BoxType.data:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.DataBody(io, self, self._root)
            elif _on == self._root.BoxType.cgen:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.CgenBody(io, self, self._root)
            elif _on == self._root.BoxType.vp09:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.Vp09Body(io, self, self._root)
            elif _on == self._root.BoxType.stsd:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.StsdBody(io, self, self._root)
            elif _on == self._root.BoxType.urn:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.UrnBody(io, self, self._root)
            elif _on == self._root.BoxType.mvhd:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.MvhdBody(io, self, self._root)
            elif _on == self._root.BoxType.stsc:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.StscBody(io, self, self._root)
            elif _on == self._root.BoxType.stbl:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.BoxList(io, self, self._root)
            elif _on == self._root.BoxType.ctoo:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.CtooBody(io, self, self._root)
            elif _on == self._root.BoxType.av01:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.Av01Body(io, self, self._root)
            elif _on == self._root.BoxType.edts:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.BoxList(io, self, self._root)
            elif _on == self._root.BoxType.vpcc:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.VpccBody(io, self, self._root)
            elif _on == self._root.BoxType.moof:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.BoxList(io, self, self._root)
            elif _on == self._root.BoxType.hdlr:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.HdlrBody(io, self, self._root)
            elif _on == self._root.BoxType.ftyp:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.FtypBody(io, self, self._root)
            elif _on == self._root.BoxType.ilst:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.IlstBody(io, self, self._root)
            elif _on == self._root.BoxType.stsz:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.StszBody(io, self, self._root)
            elif _on == self._root.BoxType.avc1:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.Avc1Body(io, self, self._root)
            elif _on == self._root.BoxType.trak:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.BoxList(io, self, self._root)
            elif _on == self._root.BoxType.mdhd:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.MdhdBody(io, self, self._root)
            elif _on == self._root.BoxType.meta:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.MetaBody(io, self, self._root)
            elif _on == self._root.BoxType.sbgp:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.SbgpBody(io, self, self._root)
            elif _on == self._root.BoxType.cday:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.CdayBody(io, self, self._root)
            elif _on == self._root.BoxType.url:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.UrlBody(io, self, self._root)
            elif _on == self._root.BoxType.moov:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.BoxList(io, self, self._root)
            elif _on == self._root.BoxType.dinf:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.BoxList(io, self, self._root)
            elif _on == self._root.BoxType.av1c:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.Av1cBody(io, self, self._root)
            elif _on == self._root.BoxType.vmhd:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.VmhdBody(io, self, self._root)
            elif _on == self._root.BoxType.minf:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.BoxList(io, self, self._root)
            elif _on == self._root.BoxType.esds:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.EsdsBody(io, self, self._root)
            elif _on == self._root.BoxType.traf:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.BoxList(io, self, self._root)
            elif _on == self._root.BoxType.elst:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.ElstBody(io, self, self._root)
            elif _on == self._root.BoxType.udta:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.BoxList(io, self, self._root)
            elif _on == self._root.BoxType.nmhd:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.NmhdBody(io, self, self._root)
            elif _on == self._root.BoxType.mp4a:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.Mp4aBody(io, self, self._root)
            elif _on == self._root.BoxType.mdia:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.BoxList(io, self, self._root)
            elif _on == self._root.BoxType.stss:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.StssBody(io, self, self._root)
            elif _on == self._root.BoxType.co64:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.Co64Body(io, self, self._root)
            elif _on == self._root.BoxType.pasp:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.PaspBody(io, self, self._root)
            elif _on == self._root.BoxType.stco:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.StcoBody(io, self, self._root)
            elif _on == self._root.BoxType.dref:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.DrefBody(io, self, self._root)
            elif _on == self._root.BoxType.fiel:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.FielBody(io, self, self._root)
            elif _on == self._root.BoxType.smhd:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.SmhdBody(io, self, self._root)
            elif _on == self._root.BoxType.stts:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.SttsBody(io, self, self._root)
            elif _on == self._root.BoxType.cart:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.CartBody(io, self, self._root)
            elif _on == self._root.BoxType.hmhd:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.HmhdBody(io, self, self._root)
            elif _on == self._root.BoxType.cnam:
                self._raw_body = self._io.read_bytes(self.len)
                io = KaitaiStream(BytesIO(self._raw_body))
                self.body = self._root.CnamBody(io, self, self._root)
            else:
                self.body = self._io.read_bytes(self.len)

        @property
        def len(self):
            if hasattr(self, '_m_len'):
                return self._m_len if hasattr(self, '_m_len') else None

            self._m_len = ((self._io.size() - 8) if self.len32 == 0 else ((self.len64 - 16) if self.len32 == 1 else (self.len32 - 8)))
            return self._m_len if hasattr(self, '_m_len') else None


    class Vp09Body(KaitaiStruct):
        """
        .. seealso::
           VP9SampleEntry class - https://www.webmproject.org/vp9/mp4/
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.visual_sample_entry = self._root.VisualSampleEntry(self._io, self, self._root)
            self.vpcc_box = self._root.Box(self._io, self, self._root)
            self.fiel_box = self._root.Box(self._io, self, self._root)
            self.pasp_box = self._root.Box(self._io, self, self._root)


    class Mp4aBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-14:2003(E), section 5.6.1, MP4AudioSampleEntry class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.audio_sample_entry = self._root.AudioSampleEntry(self._io, self, self._root)
            self.esd_box = self._root.Box(self._io, self, self._root)


    class Co64Body(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.19.2, ChunkLargeOffsetBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.entry_count = self._io.read_u4be()
            self.entries = [None] * (self.entry_count)
            for i in range(self.entry_count):
                self.entries[i] = self._io.read_u8be()



    class Av1cBody(KaitaiStruct):
        """
        .. seealso::
           ,section 2.3.3, AV1CodecConfigurationBox class - https://aomediacodec.github.io/av1-isobmff/
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.marker = self._io.read_bits_int(1) != 0
            self.version = self._io.read_bits_int(7)
            self.seq_profile = self._io.read_bits_int(3)
            self.seq_level_idx_0 = self._io.read_bits_int(5)
            self.seq_tier_0 = self._io.read_bits_int(1) != 0
            self.high_bitdepth = self._io.read_bits_int(1) != 0
            self.twelve_bit = self._io.read_bits_int(1) != 0
            self.monochrome = self._io.read_bits_int(1) != 0
            self.chroma_subsampling_x = self._io.read_bits_int(1) != 0
            self.chroma_subsampling_y = self._io.read_bits_int(1) != 0
            self.chroma_sample_position = self._io.read_bits_int(2)
            self.reserved1 = self._io.read_bits_int(3)
            self.initial_presentation_delay_present = self._io.read_bits_int(1) != 0
            if self.initial_presentation_delay_present == True:
                self.initial_presentation_delay_minus_one = self._io.read_bits_int(4)

            if self.initial_presentation_delay_present == False:
                self.reserved2 = self._io.read_bits_int(4)

            self._io.align_to_byte()
            self.config_obus = self._io.read_bytes((self._io.size() - 4))


    class SmhdBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.11.3, SMHD (Sound Media Header) Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.balance = self._root.FixedS1U1(self._io, self, self._root)
            self.reserved = self._io.read_bytes(2)


    class DecoderConfigDescriptor(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-1:2004(E), section 7.2.6.6.1, DecoderConfigDescriptor class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.object_type_indication = self._root.ObjectType(self._io.read_u1())
            self.stream_type = self._root.StreamType(self._io.read_bits_int(6))
            self.up_stream = self._io.read_bits_int(1) != 0
            self.reserved = self._io.read_bits_int(1) != 0
            self.buffer_size_db = self._io.read_bits_int(24)
            self._io.align_to_byte()
            self.max_bitrate = self._io.read_u4be()
            self.avg_bitrate = self._io.read_u4be()
            self.decoder_specific_info_tag = self._io.ensure_fixed_contents(b"\x05")
            self.decoder_specific_info_len = VlqBase128Be(self._io)
            self._raw_decoder_specific_info = self._io.read_bytes(self.decoder_specific_info_len.value)
            io = KaitaiStream(BytesIO(self._raw_decoder_specific_info))
            self.decoder_specific_info = self._root.DecoderSpecificInfo(io, self, self._root)


    class HmhdBody(KaitaiStruct):
        """ISO/IEC 14496-12:2005(E), section 8.11.4, HMHD (Hint Media Header) Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.maxpdusize = self._io.read_u2be()
            self.avgpdusize = self._io.read_u2be()
            self.maxbitrate = self._io.read_u4be()
            self.avgbitrate = self._io.read_u4be()
            self.reserved = self._io.read_bytes(4)


    class TkhdBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.5, TKHD (Track Header) Box
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            if self.version_flags.version == 1:
                self.creation_time_v1 = self._io.read_u8be()

            if self.version_flags.version == 1:
                self.modification_time_v1 = self._io.read_u8be()

            if self.version_flags.version == 1:
                self.track_id_v1 = self._io.read_u4be()

            if self.version_flags.version == 1:
                self.reserved1_v1 = self._io.read_bytes(4)

            if self.version_flags.version == 1:
                self.duration_v1 = self._io.read_u8be()

            if self.version_flags.version == 0:
                self.creation_time_v0 = self._io.read_u4be()

            if self.version_flags.version == 0:
                self.modification_time_v0 = self._io.read_u4be()

            if self.version_flags.version == 0:
                self.track_id_v0 = self._io.read_u4be()

            if self.version_flags.version == 0:
                self.reserved1_v0 = self._io.read_bytes(4)

            if self.version_flags.version == 0:
                self.duration_v0 = self._io.read_u4be()

            self.reserved2 = self._io.read_bytes(8)
            self.layer = self._io.read_u2be()
            self.alternative_group = self._io.read_u2be()
            self.volume = self._io.read_u2be()
            self.reserved3 = self._io.read_bytes(2)
            self.matrix = self._io.read_bytes(36)
            self.width = self._root.FixedS2U2(self._io, self, self._root)
            self.height = self._root.FixedS2U2(self._io, self, self._root)


    class FixedS2S2(KaitaiStruct):
        """Fixed-point 32-bit number containing signed integer 16 bit and signed fractional 16 bit part."""
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int_part = self._io.read_s2be()
            self.frac_part = self._io.read_s2be()


    class CtooBody(KaitaiStruct):
        """
        .. seealso::
           "Known iTunes Metadata Atoms" - http://atomicparsley.sourceforge.net/mpeg-4files.html
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entry = []
            i = 0
            while not self._io.is_eof():
                self.entry.append(self._root.Box(self._io, self, self._root))
                i += 1



    class SampleDeltaEntry(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.15.2.2, entry in TimeToSampleBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sample_count = self._io.read_u4be()
            self.sample_delta = self._io.read_u4be()


    class StscEntry(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.18.2, SampleToChunkBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.first_chunk = self._io.read_u4be()
            self.samples_per_chunk = self._io.read_u4be()
            self.sample_description_index = self._io.read_u4be()


    class BoxList(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.items = []
            i = 0
            while not self._io.is_eof():
                self.items.append(self._root.Box(self._io, self, self._root))
                i += 1



    class MetaBody(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.44.2, MetaBox class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version_flags = self._root.FullBox(self._io, self, self._root)
            self.handler = self._root.Box(self._io, self, self._root)
            self.optional_boxes = []
            i = 0
            while not self._io.is_eof():
                self.optional_boxes.append(self._root.Box(self._io, self, self._root))
                i += 1



    class VisualSampleEntry(KaitaiStruct):
        """
        .. seealso::
           ISO/IEC 14496-12:2005(E), section 8.16.2, VisualSampleEntry class
        """
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sample_entry = self._root.SampleEntry(self._io, self, self._root)
            self.pre_defined1 = self._io.read_u2be()
            self.reserved1 = self._io.read_bytes(2)
            self.pre_defined2 = self._io.read_bytes(12)
            self.width = self._io.read_u2be()
            self.height = self._io.read_u2be()
            self.horizresolution = self._root.FixedU2U2(self._io, self, self._root)
            self.vertresolution = self._root.FixedU2U2(self._io, self, self._root)
            self.reserved2 = self._io.read_bytes(4)
            self.frame_count = self._io.read_u2be()
            self.compressor_name_len = self._io.read_u1()
            self.compressor_name = (KaitaiStream.bytes_terminate(self._io.read_bytes(31), 0, False)).decode(u"UTF-8")
            self.depth = self._io.read_u2be()
            self.pre_defined3 = self._io.read_s2be()



