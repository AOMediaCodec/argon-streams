################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import argparse
import os
import multiprocessing
import subprocess
import time
import sys

## Function performs test on a single VP9 stream
# @param fname - the name of file containing AV1 stream
# @param args - the object generated by argparse
# @param q - the instance of multiprocessing.Queue class to communicate with main process
# @return dictionary with results of the testing
def do_single_test(fname, args, q):
  vp9_fname = streams_folder + os.sep + fname
  md5ref_fname = md5ref_folder + os.sep + fname.replace('.ivf', '.md5')
  d = {}

  # pack VP9 into MP4 container
  mp4packer_vp9_cmmd = []
  mp4packer_vp9_cmmd.append('python2')
  mp4packer_vp9_cmmd.append('mp4packer_vp9.py')
  mp4packer_vp9_cmmd.append('-f')
  mp4packer_vp9_cmmd.append(str(args.rate))
  mp4packer_vp9_cmmd.append('-s')
  mp4packer_vp9_cmmd.append('-v')
  mp4packer_vp9_cmmd.append(vp9_fname)
  output = subprocess.check_output(mp4packer_vp9_cmmd)
  output_lines = output.split('\n')

  # get mp4 file name, pixel format and amount of chunks
  for i in output_lines:
    separated = i.split(' ')
    if (separated[0] == 'File'):
      d['fname'] = separated[1]
      d['pix_fmt'] = separated[2]
      d['fname_yuv'] = separated[1] + '.yuv'
      d['fname_vpxdec_yuv'] = separated[1] + '.vpxdec.yuv'
      d['chunks'] = separated[3]

  # run ffmpeg on resulting MP4 to get raw video data
  FNULL = open(os.devnull, 'w')
  ffmpeg_cmmd = []
  ffmpeg_cmmd.append('./mp4packer_vp9_ffmpeg/bin/ffmpeg')
  ffmpeg_cmmd.append('-i')
  ffmpeg_cmmd.append(d['fname'])
  ffmpeg_cmmd.append('-y')      # -y prevents ffmpeg from asking for confirmation to overwrite the file
  ffmpeg_cmmd.append('-vsync')  # -vsync 0 options prevents ffmpeg from inserting duplicate frames
  ffmpeg_cmmd.append('-0')      # when it thinks it is decoding faster than the frame rate
  ffmpeg_cmmd.append('-c:v')
  ffmpeg_cmmd.append('rawvideo')
  ffmpeg_cmmd.append('-pix_fmt')
  ffmpeg_cmmd.append(d['pix_fmt'])
  ffmpeg_cmmd.append(d['fname_yuv'])
  proc = subprocess.Popen(  ffmpeg_cmmd,
                            stderr = subprocess.STDOUT, # redirect process stderr to stdout
                            stdout = FNULL)             # redirect process stdout to /dev/null
  proc.wait()     # wait for FFMPEG processing to finish

# if required use aomdec reference decoder to decode files
  if args.vpxdec:
    vpxdec_cmmd = []
    vpxdec_cmmd.append('./mp4packer_vp9_vpxdec/libvpx_build/bin/vpxdec')
    vpxdec_cmmd.append(vp9_fname)
    vpxdec_cmmd.append('--rawvideo')
    # vpxdec_cmmd.append('--i420')
    vpxdec_cmmd.append('-o')
    vpxdec_cmmd.append(d['fname_vpxdec_yuv'])
    proc = subprocess.Popen(  vpxdec_cmmd,
                              stderr = subprocess.STDOUT, # redirect process stderr to stdout
                              stdout = FNULL)             # redirect process stdout to /dev/null
    proc.wait()     # wait for AOMDEC processing to finish

  # calculate md5sum of YUV file generated by FFmpeg
  md5sum_cmmd = []
  md5sum_cmmd.append('md5sum')
  md5sum_cmmd.append('-t')
  md5sum_cmmd.append(d['fname_yuv'])
  output = subprocess.check_output(md5sum_cmmd)
  d['md5'] = output.split(' ')[0]

  # get reference MD5 sum
  with open(md5ref_fname, 'r') as f_in:
    output_ref = f_in.readline()
    d['md5ref'] = output_ref.split(' ')[0]

  # if the test has passed delete original bitstream and all files generated from it
  # that saves a lot of space on the hard drive
  if (d['md5'] == d['md5ref']):
    for f in os.listdir(streams_folder):
      if f.startswith(fname):
        os.remove(streams_folder + os.sep + f)

  q.put(d)  # return the results over to main process

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description = 'Script runs a batch of VP9 tests where it uses mp4packer_vp9.py to put VP9 streams into MP4 container')
  parser.add_argument('-p', '--prefix', type = str, default = 'test', help = 'AV1 test files name prefix')
  parser.add_argument('-e', '--extension', type = str, default = '.ivf', help = 'AV1 test files name extension')
  parser.add_argument('-f', '--folder', type = str, default = 'test', help = 'The name of the folder with testing AV1 streams')
  parser.add_argument('-r', '--rate', type = int, default = 30, help = 'The frame rate of resulting mp4 videos')
  parser.add_argument('-d', '--vpxdec', action = 'store_true', help = 'Enables using vpxdec to process IVF file')
  parser.add_argument('-v', '--verbose', action = 'store_true', help = 'Enables verbose debugging output')
  parser.add_argument('-c', '--cpus_to_use', type = int, default = 6, help = 'Specifies how many CPUs should be used for testing')

  args = parser.parse_args()

  STREAMS_FOLDER_NAME = 'streams'
  MD5REF_FOLDER_NAME = 'md5_ref'

  if (args.folder.endswith(os.sep)):
    folder = args.folder[:-1]   # strip the folder separator from the end
  else:
    folder = args.folder

  streams_folder = folder + os.sep + STREAMS_FOLDER_NAME
  md5ref_folder = folder + os.sep + MD5REF_FOLDER_NAME

  # first generate the list of VP9 streams to be run as tests
  tests = []
  for fname in os.listdir(streams_folder):
    if fname.startswith(args.prefix) and fname.endswith(args.extension):
      tests.append(fname)

  # run external tools in parallel to perform the testing and capture the output
  results = []
  cpus_to_use = min(args.cpus_to_use, multiprocessing.cpu_count() - 2)
  running = []
  print ('Using {:d} CPUs to run tests in parallel'.format(cpus_to_use))
  print (' ')
  total_tests = len(tests)
  test_no = 1

  while True:
    # keep as many processes running in parallel as possible
    while ((len(running) <= cpus_to_use) and (len(tests) > 0)):
      print('Running test ({:d}/{:d})'.format(test_no, total_tests))
      test_no += 1
      q = multiprocessing.Queue() # instantiate a new Queue to communicate with the child process
      p = multiprocessing.Process(target = do_single_test, args = (tests[0], args, q))
      running.append({'q' : q, 'p' : p})
      p.start() # start child process
      del tests[0] # remove the command we have just started from the queue

    # now wait for at least one process to finish
    while True:
      time.sleep(0.01)   # sleep a little bit to reduce load on the main thread
      process_finished = False
      for i in range(len(running)):
        if running[i]['p'].is_alive() is not True:
        # process has finished execution
          results.append(running[i]['q'].get()) # get the result of testing from child process
          del running[i]  # remove it from the list of running processes
          process_finished = True
          break

      if process_finished:
        break

    # check if we have run all the commands
    if (tests == []) and (running == []):
      break

  # print the results
  print(' ')
  print('Results:')
  print('Res,    file, fmt, chunks')
  print(' ')

  total_count = 0
  passed_count = 0
  for i in results:
    total_count += 1
    if i['md5'] == i['md5ref']:
      passed_count += 1
      res = 'PASS'
    else:
      overall_pass = False
      res = 'FAILED'

    print('{:s}, {:s}, {:s}, {:s}'.format(res, i['fname'], i['pix_fmt'], i['chunks']))

  print(' ')
  print('Overall result: ({:d}/{:d}) {:f}% tests passed'.format(passed_count, total_count, 100.0 * float(passed_count)/float(total_count)))

  if (passed_count < total_count):
  # some tests have failed
    sys.exit(1)
  else:
  # all tests have passed
    sys.exit(0)

