################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

from vp9_process import Vp9Parse
import argparse

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description = 'Script takes input VP9 stream at given offset and tries to parse VP9 uncompressed_header')
  parser.add_argument('in_file', type = str, help = 'The name of the input VP9 stream file')
  parser.add_argument('offset', type = int, help = 'The in input file where the parsing should start')
  parser.add_argument('-v', '--verbose', action = 'store_true', help = 'Enables verbose debugging output')
  args = parser.parse_args()

  vp9_parse = Vp9Parse(args.in_file, args)
  d = vp9_parse.parse_frame(args.offset)
  print(d)
