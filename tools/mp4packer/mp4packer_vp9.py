################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

from vp9_process import ConvertIvfToVp9, Vp9Parse
from mp4_container import Mp4Container
import argparse
import os

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description = 'Script takes input VP9 stream in IVF format and packs it into MP4 container')
  parser.add_argument('in_file', type = str, help = 'The name of the input IVF file')
  parser.add_argument('-f', '--frame_rate', type = int, default = 30, help = 'Specifies frame rate of the video given in frames/second')
  parser.add_argument('-v', '--verbose', action = 'store_true', help = 'Enables verbose debugging output')
  parser.add_argument('-s', '--show_format', action = 'store_true', help = 'Displays format that should be used for ffmpeg -pix_fmt option')
  parser.add_argument('-n', '--no_delete', action = 'store_true', help = 'Don\'t delete or rename intermediate files')
  args = parser.parse_args()

  # define file types suffixes
  VP9_SUFFIX = '.vp9'

  # convert IVF to pure VP9 stream
  convert_ivf_to_vp9 = ConvertIvfToVp9(args.in_file, args, suffix = VP9_SUFFIX)
  mp4_params = convert_ivf_to_vp9.get_mp4_params()

  # put VP9 file into MP4 containter
  mp4_container = Mp4Container(convert_ivf_to_vp9.fname_out, args, suffix = '.mp4')
  mp4_container.put_vp9_into_mp4_container(mp4_params)
  if not args.no_delete:
    convert_ivf_to_vp9.cleanup_files()

  # print out stuff if necessary
  if args.show_format:
    print('File {:s} {:s} {:d}'.format(mp4_container.fname_out, convert_ivf_to_vp9.get_ffmpeg_format(), len(mp4_params['chunks'])))

