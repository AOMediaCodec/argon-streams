################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

from mp4 import Mp4
from mp4packer_defs import *

# class putting given AV1 stream into MP4 container
class Mp4Container:
  ## Function initializes Av1NonAnnexBParse object
  # @param fname - name of input AV1 file which will get put into MP4 container
  # @param args - object with command line arguments parsed by argparse
  # @param suffix - the suffix that will be used on the output file
  def __init__(self, fname, args, suffix = '.mp4'):
    self.fname = fname
    self.fname_out = fname + suffix
    self.verbose = args.verbose
    self.frame_rate = args.frame_rate

  ## Function prints string if verbose mode is set in command line arguments
  # @param string - string to be printed
  def dprint(self, string):
    if self.verbose:
      print(string)

  ## Function amends given fields in given dictionary with the values provided in the other dictionary
  # @param default - the dictionary which will be amended
  # @param amend - the dictionary with the values to be amended
  # @returns amended dictionary
  def amend_fields(self, default, amend):
    for key in amend:
      default[key] = amend[key]   # amend only the keys specified in amendments dictionary
    return default

  ## Function prepares the structure of the output mp4 container
  # It uses default container structure and amends its content with the values specified in
  # the dictionary given as an input argument
  # @param amend - the dictionary containing the values which need to be amended in the default MP4 container
  # @returns the structure describing final MP4 container
  def prepare_mp4_structure(self, amend):
    ftyp_top_vals = {
        'major_brand' : Mp4.Brand.isom,
        'minor_version' : 512,
        'compatible_brands' : [Mp4.Brand.av01, Mp4.Brand.iso6, Mp4.Brand.mp41]
    }
    ftyp_top = FtypBox(ftyp_top_vals)

    free_top = FreeBox(0)

    #=========================================
    # The content of MOOV TRAK box describing video
    #=========================================

    # TKHD box
    moov_trak_video_tkhd_vals_default = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x03']},
      'creation_time_v0'      : 0,
      'modification_time_v0'  : 0,
      'track_id_v0'           : 1,
      'reserved1_v0'          : ['\x00', '\x00', '\x00', '\x00'],
      'duration_v0'           : 2002,
      'reserved2'             : ['\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'],
      'layer'                 : 0,
      'alternative_group'     : 0,
      'volume'                : 0,
      'reserved3'             : ['\x00', '\x00'],
      'matrix'                : ['\x00', '\x01', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', \
                                 '\x00', '\x00', '\x00', '\x00', '\x00', '\x01', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', \
                                 '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x40', '\x00', '\x00', '\x00'],
      'width'                 : { 'int_part' : 1920, 'frac_part' : 0},
      'height'                : { 'int_part' : 800, 'frac_part' : 0},
    }
    moov_trak_video_tkhd_vals = self.amend_fields(  moov_trak_video_tkhd_vals_default,
                                                    amend['moov_trak_video_tkhd_vals'])
    moov_trak_video_tkhd = TkhdBox(moov_trak_video_tkhd_vals)

    # MDHD box
    moov_trak_video_mdia_mdhd_vals_default = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x00']},
      'creation_time_v0'      : 0,
      'modification_time_v0'  : 0,
      'timescale_v0'          : 24000,
      'duration_v0'           : 48048,
      'pad'                   : False,
      'lang_0'                : 21,
      'lang_1'                : 14,
      'lang_2'                : 4,
      'pre_defined'           : 0
    }
    moov_trak_video_mdia_mdhd_vals = self.amend_fields( moov_trak_video_mdia_mdhd_vals_default,
                                                        amend['moov_trak_video_mdia_mdhd_vals'])
    moov_trak_video_mdia_mdhd = MdhdBox(moov_trak_video_mdia_mdhd_vals)

    # HDLR box
    moov_trak_video_mdia_hdlr_vals = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x00']},
      'pre_defined'           : 0,
      'handler_type'          : 'vide',
      'reserved'              : ['\x00', '\x00', '\x00', '\x00', '\x00', '\x00',  \
                                 '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'],
      'name'                  : 'GPAC ISO Video Handler',
    }
    moov_trak_video_mdia_hdlr = HdlrBox(moov_trak_video_mdia_hdlr_vals)

    # VHMD box
    moov_trak_video_minf_vmhd_vals = {
        'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x01']},
        'graphicsmode'          : 0,
        'opcolor_red'           : 0,
        'opcolor_green'         : 0,
        'opcolor_blue'          : 0
    }
    moov_trak_video_minf_vmhd = VmhdBox(moov_trak_video_minf_vmhd_vals)

    # URL box
    moov_trak_video_minf_dinf_dref_url_vals = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x01']},
      'location'              : ''
    }
    moov_trak_video_minf_dinf_dref_url = UrlBox(moov_trak_video_minf_dinf_dref_url_vals)

    # DREF box
    moov_trak_video_minf_dinf_dref_vals = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x00']},
      'entry_count'           : 1,
      'data_entry'            : [moov_trak_video_minf_dinf_dref_url]
    }
    moov_trak_video_minf_dinf_dref = DrefBox(moov_trak_video_minf_dinf_dref_vals)

    # DINF box
    moov_trak_video_minf_dinf = DinfBox([moov_trak_video_minf_dinf_dref])

    # STSD box
    moov_trak_video_minf_stbl_stsd_vals_default = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x00']},
    }
    moov_trak_video_minf_stbl_stsd_vals = self.amend_fields(  moov_trak_video_minf_stbl_stsd_vals_default,
                                                              amend['moov_trak_video_minf_stbl_stsd_vals'])
    moov_trak_video_minf_stbl_stsd = StsdBox(moov_trak_video_minf_stbl_stsd_vals)

    # STTS box
    moov_trak_video_minf_stbl_stts_vals_default = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x00']},
      'entry_count'           : 1,
      'entries'               : [{'sample_count' : 48, 'sample_delta' : 1001}],
    }
    moov_trak_video_minf_stbl_stts_vals = self.amend_fields(  moov_trak_video_minf_stbl_stts_vals_default,
                                                              amend['moov_trak_video_minf_stbl_stts_vals'])
    moov_trak_video_minf_stbl_stts = SttsBox(moov_trak_video_minf_stbl_stts_vals)

    # STSC box
    moov_trak_video_minf_stbl_stsc_vals = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x00']},
      'entry_count'           : 1,
      'entries'               : [ {'first_chunk' : 1, 'samples_per_chunk' : 1, 'sample_description_index' : 1} ]
    }
    moov_trak_video_minf_stbl_stsc = StscBox(moov_trak_video_minf_stbl_stsc_vals)

    # STSZ box
    moov_trak_video_minf_stbl_stsz_entries = [
    # index 0
      { 'entry_size'        : 8343  },
      { 'entry_size'        : 93    },
      { 'entry_size'        : 35    },
      { 'entry_size'        : 34    },
      { 'entry_size'        : 430   },
    # index 5
      { 'entry_size'        : 250   },
      { 'entry_size'        : 156   },
      { 'entry_size'        : 50    },
      { 'entry_size'        : 166   },
      { 'entry_size'        : 600   },
    # index 10
      { 'entry_size'        : 36    },
      { 'entry_size'        : 6947  },
      { 'entry_size'        : 42    },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 58    },
    # index 15
      { 'entry_size'        : 41    },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 33    },
    # index 20
      { 'entry_size'        : 33    },
      { 'entry_size'        : 3787  },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 33    },
    # index 25
      { 'entry_size'        : 33    },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 33    },
    # index 30
      { 'entry_size'        : 33    },
      { 'entry_size'        : 5254  },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 33    },
    # index 35
      { 'entry_size'        : 45    },
      { 'entry_size'        : 31    },
      { 'entry_size'        : 31    },
      { 'entry_size'        : 31    },
      { 'entry_size'        : 31    },
    # index 40
      { 'entry_size'        : 31    },
      { 'entry_size'        : 33    },
      { 'entry_size'        : 31    },
      { 'entry_size'        : 31    },
      { 'entry_size'        : 31    },
    # index 45
      { 'entry_size'        : 31    },
      { 'entry_size'        : 31    },
      { 'entry_size'        : 31    }
    ]

    moov_trak_video_minf_stbl_stsz_vals_default = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x00']},
      'sample_size'           : 0,      # each sample has a different size
      'sample_count'          : 48,
      'entries'               : moov_trak_video_minf_stbl_stsz_entries
    }
    moov_trak_video_minf_stbl_stsz_vals = self.amend_fields(  moov_trak_video_minf_stbl_stsz_vals_default,
                                                              amend['moov_trak_video_minf_stbl_stsz_vals'])
    moov_trak_video_minf_stbl_stsz = StszBox(moov_trak_video_minf_stbl_stsz_vals)

    # STCO box
    moov_trak_video_minf_stbl_stco_entries = [
      8,      8351,   8444,   8479,   8513,   8943,   9193,   9349,   9399,   9565,   \
      10165,  10201,  17148,  17190,  17223,  17281,  17322,  17355,  17388,  17421,  \
      17454,  17487,  21274,  21307,  21340,  21373,  21406,  21439,  21472,  21505,  \
      21538,  21571,  26825,  26858,  26891,  26924,  26969,  27000,  27031,  27062,  \
      27093,  27124,  27157,  27188,  27219,  27250,  27281,  27312,                  \
    ]
    moov_trak_video_minf_stbl_stco_default = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x00']},
      'entry_count'           : 48,
      'entries'               : moov_trak_video_minf_stbl_stco_entries
    }
    moov_trak_video_minf_stbl_stco_vals = self.amend_fields(  moov_trak_video_minf_stbl_stco_default,
                                                              amend['moov_trak_video_minf_stbl_stco_vals'])
    moov_trak_video_minf_stbl_stco = StcoBox(moov_trak_video_minf_stbl_stco_vals)

    # STBL box
    moov_trak_video_minf_stbl = StblBox([ moov_trak_video_minf_stbl_stsd, \
                                          moov_trak_video_minf_stbl_stts, \
                                          moov_trak_video_minf_stbl_stsc, \
                                          moov_trak_video_minf_stbl_stsz, \
                                          moov_trak_video_minf_stbl_stco])

    # MINF box
    moov_trak_video_minf = MinfBox([moov_trak_video_minf_vmhd, moov_trak_video_minf_dinf, moov_trak_video_minf_stbl])

    # MDIA box
    moov_trak_video_mdia = MdiaBox([moov_trak_video_mdia_mdhd, moov_trak_video_mdia_hdlr, moov_trak_video_minf])

    # TRAK box
    moov_trak_video = TrakBox([moov_trak_video_tkhd, moov_trak_video_mdia])

    # MVHD box
    moov_mvhd_vals_default = {
      'version_flags'         : {'version' : 0, 'flags' : [0, 0 ,0]},
      'creation_time_v0'      : 0,
      'modification_time_v0'  : 0,
      'time_scale_v0'         : 1000,
      'duration_v0'           : 2024,
      'rate'                  : { 'int_part' : 1, 'frac_part' : 0},
      'volume'                : { 'int_part' : 1, 'frac_part' : 0},
      'reserved1'             : ['\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'],
      'matrix'                : ['\x00', '\x01', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', \
                                 '\x00', '\x00', '\x00', '\x00', '\x00', '\x01', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', \
                                 '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x40', '\x00', '\x00', '\x00'],
      'pre_defined'           : ['\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', \
                                 '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', \
                                 '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'],
      'next_track_id'         : 3
    }
    moov_mvhd_vals = self.amend_fields( moov_mvhd_vals_default,
                                        amend['moov_mvhd_vals'])
    moov_mvhd = MvhdBox(moov_mvhd_vals)

    # MDAT box
    mdat_top = MdatBox(amend['mdat_top'])

    # MOOV box
    moov_top = MoovBox([moov_mvhd, moov_trak_video])

    top = [mdat_top, ftyp_top, free_top, moov_top]
    return top

  ## Function puts AV1 stream into MP4 container
  # It uses default MP4 container and modifies necessary bits in it with the values provided in input arguments
  # @param av1_params - dictionary with MP4 chunks, AV1 sequence_header and config obus
  def put_av1_into_mp4_container(self, av1_params):
    self.dprint('=========================================')
    self.dprint('Putting {:s} file into MP4 container'.format(self.fname))
    self.dprint('=========================================')

    chunks = av1_params['chunks']
    sequence_header = av1_params['sequence_header']
    config_obus = av1_params['config_obus']

    # calculate some paramemeters that will be useful later
    time_multipler = 1000
    timescale = time_multipler * self.frame_rate
    duration = time_multipler * len(chunks)

    # now use parsed information to fill AV1 related MP4 boxes
    mp4_boxes_config = {}

    # MVHD box
    moov_mvhd_vals = {
      'time_scale_v0'         : timescale,
      'duration_v0'           : duration,
    }
    mp4_boxes_config['moov_mvhd_vals'] = moov_mvhd_vals

    # TKHD box
    moov_trak_video_tkhd_vals = {
      'width'                 : { 'int_part' : sequence_header['max_frame_width_minus_1'] + 1, 'frac_part' : 0},
      'height'                : { 'int_part' : sequence_header['max_frame_height_minus_1'] + 1, 'frac_part' : 0},
      'duration_v0'           : duration,
    }
    mp4_boxes_config['moov_trak_video_tkhd_vals'] = moov_trak_video_tkhd_vals

    # MDHD boxes
    moov_trak_video_mdia_mdhd_vals = {
      'timescale_v0'          : timescale,
      'duration_v0'           : duration
    }
    mp4_boxes_config['moov_trak_video_mdia_mdhd_vals'] = moov_trak_video_mdia_mdhd_vals

    # AV1C box
    if not 'chroma_sample_position' in sequence_header['color_config']:
      sequence_header['color_config']['chroma_sample_position'] = 0   # set some default value as it is required by AV1C box

    moov_trak_video_minf_stbl_stsd_av01_av1c_vals = {
      'marker'                                : True,
      'version'                               : 1,
      'seq_profile'                           : sequence_header['seq_profile'],
      'seq_level_idx_0'                       : sequence_header['seq_level_idx'][0],
      'seq_tier_0'                            : sequence_header['seq_tier'][0],
      'high_bitdepth'                         : sequence_header['color_config']['high_bitdepth'],
      'twelve_bit'                            : sequence_header['color_config']['twelve_bit'],
      'monochrome'                            : sequence_header['color_config']['mono_chrome'],
      'chroma_subsampling_x'                  : sequence_header['color_config']['subsampling_x'],
      'chroma_subsampling_y'                  : sequence_header['color_config']['subsampling_y'],
      'chroma_sample_position'                : sequence_header['color_config']['chroma_sample_position'],
      'reserved1'                             : 0,
      'initial_presentation_delay_present'    : sequence_header['initial_display_delay_present_flag'],
      'reserved2'                             : 3,
      'config_obus'                           : config_obus
    }
    moov_trak_video_minf_stbl_stsd_av01_av1c = Av1cBox(moov_trak_video_minf_stbl_stsd_av01_av1c_vals)

    # FIEL box
    moov_trak_video_minf_stbl_stsd_av01_fiel_vals = {
      'field_count'                           : 1,
      'field_ordering'                        : Mp4.FieldOrdering.only_one_field,
    }
    moov_trak_video_minf_stbl_stsd_av01_fiel = FielBox(moov_trak_video_minf_stbl_stsd_av01_fiel_vals)

    # AV01 box
    moov_trak_video_minf_stbl_stsd_av01_vals = {
      'visual_sample_entry'   : {
        'sample_entry'          : {
          'reserved'              : ['\x00', '\x00', '\x00', '\x00', '\x00', '\x00'],
          'data_reference_index'  : 1
        },
        'pre_defined1'        : 0,
        'reserved1'           : ['\x00', '\x00'],
        'pre_defined2'        : ['\x00', '\x00', '\x00', '\x00', '\x00', '\x00',  \
                                 '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'],
        'width'               : sequence_header['max_frame_width_minus_1'] + 1,
        'height'              : sequence_header['max_frame_height_minus_1'] + 1,
        'horizresolution'     : { 'int_part' : 72, 'frac_part' : 0},
        'vertresolution'      : { 'int_part' : 72, 'frac_part' : 0},
        'reserved2'           : ['\x00', '\x00', '\x00', '\x00'],
        'frame_count'         : 1,
        'compressor_name_len' : 0,
        'compressor_name'     : '',
        'depth'               : 24,
        'pre_defined3'        : -1
      },
      'av1c_box'            : moov_trak_video_minf_stbl_stsd_av01_av1c,
      'fiel_box'            : moov_trak_video_minf_stbl_stsd_av01_fiel,
    }
    moov_trak_video_minf_stbl_stsd_av01 = Av01Box(moov_trak_video_minf_stbl_stsd_av01_vals)

    # STSD box
    moov_trak_video_minf_stbl_stsd_vals = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x00']},
      'entry_count'           : 1,
      'entry'                 : [moov_trak_video_minf_stbl_stsd_av01],
    }
    mp4_boxes_config['moov_trak_video_minf_stbl_stsd_vals'] = moov_trak_video_minf_stbl_stsd_vals

    # STTS box
    moov_trak_video_minf_stbl_stts_vals = {
      'entry_count'   : 1,
      'entries'       : [{'sample_count' : len(chunks), 'sample_delta' : time_multipler}],
    }
    mp4_boxes_config['moov_trak_video_minf_stbl_stts_vals'] = moov_trak_video_minf_stbl_stts_vals

    # STSZ box
    moov_trak_video_minf_stbl_stsz_entries = []
    for i in chunks:
      moov_trak_video_minf_stbl_stsz_entries.append({ 'entry_size' : i})

    moov_trak_video_minf_stbl_stsz_vals = {
      'sample_count'          : len(chunks),
      'entries'               : moov_trak_video_minf_stbl_stsz_entries
    }
    mp4_boxes_config['moov_trak_video_minf_stbl_stsz_vals'] = moov_trak_video_minf_stbl_stsz_vals

    # STCO box
    offset = 0x08   # offset from the beginning of MP4 file to MDAT box content
                    # MDAT box is first in MP4 container, so we only have to skip MDAT box length (4 bytes) and MDAT box type (4 bytes)
    moov_trak_video_minf_stbl_stco_entries = []
    for i in chunks:
      moov_trak_video_minf_stbl_stco_entries.append(offset)
      offset += i

    moov_trak_video_minf_stbl_stco_vals = {
      'entry_count'           : len(chunks),
      'entries'               : moov_trak_video_minf_stbl_stco_entries
    }
    mp4_boxes_config['moov_trak_video_minf_stbl_stco_vals'] = moov_trak_video_minf_stbl_stco_vals

    # MDAT box
    mp4_boxes_config['mdat_top'] = self.fname

    # prepare the structure of MP4 container
    top = self.prepare_mp4_structure(mp4_boxes_config)

    # everything is ready, so lets generate MP4 file
    with open(self.fname_out, 'wb') as outfile:
      for i in top:
        i.box.tofile(outfile)


  ## Function puts VP9 stream into MP4 container
  # It uses default MP4 containter and modifies necessary bits in it with the values provided in input argument
  # @param vp9_params - dictionary with MP4 chunks and VP9 params parsed out from VP9 stream
  def put_vp9_into_mp4_container(self, vp9_params):
    self.dprint('=========================================')
    self.dprint('Putting {:s} file into MP4 container'.format(self.fname))
    self.dprint('=========================================')

    # convert parameters extracted from VP9 stream over to the ones required by MP4 container
    profile = vp9_params['Profile']
    bit_depth = vp9_params['BitDepth']
    subsampling_x = vp9_params['subsampling_x']
    subsampling_y = vp9_params['subsampling_y']
    # calculate chroma_sampling value as described in https://www.webmproject.org/vp9/mp4/
    if ((subsampling_x == False) and (subsampling_y == False)):
    # 4:4:4
      chroma_subsampling = 3
    elif ((subsampling_x == True) and (subsampling_y == False)):
    # 4:2:2
      chroma_subsampling = 2;
    else:
    # 4:2:0
      chroma_subsampling = 0

    if 'color_range' in vp9_params:
      video_full_range_flag = vp9_params['color_range']
    else:
      video_full_range_flag = True  # default value if there is none available in VP9 color_config
    colour_primaries = vp9_params['color_space']
    chunks = vp9_params['chunks']

    width = vp9_params['width']
    if (width >= 32768):
      width = 32767   # saturate at 2^15-1 as this is the maximum value MP4 box can accept

    height = vp9_params['height']
    if (height >= 32768):
      height = 32767   # saturate at 2^15-1 as this is the maximum value MP4 box can accept

    # calculate some paramemeters that will be useful later
    time_multipler = 1000
    timescale = time_multipler * self.frame_rate
    duration = time_multipler * len(chunks)

    # now use parsed information to fill AV9 related MP4 boxes
    mp4_boxes_config = {}

    # MVHD box
    moov_mvhd_vals = {
      'time_scale_v0'         : timescale,
      'duration_v0'           : duration,
    }
    mp4_boxes_config['moov_mvhd_vals'] = moov_mvhd_vals

    # TKHD box
    moov_trak_video_tkhd_vals = {
      'width'                 : { 'int_part' : width, 'frac_part' : 0},
      'height'                : { 'int_part' : height, 'frac_part' : 0},
      'duration_v0'           : duration,
    }
    mp4_boxes_config['moov_trak_video_tkhd_vals'] = moov_trak_video_tkhd_vals

    # MDHD boxes
    moov_trak_video_mdia_mdhd_vals = {
      'timescale_v0'          : timescale,
      'duration_v0'           : duration
    }
    mp4_boxes_config['moov_trak_video_mdia_mdhd_vals'] = moov_trak_video_mdia_mdhd_vals

    # VPCC box
    moov_trak_video_minf_stbl_stsd_vp09_vpcc_vals = {
      'version_flags'                     : { 'version' : 1, 'flags' : ['\x00', '\x00', '\x00']},
      'profile'                           : profile,
      'level'                             : 10,         # according to Annex A in https://storage.googleapis.com/downloads.webmproject.org/docs/vp9/vp9-bitstream-specification-v0.6-20160331-draft.pdf only level 1 is currently supported
      'bit_depth'                         : bit_depth,
      'chroma_subsampling'                : chroma_subsampling,
      'video_full_range_flag'             : video_full_range_flag,
      'colour_primaries'                  : colour_primaries,
      'transfer_characteristics'          : 2,    # don't know where to get this value from, but FFMPEG uses 2, so will use it anyway
      'matrix_coefficients'               : 2,    # don't know where to get this value from, but FFMPEG uses 2, so will use it anyway
      'codec_initialization_data_size'    : 0,    # according to https://www.webmproject.org/vp9/mp4/ it must be set to 0 for VP9 stream
    }
    moov_trak_video_minf_stbl_stsd_vp09_vpcc = VpccBox(moov_trak_video_minf_stbl_stsd_vp09_vpcc_vals)

    # FIEL box
    moov_trak_video_minf_stbl_stsd_vp09_fiel_vals = {
      'field_count'                           : 1,
      'field_ordering'                        : Mp4.FieldOrdering.only_one_field,
    }
    moov_trak_video_minf_stbl_stsd_vp09_fiel = FielBox(moov_trak_video_minf_stbl_stsd_vp09_fiel_vals)

    # PASP box
    moov_trak_video_minf_stbl_stsd_vp09_pasp_vals = {
      'h_spacing'               : 1,
      'v_spacing'               : 1,
    }
    moov_trak_video_minf_stbl_stsd_vp09_pasp = PaspBox(moov_trak_video_minf_stbl_stsd_vp09_pasp_vals)

    # VP09 box
    moov_trak_video_minf_stbl_stsd_vp09_vals = {
      'visual_sample_entry'   : {
        'sample_entry'          : {
          'reserved'              : ['\x00', '\x00', '\x00', '\x00', '\x00', '\x00'],
          'data_reference_index'  : 1
        },
        'pre_defined1'        : 0,
        'reserved1'           : ['\x00', '\x00'],
        'pre_defined2'        : ['\x00', '\x00', '\x00', '\x00', '\x00', '\x00',  \
                                 '\x00', '\x00', '\x00', '\x00', '\x00', '\x00'],
        'width'               : width,
        'height'              : height,
        'horizresolution'     : { 'int_part' : 72, 'frac_part' : 0},
        'vertresolution'      : { 'int_part' : 72, 'frac_part' : 0},
        'reserved2'           : ['\x00', '\x00', '\x00', '\x00'],
        'frame_count'         : 1,
        'compressor_name_len' : 0,
        'compressor_name'     : '',
        'depth'               : 24,   # this value doen't matter as it is being superseded in vpcC box
        'pre_defined3'        : -1
      },
      'vpcc_box'            : moov_trak_video_minf_stbl_stsd_vp09_vpcc,
      'fiel_box'            : moov_trak_video_minf_stbl_stsd_vp09_fiel,
      'pasp_box'            : moov_trak_video_minf_stbl_stsd_vp09_pasp,
    }
    moov_trak_video_minf_stbl_stsd_vp09 = Vp09Box(moov_trak_video_minf_stbl_stsd_vp09_vals)

    # STSD box
    moov_trak_video_minf_stbl_stsd_vals = {
      'version_flags'         : { 'version' : 0, 'flags' : ['\x00', '\x00', '\x00']},
      'entry_count'           : 1,
      'entry'                 : [moov_trak_video_minf_stbl_stsd_vp09],
    }
    mp4_boxes_config['moov_trak_video_minf_stbl_stsd_vals'] = moov_trak_video_minf_stbl_stsd_vals

    # STTS box
    moov_trak_video_minf_stbl_stts_vals = {
      'entry_count'   : 1,
      'entries'       : [{'sample_count' : len(chunks), 'sample_delta' : time_multipler}],
    }
    mp4_boxes_config['moov_trak_video_minf_stbl_stts_vals'] = moov_trak_video_minf_stbl_stts_vals

    # STSZ box
    moov_trak_video_minf_stbl_stsz_entries = []
    for i in chunks:
      moov_trak_video_minf_stbl_stsz_entries.append({ 'entry_size' : i})

    moov_trak_video_minf_stbl_stsz_vals = {
      'sample_count'          : len(chunks),
      'entries'               : moov_trak_video_minf_stbl_stsz_entries
    }
    mp4_boxes_config['moov_trak_video_minf_stbl_stsz_vals'] = moov_trak_video_minf_stbl_stsz_vals

    # STCO box
    offset = 0x08   # offset from the beginning of MP4 file to MDAT box content
                    # MDAT box is first in MP4 container, so we only have to skip MDAT box length (4 bytes) and MDAT box type (4 bytes)
    moov_trak_video_minf_stbl_stco_entries = []
    for i in chunks:
      moov_trak_video_minf_stbl_stco_entries.append(offset)
      offset += i

    moov_trak_video_minf_stbl_stco_vals = {
      'entry_count'           : len(chunks),
      'entries'               : moov_trak_video_minf_stbl_stco_entries
    }
    mp4_boxes_config['moov_trak_video_minf_stbl_stco_vals'] = moov_trak_video_minf_stbl_stco_vals

    # MDAT box
    mp4_boxes_config['mdat_top'] = self.fname

    # prepare the structure of MP4 container
    top = self.prepare_mp4_structure(mp4_boxes_config)

    # everything is ready, so lets generate MP4 file
    with open(self.fname_out, 'wb') as outfile:
      for i in top:
        i.box.tofile(outfile)

if __name__ == '__main__':
  print ('This script is not supposed to be run from command line')
