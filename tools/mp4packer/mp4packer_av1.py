################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

from av1_process import Av1NonAnnexBCheckAndFix, Av1ConvertToNonAnnexB, Av1SplitOnDiffSeqHeaders, Av1ForMp4
from mp4_container import Mp4Container
import argparse
import os

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description = 'Script takes input AV1 stream and packs it into MP4 container')
  parser.add_argument('av1_file', type = str, help = 'The name of the input AV1 file')
  parser.add_argument('-f', '--frame_rate', type = int, default = 30, help = 'Specifies frame rate of the video given in frames/second')
  parser.add_argument('-d', '--dont_check_frame_show', action = 'store_true', help = 'Disables checking if resulting mp4 file contains at least one frame that will get shown')
  parser.add_argument('-b', '--annex_b', action = 'store_true', help = 'Specifies that input AV1 stream is based on Annex B Length delimited bitstream format')
  parser.add_argument('-v', '--verbose', action = 'store_true', help = 'Enables verbose debugging output')
  parser.add_argument('-s', '--show_format', action = 'store_true', help = 'Displays format that should be used for ffmpeg -pix_fmt option')
  parser.add_argument('-n', '--no_delete', action = 'store_true', help = 'Don\'t delete or rename intermediate files')
  parser.add_argument('-w', '--fix_missing_temp_del', action = 'store_true', help = 'When present, it fixes any missing temporal delimiters')
  args = parser.parse_args()

  # define file types suffixes
  NON_ANNEX_B_SUFFIX = '.nab'
  CHECKED_SUFFIX = '.checked'
  STRIP_SUFFIX = '.s'

  # convert to AV1 non Annex B format if necessary
  av1_convert_to_non_annexb = Av1ConvertToNonAnnexB(args.av1_file, args, suffix = NON_ANNEX_B_SUFFIX)

  # check and optionally fix non Annex B file
  av1_non_annexb_check_and_fix = Av1NonAnnexBCheckAndFix(av1_convert_to_non_annexb.fname_out, args, suffix = CHECKED_SUFFIX)
  if not args.no_delete:
    av1_convert_to_non_annexb.cleanup_files()

  # perform actual processing and put AV1 file into MP4 container
  av1_split_on_diff_seq_headers = Av1SplitOnDiffSeqHeaders(av1_non_annexb_check_and_fix.fname_out, args)
  split_list = av1_split_on_diff_seq_headers.split()

  if not args.no_delete:
    av1_non_annexb_check_and_fix.cleanup_files()

  for fname in split_list:
    # prepare AV1 file so it can be put into MP4 container
    av1_for_mp4 = Av1ForMp4(fname, args, suffix = STRIP_SUFFIX)
    av1_params = av1_for_mp4.get_av1_params()
    if args.verbose:
      print(av1_params['chunks'])

    # put AV1 file into MP4 containter
    mp4_container = Mp4Container(av1_for_mp4.fname_out, args, suffix = '.mp4')
    mp4_container.put_av1_into_mp4_container(av1_params)
    if not args.no_delete:
      av1_for_mp4.cleanup_files()

    # remove unncessary suffixes from the final files
    fname_out = mp4_container.fname_out
    del mp4_container # to guarantee closing final MP4 files
    new_fname_out = fname_out.replace(NON_ANNEX_B_SUFFIX, '').replace(CHECKED_SUFFIX, '').replace(STRIP_SUFFIX, '')
    os.rename(fname_out, new_fname_out)

    # print out stuff if necessary
    if args.show_format:
      print('File {:s} {:s} {:d}'.format(new_fname_out, av1_for_mp4.get_ffmpeg_format(), len(av1_params['chunks'])))

  # cleanup intermediate files
  if not args.no_delete:
    av1_split_on_diff_seq_headers.cleanup_files()
