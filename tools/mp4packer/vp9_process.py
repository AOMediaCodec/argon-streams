################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

from bitstring import ConstBitStream, BitArray, Bits, pack, BitStream
import os

# definitions of VP9 frame_type field
KEY_FRAME =       False
NON_KEY_FRAME =   True

# definitions of VP9 color_space field
CS_UNKNOWN =            0
CS_BT_601 =             1
CS_BT_709 =             2
CS_SMPTE_170 =          3
CS_SMPTE_240 =          4
CS_BT_2020 =            5
CS_RESERVED =           6
CS_RGB =                7

## Class containing utilities that allow for parsing pure VP9 stream
class Vp9Parse(object):

  ## Function initializes Vp9Parse object
  # @param fname - name of VP9 file which will be parsed
  # @param args - object with command line arguments parsed by argparse
  def __init__(self, fname, args):
    self.verbose = args.verbose
    self.fname = fname
    self.stream = ConstBitStream(filename = fname)
    self.state = {}

  ## Function prints string if verbose mode is set in command line arguments
  # @param string - string to be printed
  def dprint(self, string):
    if self.verbose:
      print(string)

  ## Function parses and checks frame_sync_code() VP9 structure as described in the spec
  def parse_frame_sync_code(self):
    stream = self.stream
    byte_0 = stream.read('uint:8')
    byte_1 = stream.read('uint:8')
    byte_2 = stream.read('uint:8')
    if (  (byte_0 != 0x49) or
          (byte_1 != 0x83) or
          (byte_2 != 0x42)):
      raise ValueError('frame sync code in file {:s} at offset 0x{:X} is bad'.format(self.fname, stream.pos / 8))

  ## Function parses VP9 color_config structure as described in the spec.
  # @return the dictionary with parsed fields
  # @param d - the dictionary containing other stuff parsed already from uncompressed_header() structure
  def parse_color_config(self, d):
    stream = self.stream
    state = self.state
    if (state['Profile'] >= 2):
      d['ten_or_twelve_bit'] = stream.read('bool')
      if d['ten_or_twelve_bit']:
        state['BitDepth'] = 12
      else:
        state['BitDepth'] = 10
    else:
      state['BitDepth'] = 8
    d['color_space'] = stream.read('uint:3')
    if (d['color_space'] != CS_RGB):
      d['color_range'] = stream.read('bool')
      if ((state['Profile'] == 1) or (state['Profile'] == 3)):
        d['subsampling_x'] = stream.read('bool')
        d['subsampling_y'] = stream.read('bool')
        reserved_zero = stream.read('bool')
        if (reserved_zero == True):
          raise ValueError('reserved_zero in color_config structure in file {:s} at offset 0x{:X} is set'.format(self.name, stream.bytepos))
      else:
        d['subsampling_x'] = True
        d['subsampling_y'] = True
    else:
      d['color_range'] = True
      if ((state['Profile'] == 1) or (state['Profile'] == 3)):
        d['subsampling_x'] = False
        d['subsampling_y'] = False
        reserved_zero = stream.read('bool')
        if (reserved_zero == True):
          raise ValueError('reserved_zero in color_config structure in file {:s} at offset 0x{:X} is set'.format(self.name, stream.bytepos))
    return d

  ## Function parses VP9 uncompressed_header() structure as described in the spec.
  # It does it to the point where it gets all the information required by mp4 container
  # @return the dictionary with parsed fields
  def parse_uncompressed_header(self):
    d = {}
    stream = self.stream
    state = self.state
    d['frame_marker'] = stream.read('uint:2')
    if (d['frame_marker'] != 2):
      raise ValueError('frame_marker in file {:s} at offset 0x{:X} is not equal to 2'.format(self.fname, stream.pos / 8))
    d['profile_low_bit'] = stream.read('bool')
    d['profile_high_bit'] = stream.read('bool')
    state['Profile'] = (int(d['profile_high_bit']) << 1) + int(d['profile_low_bit'])
    if (state['Profile'] == 3):
      d['reserved_zero'] = stream.read('bool')
      if (d['reserved_zero'] == True):
        raise ValueError('reserved_zero in file {:s} at offset 0x{:X} is set'.format(self.name, stream.pos / 8))
    d['show_existing_frame'] = stream.read('bool')
    if (d['show_existing_frame'] == True):
      d['frame_to_show_map_idx'] = stream.read('uint:3')
      d['header_size_in_bytes'] = 0
      d['refresh_frame_flags'] = 0
      d['loop_filter_level'] = 0
      return d
    d['frame_type'] = stream.read('bool')
    d['show_frame'] = stream.read('bool')
    d['error_resilient_mode'] = stream.read('bool')
    if (d['frame_type'] == KEY_FRAME):
      self.parse_frame_sync_code()
      self.parse_color_config(d)
      # the rest is not decoded here as it is not necessary for our purpose
      state['FrameIsIntra'] = True
    else:
    # it is not a KEY_FRAME
      if (d['show_frame'] == False):
        d['intra_only'] = stream.read('bool')
      else:
        d['intra_only'] = False
      state['FrameIsIntra'] = d['intra_only']
      if (d['error_resilient_mode'] == False):
        d['reset_frame_context'] = stream.read('uint:2')
      else:
        d['reset_frame_context'] = False
      if (d['intra_only'] == True):
        self.parse_frame_sync_code()
        if (state['Profile'] > 0):
          self.parse_color_config(d)
        else:
          d['color_space'] = CS_BT_601
          d['subsampling_x'] = True
          d['subsampling_y'] = True
          state['BitDepth'] = 8
          # the rest is not decoded here as it is not necessary for our purpose

    return d

  ## Function parses VP9 frame() structure as described in the spec
  # @param offset - the offset in bytes from the beginning of VP9 stream where the parsing should start
  # @return the dictionary with parsed fields
  def parse_frame(self, offset):
    stream = self.stream
    stream.bytepos = offset  # get the offset to the beginning of the frame
    d = self.parse_uncompressed_header()
    return d

## Class converts IVM file to pure VP9 stream
class ConvertIvfToVp9(Vp9Parse):

  ## Function initializes IvfConvertToVp9 object
  # @param fname - name of the file containing IVF stream to be converted
  # @param args - object with command line arguments parsed by argparse
  # @param suffix - suffix added to the output file
  def __init__(self, fname, args, suffix = '.vp9'):
    self.verbose = args.verbose
    self.fname = fname
    self.fname_out = fname + suffix
    self.ivf_header = {}
    self.mp4_params = {}
    super(ConvertIvfToVp9, self).__init__(fname, args) # open the stream so it can be parsed
    self.convert()

  ## Function parses single frame in IVF file
  # @param f_out - object representing file where pure VP9 output will get written
  # @return dictionary with parsed fields
  def parse_ivf_frame(self, f_out):
    d = {}
    stream = self.stream
    d['size'] = stream.read('uintle:32')
    d['timestamp'] = stream.read('uintle:64')
    vp9_frame_start_pos = stream.bytepos

    # write VP9 frame out to the output file
    f_out.write(stream.read('bytes:' + str(d['size'])))
    vp9_frame_end_pos = stream.bytepos

    # seek again to the beginning of VP9 frame and parse the information from the header
    d['vp9_header'] = self.parse_frame(vp9_frame_start_pos)   # parse VP9 frame to get the information from the header

    # now seek in the stream to the end of the frame
    stream.bytepos = vp9_frame_end_pos
    return d

  ## Function checks if current VP9 frame will get shown or not
  # @param vp9_header - the dictionary with values parsed out of VP9 header
  # @return True if the frame will get shown, False otherwise
  def is_frame_shown(self, vp9_header):
    show_existing_frame = vp9_header['show_existing_frame']
    if 'show_frame' in vp9_header:
      show_frame = vp9_header['show_frame']
    else:
      # missing show_frame field in the header means that show_frame is False
      show_frame = False
    self.dprint('show_existing_frame = {:s}, show_frame = {:s}'.format(str(show_existing_frame), str(show_frame)))
    res = show_existing_frame or show_frame
    return res

  ## Function converts IVF file into pure VP9 stream
  def convert(self):
    with open(self.fname_out, 'wb') as f_out:
      # first parse IVF file header
      d = self.ivf_header
      stream = self.stream
      d['signature'] = stream.read('hex:32')
      if (d['signature'] != '444b4946'):
      # IVF file signature is not ASCII 'DKIF' string
        raise ValueError('{:s} file header signature is incorrect'.format(self.fname))
      d['version'] = stream.read('uintle:16')
      d['len'] = stream.read('uintle:16')
      d['codec'] = stream.read('hex:32')
      if (d['codec'] != '56503930'):
      # IVF file codec type is not ASCII 'VP90' string
        raise ValueError('{:s} file codec signature is incorrect'.format(self.fname))
      d['width'] = stream.read('uintle:16')
      self.mp4_params['width'] = d['width']
      d['height'] = stream.read('uintle:16')
      self.mp4_params['height'] = d['height']
      d['frame_rate'] = stream.read('uintle:32')
      d['time_scale'] = stream.read('uintle:32')
      d['num_of_frames'] = stream.read('uintle:32')
      d['unused'] = stream.read('hex:32')
      self.dprint('File {:s} contains {:d} frames of {:d} x {:d} resolution'.format(self.fname, d['num_of_frames'], d['width'], d['height']))
      self.dprint(' ')

      chunks = []
      for i in range(d['num_of_frames']):
        frame = self.parse_ivf_frame(f_out)
        self.grab_mp4_params(frame['vp9_header'])
        chunks.append(frame['size'])  # MP4 chunks are always the size of IVF frame regardless if these frames are shown or not
        self.dprint('Frame ({:d}/{:d}) size={:d} bytes, timestamp={:d}'.format(i + 1, d['num_of_frames'], frame['size'], frame['timestamp']))
        self.dprint(' ')

      self.mp4_params['chunks'] = chunks

  ## Function checks if specific parameter was stored in mp4 param dictionary.
  # If not, it stores it. It also checks if the value of the parameter was consistent throughout whole VP9 stream
  # @param key - dictionary key for the parameter
  # @param source - source dictionary with the parameters
  def check_and_store_mp4_param(self, key, source):
    if key in source:
      if key in self.mp4_params:
      # we have already captured this parameter
        if(source[key] != self.mp4_params[key]):
          # raise ValueError('Inconsistent {:s} value across {:s} file'.format(key, self.fname))
          print('Inconsistent {:s} value across {:s} file'.format(key, self.fname))
      else:
      # save the parameter
        self.mp4_params[key] = source[key]

  ## Function grabs specific params out of VP9 uncompressed_header() that are useful for mp4 container
  # @param vp9_header - the dictionary containing parameters parsed out of VP9 uncompressed_header()
  def grab_mp4_params(self, vp9_header):
    # first check the parameters from VP9 state
    params = ['Profile', 'BitDepth']
    for p in params:
      self.check_and_store_mp4_param(p, self.state)

    # and then check VP9 frame parameters
    params = ['subsampling_x', 'subsampling_y', 'color_range', 'color_space']
    for p in params:
      self.check_and_store_mp4_param(p, vp9_header)

  ## Function returns specific params that are useful for mp4 container
  # @return the dictionary with the params
  def get_mp4_params(self):
    return self.mp4_params

  ## Function returns the raw video data format that is required by FFMPEG command line -pix_fmt argument
  # @ return video data description format required by FFMPEG -pix_fmt argument
  def get_ffmpeg_format(self):
    bit_depth = self.mp4_params['BitDepth']
    subsampling_x = self.mp4_params['subsampling_x']
    subsampling_y = self.mp4_params['subsampling_y']
    if (subsampling_x == True) and (subsampling_y == True):
    # VP9 profile 0 and 2
      if (bit_depth == 8):
        return 'yuv420p'      # VP9 profile 0
      elif (bit_depth == 10):
        return 'yuv420p10le'  # VP9 profile 2
      elif (bit_depth == 12):
        return 'yuv420p12le'  # VP9 profile 2
      else:
        raise ValueError('Unsupported combination of bit_depth={:d}, subsampling_x={:s}, subsampling_y={:s}'.format(bit_depth, str(subsampling_x), str(subsampling_y)))
    elif (subsampling_x == False) and (subsampling_y == False):
    # VP9 profile 1 and 3
      if (bit_depth == 8):
        return 'yuv444p'      # VP9 profile 1
      elif (bit_depth == 10):
        return 'yuv444p10le'  # VP9 profile 3
      elif (bit_depth == 12):
        return 'yuv444p12le'  # VP9 profile 3
      else:
        raise ValueError('Unsupported combination of bit_depth={:d}, subsampling_x={:s}, subsampling_y={:s}'.format(bit_depth, str(subsampling_x), str(subsampling_y)))
    elif (subsampling_x == False) and (subsampling_y == True):
    # VP9 profile 1 and 3
      if (bit_depth == 8):
        return 'yuv440p'      # VP9 profile 1
      elif (bit_depth == 10):
        return 'yuv440p10le'  # VP9 profile 3
      elif (bit_depth == 12):
        return 'yuv440p12le'  # VP9 profile 3
      else:
        raise ValueError('Unsupported combination of bit_depth={:d}, subsampling_x={:s}, subsampling_y={:s}'.format(bit_depth, str(subsampling_x), str(subsampling_y)))
    elif (subsampling_x == True) and (subsampling_y == False):
    # VP9 profile 1 and 3
      if (bit_depth == 8):
        return 'yuv422p'      # VP9 profile 1
      elif (bit_depth == 10):
        return 'yuv422p10le'  # VP9 profile 3
      elif (bit_depth == 12):
        return 'yuv422p12le'  # VP9 profile 3
      else:
        raise ValueError('Unsupported combination of bit_depth={:d}, subsampling_x={:s}, subsampling_y={:s}'.format(bit_depth, str(subsampling_x), str(subsampling_y)))
    else:
    # should never get here
      raise ValueError('Unsupported combination of bit_depth={:d}, subsampling_x={:s}, subsampling_y={:s}'.format(bit_depth, str(subsampling_x), str(subsampling_y)))

  ## Function deletes output file(s)
  def cleanup_files(self):
    self.dprint('Deleting {:s} file'.format(self.fname_out))
    os.remove(self.fname_out)
