/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

// av1_syntax_9.gen.h needs integer types defined in av1_pre.h
#include "av1_pre.h"

// This defines VIDEO_STREAM_T.
#include "av1_syntax_9.gen.h"

// This defines NUM_FRAME_CONTEXTS
#include "av1_defines.h"

struct data_s {
    uint8_t *data;   // Source of encoded data
    int      len;    // Number of bytes of encoded data

    int      bitpos; // Position within a byte (starts at 7 to indicate most
                     // significant bit read first)

    uint8    num;    // Current byte

#if !ENCODE
    int pos;         // Index into encoded data
    int chunk_end;
#endif
};

struct toplevel_s {
    VIDEO_STREAM_T video_stream;

#if ENCODE
    struct data_s *data;
    int            nest;
#else
    struct data_s  data;
#endif

    GLOBAL_DATA_T *global_data;
    FRAME_CDFS_T   frame_contexts[NUM_FRAME_CONTEXTS];
};


// Local Variables:
// mode: c++
// End:
