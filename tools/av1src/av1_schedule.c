/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

// Frame scheduling code for our encoder
// This handles the selection of what frame types, ref frames, etc.
// are to be used for each frame in the sequence.
// Doing things this way allows us to handle much more complex situations
// than generating this information "on the fly" per frame.

#if ENCODE

// Is frame number 'f' allowed to reference frame number 'ref'?
// This only checks compatibility in terms of the scalability information,
// and doesn't check (eg.) that 'ref' is actually available in the reference buffers.
uint1 enc_compatible_reference(f, ref) {
  if (!enc_use_scalability) return 1

  spatialLayerF = sched_spatial_layer[f]
  temporalLayerF = sched_temporal_layer[f]
  temporalUnitF = sched_temporal_unit[f]
  spatialLayerRef = sched_spatial_layer[ref]
  temporalLayerRef = sched_temporal_layer[ref]
  temporalUnitRef = sched_temporal_unit[ref]

  if (! (enc_temporal_ref_allowed[temporalLayerF] & (1 << temporalLayerRef)))
    return 0

  if (!enc_use_scalability_preset && temporal_group_description_present) {
    // If we have a custom temporal group, then we might have some
    // "spatial switching-up points". If this is an enhancement layer
    // (but *not* if this is a base layer), then we aren't allowed to
    // reference across one of these points.
    if (spatialLayerF > 0) {
      maxRefDist = enc_max_enhancement_ref_dist[temporalUnitF % temporal_group_size]
      temporalDistance = temporalUnitF - temporalUnitRef
      if (temporalDistance > maxRefDist) {
        return 0
      }
    }
  }

  if (enc_use_scalability_preset || temporal_group_description_present) {
    // Special case: The only allowable references in this case are:
    // * Up to three "temporal references" (ie, frames from different
    //   temporal units, but in the same spatial layer), pre-selected
    //   by the temporal group structure
    // * Any "spatial reference" (ie, same temporal unit but possibly
    //   different spatial layer) which would otherwise be allowed
    if (temporalUnitF != temporalUnitRef) {
      if (spatialLayerF != spatialLayerRef) {
        return 0
      }

      tgIndex = temporalUnitF % temporal_group_size
      // Calculate the distance between 'f' and 'ref', in frames
      // *within the current spatial layer*
      // TODO: apparently this distance is "in display order", which
      // we can't easily infer here. For the moment, we calculate the distance
      // in the encoded frame order
      dist = 0
      for (i = f-1; i >= ref; i--) {
        if (sched_spatial_layer[i] == spatialLayerF) {
          dist += 1
        }
      }
      // Check if this is one of the allowed temporal references
      for (i = 0; i < temporal_group_ref_cnt[tgIndex]; i++) {
        if (temporal_group_ref_pic_diff[tgIndex][i] == dist) {
          return 1
        }
      }
      // If we get here, this is not an allowed temporal reference
      return 0
    }
  } else if (spatial_layer_description_present) {
    // Special case: If spatial_layer_ref_id[] is set (either explicitly or
    // implicitly through a scalability preset), cross-spatial-layer
    // references are only allowed *within the same temporal unit*.
    if (temporalUnitF != temporalUnitRef)
      return (spatialLayerF == spatialLayerRef)
  }

  return (enc_spatial_ref_allowed[spatialLayerF] & (1 << spatialLayerRef)) != 0
}

// Are we allowed to use a particular reference slot for this frame?
uint1 enc_compatible_ref_slot(f, slot) {
  ref = sched_frame_in_ref_slot[f-1][slot]
  return enc_compatible_reference(f, ref)
}

uint1 enc_frame_in_operating_point(f, oppoint) {
  spatialLayer = sched_spatial_layer[f]
  temporalLayer = sched_temporal_layer[f]
  operatingPointMask = enc_layers_to_operating_points[spatialLayer][temporalLayer]
  return (operatingPointMask & (1 << oppoint)) != 0
}

// Possible values for sched_special_frame
#define NORMAL_FRAME 0
#define SHOWN_KF 1
#define FORWARD_KF 2
#define SHOW_FWD_KF 3

// First step: Set {temporal unit number, spatial ID, temporal ID, show flag}
// for each frame.
// Notes:
// * Here, setting the "show flag" means this is either a shown frame or
//   a show-existing of a previously unshown frame. We don't yet need to decide
//   which of those two the frame will be.
// * We need to set the show flag here because any shown or show-existing frame
//   always marks the end of the current spatial layer in the current temporal unit.
// * The converse of the above is not true - ie, layers (or temporal units as a whole)
//   are allowed to end with unshown frames. Thus we need to explicitly indicate
//   where temporal delimiters should go, and we do this with a temporal unit number
//   for convenience.
// This may all change depending on the answers to:
// https://bugs.chromium.org/p/aomedia/issues/detail?id=1848
enc_schedule_scalability() {
  if (enc_use_scalability) {
    ASSERT(!enc_large_scale_tile, "Scalability not allowed in large scale tile mode")
    // Which operating points have decoded a shown keyframe so far?
    // We need this so that we can ensure that the first frame decoded in
    // each operating point is always a shown keyframe.
    // Operating point 'i' is represented by bit 'i' in this value
    uint32 seen_keyframe
    seen_keyframe = 0

    // Don't start scalable streams with forward keyframes for the moment
    sched_initial_fwd_kf = 0

    // Outer loop - Generates one temporal unit per iteration.
    // We only check the frame count 'f' against the target frame count 'enc_frame_cnt'
    // once per temporal unit, since we want to generate a whole number of temporal units.
    // This may lead to f being larger than enc_frame_cnt when we finally exit the loop.
    f = 0
    current_tu = 0
    while (f < enc_frame_cnt) {
      if (temporal_group_description_present) {
        tgIndex = current_tu % temporal_group_size
        current_temporal_layer = temporal_group_temporal_id[tgIndex]
      } else {
        // Due to the way timing info is specified, temporal scalability is only
        // easily able to support cases where each temporal layer is some fixed multiple
        // of the framerate of the layer below. In practice, this will always be 2x.
        // So we make each temporal layer have twice the framerate of the one below.
        current_temporal_layer = Max(0, enc_temporal_layers - 1 - ctz(current_tu))
      }

      // Generate a bitmask of which spatial layers we're going to include in this
      // temporal unit.
      // The only constraint is that every operating point *which decodes the current
      // temporal layer* must decode at least one spatial layer within the temporal unit.
      // So we select spatial layers randomly and then fix up any constraints
      spatial_layers_in_tu = ChooseBits(enc_spatial_layers)
      for (i = 0; i < enc_num_operating_points; i++) {
        if (enc_temporal_layers_used[i] & (1 << current_temporal_layer)) {
          // This operating point includes the current temporal layer, so we
          // need to ensure that it decodes at least one spatial layer in
          // this TU
          mask = enc_spatial_layers_used[i]
          if (! (spatial_layers_in_tu & mask)) {
            // We aren't currently encoding any frames for this operating point.
            // Fix it up by adding the lowest spatial layer decoded by this
            // operating point.
            lowest_spatial_layer = ctz(mask)
            spatial_layers_in_tu |= (1 << lowest_spatial_layer)
          }
        }
      }

      // Inner loop - Generates one frame per iteration
      current_spatial_layer = 0
      for (i=0; i<=MAX_SPATIAL_ID; i++) {
        TemporalIds[i] = -1
      }
      while (current_spatial_layer < enc_spatial_layers) {
        // Check if we should skip this spatial layer
        if (! (spatial_layers_in_tu & (1 << current_spatial_layer))) {
          current_spatial_layer += 1
          continue
        }
        // Sometimes change the temporal_id
        if (!temporal_group_description_present &&
            current_spatial_layer != 0) {
          if (TemporalIds[current_spatial_layer] != -1) {
            current_temporal_layer = TemporalIds[current_spatial_layer]
          } else if (ChooseBit()) {
            // loop picking a random but valid temporal layer
            while (1) {
              current_temporal_layer = Choose(0, enc_temporal_layers)
              for (i = 0; i < enc_num_operating_points; i++) {
                if (enc_temporal_layers_used[i] & (1 << current_temporal_layer)) {
                  break
                }
              }
              if (i < enc_num_operating_points) {
                break
              }
            }
          }
        }
        TemporalIds[current_spatial_layer] = current_temporal_layer

        sched_temporal_unit[f] = current_tu
        sched_spatial_layer[f] = current_spatial_layer
        sched_temporal_layer[f] = current_temporal_layer

        // Which operating points will this frame be decoded in?
        operatingPointMask = enc_layers_to_operating_points[current_spatial_layer][current_temporal_layer]

        // Does this need to be a shown keyframe?
        sched_special_frame[f] = NORMAL_FRAME
        for (i = 0; i < enc_num_operating_points; i++) {
          if ((operatingPointMask & (1<<i)) && !(seen_keyframe & (1<<i))) {
            sched_special_frame[f] = SHOWN_KF
            seen_keyframe |= operatingPointMask
            break
          }
        }

        if (temporal_group_description_present) {
          sched_show_frame[f] = 1
        } else if (sched_special_frame[f] == SHOWN_KF) {
          sched_show_frame[f] = 1
        } else {
          sched_show_frame[f] u(0)
        }

        // Terminate each spatial layer with a shown frame, as required by the spec.
        if (sched_show_frame[f]) {
          current_spatial_layer += 1
        }

        f++
      }
      current_tu++
    }
    // We may have overshot the target frame count, since we're aiming to always
    // produce full temporal units. So we need to update enc_frame_cnt.
    enc_frame_cnt = f
    ASSERT(enc_frame_cnt <= ENC_MAX_FRAMES, "Need to increase ENC_MAX_FRAMES")
  } else {
    for (f = 0; f < enc_frame_cnt; f++) {
      sched_special_frame[f] = NORMAL_FRAME
      sched_show_frame[f] u(0)
      sched_spatial_layer[f] = 0
      sched_temporal_layer[f] = 0
    }
    // Decide between two options for beginning the stream:
    // * A single shown keyframe (creating a true "coded video sequence")
    // * A hidden keyframe followed by a show-existing frame (a random-access point)
    sched_initial_fwd_kf = 0
    if (! enc_large_scale_tile) {
      sched_initial_fwd_kf u(0)
    }
    if (sched_initial_fwd_kf) {
      sched_special_frame[0] = FORWARD_KF
      sched_show_frame[0] = 0
      sched_special_frame[1] = SHOW_FWD_KF
      sched_show_frame[1] = 1
    } else {
      sched_special_frame[0] = SHOWN_KF
      sched_show_frame[0] = 1
    }
    // Force the last frame to be shown so that it ends up
    // in the output
    sched_show_frame[enc_frame_cnt-1] = 1

    // Label each frame with its corresponding temporal unit
    current_tu = 0
    f = 0

    if (sched_initial_fwd_kf) {
      // Place the show-existing frame and the forward KF in separate
      // temporal units. This generates a slightly-invalid stream, but
      // the idea is to simulate a forward-kf stream where the intervening
      // frames have been discarded.
      sched_temporal_unit[0] = 0
      sched_temporal_unit[1] = 1
      current_tu = 2
      f = 2
    } else {
      sched_temporal_unit[0] = 0
      current_tu = 1
      f = 1
    }

    for (; f < enc_frame_cnt; f++) {
      sched_temporal_unit[f] = current_tu
      if (sched_show_frame[f]) {
        current_tu += 1
      }
    }
  }
}

// Second step: Assign some miscellaneous parameters for each frame

// Mark a small fraction of frames as "barrier frames".
// This is not an official term; what it means is that frames have refresh_flags = 0xff,
// and so no later frame can reference any earlier frame. We need to schedule these frames
// early, because otherwise the linked frame system makes it difficult to find space for
// such frames.
enc_schedule_barriers() {
  enc_barrier_prob u(0)

  for (f = 0; f < enc_frame_cnt; f++) {
    isBarrier = (sched_special_frame[f] == SHOWN_KF ||
                 sched_special_frame[f] == SHOW_FWD_KF)
    if (! (enc_large_scale_tile || isBarrier)) {
      enc_frame_is_barrier u(0)
      isBarrier = isBarrier || enc_frame_is_barrier
    }
    sched_barrier_frame[f] = isBarrier
  }
}

enc_schedule_order_hints() {
  if (! enc_enable_order_hint)
    return 0

  enc_increasing_order_hints u(0)
  inc_seg_len = enc_increasing_order_hints ? (1 << enc_order_hint_bits) - 1 : 0

  // If enc_increasing_order_hints then start by increasing in order
  // before choosing randomly
  for (f = 0; f < enc_frame_cnt; f++) {
    if (f <= inc_seg_len) {
      enc_order_hint = f
    } else {
      enc_order_hint u(0)
    }
    sched_order_hint[f] = enc_order_hint
  }
}

sched_get_valid_refs(f) {
  // Collect up lists of which reference slots we can use, and also which
  // of the valid references count as "forward" refs.
  sched_num_valid_refs = 0
  sched_num_fwd_refs = 0

  for (j = 0; j < NUM_REF_FRAMES; j++) {
    frameNum = sched_frame_in_ref_slot[f-1][j]
    ASSERT(frameNum != -1, "Frame hasn't been scheduled")
    if (enc_compatible_ref_slot(f, j)) {
      sched_valid_refs[sched_num_valid_refs] = j
      sched_num_valid_refs += 1

      if (enc_enable_order_hint) {
        refOffset = sched_order_hint[frameNum]
        if (get_relative_dist(refOffset, sched_order_hint[f]) < 0) {
          sched_fwd_refs[sched_num_fwd_refs] = j
          sched_num_fwd_refs += 1
        }
      }
    }
  }
}

// Helper: Decide what reference frames to use for an INTER or S-FRAME
// Returns 1 if this is okay, or 0 if we need to fall back to an intra-only frame
uint1 sched_pick_ref_frames(f) {
  if (sched_num_valid_refs == 0) {
    ASSERT(sched_num_links[f] == 0, "Invalid linked frames")
    return 0 // Need to fall back to an intra-only frame
  }

  // Decide whether to use short frame signalling or not
  // We don't need to be super clever here, so try something simple:
  // * If there are >2 required refs, or any required refs count as backward refs,
  //   don't try short ref signalling
  // * If there are <= 2 requried refs, and they are forward refs, use those as
  //   LAST_FRAME and GOLDEN_FRAME respectively
  // * If LAST_FRAME and GOLDEN_FRAME are not set yet, set them to random forward refs
  //   (note that there's no requirement for these to be different)
  // * Then call setup_short_frame_refs() to work out the resulting list of references
  // * If all the references are valid, then use short signalling
  // * Otherwise, don't use short signalling
  //
  // This will miss many cases where *some* short ref set would work, but seems
  // to generate plenty of uses overall
  if (enc_enable_order_hint) {
    curOffset = sched_order_hint[f]

    enc_use_short_signaling u(0)
    useShortSignalling = enc_allow_short_signalling ? enc_use_short_signaling : 0
    if (enc_use_scalability) {
      // It's difficult to support short signalling when combined with
      // scalability: Not only do we have to ensure that the selected ref frames
      // are all compatible (ie, in appropriate spatial and temporal layers),
      // but we need to ensure that the same reference list is generated
      // regardless of operating point.
      //
      // The latter is because the output of the short ref process depends on
      // *all* of the ref slots' order hints, including ones which might vary
      // depending on the operating point used (so can't be used as a reference
      // directly).
      //
      // Offline discussion: Short refs signalling isn't really
      // intended to be combined with scalability. We're not going to explicitly
      // disallow this, combination, but encoders are very unlikely to use it
      // and so we can get away with not generating this combination.
      useShortSignalling = 0
    } else if (sched_num_links[f] > 2) {
      useShortSignalling = 0
    } else if (sched_num_fwd_refs == 0) {
      // We must have at least one forward ref available to use short signalling
      useShortSignalling = 0
    } else {
      // Check that any linked frames are forward references
      for (i = 0; i < sched_num_links[f]; i++) {
        linkedSlot = sched_linked_frame[f][i]
        linkedFrameNum = sched_frame_in_ref_slot[f-1][linkedSlot]
        linkOffset = sched_order_hint[linkedFrameNum]
        if (get_relative_dist(linkOffset, curOffset) >= 0) {
          useShortSignalling = 0
          break
        }
      }
    }

    if (useShortSignalling) {
      enc_last_frame_idx u(0)
      ASSERT(enc_last_frame_idx >= 0 && enc_last_frame_idx < sched_num_fwd_refs, "enc_last_frame_idx is out of bounds. It must be between 0 and (numFwdRefs - 1) = %d inclusive.", sched_num_fwd_refs - 1)
      lstFrame = sched_num_links[f] >= 1 ? sched_linked_frame[f][0] : sched_fwd_refs[enc_last_frame_idx]

      enc_gold_frame_idx u(0)
      ASSERT(enc_gold_frame_idx >= 0 && enc_gold_frame_idx < sched_num_fwd_refs, "enc_gold_frame_idx is out of bounds. It must be between 0 and (numFwdRefs - 1) = %d inclusive.", sched_num_fwd_refs - 1)
      gldFrame = sched_num_links[f] >= 2 ? sched_linked_frame[f][1] : sched_fwd_refs[enc_gold_frame_idx]

      // Generate the full reference list into short_ref_idx[]
      // and copy into sched_active_ref_idx[] as we'll need it for later
      // Note: Our selected lstFrame and gldFrame will be copied into the
      // appropriate places in short_ref_idx[].
      setup_short_frame_refs(lstFrame, gldFrame, f)
      for (i = 0; i < INTER_REFS_PER_FRAME; i++) {
        sched_active_ref_idx[f][i] = short_ref_idx[i]
      }

      // Now we just need to check that all the refs are valid
      for (i = 0; i < INTER_REFS_PER_FRAME; i++) {
        refSlot = short_ref_idx[i]
        if (! enc_compatible_ref_slot(f, refSlot)) {
          useShortSignalling = 0
          break
        }
      }
    }
  } else {
    useShortSignalling = 0
  }

  sched_use_short_ref_signalling[f] = useShortSignalling

  // If we couldn't use short signalling, or decided not to use it, then fall
  // back to the standard way of encoding ref frames
  if (!useShortSignalling) {
    // Clear the ref frame list
    for (i = 0; i < INTER_REFS_PER_FRAME; i++) {
      sched_active_ref_idx[f][i] = -1
    }

    // Assign each linked frame to a random reference idx
    enc_assign_all_possible_refs u(0)
    count = 0
    for (i = 0; i < sched_num_links[f]; i++) {
      uint3 validIdcs[NUM_REF_FRAMES]
      numValidIdcs = 0
      for (j = 0; j < INTER_REFS_PER_FRAME; j++) {
        if (sched_active_ref_idx[f][j] == -1) {
          validIdcs[numValidIdcs] = j
          numValidIdcs += 1
        }
      }
      ASSERT(numValidIdcs >= 1, "Can't find free ref idx")
      if (enc_assign_all_possible_refs &&
          count <= (numValidIdcs-1)) {
        index = count
        count++
      } else {
        index = Choose(0, numValidIdcs-1)
      }
      refIdx = validIdcs[index]
      sched_active_ref_idx[f][refIdx] = sched_linked_frame[f][i]
    }

    // Now pick the remaining references randomly
    count = 0
    for (j = 0; j < INTER_REFS_PER_FRAME; j++) {
      if (sched_active_ref_idx[f][j] == -1) {
        if (enc_assign_all_possible_refs &&
          count <= (sched_num_valid_refs-1)) {
          index = count
          count++
        } else {
          enc_ref_frame_idx u(0)
          ASSERT(enc_ref_frame_idx >= 0 && enc_ref_frame_idx < sched_num_valid_refs, "enc_ref_frame_idx is out of bounds. It must be between 0 and (numValidRefs - 1) = %d.", sched_num_valid_refs - 1)
          index = enc_ref_frame_idx
        }
        sched_active_ref_idx[f][j] = sched_valid_refs[index]
      }
    }
  }

  return 1 // Indicate that we can use an inter frame
}

// Third step: Select the main properties of each frame:
// frame type, refresh flags, reference indices (if inter or show-existing), ...
//
// We do this in a way which guarantees that all frames contribute to
// the output in at least one operating point. That means that each frame
// is either:
// i) shown,
// ii) displayed by a later show-existing frame, or
// iii) never shown, but used as a reference for a later frame
enc_schedule_frame_type(f) {
  spatialLayer = sched_spatial_layer[f]
  temporalLayer = sched_temporal_layer[f]

  if (enc_large_scale_tile && f >= enc_anchor_frame_cnt) {
    // we force using an inter frame, we don't need links as we will copy the anchor frame to LAST_FRAME
    sched_show_existing_frame[f] = 0
    sched_frame_type[f] = INTER_FRAME
    sched_refresh_frame_flags[f] = 0
    for (i = 0; i < INTER_REFS_PER_FRAME; i++) {
      sched_active_ref_idx[f][i] = 0
    }
    for (i = 0; i < NUM_REF_FRAMES; i++) {
      sched_frame_in_ref_slot[f][i] = sched_frame_in_ref_slot[f-1][i]
    }
    return 0
  }

  // Apply rules for special frame types
  if (sched_special_frame[f] == SHOWN_KF) {
    sched_show_existing_frame[f] = 0
    sched_frame_type[f] = KEY_FRAME
    sched_refresh_frame_flags[f] = 0xff
    for (i = 0; i < NUM_REF_FRAMES; i++) {
      sched_frame_in_ref_slot[f][i] = f
    }
    return 0
  } else if (sched_special_frame[f] == FORWARD_KF) {
    sched_show_existing_frame[f] = 0
    sched_frame_type[f] = KEY_FRAME
    // Pick a reference slot which will connect this to the following
    // show-existing frame
    refSlot = ChooseBits(NUM_REF_FRAMES_LOG2)
    sched_num_links[f+1] = 1
    sched_linked_frame[f+1][0] = refSlot
    // We're going to overwrite all ref slots at the next frame (when we
    // show this key frame), so the refresh flags don't matter as long as the
    // one selected bit is set.
    sched_refresh_frame_flags[f] = ChooseBits(NUM_REF_FRAMES) | (1 << refSlot)
    for (i = 0; i < NUM_REF_FRAMES; i++) {
      if (sched_refresh_frame_flags[f] & (1<<i)) {
        sched_frame_in_ref_slot[f][i] = f
      }
    }
    return 0
  } else if (sched_special_frame[f] == SHOW_FWD_KF) {
    sched_show_existing_frame[f] = 1
    refSlot = sched_linked_frame[f][0]
    frameNum = sched_frame_in_ref_slot[f-1][refSlot]
    ASSERT(frameNum == f-1, "Invalid usage of special frame types")

    sched_index_of_existing_frame[f] = refSlot
    sched_frame_type[f] = KEY_FRAME
    sched_showable_frame[frameNum] = 1
    if (enc_enable_order_hint) {
      sched_order_hint[f] = sched_order_hint[frameNum]
    }
    sched_refresh_frame_flags[f] = 0xff
    for (i = 0; i < NUM_REF_FRAMES; i++) {
      sched_frame_in_ref_slot[f][i] = f
    }
    return 0
  }

  // Determine if this frame has been pre-selected to have refresh_flags = 0xff.
  // This affects what choices of frame type are available
  isBarrier = sched_barrier_frame[f]

  if (isBarrier) {
    for (i = 0; i < NUM_REF_FRAMES; i++) {
      frameNum = sched_frame_in_ref_slot[f][i]
      ASSERT(frameNum == -1,
             "Barrier frame must be able to set refresh flags = 0xff")
    }
  }

  // Ensure that this frame is either shown, or linked to a future frame.
  // This way, every frame will be validated in at least one operating point.
  if (!sched_show_frame[f]) {
    ASSERT(!enc_large_scale_tile, "All large scale tile frame should be shown")
    // Select a free reference slot
    uint3 validIdcs[NUM_REF_FRAMES]
    numValidIdcs = 0
    for (i = 0; i < NUM_REF_FRAMES; i++) {
      if (sched_frame_in_ref_slot[f][i] == -1) {
        validIdcs[numValidIdcs] = i
        numValidIdcs += 1
      }
    }
    ASSERT(numValidIdcs >= 1, "Can't find a free reference slot to save frame into")
    refSlot = validIdcs[Choose(0, numValidIdcs-1)]

    if (numValidIdcs == 1) {
      // Special case: If there is only one free slot, we need to be careful,
      // since the next frame might also be unshown and so might need a free
      // ref slot as well.
      // To ensure this works, we are forced to link to the next frame. This
      // is fine - the next frame is guaranteed to lie in the same spatial+temporal
      // layer as us, because we currently only change layers after a shown frame.
      targetFrame = f + 1
    } else {
      // Otherwise, we can select any compatible frame (with spatial+temporal layers
      // at least the same as the current frame).
      // To avoid generating lots of long reservations, we bias towards linking to
      // nearby frames.
      targetFrame = -1
      for (i = f + 1; i < enc_frame_cnt; i++) {
        canUseFrame = enc_compatible_reference(i, f) // Can frame 'i' reference us?
        // See if there's a reference available to attach to this frame
        // Similarly to before, we need to leave one reference free
        // in case frame i-1 ends up being forced to link to frame i.
        // This obviously doesn't apply if i-1 == f.
        if (i == f + 1) {
          canUseFrame = canUseFrame && (sched_num_links[i] <= INTER_REFS_PER_FRAME-1)
        } else {
          canUseFrame = canUseFrame && (sched_num_links[i] <= INTER_REFS_PER_FRAME-2)
        }

        if (canUseFrame && ChooseBit()) {
          targetFrame = i
          break
        }

        // We can't link across a barrier frame (but note that we can link
        // to these frames, which is why we check this at the end of the loop)
        if (sched_barrier_frame[i]) {
          break
        }
      }
      if (targetFrame == -1) {
        // We didn't select a target frame; use the next frame as a fallback
        targetFrame = f + 1
      }
    }

    // At this point, we've selected a ref slot and a target frame, we just need
    // to select a reference index from the target frame
    ASSERT(sched_num_links[targetFrame] <= INTER_REFS_PER_FRAME-1, "Can't find a free reference")
    refIdx = validIdcs[Choose(0, numValidIdcs-1)]

    // Save our choices
    sched_refresh_frame_flags[f] = (1 << refSlot)
    sched_linked_frame[targetFrame][sched_num_links[targetFrame]] = refSlot
    sched_num_links[targetFrame] += 1
    // Reserve the selected reference slot in between the two frames
    for (i = f; i < targetFrame; i++) {
      sched_frame_in_ref_slot[i][refSlot] = f
    }
  }

  sched_get_valid_refs(f)

  // Then select a frame type.
  // Test to see if we can use a show-existing-frame first, then
  // fall back to generating a regular frame
  enc_try_show_existing = 0
  if (sched_show_frame[f] && sched_num_links[f] <= 1) {
    enc_try_show_existing u(0)
  }
  if (enc_try_show_existing) {
    // Try a show-existing frame
    showExisting = 0
    refSlot = -1

    // This is bogus but with some compilation flags, GCC doesn't spot
    // that showExisting is only set if frameNum is set too and so
    // warns about a possible uninitialized access.
    frameNum = -1

    if (sched_num_links[f] == 0 && sched_num_valid_refs >= 1) {
      // We have no linked frames, so we can pick any reference slot to show
      refSlot = sched_valid_refs[Choose(0, sched_num_valid_refs-1)]
      frameNum = sched_frame_in_ref_slot[f-1][refSlot]
      if (sched_show_existing_frame[frameNum]) {
        // This must have been a show of a forward keyframe. We can't re-show
        // this, so bail out
        refSlot = -1
      } else {
        frameType = sched_frame_type[frameNum]
      }
    } else if (sched_num_links[f] == 1) {
      // We have a linked frame, so we must show that frame
      refSlot = sched_linked_frame[f][0]
      frameNum = sched_frame_in_ref_slot[f-1][refSlot]
      frameType = sched_frame_type[frameNum]
    }

    if (refSlot != -1) {
      // We need to be careful about the interaction between show-existing frames
      // and keyframes. Each keyframe can only be shown once, and causes the
      // show-existing frame to have refresh_flags = 0xff. Thus we can only reference
      // a keyframe if:
      // * The keyframe has show_frame == 0,
      // * The keyframe hasn't already been shown (ie, it has sched_showable_frame == 0), and
      // * The current frame is a barrier frame
      //
      // Conversely, we can only reference a non-keyframe if the current frame
      // *isn't* a barrier frame
      if (isBarrier) {
        // Note: We can "promote" an intra-only frame to a key frame, in order to
        // make it showable here. This increases the chance of generating show-existing
        // frames a bit.
        if ((frameType == KEY_FRAME || frameType == INTRA_ONLY_FRAME) &&
            sched_show_frame[frameNum] == 0 && sched_showable_frame[frameNum] == 0) {
          showExisting = 1
          sched_frame_type[frameNum] = KEY_FRAME
        }
      } else {
        showExisting = (frameType != KEY_FRAME)
      }
    }

    if (showExisting) {
      // Set up a show-existing frame
      sched_show_existing_frame[f] = 1
      sched_index_of_existing_frame[f] = refSlot
      sched_showable_frame[frameNum] = 1 // Mark the displayed frame as showable
      sched_frame_type[f] = sched_frame_type[frameNum]

      if (isBarrier) {
        // We're showing a forward keyframe, so need to fill the reference slots with
        // this frame's information
        sched_refresh_frame_flags[f] = 0xff
        for (i = 0; i < NUM_REF_FRAMES; i++) {
          sched_frame_in_ref_slot[f][i] = f
        }
        // To avoid an issue with short-refs signalling, we need to copy the order
        // hint of the frame we're showing
        if (enc_enable_order_hint) {
          sched_order_hint[f] = sched_order_hint[frameNum]
        }
      } else {
        // We're showing a normal frame, so this doesn't affect the reference slots
        sched_refresh_frame_flags[f] = 0
        for (i = 0; i < NUM_REF_FRAMES; i++) {
          sched_frame_in_ref_slot[f][i] = sched_frame_in_ref_slot[f-1][i]
        }
      }

      return 0
    }
  }

  canUseInter = sched_pick_ref_frames(f)
  if (sched_num_links[f] >= 1) {
    ASSERT(canUseInter, "Invalid linked frames")
    useInter = 1
  } else if (canUseInter) {
    enc_frame_is_inter u(0)
    useInter = enc_frame_is_inter
  } else {
    useInter = 0
  }

  if (useInter) {
    if (isBarrier) {
      enc_inter_not_s_frame u(0)
      sched_frame_type[f] = enc_inter_not_s_frame ? INTER_FRAME : S_FRAME
    } else {
      sched_frame_type[f] = INTER_FRAME
    }
  } else {
    if (isBarrier) {
      // We can't use INTRA_ONLY_FRAME since intra-only frames aren't allowed to
      // have refresh_flags == 0xff. So this must be a KEY_FRAME.
      sched_frame_type[f] = KEY_FRAME
    } else {
      // If this frame isn't shown, we can make it into an unshown key frame.
      // But if it is shown, we *must* use intra-only
      if (sched_show_frame[f]) {
        sched_frame_type[f] = INTRA_ONLY_FRAME
      } else {
        sched_frame_type[f] = ChooseBit() ? KEY_FRAME : INTRA_ONLY_FRAME
      }
    }
  }

  // Shown frames are automatically showable, unless they are keyframes.
  // This does not factor into anything yet, but may be useful in future
  if (sched_show_frame[f] && sched_frame_type[f] != KEY_FRAME) {
    sched_showable_frame[f] = 1
  }

  // Lastly, set up refresh_frame_flags.
  if (isBarrier) {
    sched_refresh_frame_flags[f] = 0xFF
    for (i = 0; i < NUM_REF_FRAMES; i++) {
      sched_frame_in_ref_slot[f][i] = f
    }
  } else {
    // If there are any reference slots which are available after this frame
    // is finished, they can be added to refresh_frame_flags
    for (i = 0; i < NUM_REF_FRAMES; i++) {
      if (sched_frame_in_ref_slot[f][i] == -1) {

        // Don't allow intra-only frames to have flags 0xff
        newFlags = sched_refresh_frame_flags[f] | (1<<i)
        useSlot = 0
        if ((sched_frame_type[f] != INTRA_ONLY_FRAME) || newFlags != 0xFF) {
          enc_refresh_extra_slot u(0)
          useSlot = enc_refresh_extra_slot
        }

        if (useSlot) {
          sched_refresh_frame_flags[f] = newFlags
          sched_frame_in_ref_slot[f][i] = f
        } else {
          sched_frame_in_ref_slot[f][i] = sched_frame_in_ref_slot[f-1][i]
        }
      }
    }
  }
}

// Initialize sched_last_frame_num[].
// Note: sched_last_frame_num[f] describes the (one or two) possible frames
// which could have been decoded immediately before frame f.
enc_calculate_frame_sequences() {
  // Before the first frame, there are obviously no previous frames
  for (i = 0; i < enc_num_operating_points; i++) {
    for (j = 0; j < 2; j++) {
      sched_last_frame_num[0][i][j] = -1
    }
  }

  for (f = 0; f < enc_frame_cnt-1; f++) {
    // Copy the "last frame num" from the previous frame, so that we keep
    // the correct values for any operating points which don't decode this frame
    for (i = 0; i < enc_num_operating_points; i++) {
      for (j = 0; j < 2; j++) {
        sched_last_frame_num[f+1][i][j] = sched_last_frame_num[f][i][j]
      }
    }

    if (sched_show_existing_frame[f]) {
      // Show-existing frames don't update the previous frame ID in normal
      // decoding. But, if this is showing a forward keyframe, then we need
      // to fill in the second "last frame" entry, to account for random-access
      // decoding.
      if (sched_frame_type[f] == KEY_FRAME) {
        refSlot = sched_index_of_existing_frame[f]
        refFrameNum = sched_frame_in_ref_slot[f-1][refSlot]
        for (i = 0; i < enc_num_operating_points; i++) {
          if (enc_frame_in_operating_point(f, i)) {
            // Leave sched_last_frame_num[f][i][0] as-is
            sched_last_frame_num[f+1][i][1] = refFrameNum
          }
        }
      }
    } else {
      for (i = 0; i < enc_num_operating_points; i++) {
        if (enc_frame_in_operating_point(f, i)) {
          sched_last_frame_num[f+1][i][0] = f
          sched_last_frame_num[f+1][i][1] = -1 // Clear forward keyframe reference
        }
      }
    }
  }
}

// Fourth step: Once we've selected all our frame types and references,
// select frame IDs which are compatible with this structure.
enc_schedule_frame_ids() {
  // Find the maximum distance between any frame and its references
  maxRefDistance = 0
  for (f = 0; f < enc_frame_cnt; f++) {
    if (sched_show_existing_frame[f]) {
      shownSlot = sched_index_of_existing_frame[f]
      shownFrameNum = sched_frame_in_ref_slot[f-1][shownSlot]
      maxRefDistance = Max(maxRefDistance, f - shownFrameNum)
    } else if (sched_frame_type[f] == INTER_FRAME || sched_frame_type[f] == S_FRAME) {
      for (i = 0; i < INTER_REFS_PER_FRAME; i++) {
        refSlot = sched_active_ref_idx[f][i]
        refFrameNum = sched_frame_in_ref_slot[f-1][refSlot]
        // Special case: If this reference points to a show-existing frame, then
        // it must be showing a forward keyframe, since that's the only way a
        // show-existing frame can get into the reference buffers.
        // If this happens, we need to "look through" the show-existing frame
        // and calculate the distance to the frame it's showing.
        // This is so that we maintain valid frame IDs after showing that forward
        // keyframe.
        if (sched_show_existing_frame[refFrameNum]) {
          shownSlot = sched_index_of_existing_frame[refFrameNum]
          shownFrameNum = sched_frame_in_ref_slot[refFrameNum-1][shownSlot]
          distance = f - shownFrameNum
        } else {
          distance = f - refFrameNum
        }
        maxRefDistance = Max(maxRefDistance, distance)
      }
    }
  }

  enc_calculate_frame_sequences()

  // Find the maximum distance between any two "consecutive" frames
  // in the same operating point.
  // Note: To match the way frame IDs are handled in read_uncompressed_header(),
  // we "skip over" show-existing frames.
  maxOppointDistance = 0
  for (i = 0; i < enc_num_operating_points; i++) {
    for (f = 0; f < enc_frame_cnt; f++) {
      if (!sched_show_existing_frame[f]) {
        lastFrameNormal = sched_last_frame_num[f][i][0]
        lastFrameFwdKF = sched_last_frame_num[f][i][1]
        if (lastFrameNormal != -1) {
          if (lastFrameFwdKF != -1) {
            // This frame has two possible "last frames" -
            // one following the normal sequence of frames, and one
            // from a random-access point.
            lastFrameNum = Min(lastFrameNormal, lastFrameFwdKF)
          } else {
            // This frame only has one possible "last frame"
            lastFrameNum = lastFrameNormal
          }
          distance = f - lastFrameNum
          maxOppointDistance = Max(maxOppointDistance, distance)
        }
      }
    }
  }

  // In order to ensure that it's possible to consistently assign frame IDs,
  // it's enough to ensure that the assignment sched_frame_id[f] = f for all f
  // would work.
  // Working through what the frame ID lengths and delta frame ID lengths need to be
  // in this case, we get the following results:
  minFrameIdLength = 1 + CeilLog2(maxOppointDistance + 1)
  minDeltaFrameIdLength = CeilLog2(maxRefDistance)
  delta_frame_id_length u(0)
  frame_id_length u(0)

  CHECK (2 <= delta_frame_id_length && delta_frame_id_length <= 15,
         "The delta frame ID length must be in range [2, 15] (saw: %d).",
         delta_frame_id_length)
  CHECK (delta_frame_id_length >= minDeltaFrameIdLength,
         "Stream has a maximum distance between ref frames of %d, implying a minimum delta_frame_id_length of %d, but we saw %d.",
         maxRefDistance, minDeltaFrameIdLength, delta_frame_id_length)

  delta_frame_id_length = Max(minDeltaFrameIdLength, Min(15, delta_frame_id_length))

  CHECK (frame_id_length >= delta_frame_id_length + 1,
         "With a delta frame ID length of %d, the frame ID length must be at least %d. (Saw: %d)",
         delta_frame_id_length, delta_frame_id_length + 1,
         frame_id_length)

  // For convenience, we calculate the frame IDs *without* applying wrapping,
  // and apply the wrapping as we write into sched_frame_id.
  // This helps simplify the following logic.
  ASSERT(ENC_MAX_FRAMES < (1<<15), "Need to widen unwrappedFrameId for this many frames")
  uint32 unwrappedFrameId[ENC_MAX_FRAMES]

  enc_first_frame_id u(0)
  unwrappedFrameId[0] = enc_first_frame_id

  CHECK ((enc_first_frame_id >> frame_id_length) == 0,
         "Frame ID length is %d bits, but enc_first_frame_id is %d, not in [0, %d].",
         frame_id_length, enc_first_frame_id, (1 << frame_id_length) - 1)

  sched_frame_id[0] = unwrappedFrameId[0]

  for (f = 1; f < enc_frame_cnt; f++) {
    // Work out the decoder state before frame 'f', for *every* operating point
    // (including ones where frame f isn't actually decoded). This is required
    // so that we can guarantee that we're able to select frame IDs for future frames.
    spatialLayer = sched_spatial_layer[f]
    temporalLayer = sched_temporal_layer[f]
    operatingPointMask = enc_layers_to_operating_points[spatialLayer][temporalLayer]

    latestPrevFrameId = unwrappedFrameId[f-1]
    earliestPrevFrameNum = f-1
    earliestPrevFrameId = latestPrevFrameId
    for (i = 0; i < enc_num_operating_points; i++) {
      for (j = 0; j < 2; j++) {
        prevFrameNum = sched_last_frame_num[f][i][j]
        if (prevFrameNum >= 0) {
          prevFrameId = unwrappedFrameId[prevFrameNum]
          earliestPrevFrameNum = Min(earliestPrevFrameNum, prevFrameNum)
          earliestPrevFrameId = Min(earliestPrevFrameId, prevFrameId)
        }
      }
    }

    if (sched_show_existing_frame[f]) {
      // Show-existing frames don't come with their own frame IDs.
      // To maintain the invariant that unwrappedFrameId[] is nondecreasing,
      // we act as if a show-existing frame has the same frame ID
      // as the immediately preceding non-show-existing frame
      unwrappedFrameId[f] = latestPrevFrameId + 1
    } else {
      minFrameId = latestPrevFrameId + 1

      // When working out the maximum frame ID, we need to make sure
      // that the difference in frame IDs between two successive
      // frames at a given operating point can always be encoded in
      // frame_id_length bits.
      //
      // It's possible that we'll decode at an operating point that
      // causes us to drop some frames. We've already calculated
      // maxOppointDistance, which is one more than the maximum number
      // of frames dropped between two decoded frames. We also have
      // earliestPrevFrameNum, which is the index of the earliest
      // frame which can come immediately before this one (or the
      // following non-skipped one) at some operating point.
      //
      // This means that earliestPrevFrameNum + maxOppointDistance is
      // the worst case "next frame" at some operating point. This is
      // maxRemainingDistance frames ahead of us:

      maxRemainingDistance = earliestPrevFrameNum + maxOppointDistance - f
      ASSERT (maxRemainingDistance >= 0,
              "maxOppointDistance is calculated wrong")

      // It would be a problem if we picked such a high frame ID that,
      // even incrementing by one each frame, maxRemainingDistance
      // ended up with a delta from earliestPrevFrameId that couldn't
      // be encoded as a signed number in frame_id_length bits. This
      // calculation of maxFrameId gives the upper bound to stop that
      // happening.

      maxFrameId = (earliestPrevFrameId +
                    (1 << (frame_id_length - 1)) - 1 - maxRemainingDistance)
      CHECK (minFrameId <= maxFrameId,
             "Oppoint skipping implies a max frame ID for frame %d of %d, but the previous frame had ID %d.",
             f, maxFrameId, latestPrevFrameId)

      // Another thing that might cause a problem is if future frames
      // are inter frames which reference some frames from before the
      // current frame.
      //
      // The maximum (backwards) reference distance is given by
      // maxRefDistance, so frame f' can only reference frames
      // starting at f' - maxRefDistance. For any such reference, we
      // need to be able to encode the difference in frame IDs with
      // delta_frame_id_length bits. Since frame IDs are increasing,
      // the most difficult case is between frame f' and frame
      // max(f'-maxRefDistance, 0).
      //
      // This constraint means that the ID of frame f' must be at most
      // the ID of that earlier frame. Of course, frame f' is in the
      // future. If it's i frames from now, we must have a maximum ID
      // smaller by at least i.

      for (ff = f; ff - f < maxRefDistance; ff++) {
        oldNum = Max(ff - maxRefDistance, 0)
        if (oldNum < f) {
          oldId = unwrappedFrameId[oldNum]

          // maxFFId is the maximum ID for frame ff (note the offset by 1)
          maxFFId = oldId + (1 << delta_frame_id_length)

          // To get the bound on ID for f, we must subtract (ff - f)
          // which corresponds to a delta of +1 for each frame from f to
          // ff.
          bound = maxFFId - (ff - f)

          // Spot when this bound means we can't find a valid ID
          CHECK (minFrameId <= bound,
                 "Failed to find valid frame ID for frame %d. Prev frame had id %d, but frame %d might reference frame %d with id %d; delta_frame_id_length = %d gives a maximum id for frame %d of %d.",
                 f, latestPrevFrameId, ff, oldNum, oldId, delta_frame_id_length,
                 f, bound)
          maxFrameId = Min(maxFrameId, bound)
        }
      }

      // Don't explode if maxFrameId < minFrameId (we'll already have
      // failed a check above).
      maxFrameId = Max(maxFrameId, minFrameId)

      enc_subsequent_frame_id u(0)
      CHECK (minFrameId <= enc_subsequent_frame_id &&
             enc_subsequent_frame_id <= maxFrameId,
             "Chosen frame ID, %d, doesn't lie in the allowed range: [%d, %d].",
             enc_subsequent_frame_id, minFrameId, maxFrameId)

      unwrappedFrameId[f] = enc_subsequent_frame_id
    }

    sched_frame_id[f] = wrap_uint(unwrappedFrameId[f], frame_id_length)
  }
}

// Alternative implementation for memory performance stress streams.
// For these streams, we disable optional features and use a fixed
// pattern of frames, so things are comparatively simple.
enc_schedule_memory_stress_stream() {
  // First of all, all frames are shown,
  // and we don't use scalability or show-existing frames
  for (f = 0; f < enc_frame_cnt; f++) {
    sched_temporal_unit[f] = f
    sched_spatial_layer[f] = 0
    sched_temporal_layer[f] = 0
    sched_show_existing_frame[f] = 0
    sched_show_frame[f] = 1
  }

  // Make the first 7 frames be intra frames,
  // arranged so that after the 7th frame the first
  // 7 reference slots all hold different frames.
  sched_frame_type[0] = KEY_FRAME
  sched_refresh_frame_flags[0] = (1 << f)
  for (f = 1; f < 7; f++) {
    sched_frame_type[f] = INTRA_ONLY_FRAME
    sched_refresh_frame_flags[f] = (1 << f)
  }

  // These intra frames are followed by a few inter frames
  // which exercise the memory subsystem.
  // These frames are stored back into the reference slots
  // just to add a bit of extra memory pressure
  for (f = 7; f < enc_frame_cnt; f++) {
    sched_frame_type[f] = INTER_FRAME
    // Map this frame's references to the first
    // 7 reference slots
    for (i = 0; i < INTER_REFS_PER_FRAME; i++) {
      sched_active_ref_idx[f][i] = i
    }
    sched_refresh_frame_flags[f] = (1 << (f%7))
  }
}

// Alternative implementations for special scalability streams
enc_setup_special_frame(tu, spatial_layer, temporal_layer, frame_type, refSlot, storeSlot) {
  // all frames shown
  sched_show_existing_frame[enc_frame_cnt] = 0
  sched_show_frame[enc_frame_cnt] = 1

  sched_temporal_unit[enc_frame_cnt] = tu
  aaa = enc_frame_cnt
  :C printf("sched tu %d at frame %d\n", tu, aaa)
  sched_spatial_layer[enc_frame_cnt] = spatial_layer
  sched_temporal_layer[enc_frame_cnt] = temporal_layer
  sched_frame_type[enc_frame_cnt] = frame_type

  if (frame_type == KEY_FRAME) {
    sched_barrier_frame[enc_frame_cnt] = 1
    sched_refresh_frame_flags[enc_frame_cnt] = 0xff // shown key frame
  } else if (frame_type == INTER_FRAME) {
    sched_barrier_frame[enc_frame_cnt] = 0
    for (i = 0; i < INTER_REFS_PER_FRAME; i++) {
      sched_active_ref_idx[enc_frame_cnt][i] = refSlot
    }
    sched_refresh_frame_flags[enc_frame_cnt] = (storeSlot >= 0) ? (1<<storeSlot) : 0
  } else {
    ASSERT(0, "Unexpceted frame_type %d in enc_setup_special_frame", frame_type)
  }

  enc_frame_cnt++
}

enc_schedule_scalability_l3t2_key() {
  // specify operating points
  enc_num_operating_points = 5

  // All S’
  enc_spatial_layers_used[0] = 0x5
  enc_temporal_layers_used[0] = 0x03
  // Half S’ (ignore S’1)
  enc_spatial_layers_used[1] = 0x5
  enc_temporal_layers_used[1] = 0x01
  // All S
  enc_spatial_layers_used[2] = 0x3
  enc_temporal_layers_used[2] = 0x03
  // Half S (ignore S1)
  enc_spatial_layers_used[3] = 0x3
  enc_temporal_layers_used[3] = 0x01
  // Base layer only.
  enc_spatial_layers_used[4] = 0x1
  enc_temporal_layers_used[4] = 0x01

  enc_finalise_operating_points()

  // storage plan
  //   I0   - slot 0
  //   S'0  - slot 1
  //
  //   S1   - not stored
  //   S'1  - not stored
  //
  //   S0   - slot 0 (overwriting I0 or S0)
  //   S'0  - slot 1 (overwriting S'0 )

  // schedule frames
  enc_frame_cnt = 0
  tu = -1
  for (globalRepeat=0; globalRepeat<2; globalRepeat++) {
    // Temporal Unit 0
    tu++
    //   I0
    enc_setup_special_frame(tu, /*spatial*/ 0, /*temporal*/ 0,
                            /*ftype*/ KEY_FRAME, /*refSlot*/ -1, /*storeSlot*/ -1)
    //   S'0
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 1)

    // Temporal Unit 1
    tu++
    //   S1
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
    //   S'1
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)

    // Temporal Unit 2
    tu++
    //   S0
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
    //   S'0
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)


    for (localRepeat=0; localRepeat<3; localRepeat++) {
      // Temporal Unit 3...
      tu++
      //   S1
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
      //   S'1
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)

      // Temporal Unit 4..
      tu++
      //   S0
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 0,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
      //   S'0
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)
    }
  }
}

enc_schedule_scalability_l3t3_key() {
  // specify operating points
  enc_num_operating_points = 7

  // S’2, S’1 and S’0
  enc_spatial_layers_used[0] = 0x5
  enc_temporal_layers_used[0] = 0x07
  // S’1 and S’0
  enc_spatial_layers_used[1] = 0x5
  enc_temporal_layers_used[1] = 0x03
  // S’0
  enc_spatial_layers_used[2] = 0x5
  enc_temporal_layers_used[2] = 0x01
  // S2, S1 and S0
  enc_spatial_layers_used[3] = 0x3
  enc_temporal_layers_used[3] = 0x07
  // S1 and S0
  enc_spatial_layers_used[4] = 0x3
  enc_temporal_layers_used[4] = 0x03
  // S0
  enc_spatial_layers_used[5] = 0x3
  enc_temporal_layers_used[5] = 0x01
  // Base layer only.
  enc_spatial_layers_used[6] = 0x1
  enc_temporal_layers_used[6] = 0x01

  enc_finalise_operating_points()

  // storage plan
  //   I0   - slot 0
  //   S'0  - slot 1
  //
  //   S2   - not stored
  //   S'2  - not stored
  //
  //   S1   - slot 2 (overwriting S1)
  //   S'1  - slot 3 (overwriting S'1)
  //
  //   S0   - slot 0 (overwriting I0)
  //   S'0  - slot 1 (overwriting S'0)

  // schedule frames
  enc_frame_cnt = 0
  tu = -1
  for (globalRepeat=0; globalRepeat<2; globalRepeat++) {
    // Temporal Unit 0
    tu++
    //   I0
    enc_setup_special_frame(tu, /*spatial*/ 0, /*temporal*/ 0,
                            /*ftype*/ KEY_FRAME, /*refSlot*/ -1, /*storeSlot*/ -1)
    //   S'0
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 1)

    // Temporal Unit 1
    tu++
    //   S2
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
    //   S'2
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)

    // Temporal Unit 2
    tu++
    //   S1
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 2)
    //   S'1
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 3)

    // Temporal Unit 3
    tu++
    //   S2
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ -1)
    //   S'2
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 3, /*storeSlot*/ -1)

    // Temporal Unit 4
    tu++
    //   S0
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
    //   S'0
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)


    for (localRepeat=0; localRepeat<3; localRepeat++) {
      // Temporal Unit 5...
      tu++
      //   S2
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
      //   S'2
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)

    // Temporal Unit 6..
      tu++
      //   S1
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 2)
      //   S'1
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 3)

      // Temporal Unit 7..
      tu++
      //   S2
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ -1)
      //   S'2
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 3, /*storeSlot*/ -1)

      // Temporal Unit 8..
      tu++
      //   S0
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 0,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
      //   S'0
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)
    }
  }
}

enc_schedule_scalability_l4t5_key() {
  // specify operating points
  enc_num_operating_points = 7

  // All S’’
  enc_spatial_layers_used[0] = 0xd
  enc_temporal_layers_used[0] = 0x19
  // Half S’’ (ignore S’’4)
  enc_spatial_layers_used[1] = 0xd
  enc_temporal_layers_used[1] = 0x09
  // All S’
  enc_spatial_layers_used[2] = 0x5
  enc_temporal_layers_used[2] = 0x07
  // Half S’ (ignore S’2)
  enc_spatial_layers_used[3] = 0x5
  enc_temporal_layers_used[3] = 0x03
  // All S
  enc_spatial_layers_used[4] = 0x3
  enc_temporal_layers_used[4] = 0x07
  // Half S (ignore S2)
  enc_spatial_layers_used[5] = 0x3
  enc_temporal_layers_used[5] = 0x03
  // Base layer only.
  enc_spatial_layers_used[6] = 0x1
  enc_temporal_layers_used[6] = 0x01

  enc_finalise_operating_points()

  // storage plan
  //   I0   - slot 0
  //   S'0  - slot 1
  //   S''0 - slot 2
  //
  //   S2   - not stored
  //   S'2  - not stored
  //   S''4 - not stored
  //
  //   S1   - slot 0 (overwriting I0 or S1)
  //   S'1  - slot 1 (overwriting S'0 or S'1)
  //   S''3 - slot 2 (overwiting S''0 or S''3)


  // schedule frames
  enc_frame_cnt = 0
  tu = -1
  for (globalRepeat=0; globalRepeat<2; globalRepeat++) {
    // Temporal Unit 0
    tu++
    //   I0
    enc_setup_special_frame(tu, /*spatial*/ 0, /*temporal*/ 0,
                            /*ftype*/ KEY_FRAME, /*refSlot*/ -1, /*storeSlot*/ -1)
    //   S'0
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 1)
    //   S''0
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 2)

    // Temporal Unit 1
    tu++
    //   S2
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
    //   S'2
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)
    //   S''4
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 4,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ -1)

    // Temporal Unit 2
    tu++
    //   S1
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
    //   S'1
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)
    //   S''3
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 3,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 2)

    for (localRepeat=0; localRepeat<3; localRepeat++) {
      // Temporal Unit 3..
      tu++
      //   S2
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
      //   S'2
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)
      //   S''4
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 4,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ -1)

      // Temporal Unit 4..
      tu++
      //   S1
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
      //   S'1
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)
      //   S''3
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 3,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 2)
    }
  }
}

enc_schedule_scalability_l4t7_key() {
  // specify operating points
  enc_num_operating_points = 10

  // S’’6, S’’5, S’’4
  enc_spatial_layers_used[0] = 0xd
  enc_temporal_layers_used[0] = 0x71
  // S’’5, S’’4
  enc_spatial_layers_used[1] = 0xd
  enc_temporal_layers_used[1] = 0x31
  // S’’4
  enc_spatial_layers_used[2] = 0xd
  enc_temporal_layers_used[2] = 0x11
  // S’3, S’2, S'1
  enc_spatial_layers_used[3] = 0x5
  enc_temporal_layers_used[3] = 0x0f
  // S’2, S'1
  enc_spatial_layers_used[4] = 0x5
  enc_temporal_layers_used[4] = 0x07
  // S'1
  enc_spatial_layers_used[5] = 0x5
  enc_temporal_layers_used[5] = 0x03
  // S3, S2, S1
  enc_spatial_layers_used[6] = 0x3
  enc_temporal_layers_used[6] = 0x0f
  // S2, S1
  enc_spatial_layers_used[7] = 0x3
  enc_temporal_layers_used[7] = 0x07
  // S1
  enc_spatial_layers_used[8] = 0x3
  enc_temporal_layers_used[8] = 0x03
  // Base layer only.
  enc_spatial_layers_used[9] = 0x1
  enc_temporal_layers_used[9] = 0x01

  enc_finalise_operating_points()

  // storage plan
  //   I0   - slot 0
  //   S'0  - slot 1
  //   S''0 - slot 2
  //
  //   S3   - not stored
  //   S'3  - not stored
  //   S''6 - not stored
  //
  //   S2    - slot 3 (overwritng S2)
  //   S'2   - slot 4 (overwritng S'2)
  //   S''5  - slot 5 (overwritng S''5)
  //
  //   S1    - slot 0 (overwriting I0 and S1)
  //   S'1   - slot 1 (overwriting S'0 and S'1)
  //   S''4  - slot 2 (overwriting S''0 and S''4)

  // schedule frames
  enc_frame_cnt = 0
  tu = -1
  for (globalRepeat=0; globalRepeat<2; globalRepeat++) {
    // Temporal Unit 0
    tu++
    //   I0
    enc_setup_special_frame(tu, /*spatial*/ 0, /*temporal*/ 0,
                            /*ftype*/ KEY_FRAME, /*refSlot*/ -1, /*storeSlot*/ -1)
    //   S'0
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 1)
    //   S''0
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 2)

    // Temporal Unit 1
    tu++
    //   S3
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 3,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
    //   S'3
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 3,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)
    //   S''6
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 6,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ -1)

    // Temporal Unit 2
    tu++
    //   S2
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 3)
    //   S'2
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 4)
    //   S''5
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 5,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 5)

    // Temporal Unit 3
    tu++
    //   S3
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 3,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 3, /*storeSlot*/ -1)
    //   S'3
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 3,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 4, /*storeSlot*/ -1)
    //   S''6
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 6,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 5, /*storeSlot*/ -1)

    // Temporal Unit 4
    tu++
    //   S1
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
    //   S'1
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)
    //   S''4
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 4,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 2)

    for (localRepeat=0; localRepeat<3; localRepeat++) {
      // Temporal Unit 5..
      tu++
      //   S3
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 3,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
      //   S'3
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 3,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)
      //   S''6
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 6,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ -1)

      // Temporal Unit 6..
      tu++
      //   S2
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 3)
      //   S'2
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 4)
      //   S''5
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 5,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 5)

      // Temporal Unit 7..
      tu++
      //   S3
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 3,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 3, /*storeSlot*/ -1)
      //   S'3
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 3,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 4, /*storeSlot*/ -1)
      //   S''6
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 6,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 5, /*storeSlot*/ -1)

      // Temporal Unit 8..
      tu++
      //   S1
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
      //   S'1
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)
      //   S''4
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 4,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 2)
    }
  }
}

enc_schedule_scalability_l3t2_key_shift() {
  // specify operating points
  enc_num_operating_points = 5

  // All S’
  enc_spatial_layers_used[0] = 0x5
  enc_temporal_layers_used[0] = 0x03
  // Half S’ (ignore S’1)
  enc_spatial_layers_used[1] = 0x5
  enc_temporal_layers_used[1] = 0x01
  // All S
  enc_spatial_layers_used[2] = 0x3
  enc_temporal_layers_used[2] = 0x03
  // Half S (ignore S1)
  enc_spatial_layers_used[3] = 0x3
  enc_temporal_layers_used[3] = 0x01
  // Base layer only.
  enc_spatial_layers_used[4] = 0x1
  enc_temporal_layers_used[4] = 0x01

  enc_finalise_operating_points()

  // storage plan
  //   I0   - slot 0
  //   S'0  - slot 1
  //
  //   S0   - slot 0 (overwriting I0 or S0)
  //   S'1  - not stored
  //
  //   S1   - not stored
  //   S'0  - slot 1 (overwriting S'0)

  // schedule frames
  enc_frame_cnt = 0
  tu = -1
  for (globalRepeat=0; globalRepeat<2; globalRepeat++) {
    // Temporal Unit 0
    tu++
    //   I0
    enc_setup_special_frame(tu, /*spatial*/ 0, /*temporal*/ 0,
                            /*ftype*/ KEY_FRAME, /*refSlot*/ -1, /*storeSlot*/ -1)
    //   S'0
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 1)

    // Temporal Unit 1
    tu++
    //   S0
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
    //   S'1
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)

    for (localRepeat=0; localRepeat<3; localRepeat++) {
      // Temporal Unit 2...
      tu++
      //   S1
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
      //   S'0
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)

      // Temporal Unit 3..
      tu++
      //   S0
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 0,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
      //   S'1
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)
    }
  }
}

enc_schedule_scalability_l3t3_key_shift() {
  // specify operating points
  enc_num_operating_points = 7

  // S’2, S’1 and S’0
  enc_spatial_layers_used[0] = 0x5
  enc_temporal_layers_used[0] = 0x07
  // S’1 and S’0
  enc_spatial_layers_used[1] = 0x5
  enc_temporal_layers_used[1] = 0x03
  // S’0
  enc_spatial_layers_used[2] = 0x5
  enc_temporal_layers_used[2] = 0x01
  // S2, S1 and S0
  enc_spatial_layers_used[3] = 0x3
  enc_temporal_layers_used[3] = 0x07
  // S1 and S0
  enc_spatial_layers_used[4] = 0x3
  enc_temporal_layers_used[4] = 0x03
  // S0
  enc_spatial_layers_used[5] = 0x3
  enc_temporal_layers_used[5] = 0x01
  // Base layer only.
  enc_spatial_layers_used[6] = 0x1
  enc_temporal_layers_used[6] = 0x01

  enc_finalise_operating_points()

  // storage plan
  //   I0   - slot 0
  //   S'0  - slot 1
  //
  //   S2   - not stored
  //   S'2  - not stored
  //
  //   S0   - slot 0 (overwriting I0)
  //   S'1  - slot 3 (overwriting S'1)
  //
  //   S1   - slot 2 (overwriting S1)
  //   S'0  - slot 1 (overwriting S'0)


  // schedule frames
  enc_frame_cnt = 0
  tu = -1
  for (globalRepeat=0; globalRepeat<2; globalRepeat++) {
    // Temporal Unit 0
    tu++
    //   I0
    enc_setup_special_frame(tu, /*spatial*/ 0, /*temporal*/ 0,
                            /*ftype*/ KEY_FRAME, /*refSlot*/ -1, /*storeSlot*/ -1)
    //   S'0
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 1)

    // Temporal Unit 1
    tu++
    //   S2
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
    //   S'2
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)

    // Temporal Unit 2
    tu++
    //   S0
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
    //   S'1
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 3)


    for (localRepeat=0; localRepeat<3; localRepeat++) {
      // Temporal Unit 3..
      tu++
      //   S2
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
      //   S'2
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 3, /*storeSlot*/ -1)

      // Temporal Unit 4..
      tu++
      //   S1
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 2)
      //   S'0
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)

      // Temporal Unit 5..
      tu++
      //   S2
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ -1)
      //   S'2
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)


      // Temporal Unit 6..
      tu++
      //   S0
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 0,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
      //   S'1
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 3)
    }
  }
}


enc_schedule_scalability_l4t5_key_shift() {
  // specify operating points
  enc_num_operating_points = 7

  // All S’’
  enc_spatial_layers_used[0] = 0xd
  enc_temporal_layers_used[0] = 0x19
  // Half S’’ (ignore S’’4)
  enc_spatial_layers_used[1] = 0xd
  enc_temporal_layers_used[1] = 0x09
  // All S’
  enc_spatial_layers_used[2] = 0x5
  enc_temporal_layers_used[2] = 0x07
  // Half S’ (ignore S’2)
  enc_spatial_layers_used[3] = 0x5
  enc_temporal_layers_used[3] = 0x03
  // All S
  enc_spatial_layers_used[4] = 0x3
  enc_temporal_layers_used[4] = 0x07
  // Half S (ignore S2)
  enc_spatial_layers_used[5] = 0x3
  enc_temporal_layers_used[5] = 0x03
  // Base layer only.
  enc_spatial_layers_used[6] = 0x1
  enc_temporal_layers_used[6] = 0x01

  enc_finalise_operating_points()

  // storage plan
  //   I0   - slot 0
  //   S'0  - slot 1
  //   S''0 - slot 2
  //
  //   S1   - slot 0 (overwriting I0 or S1)
  //   S'1  - slot 1 (overwriting S'0 or S'1)
  //   S''4 - not stored
  //
  //   S2   - not stored
  //   S'2  - not stored
  //   S''3 - slot 2 (overwiting S''0 or S''3)

  // schedule frames
  enc_frame_cnt = 0
  tu = -1
  for (globalRepeat=0; globalRepeat<2; globalRepeat++) {
    // Temporal Unit 0
    tu++
    //   I0
    enc_setup_special_frame(tu, /*spatial*/ 0, /*temporal*/ 0,
                            /*ftype*/ KEY_FRAME, /*refSlot*/ -1, /*storeSlot*/ -1)
    //   S'0
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 1)
    //   S''0
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 2)

    // Temporal Unit 1
    tu++
    //   S1
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
    //   S'1
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)
    //   S''4
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 4,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ -1)

    // Temporal Unit 2
    tu++
    //   S2
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
    //   S'2
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)
    //   S''3
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 3,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 2)

    for (localRepeat=0; localRepeat<3; localRepeat++) {
      // Temporal Unit 3..
      tu++
      //   S1
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
      //   S'1
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)
      //   S''4
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 4,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ -1)

      // Temporal Unit 4..
      tu++
      //   S2
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
      //   S'2
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)
      //   S''3
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 3,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 2)
    }
  }
}

enc_schedule_scalability_l4t7_key_shift() {
  // specify operating points
  enc_num_operating_points = 10

  // S’’6, S’’5, S’’4
  enc_spatial_layers_used[0] = 0xd
  enc_temporal_layers_used[0] = 0x71
  // S’’5, S’’4
  enc_spatial_layers_used[1] = 0xd
  enc_temporal_layers_used[1] = 0x31
  // S’’4
  enc_spatial_layers_used[2] = 0xd
  enc_temporal_layers_used[2] = 0x11
  // S’3, S’2, S'1
  enc_spatial_layers_used[3] = 0x5
  enc_temporal_layers_used[3] = 0x0f
  // S’2, S'1
  enc_spatial_layers_used[4] = 0x5
  enc_temporal_layers_used[4] = 0x07
  // S'1
  enc_spatial_layers_used[5] = 0x5
  enc_temporal_layers_used[5] = 0x03
  // S3, S2, S1
  enc_spatial_layers_used[6] = 0x3
  enc_temporal_layers_used[6] = 0x0f
  // S2, S1
  enc_spatial_layers_used[7] = 0x3
  enc_temporal_layers_used[7] = 0x07
  // S1
  enc_spatial_layers_used[8] = 0x3
  enc_temporal_layers_used[8] = 0x03
  // Base layer only.
  enc_spatial_layers_used[9] = 0x1
  enc_temporal_layers_used[9] = 0x01

  enc_finalise_operating_points()

  // storage plan
  //   I0   - slot 0
  //   S'0  - slot 1
  //   S''0 - slot 2
  //
  //   S1    - slot 0 (overwriting I0 and S1)
  //   S'3  - not stored
  //   S''6 - not stored
  //
  //   S3   - not stored
  //   S'1   - slot 1 (overwriting S'0 and S'1)
  //   S''5  - slot 5 (overwritng S''5)

  //   S2    - slot 3 (overwritng S2)
  //   S'2   - slot 4 (overwritng S'2)
  //   S''4  - slot 2 (overwriting S''0 and S''4)

  // schedule frames
  enc_frame_cnt = 0
  tu = -1
  for (globalRepeat=0; globalRepeat<2; globalRepeat++) {
    // Temporal Unit 0
    tu++
    //   I0
    enc_setup_special_frame(tu, /*spatial*/ 0, /*temporal*/ 0,
                            /*ftype*/ KEY_FRAME, /*refSlot*/ -1, /*storeSlot*/ -1)
    //   S'0
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 1)
    //   S''0
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 0,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 2)

    // Temporal Unit 1
    tu++
    //   S1
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
    //   S'3
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 3,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)
    //   S''6
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 6,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ -1)

    // Temporal Unit 2
    tu++
    //   S3
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 3,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
    //   S'1
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)
    //   S''5
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 5,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 5)

    // Temporal Unit 3
    tu++
    //   S2
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 3)
    //   S'3
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 3,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)
    //   S''6
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 6,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 5, /*storeSlot*/ -1)

    // Temporal Unit 4
    tu++
    //   S3
    enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 3,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 3, /*storeSlot*/ -1)
    //   S'2
    enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 4)
    //   S''4
    enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 4,
                            /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 2)

    for (localRepeat=0; localRepeat<3; localRepeat++) {
      // Temporal Unit 5..
      tu++
      //   S1
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 0)
      //   S'3
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 3,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 4, /*storeSlot*/ -1)
      //   S''6
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 6,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ -1)

      // Temporal Unit 6..
      tu++
      //   S3
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 3,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ -1)
      //   S'1
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 1,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 1)
      //   S''5
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 5,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 5)

      // Temporal Unit 7..
      tu++
      //   S2
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 0, /*storeSlot*/ 3)
      //   S'3
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 3,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ -1)
      //   S''6
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 6,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 5, /*storeSlot*/ -1)

      // Temporal Unit 8..
      tu++
      //   S3
      enc_setup_special_frame(tu, /*spatial*/ 1, /*temporal*/ 3,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 3, /*storeSlot*/ -1)
      //   S'2
      enc_setup_special_frame(tu, /*spatial*/ 2, /*temporal*/ 2,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 1, /*storeSlot*/ 4)
      //   S''4
      enc_setup_special_frame(tu, /*spatial*/ 3, /*temporal*/ 4,
                              /*ftype*/ INTER_FRAME, /*refSlot*/ 2, /*storeSlot*/ 2)
    }
  }
}


enc_schedule_special_scalability() {
  enc_pick_timing_info()

  if (enc_scalability_mode == SCALABILITY_L3T2_KEY) {
    enc_schedule_scalability_l3t2_key()
  } else if (enc_scalability_mode == SCALABILITY_L3T3_KEY) {
    enc_schedule_scalability_l3t3_key()
  } else if (enc_scalability_mode == SCALABILITY_L4T5_KEY) {
    enc_schedule_scalability_l4t5_key()
  } else if (enc_scalability_mode == SCALABILITY_L4T7_KEY) {
    enc_schedule_scalability_l4t7_key()
  } else if (enc_scalability_mode == SCALABILITY_L3T2_KEY_SHIFT) {
    enc_schedule_scalability_l3t2_key_shift()
  } else if (enc_scalability_mode == SCALABILITY_L3T3_KEY_SHIFT) {
    enc_schedule_scalability_l3t3_key_shift()
  } else if (enc_scalability_mode == SCALABILITY_L4T5_KEY_SHIFT) {
    enc_schedule_scalability_l4t5_key_shift()
  } else if (enc_scalability_mode == SCALABILITY_L4T7_KEY_SHIFT) {
    enc_schedule_scalability_l4t7_key_shift()
  } else {
    ASSERT(0, "enc_scalability_mode %d not implemented", enc_scalability_mode)
  }
}


// Top-level function to construct the frame schedule
enc_schedule_frames() {
  if (enc_still_picture) {
    enc_frame_cnt = 1
  } else if (enc_large_scale_tile) {
    enc_anchor_frame_cnt u(0)

    CHECK (1 <= enc_anchor_frame_cnt &&
           enc_anchor_frame_cnt <= MAX_ANCHOR_FRAMES,
           "enc_anchor_frame_cnt is %d but must be in the range [1, %d].",
           enc_anchor_frame_cnt, MAX_ANCHOR_FRAMES)

    enc_tile_list_frame_cnt u(0)
    enc_frame_cnt = enc_anchor_frame_cnt + enc_tile_list_frame_cnt + 1
  } else {
    enc_frame_cnt u(0)
  }
  CHECK (1 <= enc_frame_cnt && enc_frame_cnt <= ENC_MAX_FRAMES,
         "enc_frame_cnt is %d but must be in the range [1, %d].",
         enc_frame_cnt, ENC_MAX_FRAMES)

  enc_pick_scalability()

  if (enc_still_picture) {
    // Special case: Still image streams consist entirely of shown keyframes,
    // and don't use scalability
    for (f = 0; f < enc_frame_cnt; f++) {
      sched_temporal_unit[f] = f
      sched_spatial_layer[f] = 0
      sched_temporal_layer[f] = 0
      sched_show_existing_frame[f] = 0
      sched_frame_type[f] = KEY_FRAME
      sched_show_frame[f] = 1
      sched_refresh_frame_flags[f] = 0xff
      if (enc_enable_order_hint) {
        sched_order_hint[f] = ChooseBits(enc_order_hint_bits)
      }
    }
  } else if (enc_stress_memory) {
    enc_schedule_memory_stress_stream()
  } else if (enc_use_scalability &&
             enc_use_scalability_preset &&
             enc_scalability_mode >= SCALABILITY_L3T2_KEY) {
    enc_schedule_special_scalability()
  } else {
    enc_schedule_scalability() // (Re)sets enc_frame_cnt

    // Ensure that we meet the following conformance requirement:
    // "Any sequence header in a bitstream which changes the parameters
    //  must be contained in a temporal unit with temporal_id equal to zero."
    ASSERT(sched_temporal_layer[0] == 0, "Changing sequence header parameters in an invalid place!")

    enc_schedule_barriers()
    enc_schedule_order_hints()

    // Clear arrays used by enc_schedule-frame_type
    for (f = 0; f < enc_frame_cnt; f++) {
      sched_show_existing_frame[f] = 0
      sched_showable_frame[f] = 0
      sched_refresh_frame_flags[f] = 0
      sched_num_links[f] = 0
      for (i = 0; i < NUM_REF_FRAMES; i++) {
        sched_frame_in_ref_slot[f][i] = -1
      }
    }

    for (f = 0; f < enc_frame_cnt; f += 1) {
      enc_schedule_frame_type(f)
    }
  }

  enc_frame_id_numbers_present_flag u(0)
  if (enc_frame_id_numbers_present_flag) {
    enc_schedule_frame_ids()
  }
}

#else // ENCODE
empty_schedule() {}
#endif // ENCODE
