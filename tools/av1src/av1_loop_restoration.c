/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS 0
#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_LOOP_RESTORATION_OUTPUT 1
#else
#define VALIDATE_SPEC_LOOP_RESTORATION_OUTPUT 0
#endif


any_loop_restoration_used() {
  return UsesLr
}

uint2 remap_lr_type[4] = {
  RESTORE_NONE, RESTORE_SWITCHABLE, RESTORE_WIENER, RESTORE_SGRPROJ
};

read_frame_restoration_info() {
  usesChromaLr = 0
  for (i = 0; i < get_num_planes(); i++) {
    lrType u(2)
    frame_restoration_type[i] = remap_lr_type[lrType]

    if (frame_restoration_type[i] != RESTORE_NONE) {
      UsesLr = 1
      if (i > 0)
        usesChromaLr = 1
    }
  }

  // Restoration unit size
  if (any_loop_restoration_used()) {
    if (superblock_size == BLOCK_128X128) {
      lr_unit_shift u(1)
      restoration_unit_size[0] = lr_unit_shift ? 256 : 128
    } else {
      lr_unit_shift u(1)
      if (lr_unit_shift) {
        lr_unit_extra_shift u(1)
        restoration_unit_size[0] = lr_unit_extra_shift ? 256 : 128
      } else {
        restoration_unit_size[0] = 64
      }
    }

    // In 4:2:0, we can choose the chroma restoration size as either
    // the same as the luma restoration units, or half the size of the luma restoration units.
    if (subsampling_x && subsampling_y && usesChromaLr) {
      lrUVShift u(1)
    } else {
      lrUVShift = 0
    }
    restoration_unit_size[1] = restoration_unit_size[0] >> lrUVShift
    restoration_unit_size[2] = restoration_unit_size[0] >> lrUVShift
  }
}

mi_to_upscaled_x(mi_col, ssx) {
    downscaled_x = mi_col * (MI_SIZE >> ssx)
    return (downscaled_x * superres_denom) / SUPERRES_NUM
}

// Called at the start of each frame; this works out how many restoration units
// lie in each tile, and linearizes the list of restoration units into
// the various restoration info arrays
setup_tile_restoration_info() {
  for (plane = 0; plane < get_num_planes(); plane++) {
    unitSz = restoration_unit_size[plane]
    ssx = plane_subsampling_x[plane]
    ssy = plane_subsampling_y[plane]
    planeW = ROUND_POWER_OF_TWO(upscaled_width, ssx)
    planeH = ROUND_POWER_OF_TWO(upscaled_height, ssy)
    tileIdx = 0
    startIdx = 0

    tileRows = 1
    tileCols = 1

    for (row = 0; row < tileRows; row++) {
      for (col = 0; col < tileCols; col++) {
        tileStartX = 0
        tileStartY = 0
        tileEndX = planeW
        tileEndY = planeH
        tileW = tileEndX - tileStartX
        tileH = tileEndY - tileStartY

        unitRows = Max(1, (tileH + (unitSz / 2)) / unitSz)
        unitCols = Max(1, (tileW + (unitSz / 2)) / unitSz)

        restoration_unit_start[plane][tileIdx] = startIdx
        restoration_unit_rows[plane][tileIdx] = unitRows
        restoration_unit_cols[plane][tileIdx] = unitCols

        startIdx += unitRows * unitCols
        tileIdx += 1
      }
    }
  }
}

// Called once at the start of each tile; sets up a default
// set of parameters for delta-encoding, so that each tile can be
// decoded independently.
reset_lr_refs() {
  for (plane = 0; plane < get_num_planes(); plane++)
    for (i = 0; i < 2; i++) {
      for (j = 0; j < WIENER_COEFFS; j++) {
        ref_wiener_coeffs[plane][i][j] = wiener_taps_mid[j]
      }
      ref_sgr_xqd[plane][i] = sgrproj_xqd_mid[i]
    }
}

read_wiener_filter(plane, idx) {
  // The Wiener filter uses 7 taps for luma and 5 taps for chroma.
  // Because of this, we skip reading the outer (0th) coefficient
  // for chroma and assign it the value of 0.
  if (plane) {
    firstCoeff = 1
    wiener_coeffs[plane][idx][0][0] = 0
    wiener_coeffs[plane][idx][1][0] = 0
  } else {
    firstCoeff = 0
  }

#if ENCODE
  enc_use_extreme_coeff_set = (Choose(0, 9) == 0)
#endif

  for (i = 0; i < 2; i++) {
    for (j = firstCoeff; j < WIENER_COEFFS; j++) {
      min = wiener_taps_min[j]
      max = wiener_taps_max[j]
      k = wiener_taps_k[j] // Subexponential code parameter
#if ENCODE
      enc_wiener_v u(0)
      v = enc_wiener_v
      encode_range_refsubexp_ec(min, max + 1, k, ref_wiener_coeffs[plane][i][j], v)
#else
      v = decode_range_refsubexp_ec(min, max + 1, k, ref_wiener_coeffs[plane][i][j])
#endif
      wiener_coeffs[plane][idx][i][j] = v
      ref_wiener_coeffs[plane][i][j] = v // Store this value as a reference for the next restoration unit
    }
  }
}

read_sgrproj_filter(plane, idx) {
  sgr_param_set[plane][idx] ae(v)
  set = sgr_param_set[plane][idx]
  for (i = 0; i < 2; i++) {
    min = sgrproj_xqd_min[i]
    max = sgrproj_xqd_max[i]
    radius = sgr_params[set][2*i]
    if (radius == 0) {
      // This filter is skipped in this parameter set,
      // so use a default value
      if (i == 0) {
        v = 0
      } else {
        v = clamp((1 << SGRPROJ_PRJ_BITS) - sgr_xqd[plane][idx][0],
                  sgrproj_xqd_min[1], sgrproj_xqd_max[1])
      }
    } else {
#if ENCODE
      enc_sgrproj_v u(0)
      v = enc_sgrproj_v
      encode_range_refsubexp_ec(min, max + 1, SGRPROJ_PRJ_SUBEXP_K, ref_sgr_xqd[plane][i], v)
#else // ENCODE
      v = decode_range_refsubexp_ec(min, max + 1, SGRPROJ_PRJ_SUBEXP_K, ref_sgr_xqd[plane][i])
#endif // ENCODE
    }
    sgr_xqd[plane][idx][i] = v
    ref_sgr_xqd[plane][i] = v // Store this value as a reference for the next restoration unit
  }
}

// Read the restoration info for a single restoration unit
read_unit_restoration_info(plane, idx) {
  if (frame_restoration_type[plane] == RESTORE_NONE) {
    restorationType = RESTORE_NONE
  } else if (frame_restoration_type[plane] == RESTORE_WIENER) {
    useWiener ae(v)
    restorationType = useWiener ? RESTORE_WIENER : RESTORE_NONE
  } else if (frame_restoration_type[plane] == RESTORE_SGRPROJ) {
    useSgrproj ae(v)
    restorationType = useSgrproj ? RESTORE_SGRPROJ : RESTORE_NONE
  } else {
    CHECK(frame_restoration_type[plane] == RESTORE_SWITCHABLE, "Unknown frame-level restoration type")
    restorationType ae(v)
  }

  unit_restoration_type[plane][idx] = restorationType
  if (restorationType == RESTORE_WIENER) {
    read_wiener_filter(plane, idx)
  } else if (restorationType == RESTORE_SGRPROJ) {
    read_sgrproj_filter(plane, idx)
  } else {
    // NONE, so don't read any data
    :pass
  }
}

// Read the restoration info for all restoration units whose top-left corners
// lie in the current superblock
read_sb_restoration_info(mi_row, mi_col) {
  for (plane = 0; plane < get_num_planes(); plane++) {
    tileId = 0
    miRowStart = 0
    miColStart = 0

    startIdx = restoration_unit_start[plane][tileId]
    unitRows = restoration_unit_rows[plane][tileId]
    unitCols = restoration_unit_cols[plane][tileId]
    unitSz = restoration_unit_size[plane]

    ssx = plane_subsampling_x[plane]
    ssy = plane_subsampling_y[plane]

    // Calculate our position within the current tile
    relMiRow = mi_row - miRowStart
    relMiCol = mi_col - miColStart

    // Work out which restoration unit rows the top/bottom edges of this superblock
    // lie in
    rrow0 = (relMiRow * (MI_SIZE >> ssy) + (unitSz - 1)) / unitSz
    mi_row1 = relMiRow + mib_size
    rrow1 = Min(unitRows, (mi_row1 * (MI_SIZE >> ssy) + (unitSz - 1)) / unitSz)

    // Work out which restoration unit columns the left/right edges of this superblock
    // lie in
    // Calculate upscaled width using 1/8 px intermediate precision, so that we can
    // avoid losing precision in the following calculations
    upscaledMiW = (MI_SIZE >> ssx) * superres_denom
    upscaledUnitSz = unitSz * SUPERRES_NUM

    rcol0 = (relMiCol * upscaledMiW + (upscaledUnitSz - 1)) / upscaledUnitSz
    mi_col1 = relMiCol + mib_size
    rcol1 = Min(unitCols, (mi_col1 * upscaledMiW + (upscaledUnitSz - 1)) / upscaledUnitSz)

    ASSERT(rrow1 - rrow0 <= 1, "Each superblock should encode at most 1 row of LR params")
    ASSERT(rcol1 - rcol0 <= 2, "Each superblock should encode at most 2 cols of LR params")

    // Iterate over all restoration units whose top-left corners lie in this
    // superblock
    for (rrow = rrow0; rrow < rrow1; rrow++) {
      for (rcol = rcol0; rcol < rcol1; rcol++) {
        idx = startIdx + rrow * unitCols + rcol
        read_unit_restoration_info(plane, idx)
      }
    }
  }
}

loop_restoration_frame() {
  if (!any_loop_restoration_used()) {
    for (plane = 0; plane < get_num_planes(); plane++) {
      planeW = ROUND_POWER_OF_TWO(upscaled_width, plane_subsampling_x[plane])
      planeH = ROUND_POWER_OF_TWO(upscaled_height, plane_subsampling_y[plane])
      // Special case: If no loop restoration was used, copy everything
      // across directly
      for (i = 0; i < planeH; i++)
        for (j = 0; j < planeW; j++)
          lr_frame[plane][j][i] = upscaled_cdef_frame[plane][j][i]
    }
    return 0
  }

  // Walk over each restoration unit in the frame and apply filtering
  for (plane = 0; plane < get_num_planes(); plane++) {
    unitSz = restoration_unit_size[plane]
    ssx = plane_subsampling_x[plane]
    ssy = plane_subsampling_y[plane]
    planeW = ROUND_POWER_OF_TWO(upscaled_width, ssx)
    planeH = ROUND_POWER_OF_TWO(upscaled_height, ssy)
    procW = RESTORATION_PROCUNIT_SIZE >> ssx
    procH = RESTORATION_PROCUNIT_SIZE >> ssy
    offset = RESTORATION_TILE_OFFSET >> ssy

    tileRows = 1
    tileCols = 1

    tileIdx = 0
    for (tileRow = 0; tileRow < tileRows; tileRow++) {
      for (tileCol = 0; tileCol < tileCols; tileCol++) {
        startIdx = restoration_unit_start[plane][tileIdx]
        unitRows = restoration_unit_rows[plane][tileIdx]
        unitCols = restoration_unit_cols[plane][tileIdx]
        tileStartX = 0
        tileStartY = 0
        tileEndX = planeW
        tileEndY = planeH
        tileW = tileEndX - tileStartX
        tileH = tileEndY - tileStartY

        for (row = 0; row < unitRows; row++) {
          y0 = row * unitSz - offset
          y1 = (row == unitRows - 1) ? tileH : y0 + unitSz

          for (col = 0; col < unitCols; col++) {
            x0 = col * unitSz
            x1 = (col == unitCols - 1) ? tileW : x0 + unitSz

            unitIdx = startIdx + row * unitCols + col
            for (y = y0; y < y1; y += procH)
              for (x = x0; x < x1; x += procW) {
                w = Min(procW, x1-x)
                // Handle the topmost stripe of each tile specially
                if (y < 0) {
                  h = Min(procH - offset, y1)
                  restore_procunit(tileStartX + x, tileStartY + 0, w, h, plane, tileRow, tileCol, unitIdx)
                } else {
                  h = Min(procH, y1-y)
                  restore_procunit(tileStartX + x, tileStartY + y, w, h, plane, tileRow, tileCol, unitIdx)
                }
              }
          }
        }
        tileIdx += 1
      }
    }
  }
#if VALIDATE_SPEC_LOOP_RESTORATION_OUTPUT
  for (plane = 0; plane < get_num_planes(); plane++) {
    ssx = plane_subsampling_x[plane]
    ssy = plane_subsampling_y[plane]
    planeW = ROUND_POWER_OF_TWO(upscaled_width, ssx)
    planeH = ROUND_POWER_OF_TWO(upscaled_height, ssy)
    for ( i = 0; i < planeH; i++) {
        for ( j = 0; j < planeW; j++ ) {
            validate(120000)
            validate(plane)
            validate(j)
            validate(i)
            validate( lr_frame[ plane ][ j ][ i ] )
        }
    }
  }
#endif

}

// The three places we can source pixels from for the processing unit borders:
#define SRC_DEBLOCK 0 // Use deblocked pixels (default)
#define SRC_CONTINUE 1 // Use the CDEF pixels from across the tile border
#define SRC_EXT_CDEF 2 // Take CDEF pixels inside the tile and extend

setup_procunit(x, y, w, h, plane, tileRow, tileCol) {
  // Determine the region we're allowed to source pixels from
  ssx = plane_subsampling_x[plane]
  ssy = plane_subsampling_y[plane]
  planeW = ROUND_POWER_OF_TWO(upscaled_width, ssx)
  planeH = ROUND_POWER_OF_TWO(upscaled_height, ssy)
  minX = 0
  maxX = planeW - 1
  minY = 0
  maxY = planeH - 1
  hborder = 3

  vborder = 2 // Always copy 2 lines above/below

  aboveSrc = SRC_DEBLOCK
  belowSrc = SRC_DEBLOCK

  tileStartY = 0
  tileEndY = planeH

  if (y == tileStartY)
    aboveSrc = SRC_CONTINUE
  if (y+h >= tileEndY)
    belowSrc = SRC_CONTINUE

  if (y == minY)
    aboveSrc = SRC_EXT_CDEF
  if (y+h > maxY)
    belowSrc = SRC_EXT_CDEF

  // Copy the inner lines (ie, everything except the top and bottom borders)
  // from the CDEF output
  for (i = 0; i < h; i++)
    for (j = -hborder; j < w + hborder; j++) {
      src_x = clamp(x+j, minX, maxX)
      src_y = clamp(y+i, minY, maxY)
      procunit_input[j][i] = upscaled_cdef_frame[plane][src_x][src_y]
    }

  // Fill the top border from the appropriate place
  if (aboveSrc == SRC_DEBLOCK) {
    for (i = -vborder; i < 0; i++)
      for (j = -hborder; j < w + hborder; j++) {
        src_x = clamp(x+j, minX, maxX)
        src_y = clamp(y+i, minY, maxY)
        procunit_input[j][i] = upscaled_frame[plane][src_x][src_y]
      }

    // Duplicate the top line
    i = -3
    src_y = -2
    for (j = -hborder; j < w + hborder; j++)
      procunit_input[j][i] = procunit_input[j][src_y]
  } else if (aboveSrc == SRC_CONTINUE) {
    for (i = -3; i < 0; i++)
      for (j = -hborder; j < w + hborder; j++) {
        src_x = clamp(x+j, minX, maxX)
        src_y = clamp(y+i, minY, maxY)
        procunit_input[j][i] = upscaled_cdef_frame[plane][src_x][src_y]
      }
  } else /* aboveSrc == SRC_EXT_CDEF */ {
    for (i = -3; i < 0; i++)
      for (j = -hborder; j < w + hborder; j++) {
        src_x = clamp(x+j, minX, maxX)
        src_y = clamp(y+i, minY, maxY)
        procunit_input[j][i] = upscaled_cdef_frame[plane][src_x][src_y]
      }
  }

  // Fill the bottom border
  if (belowSrc == SRC_DEBLOCK) {
    for (i = h; i < h + vborder; i++)
      for (j = -hborder; j < w + hborder; j++) {
        src_x = clamp(x+j, minX, maxX)
        src_y = clamp(y+i, minY, maxY)
        procunit_input[j][i] = upscaled_frame[plane][src_x][src_y]
      }

    // Duplicate the bottom line
    i = h+2
    src_y = h+1
    for (j = -hborder; j < w + hborder; j++)
      procunit_input[j][i] = procunit_input[j][src_y]
  } else if (belowSrc == SRC_CONTINUE) {
    for (i = h; i < h + RESTORATION_BORDER; i++)
      for (j = -hborder; j < w + hborder; j++) {
        src_x = clamp(x+j, minX, maxX)
        src_y = clamp(y+i, minY, maxY)
        procunit_input[j][i] = upscaled_cdef_frame[plane][src_x][src_y]
      }
  } else /* belowSrc == SRC_EXT_CDEF */ {
    for (i = h; i < h + RESTORATION_BORDER; i++)
      for (j = -hborder; j < w + hborder; j++) {
        src_x = clamp(x+j, minX, maxX)
        src_y = clamp(y+i, minY, maxY)
        procunit_input[j][i] = upscaled_cdef_frame[plane][src_x][src_y]
      }
  }
}

restore_procunit(x, y, w, h, plane, tileRow, tileCol, unitIdx) {
  type = unit_restoration_type[plane][unitIdx]
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
  if (type != RESTORE_NONE) {
    validate(120006)
    validate(plane)
    validate(type)
  }
#endif
  if (type == RESTORE_NONE) {
    for (i = 0; i < h; i++)
      for (j = 0; j < w; j++)
        lr_frame[plane][x+j][y+i] = upscaled_cdef_frame[plane][x+j][y+i]
  } else if (type == RESTORE_WIENER) {
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
    validate(120001)
    validate(plane)
    validate(x)
    validate(y)
    validate(w)
    validate(h)
#endif
#if DECODE && COLLECT_STATS
    total_wiener_px += w * h
#endif
    // Fetch input into a temporary buffer
    setup_procunit(x, y, w, h, plane, tileRow, tileCol)
    :log_lr(b, x, y, w, h, plane, 0)
    :C log_lr(b, x, y, w, h, plane, 0)

    // Use the same rounding as the single-ref inter prediction filter
    setup_interp_rounding(0)

    int16 filter[7]
    // Horizontal convolution: Generate h+6 rows of pixels for the vertical filter to use
    filter[3] = 128
    for (i = 0; i < 3; i++) {
      // Note: We read the vertical filter then the horizontal filter. So the horizontal
      // filter coeffs are in wiener_coeffs[plane][unitIdx][1], and vertical
      // in wiener_coeffs[plane][unitIdx][0]
      coeff = wiener_coeffs[plane][unitIdx][1][i]
      filter[i] = coeff
      filter[3] -= 2*coeff
      filter[6-i] = coeff
    }
    for (i = -3; i < h+3; i++)
      for (j = 0; j < w; j++) {
        offset = (1 << (bit_depth + FILTER_BITS - 1))
        sum = 0
        for (k = -3; k < 4; k++)
          sum += procunit_input[j+k][i] * filter[k+3]

        offset = (1 << (bit_depth + (FILTER_BITS - interp_round0) - 1))
        limit = (1 << (bit_depth + 1 + (FILTER_BITS - interp_round0))) - 1
        // Note: Unlike the other convolve functions, we really do need to clamp here!
        inter_temp[j][i+3] = clamp(ROUND_POWER_OF_TWO(sum, interp_round0), -offset, limit-offset)
      }

    // Vertical convolution
    filter[3] = 128
    for (i = 0; i < 3; i++) {
      coeff = wiener_coeffs[plane][unitIdx][0][i]
      filter[i] = coeff
      filter[3] -= 2*coeff
      filter[6-i] = coeff
    }
    for (i = 0; i < h; i++)
      for (j = 0; j < w; j++) {
        sum = 0
        for (k = -3; k < 4; k++)
          sum += inter_temp[j][i+k+3] * filter[k+3]
        lr_frame[plane][x+j][y+i] = clip_pixel(ROUND_POWER_OF_TWO(sum, interp_round1))
      }

    :log_lr(b, x, y, w, h, plane, 1)
    :C log_lr(b, x, y, w, h, plane, 1)
  } else if (type == RESTORE_SGRPROJ) {
#if DECODE && COLLECT_STATS
    total_selfguided_px += w * h
#endif
    // Fetch input into a temporary buffer
    setup_procunit(x, y, w, h, plane, tileRow, tileCol)
    :log_lr(b, x, y, w, h, plane, 0)
    :C log_lr(b, x, y, w, h, plane, 0)

    int32 flt1[RESTORATION_PROCUNIT_SIZE * RESTORATION_PROCUNIT_SIZE]
    int32 flt2[RESTORATION_PROCUNIT_SIZE * RESTORATION_PROCUNIT_SIZE]
    set = sgr_param_set[plane][unitIdx]
    ASSERT(set >= 0 && set < (1 << SGRPROJ_PARAMS_BITS), "Set out of bounds")
    r1 = sgr_params[set][0]
    s1 = sgr_params[set][1]
    r2 = sgr_params[set][2]
    s2 = sgr_params[set][3]
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
    validate(120005)
    validate(r1)
    validate(r2)
#endif
    // The xqd parameters convert to weights as follows:
    w0 = sgr_xqd[plane][unitIdx][1] // Weight of unfiltered image
    w1 = sgr_xqd[plane][unitIdx][0] // Weight of first filtered image
    w2 = (1 << SGRPROJ_PRJ_BITS) - w0 - w1 // Weight of second filtered image

    self_guided_filter(x, y, w, h, r1, s1, plane, flt1)
    self_guided_filter(x, y, w, h, r2, s2, plane, flt2)
    for (i = 0; i < h; i++)
      for (j = 0; j < w; j++) {
        f0 = procunit_input[j][i] << SGRPROJ_RST_BITS // Unfiltered image
        f1 = flt1[i * RESTORATION_PROCUNIT_SIZE + j] // First filtered image
        f2 = flt2[i * RESTORATION_PROCUNIT_SIZE + j] // Second filtered image
        v = w0 * f0 + w1 * f1 + w2 * f2
        sign = (v < 0) ? -1 : 1
        offset = (v < 0) ? 0 : 1
        vBits = sign * (CeilLog2(Abs(v + offset)) + 1)
        ASSERT(-(9+bit_depth) <= vBits && vBits <= (13+bit_depth), "Test") // Check that v fits into an int(13+bd)
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
        validate(120002)
        validate(plane)
        validate(x+j)
        validate(y+i)
        validate(w0)
        validate(w1)
        validate(f0)
        if (r1) {
            validate(0)
            validate(f1)
        }
        if (r2) {
            validate(1)
            validate(f2)
        }
        validate(v)
#endif
        s = ROUND_POWER_OF_TWO(v, SGRPROJ_PRJ_BITS + SGRPROJ_RST_BITS)
        sign = (s < 0) ? -1 : 1
        offset = (s < 0) ? 0 : 1
        sBits = sign * (CeilLog2(Abs(s + offset)) + 1)
        ASSERT(-(-2+bit_depth) <= sBits && sBits <= (2+bit_depth), "Test") // Check that v fits into an int(2+bd)
        lr_frame[plane][x+j][y+i] = clip_pixel(s)
      }

    :log_lr(b, x, y, w, h, plane, 1)
    :C log_lr(b, x, y, w, h, plane, 1)
  } else {
    ASSERT(0, "Unknown unit-level restoration type")
  }
}

self_guided_filter(x, y, w, h, r, s, plane, int32pointer flt) {
  if (r == 0) {
    // This filter is skipped, so just copy pixels across
    fltStride = RESTORATION_PROCUNIT_SIZE
    for (i = 0; i < h; i++)
      for (j = 0; j < w; j++)
        flt[i * fltStride + j] = procunit_input[j][i] << SGRPROJ_RST_BITS

    return 0
  }

  uint32 u
  uint32 v
  uint32 n
  uint32 q
  uint32 z
  uint32 A[SGR_INTERMEDIATE_SIZE * SGR_INTERMEDIATE_SIZE]
  uint32 B[SGR_INTERMEDIATE_SIZE * SGR_INTERMEDIATE_SIZE]
  bufStride = SGR_INTERMEDIATE_SIZE // Can't use 'stride' as that's the stride of the overall frame
  fltStride = RESTORATION_PROCUNIT_SIZE

  n = (2*r+1) * (2*r+1) // Will always be either 9 or 25
  one_over_n = ((1 << SGRPROJ_RECIP_BITS) + (n/2)) / n

  // First stage: Calculate A and B arrays
  // Only generate odd-index rows for the r=2 case
  step = (r == 2) ? 2 : 1

  for (i = -1; i < h+1; i += step)
    for (j = -1; j < w+1; j++)
    {
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
      validate(120003)
      validate(i)
      validate(j)
#endif
      idx = (i+1) * bufStride + (j+1)

      // Construct windowed sums of (px) and (px)^2
      // We do this in a very simple way, as a check on the (more complex)
      // reference code
      sum = 0
      sum_sq = 0
      for (k = -r; k <= r; k++)
        for (l = -r; l <= r; l++) {
          px = procunit_input[j+l][i+k]
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
          validate(px)
#endif
          sum += px
          sum_sq += px*px
        }
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
        validate(sum_sq)
        validate(sum)
#endif
      // Use sum and sum_sq to calculate the A and B arrays
      u = ROUND_POWER_OF_TWO(sum_sq, 2 * (bit_depth - 8))
      v = ROUND_POWER_OF_TWO(sum, bit_depth - 8)
      q = (u * n < v * v) ? 0 : u * n - v * v // (Un-normalized) local variance

      // Map local variance by a map sending [0, infinity] -> [1, 256]
      z = ROUND_POWER_OF_TWO(q * s, SGRPROJ_MTABLE_BITS)
      if (z >= 255) {
        A[idx] = 256
      } else if (z == 0) {
        // Special case: Saturate to A[idx] >= 1, so that:
        // i) (256 - A[idx]) fits in a uint8, for hardware implementations
        // ii) The calculation of B[idx] below wont't overflow
        A[idx] = 1
      } else {
        A[idx] = ((z << SGRPROJ_SGR_BITS) + (z/2)) / (z + 1)
      }

      uint32 localMean
      uint32 blendFactor
      localMean = sum * one_over_n
      blendFactor = SGRPROJ_SGR - A[idx]
      B[idx] = ROUND_POWER_OF_TWO(localMean * blendFactor, SGRPROJ_RECIP_BITS)
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
      validate(z)
      validate(A[idx])
      validate(r)
      validate(n)
      validate(one_over_n)
      validate(B[idx])
#endif
    }

  // Second stage: Calculate filtered image
  for (i = 0; i < h; i++)
    for (j = 0; j < w; j++) {
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
      validate(120004)
      validate(i)
      validate(j)
#endif
      base = (i+1) * bufStride + (j+1)
      if (r == 2) {
        // Because we only calculated the odd rows of A and B, the even
        // and odd output rows need different calculations
        if (!(i & 1)) {
          shift = 5
          u = 6 * (A[base - bufStride] + A[base + bufStride]) +
              5 * (A[base - 1 - bufStride] + A[base + 1 - bufStride] + A[base - 1 + bufStride] + A[base + 1 + bufStride])
          v = 6 * (B[base - bufStride] + B[base + bufStride]) +
              5 * (B[base - 1 - bufStride] + B[base + 1 - bufStride] + B[base - 1 + bufStride] + B[base + 1 + bufStride])
        } else {
          shift = 4
          u = 6 * A[base] + 5 * (A[base - 1] + A[base + 1])
          v = 6 * B[base] + 5 * (B[base - 1] + B[base + 1])
        }
      } else {
        shift = 5
        u = 4*(A[base] + A[base - 1] + A[base + 1] + A[base - bufStride] + A[base + bufStride]) +
            3*(A[base - bufStride - 1] + A[base - bufStride + 1] + A[base + bufStride - 1] + A[base + bufStride + 1])
        v = 4*(B[base] + B[base - 1] + B[base + 1] + B[base - bufStride] + B[base + bufStride]) +
            3*(B[base - bufStride - 1] + B[base - bufStride + 1] + B[base + bufStride - 1] + B[base + bufStride + 1])
      }
      px = u * procunit_input[j][i] + v
      flt[i * fltStride + j] = ROUND_POWER_OF_TWO(px, SGRPROJ_SGR_BITS + shift - SGRPROJ_RST_BITS)
#if VALIDATE_SPEC_LOOP_RESTORATION_INTERNALS
      validate(u)
      validate(v)
      validate(px)
      validate(shift)
      validate(flt[i * fltStride + j])
#endif
    }
}
