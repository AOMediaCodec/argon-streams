/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

// Inter frame planner for our encoder.
// This is called each time we start encoding a tile in an inter frame,
// and acts to determine a few things up front:
// * What partition structure to use
// * Whether each block is intra or inter
// * What reference frames to use
// * Things which affect the above - eg, segment IDs and skip-mode
// This will help us to control the distributions of things like
// ref-mv stack lengths. Randomly selecting ref frames per block tends
// to bias towards short lengths - particularly for compound blocks - but
// carefully pre-planning can "smooth out" this distribution.

// Note: We only set segment IDs if seg_enabled && (!seg_update_map || preskip_seg_id)
#if ENCODE

#define PLAN_UNINITIALIZED (-2)

enc_set_segment_id(mi_row, mi_col, segId) {
  ASSERT(seg_enabled, "enc_set_segment_id should only be called if seg_enabled is true")

  // Parameters for this block, which will be needed/initialized by phase 2.
  // If these parameters are forced to take particular values due to segmentation,
  // then we set them up here.
  int8 refFrame[2]
  skipMode = enc_skip_mode[mi_col][mi_row]
  skip = enc_skip_coeff[mi_col][mi_row]
  isInter = enc_is_inter[mi_col][mi_row]
  refFrame[0] = enc_ref_frame[mi_col][mi_row][0]
  refFrame[1] = enc_ref_frame[mi_col][mi_row][1]

  if (segfeature_active_idx(segId, SEG_LVL_SKIP)) {
    isInter = Choose(0, 99) < enc_inter_prob
    refFrame[0] = isInter ? LAST_FRAME : INTRA_FRAME // May be overwritten by SEG_LVL_REF_FRAME
    refFrame[1] = NONE
    skipMode = 0
    skip = 1
  }
  if (segfeature_active_idx(segId, SEG_LVL_GLOBALMV)) {
    refFrame[0] = LAST_FRAME
    refFrame[1] = NONE
    isInter = 1
    skipMode = 0
  }
  if (segfeature_active_idx(segId, SEG_LVL_REF_FRAME)) {
    refFrame[0] = get_segdata_idx(segId, SEG_LVL_REF_FRAME)
    refFrame[1] = NONE
    isInter = (refFrame[0] != INTRA_FRAME)
    skipMode = 0
  }

  // Save the above data into the phase 2 arrays
  enc_segment_id[mi_col][mi_row] = segId
  enc_skip_mode[mi_col][mi_row] = skipMode
  enc_skip_coeff[mi_col][mi_row] = skip
  enc_is_inter[mi_col][mi_row] = isInter
  enc_ref_frame[mi_col][mi_row][0] = refFrame[0]
  enc_ref_frame[mi_col][mi_row][1] = refFrame[1]
}

enc_set_block_size(mi_row, mi_col, bsize, partition) {
  bw = Min(mi_size_wide[bsize], mi_cols - mi_col)
  bh = Min(mi_size_high[bsize], mi_rows - mi_row)
  for (i = 0; i < bh; i++) {
    for (j = 0; j < bw; j++) {
      enc_sb_sizes[mi_col + j][mi_row + i] = bsize
    }
  }
  enc_block_partition[mi_col][mi_row] = partition

  // Reset all of the block information arrays we're about to set up
  enc_segment_id[mi_col][mi_row] = PLAN_UNINITIALIZED
  enc_skip_mode[mi_col][mi_row] = PLAN_UNINITIALIZED
  enc_skip_coeff[mi_col][mi_row] = PLAN_UNINITIALIZED
  enc_is_inter[mi_col][mi_row] = PLAN_UNINITIALIZED
  enc_ref_frame[mi_col][mi_row][0] = PLAN_UNINITIALIZED
  enc_ref_frame[mi_col][mi_row][1] = PLAN_UNINITIALIZED

  // Special case: If segmentation is enabled, but seg_update_map is 0,
  // then every block's segment ID is fixed. Set the values here so that
  // they are marked as set up before we run the second phase.
  if (seg_enabled && !seg_update_map) {
    segId = get_segment_id(mi_row, mi_col, bsize, 0)
    enc_set_segment_id(mi_row, mi_col, segId)
  }
}

enc_plan_partition(mi_row, mi_col, bsize) {
  hbs = mi_size_wide[bsize] / 2
  hasRows = (mi_row + hbs) < mi_rows
  hasCols = (mi_col + hbs) < mi_cols

  if (bsize == BLOCK_4X4) {
    partition = PARTITION_NONE
  } else {
    partition = encode_partition(mi_row, mi_col, hasRows, hasCols, bsize)
    ASSERT(enc_partition_idx < MAX_TILE_AREA_MI / 3, "enc_partition_idx out of range")
    enc_partition_symbols[enc_partition_idx] = partition
    enc_partition_idx += 1
  }

  subsize = get_subsize(bsize, partition)
  bsize2 = get_subsize(bsize, PARTITION_SPLIT)
  qbs = hbs / 2

  if (partition == PARTITION_NONE) {
    enc_set_block_size(mi_row, mi_col, bsize, partition)
  } else if (partition == PARTITION_HORZ) {
    enc_set_block_size(mi_row, mi_col, subsize, partition)
    if (hasRows) {
      enc_set_block_size(mi_row+hbs, mi_col, subsize, partition)
    }
  } else if (partition == PARTITION_VERT) {
    enc_set_block_size(mi_row, mi_col, subsize, partition)
    if (hasCols) {
      enc_set_block_size(mi_row, mi_col+hbs, subsize, partition)
    }
  } else if (partition == PARTITION_SPLIT) {
    enc_plan_partition(mi_row, mi_col, subsize)
    if (hasCols) {
      enc_plan_partition(mi_row, mi_col+hbs, subsize)
    }
    if (hasRows) {
      enc_plan_partition(mi_row+hbs, mi_col, subsize)
    }
    if (hasRows && hasCols) {
      enc_plan_partition(mi_row+hbs, mi_col+hbs, subsize)
    }
  } else if (partition==PARTITION_HORZ_A) {
    enc_set_block_size(mi_row, mi_col, bsize2, partition)
    enc_set_block_size(mi_row, mi_col + hbs, bsize2, partition)
    enc_set_block_size(mi_row + hbs, mi_col, subsize, partition)
  } else if (partition==PARTITION_HORZ_B) {
    enc_set_block_size(mi_row, mi_col, subsize, partition)
    enc_set_block_size(mi_row + hbs, mi_col, bsize2, partition)
    enc_set_block_size(mi_row + hbs, mi_col + hbs, bsize2, partition)
  } else if (partition==PARTITION_VERT_A) {
    enc_set_block_size(mi_row, mi_col, bsize2, partition)
    enc_set_block_size(mi_row + hbs, mi_col, bsize2, partition)
    enc_set_block_size(mi_row, mi_col + hbs, subsize, partition)
  } else if (partition==PARTITION_VERT_B) {
    enc_set_block_size(mi_row, mi_col, subsize, partition)
    enc_set_block_size(mi_row, mi_col + hbs, bsize2, partition)
    enc_set_block_size(mi_row + hbs, mi_col + hbs, bsize2, partition)
  } else if (partition==PARTITION_HORZ_4) {
    qbs = hbs / 2
    for (i = 0; i < 4; i++) {
      row = mi_row + i * qbs
      if (row < mi_rows) {
        enc_set_block_size(row, mi_col, subsize, partition)
      } else {
        ASSERT(i != 0, "Why have we tried this partition?")
      }
    }
  } else if (partition==PARTITION_VERT_4) {
    qbs = hbs / 2
    for (i = 0; i < 4; i++) {
      col = mi_col + i * qbs
      if (col < mi_cols) {
        enc_set_block_size(mi_row, col, subsize, partition)
      } else {
        ASSERT(i != 0, "Why have we tried this partition?")
      }
    }
  }
}

// Set up all the data for a single block.
// This can (partially) initialize nearby blocks if we want to
// ensure that they have similar properties.
enc_plan_block(mi_row, mi_col, bsize) {
  bw = Min(mi_size_wide[bsize], mi_cols - mi_col)
  bh = Min(mi_size_high[bsize], mi_rows - mi_row)

  // At this point, the block's ref frames may be partially or fully set.
  // We need to select the other parameters (is_inter, skip_coeff, skip_mode, segment ID)
  // while being careful to keep compatibility with these already-set ref frames.
  int8 refs[2]
  refs[0] = enc_ref_frame[mi_col][mi_row][0]
  refs[1] = enc_ref_frame[mi_col][mi_row][1]

  // Determine whether certain properties are/aren't allowed.
  // For each value, there are three options:
  // 0 = must not be used, 1 = must be used, 2 = can choose
  isInter = PLAN_UNINITIALIZED
  isCompound = PLAN_UNINITIALIZED
  skipMode = PLAN_UNINITIALIZED
  skip = PLAN_UNINITIALIZED

  // If using skip mode, the block mode must be NEAREST_NEARESTMV.
  // There's a possibility that the nearest mv might be set to the global mv
  // for one or both of the ref frames. So, in order to avoid generating invalid
  // streams when the frame width is 16k or more, we need to check if the globalmv
  // for this block *using the skip-mode refs* is valid.
  // If it isn't, then we need to avoid skip mode.
  if (skip_mode_allowed) {
    canUseSkipMode = 1
    ref_frame[0] = skip_ref[0]
    ref_frame[1] = skip_ref[1]
    setup_globalmv(mi_row, mi_col, bsize, -1, 1)
    for (i = 0; i < 2; i++) {
      for (j = 0; j < 2; j++) {
        if (globalmv[i][j] <= MV_LOW || globalmv[i][j] >= MV_UPP) {
          canUseSkipMode = 0
        }
      }
    }
  } else {
    canUseSkipMode = 0
  }

  // Check if this must be an intra block
  if (intra_only || refs[0] == INTRA_FRAME) {
    isInter = 0
    isCompound = 0
    skipMode = 0
  } else {
    if (refs[0] != PLAN_UNINITIALIZED && refs[1] != PLAN_UNINITIALIZED) {
      // Both refs are fixed. This must be an inter block, due to the above 'if',
      // but it might be a single-ref block or a compound block
      isInter = 1
      isCompound = (refs[1] != NONE)

      if (skip_mode_allowed && refs[0] == skip_ref[0] && refs[1] == skip_ref[1]) {
        // We can use skip mode here; we need to check whether we can
        // also encode this ref pair in the "standard" way or not
        skipMode = 1
        for (i = 0; i < SIGNALLED_COMP_REFS; i++) {
          av1_set_ref_frame(MAX_REF_FRAMES + i)
          if (refs[0] == rf[0] && refs[1] == rf[1]) {
            skipMode = 2
            break
          }
        }
        // If we can only reach this ref pair via skip mode, but
        // we can't actually use skip mode, then reset the block data entirely.
        if (skipMode == 1 && !canUseSkipMode) {
          isInter = 2
          isCompound = 2
          skipMode = 0
          refs[0] = PLAN_UNINITIALIZED
          refs[1] = PLAN_UNINITIALIZED
        }
      } else {
        skipMode = 0
      }
    } else if (refs[0] != PLAN_UNINITIALIZED) {
      // One ref is fixed, the other is not yet decided
      isInter = 1
      isCompound = 2

      if (canUseSkipMode && (refs[0] == skip_ref[0] || refs[0] == skip_ref[1])) {
        skipMode = 2
      } else {
        skipMode = 0
      }
    } else {
      // Neither ref is decided yet
      isInter = 2
      isCompound = 2
      skipMode = 2
    }

    compoundPossible = is_compound_allowed(bsize)

    if (isInter == 2) {
      isInter = large_scale_tile ? 1 : (Choose(0, 99) < enc_inter_prob)
    }
    if (isCompound == 2) {
      isCompound = (isInter && compoundPossible && !large_scale_tile) ? (Choose(0, 99) < enc_compound_prob) : 0
    } else if (isCompound == 1) {
      ASSERT(isInter && compoundPossible, "Bad selection of compound")
      ASSERT(!large_scale_tile, "Not allowed compound in large_scale_tile frames")
    }
    if (skipMode == 2) {
      enc_frame_planner_skip_mode_prob u(0)
      skipMode = (isInter && isCompound && canUseSkipMode && !large_scale_tile) ? Choose(0, 99) < enc_frame_planner_skip_mode_prob : 0
    } else if (skipMode == 1) {
      ASSERT(isInter && isCompound && canUseSkipMode, "Bad selection of skip_mode")
    }
  }

  skip = large_scale_tile ? 0 : (skipMode ? 1 : (Choose(0, 99) < enc_skip_prob))

  // Pick any remaining ref frames
  if (!isInter) {
    refs[0] = INTRA_FRAME
    refs[1] = NONE
  } else if (skipMode) {
    refs[0] = skip_ref[0]
    refs[1] = skip_ref[1]
  } if (refs[0] == PLAN_UNINITIALIZED) {
    // Can pick ref frames freely
    if (isCompound) {
      ASSERT(!large_scale_tile, "Not allowed compound in large_scale_tile frames")
      refPair = Choose(0, SIGNALLED_COMP_REFS-1)
      av1_set_ref_frame(MAX_REF_FRAMES + refPair)
      refs[0] = rf[0]
      refs[1] = rf[1]
    } else {
      enc_single_ref = LAST_FRAME
      if (! (large_scale_tile || enc_force_seg_lvl_ref_frame_last_frame)) {
        enc_single_ref u(0)
        CHECK (LAST_FRAME <= enc_single_ref && enc_single_ref <= ALTREF_FRAME,
               "enc_single_ref is %d, between LAST_FRAME and ALTREF_FRAME.",
               enc_single_ref)
      }
      refs[0] = enc_single_ref
      refs[1] = NONE
    }
  } else if (refs[1] == PLAN_UNINITIALIZED) {
    // One ref is fixed, the other undetermined
    if (isCompound) {
      // Build a list of all possible ref frame pairs which include
      // our fixed reference
      ASSERT(!large_scale_tile, "Not allowed compound in large_scale_tile frames")
      uint8 validIdcs[SIGNALLED_COMP_REFS]
      numValidIdcs = 0
      for (i = 0; i < SIGNALLED_COMP_REFS; i++) {
        av1_set_ref_frame(MAX_REF_FRAMES + i)
        if (rf[0] == refs[0] || rf[1] == refs[0]) {
          validIdcs[numValidIdcs] = i
          numValidIdcs += 1
        }
      }
      // Select a ref pair
      refPair = validIdcs[Choose(0, numValidIdcs-1)]
      av1_set_ref_frame(MAX_REF_FRAMES + refPair)
      refs[0] = rf[0]
      refs[1] = rf[1]
    } else {
      refs[1] = NONE
    }
  }

  // There are three cases for segment IDs:
  // * If seg_enabled is false, or it's true but seg_update_map is false,
  //   then there's no choice of segment ID anyway. Any constraints coming
  //   from things like SEG_LVL_SKIP have already been factored in to our
  //   ref frame selection.
  //
  // * If preskip_seg_id is true, there are no constraints on what segment ID we
  //   can signal, but we must pick one which is compatible with this block
  //   So we can select the segment ID here
  //
  // * If preskip_seg_id is false, then skip blocks have their segment IDs inferred
  //   from the surrounding blocks rather than being explicitly signalled.
  //   We can't figure that out here, due to the scan order, but it's fine -
  //   in this case, any segment ID is valid
  //
  // Hence we only need to set up segment IDs here in the second case.
  if (seg_enabled && seg_update_map && preskip_seg_id) {
    ASSERT(enc_segment_id[mi_col][mi_row] == PLAN_UNINITIALIZED, "This should be true here")

    // Build a list of compatible segments to select from.
    // Note: Due to the special cases in enc_pick_feature(), segment 0
    // should always be compatible, so we should always end up with at least
    // one valid segment.
    uint8 allowedSegIds[8]
    numAllowedSegIds = 0
    for (seg = 0; seg <= last_active_seg_id; seg++) {
      allowed = 1

      // Calculate the global MV for this block in this segment.
      // For large frames, this might be an invalid motion vector; if so,
      // then we need to avoid selecting a segment with SEG_LVL_SKIP (if inter)
      // or SEG_LVL_GLOBALMV
      fixedRef = LAST_FRAME
      if (segfeature_active_idx(seg, SEG_LVL_REF_FRAME)) {
        fixedRef = get_segdata_idx(seg, SEG_LVL_REF_FRAME)
      }
      globalValid = 1
      ref_frame[0] = fixedRef
      ref_frame[1] = NONE
      if (fixedRef == INTRA_FRAME) {
        globalValid = 0
      } else {
        setup_globalmv(mi_row, mi_col, bsize, -1, 0)
        for (j = 0; j < 2; j++) {
          if (globalmv[0][j] <= MV_LOW || globalmv[0][j] >= MV_UPP) {
            globalValid = 0
          }
        }
      }

      // Check the various segment feature constraints
      if (segfeature_active_idx(seg, SEG_LVL_REF_FRAME)) {
        allowed = allowed && refs[0] == fixedRef &&
                  refs[1] == NONE
      }
      if (segfeature_active_idx(seg, SEG_LVL_GLOBALMV)) {
        allowed = allowed && refs[0] == fixedRef && globalValid &&
                  refs[1] == NONE
      }
      if (segfeature_active_idx(seg, SEG_LVL_SKIP)) {
        allowed = allowed &&
                  (refs[0] == INTRA_FRAME ||
                   (refs[0] == fixedRef && globalValid)) &&
                  refs[1] == NONE &&
                  enc_skip_mode[mi_col][mi_row] == 0 &&
                  enc_skip_coeff[mi_col][mi_row] == 1
      }

      if (allowed) {
        allowedSegIds[numAllowedSegIds] = seg
        numAllowedSegIds += 1
      }
    }

    ASSERT(numAllowedSegIds > 0, "Can't find a valid segment ID")
    enc_segment_id[mi_col][mi_row] = allowedSegIds[Choose(0, numAllowedSegIds - 1)]
  }

  enc_is_inter[mi_col][mi_row] = isInter
  enc_skip_mode[mi_col][mi_row] = skipMode
  enc_skip_coeff[mi_col][mi_row] = skip
  enc_ref_frame[mi_col][mi_row][0] = refs[0]
  enc_ref_frame[mi_col][mi_row][1] = refs[1]

  return 0
}

enc_clear_mv_block_list() {
  for (i = 0; i < 2; i++) {
    enc_same_ref_cnt[i] = 0
    enc_modifiable_cnt[i] = 0
  }
}

// Is there any signallable reference pair (including the skip-mode pair)
// which is {ref0, ref1} in either order?
// If so, this function sets rf[] to the appropriate ref pair
// (ie, just {ref0, ref1}, but re-ordered if needed so that rf[0] < rf[1])
enc_compatible_ref_pair(ref0, ref1) {
  if (ref0 == ref1)
    return 0
  if (ref0 == INTRA_FRAME || ref1 == INTRA_FRAME)
    return 0

  // All signallable ref pairs are ordered with ref0 < ref1, so re-order
  // if needed
  if (ref0 > ref1) {
    tmp = ref0
    ref0 = ref1
    ref1 = tmp
  }

  // Check against the skip-mode pair
  if (skip_mode_allowed) {
    rf[0] = skip_ref[0]
    rf[1] = skip_ref[1]
    if (ref0 == rf[0] && ref1 == rf[1]) {
      return 1
    }
  }

  // Check the remaining signallable pairs
  for (i = 0; i < SIGNALLED_COMP_REFS; i++) {
    av1_set_ref_frame(MAX_REF_FRAMES + i)
    if (ref0 == rf[0] && ref1 == rf[1]) {
      return 1
    }
  }

  return 0
}

enc_scan_block_internal(mi_row, mi_col, mvrow, mvcol, region) {
  ASSERT(region == 0 || region == 1, "Invalid parameter to enc_scan_nearby_block()")

  candBsize = enc_sb_sizes[mvcol][mvrow]
  candBw = mi_size_wide[candBsize]
  candBh = mi_size_high[candBsize]
  compoundAllowed = is_compound_allowed(candBsize)

  candTop = mvrow & ~(candBh - 1)
  candLeft = mvcol & ~(candBw - 1)

  int8 srcRef[2]
  srcRef[0] = enc_ref_frame[mi_col][mi_row][0]
  srcRef[1] = enc_ref_frame[mi_col][mi_row][1]
  isCompound = (srcRef[1] != NONE)

  int8 candRef[2]
  candRef[0] = enc_ref_frame[candLeft][candTop][0]
  candRef[1] = enc_ref_frame[candLeft][candTop][1]

  // We will set at most one of the following:
  refMatch = 0 // Does the candidate block already have the same refs as us?
  refModifiable = 0 // Can we modify the candidate block to have the same refs?

  if (isCompound) {
    // Need to distinguish three cases:
    if (candRef[0] == srcRef[0] && candRef[1] == srcRef[1]) {
      // Both refs are set, and match the current block
      refMatch = 1
    } else if (candRef[0] == PLAN_UNINITIALIZED) {
      // Both refs are free
      refModifiable = compoundAllowed
    } else if (candRef[1] == PLAN_UNINITIALIZED) {
      // One ref is set; check if it matches either of our references
      refModifiable = (candRef[0] == srcRef[0] || candRef[0] == srcRef[1]) && compoundAllowed
    }
  } else {
    if (candRef[0] == srcRef[0] || candRef[1] == srcRef[0]) {
      refMatch = 1
    } else if (candRef[0] == PLAN_UNINITIALIZED) {
      refModifiable = 1
    } else if (candRef[1] == PLAN_UNINITIALIZED) {
      // One ref is set, but it doesn't match. Check if we can make the
      // candidate block match by making it compound
      refModifiable = compoundAllowed &&
                      enc_compatible_ref_pair(candRef[0], srcRef[0])
    }
  }

  if (refMatch) {
    if (region == 0) {
      ASSERT(enc_same_ref_cnt[0] < MAX_NEARBY_REFS, "Too many nearby refs")
    } else {
      ASSERT(enc_same_ref_cnt[1] < MAX_FAR_REFS, "Too many far refs")
    }
    // Check that we aren't listing a block twice
    // Note that, for the far region, we need to check both the near and
    // far lists. But we don't have to check the "modifiable" list here,
    // as any particular block will always be classified the same way
    for (i = 0; i <= region; i++) {
      for (j = 0; j < enc_same_ref_cnt[i]; j++) {
        ASSERT(!(enc_same_ref[i][j][0] == candTop &&
                 enc_same_ref[i][j][1] == candLeft), "Listing a block twice")
      }
    }
    enc_same_ref[region][enc_same_ref_cnt[region]][0] = candTop
    enc_same_ref[region][enc_same_ref_cnt[region]][1] = candLeft
    enc_same_ref_cnt[region] += 1
  } else if (refModifiable) {
    if (region == 0) {
      ASSERT(enc_modifiable_cnt[0] < MAX_NEARBY_REFS, "Too many nearby refs")
    } else {
      ASSERT(enc_modifiable_cnt[1] < MAX_FAR_REFS, "Too many far refs")
    }
    // Check that we aren't listing a block twice
    for (i = 0; i <= region; i++) {
      for (j = 0; j < enc_modifiable_cnt[i]; j++) {
        ASSERT(!(enc_modifiable[i][j][0] == candTop &&
                 enc_modifiable[i][j][1] == candLeft), "Listing a block twice")
      }
    }
    enc_modifiable[region][enc_modifiable_cnt[region]][0] = candTop
    enc_modifiable[region][enc_modifiable_cnt[region]][1] = candLeft
    enc_modifiable_cnt[region] += 1
  }
}

enc_scan_row(mi_row, mi_col, deltamvrow, region) {
  bsize = enc_sb_sizes[mi_col][mi_row]
  bw = mi_size_wide[bsize]
  endMi = Min(Min(bw, mi_cols - mi_col), mi_size_wide[BLOCK_64X64])
  deltamvcol = 0
  useStep16 = (bw>=16) ? 1 : 0

  prevDeltaRow = deltamvrow + 1

  if (Abs(deltamvrow) > 1) {
    deltamvrow *= 2
    deltamvrow += 1

    deltamvcol = 1
    if ((mi_size_high[bsize] < mi_size_high[BLOCK_8X8]) && (mi_row & 0x01)) {
      deltamvrow += 1
    }
    if ((mi_size_wide[bsize] < mi_size_wide[BLOCK_8X8]) && (mi_col & 0x01)) {
      deltamvcol -= 1
    }
  }
  // Also calculate the row offset of the previous row we scanned
  if (Abs(prevDeltaRow) > 1) {
    prevDeltaRow *= 2
    prevDeltaRow += 1

    if ((mi_size_high[bsize] < mi_size_high[BLOCK_8X8]) && (mi_row & 0x01)) {
      prevDeltaRow += 1
    }
  }

  for (i = 0; i < endMi; i+=0) {
    mvrow = mi_row + deltamvrow
    mvcol = mi_col + deltamvcol + i
    candBsize = enc_sb_sizes[mvcol][mvrow]
    candBw = mi_size_wide[candBsize]
    candBh = mi_size_high[candBsize]
    len = Min(bw, candBw)
    if (Abs(deltamvrow) > 1) {
      len = Max(len, mi_size_wide[BLOCK_8X8])
    }
    if (useStep16) {
      len = Max(mi_size_wide[BLOCK_16X16], len)
    }

    // Check if we've already scanned this block.
    // We've already scanned the block iff it overlaps a previously-scanned row.
    scanBlock = 1
    if (Abs(deltamvrow) > 1) {
      candTop = mvrow & ~(candBh - 1)
      candBottom = candTop + candBh
      if (candBottom >= mi_row + prevDeltaRow) {
        // This block has already been scanned
        scanBlock = 0
      }
    }

    if (scanBlock) {
      enc_scan_block_internal(mi_row, mi_col, mvrow, mvcol, region)
    }
    i += len
  }
}

enc_scan_col(mi_row, mi_col, deltamvcol, region) {
  bsize = enc_sb_sizes[mi_col][mi_row]
  bh = mi_size_high[bsize]
  endMi = Min(Min(bh, mi_rows - mi_row), mi_size_high[BLOCK_64X64])
  deltamvrow = 0
  useStep16 = (bh>=16) ? 1 : 0

  prevDeltaCol = deltamvcol + 1

  if (Abs(deltamvcol) > 1) {
    deltamvcol *= 2
    deltamvcol += 1

    deltamvrow = 1
    if ((mi_size_high[bsize] < mi_size_high[BLOCK_8X8]) && (mi_row & 0x01)) {
      deltamvrow -= 1
    }
    if ((mi_size_wide[bsize] < mi_size_wide[BLOCK_8X8]) && (mi_col & 0x01)) {
      deltamvcol += 1
    }
  }
  // Also calculate the col offset of the previous col we scanned
  if (Abs(prevDeltaCol) > 1) {
    prevDeltaCol *= 2
    prevDeltaCol += 1

    if ((mi_size_wide[bsize] < mi_size_wide[BLOCK_8X8]) && (mi_col & 0x01)) {
      prevDeltaCol += 1
    }
  }

  for (i = 0; i < endMi;i+=0) {
    mvrow = mi_row + deltamvrow + i
    mvcol = mi_col + deltamvcol
    candBsize = enc_sb_sizes[mvcol][mvrow]
    candBw = mi_size_wide[candBsize]
    candBh = mi_size_high[candBsize]
    len = Min(bh, candBh)
    if (Abs(deltamvcol) > 1) {
      len = Max(len, mi_size_high[BLOCK_8X8])
    }
    if (useStep16) {
      len = Max(mi_size_high[BLOCK_16X16], len)
    }

    // Check if we've already scanned this block.
    // We've already scanned the block iff it overlaps a previously-scanned row.
    scanBlock = 1
    if (Abs(deltamvcol) > 1) {
      candLeft = mvcol & ~(candBw - 1)
      candRight = candLeft + candBw
      if (candRight >= mi_col + prevDeltaCol) {
        // This block has already been scanned
        scanBlock = 0
      }
    }

    if (scanBlock) {
      enc_scan_block_internal(mi_row, mi_col, mvrow, mvcol, region)
    }
    i += len
  }
}

enc_scan_block(mi_row, mi_col, deltamvrow, deltamvcol, region) {
  mvrow = mi_row + deltamvrow
  mvcol = mi_col + deltamvcol
  if (is_inside(mvrow, mvcol)) {
    // Check whether this block has already been scanned
    if (deltamvrow == -1 && deltamvcol == -1) {
      // Top-left corner block, so check if it overlaps the leftmost "top" block
      // or the topmost "left" block
      topBsize = enc_sb_sizes[mi_col][mi_row - 1]
      topBw = mi_size_wide[topBsize]
      topBlockLeftEdge = mi_col & ~(topBw-1)
      if (topBlockLeftEdge < mi_col) {
        // This block has already been scanned
        return 0
      }

      leftBsize = enc_sb_sizes[mi_col - 1][mi_row]
      leftBh = mi_size_high[leftBsize]
      leftBlockTopEdge = mi_row & ~(leftBh-1)
      if (leftBlockTopEdge < mi_row) {
        // This block has already been scanned
        return 0
      }
    } else if (deltamvrow == -1) {
      // Top-right corner block, so check if it overlaps the rightmost "top" block
      bw = deltamvcol // This is always the width of the "current" block
      blockRightEdge = mi_col + bw

      topMvCol = mi_col + bw - 1
      topBsize = enc_sb_sizes[topMvCol][mi_row - 1]
      topBw = mi_size_wide[topBsize]
      topBlockLeftEdge = topMvCol & ~(topBw-1)
      topBlockRightEdge = topBlockLeftEdge + topBw
      if (topBlockRightEdge >= blockRightEdge) {
        // This block has already been scanned
        return 0
      }
    }
    enc_scan_block_internal(mi_row, mi_col, mvrow, mvcol, region)
  }
}

enc_add_refs(src_row, src_col, dst_row, dst_col) {
  // Add the ref(s) from 'src' to 'dst'.
  // Due to the way enc_propagate_refs() works, we should only ever call this
  // when the result will be valid.
  int8 srcRef[2]
  int8 dstRef[2]
  srcRef[0] = enc_ref_frame[src_col][src_row][0]
  srcRef[1] = enc_ref_frame[src_col][src_row][1]
  dstRef[0] = enc_ref_frame[dst_col][dst_row][0]
  dstRef[1] = enc_ref_frame[dst_col][dst_row][1]
  // 'dst' should not have both refs already set, or else it shouldn't have
  // been in the modifiable list
  ASSERT(dstRef[1] == PLAN_UNINITIALIZED, "Block in wrong compatibility list")

  dstBsize = enc_sb_sizes[dst_col][dst_row]
  compoundAllowed = is_compound_allowed(dstBsize)

  if (srcRef[1] != NONE) {
    // Compound
    if (dstRef[0] != PLAN_UNINITIALIZED) {
      ASSERT(dstRef[0] == srcRef[0] || dstRef[0] == srcRef[1], "Block in wrong compatibility list")
    }
    ASSERT(compoundAllowed, "Block in wrong compatibility list")
    dstRef[0] = srcRef[0]
    dstRef[1] = srcRef[1]
  } else {
    // Single
    if (dstRef[0] != PLAN_UNINITIALIZED) {
      // Make this block compound
      ASSERT(dstRef[0] != srcRef[0], "Block in wrong compatibility list")
      ASSERT(compoundAllowed, "Block in wrong compatibility list")
      compatible = enc_compatible_ref_pair(srcRef[0], dstRef[0])
      ASSERT(compatible, "Block in wrong compatibility list")
      dstRef[0] = rf[0]
      dstRef[1] = rf[1]
    } else {
      dstRef[0] = srcRef[0]
    }
  }

  enc_ref_frame[dst_col][dst_row][0] = dstRef[0]
  enc_ref_frame[dst_col][dst_row][1] = dstRef[1]
}

// Once all the data for a block has been decided, we can try to "propagate"
// the same information to surrounding (smaller) blocks.
// This gives us some control over the eventual ref mv stack length.
enc_propagate_refs(mi_row, mi_col, bsize) {
  bw = mi_size_wide[bsize]
  bh = mi_size_high[bsize]
  partition = enc_block_partition[mi_col][mi_row]
  aU = (mi_row > mi_row_start)
  aL = (mi_col > mi_col_start)
  calculate_mv_search_range(mi_row, mi_col, bw, bh)
  hastr = has_top_right(mi_row, mi_col, bw, bh, partition)

  enc_clear_mv_block_list()

  // Scan the "near" (first) area
  if (Abs(maxRowOffset) >= 1) {
    enc_scan_row(mi_row, mi_col, -1, 0)
  }
  if (Abs(maxColOffset) >= 1) {
    enc_scan_col(mi_row, mi_col, -1, 0)
  }
  if (hastr) {
    enc_scan_block(mi_row, mi_col, -1, bw, 0)
  }

  // Scan the "far" (second) area
  enc_scan_block(mi_row, mi_col, -1, -1, 1)
  for (idx = 2; idx <= MVREF_ROWS; idx++) {
    rowOffset = -(idx << 1) + 1 + rowAdj
    colOffset = -(idx << 1) + 1 + colAdj
    if (Abs(rowOffset) <= Abs(maxRowOffset)) {
      enc_scan_row(mi_row, mi_col, -idx, 1)
    }
    if (Abs(colOffset) <= Abs(maxColOffset)) {
      enc_scan_col(mi_row, mi_col, -idx, 1)
    }
  }

  // Finally, decide how long we'd like the ref mv stack to be after
  // processing the "near" and "far" regions.
  // Note: This doesn't fully determine the ref mv stack size - if multiple
  // blocks end up with the same motion vector(s), then their stack entries
  // will be merged. But we get some control this way.
  uint8 targetRefMvs[2]
  targetRefMvs[0] = Min(enc_same_ref_cnt[0] + enc_modifiable_cnt[0], MAX_PLANNED_REFS)

  if (targetRefMvs[0] + enc_same_ref_cnt[1] >= MAX_PLANNED_REFS) {
    targetRefMvs[1] = enc_same_ref_cnt[1]
  } else {
    targetRefMvs[1] = Min(enc_same_ref_cnt[1] + enc_modifiable_cnt[1], MAX_PLANNED_REFS - targetRefMvs[0])
  }

  uint8 extraRefMvs[2]
  extraRefMvs[0] = targetRefMvs[0] - enc_same_ref_cnt[0]
  extraRefMvs[1] = targetRefMvs[1] - enc_same_ref_cnt[1]

  // Then pick some blocks to modify.
  // For each region, we uniformly select from all choices of 'targetRefMvs[region]'
  // out of 'enc_modifiable_cnt[region]', using Floyd's algorithm:
  // https://stackoverflow.com/a/2394292
  for (region = 0; region <= 1; region++) {
    modifiedBlocks = 0 // Bitset to track which indices we've already modified
    for (i = enc_modifiable_cnt[region] - extraRefMvs[region]; i < enc_modifiable_cnt[region]; i++) {
      idx = Choose(0, i)
      if (modifiedBlocks & (1<<idx)) {
        idx = i
      }

      // So we've picked the block at index 'idx' to be modified
      mvrow = enc_modifiable[region][idx][0]
      mvcol = enc_modifiable[region][idx][1]
      enc_add_refs(mi_row, mi_col, mvrow, mvcol)

      modifiedBlocks |= (1 << idx)
    }
  }
}

uint8 planner_bsize_order[BLOCK_SIZES_ALL] = {
  BLOCK_128X128, BLOCK_128X64, BLOCK_64X128,
    BLOCK_64X64,  BLOCK_64X32,  BLOCK_32X64, BLOCK_64X16, BLOCK_16X64,
    BLOCK_32X32,  BLOCK_32X16,  BLOCK_16X32,  BLOCK_32X8,  BLOCK_8X32,
    BLOCK_16X16,   BLOCK_16X8,   BLOCK_8X16,  BLOCK_16X4,  BLOCK_4X16,
      BLOCK_8X8,    BLOCK_8X4,    BLOCK_4X8,
      BLOCK_4X4
};

// Top level function; called before encoding each tile
enc_plan_tile() {
  // First pass: Decide on the block partitioning scheme and load any
  // parameters (such as segment IDs) which must have fixed values
  enc_partition_idx = 0
  for (mi_row = mi_row_start; mi_row < mi_row_end; mi_row += mib_size) {
    for (mi_col = mi_col_start; mi_col < mi_col_end; mi_col += mib_size) {
      enc_plan_partition(mi_row, mi_col, superblock_size)
    }
  }

  // Second pass: Walk all blocks *in decreasing order of block size*,
  // setting up any variable parameters
  if (superblock_size == BLOCK_128X128) {
    idxStart = 0
  } else {
    idxStart = 3
  }
  for (idx = idxStart; idx < BLOCK_SIZES_ALL; idx++) {
    bsize = planner_bsize_order[idx]
    bw = mi_size_wide[bsize]
    bh = mi_size_high[bsize]
    for (mi_row = mi_row_start; mi_row < mi_row_end; mi_row += bh) {
      for (mi_col = mi_col_start; mi_col < mi_col_end; mi_col += bw) {
        if (enc_sb_sizes[mi_col][mi_row] == bsize) {
          enc_plan_block(mi_row, mi_col, bsize)
          if (enc_ref_frame[mi_col][mi_row][1] != NONE) {
            // For inter blocks, try modifying some surrounding blocks to have
            // similar references
            enc_propagate_refs(mi_row, mi_col, bsize)
          }
        }
      }
    }
  }
}
#else // ENCODE
empty_frame_plan() {}
#endif // ENCODE
