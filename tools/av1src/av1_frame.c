/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_OUTPUT 0
#define VALIDATE_MVS 0
#define VALIDATE_DELTA_Q 0

#define VALIDATE_SPEC_VARTX 0
#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_UPSCALING 1
#else
#define VALIDATE_SPEC_UPSCALING 0
#endif
#define VALIDATE_SPEC_UPSCALING_INTERNALS 0
#define VALIDATE_SPEC_NEWMV 0
#define VALIDATE_SPEC_CDEF_IDX 0

#define DEBUG_BLOCK_MAP 0

decode_init() {
}

tile_init(tileRow, tileCol) {
  // Since this experiment allows non-uniform tile sizes, we explicitly
  // store the tile boundary information in an array in read_tile_info_max_tile(),
  // and just fetch that data here.
  //
  // Instead of storing a separate 'mi_col_ends' array, we add an extra entry
  // to mi_col_ends, with value mi_cols. That way, we logically have
  // mi_col_ends[i] == mi_col_starts[i+1] for all i. The mi_row_starts array
  // is modified similarly.
  mi_row_start = mi_row_starts[tileRow]
  mi_row_end = mi_row_starts[tileRow + 1]
  mi_col_start = mi_col_starts[tileCol]
  mi_col_end = mi_col_starts[tileCol + 1]
}

int get_partition_ctx(mi_row, mi_col, bsize) {
  // We merge in the partition information from all above and left neighbours
  // The partitions are zeroed if unavailable
  boffset = mi_width_log2_lookup[bsize] - mi_width_log2_lookup[BLOCK_8X8]

  // TODO this change may not be necessary
  above = (above_seg_context[mi_col] & (1 << boffset)) > 0
  left = (left_seg_context[(mi_row & mi_mask)] & (1 << boffset)) > 0

  return (left * 2 + above) + boffset * PARTITION_PLOFFSET
}

/* Helper: How many different partition types can we use for a particular block size? */
uint8 num_partition_types(bsize) {
  if (bsize == BLOCK_128X128)
   return EXT_PARTITION_TYPES - 2 // Disallow 4:1 partitions for this case

  if (mi_width_log2_lookup[bsize] > mi_width_log2_lookup[BLOCK_8X8])
    return EXT_PARTITION_TYPES

  return PARTITION_TYPES
}

/* Helpers which calculate the combined probabilities of "horizontal-split-like" vs.
   "not-horizontal-split-like" partitions (and similar for vertical splits) */
uint16 get_cdf_p15(uint16pointer cdf, sym) {
  // Note: Our CDFs are always increasing, in contrast to the reference code where
  // the CDFs are decreasing if EC_SMALLMUL is enabled
  Fl = (sym > 0) ? cdf[sym-1] : 0
  Fh = cdf[sym]
  return (Fh - Fl)
}

get_partition_horzlike_cdf(uint16pointer partitionCdf, uint16pointer outCdf, bsize) {
  psum = get_cdf_p15(partitionCdf, PARTITION_HORZ) +
         get_cdf_p15(partitionCdf, PARTITION_SPLIT)
  psum += get_cdf_p15(partitionCdf, PARTITION_HORZ_A) +
          get_cdf_p15(partitionCdf, PARTITION_HORZ_B) +
          get_cdf_p15(partitionCdf, PARTITION_VERT_A)
  if (bsize != BLOCK_128X128)
    psum += get_cdf_p15(partitionCdf, PARTITION_HORZ_4)
  outCdf[0] = (1<<DAALA_PRECISION) - psum
  outCdf[1] = (1<<DAALA_PRECISION)
  outCdf[2] = 0
}

get_partition_vertlike_cdf(uint16pointer partitionCdf, uint16pointer outCdf, bsize) {
  psum = get_cdf_p15(partitionCdf, PARTITION_VERT) +
         get_cdf_p15(partitionCdf, PARTITION_SPLIT)
  psum += get_cdf_p15(partitionCdf, PARTITION_HORZ_A) +
          get_cdf_p15(partitionCdf, PARTITION_VERT_A) +
          get_cdf_p15(partitionCdf, PARTITION_VERT_B)
  if (bsize != BLOCK_128X128)
    psum += get_cdf_p15(partitionCdf, PARTITION_VERT_4)
  outCdf[0] = (1<<DAALA_PRECISION) - psum
  outCdf[1] = (1<<DAALA_PRECISION)
  outCdf[2] = 0
}

uint_0_10 read_partition(mi_row, mi_col, bsize) {
  uint_0_10 partition
  hbs = mi_size_wide[bsize] / 2
  has_rows = (mi_row + hbs) < mi_rows
  has_cols = (mi_col + hbs) < mi_cols
#if ENCODE
  if (enc_use_frame_planner) {
    // We've already decided on a partition scheme, so just fetch
    // the partition from an array
    ASSERT(enc_partition_idx < MAX_TILE_AREA_MI / 3, "enc_partition_idx out of range")
    partition = enc_partition_symbols[enc_partition_idx]
    enc_partition_idx += 1
  } else {
    // Generate the partition at this point
    partition = encode_partition(mi_row, mi_col, has_rows, has_cols, bsize)
  }
#endif
  partition ae(v)

  return partition
}

read_intra_segment_id(mi_row, mi_col, bsize, skip) {
  if (seg_enabled && seg_update_map) {
    segment_id = read_segment_id(mi_row, mi_col, bsize, skip)
    set_segment_id(mi_row, mi_col, bsize)
#if ENCODE
    if (enc_use_frame_planner && preskip_seg_id) {
      ASSERT(segment_id == enc_segment_id[mi_col][mi_row], "segment_id does not match lookahead value")
    }
#endif
  } else {
    segment_id = 0
  }
  lossless = lossless_array[segment_id]
}

set_segment_id(mi_row, mi_col, bsize) {
  miOffset = mi_row * mi_cols + mi_col
  bw = mi_size_wide[bsize]
  bh = mi_size_high[bsize]
  xmis = Min(mi_cols - mi_col, bw)
  ymis = Min(mi_rows - mi_row, bh)
  for (y = 0; y < ymis; y++) {
    for (x = 0; x < xmis; x++) {
      seg_map[miOffset + y * mi_cols + x] = segment_id
    }
  }
}

// Search block for smallest segment id
uint_0_7 get_segment_id(mi_row, mi_col, bsize, currentFrame) {
  miOffset = mi_row * mi_cols + mi_col
  bw = mi_size_wide[bsize]
  bh = mi_size_high[bsize]
  xmis = Min(mi_cols - mi_col, bw)
  ymis = Min(mi_rows - mi_row, bh)
  seg = 7

  for (y = 0; y < ymis; y++) {
    for (x = 0; x < xmis; x++) {
      if (currentFrame) {
        segId = seg_map[miOffset + y * mi_cols + x]
      } else {
        segId = prev_frame_seg_map[miOffset + y * mi_cols + x]
      }
      seg = Min(seg, segId)
    }
  }

  return seg
}

copy_segment_id(mi_row, mi_col, bsize) {
  miOffset = mi_row * mi_cols + mi_col
  bw = mi_size_wide[bsize]
  bh = mi_size_high[bsize]
  xmis = Min(mi_cols - mi_col, bw)
  ymis = Min(mi_rows - mi_row, bh)

  for (y = 0; y < ymis; y++) {
    for (x = 0; x < xmis; x++) {
      seg_map[miOffset + y * mi_cols + x] = prev_frame_seg_map[miOffset + y * mi_cols + x]
    }
  }
}

int pred_flag_ctx(mi_row, mi_col) {
  return left_pred_flags[mi_row&mi_mask] + above_pred_flags[mi_col]
}

set_pred_flag_seg_id(mi_row, mi_col, bsize, pred_flag) {
  bw = mi_size_wide[bsize]
  bh = mi_size_high[bsize]
  xmis = Min(mi_cols - mi_col, bw)
  ymis = Min(mi_rows - mi_row, bh)

  for (y = 0; y < ymis; y++)
    left_pred_flags[(mi_row+y)&mi_mask] = pred_flag

  for (x = 0; x < xmis; x++)
    above_pred_flags[mi_col+x] = pred_flag

}

read_inter_segment_id(mi_row, mi_col, bsize, preSkip) {
  prevSegmentId = segment_id
  segment_id = 0
  if (seg_enabled) {
    pred_segment_id = get_segment_id(mi_row, mi_col, bsize, 0)
    if (!seg_update_map) {
      copy_segment_id(mi_row, mi_col, bsize)
      segment_id = pred_segment_id
    } else {
      if (preSkip && !preskip_seg_id) {
        segment_id = 0
      } else if (!preSkip && preskip_seg_id) {
          segment_id = prevSegmentId
      } else if (!preSkip && skip_coeff) {
        if (seg_temporal_update) {
          set_pred_flag_seg_id(mi_row, mi_col, bsize, 0)
        }
        segment_id = read_segment_id(mi_row, mi_col, bsize, 1)
        set_segment_id(mi_row, mi_col, bsize)
      } else
      if (seg_temporal_update) {
        uint1 pred_flag
        pred_flag ae(v)
        set_pred_flag_seg_id(mi_row, mi_col, bsize, pred_flag)
        if (pred_flag) {
          segment_id = pred_segment_id
        } else {
          segment_id = read_segment_id(mi_row, mi_col, bsize, 0)
        }
      } else {
        segment_id = read_segment_id(mi_row, mi_col, bsize, 0)
      }
      set_segment_id(mi_row, mi_col, bsize)
    }
  }
  lossless = lossless_array[segment_id]
}

can_use_skip_mode(segId, bsize) {
  if (intra_only) return 0

  if (!skip_mode_allowed) return 0

  if (preskip_seg_id) {
    ASSERT(segId != -1, "Bad segment ID")
    if (segfeature_active_idx(segId, SEG_LVL_SKIP)) return 0

    if (segfeature_active_idx(segId, SEG_LVL_REF_FRAME) ||
        segfeature_active_idx(segId, SEG_LVL_GLOBALMV))
      return 0
  }

  if (Min(block_size_wide_lookup[bsize], block_size_high_lookup[bsize]) < 8)
    return 0

  return 1
}

int skip_coeff_ctx(mi_row, mi_col) {
  ctx = 0
  if (aU)
    ctx += above_skips[mi_col]
  if (aL)
    ctx += left_skips[mi_row&mi_mask]
  return ctx
}

int skip_mode_ctx(mi_row, mi_col) {
  ctx = 0
  if (aU)
    ctx += above_skip_mode[mi_col]
  if (aL)
    ctx += left_skip_mode[mi_row&mi_mask]
  return ctx
}

read_skip_coeff(mi_row,mi_col) {
  segId = preskip_seg_id ? segment_id : -1 // Send an obviously bad value if the segment ID hasn't been read yet
  if (can_use_skip_mode(segId, sb_size)) {
    skip_mode ae(v)
  } else {
    skip_mode = 0
  }
#if ENCODE
  if (enc_use_frame_planner && !intra_only)
    CHECK(skip_mode == enc_skip_mode[mi_col][mi_row], "skip_mode does not match lookahead value")
#endif

  if (skip_mode) {
    skip_coeff = 1
  } else
  if (preskip_seg_id && segfeature_active(SEG_LVL_SKIP))
  {
    skip_coeff = 1
  } else {
    skip_coeff ae(v)
  }
}

uint_0_2 pick_spatial_seg_cdf(int prevUL, int prevU, int prevL) {
  if (prevUL < 0 || prevU < 0 || prevL < 0) { /* Edge case */
    return 0
  }
  if ((prevUL == prevU) && (prevUL == prevL)) {
    return 2
  }
  if ((prevUL == prevU) || (prevUL == prevL) || (prevU == prevL)) {
    return 1
  }
  return 0
}

uint_0_7 pick_spatial_seg_pred(int prevUL, int prevU, int prevL) {
  if (prevU == -1) { /* Edge case */
    return (prevL == -1) ? 0 : prevL
  }
  if (prevL == -1) { /* Edge case */
    return prevU
  }
  return (prevUL == prevU) ? prevU : prevL
}

#if ENCODE
uint_0_7 neg_interleave(uint_0_7 x, uint_0_7 ref, uint_0_8 max) {
  ASSERT(x < max, "Input to neg_interleave is out of range")
  diff = x - ref
  if (!ref) return x
  if (ref == (max - 1)) return (-x + max - 1)
  if (2 * ref < max) {
    if (Abs(diff) <= ref) {
      if (diff > 0)
        return (diff << 1) - 1
      else
        return ((-diff) << 1)
    }
    return x
  } else {
    if (Abs(diff) < (max - ref)) {
      if (diff > 0)
        return (diff << 1) - 1
      else
        return ((-diff) << 1)
    }
    return (max - x) - 1
  }
}
#endif // ENCODE

uint_0_7 neg_deinterleave(uint_0_7 diff, uint_0_7 ref, uint_0_8 max) {
  if (!ref) {
    return diff
  }
  if (ref >= (max - 1)) {
    return max - diff - 1
  }
  if (2 * ref < max) {
    if (diff <= 2 * ref) {
      if (diff & 1) {
        return ref + ((diff + 1) >> 1)
      }
      return ref - (diff >> 1)
    }
    return diff
  }

  if (diff <= 2 * (max - ref - 1)) {
    if (diff & 1) {
      return ref + ((diff + 1) >> 1)
    }
    return ref - (diff >> 1)
  }
  return max - (diff + 1)
}

uint_0_7 read_segment_id(mi_row, mi_col, bsize, skip) {
  prevUL = -1
  prevL = -1
  prevU = -1

  above = (mi_row > mi_row_start)
  left = (mi_col > mi_col_start)
  if (above && left) {
    prevUL = get_segment_id(mi_row - 1, mi_col - 1, BLOCK_4X4, 1)
  }

  if (above) {
    prevU = get_segment_id(mi_row - 1, mi_col, BLOCK_4X4, 1)
  }

  if (left) {
    prevL = get_segment_id(mi_row, mi_col - 1, BLOCK_4X4, 1)
  }

  cdfNum = pick_spatial_seg_cdf(prevUL, prevU, prevL)
  pred = pick_spatial_seg_pred(prevUL, prevU, prevL)

  if (skip) {
    segmentId = pred
  } else {
    coded_id ae(v)
    segmentId = neg_deinterleave(coded_id, pred, last_active_seg_id + 1)
    ASSERT(segmentId >= 0 && segmentId <= last_active_seg_id, "Invalid segmentId")
  }
  ASSERT(segmentId >= 0 && segmentId <= 7, "segmentId out of range")
  return segmentId
}

frame_lf_count() {
  if (get_num_planes() > 1)
    return FRAME_LF_COUNT
  else
    return FRAME_LF_COUNT - 2
}

read_delta_qindex(mi_row, mi_col, bsize) {
  if ( delta_q_present ) {
    reducedDeltaQIndex = 0
    readDeltaQ = ((mi_col & mi_mask) == 0 && (mi_row & mi_mask) == 0) ? 1 : 0
    if ( ( ( bsize != superblock_size ) || !skip_coeff ) && readDeltaQ ) {
      abs ae(v)
      CHECK(abs <= DELTA_Q_SMALL, "Abs out of range")
      if ( abs == DELTA_Q_SMALL ) {
        rem_bits                                 ae(v)
        rem_bits += 1
        abs_bits                                 ae(v)
        abs = abs_bits + (1 << rem_bits) + 1
#if ENCODE
        // We just wrote out the value of "abs_bits" one bit at a time
        // using rem_bits bits. The binarization code for abs_bits
        // doesn't check that this didn't overflow, so we should
        // probably check that here.
        CHECK ((abs_bits >> rem_bits) == 0,
               "Cannot encode delta_q_abs_bits = %d with %d bits.",
               abs_bits, rem_bits)
#endif
      }
      if (abs) {
        delta_q_sign_bit ae(v)
        reducedDeltaQIndex = delta_q_sign_bit ? ( abs * -1 ) : abs
      }
    }
    current_qindex = prev_qindex + reducedDeltaQIndex * delta_q_res
    current_qindex = clamp(current_qindex, 1, MAXQ)
#if VALIDATE_DELTA_Q
    validate(4235)
    validate(prev_qindex)
    validate(delta_q_res)
    validate(reducedDeltaQIndex)
    validate(current_qindex)
#endif
    prev_qindex = current_qindex

    if (delta_lf_present) {
      if (delta_lf_multi) {
        lfCount = frame_lf_count()
        for (i = 0; i < lfCount; i++) {
            reducedDeltaLfLevel = read_delta_lflevel(bsize, readDeltaQ, i, lfCount)
          current_delta_lf_multi[i] = prev_delta_lf_multi[i] + reducedDeltaLfLevel * delta_lf_res
          current_delta_lf_multi[i] = clamp(current_delta_lf_multi[i], -MAX_LOOP_FILTER, MAX_LOOP_FILTER)
          prev_delta_lf_multi[i] = current_delta_lf_multi[i]
        }
      } else {
        reducedDeltaLfLevel = read_delta_lflevel(bsize, readDeltaQ, -1, 1)
        current_delta_lf_from_base = prev_delta_lf_from_base + reducedDeltaLfLevel * delta_lf_res
        current_delta_lf_from_base = clamp(current_delta_lf_from_base, -MAX_LOOP_FILTER, MAX_LOOP_FILTER)
        prev_delta_lf_from_base = current_delta_lf_from_base
      }
    }
  }
}

read_delta_lflevel(bsize, readDeltaQ, deltaLfCdfIdx, lfCount) {
  reducedDeltaLfLevel = 0
  if ( ( ( bsize != superblock_size ) || !skip_coeff ) && readDeltaQ ) {
   abs_lf ae(v)
   abs_read = abs_lf

   CHECK(abs_lf <= DELTA_LF_SMALL, "Abs_lf out of range")
   if ( abs_lf == DELTA_LF_SMALL ) {
      rem_bits_lf ae(v)
      rem_bits_lf += 1
      abs_bits_lf ae(v)
      abs_lf = abs_bits_lf + (1 << rem_bits_lf) + 1
    }
    if (abs_lf) {
      delta_lf_sign_bit ae(v)
    } else {
      delta_lf_sign_bit = 1
    }
    reducedDeltaLfLevel = delta_lf_sign_bit ? ( abs_lf * -1 ) : abs_lf
  }
  return reducedDeltaLfLevel
}

uint_0_5 bsize_to_tx_size_cat(bsize, isInter) {
  txSz = max_txsize_rect_lookup[bsize]
  depth = 0
  while (txSz != TX_4X4) {
    depth++
    txSz = sub_tx_size_map[isInter][txSz]
  }
  return depth - 1
}

uint8 tx_size_from_tx_mode(bsize) {
  largest_tx_size = tx_mode_to_biggest_tx_size[tx_mode]
  max_rect_tx_size = max_txsize_rect_lookup[bsize]
  max_square_tx_size = max_txsize_lookup[bsize]

  if (bsize == BLOCK_4X4)
    return Min(max_square_tx_size, largest_tx_size)
  if (txsize_sqr_map[max_rect_tx_size] <= largest_tx_size)
    return max_rect_tx_size
  else
    return largest_tx_size
}

int tx_depth_ctx(mi_row, mi_col, max_tx_size) {
  maxTxW = tx_size_wide[max_tx_size]
  maxTxH = tx_size_high[max_tx_size]

  if (aU && is_inters[mi_col][mi_row-1]) {
    aboveW = block_size_wide_lookup[sb_sizes[mi_col][mi_row-1]]
  } else if (aU) {
    aboveW = above_txfm_context[mi_col]
  } else {
    aboveW = 0 // Pick a value which is definitely < maxTxW
  }

  if (aL && is_inters[mi_col-1][mi_row]) {
    leftH = block_size_high_lookup[sb_sizes[mi_col-1][mi_row]]
  } else if (aL) {
    leftH = left_txfm_context[mi_row & mi_mask]
  } else {
    leftH = 0 // Pick a value which is definitely < maxTxH
  }

  return (aboveW >= maxTxW) + (leftH >= maxTxH)
}

read_tx_size(mi_row,mi_col, bsize, allow_select) {
  if (lossless) {
    tx_size = TX_4X4
    return 0 // unused return value
  }
  max_tx_size = max_txsize_rect_lookup[bsize] // Used to determine context
  // Allow encoding "oversized" square transforms for block sizes which allow
  // rectangular transforms. If such a transform is signalled, that means
  // to use a full-block-sized rectangular transform instead.
  maxTxDepth = bsize_to_tx_size_cat(bsize, is_inter)
  depthCdfSize = Min(maxTxDepth + 1, MAX_TX_DEPTH) + 1

  if (bsize > BLOCK_4X4) { // block_signals_txsize
    if (allow_select && tx_mode == TX_MODE_SELECT) {
      txDepth ae(v)
      tx_size = max_txsize_rect_lookup[bsize]
      for (i = 0; i < txDepth; i++)
        tx_size = sub_tx_size_map[is_inter][tx_size]
    } else {
      tx_size = tx_size_from_tx_mode(bsize)
    }
  } else {
    tx_size = max_txsize_rect_lookup[bsize]
  }
}

set_txfm_ctxs(mi_row, mi_col, bsize, txw, txh) {
#if VALIDATE_SPEC_VARTX
  validate(130000)
  validate(txw)
  validate(txh)
#endif
  w = mi_size_wide[bsize] << TX_UNIT_WIDE_LOG2
  h = mi_size_high[bsize] << TX_UNIT_WIDE_LOG2
  base_x = mi_col << TX_UNIT_WIDE_LOG2
  base_y = (mi_row & mi_mask) << TX_UNIT_HIGH_LOG2
  for (i = 0; i < w; i++)
    above_txfm_context[base_x + i] = txw
  for (i = 0; i < h; i++)
    left_txfm_context[base_y + i] = txh
}

int get_sqr_tx_size(tx_dim) {
  if (tx_dim > 32)
    return TX_64X64
  else if (tx_dim == 32)
    return TX_32X32
  else if (tx_dim == 16)
    return TX_16X16
  else if (tx_dim == 8)
    return TX_8X8
  else
    return TX_4X4
}

int txfm_partition_context(mi_row, mi_col, blocky, blockx, bsize, txSz) {
  ctx_row = ((mi_row & mi_mask) << TX_UNIT_WIDE_LOG2) + blocky
  ctx_col = (mi_col << TX_UNIT_WIDE_LOG2) + blockx
  txw = tx_size_wide[txSz]
  txh = tx_size_high[txSz]
  above = above_txfm_context[ctx_col] < txw
  left = left_txfm_context[ctx_row] < txh
  category = TXFM_PARTITION_CONTEXTS

  // dummy return, not used by others.
  if (txSz <= TX_4X4)
    return 0

  maxTxSz = get_sqr_tx_size(Max(block_size_wide_lookup[bsize], block_size_high_lookup[bsize]))

  if (maxTxSz >= TX_8X8) {
    txUp = txsize_sqr_up_map[txSz]
    category = (txUp != maxTxSz && maxTxSz > TX_8X8) +
               (TX_SIZES - 1 - maxTxSz) * 2
  }
  if (category == TXFM_PARTITION_CONTEXTS)
    return category
  else
    return category * 3 + above + left
}

txfm_partition_update(mi_row, mi_col, blocky, blockx, txSz, txb_size) {
  bsize = txsize_to_bsize[txb_size]
  bh = mi_size_high[bsize] << TX_UNIT_HIGH_LOG2
  bw = mi_size_wide[bsize] << TX_UNIT_WIDE_LOG2
  txw = tx_size_wide[txSz]
  txh = tx_size_high[txSz]
  start_row = ((mi_row & mi_mask) << TX_UNIT_WIDE_LOG2) + blocky
  start_col = (mi_col << TX_UNIT_WIDE_LOG2) + blockx

  for (i = 0; i < bw; i++)
    above_txfm_context[start_col + i] = txw
  for (i = 0; i < bh; i++)
    left_txfm_context[start_row + i] = txh
}

read_tx_size_vartx(mi_row, mi_col, max_units_wide, max_units_high, blocky, blockx, txSz, depth) {
  if (blocky >= max_units_high || blockx >= max_units_wide)
    return 0

  tx_row = mi_row + (blocky >> TX_UNIT_HIGH_LOG2)
  tx_col = mi_col + (blockx >> TX_UNIT_WIDE_LOG2)
  ctx = txfm_partition_context(mi_row, mi_col, blocky, blockx, sb_size, txSz)
  ASSERT(txSz > TX_4X4, "Can't split 4x4 partitions any further")

  if (depth == MAX_VARTX_DEPTH) {
    txfm_split = 0
  } else {
    txfm_split ae(v)
  }

  if (txfm_split) {
    sub_txs = sub_tx_size_map[1][txSz]
    if (sub_txs == TX_4X4) {
      w = tx_size_wide_unit[txSz] >> TX_UNIT_WIDE_LOG2
      h = tx_size_high_unit[txSz] >> TX_UNIT_HIGH_LOG2
      for (idy = 0; idy < clamp(h, 1, mi_rows - tx_row); idy++)
        for (idx = 0; idx < clamp(w, 1, mi_cols - tx_col); idx++)
          inter_tx_size[tx_col + idx][tx_row + idy] = sub_txs
      tx_size = sub_txs
      min_tx_size = tx_size
      txfm_partition_update(mi_row, mi_col, blocky, blockx, sub_txs, txSz)
    } else {
      txw = tx_size_wide_unit[txSz]
      txh = tx_size_high_unit[txSz]
      bsw = tx_size_wide_unit[sub_txs]
      bsh = tx_size_high_unit[sub_txs]
      for (i = 0; i < txh; i += bsh)
        for (j = 0; j < txw; j += bsw) {
          new_blocky = blocky + i
          new_blockx = blockx + j
          read_tx_size_vartx(mi_row, mi_col, max_units_wide, max_units_high, new_blocky, new_blockx, sub_txs, depth+1)
        }
    }
  } else {
    w = tx_size_wide_unit[txSz] >> TX_UNIT_WIDE_LOG2
    h = tx_size_high_unit[txSz] >> TX_UNIT_HIGH_LOG2
    for (idy = 0; idy < clamp(h, 1, mi_rows - tx_row); idy++)
      for (idx = 0; idx < clamp(w, 1, mi_cols - tx_col); idx++)
        inter_tx_size[tx_col + idx][tx_row + idy] = txSz
    tx_size = txSz
    min_tx_size = (tx_size_2d[min_tx_size] <= tx_size_2d[txSz]) ? min_tx_size : txSz
    txfm_partition_update(mi_row, mi_col, blocky, blockx, txSz, txSz)
  }
}

read_inter_tx_size(mi_row,mi_col, bsize, skip, isInter) {
  if (tx_mode == TX_MODE_SELECT &&
      bsize > BLOCK_4X4 &&
      isInter && !skip && !lossless) {
    maxTxSz = max_txsize_rect_lookup[bsize]
    bh = tx_size_high_unit[maxTxSz]
    bw = tx_size_wide_unit[maxTxSz]
    width = block_size_wide_lookup[bsize] >> tx_size_wide_log2[0]
    height = block_size_high_lookup[bsize] >> tx_size_wide_log2[0]
    min_tx_size = TX_SIZES_ALL
    pxToRightEdge = (mi_cols - mi_col) * MI_SIZE
    pxToBottomEdge = (mi_rows - mi_row) * MI_SIZE
    maxUnitsWide = Min(pxToRightEdge, block_size_wide_lookup[sb_size]) >> tx_size_wide_log2[0]
    maxUnitsHigh = Min(pxToBottomEdge, block_size_high_lookup[sb_size]) >> tx_size_wide_log2[0]
    initial_depth = 0
    for (idy = 0; idy < height; idy += bh)
      for (idx = 0; idx < width; idx += bw)
        read_tx_size_vartx(mi_row, mi_col, maxUnitsWide, maxUnitsHigh, idy, idx, maxTxSz, initial_depth)
  } else {
    read_tx_size(mi_row, mi_col, bsize, !isInter || !skip)
    if (isInter) {
      for (y = 0; y < Min(mi_size_high[bsize], mi_rows - mi_row); y++)
        for (x = 0; x < Min(mi_size_wide[bsize], mi_cols - mi_col); x++)
          inter_tx_size[mi_col + x][mi_row + y] = tx_size
    }
    min_tx_size = tx_size
    if (skip && is_inter)
      set_txfm_ctxs(mi_row, mi_col, bsize, block_size_wide_lookup[bsize], block_size_high_lookup[bsize])
    else
      set_txfm_ctxs(mi_row, mi_col, bsize, tx_size_wide[tx_size], tx_size_high[tx_size])
  }
}

block_size_high( mi_row, mi_col, bsize, plane ) {
  planeBsize = get_plane_block_size(bsize, plane)
  planeBlockHeight = block_size_high_lookup[planeBsize]
  px_to_bottom_edge = (mi_rows - (mi_row + mi_size_high[bsize])) * MI_SIZE >> plane_subsampling_y[plane]
  px_to_bottom_edge = Min(0, px_to_bottom_edge) // Only allow reducing blockHeight, not increasing it
  blockRows = px_to_bottom_edge + planeBlockHeight
  CHECK(blockRows <= planeBlockHeight, "Arithmetic errors: blockRows")
  return blockRows
}

block_size_wide( mi_row, mi_col, bsize, plane ) {
  planeBsize = get_plane_block_size(bsize, plane)
  planeBlockWidth = block_size_wide_lookup[planeBsize]
  px_to_right_edge = (mi_cols - (mi_col + mi_size_wide[bsize])) * MI_SIZE >> plane_subsampling_x[plane]
  px_to_right_edge = Min(0, px_to_right_edge) // Only allow reducing blockWidth, not increasing it
  blockCols = px_to_right_edge + planeBlockWidth
  CHECK(blockCols <= planeBlockWidth, "Arithmetic errors: blockCols")
  return blockCols
}

uint_0_16 get_palette_cache(miRow, miCol, plane, uint16pointer cache) {
  aboveN = 0
  // Do not refer to above SB row when on SB boundary.
  if (((miRow * MI_SIZE) % MIN_SB_SIZE) && aU) {
    aboveN = PaletteSizes[plane_type[plane]][ miCol ][ miRow - 1 ]
  }
  leftN = 0
  if (aL) {
    leftN = PaletteSizes[plane_type[plane]][ miCol - 1 ][ miRow ]
  }
  if (aboveN == 0 && leftN == 0) {
    return 0
  }

  aboveIdx = 0
  leftIdx = 0
  n = 0
  // Merge the sorted lists of base colors from above and left to get
  // combined sorted color cache.
  while (aboveN > 0 && leftN > 0) {
    aboveC = PaletteColors[plane][miCol][miRow-1][aboveIdx]
    leftC = PaletteColors[plane][miCol-1][miRow][leftIdx]
    if (leftC < aboveC) {
      if (n == 0 || leftC != cache[n - 1]) {
        cache[n] = leftC
        n++
      }
      leftIdx++
      leftN--
    } else {
      if (n == 0 || aboveC != cache[n - 1]) {
        cache[n] = aboveC
        n++
      }
      aboveIdx++
      aboveN--
    }
  }
  for (; aboveN > 0; aboveN--) {
    val = PaletteColors[plane][miCol][miRow-1][aboveIdx]
    aboveIdx++
    if (n == 0 || val != cache[n - 1]) {
      cache[n] = val
      n++
    }
  }
  for (; leftN > 0; leftN--) {
    val = PaletteColors[plane][miCol-1][miRow][leftIdx]
    leftIdx++
    if (n == 0 || val != cache[n - 1]) {
      cache[n] = val
      n++
    }
  }
  ASSERT(n <= 2 * PALETTE_MAX_SIZE, "Invalid palette cache size")
  return n
}

int av1_ceil_log2(int n) {
  if (n < 2)
    return 0

  i = 1
  j = 2
  while (j < n) {
    i++
    j = j << 1
  }
  return i
}

// Hoare partition scheme
int qsort_partition(uint12pointer arr, first, last) {
  pivot = arr[first]
  i = first - 1
  j = last + 1
  while (1) {
    for (i++; arr[i] < pivot; i++) {}
    for (j--; arr[j] > pivot; j--) {}
    if (i >= j) {
      return j
    }
    tmp = arr[i]
    arr[i] = arr[j]
    arr[j] = tmp
  }
}

qsort(uint12pointer arr, first, last) {
  if (first < last) {
    pivot = qsort_partition(arr, first, last)
    qsort(arr, first, pivot)
    qsort(arr, pivot+1, last)
  }
}

read_palette_colors_y(miRow, miCol) {
  idx = 0
  for (i = 0; i < palette_cache_len[0] && idx < PaletteSizeY; i++) {
    use_palette_color_cache_y ae(v)
    if (use_palette_color_cache_y) {
      palette_colors_y[idx] = palette_cache[0][i]
      idx++
    }
  }
  if (idx < PaletteSizeY) {
    palette_colors_y[idx] ae(v)
    idx++
    if (idx < PaletteSizeY) {
      minBits = bit_depth - 3
      palette_num_extra_bits_y ae(v)
      paletteBits = minBits + palette_num_extra_bits_y
      range = (1 << bit_depth) - palette_colors_y[idx-1] - 1
      for (; idx < PaletteSizeY; idx++) {
        ASSERT(range >= 0, "Range must be non-negative")
        palette_delta_y ae(v)
        palette_delta_y++
        palette_colors_y[idx] = Clip3(0, (1<<bit_depth)-1, palette_colors_y[idx-1] + palette_delta_y)
        range -= (palette_colors_y[idx] - palette_colors_y[idx-1])
        paletteBits = Min(paletteBits, av1_ceil_log2(range))
      }
    }
  }
  qsort(palette_colors_y, 0, PaletteSizeY-1)
}

read_palette_colors_uv(miRow, miCol) {
  // U channel colors.
  idx = 0
  for (i = 0; i < palette_cache_len[1] && idx < PaletteSizeUV; i++) {
    use_palette_color_cache_u ae(v)
    if (use_palette_color_cache_u) {
      palette_colors_u[idx] = palette_cache[1][i]
      idx++
    }
  }
  if (idx < PaletteSizeUV) {
    palette_colors_u[idx] ae(v)
    idx++
    if (idx < PaletteSizeUV) {
      minBits = bit_depth - 3
      palette_num_extra_bits_u ae(v)
      paletteBits = minBits + palette_num_extra_bits_u
      range = (1 << bit_depth) - palette_colors_u[idx-1]
      for (; idx < PaletteSizeUV; idx++) {
        ASSERT(range >= 0, "Range must be non-negative")
        palette_delta_u ae(v)
        palette_colors_u[idx] = Clip3(0, (1<<bit_depth)-1, palette_colors_u[idx-1] + palette_delta_u)
        range -= (palette_colors_u[idx] - palette_colors_u[idx-1])
        paletteBits = Min(paletteBits, av1_ceil_log2(range))
      }
    }
  }
  qsort(palette_colors_u, 0, PaletteSizeUV-1)

  // V channel colors.
  delta_encode_palette_colors_v ae(v)
  if (delta_encode_palette_colors_v) {
    minBits = bit_depth - 4
    maxVal = 1 << bit_depth
    palette_num_extra_bits_v ae(v)
    paletteBits = minBits + palette_num_extra_bits_v

    // Set idx to zero here so that config overrides can see which entry we're
    // writing to.
    idx = 0
    palette_colors_v[0] ae(v)

    for (idx = 1; idx < PaletteSizeUV; idx++) {
      palette_delta_v ae(v)
      if (palette_delta_v) {
        palette_delta_sign_bit_v ae(v)
        if (palette_delta_sign_bit_v) {
          palette_delta_v = -palette_delta_v
        }
      }
      val = palette_colors_v[idx-1] + palette_delta_v
      if (val < 0) {
        val += maxVal
      }
      if (val >= maxVal) {
        val -= maxVal
      }
      palette_colors_v[idx] = Clip3(0,(1<<bit_depth)-1,val)
    }
  } else {
    for (idx = 0; idx < PaletteSizeUV; idx++) {
      palette_colors_v[idx] ae(v)
    }
  }
}

int palette_y_ctx(miRow, miCol) {
  ctx = 0
  if ( aU && ( PaletteSizes[0][ miCol ][ miRow - 1 ] > 0 ) )
    ctx += 1
  if ( aL && ( PaletteSizes[0][ miCol - 1 ][ miRow ] > 0 ))
    ctx += 1
  return ctx
}

read_palette_mode_info(miRow, miCol) {
  bsizeCtx = num_pels_log2_lookup[sb_size] - num_pels_log2_lookup[PALETTE_MIN_BSIZE]
  if ( y_mode == DC_PRED ) {
    has_palette_y  ae(v)
    if ( has_palette_y ) {
      // Calculate the palette cache early so the encoder can use it
      // when deciding what to set palette_size_y to
      palette_cache_len[0] = get_palette_cache(miRow, miCol, 0, palette_cache[0])
#if ENCODE
      enc_pick_palette_data(0)
#endif
      palette_size_y  ae(v)
      PaletteSizeY = palette_size_y + 2
      read_palette_colors_y(miRow, miCol)
    }
  }
  if (get_num_planes() > 1 &&
      uv_mode == DC_PRED &&
      is_chroma_reference(miRow, miCol, sb_size, 1)) {
    has_palette_uv  ae(v)
    if ( has_palette_uv ) {
      palette_cache_len[1] = get_palette_cache(miRow, miCol, 1, palette_cache[1])
#if ENCODE
      enc_pick_palette_data(1)
#endif
      palette_size_uv  ae(v)
      PaletteSizeUV = palette_size_uv + 2
      read_palette_colors_uv(miRow, miCol)
    }
  }
}

uint32 read_color_map(plane,r,c) {
  return ((plane==0) ? ColorMapY[r][c] : ColorMapUV[r][c])
}

get_palette_color_context( plane, r, c, n ) {
  uint32 max_score
  uint32 scores[PALETTE_COLORS]
  for( i = 0; i < PALETTE_COLORS; i++ ) {
    scores[ i ] = 0
  }
  for( i = 0; i < PALETTE_COLORS; i++ ) {
    ColorOrder[i] = i
  }
  if ( c > 0 ) {
    neighbor = read_color_map(plane, r, c - 1 )
    scores[ neighbor ] += 2
  }
  if ( ( r > 0 ) && ( c > 0 ) ) {
    neighbor = read_color_map(plane, r - 1, c - 1 )
    scores[ neighbor ] += 1
  }
  if ( r > 0 ) {
    neighbor = read_color_map(plane, r - 1, c )
    scores[ neighbor ] += 2
  }
  for ( i = 0; i < PALETTE_NUM_NEIGHBORS; i++ ) {
    max_score = scores[ i ]
    max_idx = i
    for ( j = i + 1; j < n; j++ ) {
      if (scores[ j ] > max_score) {
        max_score = scores[ j ]
        max_idx = j
      }
    }
    if ( max_idx != i ) {
      max_score = scores[ max_idx ]
      maxColorOrder = ColorOrder[ max_idx ]
      for ( k = max_idx; k > i; k-- ) {
        scores[ k ] = scores[ k - 1 ]
        ColorOrder[ k ] = ColorOrder[ k - 1 ]
      }
      scores[ i ] = max_score
      ColorOrder[i] = maxColorOrder
    }
  }
  CumulatedScore = 0
  for( i = 0; i < PALETTE_NUM_NEIGHBORS; i++ ) {
    CumulatedScore += scores[ i ] * palette_color_hash_multipliers[ i ]
  }
  CHECK(CumulatedScore > 0, "Palette CumulatedScore error 1")
  CHECK(CumulatedScore <= PALETTE_MAX_COLOR_CONTEXT_HASH, "Palette CumulatedScore error 2")
}

palette_tokens( mi_row, mi_col, bsize ) {
  blockRows = block_size_high( mi_row, mi_col, bsize, 0 )
  blockCols = block_size_wide( mi_row, mi_col, bsize, 0 )
  blockHeight = block_size_high_lookup[ bsize ]
  blockWidth = block_size_wide_lookup[ bsize ]

#if ENCODE
  color_index_map_y = 0
  if (PaletteSizeY) {
    color_index_map_y u(0)
  }
  CHECK (0 <= color_index_map_y && color_index_map_y <= PaletteSizeY,
         "color_index_map_y is %d, but PaletteSizeY is %d.",
         color_index_map_y, PaletteSizeY)
  encode_quniform_ec(PaletteSizeY, color_index_map_y)
#else
  color_index_map_y = decode_quniform_ec(PaletteSizeY)
#endif
  ColorMapY[0][0] = color_index_map_y

  if ( PaletteSizeY ) {
    for ( i = 1; i < blockRows + blockCols - 1; i++ ) {
      for ( j = Min( i, blockCols - 1 ); j >= Max( 0, i - blockRows + 1 ); j-- ) {
        get_palette_color_context( 0, ( i - j ), j, PaletteSizeY )
        color_idx_y ae(v)
        CHECK(color_idx_y >= 0 && color_idx_y < PaletteSizeY,
              "Invalid color_idx_y of %d; PaletteSizeY is %d.",
              color_idx_y, PaletteSizeY)
        ColorMapY[ i - j ][ j ] = ColorOrder[ color_idx_y ]
      }
    }
    // Copy last column to extra columns
    for ( i = 0; i < blockRows; i++ ) {
      for ( j = blockCols; j < blockWidth; j++ ) {
        ColorMapY[ i ][ j ] = ColorMapY[ i ][ blockCols - 1 ]
      }
    }
    // Copy last row to extra rows
    for ( i = blockRows; i < blockHeight; i++ ) {
      for ( j = 0; j < blockWidth; j++ ) {
        ColorMapY[ i ][ j ] = ColorMapY[ blockRows - 1 ][ j ]
      }
    }
  }

#if ENCODE
  color_index_map_uv = 0
  if (PaletteSizeUV) {
    color_index_map_uv u(0)
  }
  CHECK (0 <= color_index_map_uv && color_index_map_uv <= PaletteSizeUV,
         "color_index_map_uv is %d, but PaletteSizeUV is %d.",
         color_index_map_uv, PaletteSizeUV)
  encode_quniform_ec(PaletteSizeUV, color_index_map_uv)
#else
  color_index_map_uv = decode_quniform_ec(PaletteSizeUV)
#endif
  ColorMapUV[0][0] = color_index_map_uv

  if ( PaletteSizeUV ) {
    blockHeight = blockHeight >> subsampling_y
    blockWidth = blockWidth >> subsampling_x
    blockRows = blockRows >> subsampling_y
    blockCols = blockCols >> subsampling_x
    if (blockWidth < 4) {
      blockWidth += 2
      blockCols += 2
    }
    if (blockHeight < 4) {
      blockHeight += 2
      blockRows += 2
    }
    for ( i = 1; i < blockRows + blockCols - 1; i++ ) {
      for ( j = Min( i, blockCols - 1 ); j >= Max( 0, i - blockRows + 1 ); j-- ) {
        get_palette_color_context( 1, ( i - j ), j, PaletteSizeUV )
        color_idx_uv ae(v)
        CHECK(color_idx_uv >= 0 && color_idx_uv < PaletteSizeUV,
              "Invalid color_idx_uv of %d; PaletteSizeUV is %d.",
              color_idx_uv, PaletteSizeUV)
        ColorMapUV[ i - j ][ j ] = ColorOrder[ color_idx_uv ]
      }
    }
    // Copy last column to extra columns
    for ( i = 0; i < blockRows; i++ ) {
      for ( j = blockCols; j < blockWidth; j++ ) {
        ColorMapUV[ i ][ j ] = ColorMapUV[ i ][ blockCols - 1 ]
      }
    }
    // Copy last row to extra rows
    for ( i = blockRows; i < blockHeight; i++ ) {
      for ( j = 0; j < blockWidth; j++ ) {
        ColorMapUV[ i ][ j ] = ColorMapUV[ blockRows - 1 ][ j ]
      }
    }
  }
}

read_filter_intra_mode_info() {
  if (enable_filter_intra
      && y_mode == DC_PRED
      && PaletteSizeY == 0
      && block_size_wide_lookup[sb_size] <= 32
      && block_size_high_lookup[sb_size] <= 32) {
    use_filter_intra ae(v)
    if ( use_filter_intra ) {
      filter_intra_mode ae(v)
    }
  } else {
    use_filter_intra = 0
  }
}


is_directional_mode( mode, bsize ) {
  if (mode >= V_PRED && mode <= D67_PRED) {
    return 1
  }
  return 0
}

read_intra_angle_info_y(mi_row, mi_col) {
  AngleDeltaY = 0

  if ( sb_size >= BLOCK_8X8 ) {
    if ( is_directional_mode( y_mode, sb_size ) ) {
      angle_delta_y ae(v)
      AngleDeltaY = angle_delta_y - MAX_ANGLE_DELTA
    }
  }
}

read_intra_angle_info_uv(mi_row, mi_col) {
  AngleDeltaUV = 0

  if ( sb_size >= BLOCK_8X8 ) {
    if ( is_directional_mode( uv_mode, sb_size ) ) {
      angle_delta_uv ae(v)
      AngleDeltaUV = angle_delta_uv - MAX_ANGLE_DELTA
    }
  }
}

set_default_interp_filters() {
  default_filter =
    (ref_frame[0] == INTRA_FRAME || mcomp_filter_type == SWITCHABLE) ? EIGHTTAP
                                                                     : mcomp_filter_type
  for (dir = 0; dir < MB_FILTER_COUNT; dir++)
    interp_filter[dir] = default_filter
}

uint1 is_palette_allowed(bsize) {
  bw = block_size_wide_lookup[bsize]
  bh = block_size_high_lookup[bsize]
  return (bsize >= BLOCK_8X8 && bw <= 64 && bh <= 64 &&
          allow_screen_content_tools)
}

read_intra_mode_uv() {
#if ENCODE
  enc_intra_mode_uv u(0)
  CHECK (0 <= enc_intra_mode_uv && enc_intra_mode_uv < UV_INTRA_MODES,
         "Invalid UV intra mode signalled: %d (not in [0, %d]).",
         enc_intra_mode_uv, UV_INTRA_MODES)
  if (enc_intra_mode_uv == UV_CFL_PRED && ! is_cfl_allowed(sb_size)) {
    if (lossless) {
      uint32 uv_bsize
      uv_bsize = get_plane_block_size (sb_size, 1)
      CHECK (0,
             "intra_mode_uv is UV_CFL_PRED, but UV bsize is %d x %d in lossless mode.",
             block_size_wide_lookup [uv_bsize],
             block_size_high_lookup [uv_bsize])
    } else {
      CHECK (0,
             "intra_mode_uv is UV_CFL_PRED, but bsize is %d x %d.",
             block_size_wide_lookup [sb_size],
             block_size_high_lookup [sb_size])
    }
  }

  intra_mode_uv = enc_intra_mode_uv
#endif
  intra_mode_uv ae(v)

  return intra_mode_uv
}

read_intra_block_mode_info(mi_row, mi_col) {
  uint_0_9 intra_mode_y
  uint_0_9 intra_mode_uv

#if ENCODE
  if (is_palette_allowed(sb_size)) {
    enc_palette_y u(0)
    enc_palette_uv u(0)
  } else {
    enc_palette_y = 0
    enc_palette_uv = 0
  }
#endif

  ref_frame[0] = INTRA_FRAME
  ref_frame[1] = NONE
  set_default_interp_filters()
  num4x4w = num_4x4_blocks_wide_lookup[sb_size]
  num4x4h = num_4x4_blocks_high_lookup[sb_size]
  intra_mode_y ae(v)
  y_mode = intra_mode_y
  sub_modes[0] = intra_mode_y // Used when predicting intra blocks
  read_intra_angle_info_y(mi_row, mi_col)

  if (monochrome || !is_chroma_reference(mi_row, mi_col, sb_size, 1))
  {
    intra_mode_uv = UV_INTRA_MODES // mark invalid
    uv_mode = intra_mode_uv
  } else
  {
    intra_mode_uv = read_intra_mode_uv ()
    if (intra_mode_uv == UV_CFL_PRED) {
      read_cfl_alphas()
    }
    uv_mode = intra_mode_uv // Needed for the following call
    read_intra_angle_info_uv(mi_row, mi_col)
  }

  PaletteSizeY = 0
  PaletteSizeUV = 0
  if (is_palette_allowed(sb_size))
    read_palette_mode_info(mi_row,mi_col)
  read_filter_intra_mode_info()
}

uint_0_5 get_ext_tx_set_type(bsize, uint1 use_reduced_set, origTxSz) {
  txSz = txsize_sqr_map[origTxSz]
  txSzUp = txsize_sqr_up_map[origTxSz]
  if (txSzUp > TX_32X32
      ) {
    return EXT_TX_SET_DCTONLY
  }
  if (txSzUp == TX_32X32) {
    return is_inter ? EXT_TX_SET_DCT_IDTX : EXT_TX_SET_DCTONLY
  }
  if (use_reduced_set) {
    return is_inter ? EXT_TX_SET_DCT_IDTX : EXT_TX_SET_DTT4_IDTX
  }
  if (is_inter) {
    return txSz == TX_16X16 ? EXT_TX_SET_DTT9_IDTX_1DDCT : EXT_TX_SET_ALL16
  } else {
    return txSz == TX_16X16 ? EXT_TX_SET_DTT4_IDTX : EXT_TX_SET_DTT4_IDTX_1DDCT
  }
}

read_tx_type(bsize,blocky,blockx,txSz) {
  txType = DCT_DCT
  qindex = seg_enabled ? get_qindex(1, segment_id) : base_qindex
  if (!skip_coeff && qindex > 0) {
    eset = get_ext_tx_set_type(bsize, reduced_tx_set_used, txSz)
    if (num_ext_tx_set[eset] > 1) {
      if (is_inter) {
        inter_tx_type ae(v)
        txType = inter_tx_type
      } else {
        if (use_filter_intra) {
          intraDir = fimode_to_intradir[filter_intra_mode]
        } else {
          intraDir = y_mode
        }
        intra_tx_type ae(v)
        txType = intra_tx_type
      }
    }
  }
#if ENCODE
  ASSERT(txType == enc_tx_type, "Transform type mismatch")
#endif

#if DECODE
  // Note: We only set up txk_type here in decode mode because we did
  // it already in enc_pick_coeffs when encoding.
  write_txk_block (blocky, blockx, txSz, txType)
#endif
}

int is_cfl_allowed(bsize) {
  if (lossless) {
    // The reference CfL implementation can only handle the case
    // where the transform and (chroma) block sizes are equal.
    // Thus, in lossless mode, we need to restrict to 4x4 chroma blocks
    uv_bsize = get_plane_block_size(bsize, 1)
    return (uv_bsize == BLOCK_4X4)
  } else {
    bw = block_size_wide_lookup[bsize]
    bh = block_size_high_lookup[bsize]
    return (Max(bw, bh) <= CFL_MAX_BLOCK_SIZE)
  }
}

read_cfl_alphas() {
  cfl_alpha_signs ae(v)

  if (CFL_SIGN_U(cfl_alpha_signs) != CFL_SIGN_ZERO) {
    cfl_alpha_u ae(v)
    cfl_alpha_u += 1
    if (CFL_SIGN_U(cfl_alpha_signs) == CFL_SIGN_NEG)
      cfl_alpha_u = -cfl_alpha_u
  } else {
    cfl_alpha_u = 0
  }

  if (CFL_SIGN_V(cfl_alpha_signs) != CFL_SIGN_ZERO) {
    cfl_alpha_v ae(v)
    cfl_alpha_v += 1
    if (CFL_SIGN_V(cfl_alpha_signs) == CFL_SIGN_NEG)
      cfl_alpha_v = -cfl_alpha_v
  } else {
    cfl_alpha_v = 0
  }
}

read_cdef(mi_row, mi_col) {
  if (coded_lossless || !enable_cdef || skip_cdef)
    return 0

  x = mi_col >> MI_SIZE_64X64_LOG2
  y = mi_row >> MI_SIZE_64X64_LOG2
  if (cdef_idx[x][y] != -1) {
    // The CDEF information for this unit has already been read
    return 0
  }

  if (!skip_coeff) {
    // If we get here, then this is the first non-skipped block in the
    // current 64x64 unit. So we want to read the CDEF index for
    // this unit.
    cdef_idx[x][y] ae(v)
#if VALIDATE_SPEC_CDEF_IDX
    validate(60006)
    validate(x)
    validate(y)
    validate(cdef_idx[x][y])
#endif
    // Note: We only read one coefficient set for 128xH and Wx128 blocks.
    // So we may need to copy the CDEF index to the rest of the block.
    unitsW = mi_size_wide[sb_size] >> MI_SIZE_64X64_LOG2
    unitsH = mi_size_high[sb_size] >> MI_SIZE_64X64_LOG2
    for (i = 0; i < unitsH; i++)
      for (j = 0; j < unitsW; j++) {
        cdef_idx[x+j][y+i] = cdef_idx[x][y]
#if VALIDATE_SPEC_CDEF_IDX
        validate(60006)
        validate(x+j)
        validate(y+i)
        validate(cdef_idx[x][y])
#endif
      }
  }
}

update_intra_context(mi_row, mi_col, bsize) {
  num4x4w = num_4x4_blocks_wide_lookup[bsize]
  num4x4h = num_4x4_blocks_high_lookup[bsize]
  for(x=0;x<num4x4w;x++)
    above_intra_mode_ctx[mi_col+x] = intra_mode_context[y_mode]
  for(y=0;y<num4x4h;y++)
    left_intra_mode_ctx[(mi_row+y)&mi_mask2] = intra_mode_context[y_mode]
}

get_dv_ref(mi_row, mi_col, partition) {
  // Note: find_mv_refs() needs globalmv[] to be set up, but intra frames
  // can't use global motion. So zero out the entries
  for (i = 0; i < 2; i++)
    for (j = 0; j < 2; j++)
      globalmv[i][j] = 0

  ValidateMv=1
  find_mv_refs(INTRA_FRAME, mi_row, mi_col, 0, partition)
  find_best_ref_mvs(sb_size, mi_row, mi_col, 0)

  if (nearest_mv[0][0] == 0 && nearest_mv[0][1] == 0) {
    if (near_mv[0][0] == 0 && near_mv[0][1] == 0) {
      // Default to the closest block which we're allowed to use as an intrabc source.
      if (mi_row - mib_size >= mi_row_start) {
        dv_ref[0] = -mib_size * MI_SIZE * 8 // Row offset
        dv_ref[1] = 0 // Column offset
      } else {
        dv_ref[0] = 0 // Row offset
        dv_ref[1] = -(mib_size + INTRABC_DELAY_MI) * MI_SIZE * 8 // Column offset
      }
    } else {
      for (i = 0; i < 2; i++)
        dv_ref[i] = near_mv[0][i]
    }
  } else {
      for (i = 0; i < 2; i++)
        dv_ref[i] = nearest_mv[0][i]
  }
  dv_ref_wholepel = !(dv_ref[0] & 7) && !(dv_ref[1] & 7)
}

// Note when comparing the ref code: we do everything in units of
// pixels here, while the ref code does everything in units of 1/8 px
is_dv_valid(mi_row, mi_col, int32pointer dv) {
  wholepel = !(dv[0] & 7) && !(dv[1] & 7)
  if (! wholepel)
    return 0

  // Check against the motion vector size limits
  in_range = (dv[0] < MV_UPP && dv[0] > MV_LOW &&
              dv[1] < MV_UPP && dv[1] > MV_LOW)
  if (! in_range)
    return 0

  // Note: In the code below, everything is calculated relative to
  // the top-left corner of the current tile. In the reference code,
  // on the other hand, we use absolute mi_row/mi_col values.
  // This makes reasoning about quantities such as active_sb64 harder,
  // so I prefer to do things relative to the current tile.

  // Check that the top-left corner of the source is inside
  // the current tile
  src_top = ((mi_row - mi_row_start) << MI_SIZE_LOG2) + (dv[0] >> 3)
  src_left = ((mi_col - mi_col_start) << MI_SIZE_LOG2) + (dv[1] >> 3)
  if (src_top < 0 || src_left < 0)
    return 0

  // And that the bottom-right corner is inside the tile
  src_bottom = src_top + block_size_high_lookup[sb_size]
  tileHeight = (mi_row_end - mi_row_start) << MI_SIZE_LOG2
  src_right = src_left + block_size_wide_lookup[sb_size]
  tileWidth = (mi_col_end - mi_col_start) << MI_SIZE_LOG2
  if (src_bottom > tileHeight || src_right > tileWidth)
    return 0

  // For sub-8x8 blocks, the chroma prediction may (logically) extend above and
  // to the left of the luma prediction. So we need to check
  // that this won't run off the top/left edge of the current tile.
  if (!monochrome && is_chroma_reference(mi_row, mi_col, sb_size, 1)) {
    if (block_size_wide_lookup[sb_size] < 8 && subsampling_x && src_left < 4) {
      return 0
    }
    if (block_size_high_lookup[sb_size] < 8 && subsampling_y && src_top < 4) {
      return 0
    }
  }

  // Check that the bottom-right corner is in the allowed region - ie,
  // it must be in a fully-coded superblock, and must be at least a certain
  // number of units (64wide x superblock height, for some reason) behind.
  //
  // Note: This isn't the same as "on the previous row, or on the current row
  // but N superblocks behind".
  // For example, if we're in the leftmost superblock in a tile, then we
  // aren't allowed to use the last few superblocks from the previous row either,
  // as they may not be finished processing yet in a hardware implementation.
  active_sb_row = (mi_row - mi_row_start) >> mib_size_log2
  active_sb64_col = (mi_col - mi_col_start) >> (6 - MI_SIZE_LOG2)
  superblock_px = block_size_wide_lookup[superblock_size]
  src_sb_row = (src_bottom - 1) / superblock_px
  src_sb64_col = (src_right - 1) / 64

  tile_sb64_wide = (mi_col_end - mi_col_start + (MI_SIZE_64X64 - 1)) >> MI_SIZE_64X64_LOG2

  active_sb64 = (active_sb_row * tile_sb64_wide + active_sb64_col)
  src_sb64 = (src_sb_row * tile_sb64_wide + src_sb64_col)

  if (src_sb64 >= active_sb64 - INTRABC_DELAY_SB64)
    return 0

#if USE_WAVE_FRONT
  // To allow wavefront decoding, we also need to check that the
  // bottom-right corner of the source lies in a triangular region of
  // superblocks
  gradient = 1 + (superblock_size == BLOCK_128X128) + INTRABC_DELAY_SB64
  wf_offset = -INTRABC_DELAY_SB64 + (active_sb_row - src_sb_row) * gradient
  if (src_sb64_col >= active_sb64_col + wf_offset)
    return 0
#endif

  return 1
}

read_intrabc(mi_row, mi_col, partition) {
#if ENCODE
  // Work out the intrabc reference vector first
  get_dv_ref(mi_row, mi_col, partition)
  if (dv_ref_wholepel) {
    // In order to pick intrabc, we need to know that there's a valid
    // motion vector we can encode. So we need to pick that first.
    // This sets target_mv[] to the desired intrabc vector.
    can_use_intrabc = enc_pick_dv(mi_row, mi_col, sb_size)
  } else {
    can_use_intrabc = 0
  }

  enc_use_intrabc = 0
  if (can_use_intrabc) {
    enc_use_intrabc u(0)
  }
  use_intrabc = enc_use_intrabc != 0
#endif

  use_intrabc ae(v)
  if (! use_intrabc)
    return 0

  // Set some inter-like parameters so that the rest of the codec can (mostly)
  // treat this as an inter block
  is_inter = 1
  MotionMode = SIMPLE_TRANSLATION

  y_mode = DC_PRED
  uv_mode = DC_PRED
  update_intra_context(mi_row, mi_col, sb_size)
  for (i = 0; i < MB_FILTER_COUNT; i++)
    interp_filter[i] = BILINEAR

#if !ENCODE
  get_dv_ref(mi_row, mi_col, partition) // Done earlier if we're encoding
#endif
  ASSERT(dv_ref_wholepel, "Intrabc reference motion vector must be in whole pixels")

  for (i = 0; i < 2; i++)
    best_mv[0][i] = dv_ref[i]
  nmv_ctx = 3 // Use a special newmv context for intrabc blocks
  read_mv(0, 0)

  CHECK(is_dv_valid(mi_row, mi_col, mv[0]), "Invalid intrabc motion vector")
}

// Note that there are two very similar functions
// read_intra_frame_mode_info is called inside intra frames
// read_intra_block_mode_info is called inside inter frames to read the intra parts
// Different probability tables are used
// If bsize>=8x8, then this reads a single block of size bsize
// Otherwise, this reads a 8x8 consisting of bsize blocks
read_intra_frame_mode_info(mi_row, mi_col, bsize, partition) {
  uint_0_9 default_intra_mode_y
  uint_0_9 default_intra_mode_uv
  ref_frame[0] = INTRA_FRAME
  ref_frame[1] = NONE
  is_inter = 0
  PaletteSizeY = 0
  PaletteSizeUV = 0
#if ENCODE
  if (is_palette_allowed(sb_size)) {
    enc_palette_y u(0)
    enc_palette_uv u(0)
  } else {
    enc_palette_y = 0
    enc_palette_uv = 0
  }
#endif
  use_filter_intra = 0
  if (preskip_seg_id) {
    read_intra_segment_id(mi_row, mi_col, bsize, 0)
  }
  read_skip_coeff(mi_row,mi_col)
  if (!preskip_seg_id) {
    read_intra_segment_id(mi_row, mi_col, bsize, skip_coeff)
  }
  if (!allow_intrabc)
    read_cdef(mi_row, mi_col)
  read_delta_qindex( mi_row, mi_col, bsize )
  if (allow_intrabc) {
    read_intrabc(mi_row, mi_col, partition)
    if (use_intrabc) {
      // If we get here, all the block information (tx size, etc.)
      // was read by read_intrabc(). So we don't need to read
      // any further data.
      return 0
    }
  } else {
    use_intrabc = 0
  }
  set_default_interp_filters()
  num4x4w = num_4x4_blocks_wide_lookup[bsize]
  num4x4h = num_4x4_blocks_high_lookup[bsize]
  default_intra_mode_y ae(v)
  y_mode = default_intra_mode_y
  update_intra_context(mi_row, mi_col, bsize)
  read_intra_angle_info_y(mi_row, mi_col)
  if (monochrome || !is_chroma_reference(mi_row, mi_col, bsize, 1))
  {
    intra_mode_uv = INTRA_MODES // mark invalid
  } else {
    intra_mode_uv = read_intra_mode_uv ()
    if (intra_mode_uv == UV_CFL_PRED) {
      read_cfl_alphas()
    }
    uv_mode = intra_mode_uv // Needed for the following call
    read_intra_angle_info_uv(mi_row, mi_col)
  }
  default_intra_mode_uv = intra_mode_uv
  uv_mode = default_intra_mode_uv

  if (is_palette_allowed(sb_size))
    read_palette_mode_info(mi_row,mi_col)
  read_filter_intra_mode_info()
}

read_is_inter_block(mi_row, mi_col) { // mi_row, mi_col needed in encoder
  if (skip_mode)
    is_inter = 1
  else
  if (segfeature_active(SEG_LVL_REF_FRAME)) {
    is_inter = get_segdata(SEG_LVL_REF_FRAME) != INTRA_FRAME
  } else if (segfeature_active(SEG_LVL_GLOBALMV)) {
    is_inter = 1
  } else {
    is_inter ae(v)
  }
#if ENCODE
  if (enc_use_frame_planner) {
    CHECK(is_inter == enc_is_inter[mi_col][mi_row], "is_inter does not match lookahead value")
  }
#endif
}

read_inter_frame_mode_info(mi_row, mi_col, bsize, partition) {
  for(i=0;i<2;i++) {
    mv[0][i] = 0
    mv[1][i] = 0
    pred_mv[0][i] = 0
    pred_mv[1][i] = 0
  }
  use_intrabc = 0
  read_inter_segment_id(mi_row,mi_col,bsize,1)
  read_skip_coeff(mi_row,mi_col)
  read_inter_segment_id(mi_row,mi_col,bsize,0)
#if ENCODE
  if (enc_use_frame_planner && seg_enabled && preskip_seg_id) {
    ASSERT(segment_id == enc_segment_id[mi_col][mi_row], "segment_id does not match lookahead value")
  }
#endif

  read_cdef(mi_row, mi_col)
  read_delta_qindex( mi_row, mi_col, bsize )
  read_is_inter_block(mi_row, mi_col)

  if (is_inter)
    read_inter_block_mode_info(mi_row, mi_col, bsize, partition)
  else {
    read_intra_block_mode_info(mi_row, mi_col)
  }
}

int comp_mode_ctx() {
  if (aU && aL) {  // both edges available
    if (above_ref_frame[1]<=INTRA_FRAME && left_ref_frame[1]<=INTRA_FRAME)
      // neither edge uses comp pred (0/1)
      ctx = (CHECK_BACKWARD_REFS(above_ref_frame[0])) ^ (CHECK_BACKWARD_REFS(left_ref_frame[0]))
    else if (above_ref_frame[1]<=INTRA_FRAME)
      // one of two edges uses comp pred (2/3)
      ctx = 2 + (CHECK_BACKWARD_REFS(above_ref_frame[0]) || above_ref_frame[0]<=INTRA_FRAME)
    else if (left_ref_frame[1]<=INTRA_FRAME)
      // one of two edges uses comp pred (2/3)
      ctx = 2 + (CHECK_BACKWARD_REFS(left_ref_frame[0]) ||
                          left_ref_frame[0]<=INTRA_FRAME)
    else  // both edges use comp pred (4)
      ctx = 4
  } else if (aU) {  // one edge available
    if (above_ref_frame[1]<=INTRA_FRAME)
      // edge does not use comp pred (0/1)
      ctx = CHECK_BACKWARD_REFS(above_ref_frame[0])
    else
      // edge uses comp pred (3)
      ctx = 3
   } else if (aL) {  // one edge available
    if (left_ref_frame[1]<=INTRA_FRAME)
      // edge does not use comp pred (0/1)
      ctx = CHECK_BACKWARD_REFS(left_ref_frame[0])
    else
      // edge uses comp pred (3)
      ctx = 3
  } else {  // no edges available (1)
    ctx = 1
  }
  return ctx
}

// The compound ref frame contexts form a coding tree.
// At each node, we read a bit to decide which branch to go down.
// The context for this bit depends on how many of the surrounding
// blocks' ref frames lie down each branch.
// (eg, when deciding between LAST or LAST2, we compare how many of the
// surrounding blocks used LAST to how many used LAST2)
//
// Note: With ALTREF2 enabled, some (but not all) single ref
// decisions also use the same mechanism
count_nearby_refs() {
  for (i = LAST_FRAME; i <= ALTREF_FRAME; i++)
    nearby_ref_count[i] = 0

  if (aU && above_ref_frame[0] > INTRA_FRAME) {
    nearby_ref_count[above_ref_frame[0]] += 1
    if (above_ref_frame[1] > INTRA_FRAME)
      nearby_ref_count[above_ref_frame[1]] += 1
  }

  if (aL && left_ref_frame[0] > INTRA_FRAME) {
    nearby_ref_count[left_ref_frame[0]] += 1
    if (left_ref_frame[1] > INTRA_FRAME)
      nearby_ref_count[left_ref_frame[1]] += 1
  }
}

ref_count_ctx(counts0, counts1) {
  if (counts0 < counts1)
    ctx = 0
  else if (counts0 == counts1)
    ctx = 1
  else
    ctx = 2
  ASSERT(ctx >= 0 && ctx < REF_CONTEXTS, "Reference frame context out of range")
  return ctx
}

int get_pred_context_last12_or_last3_gld() {
  last12_count = nearby_ref_count[LAST_FRAME] +
                 nearby_ref_count[LAST2_FRAME]
  last3_gld_count = nearby_ref_count[LAST3_FRAME] +
                    nearby_ref_count[GOLDEN_FRAME]
  ctx = ref_count_ctx(last12_count, last3_gld_count)
  ASSERT(ctx >= 0 && ctx < REF_CONTEXTS, "invalid ctx")
  return ctx
}

int get_pred_context_last_or_last2() {
  last_count = nearby_ref_count[LAST_FRAME]
  last2_count = nearby_ref_count[LAST2_FRAME]
  ctx = ref_count_ctx(last_count, last2_count)
  ASSERT(ctx >= 0 && ctx < REF_CONTEXTS, "invalid ctx")
  return ctx
}

int get_pred_context_last3_or_gld() {
  last3_count = nearby_ref_count[LAST3_FRAME]
  gld_count = nearby_ref_count[GOLDEN_FRAME]
  ctx = ref_count_ctx(last3_count, gld_count)
  ASSERT(ctx >= 0 && ctx < REF_CONTEXTS, "invalid ctx")
  return ctx
}

int get_pred_context_brfarf2_or_arf() {
  brfarf2_count = nearby_ref_count[BWDREF_FRAME] +
                  nearby_ref_count[ALTREF2_FRAME]
  arf_count = nearby_ref_count[ALTREF_FRAME]
  ctx = ref_count_ctx(brfarf2_count, arf_count)
  ASSERT(ctx >= 0 && ctx < REF_CONTEXTS, "invalid ctx")
  return ctx
}

int get_pred_context_brf_or_arf2() {
  brf_count = nearby_ref_count[BWDREF_FRAME]
  arf2_count = nearby_ref_count[ALTREF2_FRAME]
  ctx = ref_count_ctx(brf_count, arf2_count)
  ASSERT(ctx >= 0 && ctx < REF_CONTEXTS, "invalid ctx")
  return ctx
}

int get_pred_context_comp_ref_type() {
  above_inter = aU && above_ref_frame[0]>INTRA_FRAME
  left_inter = aL && left_ref_frame[0]>INTRA_FRAME
  above_comp_inter = aU && above_ref_frame[0]>INTRA_FRAME && above_ref_frame[1]>INTRA_FRAME
  left_comp_inter = aL && left_ref_frame[0]>INTRA_FRAME && left_ref_frame[1]>INTRA_FRAME
  above_uni_comp = above_comp_inter && is_samedir_ref_pair(above_ref_frame[0], above_ref_frame[1])
  left_uni_comp = left_comp_inter && is_samedir_ref_pair(left_ref_frame[0], left_ref_frame[1])

  if (above_inter && left_inter) {
    samedir = is_samedir_ref_pair(above_ref_frame[0], left_ref_frame[0])

    if (!above_comp_inter && !left_comp_inter) {
      ctx = 1 + 2 * samedir
    } else if (!above_comp_inter) {
      if (!left_uni_comp)
        ctx = 1
      else
        ctx = 3 + samedir
    } else if (!left_comp_inter) {
      if (!above_uni_comp)
        ctx = 1
      else
        ctx = 3 + samedir
    } else {
      if (!above_uni_comp && !left_uni_comp)
        ctx = 0
      else if (!above_uni_comp || !left_uni_comp)
        ctx = 2
      else
        ctx =
            3 + ((above_ref_frame[0] == BWDREF_FRAME) == (left_ref_frame[0] == BWDREF_FRAME))
    }
  } else if (aU && aL) {
    if (above_comp_inter)
      ctx = 1 + 2 * above_uni_comp
    else if (left_comp_inter)
      ctx = 1 + 2 * left_uni_comp
    else
      ctx = 2
  } else if (above_comp_inter) {
    ctx = 4 * above_uni_comp
  } else if (left_comp_inter) {
    ctx = 4 * left_uni_comp
  } else {  // no edges available
    ctx = 2
  }

  return ctx
}

int get_pred_context_fwd_or_bwd() {
  fwd_count = nearby_ref_count[LAST_FRAME] + nearby_ref_count[LAST2_FRAME] +
              nearby_ref_count[LAST3_FRAME] + nearby_ref_count[GOLDEN_FRAME]
  bwd_count = nearby_ref_count[BWDREF_FRAME] +
              nearby_ref_count[ALTREF2_FRAME] +
              nearby_ref_count[ALTREF_FRAME]
  ctx = ref_count_ctx(fwd_count, bwd_count)
  ASSERT(ctx >= 0 && ctx < REF_CONTEXTS, "invalid ctx")
  return ctx
}

int get_pred_context_last2_or_last3_gld() {
  last2Count = nearby_ref_count[LAST2_FRAME]
  last3_or_gld_count = nearby_ref_count[LAST3_FRAME] + nearby_ref_count[GOLDEN_FRAME]
  ctx = ref_count_ctx(last2Count, last3_or_gld_count)
  ASSERT(ctx >= 0 && ctx < REF_CONTEXTS, "invalid ctx")
  return ctx
}

uint1 CHECK_BACKWARD_REFS(refFrame) {
  return (((refFrame) >= BWDREF_FRAME) && ((refFrame) <= ALTREF_FRAME))
}

#if ENCODE
// Helper: Function to decide what the unidir_comp_ref flag should be
uint1 enc_is_unidir_ref(mi_row, mi_col) {
  fwd0 = (enc_ref_frame[mi_col][mi_row][0] <= GOLDEN_FRAME) // "Is ref 0 forward?"
  fwd1 = (enc_ref_frame[mi_col][mi_row][1] <= GOLDEN_FRAME) // "Is ref 1 forward?"
  return (fwd0 == fwd1)
}
#endif

is_compound_allowed(bsize) {
  if (comp_pred_mode == SINGLE_PREDICTION_ONLY) {
    return 0
  }
  if (block_size_wide_lookup[bsize] < 8 || block_size_high_lookup[bsize] < 8) {
    return 0
  }
  return 1
}

read_ref_frames(bsize, mi_row, mi_col) { // mi_row, mi_col only needed in encoder
  uint1 single_ref_p1
  uint1 single_ref_p2

#if ENCODE
  if (enc_stress_memory) {
    enc_pick_ref_frames_stress(mi_row, mi_col)
  } else if (! enc_use_frame_planner) {
    enc_pick_ref_frames(bsize, mi_row, mi_col)
  }
#endif

  if (skip_mode) {
    ref_frame[0] = skip_ref[0]
    ref_frame[1] = skip_ref[1]
  } else if (segfeature_active(SEG_LVL_REF_FRAME)) {
    ref_frame[0] = get_segdata(SEG_LVL_REF_FRAME)
    ref_frame[1] = NONE
  } else if (segfeature_active(SEG_LVL_SKIP) ||
             segfeature_active(SEG_LVL_GLOBALMV)) {
    ref_frame[0] = LAST_FRAME
    ref_frame[1] = NONE
  } else {
    if (is_compound_allowed(bsize)) {
      comp_mode ae(v)
    } else {
      comp_mode = SINGLE_PREDICTION_ONLY
    }
    // Count the ref frames used by nearby blocks, to help determine contexts
    count_nearby_refs()
    // FIXME(rbultje) I'm pretty sure this breaks segmentation ref frame coding
    if (comp_mode == COMP_PREDICTION_ONLY) {
      comp_ref_type ae(v)

      if (comp_ref_type == UNIDIR_COMP_REFERENCE) {
        uni_comp_ref_p ae(v)
        if (uni_comp_ref_p) {
          ref_frame[0] = BWDREF_FRAME
          ref_frame[1] = ALTREF_FRAME
        } else {
          uni_comp_ref_p1 ae(v)
          if (uni_comp_ref_p1) {
            uni_comp_ref_p2 ae(v)
            if (uni_comp_ref_p2) {
              ref_frame[0] = LAST_FRAME
              ref_frame[1] = GOLDEN_FRAME
            } else {
              ref_frame[0] = LAST_FRAME
              ref_frame[1] = LAST3_FRAME
            }
          } else {
            ref_frame[0] = LAST_FRAME
            ref_frame[1] = LAST2_FRAME
          }
        }
      } else {
        idx = 1
        comp_ref_p ae(v)
        if (!comp_ref_p) {
          comp_ref_p1 ae(v)
          ref_frame[!idx] = comp_fwd_ref[comp_ref_p1 ? 1 : 0]
        } else {
          comp_ref_p2 ae(v)
          ref_frame[!idx] = comp_fwd_ref[comp_ref_p2 ? 3 : 2]
        }
        comp_bwdref_p ae(v)
        if (!comp_bwdref_p) {
          comp_bwdref_p1 ae(v)
          ref_frame[idx] = comp_bwd_ref[comp_bwdref_p1]
        } else {
          ref_frame[idx] = comp_bwd_ref[2]
        }
        //validate(4000)
        //validate(ref_frame[0])
        //validate(ref_frame[1])
      }
    } else if (comp_mode == SINGLE_PREDICTION_ONLY) {
      single_ref_p1 ae(v)
      if (single_ref_p1) {
        single_ref_p2 ae(v)
        if (! single_ref_p2) {
          single_ref_p6 ae(v)
          ref_frame[0] = single_ref_p6 ? ALTREF2_FRAME : BWDREF_FRAME
        } else {
          ref_frame[0] = ALTREF_FRAME
        }
      } else {
        single_ref_p3 ae(v)
        if (single_ref_p3) {
          single_ref_p5 ae(v)
          ref_frame[0] = single_ref_p5 ? GOLDEN_FRAME : LAST3_FRAME
        } else {
          single_ref_p4 ae(v)
          ref_frame[0] = single_ref_p4 ? LAST2_FRAME : LAST_FRAME
        }
      }
      ref_frame[1] = NONE
    } else {
      CHECK(0,"Invalid prediction mode.")
    }
  }
#if ENCODE
  if (!enc_maximize_symbols_per_bit) {
    ASSERT(ref_frame[0] == enc_ref_frame[mi_col][mi_row][0], "Ref frame mismatch during encode")
    ASSERT(ref_frame[1] == enc_ref_frame[mi_col][mi_row][1], "Ref frame mismatch during encode")
  }
#endif
}

/* Returns whether reference motion vector is sufficiently small
to merit using high precision */
uint1 use_mv_hp(refList) {
  return use_mv_hp2(best_mv[refList][1],best_mv[refList][0])
}

/*
Read the motion vectors into mv using a method specified in mode
Works based on motion vector predictions in mv_ref[list][comp]
*/
assign_mv(mode, is_compound)
{
  //:if mv_count==934:pdb.set_trace()
  for (i = 0; i < 1 + is_compound; i++) {
    component_mode = get_ref_mode(mode, i)
    if (component_mode == NEWMV) {
      nmv_ctx = 0
#if ENCODE
      // Pick the motion vector to encode
      useHp = allow_high_precision_mv && use_mv_hp(i)
      if (enc_stress_memory) {
        // In this case, target_mv[] has already been selected.
        // So skip enc_pick_mv()
        :pass
      } else {
        enc_pick_mv(i,useHp,cur_frame_force_integer_mv ? 0 : 1)
      }
#endif // ENCODE
      read_mv(i, cur_frame_force_integer_mv ? 0 : 1)
    } else if (component_mode == NEARESTMV) {
      for(j=0;j<2;j++)
        mv[i][j] = nearest_mv[i][j]
    } else if (component_mode == NEARMV) {
      for(j=0;j<2;j++)
        mv[i][j] = near_mv[i][j]
    } else if (component_mode == GLOBALMV) {
      for(j=0;j<2;j++)
        mv[i][j] = globalmv[i][j]
    } else {
      ASSERT(0, "Unknown mode")
    }
  }
  for(j=0;j<2;j++) {
    for(refList=0;refList<1+is_compound;refList++) {
      if (mode==NEWMV) {
        pred_mv[refList][j] = best_mv[refList][j]
      } else {
        pred_mv[refList][j] = mv[refList][j]
      }
    }
  }

#if VALIDATE_MVS
  for ( i = 0; i < 1 + is_compound; i++ ) {
    validate(600)
    validate(mv[ i ][0])
    validate(mv[ i ][1])
  }
#endif
  :debug_mv(b,0,mode,is_compound)
  :debug_mv(b,1,mode,is_compound)
#if DECODE
  :C debug_mv(b,0,mode,is_compound)
  :C debug_mv(b,1,mode,is_compound)
#endif // DECODE
  for ( i = 0; i < 1 + is_compound; i++ ) {
    CHECK(mv[i][0] < MV_UPP && mv[i][0] > MV_LOW, "Motion vector out of range")
    CHECK(mv[i][1] < MV_UPP && mv[i][1] > MV_LOW, "Motion vector out of range")
  }
}

int get_mv_mag(c,offset) {
  mv_class_base = ((c) ? (CLASS0_SIZE << (c + 2)) : 0)
  return mv_class_base + offset
}

/* mvcomp=0 for reading row (y) */
int read_mv_component(mvcomp, useHp, useSubPel, refList) {
  mvcomp_sign ae(v)
  mv_class ae(v)
  class0 = mv_class == MV_CLASS_0

  // Integer part
  if (class0) {
    mvcomp_class0_bits ae(v)
    mvcomp_class0_fp = 3
    mvcomp_class0_hp = 1
    if (useSubPel) {
      mvcomp_class0_fp ae(v)
      if (useHp) {
        mvcomp_class0_hp ae(v)
      }
    }
    mag = get_mv_mag(mv_class, (mvcomp_class0_bits << 3) | (mvcomp_class0_fp << 1) | mvcomp_class0_hp) + 1
  } else {
    numbits = mv_class + CLASS0_BITS - 1
    mvcomp_bits ae(v)
    mvcomp_fp = 3
    mvcomp_hp = 1
    if (useSubPel) {
      mvcomp_fp ae(v)
      if (useHp) {
        mvcomp_hp ae(v)
      }
    }
    mag = get_mv_mag(mv_class, (mvcomp_bits << 3) | (mvcomp_fp << 1) | mvcomp_hp) + 1
  }
  return mvcomp_sign ? -mag : mag
}


uint1 mv_joint_vertical(typ) {
  return typ == MV_JOINT_HZVNZ || typ == MV_JOINT_HNZVNZ
}

uint1 mv_joint_horizontal(typ) {
  return typ == MV_JOINT_HNZVZ || typ == MV_JOINT_HNZVNZ
}

uint_0_3 get_mv_joint(mvDiffRow,mvDiffCol) {
  if (mvDiffRow == 0) {
    return mvDiffCol == 0 ? MV_JOINT_ZERO : MV_JOINT_HNZVZ
  } else {
    return mvDiffCol == 0 ? MV_JOINT_HZVNZ : MV_JOINT_HNZVNZ
  }
}

uint32 mv_class_base(c) {
  return ((c) ? (CLASS0_SIZE << (c + 2)) : 0)
}

inc_mv_component(v, comp,useSubPel) {
  // TODO these should probably all be incremented in the binarisation files
  s = v < 0
  z = (s ? -v : v) - 1       /* magnitude - 1 */

  c = MV_CLASS_0
  if (z >= CLASS0_SIZE * 4096)
    c = MV_CLASS_10
  else
    c = log_in_base_2[z >> 3]

  o = z - mv_class_base(c)

  d = (o >> 3)               /* int mv data */
  f = (o >> 1) & 3           /* fractional pel mv data */
  e = (o & 1)                /* high precision mv data */
}

// Note that the increments increment high precision counts even if not read
inc_mv(mvDiffRow,mvDiffCol,useSubPel) {
  j = get_mv_joint(mvDiffRow,mvDiffCol)
  if (mv_joint_vertical(j))
    inc_mv_component(mvDiffRow,0,useSubPel)
  if (mv_joint_horizontal(j))
    inc_mv_component(mvDiffCol,1,useSubPel)
}

int FWD_RF_OFFSET(ref) {
  return Clip3(0,FWD_REFS-1,ref - LAST_FRAME) // TODO clip should not be needed
}

int BWD_RF_OFFSET(ref) {
  return Clip3(0,BWD_REFS-1,ref - BWDREF_FRAME) // TODO clip should not be needed
}

// Given two reference frames, are they in the same direction relative
// to the current frame?
// Always returns 0 if one of the frames is intra or NONE
is_samedir_ref_pair(ref0, ref1) {
  if (ref0 <= INTRA_FRAME || ref1 <= INTRA_FRAME)
    return 0

  return (ref0 >= BWDREF_FRAME) == (ref1 >= BWDREF_FRAME)
}

/* Read motion vectors for list refList
Note that y offset = row offset = 0 index!
*/
read_mv(refList, useSubPel) {
  uint_0_3 joint_type
  joint_type ae(v)
  useHp = allow_high_precision_mv && use_mv_hp(refList)

  mv_diff_row = 0
  mv_diff_col = 0

  if (mv_joint_vertical(joint_type))
    mv_diff_row = read_mv_component(0,useHp,useSubPel,refList)

  if (mv_joint_horizontal(joint_type))
    mv_diff_col = read_mv_component(1,useHp,useSubPel,refList)

  inc_mv(mv_diff_row,mv_diff_col,useSubPel)

#if VALIDATE_SPEC_NEWMV
  validate(22000)
  validate(best_mv[refList][0])
  validate(best_mv[refList][1])
  validate(mv_diff_row)
  validate(mv_diff_col)
#endif
  mv[refList][0] = best_mv[refList][0] + mv_diff_row
  mv[refList][1] = best_mv[refList][1] + mv_diff_col
}

uint1 has_second_ref() {
  return ref_frame[1] > INTRA_FRAME
}

is_newmv_mode(mode) {
  return (mode == NEWMV || mode == NEW_NEWMV)
}

have_newmv_in_inter_mode(mode) {
  return (mode == NEWMV || mode == NEW_NEWMV || mode == NEAR_NEWMV || mode == NEW_NEARMV ||
          mode == NEAREST_NEWMV || mode == NEW_NEARESTMV)
}

have_nearmv_in_inter_mode(mode) {
  return (mode == NEARMV || mode == NEAR_NEARMV || mode == NEAR_NEWMV || mode == NEW_NEARMV)
}

int8 ref_mode[2][12] = {
  {
    // Component 0
    NEARESTMV, // NEARESTMV
    NEARMV,    // NEARMV
    GLOBALMV,  // GLOBALMV
    NEWMV,     // NEWMV
    NEARESTMV, // NEAREST_NEARESTMV
    NEARMV,    // NEAR_NEARMV
    NEARESTMV, // NEAREST_NEWMV
    NEWMV,     // NEW_NEARESTMV
    NEARMV,    // NEAR_NEWMV
    NEWMV,     // NEW_NEARMV
    GLOBALMV,  // GLOBAL_GLOBALMV
    NEWMV,     // NEW_NEWMV
  },
  {
    // Component 1
    -1,        // NEARESTMV
    -1,        // NEARMV
    -1,        // GLOBALMV
    -1,        // NEWMV
    NEARESTMV, // NEAREST_NEARESTMV
    NEARMV,    // NEAR_NEARMV
    NEWMV,     // NEAREST_NEWMV
    NEARESTMV, // NEW_NEARESTMV
    NEWMV,     // NEAR_NEWMV
    NEARMV,    // NEW_NEARMV
    GLOBALMV,  // GLOBAL_GLOBALMV
    NEWMV      // NEW_NEWMV
  }
};

get_ref_mode(mode, idx) {
  ASSERT(idx == 0 || idx == 1, "Invalid component index")
  ASSERT(mode >= NEARESTMV && mode <= NEW_NEWMV, "get_ref_mode called for invalid mode")
  res = ref_mode[idx][mode - NEARESTMV]
  ASSERT(res != -1, "get_ref_mode called with idx==1 for non-compound mode")
  return res
}

int is_masked_compound_type() {
  return (compound_type != COMPOUND_AVERAGE)
}

uint8 wedge_bits_lookup[BLOCK_SIZES_ALL] = {
  0, 0, 0, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0,
  0, 0, 0,
  0, 0, 4, 4, 0, 0,
};

int get_wedge_bits(bsize) {
  return wedge_bits_lookup[bsize]
}

uint8 wedge_codebook[3][16][3] = {
  {
    // height > width
    { WEDGE_OBLIQUE27, 4, 4 },  { WEDGE_OBLIQUE63, 4, 4 },
    { WEDGE_OBLIQUE117, 4, 4 }, { WEDGE_OBLIQUE153, 4, 4 },
    { WEDGE_HORIZONTAL, 4, 2 }, { WEDGE_HORIZONTAL, 4, 4 },
    { WEDGE_HORIZONTAL, 4, 6 }, { WEDGE_VERTICAL, 4, 4 },
    { WEDGE_OBLIQUE27, 4, 2 },  { WEDGE_OBLIQUE27, 4, 6 },
    { WEDGE_OBLIQUE153, 4, 2 }, { WEDGE_OBLIQUE153, 4, 6 },
    { WEDGE_OBLIQUE63, 2, 4 },  { WEDGE_OBLIQUE63, 6, 4 },
    { WEDGE_OBLIQUE117, 2, 4 }, { WEDGE_OBLIQUE117, 6, 4 },
  },

  {
    // height < width
    { WEDGE_OBLIQUE27, 4, 4 },  { WEDGE_OBLIQUE63, 4, 4 },
    { WEDGE_OBLIQUE117, 4, 4 }, { WEDGE_OBLIQUE153, 4, 4 },
    { WEDGE_VERTICAL, 2, 4 },   { WEDGE_VERTICAL, 4, 4 },
    { WEDGE_VERTICAL, 6, 4 },   { WEDGE_HORIZONTAL, 4, 4 },
    { WEDGE_OBLIQUE27, 4, 2 },  { WEDGE_OBLIQUE27, 4, 6 },
    { WEDGE_OBLIQUE153, 4, 2 }, { WEDGE_OBLIQUE153, 4, 6 },
    { WEDGE_OBLIQUE63, 2, 4 },  { WEDGE_OBLIQUE63, 6, 4 },
    { WEDGE_OBLIQUE117, 2, 4 }, { WEDGE_OBLIQUE117, 6, 4 },
  },

  {
    // height == width
    { WEDGE_OBLIQUE27, 4, 4 },  { WEDGE_OBLIQUE63, 4, 4 },
    { WEDGE_OBLIQUE117, 4, 4 }, { WEDGE_OBLIQUE153, 4, 4 },
    { WEDGE_HORIZONTAL, 4, 2 }, { WEDGE_HORIZONTAL, 4, 6 },
    { WEDGE_VERTICAL, 2, 4 },   { WEDGE_VERTICAL, 6, 4 },
    { WEDGE_OBLIQUE27, 4, 2 },  { WEDGE_OBLIQUE27, 4, 6 },
    { WEDGE_OBLIQUE153, 4, 2 }, { WEDGE_OBLIQUE153, 4, 6 },
    { WEDGE_OBLIQUE63, 2, 4 },  { WEDGE_OBLIQUE63, 6, 4 },
    { WEDGE_OBLIQUE117, 2, 4 }, { WEDGE_OBLIQUE117, 6, 4 },
  }
};

int block_shape(bsize) {
  bw = block_size_wide_lookup[bsize]
  bh = block_size_high_lookup[bsize]
  if (bh > bw)
    return 0
  else if (bh < bw)
    return 1
  else
    return 2
}

uint8 get_wedge_direction(bsize, index) {
  return wedge_codebook[block_shape(bsize)][index][0]
}

uint8 get_wedge_xoff(bsize, index) {
  return wedge_codebook[block_shape(bsize)][index][1]
}

uint8 get_wedge_yoff(bsize, index) {
  return wedge_codebook[block_shape(bsize)][index][2]
}

int is_wedge_allowed() {
  return (wedge_bits_lookup[sb_size] > 0)
}
int is_compound_seg_allowed() {
  return (sb_size >= BLOCK_8X8)
}

int is_any_masked_compound_used(bsize) {
 ASSERT(Min(block_size_wide_lookup[bsize], block_size_high_lookup[bsize]) >= 8,
        "is_any_masked_compound_used() should never be called with sub-8x8 blocks")
  if (is_wedge_allowed()) return 1
  if (is_compound_seg_allowed()) return 1
  return 0
}

uint1 is_interintra_allowed_bsize(bsize) {
  return (bsize >= BLOCK_8X8) && (bsize <= BLOCK_32X32)
}

uint1 is_interintra_allowed() {
  return (is_interintra_allowed_bsize(sb_size) &&
          y_mode >= NEARESTMV && y_mode <= NEWMV &&
          ref_frame[0] > INTRA_FRAME && ref_frame[1] <= INTRA_FRAME)
}

read_drl_idx(mode) {
  if (is_newmv_mode(mode)) {
    // TODO would this be better expressed as a single drl_idx syntax element?
    for (idx = 0; idx < 2; idx++) {
      if (ref_mv_count[ref_frame_type] > idx + 1) {
        drl_idx = idx
        drl_mode ae(v)
        if (drl_mode==0) {
          ref_mv_idx = idx
          return 0
        }
        ref_mv_idx = idx + 1
      }
    }
  }

  if (have_nearmv_in_inter_mode(mode)) {
    for (idx = 1; idx < 3; idx++) {
      if (ref_mv_count[ref_frame_type] > idx + 1) {
        drl_idx = idx-1
        drl_mode ae(v)
        if (drl_mode==0) {
          ref_mv_idx = idx - 1
          return 0
        }
        ref_mv_idx = idx
      }
    }
  }

#if ENCODE
  //ASSERT(ref_mv_idx == enc_drl_idx, "Mismatch in coded ref_mv_idx")
#endif
}

clear_ref_mv_idx() {
  ref_mv_idx = 0
}

count_overlappable_neighbors(bsize, mi_row, mi_col) {
  overlappable_neighbors[ 0 ] = 0
  overlappable_neighbors[ 1 ] = 0
  if ( aU ) {
    bw = mi_size_wide[bsize]
    for (i=mi_col; i<Min(mi_col + bw, mi_cols); i+=step) {
      aboveMiCol = i
      blockSbSize = sb_sizes[aboveMiCol][mi_row-1]
      step = mi_size_wide[blockSbSize]
      // If we're considering a block with width 4, it should be treated as
      // half of a pair of blocks with chroma information in the second. Move
      // above_mi_col back to the start of the pair if needed, set above_mbmi
      // to point at the block with chroma information, and set mi_step to 2 to
      // step over the entire pair at the end of the iteration.
      if (step == 1) {
        i = i & ~1
        aboveMiCol = i + 1
        step = 2
      }
      if ( refframes[0][aboveMiCol][mi_row-1][0] > INTRA_FRAME ) {
        overlappable_neighbors[ 0 ] += 1
      }
    }
  }
  if ( aL ) {
    bh = mi_size_high[bsize]
    for (j=mi_row; j<Min(mi_row + bh, mi_rows); j+=step) {
      leftMiRow = j
      blockSbSize = sb_sizes[mi_col-1][leftMiRow]
      step = mi_size_high[blockSbSize]
      // If we're considering a block with width 4, it should be treated as
      // half of a pair of blocks with chroma information in the second. Move
      // above_mi_col back to the start of the pair if needed, set above_mbmi
      // to point at the block with chroma information, and set mi_step to 2 to
      // step over the entire pair at the end of the iteration.
      if (step == 1) {
        j = j & ~1
        leftMiRow = j + 1
        step = 2
      }
      if ( refframes[0][mi_col-1][leftMiRow][0] > INTRA_FRAME ) {
        overlappable_neighbors[ 1 ] += 1
      }
    }
  }
}

uint1 check_num_overlappable_neighbors() {
  if ( overlappable_neighbors[ 0 ] == 0 &&
       overlappable_neighbors[ 1 ] == 0 ) {
    return 0
  }
  return 1
}

uint_0_2 motion_mode_allowed(mi_row, mi_col) {
  if (! switchable_motion_mode)
    return SIMPLE_TRANSLATION

  if (cur_frame_force_integer_mv == 0) {
    if (is_global_mv_block(mi_row, mi_col, 0, ref_frame[0])) {
      return SIMPLE_TRANSLATION
    }
  }
  if ( (Min(block_size_wide_lookup[sb_size], block_size_high_lookup[sb_size]) >= 8) && // allowed bsize
       ( y_mode >= NEARESTMV ) && ( y_mode <= NEWMV ) && // inter mode
       ( ref_frame[1] == NONE ) ) { // allowed compound && ref_frame[1] != INTRA_FRAME

    if ( check_num_overlappable_neighbors() > 0 ) {
      refSlot = active_ref_idx[ref_frame[0] - LAST_FRAME]
      if ((wm_num_samples > 0) &&
          (cur_frame_force_integer_mv == 0) &&
          // Don't do warped predictions from scaled ref frames
          ref_width[refSlot] == crop_width && ref_height[refSlot] == crop_height &&
          allow_warped_motion
          ) {
        return WARPED_CAUSAL
      }
      return OBMC_CAUSAL
    }

  }
  return SIMPLE_TRANSLATION
}

int interp_filter_ctx(mi_row, mi_col, dir) {
  ctx = ((dir & 0x01) * 2 + (ref_frame[1] > INTRA_FRAME)) * (SWITCHABLE_FILTERS + 1)
  left_type = SWITCHABLE_FILTERS
  above_type = SWITCHABLE_FILTERS

  if (aL) {
    src_mi_row = mi_row
    src_mi_col = mi_col - 1
    if ((refframes[0][src_mi_col][src_mi_row][0] == ref_frame[0] && has_subpel_mv_component(src_mi_row, src_mi_col, 0, dir)) ||
        (refframes[0][src_mi_col][src_mi_row][1] == ref_frame[0] && has_subpel_mv_component(src_mi_row, src_mi_col, 1, dir)))
      left_type = left_interp_filter[dir]
  }

  if (aU) {
    src_mi_row = mi_row - 1
    src_mi_col = mi_col
    if ((refframes[0][src_mi_col][src_mi_row][0] == ref_frame[0] && has_subpel_mv_component(src_mi_row, src_mi_col, 0, dir)) ||
        (refframes[0][src_mi_col][src_mi_row][1] == ref_frame[0] && has_subpel_mv_component(src_mi_row, src_mi_col, 1, dir)))
      above_type = above_interp_filter[dir]
  }

  if (left_type == above_type) {
    ctx += left_type
  } else if (left_type == SWITCHABLE_FILTERS) {
    ctx += above_type
  } else if (above_type == SWITCHABLE_FILTERS) {
    ctx += left_type
  } else {
    ctx += SWITCHABLE_FILTERS
  }
  return ctx
}

// Detect if the block have sub-pixel level motion vectors
// per component.
int has_subpel_mv_component(ref, dir, src_mi_row, src_mi_col) {
  return 1
}

read_mb_interp_filter(mi_row, mi_col, allZeromv) {
  set_default_interp_filters()
  if (is_nontrans_global_motion(allZeromv))
    return 0
  if (MotionMode == WARPED_CAUSAL)
    return 0
  if (skip_mode)
    return 0

  if (mcomp_filter_type == SWITCHABLE) {
#if ENCODE
    enc_pick_switchable_interp_filters(mi_row, mi_col)
#endif
    for (dir = 0; dir < 2; dir++) {
      interp_filter[dir] = EIGHTTAP
      if (has_subpel_mv_component(mi_row, mi_col, 0, dir) ||
          (has_second_ref() && has_subpel_mv_component(mi_row, mi_col, 1, dir))) {

#if ENCODE
        // Allow configuration file to override the interpolation filters
        // picked in enc_pick_switchable_interp_filters () above.
        enc_interp_filter_cfg u(0)
        if (enc_interp_filter_cfg != -1) {
            enc_interp_filter[dir] = enc_interp_filter_cfg
            if (!enable_dual_filter) enc_interp_filter[1] = enc_interp_filter[0]
        }
#endif
        interp_filter[dir] ae(v)

        uint_0_2 read_interp_filter
        read_interp_filter = interp_filter[dir]
      }
      if (!enable_dual_filter) {
        interp_filter[1] = interp_filter[0]
        break
      }
    }
#if ENCODE
    for (dir = 0; dir < 2; dir++) {
      ASSERT(interp_filter[dir] == enc_interp_filter[dir], "Interpolation filter mismatch")
    }
#endif
  } else {
    for (i = 0; i < MB_FILTER_COUNT; i++)
      interp_filter[i] = mcomp_filter_type
  }
}

compound_group_idx_ctx(mi_row, mi_col) {
  aboveCtx = 0
  leftCtx = 0

  hasAltrefAbove = aU && above_ref_frame[0]==ALTREF_FRAME
  hasAltrefLeft = aL && left_ref_frame[0]==ALTREF_FRAME
  hasSecondRefAbove = aU && above_ref_frame[1]>INTRA_FRAME
  hasSecondRefLeft = aL && left_ref_frame[1]>INTRA_FRAME

  if (hasSecondRefAbove) {
    // aboveCtx = value of compound_group_idx from above block
    aboveCtx = (above_compound_type[mi_col] == COMPOUND_DIFFWTD || above_compound_type[mi_col] == COMPOUND_WEDGE)
  } else if (hasAltrefAbove) {
    aboveCtx = 3
  }

  if (hasSecondRefLeft) {
    // aboveCtx = value of compound_group_idx from left block
    leftCtx = (left_compound_type[mi_row&mi_mask] == COMPOUND_DIFFWTD || left_compound_type[mi_row&mi_mask] == COMPOUND_WEDGE)
  } else if (hasAltrefLeft) {
    leftCtx = 3
  }

  return Min(5, aboveCtx + leftCtx)
}

compound_idx_ctx(mi_row, mi_col) {
  aboveCtx = 0
  leftCtx = 0

  bwd_distance = Abs(get_relative_dist(ref_decode_order[ref_frame[0]], cur_frame_offset))
  fwd_distance = Abs(get_relative_dist(ref_decode_order[ref_frame[1]], cur_frame_offset))

  hasAltrefAbove = aU && above_ref_frame[0]==ALTREF_FRAME
  hasAltrefLeft = aL && left_ref_frame[0]==ALTREF_FRAME
  hasSecondRefAbove = aU && above_ref_frame[1]>INTRA_FRAME
  hasSecondRefLeft = aL && left_ref_frame[1]>INTRA_FRAME

  if (hasSecondRefAbove) {
    // aboveCtx = value of compound_idx from above block
    // Note: If the above block used a masked compound mode instead,
    // then the ref code sets this context to 1.
    aboveCtx = (above_compound_type[mi_col] != COMPOUND_DISTANCE)
  } else if (hasAltrefAbove) {
    aboveCtx = 1
  }

  if (hasSecondRefLeft) {
    // aboveCtx = value of compound_idx from left block
    // Note: If the left block used a masked compound mode instead,
    // then the ref code sets this context to 1.
    leftCtx = (left_compound_type[mi_row&mi_mask] != COMPOUND_DISTANCE)
  } else if (hasAltrefLeft) {
    leftCtx = 1
  }

  return aboveCtx + leftCtx + 3*(fwd_distance == bwd_distance)
}

read_compound_type(mi_row, mi_col, bsize) {
  ASSERT(comp_pred_mode != SINGLE_PREDICTION_ONLY, "Compound block in a single-ref frame")
  ASSERT(MotionMode == SIMPLE_TRANSLATION, "Non-translational compound blocks are not allowed")

  compound_type = COMPOUND_AVERAGE

  if (skip_mode) {
    return 0
  }

  compound_group_idx = 0
  if (enable_masked_compound && is_any_masked_compound_used(bsize)) {
    compound_group_idx ae(v)
  }

  if (compound_group_idx == 0) {
    // Select between 50:50 or distance-weighted averaging
    if (enable_jnt_comp) {
      compound_idx ae(v)
      compound_type = compound_idx ? COMPOUND_AVERAGE : COMPOUND_DISTANCE
    } else {
      compound_type = COMPOUND_AVERAGE
    }
  } else {
    if (is_wedge_allowed()) {
      compound_mask_type ae(v)
      compound_type = compound_mask_type ? COMPOUND_DIFFWTD : COMPOUND_WEDGE
    } else {
      compound_type = COMPOUND_DIFFWTD
    }
  }

  if (compound_type == COMPOUND_WEDGE) {
    wedge_index ae(v)
    wedge_sign ae(v)
  }
  if (compound_type == COMPOUND_DIFFWTD) {
    mask_type ae(v)
  }
}

// This is called for each subblock
read_inter_block_mode_info(mi_row, mi_col, bsize, partition) {
  uint8 inter_mode
  uint1 is_compound
  PaletteSizeY = 0
  PaletteSizeUV = 0

  read_ref_frames(bsize, mi_row, mi_col)
  ref_frame_type = av1_ref_frame_type(ref_frame[0], ref_frame[1])
  ref0 = ref_frame[0]
  is_compound = has_second_ref()

  // Initialize 'globalmv' to be used throughout the mv prediction
  // (up to the call to assign_mv).
  setup_globalmv(mi_row, mi_col, bsize, 0, is_compound)

  ValidateMv = !is_compound
  find_mv_refs(ref0, mi_row, mi_col, 0, partition)

  find_best_ref_mvs(bsize, mi_row, mi_col, 0)
  for(j=0;j<2;j++) {
    best_mv[0][j] = nearest_mv[0][j]
  }

  for(idx=0;idx<2;idx++) {
    lower_mv_precision(mv_ref_list[idx])
  }

  if (is_compound) {
    ref1 = ref_frame[1]
    find_mv_refs(ref1, mi_row, mi_col, 1, partition)

    find_best_ref_mvs(bsize, mi_row, mi_col, 1)
    for(j=0;j<2;j++) {
      best_mv[1][j] = nearest_mv[1][j]
    }

    for(idx=0;idx<2;idx++) {
      lower_mv_precision(mv_ref_list[idx])
    }
  }

  if (is_compound) {
    ft = ref_frame_type
    ValidateMv = 1
    find_mv_refs(ft, mi_row, mi_col, 0, partition)
  }
  clear_ref_mv_idx()

  allZeromv = 1

  if (skip_mode) {
    ASSERT(has_second_ref(), "skip_mode should only be true for compound blocks")
    mode = NEAREST_NEARESTMV
  } else {
    if (segfeature_active(SEG_LVL_SKIP) || segfeature_active(SEG_LVL_GLOBALMV)) {
      inter_mode = GLOBALMV
      mode = inter_mode
    } else {
      if (is_compound) {
        comp_inter_mode ae(v)
        inter_mode = comp_inter_mode
      } else {
        inter_mode ae(v)
      }
      mode = inter_mode
      if (is_newmv_mode(mode) || have_nearmv_in_inter_mode(mode))
        read_drl_idx(mode)
      if (mode != GLOBALMV && mode != GLOBAL_GLOBALMV)
        allZeromv = 0
    }
  }

  if (is_compound && mode != GLOBAL_GLOBALMV) {
    ft = ref_frame_type

    if (ref_mv_count[ft] > 0) {
      if (get_ref_mode(mode, 0) == NEARESTMV) {
        for(i=0;i<2;i++)
          nearest_mv[0][i] = ref_mv_stack[ft][0][0][i]
        lower_mv_precision(nearest_mv[0])
      }

      if (get_ref_mode(mode, 1) == NEARESTMV) {
        for(i=0;i<2;i++)
          nearest_mv[1][i] = ref_mv_stack[ft][0][1][i]
        lower_mv_precision(nearest_mv[1])
      }
    }

    if (ref_mv_count[ft] > 1) {
      if (get_ref_mode(mode, 0) == NEARMV) {
        for(i=0;i<2;i++)
          near_mv[0][i] = ref_mv_stack[ft][ref_mv_idx+1][0][i]
        lower_mv_precision(near_mv[0])
      }

      if (get_ref_mode(mode, 1) == NEARMV) {
        for(i=0;i<2;i++)
          near_mv[1][i] = ref_mv_stack[ft][ref_mv_idx+1][1][i]
        lower_mv_precision(near_mv[1])
      }
    }
  } else if (ref_mv_idx > 0 && mode==NEARMV) {
    // Override nearmv to ref_mv_idx+1
    for(i=0;i<2;i++)
      near_mv[0][i] = ref_mv_stack[ref_frame[0]][1 + ref_mv_idx][0][i]
    //lower_mv_precision(near_mv[0])
  }

  bw = mi_size_wide[bsize]
  bh = mi_size_high[bsize]
  if (is_compound) {
    this_ref_mv_idx = ref_mv_idx + (mode == NEAR_NEWMV || mode == NEW_NEARMV)
    ft = ref_frame_type
    if (get_ref_mode(mode, 0) == NEWMV) {
      if (ref_mv_count[ft] > 1) {
        for (i = 0; i < 2; i++)
          best_mv[0][i] = ref_mv_stack[ft][this_ref_mv_idx][0][i]
        for(i=0;i<2;i++) {
          nearest_mv[0][i] = best_mv[0][i]
        }
      }
    }
    if (get_ref_mode(mode, 1) == NEWMV) {
      if (ref_mv_count[ft] > 1) {
        for (i = 0; i < 2; i++)
          best_mv[1][i] = ref_mv_stack[ft][this_ref_mv_idx][1][i]
        for(i=0;i<2;i++) {
          nearest_mv[1][i] = best_mv[1][i]
        }
      }
    }
  } else {
    if (mode == NEWMV) {
      ft = ref_frame_type
      if (ref_mv_count[ft] > 1) {
        for (ref=0;ref<1+is_compound;ref++) {
          for(i=0;i<2;i++) {
            best_mv[ref][i] = ref_mv_stack[ft][ref_mv_idx][ref][i]
          }
          for(i=0;i<2;i++) {
            nearest_mv[ref][i] = best_mv[ref][i]
          }
        }
      }
    }
  }
  assign_mv(mode,is_compound)
  mvrow = mv[0][0]
  mvcol = mv[0][1]
  for(list=0;list<2;list++)
    for(idx=0;idx<4;idx++)
      for(comp=0;comp<2;comp++) {
        block_mvs[list][idx][comp] = mv[list][comp]
        block_pred_mvs[list][idx][comp] = pred_mv[list][comp]
      }
  y_mode = mode

  if (enable_interintra_compound && is_interintra_allowed()) {
    interintra ae(v)
    if (interintra) {
      interintra_mode ae(v)
      ref_frame[1] = INTRA_FRAME
      AngleDeltaY = 0
      AngleDeltaUV = 0
      use_filter_intra = 0
      if (get_wedge_bits(sb_size) > 0) {
        wedge_interintra ae(v)
        if (wedge_interintra) {
          wedge_index ae(v)
          wedge_sign = 0
        }
      } else {
        wedge_interintra = 0
      }
    }
  }

  wm_warpable = 1
  if (sb_size >= BLOCK_8X8 && !has_second_ref()) {
    findSamples(mi_row, mi_col, partition)
  }
  count_overlappable_neighbors(bsize, mi_row, mi_col)

  // Note: If global-motion is enabled, then we need to store the current block
  // size and Y mode(s) so that is_global_mv_block (called by motion_mode_allowed)
  // can look them up
  sb_sizes[mi_col][mi_row] = sb_size
  y_modes[mi_col][mi_row] = y_mode
  if (!is_inter)
    uv_modes[mi_col][mi_row] = uv_mode

  last_motion_mode_allowed = motion_mode_allowed(mi_row, mi_col)
  if ( last_motion_mode_allowed == OBMC_CAUSAL ) {
    // Special case for when we cannot encode WARPED_CAUSAL
    use_obmc ae(v)
    MotionMode = SIMPLE_TRANSLATION + use_obmc
  }
  else if ( last_motion_mode_allowed != SIMPLE_TRANSLATION ) {
    // Can encode any of the available motion modes
    motion_mode ae(v)
    MotionMode = SIMPLE_TRANSLATION + motion_mode
    if (MotionMode == WARPED_CAUSAL) {
      wm_warpable = !find_projection(mi_row, mi_col)
    }
  } else {
    MotionMode = SIMPLE_TRANSLATION
  }

  if (has_second_ref()) {
    read_compound_type(mi_row, mi_col, bsize)
    uint32 r
    for (r = mi_row&mi_mask; r < (mi_row&mi_mask)+mi_size_high[bsize]; r++)
      for (c = mi_col; c < mi_col+mi_size_wide[bsize]; c++) {
        above_compound_type[c] = compound_type
        left_compound_type[r] = compound_type
      }
  }

  read_mb_interp_filter(mi_row, mi_col, allZeromv)
}

// Read the modes for a block.
// if bsize<8x8 then read all the modes for an 8x8 block
read_mode_info(mi_row, mi_col, bsize, partition) {
  // Prepare context
  for(list=0;list<2;list++) {
    if (aL) {
      left_ref_frame[list] = refframes[0][mi_col-1][mi_row][list]
    }
    if (aU) {
      above_ref_frame[list] = refframes[0][mi_col][mi_row-1][list]
    }
  }
  if (aL) {
      for (i = 0; i < MB_FILTER_COUNT; i++)
        left_interp_filter[i] = left_interp_filters[mi_row&mi_mask][i]
  }
  if (aU) {
      for (i = 0; i < MB_FILTER_COUNT; i++)
        above_interp_filter[i] = above_interp_filters[mi_col][i]
  }
  if (intra_only)
    read_intra_frame_mode_info(mi_row, mi_col, bsize, partition)
  else
    read_inter_frame_mode_info(mi_row, mi_col, bsize, partition)

  // Now duplicate the contents of the mode info block across all relevant MI blocks
  bw = mi_size_wide[bsize]
  bh = mi_size_high[bsize]
  y_mis = Min(bh, mi_rows - mi_row)
  x_mis = Min(bw, mi_cols - mi_col)
  for (x = 0; x < x_mis; x++) {
    above_skips[(mi_col+x)] = skip_coeff
    above_skip_mode[mi_col+x] = skip_mode
      for (i = 0; i < MB_FILTER_COUNT; i++)
        above_interp_filters[mi_col+x][i] = interp_filter[i]
  }
  for (y = 0; y < y_mis; y++) {
    left_skips[(mi_row+y)&mi_mask] = skip_coeff
    left_skip_mode[(mi_row+y)&mi_mask] = skip_mode
      for (i = 0; i < MB_FILTER_COUNT; i++)
        left_interp_filters[(mi_row+y)&mi_mask][i] = interp_filter[i]
  }

  // Compute loop filter level
  filter_mode = y_mode>=NEARESTMV && y_mode!=GLOBALMV && y_mode!=GLOBAL_GLOBALMV

  // Store motion vector information at every MI location in the block
  //:print 'store mode',mi_col,mi_row,p.x_mis,p.y_mis,bsize
  for (y = 0; y < y_mis; y++) {
    for (x = 0; x < x_mis; x++) {
      is_inters[mi_col+x][mi_row+y] = is_inter
      sb_sizes[mi_col+x][mi_row+y] = sb_size
      skip_coeffs[mi_col+x][mi_row+y] = skip_coeff

      if (! allow_intrabc) {
        num_lf_params = monochrome ? 2 : 4
        for (i = 0; i < num_lf_params; i++)
          lf_lvls[mi_col+x][mi_row+y][i] = lvl_lookup[segment_id][ref_frame[0]][filter_mode][i]
      }

      y_modes[mi_col+x][mi_row+y] = y_mode
      if (!is_inter || use_intrabc)
      {
        uv_modes[mi_col+x][mi_row+y] = uv_mode
      }
      PaletteSizes[0][mi_col+x][mi_row+y] = PaletteSizeY
      PaletteSizes[1][mi_col+x][mi_row+y] = PaletteSizeUV
      for (i=0; i<PaletteSizeY; i++) {
        PaletteColors[0][mi_col+x][mi_row+y][i] = palette_colors_y[i]
      }
      for (i=0; i<PaletteSizeUV; i++) {
        PaletteColors[1][mi_col+x][mi_row+y][i] = palette_colors_u[i]
      }
        for (i = 0; i < MB_FILTER_COUNT; i++)
          interp_filters[mi_col+x][mi_row+y][i] = interp_filter[i]
      for(list=0;list<2;list++) {
        r = ref_frame[list]
        refframes[0][mi_col+x][mi_row+y][list] = r
        if (r > INTRA_FRAME) {
          for(idx=0;idx<4;idx++)
            for(comp=0;comp<2;comp++) {
              mvs[0][mi_col+x][mi_row+y][list][idx][comp] = block_mvs[list][idx][comp]
              pred_mvs[0][mi_col+x][mi_row+y][list][idx][comp] = block_pred_mvs[list][idx][comp]
            }
        }
      }
      if (use_intrabc) {
        // In this case, the above loop has stored the ref frames but not
        // the corresponding motion vectors. So fix that up here.
        // (note: these mvs are only used within the current frame)
        for(idx=0;idx<4;idx++)
          for(comp=0;comp<2;comp++)
            mvs[0][mi_col+x][mi_row+y][0][idx][comp] = mv[0][comp]
      }
      if (!is_inter &&
          is_directional_mode( y_mode, sb_size ) ) {
        AngleDeltasY[mi_col+x][mi_row+y] = AngleDeltaY
      }
      if (delta_lf_present) {
        if (delta_lf_multi) {
          for (i = 0; i < frame_lf_count(); i++)
            current_deltas_lf_multi[mi_col+x][mi_row+y][i] = current_delta_lf_multi[i]
        } else
          current_deltas_lf_from_base[mi_col+x][mi_row+y] = current_delta_lf_from_base
      }
      segment_ids[mi_col+x][mi_row+y] = segment_id
    }
  }
}


uint1 is_inter_block() {
  return is_inter
}

uint1 is_intrabc_block() {
  return (allow_intrabc && use_intrabc)
}

uint8 get_max_uv_txsize(bsize, isInter) {
  planeBsize = ss_size_lookup[bsize][subsampling_x][subsampling_y]
  ASSERT(planeBsize < BLOCK_SIZES_ALL, "Plane BSize invalid")
  uvTx = max_txsize_rect_lookup[planeBsize]
  if ((uvTx == TX_64X64) ||
      (uvTx == TX_64X32) ||
      (uvTx == TX_32X64)) {
    return TX_32X32
  }
  if (uvTx == TX_64X16) {
    return TX_32X16
  }
  if (uvTx == TX_16X64) {
    return TX_16X32
  }
  return uvTx
}

// Returns the log2 of the size of transform used for chroma pixels when the luma size is txSz
uint8 get_uv_tx_size(txSz, sbSz, mi_row, mi_col) {
  return get_max_uv_txsize(sbSz, (refframes[0][mi_col][mi_row][0] > INTRA_FRAME))
}

get_vartx_max_txsize(plane, plane_bsize) {
  if (lossless) {
    return TX_4X4
  }

  maxTxSz = max_txsize_rect_lookup[plane_bsize]

  if (plane == 0) {
    return maxTxSz
  }

  if ((maxTxSz == TX_64X64) ||
      (maxTxSz == TX_64X32) ||
      (maxTxSz == TX_32X64)) {
    return TX_32X32
  }
  if (maxTxSz == TX_64X16) {
    return TX_32X16
  }
  if (maxTxSz == TX_16X64) {
    return TX_16X32
  }

  return maxTxSz
}

// bsize is size of block that we are reconstructing.
// it consists of subblocks of size sb_size.
predict_and_reconstruct(mi_row,mi_col,bsize,partition) {
  // In order to match the current behaviour of CfL, we need to know exactly which luma
  // pixels have been decoded within the current block.
  // To work this out, note that we always reconstruct full transform blocks, and
  // that we don't reconstruct any transform block which lies entirely outside
  // of the frame.
  cfl_luma_w = ALIGN_POWER_OF_TWO(mi_cols*MI_SIZE, tx_size_wide_log2[tx_size])
  cfl_luma_h = ALIGN_POWER_OF_TWO(mi_rows*MI_SIZE, tx_size_high_log2[tx_size])
  blockRowsUnit = block_size_high( mi_row, mi_col, bsize, 0 ) >> tx_size_high_log2[0]
  blockColsUnit = block_size_wide( mi_row, mi_col, bsize, 0 ) >> tx_size_wide_log2[0]
  max_unit_bsize = BLOCK_64X64
  mu_blocks_wide = block_size_wide_lookup[max_unit_bsize] >> tx_size_wide_log2[0]
  mu_blocks_high = block_size_high_lookup[max_unit_bsize] >> tx_size_high_log2[0]
  block = 0
  bh_var_tx = 0
  bw_var_tx = 0
  bw_intra = 0
  bh_intra = 0
  maxTxSz = 0
  maxUnitsWide = 0
  maxUnitsHigh = 0
  // Split the block into 64x64 luma pixel units; process all planes for a given unit
  // before moving on to the next unit
  for(blocky=0;blocky<blockRowsUnit;blocky+=mu_blocks_high)
    for(blockx=0;blockx<blockColsUnit;blockx+=mu_blocks_wide)
      for (plane = 0; plane < get_num_planes(); plane++) {
        // tx_sz is size of transform blocks
        // 4x4, 8x8, 16x16, 32x32
        tx_sz = lossless ? TX_4X4 : (plane ? get_uv_tx_size(tx_size,sb_size,mi_row,mi_col) : tx_size)
        ssx = plane_subsampling_x[plane]
        ssy = plane_subsampling_y[plane]
        bsizec = scale_chroma_bsize(bsize, ssx, ssy)
        plane_bsize = get_plane_block_size(bsizec, plane)

        if (is_inter) {
          maxTxSz = get_vartx_max_txsize(plane, plane_bsize)
          bh_var_tx = tx_size_high_unit[maxTxSz]
          bw_var_tx = tx_size_wide_unit[maxTxSz]
          pxToRightEdge = (mi_cols - mi_size_wide[bsize] - mi_col) * MI_SIZE >> ssx
          pxToBottomEdge = (mi_rows - mi_size_high[bsize] - mi_row) * MI_SIZE >> ssy
          maxUnitsWide = (block_size_wide_lookup[plane_bsize] + (pxToRightEdge < 0 ? pxToRightEdge : 0)) >> tx_size_wide_log2[0]
          maxUnitsHigh = (block_size_high_lookup[plane_bsize] + (pxToBottomEdge < 0 ? pxToBottomEdge : 0)) >> tx_size_wide_log2[0]
        }
        if (! is_inter) {
          bw_intra = tx_size_wide_unit[tx_sz]
          bh_intra = tx_size_high_unit[tx_sz]
        }

        // Only visit blocks that are partially onscreen
        maxx = ((mi_cols*MI_SIZE)>>ssx)
        maxy = ((mi_rows*MI_SIZE)>>ssy)

        row = mi_row
        col = mi_col
        // Compute pixel locations
        // The destination "pointer" must point to an even mi block (MI grid is 4x4 here)
        if (ssy && (mi_row & 0x01) && (mi_size_high[bsize] == 1)) {
          row = row - 1
        }
        if (ssx && (mi_col & 0x01) && (mi_size_wide[bsize] == 1)) {
          col = col - 1
        }
        startx = ((col*MI_SIZE)>>ssx) + (blockx >> ssx)*tx_size_wide[0]
        starty = ((row*MI_SIZE)>>ssy) + (blocky >> ssy)*tx_size_high[0]
        block_idx = (blocky >> ssy) * blockColsUnit + (blockx >> ssx) * tx_size_high_unit[tx_sz]
        if (is_chroma_reference(mi_row, mi_col, bsize, plane)) {

#if VALIDATE_SPEC_STRUCTURE
        validate(60000)
        validate(plane)
        validate(blockx)
        validate(blocky)
#endif

        if (startx<maxx && starty<maxy) {
          unit_width = ROUND_POWER_OF_TWO(Min(mu_blocks_wide + blockx, blockColsUnit), ssx)
          unit_height = ROUND_POWER_OF_TWO(Min(mu_blocks_high + blocky, blockRowsUnit), ssy)
          if (is_inter_block()) {
#if DEBUG_BLOCK_MAP
            :C printf("starting inter block\n")
            :print("starting inter block")
#endif // DEBUG_BLOCK_MAP
            for (blk_row = blocky >> ssy; blk_row < unit_height; blk_row += bh_var_tx)
              for (blk_col = blockx >> ssx; blk_col < unit_width; blk_col += bw_var_tx) {
                x = ((col*MI_SIZE)>>ssx) + blk_col*tx_size_wide[0]
                y = ((row*MI_SIZE)>>ssy) + blk_row*tx_size_high[0]
                decode_reconstruct_tx(mi_row,mi_col,maxUnitsWide,maxUnitsHigh,maxTxSz,x,y,plane,block,bsize,blk_row,blk_col)
                block += tx_size_wide_unit[maxTxSz] * tx_size_high_unit[maxTxSz]
              }
          } else {
#if DEBUG_BLOCK_MAP
            :C printf("starting intra block\n")
            :print("starting intra block")
#endif // DEBUG_BLOCK_MAP
            for (blk_row = blocky >> ssy; blk_row < unit_height; blk_row += bh_intra) {
              for (blk_col = blockx >> ssx; blk_col < unit_width; blk_col += bw_intra) {
                x = ((col*MI_SIZE)>>ssx) + blk_col*tx_size_wide[0]
                y = ((row*MI_SIZE)>>ssy) + blk_row*tx_size_high[0]
                predict_and_reconstruct_intra_block(tx_sz,x,y,plane,block_idx,bsize,blk_row,blk_col,mi_row,mi_col,partition)
                set_block_decoded(plane,x,y,tx_size_wide[tx_sz],tx_size_high[tx_sz])
              }
            }
          }
        }
        if (is_inter) {
          // Note: The intention here is to fill out a region which is
          // (block size) intersect (64x64), with appropriate subsampling.
          // So this will need to be adjusted once we support ext-partition-types
          maxw = block_size_wide_lookup[max_unit_bsize] >> ssx
          maxh = block_size_high_lookup[max_unit_bsize] >> ssy
          bw = Min(block_size_wide_lookup[plane_bsize], maxw)
          bh = Min(block_size_high_lookup[plane_bsize], maxh)
          set_block_decoded(plane,startx,starty,bw,bh)
        }
        }
#if ENCODE
        countPix+=blockRowsUnit*tx_size_wide[tx_sz]*blockColsUnit*tx_size_high[tx_sz]
#endif
      }
}

int9 get_qindex(uint1 ignoreDeltaQ, uint_0_7 segmentId) {
  if (!ignoreDeltaQ && delta_q_present)
    qindex = current_qindex
  else
    qindex = base_qindex

  if (segfeature_active_idx(segmentId, SEG_LVL_ALT_Q)) {
    data = get_segdata_idx(segmentId, SEG_LVL_ALT_Q)
    segQindex = qindex + data
    return Clip3( 0, MAXQ, segQindex)  // Delta value
  } else {
    return qindex
  }
}

// This is called when reach bottom of the partitioning quadtree
// If bsize<8x8, then it needs to read a 8x8 block in units of bsize
// Otherwise, then it needs to read a single bsize block
decode_modes_b(mi_row, mi_col, uint_0_12 bsize, partition) {
  int uv_bsize

  // Sanity check the block size with chroma subsampling.
  if (! monochrome) {
    uv_bsize = ss_size_lookup[bsize][subsampling_x][subsampling_y]
    ASSERT (uv_bsize < BLOCK_SIZES_ALL,
            "Invalid block size (%d) for ss_x/ss_y = %d/%d at (%d, %d). Last partition %d.",
            bsize, subsampling_x, subsampling_y, mi_row, mi_col, partition)
  }

#if ENCODE
  if (enc_use_frame_planner) {
    ASSERT(enc_sb_sizes[mi_col][mi_row] == bsize, "Block size mismatch")
  }
#endif
#if VALIDATE_SPEC_STRUCTURE
  validate(40001)
  validate(mi_row)
  validate(mi_col)
  validate(bsize)
#endif
  compute_available(mi_row,mi_col,bsize)

  MiRow = mi_row
  MiCol = mi_col
  sb_size = bsize

  read_mode_info(mi_row, mi_col, bsize, partition)

  if (!is_inter_block()) {
    palette_tokens(mi_row, mi_col, bsize)
  }

  read_inter_tx_size(mi_row, mi_col, bsize, skip_coeff, is_inter)

  // Save txfm size contexts
  bw = mi_size_wide[bsize]
  bh = mi_size_high[bsize]
  miH = Min(bh, mi_rows - mi_row)
  miW = Min(bw, mi_cols - mi_col)
  for (x = 0; x < miW; x++) {
    above_tx_size[mi_col+x] = tx_size
  }
  for (y = 0; y < miH; y++) {
    left_tx_size[(mi_row+y)&mi_mask] = tx_size
  }
  for (y = 0; y < miH; y++) {
    for (x = 0; x < miW; x++) {
      tx_sizes[mi_col+x][mi_row+y] = tx_size
    }
  }

  // From now on bsize is the size of the block, and sb_size is the size of the subblocks

  if (skip_coeff) {
    reset_skip_context(mi_row,mi_col,bsize)
  } else {
    setup_plane_dequants(get_qindex(0, segment_id))
  }

  reset_eobtotal()
  if (!is_inter_block()) {
    // loop over transform blocks
    predict_and_reconstruct(mi_row,mi_col,bsize,partition)
  } else {
    build_inter_predictors(mi_row,mi_col,bsize,partition)
    if (MotionMode == OBMC_CAUSAL) {
       build_obmc_inter_predictors(mi_row,mi_col,bsize,partition)
    }

    if (!skip_coeff) {
      predict_and_reconstruct(mi_row,mi_col,bsize,partition)
      //a = eobtotal
      //:print 'less8x8',less8x8,'eobtotal',a
    } else {
      // Only done for logging
      predict_and_reconstruct(mi_row,mi_col,bsize,partition)
    }
  }

#if DECODE && COLLECT_STATS
  total_blocks[is_inter][sb_size] += 1
#endif
}

reset_eobtotal() {
  eobtotal = 0
}

uint_0_16 subsize_lookup[EXT_PARTITION_TYPES][BLOCK_SIZES_ALL] = {
  {     // PARTITION_NONE
    //                            4X4
                                  BLOCK_4X4,
    // 4X8,        8X4,           8X8
    BLOCK_4X8,     BLOCK_8X4,     BLOCK_8X8,
    // 8X16,       16X8,          16X16
    BLOCK_8X16,    BLOCK_16X8,    BLOCK_16X16,
    // 16X32,      32X16,         32X32
    BLOCK_16X32,   BLOCK_32X16,   BLOCK_32X32,
    // 32X64,      64X32,         64X64
    BLOCK_32X64,   BLOCK_64X32,   BLOCK_64X64,
    // 64x128,     128x64,        128x128
    BLOCK_64X128,  BLOCK_128X64,  BLOCK_128X128,
    // 4X16,       16X4,          8X32
    BLOCK_4X16,    BLOCK_16X4,    BLOCK_8X32,
    // 32X8,       16X64,         64X16
    BLOCK_32X8,    BLOCK_16X64,   BLOCK_64X16,
  }, {  // PARTITION_HORZ
    //                            4X4
                                  BLOCK_INVALID,
    // 4X8,        8X4,           8X8
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X4,
    // 8X16,       16X8,          16X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X8,
    // 16X32,      32X16,         32X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X16,
    // 32X64,      64X32,         64X64
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X32,
    // 64x128,     128x64,        128x128
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_128X64,
    // 4X16,       16X4,          8X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 32X8,       16X64,         64X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
  }, {  // PARTITION_VERT
    //                            4X4
                                  BLOCK_INVALID,
    // 4X8,        8X4,           8X8
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_4X8,
    // 8X16,       16X8,          16X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X16,
    // 16X32,      32X16,         32X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X32,
    // 32X64,      64X32,         64X64
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X64,
    // 64x128,     128x64,        128x128
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X128,
    // 4X16,       16X4,          8X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 32X8,       16X64,         64X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
  }, {  // PARTITION_SPLIT
    //                            4X4
                                  BLOCK_INVALID,
    // 4X8,        8X4,           8X8
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_4X4,
    // 8X16,       16X8,          16X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X8,
    // 16X32,      32X16,         32X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X16,
    // 32X64,      64X32,         64X64
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X32,
    // 64x128,     128x64,        128x128
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X64,
    // 4X16,       16X4,          8X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 32X8,       16X64,         64X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
  }, {  // PARTITION_HORZ_A
    //                            4X4
                                  BLOCK_INVALID,
    // 4X8,        8X4,           8X8
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X4,
    // 8X16,       16X8,          16X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X8,
    // 16X32,      32X16,         32X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X16,
    // 32X64,      64X32,         64X64
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X32,
    // 64x128,     128x64,        128x128
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_128X64,
    // 4X16,       16X4,          8X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 32X8,       16X64,         64X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
  }, {  // PARTITION_HORZ_B
    //                            4X4
                                  BLOCK_INVALID,
    // 4X8,        8X4,           8X8
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X4,
    // 8X16,       16X8,          16X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X8,
    // 16X32,      32X16,         32X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X16,
    // 32X64,      64X32,         64X64
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X32,
    // 64x128,     128x64,        128x128
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_128X64,
    // 4X16,       16X4,          8X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 32X8,       16X64,         64X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
  }, {  // PARTITION_VERT_A
    //                            4X4
                                  BLOCK_INVALID,
    // 4X8,        8X4,           8X8
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_4X8,
    // 8X16,       16X8,          16X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X16,
    // 16X32,      32X16,         32X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X32,
    // 32X64,      64X32,         64X64
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X64,
    // 64x128,     128x64,        128x128
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X128,
    // 4X16,       16X4,          8X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 32X8,       16X64,         64X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
  }, {  // PARTITION_VERT_B
    //                            4X4
                                  BLOCK_INVALID,
    // 4X8,        8X4,           8X8
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_4X8,
    // 8X16,       16X8,          16X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X16,
    // 16X32,      32X16,         32X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X32,
    // 32X64,      64X32,         64X64
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X64,
    // 64x128,     128x64,        128x128
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X128,
    // 4X16,       16X4,          8X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 32X8,       16X64,         64X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
  }, {  // PARTITION_HORZ_4
    //                            4X4
                                  BLOCK_INVALID,
    // 4X8,        8X4,           8X8
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 8X16,       16X8,          16X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X4,
    // 16X32,      32X16,         32X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_32X8,
    // 32X64,      64X32,         64X64
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_64X16,
    // 64x128,     128x64,        128x128
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 4X16,       16X4,          8X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 32X8,       16X64,         64X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
  }, {  // PARTITION_VERT_4
    //                            4X4
                                  BLOCK_INVALID,
    // 4X8,        8X4,           8X8
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 8X16,       16X8,          16X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_4X16,
    // 16X32,      32X16,         32X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_8X32,
    // 32X64,      64X32,         64X64
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_16X64,
    // 64x128,     128x64,        128x128
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 4X16,       16X4,          8X32
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
    // 32X8,       16X64,         64X16
    BLOCK_INVALID, BLOCK_INVALID, BLOCK_INVALID,
  }
};

uint_0_12 get_subsize(bsize,partition) {
  return subsize_lookup[partition][bsize]
}

uint_0_31 partition_context_above_lookup[BLOCK_SIZES_ALL] = {
  31,  // 4X4   - 0b11111
  31,  // 4X8   - 0b11111
  30,  // 8X4   - 0b11110
  30,  // 8X8   - 0b11110
  30,  // 8X16  - 0b11110
  28,  // 16X8  - 0b11100
  28,  // 16X16 - 0b11100
  28,  // 16X32 - 0b11100
  24,  // 32X16 - 0b11000
  24,  // 32X32 - 0b11000
  24,  // 32X64 - 0b11000
  16,  // 64X32 - 0b10000
  16,  // 64X64 - 0b10000
  16,   // 64X128- 0b10000
  0,   // 128X64- 0b00000
  0,    // 128X128-0b00000
  31,  // 4X16  - 0b11111
  28,  // 16X4  - 0b11100
  30,  // 8X32  - 0b11110
  24,  // 32X8  - 0b11000
  28,  // 16X64 - 0b11100
  16,  // 64X16 - 0b10000
};

uint_0_31 partition_context_left_lookup[BLOCK_SIZES_ALL] = {
  31,  // 4X4   - 0b11111
  30,  // 4X8   - 0b11110
  31,  // 8X4   - 0b11111
  30,  // 8X8   - 0b11110
  28,  // 8X16  - 0b11100
  30,  // 16X8  - 0b11110
  28,  // 16X16 - 0b11100
  24,  // 16X32 - 0b11000
  28,  // 32X16 - 0b11100
  24,  // 32X32 - 0b11000
  16,  // 32X64 - 0b10000
  24,  // 64X32 - 0b11000
  16,  // 64X64 - 0b10000
  0,   // 64X128- 0b00000
  16,   // 128X64- 0b10000
  0,    // 128X128-0b00000
  28,  // 4X16  - 0b11100
  31,  // 16X4  - 0b11111
  24,  // 8X32  - 0b11000
  30,  // 32X8  - 0b11110
  16,  // 16X64 - 0b10000
  28,  // 64X16 - 0b11100
};

update_ext_partition_context(mi_row, mi_col, subsize, bsize, partition) {
  if ((bsize >= BLOCK_8X8) &&
      (!((partition == PARTITION_SPLIT) && (bsize != BLOCK_8X8)))) {
    hbs = mi_size_wide[bsize] / 2
    bsize2 = get_subsize(bsize, PARTITION_SPLIT)
    if ((partition == PARTITION_SPLIT) ||
        (partition == PARTITION_NONE) ||
        (partition == PARTITION_HORZ) ||
        (partition == PARTITION_VERT) ||
        (partition == PARTITION_HORZ_4) ||
        (partition == PARTITION_VERT_4)) {
        update_partition_context(mi_row, mi_col, subsize, bsize)
    } else if (partition == PARTITION_HORZ_A) {
        update_partition_context(mi_row, mi_col, bsize2, subsize)
        update_partition_context(mi_row + hbs, mi_col, subsize, subsize)
    } else if (partition == PARTITION_HORZ_B) {
        update_partition_context(mi_row, mi_col, subsize, subsize)
        update_partition_context(mi_row + hbs, mi_col, bsize2, subsize)
    } else if (partition == PARTITION_VERT_A) {
        update_partition_context(mi_row, mi_col, bsize2, subsize)
        update_partition_context(mi_row, mi_col + hbs, subsize, subsize)
    } else if (partition == PARTITION_VERT_B) {
        update_partition_context(mi_row, mi_col, subsize, subsize)
        update_partition_context(mi_row, mi_col + hbs, bsize2, subsize)
    } else {
      ASSERT(0, "Invalid partition type")
    }
  }
}

// bsize is the size of the quadtree
// sb_type is the size of the subblocks inside the quadtree
update_partition_context(mi_row, mi_col, sb_type, bsize) {
  bw = mi_size_wide[bsize]
  bh = mi_size_high[bsize]

  // update the partition context at the end nodes. set partition bits
  // of block sizes larger than the current one to be one, and partition
  // bits of smaller block sizes to be zero.
  for(i=0;i<bw;i++) {
    above_seg_context[mi_col+i] = partition_context_above_lookup[sb_type]
  }
  for(j=0;j<bh;j++) {
    left_seg_context[(mi_row & mi_mask)+j] = partition_context_left_lookup[sb_type]
  }
  //:print b.video_stream.frame_data.above_seg_context[mi_col]
  //:pdb.set_trace()
}

// Note that we are allowed to use information from above tiles
compute_available(mi_row,mi_col,bsize) {
  aL = mi_col > mi_col_start
  aU = mi_row > mi_row_start
  if (monochrome)
    return 0
  chroma_aU = aU
  chroma_aL = aL
  bw = mi_size_wide[bsize]
  bh = mi_size_high[bsize]
  if (plane_subsampling_x[1] && bw < mi_size_wide[BLOCK_8X8]) {
    chroma_aL = (mi_col - 1) > mi_col_start
  }
  if (plane_subsampling_y[1] && bh < mi_size_high[BLOCK_8X8]) {
    chroma_aU = (mi_row - 1) > mi_row_start
  }
}

decode_modes_sb(mi_row, mi_col, bsize) {
  compute_available(mi_row,mi_col,bsize)

  if (!allow_intrabc)
    if (any_loop_restoration_used() && bsize == superblock_size) {
      //:print "Reading loop restoration info for superblock at (%d, %d)" % (mi_row, mi_col)
      read_sb_restoration_info(mi_row, mi_col)
    }

  hbs = mi_size_wide[bsize] / 2
  if (mi_row >= mi_rows || mi_col >= mi_cols) {
    return 0
  }
  partition = (bsize < BLOCK_8X8) ? PARTITION_NONE : read_partition(mi_row, mi_col, bsize)
#if ENCODE
  partition_cfg = partition
#endif // ENCODE
#if VALIDATE_SPEC_STRUCTURE
  validate(40000)
  validate(partition)
#endif
  subsize = get_subsize(bsize,partition)
  bsize2 = get_subsize(bsize, PARTITION_SPLIT)
  qbs = hbs / 2
  if (!monochrome)
    CHECK(BLOCK_INVALID != get_plane_block_size(subsize, 1),"Invalid block size")
    {
    if (partition==PARTITION_NONE) {
      decode_modes_b(mi_row, mi_col, subsize, partition)
    } else if (partition==PARTITION_HORZ) {
      CHECK(hbs > 0, "hbs should be > 0")
      decode_modes_b( mi_row, mi_col, subsize, partition)
      if (mi_row + hbs < mi_rows)
        decode_modes_b(mi_row + hbs, mi_col, subsize, partition)
    } else if (partition==PARTITION_VERT) {
        CHECK(hbs > 0, "hbs should be > 0")
        decode_modes_b(mi_row, mi_col, subsize, partition)
        if (mi_col + hbs < mi_cols)
          decode_modes_b(mi_row, mi_col + hbs, subsize, partition)
    } else if ( partition==PARTITION_SPLIT) {
        CHECK(hbs > 0, "hbs should be > 0")
        decode_modes_sb(mi_row, mi_col, subsize)
        decode_modes_sb(mi_row, mi_col + hbs, subsize)
        decode_modes_sb(mi_row + hbs, mi_col, subsize)
        decode_modes_sb(mi_row + hbs, mi_col + hbs, subsize)
    } else if (partition==PARTITION_HORZ_A) {
        CHECK(hbs > 0, "hbs should be > 0")
        decode_modes_b(mi_row, mi_col, bsize2, partition)
        decode_modes_b(mi_row, mi_col + hbs, bsize2, partition)
        decode_modes_b(mi_row + hbs, mi_col, subsize, partition)
    } else if (partition==PARTITION_HORZ_B) {
        CHECK(hbs > 0, "hbs should be > 0")
        decode_modes_b(mi_row, mi_col, subsize, partition)
        decode_modes_b(mi_row + hbs, mi_col, bsize2, partition)
        decode_modes_b(mi_row + hbs, mi_col + hbs, bsize2, partition)
    } else if (partition==PARTITION_VERT_A) {
        CHECK(hbs > 0, "hbs should be > 0")
        decode_modes_b(mi_row, mi_col, bsize2, partition)
        decode_modes_b(mi_row + hbs, mi_col, bsize2, partition)
        decode_modes_b(mi_row, mi_col + hbs, subsize, partition)
    } else if (partition==PARTITION_VERT_B) {
        CHECK(hbs > 0, "hbs should be > 0")
        decode_modes_b(mi_row, mi_col, subsize, partition)
        decode_modes_b(mi_row, mi_col + hbs, bsize2, partition)
        decode_modes_b(mi_row + hbs, mi_col + hbs, bsize2, partition)
    } else if (partition==PARTITION_HORZ_4) {
        CHECK(qbs > 0, "qbs should be > 0")
        for (i = 0; i < 4; i++) {
          row = mi_row + i * qbs
          if (row < mi_rows) {
            decode_modes_b(row, mi_col, subsize, partition)
          } else {
            ASSERT(i != 0, "Why have we tried this partition?")
          }
        }
    } else if (partition==PARTITION_VERT_4) {
        CHECK(qbs > 0, "qbs should be > 0")
        for (i = 0; i < 4; i++) {
          col = mi_col + i * qbs
          if (col < mi_cols) {
            decode_modes_b(mi_row, col, subsize, partition)
          } else {
            ASSERT(i != 0, "Why have we tried this partition?")
          }
        }
    } else {
      CHECK(0,"Invalid partition type")
    }
  }

  // update partition context
  update_ext_partition_context(mi_row, mi_col, subsize, bsize, partition)
}

clear_cdef() {
  if (coded_lossless || !enable_cdef || skip_cdef)
    return 0

  y0 = mi_row_start >> MI_SIZE_64X64_LOG2
  y1 = (mi_row_end + (MI_SIZE_64X64 - 1)) >> MI_SIZE_64X64_LOG2
  x0 = mi_col_start >> MI_SIZE_64X64_LOG2
  x1 = (mi_col_end + (MI_SIZE_64X64 - 1)) >> MI_SIZE_64X64_LOG2
  for (y = y0; y < y1; y++) {
    for (x = x0; x < x1; x++) {
      cdef_idx[x][y] = -1 // Special value to indicate "no cdef"
    }
  }
}

decode_tile() {
  clear_above_context()
  reset_block_decoded_map()
  init_tile_probs()
  prev_delta_lf_from_base = 0
  for (i = 0; i < frame_lf_count(); i++)
    prev_delta_lf_multi[i] = 0
  reset_lr_refs()
  clear_cdef()
#if ENCODE
  if (enc_use_frame_planner) {
    enc_plan_tile()
  }
  enc_partition_idx = 0
#endif
  TileWidth = ( mi_col_end - mi_col_start ) * MI_SIZE
  RightMostTile = (mi_col_end == mi_cols)
  if ( use_superres == 0 && RightMostTile == 0 ) {
    // not actually possible to hit this, as the minium superblock size is 64x64
    CHECK(TileWidth >= 64, "If use_superres is equal to 0 and RightMostTile is equal to 0, TileWidth must be greater than or equal to 64")
  }
  if ( use_superres == 1 && RightMostTile == 0 ) {
    CHECK(TileWidth >= 128, "[SUPERRES] If use_superres is equal to 1 and RightMostTile is equal to 0, TileWidth must be greater than or equal to 128")
  }
  for (mi_row = mi_row_start; mi_row < mi_row_end; mi_row += mib_size) {
    clear_left_context()
    for (mi_col = mi_col_start; mi_col < mi_col_end; mi_col += mib_size) {
      decode_modes_sb(mi_row, mi_col, superblock_size)
    }
  }
}

uint32 decode_tile_wrapper(tileRow, tileCol, sz) {
  prev_qindex = base_qindex
  tile_init(tileRow,tileCol)
  if (tileRow < tile_rows - 1) {
    tile_height_mid = (mi_row_end - mi_row_start) << MI_SIZE_LOG2
  } else {
    tile_height_bottom = (mi_row_end - mi_row_start) << MI_SIZE_LOG2
  }
  if (tileCol < tile_cols - 1) {
    tile_width_mid = (mi_col_end - mi_col_start) << MI_SIZE_LOG2
  } else {
    tile_width_right = (mi_col_end - mi_col_start) << MI_SIZE_LOG2
  }
  // tile_height in units of MiI
  tile_height_in_mi_clamped = Min((mi_row_end - mi_row_start),64)
  // tile width is between 4 and 64 super blocks (4*64 and 64*64 pixels)
  tile_width_in_mi_clamped = Min((mi_col_end - mi_col_start),64)

#if ENCODE
  uint64 pos
  pos = current_position()
#endif // ENCODE
  init_bool( sz )
  decode_tile()
#if ENCODE
  extra_tile_padding u(0)
  exit_bool(extra_tile_padding)
  sz = current_position()-pos
#else // ENCODE
  exit_bool(0)
#endif // ENCODE
  if ((! disable_cdf_update) && tile_id == context_tile_id) {
    save_adapted_cdfs()
  }
#if DECODE && COLLECT_STATS
  tileBits = sz * 8

  total_tiles += 1
  total_tile_bits += tileBits
  largest_tile_size_in_frame = Max(largest_tile_size_in_frame, tileBits)
  smallest_tile_size_in_frame = Min(smallest_tile_size_in_frame, tileBits)
#endif // DECODE && COLLECT_STATS
  return sz
}

#if DECODE
// Note that tiles can use information from above tiles, but not from left/right tiles.
decode_tiles(sz, decTileRow, decTileCol) {
  // Load the tile locations
  // Note: If we're not using large-scale-tile mode, then the tiles are laid out
  // in the bitstream in the same order as how we will decode them, so this step
  // isn't strictly necessary.
  // But if we are using large-scale-tile, then the we just decode one tile
  if (large_scale_tile) {
    size = decode_tile_wrapper(decTileRow, decTileCol, sz)
    ASSERT(sz == size, "size mismatch")
  } else {
    tile_id = 0
    for (tileRow = 0; tileRow < tile_rows; tileRow++) {
      for (tileCol = 0; tileCol < tile_cols; tileCol++) {
        readTileSize = (tile_id != tg_end)
        if ((tile_id >= tg_start) && (tile_id <= tg_end)) {
          tileLocation = current_position()
          if (readTileSize) {
            size le(8*(tile_sz_mag+1))
            size += 1 // We actually read (size - 1), so correct for that here
            sz -= tile_sz_mag + 1
            tileLocation += tile_sz_mag + 1
          } else {
            size = sz
          }
          sz -= size
          tile_sizes[tileCol][tileRow] = size
          tile_locations[tileCol][tileRow] = tileLocation
          unused = set_pos(tileLocation + size)
        }
        tile_id++
      }
    }

    // decode/encode tiles in raster order
    tile_id = 0
    for (tileRow = 0; tileRow < tile_rows; tileRow++) {
      for (tileCol = 0; tileCol < tile_cols; tileCol++) {
        if ((tile_id >= tg_start) && (tile_id <= tg_end)) {
          unused = set_pos(tile_locations[tileCol][tileRow])
          size = decode_tile_wrapper(tileRow, tileCol, tile_sizes[tileCol][tileRow])
          ASSERT(tile_sizes[tileCol][tileRow] == size, "size mismatch")
        }
        tile_id++
      }
    }
  }
}
#endif // DECODE

setup_block_dptrs(int ss_x, int ss_y) {
  for (i = 0; i < get_num_planes(); i++) {
    plane_type[i] = i ? PLANE_TYPE_UV : PLANE_TYPE_Y_WITH_DC
    plane_subsampling_x[i] = i ? ss_x : 0
    plane_subsampling_y[i] = i ? ss_y : 0
  }
}

// This is called at the start of each CVS
on_new_cvs() {
    // If we have a different sequence header or scalability
    // structure, we reset the decoder model.
    if (sequenceHeaderChanged || scalabilityStructureChanged) {
        reset_decoder_model()
    }
}

check_new_CVS() {
  if (SeenSeqHeaderBeforeFirstFrameHeader == 1 &&
      frame_type == KEY_FRAME &&
      show_frame == 1 &&
      show_existing_frame == 0 &&
      temporal_layer == 0) {

    // This is a new CVS
    on_new_cvs()

  } else {
    // This is not a new CVS
    ASSERT(sequenceHeaderChanged == 0, "Sequence header changed without starting a new CVS")
    ASSERT(scalabilityStructureChanged == 0, "Scalability structure changed without starting a new CVS")
  }
  sequenceHeaderChanged = 0
  scalabilityStructureChanged = 0
}


// Decide whether or not inter prediction can use the previous frames motion vectors
decode_frame_headers() {
  read_uncompressed_header()
  check_new_CVS()
  if (show_existing_frame) {
    return 0 // Decided to show a previous frame
  }
  setup_plane_dequants(base_qindex)
  setup_block_dptrs(subsampling_x,subsampling_y)
  context_updated = 0
  clear_ref_frames()

  if (! allow_intrabc)
    if (any_loop_restoration_used())
      setup_tile_restoration_info()
}

// Perform any updates which need to be applied at the end of encoding/decoding
// the frame
wrapup_frame() {
  if (frame_type == KEY_FRAME) {
    decoded_key_frame = 1
  }

  if (! disable_cdf_update
      && !frame_parallel_decoding_mode
      && !large_scale_tile) {
    ASSERT(context_updated, "save_adapted_cdfs() hasn't been called this frame")
    load_adapted_tile_coef_cdfs()
    load_adapted_tile_intra_cdfs()
    if (!intra_only) {
      load_adapted_tile_inter_cdfs()
      load_adapted_tile_mv_cdfs()
    }
  }

#if DECODE && COLLECT_STATS
  // Update the maximum tile size ratio

  // Compare (largest_tile_size_in_frame / smallest_tile_size_in_frame)
  // to (max_tile_ratio_num / max_tile_ratio_denom), without actually
  // having to do the divisions
  if (largest_tile_size_in_frame * max_tile_ratio_denom >
      smallest_tile_size_in_frame * max_tile_ratio_num) {
    // The new ratio is larger, so update
    max_tile_ratio_num = largest_tile_size_in_frame
    max_tile_ratio_denom = smallest_tile_size_in_frame
  } else {
    // The old ratio is larger, so do nothing
    :pass
  }
#endif
}

#if DECODE
decode_tiles_and_wrapup(sz, decTileRow, decTileCol) {
  decode_tiles(sz, decTileRow, decTileCol)

  uint32 numTiles
  numTiles = tile_rows * tile_cols

  // Note: We only support decoding tile groups in-order
  if (tg_end == numTiles - 1) {
    wrapup_frame()
  }
}
#endif // DECODE

// Store decoded frame into some of the reference frames
store_ref_buffers() {
  // Note: Show-existing frames do *not* update prev_frame_id,
  // but if showing a forward keyframe we *do* update ref_frame_id later.
  if (frame_id_numbers_present_flag && !show_existing_frame) {
    prev_frame_id = current_frame_id
  }

#if VALIDATE_OUTPUT
  if (!show_existing_frame) {
    validate(99999)
    for(plane=0;plane<get_num_planes();plane++) {
        w = (upscaled_width+plane_subsampling_x[plane])>>plane_subsampling_x[plane]
        h = (upscaled_height+plane_subsampling_y[plane])>>plane_subsampling_y[plane]
        for(y=0;y<h;y++) {
          for(x=0;x<w;x++) {
            validate(frame[plane][x][y])
          }
        }
      }
  }
#endif
  for(i=0;i<NUM_REF_FRAMES;i++) {
    if (refresh_frame_flags & (1<<i)) {
      // x=profile
      // t=subsampling_x
      // y=color_space
      // :print 'swap_frame_buffers',i,'profile=',x,'sx',t,'color',y
      ref_width[i]          = upscaled_width
      ref_height[i]         = upscaled_height
      ref_crop_width[i]     = crop_width
      ref_crop_height[i]    = crop_height
      ref_display_width[i]  = display_width
      ref_display_height[i] = display_height
      ref_profile[i]        = profile
      ref_bit_depth[i]      = bit_depth
      ref_subsampling_x[i]  = subsampling_x
      ref_subsampling_y[i]  = subsampling_y
      ref_valid[i]          = 1
      if (frame_id_numbers_present_flag) {
        ref_frame_id[i]     = current_frame_id
      }
      ref_color_primaries[i] = color_primaries
      ref_transfer_characteristics[i] = transfer_characteristics
      ref_matrix_coefficients[i] = matrix_coefficients
      if (subsampling_x && subsampling_y)
        ref_chroma_sample_position[i] = chroma_sample_position
      for(plane=0;plane<get_num_planes();plane++) {
        w = (upscaled_width+plane_subsampling_x[plane])>>plane_subsampling_x[plane]
        h = (upscaled_height+plane_subsampling_y[plane])>>plane_subsampling_y[plane]
        for(y=0;y<h;y++) {
          for(x=0;x<w;x++) {
            framestore[i][plane][x][y] = frame[plane][x][y]
          }
        }
      }
      save_mode_info(2+i,0)
      for (j=0; j<1+INTER_REFS_PER_FRAME; j++) {
        saved_ref_decode_order[i][j] = ref_decode_order[j]
      }
      save_cdfs(i)

      ref_showable_frame[i] = showable_frame
      ref_film_grain_params_present[i] = film_grain_params_present
      ref_apply_grain[i] = apply_grain
      if (apply_grain) {
        ref_random_seed[i] = random_seed
        ref_num_y_points[i] = num_y_points
        for (j=0; j<num_y_points; j++) {
          ref_scaling_points_y[i][j][0] = scaling_points_y[j][0]
          ref_scaling_points_y[i][j][1] = scaling_points_y[j][1]
        }
        ref_chroma_scaling_from_luma[i] = chroma_scaling_from_luma
        ref_num_cb_points[i] = num_cb_points
        for (j=0; j<num_cb_points; j++) {
          ref_scaling_points_cb[i][j][0] = scaling_points_cb[j][0]
          ref_scaling_points_cb[i][j][1] = scaling_points_cb[j][1]
        }
        ref_num_cr_points[i] = num_cr_points
        for (j=0; j<num_cr_points; j++) {
         ref_scaling_points_cr[i][j][0] = scaling_points_cr[j][0]
         ref_scaling_points_cr[i][j][1] = scaling_points_cr[j][1]
        }
        ref_scaling_shift[i] = scaling_shift
        ref_ar_coeff_lag[i] = ar_coeff_lag

        // Work out how many coefficients to save for each plane
        baseNumPos = 2 * ar_coeff_lag * (ar_coeff_lag + 1)
        numPosLuma = (num_y_points) ? baseNumPos : 0
        numPosCb = (num_cb_points || chroma_scaling_from_luma) ? baseNumPos + (num_y_points > 0) : 0
        numPosCr = (num_cr_points || chroma_scaling_from_luma) ? baseNumPos + (num_y_points > 0) : 0

        for (j = 0; j<numPosLuma; j++) {
          ref_ar_coeffs_y[i][j] = ar_coeffs_y[j]
        }
        for (j = 0; j<numPosCb; j++) {
          ref_ar_coeffs_cb[i][j] = ar_coeffs_cb[j]
        }
        for (j = 0; j<numPosCr; j++) {
          ref_ar_coeffs_cr[i][j] = ar_coeffs_cr[j]
        }
        ref_ar_coeff_shift[i] = ar_coeff_shift
        ref_grain_scale_shift[i] = grain_scale_shift
        if (num_cb_points) {
          ref_cb_mult[i] = cb_mult
          ref_cb_luma_mult[i] = cb_luma_mult
          ref_cb_offset[i] = cb_offset
        }
        if (num_cr_points) {
          ref_cr_mult[i] = cr_mult
          ref_cr_luma_mult[i] = cr_luma_mult
          ref_cr_offset[i] = cr_offset
        }
        ref_overlap_flag[i] = overlap_flag
        ref_clip_to_restricted_range[i] = clip_to_restricted_range
      }
    }
  }

  refresh_frame_flags = 0
}

save_anchor_frame() {
  ASSERT(num_large_scale_tile_anchor_frames, "save_anchor_frame should only be called in ext_tile streams")
  ASSERT(!large_scale_tile, "save_anchor_frame called when moved onto tile list portion")
  ASSERT(frame_number < num_large_scale_tile_anchor_frames, "save_anchor_frame called for too many frames")

  anchor_width = upscaled_width
  for(plane=0;plane<get_num_planes();plane++) {
    w = (upscaled_width+plane_subsampling_x[plane])>>plane_subsampling_x[plane]
    h = (upscaled_height+plane_subsampling_y[plane])>>plane_subsampling_y[plane]
    for(y=0;y<h;y++) {
      for(x=0;x<w;x++) {
        anchor_framestore[frame_number][plane][x][y] = frame[plane][x][y]
      }
    }
  }
}

load_anchor_frame(AnchorFrameIdx) {
 ASSERT(AnchorFrameIdx < num_large_scale_tile_anchor_frames, "Invalid anchor frame")
 for(plane=0;plane<get_num_planes();plane++) {
    w = (upscaled_width+plane_subsampling_x[plane])>>plane_subsampling_x[plane]
    h = (upscaled_height+plane_subsampling_y[plane])>>plane_subsampling_y[plane]
    for(y=0;y<h;y++) {
      for(x=0;x<w;x++) {
        framestore[active_ref_idx[LAST_FRAME - LAST_FRAME]][plane][x][y] = anchor_framestore[AnchorFrameIdx][plane][x][y]
      }
    }
  }
}

uint1 segfeature_active_idx(idx, feature_id) {
  return seg_enabled && (feature_mask[idx] & (1 << feature_id))
}

uint1 segfeature_active(feature_id) {
  return segfeature_active_idx(segment_id, feature_id)
}

uint8 log_in_base_2[1025] = {
  0, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4,
  4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
  5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6,
  6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
  6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
  6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
  9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 10
};

int16 resize_filter_normative[SUPERRES_FILTER_SHIFTS][SUPERRES_FILTER_TAPS] = {
  { 0, 0, 0, 128, 0, 0, 0, 0 },        { 0, 0, -1, 128, 2, -1, 0, 0 },
  { 0, 1, -3, 127, 4, -2, 1, 0 },      { 0, 1, -4, 127, 6, -3, 1, 0 },
  { 0, 2, -6, 126, 8, -3, 1, 0 },      { 0, 2, -7, 125, 11, -4, 1, 0 },
  { -1, 2, -8, 125, 13, -5, 2, 0 },    { -1, 3, -9, 124, 15, -6, 2, 0 },
  { -1, 3, -10, 123, 18, -6, 2, -1 },  { -1, 3, -11, 122, 20, -7, 3, -1 },
  { -1, 4, -12, 121, 22, -8, 3, -1 },  { -1, 4, -13, 120, 25, -9, 3, -1 },
  { -1, 4, -14, 118, 28, -9, 3, -1 },  { -1, 4, -15, 117, 30, -10, 4, -1 },
  { -1, 5, -16, 116, 32, -11, 4, -1 }, { -1, 5, -16, 114, 35, -12, 4, -1 },
  { -1, 5, -17, 112, 38, -12, 4, -1 }, { -1, 5, -18, 111, 40, -13, 5, -1 },
  { -1, 5, -18, 109, 43, -14, 5, -1 }, { -1, 6, -19, 107, 45, -14, 5, -1 },
  { -1, 6, -19, 105, 48, -15, 5, -1 }, { -1, 6, -19, 103, 51, -16, 5, -1 },
  { -1, 6, -20, 101, 53, -16, 6, -1 }, { -1, 6, -20, 99, 56, -17, 6, -1 },
  { -1, 6, -20, 97, 58, -17, 6, -1 },  { -1, 6, -20, 95, 61, -18, 6, -1 },
  { -2, 7, -20, 93, 64, -18, 6, -2 },  { -2, 7, -20, 91, 66, -19, 6, -1 },
  { -2, 7, -20, 88, 69, -19, 6, -1 },  { -2, 7, -20, 86, 71, -19, 6, -1 },
  { -2, 7, -20, 84, 74, -20, 7, -2 },  { -2, 7, -20, 81, 76, -20, 7, -1 },
  { -2, 7, -20, 79, 79, -20, 7, -2 },  { -1, 7, -20, 76, 81, -20, 7, -2 },
  { -2, 7, -20, 74, 84, -20, 7, -2 },  { -1, 6, -19, 71, 86, -20, 7, -2 },
  { -1, 6, -19, 69, 88, -20, 7, -2 },  { -1, 6, -19, 66, 91, -20, 7, -2 },
  { -2, 6, -18, 64, 93, -20, 7, -2 },  { -1, 6, -18, 61, 95, -20, 6, -1 },
  { -1, 6, -17, 58, 97, -20, 6, -1 },  { -1, 6, -17, 56, 99, -20, 6, -1 },
  { -1, 6, -16, 53, 101, -20, 6, -1 }, { -1, 5, -16, 51, 103, -19, 6, -1 },
  { -1, 5, -15, 48, 105, -19, 6, -1 }, { -1, 5, -14, 45, 107, -19, 6, -1 },
  { -1, 5, -14, 43, 109, -18, 5, -1 }, { -1, 5, -13, 40, 111, -18, 5, -1 },
  { -1, 4, -12, 38, 112, -17, 5, -1 }, { -1, 4, -12, 35, 114, -16, 5, -1 },
  { -1, 4, -11, 32, 116, -16, 5, -1 }, { -1, 4, -10, 30, 117, -15, 4, -1 },
  { -1, 3, -9, 28, 118, -14, 4, -1 },  { -1, 3, -9, 25, 120, -13, 4, -1 },
  { -1, 3, -8, 22, 121, -12, 4, -1 },  { -1, 3, -7, 20, 122, -11, 3, -1 },
  { -1, 2, -6, 18, 123, -10, 3, -1 },  { 0, 2, -6, 15, 124, -9, 3, -1 },
  { 0, 2, -5, 13, 125, -8, 2, -1 },    { 0, 1, -4, 11, 125, -7, 2, 0 },
  { 0, 1, -3, 8, 126, -6, 2, 0 },      { 0, 1, -3, 6, 127, -4, 1, 0 },
  { 0, 1, -2, 4, 127, -3, 1, 0 },      { 0, 0, -1, 2, 128, -1, 0, 0 },
};

superres_upscale_frame() {
  if (upscaled_width == crop_width) {
    // We aren't upscaling. Just copy buffers across
    for (plane = 0; plane < get_num_planes(); plane++) {
      ssx = plane_subsampling_x[plane]
      ssy = plane_subsampling_y[plane]
      planeW = ROUND_POWER_OF_TWO(crop_width, ssx)
      planeH = ROUND_POWER_OF_TWO(crop_height, ssy)

      for (y = 0; y < planeH; y++)
        for (x = 0; x < planeW; x++)
          upscaled_frame[plane][x][y] = frame[plane][x][y]
      for (y = 0; y < planeH; y++)
        for (x = 0; x < planeW; x++)
          upscaled_cdef_frame[plane][x][y] = cdef_frame[plane][x][y]
    }
#if VALIDATE_SPEC_UPSCALING
    for (t = 0; t < 2; t++) {
        for (plane = 0; plane < get_num_planes(); plane++) {
            ssx = plane_subsampling_x[plane]
            ssy = plane_subsampling_y[plane]
            planeW = ROUND_POWER_OF_TWO(crop_width, ssx)
            planeH = ROUND_POWER_OF_TWO(crop_height, ssy)
            for (i = 0; i < planeH; i++) {
                for (j = 0; j < planeW; j++) {
                    validate(110001)
                    validate(plane)
                    validate(j)
                    validate(i)
                    if ( t == 0 )
                        validate( upscaled_cdef_frame[ plane ][ j ][ i ] )
                    else
                        validate( upscaled_frame[ plane ][ j ][ i ] )
                }
            }
        }
    }
#endif
    return 0
  }

  for (plane = 0; plane < get_num_planes(); plane++) {
    ssx = plane_subsampling_x[plane]
    ssy = plane_subsampling_y[plane]
    downscaledCropW = ROUND_POWER_OF_TWO(crop_width, ssx)
    upscaledPlaneW = ROUND_POWER_OF_TWO(upscaled_width, ssx)
    ASSERT(upscaledPlaneW > downscaledCropW, "Invalid plane upscaled downscaled combination")
    planeH = ROUND_POWER_OF_TWO(upscaled_height, ssy)

    stepX = ((downscaledCropW << SUPERRES_SCALE_BITS) + (upscaledPlaneW / 2)) / upscaledPlaneW
    // Work out what sub-pixel position we should start sampling from.
    // Note: This is closely linked to the downscaler used in the reference code,
    // in the sense that the overall process:
    // (source) -> (downscale at encoder) --------> (upscale at decoder) -> (output)
    // must not introduce an overall sub-pixel shift.
    // Such a shift wouldn't really hurt human perception, but would cause a huge
    // drop in PSNR because that is based on differences of pixel values.
    err = (upscaledPlaneW * stepX) - (downscaledCropW << SUPERRES_SCALE_BITS)
    initialSubpelX =
      (-((upscaledPlaneW - downscaledCropW) << (SUPERRES_SCALE_BITS - 1)) + upscaledPlaneW / 2) / upscaledPlaneW +
      (1 << (SUPERRES_EXTRA_BITS - 1)) - err / 2
    initialSubpelX &= SUPERRES_SCALE_MASK

    // Upscale each tile column separately
    downscaledPlaneW = ALIGN_POWER_OF_TWO(downscaledCropW, 3-ssx)
    for (i = 0; i < tile_cols; i++) {
      fullTileX1 = mi_col_starts[i+1] << (MI_SIZE_LOG2 - ssx) // Un-clamped end of the current tile
      downscaledX0 = mi_col_starts[i] << (MI_SIZE_LOG2 - ssx)
      downscaledX1 = Min(fullTileX1, downscaledPlaneW)
      upscaledX0 = (downscaledX0 * superres_denom) / SUPERRES_NUM

      // Calculate the region of X pixels we're allowed to sample from
        minX = 0
        maxX = downscaledPlaneW - 1

      if (i == tile_cols - 1) {
        // Important: We really do need to special case this; here's an example of
        // how using the obvious option 'upscaledX1 = Min(upscaledPlaneW, ...)' can go wrong:
        //
        // upscaled_width = 275, superres_denom = 11, subsampling_x = 1
        // => crop_width = 200, mi_cols = 50, and upscaledPlaneW = 138.
        // Then, for the last tile column, in the U/V plane, we have:
        // fullTileX1 = 50 * 2 = 100
        // => (fullTileX1 * superres_denom) / SUPERRES_NUM = 137
        // This is less than upscaledPlaneW, so we wouldn't end up filling enough pixels
        // if we followed the obvious alternative approach.
        upscaledX1 = upscaledPlaneW
      } else {
        upscaledX1 = (fullTileX1 * superres_denom) / SUPERRES_NUM
      }

      for (y = 0; y < planeH; y++) {
        for (x = upscaledX0; x < upscaledX1; x++) {
          srcX = -(1 << SUPERRES_SCALE_BITS) + initialSubpelX + x*stepX // full position, q14
          srcXPx = (srcX >> SUPERRES_SCALE_BITS) // integer part of position
          srcXSubpel = (srcX & SUPERRES_SCALE_MASK) >> SUPERRES_EXTRA_BITS // subpel position, q6

          sum = 0
          for (k = 0; k < SUPERRES_FILTER_TAPS; k++) {
            sampleX = clamp(srcXPx + (k - SUPERRES_FILTER_OFFSET), minX, maxX)
            px = frame[plane][sampleX][y]
            sum += px * resize_filter_normative[srcXSubpel][k]
          }
          upscaled_frame[plane][x][y] = clip_pixel(ROUND_POWER_OF_TWO(sum, FILTER_BITS))
        }
      }

      // Note: Ref code only applies the upscaler in this form to the CDEF-ed frame
      :log_upscale(b, downscaledX0, downscaledX1, planeH, plane, i, 0, b.video_stream.frame_data.rcdef_frame[plane])
      :C log_upscale(b, downscaledX0, downscaledX1, planeH, b->video_stream.frame_data.rcdef_frame[plane])

      for (y = 0; y < planeH; y++) {
        for (x = upscaledX0; x < upscaledX1; x++) {
          srcX = -(1 << SUPERRES_SCALE_BITS) + initialSubpelX + x*stepX // full position, q14
          srcXPx = (srcX >> SUPERRES_SCALE_BITS) // integer part of position
          srcXSubpel = (srcX & SUPERRES_SCALE_MASK) >> SUPERRES_EXTRA_BITS // subpel position, q6

          sum = 0
          for (k = 0; k < SUPERRES_FILTER_TAPS; k++) {
            sampleX = clamp(srcXPx + (k - SUPERRES_FILTER_OFFSET), minX, maxX)
            px = cdef_frame[plane][sampleX][y]
            sum += px * resize_filter_normative[srcXSubpel][k]
          }
          upscaled_cdef_frame[plane][x][y] = clip_pixel(ROUND_POWER_OF_TWO(sum, FILTER_BITS))
#if VALIDATE_SPEC_UPSCALING_INTERNALS
          validate(110002)
          validate(upscaled_cdef_frame[plane][x][y])
#endif
        }
      }

      :log_upscale(b, upscaledX0, upscaledX1, planeH, plane, i, 1, b.video_stream.frame_data.rupscaled_cdef_frame[plane])
      :C log_upscale(b, upscaledX0, upscaledX1, planeH, b->video_stream.frame_data.rupscaled_cdef_frame[plane])
    }
  }
#if VALIDATE_SPEC_UPSCALING
  for (t = 0; t < 2; t++) {
      for (plane = 0; plane < get_num_planes(); plane++) {
          ssx = plane_subsampling_x[plane]
          ssy = plane_subsampling_y[plane]
          upscaledPlaneW = ROUND_POWER_OF_TWO(upscaled_width, ssx)
          planeH = ROUND_POWER_OF_TWO(upscaled_height, ssy)
          for (i = 0; i < planeH; i++) {
              for (j = 0; j < upscaledPlaneW; j++) {
                  validate(110000)
                  validate(plane)
                  validate(j)
                  validate(i)
                  if ( t == 0 )
                      validate( upscaled_cdef_frame[ plane ][ j ][ i ] )
                  else
                      validate( upscaled_frame[ plane ][ j ][ i ] )
              }
          }
      }
  }
#endif
}

#if DECODE
calculate_rates() {
  uint64 uncompressedSize
  uint64 compressedSize
  uint64 compressedRatio_x100

  oppoint = operating_point_level[current_operating_point_id]
  compactedLevel = compact_level[oppoint]
  if ( DisplayedLumaSamplesInTU > 0 ) {
      maxDisplayRate_x100 = ( 100 * level_max_luma_rate[compactedLevel][0] * FramesDisplayedInTU ) / DisplayedLumaSamplesInTU
      min_max_display_rate_x100 = Min(min_max_display_rate_x100, maxDisplayRate_x100)
  }

  if ( LumaSamplesInTU > 0 ) {
      maxDecodeRate_x100 = ( 100 * level_max_luma_rate[compactedLevel][1] * FramesDecodedInTU ) / LumaSamplesInTU
      min_max_decode_rate_x100 = Min(min_max_decode_rate_x100, maxDecodeRate_x100)
  }

  if (!still_picture && !show_existing_frame ) {
    if (BytesInFrame > 128) {
      if (profile == 0) {
        picSizeProfileFactor = 15
      } else if (profile == 1) {
        picSizeProfileFactor = 30
      } else {
        picSizeProfileFactor = 36
      }
      chromaOverhead = ( monochrome == 1 ) ? 0 : ( 8 >> ( subsampling_x + subsampling_y ) )

      // Calculate sizes in bytes
      uncompressedSize = (crop_width * crop_height * picSizeProfileFactor) >> 3
      compressedSize = Max (BytesInFrame - 128, 1)

      compressedRatio_x100 = ((100 * uncompressedSize) / compressedSize)
      min_comp_ratio_x100 = Min(compressedRatio_x100, min_comp_ratio_x100)
    }
  }
}
#endif // DECODE
