/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

//#line 2 "av1_mid.c"
#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <cmath>

#include "av1_mid.h"

//#define LOG_CHOOSE
//#define LOG_WRITE

#include <stdarg.h>
/* Something is fatally wrong with this stream */
void ASSERT(int cond,char *msg, ...) {
  if (cond)
    return;
  va_list argptr;
  va_start(argptr, msg);
  vprintf(msg, argptr);
  va_end(argptr);
  printf("\n");
  printf("Unable to continue decoding\nEXITING\n");
  exit(-1);
  assert(0);
}

// this bool is set if a check failed in debugmode and will cause us
// to exit with an error status at the end.
bool some_check_failed;

// Something is wrong according to the specification, but we think we
// can recover safely
void CHECK (int cond, const char *msg, ...) {
  if (cond)
    return;

  if (debugMode) {
    printf("Check Failed: ");
    va_list argptr;
    va_start(argptr, msg);
    vprintf(msg, argptr);
    va_end(argptr);
    printf("\n");

    some_check_failed = true;
  }
}

/*
OPT convert to packaging in integers
OPT optimise read_f
*/

#if ENCODE

static unsigned long next = 14;

int myrand(void) {
// We only use myrand when checking C and assembler produce the same streams
#ifdef USE_MYRAND
#define MYRAND_MAX 2147483647
   next = (next * 1103515245 + 12345) & 0xFFFFffff;
   int val = (int)(next & 0x7FFFFFFF); // Extract the low 31 bits
   //printf("%d\n",val);
   return val;
#else // USE_MYRAND
#define MYRAND_MAX RAND_MAX
   return rand();
#endif // USE_MYRAND
}

void mysrand(unsigned seed) {
   next = seed;
}

#define MAX_DATA_BUFFER 500000000
unsigned char g_data[MAX_DATA_BUFFER];
// Max frame size is 8192*4320 ~= 32M pixels.
// Allow ~4 bytes per pixel at this maximum size
#define MAX_NESTED_BUFFER 128000000
#define MAX_NESTS 1
unsigned char g_nested_data[MAX_NESTS][MAX_NESTED_BUFFER];
struct data_s g_data_groups[MAX_NESTS+1];

#ifdef LOG_WRITE
static int log_counters [MAX_NESTS];
#endif // LOG_CHOOSE

void encode_bitstream_init_data_group(TOPLEVEL_T *b, int nest) {
  b->data[nest].data = (nest==0) ? g_data : g_nested_data[nest-1];
  b->data[nest].len = 0; // This maintains the occupied length of data
  b->data[nest].bitpos = 7;
  b->data[nest].num = 0;
}

void encode_bitstream_init(TOPLEVEL_T *b) {
  b->data = g_data_groups;
  b->nest = 0;
  for (int i=0; i<=MAX_NESTS; i++) {
    encode_bitstream_init_data_group(b, i);
  }
}

uint64_t encode_bitstream_current_position(TOPLEVEL_T *b) {
  return b->data[b->nest].len;
}

int encode_bitstream_set_pos(TOPLEVEL_T *b, int pos) {
  assert(pos <= b->data[b->nest].len && pos >= 0);
  b->data[b->nest].len = pos;
  return 0;
}

uint64_t encode_bitstream_current_bitposition(TOPLEVEL_T *b) {
  return (((uint64_t)b->data[b->nest].len)<<3)+(7-b->data[b->nest].bitpos);
}

int encode_bitstream_unread_bits(TOPLEVEL_T *b, int n) {
  uint64_t full_bitpos = encode_bitstream_current_bitposition(b);
  uint64_t adj_bitpos = (uint64_t)n > full_bitpos ? 0 : (full_bitpos - n);

  // Save the current partial byte, in case we un-read to an earlier
  // point in the same byte
  b->data[b->nest].data[b->data[b->nest].len] = b->data[b->nest].num;

  b->data[b->nest].bitpos = 7 - (adj_bitpos & 7);
  b->data[b->nest].len = adj_bitpos >> 3;
  // Load the new value of 'num'.
  // Note that we need to clear out any bits which have been "un-written"
  int mask = (1 << (b->data[b->nest].bitpos+1)) - 1;
  b->data[b->nest].num = b->data[b->nest].data[b->data[b->nest].len] & ~mask;
  return 0;
}

int encode_bitstream_byte_aligned(TOPLEVEL_T *b) {
  return b->data[b->nest].bitpos==7;
}

int encode_bitstream_start_nest(TOPLEVEL_T *b) {
  assert(encode_bitstream_byte_aligned(b));
  b->nest++;
  assert(b->nest <= MAX_NESTS);
  encode_bitstream_init_data_group(b, b->nest);
#ifdef LOG_WRITE
  log_counters[b->nest] = 0;
#endif
  return 0;
}

// Copy from nest level 'from' into the current nest level
int encode_bitstream_copy_from_nest(TOPLEVEL_T *b, int from_lvl, uint64_t pos, int sz) {
#ifdef LOG_WRITE
  printf (" [Copy %d bytes from nest level %d (position %" PRIu64 ".7)]\n",
          sz, from_lvl, pos);
#endif
  assert(b->nest >= 0);
  assert(b->nest < from_lvl);
  assert((uint64_t)b->data[from_lvl].len >= (sz + pos));
  assert(b->data[b->nest].bitpos == 7);
  assert(b->data[b->nest].num == 0);

  memcpy(&b->data[b->nest].data[b->data[b->nest].len], &b->data[from_lvl].data[pos], sz);
  b->data[b->nest].len += sz;
  return 0;
}

int encode_bitstream_end_nest(TOPLEVEL_T *b) {
  b->nest--;
  assert(b->nest >= 0);
  return 0;
}

// Copy data from the current nest level into a side buffer
int encode_bitstream_copy_to_buffer(TOPLEVEL_T *b, uint8_t *buf, int pos, int sz) {
  assert(b->data[b->nest].len >= (sz + pos));
  memcpy(buf, &b->data[b->nest].data[pos], sz);
  return 0;
}

// Copy data from a side buffer to the current nest level
int encode_bitstream_copy_from_buffer(TOPLEVEL_T *b, uint8_t *buf, int sz) {
  assert(b->data[b->nest].bitpos == 7);
  assert(b->data[b->nest].num == 0);
  memcpy(&b->data[b->nest].data[b->data[b->nest].len], buf, sz);
  b->data[b->nest].len += sz;
  return 0;
}

// Helper: How many bits do we get per call to myrand()?
#if MYRAND_MAX == 0x7FFF
# define BITS_PER_RAND 15
#elif MYRAND_MAX == 2147483647
# define BITS_PER_RAND 31
#else
# error "Unexpected value of MYRAND_MAX"
#endif

uint64 builtin_ChooseBits(int n) {
  assert(n>=0);
  assert(n<=64);
#ifdef LOG_CHOOSE
  printf("b %d",n);
#endif // LOG_CHOOSE
  uint64 y = 0;
  int shift = 0;
  while (n) {
    int choose = (n > BITS_PER_RAND) ? BITS_PER_RAND : n;
    uint64 x = myrand() & ((((uint64)1)<<choose)-1);
    y |= (x << shift);
    n -= choose;
    shift += choose;
  }
#ifdef LOG_CHOOSE
  printf(" - %lu\n", y);
#endif // LOG_CHOOSE
  return y;
}

int builtin_Choose (int low,int high) {
  assert (high >= low);
  if (low == high) return low;
  uint64 rnd = (high - low <= MYRAND_MAX) ? myrand() : builtin_ChooseBits (31);
  int x = low + (rnd % (1 + high - low));
#ifdef LOG_CHOOSE
  printf("c %d %d - %d\n", low, high, x);
#endif // LOG_CHOOSE
  return x;
}

int builtin_ChooseSignedBits(int n) {
  int x;
  x = builtin_ChooseBits(n);
  if (myrand()&1) {
    x = -x;
  }
#ifdef LOG_CHOOSE
  printf("s %d - %d\n",n,x);
#endif // LOG_CHOOSE
  return x;
}

int builtin_ChooseSignedBits2(int n) {
  return builtin_Choose(-((1<<n)),(1<<n)-1);
}

int builtin_ChooseBit(void) {
  int x = myrand()&1;
#ifdef LOG_CHOOSE
  printf("bit - %d\n",x);
#endif // LOG_CHOOSE
  return x;
}

int builtin_ChoosePowerLaw(int high) {
  // Choose a random number in [1, high], using a power-law distribution p(x) ~ x^-2
  double z = myrand() / (((double)MYRAND_MAX)+1.0); // Uniformly distributed over [0, 1)
  double x = 1. / (1. - z);
  return (int)fmin(x, (double)high);
}

#if ENCODE

#include <vector>
#include <unordered_map>
#include <random>
#include <algorithm>
#include <fstream>

class CChooseAllSequence {
public:
  int low;
  int high;
  std::vector<int> seq;

public:
  CChooseAllSequence(int low, int high);
};

std::unordered_map<std::string,CChooseAllSequence> chooseAllSequences;

CChooseAllSequence::CChooseAllSequence(int low, int high):
low(low),
high(high)
{}

// Similar to std::shuffle / random.shuffle, except using a known algorithm
// and calling myrand(). This allows us to have exactly matching
// C and Python implementations.
void my_shuffle(std::vector<int>& array) {
  int n = array.size();
  // Use the Fisher-Yates shuffle algorithm:
  // For each i = 0, ..., n-2 (inclusive), select j in i, ..., n-1 (inclusive)
  // and exchange elements i and j.
  for (int i = 0; i < n-1; i++) {
    int j = builtin_Choose(i, n-1);
    int tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;
  }
}

int builtin_ChooseAll(int low, int high, std::string name) {
  //Choose a number in the given range from a random permutation of all possible values, eventually returning all of them."""
  auto it=chooseAllSequences.find(name);
  if (it==chooseAllSequences.end()) {
    chooseAllSequences.insert(std::pair<std::string,CChooseAllSequence>(name, CChooseAllSequence(low, high)));
    CChooseAllSequence &seq=chooseAllSequences.at(name);
    for (int k=seq.low; k<=seq.high; k++)
      seq.seq.push_back(k);
    my_shuffle(seq.seq);
  }
  CChooseAllSequence &seq=chooseAllSequences.at(name);
  if (seq.low>low) {
    for (int k=low; k<seq.low; k++)
      seq.seq.push_back(k);
    my_shuffle(seq.seq);
    seq.low=low;
  }
  if (seq.high<high) {
    for (int k=seq.high+1; k<=high; k++)
      seq.seq.push_back(k);
    my_shuffle(seq.seq);
    seq.high=high;
  }
  for (auto it=seq.seq.begin(); it!=seq.seq.end(); ++it) {
    int v=*it;
    if (v>=low && v<=high) {
      seq.seq.erase(it);
      return v;
    }
  }
  return builtin_Choose(low,high);
}

#if !RELEASE
void loadChooseAll(const char *filename) {
  if (debugMode) printf("Loading ChooseAll data...\n");
  std::ifstream f(filename, std::ios::in|std::ios::binary);
  if (!f.good()) return;
  int totalSequences;
  f.read(reinterpret_cast<char *>(&totalSequences), sizeof(int));
  int count=0;
  for (int i=0; i<totalSequences; i++) {
    std::string name;
    std::getline(f, name, '\0');

    int low, high;
    f.read(reinterpret_cast<char *>(&low), sizeof(int));
    f.read(reinterpret_cast<char *>(&high), sizeof(int));
    chooseAllSequences.insert(std::pair<std::string,CChooseAllSequence>(name, CChooseAllSequence(low, high)));
    CChooseAllSequence &c=chooseAllSequences.at(name);
    int byteCount=(high+8-low)>>3;
    int v=low;

    for (int j=0; j<byteCount; j++) {
      unsigned char bm;
      bm=f.get();
      for (int k=0; k<8; k++,v++) {
        if (bm&(1<<k)) {
          c.seq.push_back(v);
          count++;
        }
      }
    }
    std::random_shuffle(c.seq.begin(), c.seq.end());
  }
  if (debugMode) printf("Done loading - %d values added to %d sequences.\n", count, totalSequences);
}

void saveChooseAll(const char *filename) {
  if (debugMode) printf("Saving ChooseAll data...\n");
  std::ofstream f(filename, std::ios::out|std::ios::binary|std::ios::trunc);
  if (!f.good()) {
    printf("Can't write ChooseAll state file %s!\n", filename);
    return;
  }
  int totalSequences=chooseAllSequences.size();
  f.write(reinterpret_cast<char *>(&totalSequences), sizeof(int));
  for (auto it=chooseAllSequences.begin(); it!=chooseAllSequences.end(); ++it) {
    f << it->first << std::ends;
    CChooseAllSequence &c=it->second;
    f.write(reinterpret_cast<char *>(&c.low), sizeof(int));
    f.write(reinterpret_cast<char *>(&c.high), sizeof(int));
    if (debugMode) printf("Saving %s: seq.size() %lu\n",
                          it->first.c_str(),
                          (unsigned long) c.seq.size());
    std::sort(c.seq.begin(), c.seq.end());
    int byteCount=(c.high+8-c.low)>>3;
    auto seqIt=c.seq.begin();
    int v=c.low;
    for (int j=0; j<byteCount; j++) {
      unsigned char bm=0;
      if (seqIt!=c.seq.end()) {
        for (int k=0; k<8; k++,v++) {
          bm>>=1;
          if ((*seqIt)==v) {
            bm|=0x80;
            if ((++seqIt)==c.seq.end()) {
              bm>>=7-k;
              break;
            }
          }
        }
      }
      f.put(bm);
    }
  }
  if (debugMode) printf("Done saving.\n");
}
#endif // !RELEASE
#endif

void encode_bitstream_writebit(TOPLEVEL_T *b,int bit) {
  assert (0<=bit && bit<=1);
  assert (b->data[b->nest].len < MAX_DATA_BUFFER - 1);
  b->data[b->nest].num |= bit << b->data[b->nest].bitpos;
  if (b->data[b->nest].bitpos>0) {
    b->data[b->nest].bitpos -= 1;
  } else {
    b->data[b->nest].bitpos = 7;
    b->data[b->nest].data[b->data[b->nest].len++] = b->data[b->nest].num;
    b->data[b->nest].num = 0;
  }
}

static void log_write (const TOPLEVEL_T *b,
                       const char       *sym,
                       const char       *type,
                       unsigned          width,
                       int64_t           val)
{
#ifdef LOG_WRITE
# ifndef LOG_WRITE_ZERO_WIDTH
  if (! width)
    return;
# endif
  if (! sym)
    return;

  int nest = b->nest;

  printf("%8d[%d] (%6d.%d) %60s %2s%-2u: %8" PRId64 "\n",
         log_counters [nest], nest,
         b->data[nest].len, b->data[nest].bitpos,
         sym, type, width, val);

  ++ log_counters [nest];
#else
  (void) b;
  (void) sym;
  (void) type;
  (void) width;
  (void) val;
#endif
}

void encode_bitstream_write_f(TOPLEVEL_T *b,uint64 val,int num, int sign, const char *sym) {
  log_write (b, sym, "f", num, sign ? -val : val);

  if (num==0) {
    return;
  }
  assert(num>0);
  assert(num<=64); // exceeds size of argument
  assert(sign || val <= ((((uint64)1)<<num)-1)); // oversize val
  for(int i=0;i<num;i++) {
    encode_bitstream_writebit(b,(val>>(num-1-i))&1);
  }
}

void encode_bitstream_write_u(TOPLEVEL_T *b,uint64 val,int num, const char *sym) {
  log_write (b, sym, "u", num, (int64_t) val);
  encode_bitstream_write_f (b, val, num, 0,  NULL);
}

void encode_bitstream_write_s(TOPLEVEL_T *b,int val,int num, const char *sym) {
  log_write (b, sym, "s", num, val);
  int a = val<0 ? -val : val;
  encode_bitstream_write_f(b, a, num, 0, NULL);
  if (val>=0)
    encode_bitstream_write_f(b,0,1, 0, NULL);
  else
    encode_bitstream_write_f(b,1,1, 0, NULL);
}

void encode_bitstream_write_su(TOPLEVEL_T *b,int64_t val,int num, const char *sym) {
  log_write (b, sym, "su", num, val);
  encode_bitstream_write_f(b, val, num+1, 1, NULL);
}

void encode_bitstream_write_tu(TOPLEVEL_T *b,int val,int cMax, const char *sym) {
  log_write (b, sym, "tu", std::min (val, cMax), val);
  // write 111110, to give value 0->cMax
  int x = 0;
  while (x<cMax) {
    if (x==val) {
      encode_bitstream_writebit(b,0);
      break;
    }
    encode_bitstream_writebit(b,1);
    x += 1;
  }
}

void encode_bitstream_write_le(TOPLEVEL_T *b,uint32 v,int num, const char *sym) {
  log_write (b, sym, "le", num, v);
  assert(num==16 || num==32 || num==24 || num==8);
  int a = v&255;
  int b2 = (v>>8)&255;
  int c = (v>>16)&255;
  int d = (v>>24)&255;
  if (num==8) {
    v = a;
  } else if (num==16) {
    v = b2+(a<<8);
  } else if (num==24) {
    v = c+(b2<<8)+(a<<16);
  } else { // num==32
    v = d+(c<<8)+(b2<<16)+(a<<24);
  }
  encode_bitstream_write_f(b,v,num, 0, NULL);
}

// VP9 extends streams if the last byte has has 110x_xxxx pattern
int encode_bitstream_needs_byte_extensions(TOPLEVEL_T *b) {
  return (b->data[b->nest].len>0 && (b->data[b->nest].data[b->data[b->nest].len-1]&0xe0) == 0xc0);
}

int encode_bitstream_getbit(TOPLEVEL_T *b, uint64_t loc) {
  // Read a bit from a given bitposition
  int pos = loc>>3;
  int bitpos = (loc&7);
  int bitshift = (7-bitpos);
  int w=0;
  if (pos==b->data[b->nest].len)
    w = b->data[b->nest].num;
  else
    w = b->data[b->nest].data[pos];
  return (w>>bitshift)&1;
}

int encode_bitstream_overwritebit(TOPLEVEL_T *b, uint64 loc, int bit) {
  // Write <bit> at bit location <loc>
  int pos = loc>>3;
  int bitpos = (loc&7);
  int bitmask = 1<<(7-bitpos);
  if (bit) {
    if (pos==b->data[b->nest].len)
      b->data[b->nest].num |= bitmask;
    else
      b->data[b->nest].data[pos] |= bitmask;
  } else {
    if (pos==b->data[b->nest].len)
      b->data[b->nest].num &= ~bitmask;
    else
      b->data[b->nest].data[pos] &= ~bitmask;
  }
  return 0;
}

int encode_bitstream_overwrite(TOPLEVEL_T *b, uint64 loc, int numbits, int value) {
  // Write numbits at bit location loc
  for(int i=0;i<numbits;i++) {
    encode_bitstream_overwritebit(b,loc+i,(value>>(numbits-1-i))&1);
  }
  return 0;
}

int encode_bitstream_addcarry(TOPLEVEL_T *b) {
  // Add a 1 to the last emitted bit, may need to propogate the carry
  uint64_t loc = encode_bitstream_current_bitposition(b)-1;
  while (encode_bitstream_getbit(b,loc)==1) {
    encode_bitstream_overwritebit(b,loc,0);
    loc-=1;
  }
  encode_bitstream_overwritebit(b,loc,1);
  return 0;
}

#else // ENCODE

void bitstream_init(TOPLEVEL_T *b, const uint8 *data, int len) {

    // This cast is safe: we never trash the contents of data, but the
    // structure is defined in auto-generated code so doesn't have a
    // const flag.
    b->data.data = const_cast<uint8 *> (data);

    b->data.len=len;
    b->data.pos=0;
    b->data.bitpos=7;
    b->data.num=*data;
    b->data.chunk_end=-1;
}

int bitstream_set_pos(TOPLEVEL_T *b, int pos) {
  assert(pos <= b->data.len);
  b->data.bitpos=7;
  b->data.pos = pos;
  b->data.num = (pos == b->data.len) ? 0 : b->data.data[b->data.pos];
  return 0;
}

int bitstream_set_bool_length(TOPLEVEL_T *b, int len) {
  b->data.chunk_end = (len > 0) ? b->data.pos + len : -1;
  assert((b->data.chunk_end == -1) || (b->data.chunk_end <= b->data.len));
  return 0;
}

// Count the number of bytes (including emulation prevention bytes) from pos to here
int bitstream_count_data_decode(TOPLEVEL_T *b,int pos) {
  ASSERT(b->data.bitpos == 7,"Entry points must be byte aligned");
  int numzeros = 0;
  int num = 0;
  int di;
  for(di=pos;di<b->data.pos;di++) {
    uint8 d=b->data.data[di];
    if (numzeros==2 && d<=3) {
      num+=1;
      numzeros=0;
    }
    if (d==0)
      numzeros += 1;
    else
      numzeros = 0;
    num +=1;
  }
  return num;
}

int bitstream_numbytes(TOPLEVEL_T *b) {
  return b->data.len;
}

uint64_t bitstream_current_position(TOPLEVEL_T *b) {
  return b->data.pos;
}

uint64_t bitstream_current_bitposition(TOPLEVEL_T *b) {
  return (((uint64_t)b->data.pos)<<3)+(7-b->data.bitpos);
}

int bitstream_unread_bits(TOPLEVEL_T *b, int n) {
  uint64_t full_bitpos = bitstream_current_bitposition(b);
  uint64_t adj_bitpos = (uint64_t)n > full_bitpos ? 0 : (full_bitpos - n);
  b->data.bitpos = 7 - (adj_bitpos & 7);
  b->data.pos = adj_bitpos >> 3;
  b->data.num = b->data.data[b->data.pos];
  return 0;
}

int bitstream_peek_last_bytes(TOPLEVEL_T *b, int size, uint8_t*out) {
  for (int i=0; i<size; i++) {
    out[i] = b->data.data[b->data.pos - (size-1) + i];
  }
  return 0;
}

int bitstream_readbit(TOPLEVEL_T *b) {
  int bp = b->data.bitpos;
  int x=(b->data.num>>bp)&1;
  assert((b->data.chunk_end == -1) || (b->data.pos < b->data.chunk_end));
  if (bp>0) {
      b->data.bitpos=bp-1;
  } else {
    b->data.bitpos=7;
    b->data.pos+=1;
    if (b->data.pos<b->data.len) {
        b->data.num=b->data.data[b->data.pos];
    } else {
       b->data.num = 0; // This data should never actually be used
    }
  }
  return x;
}

int bitstream_more_rbsp_trailing_data(TOPLEVEL_T *b) {
  return b->data.pos<b->data.len-1;
}

int bitstream_more_rbsp_data(TOPLEVEL_T *b) {
  // Return 1 unless got to the stop bit in rbsp_trailing_bits
  if (b->data.pos<b->data.len-1)
      return 1;
  int t=0;
  for (int i=0;i<=b->data.bitpos;i++)
  {
      if (b->data.num&(1<<i))
          t+=1;
  }
  return t>1; // i.e. if only spot trailing bit, then say that we have finished
}

int bitstream_payload_extension_present(TOPLEVEL_T *b, int startpos, int payloadSize) {
  // Return 1 unless got to the stop bit in sei_payload
  if (b->data.pos-startpos<payloadSize-1)
      return 1;
  int t=0;
  for (int i=0;i<=b->data.bitpos;i++)
  {
      if (b->data.num&(1<<i))
          t+=1;
  }
  return t>1; // i.e. if only spot trailing bit, then say that we have finished
}

int bitstream_payload_extension_size(TOPLEVEL_T *b, int startpos, int payloadSize) {
  int extSize=0;
  //First add up everything before the last byte
  if (b->data.pos-startpos<payloadSize-1) {
      //Do the partial byte first
      extSize=b->data.bitpos+1;
      //Then do any full bytes
      if (b->data.pos-startpos<payloadSize-2) {
          extSize+=(payloadSize+startpos-b->data.pos-2)*8;
      }
  }
  //Now handle the last byte
  int lastByte=b->data.data[startpos+payloadSize-1];
  int lastOneBit;
  for (int x=0; x<8; x++) {
      lastOneBit=x;
      if ((lastByte>>x)&1)
          break;
  }
  //If we're IN the last byte we need to take our bitpos into account
  if ((b->data.pos-startpos)==(payloadSize-1)) {
      extSize=b->data.bitpos-lastOneBit;
  }
  //Otherwise we need to add all the available bits in the last byte
  else {
      extSize+=7-lastOneBit;
  }
  return extSize;
}

uint64 bitstream_read_f(TOPLEVEL_T *b,int num) {
  if (num==0)
      return 0;
  assert(num<=64); // exceeds size of return value
  uint64 t=0;
  for (int i=0;i<num;i++)
      t=(t<<1)+bitstream_readbit(b);
  return t;
}

int bitstream_next_bits(TOPLEVEL_T *b,int num) {
  int a=b->data.pos;
  int n=b->data.num;
  int c=b->data.bitpos;
  int x=bitstream_read_f(b,num);
  b->data.pos=a;
  b->data.num=n;
  b->data.bitpos=c;
  return x;
}

uint64 bitstream_read_u(TOPLEVEL_T *b,int num) {
  return bitstream_read_f(b,num);
}

uint8 bitstream_read_b(TOPLEVEL_T *b,int num) {
  uint8 x=b->data.num;
  assert((b->data.chunk_end == -1) || (b->data.pos < b->data.chunk_end));
  if (b->data.pos<b->data.len-1) {
      b->data.pos+=1;
      b->data.num=b->data.data[b->data.pos];
  }
  return x;
}

uint32 bitstream_read_ue(TOPLEVEL_T *b,int v) {
  // Unsigned Exp-Golomb as in 9.2
  int numzeros = 0;
  while (!bitstream_readbit(b) && numzeros<32)
      numzeros += 1;
  uint32 x = (1<<numzeros)+bitstream_read_f(b,numzeros)-1;
  return x;
}

int32 bitstream_read_se(TOPLEVEL_T *b,int v) {
  // Signed Exp-Golomb as in 9.2
  int x = bitstream_read_ue(b,v);
  if (x==0)
      return x;
  if (x&1)
      v = (x+1)>>1;
  else
      v = -(x>>1);
  return v;
}

int32 bitstream_read_le(TOPLEVEL_T *b,int num) {
  assert(num==16 || num==32 || num==24 || num==8);
  // Signed Exp-Golomb as in 9.2
  int v = bitstream_read_f(b,num);
  int  a = v&255;
  int  b2 = (v>>8)&255;
  int  c = (v>>16)&255;
  int  d = (v>>24)&255;
  if (num==16) {
    return b2+(a<<8);
  } else if (num==24) {
    return c+(b2<<8)+(a<<16);
  } else if (num==8) {
    return a;
  } else {
    return d+(c<<8)+(b2<<16)+(a<<24);
  }
}

int32 bitstream_read_s(TOPLEVEL_T *b,int num) {
  int x = bitstream_read_f(b,num);
  if (bitstream_readbit(b))
      return -x;
  return x;
}

int32 bitstream_read_su(TOPLEVEL_T *b,int num) {
  int v = bitstream_read_f(b,num+1);
  int sign_bit = 1 << num;
  return (v & (sign_bit - 1)) - (v & sign_bit);
}

// Read 111110, to give value 0->cMax
int32 bitstream_read_tu(TOPLEVEL_T *b,int cMax) {
  int x;
  for(x=0;x<cMax;x++) {
    if (!bitstream_readbit(b))
      break;
  }
  return x;
}

int bitstream_needs_byte_extensions(TOPLEVEL_T *b) {
  if (b->data.pos-1<b->data.len) {
    return (b->data.data[b->data.pos-1]&0xe0) == 0xc0;
  } else {
    printf("Overflowed bit buffer\n");
    return 0;
  }
}

int bitstream_byte_aligned(TOPLEVEL_T *b) {
  // Return if stream is aligned
  return (b->data.bitpos==7);
}

#endif



/*Edit this section to your taste: ********************/
static int check_bins             = 1;  //auto checking: (In dkboolreader.c LOG_CABAC)
static int check_mvs              = 1;  //auto checking: (In decodemv.c )
static int check_recon            = 1;  //auto checking: (In decodeframe.c LOG_PRED and reconintra ARGON_LOG_ALL)
static int track_intra_neighbours = 1;
static int check_loop             = 1;  //auto checking: (in loopfilter.c LOG_LOOP), superblock level loopfilter checking
static int check_cdef             = 1;  //auto checking: 64x64 level cdef checking
static int check_upscale          = 1; // auto checking: Check before/after for each upscaled tile
static int check_lr               = 1; // auto checking: Processing-unit-level loop-restoration checking
static int check_coeffs           = 1; // auto checking: (In decodeframe.c LOG_COEFFS)
// this doesn't work since the change to calculate refmvs on the fly: https://aomedia-review.googlesource.com/c/aom/+/43401
static int check_mfmv             = 0;  //auto checking: (in mvref_common.c LOG_MFMV)
static int check_output           = 1; // auto checking: Check each output frame before (0) and after (1) film grain synthesis
static int check_anchor_ref       = 1; // auto checking: Check the anchor frame loaded for each tile decoded in ext_tile
static int check_tile_output      = 1; // auto checking: Check the tile output is right

static const std::string pathToRef = "aom/build/";
/******************************************************/

int g_checkStreams=0;

void log_daala_stop() {
  printf("Stop\n");
}

static int cabac_bins_count = 0;
static std::ifstream refCabac;

void debug_cabac(const std::string &s) {
  assert (check_bins && g_checkStreams);

  if (check_bins < 2) {
    std::string f = pathToRef;
    refCabac.open(f.append("ref_cabac2.txt"));
    check_bins++;
  }
  if(!refCabac.is_open()) {
    std::cerr << "Ref cabac file open failure" << std::endl;
    exit(-1);
  }

  std::string s2;
  if(!std::getline(refCabac, s2)) {
    std::cerr << "Ref cabac file ended prematurely" << std::endl;
    exit(-1);
  }

  if (0 != s.compare(s2)) {
    // try stripping the buf= part off the end (the ref encoder zero fills, we don't)
    std::size_t s2BufEnd = s2.find(", buf=");
    std::size_t sBufEnd = s.find(", buf=");
    std::string s2Strip = s2;
    std::string sStrip = s;
    if ((sBufEnd != std::string::npos) && (s2BufEnd != std::string::npos)) {
      s2Strip = s2.substr(0, s2BufEnd);
      sStrip = s.substr(0, sBufEnd);
    }
    if (0 != sStrip.compare(s2Strip)) {
      std::cerr << "Cabac file mismatch" << std::endl;
      std::cerr << "s: " << s << std::endl;
      std::cerr << "s2: " << s2 << std::endl;
      exit(-1);
    }
  }
  cabac_bins_count++;
}

#define DBG_CABAC \
    if (! check_bins) { ++ cabac_bins_count; }  \
    else if (g_checkStreams)

void debug_cabac_daala_bits(int bit)
{
    DBG_CABAC {
        std::ostringstream s;
        s << cabac_bins_count;
        s << " bin=" << bit;
        debug_cabac(s.str());
    }
}

void debug_cabac_daala_symbol(int symbol,int nsyms,int rng,int bits,int Fh,int Fl,int buf)
{
    DBG_CABAC {
        std::ostringstream s;
        s << cabac_bins_count;
        s << " bin=" << symbol << "/" << nsyms;
        s << ", rng=0x" << std::hex << rng << std::dec;
        s << ", bits=" << bits;
        s << ", Fh=" << Fh;
        s << ", Fl=" << Fl;
        s << ", buf=" << buf;
        debug_cabac(s.str());
    }
}

void debug_cabac_bins(int binVal, int startRange, int startVal, int prob)
{
    if (startRange==255) {
        return;  // Skip marker bits as they appear out of order
    }

    DBG_CABAC {
        std::ostringstream s;
        s << cabac_bins_count;
        s << " bin=" << binVal;
        s << " sr=" << startRange;
        s << ", sv=" << startVal;
        s << " p=" << prob;
        debug_cabac(s.str());
    }
}


static int mv_count = 0;
static std::ifstream refMv;
void debug_mv(TOPLEVEL_T *b, int i, int mode, int is_compound) {
  if (i && !is_compound) {
    return;
  }
  if (!check_mvs) {
    mv_count++;
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_mvs < 2) {
    std::string f = pathToRef;
    refMv.open(f.append("ref_mv2.txt"));
    check_mvs++;
  }
  if(!refMv.is_open()) {
    std::cerr << "Ref mv file open failure" << std::endl;
    exit(-1);
  }

  std::string s2;
  if(!std::getline(refMv, s2)) {
    std::cerr << "Ref mv file ended prematurely" << std::endl;
    exit(-1);
  }

  std::ostringstream s;
  s << mv_count;
  s << " i=" << i;
  s << " mode=" << mode;
  s << " dx=" << b->video_stream.frame_data.mv[i][1];
  s << " dy=" << b->video_stream.frame_data.mv[i][0];

  if (0 != s2.compare(s.str())) {
    std::cerr << "s: " << s.str() << std::endl;
    std::cerr << "Mv file mismatch" << std::endl;
    exit(-1);
  }
  mv_count++;
}



enum {
  PREDICTION = 0,
  TRANSFORM = 1,
  ABOVE = 2,
  LEFT = 3,
  OBMC_PRE = 4,
  OBMC_POST = 5,
};


static unsigned int recon_count = 0;
static std::ifstream refRecon;
void log_prediction_rect(TOPLEVEL_T *b, int bw, int bh, int x, int y, int plane, int phase, bool isI16, uint16* I16, int32 *I32, int pitch) {
  if (!check_recon) {
    if (phase==TRANSFORM) {
      recon_count++;
    }
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_recon < 2) {
    std::string f = pathToRef;
    refRecon.open(f.append("ref_recon2.txt"));
    check_recon++;
  }
  if(!refRecon.is_open()) {
    std::cerr << "Ref recon file open failure" << std::endl;
    exit(-1);
  }

  for (int dy=0; dy<bh; dy++) {
    std::ostringstream s;
    for (int dx=0; dx<bw; dx++) {
      s << std::setfill('0') << std::setw(2) << std::hex;
      if (isI16) {
        s << I16[x+dx+(y+dy)*pitch];
      } else {
        s << I32[x+dx+(y+dy)*pitch];
      }
      s << " ";
    }

    std::string s2;
    while (true) {
      if(!std::getline(refRecon, s2)) {
        std::cerr << "Ref recon file ended prematurely" << std::endl;
        exit(-1);
      }

      if (s2[0] == 'P') {
        std::ostringstream s3;
        s3 << "Prediction " << recon_count;
        int pplane = (phase >= 0) ? plane : -1;
        s3 << " " << pplane;
        s3 << " " << phase;
        if (0 != s2.compare(s3.str())) {
          std::cerr << "s3: " << s3.str() << std::endl;
          std::cerr << "Mismatch in prediction marker" << std::endl;
          exit(-1);
        }
        continue;
      }

      if (s2[0] == 'R') {
        continue;
      }
      break;
    }

    if (0 != s2.compare(s.str())) {
      std::cerr << "s: " << s.str() << std::endl;
      if (phase==PREDICTION) {
        std::cerr << "Mismatch in prediction" << std::endl;
      } else if (phase==TRANSFORM) {
        std::cerr << "Mismatch in transform" << std::endl;
      } else if (phase==ABOVE) {
        std::cerr << "Mismatch in above" << std::endl;
      } else if (phase==LEFT) {
        std::cerr << "Mismatch in left" << std::endl;
      } else if (phase==OBMC_PRE) {
        std::cerr << "Mismatch in obmc pre" << std::endl;
      } else if (phase==OBMC_POST) {
        std::cerr << "Mismatch in obmc post" << std::endl;
      } else {
        std::cerr << "Mismatch in ???" << std::endl;
      }
      exit(-1);
    }
  }
  if (phase==TRANSFORM) {
    recon_count++;
  }
}

void log_prediction(TOPLEVEL_T *b, int tx_sz, int x, int y, int plane, int phase) {
  int bw = tx_size_wide[tx_sz];
  int bh = tx_size_high[tx_sz];
  uint16 *I = b->video_stream.frame_data.rframe[plane];
  int pitch = b->video_stream.frame_data.stride;
  log_prediction_rect(b,bw,bh,x,y,plane,phase,true,I,NULL,pitch);
}

void log_prediction_rect_pred(TOPLEVEL_T *b, int bw, int bh, int plane, int ref, int pitch, int phase) {
  int32 *I=b->video_stream.frame_data.rpredSamples[ref];
  log_prediction_rect(b,bw,bh,0,0,plane,phase,false,NULL,I,pitch);
}

void log_prediction_rect_frame(TOPLEVEL_T *b, int bw, int bh, int x, int y, int plane, int phase) {
  uint16 *I = b->video_stream.frame_data.rframe[plane];
  int pitch = b->video_stream.frame_data.stride;
  log_prediction_rect(b,bw,bh,x,y,plane,phase,true,I,NULL,pitch);
}


void log_intra_neighbours(TOPLEVEL_T *b, int txw, int txh, int mode) {
  if (!check_recon) {
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_recon < 2) {
    std::string f = pathToRef;
    refRecon.open(f.append("ref_recon2.txt"));
    check_recon++;
  }
  if(!refRecon.is_open()) {
    std::cerr << "Ref recon file open failure" << std::endl;
    exit(-1);
  }

  std::ostringstream s;
  s << "Intra " << mode << " left:";
  for (int x=0; x<(txw+txh); x++) {
    s << std::setfill('0') << std::setw(2) << std::hex << b->video_stream.frame_data.left_data[x+2];
    s << " ";
  }

  std::string s2;
  if(!std::getline(refRecon, s2)) {
    std::cerr << "Ref recon file ended prematurely" << std::endl;
    exit(-1);
  }
  if (track_intra_neighbours && (0 != s2.compare(s.str()))) {
    std::cerr << "s: " << s.str() << std::endl;
    std::cerr << "Mismatch in intra neighbours left" << std::endl;
    exit(-1);
  }

  std::ostringstream s3;
  s3 << "Intra above:";
  for (int x=-1; x<(txw+txh); x++) {
    s3 << std::setfill('0') << std::setw(2) << std::hex << b->video_stream.frame_data.above_data[x+16];
    s3 << " ";
  }

  if(!std::getline(refRecon, s2)) {
    std::cerr << "Ref recon file ended prematurely" << std::endl;
    exit(-1);
  }
  if (track_intra_neighbours && (0 != s2.compare(s3.str()))) {
    std::cerr << "s3: " << s3.str() << std::endl;
    std::cerr << "Mismatch in intra neighbours above" << std::endl;
    exit(-1);
  }
}


static unsigned int loop_count = 0;
static std::ifstream refLoop;
void log_loop(TOPLEVEL_T *b, int mi_row, int mi_col, int step, int plane, int mi_size, int lf_block_size) {
  if (!check_loop) {
    loop_count++;
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_loop < 2) {
    std::string f = pathToRef;
    refLoop.open(f.append("ref_loop2.txt"));
    check_loop++;
  }
  if(!refLoop.is_open()) {
    std::cerr << "Ref loop file open failure" << std::endl;
    exit(-1);
  }

  std::string junk;
  if(!std::getline(refLoop, junk)) {
    std::cerr << "Ref loop file ended prematurely" << std::endl;
    exit(-1);
  }

  int pitch = b->video_stream.frame_data.stride;
  int block_px = lf_block_size * mi_size;
  int w = std::min(block_px,(b->video_stream.frame_data.mi_cols - mi_col)*mi_size) >> b->global_data->plane_subsampling_x[plane];
  int h = std::min(block_px,(b->video_stream.frame_data.mi_rows - mi_row)*mi_size) >> b->global_data->plane_subsampling_y[plane];
  int x = (mi_col)*mi_size >> b->global_data->plane_subsampling_x[plane];
  int y = (mi_row)*mi_size >> b->global_data->plane_subsampling_y[plane];

  for (int dy=0; dy<h; dy++) {
    std::ostringstream s;
    for (int dx=0; dx<w; dx++) {
      s << std::setfill('0') << std::setw(2) << std::hex;
      s << b->video_stream.frame_data.rframe[plane][x+dx+(y+dy)*pitch];
      s << " ";
    }

    std::string s2;
    if(!std::getline(refLoop, s2)) {
      std::cerr << "Ref loop file ended prematurely" << std::endl;
      exit(-1);
    }

    if (0 != s2.compare(s.str())) {
      std::cerr << "s: " << s.str() << std::endl;
      std::cerr << "Mismatch in loop" << std::endl;
      exit(-1);
    }
  }

  loop_count+=1; // once for each plane
}


static unsigned int cdef_count = 0;
static std::ifstream refCdef;
void log_cdef(TOPLEVEL_T *b, int mi_row, int mi_col, int step, int mi_size, int nplanes) {
  if (!check_cdef) {
    cdef_count++;
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_cdef < 2) {
    std::string f = pathToRef;
    refCdef.open(f.append("ref_cdef2.txt"));
    check_cdef++;
  }
  if(!refCdef.is_open()) {
    std::cerr << "Ref cdef file open failure" << std::endl;
    exit(-1);
  }

  int pitch = b->video_stream.frame_data.stride;
  for (int plane=0; plane<nplanes; plane++) {
    std::string junk;
    if(!std::getline(refCdef, junk)) {
      std::cerr << "Ref cdef file ended prematurely" << std::endl;
      exit(-1);
    }

    int w = std::min(64,(b->video_stream.frame_data.mi_cols - mi_col)*mi_size) >> b->global_data->plane_subsampling_x[plane];
    int h = std::min(64,(b->video_stream.frame_data.mi_rows - mi_row)*mi_size) >> b->global_data->plane_subsampling_y[plane];
    int x = ((mi_col)*mi_size >> b->global_data->plane_subsampling_x[plane]);
    int y = ((mi_row)*mi_size >> b->global_data->plane_subsampling_y[plane]);

    for (int dy=0; dy<h; dy++) {
      std::ostringstream s;
      for (int dx=0; dx<w; dx++) {
        s << std::setfill('0') << std::setw(2) << std::hex;
        if (step==0) {
          s << b->video_stream.frame_data.rframe[plane][x+dx+(y+dy)*pitch];
        } else {
          s << b->video_stream.frame_data.rcdef_frame[plane][x+dx+(y+dy)*pitch];
        }
        s << " ";
      }

      std::string s2;
      if(!std::getline(refCdef, s2)) {
        std::cerr << "Ref cdef file ended prematurely" << std::endl;
        exit(-1);
      }

      if (0 != s2.compare(s.str())) {
        std::cerr << "s: " << s.str() << std::endl;
        std::cerr << "Mismatch in cdef" << std::endl;
        exit(-1);
      }
    }
  }

  cdef_count+=1;
}

static unsigned int upscale_count = 0;
static std::ifstream refUpscale;
void log_upscale(TOPLEVEL_T *b, int x0, int x1, int h, uint16_t *I) {
  if (!check_upscale) {
    upscale_count++;
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_upscale < 2) {
    std::string f = pathToRef;
    refUpscale.open(f.append("ref_upscale2.txt"));
    check_upscale++;
  }
  if(!refUpscale.is_open()) {
    std::cerr << "Ref upscale file open failure" << std::endl;
    exit(-1);
  }

  int pitch = b->video_stream.frame_data.stride;
  std::string junk;
  if(!std::getline(refUpscale, junk)) {
    std::cerr << "Ref upscale file ended prematurely" << std::endl;
    exit(-1);
  }

  for (int y=0; y<h; y++) {
    std::ostringstream s;
    for (int x=x0; x<x1; x++) {
      s << std::setfill('0') << std::setw(2) << std::hex;
      s << I[x+y*pitch];
      s << " ";
    }

    std::string s2;
    if(!std::getline(refUpscale, s2)) {
      std::cerr << "Ref upscale file ended prematurely" << std::endl;
      exit(-1);
    }

    if (0 != s2.compare(s.str())) {
      std::cerr << "s: " << s.str() << std::endl;
      std::cerr << "Mismatch in upscale" << std::endl;
      exit(-1);
    }
  }

  upscale_count+=1;
}

static unsigned int lr_count = 0;
static std::ifstream refLr;
void log_lr(TOPLEVEL_T *b, int basex, int basey, int w, int h, int plane, int step) {
  if (!check_lr) {
    lr_count++;
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_lr < 2) {
    std::string f = pathToRef;
    refLr.open(f.append("ref_lr2.txt"));
    check_lr++;
  }
  if(!refLr.is_open()) {
    std::cerr << "Ref lr file open failure" << std::endl;
    exit(-1);
  }

  std::string junk;
  if(!std::getline(refLr, junk)) {
    std::cerr << "Ref lr file ended prematurely" << std::endl;
    exit(-1);
  }

  /* Set up a few values:
     I = array to take data from
     (x0, y0) to (x1, y1) = region of pixels to dump
     (xoff, yoff) = position to start within I
     (basex, basey) = position of start within the frame (for info only) */
  int x0, x1, y0, y1, x, y, pitch;
  if (step == 0) {
    x = y = 3;
    x0 = y0 = -3;
    x1 = w+3;
    y1 = h+3;
    pitch = 70;
  } else {
    pitch = b->video_stream.frame_data.stride;
    x = basex;
    y = basey;
    x0 = y0 = 0;
    x1 = w;
    y1 = h;
  }

  for (int dy=y0; dy<y1; dy++) {
    std::ostringstream s;
    for (int dx=x0; dx<x1; dx++) {
      s << std::setfill('0') << std::setw(2) << std::hex;
        if (step==0) {
          s << b->video_stream.frame_data.rprocunit_input[x+dx+(y+dy)*pitch];
        } else {
          s << b->video_stream.frame_data.rlr_frame[plane][x+dx+(y+dy)*pitch];
        }
      s << " ";
    }

    std::string s2;
    if(!std::getline(refLr, s2)) {
      std::cerr << "Ref lr file ended prematurely" << std::endl;
      exit(-1);
    }

    if (0 != s2.compare(s.str())) {
      if (step==0)
        std::cerr << "Mismatch in lr input; row=" << dy << std::endl;
      else
        std::cerr << "Mismatch in lr output; row=" << dy << std::endl;
      std::cerr << "ref: " << junk << std::endl;
      std::cerr << "ref: " << s2 << std::endl;
      std::cerr << "us:  plane=" << plane << ", step=" << step << ", x=" << basex << ", y=" << basey <<
                   ", w=" << w << ", h=" << h << std::endl;
      std::cerr << "us:  " << s.str()  << std::endl;
      exit(-1);
    }
  }

  lr_count+=1; // once for each plane
}


static unsigned int coeffs_count = 0;
static std::ifstream refCoeffs;
void log_coeffs(TOPLEVEL_T *b) {
  if (!check_coeffs) {
    coeffs_count++;
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_coeffs < 2) {
    std::string f = pathToRef;
    refCoeffs.open(f.append("ref_coeffs2.txt"));
    check_coeffs++;
  }
  if(!refCoeffs.is_open()) {
    std::cerr << "Ref coeffs file open failure" << std::endl;
    exit(-1);
  }

  std::ostringstream s;
  int Eob = b->video_stream.frame_data.eob;
  s << Eob << ": ";
  for (int c=0; c<Eob; c++) {
    s << b->video_stream.frame_data.dequant[c]; // ignore scan as to provide a check of that too
    s << " ";
  }

  std::string s2;
  if(!std::getline(refCoeffs, s2)) {
    std::cerr << "Ref coeffs file ended prematurely" << std::endl;
    exit(-1);
  }

  if (0 != s2.compare(s.str())) {
    std::cerr << "s: " << s.str() << std::endl;
    std::cerr << "Mismatch in coeffs" << std::endl;
    exit(-1);
  }

  coeffs_count+=1;
}

static unsigned int mfmv_count = 0;
static std::ifstream refMfmv;
void validate_mfmv(bool checkThisOne, TOPLEVEL_T *b, int h8, int w8) {
  for ( int ref = 1; ref <= 7; ref++ ) {
    for ( int y = 0; y < h8 ; y++ ) {
      for ( int x = 0; x < w8; x++ ) {
        std::ostringstream s;
        s << "ref " << ref
          << ", y " << y
          << ", x " << x
          << ", ";
        s << b->global_data->rmotion_field_mvs[ref - 1][(y * (b->video_stream.frame_data.mi_cols >> ((int)1))) + x][0];
        s << ":";
        s << b->global_data->rmotion_field_mvs[ref - 1][(y * (b->video_stream.frame_data.mi_cols >> ((int)1))) + x][1];

        std::string s2;
        if(!std::getline(refMfmv, s2)) {
          std::cerr << "Ref mfmv file ended prematurely" << std::endl;
          exit(-1);
        }

        if (checkThisOne && 0 != s2.compare(s.str())) {
          std::cerr << "s: " << s.str() << std::endl;
          std::cerr << "s2: " << s2 << std::endl;
          std::cerr << "cabac_bins_count: " << cabac_bins_count << ", mfmv_count (unreliable): " << mfmv_count << ", ref: " << ref << ", y: " << y << ", x: " << x << std::endl;
          std::cerr << "Mismatch in mfmv" << std::endl;
          exit(-1);
        }
      }
    }
  }
}

void log_mfmv(TOPLEVEL_T *b, int h8, int w8) {
  if (!check_mfmv) {
    mfmv_count++;
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_mfmv < 2) {
    std::string f = pathToRef;
    refMfmv.open(f.append("ref_mfmv2.txt"));
    check_mfmv++;
  }
  if(!refMfmv.is_open()) {
    std::cerr << "Ref mfmv file open failure" << std::endl;
    exit(-1);
  }

  while(true) {
    std::string s2;
    if(!std::getline(refMfmv, s2)) {
      std::cerr << "Ref mfmv file ended prematurely" << std::endl;
      exit(-1);
    }
    std::ostringstream s;
    s << cabac_bins_count << " ";
    s << h8 << " " << w8;
    bool checkThisOne = true;
    if (0 != s2.compare(s.str())) {
      std::cerr << "s: " << s.str() << std::endl;
      std::cerr << "s2: " << s2 << std::endl;
      std::cerr << "Ref mfmv header liner mismatched" << std::endl;
      exit(-1);
    }
    validate_mfmv(checkThisOne, b, h8, w8);
    if (checkThisOne) {
      break;
    }
  }


  mfmv_count+=1;
}

static unsigned int output_count = 0;
static std::ifstream refOutput;
void log_output(TOPLEVEL_T *b, int phase, int nplanes) {
  if (!check_output) {
    output_count++;
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_output < 2) {
    std::string f = pathToRef;
    refOutput.open(f.append("ref_output2.txt"));
    check_output++;
  }
  if(!refOutput.is_open()) {
    std::cerr << "Ref output file open failure" << std::endl;
    exit(-1);
  }

  int pitch = b->video_stream.frame_data.stride;
  int w = b->video_stream.frame_data.upscaled_width;
  int h = b->video_stream.frame_data.upscaled_height;
  for (int plane=0; plane<nplanes; plane++) {
    std::string junk;
    if(!std::getline(refOutput, junk)) {
      std::cerr << "Ref output file ended prematurely" << std::endl;
      exit(-1);
    }

    int ssx = b->global_data->plane_subsampling_x[plane];
    int ssy = b->global_data->plane_subsampling_y[plane];
    int planeW = (w + ssx) >> ssx;
    int planeH = (h + ssy) >> ssy;

    for (int y=0; y<planeH; y++) {
      std::ostringstream s;
      for (int x=0; x<planeW; x++) {
        s << std::setfill('0') << std::setw(2) << std::hex;
        s << b->video_stream.frame_data.rframe[plane][y*pitch+x];
        s << " ";
      }

      std::string s2;
      if(!std::getline(refOutput, s2)) {
        std::cerr << "Ref output file ended prematurely" << std::endl;
        exit(-1);
      }

      if (0 != s2.compare(s.str())) {
        std::cerr << "s: " << s.str() << std::endl;
        std::cerr << "Mismatch in output" << std::endl;
        exit(-1);
      }
    }
  }

  output_count+=1;
}

static unsigned int anchor_ref_count = 0;
static std::ifstream refAnchor_Ref;
void log_anchor_ref(TOPLEVEL_T *b) {
  if (!check_anchor_ref) {
    anchor_ref_count++;
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_anchor_ref < 2) {
    std::string f = pathToRef;
    refAnchor_Ref.open(f.append("ref_anchor_ref2.txt"));
    check_anchor_ref++;
  }
  if(!refAnchor_Ref.is_open()) {
    std::cerr << "Ref anchor_ref file open failure" << std::endl;
    exit(-1);
  }

  int pitch = b->global_data->ref_width[0];
  int w = b->video_stream.frame_data.upscaled_width;
  int h = b->video_stream.frame_data.upscaled_height;
  for (int plane=0; plane<3; plane++) {
    int ssx = b->global_data->plane_subsampling_x[plane];
    int ssy = b->global_data->plane_subsampling_y[plane];
    int planeW = (w + ssx) >> ssx;
    int planeH = (h + ssy) >> ssy;

    std::ostringstream marker;
    marker << std::dec
           << "-- " << anchor_ref_count
           << " p" << plane
           << " w" << planeW
           << " h" << planeH;

    std::string marker2;
    if(!std::getline(refAnchor_Ref, marker2)) {
      std::cerr << "Ref anchor_ref file ended prematurely - marker" << std::endl;
      exit(-1);
    }

    if (0 != marker2.compare(marker.str())) {
      std::cerr << "marker: " << marker.str() << std::endl;
      std::cerr << "marker2: " << marker2 << std::endl;
      std::cerr << "Mismatch in tile_output" << std::endl;
      exit(-1);
    }

    for (int y=0; y<planeH; y++) {
      std::ostringstream s;
      for (int x=0; x<planeW; x++) {
        s << std::setfill('0') << std::setw(2) << std::hex;
        s << b->global_data->rframestore[b->video_stream.frame_data.active_ref_idx[0]][plane][y*pitch+x];
        s << " ";
      }

      std::string s2;
      if(!std::getline(refAnchor_Ref, s2)) {
        std::cerr << "Ref anchor_ref file ended prematurely" << std::endl;
        exit(-1);
      }

      if (0 != s2.compare(s.str())) {
        std::cerr << "s: " << s.str() << std::endl;
        std::cerr << "Mismatch in anchor_ref" << std::endl;
        exit(-1);
      }
    }
  }

  anchor_ref_count+=1;
}

static unsigned int tile_output_count = 0;
static std::ifstream refTile_Output;
void log_tile_output(TOPLEVEL_T *b, int decTileRow, int decTileCol) {
  if (!check_tile_output) {
    tile_output_count++;
    return;
  }
  if (!g_checkStreams) {
    return;
  }
  if (check_tile_output < 2) {
    std::string f = pathToRef;
    refTile_Output.open(f.append("ref_tile_output2.txt"));
    check_tile_output++;
  }
  if(!refTile_Output.is_open()) {
    std::cerr << "Ref tile_output file open failure" << std::endl;
    exit(-1);
  }

  int pitch = b->video_stream.frame_data.stride;
  int w = b->video_stream.frame_data.tile_width * MI_SIZE;
  int h = b->video_stream.frame_data.tile_height * MI_SIZE;
  for (int plane=0; plane<3; plane++) {
    int ssx = b->global_data->plane_subsampling_x[plane];
    int ssy = b->global_data->plane_subsampling_y[plane];
    int planeW = (w + ssx) >> ssx;
    int planeH = (h + ssy) >> ssy;
    int srcX = (decTileCol * w) >> ssx;
    int srcY = (decTileRow * h) >> ssy;

    std::ostringstream marker;
    marker << std::dec
           << "-- " << tile_output_count
           << " p" << plane
           << " w" << planeW
           << " h" << planeH;

    std::string marker2;
    if(!std::getline(refTile_Output, marker2)) {
      std::cerr << "Ref tile_output file ended prematurely - marker" << std::endl;
      exit(-1);
    }

    if (0 != marker2.compare(marker.str())) {
      std::cerr << "marker: " << marker.str() << std::endl;
      std::cerr << "marker2: " << marker2 << std::endl;
      std::cerr << "Mismatch in tile_output" << std::endl;
      exit(-1);
    }

    for (int y=0; y<planeH; y++) {
      std::ostringstream s;
      for (int x=0; x<planeW; x++) {
        s << std::setfill('0') << std::setw(2) << std::hex;
        s << b->video_stream.frame_data.rframe[plane][(srcY+y)*pitch+(srcX+x)];
        s << " ";
      }

      std::string s2;
      if(!std::getline(refTile_Output, s2)) {
        std::cerr << "Ref tile_output file ended prematurely - output" << std::endl;
        exit(-1);
      }

      if (0 != s2.compare(s.str())) {
        std::cerr << "s: " << s.str() << std::endl;
        std::cerr << "s2: " << s2 << std::endl;
        std::cerr << "Mismatch in tile_output" << std::endl;
        exit(-1);
      }
    }

    tile_output_count+=1;
  }
}

#if DECODE
#include <openssl/md5.h>
int md5Active = 0;
MD5_CTX md5Context;
unsigned char md5Buf[MD5_DIGEST_LENGTH*2];

void builtin_start_md5() {
  // Save previous MD5
  memcpy(md5Buf+MD5_DIGEST_LENGTH, md5Buf, MD5_DIGEST_LENGTH*sizeof(unsigned char));
  MD5_Init(&md5Context);
  md5Active = 1;
}

void builtin_end_md5() {
  MD5_Final(md5Buf, &md5Context);
  md5Active = 0;
}

int builtin_check_md5s() {
  return (memcmp(md5Buf, md5Buf+MD5_DIGEST_LENGTH*sizeof(unsigned char), MD5_DIGEST_LENGTH*sizeof(unsigned char))==0);
}

// This function is called on every syntax element read when decoding
// (see the insert_cabac pass of the streams compiler).
int64 builtin_syntax_elem(int64 v) {
  if (md5Active)
      MD5_Update(&md5Context, &v, sizeof(int));
  return v;
}
#endif // DECODE
