/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "av1_enc_command_line.hh"

#include <cassert>
#include <cstring>

#include <iostream>
#include <limits>
#include <sstream>

#include <getopt.h>
#include <libgen.h>

EncCommandLine::EncCommandLine (int argc, char **argv)
    : app_name (basename (argv [0])),
      seed (1),
      filename (nullptr),
      config_file (nullptr),
      config_selected (nullptr),
      large_scale_tile (false),
      annex_b (true),
#if !RELEASE
      choose_all_filename (nullptr),
      decrease_target_bitrate_pc (0),
#endif
      verbose (true)
{
    parse (argc, argv);
}

void
EncCommandLine::parse (int argc, char **argv)
{
    static struct option long_options[] = {
        { "seed",                            required_argument, 0, 's' },
        { "output",                          required_argument, 0, 'o' },
        { "config-file",                     required_argument, 0, 'C' },
        { "config",                          required_argument, 0, 'i' },
        { "config-debug",                    required_argument, 0, 'd' },
        { "large-scale-tile",                no_argument,       0, 'L' },
        { "not-annexb",                      no_argument,       0, 'b' },
#if !RELEASE
        { "choose-all-filename",             required_argument, 0, 'c' },
        { "decrease-target-bitrate-percent", required_argument, 0, '%' },
#endif
        { "help",                            no_argument,       0, 'h' },
        { "verbose",                         no_argument,       0, 'v' },
        { NULL,                              0,                 0, 0 },
    };

    static const char *short_options =
        "-s:o:C:i:d:"
#if !RELEASE
        "c:"
#endif
        "hv";

    while (optind < argc) {
        int option_index = 0;
        int c = getopt_long (argc, argv,
                             short_options, long_options, & option_index);
        if (c == -1)
            break;

        if ((c == ':') || (c == '?')) {
            syntax (std::cerr);
            exit (1);
        }

        switch (c) {
        case 1: {
            // Positional argument. But we don't expect one. Complain.
            std::ostringstream oss;
            oss << "Unrecognised argument: `" << argv [optind - 1] << "'.";
            die (oss.str ().c_str ());
            break;
        }

        case 's':
            seed = read_int_arg ("frame-limit", optarg, true);
            break;

        case 'o':
            filename = optarg;
            break;

        case 'C':
            config_file = optarg;
            break;

        case 'i':
            config_selected = optarg;
            break;

        case 'd':
            read_debug_arg (optarg);
            break;

        case 'L':
            large_scale_tile = true;
            break;

        case 'b':
            annex_b = false;
            break;

#if !RELEASE
        case 'c':
            choose_all_filename = optarg;
            break;

        case '%':
            decrease_target_bitrate_pc =
                read_int_arg ("decrease-target-bitrate-percent", optarg, false);
            break;
#endif

        case 'h':
            syntax (std::cout);
            exit (0);
            break;

        case 'v':
            verbose = true;
            break;

        default:
            // This shouldn't happen (it means we gave getopt an
            // argument and didn't handle it in the switch)
            assert (0);
        }
    }
}

void EncCommandLine::syntax (std::ostream &os) const {
    os << ("\n  Argon Design AV1 Bit-stream Generator build " AV1_VERSION "\n"
           "  Copyright (C) Argon Design 2016-2019\n\n"
           "    Syntax:\n")
       << "      " << app_name;

    static const char *short_args[] = {
        "[-v]",
        "[-s <seed>]",
        "[-C <config-file>]",
        "[-i <config>]",
        "-o <output>",
        NULL
    };

    const unsigned arg_indent_len = 6 + strlen (app_name) + 1;
    const std::string arg_indent (arg_indent_len, ' ');
    unsigned hpos = arg_indent_len;

    for (unsigned i = 0; short_args [i]; ++ i) {
        unsigned w = strlen (short_args [i]);
        if (hpos + 1 + w >= 80) {
            os << '\n' << arg_indent;
            hpos = arg_indent_len;
        } else {
            os << ' ';
        }
        os << short_args [i];
        hpos += 1 + w;
    }
    os << "\n\n";

    os << ("    Options:\n"
           "      -h, --help\n\n"
           "        Display this message\n\n\n"
           "      -v, --verbose\n\n"
           "        Enable internal verbose messages\n\n\n"
           "      -s <N>, --seed <N>\n\n"
           "        Seed the random number generator\n\n\n"
           "      -o <filename>, --output <filename>\n\n"
           "        Filename for output [required].\n\n\n"
#if !RELEASE
           "      -c <filename>, --choose-all-filename <filename>\n\n"
           "        Filename for choose-all support.\n\n\n"
#endif
           "      --not-annexb\n\n"
           "        Don't use annex B.\n\n\n"
           "      --large-scale-tile\n\n"
           "        Generate a large scale tile stream.\n\n\n"
#if !RELEASE
           "      --decrease-target-bitrate-percent <N>\n\n"
           "        Decrease the target bitrate by N%.\n\n\n"
#endif
           "    Config file support:\n"
           "      -C <config-file>, --config-file <config-file>\n\n"
           "        Bit-stream config file (default: none).\n\n\n"
           "      -i <config>, --config <config>\n\n"
           "        Specific configuration (default: 'default').\n\n\n"
           "      -d <regex>, --config-debug <regex>\n\n"
           "        Add debugging prints when evaluating bit-stream elements\n"
           "        matching the given regular expression. Can be specified\n"
           "        more than once to debug several elements. The full\n"
           "        syntax is -d '<regex>:<verbosity>' where <verbosity> is an\n"
           "        integer from 0 to 2. If the verbosity is set to 0, no debug\n"
           "        messages will be printed for matching elements.\n\n\n"
           );
}

void EncCommandLine::die (const char *msg) const {
    if (msg)
        std::cerr << msg << "\n\n";

    syntax (std::cerr);
    exit (1);
}

int
EncCommandLine::read_int_arg (const char *name,
                              const char *arg,
                              bool        allow_negative) const
{
    assert (name && arg);

    // We want to use strtol, but want to catch leading '-' signs and
    // disallow them if allow_negative is false (to avoid "hey, that's
    // too big" when given -1).
    while (isspace (* arg)) ++ arg;
    bool bad_neg = (arg [0] == '-') && ! allow_negative;

    const char *problem = nullptr;
    char *endptr;
    errno = 0;
    long num = strtol (arg, & endptr, 0);
    if (arg [0] == '\0' || *endptr != '\0') {
        problem = "not a non-negative integer";
    } else if ((errno == ERANGE) ||
               (std::numeric_limits<int>::max () < num) ||
               (std::numeric_limits<int>::min () > num)) {
        problem = bad_neg ? "negative" : "too large";
    }

    if (problem) {
        std::cerr << "Failed to parse " << name << " argument: `"
                  << arg << "' is " << problem << ".\n";
        die (nullptr);
    }

    return static_cast<int> (num);
}

void
EncCommandLine::read_debug_arg (const char *arg)
{
    assert (arg);

    // arg is supposed to be a regular expression, possibly followed
    // by a colon and a verbosity level. We don't allow a colon inside
    // the regex, but that shouldn't matter since it's supposed to
    // match bitstream elements, which don't contain colons!
    const char *colon = strchr (arg, ':');
    if (! colon) {
        // This is emulating strchrnul (which we don't have on Cygwin
        // builds).
        colon = arg + strlen (arg);
    }
    assert (arg <= colon);

    size_t regex_len = colon - arg;
    if (! regex_len) {
        std::cerr << "Empty regular expression in argument "
                  << "for --config-debug: `" << arg << "'.\n";
        die (nullptr);
    }

    std::string regex (arg, regex_len);

    unsigned verbosity = 1;
    if (* colon) {
        // Allowed values on the RHS are 0, 1 or 2
        bool good = (('0' <= colon [1]) && (colon [1] <= '2') &&
                     colon [2] == '\0');
        if (! good) {
            std::cerr << "Invalid verbosity for regular expression in "
                      << "--config-debug argument: `"
                      << arg
                      << "'. Should be 0, 1 or 2.\n";
            die (nullptr);
        }

        verbosity = colon [1] - '0';
        assert (verbosity <= 2);
    }

    config_element_debug.push_back (elt_dbg_pair_t (std::move (regex),
                                                    verbosity));
}
