/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

// This file explains how to derive the context increments for the different elements
//
// Functions can be written as an expression for the value for ctxIdxInc or -1 to signify bypass,
// or as a function which ends up setting the variable ctxIdxOffset

// keywords:
// ctDepth = current coding unit depth
// skipFlag = Whether unit is skipped
// L = address of left (in minimum coding units)
// P = our address
// aL = whether left is available

golomb_length_bit : unused_init, b_u(1) = -1;
golomb_data_bit : unused_init, b_u(1) = -1;
all_zero : unused_init, b_all_zero() = {
  maxX4 = (mi_cols*(MI_SIZE/4)) >> plane_subsampling_x[plane]
  maxY4 = (mi_rows*(MI_SIZE/4)) >> plane_subsampling_y[plane]
  w = tx_size_wide_unit[txSz]
  h = tx_size_high_unit[txSz]
  x4 = x>>2
  y4 = y>>2
  planeBsize = get_plane_block_size(bsize, plane)
  if (plane == 0) {
    top = 0
    left = 0
    for (k = 0; k < w; k++) {
      // Don't read points off the edge of the valid grid
      if ( x4+k<maxX4)
        top = Max(top, (above_context[plane][x4+k] & COEFF_CONTEXT_MASK))
    }
    for (k = 0; k < h; k++) {
      if ( y4+k<maxY4)
        left = Max(left, (left_context[plane][(y4+k)&mi_mask2] & COEFF_CONTEXT_MASK))
    }
    top = Min(top, 255)
    left = Min(left, 255)
    if (planeBsize == txsize_to_bsize[txSz]) {
      ctxIdxOffset = 0
    } else if (top == 0 && left == 0) {
      ctxIdxOffset = 1
    } else if (top == 0 || left == 0) {
      ctxIdxOffset = 2 + (Max(top, left) > 3)
    } else if (Max(top, left) <= 3) {
      ctxIdxOffset = 4
    } else if (Min(top, left) <= 3) {
      ctxIdxOffset = 5
    } else {
      ctxIdxOffset = 6
    }
  } else { // plane != 0
    above = 0
    left = 0
    for(i=0;i<w;i++) {
      // Don't read points off the edge of the valid grid
      if ( x4+i<maxX4)
        above |= above_context[plane][(x4+i)]
          }
    for(i=0;i<h;i++) {
      if ( y4+i<maxY4)
        left |= left_context[plane][(y4+i)&mi_mask2]
    }
    ctxIdxOffset = (above!=0) + (left!=0)
    ctxIdxOffset += 7

    if (num_pels_log2_lookup[planeBsize] > num_pels_log2_lookup[txsize_to_bsize[txSz]]) {
      ctxIdxOffset += 3
    }
  }
  ASSERT(ctxIdxOffset >= 0 && ctxIdxOffset < TXB_SKIP_CONTEXTS, "Invalid context")
}
is_non_zero : unused_init, b_is_non_zero() = {
  ctxIdxOffset = get_nz_map_ctx(txSz, plane, blocky, blockx, scan[c])
  ASSERT(ctxIdxOffset >= 0 && ctxIdxOffset < SIG_COEF_CONTEXTS, "Invalid context")
}
eob_pt_16 : unused_init, b_eob_pt_16() = {
  txType = get_tx_type(txSz,plane,blocky,blockx)
  ctxIdxOffset = (tx_type_to_class[txType] == TX_CLASS_2D) ? 0 : 1
}

eob_pt_32 : unused_init, b_eob_pt_32() = {
  txType = get_tx_type(txSz,plane,blocky,blockx)
  ctxIdxOffset = (tx_type_to_class[txType] == TX_CLASS_2D) ? 0 : 1
}

eob_pt_64 : unused_init, b_eob_pt_64() = {
  txType = get_tx_type(txSz,plane,blocky,blockx)
  ctxIdxOffset = (tx_type_to_class[txType] == TX_CLASS_2D) ? 0 : 1
}

eob_pt_128 : unused_init, b_eob_pt_128() = {
  txType = get_tx_type(txSz,plane,blocky,blockx)
  ctxIdxOffset = (tx_type_to_class[txType] == TX_CLASS_2D) ? 0 : 1
}

eob_pt_256 : unused_init, b_eob_pt_256() = {
  txType = get_tx_type(txSz,plane,blocky,blockx)
  ctxIdxOffset = (tx_type_to_class[txType] == TX_CLASS_2D) ? 0 : 1
}

eob_pt_512 : unused_init, b_eob_pt_512() = {
  txType = get_tx_type(txSz,plane,blocky,blockx)
  ctxIdxOffset = (tx_type_to_class[txType] == TX_CLASS_2D) ? 0 : 1
}

eob_pt_1024 : unused_init, b_eob_pt_1024() = {
  txType = get_tx_type(txSz,plane,blocky,blockx)
  ctxIdxOffset = (tx_type_to_class[txType] == TX_CLASS_2D) ? 0 : 1
}

eob_extra: unused_init, b_eob_extra() = {
  ctxIdxOffset = eobPt
}
eob_extra_bit : unused_init, b_u(1) = -1;
coeff_base : unused_init, b_coeff_base() = {
  ASSERT(c!=(eob-1), "Logic error")
  ctxIdxOffset = get_nz_map_ctx(txSz, plane, blocky, blockx, scan[c], c, 0)
  ASSERT(ctxIdxOffset >= 0 && ctxIdxOffset < COEFF_BASE_CONTEXTS, "Invalid context")
}
coeff_base_eob : unused_init, b_coeff_base_eob() = {
  ASSERT(c==(eob-1), "Logic error")
  ctxIdxOffset = get_nz_map_ctx(txSz, plane, blocky, blockx, scan[c], c, 1) - SIG_COEF_CONTEXTS + SIG_COEF_CONTEXTS_EOB
  ASSERT(ctxIdxOffset >= 0 && ctxIdxOffset < SIG_COEF_CONTEXTS_EOB, "Invalid context")
}
dc_sign : unused_init, b_dc_sign() = {
  maxX4 = (mi_cols*(MI_SIZE/4)) >> plane_subsampling_x[plane]
  maxY4 = (mi_rows*(MI_SIZE/4)) >> plane_subsampling_y[plane]
  w = tx_size_wide_unit[txSz]
  h = tx_size_high_unit[txSz]
  x4 = x>>2
  y4 = y>>2
  dcSign = 0
  for (k = 0; k < w; k++) {
    // Don't read points off the edge of the valid grid
    if ( x4+k<maxX4) {
      sign = above_context[plane][x4+k] >> COEFF_CONTEXT_BITS
      if (sign == 1) {
        dcSign--
      } else if (sign == 2) {
        dcSign++
      } else {
        ASSERT(sign == 0, "Invalid sign")
      }
    }
  }
  for (k = 0; k < h; k++) {
    if ( y4+k<maxY4) {
      sign = left_context[plane][(y4+k)&mi_mask2] >> COEFF_CONTEXT_BITS
      if (sign == 1) {
        dcSign--
      } else if (sign == 2) {
        dcSign++
      } else {
        ASSERT(sign == 0, "Invalid sign")
      }
    }
  }
  if (dcSign < 0) {
    ctxIdxOffset = 1
  } else if (dcSign > 0) {
    ctxIdxOffset = 2
  } else {
    ctxIdxOffset = 0
  }
  ASSERT(ctxIdxOffset >= 0 && ctxIdxOffset < DC_SIGN_CONTEXTS, "Invalid context")
}

coeff_br : unused_init, b_coeff_br() = {
  ctxIdxOffset = get_br_ctx(txSz, plane, blocky, blockx, scan[c])
  ASSERT(ctxIdxOffset >= 0 && ctxIdxOffset < LEVEL_CONTEXTS, "Invalid context")
}

coef_extra_bits : unused_init, b_coef_extra_bits() = -1;

skip_coeff : unused_init, b_skip_coeff() = skip_coeff_ctx(mi_row, mi_col);

skip_mode : unused_init, b_skip_mode() = skip_mode_ctx(mi_row, mi_col);

txDepth : unused_init, b_tx_depth() = tx_depth_ctx(mi_row, mi_col, max_tx_size);

intra_tx_type : unused_init, b_intra_tx_type() = 0;
inter_tx_type : unused_init, b_inter_tx_type()  = 0;

txfm_split : unused_init, b_txfm_split() = ctx;

use_obmc: unused_init, b_use_obmc() = 0;
motion_mode : unused_init, b_motion_mode() = 0;

intra_mode_y : unused_init, b_intra_mode_y() = size_group_lookup[sb_size];
sub_intra_mode_y : unused_init, b_intra_mode_y() = 0;

default_intra_mode_y : unused_init, b_default_intra_mode_y() = -1;

intra_mode_uv : unused_init, b_intra_mode_uv() = -1;

comp_mode : unused_init, b_comp_mode() = comp_mode_ctx();

comp_ref_p : unused_init, b_comp_ref_p() = get_pred_context_last12_or_last3_gld();
comp_ref_p1 : unused_init, b_comp_ref_p1() = get_pred_context_last_or_last2();
comp_ref_p2 : unused_init, b_comp_ref_p2() = get_pred_context_last3_or_gld();
comp_bwdref_p : unused_init, b_comp_bwdref_p() = get_pred_context_brfarf2_or_arf();
comp_bwdref_p1 : unused_init, b_comp_bwdref_p1() = get_pred_context_brf_or_arf2();

single_ref_p1 : unused_init, b_single_ref_p1() = get_pred_context_fwd_or_bwd();
single_ref_p2 : unused_init, b_single_ref_p2() = get_pred_context_brfarf2_or_arf();
single_ref_p3 : unused_init, b_single_ref_p3() = get_pred_context_last12_or_last3_gld();
single_ref_p4 : unused_init, b_single_ref_p4() = get_pred_context_last_or_last2();
single_ref_p5 : unused_init, b_single_ref_p5() = get_pred_context_last3_or_gld();
single_ref_p6 : unused_init, b_single_ref_p6() = get_pred_context_brf_or_arf2();

comp_ref_type : unused_init, b_comp_ref_type() = get_pred_context_comp_ref_type();

uni_comp_ref_p : unused_init, b_uni_comp_ref_p() = get_pred_context_fwd_or_bwd();
uni_comp_ref_p1 : unused_init, b_uni_comp_ref_p1() = get_pred_context_last2_or_last3_gld();
uni_comp_ref_p2 : unused_init, b_uni_comp_ref_p2() = get_pred_context_last3_or_gld();

joint_type : unused_init, b_joint_type() = -1;

mvcomp_sign : unused_init, b_mvcomp_sign() = -1;

mv_class : unused_init, b_mv_class() = -1;

mvcomp_class0_bits : unused_init, b_mvcomp_class0_bits() = -1;

mvcomp_class0_fp : unused_init, b_mvcomp_class0_fp() = -1;

mvcomp_class0_hp : unused_init, b_mvcomp_class0_hp() = -1;

mvcomp_bits : unused_init, b_mvcomp_bits() = -1;

mvcomp_fp : unused_init, b_mvcomp_fp() = -1;

mvcomp_hp : unused_init, b_mvcomp_hp() = -1;

inter_mode : unused_init, b_inter_mode() = av1_mode_context_analyzer(-1);
b_mode : unused_init, b_inter_mode() = av1_mode_context_analyzer(j);

comp_inter_mode : unused_init, b_inter_compound_mode() = av1_mode_context_analyzer(-1);

compound_group_idx : unused_init, b_compound_group_idx() = compound_group_idx_ctx(mi_row, mi_col);

compound_idx : unused_init, b_compound_idx() = compound_idx_ctx(mi_row, mi_col);

compound_mask_type : unused_init, b_compound_mask_type() = -1;

wedge_index : unused_init, b_wedge_idx() = -1;
wedge_sign : unused_init, b_u(1) = -1;
mask_type : unused_init, b_u(MAX_SEG_MASK_BITS) = -1;

interintra : unused_init, b_interintra() = -1;
interintra_mode : unused_init, b_interintra_mode() = -1;
wedge_interintra : unused_init, b_wedge_interintra() = -1;

drl_mode : unused_init, b_drl_mode() = drl_ctx(ref_frame_type,idx);

interp_filter : unused_init, b_interp_filter() = interp_filter_ctx(mi_row, mi_col, dir);

partition : unused_init, b_partition() = get_partition_ctx(mi_row, mi_col, bsize);

diff_update : unused_init, b_bool(DIFF_UPDATE_PROB) = -1;
group_diff_update : unused_init, b_bool(GROUP_DIFF_UPDATE_PROB) = -1;
mv_prob_coded : unused_init, b_bool(NMV_UPDATE_PROB) = -1;

is_inter : unused_init, b_is_inter() = {
  above_intra2 = !aU || above_ref_frame[0]<=INTRA_FRAME
  left_intra2 = !aL || left_ref_frame[0]<=INTRA_FRAME

  if (aU && aL)  // both edges available
    ctxIdxOffset = (left_intra2 && above_intra2) ? 3 : left_intra2 || above_intra2
  else if (aU || aL)  // one edge available
    ctxIdxOffset = 2 * (aU ? above_intra2 : left_intra2)
  else
    ctxIdxOffset = 0
}

pred_flag : unused_init, b_pred_flag() = pred_flag_ctx(mi_row, mi_col);

marker : unused_init, b_u(1) = -1;
coef_probs_coded : unused_init, b_u(1) = -1;
mode : unused_init, b_u(2) = -1;
extra_mode_bit : unused_init, b_u(1) = -1;
subexpMoreBits : unused_init, b_u(1) = -1;
subexpBits : unused_init, b_u(b2) = -1;
extraBit : unused_init, b_u(1) = -1;
baseBits : unused_init, b_u(l - 1) = -1;
sign_bit : unused_init, b_u(1) = -1;
pred_mode : unused_init, b_u(1) = -1;
pred_mode_extra : unused_init, b_u(1) = -1;
mv_prob : unused_init, b_u(7) = -1;

has_palette_y : unused_init, b_has_palette_y() = palette_y_ctx(miRow, miCol);
has_palette_uv : unused_init, b_has_palette_uv() = ( PaletteSizeY > 0 ) ? 1 : 0;
palette_size_y : unused_init, b_palette_size_y() = {
  ctxIdxOffset = bsizeCtx
  CHECK(ctxIdxOffset >= 0 && ctxIdxOffset < PALETTE_BSIZE_CTXS, "ctxIdxOffset out of range")
}
palette_size_uv : unused_init, b_palette_size_uv() = {
  ctxIdxOffset = bsizeCtx
  CHECK(ctxIdxOffset >= 0 && ctxIdxOffset < PALETTE_BSIZE_CTXS, "ctxIdxOffset out of range")
}

color_idx_y : unused_init, b_color_idx_y() = {
  ctxIdxOffset = palette_color_context_lookup[CumulatedScore]
  CHECK(ctxIdxOffset >= 0, "Palette ctxIdxOffer y error 1")
  CHECK(ctxIdxOffset < PALETTE_COLOR_CONTEXTS, "Palette ctxIdxOffer y error 2")
}

color_idx_uv : unused_init, b_color_idx_uv() = {
  ctxIdxOffset = palette_color_context_lookup[CumulatedScore]
  CHECK(ctxIdxOffset >= 0, "Palette ctxIdxOffer uv error 1")
  CHECK(ctxIdxOffset < PALETTE_COLOR_CONTEXTS, "Palette ctxIdxOffer uv error 2")
}

use_palette_color_cache_y : unused_init, b_u(1) = -1;
use_palette_color_cache_u : unused_init, b_u(1) = -1;
palette_num_extra_bits_y : unused_init, b_u(2) = -1;
palette_num_extra_bits_u : unused_init, b_u(2) = -1;
palette_num_extra_bits_v : unused_init, b_u(2) = -1;
palette_delta_y : unused_init, b_u(paletteBits) = -1;
palette_delta_u : unused_init, b_u(paletteBits) = -1;
palette_delta_v : unused_init, b_u(paletteBits) = -1;
delta_encode_palette_colors_v : unused_init, b_u(1) = -1;
palette_delta_sign_bit_v : unused_init, b_u(1) = -1;
palette_colors_y : unused_init, b_u(bit_depth) = -1;
palette_colors_u : unused_init, b_u(bit_depth) = -1;
palette_colors_v : unused_init, b_u(bit_depth) = -1;

use_intrabc : unused_init, b_use_intrabc() = -1;

use_filter_intra : unused_init, b_use_filter_intra() = -1;
filter_intra_mode : unused_init, b_filter_intra_mode() = -1;


angle_delta_y : unused_init, b_angle_delta_y() = -1;
angle_delta_uv : unused_init, b_angle_delta_uv() = -1;

coded_id : unused_init, b_coded_id() = -1;

rem_bits  : unused_init, b_u(3) = -1;
abs_bits  : unused_init, b_u(rem_bits) = -1;
delta_q_sign_bit : unused_init, b_u(1) = -1;
abs : unused_init, b_abs() = -1;
delta_q_present : unused_init, b_u(1) = -1;
deltaQRes : unused_init, b_u(2) = -1;
rem_bits_lf  : unused_init, b_u(3) = -1;
abs_bits_lf  : unused_init, b_u(rem_bits_lf) = -1;
delta_lf_sign_bit : unused_init, b_u(1) = -1;
abs_lf : unused_init, b_abs_lf() = -1;
delta_lf_present : unused_init, b_u(1) = -1;
deltaLfRes : unused_init, b_u(2) = -1;

gm_bit_1 : unused_init, b_u(1) = -1;
gm_bit_2 : unused_init, b_u(1) = -1;
gm_bit_3 : unused_init, b_u(1) = -1;

cdef_idx : unused_init, b_u(cdef_bits) = -1;

cfl_alpha_signs : unused_init, b_cfl_alpha_signs() = -1;
cfl_alpha_u : unused_init, b_cfl_alpha() = CFL_CONTEXT_U(cfl_alpha_signs);
cfl_alpha_v : unused_init, b_cfl_alpha() = CFL_CONTEXT_V(cfl_alpha_signs);

useWiener : unused_init, b_use_wiener() = -1;
useSgrproj : unused_init, b_use_sgrproj() = -1;
restorationType : unused_init, b_restoration_type() = -1;

sgr_param_set : unused_init, b_u(SGRPROJ_PARAMS_BITS) = -1;
