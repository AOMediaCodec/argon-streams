/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

/*
The uncompressed header describes basic properties of the frame.
All fields in the uncompressed header consist of fixed numbers of bits.
However, some fields may not be present so the actual size of the header can vary.
*/
// A block is 4*4 pixels
// A mode info block is 8*8 pixels
// A super block is 64*64 pixels

#define VALIDATE_SPEC_TILES 0

#if ENCODE
// Extra choice function - select a value in a particular range,
// with a bias toward the specific values {min, max, 0} (0 only included
// if it is in range).
// This is similar in intent to ChooseSpecial(), but more specific
av1_choose_interesting(min, max) {
  ASSERT(min <= max, "Invalid range")
  z = Choose(0, 99) // Random %ile for selecting between options
  if (z < 10) {
    return min
  } else if (z < 20) {
    return max
  } else if (z < 30 && min < 0 && max > 0) {
    return 0
  } else {
    return Choose(min, max)
  }
}
#endif // ENCODE

// This is used to get integer constants equivalent to 1LL, so that we can
// apply shifts of potentially 31+ places without issues.
int64 convert_to_int64(x) {
  return x
}
uint64 convert_to_uint64(x) {
  return x
}

// These functions maybe should go into bitstream?
int64 ROUND_POWER_OF_TWO(int64 value, n) {
  return (value + ((convert_to_int64(1) << n) >> 1)) >> n
}
int64 ROUND_POWER_OF_TWO_SIGNED(int64 value, n) {
  return (value < 0) ? -ROUND_POWER_OF_TWO(-value, n) : ROUND_POWER_OF_TWO(value, n)
}

int ROUND_POWER_OF_TWO_SATCHECK(value, n) {
  intermediateResult = value + (1 << (n - 1))
#if ENCODE
  range_check_value(intermediateResult, bit_depth + 8)
#endif
  return intermediateResult >> n
}

// Round up to the next 2**n
int ALIGN_POWER_OF_TWO(value, n) {
  mask = (1<<n) - 1
  a = value + mask
  return a-(a&mask)
}

// Divide by a power of two, *rounding up*
int64 ROUND_POWER_OF_TWO_UP(int64 value, n) {
  offset = (convert_to_int64(1) << n) - 1
  return (value + offset) >> n
}

// Calculate x mod y, with the result being in the range [0, y-1]
// regardless of the sign of x and y.
int mod(x, y) {
  signed_remainder = x % y
  if (signed_remainder < 0) {
    return signed_remainder + y
  } else {
    return signed_remainder
  }
}

// Helpers for dealing with wrap-around counters:
// Take a value and wrap it to either an 'n' bit signed integer,
// or an 'n' bit unsigned integer.
int64 wrap_int(int64 v, n) {
  int64 m
  m = convert_to_int64(1) << (n-1)
  return (v & (m-1)) - (v & m)
}

int64 wrap_uint(int64 v, n) {
  int64 m
  m = convert_to_int64(1) << n
  return v & (m-1)
}

// Helper for unwrapping a "wrapped counter".
// The intended use case is to have a loop like this:
// prev = 0
// foreach frame {
//    signalledValue u(n)
//    current = unwrap_counter(prev, signalledValue, n)
//    [other frame processing]
//    prev = current
// }
// The idea is that the 'current' values will always
// increase monotonically, but the signalled value is
// only the lowest n bits and so will wrap around
int64 unwrap_counter(int64 prev, int64 lowBits, n) {
  int64 m
  m = (convert_to_int64(1) << n)
  CHECK(0 <= lowBits && lowBits < m, "Invalid wrapped counter value")

  int64 prevHighBits
  int64 prevLowBits
  int64 highBits

  prevHighBits = prev & ~(m-1)
  prevLowBits = prev & (m-1)

  if (lowBits < prevLowBits) {
    // If the low bits have decreased, then the counter must
    // have wrapped. So the high bits must have increased.
    highBits = prevHighBits + m
  } else {
    highBits = prevHighBits
  }

  return (highBits | lowBits)
}

uint_1_3 get_num_planes() {
  return monochrome ? 1 : MAX_MB_PLANE
}

read_superres() {
  if (!enable_superres) {
    use_superres = 0
    superres_denom = SUPERRES_NUM
    crop_width = upscaled_width
    crop_height = upscaled_height
    return 0
  }

#if ENCODE
  encode_setup_superres()
#endif // ENCODE
  use_superres u(1)
  if (use_superres) {
    codedDenom u(SUPERRES_DENOM_BITS)
    superres_denom = codedDenom + SUPERRES_DENOM_MIN
  } else {
    superres_denom = SUPERRES_NUM // 1:1 scaling
  }
  crop_width = (upscaled_width * SUPERRES_NUM + (superres_denom / 2)) / superres_denom
  crop_height = upscaled_height // Don't scale height

#if DECODE && COLLECT_STATS
  if (min_superres_denom == 0 || superres_denom < min_superres_denom) {
    min_superres_denom = superres_denom
  }
#endif
}

read_frame_size() {
  if (frame_size_override_flag) {
    frame_width_minus1 u(frame_width_bits)
    upscaled_width = frame_width_minus1+1
    frame_height_minus1 u(frame_height_bits)
    upscaled_height = frame_height_minus1+1
  } else {
    // Note: This isn't dependent on spatial_layer_max_{width,height}, since that data
    // may be in a (droppable) metadata OBU, and we must get the same results whether we
    // parsed that or not.
    upscaled_width = max_frame_width
    upscaled_height = max_frame_height
  }

  CHECK(upscaled_width <= spatial_layer_max_width[spatial_layer], "Signalled frame width > max frame width for this spatial layer")
  CHECK(upscaled_height <= spatial_layer_max_height[spatial_layer], "Signalled frame height > max frame height for this spatial layer")
}

compute_image_size() {
  // Note: For various reasons (including cdef), the decoded region of the frame must be a multiple
  // of 8 luma pixels wide and high, even if the visible region is not. So we pad out the visible
  // dimensions (crop_width, crop_height) when calculating mi_rows and mi_cols.
  mi_cols = (ALIGN_POWER_OF_TWO(crop_width,3)+(1<<MI_SIZE_LOG2)-1)>>MI_SIZE_LOG2  // Number of mode-info units across
  mi_rows = (ALIGN_POWER_OF_TWO(crop_height,3)+(1<<MI_SIZE_LOG2)-1)>>MI_SIZE_LOG2  // Number of mode-info units down
  ref_mi_cols[0] = mi_cols
  ref_mi_rows[0] = mi_rows
  stride = ALIGN_POWER_OF_TWO(upscaled_width, MAX_SB_SIZE_LOG2) // Align stride to superblock size
#if ENCODE
  totalPix=0
  for (plane = 0; plane < get_num_planes(); plane++) {
    totalPix+=((mi_rows * MI_SIZE)>>plane_subsampling_x[plane])*((mi_cols * MI_SIZE)>>plane_subsampling_y[plane])
  }
  countPix=0
#endif
}

// setup_render_size in libvpx
read_display_size() {
  has_scaling u(1)
  if (has_scaling) {
    display_width_minus1 u(16)
    display_width = display_width_minus1+1
    display_height_minus1 u(16)
    display_height = display_height_minus1+1
  } else {
    display_width = upscaled_width
    display_height = upscaled_height
  }
}

read_loopfilter() {
  // We set i before reading the filter_level bitstream elements, which gets
  // captured and passed to the config language (so code can override
  // filter_level[2] differently from filter_level[3] or whatever).
  i = 0
  filter_level[0] u(6)
  i = 1
  filter_level[1] u(6)
  if (get_num_planes() > 1) {
    if (filter_level[0] || filter_level[1]) {
      i = 2
      filter_level[2] u(6)
      i = 3
      filter_level[3] u(6)
    } else {
      filter_level[2] = 0
      filter_level[3] = 0
    }
  }
  sharpness_level u(3)
  mode_ref_delta_enabled u(1)

  if (mode_ref_delta_enabled) {
    mode_ref_delta_update u(1)
    if (mode_ref_delta_update) {
      for (i = 0; i<TOTAL_REFS_PER_FRAME;i++) {
        update_ref_delta u(1)
        if (update_ref_delta) {
          ref_lf_deltas[i] su(6)
        }
      }
      for (i = 0; i<MAX_MODE_LF_DELTAS;i++) {
        update_mode_delta u(1)
        if (update_mode_delta) {
          mode_lf_deltas[i] su(6)
        }
      }
    }
  }
}

int5 read_delta_q(yuv, is_ac) {
  uint6 adelta // TODO add sign flag for delta to cross
  delta_coded u(1)

  delta = 0
  if (delta_coded) {
#if ENCODE
    // If enc_lossless is true, we always want to write a delta of
    // zero. Otherwise, we evaluate enc_delta_q to decide what to
    // write.
    if (! enc_lossless) {
      enc_delta_q u(0)
      CHECK (-64 <= enc_delta_q && enc_delta_q <= 63,
             "enc_delta_q is %d, which can't be encoded as su(1+6).",
             enc_delta_q)
      delta = enc_delta_q
    }
#endif
    delta su(6)
    adelta = Abs(delta)
  }

  return delta
}

read_quantization() {
#if ENCODE
  enc_base_qindex u(0)
  if (enc_base_qindex < 0) {
    // enc_base_qindex is negative if it hasn't been overridden
    // explicitly. In which case, the enc_pick_base_qindex function
    // (which is the default behaviour for base_qindex element) will
    // base its behaviour on the enc_lossless and enc_special_lossless
    // flags.
    enc_lossless u(0)
    enc_special_lossless u(0)
  } else {
    // If enc_base_qindex is non-negative, it was set explicitly. We
    // should set enc_lossless and enc_special_lossless to match.
    if (enc_base_qindex == 0) {
      enc_lossless u(0)
    } else {
      enc_lossless = 0
    }

    // It's possible to take the enc_special_lossless route to get a
    // lossless encoding iff enc_base_qindex is at most equal to
    // seg_feature_data_max[SEG_LVL_ALT_Q].
    uint32 uenc_base_qindex
    uenc_base_qindex = enc_base_qindex
    if (uenc_base_qindex <= seg_feature_data_max[SEG_LVL_ALT_Q]) {
      enc_special_lossless u(0)
    } else {
      enc_special_lossless = 0
    }
  }
#endif

  base_qindex u(QINDEX_BITS)
  dc_delta_qindex[0] = read_delta_q(0, 0)
#if ENCODE
  // For bit-stream generator
  DeltaQYDc = dc_delta_qindex[0]
  DeltaQUDc = 0
  DeltaQUAc = 0
  DeltaQVDc = 0
  DeltaQVAc = 0
#endif // ENCODE
  ac_delta_qindex[0] = 0

  if (get_num_planes() > 1) {
    if (separate_uv_delta_q)
      diffUVDelta u(1)
    else
      diffUVDelta = 0

    dc_delta_qindex[1] = read_delta_q(1, 0)
    ac_delta_qindex[1] = read_delta_q(1, 1)
    if (diffUVDelta) {
      dc_delta_qindex[2] = read_delta_q(2, 0)
      ac_delta_qindex[2] = read_delta_q(2, 1)
    } else {
      dc_delta_qindex[2] = dc_delta_qindex[1]
      ac_delta_qindex[2] = ac_delta_qindex[1]
    }
  }

#if ENCODE
  DeltaQUDc = dc_delta_qindex[1]
  DeltaQUAc = ac_delta_qindex[1]
  DeltaQVDc = dc_delta_qindex[2]
  DeltaQVAc = ac_delta_qindex[2]
#endif // ENCODE

  using_qmatrix u(1)
  if (using_qmatrix) {
    qm_y u(QM_LEVEL_BITS)
    qm_u u(QM_LEVEL_BITS)
    if (!separate_uv_delta_q) {
      qm_v = qm_u
    } else {
      qm_v u(QM_LEVEL_BITS)
    }
  }
}

uint8 read_prob() {
  prob_coded u(1)
  if (prob_coded) {
    prob u(8)
  } else {
    prob = MAX_PROB
  }
  return prob
}

set_feature_data(segmentId, feature, data) {
  feature_data[segmentId][feature] = data
}

int32 get_segdata_idx(idx, feature) {
  return feature_data[idx][feature]
}

int32 get_segdata(feature) {
  return get_segdata_idx(segment_id, feature)
}

enable_feature(segmentId,feature) {
  feature_mask[segmentId] |= 1 << feature
  FeatureEnabled[segmentId][feature] = 1
}


read_segmentation_data() {
  clearall_segfeatures() // Turn all features off, and only reenable the ones explicitly enabled
#if ENCODE
  encNumSegments u(0) // Select how many segments can have nonzero feature flags
#endif
  for (segmentId = 0; segmentId<MAX_SEGMENTS; segmentId++) {
    for (feature = 0; feature<SEG_LVL_MAX;feature++) {
      feature_enabled u(1)
      data = 0
      if (feature_enabled) {
        preskip_seg_id |= (feature >= SEG_LVL_REF_FRAME)
        last_active_seg_id = (segmentId)
        enable_feature(segmentId,feature)
        maxValue = seg_feature_data_max[feature]
        numbits = get_unsigned_bits(maxValue)
        isSigned = seg_feature_data_signed[feature]
        if (isSigned) {
          decode_data su(numbits)
          data = clamp(decode_data,-maxValue,maxValue)
        } else {
          decode_data u(numbits)
          data = Min(decode_data,maxValue)
        }
      }
      set_feature_data(segmentId, feature, data)
    }
  }
}

read_segmentation() {
  seg_update_map = 0
  seg_update_data = 0
  seg_temporal_update = 0
  preskip_seg_id = 0
  seg_enabled u(1)
  if (!seg_enabled) {
    clear_segmap()
    clearall_segfeatures()
    return 0
  }
  // Decide if we need to clear out the prev_frame_seg_map
  if ((frame_type == KEY_FRAME) ||
      (ref_mi_rows[1] != mi_rows) ||
      (ref_mi_cols[1] != mi_cols)) {
    for (i=0; i<mi_rows*mi_cols; i++)
      prev_frame_seg_map[i] = 0
  }

  if (primary_ref_frame == PRIMARY_REF_NONE) {
    seg_update_map = 1
    seg_temporal_update = 0
    seg_update_data = 1
  } else {
    seg_update_map u(1)
    if (seg_update_map) {
      seg_temporal_update u(1)
    } else {
      seg_temporal_update = 0
    }
    seg_update_data u(1)
  }
  if (seg_update_data) {
    read_segmentation_data()
  } else {
    // Load segment features from prev_frame
    copy_segfeatures(0, 1)
  }

  SegIdPreSkip = preskip_seg_id
  LastActiveSegId = last_active_seg_id
}

delta_q_params( ) {
  delta_q_res = 1
  delta_q_present = 0
  delta_q_allowed = 1
  delta_lf_res = 1
  delta_lf_present = 0
  delta_lf_multi = 0
  if ( delta_q_allowed && base_qindex > 0 ) {
    delta_q_present u(1)
    CHECK(!(coded_lossless && delta_q_present),
          "It is a requirement of bit-stream conformance that delta_q_present be 0 when CodedLossless is true")
  }
  if ( delta_q_present ) {
    deltaQRes u(2)
    delta_q_res = 1 << deltaQRes
    if (allow_intrabc) {
      return 0
    }
      delta_lf_present u(1)
      if ( delta_lf_present ) {
        deltaLfRes u(2)
        delta_lf_res = 1 << deltaLfRes
        delta_lf_multi u(1)
        //for (i = 0; i < FRAME_LF_COUNT; i++)
        //  prev_delta_lf[i] = 0
      }
  }
}

mi_cols_aligned_to_sb(mis) {
  return ALIGN_POWER_OF_TWO(mis, mib_size_log2)
}

// Calculate the smallest k >= 0 such that blk_size << k >= target.
// Note: We have to do things this way since blk_size isn't always a power of 2.
tile_log2(blk_size, target) {
  ASSERT(blk_size > 0, "Invalid argument to tile_log2()")
  for (k = 0; (blk_size << k) < target; k++) {
    :pass
  }
  return k
}

read_tile_info_max_tile() {
  sb_cols = ROUND_POWER_OF_TWO_UP(mi_cols, mib_size_log2)
  sb_rows = ROUND_POWER_OF_TWO_UP(mi_rows, mib_size_log2)

  sbSizeLog2 = mib_size_log2 + MI_SIZE_LOG2
  max_tile_width_sb = MAX_TILE_WIDTH >> sbSizeLog2
  max_tile_area_sb = (MAX_TILE_AREA >> (2 * sbSizeLog2))

  min_log2_tile_cols = tile_log2(max_tile_width_sb, sb_cols)
  max_log2_tile_cols = tile_log2(1, Min(sb_cols, MAX_TILE_COLS))
  max_log2_tile_rows = tile_log2(1, Min(sb_rows, MAX_TILE_ROWS))
  min_log2_tiles = Max(min_log2_tile_cols, tile_log2(max_tile_area_sb, sb_rows * sb_cols))

  // Work out the constraints on the number of tiles/tile cols coming
  // from the current level.
  // In the encoder, this must be satisfied for every level at which
  // the current frame will be decoded - conveniently, this is equivalent
  // to looking at just the lowest level which could be used.
#if ENCODE
  if (enc_allow_too_many_tiles) {
    level = LEVEL_MAX_PARAMS
  } else {
    level = enc_layers_to_min_level[spatial_layer][temporal_layer]
  }
#else
  level = operating_point_level[current_operating_point_id]
#endif
  levelMaxTiles = level_max_tiles[compact_level[level]][0]
  levelMaxTileCols = level_max_tiles[compact_level[level]][1]
#if ENCODE
  if (large_scale_tile) {
    ASSERT(levelMaxTileCols >= enc_tile_cols, "Invalid LST enc_tile_cols")
    ASSERT(levelMaxTiles >= (enc_tile_cols*enc_tile_rows), "Invalid LST enc_tile_cols*rows")
  }
#endif

  uniform_tile_spacing_flag u(1)
#if VALIDATE_SPEC_TILES
  validate(4444)
#endif
  if (uniform_tile_spacing_flag) {
#if VALIDATE_SPEC_TILES
    validate(4445)
#endif
    log2_tile_cols = min_log2_tile_cols
    while (log2_tile_cols < max_log2_tile_cols) {
      more_tile_cols u(1)
      if (!more_tile_cols)
        break
      log2_tile_cols += 1
    }

    tile_width_sb = ROUND_POWER_OF_TWO_UP(sb_cols, log2_tile_cols)
    ASSERT(tile_width_sb <= max_tile_width_sb, "Tiles are too wide")
    ASSERT((tile_width_sb << sbSizeLog2) * superres_denom / SUPERRES_NUM <= MAX_TILE_WIDTH, "Tiles don't meet superres width restriction")
    i = 0
    for (sb_col = 0; sb_col < sb_cols; sb_col += tile_width_sb) {
      mi_col_starts[i] = sb_col << mib_size_log2
#if VALIDATE_SPEC_TILES
      validate(mi_col_starts[ i ])
#endif
      i += 1
    }
    mi_col_starts[i] = mi_cols
    if (large_scale_tile) {
      ASSERT(mi_col_starts[i] - mi_col_starts[i-1] == (tile_width_sb << mib_size_log2),
             "Tile column %d has is %d MI units wide; LST mode means it should be %d.",
             i - 1,
             mi_col_starts[i] - mi_col_starts[i-1],
             (tile_width_sb << mib_size_log2))
    }
    tile_cols = i

    levelMaxTileRows = levelMaxTiles / tile_cols

    min_log2_tile_rows = (min_log2_tiles > log2_tile_cols) ? min_log2_tiles - log2_tile_cols : 0
    max_tile_height_sb = sb_rows >> min_log2_tile_rows
    log2_tile_rows = min_log2_tile_rows
    while (log2_tile_rows < max_log2_tile_rows) {
      more_tile_rows u(1)
      if (!more_tile_rows)
        break
      log2_tile_rows += 1
    }
    tile_width = Min(tile_width_sb << mib_size_log2, mi_cols)

    tile_height_sb = ROUND_POWER_OF_TWO_UP(sb_rows, log2_tile_rows)
    i = 0
    for (sb_row = 0; sb_row < sb_rows; sb_row += tile_height_sb) {
      mi_row_starts[i] = sb_row << mib_size_log2
#if VALIDATE_SPEC_TILES
      validate(mi_row_starts[ i ])
#endif
      i += 1
    }
    mi_row_starts[i] = mi_rows
    if (large_scale_tile) {
      ASSERT(mi_row_starts[i] - mi_row_starts[i-1] == (tile_height_sb << mib_size_log2),
             "Tile row %d has is %d MI units high; LST mode means it should be %d.",
             i - 1,
             mi_row_starts[i] - mi_row_starts[i-1],
             (tile_height_sb << mib_size_log2))
    }
    tile_rows = i
    ASSERT(tile_width_sb * tile_height_sb <= max_tile_area_sb, "Tiles are too big")
    tile_height = Min(tile_height_sb << mib_size_log2, mi_rows)
  } else {
#if VALIDATE_SPEC_TILES
    validate(4446)
#endif
#if ENCODE
    // The usual mode has enc_specify_nonuniform_tiles set to zero. In
    // which case, enc_pick_nonuniform_tile_sizes comes up with a
    // sensible layout. If enc_specify_nonuniform_tiles is nonzero, we
    // come up with the tile sizes on the fly. We expect the
    // profile/config file to have specified values for the sizes too
    // but default to 1 SB each time if it didn't.
    enc_specify_nonuniform_tiles u(0)
    if (! enc_specify_nonuniform_tiles) {
      enc_pick_nonuniform_tile_sizes(level)
    }
#endif
    // For non-uniform tile widths, we need to explicitly read the tile widths
    widest_tile_sb = 0
    start_sb = 0
    i = 0
    while (start_sb < sb_cols && i < MAX_TILE_COLS) {
      mi_col_starts[i] = start_sb << mib_size_log2
#if VALIDATE_SPEC_TILES
      validate(mi_col_starts[i])
#endif
      max_width = Min(sb_cols - start_sb, max_tile_width_sb)
#if ENCODE
      ASSERT(! large_scale_tile,
             "Non-uniform tile spacing not supported for LST in aomdec")

      if (enc_specify_nonuniform_tiles) {
        width_in_sbs_minus_1 u(0)
        size_sb = width_in_sbs_minus_1 + 1
      } else {
        size_sb = enc_nonuniform_tile_width[i]
      }

      ASSERT (0 < size_sb,
              "Non-uniform tile width must be positive. Got %d SBs.",
              size_sb)
      ASSERT (size_sb <= max_width,
              "Non-uniform tile width is %d, greater than max_width = %d (cols left: %d; max tile width SB: %d).",
              size_sb, max_width, sb_cols - start_sb, max_tile_width_sb)

      encode_quniform_bits(max_width, size_sb - 1)

#else // ENCODE
      size_sb = decode_quniform_bits(max_width) + 1
#endif // ENCODE

      // Track the width of the widest tile.
      if (size_sb > widest_tile_sb)
        widest_tile_sb = size_sb

      start_sb += size_sb
      i += 1
    }
    ASSERT ((start_sb << mib_size_log2) == ALIGN_POWER_OF_TWO(mi_cols, mib_size_log2),
            "Rightmost tile is the wrong width (start_sb is %d MI units, not %d)",
            start_sb << mib_size_log2,
            ALIGN_POWER_OF_TWO (mi_cols, mib_size_log2))

    mi_col_starts[i] = mi_cols
    tile_cols = i
    log2_tile_cols = tile_log2(1, tile_cols)

    levelMaxTileRows = levelMaxTiles / tile_cols

    if (min_log2_tiles)
      max_tile_area_sb = (sb_rows * sb_cols) >> (min_log2_tiles + 1)
    else
      max_tile_area_sb = sb_rows * sb_cols
    max_tile_height_sb = Max(max_tile_area_sb / widest_tile_sb, 1)

    start_sb = 0
    i = 0
    while (start_sb < sb_rows && i < MAX_TILE_ROWS) {
      mi_row_starts[i] = start_sb << mib_size_log2
#if VALIDATE_SPEC_TILES
      validate(mi_row_starts[ i ])
#endif
      max_height = Min(sb_rows - start_sb, max_tile_height_sb)
#if ENCODE
      if (enc_specify_nonuniform_tiles) {
        height_in_sbs_minus_1 u(0)
        size_sb = height_in_sbs_minus_1 + 1
      } else {
        size_sb = enc_nonuniform_tile_height[i]
      }

      ASSERT (0 < size_sb,
              "Non-uniform tile height must be positive. Got %d SBs.",
              size_sb)
      ASSERT (size_sb <= max_height,
              "Non-uniform tile height is %d, greater than max_height = %d (rows left: %d; max tile width SB: %d).",
              size_sb, max_height, sb_rows - start_sb, max_tile_height_sb)

      encode_quniform_bits(max_height, size_sb - 1)
#else // ENCODE
      size_sb = decode_quniform_bits(max_height) + 1
#endif // ENCODE
      start_sb += size_sb
      i += 1
    }
    ASSERT((start_sb << mib_size_log2) == ALIGN_POWER_OF_TWO(mi_rows, mib_size_log2),
            "Bottommost tile is the wrong height (start_sb is %d MI units, not %d)",
            start_sb << mib_size_log2,
            ALIGN_POWER_OF_TWO (mi_rows, mib_size_log2))

    mi_row_starts[i] = mi_rows
    tile_rows = i
    log2_tile_rows = tile_log2(1, tile_rows)
  }

  // Check level-dependent constraints (if level is in the table)
  if (operating_point_level[0] != 31) {
    CHECK(tile_cols <= levelMaxTileCols, "[TILE_BOUNDS] Too many tile columns (%d) for selected level (Max: %d)",tile_cols,levelMaxTileCols)
    CHECK(tile_rows * tile_cols <= levelMaxTiles, "[TILE_BOUNDS] Too many tiles (%d) for selected level (Max: %d)",tile_rows*tile_cols,levelMaxTiles)
    CHECK(crop_width - (mi_col_starts[tile_cols-1]*MI_SIZE) >= 8, "[CROPPEDTILE] CroppedTileWidth (%d) must be greater than or equal to 8 for each tile",crop_width - (mi_col_starts[tile_cols-1]*MI_SIZE))
    CHECK(crop_height - (mi_row_starts[tile_rows-1]*MI_SIZE) >= 8, "[CROPPEDTILE] CroppedTileHeight (%d = %d - %d) must be greater than or equal to 8 for each tile",crop_height - (mi_row_starts[tile_rows-1]*MI_SIZE),crop_height, (mi_row_starts[tile_rows-1]*MI_SIZE))
  }
}

read_tiles() {
  read_tile_info_max_tile()

  uint32 numTiles
  numTiles = tile_rows * tile_cols

  CSV(CSV_TILES, numTiles)
  CSV(CSV_TILECOLS, tile_cols)
  CSV(CSV_TILEROWS, tile_rows)

  if (numTiles > 1) {
    context_tile_id u(log2_tile_rows + log2_tile_cols)
    CHECK(context_tile_id < numTiles, "Invalid tile selected for context update")
  } else {
    context_tile_id = 0
  }


  if (tile_rows > 1 || tile_cols > 1) {
#if ENCODE
    tile_sz_mag_location = current_bitposition()
    tile_sz_mag = 0 // overwritten later
#endif // ENCODE
    tile_sz_mag u(2)
  }
}


frame_size_with_refs() {
  found = 0
  for (i = 0; i < ALLOWED_REFS_PER_FRAME; i++) {
    use_ref_frame u(1)
    if (use_ref_frame) {
      refIdx = active_ref_idx[i]
      upscaled_width = ref_width[refIdx]
      upscaled_height = ref_height[refIdx]
      display_width = ref_display_width[refIdx]
      display_height = ref_display_height[refIdx]
      read_superres()
      found = 1
      break
    }
  }

  if (!found) {
    read_frame_size()
    read_superres()
    read_display_size()
  }

  // TODO should we clear frame buffers in setup independence?
  CHECK(crop_width && crop_height,"Referenced frame with invalid size")

  compute_image_size()
}

read_mcomp_filter_type() {
  filter_type_switchable u(1)
  if (filter_type_switchable) {
    mcomp_filter_type = SWITCHABLE
  } else {
    mcomp_filter_type u(2)
  }
}

read_bitdepth_colorspace_subsampling() {
  use10or12bit u(1)
  if (profile >= 2 && use10or12bit) {
    use12bit u(1)
    bit_depth = use12bit ? 12 : 10
  } else {
    bit_depth = use10or12bit ? 10 : 8
  }

  isMonochrome = 0
  if (profile != 1) {
    isMonochrome u(1)
#if ENCODE
    CHECK(!enc_large_scale_tile || !isMonochrome, "Monochrome is not allowed in large scale tile streams")
#endif //ENCODE
  }
  monochrome = isMonochrome
  NumPlanes = monochrome ? 1 : 3

#if ENCODE
  colorDescriptionPresent = 1
  if (enc_color_primaries == AOM_CICP_CP_UNSPECIFIED &&
      enc_transfer_characteristics == AOM_CICP_TC_UNSPECIFIED &&
      enc_matrix_coefficients == AOM_CICP_MC_UNSPECIFIED) {
    enc_color_description_present_flag u(0)
    colorDescriptionPresent = enc_color_description_present_flag
  }
#endif
  colorDescriptionPresent u(1)

  if (colorDescriptionPresent) {
    color_primaries u(8)
    transfer_characteristics u(8)
    matrix_coefficients u(8)
  } else {
    color_primaries = AOM_CICP_CP_UNSPECIFIED
    transfer_characteristics = AOM_CICP_TC_UNSPECIFIED
    matrix_coefficients = AOM_CICP_MC_UNSPECIFIED
  }
  if (isMonochrome) {
    range16 u(1) // [16,235] vs [0,255] range
    color_range = range16
    subsampling_x = 1
    subsampling_y = 1
    chroma_sample_position = CSP_UNKNOWN
    separate_uv_delta_q = 0
  } else {
    isSRGB = (color_primaries == AOM_CICP_CP_BT_709 &&
              transfer_characteristics == AOM_CICP_TC_SRGB &&
              matrix_coefficients == AOM_CICP_MC_IDENTITY)
    if (isSRGB) {
      // sRGB requires 4:4:4 subsampling
      CHECK(profile == 1 || (profile == 2 && bit_depth == 12),
            "SRGB colorspace requires not compatible with profile")
      range16 = 1
      subsampling_x = 0
      subsampling_y = 0
    } else {
      range16 u(1) // [16,235] vs [0,255] range

      // Select subsampling based on profile
      if (profile == 0) {
        // 4:2:0, 8/10 bit
        subsampling_x = 1
        subsampling_y = 1
      } else if (profile == 1) {
        // 4:4:4, 8/10 bit
        subsampling_x = 0
        subsampling_y = 0
      } else {
        if (bit_depth == 12) {
          // 4:4:4 / 4:2:2 / 4:2:0 [/ 4:4:0, but I think that shouldn't be supported], 12-bit
          subsampling_x u(1)
          if (subsampling_x == 0)
            subsampling_y = 0
          else
            subsampling_y u(1)
        } else {
          // 4:2:2, 8/10 bit
          subsampling_x = 1
          subsampling_y = 0
        }
      }

      if (matrix_coefficients == AOM_CICP_MC_IDENTITY) {
        CHECK(subsampling_x == 0 && subsampling_y == 0,
              "matrix_coefficients == IDENTITY requires 4:4:4 subsampling")
      }

      if (subsampling_x && subsampling_y)
        chroma_sample_position u(2)
    }

    separate_uv_delta_q u(1)
  }

#if ENCODE
  ASSERT(subsampling_x == enc_subsampling_x && subsampling_y == enc_subsampling_y,
         "subsampling_{x,y} disagrees with enc_subsampling_{x,y}")
#endif

  if (matrix_coefficients == AOM_CICP_MC_IDENTITY) {
    ASSERT(subsampling_x == 0 && subsampling_y == 0,
           "IDENTITY matrix coefficients can only be used with 4:4:4 subsampling")
  }

  setup_block_dptrs(subsampling_x, subsampling_y)
}

// tx_mode can choose maximum transform size 4x4, 8x8, 16x16, 32x32
// extra_mode_bit says to allow configurable tx_size per block
uint_0_4 read_tx_mode() {
  uint_0_4 mode
  if (coded_lossless) {
    return ONLY_4X4
  }

  // Note this is now read in the uncompressed header
  extra_mode_bit u(1)
  if (extra_mode_bit) {
    mode = TX_MODE_SELECT
  } else {
    mode = TX_MODE_LARGEST
  }
  return mode
}

// TODO better as binarisation?
uint_0_2 read_comp_pred_mode() {
  uint_0_2 pred_mode
  pred_mode u(1)
  reference_select = pred_mode
  return pred_mode ? HYBRID_PREDICTION : SINGLE_PREDICTION_ONLY
}

setup_compound_prediction() {
  comp_fwd_ref[0] = LAST_FRAME
  comp_fwd_ref[1] = LAST2_FRAME
  comp_fwd_ref[2] = LAST3_FRAME
  comp_fwd_ref[3] = GOLDEN_FRAME

  comp_bwd_ref[0] = BWDREF_FRAME
  comp_bwd_ref[1] = ALTREF2_FRAME
  comp_bwd_ref[2] = ALTREF_FRAME
}

read_ext_skip() {
  skip_mode_allowed = 0
  // Only signal for compound inter frames
  if (frame_type == KEY_FRAME || intra_only || comp_pred_mode == SINGLE_PREDICTION_ONLY ||
      !enable_order_hint) {
    return 0
  }

  // Work out the two closest "forward" (earlier in time) reference frames, as well as
  // the closest "backward" (later in time) reference frame.
  // Note that the two forward refs are required to be distinct - repeats of
  // the same frame offset are ignored
  int8 fwdRef[2]
  int fwdRefOffset[2]
  fwdRef[0] = 0
  fwdRef[1] = 0
  fwdRefOffset[0] = -1
  fwdRefOffset[1] = -1
  bwdRef = 0
  bwdRefOffset = -1

  curOffset = cur_frame_offset
  for (i = LAST_FRAME; i <= ALTREF_FRAME; i++) {
    refOffset = ref_decode_order[i]
    if (get_relative_dist(refOffset, curOffset) < 0) {
      // Forward reference - want to keep the frame with the largest "offset" value
      if (fwdRefOffset[0] == -1 || get_relative_dist(refOffset, fwdRefOffset[0]) > 0) {
        // This is the new closest ref
        fwdRef[0] = i
        fwdRefOffset[0] = refOffset
      }
    } else if (get_relative_dist(refOffset, curOffset) > 0) {
      // Backward reference - want to keep the frame with the smallest "offset" value
      if (bwdRefOffset == -1 || get_relative_dist(refOffset, bwdRefOffset) < 0) {
        bwdRef = i
        bwdRefOffset = refOffset
      }
    }
  }

  if (fwdRefOffset[0] != -1 && bwdRefOffset != -1) {
    // Bidirectional prediction - use closest fwd+bwd refs
    skip_mode_allowed u(1)
    skip_ref[0] = Min(fwdRef[0], bwdRef)
    skip_ref[1] = Max(fwdRef[0], bwdRef)
  } else if (fwdRefOffset[0] != -1) {
    // We found a forward ref, but no backward refs. Search for a second forward
    // ref.
    // We must do things this way, rather than searching everything together,
    // in order to match the results of the reference code / spec in all cases
    for (i = LAST_FRAME; i <= ALTREF_FRAME; i++) {
      refOffset = ref_decode_order[i]
      if (get_relative_dist(refOffset, fwdRefOffset[0]) < 0) {
        if (fwdRefOffset[1] == -1 || get_relative_dist(refOffset, fwdRefOffset[1]) > 0) {
          fwdRef[1] = i
          fwdRefOffset[1] = refOffset
        }
      }
    }

    if (fwdRefOffset[1] != -1) {
      // Use the two closest forward refs
      skip_mode_allowed u(1)
      skip_ref[0] = Min(fwdRef[0], fwdRef[1])
      skip_ref[1] = Max(fwdRef[0], fwdRef[1])
    }
  }
}

read_timing_info() {
  num_units_in_display_tick u(32)

#if ENCODE

  // At this point, we know
  //
  //    enc_frame_rate:                 Target frame rate
  //    enc_num_units_in_decoding_tick: Decode clock divider
  //    enc_num_units_in_display_tick:  Display clock divider
  //    enc_num_ticks_per_picture:      Further divide display clock
  //
  // A clock running at time_scale ticks/second will tick
  //
  //    enc_num_units_in_display_tick * enc_num_ticks_per_picture
  //
  // times per picture. This needs to happen enc_frame_rate times per second,
  // yielding:
  time_scale = (enc_frame_rate *
                enc_num_units_in_display_tick *
                enc_num_ticks_per_picture)

#endif
  time_scale u(32)

  // Note: In the spec, DispCT = (num_units_in_display_tick / time_scale) seconds,
  // where the division is exact.
  // Here, we measure in units of 1/90000 of a second, and round up so that
  // the display clock never runs "too fast"
  DispCT = (num_units_in_display_tick * 90000 + (time_scale-1)) / time_scale
  equal_picture_interval u(1)
  if (equal_picture_interval) {
#if ENCODE
    num_ticks_per_picture u(0)
    write_uvlc(num_ticks_per_picture - 1)
#else
    num_ticks_per_picture = read_uvlc() + 1
#endif
  }
}

read_decoder_model_info() {
  encoder_decoder_buffer_delay_length u(5)
  encoder_decoder_buffer_delay_length += 1
  num_units_in_decoding_tick u(32)
  // DecCT is calculated similarly to DispCT above
  DecCT = (num_units_in_decoding_tick * 90000 + (time_scale-1)) / time_scale
  buffer_removal_delay_length u(5)
  buffer_removal_delay_length += 1
  frame_presentation_delay_length u(5)
  frame_presentation_delay_length += 1
}

read_decoder_model_oppoint_info(i) {
  decoder_model_param_present_flag[i] = 0
  if (decoder_model_info_present_flag) {
    ASSERT(decoder_model_mode == DECODER_MODEL_SCHEDULE, "We should only get here in DECODER_MODEL_SCHEDULE mode")
    decoder_model_param_present_flag[i] u(1)
  }

  if (decoder_model_mode == DECODER_MODEL_SCHEDULE) {
    ASSERT(decoder_model_param_present_flag[i], "For DECODER_MODEL_SCHEDULE, all operating points must have decoder model parameters")
  }

  if (decoder_model_param_present_flag[i]) {
    decoder_buffer_delay[i] u(encoder_decoder_buffer_delay_length)
    encoder_buffer_delay[i] u(encoder_decoder_buffer_delay_length)
    low_delay_mode_flag[i]  u(1)
  } else {
    // Default values are set for all operating points in
    // read_sequence_header.
  }
}

read_tu_pts_info() {
#if ENCODE
  enc_tu_presentation_delay_offset = current_bitposition() - frameHeaderStart
  tuPresentationDelay u(frame_presentation_delay_length)
  // The correct value will be filled in once the frame is fully encoded
#else
  i = current_operating_point_id
  dispIdx = current_show_frame_idx[i]
  int64 prevTime
  if (dispIdx == 0) {
    prevTime = 0
  } else {
    prevTime = frame_presentation_time[i][dispIdx - 1]
  }
  tuPresentationDelay u(frame_presentation_delay_length)
  frame_presentation_time[i][dispIdx] = unwrap_counter(prevTime, tuPresentationDelay, frame_presentation_delay_length)
#endif
}

// Read the level for the i'th operating point,
// and split up into major/minor parts
read_operating_point_level(i, readTier) {
  level u(LEVEL_BITS)
  CHECK(compact_level[level] != -1, "Invalid / undefined operating point level")
#if ENCODE
  enc_choose_max_bitrate u(0)
  enc_override_tier u(0)
  CHECK(level == enc_operating_point_level[i] || level == LEVEL_MAX_PARAMS, "Level mismatch in encoder")
#endif

  operating_point_level[i] = level

  majorLevel = (level >> LEVEL_MINOR_BITS) + LEVEL_MAJOR_MIN
  minorLevel = level & LEVEL_MINOR_MASK
  if (readTier && majorLevel > 3) {
    tier[i] u(1)
  } else {
    tier[i] = 0
  }

  // Work out the maximum bitrate and buffer size for this operating point
  compactedLevel = compact_level[operating_point_level[i]]
  MaxBitrate[i] = level_bitrate[compactedLevel][tier[i]] * profile_bitrate_factor[profile]
  MaxBufferSize[i] = MaxBitrate[i] // * 1 second, but that's handled by our choice of units
}

// Reset timing information to the baseline that came from command line
// arguments. This sets values that will be overridden in read_timing_info if
// timing information is present.
//
// If we're decoding, the default frame rate can be specified on the command
// line. If encoding, the higher the frame rate we choose, the more finicky the
// checks will be. Let's be super-lax and choose a notional frame rate of 1 fps.
reset_timing_info() {
  num_units_in_display_tick = 1
#if DECODE
  time_scale = default_frame_rate
#else
  time_scale = 1
#endif
  DispCT = ((90000 + (time_scale - 1)) / time_scale)
  equal_picture_interval = 1
  num_ticks_per_picture = 1
}

read_sequence_header() {
  SeenSeqHeaderBeforeFirstFrameHeader = 1
  SeenSeqHeader = 1
#if DECODE
  start_md5()
#endif
  read_profile()

  still_picture u(1)
  reduced_still_picture_hdr u(1)
  CHECK(still_picture == 1 || reduced_still_picture_hdr == 0,
        "reduced_still_picture_hdr=1 is only allowed if still_picture=1")

  // Set default values for decoder model parameters across all operating
  // points. read_decoder_model_oppoint_info will set them again for operating
  // point i if decoder_model_param_present_flag[i] is true, but this way we
  // ensure we have sensible defaults.
  for (i = 0; i <= 31; i++) {
    decoder_buffer_delay[i] = 70000
    encoder_buffer_delay[i] = 20000
    low_delay_mode_flag[i]  = 0
  }

  // Reset the timing info structure here. If timing info is present, we'll
  // overwrite it again in a second with correct values but if it isn't, we
  // need to default to something sensible.
  //
  // This isn't part of the spec: it's designed to implement the note in E3.3
  // that says if timing info isn't present in the bitstream, the decoder has
  // to provide it some other way(!).
  reset_timing_info()

  if (reduced_still_picture_hdr) {
    timing_info_present = 0
    display_model_info_present_flag = 0
    decoder_model_info_present_flag = 0
    decoder_model_param_present_flag[0] = 0
    display_model_param_present_flag[0] = 0
    decoder_model_mode = DECODER_MODEL_RESOURCE_AVAILABILITY
    num_operating_points = 1
    current_operating_point_id = 0

    // Use special case: If the spatial and temporal layer masks
    // are both 0, that actually means "Decode all OBUs"
    spatial_layers_used[0] = 0
    temporal_layers_used[0] = 0
    read_operating_point_level(0, 0)
    decoder_rate_model_present[0] = 0
  } else {
    timing_info_present u(1)
    if (timing_info_present) {
      read_timing_info()
      decoder_model_info_present_flag u(1)
      if (decoder_model_info_present_flag) {
        read_decoder_model_info()
      } else {
        // The spec doesn't actually give a default value
        // for num_units_in_decoding_tick, so default to 1
        num_units_in_decoding_tick = 1
        DecCT = (num_units_in_decoding_tick * 90000 + (time_scale-1)) / time_scale
      }
    } else {
      decoder_model_info_present_flag = 0
    }

    // Work out which decoder model mode is in use
    decoder_model_mode = (decoder_model_info_present_flag ?
                          DECODER_MODEL_SCHEDULE :
                          DECODER_MODEL_RESOURCE_AVAILABILITY)

    // If we have any anchor frames, we're in large-scale-tile mode.
    // This behaves differently and the decoder model is essentially
    // disabled.
    if (num_large_scale_tile_anchor_frames) {
      decoder_model_mode = DECODER_MODEL_DISABLED
    }

    // The av1src encoder and decoder both assume that a stream with
    // non-equal picture interval has decoder model info and operates
    // in DECODER_MODEL_SCHEDULE mode. This isn't guaranteed by the
    // spec, but it's all we support.
    if (timing_info_present &&
        (decoder_model_mode == DECODER_MODEL_RESOURCE_AVAILABILITY)) {
      ASSERT (equal_picture_interval,
              "av1src only supports equal picture interval without decoder model info.")
    }

#if ENCODE
    // In encode mode, we might actually disable the decoder model
    // entirely.
    if (enc_decoder_model_mode == DECODER_MODEL_DISABLED && ! timing_info_present) {
      decoder_model_mode = DECODER_MODEL_DISABLED
    }
    ASSERT(decoder_model_mode == enc_decoder_model_mode,
           "Decoder model mode mismatch")
#endif

#if ENCODE
    if (enc_large_scale_tile || enc_special_operating_points) {
      display_model_info_present_flag = 0
    } else {
      enc_display_model_info_present_flag u(0)
      display_model_info_present_flag = enc_display_model_info_present_flag
    }
#endif
    display_model_info_present_flag u(1)

    num_operating_points_minus1 u(5)
    num_operating_points = num_operating_points_minus1 + 1
#if DECODE
    current_operating_point_id = desired_oppoint
    if (current_operating_point_id < 0 || current_operating_point_id > num_operating_points_minus1) {
      current_operating_point_id = 0
    }
#endif // DECODE
    for (i = 0; i < num_operating_points; i++) {
      spatial_layers_used[i] u(4)
      temporal_layers_used[i] u(8)

      operating_point_idc[i] = (spatial_layers_used[i] << 8) | temporal_layers_used[i]

      read_operating_point_level(i, 1)

      // TODO: Check that the operating points are "reasonable" in some sense
      read_decoder_model_oppoint_info(i)
      initial_display_delay[i] = 10 // If not signalled, this takes its maximum value
      display_model_param_present_flag[i] = 0
      if (display_model_info_present_flag) {
        display_model_param_present_flag[i] u(1)
        if (display_model_param_present_flag[i]) {
          initial_display_delay[i] u(4)
          initial_display_delay[i] += 1
        }
      }
      CHECK(initial_display_delay[i] <= 10,
            "initial_display_delay_minus_1[%d] = %d should be <= 9",
            i, initial_display_delay[i] - 1)
    }
  }

#if DECODE
  CHECK(current_operating_point_id < num_operating_points, "Selected operating point is not present in this stream")
  maxSpatialLayer = 0
  maxTemporalLayer = 0
  // Work out the number of spatial / temporal layers in the stream (not just
  // in the selected operating point), for output to the CSV file
  // In addition, until the --all-layers option is stabilized in the reference decoder,
  // calculate the highest-index spatial layer in the current operating point
  if (spatial_layers_used[current_operating_point_id] == 0 &&
      temporal_layers_used[current_operating_point_id] == 0) {
    // Scalability is not in use
    useScalability = 0
    maxSpatialLayer = 0
    maxTemporalLayer = 0
  } else {
    useScalability = 1
    for (i = 0; i < num_operating_points; i++) {
      CHECK(spatial_layers_used[i] != 0 && temporal_layers_used[i], "Invalid operating point flags")
      maxSpatialLayer = Max(maxSpatialLayer, get_msb(spatial_layers_used[i]))
      maxTemporalLayer = Max(maxTemporalLayer, get_msb(temporal_layers_used[i]))
    }
  }
#else // DECODE
  maxSpatialLayer = enc_spatial_layers - 1
  maxTemporalLayer = enc_temporal_layers - 1
#endif // DECODE

#if ENCODE
  frame_width_bits u(0)
  frame_height_bits u(0)
  max_frame_width u(0)
  max_frame_height u(0)
  ASSERT(frame_width_bits > 0 && frame_width_bits <= 16, "frame_width_bits out of range")
  ASSERT(frame_height_bits > 0 && frame_height_bits <= 16, "frame_height_bits out of range")
  codedFrameWidthBits = frame_width_bits - 1
  codedFrameHeightBits = frame_height_bits - 1
  codedMaxFrameWidth = max_frame_width - 1
  codedMaxFrameHeight = max_frame_height - 1
#endif // ENCODE

  codedFrameWidthBits u(4)
  codedFrameHeightBits u(4)
  frame_width_bits = codedFrameWidthBits + 1
  frame_height_bits = codedFrameHeightBits + 1
  codedMaxFrameWidth u(frame_width_bits)
  codedMaxFrameHeight u(frame_height_bits)
  max_frame_width = codedMaxFrameWidth + 1
  max_frame_height = codedMaxFrameHeight + 1

  // Each spatial layer has its own maximum width/height, which can
  // be set by a scalability metadata OBU later in the stream.
  // However, this data is optional, so in case it isn't present,
  // we default to allowing any size up to the maximum signalled in
  // the sequence header.
  for (i = 0; i <= maxSpatialLayer; i++) {
    spatial_layer_max_width[i] = max_frame_width
    spatial_layer_max_height[i] = max_frame_height
  }

  if (reduced_still_picture_hdr) {
    frame_id_numbers_present_flag = 0
  } else {
    frame_id_numbers_present_flag u(1)
  }

  if (frame_id_numbers_present_flag) {
#if ENCODE
    codedDeltaFrameIdLength = delta_frame_id_length - 2
    codedFrameIdLength = frame_id_length - (delta_frame_id_length + 1)
#endif
    codedDeltaFrameIdLength u(4)
    codedFrameIdLength u(3)
#if ENCODE
    ASSERT(delta_frame_id_length == codedDeltaFrameIdLength + 2, "delta_frame_id_length did not round-trip properly")
    ASSERT(frame_id_length == codedFrameIdLength + delta_frame_id_length + 1, "frame_id_length did not round-trip properly")
    additional_frame_id_length = codedFrameIdLength + 1
#else
    delta_frame_id_length = codedDeltaFrameIdLength + 2
    frame_id_length = codedFrameIdLength + delta_frame_id_length + 1
#endif
    CHECK(frame_id_length <= 16, "frame_id_length is too large")
  }

  read_sb_size()

  enable_filter_intra u(1)

  enable_intra_edge_filter u(1)

  if (reduced_still_picture_hdr) {
    enable_interintra_compound = 0
    enable_masked_compound = 0
    enable_warped_motion = 0

    enable_dual_filter = 0

    enable_order_hint = 0

    enable_jnt_comp = 0
    enable_ref_frame_mvs = 0

    seq_force_screen_content_tools = 2

    seq_force_integer_mv = 2

    order_hint_bits_minus1 = -1
  } else {
    enable_interintra_compound u(1)
    enable_masked_compound u(1)
    enable_warped_motion u(1)

    enable_dual_filter u(1)

    enable_order_hint u(1)

    if (enable_order_hint) {
      enable_jnt_comp u(1)
      enable_ref_frame_mvs u(1)
    } else {
      enable_jnt_comp = 0
      enable_ref_frame_mvs = 0
    }

    seq_choose_screen_content_tools_bit u(1)
    if (seq_choose_screen_content_tools_bit) {
      seq_force_screen_content_tools = 2
    } else {
      seq_force_screen_content_tools_bit u(1)
      seq_force_screen_content_tools = seq_force_screen_content_tools_bit
    }

    if (seq_force_screen_content_tools > 0) {
      seq_choose_integer_mv_bit u(1)
      if (seq_choose_integer_mv_bit) {
        seq_force_integer_mv = 2
      } else {
        seq_force_integer_mv_bit u(1)
        seq_force_integer_mv = seq_force_integer_mv_bit
      }
    } else {
      seq_force_integer_mv = 2
    }

    if (enable_order_hint) {
      order_hint_bits_minus1 u(3)
    } else {
      order_hint_bits_minus1 = -1
    }
  }

  enable_superres u(1)
  enable_cdef u(1)
  enable_loop_restoration u(1)

  read_bitdepth_colorspace_subsampling()
  film_grain_params_present u(1)
#if DECODE
  end_md5()
  if (!check_md5s() && !startOfFile) {
      CSV(CSV_NEWCVS,0)
      CSV(CSV_CVS_NUMBER,1)
  }
  startOfFile = 0
  CSV(CSV_BITDEPTH, bit_depth)
  CSV(CSV_MONOCHROME, monochrome)
  // Include the level of operating point 0 in the CSV file.
  // We split the value into major and minor components for convenience.
  // Note that the special "max parameters" level is interpreted as "level 9.3".
  csv_majorLevel = (operating_point_level[0] >> LEVEL_MINOR_BITS) + LEVEL_MAJOR_MIN
  csv_minorLevel = operating_point_level[0] & LEVEL_MINOR_MASK
  CSV(CSV_LEVEL, csv_majorLevel, csv_minorLevel)
  CSV(CSV_SCALABILITY, useScalability)
  CSV(CSV_LAYER_INFO, maxSpatialLayer + 1, maxTemporalLayer + 1, num_operating_points)
#endif
}

load_film_grain_params(bufIdx) {
  film_grain_params_present = ref_film_grain_params_present[bufIdx]
  apply_grain = ref_apply_grain[bufIdx]
  if (apply_grain) {
    random_seed = ref_random_seed[bufIdx]
    num_y_points = ref_num_y_points[bufIdx]
    for (j=0; j<num_y_points; j++) {
      scaling_points_y[j][0] = ref_scaling_points_y[bufIdx][j][0]
      scaling_points_y[j][1] = ref_scaling_points_y[bufIdx][j][1]
    }
    chroma_scaling_from_luma = ref_chroma_scaling_from_luma[bufIdx]
    num_cb_points = ref_num_cb_points[bufIdx]
    for (j=0; j<num_cb_points; j++) {
      scaling_points_cb[j][0] = ref_scaling_points_cb[bufIdx][j][0]
      scaling_points_cb[j][1] = ref_scaling_points_cb[bufIdx][j][1]
    }
    num_cr_points = ref_num_cr_points[bufIdx]
    for (j=0; j<num_cr_points; j++) {
      scaling_points_cr[j][0] = ref_scaling_points_cr[bufIdx][j][0]
      scaling_points_cr[j][1] = ref_scaling_points_cr[bufIdx][j][1]
    }
    scaling_shift = ref_scaling_shift[bufIdx]
    ar_coeff_lag = ref_ar_coeff_lag[bufIdx]

    // Work out how many coefficients to load for each plane
    baseNumPos = 2 * ar_coeff_lag * (ar_coeff_lag + 1)
    numPosLuma = (num_y_points) ? baseNumPos : 0
    numPosCb = (num_cb_points || chroma_scaling_from_luma) ? baseNumPos + (num_y_points > 0) : 0
    numPosCr = (num_cr_points || chroma_scaling_from_luma) ? baseNumPos + (num_y_points > 0) : 0

    for (j = 0; j<numPosLuma; j++) {
      ar_coeffs_y[j] = ref_ar_coeffs_y[bufIdx][j]
    }
    for (j = 0; j<numPosCb; j++) {
      ar_coeffs_cb[j] = ref_ar_coeffs_cb[bufIdx][j]
    }
    for (j = 0; j<numPosCr; j++) {
      ar_coeffs_cr[j] = ref_ar_coeffs_cr[bufIdx][j]
    }
    ar_coeff_shift = ref_ar_coeff_shift[bufIdx]
    grain_scale_shift = ref_grain_scale_shift[bufIdx]
    if (num_cb_points) {
      cb_mult = ref_cb_mult[bufIdx]
      cb_luma_mult = ref_cb_luma_mult[bufIdx]
      cb_offset = ref_cb_offset[bufIdx]
    }
    if (num_cr_points) {
      cr_mult = ref_cr_mult[bufIdx]
      cr_luma_mult = ref_cr_luma_mult[bufIdx]
      cr_offset = ref_cr_offset[bufIdx]
    }
    overlap_flag = ref_overlap_flag[bufIdx]
    clip_to_restricted_range = ref_clip_to_restricted_range[bufIdx]
  }
}

read_film_grain() {
  if (! (film_grain_params_present && (show_frame || showable_frame))) {
    apply_grain = 0
    return 0
  }

  apply_grain u(1)
  if (! apply_grain) {
    return 0
  }

  random_seed u(16)

# if ENCODE
  // This chooses enc_update_params and enc_film_grain_params_ref_idx.
  enc_choose_film_grain_params()
# endif // ENCODE

  if (frame_type == INTER_FRAME) {
    update_params u(1)
  } else {
    update_params = 1
  }
  update_grain = update_params

  if (!update_params) {
    film_grain_params_ref_idx u(3)
    bufIdx = film_grain_params_ref_idx
    ASSERT(ref_film_grain_params_present[bufIdx],
           "Copy requested for frame without reference parameters")
    found = 0
    for (i=0; i<INTER_REFS_PER_FRAME; i++) {
      if (active_ref_idx[i] == bufIdx) {
        found = 1
      }
    }
    ASSERT(found, "Copy requested from non active frame")
    // copy all film grain params from other from except the random seed
    rs = random_seed
    load_film_grain_params(bufIdx)
    random_seed = rs
  } else {
    // Note: In our coverage report, there is a cross coverage point
    // which checks that we cover all of the combinations of some
    // points being zero and some points being nonzero.
    //
    // We intentionally bias the distribution of values here to try to
    // give relatively uniform coverage of the available cases.

    num_y_points u(4)
    ASSERT(num_y_points <= 14, "Invalid num_y_points: %d.", num_y_points)
    for (i = 0; i < num_y_points; i++) {
      scaling_points_y0 u(8)
      scaling_points_y[i][0] = scaling_points_y0
      if (i) {
        ASSERT (scaling_points_y[i - 1][0] < scaling_points_y[i][0],
                "At i = %d, film grain point_y_value not increasing (%d >= %d).",
                i, scaling_points_y[i - 1][0], scaling_points_y[i][0])
      }

      scaling_points_y1 u(8)
      scaling_points_y[i][1] = scaling_points_y1
    }

    chroma_scaling_from_luma = 0
    if (!monochrome) {
      chroma_scaling_from_luma u(1)
    }

    if (monochrome || chroma_scaling_from_luma ||
        (subsampling_x && subsampling_y && (num_y_points == 0))) {
      num_cb_points = 0
      num_cr_points = 0
    } else {
      num_cb_points u(4)
      ASSERT (num_cb_points <= 10,
              "Invalid num_cb_points (%d not in [0, 10])",
              num_cb_points)
      for (i = 0; i < num_cb_points; i++) {
        scaling_points_cb0 u(8)
        scaling_points_cb[i][0] = scaling_points_cb0
        if (i) {
          ASSERT (scaling_points_cb[i - 1][0] < scaling_points_cb[i][0],
                  "At i = %d, film grain point_cb_value not increasing (%d >= %d).",
                  i, scaling_points_cb[i - 1][0], scaling_points_cb[i][0])
        }

        scaling_points_cb1 u(8)
        scaling_points_cb[i][1] = scaling_points_cb1
      }

      num_cr_points u(4)
      ASSERT(num_cr_points <= 10, "Invalid num_cr_points")
      for (i = 0; i < num_cr_points; i++) {
        scaling_points_cr0 u(8)
        scaling_points_cr[i][0] = scaling_points_cr0
        if (i) {
          ASSERT (scaling_points_cr[i - 1][0] < scaling_points_cr[i][0],
                  "At i = %d, film grain point_cr_value not increasing (%d >= %d).",
                  i, scaling_points_cr[i - 1][0], scaling_points_cr[i][0])
        }

        scaling_points_cr1 u(8)
        scaling_points_cr[i][1] = scaling_points_cr1
      }

      if (subsampling_x && subsampling_y) {
        if (num_cb_points) {
          ASSERT (num_cr_points,
                  "With 4:2:0 subsampling, num_cb_points = %d (nonzero) but num_cr_points = 0.",
                  num_cb_points)
        } else {
          ASSERT (! num_cr_points,
                  "With 4:2:0 subsampling, num_cr_points = %d (nonzero) but num_cb_points = 0.",
                  num_cr_points)
        }
      }
    }

    scaling_shift u(2)
    grain_scaling_minus_8 = scaling_shift
    scaling_shift += 8

    ar_coeff_lag u(2)

    // Work out how many coefficients to read for each plane
    baseNumPos = 2 * ar_coeff_lag * (ar_coeff_lag + 1)
    numPosLuma = (num_y_points) ? baseNumPos : 0
    numPosCb = (num_cb_points || chroma_scaling_from_luma) ? baseNumPos + (num_y_points > 0) : 0
    numPosCr = (num_cr_points || chroma_scaling_from_luma) ? baseNumPos + (num_y_points > 0) : 0

#if ENCODE
    // Special values used by choose_film_grain() to bias
    // toward interesting parameter sets
    enc_film_grain_signs u(0)
#endif

    for (i = 0; i < numPosLuma; i++) {
      ar_coeff_y u(8)
      ar_coeffs_y[i] = ar_coeff_y - 128
    }

    for (i = 0; i < numPosCb; i++) {
      ar_coeff_u u(8)
      ar_coeffs_cb[i] = ar_coeff_u - 128
    }

    for (i = 0; i < numPosCr; i++) {
      ar_coeff_v u(8)
      ar_coeffs_cr[i] = ar_coeff_v - 128
    }

    ar_coeff_shift u(2)
    ar_coeff_shift += 6

    grain_scale_shift u(2)

    if (num_cb_points) {
      cb_mult u(8)
      cb_luma_mult u(8)
      cb_offset u(9)
    }

    if (num_cr_points) {
      cr_mult u(8)
      cr_luma_mult u(8)
      cr_offset u(9)
    }

    overlap_flag u(1)

    clip_to_restricted_range u(1)
  }
}


uint1 frame_might_use_prev_frame_mvs() {
  return (enable_order_hint
          && enable_ref_frame_mvs
          && !error_resilient_mode
          && !intra_only)
}

uint1 frame_can_use_prev_frame_mvs() {
  fr = active_ref_idx[0]

  return (frame_might_use_prev_frame_mvs()
          && !ref_intra_only[fr]
          && crop_width==ref_crop_width[fr] && crop_height==ref_crop_height[fr])
}


setup_cdef() {
  cdef_damping u(2)
  cdef_damping += 3
  cdef_bits u(2)
  for (i = 0; i < (1 << cdef_bits); i++) {
    cdef_y_pri_strength[i] u(4)
    cdef_y_sec_strength[i] u(2)
    if (cdef_y_sec_strength[i] == 3)
      cdef_y_sec_strength[i] += 1

    if (get_num_planes() > 1) {
      cdef_uv_pri_strength[i] u(4)
      cdef_uv_sec_strength[i] u(2)
      if (cdef_uv_sec_strength[i] == 3) {
        cdef_uv_sec_strength[i] += 1
      }
    } else {
      cdef_uv_pri_strength[i] = 0
      cdef_uv_sec_strength[i] = 0
    }
  }

  // Detect when we can skip CDEF entirely
  // This is not normatively required, but is needed so that our sequence of calls
  // to log_cdef() match those in the reference code
  skip_cdef = (cdef_bits == 0 && cdef_y_pri_strength[0] == 0 && cdef_y_sec_strength[0] == 0 &&
               cdef_uv_pri_strength[0] == 0 && cdef_uv_sec_strength[0] == 0)
}

read_sb_size() {
  use_128x128_superblock u(1)
  superblock_size = use_128x128_superblock ? BLOCK_128X128 : BLOCK_64X64
  mib_size = mi_size_wide[superblock_size]
  mib_size_log2 = mi_width_log2_lookup[superblock_size]
  mi_mask = mib_size - 1
  mi_mask2 = (mib_size * 2) - 1
}

// Update which references are valid once we've selected a frame ID.
update_references() {
  delta_frame_id = wrap_int(current_frame_id - prev_frame_id, frame_id_length)
  CHECK (delta_frame_id > 0,
         "Invalid value of current_frame_id. prev_id=%d; cur_id=%d; delta=%d (not positive).",
         prev_frame_id, current_frame_id, delta_frame_id)

  for (i = 0; i < NUM_REF_FRAMES; i++) {
    // Reference slots can be marked invalid in the following ways:
    // * The reference's frame ID differs too much from the current frame ID
    //   (taking into account the wrap-around behaviour of the frame IDs)
    // * Or, the reference's frame offset differs to much from the current frame number.
    //   Note: this is currently too strict - we only need this for non-shown, inter frames
    //   (as they explicitly signal the frame offset), plus maybe we can get away with something
    //   a bit less strict in other ways?
    delta = wrap_int(ref_frame_id[i] - current_frame_id, frame_id_length)
    deltaValid = ((delta < 0) && (delta >= -(1 << delta_frame_id_length)))
    if (ref_valid[i] && !deltaValid) {
      ref_valid[i] = 0
    }
  }
}

// Initialize the 'ref_valid_this_frame' array to indicate which references we're
// allowed to use for this frame
get_valid_ref_slots() {
  for (i = 0; i < NUM_REF_FRAMES; i++) {
    if (frame_number > 0) {
      ref_valid_this_frame[i] = ref_valid[i] &&
                                (ref_spatial_layer[i] <= spatial_layer) &&
                                (ref_temporal_layer[i] <= temporal_layer)
    } else {
      ref_valid_this_frame[i] = 0
    }
  }
}

read_profile() {
  profile u(3)
  CHECK(profile <= 2, "AV1 only has profiles 0,1,2, but signalled profile is >2")
}

load_previous() {
  prev_frame_slot = active_ref_idx[primary_ref_frame]
  save_mode_info(1,2+prev_frame_slot)
  setup_motion_field()
}

maybe_set_ref(dst_ref, slot) {
  if (slot != -1) {
    ASSERT(usedFrame[slot] == 0, "BUG in ref frame search")
    short_ref_idx[dst_ref - LAST_FRAME] = slot
    usedFrame[slot] = 1
  }
}

find_latest_backward(dst_ref, curOffset) {
  slot = -1
  latestOrderHint = 0
  for (i = 0; i < NUM_REF_FRAMES; i++) {
    hint = shifted_order_hints[i]
    if (!usedFrame[i] &&
        hint >= curOffset &&
        (slot < 0 || hint >= latestOrderHint)) {
      slot = i
      latestOrderHint = hint
    }
  }
  maybe_set_ref(dst_ref, slot)
}

find_earliest_backward(dst_ref, curOffset) {
  slot = -1
  earliestOrderHint = 0
  for (i = 0; i < NUM_REF_FRAMES; i++) {
    hint = shifted_order_hints[i]
    if (!usedFrame[i] &&
        hint >= curOffset &&
        (slot < 0 || hint < earliestOrderHint)) {
      slot = i
      earliestOrderHint = hint
    }
  }
  maybe_set_ref(dst_ref, slot)
}

find_latest_forward(dst_ref, curOffset) {
  slot = -1
  latestOrderHint = 0
  for (i = 0; i < NUM_REF_FRAMES; i++) {
    hint = shifted_order_hints[i]
    if (!usedFrame[i] &&
        hint < curOffset &&
        (slot < 0 || hint >= latestOrderHint)) {
      slot = i
      latestOrderHint = hint
    }
  }
  maybe_set_ref(dst_ref, slot)
}

// Fallback option: Find the earliest reference frame available,
// regardless of whether it's already in use, and regardless of which side
// of the current frame it lies on
find_earliest_ref() {
  slot = -1
  earliestOrderHint = 0
  for (i = 0; i < NUM_REF_FRAMES; i++) {
    hint = shifted_order_hints[i]
    if (slot < 0 || hint < earliestOrderHint) {
      slot = i
      earliestOrderHint = hint
    }
  }
  return slot
}

setup_short_frame_refs(lstIdx, gldIdx, schedCurrentFrame) {
#if DECODE
  ASSERT(schedCurrentFrame == -1, "schedCurrentFrame should be -1 outside the av1src encoder")
#endif

  // Adjust the order hints into a range centered around the current frame.
  // This is done so that earlier (in logical order) frames have a lower shifted_order_hint
  // than later frames - which may not be true of the values in saved_ref_decode_order.
#if ENCODE
  if (schedCurrentFrame != -1) {
    curOffset = sched_order_hint[schedCurrentFrame]
    for (i = 0; i < NUM_REF_FRAMES; i++) {
      refFrame = sched_frame_in_ref_slot[schedCurrentFrame-1][i]
      refOffset = sched_order_hint[refFrame]
      shifted_order_hints[i] = curOffset + get_relative_dist(refOffset, curOffset)
    }
  } else {
#endif
    curOffset = cur_frame_offset
    for (i = 0; i < NUM_REF_FRAMES; i++) {
      refOffset = saved_ref_decode_order[i][0]
      shifted_order_hints[i] = curOffset + get_relative_dist(refOffset, curOffset)
    }
#if ENCODE
  }
#endif

  // Check that LAST_FRAME and GOLDEN_FRAME are both "forward" (earlier in time) references
  // TODO: Make sure the CHECK conditions are equivalent to the ones in the ref code
  lstOffset = shifted_order_hints[lstIdx]
  gldOffset = shifted_order_hints[gldIdx]
  CHECK(curOffset > lstOffset, "Can't have LAST_FRAME as a lookahead ref when using short signalling")
  CHECK(curOffset > gldOffset, "Can't have GOLDEN_FRAME as a lookahead ref when using short signalling")

  for (i = 0; i < INTER_REFS_PER_FRAME; i++) {
    short_ref_idx[i] = -1 // Placeholder to mark which refs we haven't found yet
  }
  for (i = 0; i < NUM_REF_FRAMES; i++) {
    usedFrame[i] = 0
  }

  // The LAST and GOLDEN frames are signalled explicitly.
  // These are allowed to be the same, so don't use maybe_set_ref() directly
  short_ref_idx[LAST_FRAME - LAST_FRAME] = lstIdx
  usedFrame[lstIdx] = 1
  short_ref_idx[GOLDEN_FRAME - LAST_FRAME] = gldIdx
  usedFrame[gldIdx] = 1

  // Search for backward ref frames to use
  find_latest_backward(ALTREF_FRAME, curOffset)
  find_earliest_backward(BWDREF_FRAME, curOffset)
  find_earliest_backward(ALTREF2_FRAME, curOffset)

  // Search for the remaining forward refs
  for (ref = LAST2_FRAME; ref < GOLDEN_FRAME; ref++) {
    find_latest_forward(ref, curOffset)
  }

  // If we couldn't find backward ref candidates, fill them in
  // with more forward refs
  for (ref = BWDREF_FRAME; ref <= ALTREF_FRAME; ref++) {
    if (short_ref_idx[ref - LAST_FRAME] < 0)
      find_latest_forward(ref, curOffset)
  }

  // As a last resort, in extreme cases, fill any remaining refs
  // with the "earliest" reference available
  // I think this can only happen if we run out of "forward" refs -
  // the block just above handles the case where we run out of "backward" refs
  slot = find_earliest_ref()
  for (ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++) {
    if (short_ref_idx[ref - LAST_FRAME] < 0) {
      short_ref_idx[ref - LAST_FRAME] = slot
    }
  }
}

// Initialize all values related to lossless mode
setup_lossless() {
  coded_lossless = 1
  for (segmentId = 0; segmentId < MAX_SEGMENTS; segmentId++) {
    qindex = get_qindex(1, segmentId)
    lossless_array[segmentId] = qindex == 0
    for (plane = 0; plane < get_num_planes(); plane++) {
      lossless_array[segmentId] = lossless_array[segmentId] &&
                                  dc_delta_qindex[plane]==0 && ac_delta_qindex[plane]==0
    }
    coded_lossless = coded_lossless && lossless_array[segmentId]
  }
  all_lossless = coded_lossless && (upscaled_width == crop_width)

#if ENCODE
  ASSERT(IMPLIES(enc_lossless, coded_lossless), "Decided to code a lossless frame, but coded_lossless = 0")
#endif
}

aloc_buffers() {
  if (max_frame_width > aloc_frame_width || max_frame_height > aloc_frame_height) {
    // This must be the start of a new CVS, so we can safely
    // free and reallocate all of our arrays
    paddedW = ALIGN_POWER_OF_TWO(max_frame_width, MAX_SB_SIZE_LOG2)
    paddedH = ALIGN_POWER_OF_TWO(max_frame_height, MAX_SB_SIZE_LOG2)
    paddedArea = paddedW * paddedH

    miW = paddedW >> MI_SIZE_LOG2
    miH = paddedH >> MI_SIZE_LOG2
    miArea = paddedArea >> (2*MI_SIZE_LOG2)

    // Frame buffers
    alloc rframestore[8][3][max_frame_width * max_frame_height] // Note: This array does *not* require padding
    alloc rframe[3][paddedW * paddedH]
    alloc rcdef_frame[3][paddedW * paddedH]
    alloc rlr_frame[3][paddedW * paddedH]
    alloc rupscaled_frame[3][paddedW * paddedH]
    alloc rupscaled_cdef_frame[3][paddedW * paddedH]

    // Other arrays which depend on the frame area
    alloc rmvs[2+NUM_REF_FRAMES][miArea][2][4][2]
    alloc rcdef_idx[paddedArea >> (2 * MI_SIZE_64X64_LOG2)]
    alloc seg_maps[2+NUM_REF_FRAMES][miArea]
    alloc rpredmvs[2+NUM_REF_FRAMES][miArea][2][4][2]
#if STACKED_MFMV
    alloc rmotion_field_mvs_stacked[INTER_REFS_PER_FRAME][miArea>>(3-MI_SIZE_LOG2)][MFMV_STACK_SIZE][2]
#endif
    alloc rmotion_field_mvs[INTER_REFS_PER_FRAME][miArea>>(3-MI_SIZE_LOG2)][2]
    alloc rrefframes[2+NUM_REF_FRAMES][miArea][2]
    alloc rsegment_ids[miArea]
    alloc ry_modes[miArea]
    alloc ruv_modes[miArea]
    alloc rsb_sizes[miArea]
    alloc rinterp_filters[miArea][MB_FILTER_COUNT]
    alloc rtx_sizes[miArea]
    alloc rskip_coeffs[miArea]
    alloc rinter_tx_size[miArea]
    alloc rpalettesizes[2][miArea]
    alloc rpalettecolors[2][miArea][PALETTE_MAX_SIZE]
    alloc rangledeltasy[miArea]
    alloc rintrafilters[miArea]
    alloc rlf_lvls[miArea][FRAME_LF_COUNT]
    alloc ris_inters[miArea]
    alloc block_decoded_map_data[3][miArea*TXFM_PER_MI_COL*TXFM_PER_MI_ROW]
    alloc rcurrent_deltas_lf_from_base[miArea]
    alloc rcurrent_deltas_lf_multi[miArea][FRAME_LF_COUNT]
    alloc noise_stripe[(paddedH+33)/32][3][34][paddedW+34]
    alloc noise_image[3][paddedW][paddedH]
#if ENCODE
    if (enc_use_frame_planner) {
      alloc renc_sb_sizes[miArea]
      alloc enc_partition_symbols[miArea / 3] // See comment in av1_extra.c for why there is a / 3 here
      alloc renc_block_partition[miArea]
      alloc renc_segment_id[miArea]
      alloc renc_is_inter[miArea]
      alloc renc_skip_mode[miArea]
      alloc renc_skip_coeff[miArea]
    }
    alloc renc_ref_frame[miArea][2]
#endif

    if (num_large_scale_tile_anchor_frames > 0) {
      // This is a large-scale-tile stream, so allocate
      // the anchor frames
      CHECK(aloc_frame_width == 0 && aloc_frame_height == 0,
            "Large-scale-tile frames should only consist of a single CVS")
      alloc ranchor_framestore[num_large_scale_tile_anchor_frames+1][3][max_frame_width * max_frame_height]
    }

    aloc_frame_width = max_frame_width
    aloc_frame_height = max_frame_height
  }
}

read_uncompressed_header() {
  // Allocate frame buffers, if we haven't already.
  // We do this at the start of the first frame header to avoid
  // complexity due to the fact that:
  // i) whether we're in a large-scale-tile stream, and
  // ii) how many anchor frames we have
  // can be specified either on the command line *or* in a metadata OBU.
  // In either case, we know the answers to those two questions by this point.
  aloc_buffers()

#if DECODE && COLLECT_STATS
  // Reset tile size counters
  largest_tile_size_in_frame = 0
  smallest_tile_size_in_frame = convert_to_int64(1) << 32
#endif

  is_reference_frame = 1 // Default value
  for (i=0;i<ALLOWED_REFS_PER_FRAME;i++) {
    active_ref_idx[i] = -1
  }

#if ENCODE
  per_frame_0 u(0)
  per_frame_1 u(0)
  per_frame_2 u(0)
  per_frame_3 u(0)
  per_frame_4 u(0)
  per_frame_5 u(0)
  per_frame_6 u(0)
  per_frame_7 u(0)

  // For bit-stream generator only
  if (reduced_still_picture_hdr) {
    FrameIsIntra = 1
  }
#endif // ENCODE
  if (reduced_still_picture_hdr) {
    show_existing_frame = 0
    FrameIsIntra = 1
  } else {
    show_existing_frame u(1)
  }
  if (show_existing_frame) {
    get_valid_ref_slots()
    CSV(CSV_SHOWEXISTINGCOUNT,1)
    index_of_existing_frame u(3)
    CHECK(ref_frame_number[index_of_existing_frame] != -1, "Can't show a not-yet-decoded frame")

    // Check the rule that any frame displayed this way must have showable_frame == 1
    CHECK(ref_showable_frame[index_of_existing_frame], "Can't use show_existing_frame for a non-showable keyframe")

    // Enforce the rule that keyframes can only be shown once.
    // We do this by resetting ref_showable_frame[] to 0 for all ref slots
    // containing the frame being shown, but *only* if it is a keyframe.
    frame_type = ref_type[index_of_existing_frame]
    if (frame_type == KEY_FRAME) {
      for (i = 0; i < NUM_REF_FRAMES; i++) {
        if (ref_frame_number[i] == ref_frame_number[index_of_existing_frame]) {
          ref_showable_frame[i] = 0
        }
      }
    }

    if (decoder_model_info_present_flag && equal_picture_interval == 0) {
      read_tu_pts_info()
    }
    show_frame = 1
    refresh_frame_flags = 0
    for (i = 0; i < FRAME_LF_COUNT; i++)
      filter_level[i] = 0
    load_film_grain_params(index_of_existing_frame)
    CHECK(ref_valid_this_frame[index_of_existing_frame] != 0, "Invalid frame for display")
    CHECK(profile == ref_profile[index_of_existing_frame],"Profile does not match profile of existing frame")
    CHECK(bit_depth == ref_bit_depth[index_of_existing_frame],"Bit depth does not match bit depth of existing frame")
    CHECK(subsampling_x == ref_subsampling_x[index_of_existing_frame],"subsampling_x does not match subsampling_x of existing frame")
    CHECK(subsampling_y == ref_subsampling_y[index_of_existing_frame],"subsampling_y does not match subsampling_y of existing frame")

    if (frame_id_numbers_present_flag) {
#if ENCODE
      display_frame_id = ref_frame_id[index_of_existing_frame]
#endif
      display_frame_id u(frame_id_length)
      CHECK(display_frame_id == ref_frame_id[index_of_existing_frame], "Reference buffer frame ID mismatch")
    }

    // Reload the frame buffer as if we had just decoded this frame
    upscaled_width = ref_width[index_of_existing_frame]
    upscaled_height = ref_height[index_of_existing_frame]
    stride = upscaled_width
    for (plane = 0; plane < get_num_planes(); plane++) {
      planeW = ROUND_POWER_OF_TWO(upscaled_width, plane_subsampling_x[plane])
      planeH = ROUND_POWER_OF_TWO(upscaled_height, plane_subsampling_y[plane])
      for (y = 0; y < planeH; y++)
        for (x = 0; x < planeW; x++) {
          frame[plane][x][y] = framestore[index_of_existing_frame][plane][x][y]
        }
    }

    if (frame_type == KEY_FRAME) {
      refresh_frame_flags = (1 << NUM_REF_FRAMES) - 1

      // If showing a forward keyframe, we need to reload some further values
      // from that frame
      mi_rows = ref_mi_rows[0]
      mi_cols = ref_mi_cols[0]
      intra_only = 1
      showable_frame = 0 // This frame will be stored back into the ref buffers, so make sure it can't be shown again
      crop_width = ref_crop_width[index_of_existing_frame]
      crop_height = ref_crop_height[index_of_existing_frame]
      display_width = ref_display_width[index_of_existing_frame]
      display_height = ref_display_height[index_of_existing_frame]
      for (i = 0; i < 1+INTER_REFS_PER_FRAME; i++) {
        ref_decode_order[i] = saved_ref_decode_order[index_of_existing_frame][i]
      }
      current_frame_id = display_frame_id

      save_mode_info(0, 2+index_of_existing_frame)
      load_cdfs(index_of_existing_frame)
    }

    return 0
  }
  if (reduced_still_picture_hdr) {
    frame_type = KEY_FRAME
    show_frame = 1
    showable_frame = 0
    intra_only = 0 // Until this gets set to 1 later
    error_resilient_mode = 1
  } else {
    frame_type u(2)

#if ENCODE
    // For bit-stream generator only
    FrameIsIntra = (frame_type == INTRA_ONLY_FRAME || frame_type == KEY_FRAME)
#endif // ENCODE

    show_frame u(1)
    if (still_picture) {
      CHECK(frame_type == KEY_FRAME && show_frame == 1,
            "Still pictures must be coded as shown keyframes")
    }
    if (show_frame) {
      if (decoder_model_info_present_flag && equal_picture_interval == 0) {
        read_tu_pts_info()
      }
    }
    if (show_frame) {
      showable_frame = (frame_type != KEY_FRAME)
    } else {
      showable_frame u(1)
    }
    intra_only = (frame_type == INTRA_ONLY_FRAME)
    if (frame_type == S_FRAME || (frame_type == KEY_FRAME && show_frame)) {
      error_resilient_mode = 1
    } else {
      error_resilient_mode u(1)
    }
  }

  disable_cdf_update u(1)

  if (seq_force_screen_content_tools == 2) {
    allow_screen_content_tools u(1)
  } else {
    allow_screen_content_tools = seq_force_screen_content_tools
  }

  if (allow_screen_content_tools) {
    if (seq_force_integer_mv == 2) {
      cur_frame_force_integer_mv u(1)
    } else {
      cur_frame_force_integer_mv = seq_force_integer_mv
    }
  } else {
    cur_frame_force_integer_mv = 0
  }

  if (reduced_still_picture_hdr) {
    primary_ref_frame = PRIMARY_REF_NONE
    frame_size_override_flag = 0
  } else {
    if (frame_id_numbers_present_flag) {
      if (frame_number > 0 && !(frame_type == KEY_FRAME && show_frame)) {
        current_frame_id u(frame_id_length)
        update_references()
      } else {
        // Frame 0, or any shown keyframe, has no previous frame ID to compare to.
        // Thus any frame ID is valid.
        current_frame_id u(frame_id_length)
      }
    }
    if (frame_type == S_FRAME) {
      frame_size_override_flag = 1
    } else {
      frame_size_override_flag u(1)
    }

    if (enable_order_hint) {
      cur_frame_offset u(order_hint_bits_minus1+1)
      current_video_frame = cur_frame_offset
    }
    setup_frame_buf_refs()

    if (! (intra_only || frame_type == KEY_FRAME || error_resilient_mode)) {
      primary_ref_frame u(PRIMARY_REF_BITS)
    } else {
      primary_ref_frame = PRIMARY_REF_NONE
    }
  }

  if (decoder_model_info_present_flag) {
    buffer_removal_delay_present u(1)
    if (decoder_model_mode == DECODER_MODEL_SCHEDULE) {
      ASSERT(buffer_removal_delay_present, "DECODER_MODEL_SCHEDULE requires buffer removal times to be specified")
    }
    if (buffer_removal_delay_present) {
      for (opNum = 0; opNum < num_operating_points; opNum++) {
        if (decoder_model_param_present_flag[opNum]) {
#if ENCODE
          // For bit-stream generator
          opPtIdc = operating_point_idc[opNum]
          inTemporalLayer = (opPtIdc >> temporal_layer) & 1
          inSpatialLayer = (opPtIdc >> (spatial_layer + 8)) & 1
#endif
          if (is_frame_in_operating_point(opNum)) {
#if ENCODE
            enc_buffer_removal_delay_offset[opNum] = current_bitposition() - frameHeaderStart
            bufferRemovalDelay u(buffer_removal_delay_length)
            // The correct value will be filled in once the frame is fully encoded
#else
            bufferRemovalDelay u(buffer_removal_delay_length)
            if (opNum == current_operating_point_id) {
              dfgIdx = current_dfg[opNum]
              int64 prevDelay
              if (dfgIdx == 0) {
                prevDelay = 0
              } else {
                prevDelay = buffer_removal_delay[opNum][dfgIdx - 1]
              }
              buffer_removal_delay[opNum][dfgIdx] = unwrap_counter(prevDelay, bufferRemovalDelay, buffer_removal_delay_length)
            }
#endif
          }
        }
      }
    }
  }

  if (frame_type == KEY_FRAME) {
    if (show_frame) {
      // "Immediate" keyframe (not an official term) -
      // need to update all reference frames
      refresh_frame_flags = (1<<NUM_REF_FRAMES)-1
    } else {
      // "Forward-coded" keyframe - to be used as a backward reference
      // for the next group of frames, so we only want to update some
      // reference slots
      refresh_frame_flags u(NUM_REF_FRAMES)
    }
    intra_only = 1
    CSV(CSV_KEYFRAMECOUNT,1)
  } else {
    if (intra_only) {
      refresh_frame_flags u(NUM_REF_FRAMES)
      CHECK(refresh_frame_flags != 0xFF, "Intra-only frames can't have refresh_frame_flags = 0xFF")
    } else {
      if (frame_type == S_FRAME)
        refresh_frame_flags = (1 << NUM_REF_FRAMES) - 1 // Refresh all ref frame slots
      else
        refresh_frame_flags u(NUM_REF_FRAMES)
    }
  }
  // Read all ref frame order hints if error_resilient_mode == 1
  if (error_resilient_mode && enable_order_hint &&
      (!intra_only || refresh_frame_flags != 0xFF)) {
    for (refIdx = 0; refIdx < NUM_REF_FRAMES; refIdx++) {
#if ENCODE
      frame_offset = saved_ref_decode_order[refIdx][0]
#endif // ENCODE
      frame_offset u(order_hint_bits_minus1 + 1)
      if (frame_offset != saved_ref_decode_order[refIdx][0]) {
        // This indicates one of two things:
        // i) The frame which should be in that ref slot was dropped.
        //    We don't implement dropped-frame recovery, so ignore that case.
        // ii) The frame which is in slot 'refIdx' at this point *depends on
        //     which operating point is being decoded*, and the encoder has sent
        //     the order hint for a different operating point to the one we're using.
        //     If this happens, then that slot should only be referenced by the
        //     encoder's chosen operating point; to help check this, we mark the
        //     slot as invalid for the current operating point
        ref_valid[refIdx] = 0
      }
    }
  }
  if (intra_only || error_resilient_mode) {
    can_use_previous = 0
  }

  get_valid_ref_slots()

#if ENCODE
  enc_allowed_motion_modes u(0)
#endif

  if (frame_type == KEY_FRAME) {
    read_frame_size()
    read_superres()
    compute_image_size()
    read_display_size()
    can_use_ref = 0
    can_use_previous = 0
    allow_high_precision_mv = 0
    if (allow_screen_content_tools && upscaled_width == crop_width) {
      allow_intrabc u(1)
    } else {
      allow_intrabc = 0
    }
    can_use_previous = 0
  } else {
    if (intra_only) {
      CSV(CSV_INTRAONLYCOUNT,1)
      read_frame_size()
      read_superres()
      compute_image_size()
      read_display_size()
      can_use_ref = 0
      can_use_previous = 0
      allow_high_precision_mv = 0
      if (allow_screen_content_tools && upscaled_width == crop_width) {
        allow_intrabc u(1)
      } else {
        allow_intrabc = 0
      }
    } else {
      CHECK(profile==dec_profile,"Profile has changed at Inter frame")
      CHECK(bit_depth==dec_bit_depth,"Bit depth has changed at Inter frame")
      CSV(CSV_INTERCOUNT,1)
      if (!refresh_frame_flags)
        is_reference_frame = 0

      frame_refs_short_signaling = 0
      if (enable_order_hint) {
        frame_refs_short_signaling u(1)
      }

      if (frame_refs_short_signaling) {
        // Just read LAST_FRAME and GOLDEN_FRAME, and generate all other
        // ref idxs from there
        lstIdx u(NUM_REF_FRAMES_LOG2)
        gldIdx u(NUM_REF_FRAMES_LOG2)
        setup_short_frame_refs(lstIdx, gldIdx, -1)
      }

      for (i=0;i<ALLOWED_REFS_PER_FRAME;i++) {
        if (frame_refs_short_signaling)
          active_ref_idx[i] = short_ref_idx[i]
        else
          active_ref_idx[i] u(NUM_REF_FRAMES_LOG2)
        if (frame_type == S_FRAME) {
          ref_frame_sign_bias[LAST_FRAME+i] = 0
        } else {
          // ref_frame_sign_bias is set up later, after we've read
          // the decode offset for this frame
          :pass
        }
        CHECK(ref_frame_number[active_ref_idx[i]] != -1, "Can't reference a not-yet-decoded frame")
        CHECK(ref_valid_this_frame[active_ref_idx[i]] != 0, "Invalid reference frame")
        CHECK(profile==ref_profile[active_ref_idx[i]],"Predicting from reference frame with mismatch in profile")
        CHECK(bit_depth==ref_bit_depth[active_ref_idx[i]],"Predicting from reference frame with mismatch in bit depth")
        CHECK(subsampling_x==ref_subsampling_x[active_ref_idx[i]],"Predicting from reference frame with mismatch in subsampling_x")
        CHECK(subsampling_y==ref_subsampling_y[active_ref_idx[i]],"Predicting from reference frame with mismatch in subsampling_y")
        CHECK(color_primaries==ref_color_primaries[active_ref_idx[i]],"Predicting from reference frame with mismatch in color_primaries")
        CHECK(transfer_characteristics==ref_transfer_characteristics[active_ref_idx[i]],"Predicting from reference frame with mismatch in transfer_characteristics")
        CHECK(matrix_coefficients==ref_matrix_coefficients[active_ref_idx[i]],"Predicting from reference frame with mismatch in matrix_coefficients")
        if (subsampling_x && subsampling_y)
          CHECK(chroma_sample_position==ref_chroma_sample_position[active_ref_idx[i]],"Predicting from reference frame with mismatch in chroma_sample_position")
        if (frame_id_numbers_present_flag) {
# if ENCODE
          delta_frame_id_minus1 = wrap_uint(current_frame_id - ref_frame_id[active_ref_idx[i]], frame_id_length) - 1
# endif // ENCODE

          delta_frame_id_minus1 u(delta_frame_id_length)
          DeltaFrameId = delta_frame_id_minus1 + 1
#if ENCODE
          // For bit-stream generator
          expectedFrameId[i] = ((current_frame_id + (1 << frame_id_length)
                                 - DeltaFrameId) % (1 << frame_id_length))
#endif // ENCODE

          uint22 ref_frame_id_calc
          ref_frame_id_calc = wrap_uint(current_frame_id - (delta_frame_id_minus1 + 1), frame_id_length)
          CHECK(ref_frame_id_calc == ref_frame_id[active_ref_idx[i]], "Reference buffer frame ID mismatch")
        }
      }
#if ENCODE
      // Check that the references were set up as expected
      for (i = 0; i < INTER_REFS_PER_FRAME; i++) {
        ASSERT(active_ref_idx[i] == sched_active_ref_idx[frame_number][i],
               "active_ref_idx[] does not match its expected values")
      }
#endif
      if (frame_size_override_flag && !error_resilient_mode) {
        frame_size_with_refs()
      } else {
        read_frame_size()
        read_superres()
        compute_image_size()
        read_display_size()
      }
      if (cur_frame_force_integer_mv) {
        allow_high_precision_mv = 0
      } else {
        allow_high_precision_mv u(1)
      }
      allow_intrabc = 0
      read_mcomp_filter_type()

      switchable_motion_mode u(1)

      if (frame_might_use_prev_frame_mvs()) {
        can_use_ref u(1)
      } else {
        can_use_ref = 0
      }
      can_use_previous = can_use_ref && frame_can_use_prev_frame_mvs()
      //for (i = 0; i < ALLOWED_REFS_PER_FRAME; i++)
      //  setup_scale_factors(i)
    }
#if ENCODE
    if (show_frame==0) {
      // Mark the first refreshed frame as being unshown
      CHECK(enc_large_scale_tile || refresh_frame_flags,"Unshown frame must be stored in frame buffer")
      for(i=0;i<NUM_REF_FRAMES;i++) {
        m = 1<<i
        if (refresh_frame_flags & m) {
          break
        }
      }
    }
#endif
  }

  setup_frame_buf_refs()

  CSV(CSV_PROFILE,profile,subsampling_x,subsampling_y)
  CSV(CSV_PICTURE_SIZE,crop_width,crop_height)

  // Save decode parameters
  dec_profile  = profile
  dec_bit_depth     = bit_depth
  dec_subsampling_x = subsampling_x
  dec_subsampling_y = subsampling_y
  dec_color_primaries = color_primaries
  dec_transfer_characteristics = transfer_characteristics
  dec_matrix_coefficients = matrix_coefficients
  if (subsampling_x && subsampling_y)
    dec_chroma_sample_position = chroma_sample_position

  // REFRESH_FRAME_CONTEXT_OFF means no update of probs
  //    signalled by refresh_frame_context=0
  // REFRESH_FRAME_CONTEXT_FORWARD means probs updated after header
  //    signalled by refresh_frame_context=1,frame_parallel_decoding_mode=1
  // REFRESH_FRAME_CONTEXT_BACKWARD means probs updated after adaption
  //    signalled by refresh_frame_context=1,frame_parallel_decoding_mode=0
  //
  // Looks like this has changed again on nextgenv2
  //   Looks like OFF has been removed!
  //   Only choices are forward or backward!
  //   Effectively this means that refresh_frame_context is always 1, and frame_parallel_decoding_mode is 1 for forward
  //   TODO refactor this code so refresh_frame_context indicates direction (as in code)
  if (!reduced_still_picture_hdr && !disable_cdf_update) {
    refresh_frame_context = 1
    if (refresh_frame_context) {
      frame_parallel_decoding_mode u(1)
    } else {
      frame_parallel_decoding_mode = 1
    }
  } else {
    refresh_frame_context = 0
    frame_parallel_decoding_mode = 1
  }
  if (intra_only || error_resilient_mode) {
    setup_past_independence()
  } else if (primary_ref_frame == PRIMARY_REF_NONE) {
    init_frame_context() // Load default CDFs
    setup_past_independence() // Test
    setup_motion_field()
    for (ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++) {
      set_default_warp_params(ref_gm_params[1][ref])
    }
  } else {
    load_cdfs(active_ref_idx[primary_ref_frame])
    load_previous()
  }
  setup_ref_frame_side()

  numLfParams = monochrome ? 2 : FRAME_LF_COUNT
  for (i = 0; i < numLfParams; i++) {
    filter_level[i] = 0
  }
  mode_ref_delta_enabled = 0
  cdef_bits = 0
  cdef_y_pri_strength[0] = 0
  cdef_y_sec_strength[0] = 0
  cdef_uv_pri_strength[0] = 0
  cdef_uv_sec_strength[0] = 0
  frame_restoration_type[0] = RESTORE_NONE
  frame_restoration_type[1] = RESTORE_NONE
  frame_restoration_type[2] = RESTORE_NONE

  read_tiles()
  read_quantization()

  if (intra_only || error_resilient_mode || primary_ref_frame == PRIMARY_REF_NONE) {
    init_txb_probs()
  }

  read_segmentation()

  // Note: In the ref code, the following section (up to setup_lossless())
  // occurs *after* delta_q_params(). But for our purposes, it's more useful
  // to have it before, so that delta_q_params() can look at the
  // 'coded_lossless' value.
  // This movement is fine because the following block is pure calculation,
  // ie. doesn't read anything from the bitstream, and does not depend on
  // the delta-q information in any way.
  for (segmentId = 0; segmentId < MAX_SEGMENTS; segmentId++) {
    if (using_qmatrix) {
      qm_levels[segmentId][0] = qm_y
      qm_levels[segmentId][1] = qm_u
      qm_levels[segmentId][2] = qm_v
    }
  }
  setup_lossless()

  delta_q_params( )

  // Initialize the loop filter deltas even if we don't actually use loop filtering
  // this frame.
  if (primary_ref_frame == PRIMARY_REF_NONE || allow_intrabc || coded_lossless) {
    set_default_lf_deltas()
  } else {
    copy_lf_deltas(0, 1) // Load deltas from prev_frame
  }

  skip_cdef = 1
  if (! allow_intrabc) {
    if (!coded_lossless) {
      read_loopfilter()
    }
    if (enable_cdef && !coded_lossless) {
      setup_cdef()
    }
    UsesLr = 0
    if (enable_loop_restoration && !all_lossless) {
      read_frame_restoration_info()
    }
    loop_filter_frame_init()
  }

  tx_mode = read_tx_mode()
  compound_allowed = !intra_only
  reference_select = 0
  comp_pred_mode = compound_allowed ? read_comp_pred_mode() : SINGLE_PREDICTION_ONLY

  if (compound_allowed)
    setup_compound_prediction()
  read_ext_skip()
  if (error_resilient_mode || intra_only || !enable_warped_motion)
    allow_warped_motion = 0
  else
    allow_warped_motion u(1)
  reduced_tx_set_used u(1)

  for (ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++) {
    gm_type[ref] = IDENTITY
    set_default_warp_params(ref_gm_params[0][ref])
    // Default to warpable, so that IDENTITY and TRANSLATION models are marked
    // as warpable.
    // (this interacts with warped-motion in allow_warp())
    gm_warpable[ref] = 1
  }
  if (!intra_only) {
    read_global_motion()
  }

  read_film_grain()

  return 0
}

setup_frame_buf_refs() {
  if (!enable_order_hint)
    cur_frame_offset = current_video_frame

  ref_decode_order[0] = cur_frame_offset

  for (i = LAST_FRAME; i <= ALTREF_FRAME; i++) {
    bufIdx = active_ref_idx[i - LAST_FRAME]
    if (bufIdx >= 0) {
      ref_decode_order[i] = saved_ref_decode_order[bufIdx][0]
    } else {
      ref_decode_order[i] = 0
    }
  }

  // Derive ref frame sign biases from the frame offsets:
  // "backward" (later in time) references get sign bias 1,
  // "forward" (earlier in time) references get sign bias 0.
  if (enable_order_hint) {
    for (i = LAST_FRAME; i <= ALTREF_FRAME; i++) {
      //slot = active_ref_idx[i - LAST_FRAME]
      refOffset = ref_decode_order[i]
      ref_frame_sign_bias[i] = (get_relative_dist(refOffset, cur_frame_offset) > 0)
    }
  } else {
    for (i = LAST_FRAME; i <= ALTREF_FRAME; i++) {
      ref_frame_sign_bias[i] = 0
    }
  }
}
