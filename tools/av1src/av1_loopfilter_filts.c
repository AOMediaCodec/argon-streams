/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_FILTERS 0

// Specification for deblocking filters


int16 signed_char_clamp(int t) {
  return Clip3( -(1<<(bit_depth-1)), (1<<(bit_depth-1))-1, t)
}

make_filter_mask_filter(limit, blimit, filter_len, q0, q1, q2, q3, p0, p1, p2, p3) {
  uint8 mask

  // should we apply any filter at all: 11111111 yes, 00000000 no
  limit16 = limit<<(bit_depth-8)
  blimit16 = blimit<<(bit_depth-8)
  mask  = 0
  if (filter_len >= 4) {
    mask |= (Abs(p1 - p0) > limit16)
    mask |= (Abs(q1 - q0) > limit16)
    mask |= (Abs(p0 - q0) * 2 + Abs(p1 - q1) / 2  > blimit16)
  }
  if (filter_len >= 6) {
    mask |= (Abs(p2 - p1) > limit16)
    mask |= (Abs(q2 - q1) > limit16)
  }
  if (filter_len >= 8) {
    mask |= (Abs(p3 - p2) > limit16)
    mask |= (Abs(q3 - q2) > limit16)
  }
  filter_mask =  mask == 0
}

make_filter_mask_hev(q0, q1, p0, p1, thresh) {
  // is there high edge variance internal edge: 11111111 yes, 00000000 no
  hev_mask = 0
  thresh16 = thresh << (bit_depth-8)
  hev_mask  |= (Abs(p1 - p0) > thresh16)
  hev_mask  |= (Abs(q1 - q0) > thresh16)
}

make_filter_mask(limit, blimit, thresh, x, y, plane, dx, dy, filter_len) {
  uint8 mask
  q0 = frame[plane][x][y]
  q1 = frame[plane][x+dx][y+dy]
  q2 = frame[plane][x+dx*2][y+dy*2]
  q3 = frame[plane][x+dx*3][y+dy*3]
  p0 = frame[plane][x-dx][y-dy]
  p1 = frame[plane][x-dx*2][y-dy*2]
  p2 = frame[plane][x-dx*3][y-dy*3]
  p3 = frame[plane][x-dx*4][y-dy*4]

  make_filter_mask_filter(limit, blimit, filter_len, q0, q1, q2, q3, p0, p1, p2, p3)

  make_filter_mask_hev(q0, q1, p0, p1, thresh)

  thresh16 = 1<<(bit_depth-8)
  if (filter_len>=6) { // 6, 8, or 16-tap filters
    mask = 0
    mask |= (Abs(p1 - p0) > thresh16)
    mask |= (Abs(q1 - q0) > thresh16)
    mask |= (Abs(p2 - p0) > thresh16)
    mask |= (Abs(q2 - q0) > thresh16)
#if PARALLEL_DEBLOCKING_5_TAP_CHROMA
    if (filter_len >= 8) {
#endif
      mask |= (Abs(p3 - p0) > thresh16)
      mask |= (Abs(q3 - q0) > thresh16)
#if PARALLEL_DEBLOCKING_5_TAP_CHROMA
    }
#endif
    flat_mask = mask==0
  }
  if (filter_len>=16) {
    q4 = frame[plane][x+dx*4][y+dy*4]
    q5 = frame[plane][x+dx*5][y+dy*5]
    q6 = frame[plane][x+dx*6][y+dy*6]
    p4 = frame[plane][x-dx*5][y-dy*5]
    p5 = frame[plane][x-dx*6][y-dy*6]
    p6 = frame[plane][x-dx*7][y-dy*7]
    mask = 0
    mask |= (Abs(p6 - p0) > thresh16)
    mask |= (Abs(q6 - q0) > thresh16)
    mask |= (Abs(p5 - p0) > thresh16)
    mask |= (Abs(q5 - q0) > thresh16)
    mask |= (Abs(p4 - p0) > thresh16)
    mask |= (Abs(q4 - q0) > thresh16)
    flat_mask2 = mask==0
  }
}

filter4(x,y,plane,dx,dy) {
  int16 filter
  int16 filter1
  int16 filter2
  shift = bit_depth - 8
  midlevel = 0x80 << shift
  q0 = frame[plane][x][y]
  q1 = frame[plane][x+dx][y+dy]

  p0 = frame[plane][x-dx][y-dy]
  p1 = frame[plane][x-dx*2][y-dy*2]

  m = filter_mask

  if (filter_mask) {
    ps1 = p1 - midlevel
    ps0 = p0 - midlevel
    qs0 = q0 - midlevel
    qs1 = q1 - midlevel

    // add outer taps if we have high edge variance
    filter = hev_mask ? signed_char_clamp(ps1 - qs1) : 0

    // inner taps
    filter = signed_char_clamp(filter + 3 * (qs0 - ps0))

    // save bottom 3 bits so that we round one side +4 and the other +3
    // if it equals 4 we'll set to adjust by -1 to account for the fact
    // we'd round 3 the other way
    filter1 = signed_char_clamp(filter + 4) >> 3
    filter2 = signed_char_clamp(filter + 3) >> 3

    oq0_base = qs0 - filter1 // (6-1) [RNG-Filter4-1]
    oq0 = signed_char_clamp(oq0_base) + midlevel
    op0_base = ps0 + filter2 // (6-2) [RNG-Filter4-2]
    op0 = signed_char_clamp(op0_base) + midlevel
    frame[plane][x][y]           = oq0
    frame[plane][x-dx][y-dy]     = op0

#if VALIDATE_FILTERS
    validate(4000)
    validate(oq0)
    validate(op0)
#endif

    // outer tap adjustments
    if (!hev_mask) {
      // Only change outside pixels if don't have high edge variance
      filter = ROUND_POWER_OF_TWO(filter1, 1)
      oq1_base = qs1 - filter // (6-3) [RNG-Filter4-3]
      oq1 = signed_char_clamp(oq1_base) + midlevel
      op1_base = ps1 + filter // (6-4) [RNG-Filter4-4]
      op1 = signed_char_clamp(op1_base) + midlevel
      frame[plane][x+dx][y+dy]     = oq1
      frame[plane][x-dx*2][y-dy*2] = op1

#if VALIDATE_FILTERS
    validate(oq1)
    validate(op1)
#endif

    } else {
      oq1 = q1
      op1 = p1
    }
  } else {
    op1 = p1
    op0 = p0
    oq0 = q0
    oq1 = q1
  }
}

lpf_horizontal_edge_4(limit,blimit,thresh,x,y,plane) {
    make_filter_mask(limit,blimit,thresh,x,y,plane,0,1,4)
    filter4(x,y,plane,0,1)
}

lpf_vertical_edge_4(limit,blimit,thresh,x,y,plane) {
    make_filter_mask(limit,blimit,thresh,x,y,plane,1,0,4)
    filter4(x,y,plane,1,0)
}

filter6(x,y,plane,dx,dy) {
  ASSERT(plane != 0, "6-tap deblock filter should only be used for chroma planes")
  m = filter_mask
  f = flat_mask
  if (flat_mask && filter_mask) {
    q0 = frame[plane][x][y]
    q1 = frame[plane][x+dx][y+dy]
    q2 = frame[plane][x+dx*2][y+dy*2]

    p0 = frame[plane][x-dx][y-dy]
    p1 = frame[plane][x-dx*2][y-dy*2]
    p2 = frame[plane][x-dx*3][y-dy*3]

    // 5-tap filter [1, 2, 2, 2, 1]
    op1 = ROUND_POWER_OF_TWO(p2 * 3 + p1 * 2 + p0 * 2 + q0, 3)      // (7-1) [RNG-Filter6-1]
    op0 = ROUND_POWER_OF_TWO(p2 + p1 * 2 + p0 * 2 + q0 * 2 + q1, 3) // (7-2) [RNG-Filter6-2]
    oq0 = ROUND_POWER_OF_TWO(p1 + p0 * 2 + q0 * 2 + q1 * 2 + q2, 3) // (7-3) [RNG-Filter6-3]
    oq1 = ROUND_POWER_OF_TWO(p0 + q0 * 2 + q1 * 2 + q2 * 3, 3)      // (7-4) [RNG-Filter6-4]

    frame[plane][x][y]           = oq0
    frame[plane][x+dx][y+dy]     = oq1

    frame[plane][x-dx][y-dy]     = op0
    frame[plane][x-dx*2][y-dy*2] = op1

#if VALIDATE_FILTERS
    validate(5000)
    validate(op1)
    validate(op0)
    validate(oq0)
    validate(oq1)
#endif
  } else {
    filter4(x,y,plane,dx,dy)
  }
}


lpf_horizontal_edge_6(limit,blimit,thresh,x,y,plane) {
    make_filter_mask(limit,blimit,thresh,x,y,plane,0,1,6)
    filter6(x,y,plane,0,1)
}

lpf_vertical_edge_6(limit,blimit,thresh,x,y,plane) {
    make_filter_mask(limit,blimit,thresh,x,y,plane,1,0,6)
    filter6(x,y,plane,1,0)
}

filter8(x,y,plane,dx,dy) {
  ASSERT(plane == 0, "8-tap deblock filter should only be used for luma plane")
  m = filter_mask
  f = flat_mask
  if (flat_mask && filter_mask) {

    q0 = frame[plane][x][y]
    q1 = frame[plane][x+dx][y+dy]
    q2 = frame[plane][x+dx*2][y+dy*2]
    q3 = frame[plane][x+dx*3][y+dy*3]

    p0 = frame[plane][x-dx][y-dy]
    p1 = frame[plane][x-dx*2][y-dy*2]
    p2 = frame[plane][x-dx*3][y-dy*3]
    p3 = frame[plane][x-dx*4][y-dy*4]

    // 7-tap filter [1, 1, 1, 2, 1, 1, 1]
    op2 = ROUND_POWER_OF_TWO(p3 + p3 + p3 + 2 * p2 + p1 + p0 + q0, 3) // (7-1) [RNG-Filter8-1]
    op1 = ROUND_POWER_OF_TWO(p3 + p3 + p2 + 2 * p1 + p0 + q0 + q1, 3) // (7-2) [RNG-Filter8-2]
    op0 = ROUND_POWER_OF_TWO(p3 + p2 + p1 + 2 * p0 + q0 + q1 + q2, 3) // (7-3) [RNG-Filter8-3]
    oq0 = ROUND_POWER_OF_TWO(p2 + p1 + p0 + 2 * q0 + q1 + q2 + q3, 3) // (7-4) [RNG-Filter8-4]
    oq1 = ROUND_POWER_OF_TWO(p1 + p0 + q0 + 2 * q1 + q2 + q3 + q3, 3) // (7-5) [RNG-Filter8-5]
    oq2 = ROUND_POWER_OF_TWO(p0 + q0 + q1 + 2 * q2 + q3 + q3 + q3, 3) // (7-6) [RNG-Filter8-6]

    frame[plane][x][y]           = oq0
    frame[plane][x+dx][y+dy]     = oq1
    frame[plane][x+dx*2][y+dy*2] = oq2

    frame[plane][x-dx][y-dy]     = op0
    frame[plane][x-dx*2][y-dy*2] = op1
    frame[plane][x-dx*3][y-dy*3] = op2

#if VALIDATE_FILTERS
    validate(5000)
    validate(op2)
    validate(op1)
    validate(op0)
    validate(oq0)
    validate(oq1)
    validate(oq2)
#endif
  } else {
    filter4(x,y,plane,dx,dy)
  }
}


lpf_horizontal_edge_8(limit,blimit,thresh,x,y,plane) {
    make_filter_mask(limit,blimit,thresh,x,y,plane,0,1,8)
    filter8(x,y,plane,0,1)
}

lpf_vertical_edge_8(limit,blimit,thresh,x,y,plane) {
    make_filter_mask(limit,blimit,thresh,x,y,plane,1,0,8)
    filter8(x,y,plane,1,0)
}

filter16(x,y,plane,dx,dy) {
  ASSERT(plane == 0, "16-tap deblock filter should only be used for luma plane")
  m = filter_mask
  f = flat_mask
  f2 = flat_mask2
  if (flat_mask2 && flat_mask && filter_mask) {
    q0 = frame[plane][x][y]
    q1 = frame[plane][x+dx][y+dy]
    q2 = frame[plane][x+dx*2][y+dy*2]
    q3 = frame[plane][x+dx*3][y+dy*3]
    q4 = frame[plane][x+dx*4][y+dy*4]
    q5 = frame[plane][x+dx*5][y+dy*5]
    q6 = frame[plane][x+dx*6][y+dy*6]
    p0 = frame[plane][x-dx][y-dy]
    p1 = frame[plane][x-dx*2][y-dy*2]
    p2 = frame[plane][x-dx*3][y-dy*3]
    p3 = frame[plane][x-dx*4][y-dy*4]
    p4 = frame[plane][x-dx*5][y-dy*5]
    p5 = frame[plane][x-dx*6][y-dy*6]
    p6 = frame[plane][x-dx*7][y-dy*7]

    // 13-tap filter [1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1]
    op5 = ROUND_POWER_OF_TWO(p6 * 7 + p5 * 2 + p4 * 2 + p3 + p2 + p1 + p0 + q0, 4)                            // (8-1) [RNG-Filter14-1]
    op4 = ROUND_POWER_OF_TWO(p6 * 5 + p5 * 2 + p4 * 2 + p3 * 2 + p2 + p1 + p0 + q0 + q1, 4)                   // (8-2) [RNG-Filter14-2]
    op3 = ROUND_POWER_OF_TWO(p6 * 4 + p5 + p4 * 2 + p3 * 2 + p2 * 2 + p1 + p0 + q0 + q1 + q2, 4)              // (8-3) [RNG-Filter14-3]
    op2 = ROUND_POWER_OF_TWO(p6 * 3 + p5 + p4 + p3 * 2 + p2 * 2 + p1 * 2 + p0 + q0 + q1 + q2 + q3, 4)         // (8-4) [RNG-Filter14-4]
    op1 = ROUND_POWER_OF_TWO(p6 * 2 + p5 + p4 + p3 + p2 * 2 + p1 * 2 + p0 * 2 + q0 + q1 + q2 + q3 + q4, 4)    // (8-5) [RNG-Filter14-5]
    op0 = ROUND_POWER_OF_TWO(p6 + p5 + p4 + p3 + p2 + p1 * 2 + p0 * 2 + q0 * 2 + q1 + q2 + q3 + q4 + q5, 4)   // (8-6) [RNG-Filter14-6]
    oq0 = ROUND_POWER_OF_TWO(p5 + p4 + p3 + p2 + p1 + p0 * 2 + q0 * 2 + q1 * 2 + q2 + q3 + q4 + q5 + q6, 4)   // (8-7) [RNG-Filter14-7]
    oq1 = ROUND_POWER_OF_TWO(p4 + p3 + p2 + p1 + p0 + q0 * 2 + q1 * 2 + q2 * 2 + q3 + q4 + q5 + q6 * 2, 4)    // (8-8) [RNG-Filter14-8]
    oq2 = ROUND_POWER_OF_TWO(p3 + p2 + p1 + p0 + q0 + q1 * 2 + q2 * 2 + q3 * 2 + q4 + q5 + q6 * 3, 4)         // (8-9) [RNG-Filter14-9]
    oq3 = ROUND_POWER_OF_TWO(p2 + p1 + p0 + q0 + q1 + q2 * 2 + q3 * 2 + q4 * 2 + q5 + q6 * 4, 4)              // (8-10) [RNG-Filter14-10]
    oq4 = ROUND_POWER_OF_TWO(p1 + p0 + q0 + q1 + q2 + q3 * 2 + q4 * 2 + q5 * 2 + q6 * 5, 4)                   // (8-11) [RNG-Filter14-11]
    oq5 = ROUND_POWER_OF_TWO(p0 + q0 + q1 + q2 + q3 + q4 * 2 + q5 * 2 + q6 * 7, 4)                            // (8-12) [RNG-Filter14-12]

    frame[plane][x][y]           = oq0
    frame[plane][x+dx][y+dy]     = oq1
    frame[plane][x+dx*2][y+dy*2] = oq2
    frame[plane][x+dx*3][y+dy*3] = oq3
    frame[plane][x+dx*4][y+dy*4] = oq4
    frame[plane][x+dx*5][y+dy*5] = oq5

    frame[plane][x-dx][y-dy]     = op0
    frame[plane][x-dx*2][y-dy*2] = op1
    frame[plane][x-dx*3][y-dy*3] = op2
    frame[plane][x-dx*4][y-dy*4] = op3
    frame[plane][x-dx*5][y-dy*5] = op4
    frame[plane][x-dx*6][y-dy*6] = op5

#if VALIDATE_FILTERS
    validate(5000)
    validate(op5)
    validate(op4)
    validate(op3)
    validate(op2)
    validate(op1)
    validate(op0)
    validate(oq0)
    validate(oq1)
    validate(oq2)
    validate(oq3)
    validate(oq4)
    validate(oq5)
#endif

  } else {
    filter8(x,y,plane,dx,dy)
  }
}

lpf_horizontal_edge_16(limit,blimit,thresh,x,y,plane) {
    make_filter_mask(limit,blimit,thresh,x,y,plane,0,1,16)
    filter16(x,y,plane,0,1)
}

lpf_vertical_edge_16(limit,blimit,thresh,x,y,plane) {
    make_filter_mask(limit,blimit,thresh,x,y,plane,1,0,16)
    filter16(x,y,plane,1,0)
}


