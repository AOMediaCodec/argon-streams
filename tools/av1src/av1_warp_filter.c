/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

// Common functions between the global-motion and warped-motion experiments

#define VALIDATE_WARP 0

copy_warp_params(int32pointer params, int16pointer aux){
  for (i = 0; i < 6; i++)
    warp_params[i] = params[i]
  for (i = 0; i < 4; i++)
    warp_aux[i] = aux[i]
}

// This function will determine whether or not to create a warped
// prediction and return the appropriate motion model.
//
// Important: When MOTION_VAR is enabled, we must have src_mi_row = mi_row + mi_row_offset
// (where mi_row, mi_row_offset are the arguments to this function in the reference code),
// and similar for src_mi_col
int allow_warp(block, ref, src_mi_row, src_mi_col, w, h, build_for_obmc) {
  // Don't warp sub-8x8 blocks
  if (w < 8 || h < 8)
    return 0

  src_ref_frame = refframes[0][src_mi_col][src_mi_row][ref]
  src_ref_slot = active_ref_idx[src_ref_frame - LAST_FRAME]

  // Don't apply warp to scaled ref frames
  if (crop_width != ref_width[src_ref_slot] || crop_height != ref_height[src_ref_slot])
    return 0

  // Can't apply warp during OBMC predictions
  if (build_for_obmc)
    return 0

  if (MotionMode == WARPED_CAUSAL && wm_warpable) {
    copy_warp_params(wm_params, wm_aux)
    return 1
  }

  if (is_global_mv_block(src_mi_row, src_mi_col, block, src_ref_frame) &&
      gm_warpable[src_ref_frame]) {
    copy_warp_params(ref_gm_params[0][src_ref_frame], gm_aux[src_ref_frame])
    return 1
  }

  return 0
}

uint16 div_lut[DIV_LUT_NUM + 1] = {
  16384, 16320, 16257, 16194, 16132, 16070, 16009, 15948, 15888, 15828, 15768,
  15709, 15650, 15592, 15534, 15477, 15420, 15364, 15308, 15252, 15197, 15142,
  15087, 15033, 14980, 14926, 14873, 14821, 14769, 14717, 14665, 14614, 14564,
  14513, 14463, 14413, 14364, 14315, 14266, 14218, 14170, 14122, 14075, 14028,
  13981, 13935, 13888, 13843, 13797, 13752, 13707, 13662, 13618, 13574, 13530,
  13487, 13443, 13400, 13358, 13315, 13273, 13231, 13190, 13148, 13107, 13066,
  13026, 12985, 12945, 12906, 12866, 12827, 12788, 12749, 12710, 12672, 12633,
  12596, 12558, 12520, 12483, 12446, 12409, 12373, 12336, 12300, 12264, 12228,
  12193, 12157, 12122, 12087, 12053, 12018, 11984, 11950, 11916, 11882, 11848,
  11815, 11782, 11749, 11716, 11683, 11651, 11619, 11586, 11555, 11523, 11491,
  11460, 11429, 11398, 11367, 11336, 11305, 11275, 11245, 11215, 11185, 11155,
  11125, 11096, 11067, 11038, 11009, 10980, 10951, 10923, 10894, 10866, 10838,
  10810, 10782, 10755, 10727, 10700, 10673, 10645, 10618, 10592, 10565, 10538,
  10512, 10486, 10460, 10434, 10408, 10382, 10356, 10331, 10305, 10280, 10255,
  10230, 10205, 10180, 10156, 10131, 10107, 10082, 10058, 10034, 10010, 9986,
  9963,  9939,  9916,  9892,  9869,  9846,  9823,  9800,  9777,  9754,  9732,
  9709,  9687,  9664,  9642,  9620,  9598,  9576,  9554,  9533,  9511,  9489,
  9468,  9447,  9425,  9404,  9383,  9362,  9341,  9321,  9300,  9279,  9259,
  9239,  9218,  9198,  9178,  9158,  9138,  9118,  9098,  9079,  9059,  9039,
  9020,  9001,  8981,  8962,  8943,  8924,  8905,  8886,  8867,  8849,  8830,
  8812,  8793,  8775,  8756,  8738,  8720,  8702,  8684,  8666,  8648,  8630,
  8613,  8595,  8577,  8560,  8542,  8525,  8508,  8490,  8473,  8456,  8439,
  8422,  8405,  8389,  8372,  8355,  8339,  8322,  8306,  8289,  8273,  8257,
  8240,  8224,  8208,  8192
};

int16 resolve_divisor(int64 D) {
  ASSERT(D >= 1968, "It has been proven that the input to resolve_divisor() should always >= 1968")

  int64 e
  int64 f
  div_shift = get_msb(D)
  // e is obtained from D after resetting the most significant 1 bit.
  e = D - (convert_to_int64(1) << div_shift)
  // Get the most significant DIV_LUT_BITS (8) bits of e into f
  f = ROUND_POWER_OF_TWO(e, div_shift - DIV_LUT_BITS)
  CHECK(f <= DIV_LUT_NUM, "Out-of-range value in resolve_divisor")
  div_shift += DIV_LUT_PREC_BITS
  // Use f as lookup into the precomputed table of multipliers
  div_factor = div_lut[f]
}

// Calculate the auxiliary shear parameters (alpha, beta, gamma, delta)
// from a warp model in 'params'. Returns 1 if we can warp using this model,
// 0 if we can't.
int setup_shear_params(int32pointer params, int16pointer aux_params) {
  if (params[2] <= 0) return 0

  aux_params[0] =
      clamp(params[2] - (1 << WARPEDMODEL_PREC_BITS),
            INT16_MIN, INT16_MAX)
  aux_params[1] = clamp(params[3], INT16_MIN, INT16_MAX)
  resolve_divisor(params[2])

  int64 v
  v = params[4]
  v = (v * (1 << WARPEDMODEL_PREC_BITS)) * div_factor
  aux_params[2] =
      clamp(ROUND_POWER_OF_TWO_SIGNED(v, div_shift), INT16_MIN, INT16_MAX)

  v = params[3]
  v = (v * params[4]) * div_factor
  aux_params[3] = clamp(params[5] -
                        ROUND_POWER_OF_TWO_SIGNED(v, div_shift) -
                        (1 << WARPEDMODEL_PREC_BITS),
                    INT16_MIN, INT16_MAX)

  aux_params[0] = ROUND_POWER_OF_TWO_SIGNED(aux_params[0], WARP_PARAM_REDUCE_BITS) *
                  (1 << WARP_PARAM_REDUCE_BITS)
  aux_params[1] = ROUND_POWER_OF_TWO_SIGNED(aux_params[1], WARP_PARAM_REDUCE_BITS) *
                  (1 << WARP_PARAM_REDUCE_BITS)
  aux_params[2] = ROUND_POWER_OF_TWO_SIGNED(aux_params[2], WARP_PARAM_REDUCE_BITS) *
                  (1 << WARP_PARAM_REDUCE_BITS)
  aux_params[3] = ROUND_POWER_OF_TWO_SIGNED(aux_params[3], WARP_PARAM_REDUCE_BITS) *
                  (1 << WARP_PARAM_REDUCE_BITS)

  if ((4 * Abs(aux_params[0]) + 7 * Abs(aux_params[1]) >= (1 << WARPEDMODEL_PREC_BITS)) ||
      (4 * Abs(aux_params[2]) + 4 * Abs(aux_params[3]) >= (1 << WARPEDMODEL_PREC_BITS))) {
    return 0
  }

  return 1
}

int16 warped_filter[WARPEDPIXEL_PREC_SHIFTS * 3 + 1][8] = {
#if WARPEDPIXEL_PREC_BITS == 6
  // [-1, 0)
  { 0,   0, 127,   1,   0, 0, 0, 0 }, { 0, - 1, 127,   2,   0, 0, 0, 0 },
  { 1, - 3, 127,   4, - 1, 0, 0, 0 }, { 1, - 4, 126,   6, - 2, 1, 0, 0 },
  { 1, - 5, 126,   8, - 3, 1, 0, 0 }, { 1, - 6, 125,  11, - 4, 1, 0, 0 },
  { 1, - 7, 124,  13, - 4, 1, 0, 0 }, { 2, - 8, 123,  15, - 5, 1, 0, 0 },
  { 2, - 9, 122,  18, - 6, 1, 0, 0 }, { 2, -10, 121,  20, - 6, 1, 0, 0 },
  { 2, -11, 120,  22, - 7, 2, 0, 0 }, { 2, -12, 119,  25, - 8, 2, 0, 0 },
  { 3, -13, 117,  27, - 8, 2, 0, 0 }, { 3, -13, 116,  29, - 9, 2, 0, 0 },
  { 3, -14, 114,  32, -10, 3, 0, 0 }, { 3, -15, 113,  35, -10, 2, 0, 0 },
  { 3, -15, 111,  37, -11, 3, 0, 0 }, { 3, -16, 109,  40, -11, 3, 0, 0 },
  { 3, -16, 108,  42, -12, 3, 0, 0 }, { 4, -17, 106,  45, -13, 3, 0, 0 },
  { 4, -17, 104,  47, -13, 3, 0, 0 }, { 4, -17, 102,  50, -14, 3, 0, 0 },
  { 4, -17, 100,  52, -14, 3, 0, 0 }, { 4, -18,  98,  55, -15, 4, 0, 0 },
  { 4, -18,  96,  58, -15, 3, 0, 0 }, { 4, -18,  94,  60, -16, 4, 0, 0 },
  { 4, -18,  91,  63, -16, 4, 0, 0 }, { 4, -18,  89,  65, -16, 4, 0, 0 },
  { 4, -18,  87,  68, -17, 4, 0, 0 }, { 4, -18,  85,  70, -17, 4, 0, 0 },
  { 4, -18,  82,  73, -17, 4, 0, 0 }, { 4, -18,  80,  75, -17, 4, 0, 0 },
  { 4, -18,  78,  78, -18, 4, 0, 0 }, { 4, -17,  75,  80, -18, 4, 0, 0 },
  { 4, -17,  73,  82, -18, 4, 0, 0 }, { 4, -17,  70,  85, -18, 4, 0, 0 },
  { 4, -17,  68,  87, -18, 4, 0, 0 }, { 4, -16,  65,  89, -18, 4, 0, 0 },
  { 4, -16,  63,  91, -18, 4, 0, 0 }, { 4, -16,  60,  94, -18, 4, 0, 0 },
  { 3, -15,  58,  96, -18, 4, 0, 0 }, { 4, -15,  55,  98, -18, 4, 0, 0 },
  { 3, -14,  52, 100, -17, 4, 0, 0 }, { 3, -14,  50, 102, -17, 4, 0, 0 },
  { 3, -13,  47, 104, -17, 4, 0, 0 }, { 3, -13,  45, 106, -17, 4, 0, 0 },
  { 3, -12,  42, 108, -16, 3, 0, 0 }, { 3, -11,  40, 109, -16, 3, 0, 0 },
  { 3, -11,  37, 111, -15, 3, 0, 0 }, { 2, -10,  35, 113, -15, 3, 0, 0 },
  { 3, -10,  32, 114, -14, 3, 0, 0 }, { 2, - 9,  29, 116, -13, 3, 0, 0 },
  { 2, - 8,  27, 117, -13, 3, 0, 0 }, { 2, - 8,  25, 119, -12, 2, 0, 0 },
  { 2, - 7,  22, 120, -11, 2, 0, 0 }, { 1, - 6,  20, 121, -10, 2, 0, 0 },
  { 1, - 6,  18, 122, - 9, 2, 0, 0 }, { 1, - 5,  15, 123, - 8, 2, 0, 0 },
  { 1, - 4,  13, 124, - 7, 1, 0, 0 }, { 1, - 4,  11, 125, - 6, 1, 0, 0 },
  { 1, - 3,   8, 126, - 5, 1, 0, 0 }, { 1, - 2,   6, 126, - 4, 1, 0, 0 },
  { 0, - 1,   4, 127, - 3, 1, 0, 0 }, { 0,   0,   2, 127, - 1, 0, 0, 0 },

  // [0, 1)
  { 0,  0,   0, 127,   1,   0,  0,  0}, { 0,  0,  -1, 127,   2,   0,  0,  0},
  { 0,  1,  -3, 127,   4,  -2,  1,  0}, { 0,  1,  -5, 127,   6,  -2,  1,  0},
  { 0,  2,  -6, 126,   8,  -3,  1,  0}, {-1,  2,  -7, 126,  11,  -4,  2, -1},
  {-1,  3,  -8, 125,  13,  -5,  2, -1}, {-1,  3, -10, 124,  16,  -6,  3, -1},
  {-1,  4, -11, 123,  18,  -7,  3, -1}, {-1,  4, -12, 122,  20,  -7,  3, -1},
  {-1,  4, -13, 121,  23,  -8,  3, -1}, {-2,  5, -14, 120,  25,  -9,  4, -1},
  {-1,  5, -15, 119,  27, -10,  4, -1}, {-1,  5, -16, 118,  30, -11,  4, -1},
  {-2,  6, -17, 116,  33, -12,  5, -1}, {-2,  6, -17, 114,  35, -12,  5, -1},
  {-2,  6, -18, 113,  38, -13,  5, -1}, {-2,  7, -19, 111,  41, -14,  6, -2},
  {-2,  7, -19, 110,  43, -15,  6, -2}, {-2,  7, -20, 108,  46, -15,  6, -2},
  {-2,  7, -20, 106,  49, -16,  6, -2}, {-2,  7, -21, 104,  51, -16,  7, -2},
  {-2,  7, -21, 102,  54, -17,  7, -2}, {-2,  8, -21, 100,  56, -18,  7, -2},
  {-2,  8, -22,  98,  59, -18,  7, -2}, {-2,  8, -22,  96,  62, -19,  7, -2},
  {-2,  8, -22,  94,  64, -19,  7, -2}, {-2,  8, -22,  91,  67, -20,  8, -2},
  {-2,  8, -22,  89,  69, -20,  8, -2}, {-2,  8, -22,  87,  72, -21,  8, -2},
  {-2,  8, -21,  84,  74, -21,  8, -2}, {-2,  8, -22,  82,  77, -21,  8, -2},
  {-2,  8, -21,  79,  79, -21,  8, -2}, {-2,  8, -21,  77,  82, -22,  8, -2},
  {-2,  8, -21,  74,  84, -21,  8, -2}, {-2,  8, -21,  72,  87, -22,  8, -2},
  {-2,  8, -20,  69,  89, -22,  8, -2}, {-2,  8, -20,  67,  91, -22,  8, -2},
  {-2,  7, -19,  64,  94, -22,  8, -2}, {-2,  7, -19,  62,  96, -22,  8, -2},
  {-2,  7, -18,  59,  98, -22,  8, -2}, {-2,  7, -18,  56, 100, -21,  8, -2},
  {-2,  7, -17,  54, 102, -21,  7, -2}, {-2,  7, -16,  51, 104, -21,  7, -2},
  {-2,  6, -16,  49, 106, -20,  7, -2}, {-2,  6, -15,  46, 108, -20,  7, -2},
  {-2,  6, -15,  43, 110, -19,  7, -2}, {-2,  6, -14,  41, 111, -19,  7, -2},
  {-1,  5, -13,  38, 113, -18,  6, -2}, {-1,  5, -12,  35, 114, -17,  6, -2},
  {-1,  5, -12,  33, 116, -17,  6, -2}, {-1,  4, -11,  30, 118, -16,  5, -1},
  {-1,  4, -10,  27, 119, -15,  5, -1}, {-1,  4,  -9,  25, 120, -14,  5, -2},
  {-1,  3,  -8,  23, 121, -13,  4, -1}, {-1,  3,  -7,  20, 122, -12,  4, -1},
  {-1,  3,  -7,  18, 123, -11,  4, -1}, {-1,  3,  -6,  16, 124, -10,  3, -1},
  {-1,  2,  -5,  13, 125,  -8,  3, -1}, {-1,  2,  -4,  11, 126,  -7,  2, -1},
  { 0,  1,  -3,   8, 126,  -6,  2,  0}, { 0,  1,  -2,   6, 127,  -5,  1,  0},
  { 0,  1,  -2,   4, 127,  -3,  1,  0}, { 0,  0,   0,   2, 127,  -1,  0,  0},

  // [1, 2)
  { 0, 0, 0,   1, 127,   0,   0, 0 }, { 0, 0, 0, - 1, 127,   2,   0, 0 },
  { 0, 0, 1, - 3, 127,   4, - 1, 0 }, { 0, 0, 1, - 4, 126,   6, - 2, 1 },
  { 0, 0, 1, - 5, 126,   8, - 3, 1 }, { 0, 0, 1, - 6, 125,  11, - 4, 1 },
  { 0, 0, 1, - 7, 124,  13, - 4, 1 }, { 0, 0, 2, - 8, 123,  15, - 5, 1 },
  { 0, 0, 2, - 9, 122,  18, - 6, 1 }, { 0, 0, 2, -10, 121,  20, - 6, 1 },
  { 0, 0, 2, -11, 120,  22, - 7, 2 }, { 0, 0, 2, -12, 119,  25, - 8, 2 },
  { 0, 0, 3, -13, 117,  27, - 8, 2 }, { 0, 0, 3, -13, 116,  29, - 9, 2 },
  { 0, 0, 3, -14, 114,  32, -10, 3 }, { 0, 0, 3, -15, 113,  35, -10, 2 },
  { 0, 0, 3, -15, 111,  37, -11, 3 }, { 0, 0, 3, -16, 109,  40, -11, 3 },
  { 0, 0, 3, -16, 108,  42, -12, 3 }, { 0, 0, 4, -17, 106,  45, -13, 3 },
  { 0, 0, 4, -17, 104,  47, -13, 3 }, { 0, 0, 4, -17, 102,  50, -14, 3 },
  { 0, 0, 4, -17, 100,  52, -14, 3 }, { 0, 0, 4, -18,  98,  55, -15, 4 },
  { 0, 0, 4, -18,  96,  58, -15, 3 }, { 0, 0, 4, -18,  94,  60, -16, 4 },
  { 0, 0, 4, -18,  91,  63, -16, 4 }, { 0, 0, 4, -18,  89,  65, -16, 4 },
  { 0, 0, 4, -18,  87,  68, -17, 4 }, { 0, 0, 4, -18,  85,  70, -17, 4 },
  { 0, 0, 4, -18,  82,  73, -17, 4 }, { 0, 0, 4, -18,  80,  75, -17, 4 },
  { 0, 0, 4, -18,  78,  78, -18, 4 }, { 0, 0, 4, -17,  75,  80, -18, 4 },
  { 0, 0, 4, -17,  73,  82, -18, 4 }, { 0, 0, 4, -17,  70,  85, -18, 4 },
  { 0, 0, 4, -17,  68,  87, -18, 4 }, { 0, 0, 4, -16,  65,  89, -18, 4 },
  { 0, 0, 4, -16,  63,  91, -18, 4 }, { 0, 0, 4, -16,  60,  94, -18, 4 },
  { 0, 0, 3, -15,  58,  96, -18, 4 }, { 0, 0, 4, -15,  55,  98, -18, 4 },
  { 0, 0, 3, -14,  52, 100, -17, 4 }, { 0, 0, 3, -14,  50, 102, -17, 4 },
  { 0, 0, 3, -13,  47, 104, -17, 4 }, { 0, 0, 3, -13,  45, 106, -17, 4 },
  { 0, 0, 3, -12,  42, 108, -16, 3 }, { 0, 0, 3, -11,  40, 109, -16, 3 },
  { 0, 0, 3, -11,  37, 111, -15, 3 }, { 0, 0, 2, -10,  35, 113, -15, 3 },
  { 0, 0, 3, -10,  32, 114, -14, 3 }, { 0, 0, 2, - 9,  29, 116, -13, 3 },
  { 0, 0, 2, - 8,  27, 117, -13, 3 }, { 0, 0, 2, - 8,  25, 119, -12, 2 },
  { 0, 0, 2, - 7,  22, 120, -11, 2 }, { 0, 0, 1, - 6,  20, 121, -10, 2 },
  { 0, 0, 1, - 6,  18, 122, - 9, 2 }, { 0, 0, 1, - 5,  15, 123, - 8, 2 },
  { 0, 0, 1, - 4,  13, 124, - 7, 1 }, { 0, 0, 1, - 4,  11, 125, - 6, 1 },
  { 0, 0, 1, - 3,   8, 126, - 5, 1 }, { 0, 0, 1, - 2,   6, 126, - 4, 1 },
  { 0, 0, 0, - 1,   4, 127, - 3, 1 }, { 0, 0, 0,   0,   2, 127, - 1, 0 },
  // dummy (replicate row index 191)
  { 0, 0, 0,   0,   2, 127, - 1, 0 },

#elif WARPEDPIXEL_PREC_BITS == 5
  // [-1, 0)
  {0,   0, 127,   1,   0, 0, 0, 0}, {1,  -3, 127,   4,  -1, 0, 0, 0},
  {1,  -5, 126,   8,  -3, 1, 0, 0}, {1,  -7, 124,  13,  -4, 1, 0, 0},
  {2,  -9, 122,  18,  -6, 1, 0, 0}, {2, -11, 120,  22,  -7, 2, 0, 0},
  {3, -13, 117,  27,  -8, 2, 0, 0}, {3, -14, 114,  32, -10, 3, 0, 0},
  {3, -15, 111,  37, -11, 3, 0, 0}, {3, -16, 108,  42, -12, 3, 0, 0},
  {4, -17, 104,  47, -13, 3, 0, 0}, {4, -17, 100,  52, -14, 3, 0, 0},
  {4, -18,  96,  58, -15, 3, 0, 0}, {4, -18,  91,  63, -16, 4, 0, 0},
  {4, -18,  87,  68, -17, 4, 0, 0}, {4, -18,  82,  73, -17, 4, 0, 0},
  {4, -18,  78,  78, -18, 4, 0, 0}, {4, -17,  73,  82, -18, 4, 0, 0},
  {4, -17,  68,  87, -18, 4, 0, 0}, {4, -16,  63,  91, -18, 4, 0, 0},
  {3, -15,  58,  96, -18, 4, 0, 0}, {3, -14,  52, 100, -17, 4, 0, 0},
  {3, -13,  47, 104, -17, 4, 0, 0}, {3, -12,  42, 108, -16, 3, 0, 0},
  {3, -11,  37, 111, -15, 3, 0, 0}, {3, -10,  32, 114, -14, 3, 0, 0},
  {2,  -8,  27, 117, -13, 3, 0, 0}, {2,  -7,  22, 120, -11, 2, 0, 0},
  {1,  -6,  18, 122,  -9, 2, 0, 0}, {1,  -4,  13, 124,  -7, 1, 0, 0},
  {1,  -3,   8, 126,  -5, 1, 0, 0}, {0,  -1,   4, 127,  -3, 1, 0, 0},
  // [0, 1)
  { 0,  0,   0, 127,   1,   0,   0,  0}, { 0,  1,  -3, 127,   4,  -2,   1,  0},
  { 0,  2,  -6, 126,   8,  -3,   1,  0}, {-1,  3,  -8, 125,  13,  -5,   2, -1},
  {-1,  4, -11, 123,  18,  -7,   3, -1}, {-1,  4, -13, 121,  23,  -8,   3, -1},
  {-1,  5, -15, 119,  27, -10,   4, -1}, {-2,  6, -17, 116,  33, -12,   5, -1},
  {-2,  6, -18, 113,  38, -13,   5, -1}, {-2,  7, -19, 110,  43, -15,   6, -2},
  {-2,  7, -20, 106,  49, -16,   6, -2}, {-2,  7, -21, 102,  54, -17,   7, -2},
  {-2,  8, -22,  98,  59, -18,   7, -2}, {-2,  8, -22,  94,  64, -19,   7, -2},
  {-2,  8, -22,  89,  69, -20,   8, -2}, {-2,  8, -21,  84,  74, -21,   8, -2},
  {-2,  8, -21,  79,  79, -21,   8, -2}, {-2,  8, -21,  74,  84, -21,   8, -2},
  {-2,  8, -20,  69,  89, -22,   8, -2}, {-2,  7, -19,  64,  94, -22,   8, -2},
  {-2,  7, -18,  59,  98, -22,   8, -2}, {-2,  7, -17,  54, 102, -21,   7, -2},
  {-2,  6, -16,  49, 106, -20,   7, -2}, {-2,  6, -15,  43, 110, -19,   7, -2},
  {-1,  5, -13,  38, 113, -18,   6, -2}, {-1,  5, -12,  33, 116, -17,   6, -2},
  {-1,  4, -10,  27, 119, -15,   5, -1}, {-1,  3,  -8,  23, 121, -13,   4, -1},
  {-1,  3,  -7,  18, 123, -11,   4, -1}, {-1,  2,  -5,  13, 125,  -8,   3, -1},
  { 0,  1,  -3,   8, 126,  -6,   2,  0}, { 0,  1,  -2,   4, 127,  -3,   1,  0},
  // [1, 2)
  {0, 0, 0,   1, 127,   0,   0, 0}, {0, 0, 1,  -3, 127,   4,  -1, 0},
  {0, 0, 1,  -5, 126,   8,  -3, 1}, {0, 0, 1,  -7, 124,  13,  -4, 1},
  {0, 0, 2,  -9, 122,  18,  -6, 1}, {0, 0, 2, -11, 120,  22,  -7, 2},
  {0, 0, 3, -13, 117,  27,  -8, 2}, {0, 0, 3, -14, 114,  32, -10, 3},
  {0, 0, 3, -15, 111,  37, -11, 3}, {0, 0, 3, -16, 108,  42, -12, 3},
  {0, 0, 4, -17, 104,  47, -13, 3}, {0, 0, 4, -17, 100,  52, -14, 3},
  {0, 0, 4, -18,  96,  58, -15, 3}, {0, 0, 4, -18,  91,  63, -16, 4},
  {0, 0, 4, -18,  87,  68, -17, 4}, {0, 0, 4, -18,  82,  73, -17, 4},
  {0, 0, 4, -18,  78,  78, -18, 4}, {0, 0, 4, -17,  73,  82, -18, 4},
  {0, 0, 4, -17,  68,  87, -18, 4}, {0, 0, 4, -16,  63,  91, -18, 4},
  {0, 0, 3, -15,  58,  96, -18, 4}, {0, 0, 3, -14,  52, 100, -17, 4},
  {0, 0, 3, -13,  47, 104, -17, 4}, {0, 0, 3, -12,  42, 108, -16, 3},
  {0, 0, 3, -11,  37, 111, -15, 3}, {0, 0, 3, -10,  32, 114, -14, 3},
  {0, 0, 2,  -8,  27, 117, -13, 3}, {0, 0, 2,  -7,  22, 120, -11, 2},
  {0, 0, 1,  -6,  18, 122,  -9, 2}, {0, 0, 1,  -4,  13, 124,  -7, 1},
  {0, 0, 1,  -3,   8, 126,  -5, 1}, {0, 0, 0,  -1,   4, 127,  -3, 1},
  // dummy (replicate row index 95)
  {0, 0, 0,  -1,   4, 127,  -3, 1},

#else
#error "Unsupported value of WARPEDPIXEL_PREC_BITS"
#endif  // WARPEDPIXEL_PREC_BITS
};

av1_warp_plane(plane, p_col, p_row, p_width, p_height, ref, ref_frm) {
#if VALIDATE_SPEC_INTER2
  int16 validate_spec_inter2_aux_tmp[4]
#endif
  ref_w = ((ref_width[ref_idx]+plane_subsampling_x[plane]) >> plane_subsampling_x[plane])
  ref_h = ((ref_height[ref_idx]+plane_subsampling_y[plane]) >> plane_subsampling_y[plane])

  alpha = warp_aux[0]
  beta = warp_aux[1]
  gamma = warp_aux[2]
  delta = warp_aux[3]

#if VALIDATE_WARP
  validate(2100)
  validate(plane)
  validate(p_row)
  validate(p_col)
  validate(p_width)
  validate(p_height)
  validate(ref)
  validate(ref_frm)
#endif

#if VALIDATE_SPEC_INTER2
  if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
      validate(81014)
      validate(warp_params[0])
      validate(warp_params[1])
      validate(warp_params[2])
      validate(warp_params[3])
      validate(warp_params[4])
      validate(warp_params[5])
      validate(alpha)
      validate(beta)
      validate(gamma)
      validate(delta)
      setup_shear_params(warp_params, validate_spec_inter2_aux_tmp)
  }
#endif

  for (i = p_row; i < p_row + p_height; i += 8) {
    for (j = p_col; j < p_col + p_width; j += 8) {
#if DECODE && COLLECT_STATS
      total_warp_preds += 1
#endif
      src_x = (j + 4) << plane_subsampling_x[plane]
      src_y = (i + 4) << plane_subsampling_y[plane]
      dst_x = warp_params[2] * src_x + warp_params[3] * src_y + warp_params[0]
      dst_y = warp_params[4] * src_x + warp_params[5] * src_y + warp_params[1]
      x4 = dst_x >> plane_subsampling_x[plane]
      y4 = dst_y >> plane_subsampling_y[plane]

      ix4 = x4 >> WARPEDMODEL_PREC_BITS
      sx4 = x4 & ((1 << WARPEDMODEL_PREC_BITS) - 1)
      iy4 = y4 >> WARPEDMODEL_PREC_BITS
      sy4 = y4 & ((1 << WARPEDMODEL_PREC_BITS) - 1)

      sx4 += alpha * (-4) + beta * (-4)
      sy4 += gamma * (-4) + delta * (-4)

      sx4 &= ~((1 << WARP_PARAM_REDUCE_BITS) - 1)
      sy4 &= ~((1 << WARP_PARAM_REDUCE_BITS) - 1)

      // Horizontal filter
      for (k = -7; k < 8; k++) {
        // Clamp to top/bottom edge of the frame
        iy = iy4 + k
        if (iy < 0)
          iy = 0
        else if (iy > ref_h - 1)
          iy = ref_h - 1

        sx = sx4 + beta * (k + 4)

        for (l = -4; l < 4; l++) {
          ix = ix4 + l - 3
          // At this point, sx = sx4 + alpha * l + beta * k
          offs = ROUND_POWER_OF_TWO(sx, WARPEDDIFF_PREC_BITS) +
                           WARPEDPIXEL_PREC_SHIFTS
          ASSERT(offs >= 0 && offs <= WARPEDPIXEL_PREC_SHIFTS * 3,
                "Overflow in calculation of 'offs' in warp filter")

          sum = 0
          for (m = 0; m < 8; m++) {
            // Clamp to left/right edge of the frame
            sample_x = ix + m
            if (sample_x < 0)
              sample_x = 0
            else if (sample_x > ref_w - 1)
              sample_x = ref_w - 1

            sum += framestore[ref_idx][plane][sample_x][iy] * warped_filter[offs][m]
#if VALIDATE_SPEC_INTER2
            if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
                validate(81012)
                validate(k)
                validate(l)
                validate(m)
                validate(offs)
                validate(warped_filter[ offs ][ m ])
                validate(framestore[ref_idx][plane][sample_x][iy])
                validate(sum)
            }
#endif
          }
          sum = ROUND_POWER_OF_TWO(sum, interp_round0)
          inter_temp[(k + 7)][(l + 4)] = sum
#if VALIDATE_SPEC_INTER2
          if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
              validate(81010)
              validate(k)
              validate(l)
              validate(sum)
          }
#endif
          sx += alpha
        }
      }

      // Vertical filter
      for (k = -4; k < Min(4, p_row + p_height - i - 4); k++) {
        sy = sy4 + delta * (k + 4)
        for (l = -4; l < Min(4, p_col + p_width - j - 4); l++) {
          offs = ROUND_POWER_OF_TWO(sy, WARPEDDIFF_PREC_BITS) +
                           WARPEDPIXEL_PREC_SHIFTS
          ASSERT(offs >= 0 && offs <= WARPEDPIXEL_PREC_SHIFTS * 3,
                "Overflow in calculation of 'offs' in warp filter")

          sum = 0
          for (m = 0; m < 8; m++) {
            sum += inter_temp[(k + m + 4)][(l + 4)] * warped_filter[offs][m]
#if VALIDATE_SPEC_INTER2
            if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
                validate(81013)
                validate(k)
                validate(l)
                validate(m)
                validate(offs)
                validate(warped_filter[ offs ][ m ])
                validate(inter_temp[(k + m + 4)][(l + 4)])
                validate(sum)
            }
#endif
          }
          px = ROUND_POWER_OF_TWO(sum, interp_round1)
          predSamples[ref][j - p_col + l + 4][i - p_row + k + 4] = px
#if VALIDATE_SPEC_INTER2
          if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
              validate(81011)
              validate(k)
              validate(l)
              validate(px)
          }
#endif
          sy += gamma
        }
      }
    }
  }

#if VALIDATE_WARP
  for(i=0;i<p_height;i++)
    for(j=0;j<p_width;j++)
      validate(predSamples[j][i])
#endif
}
