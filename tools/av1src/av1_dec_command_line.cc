/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "av1_dec_command_line.hh"

#include <cassert>
#include <cstring>

#include <iostream>
#include <limits>
#include <sstream>

#include <getopt.h>
#include <libgen.h>

DecCommandLine::DecCommandLine (int argc, char **argv)
    : app_name (basename (argv [0])),
      frame_limit (-1),
#if WRITE_YUV
      yuv_output_file ("out.yuv"),
#endif
      csv_file (nullptr),
      csv_header_only (false),
      csv_headerless (false),
      file_list (nullptr),
      check_streams (false),
      annex_b (true),
      max_num_oppoints(-1),
      operating_point (0),
      no_film_grain (false),
      output_stats (false),
      lst_anchor_frames (0),
      lst_tile_list_obus (0),
      frame_rate (1),
      verbose (false)
{
    parse (argc, argv);
}

void
DecCommandLine::parse (int argc, char **argv)
{
    enum index_t {
        IDX_ENUM_START = 256,
        CSV_HEADER_ONLY_IDX,
        CHECK_STREAMS_IDX,
        NOT_ANNEX_B_IDX,
        MAX_NUM_OPPOINTS_IDX,
        OPERATING_POINT_IDX,
        NO_FILM_GRAIN_IDX,
        OUTPUT_STATS_IDX,
        LARGE_SCALE_TILE_IDX,
        FRAME_RATE_IDX
    };

    static struct option long_options[] = {
        { "frame-limit",        required_argument, 0, 'f' },
#if WRITE_YUV
        { "output",             required_argument, 0, 'o' },
#endif
        { "csv-file",           required_argument, 0, 'c' },
        { "csv-header-only",    no_argument,       0, CSV_HEADER_ONLY_IDX },
        { "csv-headerless",     no_argument,       0, 'H' },

        { "file-list",          required_argument, 0, 'T' },

        { "check_streams",      no_argument,       0, CHECK_STREAMS_IDX },
        { "not-annexb",         no_argument,       0, NOT_ANNEX_B_IDX },
        { "max-num-oppoints",   no_argument,       0, MAX_NUM_OPPOINTS_IDX },
        { "oppoint",            required_argument, 0, OPERATING_POINT_IDX },
        { "no-film-grain",      no_argument,       0, NO_FILM_GRAIN_IDX },
        { "output-stats",       no_argument,       0, OUTPUT_STATS_IDX },
        { "large-scale-tile",   no_argument,       0, LARGE_SCALE_TILE_IDX },
        { "frame-rate",         required_argument, 0, FRAME_RATE_IDX },

        { "help",               no_argument,       0, 'h' },
        { "verbose",            no_argument,       0, 'v' },
        { NULL,                 0,                 0, 0 },
    };

    static const char *short_options =
        "-f:"
#if WRITE_YUV
        "o:d:"
#endif
        "c:H"
        "T:"
        "hv"
        ;

    while (optind < argc) {
        int option_index = 0;
        int c = getopt_long (argc, argv,
                             short_options, long_options, & option_index);
        if (c == -1)
            break;

        switch (c) {
        case 1:
            // Positional argument. Read it.
            inputs.push_back (argv [optind - 1]);
            break;

        case 'f':
            frame_limit = read_int_arg ("frame-limit", optarg,
                                        0, std::numeric_limits<int>::max ());
            break;

#if WRITE_YUV
        case 'o':
            yuv_output_file = optarg;
            break;
#endif

        case 'c':
            csv_file = optarg;
            break;

        case CSV_HEADER_ONLY_IDX:
            csv_header_only = true;
            break;

        case 'H':
            csv_headerless = true;
            break;

        case 'T':
            file_list = optarg;
            break;

        case CHECK_STREAMS_IDX:
            check_streams = true;
            break;

        case NOT_ANNEX_B_IDX:
            annex_b = false;
            break;

        case OPERATING_POINT_IDX:
            operating_point = read_int_arg ("oppoint", optarg, 0, 31);
            break;

        case MAX_NUM_OPPOINTS_IDX:
            max_num_oppoints = 0; // 0 means active
            break;

        case NO_FILM_GRAIN_IDX:
            no_film_grain = true;
            break;

        case OUTPUT_STATS_IDX:
            output_stats = true;
            break;

        case LARGE_SCALE_TILE_IDX:
            // --large-scale-tile is a little odd and expects 2
            // arguments (num anchor frames, num tile list obus).
            // We've told getopt that it takes none, so we eat two
            // manually and then update optind to match.
            //
            // Index of next elt is optind, so the arguments we need
            // to read are argv[optind] and argv[optind + 1].
            if (! (optind + 1 < argc))
                die ("Not enough arguments for --large-scale-tile.");

            read_int_arg ("num anchor frames", argv [optind + 0], 1, 1000);
            read_int_arg ("num tile list obus", argv [optind + 1], 1, 1000);
            optind += 2;
            break;

        case FRAME_RATE_IDX:
            frame_rate = read_int_arg ("frame-rate", optarg, 1, 1000);
            break;

        case '?':
            syntax (std::cerr);
            exit (1);
            break;

        case 'h':
            syntax (std::cout);
            exit (0);
            break;

        case 'v':
            verbose = true;
            break;

        default:
            // This shouldn't happen (it means we gave getopt an
            // argument and didn't handle it in the switch)
            assert (0);
        }
    }
}

void DecCommandLine::syntax (std::ostream &os) const {
    os << ("\n  Argon Design AV1 Coverage Tool build " AV1_VERSION "\n"
           "  Copyright (C) Argon Design 2016-2019\n\n"
           "    Syntax:\n")
       << "      " << app_name;

    static const char *short_args[] = {
        "[-v]",
        "[-f <frame-limit>]",
#if WRITE_YUV
        "[-o <output>]",
#endif
        "[other options]",
        "<input file> [<input file>...]",
        NULL
    };

    const unsigned arg_indent_len = 6 + strlen (app_name) + 1;
    const std::string arg_indent (arg_indent_len, ' ');
    unsigned hpos = arg_indent_len;

    for (unsigned i = 0; short_args [i]; ++ i) {
        unsigned w = strlen (short_args [i]);
        if (hpos + 1 + w >= 80) {
            os << '\n' << arg_indent;
            hpos = arg_indent_len;
        } else {
            os << ' ';
        }
        os << short_args [i];
        hpos += 1 + w;
    }
    os << "\n\n";

    os << ("    Options:\n"
           "      -h, --help\n\n"
           "        Display this message\n\n\n"
           "      -v, --verbose\n\n"
           "        Enable internal verbose messages\n\n\n"
           "      -f <num-frames>, --frame-limit <num-frames>\n\n"
           "        Maximum number of frames to be decoded\n"
           "        (default is to decode all frames)\n\n\n"
           "      --check-streams\n\n"
           "        Enable internal error checking.\n\n\n"
           "      --not-annexb\n\n"
           "        Don't use annex B.\n\n\n"
           "      --oppoint <N>\n\n"
           "        Operating point.\n\n\n"
           "      --max-num-oppoints <N>\n\n"
           "        Returns the maximum number of oppoints in this stream.\n\n\n"
           "      --no-film-grain\n\n"
           "        Force disable film grain support.\n\n\n"
           "      --output-stats\n\n"
           "        Output statistics.\n\n\n"
           "      --large-scale-tile <num-anchor-frames> <n-tile-list-obus>\n\n"
           "        Read stream as a large scale tile stream without metadata.\n\n\n"
           "      --frame-rate <rate>\n\n"
           "        Nominal frame rate (used if no timing info in bitstream). Defaults\n"
           "        to 1.\n\n\n"
#if WRITE_YUV
           "     YUV output options:\n"
           "      -o <filename>, --output <filename>\n\n"
           "        Specify YUV output filename (default is no output)\n\n\n"
#endif
           "     Input file options:\n"
           "      -T <filename>, --file-list <filename>\n\n"
           "        Specify a file with a list of input names\n\n\n"
           "     CSV options:\n"
           "      -c <filename>, --csv-file <filename>\n\n"
           "        Specify CSV output filename (default disabled)\n\n\n"
           "      --csv-header-only\n\n"
           "        Only output CSV header.\n\n\n"
           "      -H, --csv-headerless\n\n"
           "        Don't output CSV header.\n\n\n"
           );

}

void DecCommandLine::die (const char *msg) const {
    if (msg)
        std::cerr << msg << "\n\n";

    syntax (std::cerr);
    exit (1);
}

int
DecCommandLine::read_int_arg (const char *name,
                              const char *arg,
                              int         min_allowed,
                              int         max_allowed) const
{
    assert (name && arg);

    // We want to use strtol, but want to catch leading '-' signs and
    // disallow them if allow_negative is false (to avoid "hey, that's
    // too big" when given -1).
    while (isspace (* arg)) ++ arg;

    const char *problem = NULL;
    char *endptr;
    errno = 0;
    long num = strtol (arg, & endptr, 0);
    bool out_of_range = false;

    if (arg [0] == '\0' || *endptr != '\0') {
        problem = "not an integer";
    } else if (errno == ERANGE) {
        problem = "not in range";
        out_of_range = true;
    }

    if (problem) {
        std::cerr << "Failed to parse " << name << " argument: `"
                  << arg << "' is " << problem << ".";
        if (out_of_range) {
            std::cerr << " Valid range is [" << min_allowed
                      << ", " << max_allowed << "].";
        }
        std::cerr << "\n";
        die (NULL);
    }

    return static_cast<int> (num);
}
