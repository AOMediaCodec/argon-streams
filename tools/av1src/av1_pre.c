/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "av1_pre.h"

static inline int64 builtin_Max(int64 a, int64 b) {
    return a<b?b:a;
}
static inline int64 builtin_Abs(int64 a) {
    return a<0?-a:a;
}
static inline int builtin_Sign(int64 a) {
    return a<=0?a<0?-1:0:1;
}
static inline int64 builtin_Min(int64 a, int64 b) {
    return a<b?a:b;
}
static inline int64 builtin_CeilDiv(int64 a, int64 b) {
    return (a + b - 1)/b;
}
static inline int builtin_get_msb (uint64_t n) {
    assert (n != 0);
    return 63 - __builtin_clzll (n);
}

#define builtin_Clip3(x,y,z) builtin_Max(builtin_Min(y,z),x)
//#define builtin_Clip1Y(x) builtin_Clip3(0,255,x)
//#define builtin_Clip1C(x) builtin_Clip3(0,255,x)
static inline int builtin_Log2(int a) {
  int b=0;
  while ((1<<b)<a)
      b+=1;
  return b;
}
#define builtin_CeilLog2(a) builtin_Log2(a)

#if ENCODE

#define builtin_more_rbsp_trailing_data() encode_bitstream_more_rbsp_trailing_data(b)
#define builtin_needs_byte_extensions() encode_bitstream_needs_byte_extensions(b)
#define builtin_next_bits(n) encode_bitstream_next_bits(b,n)
#define builtin_count_data_decode(pos) encode_bitstream_count_data_decode(b,pos)
#define builtin_current_position() encode_bitstream_current_position(b)
#define builtin_set_pos(pos) encode_bitstream_set_pos(b,pos)
#define builtin_current_bitposition() encode_bitstream_current_bitposition(b)
#define builtin_more_rbsp_data() encode_bitstream_more_rbsp_data(b)
#define builtin_payload_extension_present(sp,ps) encode_bitstream_payload_extension_present(b,sp,ps)
#define builtin_payload_extension_size(sp,ps) encode_bitstream_payload_extension_size(b,sp,ps)
#define builtin_byte_aligned() encode_bitstream_byte_aligned(b)
#define builtin_addcarry() encode_bitstream_addcarry(b)
#define builtin_overwrite(loc,numbits,value) encode_bitstream_overwrite(b,loc,numbits,value)
#define builtin_overwritebit(loc,bit) encode_bitstream_overwritebit(b,loc,bit)
#define builtin_start_nest() encode_bitstream_start_nest(b)
#define builtin_copy_from_nest(from_lvl,pos,sz) encode_bitstream_copy_from_nest(b,from_lvl,pos,sz)
#define builtin_end_nest() encode_bitstream_end_nest(b)
#define builtin_copy_to_buffer(buf,pos,sz) encode_bitstream_copy_to_buffer(b,buf,pos,sz)
#define builtin_copy_from_buffer(buf,sz) encode_bitstream_copy_from_buffer(b,buf,sz)
#define builtin_unread_bits(n) encode_bitstream_unread_bits(b,n)

void saveChooseAll(const char *filename);
void loadChooseAll(const char *filename);

#define True 1

#else

#define builtin_more_rbsp_trailing_data() bitstream_more_rbsp_trailing_data(b)
#define builtin_needs_byte_extensions() bitstream_needs_byte_extensions(b)
#define builtin_next_bits(n) bitstream_next_bits(b,n)
#define builtin_count_data_decode(pos) bitstream_count_data_decode(b,pos)
#define builtin_current_position() bitstream_current_position(b)
#define builtin_current_bitposition() bitstream_current_bitposition(b)
#define builtin_more_rbsp_data() bitstream_more_rbsp_data(b)
#define builtin_payload_extension_present(sp,ps) bitstream_payload_extension_present(b,sp,ps)
#define builtin_payload_extension_size(sp,ps) bitstream_payload_extension_size(b,sp,ps)
#define builtin_byte_aligned() bitstream_byte_aligned(b)
#define builtin_set_pos(pos) bitstream_set_pos(b,pos)
#define builtin_set_bool_length(sz) bitstream_set_bool_length(b,sz)
#define builtin_unread_bits(n) bitstream_unread_bits(b,n)
#define builtin_peek_last_bytes(size,out) bitstream_peek_last_bytes(b,size,out)

#endif

#define builtin_profile(a,b) profile_func(a,b)
#define hevc_parse_assert(a,b,x) assert(x)
//#define hevc_parse_assert(a,b,x)
#define hevc_parse_RangeStep(a,b,x,low,high,step)
#define hevc_parse_Range(a,b,x,low,high)

void decode_log(int r,int off,int s,int v);
void validate(int x);
void video_output (TOPLEVEL_T *b,
                   int bit_depth, int w, int h, int pitch,
                   int srcX, int srcY,
                   int sw, int sh, int is_monochrome);
void profile_output(const char *profXmlFile, const char *profPyFile, const char *all_streams[], int all_streams_len, int outputAllCoveringStreams);
void profile_report(void);
long long profile_func(const char *str,long long val);
int make_hash(const char *p);

int debugMode=1;
