/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_SPEC_GM_PARAM 0

// Global-motion-only functions
is_global_mv_block(src_mi_row, src_mi_col, block, refIdx) {
  if (intra_only)
    return 0

  if (Min(block_size_wide_lookup[sb_sizes[src_mi_col][src_mi_row]], block_size_high_lookup[sb_sizes[src_mi_col][src_mi_row]]) < 8) {
    return 0
  } else {
    mode = y_modes[src_mi_col][src_mi_row]
  }

  type = gm_type[refIdx]

  return ((mode == GLOBALMV || mode == GLOBAL_GLOBALMV) && type > TRANSLATION)
}

is_nontrans_global_motion(allZeromv) {
  if (!allZeromv) return 0

  if (Min(mi_size_wide[sb_size], mi_size_high[sb_size]) < 2) return 0

  // Now check if all global motion is non translational
  for (ref = 0; ref < 1 + has_second_ref(); ref++) {
    if (gm_type[ref_frame[ref]] == TRANSLATION) return 0
  }
  return 1
}

set_default_warp_params(int32pointer params) {
  params[0] = 0
  params[1] = 0
  params[2] = 1 << WARPEDMODEL_PREC_BITS
  params[3] = 0
  params[4] = 0
  params[5] = 1 << WARPEDMODEL_PREC_BITS
}

#if ENCODE
pick_gm_params(ref, type) {
    // We'll start by checking whether we've overridden gm_params from a
    // configuration file. We can detect this because reading enc_gm_params will
    // give a value greater than -4096 (the minimum possible value).
    num_params = ((type == AFFINE) ? 6 :
                  ((type >= ROTZOOM) ? 4 :
                   ((type >= TRANSLATION ? 2 :
                     0))))
    num_overridden = 0

    for (idx = 0; idx < num_params; idx++) {
      absBits = GM_ABS_ALPHA_BITS
      precBits = GM_ALPHA_PREC_BITS
      if (idx < 2) {
        if (type == TRANSLATION) {
          absBits = GM_ABS_TRANS_ONLY_BITS - !allow_high_precision_mv
          precBits = GM_TRANS_ONLY_PREC_BITS - !allow_high_precision_mv
        } else {
          absBits = GM_ABS_TRANS_BITS
          precBits = GM_TRANS_PREC_BITS
        }
      }
      precDiff = WARPEDMODEL_PREC_BITS - precBits
      round = (idx % 3) == 2 ? (1 << WARPEDMODEL_PREC_BITS) : 0
      mx = (1 << absBits)

      gm_min = ((-mx) << precDiff) + round
      gm_max = (mx << precDiff) + round

      enc_gm_params  u(0)

      // Check that we either override everything or nothing. This early exit
      // means that in the normal case, we don't bother doing all the stuff
      // above for idx > 0.
      if (num_overridden) {
        CHECK (gm_min <= enc_gm_params,
               "After overriding %d of %d values of gm_params, we stopped.",
               num_overridden, num_params)
      }

      if (enc_gm_params < gm_min) {
        break
      }

      num_overridden++

      CHECK (enc_gm_params <= gm_max,
             "Overridden gm_params has value %d, not in range [%d, %d].",
             enc_gm_params, gm_min, gm_max)

      // Clip here to make sure we don't break something
      enc_gm_params = Clip3 (gm_min, gm_max, enc_gm_params)

      gm_params[ref][idx] = enc_gm_params
    }

    // If we've overridden all the parameters, we're done here.
    if (num_overridden == num_params) {
      return 0
    }

    // There's a special case we need to pay attention to:
    // When we signal ROTZOOM or AFFINE, but set all of the linear parameters
    // to zero, then we still use the warp filter but it effectively reduces
    // to a "normal" non-warped interpolation, just with a special coefficient set.
    // This is useful, as it means that the same pixel patterns will work for the
    // warp filter and the regular inter filter, when it comes to maximizing
    // or minimizing intermediate values.
    // The only caveat is that we *also* have to pick the translational part
    // very carefully
    specialTranslationMode = 0
    enc_gm_special_prob u(0)
    if (type >= ROTZOOM) {
      // Boost the chance of generating a couple of interesting cases:
      if (Choose(0, 99) < enc_gm_special_prob) {
        if (ChooseBit()) {
          // To hit the most extreme values in the warp filter, it's easiest
          // to go via global motion since we can control the resulting
          // model more easily than with warped motion.
          //
          // In this case, we *signal* ROTZOOM or AFFINE transformation types,
          // but we *use* a purely translational model. What that means is that we
          // explicitly signal the linear part of the model, but always set it
          // to /1 0\
          //    \0 1/
          // This way, we end up going into the warp filter, but with
          // {alpha, beta, gamma, delta} all set to 0. This causes the
          // filter to act just like the normal convolve filter, but
          // with the warp filter coefficients.
          // Thus the same pixel pattern which maximizes the regular convolve
          // filter will also maximize the warp filter with these settings.
          w = 0
          x = 0
          y = 0
          z = 0
          specialTranslationMode = 1
        } else {
          // All parameters take extreme values
          // This is useful for hitting the extreme cases in setup_shear_params()
          w = ChooseBit() ? GM_ALPHA_MAX : -GM_ALPHA_MAX
          x = ChooseBit() ? GM_ALPHA_MAX : -GM_ALPHA_MAX
          y = ChooseBit() ? GM_ALPHA_MAX : -GM_ALPHA_MAX
          z = ChooseBit() ? GM_ALPHA_MAX : -GM_ALPHA_MAX
        }
      } else {
        // In the normal case, choose each parameter independently,
        // but still with a bias toward extreme values and zero
        w = av1_choose_interesting(-GM_ALPHA_MAX, GM_ALPHA_MAX)
        x = av1_choose_interesting(-GM_ALPHA_MAX, GM_ALPHA_MAX)
        y = av1_choose_interesting(-GM_ALPHA_MAX, GM_ALPHA_MAX)
        z = av1_choose_interesting(-GM_ALPHA_MAX, GM_ALPHA_MAX)
      }
      // Select the linear part of the GM model based on the above values
      gm_params[ref][2] = w * GM_ALPHA_DECODE_FACTOR + (1 << WARPEDMODEL_PREC_BITS)
      gm_params[ref][3] = x * GM_ALPHA_DECODE_FACTOR
      if (type == AFFINE) {
        gm_params[ref][4] = y * GM_ALPHA_DECODE_FACTOR
        gm_params[ref][5] = z * GM_ALPHA_DECODE_FACTOR + (1 << WARPEDMODEL_PREC_BITS)
      } else {
        gm_params[ref][4] = -gm_params[ref][3]
        gm_params[ref][5] = gm_params[ref][2]
      }
    }

    if (type >= TRANSLATION) {
      // Select the translational part of the model
      transBits = (type == TRANSLATION)
                    ? GM_ABS_TRANS_ONLY_BITS - !allow_high_precision_mv
                    : GM_ABS_TRANS_BITS
      transPrecDiff = (type == TRANSLATION)
                            ? GM_TRANS_ONLY_PREC_DIFF + !allow_high_precision_mv
                            : GM_TRANS_PREC_DIFF
      transMax = 1 << transBits
      if (specialTranslationMode) {
        // In this case, we pick the translational part of the model so that
        // we get the most extreme filter set possible.
        // Interestingly, in contrast to the regular inter predictor, the
        // most extreme coefficient set is *not* when the fractional part
        // of the translation is 1/2 of a pixel (offs = 96 in the warp filter)!
        // Instead, it's when offs = 93 or 99, corresponding to a translation
        // slightly off from 1/2 of a pixel.
        bits = (GM_ABS_TRANS_BITS - GM_TRANS_PREC_BITS)
        shift = WARPEDMODEL_PREC_BITS
        min = -(1<<bits)
        max = (1<<bits)-1 // Because we're going to add a little extra, reduce the max by 1
        // For each translation, pick a random pixel offset, then add half a pixel,
        // and finally add +/- 3/64 of a pixel to that. This will cause 'offs' in the
        // warp filter to be 93 or 99, as desired.
        offset_x = ChooseBit() ? -3 : 3
        gm_params[ref][0] = (Choose(min, max) << shift) + (1 << (shift-1)) + (offset_x << WARPEDDIFF_PREC_BITS)
        offset_y = ChooseBit() ? -3 : 3
        gm_params[ref][1] = (Choose(min, max) << shift) + (1 << (shift-1)) + (offset_y << WARPEDDIFF_PREC_BITS)
      } else {
        gm_params[ref][0] = Choose(-transMax, transMax) * (1 << transPrecDiff)
        gm_params[ref][1] = Choose(-transMax, transMax) * (1 << transPrecDiff)
      }
    }
}
#endif

read_global_motion() {
  // Delta encode the global motion parameters against those used by the previous frame,
  // unless this frame is marked as error-resilient. In that case, delta encode against
  // the default parameter set instead.
  if (primary_ref_frame == PRIMARY_REF_NONE) {
    for (ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++)
      set_default_warp_params(ref_gm_params[1][ref])
  }

  // Decode the global motion parameters
  for (ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++) {
    gm_bit_1 u(1)
    if (gm_bit_1) {
      gm_bit_2 u(1)
      if (gm_bit_2) {
        type = ROTZOOM
      } else {
        gm_bit_3 u(1)
        type = gm_bit_3 ? TRANSLATION : AFFINE
      }
    } else {
      type = IDENTITY
    }
    gm_type[ref] = type

#if ENCODE
    pick_gm_params(ref, type)
#endif

    if (type >= ROTZOOM) {
#if ENCODE
      encode_range_refsubexp_bits(-GM_ALPHA_MAX, GM_ALPHA_MAX + 1, SUBEXPFIN_K,
                             (prev_gm_params[ref][2] >> GM_ALPHA_PREC_DIFF) -
                               (1 << GM_ALPHA_PREC_BITS),
                             (gm_params[ref][2] >> GM_ALPHA_PREC_DIFF) -
                               (1 << GM_ALPHA_PREC_BITS))
      encode_range_refsubexp_bits(-GM_ALPHA_MAX, GM_ALPHA_MAX + 1, SUBEXPFIN_K,
                             prev_gm_params[ref][3] >> GM_ALPHA_PREC_DIFF,
                             gm_params[ref][3] >> GM_ALPHA_PREC_DIFF)
#else
      gm_params[ref][2] =
        decode_range_refsubexp_bits(-GM_ALPHA_MAX, GM_ALPHA_MAX + 1, SUBEXPFIN_K,
                               (prev_gm_params[ref][2] >> GM_ALPHA_PREC_DIFF) -
                                 (1 << GM_ALPHA_PREC_BITS)) *
                               GM_ALPHA_DECODE_FACTOR + (1 << WARPEDMODEL_PREC_BITS)
      gm_params[ref][3] =
        decode_range_refsubexp_bits(-GM_ALPHA_MAX, GM_ALPHA_MAX + 1, SUBEXPFIN_K,
                               prev_gm_params[ref][3] >> GM_ALPHA_PREC_DIFF) *
                               GM_ALPHA_DECODE_FACTOR
#endif
      if (type == AFFINE) {
#if ENCODE
        encode_range_refsubexp_bits(-GM_ALPHA_MAX, GM_ALPHA_MAX + 1, SUBEXPFIN_K,
                               prev_gm_params[ref][4] >> GM_ALPHA_PREC_DIFF,
                               gm_params[ref][4] >> GM_ALPHA_PREC_DIFF)
        encode_range_refsubexp_bits(-GM_ALPHA_MAX, GM_ALPHA_MAX + 1, SUBEXPFIN_K,
                               (prev_gm_params[ref][5] >> GM_ALPHA_PREC_DIFF) -
                                 (1 << GM_ALPHA_PREC_BITS),
                               (gm_params[ref][5] >> GM_ALPHA_PREC_DIFF) -
                                 (1 << GM_ALPHA_PREC_BITS))
#else
        gm_params[ref][4] =
          decode_range_refsubexp_bits(-GM_ALPHA_MAX, GM_ALPHA_MAX + 1, SUBEXPFIN_K,
                                 prev_gm_params[ref][4] >> GM_ALPHA_PREC_DIFF) *
                                 GM_ALPHA_DECODE_FACTOR
        gm_params[ref][5] =
          decode_range_refsubexp_bits(-GM_ALPHA_MAX, GM_ALPHA_MAX + 1, SUBEXPFIN_K,
                                 (prev_gm_params[ref][5] >> GM_ALPHA_PREC_DIFF) -
                                   (1 << GM_ALPHA_PREC_BITS)) *
                                 GM_ALPHA_DECODE_FACTOR + (1 << WARPEDMODEL_PREC_BITS)
#endif
      } else {
        gm_params[ref][4] = -gm_params[ref][3]
        gm_params[ref][5] = gm_params[ref][2]
      }

      // Calculate the warp filter parameters and check if the model is compatible
      // with the warp filter.
      gm_warpable[ref] = setup_shear_params(ref_gm_params[0][ref], gm_aux[ref])
    }

    if (type >= TRANSLATION) {
      transBits = (type == TRANSLATION)
                    ? GM_ABS_TRANS_ONLY_BITS - !allow_high_precision_mv
                    : GM_ABS_TRANS_BITS
      transPrecDiff = (type == TRANSLATION)
                            ? GM_TRANS_ONLY_PREC_DIFF + !allow_high_precision_mv
                            : GM_TRANS_PREC_DIFF
      transMax = 1 << transBits
#if ENCODE
      encode_range_refsubexp_bits(-transMax, transMax + 1, SUBEXPFIN_K,
                             prev_gm_params[ref][0] >> transPrecDiff,
                             gm_params[ref][0] >> transPrecDiff)
      encode_range_refsubexp_bits(-transMax, transMax + 1, SUBEXPFIN_K,
                             prev_gm_params[ref][1] >> transPrecDiff,
                             gm_params[ref][1] >> transPrecDiff)
#else
# if VALIDATE_SPEC_GM_PARAM
      validate(222000)
# endif
      gm_params[ref][0] =
        decode_range_refsubexp_bits(-transMax, transMax + 1, SUBEXPFIN_K,
                               prev_gm_params[ref][0] >> transPrecDiff) *
                               (1 << transPrecDiff)
      gm_params[ref][1] =
        decode_range_refsubexp_bits(-transMax, transMax + 1, SUBEXPFIN_K,
                               prev_gm_params[ref][1] >> transPrecDiff) *
                               (1 << transPrecDiff)
# if VALIDATE_SPEC_GM_PARAM
      validate(transPrecDiff)
      validate(prev_gm_params[ref][0]>> transPrecDiff)
      validate(gm_params[ref][0])
      validate(transPrecDiff)
      validate(prev_gm_params[ref][1]>> transPrecDiff)
      validate(gm_params[ref][1])
# endif
#endif
    }
  }
}

