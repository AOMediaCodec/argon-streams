/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <cstdint>
#include "arrays.h"

// Auto-generated file from build directory
#include "cfg_tables.h"

#include "av1_enc_command_line.hh"

// Make a symbol table for use in the config parser. This is used to
// translate between names (in the configuration files) and memory
// addresses (inside tl).
//
// This gets stored in a global variable and will be used by calls to
// read_config_file and builtin_get_cfg_syntax.
void setup_symbol_table (const TOPLEVEL_T &tl);

// Read a config file from the given filename. Returns true on
// success. Prints one or more error messages to stderr and returns
// false on error. This also seeds the random number generator used
// for evaluating config files.
//
// setup_symbol_table must be called before calling this function.
bool read_config_file (const char                          *filename,
                       const char                          *selected_config,
                       int                                  seed,
                       const EncCommandLine::elt_dbg_vec_t &debug);

bool builtin_get_cfg_syntax (unsigned idx,
                             flat_array<int64_t, 1> &out_array);

// Local Variables:
// mode: c++
// End:
