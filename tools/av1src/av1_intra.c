/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_SPEC_INTRA 0

#define VALIDATE_INTRA 0
#define DEBUG_BLOCK_MAP 0

// Perform intra prediction
// Looks like for sub 8x8 blocks we perform prediction in 4x4 blocks
//
// Extends above right unavailable data
// Sets left and above unavailable to default values
// Prediction may extend off the edge of the image, in which case we duplicate the edge pixels (image is treated as being in multiples of mi_cols and mi_rows)
// This means that all modes are valid
//
// Dc prediction computes rounded average of all available up and left pixels

uint16 orders_vert_128x128[1] = {
  0
};
uint16 orders_vert_64x64[4] = {
  0, 2, 1, 3,
};
uint16 orders_vert_32x32[16] = {
  0, 2,  4,  6,  1, 3,  5,  7,
  8, 10, 12, 14, 9, 11, 13, 15,
};
uint16 orders_vert_16x16[64] = {
  0,  2,  4,  6,  16, 18, 20, 22, 1,  3,  5,  7,  17, 19, 21, 23,
  8,  10, 12, 14, 24, 26, 28, 30, 9,  11, 13, 15, 25, 27, 29, 31,
  32, 34, 36, 38, 48, 50, 52, 54, 33, 35, 37, 39, 49, 51, 53, 55,
  40, 42, 44, 46, 56, 58, 60, 62, 41, 43, 45, 47, 57, 59, 61, 63
};

uint16 orders_vert_8x8[256] = {
  0,   2,   4,   6,   16,  18,  20,  22,  64,  66,  68,  70,  80,  82,  84,
  86,  1,   3,   5,   7,   17,  19,  21,  23,  65,  67,  69,  71,  81,  83,
  85,  87,  8,   10,  12,  14,  24,  26,  28,  30,  72,  74,  76,  78,  88,
  90,  92,  94,  9,   11,  13,  15,  25,  27,  29,  31,  73,  75,  77,  79,
  89,  91,  93,  95,  32,  34,  36,  38,  48,  50,  52,  54,  96,  98,  100,
  102, 112, 114, 116, 118, 33,  35,  37,  39,  49,  51,  53,  55,  97,  99,
  101, 103, 113, 115, 117, 119, 40,  42,  44,  46,  56,  58,  60,  62,  104,
  106, 108, 110, 120, 122, 124, 126, 41,  43,  45,  47,  57,  59,  61,  63,
  105, 107, 109, 111, 121, 123, 125, 127, 128, 130, 132, 134, 144, 146, 148,
  150, 192, 194, 196, 198, 208, 210, 212, 214, 129, 131, 133, 135, 145, 147,
  149, 151, 193, 195, 197, 199, 209, 211, 213, 215, 136, 138, 140, 142, 152,
  154, 156, 158, 200, 202, 204, 206, 216, 218, 220, 222, 137, 139, 141, 143,
  153, 155, 157, 159, 201, 203, 205, 207, 217, 219, 221, 223, 160, 162, 164,
  166, 176, 178, 180, 182, 224, 226, 228, 230, 240, 242, 244, 246, 161, 163,
  165, 167, 177, 179, 181, 183, 225, 227, 229, 231, 241, 243, 245, 247, 168,
  170, 172, 174, 184, 186, 188, 190, 232, 234, 236, 238, 248, 250, 252, 254,
  169, 171, 173, 175, 185, 187, 189, 191, 233, 235, 237, 239, 249, 251, 253,
  255,
};

uint16 orders_32x128[4] = {
  0, 1, 2, 3,
};
uint16 orders_128x32[4] = {
  0, 1, 2, 3,
};

uint16 orders_64x16[16] = {
  0, 4, 1, 5, 2, 6, 3, 7, 8, 12, 9, 13, 10, 14, 11, 15,
};
uint16 orders_16x64[16] = {
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,
};
uint16 orders_32x8[64] = {
  0,  4,  16, 20, 1,  5,  17, 21, 2,  6,  18, 22, 3,  7,  19, 23,
  8,  12, 24, 28, 9,  13, 25, 29, 10, 14, 26, 30, 11, 15, 27, 31,
  32, 36, 48, 52, 33, 37, 49, 53, 34, 38, 50, 54, 35, 39, 51, 55,
  40, 44, 56, 60, 41, 45, 57, 61, 42, 46, 58, 62, 43, 47, 59, 63,
};
uint16 orders_8x32[64] = {
  0,  1,  2,  3,  4,  5,  6,  7,  16, 17, 18, 19, 20, 21, 22, 23,
  8,  9,  10, 11, 12, 13, 14, 15, 24, 25, 26, 27, 28, 29, 30, 31,
  32, 33, 34, 35, 36, 37, 38, 39, 48, 49, 50, 51, 52, 53, 54, 55,
  40, 41, 42, 43, 44, 45, 46, 47, 56, 57, 58, 59, 60, 61, 62, 63,
};

uint16 orders_16x4[256] = {
  0,   4,   16,  20,  64,  68,  80,  84,  1,   5,   17,  21,  65,  69,  81,
  85,  2,   6,   18,  22,  66,  70,  82,  86,  3,   7,   19,  23,  67,  71,
  83,  87,  8,   12,  24,  28,  72,  76,  88,  92,  9,   13,  25,  29,  73,
  77,  89,  93,  10,  14,  26,  30,  74,  78,  90,  94,  11,  15,  27,  31,
  75,  79,  91,  95,  32,  36,  48,  52,  96,  100, 112, 116, 33,  37,  49,
  53,  97,  101, 113, 117, 34,  38,  50,  54,  98,  102, 114, 118, 35,  39,
  51,  55,  99,  103, 115, 119, 40,  44,  56,  60,  104, 108, 120, 124, 41,
  45,  57,  61,  105, 109, 121, 125, 42,  46,  58,  62,  106, 110, 122, 126,
  43,  47,  59,  63,  107, 111, 123, 127, 128, 132, 144, 148, 192, 196, 208,
  212, 129, 133, 145, 149, 193, 197, 209, 213, 130, 134, 146, 150, 194, 198,
  210, 214, 131, 135, 147, 151, 195, 199, 211, 215, 136, 140, 152, 156, 200,
  204, 216, 220, 137, 141, 153, 157, 201, 205, 217, 221, 138, 142, 154, 158,
  202, 206, 218, 222, 139, 143, 155, 159, 203, 207, 219, 223, 160, 164, 176,
  180, 224, 228, 240, 244, 161, 165, 177, 181, 225, 229, 241, 245, 162, 166,
  178, 182, 226, 230, 242, 246, 163, 167, 179, 183, 227, 231, 243, 247, 168,
  172, 184, 188, 232, 236, 248, 252, 169, 173, 185, 189, 233, 237, 249, 253,
  170, 174, 186, 190, 234, 238, 250, 254, 171, 175, 187, 191, 235, 239, 251,
  255,
};
uint16 orders_4x16[256] = {
  0,   1,   2,   3,   4,   5,   6,   7,   16,  17,  18,  19,  20,  21,  22,
  23,  64,  65,  66,  67,  68,  69,  70,  71,  80,  81,  82,  83,  84,  85,
  86,  87,  8,   9,   10,  11,  12,  13,  14,  15,  24,  25,  26,  27,  28,
  29,  30,  31,  72,  73,  74,  75,  76,  77,  78,  79,  88,  89,  90,  91,
  92,  93,  94,  95,  32,  33,  34,  35,  36,  37,  38,  39,  48,  49,  50,
  51,  52,  53,  54,  55,  96,  97,  98,  99,  100, 101, 102, 103, 112, 113,
  114, 115, 116, 117, 118, 119, 40,  41,  42,  43,  44,  45,  46,  47,  56,
  57,  58,  59,  60,  61,  62,  63,  104, 105, 106, 107, 108, 109, 110, 111,
  120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134,
  135, 144, 145, 146, 147, 148, 149, 150, 151, 192, 193, 194, 195, 196, 197,
  198, 199, 208, 209, 210, 211, 212, 213, 214, 215, 136, 137, 138, 139, 140,
  141, 142, 143, 152, 153, 154, 155, 156, 157, 158, 159, 200, 201, 202, 203,
  204, 205, 206, 207, 216, 217, 218, 219, 220, 221, 222, 223, 160, 161, 162,
  163, 164, 165, 166, 167, 176, 177, 178, 179, 180, 181, 182, 183, 224, 225,
  226, 227, 228, 229, 230, 231, 240, 241, 242, 243, 244, 245, 246, 247, 168,
  169, 170, 171, 172, 173, 174, 175, 184, 185, 186, 187, 188, 189, 190, 191,
  232, 233, 234, 235, 236, 237, 238, 239, 248, 249, 250, 251, 252, 253, 254,
  255,
};
uint16 orders_128x128[1] = { 0 };
uint16 orders_128x64[2] = { 0, 1 };
uint16 orders_64x128[2] = { 0, 1 };
uint16 orders_64x64[4] = {
  0, 1, 2, 3,
};
uint16 orders_64x32[8] = {
  0, 2, 1, 3, 4, 6, 5, 7,
};
uint16 orders_32x64[8] = {
  0, 1, 2, 3, 4, 5, 6, 7,
};
uint16 orders_32x32[16] = {
  0, 1, 4, 5, 2, 3, 6, 7, 8, 9, 12, 13, 10, 11, 14, 15,
};
uint16 orders_32x16[32] = {
  0,  2,  8,  10, 1,  3,  9,  11, 4,  6,  12, 14, 5,  7,  13, 15,
  16, 18, 24, 26, 17, 19, 25, 27, 20, 22, 28, 30, 21, 23, 29, 31,
};
uint16 orders_16x32[32] = {
  0,  1,  2,  3,  8,  9,  10, 11, 4,  5,  6,  7,  12, 13, 14, 15,
  16, 17, 18, 19, 24, 25, 26, 27, 20, 21, 22, 23, 28, 29, 30, 31,
};
uint16 orders_16x16[64] = {
  0,  1,  4,  5,  16, 17, 20, 21, 2,  3,  6,  7,  18, 19, 22, 23,
  8,  9,  12, 13, 24, 25, 28, 29, 10, 11, 14, 15, 26, 27, 30, 31,
  32, 33, 36, 37, 48, 49, 52, 53, 34, 35, 38, 39, 50, 51, 54, 55,
  40, 41, 44, 45, 56, 57, 60, 61, 42, 43, 46, 47, 58, 59, 62, 63,
};

uint16 orders_16x8[128] = {
  0,  2,  8,  10, 32,  34,  40,  42,  1,  3,  9,  11, 33,  35,  41,  43,
  4,  6,  12, 14, 36,  38,  44,  46,  5,  7,  13, 15, 37,  39,  45,  47,
  16, 18, 24, 26, 48,  50,  56,  58,  17, 19, 25, 27, 49,  51,  57,  59,
  20, 22, 28, 30, 52,  54,  60,  62,  21, 23, 29, 31, 53,  55,  61,  63,
  64, 66, 72, 74, 96,  98,  104, 106, 65, 67, 73, 75, 97,  99,  105, 107,
  68, 70, 76, 78, 100, 102, 108, 110, 69, 71, 77, 79, 101, 103, 109, 111,
  80, 82, 88, 90, 112, 114, 120, 122, 81, 83, 89, 91, 113, 115, 121, 123,
  84, 86, 92, 94, 116, 118, 124, 126, 85, 87, 93, 95, 117, 119, 125, 127,
};
uint16 orders_8x16[128] = {
  0,  1,  2,  3,  8,  9,  10, 11, 32,  33,  34,  35,  40,  41,  42,  43,
  4,  5,  6,  7,  12, 13, 14, 15, 36,  37,  38,  39,  44,  45,  46,  47,
  16, 17, 18, 19, 24, 25, 26, 27, 48,  49,  50,  51,  56,  57,  58,  59,
  20, 21, 22, 23, 28, 29, 30, 31, 52,  53,  54,  55,  60,  61,  62,  63,
  64, 65, 66, 67, 72, 73, 74, 75, 96,  97,  98,  99,  104, 105, 106, 107,
  68, 69, 70, 71, 76, 77, 78, 79, 100, 101, 102, 103, 108, 109, 110, 111,
  80, 81, 82, 83, 88, 89, 90, 91, 112, 113, 114, 115, 120, 121, 122, 123,
  84, 85, 86, 87, 92, 93, 94, 95, 116, 117, 118, 119, 124, 125, 126, 127,
};
uint16 orders_8x8[256] = {
  0,   1,   4,   5,   16,  17,  20,  21,  64,  65,  68,  69,  80,  81,  84,
  85,  2,   3,   6,   7,   18,  19,  22,  23,  66,  67,  70,  71,  82,  83,
  86,  87,  8,   9,   12,  13,  24,  25,  28,  29,  72,  73,  76,  77,  88,
  89,  92,  93,  10,  11,  14,  15,  26,  27,  30,  31,  74,  75,  78,  79,
  90,  91,  94,  95,  32,  33,  36,  37,  48,  49,  52,  53,  96,  97,  100,
  101, 112, 113, 116, 117, 34,  35,  38,  39,  50,  51,  54,  55,  98,  99,
  102, 103, 114, 115, 118, 119, 40,  41,  44,  45,  56,  57,  60,  61,  104,
  105, 108, 109, 120, 121, 124, 125, 42,  43,  46,  47,  58,  59,  62,  63,
  106, 107, 110, 111, 122, 123, 126, 127, 128, 129, 132, 133, 144, 145, 148,
  149, 192, 193, 196, 197, 208, 209, 212, 213, 130, 131, 134, 135, 146, 147,
  150, 151, 194, 195, 198, 199, 210, 211, 214, 215, 136, 137, 140, 141, 152,
  153, 156, 157, 200, 201, 204, 205, 216, 217, 220, 221, 138, 139, 142, 143,
  154, 155, 158, 159, 202, 203, 206, 207, 218, 219, 222, 223, 160, 161, 164,
  165, 176, 177, 180, 181, 224, 225, 228, 229, 240, 241, 244, 245, 162, 163,
  166, 167, 178, 179, 182, 183, 226, 227, 230, 231, 242, 243, 246, 247, 168,
  169, 172, 173, 184, 185, 188, 189, 232, 233, 236, 237, 248, 249, 252, 253,
  170, 171, 174, 175, 186, 187, 190, 191, 234, 235, 238, 239, 250, 251, 254,
  255,
};

uint16 orders_4x8[512] = {
  0,   1,   2,   3,   8,   9,   10,  11,  32,  33,  34,  35,  40,  41,  42,
  43,  128, 129, 130, 131, 136, 137, 138, 139, 160, 161, 162, 163, 168, 169,
  170, 171, 4,   5,   6,   7,   12,  13,  14,  15,  36,  37,  38,  39,  44,
  45,  46,  47,  132, 133, 134, 135, 140, 141, 142, 143, 164, 165, 166, 167,
  172, 173, 174, 175, 16,  17,  18,  19,  24,  25,  26,  27,  48,  49,  50,
  51,  56,  57,  58,  59,  144, 145, 146, 147, 152, 153, 154, 155, 176, 177,
  178, 179, 184, 185, 186, 187, 20,  21,  22,  23,  28,  29,  30,  31,  52,
  53,  54,  55,  60,  61,  62,  63,  148, 149, 150, 151, 156, 157, 158, 159,
  180, 181, 182, 183, 188, 189, 190, 191, 64,  65,  66,  67,  72,  73,  74,
  75,  96,  97,  98,  99,  104, 105, 106, 107, 192, 193, 194, 195, 200, 201,
  202, 203, 224, 225, 226, 227, 232, 233, 234, 235, 68,  69,  70,  71,  76,
  77,  78,  79,  100, 101, 102, 103, 108, 109, 110, 111, 196, 197, 198, 199,
  204, 205, 206, 207, 228, 229, 230, 231, 236, 237, 238, 239, 80,  81,  82,
  83,  88,  89,  90,  91,  112, 113, 114, 115, 120, 121, 122, 123, 208, 209,
  210, 211, 216, 217, 218, 219, 240, 241, 242, 243, 248, 249, 250, 251, 84,
  85,  86,  87,  92,  93,  94,  95,  116, 117, 118, 119, 124, 125, 126, 127,
  212, 213, 214, 215, 220, 221, 222, 223, 244, 245, 246, 247, 252, 253, 254,
  255, 256, 257, 258, 259, 264, 265, 266, 267, 288, 289, 290, 291, 296, 297,
  298, 299, 384, 385, 386, 387, 392, 393, 394, 395, 416, 417, 418, 419, 424,
  425, 426, 427, 260, 261, 262, 263, 268, 269, 270, 271, 292, 293, 294, 295,
  300, 301, 302, 303, 388, 389, 390, 391, 396, 397, 398, 399, 420, 421, 422,
  423, 428, 429, 430, 431, 272, 273, 274, 275, 280, 281, 282, 283, 304, 305,
  306, 307, 312, 313, 314, 315, 400, 401, 402, 403, 408, 409, 410, 411, 432,
  433, 434, 435, 440, 441, 442, 443, 276, 277, 278, 279, 284, 285, 286, 287,
  308, 309, 310, 311, 316, 317, 318, 319, 404, 405, 406, 407, 412, 413, 414,
  415, 436, 437, 438, 439, 444, 445, 446, 447, 320, 321, 322, 323, 328, 329,
  330, 331, 352, 353, 354, 355, 360, 361, 362, 363, 448, 449, 450, 451, 456,
  457, 458, 459, 480, 481, 482, 483, 488, 489, 490, 491, 324, 325, 326, 327,
  332, 333, 334, 335, 356, 357, 358, 359, 364, 365, 366, 367, 452, 453, 454,
  455, 460, 461, 462, 463, 484, 485, 486, 487, 492, 493, 494, 495, 336, 337,
  338, 339, 344, 345, 346, 347, 368, 369, 370, 371, 376, 377, 378, 379, 464,
  465, 466, 467, 472, 473, 474, 475, 496, 497, 498, 499, 504, 505, 506, 507,
  340, 341, 342, 343, 348, 349, 350, 351, 372, 373, 374, 375, 380, 381, 382,
  383, 468, 469, 470, 471, 476, 477, 478, 479, 500, 501, 502, 503, 508, 509,
  510, 511,
};

uint16 orders_8x4[512] = {
  0,   2,   8,   10,  32,  34,  40,  42,  128, 130, 136, 138, 160, 162, 168,
  170, 1,   3,   9,   11,  33,  35,  41,  43,  129, 131, 137, 139, 161, 163,
  169, 171, 4,   6,   12,  14,  36,  38,  44,  46,  132, 134, 140, 142, 164,
  166, 172, 174, 5,   7,   13,  15,  37,  39,  45,  47,  133, 135, 141, 143,
  165, 167, 173, 175, 16,  18,  24,  26,  48,  50,  56,  58,  144, 146, 152,
  154, 176, 178, 184, 186, 17,  19,  25,  27,  49,  51,  57,  59,  145, 147,
  153, 155, 177, 179, 185, 187, 20,  22,  28,  30,  52,  54,  60,  62,  148,
  150, 156, 158, 180, 182, 188, 190, 21,  23,  29,  31,  53,  55,  61,  63,
  149, 151, 157, 159, 181, 183, 189, 191, 64,  66,  72,  74,  96,  98,  104,
  106, 192, 194, 200, 202, 224, 226, 232, 234, 65,  67,  73,  75,  97,  99,
  105, 107, 193, 195, 201, 203, 225, 227, 233, 235, 68,  70,  76,  78,  100,
  102, 108, 110, 196, 198, 204, 206, 228, 230, 236, 238, 69,  71,  77,  79,
  101, 103, 109, 111, 197, 199, 205, 207, 229, 231, 237, 239, 80,  82,  88,
  90,  112, 114, 120, 122, 208, 210, 216, 218, 240, 242, 248, 250, 81,  83,
  89,  91,  113, 115, 121, 123, 209, 211, 217, 219, 241, 243, 249, 251, 84,
  86,  92,  94,  116, 118, 124, 126, 212, 214, 220, 222, 244, 246, 252, 254,
  85,  87,  93,  95,  117, 119, 125, 127, 213, 215, 221, 223, 245, 247, 253,
  255, 256, 258, 264, 266, 288, 290, 296, 298, 384, 386, 392, 394, 416, 418,
  424, 426, 257, 259, 265, 267, 289, 291, 297, 299, 385, 387, 393, 395, 417,
  419, 425, 427, 260, 262, 268, 270, 292, 294, 300, 302, 388, 390, 396, 398,
  420, 422, 428, 430, 261, 263, 269, 271, 293, 295, 301, 303, 389, 391, 397,
  399, 421, 423, 429, 431, 272, 274, 280, 282, 304, 306, 312, 314, 400, 402,
  408, 410, 432, 434, 440, 442, 273, 275, 281, 283, 305, 307, 313, 315, 401,
  403, 409, 411, 433, 435, 441, 443, 276, 278, 284, 286, 308, 310, 316, 318,
  404, 406, 412, 414, 436, 438, 444, 446, 277, 279, 285, 287, 309, 311, 317,
  319, 405, 407, 413, 415, 437, 439, 445, 447, 320, 322, 328, 330, 352, 354,
  360, 362, 448, 450, 456, 458, 480, 482, 488, 490, 321, 323, 329, 331, 353,
  355, 361, 363, 449, 451, 457, 459, 481, 483, 489, 491, 324, 326, 332, 334,
  356, 358, 364, 366, 452, 454, 460, 462, 484, 486, 492, 494, 325, 327, 333,
  335, 357, 359, 365, 367, 453, 455, 461, 463, 485, 487, 493, 495, 336, 338,
  344, 346, 368, 370, 376, 378, 464, 466, 472, 474, 496, 498, 504, 506, 337,
  339, 345, 347, 369, 371, 377, 379, 465, 467, 473, 475, 497, 499, 505, 507,
  340, 342, 348, 350, 372, 374, 380, 382, 468, 470, 476, 478, 500, 502, 508,
  510, 341, 343, 349, 351, 373, 375, 381, 383, 469, 471, 477, 479, 501, 503,
  509, 511,
};

uint16 orders_4x4[1024] = {
  0,    1,    4,    5,    16,   17,   20,   21,   64,   65,   68,   69,   80,
  81,   84,   85,   256,  257,  260,  261,  272,  273,  276,  277,  320,  321,
  324,  325,  336,  337,  340,  341,  2,    3,    6,    7,    18,   19,   22,
  23,   66,   67,   70,   71,   82,   83,   86,   87,   258,  259,  262,  263,
  274,  275,  278,  279,  322,  323,  326,  327,  338,  339,  342,  343,  8,
  9,    12,   13,   24,   25,   28,   29,   72,   73,   76,   77,   88,   89,
  92,   93,   264,  265,  268,  269,  280,  281,  284,  285,  328,  329,  332,
  333,  344,  345,  348,  349,  10,   11,   14,   15,   26,   27,   30,   31,
  74,   75,   78,   79,   90,   91,   94,   95,   266,  267,  270,  271,  282,
  283,  286,  287,  330,  331,  334,  335,  346,  347,  350,  351,  32,   33,
  36,   37,   48,   49,   52,   53,   96,   97,   100,  101,  112,  113,  116,
  117,  288,  289,  292,  293,  304,  305,  308,  309,  352,  353,  356,  357,
  368,  369,  372,  373,  34,   35,   38,   39,   50,   51,   54,   55,   98,
  99,   102,  103,  114,  115,  118,  119,  290,  291,  294,  295,  306,  307,
  310,  311,  354,  355,  358,  359,  370,  371,  374,  375,  40,   41,   44,
  45,   56,   57,   60,   61,   104,  105,  108,  109,  120,  121,  124,  125,
  296,  297,  300,  301,  312,  313,  316,  317,  360,  361,  364,  365,  376,
  377,  380,  381,  42,   43,   46,   47,   58,   59,   62,   63,   106,  107,
  110,  111,  122,  123,  126,  127,  298,  299,  302,  303,  314,  315,  318,
  319,  362,  363,  366,  367,  378,  379,  382,  383,  128,  129,  132,  133,
  144,  145,  148,  149,  192,  193,  196,  197,  208,  209,  212,  213,  384,
  385,  388,  389,  400,  401,  404,  405,  448,  449,  452,  453,  464,  465,
  468,  469,  130,  131,  134,  135,  146,  147,  150,  151,  194,  195,  198,
  199,  210,  211,  214,  215,  386,  387,  390,  391,  402,  403,  406,  407,
  450,  451,  454,  455,  466,  467,  470,  471,  136,  137,  140,  141,  152,
  153,  156,  157,  200,  201,  204,  205,  216,  217,  220,  221,  392,  393,
  396,  397,  408,  409,  412,  413,  456,  457,  460,  461,  472,  473,  476,
  477,  138,  139,  142,  143,  154,  155,  158,  159,  202,  203,  206,  207,
  218,  219,  222,  223,  394,  395,  398,  399,  410,  411,  414,  415,  458,
  459,  462,  463,  474,  475,  478,  479,  160,  161,  164,  165,  176,  177,
  180,  181,  224,  225,  228,  229,  240,  241,  244,  245,  416,  417,  420,
  421,  432,  433,  436,  437,  480,  481,  484,  485,  496,  497,  500,  501,
  162,  163,  166,  167,  178,  179,  182,  183,  226,  227,  230,  231,  242,
  243,  246,  247,  418,  419,  422,  423,  434,  435,  438,  439,  482,  483,
  486,  487,  498,  499,  502,  503,  168,  169,  172,  173,  184,  185,  188,
  189,  232,  233,  236,  237,  248,  249,  252,  253,  424,  425,  428,  429,
  440,  441,  444,  445,  488,  489,  492,  493,  504,  505,  508,  509,  170,
  171,  174,  175,  186,  187,  190,  191,  234,  235,  238,  239,  250,  251,
  254,  255,  426,  427,  430,  431,  442,  443,  446,  447,  490,  491,  494,
  495,  506,  507,  510,  511,  512,  513,  516,  517,  528,  529,  532,  533,
  576,  577,  580,  581,  592,  593,  596,  597,  768,  769,  772,  773,  784,
  785,  788,  789,  832,  833,  836,  837,  848,  849,  852,  853,  514,  515,
  518,  519,  530,  531,  534,  535,  578,  579,  582,  583,  594,  595,  598,
  599,  770,  771,  774,  775,  786,  787,  790,  791,  834,  835,  838,  839,
  850,  851,  854,  855,  520,  521,  524,  525,  536,  537,  540,  541,  584,
  585,  588,  589,  600,  601,  604,  605,  776,  777,  780,  781,  792,  793,
  796,  797,  840,  841,  844,  845,  856,  857,  860,  861,  522,  523,  526,
  527,  538,  539,  542,  543,  586,  587,  590,  591,  602,  603,  606,  607,
  778,  779,  782,  783,  794,  795,  798,  799,  842,  843,  846,  847,  858,
  859,  862,  863,  544,  545,  548,  549,  560,  561,  564,  565,  608,  609,
  612,  613,  624,  625,  628,  629,  800,  801,  804,  805,  816,  817,  820,
  821,  864,  865,  868,  869,  880,  881,  884,  885,  546,  547,  550,  551,
  562,  563,  566,  567,  610,  611,  614,  615,  626,  627,  630,  631,  802,
  803,  806,  807,  818,  819,  822,  823,  866,  867,  870,  871,  882,  883,
  886,  887,  552,  553,  556,  557,  568,  569,  572,  573,  616,  617,  620,
  621,  632,  633,  636,  637,  808,  809,  812,  813,  824,  825,  828,  829,
  872,  873,  876,  877,  888,  889,  892,  893,  554,  555,  558,  559,  570,
  571,  574,  575,  618,  619,  622,  623,  634,  635,  638,  639,  810,  811,
  814,  815,  826,  827,  830,  831,  874,  875,  878,  879,  890,  891,  894,
  895,  640,  641,  644,  645,  656,  657,  660,  661,  704,  705,  708,  709,
  720,  721,  724,  725,  896,  897,  900,  901,  912,  913,  916,  917,  960,
  961,  964,  965,  976,  977,  980,  981,  642,  643,  646,  647,  658,  659,
  662,  663,  706,  707,  710,  711,  722,  723,  726,  727,  898,  899,  902,
  903,  914,  915,  918,  919,  962,  963,  966,  967,  978,  979,  982,  983,
  648,  649,  652,  653,  664,  665,  668,  669,  712,  713,  716,  717,  728,
  729,  732,  733,  904,  905,  908,  909,  920,  921,  924,  925,  968,  969,
  972,  973,  984,  985,  988,  989,  650,  651,  654,  655,  666,  667,  670,
  671,  714,  715,  718,  719,  730,  731,  734,  735,  906,  907,  910,  911,
  922,  923,  926,  927,  970,  971,  974,  975,  986,  987,  990,  991,  672,
  673,  676,  677,  688,  689,  692,  693,  736,  737,  740,  741,  752,  753,
  756,  757,  928,  929,  932,  933,  944,  945,  948,  949,  992,  993,  996,
  997,  1008, 1009, 1012, 1013, 674,  675,  678,  679,  690,  691,  694,  695,
  738,  739,  742,  743,  754,  755,  758,  759,  930,  931,  934,  935,  946,
  947,  950,  951,  994,  995,  998,  999,  1010, 1011, 1014, 1015, 680,  681,
  684,  685,  696,  697,  700,  701,  744,  745,  748,  749,  760,  761,  764,
  765,  936,  937,  940,  941,  952,  953,  956,  957,  1000, 1001, 1004, 1005,
  1016, 1017, 1020, 1021, 682,  683,  686,  687,  698,  699,  702,  703,  746,
  747,  750,  751,  762,  763,  766,  767,  938,  939,  942,  943,  954,  955,
  958,  959,  1002, 1003, 1006, 1007, 1018, 1019, 1022, 1023,
};

uint16 get_order(bsize, partition, ind) {
  order = -1
  vertPartition = ((partition == PARTITION_VERT_A)|| (partition == PARTITION_VERT_B))
  if (bsize==BLOCK_4X4) { // 4X4
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_4x4[ind]
  } else if (bsize<=BLOCK_4X8) { // 4X8
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_4x8[ind]
  } else if (bsize<=BLOCK_8X4) { // 8X4
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_8x4[ind]
  } else if (bsize<=BLOCK_8X8) { // 8X8
    if (vertPartition) {
      order = orders_vert_8x8[ind]
    } else {
      order = orders_8x8[ind]
    }
  } else if (bsize==BLOCK_8X16) {
    order = orders_8x16[ind]
  } else if (bsize==BLOCK_16X8) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_16x8[ind]
  } else if (bsize==BLOCK_16X16) {
    if (vertPartition) {
      order = orders_vert_16x16[ind]
    } else {
      order = orders_16x16[ind]
    }
  } else if (bsize==BLOCK_16X32) {
    order = orders_16x32[ind]
  } else if (bsize==BLOCK_32X16) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_32x16[ind]
  } else if (bsize==BLOCK_32X32) {
    if (vertPartition) {
      order = orders_vert_32x32[ind]
    } else {
      order = orders_32x32[ind]
    }
  } else if (bsize==BLOCK_32X64) {
    order = orders_32x64[ind]
  } else if (bsize==BLOCK_64X32) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_64x32[ind]
  } else if (bsize==BLOCK_64X64) {
    if (vertPartition) {
      order = orders_vert_64x64[ind]
    } else {
      order = orders_64x64[ind]
    }
  } else if (bsize==BLOCK_64X128) {
    order = orders_64x128[ind]
  } else if (bsize==BLOCK_128X64) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_128x64[ind]
  } else if (bsize==BLOCK_128X128) {
    if (vertPartition) {
      order = orders_vert_128x128[ind]
    } else {
      order = orders_128x128[ind]
    }
  } else if (bsize==BLOCK_4X16) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_4x16[ind]
  } else if (bsize==BLOCK_16X4) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_16x4[ind]
  } else if (bsize==BLOCK_8X32) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_8x32[ind]
  } else if (bsize==BLOCK_32X8) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_32x8[ind]
  } else if (bsize==BLOCK_16X64) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_16x64[ind]
  } else if (bsize==BLOCK_64X16) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_64x16[ind]
  } else if (bsize==BLOCK_32X128) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_32x128[ind]
  } else if (bsize==BLOCK_128X32) {
    ASSERT(!vertPartition, "Shouldn't get this block size from a vert partition")
    order = orders_128x32[ind]
  } else {
    ASSERT(0,"Impossible bsize")
  }
  return order
}

// Warning, in reference code bsize actually contains sb_type!
uint1 av1_has_right(bsize, mi_row, mi_col, right_available, txsz, blocky, blockx, ss_x, ss_y, partition) {
  step = tx_size_wide_unit[txsz]
  x = blockx
  y = blocky

  if (!right_available)  // TODO is this correct for small blocks???  Seems odd
    return 0


  wl = mi_width_log2_lookup[bsize]
  w = Max((block_size_wide_lookup[bsize] >> tx_size_wide_log2[0]) >> ss_x, 1)
  if (y == 0) {
    hl = mi_height_log2_lookup[bsize]

    // If top-right is within the block, we have top-right because we decode in raster order
    if (x + step < w) return 1

    mirow2 = (mi_row & mi_mask) >> hl
    micol2 = (mi_col & mi_mask) >> wl

    if (mirow2 == 0) return 1 // If on the top we have the top-right

    if (((micol2 + 1) << wl) >= mib_size) return 0 // if we go off the edge of superblock we do not have the top-right

    myind = ((mirow2 + 0) << (mib_size_log2 - wl)) + micol2 + 0
    trind = ((mirow2 - 1) << (mib_size_log2 - wl)) + micol2 + 1
    // @todo comment that the orders numbers here are one up from the ref code?
    // Note: When ext-partition is enabled, the 'orders' arrays are encoded
    // assuming a 128x128 superblock. If using a 64x64 superblock, we can correct for
    // this assumption by doubling the effective block size.
    // For example, the scan order for 32x16 units within a 64x64 superblock
    // is identical to the scan order for 64x32 units within a 128x128 superblock.
    if (superblock_size == BLOCK_64X64) {
      ASSERT((bsize <= BLOCK_64X64) || (bsize >= BLOCK_4X16 && bsize <= BLOCK_64X16),
             "Block size for intra prediction is larger than the superblock size")
      bsize = double_block_size[bsize]
    }

   myorder = get_order(bsize, partition, myind)
   trorder = get_order(bsize, partition, trind)
#if DEBUG_BLOCK_MAP
    :C printf("myorder %d > trorder %d\n", myorder, trorder)
    :print("myorder %d > trorder %d" % (myorder, trorder))
#endif // DEBUG_BLOCK_MAP
    return myorder > trorder // Testing if outside the block, but already decoded in quad-tree for this superblock
  } else {
    // For subsequent raster lines just check we are not going beyond the right edge
    // We need a special case to deal with the way that 128x128 blocks are split
    // into 64x64 (luma pixel) sub units
    // The rule is that we scan in raster order within each 64x64 (luma) unit,
    // so the top-right is available iff it lies inside the same 64px wide column.
    // (with one exception, below)
    if (block_size_wide_lookup[bsize] > block_size_wide_lookup[BLOCK_64X64]) {
      // Extra-special case, for 128x128 blocks: The top-right transform
      // unit within the bottom-left 64x64 does in fact have a top-right.
      if (y == mi_size_high[BLOCK_64X64] >> ss_y &&
          x + step == mi_size_wide[BLOCK_64X64] >> ss_x) {
        return 1
      }

      unitW = mi_size_wide[BLOCK_64X64] >> ss_x
      colInUnit = x % unitW
      return colInUnit + step < unitW
    } else
      return x + step < w
  }
}

uint1 av1_has_bottom(bsize, mi_row, mi_col, bottom_available, txsz, blocky, blockx, ss_x, ss_y, partition) {
  step = tx_size_high_unit[txsz]
  x = blockx
  y = blocky

  if (!bottom_available)
    return 0

  if (mi_size_wide[bsize] > mi_size_wide[BLOCK_64X64] && x > 0) {
    unitW = mi_size_wide[BLOCK_64X64] >> ss_x
    colInUnit = x % unitW
    if (colInUnit == 0) {
      unitH = mi_size_high[BLOCK_64X64] >> ss_y
      rowInUnit = y % unitH
      return rowInUnit + step < Min(mi_size_high[bsize] >> ss_y, unitH)
    }
  }

  if (x != 0) {
    return 0
  } else {
    // TODO review this computation, seems different in VP10?  Returns 1 earlier in VP10 when using small transform blocks
    wl = mi_width_log2_lookup[bsize]
    hl = mi_height_log2_lookup[bsize]
    h = Max((block_size_high_lookup[bsize] >> tx_size_high_log2[0]) >> ss_y, 1)


    mirow2 = (mi_row & mi_mask) >> hl
    micol2 = (mi_col & mi_mask) >> wl
    if (micol2 == 0) {
      // We're at the left edge of a superblock, so the bottom-left is available iff
      // it lies entirely within the superblock to the left (as opposed to the superblock
      // below and to the left, which hasn't been decoded yet)
      bottom_of_left_ctx = (mirow2 << (hl + MI_SIZE_LOG2 - tx_size_wide_log2[0]) >> ss_y) + y + tx_size_high_unit[txsz]
      bottom_of_left_sb = mib_size << (MI_SIZE_LOG2 - tx_size_wide_log2[0]) >> ss_y
      return bottom_of_left_ctx < bottom_of_left_sb
    }

    if (y + step < h) return 1 // Needs to be before next line so small tx_sizes within a larger block work correctly

    if (((mirow2 + 1) << hl) >= mib_size) return 0


    myind = ((mirow2 + 0) << (mib_size_log2 - wl)) + micol2 + 0
    trind = ((mirow2 + 1) << (mib_size_log2 - wl)) + micol2 - 1

    // @todo ?? comment that the orders numbers here are one up from the ref code?
    // Note: When ext-partition is enabled, the 'orders' arrays are encoded
    // assuming a 128x128 superblock. If using a 64x64 superblock, we can correct for
    // this assumption by doubling the effective block size.
    // For example, the scan order for 32x16 units within a 64x64 superblock
    // is identical to the scan order for 64x32 units within a 128x128 superblock.
    if (superblock_size == BLOCK_64X64) {
      ASSERT((bsize <= BLOCK_64X64) || (bsize >= BLOCK_4X16 && bsize <= BLOCK_64X16),
             "Block size for intra prediction is larger than the superblock size")
      bsize = double_block_size[bsize]
    }

   myorder = get_order(bsize, partition, myind)
   trorder = get_order(bsize, partition, trind)
#if DEBUG_BLOCK_MAP
    :C printf("trorder %d < myorder %d\n", trorder, myorder)
    :print("trorder %d < myorder %d" % (trorder, myorder))
#endif // DEBUG_BLOCK_MAP
    return trorder < myorder
  }
}

scale_chroma_bsize(bsize, ss_x, ss_y) {
  if (bsize == BLOCK_4X4) {
    if (ss_x == 1 && ss_y == 1) {
      return BLOCK_8X8
    } else if (ss_x == 1) {
      return BLOCK_8X4
    } else if (ss_y == 1) {
      return BLOCK_4X8
    }
  } else if (bsize == BLOCK_4X8) {
    if (ss_x == 1 && ss_y == 1) {
      return BLOCK_8X8
    } else if (ss_x == 1) {
      return BLOCK_8X8
    } else if (ss_y == 1) {
      return BLOCK_4X8
    }
  } else if (bsize == BLOCK_8X4) {
    if (ss_x == 1 && ss_y == 1) {
      return BLOCK_8X8
    } else if (ss_x == 1) {
      return BLOCK_8X4
    } else if (ss_y == 1) {
      return BLOCK_8X8
    }
  } else if (bsize == BLOCK_4X16) {
    if (ss_x == 1 && ss_y == 1) {
      return BLOCK_8X16
    } else if (ss_x == 1) {
      return BLOCK_8X16
    } else if (ss_y == 1) {
      return BLOCK_4X16
    }
  } else if (bsize == BLOCK_16X4) {
    if (ss_x == 1 && ss_y == 1) {
      return BLOCK_16X8
    } else if (ss_x == 1) {
      return BLOCK_16X4
    } else if (ss_y == 1) {
      return BLOCK_16X8
    }
  }
  return bsize
}

// tx_sz is size of the transform block 4x4, 8x8, etc.
reset_block_decoded_map() {
#if DEBUG_BLOCK_MAP
  :C printf("reset_block_map\n")
  :print("reset_block_map")
#endif // DEBUG_BLOCK_MAP
  for (plane=0; plane<3; plane++) {
    for (i=0; i<(mi_rows*TXFM_PER_MI_ROW)*(mi_cols*TXFM_PER_MI_COL); i++) {
      block_decoded_map_data[plane][i] = 0
    }
  }
}

set_block_decoded(plane,x,y,bw,bh) {
  unitsX = bw >> tx_size_wide_log2[0]
  unitsY = bh >> tx_size_wide_log2[0]
  blockX = x / tx_size_wide[0]
  blockY = y / tx_size_high[0]
  maxBlockX = ((mi_cols*MI_SIZE)>>plane_subsampling_x[plane]) / tx_size_wide[0]
  maxBlockY = ((mi_rows*MI_SIZE)>>plane_subsampling_y[plane]) / tx_size_high[0]
#if DEBUG_BLOCK_MAP
  :C printf("set - plane %d, bw %d, bh %d, unitsX %d, unitsY %d, blockX %d, blockY %d, maxBlockX %d, maxBlockY %d\n", plane, bw, bh, unitsX, unitsY, blockX, blockY, maxBlockX, maxBlockY)
  :print("set - plane %d, bw %d, bh %d, unitsX %d, unitsY %d, blockX %d, blockY %d, maxBlockX %d, maxBlockY %d" % (plane, bw, bh, unitsX, unitsY, blockX, blockY, maxBlockX, maxBlockY))
#endif // DEBUG_BLOCK_MAP
  for (i=0; i<unitsX; i++) {
    for (j=0; j<unitsY; j++) {
      if (((blockX+i) < maxBlockX) &&
          ((blockY+j) < maxBlockY)) {
        block_decoded_map[plane][blockX + i][blockY + j] = 1
#if DEBUG_BLOCK_MAP
        :C printf("setting pos plane %d, %d,%d - %d\n", plane, blockX+i, blockY+j, ((((blockY + j) * b->video_stream.frame_data.mi_cols)) + (blockX + i)))
        :print("setting pos plane %d, %d,%d - %d" % (plane, blockX+i, blockY+j, ((((blockY + j) * b.video_stream.frame_data.mi_cols)) + (blockX + i))))
      } else {
        :C printf("not setting pos plane %d %d,%d\n", plane, blockX+i, blockY+j)
        :print("not setting pos plane %d %d,%d" % (plane, blockX+i, blockY+j))
        :aaa=0
#endif // DEBUG_BLOCK_MAP
      }
    }
  }
}

uint1 block_decoded_top_right(have_top,plane,col_off,row_off,x,y,tx_sz,bsize,mi_row,mi_col) {
  if (!have_top) {
#if DEBUG_BLOCK_MAP
    :C printf("tr - no top\n")
    :print("tr - no top")
#endif // DEBUG_BLOCK_MAP
    return 0
  }


  unitsX = tx_size_wide_unit[tx_sz]
  unitsY = tx_size_high_unit[tx_sz]
  blockX = x / tx_size_wide[0]
  blockY = y / tx_size_high[0]
  maxX = ((mi_cols*MI_SIZE)>>plane_subsampling_x[plane])
  maxBlockX = maxX / tx_size_wide[0]
  xrChrOffset = 0
#if DEBUG_BLOCK_MAP
  :C printf("tr - plane %d, tx_sz %d, unitsX %d, unitsY %d, blockX %d, blockY %d, maxBlockX %d, xrChrOffset %d\n", plane, tx_sz, unitsX, unitsY, blockX, blockY, maxBlockX, xrChrOffset)
  :print("tr - plane %d, tx_sz %d, unitsX %d, unitsY %d, blockX %d, blockY %d, maxBlockX %d, xrChrOffset %d" % (plane, tx_sz, unitsX, unitsY, blockX, blockY, maxBlockX, xrChrOffset))
#endif // DEBUG_BLOCK_MAP

  if (x + tx_size_wide[tx_sz] + xrChrOffset >= maxX) { // right available
    //@todo?? perhaps this needs to look more like the reconintra.c code, acting on mi_* stuff?  understand that first...
#if DEBUG_BLOCK_MAP
    :C printf("tr - greater than max\n")
    :print("tr - greater than max")
#endif // DEBUG_BLOCK_MAP
    return 0
  }
  CHECK((blockY - 1 >= 0), "BlockY out of range")
  decoded = (blockX + unitsX < maxBlockX) ? 1 : 0
  for (i=0; i<unitsX*2; i++) {
    if (blockX + i < maxBlockX) { // extrapolation out of range
      decoded &= block_decoded_map[plane][blockX + i][blockY - 1]
    }
#if DEBUG_BLOCK_MAP
    :C printf("tr - i %d, idx %d, decoded %d\n", i, ((((blockY - ((int)1)) * b->video_stream.frame_data.mi_cols) * ((int)1)) + (blockX + i)), decoded)
    :print("tr - i %d, idx %d, decoded %d" % (i, ((((blockY - 1) * b.video_stream.frame_data.mi_cols) * (1)) + (blockX + i)), decoded))
#endif // DEBUG_BLOCK_MAP
  }
  return decoded
}

uint1 block_decoded_bottom_left(have_left,plane,col_off,x,y,tx_sz,bsize,mi_row,mi_col) {
  if (!have_left) {
#if DEBUG_BLOCK_MAP
    :C printf("bl - no left\n")
    :print("bl - no left")
#endif // DEBUG_BLOCK_MAP
    return 0
  }

  unitsX = tx_size_wide_unit[tx_sz]
  unitsY = tx_size_high_unit[tx_sz]
  blockX = x / tx_size_wide[0]
  blockY = y / tx_size_high[0]
  maxY = ((mi_rows*MI_SIZE)>>plane_subsampling_y[plane])
  maxBlockY = maxY / tx_size_high[0]
  ydChrOffset = 0
#if DEBUG_BLOCK_MAP
  :C printf("bl - plane %d, tx_sz %d, unitsX %d, unitsY %d, blockX %d, blockY %d, maxBlockY %d, ydChrOffset %d\n", plane, tx_sz, unitsX, unitsY, blockX, blockY, maxBlockY, ydChrOffset)
  :print("bl - plane %d, tx_sz %d, unitsX %d, unitsY %d, blockX %d, blockY %d, maxBlockY %d, ydChrOffset %d\n" % (plane, tx_sz, unitsX, unitsY, blockX, blockY, maxBlockY, ydChrOffset))
#endif // DEBUG_BLOCK_MAP

  if (y + tx_size_high[tx_sz] + ydChrOffset >= maxY) { // bottom available
#if DEBUG_BLOCK_MAP
    :C printf("bl - greater than max\n")
    :print("bl - greater than max")
#endif // DEBUG_BLOCK_MAP
    return 0
  }
  CHECK((blockX - 1 >= 0), "BlockX out of range")
  decoded = (blockY + unitsY < maxBlockY) ? 1 : 0
  for (j=0; j<unitsY*2; j++) {
    if (blockY + j < maxBlockY) { // extrapolation out of range
      decoded &= block_decoded_map[plane][blockX - 1][blockY + j]
    }
#if DEBUG_BLOCK_MAP
    :C printf("bl - j %d, idx %d, decoded %d\n", j, ((((blockY + j) * b->video_stream.frame_data.mi_cols) * ((int)1)) + (blockX - ((int)1))), decoded)
    :print("bl - j %d, idx %d, decoded %d\n" % (j, ((((blockY + j) * b.video_stream.frame_data.mi_cols) * (1)) + (blockX - (1))), decoded))
#endif // DEBUG_BLOCK_MAP
  }
  return decoded
}

// tx_sz is size of the transform block 0=4x4 1=8x8
// block_idx is in units of 4x4 blocks that have been transformed
// increments according to raster pattern
// TODO understand and check have logic
// x,y are in plane coordinates
predict_intra_block_helper(x, y, plane, tx_sz, mode, bsize,blocky,blockx,mi_row,mi_col, partition) {
  have_top = (blocky != 0) || (plane_subsampling_y[plane] ? chroma_aU : aU)
  have_left = (blockx != 0) || (plane_subsampling_x[plane] ? chroma_aL : aL)
  //:if recon_count==41:pdb.set_trace()
  xrChrOffset = 0
  ydChrOffset = 0
  // force 4x4 chroma component block size.
  yBsize = bsize
  bsize = scale_chroma_bsize(bsize, plane_subsampling_x[plane], plane_subsampling_y[plane])
  right_available = (x + tx_size_wide[tx_sz] + xrChrOffset) < ((mi_col_end*MI_SIZE)>>plane_subsampling_x[plane])
  have_right = av1_has_right(bsize,mi_row,mi_col,right_available,tx_sz,blocky,blockx,plane_subsampling_x[plane],plane_subsampling_y[plane], partition)
  bottom_available = (y + tx_size_high[tx_sz] + ydChrOffset) < ((mi_rows*MI_SIZE)>>plane_subsampling_y[plane]) // @todo all this can go too
  have_bottom = av1_has_bottom(bsize,mi_row,mi_col,bottom_available,tx_sz,blocky,blockx,plane_subsampling_x[plane],plane_subsampling_y[plane], partition)
#if DEBUG_BLOCK_MAP
  :C printf("decoding mi_col %d, mi_row %d, bsize %d, x %d, y %d, blockx %d, blocky %d, tx_sz %d\n", mi_col, mi_row, bsize, x, y, blockx, blocky, tx_sz)
  :print("decoding mi_col %d, mi_row %d, bsize %d, x %d, y %d, blockx %d, blocky %d, tx_sz %d\n" % (mi_col, mi_row, bsize, x, y, blockx, blocky, tx_sz))
#endif // DEBUG_BLOCK_MAP

      top_right_decoded = block_decoded_top_right(have_top,plane,blockx,blocky,x,y,tx_sz,bsize,mi_row,mi_col)
      ASSERT(top_right_decoded == (have_top && have_right), "have_right_mismatch")

  bottom_left_decoded = block_decoded_bottom_left(have_left,plane,blockx,x,y,tx_sz,bsize,mi_row,mi_col)
  ASSERT(bottom_left_decoded == (have_bottom && have_left), "have_bottom_mismatch")
  filterType = get_filter_type(mi_row, mi_col, plane)
  build_intra_predictors(x,y,plane,mode,tx_sz,have_top,have_left,have_right,have_bottom,xrChrOffset,ydChrOffset,blocky, filterType)
}


// pred_bsize = Size of block to predict
// bsize = Size of the containing luma block
predict_intra_block(x, y, plane, pred_bsize, mode, bsize, blocky, blockx, mi_row, mi_col, partition) {
  // Break the prediction block into sub-blocks according to the following rules:
  // * The sub-blocks must match some transform size (ie, not 32x64 or 64x32 or 64x64)
  // * If RECT_INTRA_PRED is disabled, the sub-blocks must be square
  // * For blocks with luma width/height greater than 64, we process
  //   64x64 (luma) units in raster order, and process transform units
  //   within each 64x64 unit in raster order.
  // * For smaller blocks, we just process transform units in raster order.
  tx_sz = max_txsize_rect_lookup[pred_bsize]
  bw = block_size_wide_lookup[pred_bsize]
  bh = block_size_high_lookup[pred_bsize]
  txw = tx_size_wide[tx_sz]
  txh = tx_size_high[tx_sz]
  stepr = 64 >> plane_subsampling_y[plane]
  stepc = 64 >> plane_subsampling_x[plane]
  for (i = 0; i < bh; i += txh)
    for (j = 0; j < bw; j += txw) {
      predict_intra_block_helper(x + j, y + i, plane, tx_sz, mode, bsize,
                                 blocky + (i >> tx_size_high_log2[0]),
                                 blockx + (j >> tx_size_wide_log2[0]), mi_row, mi_col,
                                 partition)
      set_block_decoded(plane, x + j, y + i, txw, txh)
    }
}

predict_intra_block_facade(x, y, plane, txSz, mode, bsize, blocky, blockx, mi_row, mi_col, partition) {
  if (mode == UV_CFL_PRED) {
    ASSERT(plane == 1 || plane == 2, "Can't use UV_CFL_PRED for planes other than U and V")
    // Use the normal intra predictor to calculate the DC part of
    // the CfL prediction
    predict_intra_block(x, y, plane, txsize_to_bsize[txSz], DC_PRED, bsize, blocky, blockx, mi_row, mi_col, partition)
    // Add in the AC part of the CfL prediction
    cfl_predict_ac(x, y, plane, txSz, bsize, mi_row, mi_col)
  }
  else
    predict_intra_block(x, y, plane, txsize_to_bsize[txSz], mode, bsize, blocky, blockx, mi_row, mi_col, partition)
}

build_intra_predictors(x,y,plane,mode,tx_sz,up_available,left_available,right_available,bottom_available,xrChrOffset,ydChrOffset,blocky,filterType) {
  uint_0_9 cross_mode
  cross_mode = mode
  maxX = ((mi_cols*MI_SIZE)>>plane_subsampling_x[plane])-1-xrChrOffset
  maxY = ((mi_rows*MI_SIZE)>>plane_subsampling_y[plane])-1-ydChrOffset
  txw = tx_size_wide[tx_sz]
  txh = tx_size_high[tx_sz]

#if VALIDATE_SPEC_INTRA
  validate(50005)
  validate(plane)
  validate(txw)
  validate(txh)
  validate(left_available?1:0)
  validate(up_available?1:0)
  if (left_available)
    validate(bottom_available?1:0)
  if (up_available)
    validate(right_available?1:0)
#endif
  // left
  left_px_needed = txw+txh
  if (left_available) {
    if (bottom_available) {
      for (i = 0; i < 2*txh; i++) {
        left_col[i] = frame[plane][x-1][Min(maxY,y+i)]
#if VALIDATE_SPEC_INTRA
        if ( i < left_px_needed ) {
            validate(51000)
            validate(i)
            validate(plane)
            validate(Min(maxY,y+i))
            validate(x-1)
            validate(left_col[i])
		}
#endif
      }
      for (;i<left_px_needed;i++) {
        left_col[i] = left_col[2*txh-1]
#if VALIDATE_SPEC_INTRA
        validate(51000)
        validate(i)
        validate(plane)
        validate(Min(maxY,y+2*txh-1))
        validate(x-1)
        validate(left_col[i])
#endif
      }
    } else {
      for (i = 0; i < txh; i++) {
        left_col[i] = frame[plane][x-1][Min(maxY,y+i)]
#if VALIDATE_SPEC_INTRA
        validate(51002)
        validate(i)
        validate(plane)
        validate(Min(maxY,y+i))
        validate(x-1)
        validate(left_col[i])
#endif
      }
      for (;i<left_px_needed;i++) {
        left_col[i] = left_col[txh-1]
#if VALIDATE_SPEC_INTRA
        validate(51002)
        validate(i)
        validate(plane)
        validate(Min(maxY,y+txh-1))
        validate(x-1)
        validate(left_col[i])
#endif
      }
    }
  } else {
    if (up_available) {
      for (i = 0; i < left_px_needed; i++) {
        left_col[i] = frame[plane][x][y-1]
#if VALIDATE_SPEC_INTRA
        validate(51004)
        validate(i)
        validate(left_col[i])
#endif
      }
    } else {
      for (i = 0; i < left_px_needed; i++) {
        left_col[i] = (1<<(bit_depth-1)) + 1
#if VALIDATE_SPEC_INTRA
        validate(51005)
        validate(i)
        validate(left_col[i])
#endif
      }
    }
  }


  // above
  above_px_needed = txw+txh
  if (up_available) {
    if (right_available) {
      for(i=0;i<txw*2;i++) {
        above_row[i] = frame[plane][Min(maxX,x+i)][y-1]
#if VALIDATE_SPEC_INTRA
        if (i<above_px_needed) {
          validate(51006)
          validate(i)
          validate(above_row[i])
		}
#endif
      }
      for (;i<above_px_needed;i++) {
        above_row[i] = above_row[txw*2-1]
#if VALIDATE_SPEC_INTRA
        validate(51006)
        validate(i)
        validate(above_row[i])
#endif
      }
    } else {
      for(i=0;i<txw;i++) {
        above_row[i] = frame[plane][Min(maxX,x+i)][y-1]
#if VALIDATE_SPEC_INTRA
        validate(51008)
        validate(i)
        validate(above_row[i])
#endif
      }
      for (;i<above_px_needed;i++) {
        above_row[i] = above_row[txw-1]
#if VALIDATE_SPEC_INTRA
        validate(51008)
        validate(i)
        validate(above_row[i])
#endif
      }
    }
  } else {
    if (left_available) {
      for(i=0;i<above_px_needed;i++) {
        above_row[i] = frame[plane][x-1][y]
#if VALIDATE_SPEC_INTRA
        validate(51010)
        validate(i)
        validate(above_row[i])
#endif
      }
    } else {
      for(i=0;i<above_px_needed;i++) {
        above_row[i] = (1<<(bit_depth-1)) - 1
#if VALIDATE_SPEC_INTRA
        validate(51011)
        validate(i)
        validate(above_row[i])
#endif
      }
    }
  }
  if (up_available && left_available) {
      above_row[-1] = frame[plane][x-1][y-1]
#if VALIDATE_SPEC_INTRA
      validate(51012)
      validate(above_row[-1])
#endif
  } else if (up_available) {
      above_row[-1] = frame[plane][x][y-1]
#if VALIDATE_SPEC_INTRA
      validate(51013)
      validate(above_row[-1])
#endif
  } else if (left_available) {
      above_row[-1] = frame[plane][x-1][y]
#if VALIDATE_SPEC_INTRA
      validate(51014)
      validate(above_row[-1])
#endif
  } else {
      above_row[-1] = 1<<(bit_depth-1)
#if VALIDATE_SPEC_INTRA
      validate(51015)
      validate(above_row[-1])
#endif
  }
  left_col[-1] = above_row[-1]
  :C log_intra_neighbours(b,txw,txh,mode)
  :log_intra_neighbours(b,txw,txh,mode)

#if DECODE && COLLECT_STATS
  if (plane == 0) {
    total_intra_px += txw * txh
  }
#endif

  // Original code does not use the above_right buffer for 45 and 63 angles and large blocks

  // predict
  useFilterIntraMode = (plane == 0) ? use_filter_intra : 0
  if (useFilterIntraMode == 1) {
    filter_intra_pred(tx_sz,x,y,plane)
  } else
  if ( is_directional_mode( mode, sb_size ) ) {
     angleDelta = (plane != PLANE_TYPE_Y_WITH_DC) ? AngleDeltaUV : AngleDeltaY
     pAngle = (mode_to_angle_map[mode] + angleDelta * ANGLE_STEP)
#if VALIDATE_SPEC_INTRA
     validate(50001)
     validate(angleDelta)
     validate(mode)
     validate(pAngle)
     validate(up_available?1:0)
     validate(left_available?1:0)
#endif

    upsampleAbove = 0
    upsampleLeft = 0
    if (enable_intra_edge_filter) {
      if (pAngle != 90 && pAngle != 180) {
        if ((pAngle > 90) && (pAngle < 180) && ((txw + txh) >= 24)) {
          filter_intra_edge_corner()
        }
        if (up_available) {
          strength = intra_edge_filter_strength(txw, txh, pAngle - 90, filterType)
          numPx = Min(txw, (maxX-x+1)) + (pAngle < 90 ? txh : 0) + 1
          filter_intra_edge(numPx, strength, 0 /*above*/)
        }
        if (left_available) {
          strength = intra_edge_filter_strength(txh, txw, pAngle - 180, filterType)
          numPx = Min(txh, (maxY-y+1)) + (pAngle > 180 ? txw : 0) + 1
          filter_intra_edge(numPx, strength, 1 /*left*/)
        }
      }
      upsampleAbove = use_intra_edge_upsample(txw, txh, pAngle - 90, filterType)
      if (upsampleAbove) {
        numPx = txw + (pAngle < 90 ? txh : 0)
        upsample_intra_edge(numPx, 0 /*above*/)
      }
      upsampleLeft = use_intra_edge_upsample(txh, txw, pAngle - 180, filterType)
      if (upsampleLeft) {
        numPx = txh + (pAngle > 180 ? txw : 0)
        upsample_intra_edge(numPx, 1 /*left*/)
      }
    }
     intra_dr_pred(tx_sz,x,y,plane,pAngle,upsampleAbove,upsampleLeft)
  } else
  if (mode == DC_PRED) {
    intra_dc_pred(left_available,up_available,txw,txh,x,y,plane)
  } else {
    intra_pred(mode,tx_sz,x,y,plane)
  }
#if VALIDATE_SPEC_INTRA
  validate(52000)
  for ( i = 0; i < txh; i++ ) {
    for ( j = 0; j < txw; j++ ) {
      validate(frame[plane][x+j][y+i])
    }
  }
#endif
#if VALIDATE_INTRA
  validate(1100)
  validate(mode)
  validate(tx_sz)
  validate(x)
  validate(y)
  validate(plane)
  for(i=-1;i<txw*2;i++)
      validate(above_row[i])
  for(i=0;i<txh;i++)
      validate(left_col[i])
  for(i=0;i<txh;i++)
    for(j=0;j<txw;j++)
      validate(frame[plane][x+j][y+i])
#endif
}

intra_dc_pred(left_available,up_available,txw,txh,x,y,plane) {
#if VALIDATE_SPEC_INTRA
  validate(50003)
#endif
  sum = 0
  count = 0
  if (left_available) {
    for (i = 0; i < txh; i++) {
      sum += left_col[i]
    }
    count += txh
  }
  if (up_available) {
    for (i = 0; i < txw; i++) {
      sum += above_row[i]
    }
    count += txw
  }
  sum = sum + (count >> 1)
  if (count==0)  {
    expected_dc = 1<<(bit_depth-1)
  } else {
    expected_dc = sum / count
  }
  for(i=0;i<txh;i++)
    for(j=0;j<txw;j++)
      frame[plane][x+j][y+i] = clip_pixel(expected_dc)
}

intra_pred(mode,tx_sz,x,y,plane) {
  txw = tx_size_wide[tx_sz]
  txh = tx_size_high[tx_sz]
  if (mode==V_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50004)
#endif
    for(i=0;i<txh;i++)
      for(j=0;j<txw;j++)
        frame[plane][x+j][y+i] = above_row[j] // (1-1) [RNG-Intra1]
  } else if (mode==H_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50004)
#endif
    for(i=0;i<txh;i++)
      for(j=0;j<txw;j++)
        frame[plane][x+j][y+i] = left_col[i] // (1-2) [RNG-Intra2]
  } else if (mode==D203_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50004)
#endif
    for (r = 0; r < txh; r++) {
      for (c = 0; c < txw; c++) {
        frame[plane][x+c][y+r] = c & 1 ? ROUND_POWER_OF_TWO(left_col[(c >> 1) + r] + left_col[(c >> 1) + r + 1] * 2 + left_col[(c >> 1) + r + 2], 2)
                                       : ROUND_POWER_OF_TWO(left_col[(c >> 1) + r] + left_col[(c >> 1) + r + 1], 1)
      }
    }
  } else if (mode==D45_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50004)
#endif
    for (r = 0; r < txh; r++)
      for (c = 0; c < txw; c++) {
        if (r + c + 2 < txw + txh)
          frame[plane][x+c][y+r] = ROUND_POWER_OF_TWO(above_row[r + c] + above_row[r + c + 1] * 2 + above_row[r + c + 2], 2) // (1-10) [RNG-Intra10]
        else
         frame[plane][x+c][y+r] = ROUND_POWER_OF_TWO(above_row[r + c] + above_row[r + c + 1] * 3, 2) // (1-11) [RNG-Intra11]
      }
  } else if (mode==D67_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50004)
#endif
    for (r = 0; r < txh; r++)
      for (c = 0; c < txw; c++) {
        if (r & 1)
          frame[plane][x+c][y+r] = ROUND_POWER_OF_TWO(above_row[r/2 + c] + above_row[r/2 + c + 1] * 2 + above_row[r/2 + c + 2], 2)  // (1-12) [RNG-Intra12]
        else
          frame[plane][x+c][y+r] = ROUND_POWER_OF_TWO(above_row[r/2 + c] + above_row[r/2 + c + 1], 1) // (1-13) [RNG-Intra13]
      }

  } else if (mode==D113_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50004)
#endif
    // first row
    for (c = 0; c < txw; c++)
      frame[plane][x+c][y] = ROUND_POWER_OF_TWO(above_row[c - 1] + above_row[c], 1) // (1-14) [RNG-Intra14]

    // second row
    frame[plane][x][y+1] = ROUND_POWER_OF_TWO(left_col[0] + above_row[-1] * 2 + above_row[0], 2) // (1-15) [RNG-Intra15]
    for (c = 1; c < txw; c++)
      frame[plane][x+c][y+1] = ROUND_POWER_OF_TWO(above_row[c - 2] + above_row[c - 1] * 2 + above_row[c], 2) // (1-16) [RNG-Intra16]

    // the rest of first col
    frame[plane][x][y+2] = ROUND_POWER_OF_TWO(above_row[-1] + left_col[0] * 2 + left_col[1], 2) // (1-17) [RNG-Intra17]
    for (r = 3; r < txh; r++)
      frame[plane][x][y+r] = ROUND_POWER_OF_TWO(left_col[r - 3] + left_col[r - 2] * 2 + left_col[r - 1], 2) // (1-18) [RNG-Intra18]

    // the rest of the block
    for (r = 2; r < txh; r++)
      for (c = 1; c < txw; c++)
        frame[plane][x+c][y+r] = frame[plane][x+c-1][y+r-2] // (1-19) [RNG-Intra19]

  } else if (mode==D135_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50004)
#endif
    frame[plane][x][y] = ROUND_POWER_OF_TWO(left_col[0] + above_row[-1] * 2 + above_row[0], 2) // (1-20) [RNG-Intra20]
    for (c = 1; c < txw; c++)
      frame[plane][x+c][y] = ROUND_POWER_OF_TWO(above_row[c - 2] + above_row[c - 1] * 2 + above_row[c], 2) // (1-21) [RNG-Intra21]

    frame[plane][x][y+1] = ROUND_POWER_OF_TWO(above_row[-1] + left_col[0] * 2 + left_col[1], 2) // (1-22) [RNG-Intra22]
    for (r = 2; r < txh; r++)
      frame[plane][x][y+r] = ROUND_POWER_OF_TWO(left_col[r - 2] + left_col[r - 1] * 2 + left_col[r], 2) // (1-23) [RNG-Intra23]

    for (r = 1; r < txh; r++)
      for (c = 1; c < txw; c++)
        frame[plane][x+c][y+r] = frame[plane][x+c-1][y+r-1] // (1-24) [RNG-Intra24]
  } else if (mode==D157_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50004)
#endif
    frame[plane][x][y] = ROUND_POWER_OF_TWO(above_row[-1] + left_col[0], 1) // (1-25) [RNG-Intra25]
    for (r = 1; r < txh; r++)
      frame[plane][x][y+r] = ROUND_POWER_OF_TWO(left_col[r - 1] + left_col[r], 1) // (1-26) [RNG-Intra26]

    frame[plane][x+1][y] = ROUND_POWER_OF_TWO(left_col[0] + above_row[-1] * 2 + above_row[0], 2) // (1-27) [RNG-Intra27]
    frame[plane][x+1][y+1] = ROUND_POWER_OF_TWO(above_row[-1] + left_col[0] * 2 + left_col[1], 2) // (1-28) [RNG-Intra28]
    for (r = 2; r < txh; r++)
      frame[plane][x+1][y+r] = ROUND_POWER_OF_TWO(left_col[r - 2] + left_col[r - 1] * 2 + left_col[r], 2) // (1-29) [RNG-Intra29]

    for (c = 0; c < txw - 2; c++)
      frame[plane][x+2+c][y] = ROUND_POWER_OF_TWO(above_row[c - 1] + above_row[c] * 2 + above_row[c + 1], 2) // (1-30) [RNG-Intra30]

    for (r = 1; r < txh; r++)
      for (c = 0; c < txw - 2; c++)
        frame[plane][x+2+c][y+r] = frame[plane][x+c][y+r-1] // (1-31) [RNG-Intra31]
  } else if (mode==PAETH_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50004)
#endif
    for(i=0;i<txh;i++) {
      for(j=0;j<txw;j++) {
        frame[plane][x+j][y+i] = paeth_predictor_single(left_col[i], above_row[j], above_row[-1])
     }
    }
  } else if (mode==SMOOTH_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50002)
#endif
    scale = 1 << SM_WEIGHT_LOG2_SCALE
    tx_r = txsize_vert_map[tx_sz]
    tx_c = txsize_horz_map[tx_sz]
    for(i=0;i<txh;i++) {
      for(j=0;j<txw;j++) {
        uint32 pred
        pred =  sm_weights[tx_r][i] * above_row[j]
        pred += (scale - sm_weights[tx_r][i]) * left_col[txh - 1]
        pred += sm_weights[tx_c][j] * left_col[i]
        pred += (scale - sm_weights[tx_c][j]) * above_row[txw - 1]
        frame[plane][x+j][y+i] = clip_pixel(divide_round(pred, SM_WEIGHT_LOG2_SCALE+1))
      }
    }
  } else if (mode==SMOOTH_V_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50002)
#endif
    tx_r = txsize_vert_map[tx_sz]
    scale = 1 << SM_WEIGHT_LOG2_SCALE
    for(i=0;i<txh;i++) {
      for(j=0;j<txw;j++) {
        pred = sm_weights[tx_r][i] * above_row[j]
        pred += (scale - sm_weights[tx_r][i]) * left_col[txh - 1]
        frame[plane][x+j][y+i] = clip_pixel(divide_round(pred, SM_WEIGHT_LOG2_SCALE))
      }
    }
  } else if (mode==SMOOTH_H_PRED) {
#if VALIDATE_SPEC_INTRA
    validate(50002)
#endif
    tx_c = txsize_horz_map[tx_sz]
    scale = 1 << SM_WEIGHT_LOG2_SCALE
    for(i=0;i<txh;i++) {
      for(j=0;j<txw;j++) {
        pred = sm_weights[tx_c][j] * left_col[i]
        pred += (scale - sm_weights[tx_c][j]) * above_row[txw - 1]
        frame[plane][x+j][y+i] = clip_pixel(divide_round(pred, SM_WEIGHT_LOG2_SCALE))
      }
    }
  } else {
    ASSERT(0,"Intra pred internal error")
  }
}

uint8 sm_weights[TX_SIZES][64] = {
  {
    // TX_4X4
    255, 149, 85, 64,
    // Unused
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  },
  {
    // TX_8X8
    255, 197, 146, 105, 73, 50, 37, 32,
    // Unused
                            0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  },
  {
    // TX_16X16
    255, 225, 196, 170, 145, 123, 102, 84, 68, 54, 43, 33, 26, 20, 17, 16,
    // Unused
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  },
  {
    // TX_32X32
    255, 240, 225, 210, 196, 182, 169, 157, 145, 133, 122, 111, 101, 92, 83, 74,
    66, 59, 52, 45, 39, 34, 29, 25, 21, 17, 14, 12, 10, 9, 8, 8,
    // Unused
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  },
  {
    // TX_64X64
    255, 248, 240, 233, 225, 218, 210, 203, 196, 189, 182, 176, 169, 163, 156,
    150, 144, 138, 133, 127, 121, 116, 111, 106, 101, 96, 91, 86, 82, 77, 73, 69,
    65, 61, 57, 54, 50, 47, 44, 41, 38, 35, 32, 29, 27, 25, 22, 20, 18, 16, 15,
    13, 12, 10, 9, 8, 7, 6, 6, 5, 5, 4, 4, 4,
  },
};

uint32 divide_round(uint32 value, bits) {
  return (value + (1 << (bits-1))) >> bits
}

int abs_diff(int x, int y) {
  return (x > y) ? (x - y) : (y - x)
}

uint16 paeth_predictor_single(uint16 left, uint16 top, uint16 topLeft) {
  base = top + left - topLeft
  pLeft = abs_diff(base, left)
  pTop = abs_diff(base, top)
  pTopLeft = abs_diff(base, topLeft)

  // Return nearest to base of left, top and top_left.
  return (pLeft <= pTop && pLeft <= pTopLeft)
    ? left
    : (pTop <= pTopLeft) ? top : topLeft
}

uint16 clip_pixel(int32 a) {
  return Clip3(0,(1<<bit_depth)-1,a)
}

intra_dr_pred(tx_sz,x,y,plane,pAngle,upsampleAbove,upsampleLeft) {
  txw = tx_size_wide[tx_sz]
  txh = tx_size_high[tx_sz]
  filterType = 0
  if ((pAngle > 0) && (pAngle < 90)) {
    dx = dr_intra_derivative[ pAngle ]
  } else if ((pAngle > 90) && (pAngle < 180)) {
    dx = dr_intra_derivative[ 180 - pAngle ]
  } else {
    dx = -1 // remove compile warning
  }
  if ((pAngle > 90) && (pAngle < 180)) {
    dy = dr_intra_derivative[ pAngle - 90 ]
  } else if ((pAngle > 180) && (pAngle < 270)) {
    dy = dr_intra_derivative[ 270 - pAngle ]
  } else {
    dy = -1 // remove compile warning
  }

#if VALIDATE_SPEC_INTRA
  validate(50006)
  validate(dx)
  validate(dy)
  validate(upsampleAbove?1:0)
  validate(upsampleLeft?1:0)
#endif

  if ((pAngle > 0) && (pAngle < 90)) {
    intra_dr_pred_z1(txw,txh,x,y,dx,plane,filterType,upsampleAbove)
  } else if ((pAngle > 90) && (pAngle < 180)) {
    intra_dr_pred_z2(txw,txh,x,y,dx,dy,plane,filterType,upsampleAbove,upsampleLeft)
  } else if ((pAngle > 180) && (pAngle < 270)) {
    intra_dr_pred_z3(txw,txh,x,y,dy,plane,filterType,upsampleLeft)
  } else if (pAngle == 90) {
#if VALIDATE_SPEC_INTRA
    validate(500013)
#endif
    for(i=0;i<txh;i++)
      for(j=0;j<txw;j++) {
        frame[plane][x+j][y+i] = above_row[j]
#if VALIDATE_SPEC_INTRA
        validate(frame[plane][x+j][y+i])
#endif
      }
  } else if (pAngle == 180) {
#if VALIDATE_SPEC_INTRA
    validate(500014)
#endif
    for(i=0;i<txh;i++)
      for(j=0;j<txw;j++) {
        frame[plane][x+j][y+i] = left_col[i]
#if VALIDATE_SPEC_INTRA
        validate(frame[plane][x+j][y+i])
#endif
      }
  }
}

intra_subpel_interp( base, shift, useAbove, refStartIdx, refEndIdx, filterType ) {
    val = (useAbove ? above_row[base] : left_col[base]) * (32 - shift)
        + (useAbove ? above_row[base+1] : left_col[base+1]) * shift
    val = ROUND_POWER_OF_TWO( val, 5 )
  return val
}

intra_dr_get_shift(i, upsample) {
    return ((i * (1 << upsample)) >> 1) & 0x1F
}

intra_dr_pred_z1(txw,txh,x,y,dx,plane,filterType,upsampleAbove) {
  CHECK(dx > 0, "Invalid dx")
#if VALIDATE_SPEC_INTRA
  validate(500010)
#endif
  maxBaseX = ((txw + txh) - 1) << upsampleAbove
  fracBits = 8 - 2 - upsampleAbove
  baseInc = 1 << upsampleAbove
  for ( r = 0; r < txh; r++ ) {
    i = ( r + 1 ) * dx
    base = i >> fracBits
    shift = intra_dr_get_shift(i, upsampleAbove)
    for ( c = 0; c < txw; c++, base += baseInc ) {
#if VALIDATE_SPEC_INTRA
      validate(base)
      validate(shift)
      validate(maxBaseX)
#endif
      if ( base < maxBaseX ) {
#if VALIDATE_SPEC_INTRA
        validate(1)
        validate(i)
        validate(above_row[ base ])
        validate(above_row[ base + 1 ])
#endif
        val = intra_subpel_interp( base, shift, 1 /*above_row*/, 0 , ((txw + txh) - 1), filterType )
        frame[plane][x+c][y+r] = clip_pixel( val )
#if VALIDATE_SPEC_INTRA
        validate(frame[plane][x+c][y+r])
#endif
      } else {
#if VALIDATE_SPEC_INTRA
        validate(2)
        validate(i)
#endif
        frame[plane][x+c][y+r] = above_row[ maxBaseX ]
#if VALIDATE_SPEC_INTRA
        validate(frame[plane][x+c][y+r])
#endif
      }
    }
  }
 }

intra_dr_pred_z2(txw,txh,x,y,dx,dy,plane,filterType,upsampleAbove,upsampleLeft) {
  CHECK(dy > 0, "Invalid dy")
  CHECK(dx > 0, "Invalid dx")
#if VALIDATE_SPEC_INTRA
  validate(500011)
#endif
  minBaseX = -(1 << upsampleAbove)
  fracBitsX = 8 - 2 - upsampleAbove
  fracBitsY = 8 - 2 - upsampleLeft
  for ( r = 0; r < txh; r++ ) {
    for ( c = 0; c < txw; c++ ) {
      i = ( c << 6 ) - ( r + 1 ) * dx
      baseX = i >> fracBitsX
      if ( baseX >= minBaseX ) {
        shift = intra_dr_get_shift(i, upsampleAbove)
#if VALIDATE_SPEC_INTRA
        validate(1)
        validate(i)
        validate(baseX)
        validate(shift)
        validate(above_row[ baseX ])
        validate(above_row[ baseX + 1 ])
#endif
        val = intra_subpel_interp( baseX, shift, 1 /*above_row*/, -1, txw - 1, filterType )
      } else {
        j = ( r << 6 ) - ( c + 1 ) * dy
        baseY = j >> fracBitsY
        ASSERT(baseY >= -(1<<upsampleLeft), "Invalid z2 baseY")
        shift = intra_dr_get_shift(j, upsampleLeft)
#if VALIDATE_SPEC_INTRA
        validate(2)
        validate(j)
        validate(baseY)
        validate(shift)
        validate(left_col[ baseY ])
        validate(left_col[ baseY + 1 ])
#endif
        val = intra_subpel_interp( baseY, shift, 0 /*left_col*/, -1, txh - 1, filterType )
      }
      frame[plane][x+c][y+r] = clip_pixel( val )
#if VALIDATE_SPEC_INTRA
      validate(frame[plane][x+c][y+r])
#endif
    }
  }
}

 intra_dr_pred_z3(txw,txh,x,y,dy,plane,filterType, upsampleLeft) {
  CHECK(dy > 0, "Invalid dy")
#if VALIDATE_SPEC_INTRA
  validate(500012)
#endif
  fracBits = 8 - 2 - upsampleLeft
  baseInc = 1 << upsampleLeft
  for ( c = 0; c < txw; c++ ) {
    j = ( c + 1 ) * dy
    base = j >> fracBits
    shift = intra_dr_get_shift(j, upsampleLeft)
    for ( r = 0; r < txh; r++, base += baseInc ) {
#if VALIDATE_SPEC_INTRA
      validate(j)
      validate(base)
      validate(shift)
#endif
      val = intra_subpel_interp( base, shift, 0 /*left_col*/, 0 , ((txw + txh) - 1), filterType )
      frame[plane][x+c][y+r] = clip_pixel( val )
#if VALIDATE_SPEC_INTRA
      validate(1)
      validate(left_col[ base ])
      validate(left_col[ base + 1 ])
      validate(frame[plane][x+c][y+r])
#endif
    }
  }
}

filter_intra_pred(tx_sz,x,y,plane) {
#if VALIDATE_SPEC_INTRA
  validate(50000)
#endif
  txw = tx_size_wide[tx_sz]
  txh = tx_size_high[tx_sz]
  ASSERT(txw <= 32 && txh <= 32, "invalid tx_size")

  // TODO this clearing might not be required?
  for ( r = 0; r < ( txh + 1 ); r++ ) {
    for ( c = 0; c < ( txw + 1 ); c++ ) {
      intraBuffer[ r ][ c ] = 0
    }
  }

  for ( r = 0; r < txh; r++ ) {
    intraBuffer[ r + 1 ][ 0 ] = left_col[ r ]
  }

  for ( c = 0; c < ( txw + 1 ); c++ ) {
    intraBuffer[ 0 ][ c ] = above_row[ c - 1 ]
  }

  for ( r = 1 ; r < ( txh + 1 ); r+=2 ) {
    for ( c = 1; c < ( txw + 1 ); c+=4 ) {
      p0 = intraBuffer[r - 1][c - 1]
      p1 = intraBuffer[r - 1][c]
      p2 = intraBuffer[r - 1][c + 1]
      p3 = intraBuffer[r - 1][c + 2]
      p4 = intraBuffer[r - 1][c + 3]
      p5 = intraBuffer[r][c - 1]
      p6 = intraBuffer[r + 1][c - 1]
      for (k = 0; k < 8; k++) {
        rOffset = k >> 2
        cOffset = k & 0x03
        ipred = filter_intra_taps_4x2procunit[filter_intra_mode][k][0] * p0 +
                filter_intra_taps_4x2procunit[filter_intra_mode][k][1] * p1 +
                filter_intra_taps_4x2procunit[filter_intra_mode][k][2] * p2 +
                filter_intra_taps_4x2procunit[filter_intra_mode][k][3] * p3 +
                filter_intra_taps_4x2procunit[filter_intra_mode][k][4] * p4 +
                filter_intra_taps_4x2procunit[filter_intra_mode][k][5] * p5 +
                filter_intra_taps_4x2procunit[filter_intra_mode][k][6] * p6
        ipred = ROUND_POWER_OF_TWO_SIGNED( ipred, FILTER_INTRA_SCALE_BITS )
        intraBuffer[ r + rOffset ][ c + cOffset ] = clip_pixel( ipred )
      }
    }
  }

  for ( r = 0; r < txh; r++ ) {
    for ( c = 0; c < txw; c++ ) {
      frame[plane][x+c][y+r] = intraBuffer[ r + 1 ][ c + 1 ]
    }
  }
}

uint1 is_smooth(row, col, plane) {
  if (plane == 0) {
    mode = y_modes[col][row]
  } else {
    // uv_mode is not set for inter blocks, so need to explicitly
    // detect that case.
    if (refframes[0][col][row][0] > INTRA_FRAME)
      return 0
    mode = uv_modes[col][row]
  }

  return (mode == SMOOTH_PRED || mode == SMOOTH_V_PRED || mode == SMOOTH_H_PRED)
}

uint1 get_filter_type(mi_row, mi_col, plane) {
  aboveSmooth = 0
  leftSmooth = 0

  ss_x = plane_subsampling_x[plane]
  ss_y = plane_subsampling_y[plane]

  upAvailable = (plane > 0) ? chroma_aU : aU
  leftAvailable = (plane > 0) ? chroma_aL : aL

  if (upAvailable) {
    // Need to locate the chroma information for the "above" block.
    // The various cases are:
    // If ss_x == 0, then col = mi_col
    // If (ss_x == 1) and (mi_col & 1), then col = mi_col
    // If (ss_x == 1) and !(mi_col & 1), then col = mi_col + 1
    // This is equivalent to col = mi_col | ss_y
    //
    // If ss_y == 0, then row = mi_row - 1
    // If (ss_y == 1) and (mi_row & 1), then row = mi_row - 2
    // If (ss_y == 1) and !(mi_row & 1), then row = mi_row - 1
    // This is equivalent to row = (mi_row & ~ss_y) - 1
    row = (mi_row & ~ss_y) - 1
    col = mi_col | ss_x
    aboveSmooth = is_smooth(row, col, plane)
  }
  if (leftAvailable) {
    // Need to locate the chroma information for the "left" block.
    // This works similarly to the "above" case, but with the
    // derivations for row and col switched.
    row = mi_row | ss_y
    col = (mi_col & ~ss_x) - 1
    leftSmooth = is_smooth(row, col, plane)
  }

  return (aboveSmooth || leftSmooth)
}

filter_intra_edge_corner() {
  s = left_col[0] * filter_intra_edge_corner_kernel[0]
    + above_row[-1] * filter_intra_edge_corner_kernel[1]
    + above_row[0] * filter_intra_edge_corner_kernel[2]
  s = (s + 8) >> 4
  above_row[-1] = s
  left_col[-1] = s
}

uint_0_3 intra_edge_filter_strength(bs0, bs1, delta, type) {
  d = Abs(delta)
  strength = 0
  blkWh = bs0 + bs1
  if (type == 0) {
    if (blkWh <= 8) {
      if (d >= 56) strength = 1
    } else if (blkWh <= 12) {
      if (d >= 40) strength = 1
    } else if (blkWh <= 16) {
      if (d >= 40) strength = 1
    } else if (blkWh <= 24) {
      if (d >= 8) strength = 1
      if (d >= 16) strength = 2
      if (d >= 32) strength = 3
    } else if (blkWh <= 32) {
      if (d >= 1) strength = 1
      if (d >= 4) strength = 2
      if (d >= 32) strength = 3
    } else {
      if (d >= 1) strength = 3
    }
  } else {
    if (blkWh <= 8) {
      if (d >= 40) strength = 1
      if (d >= 64) strength = 2
    } else if (blkWh <= 16) {
      if (d >= 20) strength = 1
      if (d >= 48) strength = 2
    } else if (blkWh <= 24) {
      if (d >= 4) strength = 3
    } else {
      if (d >= 1) strength = 3
    }
  }
  return strength
}

filter_intra_edge(sz, strength, uint1 left) {
  if (strength) {
    ASSERT(strength<=INTRA_EDGE_FILT, "intra edge strength too large")
    uint16 edge[129]
    for (i=0; i<sz; i++) {
      edge[i] = left ? left_col[i-1] : above_row[i-1]
    }
    for (i = 1; i < sz; i++) {
      s = 0
      for (j = 0; j < INTRA_EDGE_TAPS; j++) {
        k = i - 2 + j
        if (k < 0) {
          k = 0
        } else if (k > (sz - 1)) {
          k = (sz - 1)
        }
        s += edge[k] * intra_edge_kernel[strength-1][j]
#if VALIDATE_SPEC_INTRA
        validate(60005)
        validate(k)
        validate(intra_edge_kernel[ strength - 1 ][ j ])
        validate( left ? 1 : 0 )
        validate( edge[k] )
        validate(s)
#endif
      }
      s = (s + 8) >> 4
      if (left) {
        left_col[i-1] = s
#if VALIDATE_SPEC_INTRA
        validate(60003)
        validate(s)
#endif
      } else {
        above_row[i-1] = s
#if VALIDATE_SPEC_INTRA
        validate(60004)
        validate(s)
#endif
      }
    }
  }
}

uint1 use_intra_edge_upsample(bs0, bs1, delta, type) {
  d = Abs(delta)
  blkWh = bs0 + bs1
  if (d <= 0 || d >= 40) {
    return 0
  }
  return type ? (blkWh <= 8) : (blkWh <= 16)
}

upsample_intra_edge(sz, uint1 left) {
  // interpolate half-sample positions
  ASSERT(sz <= MAX_UPSAMPLE_SZ, "Invalid size")

  uint16 dup[MAX_UPSAMPLE_SZ + 3]
  // copy pix[-1..(sz-1)] and extend first and last samples
  dup[0] = left ? left_col[-1] : above_row[-1]
  dup[1] = left ? left_col[-1] : above_row[-1]
  for (i = 0; i < sz; i++) {
    dup[i + 2] = left ? left_col[i] : above_row[i]
  }
  dup[sz + 2] = left ? left_col[sz - 1] : above_row[sz - 1]

  // interpolate half-sample edge positions
  if (left) {
      left_col[-2] = dup[0]
    } else {
      above_row[-2] = dup[0]
    }
  for (i = 0; i < sz; i++) {
    s = -dup[i] + (9 * dup[i + 1]) + (9 * dup[i + 2]) - dup[i + 3]
    s = clip_pixel((s + 8) >> 4)
    if (left) {
      left_col[2 * i - 1] = s
      left_col[2 * i] = dup[i + 2]
    } else {
      above_row[2 * i - 1] = s
      above_row[2 * i] = dup[i + 2]
    }
  }
}

