/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/


#define VALIDATE_SPEC_ARITH 0


/*
The decoder is initialized by setting range = 255 and reading the
first 8 input bits into value. The decoder maintains range and calculates split in
exactly the same way as does the encoder.
*/
#define VALIDATE_DAALA_EC 0

#if ENCODE
// Given that we are at <node> in the tree with decoded bits in <val> and need to test at <bitpos>
// Descend the tree and store the bitpositions for any tokens found
enc_prepare_encodings(int8pointer tree,uint32pointer encodings,node,val,bitpos) {
  for(i=0;i<2;i++) {
    token = tree[node+i]
    x = val+(i<<bitpos)
    if (token<=0) {
      encodings[-token]=x
    } else {
      enc_prepare_encodings(tree,encodings,token,x,bitpos+1)
    }
  }
}
#endif // ENCODE

uint16 process_node(int8pointer tree, uint8pointer probs, uint16pointer ranges, uint8 idx, uint16 range)
{
  CHECK(range < (1<<(DAALA_PRECISION)), "Arithmetic error")

  if (tree[idx] <= 0) { // symbol found
    if (range == 0) { // all symbols must have some chance
      range = 1
    }
    ranges[-tree[idx]] = range
  } else { // continue down the tree
    range = tree_to_ranges(tree, probs, ranges, tree[idx], range)
  }
  return range
}

uint16 tree_to_ranges(int8pointer tree, uint8pointer probs, uint16pointer ranges, uint8 root, uint16 range) {
  CHECK(root % 2 == 0, "Invalid root") // left-right assumption requires even root

  prob = probs[root>>1]
  if (prob < 128) { // smallest probability first
    rL = (range * prob  + (1<<7)) >> 8
    rL = process_node(tree, probs, ranges, root, rL)

    rR = (rL > range) ? 0 : (range - rL)
    rR = process_node(tree, probs, ranges, root+1, rR)
  } else {
     rR = (range * (256 - prob) + (1<<7)) >> 8
     rR = process_node(tree, probs, ranges, root+1, rR)

     rL = (rR > range) ? 0 : (range - rR)
     rL = process_node(tree, probs, ranges, root, rL)
  }

  CHECK((rL+rR) >= range, "Tree to ranges arithmetic error")
  return rL + rR
}

uint8 compute_map(int8pointer tree, uint8pointer map, uint8 root, uint8 numSymbolsFound) {
  CHECK(root % 2 == 0, "Invalid root") // left-right assumption requires even root

  for (i=0; i<2; i++) {
    if (tree[root+i] <= 0) { // symbol found
      map[numSymbolsFound] = -tree[root+i]
      numSymbolsFound++
    } else { // continue down the tree
      numSymbolsFound = compute_map(tree, map, tree[root+i], numSymbolsFound)
    }
  }
  return numSymbolsFound
}

tree_to_cdf(int8pointer tree, uint8pointer probs, uint16pointer cdf, uint8 root, uint8pointer map, uint8pointer nsymbs) {
  uint16 ranges[MOST_SYMBOLS+1]
  range = tree_to_ranges(tree, probs, ranges, root, 1<<DAALA_PRECISION)
  CHECK(range == 1<<DAALA_PRECISION, "Tree to cdf error (range)")

  nsymbs[0] = compute_map(tree, map, root, 0)

  // re-order based on the map and sum
  sum = 0
  for (i=0; i<nsymbs[0]; i++) {
    cdf[i] = ranges[map[i]] + sum
    sum = cdf[i]
  }
  CHECK(sum == 1<<DAALA_PRECISION, "Invalid cdfs computed - sum")
  cdf[nsymbs[0]] = 0
}

update_cdf(uint16pointer cdf, uint_0_15 symbol, uint_2_16 nsymbs) {
  if (disable_cdf_update)
    return 0

  // @todo rename these to make it clearer
  rate = 3 + (cdf[nsymbs] > 15) + (cdf[nsymbs] > 31) + Min( get_unsigned_bits( nsymbs ) - 1, 2 )
#if VALIDATE_SPEC_ARITH
  validate(10003)
  validate(nsymbs)
  validate(cdf[nsymbs])
  validate(rate)
#endif
  tmp = 0
  for (i = 0; i < nsymbs - 1; i++) {
    tmp = (i == symbol) ? (1<<DAALA_PRECISION) : tmp
#if VALIDATE_SPEC_ARITH
    validate(10004)
    validate(tmp)
    validate(i)
    validate(cdf[ i ])
#endif
    if (tmp < cdf[i]) {
      cdf[i] -= ((cdf[i] - tmp) >> rate)
    } else {
      cdf[i] += ((tmp - cdf[i]) >> rate)
    }
#if VALIDATE_SPEC_ARITH
    validate(cdf[ i ])
#endif
  }
  cdf[nsymbs] += (cdf[nsymbs] < 32)
#if VALIDATE_SPEC_ARITH
  validate(10005)
  validate(cdf[nsymbs])
#endif
}

#if ENCODE
init_bool(size)
{
  Count = -DAALA_EXTRA_COUNT
  Offs = 0
  CurByte = 0
  NumCurBits = 0
  NumBytes = 0
  Low = 0
  Range = 1 << DAALA_PRECISION
}

exit_bool(padBytes)
{
  /*We output the minimum number of bits that ensures that the symbols encoded
     thus far will be decoded correctly regardless of the bits that follow.*/
  uint32 mask
  uint32 extra
  uint32 i
  CHECK(DAALA_PRECISION==15, "Check me") // Count=-9 and send=9 are tuned based on 15
  // Adjustments to make sure we inject a trailing '1' bit
  send = DAALA_EXTRA_COUNT + 1
  mask = (1 << (DAALA_PRECISION-1)) - 1
  extra = ((Low + mask) & ~mask) | (mask+1)
  send += Count
  if (send > 0) {
    n = (1 << (Count + 16)) - 1
    do {
      PreCarry[Offs] =  (extra >> (Count + 16)) & 0xFFFF
      Offs++
      CHECK(Offs<DAALA_MAX_SYMBOL_BYTES_PER_CHUNK, "DAALA_MAX_SYMBOL_BYTES_PER_CHUNK is too small")
      extra = extra & n
      send -= 8
      Count -= 8
      n = n >> 8
    } while (send > 0)
  }

  // Perform carry progration
  carry = 0
  for (j=Offs-1; j>=0; j--) {
    carry = PreCarry[j] + carry
    PostCarry[j] = (carry & 0xFF)
    carry = carry >> 8
  }

  // Output data
  for (i=0; i<Offs; i++) {
     PostCarry[i] u(8)
  }
  sent = Offs

  // Extra padding
  for (j=0; j<padBytes; j++) {
    padding u(8) //@todo remove tileByte from av1_profile.c?
    sent++
  }



#if VALIDATE_DAALA_EC
  validate(8007)
  validate(sent)
#endif
}

write_symbol_noadapt(uint16pointer cdf, int8 symbol, uint_2_16 nsymbs)
{
  CHECK(symbol>=0 && symbol < nsymbs && nsymbs <= 16, "Invalid symbol (%d) or nsymbs (%d)",symbol,nsymbs)
  CHECK(cdf[nsymbs-1] == 1<<DAALA_PRECISION, "Invalid cdf - top")
  CHECK(8+7==DAALA_PRECISION, "8+7==DAALA_PRECISION")
  fl = (1<<DAALA_PRECISION) - ((symbol > 0) ? cdf[symbol-1] : 0)
  fh = (1<<DAALA_PRECISION) - cdf[symbol]
  upper = ((Range >> 8) * (fh >> EC_PROB_SHIFT)) >> (7 - EC_PROB_SHIFT)
  upper += EC_MIN_PROB * (nsymbs - symbol - 1)
  if (fl < (1<<DAALA_PRECISION)) {
    lower = ((Range >> 8) * (fl >> EC_PROB_SHIFT)) >> (7 - EC_PROB_SHIFT)
    lower += EC_MIN_PROB * (nsymbs - symbol)
    Low += Range - lower
    newRange = lower - upper
  } else {
#if VALIDATE_DAALA_EC
    lower = Range
#endif // VALIDATE_DAALA_EC
    newRange = Range - upper
  }
  CHECK(newRange > 0, "Invalid cdf - zero range")
#if VALIDATE_DAALA_EC
  rangePre = Range
#endif

  bits = DAALA_PRECISION - ( get_unsigned_bits( newRange ) - 1 )
  send = Count + bits
  if (send >= 0) {
    Count += 16
    mask = (1 << Count) - 1
    if (send >= 8) {
      PreCarry[Offs] = ((Low >> Count) & 0xFFFF)
      Offs++
      CHECK(Offs<DAALA_MAX_SYMBOL_BYTES_PER_CHUNK, "DAALA_MAX_SYMBOL_BYTES_PER_CHUNK is too small")
      Low = Low & mask
      Count -= 8
      mask = mask >> 8
    }
    PreCarry[Offs] = ((Low >> Count) & 0xFFFF)
    Offs++
    CHECK(Offs<DAALA_MAX_SYMBOL_BYTES_PER_CHUNK, "DAALA_MAX_SYMBOL_BYTES_PER_CHUNK is too small")
    send = Count + bits - 24
    Low = Low & mask
  }
  Low = Low << bits
  Range = newRange << bits
  Count = send

#if VALIDATE_DAALA_EC
  validate(8001)
  validate(symbol)
  validate(lower)
  validate(upper)
  validate(rangePre)
  validate(bits)
#endif
}

write_symbol(uint16pointer cdf, int8 symbol, uint_2_16 nsymbs) {
  write_symbol_noadapt(cdf, symbol, nsymbs)
  update_cdf(cdf, symbol, nsymbs)
}

write_bit(bit)
{
  write_bool(128, bit)

#if VALIDATE_DAALA_EC
  validate(8002)
  validate(bit)
#endif
}


// write_bool returns the value written.
uint1 write_bool(uint8 probability,bit)
{
  uint16 cdf[3]
  cdf[ 0 ] = ( ( probability << DAALA_PRECISION ) + 256 - probability ) >> 8
  cdf[ 1 ] = 1 << DAALA_PRECISION
  cdf[ 2 ] = 0

  write_symbol(cdf, bit, 2)
  return bit
}
#endif // ENCODE

#if DECODE
refill_bool(bits) {
  if (symbolMaxBits <= 0) {
    newData = 0
  } else if (symbolMaxBits < bits) {
    newData u(symbolMaxBits)
    newData = newData << (bits - symbolMaxBits)
  } else {
    newData u(bits)
  }

  symbolMaxBits -= bits
  Buffer = ( ( Buffer + 1 ) << bits ) - 1
  Buffer = Buffer ^ newData
#if VALIDATE_SPEC_ARITH
  validate(10007)
  validate(Buffer)
#endif
}

init_bool(size)
{
  unused = set_bool_length(size)
  symbolMaxBits = size * 8
  Buffer = 0
  refill_bool(DAALA_PRECISION) // Read the initial state

  Range = 1 << DAALA_PRECISION
  Size = size
}

exit_bool(padBytes)
{
#if VALIDATE_SPEC_ARITH
  validate(10008)
  validate(symbolMaxBits)
  validate(current_bitposition())
#endif
  CHECK(padBytes==0, "Internal error")

  // Rewind to where we expect the trailing '1' bit to be,
  // and check that the tile data is terminated properly
  ASSERT(symbolMaxBits > -15, "Entropy coder read too many bits")
  uint64 paddingEnd
  paddingEnd = current_bitposition() + Max(0, symbolMaxBits)
  rewindBits = Min(15, symbolMaxBits + 15)
  unused = unread_bits(rewindBits)

  one_bit u(1) ASSERT(one_bit == 1, "Tile data is improperly terminated")
  while (current_bitposition() < paddingEnd) {
    padding u(1) ASSERT(padding == 0, "Tile data is improperly terminated")
  }


#if VALIDATE_DAALA_EC
  validate(8007)
  size = Size
  validate(size)
#endif

  unused = set_bool_length(-1)
}

uint_0_15 read_symbol_noadapt(uint16pointer cdf, uint_2_16 nsymbs)
{
#if VALIDATE_SPEC_ARITH
  validate(10001)
  validate(Range)
  validate(Buffer)
  validate(nsymbs)
  validate(disable_cdf_update ? 1 : 0)
#endif
  CHECK(nsymbs <= 16, "invalid nsymbs")
  CHECK(cdf[nsymbs-1] == 1<<DAALA_PRECISION, "Invalid cdf - top")
#if VALIDATE_DAALA_EC
  rangePre=Range
#endif

  cur = Range
  symbol = -1
  do {
    symbol++
    prev = cur
    CHECK(8+7==DAALA_PRECISION, "8+7==DAALA_PRECISION")
    f = ( 1 << DAALA_PRECISION ) - cdf[ symbol ]
    cur = ((Range >> 8) * (f >> EC_PROB_SHIFT)) >> (7 - EC_PROB_SHIFT)
    cur += EC_MIN_PROB * (nsymbs - symbol - 1)
#if VALIDATE_SPEC_ARITH
    validate(10002)
    validate(f)
    validate(symbol)
    validate(cur)
    validate(cdf[ symbol ])
#endif
  } while ( Buffer < cur )
  CHECK(cur<prev, "cur<prev")
  CHECK(prev<=Range,"prev<=Range")
  newRange = prev - cur
  Buffer -= cur
  bits = DAALA_PRECISION - ( get_unsigned_bits( newRange ) - 1 )
  newRange = newRange << bits
  Range = newRange

  refill_bool(bits)
#if VALIDATE_SPEC_ARITH
  validate(10006)
  validate(symbolMaxBits)
  validate(symbol)
#endif

  fl = (symbol>0) ? cdf[symbol-1] : 0
  fh = cdf[symbol]
  fl = ( 1 << DAALA_PRECISION ) - fl
  fh = ( 1 << DAALA_PRECISION ) - fh
  buf = Buffer
  :debug_cabac_daala_symbol(symbol,nsymbs,newRange,bits,fh,fl,buf)
  :C debug_cabac_daala_symbol(symbol,nsymbs,newRange,bits,fh,fl,buf)

#if VALIDATE_DAALA_EC
  validate(8001)
  validate(symbol)
  validate(prev)
  validate(cur)
  validate(rangePre)
  validate(bits)
#endif
  CHECK(symbol>=0 && symbol < nsymbs, "Invalid symbol or nsymbs")
#if COLLECT_STATS
  total_symbols += 1
#endif
  return symbol
}

uint_0_15 read_symbol(uint16pointer cdf, uint_2_16 nsymbs) {
  symbol = read_symbol_noadapt(cdf, nsymbs)
  update_cdf(cdf, symbol, nsymbs)
  return symbol
}

uint1 read_bit()
{
  bit = read_bool(128)

#if VALIDATE_DAALA_EC
  validate(8002)
  validate(bit)
#endif
  return bit
}

uint1 read_bool(uint8 probability)
{
  uint16 cdf[3]
  cdf[ 0 ] = ( ( probability << DAALA_PRECISION ) + 256 - probability ) >> 8
  cdf[ 1 ] = 1 << DAALA_PRECISION
  cdf[ 2 ] = 0

  bit = read_symbol(cdf, 2)

  return bit
}
#endif // DECODE
