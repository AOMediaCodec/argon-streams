/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

class write_yuv_t
{
public:
    write_yuv_t (const char *path);

    ~write_yuv_t ();

    void write (const uint16_t *y, const uint16_t *u, const uint16_t *v,
                size_t width, size_t height, size_t stride,
                unsigned bit_depth, bool ss_x, bool ss_y, bool monochrome);

private:
    // Output file handle
    FILE     *handle;
};
