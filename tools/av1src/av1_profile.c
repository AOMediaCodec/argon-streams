/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.

DESCRIPTION:
Each function defines a profile for the encoder.
The function contains a number of assign statements.
These are inserted into the encoder at the point where the corresponding syntax
element is decoded.

Functions can be called in order to specify a hierarchy of profiles.
*******************************************************************************/

default() {
  // encoder settings
  // Probabilities of using certain features

  // Probability of having a hidden frame.
  // Note that, if this is set to a large value, we might reduce it again
  // in enc_pick_timing_info() in order to guarantee that a certain
  // minimum display framerate is possible
  enc_hide_frame_prob = enc_large_scale_tile ? 0 : Choose(0, 40)
  // Probability that we split a partition.
  // In order to avoid pathological cases (eg, a 4K or 8K stream with all blocks being 4x4),
  // we limit the split probability to 75% if the stream exceeds the level 2.0 max area
  enc_split_prob = (enc_stream_width * enc_stream_height < 512*288) ? Choose(0, 100) : Choose(0,75)
  enc_skip_prob = Choose(0,100)
  enc_zero_coef_prob = Choose(0,100)
  enc_zero_rowcol_prob = (enc_zero_coef_prob == 0) ? 0 : Choose(0,30)

  // 100 in a million means 1 in 10,000 chance
  enc_tx_coeffs_per_million = 100

  enc_inter_prob = Choose(0,100)
  enc_compound_prob = Choose(0, 100)
  enc_empty_block_prob = Choose(0,100) // Prob of coding a block with no coefficients
  enc_barrier_prob = Choose(0, 20) // % chance of setting refresh_flags=0xff for any particular frame
  enc_frame_is_barrier = Choose(0, 99) < enc_barrier_prob

  enc_palette_prob = enc_enable_palette ? Choose(0, 100) : 0
  // 10% of streams use lossless mode for every frame; the remaining 90% select between
  // lossless and lossy per frame, with a 10% chance of being lossless
  enc_lossless_prob = (Choose(0,99)<10) ? 100 : 10
  enc_enable_palette = ChooseBit()

  // When a frame could validly be either inter or intra-only, this variable
  // is used to decide which one it will be.
  // Note that this alone can't force all frames to be inter, or all intra-only,
  // due to extra constraints in the frame scheduler.
  enc_frame_is_inter = enc_force_bug_libaom_2191 ? 1 : ChooseBit()

  // The case enc_allow_scaling == false is intended to help cover things like
  // warped motion and MFMV, which can only be used between frames with the same mi_rows
  // and mi_cols values. This requires both that the signalled frame sizes are all the same,
  // *and* that superres is not used. So we bias away from allowing superres when enc_allow_scaling
  // is false.
  enc_superres_prob = enc_allow_scaling ? 50 : 20

  // Parameters for the "leaky bit bucket" model (described by a comment in generate_one_tile())
  enc_use_bit_bucket = 1
  enc_bit_bucket_offset_factor = 30 // Try to use up 30% of the bucket's capacity each frame
  enc_bit_bucket_leak_factor = 20 // Leak 20% of the bucket's capacity after each frame
  // Note: the following three values are regenerated once per tile
  enc_bit_bucket_fill_rate = intra_only ? 50 : 20 // in units of 1/100 of a bit per luma pixel
  enc_min_bpp              = intra_only ? 30 :  0 // in units of 1/100 of a bit per luma pixel
  enc_max_bpp              = intra_only ? 70 : 40 // in units of 1/100 of a bit per luma pixel

  enc_chosen_plane = Choose(0,2)
  enc_still_picture = enc_huge_picture ? 1 : (Choose(0, 99) < 5)
  enc_reduced_headers = ChooseBit()
  enc_coef_max = Min(1<<Choose(2,21), QCOEFF_MAX)
  enc_force_intra_only_frames = 0
  enc_use_profile_switching = 0
  enc_allow_retry_shift = 1 // Fix saturating transforms by shifting
  enc_allow_retry_bias = 0
  enc_push_coefs = 0
  enc_use_large_coefs_prob = 1 // 1% chance of biasing toward large coefficients
  enc_use_large_coefs = (Choose(0, 99) < enc_use_large_coefs_prob) ? 1 : 0
  enc_coef_sign_pattern = (Choose(0, 99) < 25) ? 1 : 0 // 25% chance of generating "patterned" coefficient signs
  enc_coef_scale = 0 // Unused unless pushing
  //If set, only uses powers of 2 for mv components
  enc_mv_log2 = 1
  //If set, limit MVs to the size of a typical image
  enc_mv_small_prob = 50
  enc_mv_small = Choose(0,99) < enc_mv_small_prob
  enc_try_pick_large_dv = 0
  enc_force_intrabc = 0
  // TODO limit max mv for typical images
  enc_mv_max_row = enc_mv_small?enc_stream_height:(1<<(enc_mv_log2?Choose(1,14):14)) // Greatest absolute value for motion vectors dy - must be <= 1<<14 to avoid needing to use mv class 11
  enc_mv_max_col = enc_mv_small?enc_stream_width:(1<<(enc_mv_log2?Choose(1,14):14)) // Greatest absolute value for motion vectors dx - must be <= 1<<14 to avoid needing to use mv class 11
  enc_mv_specific = 0
  enc_mv_all = 0

  // Special flag only used for choose_levelMax(). If set, the stream is picked
  // to be too large to fit into level 6.3
  enc_huge_picture = 0

  enc_is_license_resolution = 0
  enc_limit_anchor_frames = 1
  enc_is_stress_stream = 0
  sched_initial_fwd_kf = ChooseBit()

  isSRGB = ChooseBit()
  useColorDescription = isSRGB ? 1 : ChooseBit() // Must specify color descriptions to use sRGB
  enc_hit_color_primaries_branch = 0

  timing_info_present = (enc_decoder_model_mode == DECODER_MODEL_DISABLED) ? 0 : 1
  equal_picture_interval = (enc_decoder_model_mode == DECODER_MODEL_RESOURCE_AVAILABILITY) ? 1 : ChooseBit()

  // Because we pick time_scale based on the other parameters (a different order
  // from the bitstream), we have to define everything via parameters with enc_
  // prefixes.
  //
  // We're going to pick a time scale so that (if equal_picture_interval is set)
  // the frame rate comes out as enc_frame_rate. This is a function of
  // enc_num_units_in_display_tick and enc_num_ticks_per_picture (see
  // av1_uncompressed_header.c).
  //
  // In order that we can generate streams with lots of early hidden frames, we
  // make sure the decoder clock runs somewhat faster: at least a factor of 5.
  enc_num_units_in_display_tick = Choose(5, 50)
  enc_num_ticks_per_picture = Choose(1, 50)
  enc_num_units_in_decoding_tick = Choose(1, enc_num_units_in_display_tick * enc_num_ticks_per_picture / 5)

  num_units_in_decoding_tick = enc_num_units_in_decoding_tick
  num_units_in_display_tick = enc_num_units_in_display_tick
  num_ticks_per_picture = enc_num_ticks_per_picture

  enc_use_decoder_model = 1
  enc_decoder_model_use_schedule = ChooseBit()

  enc_display_model_info_present_flag = ChooseBit()
  decoder_model_info_present_flag = (enc_decoder_model_mode == DECODER_MODEL_SCHEDULE) ? 1 : 0
  enc_choose_max_bitrate = 0
  enc_override_tier = enc_choose_max_bitrate ? (enc_profile > 0 ? 0 : 1) : -1
  tier = (enc_override_tier >= 0) ? enc_override_tier : ChooseBit()
  display_model_param_present_flag = ChooseBit()
  initial_display_delay = Choose(0, 9)
  decoder_model_param_present_flag = (enc_decoder_model_mode == DECODER_MODEL_SCHEDULE) ? 1 : ChooseBit()

  enc_special_operating_points = Choose(0, 99) < 5

  encoder_decoder_buffer_delay_length = Choose(17, 32)-1 // The -1 compensates for a +1 in the decoder
  decoder_buffer_delay = ChooseBits (encoder_decoder_buffer_delay_length)
  encoder_buffer_delay = ChooseBits (encoder_decoder_buffer_delay_length)

  // The following symbols have certain minimum values we can get away with, depending
  // on the details of enc_pick_timing_info().
  // At the moment, we have a "decode clock" of at most 1000x the nominal frame rate,
  // and decode timings are signalled per operating point. Thus buffer_removal_delay
  // should be at most about 1000, so 10 bits is enough.
  buffer_removal_delay_length = Choose(10, 32)-1 // The -1 compensates for a +1 in the decoder
  // On the other hand, the "display clock" is currently exactly equal to the
  // nominal rame rate. But there is only one presentation delay signalled per frame,
  // and we may have up to 8 temporal layers where each layer has half of the
  // frame rate of the layer above.
  // Thus we need at least 'enc_temporal_layers' many bits.
  frame_presentation_delay_length = Choose(enc_temporal_layers, 32)-1 // The -1 compensates for a +1 in the decoder

  low_delay_mode_flag = ChooseBit()
  tuPresentationDelay = 0 // Gets overwritten at the end of encoding the frame
  buffer_removal_delay_present = (enc_decoder_model_mode == DECODER_MODEL_SCHEDULE) ? 1 : ChooseBit()
  bufferRemovalDelay = 0 // Gets overwritten at the end of encoding the frame

  // Note: For most streams, the profile+bit depth combination forces a specific
  // subsampling mode. In that case, encode_choose_profile() will set enc_subsampling_*
  // to the correct values automatically.
  // Thus the following lines, picking a random choice of subsampling mode, are only used
  // in the case where the subsampling can be selected at a bitstream level. For the current
  // set of AV1 profiles, that only occurs for 12-bit, non-sRGB streams.
  enc_subsampling_x = ChooseBit()
  enc_subsampling_y = ChooseBit()
  subsampling_x = enc_subsampling_x
  subsampling_y = enc_subsampling_y

  // When generating various LEB128 format values, we randomly select the length
  // of the LEB128 field itself.
  // Note that we want to exercise as many values as possible, but also we need
  // to ensure that the smallest value is "large enough" to fit the relevant data in.
  // Since we don't have an easy way to predict most of these sizes up-front, we have
  // to be quite conservative about the sizes we assign.
  tuSizeBytes = Choose(4, LEB128_MAX_SIZE)
  frameSizeBytes = Choose(4, LEB128_MAX_SIZE)
  obuSizeBytes = Choose(4, LEB128_MAX_SIZE)
  metadataTypeBytes = Choose(uleb128_min_bytes(enc_metadata_type), LEB128_MAX_SIZE)

  // OBU header
  obu_forbidden_bit = 0 // Must be 0
  enc_obu_extension_flag = ChooseBit ()
  obu_extension_flag = enc_use_scalability && (obu_must_use_extension_header[obu_type] || (obu_can_use_extension_header[obu_type] && enc_obu_extension_flag))
  // Note: obu_has_payload_size_field is required to be 1 for non-annexb streams
  obu_has_payload_size_field = !use_annexb || obu_has_size_field_cfg
  obu_has_size_field_cfg = ChooseBit()
  payloadSizeBytes = Choose(4, LEB128_MAX_SIZE)

  // Sequence header
  level = enc_use_max_params_level ? LEVEL_MAX_PARAMS : enc_operating_point_level[i]

  // Padding OBU
  enc_padding_size = Choose(0, 32)
  obu_padding_byte = ChooseBits(8)

  // Metadata OBU
  enc_metadata_type = enc_select_metadata_type()
  metadata_type = enc_metadata_type

  enc_specify_metadata = 0
  enc_num_metadata_obus = 0
  enc_metadata_type_cfg = 0

  // TODO: Generate sensible values for the following
  itu_t_t35_country_code = ChooseBits(8)
  itu_t_t35_country_code_extension_byte = ChooseBits(8)
  itu_t_t35_payload_size = Choose(0, 32)
  metadata_itut_t35_payload_byte = ChooseBits(8)
  arbitrary_payload_size = Choose(0, 32)
  metadata_arbitrary_payload_byte = ChooseBits(8)
  max_cll = 0
  max_fall = 0
  primary_chromaticity_x = 0
  primary_chromaticity_y = 0
  white_point_chromaticity_x = 0
  white_point_chromaticity_y = 0
  luminance_max = 0
  luminance_min = 0

  // Timecode metadata
  enc_specify_timestamp_flags = 0
  enc_full_timestamp_flag = 0
  enc_seconds_flag = 0
  enc_minutes_flag = 0
  enc_hours_flag = 0
  counting_type = 0
  full_timestamp_flag = enc_full_timestamp_flag
  discontinuity_flag = 0
  cnt_dropped_flag = 0
  n_frames = 0
  seconds_flag = enc_seconds_flag
  minutes_flag = enc_minutes_flag
  hours_flag = enc_hours_flag
  seconds_value = 0
  minutes_value = 0
  hours_value = 0
  time_offset_length = ChooseBit() * 3
  time_offset_value = 0

  use_obu_frame = ChooseBit()

  enc_use_scalability = ChooseBit()
  enc_use_scalability_preset = ChooseBit()
  enc_scalability_preset = Choose(SCALABILITY_L1T2, SCALABILITY_L4T7_KEY_SHIFT)
  scalability_mode = enc_scalability_mode

  enc_spatial_layers_count = Choose(1, MAX_NUM_SPATIAL_LAYERS)
  enc_spatial_layer_dimensions_present_flag = ChooseBit()
  enc_specify_spatial_layer_sizes = 0
  enc_spatial_layer_max_width = enc_stream_width
  enc_spatial_layer_max_height = enc_stream_height

  enc_spatial_layer_description_present_flag = ChooseBit()
  enc_spatial_layer_ref_id = ((i == 0 || ChooseBit ()) ?
                              255 :
                              Choose (0, i - 1))

  spatial_layers_count_minus1 = enc_spatial_layers - 1

  enc_temporal_layers_count = Choose(1, MAX_NUM_TEMPORAL_LAYERS)
  enc_temporal_group_temporal_switching_up_point_flag = Choose(0, 99) < 5
  enc_temporal_group_spatial_switching_up_point_flag  = Choose(0, 99) < 5
  enc_temporal_group_ref_cnt = Choose (0, 7)
  enc_temporal_ref_allowed = ChooseBits(i-1)

  // Note: The scalability structure) is set up in enc_pick_scalability(),
  // so should not be listed here

  num_operating_points_minus1 = enc_num_operating_points - 1
  // Operating point parameters - currently ignored by the reference code
  spatial_layers_used = enc_spatial_layers_used[i]
  temporal_layers_used = enc_temporal_layers_used[i]

  has_alpha = 0

  zero_byte = 0
  // ivf_file_header
  header = 0x444b4946
  ivf_version = 0
  headersize = 32
  fourcc = 0x41563031

  enc_tile_group_mode = Choose(ENC_TG_MODE_ONE_TG, ENC_TG_MODE_UNIFORM)
  enc_num_tile_groups = -1
  enc_specify_tile_groups = 0
  enc_tg_size = 0

  // Special value - if this is set to 1, then each operating point is
  // set to the special "max params" level instead of whatever level
  // it would normally be assigned
  enc_use_max_params_level = 0

  // Level and stream size selection - see the corresponding section
  // of av1_extra.c for details on how these values are used
  enc_min_level = 0
  enc_max_level = 0

  enc_speed_group = Choose(0, 3)
  enc_framerate_choice = Choose(0, 2)

  enc_force_base_level = -1

  enc_chooseWidthFirst  = ChooseBit()
  //     // Here we guarantee that if not at level2.0 at least one dimension will be greater than level2.0
  //     //enc_profile_min_width = (enc_min_level > 0 && enc_chooseWidthFirst) ? (level_max_pic_size[enc_min_level-1][1] + 1) : MIN_FRAME_WIDTH
  //     //enc_profile_min_height = (enc_min_level > 0 && !enc_chooseWidthFirst) ? (level_max_pic_size[enc_min_level-1][2] + 1) : MIN_FRAME_HEIGHT
  //     //enc_profile_max_width = level_max_pic_size[enc_max_level][1]
  //     //enc_profile_max_height = level_max_pic_size[enc_max_level][2]

  //All of the explicit limits are disabled by default - this will cause the encoder
  //to aim to make the stream bigger than the maximum size for the level below, so
  //that we get streams that can only be encoded in our chosen level.

  enc_profile_min_width = -1
  enc_profile_max_width = -1
  enc_profile_min_height = -1
  enc_profile_max_height = -1

  enc_frame_min_width_limit = MIN_FRAME_WIDTH
  enc_frame_min_height_limit = MIN_FRAME_HEIGHT

  enc_profile_max_tiles = level_max_tiles[enc_max_level][0]
  enc_profile_max_tile_cols = level_max_tiles[enc_max_level][1]

  enc_exactly_one_tile = 0

  // IVF file header
  g_w = enc_stream_width
  g_h = enc_stream_height
  g_timebase_den = 1000
  g_timebase_num = 1 // 1/1000 chosen from reference encoder
  frame_cnt = enc_frame_cnt
  enc_frame_cnt = 10
  enc_anchor_frame_cnt = enc_pick_anchor_frame_cnt()
  enc_tile_list_frame_cnt = Choose(1,10)
  unused = 0 // TODO
  // IVF frame header
  frame_sz = num
  timebase_high = 0
  // uncompressed header
  enc_profile = 0
  profile = enc_profile
  // Mark all streams as video streams for now.
  still_picture = enc_still_picture
  reduced_still_picture_hdr = enc_reduced_headers

  enc_bit_depth = (enc_profile == 2 && Choose(0, 99) < 75) ? 12 : (ChooseBit() ? 10 : 8)
  use10or12bit = (enc_bit_depth >= 10)
  use12bit = (enc_bit_depth == 12)
  frame_marker = FRAME_MARKER
  reserved = 0
  show_existing_frame = enc_show_existing_frame
  index_of_existing_frame = enc_index_of_existing_frame
  sched_show_frame = enc_large_scale_tile ? 1 : (Choose(0, 99) >= enc_hide_frame_prob)
  show_frame = enc_show_frame
  frame_type = enc_frame_type
  error_resilient_mode = enc_large_scale_tile ? 0 : ChooseBit()

  // Sequence level enable/disable flags for coding tools
  enable_filter_intra = ChooseBit()
  enable_intra_edge_filter = ChooseBit()
  enable_interintra_compound = ChooseBit()
  enable_masked_compound = ChooseBit()
  // Warped motion can only be used between equal-sized frames, so
  // boost the probability of allowing it when all frames are the same size
  enable_warped_motion = enc_allow_scaling ? (enc_force_bug_libaom_2191 ? 1 : ChooseBit()) : (Choose(0, 99) < 80)
  enable_dual_filter = ChooseBit()
  enc_enable_order_hint = Choose(0, 99) < 75 // Needs to be selected early, for the scheduler to work
  enc_allow_short_signalling = 1
  enc_use_short_signaling = ChooseBit ()
  enc_last_frame_idx = Choose(0, sched_num_fwd_refs-1)
  enc_gold_frame_idx = Choose(0, sched_num_fwd_refs-1)
  enable_order_hint = enc_enable_order_hint
  enable_jnt_comp = ChooseBit()
  enable_ref_frame_mvs = (Choose(0, 99) < 75)
  seq_choose_screen_content_tools_bit = ChooseBit()
  seq_force_screen_content_tools_bit = ChooseBit()

  // These are the seq_choose_integer_mv and seq_force_integer_mv
  // bit-stream elements, which together specify the
  // seq_force_integer_mv variable.
  seq_choose_integer_mv_bit = ChooseBit()
  seq_force_integer_mv_bit  = ChooseBit()

  enable_superres = enc_large_scale_tile ? 0 : ChooseBit()
  enable_cdef = enc_large_scale_tile ? 0 : ChooseBit()
  enable_loop_restoration = enc_large_scale_tile ? 0 : ChooseBit()

  // Frame-level enable-disable flags for coding tools
  disable_cdf_update = enc_large_scale_tile ? 1 : ChooseBit()
  allow_screen_content_tools = ChooseBit()
  cur_frame_force_integer_mv = ChooseBit()
  allow_intrabc = ChooseBit()
  allow_high_precision_mv = ChooseBit()
  enc_allowed_motion_modes = enc_force_bug_libaom_2191 ? 1 : ((enable_warped_motion && ChooseBit()) ? 2 : ChooseBit())
  switchable_motion_mode = (enc_allowed_motion_modes > 0)
  // Note: If switchable_motion_mode == 0, then we still code allow_warped_motion,
  // but its value is meaningless. So we give allow_warped_motion a 50/50 chance in that case.
  allow_warped_motion = (enc_allowed_motion_modes == 2) || (enc_allowed_motion_modes == 0 && ChooseBit())

  context_tile_id = Choose(0, tile_rows * tile_cols - 1)

  enc_use_intrabc = enc_force_intrabc ? 1 : ChooseBit()

  enc_palette_y = Choose(0, 99) < enc_palette_prob
  enc_palette_uv = Choose(0, 99) < enc_palette_prob
  has_palette_y = enc_palette_y
  has_palette_uv = enc_palette_uv

  enc_palette_size_y = Choose(2, 8)
  enc_palette_specify_color_cache_y = 0
  enc_palette_color_cache_y_length = Choose(0, Min(psize, cacheN))
  enc_use_palette_color_cache_y = 0

  enc_palette_size_uv = Choose(2, 8)
  enc_palette_specify_color_cache_u = 0
  enc_palette_color_cache_u_length = Choose(0, Min(psize, cacheN))
  enc_use_palette_color_cache_u = 0

  palette_size_y = (enc_palette_len[0] - 2)
  palette_size_uv = (enc_palette_len[1] - 2)
  color_idx_y = enc_palette_index(0, i-j, j)
  color_idx_uv = enc_palette_index(1, i-j, j)
  use_palette_color_cache_y = enc_use_palette_cache[0][i]
  use_palette_color_cache_u = enc_use_palette_cache[1][i]
  // To maximize the coverage of distinct values, make it relatively
  // likely that we'll use the maximum number of bits possible
  palette_num_extra_bits_y = (enc_use_palette_pattern[0] || ChooseBit()) ? 3 : Choose(0,2)
  palette_num_extra_bits_u = (enc_use_palette_pattern[1] || ChooseBit()) ? 3 : Choose(0,2)
  palette_num_extra_bits_v = ChooseBit() ? 3 : Choose(0,2)
  palette_delta_y = enc_use_palette_pattern[0] ? (enc_pattern_colours[0][idx] - enc_pattern_colours[0][idx-1] - 1) : ChooseAll(0, (1<<paletteBits)-1, PALETTE_DELTA_Y)
  palette_delta_u = enc_use_palette_pattern[1] ? (enc_pattern_colours[1][idx] - enc_pattern_colours[1][idx-1]) : ChooseAll(0, (1<<paletteBits)-1, PALETTE_DELTA_U)
  palette_delta_v = ChooseAll(0, (1<<paletteBits)-1, PALETTE_DELTA_V)
  delta_encode_palette_colors_v = ChooseBit()
  palette_delta_sign_bit_v = ChooseBit()
  palette_colors_y = enc_use_palette_pattern[0] ? enc_pattern_colours[0][idx] : ChooseAll(0, (1<<bit_depth)-1, PALETTE_COLORS_Y)
  palette_colors_u = enc_use_palette_pattern[1] ? enc_pattern_colours[0][idx] : ChooseAll(0, (1<<bit_depth)-1, PALETTE_COLORS_U)
  palette_colors_v = ChooseAll(0, (1<<bit_depth)-1, PALETTE_COLORS_V)

  color_index_map_y = enc_palette_index (0, 0, 0)
  color_index_map_uv = enc_palette_index (1, 0, 0)

  film_grain_params_present = enc_large_scale_tile ? 0 : ChooseBit()
  apply_grain = ChooseBit()
  random_seed = ChooseBits(16)
  enc_update_params = ChooseBit()
  update_params = enc_update_params
  enc_film_grain_params_ref_idx = enc_compatible_refs[Choose(0, numIdcs-1)]
  film_grain_params_ref_idx = enc_film_grain_params_ref_idx

  // 25% of the time, we have no Y points. Otherwise, we pick a count
  // from 1 to 14.
  num_y_points = (ChooseBits(2) == 0) ? 0 : Choose(1, 14)

  chroma_scaling_from_luma = ChooseBit()

  // We only evaluate num_cb_points if we aren't monochrome or doing
  // chroma scaling from luma. Also, we don't evaluate it if we're
  // doing 420 subsampling and num_y_points is zero. The rest of the
  // time, bias towards zero slightly.
  num_cb_points = ChooseBit() ? 0 : Choose(1, 10)

  // With 420 subsampling, it's a bitstream conformance requirement
  // that either both num_cb_points and num_cr_points are zero or
  // neither are. Otherwise, we can pick freely (and we bias slightly
  // towards zero).
  num_cr_points = ((subsampling_x && subsampling_y) ?
                   (num_cb_points ? Choose(1, 10) : 0) :
                   (ChooseBit() ? 0 : Choose(1, 10)))

  scaling_points_y0 = Choose((i==0) ? 0 : scaling_points_y[i - 1][0]+1, (1<<8)-1-(num_y_points-1-i))
  scaling_points_y1 = ChooseBits(8)
  scaling_points_cb0 = Choose((i==0) ? 0 : scaling_points_cb[i - 1][0]+1, (1<<8)-1-(num_cb_points-1-i))
  scaling_points_cb1 = ChooseBits(8)
  scaling_points_cr0 = Choose((i==0) ? 0 : scaling_points_cr[i - 1][0]+1, (1<<8)-1-(num_cr_points-1-i))
  scaling_points_cr1 = ChooseBits(8)
  scaling_shift = ChooseBits(2)
  ar_coeff_lag = ChooseBits(2)
  enc_film_grain_signs = 0
  ar_coeff_y = ChooseBits(8)
  ar_coeff_u = ChooseBits(8)
  ar_coeff_v = ChooseBits(8)
  ar_coeff_shift = ChooseBits(2)
  grain_scale_shift = ChooseBits(2)
  cb_mult = ChooseBits(8)
  cb_luma_mult = ChooseBits(8)
  cb_offset = ChooseBits(9)
  cr_mult = ChooseBits(8)
  cr_luma_mult = ChooseBits(8)
  cr_offset = ChooseBits(9)
  overlap_flag = ChooseBits(1)
  clip_to_restricted_range = ChooseBits(1)
  showable_frame = sched_showable_frame[frame_number]
  use_filter_intra = ChooseBit()
  filter_intra_mode = Choose(0,FILTER_INTRA_MODES-1)
  angle_delta_y = Choose(0,2*MAX_ANGLE_DELTA)
  angle_delta_uv = Choose(0,2*MAX_ANGLE_DELTA)
  enc_monochrome = ChooseBit()
  isMonochrome = enc_monochrome
  enc_color_description_present_flag = ChooseBit()
  color_primaries = enc_color_primaries
  transfer_characteristics = enc_transfer_characteristics
  matrix_coefficients = enc_matrix_coefficients
  enc_color_primaries = 256 // Valid values are 0-255
  enc_transfer_characteristics = 256 // Valid values are 0-255
  enc_matrix_coefficients = 256 // Valid values are 0-255
  color_space = enc_color_space
  transfer_function = enc_transfer_function
  chroma_sample_position = Choose(CSP_VERTICAL,CSP_COLOCATED)
  enc_allow_scaling = 0 // unless config_scaling() is used

  // These are normally set in enc_pick_frame_size, but both may be
  // overridden (which is exposed to the config language).
  enc_frame_width = 0
  enc_frame_height = 0

  enc_use_obu_frame = ChooseBit()

  frame_width_minus1 = enc_frame_width - 1
  frame_height_minus1 = enc_frame_height - 1
  range16 = ChooseBit()
  has_scaling = ChooseBit()
  display_width_minus1 = ChooseBits(16)
  display_height_minus1 = ChooseBits(16)
  frame_context_idx = (frame_type == KEY_FRAME || intra_only) ? 0 : ChooseBits(FRAME_CONTEXTS_LOG2)  // Comment in vp9_encoder.c:setup_frame suggests that only 0 frame context to be used for intra frames.
  filter_level = enc_large_scale_tile ? 0 : (Choose(0, 99) < 10 ? 0 : ChooseBits(6)) // Boost the chance of selecting 0, to help cover branches where filter_level[0] == filter_level[1] == 0
  sharpness_level = ChooseBits(3)
  mode_ref_delta_enabled = ChooseBit()
  mode_ref_delta_update = ChooseBit()
  update_ref_delta = ChooseBit()
  update_mode_delta = ChooseBit()
  coded = ChooseBit()

  // If enc_base_qindex is overridden to a non-negative number, it
  // sets the base qindex, which might mean we ignore enc_lossless and
  // enc_special_lossless.
  enc_base_qindex = -1

  enc_lossless = Choose(0,99)<enc_lossless_prob

  // There are two possible paths to lossless mode, which are both worth testing:
  // i) The obvious path:
  //    base_qindex = 0, all dc_delta_qindex / ac_delta_qindex values = 0,
  //    all segments either don't have SEG_LVL_ALT_Q or the associated values are negative
  //    In this case, delta_q_present is implicitly set to 0
  //
  // ii) The unusual path:
  //     base_qindex != 0, all dc_delta_qindex / ac_delta_qindex values = 0,
  //     all segments have SEG_LVL_ALT_Q with values <= -base_qindex
  //     so that all segment quantizers are 0.
  //     In this case, delta_q_present is signalled but must be set to 0
  //     for bitstream conformance.
  enc_special_lossless = Choose(0, 99)<5 // 5% chance

  delta_coded = ChooseBit()
  enc_delta_q = ChooseSignedBits2(6)

  base_qindex = enc_pick_base_qindex()

  using_qmatrix = ChooseBit()
  qm_y = ChooseBits(QM_LEVEL_BITS)
  qm_u = ChooseBits(QM_LEVEL_BITS)
  qm_v = ChooseBits(QM_LEVEL_BITS)

  ref_lf_deltas = ChooseSignedBits2(6)
  mode_lf_deltas = ChooseSignedBits2(6)
  refresh_frame_context = enc_large_scale_tile ? 0 : ChooseBit()
  frame_parallel_decoding_mode = ChooseBit()
  seg_enabled = enc_special_lossless ? 1 : ChooseBit()

  /* ref decoder doesn't support non uniform for ext_tile */
  uniform_tile_spacing_flag = (enc_large_scale_tile || enc_exactly_one_tile) ? 1 : ChooseBit()
  more_tile_cols = enc_more_tile_cols(sb_cols, log2_tile_cols, levelMaxTileCols)
  more_tile_rows = enc_more_tile_rows(sb_rows, log2_tile_rows, tile_width_sb, levelMaxTileRows)

  enc_specify_nonuniform_tiles = 0
  width_in_sbs_minus_1 = 0
  height_in_sbs_minus_1 = 0

  // nb we get away with the "identical for all tiles constraint by
  // only sending one camera frame ready header
  num_large_scale_tile_anchor_frames = enc_anchor_frame_cnt
  num_large_scale_tile_tile_list_obus = enc_tile_list_frame_cnt
  // TODO can we improve the randomness here?
  output_frame_width_in_tiles_minus_1 = (upscaled_width / (MI_SIZE * tile_width)) - 1
  output_frame_height_in_tiles_minus_1 = (upscaled_height / (MI_SIZE * tile_height)) - 1
  tile_count_minus_1 = Choose(0, Min(511, (output_frame_width_in_tiles_minus_1 + 1) * (output_frame_height_in_tiles_minus_1 + 1) - 1))
  enc_anchorFrameIdx_cfg = Choose (0, enc_anchor_frame_cnt - 1)
  anchorFrameIdx = enc_anchorFrameIdx[enc_decTileCol][enc_decTileRow]
  enc_decTileRow = Choose(0,tile_rows-1)
  enc_decTileCol = Choose(0,tile_cols-1)
  decTileRow = enc_decTileRow
  decTileCol = enc_decTileCol
  group_diff_update = ChooseBit()
  enc_tx_type_cfg = -1
  inter_tx_type = enc_tx_type
  intra_tx_type = enc_tx_type
  reduced_tx_set_used = ChooseBit()

  // Coefficient coding
  all_zero = (c_eob==0)
  eob_pt_16 = enc_eobPt - 1
  eob_pt_32 = enc_eobPt - 1
  eob_pt_64 = enc_eobPt - 1
  eob_pt_128 = enc_eobPt - 1
  eob_pt_256 = enc_eobPt - 1
  eob_pt_512 = enc_eobPt - 1
  eob_pt_1024 = enc_eobPt - 1
  eob_extra = ((c_eob - eob - eobExtra) >= (1 << eobShift))
  eob_extra_bit = ((c_eob - eob - eobExtra) >= (1 << eobShift))
  coeff_base_eob = Min(enc_coef[c], 3) - 1
  coeff_base = Min(enc_coef[c], 3)
  coeff_br = Min(enc_coef[c] - level, BR_CDF_SIZE - 1)

  txfm_split = enc_pick_txfm_split(txSz)
  //feature_sign_bit = ChooseBit()
  feature_sign_bit = (seg_feature_data_signed[feature]) ? 0 : ChooseBit() // Paul says it is illegal for streams to do this wrong
  header_size = 1 // Dummy value.  This gets overwritten once the size of the compressed header is known
  padding = 0
  zero_bit = 0
  one_bit = 1
  prob_coded = ChooseBit()
  prob = ChooseBits(8)
  seg_temporal_update = enc_large_scale_tile ? 0 : ChooseBit()
  can_use_ref = enc_large_scale_tile ? 0 : ChooseBit()
  enc_order_hint_bits = Choose(1, (1<<3))
  enc_order_hint = ChooseBits(enc_order_hint_bits)
  order_hint_bits_minus1 = enc_order_hint_bits - 1
  cur_frame_offset = sched_order_hint[frame_number]
  filter_type_switchable = ChooseBit()
  // Bias the filter selection toward EIGHTTAP_SHARP,
  // as this gives the best coverage of intermediate values
  mcomp_filter_type = ChooseBit() ? EIGHTTAP_SHARP : ChooseBits(2) // Frame level
  enc_interp_filter_cfg = -1
  enc_interp_filter = Choose(EIGHTTAP, EIGHTTAP_SHARP)
  interp_filter = enc_interp_filter[dir]
  // CABAC encoding
  byte_extension = 0
  new_bit = (arith_bottom>>7)&1
  marker = 0
  // Compressed header
  mode = Choose(0,ALLOW_32X32)
  extra_mode_bit = ChooseBit()
  mode_bit_64x64 = ChooseBit()
  coef_probs_coded = ChooseBit()
  diff_update = ChooseBit()
  // Key frame data
  extra_tile_padding = Choose(0, 8) // #extra padding bytes at the end of each OBU
  extra_obu_padding = Choose(0, 8) // #extra padding bytes at the end of each OBU
  tileByte = ChooseBits(8)
  padding_byte = 0
  enc_target_tx_size = -1
  enc_min_partition_size = BLOCK_4X4
  enc_max_partition_size = BLOCK_128X128
  enc_bias_to_big_partitions_prob = 0
  split_partition = Choose(0,99) < enc_split_prob
  enc_specify_partitions = 0
  enc_partition = PARTITION_SPLIT
  enc_split_or_horz = 1 // choose PARTITION_SPLIT
  enc_split_or_vert = 1 // choose PARTITION_SPLIT
  skip_coeff = enc_use_frame_planner ? enc_skip_coeff[mi_col][mi_row] : (Choose(0, 99) < enc_skip_prob)
  enc_frame_planner_skip_mode_prob = 50
  skip_mode = enc_use_frame_planner ? enc_skip_mode[mi_col][mi_row] : (Choose(0, 99) < enc_skip_prob)
  default_intra_mode_y = enc_palette_y ? DC_PRED : Choose(0,INTRA_MODES-1)
  seg_update_map = ChooseBit()
  enc_seg_update_data = ChooseBit()
  seg_update_data = enc_update_segdata()
  enc_force_seg_lvl_ref_frame_last_frame = 0
  coded_id = enc_coded_id(mi_row, mi_col, pred)
  pred_flag = (!enc_use_frame_planner || !preskip_seg_id || enc_segment_id[mi_col][mi_row] == pred_segment_id) ? ChooseBit() : 0
  encNumSegments = enc_special_lossless ? MAX_SEGMENTS : Choose(0, MAX_SEGMENTS)
  enc_enable_feature = ChooseBit()
  feature_enabled = enc_pick_feature(segmentId, feature, encNumSegments)
  decode_data = enc_pick_feature_data(feature, numbits, isSigned)
  enc_last_coef = enc_pick_num_coefs(seg_eob)
  txDepth = enc_pick_tx_depth(bsize, maxTxDepth)
  enc_row_sign = (Choose(0, 99) < enc_zero_rowcol_prob) ? 0 : (ChooseBit() ? -1 : 1)
  enc_col_sign = (Choose(0, 99) < enc_zero_rowcol_prob) ? 0 : (ChooseBit() ? -1 : 1)
  enc_coef = enc_choose_coef(c, enc_last_coef, adjusted_max_coef, adjTxSize)
  coef = enc_coef[c]
  enc_sign_bit = enc_choose_coef_sign(c, adjTxSize)
  sign_bit = enc_sign_bit[c]
  dc_sign = enc_sign_bit[0]
  enc_specify_coeffs = 0
  enc_quantized_coeff = 0
  refresh_frame_flags = enc_refresh_frame_flags
  enc_use_128x128_superblock = ChooseBit()
  use_128x128_superblock = enc_use_128x128_superblock
  current_frame_id = enc_current_frame_id

  enc_first_frame_id      = ChooseBits(frame_id_length)
  enc_subsequent_frame_id = Choose(minFrameId, maxFrameId)

  // P frame data
  active_ref_idx = sched_active_ref_idx[frame_number][i]
  frame_refs_short_signaling = sched_use_short_ref_signalling[frame_number]
  lstIdx = sched_active_ref_idx[frame_number][LAST_FRAME - LAST_FRAME]
  gldIdx = sched_active_ref_idx[frame_number][GOLDEN_FRAME - LAST_FRAME]
  intra_reset = ChooseBit()
  intra_reset_extra = ChooseBit()
  primary_ref_frame = ChooseBits(PRIMARY_REF_BITS)
  ref_frame_sign_bias = ChooseBit()
  use_ref_frame = (enc_copy_frame_from_ref == i)
  mv_prob_coded = ChooseBit()
  mv_prob = ChooseBits(7)
  is_inter = enc_use_frame_planner ? enc_is_inter[mi_col][mi_row] : (Choose(0, 99) < enc_inter_prob)

  pred_mode = large_scale_tile ? 0 : ChooseBit() // Ok to use compound for the anchor frames
  comp_mode = (enc_ref_frame[mi_col][mi_row][1] != NONE) // 0 = Single prediction, 1 = Compound
  intra_mode_y = enc_palette_y ? DC_PRED : Choose(0,INTRA_MODES-1)

  enc_intra_mode_uv = (enc_palette_uv ? DC_PRED :
                       Choose(0,is_cfl_allowed(sb_size) ? UV_INTRA_MODES-1 : INTRA_MODES-1))

  cfl_alpha_signs = Choose(0, CFL_JOINT_SIGNS-1)
  cfl_alpha_u = Choose(0, CFL_ALPHABET_SIZE-1)
  cfl_alpha_v = Choose(0, CFL_ALPHABET_SIZE-1)

  enc_inter_mode = validModes[ Choose(0, numValidModes-1) ]
  inter_mode = enc_pick_inter_mode()
  b_mode = Choose(NEARESTMV,NEWMV)
  enc_drl_idx_cfg = ValidDrlIndices[Choose (0, NumValidDrlIndices - 1)]
  drl_mode = (drl_idx < enc_drl_idx)

  skip_mode_allowed = ChooseBit()

  enc_compound_mode = (numValidModes ?
                       validModes [Choose (0, numValidModes - 1)] :
                       NEW_NEWMV) - NEAREST_NEARESTMV

  comp_inter_mode = enc_pick_comp_mode()
  comp_b_mode = Choose(NEAREST_NEARESTMV, NEW_NEWMV)

  wedge_index = Choose(0, 16-1)
  wedge_sign = ChooseBit()

  mask_type = ChooseBits(MAX_SEG_MASK_BITS)

  compound_group_idx = ChooseBit()
  compound_idx = ChooseBit()
  compound_mask_type = ChooseBit()

  interintra = ChooseBit()
  interintra_mode = Choose(0, INTERINTRA_MODES - 1)
  wedge_interintra = ChooseBit()

  use_obmc = ChooseBit()

  motion_mode = ChooseBit() ? WARPED_CAUSAL : (ChooseBit() ? OBMC_CAUSAL : SIMPLE_TRANSLATION)

  enc_specify_mv = 0
  enc_mv_coord = 0

  delta_q_present = (large_scale_tile || coded_lossless) ? 0 : ChooseBit()
  deltaQRes = ChooseBits(2)
  abs = Choose(0, DELTA_Q_SMALL)
  rem_bits = ChooseBits(3)
  abs_bits = ChooseAll(0, (1<<rem_bits)-1, DELTA_Q_ABS_BITS)
  delta_q_sign_bit = ChooseBit()
  separate_uv_delta_q = ChooseBit()
  diffUVDelta = ChooseBit()
  delta_lf_present = large_scale_tile ? 0 : ChooseBit()
  deltaLfRes = ChooseBits(2)
  abs_lf = Choose(0, DELTA_Q_SMALL)
  rem_bits_lf = ChooseBits(3)
  abs_bits_lf = ChooseAll(0, (1<<rem_bits_lf)-1, DELTA_LF_ABS_BITS)
  delta_lf_sign_bit = ChooseBit()
  delta_lf_multi = ChooseBit()
  joint_type = enc_mv_joints(refList)
  mvcomp_sign = enc_mvcomp_sign(refList,useHp,useSubPel,mvcomp)
  mv_class = enc_mvcomp_class
  mvcomp_class0_bits = enc_mvcomp_bits
  mvcomp_class0_fp = enc_mvcomp_fp
  mvcomp_class0_hp = enc_mvcomp_hp
  mvcomp_bits =enc_mvcomp_bits
  mvcomp_fp = enc_mvcomp_fp
  mvcomp_hp =enc_mvcomp_hp

  enc_is_compound = Choose(0, 99) < enc_compound_prob
  enc_single_ref = Choose(LAST_FRAME, ALTREF_FRAME)
  enc_compound_ref_0 = -1
  enc_compound_ref_1 = -1

  // Branches for encoding ref frames. These need to be set such that
  // we end up with ref_frame[i] == enc_ref_frame[i]
  single_ref_p1 = (enc_ref_frame[mi_col][mi_row][0] > GOLDEN_FRAME)
  single_ref_p2 = (enc_ref_frame[mi_col][mi_row][0] == ALTREF_FRAME)
  single_ref_p3 = (enc_ref_frame[mi_col][mi_row][0] > LAST2_FRAME)
  single_ref_p4 = (enc_ref_frame[mi_col][mi_row][0] == LAST2_FRAME)
  single_ref_p5 = (enc_ref_frame[mi_col][mi_row][0] == GOLDEN_FRAME)
  single_ref_p6 = (enc_ref_frame[mi_col][mi_row][0] == ALTREF2_FRAME)
  comp_bwdref_p = (enc_ref_frame[mi_col][mi_row][1] == ALTREF_FRAME)
  comp_bwdref_p1 = (enc_ref_frame[mi_col][mi_row][1] == ALTREF2_FRAME)
  comp_ref_p = (enc_ref_frame[mi_col][mi_row][0] > LAST2_FRAME)
  comp_ref_p1 = (enc_ref_frame[mi_col][mi_row][0] == LAST2_FRAME)
  comp_ref_p2 = (enc_ref_frame[mi_col][mi_row][0] == GOLDEN_FRAME)
  comp_ref_type = !enc_is_unidir_ref(mi_row, mi_col)
  uni_comp_ref_p = (enc_ref_frame[mi_col][mi_row][1] == ALTREF_FRAME)
  uni_comp_ref_p1 = (enc_ref_frame[mi_col][mi_row][1] > LAST2_FRAME)
  uni_comp_ref_p2 = (enc_ref_frame[mi_col][mi_row][1] == GOLDEN_FRAME)

  gm_bit_1 = ChooseBit()
  gm_bit_2 = ChooseBit()
  gm_bit_3 = ChooseBit()

  zero_byte = 0

  frame_width_bits = get_unsigned_bits(enc_stream_width)
  frame_height_bits = get_unsigned_bits(enc_stream_height)
  max_frame_width = enc_stream_width
  max_frame_height = enc_stream_height
  enc_frame_id_numbers_present_flag = ChooseBit()
  frame_id_numbers_present_flag = enc_frame_id_numbers_present_flag
  // Note: Based on the actual syntax, we can signal values as large as
  // delta_frame_id_length = 2 + u(4) <= 17
  // frame_id_length = delta_frame_id_length + 1 + u(3) <= 25
  // But we have a conformance requirement that frame_id_length <= 16
  // This in turn requires delta_frame_id_length <= 15
  delta_frame_id_length = Choose(Max(2, minDeltaFrameIdLength), 15)
  frame_id_length = Choose(Max(delta_frame_id_length + 1, minFrameIdLength), Min(delta_frame_id_length + 1 + 7, 16))

  // frame_size_override_flag can only be 0 if we're using the maximum allowed size
  frame_size_override_flag = enc_large_scale_tile ? 0 : ((enc_frame_width == max_frame_width && enc_frame_height == max_frame_height) ? ChooseBit() : 1)

  cdef_damping = ChooseBits(2)
  cdef_bits = ChooseBits(2)
  cdef_y_pri_strength = av1_choose_interesting(0, 15)
  cdef_y_sec_strength = ChooseBits(2)
  cdef_uv_pri_strength = av1_choose_interesting(0, 15)
  cdef_uv_sec_strength = ChooseBits(2)
  cdef_idx = ChooseBits(cdef_bits)

  lrType = ChooseBits(2)
  lr_unit_shift = ChooseBit()
  lr_unit_extra_shift = ChooseBit()
  lrUVShift = ChooseBit()
  sgr_param_set = ChooseBits(SGRPROJ_PARAMS_BITS)
  useWiener = ChooseBit()
  useSgrproj = ChooseBit()
  restorationType = Choose(RESTORE_NONE, RESTORE_SWITCHABLE_TYPES-1)

  enc_wiener_v = enc_use_extreme_coeff_set ? min : av1_choose_interesting(min, max)
  enc_sgrproj_v = av1_choose_interesting(min, max)

  // The encoder selects superres_denom in [8, 16]; we need to split
  // the superres scale into two parts for encoding
  enc_superres_denom = -1
  use_superres = (superres_denom > SUPERRES_NUM)
  codedDenom = (superres_denom - SUPERRES_DENOM_MIN)

  enc_tile_group_focus = 0
  enc_allow_too_many_tiles = 0
  enc_force_max_tile_cols = 0
  enc_force_max_tile_rows = 0

  enc_pick_half_integer_mv = (Choose(0, 7) == 0)
  // % chance of using each possible pixel pattern
  enc_pixel_pattern_prob = 5
  enc_gm_special_prob = 5
  enc_use_special_coeffs = ChooseBit()

  // This has no effect if it's less than gm_min
  enc_gm_params = gm_min - 1

  enc_no_seg_lf = 0
  enc_no_seg_mode = 0
  enc_small_blocks = 0
  enc_force_4096x4096 = 0

  // Enable the "frame planner", which helps to hit some otherwise-difficult
  // coverage points
  enc_use_frame_planner = 1

  // Should we try to generate a show-existing frame if possible?
  enc_try_show_existing = ChooseBit()

  enc_inter_mode_newmv = ChooseBit()
  enc_comp_mode_newmv = ChooseBit()
  enc_many_mvs = 0
  enc_ref_frames_same_res = ChooseBit()
  enc_increasing_order_hints = 0
  enc_assign_all_possible_refs = 0
  enc_ref_frame_idx = Choose(0, sched_num_valid_refs - 1)
  enc_inter_not_s_frame = ChooseBit()
  enc_clamp_mv = ChooseBit()
  enc_refresh_extra_slot = ChooseBit()

  enc_ref_frame_size_ref = -2

  // Disable special behaviour which is only intended for memory stress streams
  enc_stress_memory = 0
  enc_num_shown_frames_required = -1

  enc_temporal_group_description_present = ChooseBit()
  enc_tile_start_and_end_present = ChooseBit()

  enc_max_recode_count = 10

  enc_force_seg_lvl_globalmv = 0
  enc_force_bug_libaom_2191 = 0
  enc_client_a_custom_set = 0
  enc_encourage_not_hasCols = 0
  enc_encourage_not_hasRows = 0
  enc_encourage_upscale = 0
  enc_maximize_symbols_per_bit = 0
  enc_force_tx_type = -1

  enc_repeat_sequence_header = Choose (0, 9) == 0

  per_stream_0 = 0
  per_stream_1 = 0
  per_stream_2 = 0
  per_stream_3 = 0
  per_stream_4 = 0
  per_stream_5 = 0
  per_stream_6 = 0
  per_stream_7 = 0

  per_frame_0 = 0
  per_frame_1 = 0
  per_frame_2 = 0
  per_frame_3 = 0
  per_frame_4 = 0
  per_frame_5 = 0
  per_frame_6 = 0
  per_frame_7 = 0
}

// Generate streams with 2.0 < level <= 5.3
choose_level5() {
  enc_min_level = 0 // level 2.0
  enc_max_level = 9 // level 5.3
}

// Generate streams with 5.3 < level <= 6.3
choose_level6() {
  enc_min_level = 10 // level 6.0
  enc_max_level = 13 //  level 6.3
}

// Generate specific resolutions
choose_res_64x64() {
  enc_min_level = 0
  enc_max_level = 0
  enc_license_resolution_w = 64
  enc_license_resolution_h = 64
  enc_profile_min_width = 64
  enc_profile_max_width = 64
  enc_profile_max_height = 64
  enc_profile_min_height = 64
}
choose_res_3840x2160() {
  choose_level5()
  enc_min_level = 6
  enc_license_resolution_w = 3840
  enc_license_resolution_h = 2160
  enc_profile_min_width = 3840
  enc_profile_max_width = 3840
  enc_profile_max_height = 2160
  enc_profile_min_height = 2160
}
choose_res_7680x4320() {
  enc_license_resolution_w = 7680
  enc_license_resolution_h = 4320
}


// Generate specific levels
choose_level5_0() {
  choose_res_3840x2160()
  enc_min_level = 6
  enc_max_level = 6
  enc_num_shown_frames_required = 30
}
choose_level5_1() {
  choose_res_3840x2160()
  enc_min_level = 7
  enc_max_level = 7
  enc_num_shown_frames_required = 60
}
choose_level5_2() {
  choose_res_3840x2160()
  enc_min_level = 8
  enc_max_level = 8
  enc_num_shown_frames_required = 120
}
choose_level5_3() {
  choose_res_3840x2160()
  enc_min_level = 9
  enc_max_level = 9
  enc_num_shown_frames_required = 120
}
choose_level6_0() {
  choose_res_7680x4320()
  enc_min_level = 10
  enc_max_level = 10
  enc_num_shown_frames_required = 30
}
choose_level6_1() {
  choose_res_7680x4320()
  enc_min_level = 11
  enc_max_level = 11
  enc_num_shown_frames_required = 60
}
choose_level6_2() {
  choose_res_7680x4320()
  enc_min_level = 12
  enc_max_level = 12
  enc_num_shown_frames_required = 120
}
choose_level6_3() {
  choose_res_7680x4320()
  enc_min_level = 13
  enc_max_level = 13
  enc_num_shown_frames_required = 120
}

choose_stress_streams() {
  choose_license_resolutions()

  enc_frame_cnt = ENC_MAX_FRAMES // set to max to ensure there's enough frames planned to achieve one second
  enc_is_stress_stream = 1
  enc_use_scalability = 0 // makes calculating the number of frames etc quite difficult
  enc_choose_max_bitrate = 0
  // Disable show-existing frames, as these are very easy
  // for decoders to handle
  enc_try_show_existing = 0
}

choose_bug_libaom_2191() {
  enc_choose_max_bitrate = 0
  enc_min_level = 6 // level 5.0
  enc_max_level = 6 // level 5.0
  enc_profile_min_width = 128
  enc_profile_max_width = 128
  enc_profile_min_height = 4351
  enc_profile_max_height = 4352
  enc_still_picture = 0
  enc_use_scalability = 0
  enc_force_bug_libaom_2191 = 1
}

choose_bug_libaom_2191_client_c() {
  enc_choose_max_bitrate = 0
  enc_min_level = 6 // level 5.0
  enc_max_level = 6 // level 5.0
  enc_profile_min_width = 384
  enc_profile_max_width = 395 //384
  enc_profile_min_height = 4351
  enc_profile_max_height = 4352
  enc_still_picture = 0
  enc_use_scalability = 0
  enc_force_bug_libaom_2191 = 1

  enc_speed_group = Choose(0, 3)
  enc_framerate_choice = Choose(0, 2)
  enc_superres_denom = 9
}


// Generate streams using the "max parameters" level.
// This choice function generates three kinds of streams:
//
// * Single huge frames. These exceed the width/height/area limits
//   for level 6.3. To keep the files from getting too huge, and to
//   reflect their most likely use as still images, we only generate
//   one frame in this case.
//   These have enc_huge_picture == 1.
//
// * Streams which are perfectly conformant to some defined level,
//   but which signal the level as the "max parameters" level instead.
//   These have enc_huge_picture == 0, enc_allow_too_many_tiles == 0
//
// * Streams where the size falls within a defined level range, but
//   there are too many tiles for that level.
//   These have enc_huge_picture == 0, enc_allow_too_many_tiles == 1
//
// These are the three cases which seem most likely to be used
// in practice with the max params level.
choose_levelMax() {
  enc_huge_picture = Choose(1, 4)>1

  // Since we use the max params level, we don't need to follow the
  // per-level limit on the number of tiles. Sometimes we do, sometimes
  // we don't, in order to provide as wide a range of test cases as possible.
  enc_allow_too_many_tiles = (!enc_huge_picture) ? ChooseBit() : 0

  // If using a single frame, ensure that it uses the size
  // signalled in the sequence header.
  enc_allow_scaling = !enc_huge_picture

  // If not using enc_huge_picture, then target any valid level
  enc_min_level = 0 // level 2.0
  enc_max_level = 13 // level 6.3

  // Always signal the "max params" level, regardless of the above.
  enc_use_max_params_level = 1

  // Finally:
  // * If allowing too many tiles, *always* use too many tiles. This helps
  //   simplify the logic for detecting these streams in the coverage tool.
  // * In huge-picture mode, sometimes force there to be 64
  //   tile rows and/or 64 tile cols.
  enc_force_max_tile_cols = enc_allow_too_many_tiles ? 1 : (enc_huge_picture ? ChooseBit() : 0)
  enc_force_max_tile_rows = enc_force_max_tile_cols?(Choose(1, 5)>1):(enc_allow_too_many_tiles ? 1 : (enc_huge_picture ? ChooseBit() : 0))
}

// Helper - to speed up testing, instead of doing a few streams with each
// of the above choice functions (and having to build many encoders)
// this function effectively generates streams of each type.
// This is used in the profile_regression*_max() profiles, so that we only
// have to generate one encoder for each profile instead of many.
choose_large() {
  enc_min_level = 1 // level 2.1
  enc_max_level = 13 // level 6.3
  enc_use_max_params_level = (Choose(0, 99) < 20) ? 1 : 0
  enc_huge_picture = (enc_use_max_params_level && !enc_large_scale_tile) ? ChooseBit() : 0
  enc_allow_scaling = !enc_huge_picture
  //enc_allow_too_many_tiles = (!enc_huge_picture) ? ChooseBit() : 0
  enc_force_max_tile_cols = enc_allow_too_many_tiles ? 1 : (enc_huge_picture ? ChooseBit() : 0)
  enc_force_max_tile_rows = enc_allow_too_many_tiles ? 1 : (enc_huge_picture ? ChooseBit() : 0)
}

// Allow frames to be resized
config_scaling() {
  enc_allow_scaling = (Choose(0, 99) < 80) // Keep a 20% chance of forcing all frames to be the same size
}

choose_max_bitdepth() {
  enc_bit_depth = (enc_profile == 2) ? 12 : 10
}

choose_12bit() {
  enc_bit_depth = 12
}

choose_profile0() {
  enc_profile = 0
}

choose_profile1() {
  enc_profile = 1
}

choose_profile2() {
  enc_profile = 2
}

// Force all frames to be intra-only or key frames
// Due to the way the frame scheduler works, the simplest way to ensure
// this is to:
// * Make all frames shown, so that no frame links are generated
//   (as any frame with 1 link must be either show-existing or inter,
//    and any frame with >1 link must be inter)
// * When a frame could inter or intra-only, always pick intra-only.
choose_intra_only() {
  enc_hide_frame_prob = 0
  enc_frame_is_inter = 0
}

// Try to hit difficult coverage points in intrabc:
// * Always pick (at least) the max height for level 2.0
// * Always have exactly one tile
//   (this means it cannot be used with choose_level6() or choose_levelMax(),
//    since some of the larger streams are forced to use tiles)
// * Set various parameters to ensure intrabc is always possible
choose_intrabc() {
  // Intrabc can only be used on intra-only or key frames
  choose_intra_only()

  // Always set the width or height to exactly the level 2.0 limit.
  // This is because, to reach MV class 9, we need to be able to
  // encode a motion vector diff of +/-1024 pixels (class 9, mvbits 0)
  // and +/- 1280 (class 9, mvbits 0x100), while having the resulting
  // MV still point within the frame. This requires the frame to have
  // a length greater than 1024/1280 in the relevant direction.
  //
  // For "standard" mvs we can achieve both, as MVs can go off the
  // edge of the image
  //
  // For "intrabc" mvs (which we are targetting here) the MVs need to
  // be in bounds, so +/- 1280 is not possible for vertical MVs
  //
  // When in large scale tile mode, we set enc_profile_min_height to
  // enc_frame_min_height_limit. We don't really care about this case,
  // but setting it too big would mean we couldn't find a valid frame
  // size for low levels.
  enc_profile_min_height = ((enc_chooseWidthFirst || enc_large_scale_tile) ?
                            enc_frame_min_height_limit :
                            Min (1152,
                                 level_max_pic_size[enc_base_level][0]/enc_frame_min_width_limit))

  enc_profile_min_width = enc_chooseWidthFirst ? Min(2048,level_max_pic_size[enc_base_level][0]/enc_frame_min_height_limit) : enc_frame_min_width_limit

  // Maximise the number of frames on which these resolutions hold
  enc_use_decoder_model = 0
  enc_use_scalability = 0
  enc_allow_scaling = 0

  // Force there to be exactly one tile per frame
  enc_exactly_one_tile = 1

  // Encourage large mvs
  enc_mv_small = 0
  enc_try_pick_large_dv = 1

  // Make sure that intrabc is always allowed
  seq_choose_screen_content_tools_bit = 1
  allow_screen_content_tools = 1
  enable_superres = 0
  allow_intrabc = 1
  enc_force_intrabc = 1
}

choose_level5_intrabc() {
  choose_intrabc()
  enc_min_level = 2 // level 3.0
  enc_max_level = 6 // level 5.0

  // we are optimising for vertical resolution >= 1280
  enc_chooseWidthFirst = 0
  enc_profile_min_height = 2448
  enc_profile_min_width = enc_frame_min_width_limit
}

choose_intrabc_client_a() {
  choose_intrabc()
  //level_max_pic_size[enc_base_level][0]/enc_frame_min_width_limit in level2.0 this gives us:
  // 147456 / 144 == 1024, we take the closest client_a resolution under that: 352x288
  //level_max_pic_size[enc_base_level][0]/enc_frame_min_height_limit in level2.0 this gives us:
  // 147456 / 128 == 1152, we take the closest client_a resolution under that: 352x288
  enc_profile_min_height = enc_chooseWidthFirst ? 288 : 288
  enc_profile_min_width = enc_chooseWidthFirst ? 352 : 352
}

choose_intrabc_client_a_level5() {
  choose_level5_intrabc()
  enc_profile_min_height = 128
}

// Choice function to boost palette coverage
choose_palette() {
  // Force the max bit depth for this profile
  choose_max_bitdepth()
  // Ensure that all frames have palette mode enabled
  seq_choose_screen_content_tools_bit = 1
  allow_screen_content_tools = 1
  enc_enable_palette = 1
  enc_palette_prob = Choose(50, 100)
  // Disable sub-8x8 blocks, as these can't use palette mode
  enc_min_partition_size = BLOCK_8X8
}

choose_width16() {
  enc_min_level = 0
  enc_max_level = 0
  enc_chooseWidthFirst  = 1
  enc_profile_min_width = 16
  enc_profile_max_width = 16
}

choose_height16() {
  enc_min_level = 0
  enc_max_level = 0
  enc_chooseWidthFirst  = 0
  enc_profile_min_height = 16
  enc_profile_max_height = 16
}

choose_max_8192x4352() {
  choose_level6()
  enc_profile_min_width = 8192
  enc_profile_max_width = 8192
  enc_profile_min_height = 4352
  enc_profile_max_height = 4352
  enc_use_scalability = 0
}

choose_width16384() {
  choose_level6()
  enc_chooseWidthFirst  = 1
  enc_profile_min_width = 16384
  enc_profile_max_width = 16384
  enc_profile_max_height = 2176
  enc_use_scalability = 0
}

choose_height8704() {
  choose_level6()
  enc_chooseWidthFirst  = 0
  enc_profile_min_height = 8704
  enc_profile_max_height = 8704
  enc_profile_max_width = 4096
  enc_use_scalability = 0
}

choose_width3840() {
  choose_level5()
  enc_min_level = 2
  enc_chooseWidthFirst  = 1
  enc_profile_min_width = 3840
  enc_profile_max_width = 3840
  enc_profile_max_height = 2160
  enc_use_scalability = 0
}

choose_height2160() {
  choose_level5()
  enc_min_level = 2
  enc_chooseWidthFirst  = 0
  enc_profile_min_height = 2160
  enc_profile_max_height = 2160
  enc_profile_max_width = 3840
  enc_use_scalability = 0
}

choose_tile_groups() {
  enc_tile_group_focus = 1
  enc_superres_prob = 0
  enc_tile_group_mode = ENC_TG_MODE_UNIFORM
  enc_use_128x128_superblock = 0
  enc_tile_width_sb = 1
  enc_tile_height_sb = 1
  uniform_tile_spacing_flag = 1
}

// Level 5.x allows up to 8 tile columns and 64 tiles
choose_tile_groups_level5() {
  choose_level5()
  enc_min_level = 6
  choose_tile_groups()
  enc_tile_rows = 8
  enc_tile_cols = 8
  enc_profile_min_width = 512
  enc_profile_max_width = 512
  enc_profile_min_height = 512
  enc_profile_max_height = 512
  enc_hide_frame_prob = 50
}

// Level 5.x allows up to 8 tile columns and 64 tiles
choose_tile_groups_level5_client_a() {
  choose_tile_groups_level5()
  enc_profile_min_width = 1280
  enc_profile_max_width = 1280
  enc_profile_min_height = 720
  enc_profile_max_height = 720
  uniform_tile_spacing_flag = 0
  enc_force_max_tile_cols = 1
  enc_force_max_tile_rows = 1
}

// Level 6.x allows up to 16 tile columns and 128 tiles
choose_tile_groups_level6() {
  choose_level6()
  choose_tile_groups()
  enc_tile_rows = 8
  enc_tile_cols = 16
  enc_profile_min_width = 1024
  enc_profile_max_width = 1024
  enc_profile_min_height = 512
  enc_profile_max_height = 512
}

// Bias toward generating specific patterns of pixels
// which are useful for filling out range coverage
choose_pixel_pattern() {
  // Our pixel pattern generator requires palette
  // blocks, so make sure we generate lots of those.
  choose_palette()
  enc_pixel_pattern_prob = 25
}

choose_warped() {
  choose_pixel_pattern()

  // Ensure all frames are the same size
  enc_allow_scaling = 0
  enable_superres = 0

  // Disable all loop filtering. This gives the best chance
  // of the important pixel patterns being stored into the reference buffers
  filter_level = 0
  enable_cdef = 0
  enable_loop_restoration = 0

  // Settings for global motion:
  // Always pick AFFINE or ROTZOOM modes, and give a large chance
  // of picking one of the two special cases defined in av1_global_motion.c
  // This helps cover extreme values in the warp filter and setup_shear_params()
  gm_bit_1 = 1
  gm_bit_2 = ChooseBit()
  gm_bit_3 = 0
  enc_gm_special_prob = 75

  // Settings for warped motion
  // To help hit the extreme values in the warp estimation process,
  // we strongly boost the chance of using warped motion.
  enable_warped_motion = Choose(0, 99) < 95
  enc_allowed_motion_modes = (enable_warped_motion && (Choose(0, 99) < 80)) ? 2 : (Choose(0, 99) < 80)
  switchable_motion_mode = (enc_allowed_motion_modes > 0)
  motion_mode = ( Choose(0, 99) < 95 ) ? WARPED_CAUSAL : (ChooseBit() ? OBMC_CAUSAL : SIMPLE_TRANSLATION)
}

choose_inter_max_range() {
  choose_pixel_pattern()

  // Always allow fractional MVs
  seq_choose_integer_mv_bit = 0
  seq_force_integer_mv_bit = 0

  // Ensure all frames are the same size
  enc_allow_scaling = 0
  enable_superres = 0

  // Disable all loop filtering
  filter_level = 0
  enable_cdef = 0
  enable_loop_restoration = 0

  enable_warped_motion = 0
  enc_allowed_motion_modes = 0
  enc_pick_half_integer_mv = 1 // Make all motion vectors 1/2 pixel aligned
}

choose_diffwtd() {
  choose_inter_max_range()

  // Boost the amount of compound blocks
  pred_mode = 1
  enc_compound_prob = 100
  // Make as many blocks as possible use difference-weighted compound
  compound_group_idx = 1
  compound_mask_type = 1
}

choose_loop_restoration() {
  choose_pixel_pattern()

  // Disable all loop filtering except for loop-restoration
  filter_level = 0
  enable_cdef = 0
  enable_superres = 0
  enable_loop_restoration = 1

  // loop-restoration and intrabc are incompatible
  allow_intrabc = 0
  // Always use some form of loop-restoration on each frame
  lrType = Choose(1, 3)
  // Bias the self-guided filter toward the parameter set which gives
  // the most extreme values
  sgr_param_set = ChooseBit() ? 15 : ChooseBits(SGRPROJ_PARAMS_BITS)
}

choose_cdef() {
  choose_pixel_pattern()
  allow_intrabc = 0 // CDEF and intrabc are incompatible
  filter_level = 0 // Disable deblocking since that happens before cdef
  enable_cdef = 1
  // Give each frame 1 parameter set, with parameters aimed at
  // getting the largest intermediate values we can
  cdef_bits = 0
  cdef_damping = 3
  cdef_y_pri_strength = 15
  cdef_y_sec_strength = 3
  // The CDEF filter pattern requires blocks which are not skipped,
  // but which nonetheless contain no coefficients.
  enc_skip_prob = Choose(0, 50)
  enc_empty_block_prob = Choose(50, 100)
}

choose_cfl() {
  // Enable pixel patterns in the Y plane, so that we can generate
  // the necessary patterns. But disable palette entirely in the
  // UV planes, since UV palette and CfL are mutually exclusive
  choose_pixel_pattern()
  enc_palette_uv = 0
  // Boost the chance of using CfL when we might have
  // the desired pixel pattern in the Y plane
  intra_mode_uv = (!lossless && sb_size == BLOCK_32X32 && enc_palette_y && ChooseBit()) ? UV_CFL_PRED : Choose(0,is_cfl_allowed(sb_size) ? UV_INTRA_MODES-1 : INTRA_MODES-1)
  // Boost the chance of choosing the alphas to be +/- 16
  cfl_alpha_u = ChooseBit() ? (CFL_ALPHABET_SIZE-1) : Choose(0, CFL_ALPHABET_SIZE-2)
  cfl_alpha_v = ChooseBit() ? (CFL_ALPHABET_SIZE-1) : Choose(0, CFL_ALPHABET_SIZE-2)
}

choose_film_grain() {
  film_grain_params_present = 1
  apply_grain = 1

  // Try to cover the 8 interesting coeff sets (all +ve / all -ve for each
  // of Y, U, and V) as evenly as possible
  enc_film_grain_signs = ChooseAll(0, 7, FILM_GRAIN_SIGNS)
  ar_coeff_y = ((enc_film_grain_signs >> 2) & 1) ? 0 : 255
  ar_coeff_u = ((enc_film_grain_signs >> 1) & 1) ? 0 : 255
  ar_coeff_v = ((enc_film_grain_signs >> 0) & 1) ? 0 : 255

  // Bias toward parameters which help with range coverage
  scaling_shift = 0
  ar_coeff_lag = 3
  ar_coeff_shift = 0
  grain_scale_shift = 0
  cb_mult = ChooseBit() ? 0 : 255
  cb_luma_mult = ChooseBit() ? 0 : 255
  cb_offset = ChooseBit() ? 0 : 511
  cr_mult = ChooseBit() ? 0 : 255
  cr_luma_mult = ChooseBit() ? 0 : 255
  cr_offset = ChooseBit() ? 0 : 511
}

// A profile that tries to exercise range maximums and minimums in the
// transform code.
choose_txfm() {
  choose_max_bitdepth()
  enc_use_special_coeffs = 1
  using_qmatrix = 0
  // Code all frames using base_qindex = 0. This makes it very
  // likely for each frame to either be lossless (if delta-qindex is
  // not set), or lossy but with qindex=0 (if delta-qindex is
  // set and negative)
  base_qindex = 0
  // Always use the smallest tx size possible, to bias
  // toward having more 4x4 transform units
  txDepth = Min(maxTxDepth + 1, MAX_TX_DEPTH)
  txfm_split = 1
}

// A profile that tries to generate ADST_ADST transforms with special
// coefficients.
choose_txfm_adst() {
  choose_txfm()

  // Getting a transform type of ADST_ADST and a quantiser of 4 is no
  // mean feat. The constraints are:
  //
  //   (1) qindex must be non-zero in read_tx_type (otherwise the
  //       transform type is forced to DCT_DCT).
  //
  //   (2) qindex must be zero when passed to setup_plane_dequants
  //       (it's used as the AC quantiser index, so must be 0 to get 4
  //       from the table).
  //
  // If segmentation is disabled, the qindex in read_tx_type is just
  // base_qindex. If delta_q_present is set, the qindex passed to
  // setup_plane_dequants is current_qindex. Unfortunately, the
  // adaptation for current_qindex is clamped so that current_qindex
  // stays at least 1.
  //
  // With segmentation enabled, we can have base_qindex = 2;
  // current_qindex = 1 and get_segdata_idx(seg, SEG_LVL_ALT_Q) = -1
  // which gives us what we want. Phew!

  seg_enabled = 1
  feature_enabled = (feature == SEG_LVL_ALT_Q)
  decode_data = -1

  base_qindex = 2

  // This is the delta across the frame. We encode a drop of 1 every
  // time but will be immediately clamped to current_qindex = 1.
  delta_q_present = 1
  abs = 1
  delta_q_sign_bit = 1

  // We definitely don't want lossless mode.
  enc_lossless_prob = 0

  // We want a transform size of 4x4
  enc_target_tx_size = TX_4X4
  enc_min_partition_size = BLOCK_4X4
  enc_max_partition_size = BLOCK_4X4

  // Don't use a reduced tx set
  reduced_tx_set_used = 0

  // Force a transform type of ADST_ADST
  enc_force_tx_type = ADST_ADST

  // Never skip (we always want some actual coefficients)
  skip_mode_allowed = 0
  skip_coeff = 0
}

// Select parameters to test decoders' memory latency and bandwidth,
// by forcing as many cache misses as possible
choose_memory_performance() {
  // Disable features which would make our life harder
  enc_still_picture = 0
  enc_use_frame_planner = 0
  enc_use_scalability = 0
  enc_frame_id_numbers_present_flag = 0
  enc_enable_order_hint = 0

  pred_mode = 0 // No compound prediction
  enc_exactly_one_tile = 1
  enc_no_seg_mode = 1 // Disable SEG_LVL_{REF_FRAME, SKIP, GLOBALMV} as these
                      // can interfere with our mode selection
  enc_allowed_motion_modes = 0 // Disable OBMC
  enc_use_bit_bucket = 0 // Turn off rate control

  // Disable all screen content tools:
  // * Palette mode can use many bits, and we don't need it here
  // * Intrabc may also be worth testing, but via a different profile
  // * Integer MV interferes with trying to get the decoder to perform
  //   interpolation on every inter block
  seq_choose_screen_content_tools_bit = 0
  seq_force_screen_content_tools_bit = 0

  // Make all possible blocks be 4x4 inter blocks.
  // Note that these must be single-ref blocks, since compound blocks
  // must be at least 8x8
  enc_inter_prob = 100
  enc_min_partition_size = BLOCK_4X4
  enc_max_partition_size = BLOCK_4X4

  // Enable special behaviour (mode selection, MV selection, etc.)
  enc_stress_memory = 1
  inter_mode = enc_pick_inter_mode_stress(mi_row, mi_col)
  comp_inter_mode = enc_pick_comp_inter_mode_stress(mi_col)
}

profile_dav1d() {
  default()
  // dav1d only supports 1 tile per tile group
  enc_tile_group_mode = ENC_TG_MODE_MAX_TG
  // dav1d doesn't support scalability
  enc_use_scalability = 0
  // dav1d doesn't support short signalling
  enc_allow_short_signalling = 0

  // Don't allow only allow IDENTITY as a global motion type. This is because
  // dav1d incorrectly errors out when the warp params look dodgy in
  // parse_frame_hdr.
  gm_bit_1 = 0

  // Dav1d successfully reads this bit, but ignores it in ipred.c and
  // the associated hand-written assembly code (treating it as if it
  // is always true).
  enable_intra_edge_filter = 1

  // Dav1d has not implemented superres
  enable_superres = 0

  seg_enabled = 0

  // Always show all the frames. Since MV stuff is borked, I want to
  // see frame 1 which has a hope of being right!
  enc_hide_frame_prob = 0

  // Temporarily switch off all loop-filtering. Maybe this will tease out what's going wrong?
  filter_level = 0
  enable_cdef = 0
  enable_loop_restoration = 0
}

// Helper function to generate "dense" streams.
// By this, we mean streams with a comparatively number of symbols per bit.
// As of the initial version of this function, this boosts the average
// symbols per bit from ~0.5 to ~1.2; compare to aomenc, which fairly consistently
// generates streams with around 1 symbol per bit.
choose_symbols_per_bit() {
  // Turn off various things which deliberately cause low density
  extra_tile_padding = 0
  extra_obu_padding = 0
  enc_pixel_pattern_prob = 0
  enc_coef_sign_pattern = 0
  enc_use_frame_planner = 0
  enc_special_lossless = 0

  // Enable special behaviour
  enc_maximize_symbols_per_bit = 1

  txfm_split = enc_choose_from_cdf(tile_txfm_partition_cdf[ctx], 2)
  txDepth = enc_choose_from_cdf(tile_tx_cdfs[maxTxDepth][tx_depth_ctx(mi_row, mi_col, max_tx_size)], depthCdfSize)
  cfl_alpha_signs = enc_choose_from_cdf(tile_cfl_sign_cdf, CFL_JOINT_SIGNS)
  cfl_alpha_u = enc_choose_from_cdf(tile_cfl_alpha_cdf[CFL_CONTEXT_U(cfl_alpha_signs)], CFL_ALPHABET_SIZE)
  cfl_alpha_v = enc_choose_from_cdf(tile_cfl_alpha_cdf[CFL_CONTEXT_V(cfl_alpha_signs)], CFL_ALPHABET_SIZE)
  drl_mode = enc_choose_from_cdf(tile_drl_cdf[drl_ctx(ref_frame_type,idx)], 2)
  inter_mode = enc_pick_inter_mode_arith_stress()
  comp_inter_mode = NEAREST_NEARESTMV + enc_choose_from_cdf(tile_inter_compound_mode_cdf[av1_mode_context_analyzer(-1)], INTER_COMPOUND_MODES)
  wedge_index = enc_choose_from_cdf(tile_wedge_idx_cdf[bsize], 16)
  wedge_sign = ChooseBit()
  mask_type = ChooseBits(MAX_SEG_MASK_BITS)
  compound_mask_type = enc_choose_from_cdf(tile_compound_mask_type_cdf[sb_size], 2)
  interintra = enc_choose_from_cdf(tile_interintra_cdf[size_group_lookup[sb_size]], 2)
  interintra_mode = enc_choose_from_cdf(tile_interintra_mode_cdf[size_group_lookup[sb_size]], INTERINTRA_MODES)
  wedge_interintra = enc_choose_from_cdf(tile_wedge_interintra_cdf[sb_size], 2)
  use_obmc = enc_choose_from_cdf(tile_obmc_cdf[sb_size], 2)
  motion_mode = enc_choose_from_cdf(tile_motion_mode_cdf[sb_size], MOTION_MODES)
  useWiener = enc_choose_from_cdf(tile_wiener_cdf, 2)
  useSgrproj = enc_choose_from_cdf(tile_sgrproj_cdf, 2)
  restorationType = enc_choose_from_cdf(tile_switchable_restore_cdf, RESTORE_SWITCHABLE_TYPES)
  abs = enc_choose_from_cdf(tile_delta_q_cdf, DELTA_Q_SMALL+1)
  abs_lf = enc_choose_from_cdf((deltaLfCdfIdx >= 0 ? tile_delta_lf_multi_cdf[deltaLfCdfIdx] : tile_delta_lf_cdf), DELTA_LF_SMALL+1)
  default_intra_mode_y = enc_choose_from_cdf(tile_kf_y_cdf[above_intra_mode_ctx[mi_col]][left_intra_mode_ctx[mi_row & mi_mask2]], INTRA_MODES)
  intra_mode_y = enc_choose_from_cdf(tile_y_mode_cdf[size_group_lookup[sb_size]], INTRA_MODES)
  intra_mode_uv = enc_choose_from_cdf(tile_uv_mode_cdf[is_cfl_allowed(sb_size)][y_mode], INTRA_MODES + is_cfl_allowed(sb_size))
  angle_delta_y = enc_choose_from_cdf(tile_angle_delta_cdf[y_mode - V_PRED], (2*MAX_ANGLE_DELTA + 1))
  angle_delta_uv = enc_choose_from_cdf(tile_angle_delta_cdf[uv_mode - V_PRED], (2*MAX_ANGLE_DELTA + 1))
  has_palette_y = enc_choose_from_cdf(tile_palette_y_mode_cdf[ num_pels_log2_lookup[sb_size] - num_pels_log2_lookup[PALETTE_MIN_BSIZE] ][ palette_y_ctx(miRow, miCol) ], 2)
  has_palette_uv = enc_choose_from_cdf(tile_palette_uv_mode_cdf[ ( PaletteSizeY > 0 ) ? 1 : 0 ], 2)
  use_filter_intra = enc_choose_from_cdf(tile_filter_intra_cdfs[sb_size], 2)
  filter_intra_mode = enc_choose_from_cdf(tile_filter_intra_mode_cdf, FILTER_INTRA_MODES)
  use_intrabc = can_use_intrabc ? enc_choose_from_cdf(tile_intrabc_cdf, 2) : 0
  compound_group_idx = enc_choose_from_cdf(tile_compound_group_idx_cdf[compound_group_idx_ctx(mi_row, mi_col)], 2)
  compound_idx = enc_choose_from_cdf(tile_compound_idx_cdf[compound_idx_ctx(mi_row, mi_col)], 2)
  enc_interp_filter = enc_choose_from_cdf(tile_switchable_interp_cdf[interp_filter_ctx(mi_row, mi_col, dir)], SWITCHABLE_FILTERS)
  skip_coeff = enc_choose_from_cdf(tile_mbskip_cdf[skip_coeff_ctx(mi_row, mi_col)], 2)
  skip_mode = enc_choose_from_cdf(tile_skip_mode_cdf[skip_mode_ctx(mi_row, mi_col)], 2)
  pred_flag = enc_choose_from_cdf(tile_segment_pred_cdf[pred_flag_ctx(mi_row, mi_col)], 2)
  // Note: coded_id always uses a CDF of length 'MAX_SEGMENTS', but we're only
  // allowed to code values in the range [0, last_active_seg_id].
  // The function enc_choose_from_cdf is written to be able to handle this case cleanly.
  coded_id = enc_choose_from_cdf(tile_spatial_pred_seg_cdf[cdfNum], last_active_seg_id+1)

  // Reference frame selection
  comp_mode = enc_choose_from_cdf(tile_comp_inter_cdf[comp_mode_ctx()], 2)
  comp_ref_p = enc_choose_from_cdf(tile_comp_ref_cdf[get_pred_context_last12_or_last3_gld()][0], 2)
  comp_ref_p1 = enc_choose_from_cdf(tile_comp_ref_cdf[get_pred_context_last_or_last2()][1], 2)
  comp_ref_p2 = enc_choose_from_cdf(tile_comp_ref_cdf[get_pred_context_last3_or_gld()][2], 2)
  comp_bwdref_p = enc_choose_from_cdf(tile_comp_bwdref_cdf[get_pred_context_brfarf2_or_arf()][0], 2)
  comp_bwdref_p1 = enc_choose_from_cdf(tile_comp_bwdref_cdf[get_pred_context_brf_or_arf2()][1], 2)
  single_ref_p1 = enc_choose_from_cdf(tile_single_ref_cdf[get_pred_context_fwd_or_bwd()][0], 2)
  single_ref_p2 = enc_choose_from_cdf(tile_single_ref_cdf[get_pred_context_brfarf2_or_arf()][1], 2)
  single_ref_p3 = enc_choose_from_cdf(tile_single_ref_cdf[get_pred_context_last12_or_last3_gld()][2], 2)
  single_ref_p4 = enc_choose_from_cdf(tile_single_ref_cdf[get_pred_context_last_or_last2()][3], 2)
  single_ref_p5 = enc_choose_from_cdf(tile_single_ref_cdf[get_pred_context_last3_or_gld()][4], 2)
  single_ref_p6 = enc_choose_from_cdf(tile_single_ref_cdf[get_pred_context_brf_or_arf2()][5], 2)
  comp_ref_type = enc_choose_from_cdf(tile_comp_ref_type_cdf[get_pred_context_comp_ref_type()], 2)
  uni_comp_ref_p = enc_choose_from_cdf(tile_uni_comp_ref_cdf[get_pred_context_fwd_or_bwd()][0], 2)
  uni_comp_ref_p1 = enc_choose_from_cdf(tile_uni_comp_ref_cdf[get_pred_context_last2_or_last3_gld()][1], 2)
  uni_comp_ref_p2 = enc_choose_from_cdf(tile_uni_comp_ref_cdf[get_pred_context_last3_or_gld()][2], 2)

  // The following values are handled by special cases in other functions:
  // * Palette data (enc_pick_palette_data() and enc_palette_index())
  // * inter_tx_type and intra_tx_type (enc_pick_tx_type())
  // * Partition choice (encode_partition())

  enc_coef = enc_choose_coef_from_cdf(c, enc_last_coef, adjusted_max_coef, txSz, txSzCtx, plane, blockx, blocky)
}

// Choice function to stress arithmetic decode.
// For this case, we want to maximize the average number of symbols per
// decoded pixel, as this ratio determines the required speed of the
// arithmetic decode unit as compared to the pixel decode pipeline.
//
// Note that there's no explicit limit on how many *symbols* per pixel
// can be decoded, but there is an explicit limit on bitrate (and so on
// average bits per pixel at max resolution).
// So, in order to maximize the number of symbols per pixel, we start
// with the "densest" streams we can in terms of symbols per bit, then
// add further tweaks to increase the number of symbols used.
choose_symbols_per_px() {
  choose_symbols_per_bit()
  enc_last_coef = enc_pick_max_coefs(seg_eob)
}

profile_regression0() {
  default()
  config_scaling()
  choose_profile0()
}

profile_regression1() {
  default()
  config_scaling()
  choose_profile1()
}

profile_regression2() {
  default()
  config_scaling()
  choose_profile2()
}

profile_regression0_max() {
  default()
  choose_large()
  config_scaling()
  choose_profile0()
}

profile_regression1_max() {
  default()
  choose_large()
  config_scaling()
  choose_profile1()
}

profile_regression2_max() {
  default()
  choose_large()
  config_scaling()
  choose_profile2()
}

profile_license_resolutions() {
  default()
  choose_license_resolutions()
  choose_reduce_bitrate()
}

choose_license_resolutions() {
  enc_is_license_resolution = 1
  // Allow any level - which one is used will depend on the frame size and
  // selected framerate
  enc_min_level = enc_large_scale_tile ? Max( enc_tile_cols <= 4 ? 0 : (enc_tile_cols <= 6 ? 2 : (enc_tile_cols <= 8 ? 4 : 10 ) ) ,enc_tile_cols*enc_tile_rows <= 8 ? 0 : (enc_tile_cols*enc_tile_rows <= 16 ? 2 : (enc_tile_cols*enc_tile_rows <= 32 ? 4 : (enc_tile_cols*enc_tile_rows <= 64 ? 6 : 10 ) ) )) : 0
  enc_max_level = 13
  enc_profile_max_width = enc_license_resolution_w
  enc_profile_max_height = enc_license_resolution_h

  enc_frame_cnt = enc_licence_frame_cnt // Can only produce 1 frame in large scale tile mode

  // This rather circular definition means that reading
  // enc_tile_list_frame_cnt with u(0) has no effect. This is what we
  // want because, by the time the scheduler tries to read it, we have
  // already run encode_choose_license_resolution() to set the value.
  enc_tile_list_frame_cnt = enc_tile_list_frame_cnt

  enc_still_picture = 0 // Don't generate still images in this mode
  enc_allow_scaling = 0 // Force all frames to be the same size (modulo superres)

  // Some license resolution streams have fixed target bitrates,
  // while others want to just use the default settings (copied here)
  enc_bit_bucket_fill_rate = enc_license_resolution_specific_bitrate ? ((enc_target_bits_per_frame * 100 + (frameArea/2)) / (frameArea)) : (intra_only ? 50 : 20)
  enc_min_bpp =              enc_license_resolution_specific_bitrate ? ((enc_bit_bucket_fill_rate + 1) / 2)                              : (intra_only ? 30 :  0)
  enc_max_bpp =              enc_license_resolution_specific_bitrate ? ((3 * enc_bit_bucket_fill_rate + 1) / 2)                          : (intra_only ? 70 : 40)
}

profile_license_resolutions_profile0() {
    profile_license_resolutions()
    choose_profile0()
}

profile_license_resolutions_profile1() {
    profile_license_resolutions()
    choose_profile1()
}

profile_license_resolutions_profile2() {
    profile_license_resolutions()
    choose_profile2()
}

profile_license_resolutions_profile2_12() {
    profile_license_resolutions()
    choose_profile2()
    choose_12bit()
}

choose_client_a_custom() {
  enc_profile_min_width = 144
  enc_profile_min_height = 128
  enc_frame_min_width_limit = 144
  enc_frame_min_height_limit = 128
  enc_client_a_custom_set = 1
}

//Choice function to hit range equation Upscale1 with min_width == 144 (Deterimine limit using av1_upscale_range.py)
choose_client_a_upscale() {
  enc_frame_min_width_limit = 156
  enc_profile_min_width = 156
  enc_encourage_upscale = 1
}

choose_upscale() {
  //Profile 0/2: max stepX = 15292 for chroma planes,
  //    when superresDenom = 9, upscaledWidth = 30
  //    => FrameWidth = 27, chromaUpscaledWidth = 15, chromaWidth = 14

  enc_encourage_upscale = 1
  /*enable_superres = 1
  // The encoder selects superres_denom in [8, 16]; we need to split
  // the superres scale into two parts for encoding
  enc_superres_denom = -1
  use_superres = (superres_denom > SUPERRES_NUM)
  codedDenom = (superres_denom - SUPERRES_DENOM_MIN)*/
}

choose_not_hascols() {
  enc_split_prob = 0
  enc_encourage_not_hasCols = 1
}

choose_not_hasrows() {
  enc_split_prob = 0
  enc_encourage_not_hasRows = 1
}

choose_many_mv_found() {
  seg_enabled = 0
  enc_frame_planner_skip_mode_prob = 0
  enc_skip_prob = 0
  enc_inter_mode_newmv = 1
  enc_inter_prob = 100
  enc_frame_is_inter = 1
  enc_comp_mode_newmv = 1
  enc_lossless_prob = 0
  enc_many_mvs = 1
  enc_clamp_mv = 0
  enc_still_picture = 0
  can_use_ref = 1
  enc_increasing_order_hints = 1
  enc_enable_order_hint = 1
  enc_order_hint_bits = (1<<3)
  enable_ref_frame_mvs = 1
  error_resilient_mode = 0
  enc_ref_frames_same_res = 1
  enc_use_scalability = 0
  enc_allow_short_signalling = 0
  enc_assign_all_possible_refs = 1
  enc_try_show_existing = 0
  enc_inter_not_s_frame = 1
  // increase range of MVs
  seq_choose_integer_mv_bit = 1
  allow_high_precision_mv = 1
  // increase the chance
  enc_frame_cnt = 20
}

choose_warp_local() {
  error_resilient_mode = 0
  enc_frame_planner_skip_mode_prob = 0
  enc_skip_prob = 0
  enc_inter_mode_newmv = 1
  enc_inter_prob = 100
  enc_frame_is_inter = 1
  enc_comp_mode_newmv = 1
  enc_lossless_prob = 0
  enc_many_mvs = 1
  enc_clamp_mv = 0
  enc_still_picture = 0
  can_use_ref = 1
  enc_increasing_order_hints = 1
  enc_enable_order_hint = 1
  enc_order_hint_bits = (1<<3)
  enable_ref_frame_mvs = 1
  error_resilient_mode = 0
  enc_ref_frames_same_res = 1
  enc_use_scalability = 0
  enc_allow_short_signalling = 0
  enc_assign_all_possible_refs = 1
  enc_try_show_existing = 0
  enc_inter_not_s_frame = 1
  cur_frame_force_integer_mv = 0
  seq_force_integer_mv_bit = 0
  seq_choose_integer_mv_bit = 0
  enc_compound_prob = 0
  encNumSegments = (SEG_LVL_REF_FRAME+1)
  seg_enabled = 1
  seg_update_map = 0
  enc_seg_update_data = 1
  enc_force_seg_lvl_ref_frame_last_frame = 1
  primary_ref_frame = Choose(0,PRIMARY_REF_NONE-1)

  enable_warped_motion = 1
  enc_allowed_motion_modes = 2
  gm_bit_1 = 0 // not global motion
  motion_mode = WARPED_CAUSAL
}

choose_segmentation() {
  enc_special_lossless = 0
  seg_enabled = 1
  enc_seg_update_data = 1
  encNumSegments = MAX_SEGMENTS
  enc_enable_feature = 1
}

choose_motion_mode() {
  choose_warp_local()

  cur_frame_force_integer_mv = 1

  // reset these to default
  gm_bit_1 = ChooseBit()
  motion_mode = ChooseBit() ? WARPED_CAUSAL : (ChooseBit() ? OBMC_CAUSAL : SIMPLE_TRANSLATION)
}

choose_mode_to_txfm() {
  choose_intra_only()

  enc_enable_palette = 0
  enc_monochrome = 0
  enc_lossless = 0
  enc_special_lossless = 0
}

choose_color_primaries() {
  isSRGB = 0
  useColorDescription = 1
  enc_hit_color_primaries_branch = 1
}

choose_selfguided() {
  allow_intrabc = 0 // Intrabc is incompatible with any loop filtering
  enc_lossless = 0
  enc_special_lossless = 0
  enable_loop_restoration = 1

  // Make sure that loop restoration is always applied
  useWiener = 1
  useSgrproj = 1
  restorationType = Choose(RESTORE_WIENER, RESTORE_SGRPROJ)
  frame_restoration_type = 3 // Remaps to RESTORE_SGRPROJ
}

choose_tx_type_in_set_inter() {
  enc_still_picture = 0
  enc_monochrome = 0

  enc_lossless_prob = 0
  enc_lossless = 0
  enc_special_lossless = 0
  base_qindex = Choose(1, (1<<QINDEX_BITS)-1) // needs to be >0 to read TxType
  seg_enabled = 0

  enc_skip_prob = 0

  enc_barrier_prob = 0
  enc_inter_prob = 100
  enc_frame_is_inter = 1

  extra_mode_bit = 1 // we want TX_MODE_SELECT
  reduced_tx_set_used = 0

  enc_subsampling_x = 0
  enc_subsampling_y = 0

  enc_min_partition_size = BLOCK_32X32
  enc_max_partition_size = BLOCK_32X32

  enc_target_tx_size = TX_8X8
}

choose_quant_dist() {
  enc_still_picture = 0
  enc_monochrome = 0

  enc_lossless_prob = 0
  enc_lossless = 0
  enc_special_lossless = 0
  enc_skip_prob = 0
  enc_barrier_prob = 0
  enc_inter_prob = 100
  enc_frame_is_inter = 1

  enc_skip_prob = 0
  enc_compound_prob = 100
  use_intrabc = 0
  // target COMPOUND_DISTANCE
  enable_masked_compound = 0
  enable_jnt_comp = 1
  enable_order_hint = 1 // required to set enable_jnt_comp to 1
  compound_idx = 0
}

choose_reduce_bitrate() {
  enc_split_prob = 50
  enc_skip_prob = 75
  enc_use_large_coefs_prob = 10
  enc_zero_coef_prob = 75
  enc_empty_block_prob = 75
  enc_palette_prob = 5
  enc_inter_prob = 75
  enc_compound_prob = 50
  enc_lossless_prob = 10
  enc_small_blocks = Choose(0,99) < (75)
  enc_bias_to_big_partitions_prob = 90
  enc_mv_small_prob = 90
  enc_monochrome = Choose(0,99) < 25
}

choose_base_qindex() {
  enc_base_qindex = Choose(-1,0)
}

configurable() {
  default ()
  config_scaling ()

  // Don't use the frame planner for now, as it causes problems when
  // overriding certain elements
  enc_use_frame_planner = 0

  // Never use enc_special_lossless. This is an Argon-only feature
  // really, and it's a bit difficult to document what it does.
  enc_special_lossless = 0

  // Disable the special scalability mode by default. This isn't
  // really compatible with the "make a random stream and poke it"
  // approach that we're using here, so we assume that it's probably
  // not what the ClientE engineers want unless they explicitly set it.
  enc_use_scalability_preset = 0

  // Also disable using temporal group descriptions for the
  // scheduling. Again, this is a clever canned scheduling mode, which
  // messes with what you might naively think the code does.
  enc_temporal_group_description_present = 0

  // Only use DECODER_MODEL_SCHEDULE (rather than
  // DECODER_MODEL_RESOURCE_AVAILABILITY) for ClientE streams. Mainly
  // because it's much easier to explain what's going on. Note that if
  // it were DECODER_MODEL_RESOURCE_AVAILABILITY then we would be
  // forced to set equal_picture_interval to 1.
  enc_decoder_model_use_schedule = 1

  // enc_special_operating_points is a weird mode when scalability is
  // disabled, where we send nonzero operating_point_idc. Don't do
  // this in configurable mode.
  enc_special_operating_points = 0

  // Don't use palette patterns, otherwise the various palette control
  // knobs won't work.
  enc_pixel_pattern_prob = 0

  // This slightly increases the minimum delta_frame_id_length to make
  // absolutely sure that we never fail to find a valid frame ID in
  // enc_schedule_frame_ids. Obviously, we can't do this if we're
  // trying to get coverage, but it's perfectly safe here.
  delta_frame_id_length = Choose(Max(5, minDeltaFrameIdLength), 15)

  // Disable recode loop
  enc_max_recode_count = 1

  // A simpler default behaviour (which we can easily describe in
  // cfg_elements.txt)
  enable_warped_motion = ChooseBit()

  // enc_inter_mode_newmv is designed to bias the logic in
  // enc_pick_inter_mode. The idea seems to be to boost the chances of
  // unconditionally coding NEWMV. This isn't what we described in the
  // docs for <enc_inter_mode> and (frankly) is a bit confusing. So
  // we'll switch it off for the configurable profile.
  enc_inter_mode_newmv = 0
}

client_b() {
  // Specialized settings for ClientB streams
  configurable()

  enc_profile = 0

  // Since we're going to stick to level 2.0, we don't need to worry
  // about constraining the maximum frame size. But we do need to
  // worry about the minimum.
  enc_frame_min_width_limit = 64
  enc_frame_min_height_limit = 64

  // Disable superres by default: we'll turn it on explicitly if we
  // need it, but this avoids problems where superres + small width
  // gives a frame width smaller than 64.
  enable_superres = 0

  // Also, don't use DECODER_MODEL_SCHEDULE: that just makes life
  // harder and I don't think we care about it. Do use the decoder
  // model in general (this is the default): we want to know if we've
  // got it wrong!
  enc_decoder_model_use_schedule = 0
}

client_c() {
  // Specialized settings for ClientC streams
  //configurable()
  default ()
  config_scaling ()

  enc_profile = 0
  enc_max_level = 1

  enc_speed_group = 0
  enc_framerate_choice = enc_speed_group == 1 ? 0 : Choose(0, 2)


  // Since we're going to stick to level 2.0, we don't need to worry
  // about constraining the maximum frame size. But we do need to
  // worry about the minimum.
  enc_frame_min_width_limit = 384
  enc_frame_min_height_limit = 384

  // Disable superres by default: we'll turn it on explicitly if we
  // need it, but this avoids problems where superres + small width
  // gives a frame width smaller than 64.
  //enable_superres = 0

  // Also, don't use DECODER_MODEL_SCHEDULE: that just makes life
  // harder and I don't think we care about it. Do use the decoder
  // model in general (this is the default): we want to know if we've
  // got it wrong!
  //enc_decoder_model_use_schedule = 0
}

lst_client_c() {
  //lst streams use a power of 2, so we bump up the max resolutions to include one (from 384)
  //enc_profile_min_width = 512
  //enc_profile_max_width = 512
  //enc_profile_min_height = 512
  choose_level5()
  enc_min_level = 1 // level 2.0 doesn't allow minimum resolution if height has to be power of 2
  enc_frame_min_height_limit = 512
  enc_profile_max_height = 512
  enc_limit_anchor_frames = 0
}

client_d() {
  // Specialized settings for ClientD streams
  default ()
  config_scaling ()

  enc_profile = 0
  //Ensure the smaller dimension doens't exceed 4352
  enc_profile_min_width = 16
  enc_profile_min_height = 16
  enc_profile_max_width = enc_chooseWidthFirst ? 8192 : (enc_profile_max_height > 4352 ? 4352 : 8192)
  enc_profile_max_height = enc_chooseWidthFirst ? (enc_profile_max_width > 4352 ? 4352 : 8192) : 8192
}
