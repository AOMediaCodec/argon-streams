/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "av1-cfg.h"
#include "arrays.h"

// Defining TOPLEVEL_T
#include "av1_mid.h"

// Includes from config parser
#include "symbol.hh"
#include "passes.hh"
#include "runtime-error.hh"

#include <cstring>
#include <iostream>
#include <regex>

typedef std::map<std::string, var_desc_t> vd_map_t;

// Pointers that are set up in setup_symbol_table
static std::unique_ptr<vd_map_t>  var_to_pointer;
static const int64_t             *locals_start;
static std::unique_ptr<symtab_t>  symbol_table;

// Set up in read_config_file
static std::mt19937                       random_engine;
static std::unique_ptr<swizzle::map_t>    config_file;
static std::array <int, cfg_elts.size ()> debug_flags;

// Return the index of the local variable group for this element if
// there is one, otherwise -1.
static int get_element_group (const char *element)
{
    if (! element)
        return -1;

    // Otherwise let's look to see whether there's a local. First we
    // get any group for this element name.
    std::map<std::string, uint32_t>::const_iterator
        it = cfg_elt_to_elt_idx.find (element);

    if (it == cfg_elt_to_elt_idx.end ()) {
        return false;
    }

    uint32_t elt_idx = it->second;
    return cfg_elt_idx_to_group.at (elt_idx);
}

static bool symbol_lookup (var_desc_t *dest,
                           const char *element,
                           const char *symbol)
{
    assert (dest && symbol);
    assert (var_to_pointer);
    assert (locals_start);

    // First look for a global symbol with the given name
    vd_map_t::const_iterator it = var_to_pointer->find (symbol);
    if (it != var_to_pointer->end ()) {
        // We found one!
        * dest = it->second;
        return true;
    }

    // No luck? Does this element belong to a group?
    int group_idx = get_element_group (element);

    // If group_idx is -1, this element doesn't have a group of
    // locals, so we'll have to give up.
    if (group_idx < 0)
        return false;

    // Otherwise, group_idx should be a valid index for the
    // group_to_locals array.
    const std::array<const char*, 16> &locals = group_to_locals.at (group_idx);

    // Now we can hunt through that array for the name
    int local_idx = -1;
    for (unsigned i = 0; i < locals.size (); ++ i) {
        if (! locals [i])
            break;

        if (0 == strcmp (symbol, locals [i])) {
            local_idx = i;
            break;
        }
    }

    // If we didn't find the local then local_idx will be -1.
    if (local_idx < 0)
        return false;

    // Otherwise local_idx is the index of the local.
    dest->svd.addr = (uintptr_t) (locals_start + local_idx);
    dest->svd.type = -8;
    dest->dim = 0;
    dest->lengths = nullptr;
    return true;
}

static const char *symbol_invert (const char *element, uintptr_t address)
{
    assert (var_to_pointer && locals_start);

    // Is address in the locals array?
    if (((uintptr_t) locals_start <= address) &&
        (address < (uintptr_t) (locals_start + 16)) &&
        ((address - (uintptr_t) locals_start) & 0x7) == 0) {

        unsigned local_idx = (address - (uintptr_t) locals_start) / 8;

        int group_idx = get_element_group (element);

        // If group_idx is -1, this element doesn't have a group of
        // locals, which is kind of weird because the address appears
        // to be a local. Hmm. Oh, well, we'll just have to give up
        // while looking a trifle confused.
        if (group_idx < 0)
            return "(unknown local symbol)";

        // Otherwise, we have a name!
        return group_to_locals.at (group_idx).at (local_idx);
    }

    // If this wasn't a local address, do a (slow) linear search
    // through the var_to_pointer map.
    for (vd_map_t::const_iterator it = var_to_pointer->begin ();
         it != var_to_pointer->end ();
         ++ it) {

        if (it->second.svd.addr == address)
            return it->first.c_str ();
    }

    // If that didn't work either, we'll have to give up.
    return "(unknown symbol)";
}

static int64_t cfg_abs (int64_t x)
{
    return (x < 0) ? -x : x;
}

static int64_t cfg_min (int64_t x, int64_t y)
{
    return (x < y) ? x : y;
}

static int64_t cfg_max (int64_t x, int64_t y)
{
    return (x < y) ? y : x;
}

static int64_t cfg_clip3 (int64_t lo, int64_t hi, int64_t x)
{
    return cfg_max (lo, cfg_min (hi, x));
}

static int64_t cfg_clip_bits (int64_t bit_depth, int64_t x)
{
    if (bit_depth < 0)
        throw runtime_error ();

    if (bit_depth >= 63)
        return cfg_max (0, x);

    return cfg_clip3 (0, (1 << bit_depth) - 1, x);
}

static int64_t cfg_round2 (int64_t x, int64_t n)
{
    if (n <= 0)
        throw runtime_error ();

    if (n == 0)
        return x;

    if (n >= 63)
        return 0;

    return (x + (1 << (n - 1))) >> n;
}

static int64_t cfg_round2_signed (int64_t x, int64_t n)
{
    return ((x > 0) ?
            cfg_round2 (x, n) :
            - cfg_round2 (-x, n));
}

static int64_t cfg_floor_log2 (int64_t x)
{
    if (x < 1)
        throw runtime_error ();

    int64_t s = 0;
    while (x) {
        x >>= 1;
        ++ s;
    }
    return s - 1;
}

static int64_t cfg_ceil_log2 (int64_t x)
{
    if (x < 0)
        throw runtime_error ();

    if (x == 1)
        return 0;

    int64_t i = 1;
    int64_t p = 2;
    while (p < x) {
        ++ i;
        p <<= 1;
    }
    return i;
}

static int64_t choose_bit ()
{
    std::uniform_int_distribution<int64_t> distribution (0, 1);
    return distribution (random_engine);
}

static int64_t choose_bits (int64_t nbits)
{
    if (nbits <= 0)
        return 0;

    int64_t max_value = std::numeric_limits<int64_t>::max ();
    if (nbits <= 63) {
        max_value >>= 63 - nbits;
    }

    // At this point, max_value is 2^k-1 where k=min(nbits, 63).
    std::uniform_int_distribution<int64_t> distribution (0, max_value);

    return distribution (random_engine);
}

static int64_t choose_bernoulli_2 (int64_t p, int64_t n)
{
    n = cfg_max (n, 1);
    p = cfg_clip3 (0, n, p);

    std::uniform_int_distribution<int64_t> distribution (0, n - 1);
    return (distribution (random_engine) < p);
}

static int64_t choose_bernoulli_1 (int64_t p)
{
    return choose_bernoulli_2 (p, 100);
}

void
setup_symbol_table (const TOPLEVEL_T &tl)
{
    var_to_pointer = mk_var_to_pointer (tl);
    symbol_table.reset (new symtab_t (symbol_lookup, symbol_invert));
    symbol_table->constants = & cfg_constants;
    locals_start = & tl.global_data->cfg_syntax [0];

    // Functions from the AV1 spec
    symbol_table->fun1s ["Abs"] = std::make_pair (& cfg_abs, true);
    symbol_table->fun2s ["Min"] = std::make_pair (& cfg_min, true);
    symbol_table->fun2s ["Max"] = std::make_pair (& cfg_max, true);
    symbol_table->fun3s ["Clip3"] = std::make_pair (& cfg_clip3, true);
    symbol_table->fun2s ["ClipBits"] = std::make_pair (& cfg_clip_bits, true);
    symbol_table->fun2s ["Round2"] = std::make_pair (& cfg_round2, true);
    symbol_table->fun2s ["Round2Signed"] = \
        std::make_pair (& cfg_round2_signed, true);
    symbol_table->fun1s ["FloorLog2"] = std::make_pair (& cfg_floor_log2, true);
    symbol_table->fun1s ["FloorCeil2"] = std::make_pair (& cfg_ceil_log2, true);

    // abs, min and max also appear without capitalisation.
    symbol_table->fun1s ["abs"] = std::make_pair (& cfg_abs, true);
    symbol_table->fun2s ["min"] = std::make_pair (& cfg_min, true);
    symbol_table->fun2s ["max"] = std::make_pair (& cfg_max, true);

    // clamp (from the BSG spec) is the same as Clip3.
    symbol_table->fun3s ["clamp"] = std::make_pair (& cfg_clip3, true);

    // Random number helper functions
    symbol_table->fun0s ["choose_bit"] = std::make_pair (& choose_bit, false);
    symbol_table->fun1s ["choose_bits"] = std::make_pair (& choose_bits, false);
    symbol_table->fun1s ["choose_bernoulli"] =
        std::make_pair (& choose_bernoulli_1, false);
    symbol_table->fun2s ["choose_bernoulli"] =
        std::make_pair (& choose_bernoulli_2, false);

    // Note: We have to add the elements in increasing order of index,
    //       which is the same order as the cfg_elts array.
    for (const char *elt : cfg_elts) {
        symbol_table->add_element (elt);
    }
}

bool read_config_file (const char                          *filename,
                       const char                          *selected_config,
                       int                                  seed,
                       const EncCommandLine::elt_dbg_vec_t &debug)
{
    assert (symbol_table);
    random_engine.seed (seed);
    config_file = run_passes (filename, selected_config,
                              * symbol_table, false);
    if (! config_file)
        return false;

    bool needs_print = false;
    debug_flags.fill (0);

    std::unique_ptr<std::regex> regex;

    for (const EncCommandLine::elt_dbg_pair_t &pr : debug) {
        try {
            regex.reset (new std::regex (pr.first, std::regex::extended));
        }
        catch (const std::regex_error &e) {
            std::cerr << "Failed to interpret --config-debug "
                      << "argument as an extended regular expression: "
                      << e.what () << "\n";
            return false;
        }

        // Apply the regex to each element in turn.
        bool found_match = false;
        for (size_t idx = 0; idx < config_file->vec.size (); ++ idx) {
            const swizzle::block_slice_t *code = config_file->vec [idx].get ();
            if (! code)
                continue;

            if (std::regex_search (symbol_table->element_name (idx), * regex)) {
                found_match = true;
                needs_print = true;
                debug_flags [idx] = pr.second;
            }
        }

        if (! found_match) {
            std::cerr << "Warning: The --config-debug regular expression `"
                      << pr.first << "' didn't match any bitstream elements.\n";
        }
    }

    if (! needs_print)
        return true;

    // The user has used --config-debug to enable debugging for some
    // bitstream elements. Print out the code for any that are being
    // traced.
    bool first = true;
    for (size_t idx = 0; idx < config_file->vec.size (); ++ idx) {
        if (! debug_flags [idx])
            continue;

        const swizzle::block_slice_t *code = config_file->vec [idx].get ();
        assert (code);

        const std::string &elt_name = symbol_table->element_name (idx);

        if (first) {
            std::cout << "Runtime configuration for bitstream elements:\n";
        } else {
            std::cout << '\n';
        }

        std::cout << elt_name << ":\n";
        code->display (std::cout, 0, * symbol_table, elt_name.c_str ());

        first = false;
    }
    if (! first)
        std::cout << "\n";

    return true;
}

bool builtin_get_cfg_syntax (unsigned element_idx,
                             flat_array<int64_t, 1> &out_array)
{
    if (! config_file) return false;

    // Look in config_file to get any code for evaluating this
    // element.
    swizzle::block_slice_t *code = config_file->vec.at (element_idx).get ();
    if (! code)
        return false;

    unsigned dbg_flag = debug_flags.at (element_idx);

    // Is verbose debugging enabled for this element? If so, we need
    // to print out the value of everything that it's sensitive to and
    // then the value that it ended up with.
    if (dbg_flag >= 2) {
        const std::string &element = symbol_table->element_name (element_idx);
        std::cout << "Evaluating " << element << ":\n";

        std::vector<simple_var_desc_t> sensitivity_list;
        code->fill_sensitivity_list (& sensitivity_list);

        for (const simple_var_desc_t &svd : sensitivity_list) {
            std::cout << "  "
                      << symbol_table->sym_invert (element.c_str (),
                                                   svd.addr)
                      << " = ";
            // Let's make sure that we print something useful if read
            // causes a segfault.
            std::cout.flush ();
            std::cout << svd.read ()
                      << "\n";
        }

        // Again, flush in case evaluation causes a segfault.
        std::cout.flush ();
    }

    bool found_val = code->eval (random_engine,
                                 config_file->paths,
                                 & out_array [0]);

    if (dbg_flag >= 2) {
        std::cout << "=> ";
        if (! found_val)
            std::cout << "<no value>";
        else
            std::cout << out_array [0];
        std::cout << "\n\n";
        std::cout.flush ();
    } else if (dbg_flag == 1) {
        const std::string &element = symbol_table->element_name (element_idx);
        std::cout << element << " = ";
        if (! found_val)
            std::cout << "<no value>";
        else
            std::cout << out_array [0];
        std::cout << '\n';
    }

    return found_val;
}
