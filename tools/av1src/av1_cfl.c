/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_SPEC_CFL 0

// Calculate the CfL prediction for one transform unit.
// Important: we only compute the AC components of the prediction - the DC component must
// be computed and already stored into the appropriate place in 'frame'.
cfl_predict_ac(x, y, plane, txSz, bsize, mi_row, mi_col) {
#if VALIDATE_SPEC_CFL
  validate(60001)
#endif
  plane_bsize = get_plane_block_size(sb_size, plane)
  plane_bsize = Max(BLOCK_4X4, plane_bsize)

  // Predict the current block.
  // Note: This folds together cfl_compute_averages + cfl_predict_block; may want to split it out later,
  // or do U+V together?
  txw = tx_size_wide[txSz]
  txh = tx_size_high[txSz]
  num_pel_log2 = tx_size_high_log2[txSz] + tx_size_wide_log2[txSz] // May be as small as 2
  alpha_q3 = (plane == 1) ? cfl_alpha_u : cfl_alpha_v
  sum_q3 = 0
  for (i = 0; i < txh; i++)
    for (j = 0; j < txw; j++) {
      sum_q3 += cfl_get_luma_q3(x+j, y+i)
    }
  y_avg_q3 = ROUND_POWER_OF_TWO(sum_q3, num_pel_log2)

#if VALIDATE_SPEC_CFL
  validate(alpha_q3)
  validate(sum_q3)
  validate(y_avg_q3)
#endif

  for (i = 0; i < txh; i++)
    for (j = 0; j < txw; j++) {
      y_pix_q3 = cfl_get_luma_q3(x+j, y+i)
      scaled_luma_ac = ROUND_POWER_OF_TWO_SIGNED(alpha_q3 * (y_pix_q3 - y_avg_q3), 6)
      uv_dc_pred = frame[plane][x+j][y+i]
      frame[plane][x+j][y+i] = clip_pixel(uv_dc_pred + scaled_luma_ac)
#if VALIDATE_SPEC_CFL
      validate(60002)
      validate(y_pix_q3)
      validate(uv_dc_pred)
      validate(scaled_luma_ac)
      validate(frame[plane][x+j][y+i])
#endif
    }
}

// Fetch the luma data corresponding to chroma pixel location (x, y).
// When subsampling, we average a group of 2 or 4 such pixels, as appropriate.
// Data is returned in q3 precision.
cfl_get_luma_q3(x, y) {
  if (subsampling_x && subsampling_y) {
    if (2*x >= cfl_luma_w)
      x = (cfl_luma_w / 2) - 1
    if (2*y >= cfl_luma_h)
      y = (cfl_luma_h / 2) - 1
    return (frame[0][2*x][2*y] + frame[0][2*x+1][2*y] + frame[0][2*x][2*y+1] + frame[0][2*x+1][2*y+1]) << 1
  }
  else if (subsampling_x) {
    if (2*x >= cfl_luma_w)
      x = (cfl_luma_w / 2) - 1
    if (y >= cfl_luma_h)
      y = cfl_luma_h - 1
    return (frame[0][2*x][y] + frame[0][2*x+1][y]) << 2
  } else if (subsampling_y) {
    if (x >= cfl_luma_w)
      x = cfl_luma_w - 1
    if (2*y >= cfl_luma_h)
      y = (cfl_luma_h / 2) - 1
    return (frame[0][x][2*y] + frame[0][x][2*y+1]) << 2
  } else {
    if (x >= cfl_luma_w)
      x = cfl_luma_w - 1
    if (y >= cfl_luma_h)
      y = cfl_luma_h - 1
    return frame[0][x][y] << 3
  }
}
