/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#ifndef AV1_DEFINES_H
#define AV1_DEFINES_H

//Spec validation
#define VALIDATE_SPEC_USUAL_SUSPECTS 0
#define VALIDATE_SPEC_STRUCTURE 0
#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_INTER2 1
#else
#define VALIDATE_SPEC_INTER2 0
#endif
#define VALIDATE_SPEC_INTER2_PLANE 0

// Should we collect per-stream statistics during decoding?
#define COLLECT_STATS 1
// If we are collecting those stats, should we print them at the end of
// decoding each stream?
#define PRINT_STATS 1

// Switch to turn decoder model checking on/off.
// We can't currently guarantee that we generate valid decoder models,
// so each stream has a small (~0.1%) chance of failing to generate.
#define CHECK_DECODER_MODELS 1

// Helper for writing CHECK/ASSERT statements
#define IMPLIES(p, q) (!(p) || (q))


#define VP9_FRAME_MARKER 0x2

#define OBU_SEQUENCE_HEADER 1
#define OBU_TD 2
#define OBU_FRAME_HEADER 3
#define OBU_TILE_GROUP 4
#define OBU_METADATA 5
#define OBU_FRAME 6
#define OBU_REDUNDANT_FRAME_HEADER 7
#define OBU_TILE_LIST 8
#define OBU_PADDING 15

#define OBU_TYPES 16

#define METADATA_TYPE_AOM_RESERVED_0 0 // Defined in ref code, but no actual implementation
#define METADATA_TYPE_HDR_CLL 1 // "HDR content light level"
#define METADATA_TYPE_HDR_MDCV 2 // "HDR mastering display colour volume"
#define METADATA_TYPE_SCALABILITY 3 // Which scalability structure is in use?
#define METADATA_TYPE_ITUT_T35 4
#define METADATA_TYPE_TIMECODE 5
#define METADATA_TYPE_MAX METADATA_TYPE_TIMECODE
#define METADATA_TYPE_ARGON_DESIGN_LARGE_SCALE_TILE 30
#define NUM_METADATA_TYPES (METADATA_TYPE_MAX+1)


#define LEB128_MAX_SIZE 8

#define NUM_REF_FRAMES_LOG2 3
#define NUM_REF_FRAMES 8

#define FRAME_CONTEXTS (NUM_REF_FRAMES + 8)
# define NUM_FRAME_CONTEXTS 8 /* if this number is changed, you need to change av1_base.py and av1_base_encode.py too */
# define FRAME_CONTEXTS_LOG2 3

# define PRIMARY_REF_BITS 3
# define PRIMARY_REF_NONE 7

#define MI_SIZE_LOG2 2
#define MI_SIZE 4
#define TXFM_PER_MI_COL 1
#define TXFM_PER_MI_ROW 1

// Determine the internal precision of the interpolation filters.
// Note: compound-round takes precedence over convolve-round in the reference code,
// so copy that behaviour here.
#define INTERP_ROUND0_BASE 3
#define INTERP_ROUND1_BASE 7

#define WIENER_ROUND0 5
#define WIENER_ROUND1 9

#define QINDEX_BITS 8
#define QINDEX_RANGE 256
//@todo check this is right
#define LF_LEVEL_RANGE 256

#define MIN_TILE_WIDTH_MAX_SB 2
#define MAX_TILE_WIDTH_MAX_SB 32

#define DIFF_UPDATE_PROB 252
#define NMV_UPDATE_PROB 252
#define GROUP_DIFF_UPDATE_PROB 252

#define MBSKIP_CONTEXTS 3
#define SKIP_MODE_CONTEXTS 3

#define MAX_MV_REF_CANDIDATES 2

#define NONE -1
#define INTRA_FRAME 0
#define LAST_FRAME 1
# define LAST2_FRAME 2
# define LAST3_FRAME 3
# define GOLDEN_FRAME 4
# define BWDREF_FRAME 5
#  define ALTREF2_FRAME 6
#  define ALTREF_FRAME 7
# define INTER_REFS_PER_FRAME (ALTREF_FRAME - LAST_FRAME + 1)
# define LAST_REF_FRAMES 3
# define ALLOWED_REFS_PER_FRAME ALTREF_FRAME


#define INTRA_INTER_CONTEXTS 4
#define COMP_INTER_CONTEXTS 5
#define REF_CONTEXTS 3

#define MAX_SEGMENTS 8
#define SEG_TREE_PROBS 7
#define PREDICTION_PROBS 3

#define SEG_LVL_ALT_Q 0
#define SEG_LVL_ALT_LF_Y_V 1
#define SEG_LVL_ALT_LF_Y_H 2
#define SEG_LVL_ALT_LF_U 3
#define SEG_LVL_ALT_LF_V 4
#define SEG_LVL_REF_FRAME 5
#define SEG_LVL_SKIP 6
#define SEG_LVL_GLOBALMV 7
#define SEG_LVL_MAX 8

#define BLOCK_TYPES 2
#define REF_TYPES 2
#define COEF_BANDS 6
#define PREV_COEF_CONTEXTS 6
#define DC_COEF_CONTEXTS 3
#define BLOCKZ_CONTEXTS 3

#define SWITCHABLE_FILTERS 3
#define SWITCHABLE_FILTER_CONTEXTS ((SWITCHABLE_FILTERS+1)*4)

#define BLOCK_4X4 0
#define BLOCK_4X8 (BLOCK_4X4 + 1)
#define BLOCK_8X4 (BLOCK_4X4 + 2)
#define BLOCK_8X8 (BLOCK_4X4 + 3)
#define BLOCK_8X16 (BLOCK_4X4 + 4)
#define BLOCK_16X8 (BLOCK_4X4 + 5)
#define BLOCK_16X16 (BLOCK_4X4 + 6)
#define BLOCK_16X32 (BLOCK_4X4 + 7)
#define BLOCK_32X16 (BLOCK_4X4 + 8)
#define BLOCK_32X32 (BLOCK_4X4 + 9)
#define BLOCK_32X64 (BLOCK_4X4 + 10)
#define BLOCK_64X32 (BLOCK_4X4 + 11)
#define BLOCK_64X64 (BLOCK_4X4 + 12)
# define BLOCK_64X128 (BLOCK_4X4 + 13)
# define BLOCK_128X64 (BLOCK_4X4 + 14)
# define BLOCK_128X128 (BLOCK_4X4 + 15)
# define BLOCK_SIZES (BLOCK_128X128 + 1)
#define BLOCK_4X16 (BLOCK_SIZES + 0)
#define BLOCK_16X4 (BLOCK_SIZES + 1)
#define BLOCK_8X32 (BLOCK_SIZES + 2)
#define BLOCK_32X8 (BLOCK_SIZES + 3)
#define BLOCK_16X64 (BLOCK_SIZES + 4)
#define BLOCK_64X16 (BLOCK_SIZES + 5)
#define BLOCK_SIZES_ALL (BLOCK_64X16 + 1)

// Extra block sizes - used only for looking up block orders for intra.
// These are used when the true block size is 16x64 or 64x16, and the
// superblock size is 64x64.
# define BLOCK_32X128 (BLOCK_SIZES_ALL)
# define BLOCK_128X32 (BLOCK_SIZES_ALL + 1)
#define BLOCK_LARGEST (BLOCK_SIZES - 1)
#define BLOCK_INVALID 255

#define MAX_SB_SIZE_LOG2 7
#define PARTITION_CONTEXTS 20

#define MAX_SB_SIZE (1 << MAX_SB_SIZE_LOG2)
#define BLOCK_SIZE_GROUPS 4
#define PARTITION_PLOFFSET 4
#define MI_BLOCK_SIZE_LOG2 (MAX_SB_SIZE_LOG2 - MI_SIZE_LOG2)
#define MI_BLOCK_SIZE (1 << MI_BLOCK_SIZE_LOG2)
#define MAX_MIB_MASK (MI_BLOCK_SIZE - 1)
#define MAX_MIB_MASK2 (2*MI_BLOCK_SIZE - 1)

#define PARTITION_NONE 0
#define PARTITION_HORZ 1
#define PARTITION_VERT 2
#define PARTITION_SPLIT 3
#define PARTITION_TYPES 4
# define PARTITION_HORZ_A 4 // HORZ split and the top partition is split again
# define PARTITION_HORZ_B 5 // HORZ split and the bottom partition is split again
# define PARTITION_VERT_A 6 // VERT split and the left partition is split again
# define PARTITION_VERT_B 7 // VERT split and the right partition is split again
# define PARTITION_HORZ_4 8 // 4:1 horizontal partition
# define PARTITION_VERT_4 9 // 4:1 vertical partition
# define EXT_PARTITION_TYPES 10
#define PARTITION_INVALID 255

#define ALLOW_128X32_BLOCKS 0

# define TX_4X4 0
#define TX_8X8 (TX_4X4 + 1)
#define TX_16X16 (TX_4X4 + 2)
#define TX_32X32 (TX_4X4 + 3)
# define TX_64X64 (TX_4X4 + 4)
# define TX_SIZES (TX_64X64 + 1)
#define TX_SIZE_LUMA_MIN TX_4X4
#define TX_SIZE_CTX_MIN (TX_SIZE_LUMA_MIN + 1)
#define MAX_TX_CATS (TX_SIZES - TX_SIZE_CTX_MIN)
#define MAX_TX_DEPTH 2
#define TX_SIZES_LARGEST TX_64X64

#if MAX_TX_DEPTH == 2
#define TX_SIZE_CONTEXTS 3
#else
#error "No longer supported"
#endif

# define TX_4X8 (TX_SIZES+0)
# define TX_8X4 (TX_SIZES+1)
# define TX_8X16 (TX_SIZES+2)
# define TX_16X8 (TX_SIZES+3)
# define TX_16X32 (TX_SIZES+4)
# define TX_32X16 (TX_SIZES+5)
#  define TX_32X64 (TX_SIZES+6)
#  define TX_64X32 (TX_SIZES+7)
#   define TX_4X16  (TX_SIZES+8)
#  define TX_16X4 (TX_4X16+1)
#  define TX_8X32 (TX_4X16+2)
#  define TX_32X8 (TX_4X16+3)
#   define TX_16X64 (TX_4X16+4)
#   define TX_64X16 (TX_4X16+5)
#   define TX_SIZES_ALL (TX_64X16+1)

# define TX_SIZE_W_MIN 4
# define TX_SIZE_H_MIN 4

#define EXT_TX_SIZES 4
#define EXT_TX_SETS_INTER 4
#define EXT_TX_SETS_INTRA 3

#define EXT_TX_SET_DCTONLY 0
#define EXT_TX_SET_DCT_IDTX 1
#define EXT_TX_SET_DTT4_IDTX 2
#define EXT_TX_SET_DTT4_IDTX_1DDCT 3
#define EXT_TX_SET_DTT9_IDTX_1DDCT 4
#define EXT_TX_SET_ALL16 5
#define EXT_TX_SET_TYPES 6

# define ONLY_4X4 0
# define TX_MODE_LARGEST 1
# define TX_MODE_SELECT 2
# define TX_MODES 3


#define DCT_DCT 0
#define ADST_DCT 1
#define DCT_ADST 2
#define ADST_ADST 3
#define FLIPADST_DCT 4
#define DCT_FLIPADST 5
#define FLIPADST_FLIPADST 6
#define ADST_FLIPADST 7
#define FLIPADST_ADST 8
#define IDTX 9
#define V_DCT 10
#define H_DCT 11
#define V_ADST 12
#define H_ADST 13
#define V_FLIPADST 14
#define H_FLIPADST 15
#define TX_TYPES 16
#define INTRA_TX_TYPES 7

// Some values used in the inverse transform
#define INVSQRT2_12BIT 2896
#define SQRT2_12BIT 5793

// Precision of trigonometric constants used in the inverse transforms
#define INV_COS_BIT 12

/* List of supported color primaries */
#define AOM_CICP_CP_RESERVED_0 0   /* For future use */
#define AOM_CICP_CP_BT_709 1       /* BT.709 */
#define AOM_CICP_CP_UNSPECIFIED 2  /* Unspecified */
#define AOM_CICP_CP_RESERVED_3 3   /* For future use */
#define AOM_CICP_CP_BT_470_M 4     /* BT.470 System M (historical) */
#define AOM_CICP_CP_BT_470_B_G 5   /* BT.470 System B, G (historical) */
#define AOM_CICP_CP_BT_601 6       /* BT.601 */
#define AOM_CICP_CP_SMPTE_240 7    /* SMPTE 240 */
#define AOM_CICP_CP_GENERIC_FILM 8 /* Generic film (color filters using illuminant C) */
#define AOM_CICP_CP_BT_2020 9      /* BT.2020, BT.2100 */
#define AOM_CICP_CP_XYZ 10         /* SMPTE 428 (CIE 1921 XYZ) */
#define AOM_CICP_CP_SMPTE_431 11   /* SMPTE RP 431-2 */
#define AOM_CICP_CP_SMPTE_432 12   /* SMPTE EG 432-1  */
#define AOM_CICP_CP_RESERVED_13 13 /* For future use (values 13 - 21)  */
#define AOM_CICP_CP_EBU_3213 22    /* EBU Tech. 3213-E  */
#define AOM_CICP_CP_RESERVED_23 23 /* For future use (values 23 - 255)  */

/* List of supported transfer functions */
#define AOM_CICP_TC_RESERVED_0 0      /* For future use */
#define AOM_CICP_TC_BT_709 1          /* BT.709 */
#define AOM_CICP_TC_UNSPECIFIED 2     /* Unspecified */
#define AOM_CICP_TC_RESERVED_3 3      /* For future use */
#define AOM_CICP_TC_BT_470_M 4        /* BT.470 System M (historical)  */
#define AOM_CICP_TC_BT_470_B_G 5      /* BT.470 System B, G (historical) */
#define AOM_CICP_TC_BT_601 6          /* BT.601 */
#define AOM_CICP_TC_SMPTE_240 7       /* SMPTE 240 M */
#define AOM_CICP_TC_LINEAR 8          /* Linear */
#define AOM_CICP_TC_LOG_100 9         /* Logarithmic (100 : 1 range) */
#define AOM_CICP_TC_LOG_100_SQRT10 10 /* Logarithmic (100 * Sqrt(10) : 1 range) */
#define AOM_CICP_TC_IEC_61966 11      /* IEC 61966-2-4 */
#define AOM_CICP_TC_BT_1361 12        /* BT.1361 */
#define AOM_CICP_TC_SRGB 13           /* sRGB or sYCC*/
#define AOM_CICP_TC_BT_2020_10_BIT 14 /* BT.2020 10-bit systems */
#define AOM_CICP_TC_BT_2020_12_BIT 15 /* BT.2020 12-bit systems */
#define AOM_CICP_TC_SMPTE_2084 16     /* SMPTE ST 2084, ITU BT.2100 PQ */
#define AOM_CICP_TC_SMPTE_428 17      /* SMPTE ST 428 */
#define AOM_CICP_TC_HLG 18            /* BT.2100 HLG, ARIB STD-B67 */
#define AOM_CICP_TC_RESERVED_19 19    /* For future use (values 19-255) */

/* List of supported matrix coefficients */
#define AOM_CICP_MC_IDENTITY 0     /* Identity matrix */
#define AOM_CICP_MC_BT_709 1       /* BT.709 */
#define AOM_CICP_MC_UNSPECIFIED 2  /* Unspecified */
#define AOM_CICP_MC_RESERVED_3 3   /* For future use */
#define AOM_CICP_MC_FCC 4          /* US FCC 73.628 */
#define AOM_CICP_MC_BT_470_B_G 5   /* BT.470 System B, G (historical) */
#define AOM_CICP_MC_BT_601 6       /* BT.601 */
#define AOM_CICP_MC_SMPTE_240 7    /* SMPTE 240 M */
#define AOM_CICP_MC_SMPTE_YCGCO 8  /* YCgCo */
#define AOM_CICP_MC_BT_2020_NCL 9  /* BT.2020 non-constant luminance, BT.2100 YCbCr  */
#define AOM_CICP_MC_BT_2020_CL 10  /* BT.2020 constant luminance */
#define AOM_CICP_MC_SMPTE_2085 11  /* SMPTE ST 2085 YDzDx */
#define AOM_CICP_MC_CHROMAT_NCL 12 /* Chromaticity-derived non-constant luminance */
#define AOM_CICP_MC_CHROMAT_CL 13  /* Chromaticity-derived constant luminance */
#define AOM_CICP_MC_ICTCP 14       /* BT.2100 ICtCp */
#define AOM_CICP_MC_RESERVED_15 15 /* For future use (values 15-255)  */


// Supported colour ranges
#define CR_STUDIO_RANGE 0
#define CR_FULL_RANGE 1

// Supported chroma sample locations
#define CSP_UNKNOWN 0
#define CSP_VERTICAL 1
#define CSP_COLOCATED 2
#define CSP_RESERVED 3

#define KEY_FRAME 0
#define INTER_FRAME 1
#define INTRA_ONLY_FRAME 2
#define S_FRAME 3

#define DC_PRED   0
#define V_PRED    1
#define H_PRED    2
#define D45_PRED  3
#define D135_PRED 4
#define D113_PRED 5
#define D157_PRED 6
#define D203_PRED 7
#define D67_PRED  8
# define SMOOTH_PRED     9
#  define SMOOTH_V_PRED 10
#  define SMOOTH_H_PRED 11
#  define PAETH_PRED    12
#  define INTRA_MODES   13
#   define UV_CFL_PRED    13 // UV only, so isn't counted under INTRA_MODES
#   define UV_INTRA_MODES 14
# define DIRECTIONAL_MODES 8

// These are the modes that can appear for inter predictions in the Y
// plane. Since UV_CFL_PRED isn't a valid mode, this could
// theoretically start at 13, rather than 14. But that's massively
// confusing and, besides, doesn't match the numbering in the spec. So
// we start at 14.
#define NEARESTMV         (UV_INTRA_MODES+0)
#define NEARMV            (UV_INTRA_MODES+1)
#define GLOBALMV          (UV_INTRA_MODES+2)
#define NEWMV             (UV_INTRA_MODES+3)
#define NEAREST_NEARESTMV (UV_INTRA_MODES+4)
#define NEAR_NEARMV       (UV_INTRA_MODES+5)
#define NEAREST_NEWMV     (UV_INTRA_MODES+6)
#define NEW_NEARESTMV     (UV_INTRA_MODES+7)
#define NEAR_NEWMV        (UV_INTRA_MODES+8)
#define NEW_NEARMV        (UV_INTRA_MODES+9)
#define GLOBAL_GLOBALMV   (UV_INTRA_MODES+10)
#define NEW_NEWMV         (UV_INTRA_MODES+11)

// More confusingly yet, MB_MODE_COUNT *equals* NEW_NEWMV (because
// we're skipping UV_CFL_PRED). So an array indexed by Y mode should
// have MB_MODE_COUNT+1 entries. Obvious, right? :-/
#define MB_MODE_COUNT     (NEW_NEWMV + 0)

#define INTER_COMPOUND_MODES 8

#define INTER_MODES 4

#define SM_WEIGHT_LOG2_SCALE 8

#define MV_JOINT_ZERO 0
#define MV_JOINT_HNZVZ 1
#define MV_JOINT_HZVNZ 2
#define MV_JOINT_HNZVNZ 3
#define MV_JOINTS 4

#define MV_CLASSES 11
#define CLASS0_SIZE 2
#define MV_OFFSET_BITS 10
#define MV_FP_SIZE 4

#define MV_IN_USE_BITS 14
#define MV_UPP   (1 << MV_IN_USE_BITS)
#define MV_LOW   (-(1 << MV_IN_USE_BITS))

#define CLASS0_BITS    1  /* bits at integer precision for class 0 */

#define MV_MAX_BITS    (MV_CLASSES + CLASS0_BITS + 2)
#define MV_MAX         ((1 << MV_MAX_BITS) - 1)
#define MV_VALS        ((MV_MAX << 1) + 1)


#define COMP_NEWMV_CTXS 5
#define INTER_MODE_CONTEXTS 8

#define SUBEXP_PARAM 4
#define MODULUS_PARAM 13

#define MAX_PROB 255

#define MAX_MB_PLANE 3
#define PLANE_TYPE_Y_WITH_DC 0
#define PLANE_TYPE_UV 1
#define PLANE_TYPES 2

#define MAX_MODE_LF_DELTAS 2

#define EIGHTTAP 0
#define EIGHTTAP_SMOOTH 1
#define EIGHTTAP_SHARP 2
#define BILINEAR 3
#define SWITCHABLE 4
# define FOURTAP 4
# define FOURTAP_SMOOTH 5
# define SUBPEL_FILTERS 6

#define SINGLE_PREDICTION_ONLY 0
#define COMP_PREDICTION_ONLY 1
#define HYBRID_PREDICTION 2
#define NB_PREDICTION_TYPES 3

// Minimum size of frames supported
// These are specified in the "Levels" annex to the specification
#define MIN_FRAME_WIDTH 16
#define MIN_FRAME_HEIGHT 16


// Maximum size of frames we support
// Allow up to 2x the level 6.3 limits on each of
// width, height, and area
// We also add a 128-pixel border on each side when calculating
// the maximum area, so that we don't have to worry about values
// wrapping around
#define MAX_FRAME_WIDTH 16384 + 256
#define MAX_FRAME_HEIGHT 8704 + 256
#define MAX_FRAME_AREA ((16384 + 256 + 128) * (2176 + 256 + 128))

// For encoding, we also want to define the maximum *actual*
// area (ie, without borders) that we'll generate.
#define MAX_CODED_FRAME_AREA ((16384 + 256) * (2176 + 256))

#define MAX_MI_ROWS (MAX_FRAME_HEIGHT / MI_SIZE)
#define MAX_SB_ROWS (MAX_MI_ROWS / MI_SIZE_64X64)
#define MAX_MI_COLS (MAX_FRAME_WIDTH / MI_SIZE)

#define MAX_PX_IN_FRAME (MAX_FRAME_AREA)
#define MAX_MI_IN_FRAME (MAX_MI_ROWS*MAX_MI_COLS)

// Some encoder side limits:

// Maximum number of frames we support encoding
#define ENC_MAX_FRAMES 500

// Maximum size of a sequence header, in bytes
#define ENC_MAX_SEQ_HDR_LEN 512

#define ZERO_TOKEN 0
#define ONE_TOKEN 1
#define TWO_TOKEN 2
#define THREE_TOKEN 3
#define FOUR_TOKEN 4
#define DCT_VAL_CATEGORY1 5
#define DCT_VAL_CATEGORY2 6
#define DCT_VAL_CATEGORY3 7
#define DCT_VAL_CATEGORY4 8
#define DCT_VAL_CATEGORY5 9
#define DCT_VAL_CATEGORY6 10
#define DCT_EOB_TOKEN 11
#define MAX_ENTROPY_TOKENS 12
#define NO_EOB 0
#define EARLY_EOB 1
#define COEFF_PROB_MODELS 255
#define BLOCK_Z_TOKEN 255
#define DC_HEAD_TOKENS 6
#define AC_HEAD_TOKENS 5
#define HEAD_TOKENS 5
#define TAIL_TOKENS 9
#define ONE_TOKEN_EOB 1
#define ONE_TOKEN_NEOB 2
#define TWO_TOKEN_PLUS_EOB 3
#define TWO_TOKEN_PLUS_NEOB 4
#define ENTROPY_NODES 11
#define UNCONSTRAINED_NODES 3
#define MODEL_NODES 8
#define TAIL_NODES 9

#define DCT_EOB_MODEL_TOKEN 3
#define MINQ 0
#define MAXQ 255

#define COMPANDED_MVREF_THRESH 8
#define MVREF_ROWS 3
#define MVREF_COLS 3

// How many blocks can be scanned in each of the two surrounding
// regions in setup_ref_mv_list() ?
// (only used in encoder)
#define MAX_NEARBY_REFS 17
#define MAX_FAR_REFS 17
#define MAX_MV_REGION_REFS Max(MAX_NEARBY_REFS, MAX_FAR_REFS)

// Limit the number of surround blocks which we'll copy any particular
// block's ref frame(s) to. This must be at least MAX_REF_MV_STACK_SIZE+1,
// so that we can cover cases where the ref mv stack overflows. In fact,
// we want it a little higher than that since some blocks might end up with
// the same motion vector.
#define MAX_PLANNED_REFS 12

#define MV_CLASS_0 0
#define MV_CLASS_1 1
#define MV_CLASS_2 2
#define MV_CLASS_3 3
#define MV_CLASS_4 4
#define MV_CLASS_5 5
#define MV_CLASS_6 6
#define MV_CLASS_7 7
#define MV_CLASS_8 8
#define MV_CLASS_9 9
#define MV_CLASS_10 10

#define MAX_LOOP_FILTER 63

#define SCALE_PX_TO_MV 8

#define REF_SCALE_SHIFT 14
#define SUBPEL_BITS 4
#define SUBPEL_MASK 15
#define SUBPEL_TAPS 8
#define SUBPEL_SHIFTS 16
#define FILTER_BITS 7

#define SCALE_SUBPEL_BITS 10

#define SCALE_SUBPEL_SHIFTS (1 << SCALE_SUBPEL_BITS)
#define SCALE_SUBPEL_MASK (SCALE_SUBPEL_SHIFTS - 1)
#define SCALE_EXTRA_BITS (SCALE_SUBPEL_BITS - SUBPEL_BITS)
#define SCALE_EXTRA_OFF ((1 << SCALE_EXTRA_BITS) / 2)

// Maximum number of intermediate rows during scaled convolve
#define MAX_EXT_SIZE 263

#define FRAME_MARKER 2
#define VP9_INTERP_EXTEND 4

#define MIN_SB_SIZE_LOG2 6
#define MIN_SB_SIZE (1 << MIN_SB_SIZE_LOG2)
#define MAX_MIB_SIZE_LOG2 (MAX_SB_SIZE_LOG2 - MI_SIZE_LOG2)
#define MAX_MIB_SIZE (1 << MAX_MIB_SIZE_LOG2)

// Loop filtering is done in 128x128 units as of
// https://aomedia-review.googlesource.com/c/aom/+/56581
#define LOOP_FILTER_BLOCK_SIZE (128 / MI_SIZE)

#define FWD_REFS 4
#define BWD_REFS 3
#define MAX_REF_FRAMES 8
#define TOTAL_REFS_PER_FRAME 8

#define COMP_REF_TYPE_CONTEXTS 5
#define UNI_COMP_REF_CONTEXTS 3

#define UNIDIR_COMP_REFERENCE 0
#define BIDIR_COMP_REFERENCE 1
#define COMP_REFERENCE_TYPES 2

#define LAST_LAST2_FRAMES 0
#define LAST_LAST3_FRAMES 1
#define LAST_GOLDEN_FRAMES 2
#define BWDREF_ALTREF_FRAMES 3
#define SIGNALLED_UNIDIR_COMP_REFS 4
#define SIGNALLED_COMP_REFS (FWD_REFS * BWD_REFS + SIGNALLED_UNIDIR_COMP_REFS)

#define SINGLE_REFS (FWD_REFS + BWD_REFS)

// With this experiment, any combination of distinct reference frames is
// possible, subject to ref_frame[0] < ref_frame[1].
// Thus, for N single ref frames, we have (N choose 2) ref frame pairs
#define COMP_REFS (SINGLE_REFS * (SINGLE_REFS - 1) / 2)

#define MODE_CTX_REF_FRAMES (TOTAL_REFS_PER_FRAME + COMP_REFS)

#define MAX_REF_MV_STACK_SIZE 8
# define REF_CAT_LEVEL 640
// Intrabc blocks use separate CDFs to normal inter blocks for new-mv predictions.
// In the ref code, these are stored in a separate variable 'ndvc',
// but here we just add an extra entry to the newmv CDF tables.
#define NMV_CONTEXTS 4
#define MV_BITS_CONTEXTS 6

#define NEWMV_MODE_CONTEXTS 6
#define GLOBALMV_MODE_CONTEXTS 2
#define REFMV_MODE_CONTEXTS 6
#define DRL_MODE_CONTEXTS 3

#define GLOBALMV_OFFSET 3
#define REFMV_OFFSET 4

#define NEWMV_CTX_MASK ((1<<3)-1)
#define GLOBALMV_CTX_MASK ((1<<(4-3))-1)
#define REFMV_CTX_MASK ((1<<(8-4))-1)

#define UNUSED_PROB 128
#define PALETTE_COLOR_CONTEXTS 5
#define PALETTE_MIN_BSIZE BLOCK_4X4
#define PALETTE_MAX_COLOR_CONTEXT_HASH 8
#define PALETTE_MAX_SIZE 8
#define PALETTE_BSIZE_CTXS 9
#define PALETTE_Y_MODE_CONTEXTS 3
#define PALETTE_UV_MODE_CONTEXTS 2
#define PALETTE_SIZES 7
#define PALETTE_COLORS 8
#define PATTERN_MAX_SIZE 4
#define PALETTE_NUM_NEIGHBORS 3

#define TWO_COLORS 0
#define THREE_COLORS 1
#define FOUR_COLORS 2
#define FIVE_COLORS 3
#define SIX_COLORS 4
#define SEVEN_COLORS 5
#define EIGHT_COLORS 6

#define PALETTE_COLOR_ONE 0
#define PALETTE_COLOR_TWO 1
#define PALETTE_COLOR_THREE 2
#define PALETTE_COLOR_FOUR 3
#define PALETTE_COLOR_FIVE 4
#define PALETTE_COLOR_SIX 5
#define PALETTE_COLOR_SEVEN 6
#define PALETTE_COLOR_EIGHT 7

#define FILTER_INTRA_MODES 5
#define MAX_ANGLE_DELTA 3
#define ANGLE_STEP 3

#define FILTER_INTRA_SCALE_BITS 4

#define NEED_LEFT 1
#define NEED_ABOVE 2

#define DAALA_EXTRA_COUNT 9
#define DAALA_PRECISION 15
#define DAALA_MAX_BITS_BYTES_PER_CHUNK 50000000
#define DAALA_MAX_SYMBOL_BYTES_PER_CHUNK 50000000
#define MOST_SYMBOLS 16

#define EC_PROB_SHIFT 6
#define EC_MIN_PROB 4  // must be <= (1<<EC_PROB_SHIFT)/16

#define SIMPLE_TRANSLATION 0

#define OBMC_CAUSAL 1
#define WARPED_CAUSAL 2
#define MOTION_MODES 3

#define OBMC_NONE 0
#define OBMC_LEFT 1
#define OBMC_ABOVE 2

#define DELTA_Q_SMALL 3
#define DELTA_LF_SMALL 3
#define DEFAULT_DELTA_LF_RES 2

#define TXFM_WHT 0
#define TXFM_DCT 1
#define TXFM_ADST 2
#define TXFM_IDTX 3
#define NUM_1D_TXFMS 4

#ifndef INT16_MIN
# define INT16_MIN -32768
#endif
#ifndef INT16_MAX
# define INT16_MAX 32767
#endif

#define WARPEDMODEL_PREC_BITS 16
#define WARP_PARAM_REDUCE_BITS 6

#define WARPEDPIXEL_PREC_BITS 6
#define WARPEDPIXEL_PREC_SHIFTS (1 << WARPEDPIXEL_PREC_BITS)
#define WARPEDDIFF_PREC_BITS (WARPEDMODEL_PREC_BITS - WARPEDPIXEL_PREC_BITS)

#define DIV_LUT_PREC_BITS 14
#define DIV_LUT_BITS 8
#define DIV_LUT_NUM (1 << DIV_LUT_BITS)

#define IDENTITY 0
#define TRANSLATION 1
#define ROTZOOM 2
#define AFFINE 3

#define GLOBAL_TRANS_TYPES 4
#define CODED_GM_TYPES 3
#define GLOBAL_TYPE_ENCODE_BITS 2

#define SUBEXPFIN_K 3
#define GM_TRANS_PREC_BITS 6
#define GM_TRANS_ONLY_PREC_BITS 3
#define GM_ABS_TRANS_BITS 12
#define GM_ABS_TRANS_ONLY_BITS 9
#define GM_TRANS_PREC_DIFF (WARPEDMODEL_PREC_BITS - GM_TRANS_PREC_BITS)
#define GM_TRANS_ONLY_PREC_DIFF (WARPEDMODEL_PREC_BITS - GM_TRANS_ONLY_PREC_BITS)
#define GM_ALPHA_PREC_BITS 15
#define GM_ABS_ALPHA_BITS 12
#define GM_ALPHA_PREC_DIFF (WARPEDMODEL_PREC_BITS - GM_ALPHA_PREC_BITS)
#define GM_ALPHA_DECODE_FACTOR (1 << GM_ALPHA_PREC_DIFF)
#define GM_ALPHA_MAX (1 << GM_ABS_ALPHA_BITS)

#define MUL_PREC_BITS 16
#define WARPEDMODEL_TRANS_CLAMP (128 << WARPEDMODEL_PREC_BITS)
#define WARPEDMODEL_NONDIAGAFFINE_CLAMP (1 << (WARPEDMODEL_PREC_BITS - 3))

#define LEAST_SQUARES_SAMPLES_MAX_BITS 3
#define LEAST_SQUARES_SAMPLES_MAX (1 << LEAST_SQUARES_SAMPLES_MAX_BITS)
#define SAMPLES_ARRAY_SIZE LEAST_SQUARES_SAMPLES_MAX

#define LS_MV_MAX 256
#define LS_STEP 8

#define LS_MAT_RANGE_BITS \
  ((MAX_SB_SIZE_LOG2 + 4) * 2 + LEAST_SQUARES_SAMPLES_MAX_BITS)
#define LS_MAT_DOWN_BITS 2
#define LS_MAT_BITS (LS_MAT_RANGE_BITS - LS_MAT_DOWN_BITS)
#define LS_MAT_MIN (-(1 << (LS_MAT_BITS - 1)))
#define LS_MAT_MAX ((1 << (LS_MAT_BITS - 1)) - 1)

// Maximum number of tile rows/cols we can use.
# define MAX_TILE_ROWS 64
# define MAX_TILE_COLS 64
#define MAX_TILES (MAX_TILE_ROWS * MAX_TILE_COLS)


#define COMPOUND_AVERAGE 0
#define COMPOUND_DISTANCE 1
#define COMPOUND_WEDGE 2
#define COMPOUND_DIFFWTD 3
// Deliberately don't define COMPOUND_TYPES,
// as nothing should refer to it directly


#define WEDGE_HORIZONTAL 0
#define WEDGE_VERTICAL 1
#define WEDGE_OBLIQUE27 2
#define WEDGE_OBLIQUE63 3
#define WEDGE_OBLIQUE117 4
#define WEDGE_OBLIQUE153 5
#define WEDGE_DIRECTIONS 6

#define WEDGE_WEIGHT_BITS 6
#define WEDGE_TYPES (1 << 4)
#define MAX_WEDGE_SIZE (1 << 5)
#define MASK_MASTER_SIZE (MAX_WEDGE_SIZE << 1)
#define MASK_MASTER_STRIDE MASK_MASTER_SIZE

#define COMPOUND_SEGMENT_TYPE 1
#define MAX_SEG_MASK_BITS 1

#if COMPOUND_SEGMENT_TYPE == 0
#define UNIFORM_45 0
#define UNIFORM_45_INV 1
#else
#define DIFF_FACTOR 16
#define DIFFWTD_38 0
#define DIFFWTD_38_INV 1
#endif // COMPOUND_SEGMENT_TYPE == 0


#define II_DC_PRED 0
#define II_V_PRED 1
#define II_H_PRED 2
#define II_SMOOTH_PRED 3
#define INTERINTRA_MODES 4

#define MB_FILTER_COUNT 2

#define PARALLEL_DEBLOCKING_15TAPLUMAONLY 1
#define PARALLEL_DEBLOCKING_DISABLE_15TAP 0

# define PARALLEL_DEBLOCKING_5_TAP_CHROMA 1

#define MAX_VARTX_DEPTH 2
#define TXFM_PARTITION_CONTEXTS ((TX_SIZES - TX_8X8) * 6 - 3)
#define TX_UNIT_WIDE_LOG2 (MI_SIZE_LOG2 - tx_size_wide_log2[0])
#define TX_UNIT_HIGH_LOG2 (MI_SIZE_LOG2 - tx_size_high_log2[0])

#define QM_LEVEL_BITS 4
#define NUM_QM_LEVELS (1 << QM_LEVEL_BITS)
#define AOM_QM_BITS 5

#define CDEF_MAX_STRENGTHS 16
#define CDEF_VERY_LARGE 30000

#define MI_SIZE_64X64_LOG2 (6 - MI_SIZE_LOG2)
#define MI_SIZE_64X64 (1 << MI_SIZE_64X64_LOG2)

# define INTRA_EDGE_FILT 3
# define INTRA_EDGE_TAPS 5
#  define MAX_UPSAMPLE_SZ 16

// Define tile maximum width and area
// There is no maximum height since height is limited by area and width limits
// The minimum tile width or height is fixed at one superblock
#define MAX_TILE_WIDTH (4096)  // Max Tile width in pixels
#define MAX_TILE_AREA (4096 * 2304)  // Maximum tile area in pixels
#define MAX_TILE_AREA_MI (MAX_TILE_AREA >> (2 * MI_SIZE_LOG2))

# define MAX_ANCHOR_FRAMES 128

// TODO we need all of these?
# define TXB_SKIP_CONTEXTS 13
# define SIG_REF_DIFF_OFFSET_NUM 5
# define SIG_COEF_CONTEXTS_2D 26
# define SIG_COEF_CONTEXTS_1D 16
# define SIG_COEF_CONTEXTS_EOB 4
# define SIG_COEF_CONTEXTS (SIG_COEF_CONTEXTS_2D + SIG_COEF_CONTEXTS_1D)
# define EOB_COEF_CONTEXTS 22
# define COEFF_BASE_CONTEXTS SIG_COEF_CONTEXTS
# define DC_SIGN_CONTEXTS 3

# define BR_MAG_OFFSET 1
# define BR_TMP_OFFSET 12
# define BR_REF_CAT 4
# define LEVEL_CONTEXTS 21

# define NUM_BASE_LEVELS 2
# define CONTEXT_MAG_POSITION_NUM 3
# define BR_CDF_SIZE 4
# define COEFF_BASE_RANGE (4 * (BR_CDF_SIZE - 1))

// Maximum quantized coefficient it's possible to encode
# define GOLOMB_MAX ((1<<20) - 2) // Max of the Golomb part
# define QCOEFF_MAX (GOLOMB_MAX + COEFF_BASE_RANGE + 1 + NUM_BASE_LEVELS)

# define COEFF_CONTEXT_BITS 6
# define COEFF_CONTEXT_MASK ((1 << COEFF_CONTEXT_BITS) - 1)

#define BR_CONTEXT_POSITION_NUM 8
# define BASE_CONTEXT_POSITION_NUM 12

# define TX_CLASS_2D 0
# define TX_CLASS_HORIZ 1
# define TX_CLASS_VERT 2
# define TX_CLASSES 3

#define CFL_MAX_BLOCK_SIZE 32

#define CFL_SIGN_ZERO 0
#define CFL_SIGN_NEG 1
#define CFL_SIGN_POS 2
#define CFL_SIGNS 3

#define CFL_JOINT_SIGNS (CFL_SIGNS * CFL_SIGNS - 1)
// CFL_SIGN_U is equivalent to (js + 1) / 3 for js in 0 to 8
#define CFL_SIGN_U(js) (((js + 1) * 11) >> 5)
// CFL_SIGN_V is equivalent to (js + 1) % 3 for js in 0 to 8
#define CFL_SIGN_V(js) ((js + 1) - CFL_SIGNS * CFL_SIGN_U(js))

// There is no context when the alpha for a given plane is zero.
// So there are 2 fewer contexts than joint signs.
#define CFL_ALPHA_CONTEXTS (CFL_JOINT_SIGNS + 1 - CFL_SIGNS)
#define CFL_CONTEXT_U(js) (js + 1 - CFL_SIGNS)
// Also, the contexts are symmetric under swapping the planes.
#define CFL_CONTEXT_V(js) \
  (CFL_SIGN_V(js) * CFL_SIGNS + CFL_SIGN_U(js) - CFL_SIGNS)

#define CFL_ALPHABET_SIZE_LOG2 4
#define CFL_ALPHABET_SIZE (1 << CFL_ALPHABET_SIZE_LOG2)

#define TOKEN_CDF_Q_CTXS 4

#define KF_MODE_CONTEXTS 5

#define FRAME_LF_COUNT 4

# define MFMV_STACK_SIZE 3
# define INVALID_MV -(1<<15)
# define MAX_OFFSET_WIDTH 8
# define MAX_OFFSET_HEIGHT 0
# define REFMVS_LIMIT (1 << 12) - 1

// debug
# define STACKED_MFMV 1

#define RESTORE_NONE 0
#define RESTORE_WIENER 1
#define RESTORE_SGRPROJ 2
#define RESTORE_SWITCHABLE 3
#define RESTORE_SWITCHABLE_TYPES RESTORE_SWITCHABLE
#define RESTORE_TYPES 4
#define RESTORATION_TILESIZE_MAX 256
#define RESTORATION_PROCUNIT_SIZE 64
#define SGR_INTERMEDIATE_SIZE (RESTORATION_PROCUNIT_SIZE + 2)

// Probability (out of 256) that we don't use loop restoration
// for a particular tile, when using a fixed restoration type.
#define LR_NONE_PROB 64

// The Wiener filter used in loop-restoration has 7 taps, but
// is constrained to be symmetric and have all its taps sum to
// 128. This leaves 3 values which need to be read from the bitstream.
#define WIENER_COEFFS 3

#define SGRPROJ_PARAMS_BITS 4
#define SGRPROJ_PRJ_SUBEXP_K 4

// Precision bits for projection
#define SGRPROJ_PRJ_BITS 7
// Restoration precision bits generated higher than source before projection
#define SGRPROJ_RST_BITS 4
// Precision bits for division by n^2 * eps
#define SGRPROJ_MTABLE_BITS 20
// Precision bits for division by n
#define SGRPROJ_RECIP_BITS 12
// Internal precision bits for core selfguided_restoration
#define SGRPROJ_SGR_BITS 8
#define SGRPROJ_SGR (1 << SGRPROJ_SGR_BITS)

#define WIENER_HALFWIN 3

#define RESTORATION_CTX_VERT 2
#define RESTORATION_BORDER 3

#define MAX_LR_UNITS_IN_FRAME ((MAX_PX_IN_FRAME + (64*64) - 1) / (64*64))

#define RESTORATION_TILE_OFFSET 8



# define SPATIAL_PREDICTION_PROBS 3

// The superres ratio is (8 / superres_denom),
// where superres_denom is in the range [8, 16] inclusive.
// If superres_denom == 8, that means superres is not used.
#define SUPERRES_NUM 8
#define SUPERRES_DENOM_MIN 9
#define SUPERRES_DENOM_BITS 3
#define SUPERRES_DENOM_MAX (SUPERRES_DENOM_MIN + (1 << SUPERRES_DENOM_BITS) - 1)

// Internal position for upscale filter kernel selection
#define SUPERRES_FILTER_BITS 6
#define SUPERRES_FILTER_SHIFTS (1 << SUPERRES_FILTER_BITS)
#define SUPERRES_FILTER_TAPS 8
#define SUPERRES_FILTER_OFFSET (SUPERRES_FILTER_TAPS/2 - 1)

// Internal precision for resizing calculations
#define SUPERRES_SCALE_BITS 14
#define SUPERRES_SCALE_SHIFTS (1 << SUPERRES_SCALE_BITS)
#define SUPERRES_SCALE_MASK (SUPERRES_SCALE_SHIFTS - 1)
#define SUPERRES_EXTRA_BITS (SUPERRES_SCALE_BITS - SUPERRES_FILTER_BITS)

# define FRAME_OFFSET_BITS 5
# define MAX_FRAME_DISTANCE ((1 << FRAME_OFFSET_BITS) - 1)

#define COMP_INDEX_CONTEXTS 6
#define COMP_GROUP_IDX_CONTEXTS 6

#define DIST_PRECISION_BITS 4

// When computing an intrabc prediction, how many superblocks must the
// source be "behind" the destination? (to allow for processing delays
// in hardware)
#define INTRABC_DELAY_MI 64
#define INTRABC_DELAY_SB64 (INTRABC_DELAY_MI / mi_size_wide[BLOCK_64X64])

// Tweaks to enable hardware to reconstruct the image in a parallel
// "wavefront" mode.
// Note: This only affects the reconstruction stage; the syntax stage
// is serial within each tile.
#define USE_WAVE_FRONT 1

// The possible scalability structures
#define SCALABILITY_L1T2 0
#define SCALABILITY_L1T3 1
#define SCALABILITY_L2T1 2
#define SCALABILITY_L2T2 3
#define SCALABILITY_L2T3 4
#define SCALABILITY_S2T1 5
#define SCALABILITY_S2T2 6
#define SCALABILITY_S2T3 7
#define SCALABILITY_L2T1h 8
#define SCALABILITY_L2T2h 9
#define SCALABILITY_L2T3h 10
#define SCALABILITY_S2T1h 11
#define SCALABILITY_S2T2h 12
#define SCALABILITY_S2T3h 13
#define SCALABILITY_SS 14
#define SCALABILITY_L3T1 15
#define SCALABILITY_L3T2 16
#define SCALABILITY_L3T3 17
#define SCALABILITY_S3T1 18
#define SCALABILITY_S3T2 19
#define SCALABILITY_S3T3 20
#define SCALABILITY_L3T2_KEY 21
#define SCALABILITY_L3T3_KEY 22
#define SCALABILITY_L4T5_KEY 23
#define SCALABILITY_L4T7_KEY 24
#define SCALABILITY_L3T2_KEY_SHIFT 25
#define SCALABILITY_L3T3_KEY_SHIFT 26
#define SCALABILITY_L4T5_KEY_SHIFT 27
#define SCALABILITY_L4T7_KEY_SHIFT 28

#define SCALABILITY_NUM_PRESETS (SCALABILITY_L4T7_KEY_SHIFT - SCALABILITY_L1T2 + 1)


#define MAX_NUM_TEMPORAL_LAYERS 8
#define MAX_NUM_SPATIAL_LAYERS 4
#define MAX_NUM_OPERATING_POINTS (MAX_NUM_TEMPORAL_LAYERS * MAX_NUM_SPATIAL_LAYERS)

#define MAX_TEMPORAL_GROUP_SIZE 255

#define LEVEL_MAJOR_BITS 3
#define LEVEL_MINOR_BITS 2
#define LEVEL_BITS (LEVEL_MAJOR_BITS + LEVEL_MINOR_BITS)
#define LEVEL_MINOR_MASK ((1 << LEVEL_MINOR_BITS) - 1)

#define LEVEL_MINOR_MIN 0
#define LEVEL_MINOR_MAX 3
// Note: We define LEVEL_MAJOR_MAX slightly differently to the reference
// code, to help check conformance:
// LEVEL_MAJOR_MAX = 7 is the largest allowed major level *for video type streams*.
// As a special case, the level value 31 (interpreted as "level 9.3") is used
// for still-picture streams
#define LEVEL_MAJOR_MIN 2
#define LEVEL_MAJOR_MAX 7

#define NUM_LEVELS (1 << LEVEL_BITS)

// We compact the valid levels into a contiguous range.
// In this "compacted" format, the last entry is the special
// "max parameters" level. This is valid, but has no specific
// constraints, so in some cases we want to include it and in other
// cases we want to exclude it.
#define NUM_VALID_LEVELS 15
#define NUM_DEFINED_LEVELS 14

// Special case: When seq_level_idx[i] = 31, that means that
// operating point i has no level-specific constraints.
// However, it is still constrained by MAX_TILE_ROWS / MAX_TILE_COLS,
// ie. it can only have up to 64 tile rows and 64 tile cols.
#define LEVEL_MAX_PARAMS 31

#define GRAIN_BLOCK_MAX_W 82
#define GRAIN_BLOCK_MAX_H 73

// The possible methods used in the encoder when picking the tile group
// layout for each frame.
#define ENC_TG_MODE_ONE_TG 0 // One tile group for each frame
#define ENC_TG_MODE_MAX_TG 1 // One tile group per tile
#define ENC_TG_MODE_UNIFORM 2 // Uniformly select between [1, #tiles] tile groups for each frame

// The possible modes for the decoder model
#define DECODER_MODEL_UNKNOWN -1 // Used in the decoder when the mode hasn't been worked out yet
#define DECODER_MODEL_DISABLED 0
#define DECODER_MODEL_RESOURCE_AVAILABILITY 1
#define DECODER_MODEL_SCHEDULE 2

#define DECODER_MODEL_BUFFER_POOL_SIZE 255 /*Allow up to this many frames*/
#define DECODER_MODEL_VBI_SIZE NUM_REF_FRAMES

// Maximum number of copies of a single frame header which
// we will generate. This number includes the initial frame header.
#define ENC_MAX_FRAME_HEADER_COPIES 4

#define NUM_LEVEL_SIZE_GROUPS 8

#define MAX_SPATIAL_ID 3

#define COMP_BASIS_INVALID (1<<31) /* Something so big it shouldn't be possible to achieve */

#define MAX_SEQUENCE_HEADER_SIZE 512
#define MAX_SCALABILITY_STRUCTURE_SIZE 2064

// Defines for custom sets
#define NUM_CLIENT_A_RESOLUTIONS 6


#endif // AV1_DEFINES_H
