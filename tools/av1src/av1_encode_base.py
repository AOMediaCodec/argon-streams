################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

from optparse import OptionParser
import struct,os
import random

seed=34

path_to_ref = r"vp9/build/"
log_bins = 0
if log_bins:
  dut_cabac_log=open(path_to_ref+'dut_enc_cabac.txt','wb')
  cabac_bins_count = 0

  def debug_cabac(s):
      global cabac_bins_count
      print >>dut_cabac_log,s
      #if cabac_bins_count==7:
      #  pdb.set_trace()
      cabac_bins_count+=1

  def debug_cabac_daala_bits(binVal):
      s='%d bin=%d'%(cabac_bins_count,binVal)
      debug_cabac(s)

  def debug_cabac_daala_symbol(symbol,nsyms,rng,bits,Fh,Fl,buf):
      s='%d bin=%d/%d, rng=0x%x, bits=%d, Fh=%d, Fl=%d, buf=%d'%(cabac_bins_count,symbol,nsyms,rng,bits,Fh,Fl,buf)
      debug_cabac(s)

  def debug_cabac_bins(binVal,startRange,startVal,prob):
      s='%d bin=%d sr=%d, sv=%d p=%d'%(cabac_bins_count,binVal,startRange,startVal,prob)
      debug_cabac(s)
else:
  def debug_cabac_bins(binVal,startRange,startVal,prob):
    pass
  def debug_cabac_daala_bits(binVal):
    pass
  def debug_cabac_daala_symbol(symbol,nsyms,rng,bits,Fh,Fl,buf):
    pass

def debug_mv(b,i,mode,compound):
  pass
def debug_lf(b,phase,sz,x,y,dx,dy,plane):
  pass
def debug_inter(subpel_x,subpel_y,w,h,mvrow,mvcol):
  pass
PREDICTION = 0
TRANSFORM = 1
ABOVE = 2
LEFT = 3
OBMC_PRE = 4
OBMC_POST = 5
def log_prediction(b,tx_sz,x,y,plane,phase):
  pass
def log_prediction_rect_pred(b,bw,bh,plane,ref,pitch,phase):
  pass
def log_prediction_rect_frame(b,bw,bh,x,y,plane,phase):
  pass
def log_intra_neighbours(b,txw,txh,mode):
  pass
def log_loop(b,r,c,step,plane,mi_size,lf_block_size):
  pass
def log_cdef(b,r,c,step,mi_size,nplanes):
  pass
def log_lr(b,x,y,w,h,plane,step):
  pass
def log_upscale(b,x0,x1,h,plane,tile_col,phase,I):
  pass
def log_coeffs(b):
  pass
def log_output(b,phase,nplanes):
  pass
def log_anchor_ref(b):
  pass
def log_tile_output(b, decTileRow, decTileCol):
  pass

def ASSERT(cond,msg):
  """Check that cond is True, print msg otherwise"""
  if cond:
    return
  print msg
  assert False

def CHECK(cond,msg, *args):
  """Check that cond is True, print msg otherwise"""
  if cond:
    return
  print "Check Failed: {}".format(msg % tuple(args))
  assert False

def CSV(id, val):
  pass

val_fid = open('validate_encode_python.txt','wb')
val_count = 0
def validate(x):
  global val_count
  print >>val_fid,val_count,x
  #if val_count==51769:
  #  pdb.set_trace()
  val_count+=1

if 1:
  def video_output(b,bitdepthy,bitdepthc, w, h,crop_left,crop_right,crop_top,crop_bottom,pitch,srcX,srcY,subw,subh,is_monochrome):
    #assert crop_left==crop_right==crop_top==crop_bottom==0
    #I=b.video_stream.frame_data.rframe
    subw=subw-1
    subh=subh-1
    I=b.video_stream.frame_data.rframe
    w2=w-crop_left-crop_right
    h2=h-crop_top-crop_bottom
    baseY=(srcY+crop_top)*(pitch)+srcX+crop_left
    baseC=(((srcY+subh)>>subh)+(crop_top>>1))*(pitch)+((srcX+subw)>>subw)+(crop_left>>1)

    if b.global_data.large_scale_tile: # aomedia:1999
      bitdepthy = 16

    if bitdepthy>8 or bitdepthc>8:
      for y in xrange(h2):
        out_python.write(''.join(struct.pack('<H',I[0][baseY+y*pitch+x]) for x in xrange(w2)))
      if not is_monochrome:
        for c in [1,2]:
          for y in xrange((h2+subh)>>subh):
            out_python.write(''.join(struct.pack('<H',I[c][baseC+y*pitch+x]) for x in xrange((w2+subw)>>subw)))
    else:
      for y in xrange(h2):
        out_python.write(''.join(chr(I[0][baseY+y*pitch+x]) for x in xrange(w2)))
      if not is_monochrome:
        for c in [1,2]:
          for y in xrange((h2+subh)>>subh):
            out_python.write(''.join(chr(I[c][baseC+y*pitch+x]) for x in xrange((w2+subw)>>subw)))
    out_python.flush()

v='error'

parser = OptionParser()
parser.add_option("-o","--output",dest="filename",default='../data/out.ivf',help="file to write with encoded bitstream")
parser.add_option("-s","--seed",dest="seed",default=seed,type="int",help="random seed")
parser.add_option("-u","--use_myrand",dest="use_myrand",default=False,action="store_true",help="use myrand")
parser.add_option("-x","--width",dest="width",default=320,type="int",help="image width")
parser.add_option("-y","--height",dest="height",default=240,type="int",help="image height")
parser.add_option("-f","--frames",dest="numframes",default=10,type="int",help="number of frames") # TODO increase number of frames
parser.add_option("-e","--yuv",dest="yuvfilename",default='out_enc_python.yuv',help="file to write with decoded YUV")
parser.add_option("--large-scale-tile",dest="largeScaleTile",action="store_const",default=0,const=1,help="use large scale tile")
parser.add_option("--not-annexb",dest="useAnnexB",action="store_const",default=1,const=0,help="use annex-b file format")
(options,args) = parser.parse_args()

random.seed(options.seed) # Omit this to generate different sequences based on time
mysrand(options.seed)
set_use_myrand(options.use_myrand)

out_python=open(options.yuvfilename,'wb')

isObu = options.filename.endswith(".obu")

global_data=hevc_parse_global_data(None, options.useAnnexB, isObu)
global_data.enc_stream_seed=options.seed
global_data.enc_large_scale_tile=options.largeScaleTile

log_writes=False

with open(options.filename,'wb') as fd:
  frame_contexts = checked_dict(8) # NUM_FRAME_CONTEXTS
  # File header
  b = EncodeBitstream(global_data,opts=options,logging=log_writes)
  b.frame_contexts = frame_contexts
  if isObu:
    hevc_parse_obu_file_init(b)
  else:
    hevc_parse_ivf_file_header(b)
  fd.write(''.join(map(chr,b.data[0].data)))
  i = 0
  while i < global_data.frame_cnt:
    if global_data.camera_frame_header_ready:
      video_stream = b.video_stream
    b = EncodeBitstream(global_data,opts=options,logging=log_writes)
    if global_data.camera_frame_header_ready:
      b.video_stream = video_stream
    b.frame_contexts = frame_contexts
    i += hevc_parse_encode_temporal_unit(b,global_data.frame_cnt-i)

    if not isObu:
      h = EncodeBitstream(global_data,opts=options,logging=log_writes)
      b.frame_contexts = frame_contexts
      hevc_parse_ivf_frame_header(h,b.numbytes())
      fd.write(''.join(map(chr,h.data[0].data))) # header

    fd.write(''.join(map(chr,b.data[0].data))) # frame

