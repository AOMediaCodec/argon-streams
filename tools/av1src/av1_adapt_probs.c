/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

/*
Probabilities can be updated at the end of a frame based on the observed contents.

Each frame can choose which frame context it is based upon.
These probabilities are only updated if refresh_frame_context is true.
*/

#define COEF_COUNT_SAT 24
#define COEF_MAX_UPDATE_FACTOR 112
#define COEF_COUNT_SAT_KEY 24
#define COEF_MAX_UPDATE_FACTOR_KEY 112
#define COEF_COUNT_SAT_AFTER_KEY 24
#define COEF_MAX_UPDATE_FACTOR_AFTER_KEY 128

#define COUNT_SAT 20
#define MAX_UPDATE_FACTOR 128

clearall_segfeatures() {
  for(a=0;a<MAX_SEGMENTS;a++)
    for(a2=0;a2<SEG_LVL_MAX;a2++)
      feature_data[a][a2] = 0
  for(a=0;a<MAX_SEGMENTS;a++) {
    feature_mask[a] = 0
    for(a2=0;a2<SEG_LVL_MAX;a2++)
      FeatureEnabled[a][a2] = 0
  }
  last_active_seg_id = 0
  preskip_seg_id = 0
}

copy_segfeatures(dst, src) {
  for (i=0; i<MAX_SEGMENTS; i++) {
    for (j=0; j<SEG_LVL_MAX;j++) {
      ref_feature_data[dst][i][j] = ref_feature_data[src][i][j]
    }
    ref_feature_mask[dst][i] = ref_feature_mask[src][i]

    // We keep an expanded version of ref_feature_mask[0] (known by a
    // rewrite as feature_mask) in FeatureEnabled.
    if (dst == 0) {
      for (j=0; j<SEG_LVL_MAX; j++) {
        FeatureEnabled[i][j] = (ref_feature_mask[src][i] >> j) & 1
      }
    }
  }
  ref_last_active_seg_id[dst] = ref_last_active_seg_id[src]
  ref_preskip_seg_id[dst] = ref_preskip_seg_id[src]
}

// Reset all features for the current frame_context_idx
init_frame_context() {
  init_mbmode_probs()
  init_mv_probs()
  init_delta_q_cdf()
  init_filter_intra_probs()
  init_inter_mode_probs()
  init_motion_mode_probs()
  init_palette_cdfs()
  init_ext_intra_mod_probs()
}

// This is called at independence, or whenever frame size changes
clear_segmap() {
  ASSERT(mi_rows*mi_cols<=MAX_MI_IN_FRAME,"Too many mode info units in frame")
  for(i=0;i<mi_rows*mi_cols;i++) {
    seg_map[i] = 0
  }
}

// Store the current frame context (CDFs, etc.) into the relevant slot.

// Reset segment features to all disabled, with delta coding.
// Reset probabilities to default values
setup_past_independence() {
  //validate(8000)
  // Reset the segment feature data to the default stats:
  // Features disabled, 0, with delta coding (Default state).
  clearall_segfeatures()
  clear_segmap()

  // Reset the mode ref deltas for loop filter
  set_default_lf_deltas()

  init_frame_context()

}

save_adapted_cdfs() {
#if DECODE
  ASSERT(context_updated == 0, "save_adapted_cdfs() should only be called once per frame")
#endif
  context_updated = 1

  for (i=0; i<MAX_TX_CATS; i++) {
    for(a=0;a<TX_SIZE_CONTEXTS;a++) {
      for(a2=0;a2<MAX_TX_DEPTH+1;a2++) {
        adapted_tx_cdfs[i][a][a2] = tile_tx_cdfs[i][a][a2]
      }
    }
  }

  for (i = 0; i < EXT_TX_SETS_INTER; i++) {
    for (j = 0; j < EXT_TX_SIZES; j++) {
      for (k = 0; k < TX_TYPES; k++) {
        adapted_inter_ext_tx_cdf[i][j][k] = tile_inter_ext_tx_cdf[i][j][k]
      }
    }
  }
  for (i = 0; i < EXT_TX_SETS_INTRA; i++) {
    for (j = 0; j < EXT_TX_SIZES; j++) {
      for (k = 0; k < INTRA_MODES; k++) {
        for (l = 0; l < TX_TYPES; l++) {
          adapted_intra_ext_tx_cdf[i][j][k][l] = tile_intra_ext_tx_cdf[i][j][k][l]
        }
      }
    }
  }

  for (i = 0; i < MAX_SEGMENTS; i++) {
    adapted_seg_tree_cdf[i] = tile_seg_tree_cdf[i]
  }
  for (i=0; i<SPATIAL_PREDICTION_PROBS; i++) {
    for (j=0; j<MAX_SEGMENTS; j++) {
      adapted_spatial_pred_seg_cdf[i][j] = tile_spatial_pred_seg_cdf[i][j]
    }
  }

  for (i=0;i<FILTER_INTRA_MODES;i++) {
    adapted_filter_intra_mode_cdf[i] = tile_filter_intra_mode_cdf[i]
  }
  for (i=0;i<BLOCK_SIZES_ALL;i++) {
    for (j=0;j<2;j++) {
      adapted_filter_intra_cdfs[i][j] = tile_filter_intra_cdfs[i][j]
    }
  }


  for(a=0;a<2;a++) {
    for(a2=0;a2<INTRA_MODES;a2++) {
      for(a3=0;a3<UV_INTRA_MODES;a3++) {
        adapted_uv_mode_cdf[a][a2][a3] = tile_uv_mode_cdf[a][a2][a3]
      }
    }
  }

  for(a2=0;a2<BLOCK_SIZE_GROUPS;a2++) {
    for(c=0;c<INTRA_MODES;c++) {
      adapted_y_mode_cdf[a2][c] = tile_y_mode_cdf[a2][c]
    }
  }

  for(idx=0;idx<NMV_CONTEXTS;idx++) {
    for(i=0;i<MV_JOINTS;i++) {
      adapted_joint_cdf[idx][i] = tile_joint_cdf[idx][i]
    }
    for(comp=0;comp<2;comp++) {
      for(i=0;i<MV_CLASSES;i++) {
        adapted_mvcomp_classes_cdf[idx][comp][i] = tile_mvcomp_classes_cdf[idx][comp][i]
      }
      for(i=0;i<CLASS0_SIZE;i++) {
        for(j=0;j<MV_FP_SIZE;j++) {
          adapted_mvcomp_class0_fp_cdf[idx][comp][i][j] = tile_mvcomp_class0_fp_cdf[idx][comp][i][j]
        }
      }
      for(i=0;i<MV_FP_SIZE;i++) {
        adapted_mvcomp_fp_cdf[idx][comp][i] = tile_mvcomp_fp_cdf[idx][comp][i]
      }
    }
  }

  for(a=0;a<INTER_MODE_CONTEXTS;a++) {
    for(a2=0;a2<INTER_COMPOUND_MODES;a2++) {
      adapted_inter_compound_mode_cdf[a][a2] = tile_inter_compound_mode_cdf[a][a2]
    }
  }

  for (a=0;a<COMP_INDEX_CONTEXTS;a++)
    for (a2=0;a2<2;a2++)
      adapted_compound_idx_cdf[a][a2] = tile_compound_idx_cdf[a][a2]
  for (a=0;a<COMP_GROUP_IDX_CONTEXTS;a++)
    for (a2=0;a2<2;a2++)
      adapted_compound_group_idx_cdf[a][a2] = tile_compound_group_idx_cdf[a][a2]
  for (a=0;a<BLOCK_SIZES_ALL;a++)
    for (a2=0;a2<2;a2++)
      adapted_compound_mask_type_cdf[a][a2] = tile_compound_mask_type_cdf[a][a2]

  for(a=0;a<BLOCK_SIZE_GROUPS;a++) {
    for(a2=0;a2<INTERINTRA_MODES;a2++) {
      adapted_interintra_mode_cdf[a][a2] = tile_interintra_mode_cdf[a][a2]
    }
  }
  for(a=0;a<BLOCK_SIZE_GROUPS;a++)
    for(a2=0;a2<2;a2++)
      adapted_interintra_cdf[a][a2] = tile_interintra_cdf[a][a2]
  for(a=0;a<BLOCK_SIZES_ALL;a++) {
    for(a2=0;a2<16;a2++) {
      adapted_wedge_idx_cdf[a][a2] = tile_wedge_idx_cdf[a][a2]
    }
    for(a2=0;a2<2;a2++) {
      adapted_wedge_interintra_cdf[a][a2] = tile_wedge_interintra_cdf[a][a2]
    }
  }

  for(a=0;a<SWITCHABLE_FILTER_CONTEXTS;a++) {
    for(a2=0;a2<SWITCHABLE_FILTERS;a2++) {
      adapted_switchable_interp_cdf[a][a2] = tile_switchable_interp_cdf[a][a2]
    }
  }
  for(a=0;a<PARTITION_CONTEXTS;a++) {
    for(a2=0;a2<EXT_PARTITION_TYPES;a2++) {
      adapted_partition_cdf[a][a2] = tile_partition_cdf[a][a2]
    }
  }

  for ( i = 0; i < DELTA_Q_SMALL+1; i++ ) {
    adapted_delta_q_cdf[i] = tile_delta_q_cdf[i]
  }
  for ( i = 0; i < DELTA_LF_SMALL+1; i++ ) {
    adapted_delta_lf_cdf[i] = tile_delta_lf_cdf[i]
  }
  for (i = 0; i < FRAME_LF_COUNT; i++)
    for (j = 0; j < DELTA_LF_SMALL+1; j++)
      adapted_delta_lf_multi_cdf[i][j] = tile_delta_lf_multi_cdf[i][j]

  for (i = 0; i < TX_SIZES; i++) {
    for (j = 0; j < TXB_SKIP_CONTEXTS; j++) {
      for (a = 0; a < 2; a++) {
        adapted_txb_skip_cdfs[i][j][a] = tile_txb_skip_cdfs[i][j][a]
      }
    }
  }
  for (i = 0; i < PLANE_TYPES; i++) {
    for (j = 0; j < 2; j++) {
      for (k = 0; k < 5; k++) {
        adapted_eob_flag_16_cdfs[i][j][k] = tile_eob_flag_16_cdfs[i][j][k]
      }
      for (k = 0; k < 6; k++) {
        adapted_eob_flag_32_cdfs[i][j][k] = tile_eob_flag_32_cdfs[i][j][k]
      }
      for (k = 0; k < 7; k++) {
        adapted_eob_flag_64_cdfs[i][j][k] = tile_eob_flag_64_cdfs[i][j][k]
      }
      for (k = 0; k < 8; k++) {
        adapted_eob_flag_128_cdfs[i][j][k] = tile_eob_flag_128_cdfs[i][j][k]
      }
      for (k = 0; k < 9; k++) {
        adapted_eob_flag_256_cdfs[i][j][k] = tile_eob_flag_256_cdfs[i][j][k]
      }
      for (k = 0; k < 10; k++) {
        adapted_eob_flag_512_cdfs[i][j][k] = tile_eob_flag_512_cdfs[i][j][k]
      }
      for (k = 0; k < 11; k++) {
        adapted_eob_flag_1024_cdfs[i][j][k] = tile_eob_flag_1024_cdfs[i][j][k]
      }
    }
  }
  for (i = 0; i < TX_SIZES; i++) {
    for (j = 0; j < PLANE_TYPES; j++) {
      for (k = 0; k < EOB_COEF_CONTEXTS; k++) {
        for (a = 0; a < 2; a++) {
          adapted_eob_extra_cdfs[i][j][k][a] = tile_eob_extra_cdfs[i][j][k][a]
        }
      }
    }
  }
  for (i = 0; i < PLANE_TYPES; i++) {
    for (j = 0; j < DC_SIGN_CONTEXTS; j++) {
      for (a = 0; a < 2; a++) {
        adapted_dc_sign_cdfs[i][j][a] = tile_dc_sign_cdfs[i][j][a]
      }
    }
  }
  for (i = 0; i < TX_SIZES; i++) {
    for (j = 0; j < PLANE_TYPES; j++) {
      for (k = 0; k < SIG_COEF_CONTEXTS_EOB; k++) {
        for (a = 0; a < 3; a++) {
          adapted_coeff_base_eob_cdfs[i][j][k][a] = tile_coeff_base_eob_cdfs[i][j][k][a]
        }
      }
      for (k = 0; k < SIG_COEF_CONTEXTS; k++) {
        for (a = 0; a < 4; a++) {
          adapted_coeff_base_cdfs[i][j][k][a] = tile_coeff_base_cdfs[i][j][k][a]
        }
      }
      for (k = 0; k < LEVEL_CONTEXTS; k++) {
        for (a = 0; a < BR_CDF_SIZE; a++) {
          adapted_coeff_br_cdfs[i][j][k][a] = tile_coeff_br_cdfs[i][j][k][a]
        }
      }
    }
  }


  for (i=BLOCK_8X8;i<BLOCK_SIZES_ALL;i++) {
    for(j=0;j<2;j++)
      adapted_obmc_cdf[i][j] = tile_obmc_cdf[i][j]
    for (j=0;j<MOTION_MODES;j++) {
      adapted_motion_mode_cdf[i][j] = tile_motion_mode_cdf[i][j]
    }
  }

  for (i=0; i<PALETTE_BSIZE_CTXS; i++) {
    for (j=0; j<PALETTE_SIZES; j++) {
      adapted_palette_y_size_cdf[i][j] = tile_palette_y_size_cdf[i][j]
      adapted_palette_uv_size_cdf[i][j] = tile_palette_uv_size_cdf[i][j]
    }
  }
  for (i=0; i<PALETTE_MAX_SIZE-1; i++) {
    for (j=0; j<PALETTE_COLOR_CONTEXTS; j++) {
      for (k=0; k<PALETTE_COLORS; k++) {
        adapted_palette_y_color_cdf[i][j][k] = tile_palette_y_color_cdf[i][j][k]
        adapted_palette_uv_color_cdf[i][j][k] = tile_palette_uv_color_cdf[i][j][k]
      }
    }
  }
  for (i=0; i<PALETTE_BSIZE_CTXS; i++)
    for (j=0; j<PALETTE_Y_MODE_CONTEXTS; j++)
      for (k=0;k<2;k++)
        adapted_palette_y_mode_cdf[i][j][k] = tile_palette_y_mode_cdf[i][j][k]
  for (i=0; i<PALETTE_UV_MODE_CONTEXTS; i++)
    for (j=0;j<2;j++)
      adapted_palette_uv_mode_cdf[i][j] = tile_palette_uv_mode_cdf[i][j]

for (i=0;i<DIRECTIONAL_MODES;i++) {
  for (j=0;j<(2 * MAX_ANGLE_DELTA + 1);j++) {
    adapted_angle_delta_cdf[i][j] = tile_angle_delta_cdf[i][j]
  }
}

  for(i=0;i<NEWMV_MODE_CONTEXTS;i++)
    for(j=0;j<2;j++)
      adapted_newmv_cdf[i][j] = tile_newmv_cdf[i][j]
  for(i=0;i<GLOBALMV_MODE_CONTEXTS;i++)
    for(j=0;j<2;j++)
      adapted_globalmv_cdf[i][j] = tile_globalmv_cdf[i][j]
  for(i=0;i<REFMV_MODE_CONTEXTS;i++)
    for(j=0;j<2;j++)
      adapted_refmv_cdf[i][j] = tile_refmv_cdf[i][j]
  for(i=0;i<DRL_MODE_CONTEXTS;i++)
    for(j=0;j<2;j++)
      adapted_drl_cdf[i][j] = tile_drl_cdf[i][j]
  for(i=0;i<INTRA_INTER_CONTEXTS;i++)
    for(j=0;j<2;j++)
      adapted_intra_inter_cdf[i][j] = tile_intra_inter_cdf[i][j]
  for(i=0;i<COMP_INTER_CONTEXTS;i++)
    for(j=0;j<2;j++)
      adapted_comp_inter_cdf[i][j] = tile_comp_inter_cdf[i][j]
  for(i=0;i<MBSKIP_CONTEXTS;i++)
    for(j=0;j<2;j++)
      adapted_mbskip_cdf[i][j] = tile_mbskip_cdf[i][j]
  for(i=0;i<SKIP_MODE_CONTEXTS;i++)
    for(j=0;j<2;j++)
      adapted_skip_mode_cdf[i][j] = tile_skip_mode_cdf[i][j]
  for (i = 0; i < REF_CONTEXTS; i++)
    for (j = 0; j < FWD_REFS-1; j++)
      for (k = 0; k < 2; k++)
        adapted_comp_ref_cdf[i][j][k] = tile_comp_ref_cdf[i][j][k]
  for (i = 0; i < REF_CONTEXTS; i++)
    for (j = 0; j < BWD_REFS-1; j++)
      for (k = 0; k < 2; k++)
        adapted_comp_bwdref_cdf[i][j][k] = tile_comp_bwdref_cdf[i][j][k]
  for (i = 0; i < REF_CONTEXTS; i++)
    for (j = 0; j < SINGLE_REFS-1; j++)
      for (k = 0; k < 2; k++)
        adapted_single_ref_cdf[i][j][k] = tile_single_ref_cdf[i][j][k]
  for(i=0;i<COMP_REF_TYPE_CONTEXTS;i++)
    for(j=0;j<2;j++)
      adapted_comp_ref_type_cdf[i][j] = tile_comp_ref_type_cdf[i][j]
  for (i = 0; i < UNI_COMP_REF_CONTEXTS; i++)
    for (j = 0; j < SIGNALLED_UNIDIR_COMP_REFS - 1; j++)
      for (k = 0; k < 2; k++)
        adapted_uni_comp_ref_cdf[i][j][k] = tile_uni_comp_ref_cdf[i][j][k]
  for (i = 0; i < PREDICTION_PROBS; i++)
    for (j = 0; j < 2; j++)
      adapted_segment_pred_cdf[i][j] = tile_segment_pred_cdf[i][j]
  for (i = 0; i < NMV_CONTEXTS; i++)
    for (j = 0; j < 2; j++)
      for (k = 0; k < 2; k++)
        adapted_sign_cdf[i][j][k] = tile_sign_cdf[i][j][k]
  for (i = 0; i < NMV_CONTEXTS; i++)
    for (j = 0; j < 2; j++)
      for (k = 0; k < 2; k++)
        adapted_class0_hp_cdf[i][j][k] = tile_class0_hp_cdf[i][j][k]
  for (i = 0; i < NMV_CONTEXTS; i++)
    for (j = 0; j < 2; j++)
      for (k = 0; k < 2; k++)
        adapted_hp_cdf[i][j][k] = tile_hp_cdf[i][j][k]
  for (i = 0; i < NMV_CONTEXTS; i++)
    for (j = 0; j < 2; j++)
      for (k = 0; k < 2; k++)
        adapted_class0_cdf[i][j][k] = tile_class0_cdf[i][j][k]
  for (i = 0; i < NMV_CONTEXTS; i++)
    for (j = 0; j < MV_OFFSET_BITS; j++)
      for (k = 0; k < 2; k++)
        for (l = 0; l < 2; l++)
          adapted_mvcomp_bits_cdf[i][j][k][l] = tile_mvcomp_bits_cdf[i][j][k][l]
  for (i = 0; i < TXFM_PARTITION_CONTEXTS; i++)
    for (j = 0; j < 2; j++)
      adapted_txfm_partition_cdf[i][j] = tile_txfm_partition_cdf[i][j]

  for (i = 0; i < CFL_JOINT_SIGNS; i++)
    adapted_cfl_sign_cdf[i] = tile_cfl_sign_cdf[i]
  for (i = 0; i < CFL_ALPHA_CONTEXTS; i++)
    for (j = 0; j < CFL_ALPHABET_SIZE; j++)
      adapted_cfl_alpha_cdf[i][j] = tile_cfl_alpha_cdf[i][j]

  for (i = 0; i < RESTORE_SWITCHABLE_TYPES; i++)
    adapted_switchable_restore_cdf[i] = tile_switchable_restore_cdf[i]
  for (i = 0; i < 2; i++) {
    adapted_wiener_cdf[i] = tile_wiener_cdf[i]
    adapted_sgrproj_cdf[i] = tile_sgrproj_cdf[i]
  }
}

copy_cdf(uint16pointer dst, uint32pointer src, uint8 nsymbs) {
  topSeen = 0
  for (i=0; i<nsymbs+1; i++) {
    if (topSeen || (i>=nsymbs)) { // some cdfs are all zero, for ease we just handle those here
      CHECK(topSeen || dst[i-1] == 0, "Top not seen at end of cdf")
      dst[i] = 0
    } else {
      dst[i] = src[i]
      if (dst[i] == (1<<DAALA_PRECISION)) {
        topSeen = 1
      }
    }
  }
}

load_adapted_tile_coef_cdfs() {
  for (i = 0; i < TX_SIZES; i++) {
    for (j = 0; j < TXB_SKIP_CONTEXTS; j++) {
      copy_cdf(frame_txb_skip_cdfs[i][j], adapted_txb_skip_cdfs[i][j], 2)
    }
  }
  for (i = 0; i < PLANE_TYPES; i++) {
    for (j = 0; j < 2; j++) {
      copy_cdf(frame_eob_flag_16_cdfs[i][j], adapted_eob_flag_16_cdfs[i][j], 5)
      copy_cdf(frame_eob_flag_32_cdfs[i][j], adapted_eob_flag_32_cdfs[i][j], 6)
      copy_cdf(frame_eob_flag_64_cdfs[i][j], adapted_eob_flag_64_cdfs[i][j], 7)
      copy_cdf(frame_eob_flag_128_cdfs[i][j], adapted_eob_flag_128_cdfs[i][j], 8)
      copy_cdf(frame_eob_flag_256_cdfs[i][j], adapted_eob_flag_256_cdfs[i][j], 9)
      copy_cdf(frame_eob_flag_512_cdfs[i][j], adapted_eob_flag_512_cdfs[i][j], 10)
      copy_cdf(frame_eob_flag_1024_cdfs[i][j], adapted_eob_flag_1024_cdfs[i][j], 11)
    }
  }
  for (i = 0; i < TX_SIZES; i++) {
    for (j = 0; j < PLANE_TYPES; j++) {
      for (k = 0; k < EOB_COEF_CONTEXTS; k++) {
        copy_cdf(frame_eob_extra_cdfs[i][j][k], adapted_eob_extra_cdfs[i][j][k], 2)
      }
    }
  }
  for (i = 0; i < PLANE_TYPES; i++) {
    for (j = 0; j < DC_SIGN_CONTEXTS; j++) {
      copy_cdf(frame_dc_sign_cdfs[i][j], adapted_dc_sign_cdfs[i][j], 2)
    }
  }
  for (i = 0; i < TX_SIZES; i++) {
    for (j = 0; j < PLANE_TYPES; j++) {
      for (k = 0; k < SIG_COEF_CONTEXTS_EOB; k++) {
        copy_cdf(frame_coeff_base_eob_cdfs[i][j][k], adapted_coeff_base_eob_cdfs[i][j][k], 3)
      }
      for (k = 0; k < COEFF_BASE_CONTEXTS; k++) {
        copy_cdf(frame_coeff_base_cdfs[i][j][k], adapted_coeff_base_cdfs[i][j][k], 4)
      }
      for (k = 0; k < LEVEL_CONTEXTS; k++) {
        copy_cdf(frame_coeff_br_cdfs[i][j][k], adapted_coeff_br_cdfs[i][j][k], BR_CDF_SIZE)
      }
    }
  }
}

load_adapted_tile_mv_cdfs() {
  for(idx=0;idx<NMV_CONTEXTS;idx++) {
    copy_cdf(frame_joint_cdf[idx], adapted_joint_cdf[idx], MV_JOINTS)
    for(comp=0;comp<2;comp++) {
      copy_cdf(frame_mvcomp_classes_cdf[idx][comp],  adapted_mvcomp_classes_cdf[idx][comp], MV_CLASSES)
      for(i=0;i<CLASS0_SIZE;i++) {
        copy_cdf(frame_mvcomp_class0_fp_cdf[idx][comp][i], adapted_mvcomp_class0_fp_cdf[idx][comp][i], MV_FP_SIZE)
      }
      copy_cdf(frame_mvcomp_fp_cdf[idx][comp], adapted_mvcomp_fp_cdf[idx][comp], MV_FP_SIZE)
    }
  }
}

load_adapted_tile_intra_cdfs() {
  for (i=0; i<MAX_TX_CATS; i++) {
    for(a=0;a<TX_SIZE_CONTEXTS;a++) {
      copy_cdf(frame_tx_cdfs[i][a], adapted_tx_cdfs[i][a], Min(i + 1, MAX_TX_DEPTH) + 1)
    }
  }

  for (i = 0; i < EXT_TX_SETS_INTER; i++) {
    for (j = 0; j < EXT_TX_SIZES; j++) {
      copy_cdf(frame_inter_ext_tx_cdf[i][j], adapted_inter_ext_tx_cdf[i][j], TX_TYPES)
    }
  }
  for (i = 0; i < EXT_TX_SETS_INTRA; i++) {
    for (j = 0; j < EXT_TX_SIZES; j++) {
      for (k = 0; k < INTRA_MODES; k++) {
        copy_cdf(frame_intra_ext_tx_cdf[i][j][k], adapted_intra_ext_tx_cdf[i][j][k], TX_TYPES)
      }
    }
  }
  copy_cdf(frame_seg_tree_cdf, adapted_seg_tree_cdf, MAX_SEGMENTS)
  for (i=0; i<SPATIAL_PREDICTION_PROBS; i++) {
    copy_cdf(frame_spatial_pred_seg_cdf[i], adapted_spatial_pred_seg_cdf[i], MAX_SEGMENTS)
  }

  for(a=0;a<2;a++) {
    for(a2=0;a2<INTRA_MODES;a2++) {
      copy_cdf(frame_uv_mode_cdf[a][a2], adapted_uv_mode_cdf[a][a2], UV_INTRA_MODES)
    }
  }

  for(a=0;a<PARTITION_CONTEXTS;a++) {
    copy_cdf(frame_partition_cdf[a], adapted_partition_cdf[a], EXT_PARTITION_TYPES)
  }

  copy_cdf(frame_delta_q_cdf, adapted_delta_q_cdf, DELTA_Q_SMALL+1)
  copy_cdf(frame_delta_lf_cdf, adapted_delta_lf_cdf, DELTA_LF_SMALL+1)
  for (i = 0; i < FRAME_LF_COUNT; i++)
    copy_cdf(frame_delta_lf_multi_cdf[i], adapted_delta_lf_multi_cdf[i], DELTA_LF_SMALL+1)

  copy_cdf(frame_filter_intra_mode_cdf, adapted_filter_intra_mode_cdf, FILTER_INTRA_MODES)
  for (i=0;i<BLOCK_SIZES_ALL;i++) {
    copy_cdf(frame_filter_intra_cdfs[i], adapted_filter_intra_cdfs[i], 2)
  }


  for (i=0; i<PALETTE_BSIZE_CTXS; i++) {
    copy_cdf(frame_palette_y_size_cdf[i], adapted_palette_y_size_cdf[i], PALETTE_SIZES)
    copy_cdf(frame_palette_uv_size_cdf[i], adapted_palette_uv_size_cdf[i], PALETTE_SIZES)
  }
  for (i=0; i<PALETTE_MAX_SIZE-1; i++) {
    for (j=0; j<PALETTE_COLOR_CONTEXTS; j++) {
      copy_cdf(frame_palette_y_color_cdf[i][j],
                  adapted_palette_y_color_cdf[i][j],
                  PALETTE_COLORS)
      copy_cdf(frame_palette_uv_color_cdf[i][j],
                  adapted_palette_uv_color_cdf[i][j],
                  PALETTE_COLORS)
    }
  }
  for (i=0; i<PALETTE_BSIZE_CTXS; i++)
    for (j=0; j<PALETTE_Y_MODE_CONTEXTS; j++)
      copy_cdf(frame_palette_y_mode_cdf[i][j], adapted_palette_y_mode_cdf[i][j], 2)
  for (i=0; i<PALETTE_UV_MODE_CONTEXTS; i++)
    copy_cdf(frame_palette_uv_mode_cdf[i], adapted_palette_uv_mode_cdf[i], 2)

  for (i=0;i<DIRECTIONAL_MODES;i++) {
    copy_cdf(frame_angle_delta_cdf[i], adapted_angle_delta_cdf[i], (2 * MAX_ANGLE_DELTA + 1))
  }

  for(a=0;a<MBSKIP_CONTEXTS;a++)
    copy_cdf(frame_mbskip_cdf[a], adapted_mbskip_cdf[a], 2)

  copy_cdf(frame_cfl_sign_cdf, adapted_cfl_sign_cdf, CFL_JOINT_SIGNS)
  for (i = 0; i < CFL_ALPHA_CONTEXTS; i++)
    copy_cdf(frame_cfl_alpha_cdf[i], adapted_cfl_alpha_cdf[i], CFL_ALPHABET_SIZE)

  copy_cdf(frame_switchable_restore_cdf, adapted_switchable_restore_cdf, RESTORE_SWITCHABLE_TYPES)
  copy_cdf(frame_wiener_cdf, adapted_wiener_cdf, 2)
  copy_cdf(frame_sgrproj_cdf, adapted_sgrproj_cdf, 2)
  for (i = 0; i < TXFM_PARTITION_CONTEXTS; i++)
    copy_cdf(frame_txfm_partition_cdf[i], adapted_txfm_partition_cdf[i], 2)
}



load_adapted_tile_inter_cdfs() {
  for(a2=0;a2<BLOCK_SIZE_GROUPS;a2++) {
    copy_cdf(frame_y_mode_cdf[a2], adapted_y_mode_cdf[a2], INTRA_MODES)
  }

  for(a=0;a<INTER_MODE_CONTEXTS;a++) {
    copy_cdf(frame_inter_compound_mode_cdf[a], adapted_inter_compound_mode_cdf[a], INTER_COMPOUND_MODES)
  }


  for(a=0;a<COMP_INDEX_CONTEXTS;a++) {
    copy_cdf(frame_compound_idx_cdf[a], adapted_compound_idx_cdf[a], 2)
  }
  for(a=0;a<COMP_GROUP_IDX_CONTEXTS;a++) {
    copy_cdf(frame_compound_group_idx_cdf[a], adapted_compound_group_idx_cdf[a], 2)
  }
  for(a=0;a<BLOCK_SIZES_ALL;a++) {
    copy_cdf(frame_compound_mask_type_cdf[a], adapted_compound_mask_type_cdf[a], 2)
  }

  for(a=0;a<BLOCK_SIZE_GROUPS;a++)
    copy_cdf(frame_interintra_mode_cdf[a], adapted_interintra_mode_cdf[a], INTERINTRA_MODES)
  for(a=0;a<BLOCK_SIZE_GROUPS;a++)
    copy_cdf(frame_interintra_cdf[a], adapted_interintra_cdf[a], 2)
  for(a=0;a<BLOCK_SIZES_ALL;a++) {
    copy_cdf(frame_wedge_idx_cdf[a], adapted_wedge_idx_cdf[a], 16)
    copy_cdf(frame_wedge_interintra_cdf[a], adapted_wedge_interintra_cdf[a], 2)
  }

  if (mcomp_filter_type == SWITCHABLE) {
    for(a=0;a<SWITCHABLE_FILTER_CONTEXTS;a++) {
      copy_cdf(frame_switchable_interp_cdf[a], adapted_switchable_interp_cdf[a], SWITCHABLE_FILTERS)
    }
  }

  // Note: The following has some slightly weird behaviour - we use motion_mode_cdf even if
  // new-multisymbol is disabled, but we only adapt it if new-multisymbol is on!
  // This is due to a bug in the reference code, but since new-multisymbol is adopted
  // it will eventually be ironed out anyway.
  for (i=BLOCK_8X8;i<BLOCK_SIZES_ALL;i++) {
    copy_cdf(frame_obmc_cdf[i], adapted_obmc_cdf[i], 2)
    copy_cdf(frame_motion_mode_cdf[i], adapted_motion_mode_cdf[i], MOTION_MODES)
  }

  for(a=0;a<NEWMV_MODE_CONTEXTS;a++)
    copy_cdf(frame_newmv_cdf[a], adapted_newmv_cdf[a], 2)
  for(a=0;a<GLOBALMV_MODE_CONTEXTS;a++)
    copy_cdf(frame_globalmv_cdf[a], adapted_globalmv_cdf[a], 2)
  for(a=0;a<REFMV_MODE_CONTEXTS;a++)
    copy_cdf(frame_refmv_cdf[a], adapted_refmv_cdf[a], 2)
  for(a=0;a<DRL_MODE_CONTEXTS;a++)
    copy_cdf(frame_drl_cdf[a], adapted_drl_cdf[a], 2)
  for(a=0;a<INTRA_INTER_CONTEXTS;a++)
    copy_cdf(frame_intra_inter_cdf[a], adapted_intra_inter_cdf[a], 2)
  for(a=0;a<COMP_INTER_CONTEXTS;a++)
    copy_cdf(frame_comp_inter_cdf[a], adapted_comp_inter_cdf[a], 2)
  for(i=0;i<MBSKIP_CONTEXTS;i++)
    copy_cdf(frame_mbskip_cdf[i], adapted_mbskip_cdf[i], 2)
  for(i=0;i<SKIP_MODE_CONTEXTS;i++)
    copy_cdf(frame_skip_mode_cdf[i], adapted_skip_mode_cdf[i], 2)
  for (i = 0; i < REF_CONTEXTS; i++)
    for (j = 0; j < FWD_REFS-1; j++)
      copy_cdf(frame_comp_ref_cdf[i][j], adapted_comp_ref_cdf[i][j], 2)
  for (i = 0; i < REF_CONTEXTS; i++)
    for (j = 0; j < BWD_REFS-1; j++)
      copy_cdf(frame_comp_bwdref_cdf[i][j], adapted_comp_bwdref_cdf[i][j], 2)
  for (i = 0; i < REF_CONTEXTS; i++)
    for (j = 0; j < SINGLE_REFS-1; j++)
      copy_cdf(frame_single_ref_cdf[i][j], adapted_single_ref_cdf[i][j], 2)
  for(i=0;i<COMP_REF_TYPE_CONTEXTS;i++)
    copy_cdf(frame_comp_ref_type_cdf[i], adapted_comp_ref_type_cdf[i], 2)
  for (i = 0; i < UNI_COMP_REF_CONTEXTS; i++)
    for (j = 0; j < SIGNALLED_UNIDIR_COMP_REFS - 1; j++)
        copy_cdf(frame_uni_comp_ref_cdf[i][j], adapted_uni_comp_ref_cdf[i][j], 2)
  for (i = 0; i < PREDICTION_PROBS; i++)
    copy_cdf(frame_segment_pred_cdf[i], adapted_segment_pred_cdf[i], 2)
  for (i = 0; i < NMV_CONTEXTS; i++)
    for (j = 0; j < 2; j++)
      copy_cdf(frame_sign_cdf[i][j], adapted_sign_cdf[i][j], 2)
  for (i = 0; i < NMV_CONTEXTS; i++)
    for (j = 0; j < 2; j++)
      copy_cdf(frame_class0_hp_cdf[i][j], adapted_class0_hp_cdf[i][j], 2)
  for (i = 0; i < NMV_CONTEXTS; i++)
    for (j = 0; j < 2; j++)
      copy_cdf(frame_hp_cdf[i][j], adapted_hp_cdf[i][j], 2)
  for (i = 0; i < NMV_CONTEXTS; i++)
    for (j = 0; j < 2; j++)
      copy_cdf(frame_class0_cdf[i][j], adapted_class0_cdf[i][j], 2)
  for (i = 0; i < NMV_CONTEXTS; i++)
    for (j = 0; j < MV_OFFSET_BITS; j++)
      for (k = 0; k < 2; k++)
        copy_cdf(frame_mvcomp_bits_cdf[i][j][k], adapted_mvcomp_bits_cdf[i][j][k], 2)
}
