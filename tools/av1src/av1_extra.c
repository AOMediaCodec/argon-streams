/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

// Data that needs to be preserved across frames
global_data() {
  // Flags which affect the overall file format.
  // These are currently set in ivf_file_header()
  //
  // Does the stream use an IVF container, or is it a stream of raw OBUs?
  autostruct uint1 is_ivf_file

  // Should the OBUs be in annex-b (length before header) or non-annex-b (length
  // after header) format?
  autostruct uint1 use_annexb

#if DECODE
  // Force disable film grain in decode
  autostruct uint1 override_no_film_grain

  // The notional frame rate from the command line. This is ignored if
  // the most recent sequence header contains timing info, but is how
  // we pass backdoor information to the decoder model (as specified
  // in E3.3 of the spec)
  autostruct uint32 default_frame_rate
#endif

  // Type of current frame (needs to be in global as is used by the following
  // frame to update last_frame_type)
  autostruct uint1 frame_type

  // Marks whether we have seen a key frame yet
  autostruct uint1 decoded_key_frame

#if DECODE
  autostruct uint64 input_length // Number of bytes in input file
#endif

#if DECODE
  // To help decide if all frames are the same size (for the CSV files), track
  // the size of the previous frame in decode order
  autostruct uint_1_65536 last_width
  autostruct uint_1_65536 last_height
#endif

  // We need to save some information about previous frames' sizes:
  // * Crop size of each reference image.
  //   Currently only seems to be used by frame_can_use_prev_frame_mvs()
  autostruct uint_1_65536 ref_crop_width[NUM_REF_FRAMES]
  autostruct uint_1_65536 ref_crop_height[NUM_REF_FRAMES]

  // * Size of each reference image.
  //   Note: this is the upscaled size of each frame, but there are certain cases where we want
  //   to compare a ref_width to the current frame's crop_width, eg. to determine how much we
  //   need to scale the reference image by when performing inter prediction.
  autostruct uint_1_65536 ref_width[NUM_REF_FRAMES]
  autostruct uint_1_65536 ref_height[NUM_REF_FRAMES]

  // * Display size of each reference image
  //   This is only used when reusing the size of a reference image - in that case,
  //   we copy both the upscaled and display sizes
  autostruct uint_1_65536 ref_display_width[NUM_REF_FRAMES]
  autostruct uint_1_65536 ref_display_height[NUM_REF_FRAMES]

  autostruct uint16 g_w
  autostruct uint16 g_h
  // Number of frames in the stream, *if we know it*.
  // We know the number of frames if either:
  // i) We're encoding, or
  // ii) This is an IVF format file, as the IVF format includes a frame count in the file header.
#if ENCODE
  autostruct int32 enc_frame_cnt
  autostruct uint1 enc_tile_group_focus
  autostruct uint_1_128 enc_anchor_frame_cnt
  autostruct uint32 enc_tile_list_frame_cnt

  // Special stuff for choose_levelMax():

  // Sometimes the encoder selects a nominal level for a stream,
  // but signals it as level 9.3 anyway. In this case, we are allowed
  // to use many more tiles than the actual level limit.
  // We want to generate some streams which follow the level-based limit
  // and others which don't; this flag picks between these behaviours
  autostruct uint1 enc_allow_too_many_tiles

  // In some cases, we want to ensure that we generate as many tile
  // rows or tile cols as possible - in particular, so we can hit
  // the 64 tile rows * 64 tile cols limit.
  autostruct uint1 enc_force_max_tile_cols
  autostruct uint1 enc_force_max_tile_rows

  autostruct uint1 enc_use_frame_planner

  // Used for memory stress streams:
  // For these streams, we want to set up target_mv[] "early" (at the same time
  // that we pick a mode, rather than during assign_mv()). So we need to disable
  // the call to enc_pick_mv() inside assign_mv() in this case.
  // This flag implements that behaviour - it's set to 0 at the beginning of time,
  // and if it is set to 1 when we reach assign_mv() then we'll skip enc_pick_mv()
  // and reset the flag to 0.
  autostruct uint1 enc_stress_memory
#endif // ENCODE
  autostruct uint32 frame_cnt

  // Reference frames can have different profiles
  autostruct uint2 ref_profile[NUM_REF_FRAMES]
  autostruct uint1 ref_subsampling_x[NUM_REF_FRAMES]
  autostruct uint1 ref_subsampling_y[NUM_REF_FRAMES]
  autostruct uint8 ref_color_primaries[NUM_REF_FRAMES]
  autostruct uint8 ref_transfer_characteristics[NUM_REF_FRAMES]
  autostruct uint8 ref_matrix_coefficients[NUM_REF_FRAMES]
  autostruct uint2 ref_chroma_sample_position[NUM_REF_FRAMES]
  autostruct uint_8_12 ref_bit_depth[NUM_REF_FRAMES]
#if ENCODE
  autostruct uint_0_2 enc_frame_id_mode
#endif
  // Note: We signal (# bits in frame id) - 7, and that value is sent using 4 bits.
  // That means that the maximum number of bits in a frame ID is 15 + 7 = 22.
  autostruct uint22 ref_frame_id[NUM_REF_FRAMES]

  // There are various constraints which might mean that we can't use a particular
  // reference slot in the current frame:
  // Some constraints need to be tracked across frames:
  // * The frame ID needs to be recent
  // * If we have an error-resilient frame, we explicitly signal the order hints
  //   for all of the reference slots. If using scalability, these might differ from
  //   the decoder's current idea of the reference slots' order hints. If that happens,
  //   it means that we can't use that particular ref slot until it is refreshed.
  autostruct uint1 ref_valid[NUM_REF_FRAMES]

  // In addition, a frame can only reference frames from the same or lower
  // spatial and temporal layers.
  // This condition needs to be recomputed each frame, so we combine 'ref_valid'
  // with this constraint each frame to form the following array.
  autostruct uint1 ref_valid_this_frame[NUM_REF_FRAMES]

  autostruct uint22 current_frame_id
  autostruct uint22 prev_frame_id
  autostruct int32 current_video_frame
  // Note:
  // saved_ref_decode_order[slot][0] = logical index of the frame in the given slot,
  //                                   with wrapping mod 2^(order_hint_bits)
  // saved_ref_decode_order[slot][i] (i > 0) = logical index of the i'th reference frame
  //                                           of the frame in the given slot,
  //                                           with wrapping mod 2^(order_hint_bits)
  autostruct int32 saved_ref_decode_order[NUM_REF_FRAMES][1+INTER_REFS_PER_FRAME]
  // shifted_order_hints[i] == decode index of the frame in the i'th slot,
  // reduced into a range *centered about the current frame*.
  // This has the effect that shifted_order_hints[i] <= shifted_order_hints[j] is equivalent to
  // frame i being earlier than frame j (in logical order, not decode order).
  // This is used during short-refs signalling setup.
  autostruct int32 shifted_order_hints[NUM_REF_FRAMES]

#if ENCODE
  autostruct uint1 enc_frame_id_numbers_present_flag
#endif // ENCODE
  autostruct uint1 frame_id_numbers_present_flag
  autostruct uint_2_17 delta_frame_id_length
  autostruct uint_1_25 frame_id_length
#  if ENCODE
  // Next line only for bit-stream generator
  autostruct uint8 additional_frame_id_length
  autostruct uint1 enc_monochrome
#  endif // ENCODE
  autostruct uint1 monochrome
  autostruct uint2 NumPlanes

#if ENCODE
  autostruct uint2 enc_tile_group_mode // ENC_TG_MODE_*
#endif

  autostruct uint8 frame_width_bits
  autostruct uint8 frame_height_bits
  autostruct uint32 max_frame_height
  autostruct uint32 max_frame_width

  autostruct uint32 codedMaxFrameWidth
  autostruct uint32 codedMaxFrameHeight

  // Scalability structure information
  autostruct uint1 CVSTemporalGroupDescriptionPresentFlag
  autostruct uint2 spatial_layers_count_minus1
  autostruct uint1 spatial_layer_dimensions_present
  autostruct uint1 spatial_layer_description_present
  autostruct uint1 temporal_group_description_present

  autostruct uint16 spatial_layer_max_width[MAX_NUM_SPATIAL_LAYERS]
  autostruct uint16 spatial_layer_max_height[MAX_NUM_SPATIAL_LAYERS]

  autostruct uint8 spatial_layer_ref_id[MAX_NUM_SPATIAL_LAYERS]

  autostruct uint8 temporal_group_size
  autostruct uint3 temporal_group_temporal_id[MAX_TEMPORAL_GROUP_SIZE]
  autostruct uint1 temporal_group_temporal_switching_up_point_flag[MAX_TEMPORAL_GROUP_SIZE]
  autostruct uint1 temporal_group_spatial_switching_up_point_flag[MAX_TEMPORAL_GROUP_SIZE]
  autostruct uint2 temporal_group_ref_cnt[MAX_TEMPORAL_GROUP_SIZE]
  autostruct uint8 temporal_group_ref_pic_diff[MAX_TEMPORAL_GROUP_SIZE][7]

  autostruct uint8 itu_t_t35_country_code
  autostruct uint8 itu_t_t35_country_code_extension_byte

  autostruct uint16 max_cll
  autostruct uint16 max_fall

  autostruct uint16 primary_chromaticity_x [3]
  autostruct uint16 primary_chromaticity_y [3]
  autostruct uint16 white_point_chromaticity_x
  autostruct uint16 white_point_chromaticity_y
  autostruct uint32 luminance_max
  autostruct uint32 luminance_min

  autostruct uint5  counting_type
  autostruct uint1  full_timestamp_flag
  autostruct uint1  discontinuity_flag
  autostruct uint1  cnt_dropped_flag
  autostruct uint9  n_frames
  autostruct uint6  seconds_value
  autostruct uint6  minutes_value
  autostruct uint5  hours_value
  autostruct uint1  seconds_flag
  autostruct uint1  minutes_flag
  autostruct uint1  hours_flag
  autostruct uint5  time_offset_length
  autostruct uint32 time_offset_value

#if ENCODE
  // If using a custom temporal group, we need to be able to easily look up
  // whether an enhancement frame (spatial layer >= 1) in temporal unit i
  // can reference a frame in temporal unit j.
  // To help with this, we calculate the distance from each temporal unit in the
  // temporal group to the latest spatial switching-up point.
  // This value is set to ENC_MAX_FRAMES if no spatial switching-up points are
  // present.
  autostruct uint32 enc_max_enhancement_ref_dist[MAX_TEMPORAL_GROUP_SIZE]
#endif

  autostruct int32 ref_gm_params[2+NUM_REF_FRAMES][TOTAL_REFS_PER_FRAME][6]
  rewrite gm_params[j][k] ref_gm_params[0][j][k]
  rewrite prev_gm_params[j][k] ref_gm_params[1][j][k]

  // Subsampling seems to be read at keyframes and assumed to stay the same for the following frames
  autostruct uint1 color_range
  autostruct uint1 subsampling_x
  autostruct uint1 subsampling_y
  autostruct uint1 has_alpha
  autostruct uint1 plane_type[MAX_MB_PLANE]
  autostruct uint1 plane_subsampling_x[MAX_MB_PLANE]
  autostruct uint1 plane_subsampling_y[MAX_MB_PLANE]

  autostruct uint_8_12 bit_depth
  autostruct uint2 profile

  autostruct uint1 colorDescriptionPresent
  autostruct uint8 color_primaries
  autostruct uint8 transfer_characteristics
  autostruct uint8 matrix_coefficients
  autostruct uint2 chroma_sample_position

  // Sequence-level enable-disable flags for coding tools
  autostruct uint1 enable_filter_intra
  autostruct uint1 enable_intra_edge_filter
  autostruct uint1 enable_interintra_compound
  autostruct uint1 enable_masked_compound
  autostruct uint1 enable_warped_motion
  autostruct uint1 enable_dual_filter
  autostruct uint1 enable_order_hint
  autostruct uint1 enable_jnt_comp
  autostruct uint1 enable_ref_frame_mvs
  autostruct uint1 seq_choose_screen_content_tools_bit
  autostruct uint2 seq_force_screen_content_tools
  autostruct uint2 seq_force_integer_mv
  autostruct uint1 enable_superres
  autostruct uint1 enable_cdef
  autostruct uint1 enable_loop_restoration
#if ENCODE
  // Note: If screen content tools are enabled, then palette is always allowed,
  // whereas the other tools (intrabc, integer-only MVs) are behind additional flags.
  // So, to balance things out, we make it so that we don't always use palette mode
  // even if screen content tools are used for a particular stream.
  autostruct uint1 enc_enable_palette
#endif

  // Sequence-level flag: Do we use the full "video" frame headers, or reduced
  // "image" headers?

  autostruct uint1 SeenSeqHeaderBeforeFirstFrameHeader
  autostruct uint1 SeenSeqHeader
  autostruct uint1 still_picture
  autostruct uint1 reduced_still_picture_hdr

  autostruct uint3 order_hint_bits_minus1

  autostruct uint3 temporal_layer
  autostruct int_(-1)_7 TemporalIds[MAX_SPATIAL_ID+1]
  autostruct uint2 spatial_layer

  // Note: A frame in temporal layer T and spatial layer S is only allowed to
  // reference frames with temporal layer <= T and spatial layer <= S.
  autostruct uint3 ref_temporal_layer[NUM_REF_FRAMES]
  autostruct uint2 ref_spatial_layer[NUM_REF_FRAMES]

  autostruct uint8 num_operating_points
  autostruct uint8 temporal_layers_used[MAX_NUM_OPERATING_POINTS]
  autostruct uint4 spatial_layers_used[MAX_NUM_OPERATING_POINTS]
  autostruct uint8 operating_point_level[MAX_NUM_OPERATING_POINTS]
  autostruct uint1 tier[MAX_NUM_OPERATING_POINTS]
  autostruct uint1 decoder_rate_model_present[MAX_NUM_OPERATING_POINTS]
  autostruct uint4 initial_display_delay[MAX_NUM_OPERATING_POINTS]
  // The following variable is a combination of spatial_layers_used and
  // temporal_layers_used, and is defined purely to be exposed in the
  // configuration language
  autostruct uint12 operating_point_idc[MAX_NUM_OPERATING_POINTS]

  autostruct int LumaSamplesInTU
  autostruct int FramesDecodedInTU
  autostruct int DisplayedLumaSamplesInTU
  autostruct int FramesDisplayedInTU
  autostruct int BytesInFrame

#if DECODE
  // Note: libaom's "current_operating_point" is actually a combination
  // of temporal_layers_used[current_operating_point_id]
  // and spatial_layers_used[current_operating_point_id]
  autostruct uint8 current_operating_point_id
  autostruct uint8 desired_oppoint

  // Used to find the maximum number of operating points in this stream
  //    -1 means not active, >=0 active
  autostruct int max_num_oppoints

  // Special: When the final frame of a sequence isn't in the current operating point,
  // we shouldn't output anything
  autostruct uint1 skipped_final_frame
#endif

#if ENCODE
  autostruct int enc_override_tier
  autostruct uint1 enc_choose_max_bitrate
  autostruct uint1 enc_client_a_custom_set
#endif // ENCODE

  // The decoder has a notion of the current parameters
  // These parameters can only switch at an intra or key frame
  autostruct uint8 dec_color_primaries
  autostruct uint8 dec_transfer_characteristics
  autostruct uint8 dec_matrix_coefficients
  autostruct uint2 dec_chroma_sample_position
  autostruct uint2 dec_profile
  autostruct uint_8_12 dec_bit_depth
  autostruct uint1 dec_subsampling_x
  autostruct uint1 dec_subsampling_y
  autostruct uint1 timing_info_present
  autostruct uint1 equal_picture_interval
  autostruct uint1 display_model_info_present_flag
  autostruct uint1 decoder_model_info_present_flag
  autostruct uint1 display_model_param_present_flag[MAX_NUM_OPERATING_POINTS]
  autostruct uint1 decoder_model_param_present_flag[MAX_NUM_OPERATING_POINTS]
  autostruct uint5 encoder_decoder_buffer_delay_length
  autostruct uint5 frame_presentation_delay_length
  autostruct uint5 buffer_removal_delay_length
  autostruct uint1 buffer_removal_delay_present

#if ENCODE
  autostruct uint12 opPtIdc
  autostruct uint1 inTemporalLayer
  autostruct uint1 inSpatialLayer
#endif

  // Stream-level timing parameters
  //
  // Note: In the spec, everything appears to be measured in seconds,
  // and all values are real numbers so can take any value.
  // For our purposes, we measure times in units of 1/90000 of a second
  // and always round appropriately, so that all times are integer multiples
  // of this basic unit.
  // Note also: Many of these values are read as uint32s, but we store
  // them in int64 variables to avoid overflow.
  autostruct int64 time_scale
  autostruct int64 num_units_in_display_tick
  autostruct int64 num_units_in_decoding_tick
  autostruct int64 DecCT // Decoding clock tick length
  autostruct int64 DispCT // Display clock tick length
  autostruct int64 num_ticks_per_picture // Used if equal_picture_interval == 1
#if ENCODE
  autostruct int64 enc_num_units_in_decoding_tick
  autostruct int64 enc_num_units_in_display_tick
  autostruct int64 enc_num_ticks_per_picture

  autostruct int64 enc_frame_rate
#endif

  // Per-operating point timing parameters
  autostruct uint32 decoder_buffer_delay[MAX_NUM_OPERATING_POINTS]
  autostruct uint32 encoder_buffer_delay[MAX_NUM_OPERATING_POINTS]
  autostruct uint1  low_delay_mode_flag[MAX_NUM_OPERATING_POINTS]
  autostruct int64  Time[MAX_NUM_OPERATING_POINTS]
  autostruct int64  InitialPresentationDelay[MAX_NUM_OPERATING_POINTS]
  autostruct int64  MaxBitrate[MAX_NUM_OPERATING_POINTS]

  // Track the decoder buffer size for each operating point, so that we can check
  // underflow / overflow conditions.
  // Note: In order to get this correct, we need to maintain two separate time
  // variables, which both advance with each frame:
  // * "BufferTime" advances to the LastBitArrival time for each frame
  // * "Time" advances to the Removal time for each frame
  // For convenience, we also track the index of the "next" DFG which will be removed
  // after BufferTime
  autostruct int64 BufferTime[MAX_NUM_OPERATING_POINTS]
  autostruct int64 NextRemovalIdx[MAX_NUM_OPERATING_POINTS]
  autostruct int64 BufferSize[MAX_NUM_OPERATING_POINTS]
  autostruct int64 MaxBufferSize[MAX_NUM_OPERATING_POINTS]

  // Per-frame timing parameters.
  // buffer_removal_delay is maintained per DFG, per operating point, and is
  // measured in decoder clock ticks
  autostruct int64 buffer_removal_delay[MAX_NUM_OPERATING_POINTS][ENC_MAX_FRAMES]
  // Meanwhile, frame_presentation_time is maintained per shown frame, and is
  // measured in display clock ticks
  // Important: Only one value is signalled per frame, independent of operating point.
  // However, which frames are shown and their ordering may depend on the operating
  // point, so we maintain this array per operating point.
  autostruct int64 frame_presentation_time[MAX_NUM_OPERATING_POINTS][ENC_MAX_FRAMES]

  // For each displayed frame, there is a minimum interval that it must be displayed for.
  // We can only check this once we know the presentation time of the *next* frame.
  // So, after decoding each shown frame, we work out its minimum presentation interval
  // and store it here.
  autostruct int64 minPresentationInterval[MAX_NUM_OPERATING_POINTS]

#if ENCODE
  // Track the locations of the frame timing parameters within the uncompressed
  // header.
  // Important: This is stored as an offset from the start of the uncompressed header,
  // *not* an absolute bit position. This is because we generate the uncompressed header
  // well before we pack the frame into OBUs, so at the time these variables are written
  // we don't know their eventual absolute bit positions.
  // This information is only needed until the end of encoding each frame, so we
  // don't need a frame number index.
  autostruct int64 enc_buffer_removal_delay_offset[MAX_NUM_OPERATING_POINTS]
  autostruct int64 enc_tu_presentation_delay_offset

  // We can only signal one presentation time per frame, but we get one set of
  // constraints per operating point. So we use an extra variable to track the
  // earliest possible presentation time for each frame across all operating points
  // in which it is decoded.
  autostruct int64 enc_min_presentation_time
#endif

  autostruct uint64 tuPresentationDelay

  // Derived timing information. In the spec, these depend only on the
  // DFG index. However, in practice they also depend on the selected
  // operating point.
  // We only list values here which we need to carry between frames.
  autostruct int64 RemovalTime[MAX_NUM_OPERATING_POINTS][ENC_MAX_FRAMES]
  autostruct int64 TimeToDecode[MAX_NUM_OPERATING_POINTS][ENC_MAX_FRAMES]

  // Virtual buffer structure. Again, these depend on the selected operating point
  autostruct int8 VBI[MAX_NUM_OPERATING_POINTS][DECODER_MODEL_VBI_SIZE] // Virtual buffer indices
  autostruct int DecoderRefCount[MAX_NUM_OPERATING_POINTS][DECODER_MODEL_BUFFER_POOL_SIZE]
  autostruct int PlayerRefCount[MAX_NUM_OPERATING_POINTS][DECODER_MODEL_BUFFER_POOL_SIZE]

  // Note: I'm not convinced that the spec maintains reference counts and
  // display timings correctly for frames which are decoded before we know
  // the value of InitialPresentationDelay.
  //
  // So, instead of following the spec, we augment each frame buffer with its
  // *latest known* index in display order. Ie, if a frame buffer is shown multiple
  // times, then we only track the last one in display order.
  //
  // This approach means that, once we do know InitialPresentationDelay,
  // it's easy to work out the correct presentation times for all frames.
  autostruct int64 LatestDisplayIdx[MAX_NUM_OPERATING_POINTS][DECODER_MODEL_BUFFER_POOL_SIZE]



  // Segmentation data
  autostruct uint3 seg_maps[][]
  autostruct int16 ref_feature_data[2+NUM_REF_FRAMES][MAX_SEGMENTS][SEG_LVL_MAX]
  autostruct uint32 ref_feature_mask[2+NUM_REF_FRAMES][MAX_SEGMENTS]
  autostruct uint_0_7 ref_last_active_seg_id[2+NUM_REF_FRAMES]
  autostruct uint_0_7 ref_preskip_seg_id[2+NUM_REF_FRAMES]

  rewrite seg_map[i] seg_maps[0][i]
  rewrite feature_data[i][j] ref_feature_data[0][i][j]
  rewrite feature_mask[i] ref_feature_mask[0][i]
  rewrite prev_frame_seg_map[i] seg_maps[1][i]
  rewrite prev_feature_data[i][j] ref_feature_data[1][i][j]
  rewrite prev_feature_mask[i] ref_feature_mask[1][i]
  rewrite last_active_seg_id ref_last_active_seg_id[0]
  rewrite preskip_seg_id ref_preskip_seg_id[0]

  // Mirrors of preskip_seg_id and last_active_seg_id which get
  // updated at the times shown in the spec (otherwise code that
  // overrides feature_enabled or feature_value could see the
  // difference).
  autostruct uint_0_7 SegIdPreSkip
  autostruct uint_0_7 LastActiveSegId

  // Size of the per-MI information arrays we've carried over from previous frames
  autostruct uint_1_8192 ref_mi_rows[2+NUM_REF_FRAMES]
  autostruct uint_1_8192 ref_mi_cols[2+NUM_REF_FRAMES]

  // Motion vector information
  autostruct int24 rmvs[][][][][] //prev,col,row,reflist,block_idx,comp
  rewrite mvs[prev][col][row][list][block][comp] rmvs[prev][row*ref_mi_cols[prev]+col][list][block][comp]
# if STACKED_MFMV
  autostruct int24 rmotion_field_mvs_stacked[][][][]
  rewrite motion_field_mvs_stacked[ref][col][row][stack][comp] rmotion_field_mvs_stacked[ref - LAST_FRAME][row*(mi_cols>>(3-MI_SIZE_LOG2))+col][stack][comp]
# endif // STACKED_MFMV
  autostruct int24 rmotion_field_mvs[][][]
  rewrite motion_field_mvs[ref][col][row][comp] rmotion_field_mvs[ref - LAST_FRAME][row*(mi_cols>>(3-MI_SIZE_LOG2))+col][comp]
  autostruct int8 refFrameSide[TOTAL_REFS_PER_FRAME]
  autostruct int_(-1)_6 rrefframes[][][] // prev,col,row,reflist
  rewrite refframes[prev][col][row][list] rrefframes[prev][row*ref_mi_cols[prev]+col][list] // prev,col,row,reflist
  autostruct int24 rpredmvs[][][][][] //prev,col,row,reflist,block_idx,comp
  rewrite pred_mvs[prev][col][row][list][block][comp] rpredmvs[prev][row*ref_mi_cols[prev]+col][list][block][comp]
  autostruct int32 ref_frame_number[NUM_REF_FRAMES]  // Use this to keep track of whether our prev_mv corresponds to a valid reference frame
  autostruct int32 prev_mv_frame_number
  // Which reference slot (out of the 8-element potential reference list) holds prev_frame?
  autostruct uint3 prev_frame_slot
  autostruct uint1 ref_intra_only[NUM_REF_FRAMES] // Whether previous frame was intra_only or not
  // Store the frame_type for each reference slot
  autostruct uint2 ref_type[NUM_REF_FRAMES]

  autostruct uint1 separate_uv_delta_q

  // Loopfilter modifications to filter level
  autostruct int_(-63)_63 frame_ref_lf_deltas[2+MAX_REF_FRAMES][TOTAL_REFS_PER_FRAME]
  autostruct int_(-63)_63 frame_mode_lf_deltas[2+MAX_REF_FRAMES][MAX_MODE_LF_DELTAS]
  rewrite ref_lf_deltas[i] frame_ref_lf_deltas[0][i]
  rewrite mode_lf_deltas[i] frame_mode_lf_deltas[0][i]

  // Helper variables, to track:
  // i) Whether we've allocated the various frame buffers yet,
  // ii) What size we've allocated them as
  autostruct uint32 aloc_frame_width
  autostruct uint32 aloc_frame_height

  // Reference frames
  autostruct uint16 rframestore[][][]  // uint16 just to allow display_yuv to work unchanged
  rewrite framestore[idx][plane][x][y] rframestore[idx][plane][y*ref_width[idx]+x]
  autostruct uint16 ranchor_framestore[][][]  // uint16 just to allow display_yuv to work unchanged
  rewrite anchor_framestore[idx][plane][x][y] ranchor_framestore[idx+1][plane][y*anchor_width+x]
  autostruct uint_1_65536 anchor_width

  // Note: For technical reasons, we can't set up the wedge masks at the same time as the other
  // global data. So we postpone setting them up until the first time a wedge is used.
  autostruct uint1 wedge_initialized // Have the wedge masks been set up yet?
  autostruct uint8 wedge_masks[BLOCK_SIZES_ALL][2][WEDGE_TYPES][MAX_WEDGE_SIZE*MAX_WEDGE_SIZE]

  // When using short ref signalling, we store the expanded list of reference frames
  // into a special array. This is needed because setup_short_frame_refs() is called
  // both by the main decode process and by the frame scheduler
  autostruct int4 short_ref_idx[ALLOWED_REFS_PER_FRAME]

  // When we use short ref signalling, this array is used to (try to)
  // select different reference slots for each element of active_ref_idx
  autostruct uint1 usedFrame[NUM_REF_FRAMES]

#if ENCODE
  // Frame schedule information - this is set up by enc_schedule_frames() at the beginning of encoding
  autostruct uint32 sched_temporal_unit[ENC_MAX_FRAMES]
  autostruct uint8 sched_spatial_layer[ENC_MAX_FRAMES]
  autostruct uint8 sched_temporal_layer[ENC_MAX_FRAMES]
  autostruct uint1 sched_show_frame[ENC_MAX_FRAMES] // Shown or show-existing
  autostruct uint1 sched_showable_frame[ENC_MAX_FRAMES] // Marks which non-shown frames get displayed via a show-existing frame
  autostruct int8 sched_active_ref_idx[ENC_MAX_FRAMES][INTER_REFS_PER_FRAME] // -1 or a reference slot
  autostruct uint8 sched_refresh_frame_flags[ENC_MAX_FRAMES]

  // We might construct "links" between frames, to say that a particular frame
  // must reference a particular reference slot. This is used to ensure that all
  // frames are verified properly.
  autostruct uint3 sched_num_links[ENC_MAX_FRAMES]
  autostruct uint3 sched_linked_frame[ENC_MAX_FRAMES][INTER_REFS_PER_FRAME]

  // We also mark a small number of frames as "reference barriers".
  // See enc_schedule_barriers() for what that means
  autostruct uint1 sched_barrier_frame[ENC_MAX_FRAMES]

  autostruct uint1 sched_show_existing_frame[ENC_MAX_FRAMES]
  autostruct uint3 sched_index_of_existing_frame[ENC_MAX_FRAMES]
  autostruct uint2 sched_frame_type[ENC_MAX_FRAMES]
  autostruct uint1 sched_use_short_ref_signalling[ENC_MAX_FRAMES]
  autostruct uint22 sched_frame_id[ENC_MAX_FRAMES]
  autostruct uint8 sched_order_hint[ENC_MAX_FRAMES]

  // We also keep a table of which reference slots have been assigned at each point
  // in time. The entries here use -1 to indicate "no assigned"
  //
  // Important: sched_frame_in_ref_slot[f] describes the reference slots *after*
  // frame f has been decoded. Thus the state at the *start* of decoding frame f
  // is described by sched_frame_in_ref_slot[f-1]
  autostruct int32 sched_frame_in_ref_slot[ENC_MAX_FRAMES][MAX_REF_FRAMES]

  // Certain frames may need to be of specific types, in order to start the stream
  // in a valid way. See the define block above enc_schedule_scalability() for
  // possible values.
  autostruct uint8 sched_special_frame[ENC_MAX_FRAMES]

  // In the encoder, we need to make sure that each frame's ID is valid no matter
  // what operating point it's being decoded as part of. To do this, we need to track the last
  // frame seen in each operating point
  // Normally there is only one possible "previous frame" for a particular operating point,
  // but the frame immediately after a forward keyframe is shown has two - in normal decoding,
  // the previous frame is whatever was coded before the show-existing frame, but in random-access
  // decoding the previous frame is the forward keyframe itself.
  // To keep track of these two options, we need to store two elements per operating point.
  // Element 0 is the "normal" previous frame, element 1 (if not equal to -1) is a forward keyframe.
  autostruct uint32 sched_last_frame_num[ENC_MAX_FRAMES][MAX_NUM_OPERATING_POINTS][2]

  // For each frame, we maintain a list of which ref slots are allowed, and (if using
  // order hints) which ones count as "forward references"
  autostruct uint3 sched_valid_refs[NUM_REF_FRAMES]
  autostruct uint3 sched_num_valid_refs
  autostruct uint3 sched_fwd_refs[NUM_REF_FRAMES]
  autostruct uint3 sched_num_fwd_refs

  // Encoder settings
  autostruct uint1 enc_use_scalability // Are we generating scalability information?
  autostruct uint1 enc_use_scalability_preset // If so, are we using a preset mode or a custom mode?
  autostruct uint8 enc_scalability_mode
  autostruct uint3 enc_temporal_layers
  autostruct uint2 enc_spatial_layers
  autostruct uint1 enc_special_operating_points

  // The value that was actually read in read_scalability
  autostruct uint8 scalability_mode

  // enc_spatial_ref_allowed[i] & (1 << j) == "Is spatial layer i allowed
  // to reference spatial layer j?"
  autostruct uint8 enc_spatial_ref_allowed[MAX_NUM_SPATIAL_LAYERS]
  // Similar for the temporal layers
  autostruct uint8 enc_temporal_ref_allowed[MAX_NUM_TEMPORAL_LAYERS]

  autostruct uint8 enc_num_operating_points
  autostruct uint8 enc_temporal_layers_used[MAX_NUM_OPERATING_POINTS]
  autostruct uint4 enc_spatial_layers_used[MAX_NUM_OPERATING_POINTS]

  // The level we're targeting for each operating point.
  //
  // Note: When enc_use_max_params_level is set, this may be set to some
  // defined level (which we're trying to target) while operating_point_level[i] == 31,
  // reflecting the fact that we don't tell the decoder what level we're targeting.
  // This is intentional.
  autostruct uint8 enc_operating_point_level[MAX_NUM_OPERATING_POINTS]

  // For each (spatial, temporal) layer pair, which operating points decode
  // this combination of layers?
  // Operating point 'i' is represented by bit 'i' in the result.
  autostruct uint32 enc_layers_to_operating_points[MAX_NUM_SPATIAL_LAYERS][MAX_NUM_TEMPORAL_LAYERS]

  // For each (spatial, temporal) layer pair, what is the lowest level among all
  // of the operating points which decode this layer?
  autostruct uint8 enc_layers_to_min_level[MAX_NUM_SPATIAL_LAYERS][MAX_NUM_TEMPORAL_LAYERS]

  // Sequence-level encode parameters:
  autostruct uint_0_100 enc_lossless_prob // Probability that each frame is lossless
  autostruct uint_0_100 enc_hide_frame_prob // Prob of having a hidden frame
  autostruct uint_0_100 enc_superres_prob // Probability of using superres, when it is possible
  autostruct uint1 enc_force_seg_lvl_ref_frame_last_frame
  autostruct uint1 enc_allow_scaling // Do we allow generating frames of different sizes?
  autostruct uint1 enc_still_picture // Is this a "still image" stream?
  autostruct uint1 enc_reduced_headers // If so, do we want to use the reduced frame header format?
  autostruct uint1 enc_enable_order_hint
  autostruct uint1 enc_allow_short_signalling
  autostruct uint8 enc_order_hint_bits

  // Frame/tile-level encode parameters:
  autostruct uint_0_100 enc_split_prob // Probability we split a partition
  autostruct uint_0_100 enc_skip_prob  // Probability we skip a block
  autostruct uint_0_100 enc_zero_coef_prob // Probability coefficients are zero
  autostruct uint_0_100 enc_use_large_coefs_prob // Probability of allowing large coeffs to be selected
  // When using patterned signs, the probability of any given row/col being all zero
  autostruct uint_0_100 enc_zero_rowcol_prob
  // When when generating coefficients, we sometimes apply a weird transform to
  // them, making them much larger. This governs the probability "transforms per
  // million" of doing so.
  autostruct int32      enc_tx_coeffs_per_million
  autostruct uint_0_100 enc_inter_prob // Probability of using an inter block
  autostruct uint_0_100 enc_compound_prob // Probability of using a compound block
  autostruct uint_0_100 enc_empty_block_prob // Probability of coding a completely empty transform block
  autostruct uint_0_100 enc_palette_prob // Prob of using a palette for a given block (if screen content tools are enabled)
  autostruct uint_0_100 enc_bias_to_big_partitions_prob
  autostruct uint_0_100 enc_mv_small_prob
  autostruct int32 enc_base_qindex
  autostruct uint1 enc_lossless // True if the current frame is lossless
  autostruct uint1 enc_special_lossless // True if the current frame is lossless in a special way (see entry in av1_profile.c)
  autostruct uint1 enc_allowed_motion_modes // Which motion modes will this frame allow at a syntax level?

  autostruct uint1 enc_full_timestamp_flag
  autostruct uint1 enc_seconds_flag
  autostruct uint1 enc_minutes_flag
  autostruct uint1 enc_hours_flag

  // Allow profiles to specify a target minimum and maximum partition size.
  // We will try to respect this as well as we can, but on frame edges we may be forced
  // to split down below enc_min_partition_size.
  //
  // Non-square block sizes are treated specially:
  // * If enc_min_partition_size is non-square, then whenever we reach the corresponding
  //   square partition size (ie, block_size_sqr_up_map[enc_min_partition_size]), we'll
  //   only pick PARTITION_NONE or the partition which takes us to enc_min_partition_size,
  //   except possibly at frame edges
  // * If enc_max_partition_size is non-square, then whenever we reach the corresponding
  //   square partition size, we'll only pick PARTITION_SPLIT or the partition which takes
  //   us to enc_max_partition_size. This is always possible, as PARTITION_SPLIT is always
  //   allowed.
  // * If both are the *same* non-square partition size, then we'll always partition to blocks
  //   of exactly that size, except possibly at frame edges
  autostruct uint_0_13 enc_min_partition_size
  autostruct uint_0_13 enc_max_partition_size

  autostruct int_(-1)_19 enc_target_tx_size // If this is not -1, then we try to make all transforms this size if possible

  autostruct uint_0_100 enc_coef_scale //Used to scale coef when pushing
  autostruct uint1 enc_use_large_coefs // Decide whether we bias toward small or large coefficients
  autostruct uint32 enc_coef_max // Maximum size of coefficients
  autostruct uint32 timebase_low // Time for the frame to be displayed
  autostruct uint32 enc_frames_left // Number of frames after this one
  autostruct uint3 enc_show_existing_frame // Prechoose whether to show a previously-coded frame
  autostruct uint3 enc_index_of_existing_frame // Prechoose which frame to show
  autostruct uint1 enc_frame_type // Prechoose which frame type to show
  autostruct uint1 enc_show_frame // Prechoose whether to show
  autostruct uint8 enc_refresh_frame_flags // Prechoose whether to refresh buffers
  autostruct uint1 enc_disable_recon // Can turn off recon when testing transforms
  autostruct uint1 enc_seen_saturation // Track whether transform saturates
  autostruct uint16 enc_mv_max_row // Greatest allowed value for motion vectors
  autostruct uint16 enc_mv_max_col // Greatest allowed value for motion vectors
  autostruct uint1 enc_extend_newmv_range[2] // Sometimes we need to ignore the above limits
  autostruct uint1 enc_intra_only // Whether this is to be an intra only frame
  autostruct uint22 enc_current_frame_id

  // Special flag only used for choose_levelMax(). If set, the stream is picked
  // to be too large to fit into level 6.3
  autostruct uint1 enc_huge_picture
  autostruct uint1 enc_chooseWidthFirst

  // During encoding, we maintain a "leaky bit bucket" to help target
  // a specific bitrate. The parameters for this model are:
  autostruct uint1 enc_use_bit_bucket // Should we use rate control at all?
  autostruct int64 enc_bit_bucket_capacity
  autostruct int64 enc_bit_bucket_offset_factor // What % of the bucket capacity should we try to "use up"
                                                 // each frame?
  autostruct int64 enc_bit_bucket_leak_factor // What % of the bucket capcity "leaks out" after each frame?
                                               // (note: Setting this to 100 makes all frames independent)

  // Some parameters are selected once per tile, to allow us to model two situations:
  // * Making intra-only (including key) frames larger than inter frames, similarly to real video
  // * Making a single tile in each frame larger (in bits per pixel terms) than the rest
  autostruct int64 enc_bit_bucket_fill_rate // in 1/100 of a bit per luma pixel
  autostruct int64 enc_min_bpp // in 1/100 of a bit per luma pixel
  autostruct int64 enc_max_bpp // in 1/100 of a bit per luma pixel

  // The following is set if we're going to signal the special
  // "max params" level. In this case, we may still constrain ourselves
  // to some defined level, but for some things (in particular decoder model
  // selection) we want to know that we're going to do this up front.
  autostruct uint1 enc_use_max_params_level

  // level bounds
  autostruct int enc_min_level
  autostruct int enc_max_level

  // Selected level for operating point 0
  autostruct uint8 enc_size_group
  autostruct uint8 enc_speed_group
  autostruct uint8 enc_base_level

  // Each profile can set bounds on how large the stream as a whole can be.
  // By default, these are derived from enc_min_level and enc_max_level,
  // but they can be set separately if desired.
  autostruct int32 enc_profile_min_width
  autostruct int32 enc_profile_max_width
  autostruct int32 enc_profile_min_height
  autostruct int32 enc_profile_max_height

  autostruct uint32 enc_frame_min_width_limit
  autostruct uint32 enc_frame_min_height_limit

  // We choose a size within the above bounds to be the "stream size",
  // ie. the value we signal in the sequence header.
  autostruct uint32 enc_stream_width
  autostruct uint32 enc_stream_height

  // Finally, for each frame we calculate the allowable range of sizes
  // based on the stream size, any references, and the absolute minimum
  // frame size allowed (16x16).
  // Note that a frame's size *is* allowed to be smaller than
  // enc_profile_min_width x enc_profile_min_height, since those values
  // only bound the size signalled in the sequence header.
  autostruct uint32 enc_frame_max_width
  autostruct uint32 enc_frame_max_height
  autostruct uint32 enc_frame_min_width
  autostruct uint32 enc_frame_min_height

  // The reference slot indices in increasing order that have a size
  // compatible with the stream (used in enc_pick_frame_size) or have valid film
  // grain parameters (used in enc_choose_film_grain_params)
  autostruct uint3  enc_compatible_refs [8]

  // Selected (upscaled) frame size.
  // Note: If config_scaling() is not used, then the frame size
  // will always be enc_stream_width x enc_stream_height.
  autostruct uint32 enc_frame_width
  autostruct uint32 enc_frame_height

  // Special: Sometimes we want there to be exactly one tile in any given frame
  autostruct uint1 enc_exactly_one_tile

  autostruct int8 enc_copy_frame_from_ref // any value >= 0 means to copy size from that ref idx; -1 means to not copy size

  autostruct uint32 enc_license_resolution_w
  autostruct uint32 enc_license_resolution_h
  autostruct uint32 enc_licence_frame_cnt
  autostruct uint1 enc_license_resolution_specific_bitrate

  autostruct uint32 enc_stream_seed
  autostruct uint1 enc_subsampling_x
  autostruct uint1 enc_subsampling_y
  autostruct uint1 enc_use_profile_switching
  autostruct uint2 enc_profile // Only used when switching profile
  autostruct uint_8_12 enc_bit_depth
  autostruct uint9 enc_color_primaries
  autostruct uint9 enc_transfer_characteristics
  autostruct uint9 enc_matrix_coefficients
  autostruct uint1 enc_force_intra_only_frames
  autostruct uint64 enc_target_bits_per_frame
  autostruct uint32 countPix
  autostruct uint32 totalPix

  autostruct uint1 enc_insert_td // Do we need to insert a temporal delimiter before the next frame?
  autostruct uint1 enc_is_license_resolution // Is this a fixed-resolution stream?
  autostruct uint1 enc_is_stress_stream // Is this a stress stream?
  autostruct uint1 enc_maximize_symbols_per_bit // Special behaviour for arithmetic decode stress streams
  // If this is not -1, it forces the encoder to try to choose this tx type
  autostruct int8  enc_force_tx_type

  // tx_type_allowed[tx_type] = 1 if tx_type is an allowed transform type
  autostruct uint1 tx_type_allowed[TX_TYPES]
  // allowed_tx_types is an increasing list of the num_allowed_tx_types allowed transform types
  autostruct uint_0_15 allowed_tx_types[TX_TYPES]
  autostruct uint_0_15 num_allowed_tx_types

#endif

  // Note: frame_number does not exist in the spec or ref code, but is important for
  // both our encoder and decoder. It is a monotonic counter of how many frames have
  // been encoded/decoded, *including* show-existing frames.
  autostruct uint32 frame_number
  autostruct int32 num_shown_frames
#if ENCODE
  autostruct int32 enc_num_shown_frames_required
#endif // ENCODE

  // When ext-partition is enabled, the superblock size can be chosen
  // per frame - so store it here in various formats:
  autostruct uint8 superblock_size // As a block size
  autostruct uint8 mib_size // In #mi units wide
  autostruct uint8 mib_size_log2
  autostruct uint32 mi_mask // Mask to identify the "within-superblock" part of an mi location
  autostruct uint32 mi_mask2 // Similar, but for 2-superblock pairs.

# if ENCODE
  autostruct int enc_decrease_target_bitrate_percent
  autostruct uint1 enc_force_bug_libaom_2191
  autostruct uint1 enc_large_scale_tile
  autostruct int enc_use_128x128_superblock
  autostruct uint1 enc_large_scale_tile_metadata_sent
  autostruct uint32 renc_anchorFrameIdx[MAX_TILES]
  rewrite enc_anchorFrameIdx[x][y] renc_anchorFrameIdx[y*tile_cols + x]
  autostruct int32 enc_tile_cols
  autostruct int32 enc_tile_rows
  autostruct int32 enc_tile_width_sb
  autostruct int32 enc_tile_height_sb
# endif // ENCODE
  autostruct uint1 use_128x128_superblock
  autostruct uint1 large_scale_tile
  autostruct uint8 num_large_scale_tile_anchor_frames
  autostruct uint16 num_large_scale_tile_tile_list_obus
  autostruct uint1 camera_frame_header_ready
  autostruct int tile_col_sizes[MAX_TILE_COLS]
  autostruct int output_frame_width_in_tiles
  autostruct int output_frame_height_in_tiles
  autostruct int tile_count
  autostruct int rtile_sizes[MAX_TILES]
  rewrite tile_sizes[x][y] rtile_sizes[y*tile_cols + x]
  autostruct uint32 rtile_locations[MAX_TILES]
  rewrite tile_locations[x][y] rtile_locations[y*tile_cols + x]

  autostruct uint8 anchorFrameIdx
  autostruct uint8 decTileRow
  autostruct uint8 decTileCol

#if ENCODE
  // Latest sequence header generated.
  // The sequence header will always be followed by trailing bits,
  // so we include those (but no padding!) in the length.
  // This needs to go in its own buffer because, after each temporal unit,
  // we flush the normal bitstream buffers and reset them. So, to keep
  // this data between frames, we need to copy it to a separate buffer
  autostruct uint1 sequenceHeaderGenerated
  autostruct uint8 sequenceHeaderData[ENC_MAX_SEQ_HDR_LEN]
  autostruct uint32 sequenceHeaderLen

  // Frame header start and length, in bytes.
  // The frame header is generated and byte-aligned, but trailing bits
  // are *not* generated.
  // If trailing bits need to be inserted, frameHeaderRewindBits tracks
  // how many bits we need to "rewind" before placing the trailing bits
  autostruct uint64 frameHeaderStart
  autostruct uint64 frameHeaderStartBytes
  autostruct uint64 frameHeaderBytes
  autostruct uint3 frameHeaderRewindBits

  // Also track the start position of each copy of the frame header,
  // so that it can be updated once the frame is fully generated.
  // Each location is tracked in bits within the level-0 nest buffer.
  autostruct uint8 frameHeaderCopies
  autostruct uint64 frameHeaderCopyStart[ENC_MAX_FRAME_HEADER_COPIES]
#endif

# if ENCODE
  autostruct uint1 enc_update_params
  autostruct uint3 enc_film_grain_params_ref_idx
# endif // ENCODE
  // Per-frame parameters
  autostruct uint1 showable_frame
  autostruct uint1 film_grain_params_present
  autostruct uint1 apply_grain
  autostruct uint16 random_seed
  autostruct uint1 update_grain
  autostruct uint3 film_grain_params_ref_idx

  autostruct uint_0_15 num_y_points
  autostruct uint8 scaling_points_y[14][2]
  autostruct uint1 chroma_scaling_from_luma
  autostruct uint_0_10 num_cb_points
  autostruct uint8 scaling_points_cb[10][2]
  autostruct uint_0_10 num_cr_points
  autostruct uint8 scaling_points_cr[10][2]
  autostruct uint_0_3  grain_scaling_minus_8
  autostruct uint_8_11 scaling_shift
  autostruct uint2 ar_coeff_lag
  // Coefficients for the auto-regressive filter.
  // Note: These are read as uint8 values, then we subtract 128.
  // So it's slightly different to reading an int8 value.
  autostruct int8 ar_coeffs_y[24]
  autostruct int8 ar_coeffs_cb[25]
  autostruct int8 ar_coeffs_cr[25]
  autostruct uint_6_9 ar_coeff_shift
  autostruct uint2 grain_scale_shift
  autostruct uint8 cb_mult
  autostruct uint8 cb_luma_mult
  autostruct uint9 cb_offset
  autostruct uint8 cr_mult
  autostruct uint8 cr_luma_mult
  autostruct uint9 cr_offset
  autostruct uint1 overlap_flag
  autostruct uint1 clip_to_restricted_range

  autostruct uint1 ref_showable_frame[NUM_REF_FRAMES]
  autostruct uint1 ref_film_grain_params_present[NUM_REF_FRAMES]
  autostruct uint1 ref_apply_grain[NUM_REF_FRAMES]
  autostruct uint16 ref_random_seed[NUM_REF_FRAMES]
  autostruct uint_0_15 ref_num_y_points[NUM_REF_FRAMES]
  autostruct uint8 ref_scaling_points_y[NUM_REF_FRAMES][14][2]
  autostruct uint1 ref_chroma_scaling_from_luma[NUM_REF_FRAMES]
  autostruct uint_0_10 ref_num_cb_points[NUM_REF_FRAMES]
  autostruct uint8 ref_scaling_points_cb[NUM_REF_FRAMES][10][2]
  autostruct uint_0_10 ref_num_cr_points[NUM_REF_FRAMES]
  autostruct uint8 ref_scaling_points_cr[NUM_REF_FRAMES][10][2]
  autostruct uint_8_11 ref_scaling_shift[NUM_REF_FRAMES]
  autostruct uint2 ref_ar_coeff_lag[NUM_REF_FRAMES]
  autostruct uint8 ref_ar_coeffs_y[NUM_REF_FRAMES][24]
  autostruct uint8 ref_ar_coeffs_cb[NUM_REF_FRAMES][25]
  autostruct uint8 ref_ar_coeffs_cr[NUM_REF_FRAMES][25]
  autostruct uint_6_9 ref_ar_coeff_shift[NUM_REF_FRAMES]
  autostruct uint2 ref_grain_scale_shift[NUM_REF_FRAMES]
  autostruct uint8 ref_cb_mult[NUM_REF_FRAMES]
  autostruct uint8 ref_cb_luma_mult[NUM_REF_FRAMES]
  autostruct uint9 ref_cb_offset[NUM_REF_FRAMES]
  autostruct uint8 ref_cr_mult[NUM_REF_FRAMES]
  autostruct uint8 ref_cr_luma_mult[NUM_REF_FRAMES]
  autostruct uint9 ref_cr_offset[NUM_REF_FRAMES]
  autostruct uint1 ref_overlap_flag[NUM_REF_FRAMES]
  autostruct uint1 ref_clip_to_restricted_range[NUM_REF_FRAMES]

  // Values used during the synthesis process itself
  autostruct int16 grain_min
  autostruct int16 grain_max
  autostruct uint16 random_register // Current state for the RNG used in film grain synthesis
  autostruct int16 luma_grain[GRAIN_BLOCK_MAX_W*GRAIN_BLOCK_MAX_H]
  autostruct int16 cb_grain[GRAIN_BLOCK_MAX_W*GRAIN_BLOCK_MAX_H]
  autostruct int16 cr_grain[GRAIN_BLOCK_MAX_W*GRAIN_BLOCK_MAX_H]
  autostruct int16 scaling_lut[3][256]
  autostruct int16 noise_stripe[][][][]
  autostruct int16 noise_image[][][]
  autostruct uint1 ValidateMv
  autostruct uint1 ValidationInterIsObmc

  // Decoder model information.
  // The encoder needs to maintain this information per operating point,
  // while the decoder can only check for the current operating point
#if ENCODE
  autostruct int8 enc_decoder_model_mode
#endif
  // Decoder timing information is maintained per DFG ("Decodable Frame Group"),
  // which is all OBUs from the end of one non-show-existing frame to the end
  // of the next non-show-existing frame
  autostruct uint32 current_dfg[MAX_NUM_OPERATING_POINTS]
  autostruct uint32 dfg_bits[MAX_NUM_OPERATING_POINTS][ENC_MAX_FRAMES]
  // Meanwhile, display timing information is maintained per shown frame
  // (ie, per frame with show_frame == 1 or show_existing_frame == 1)
  autostruct uint32 current_show_frame_idx[MAX_NUM_OPERATING_POINTS]

  autostruct int8 decoder_model_mode // DECODER_MODEL_* constants

  autostruct uint1 startOfFile

#if DECODE
  // Used for statistics collection - when we decode a uleb128 value,
  // the number of *bytes* read is stored into this variable
  autostruct uint32 uleb_length
#endif

#if DECODE && COLLECT_STATS
  // Some statistics about the stream being decoded.
  // These are "cumulative" counters - they are cleared only at the beginning of the stream,
  // and count up from there.
  // This allows derived statistics (eg, "average #bits per symbol") to be calculated over
  // any desired interval (tile, frame, or stream) by storing "before" and "after" values.
  // These values are intended to be a minimal set providing all the statistics you might want;
  // you may have to do some extra processing to get what you want.
  //
  // Important: The counters are updated at different rates, so you need to be careful to
  //            get a consistent view of the world:
  //            * Tile-level stats (total_tiles and total_tile_bits) are updated at the end of each tile,
  //            * Frame-level stats (total_frames, total_bits, total_decoded_px, total_upscaled_px)
  //              are updated at the end of each frame
  //            * Everything else is updated immediately after decoding the relevant symbols /
  //              processing the relevant blocks / etc.
  autostruct uint32 total_tiles
  autostruct uint32 total_frames[5] // How many frames of each type have we seen?
                                    // (special value 4 means 'show-existing')
  autostruct uint32 total_frame_bits[5] // How many bits have been spent on each frame type?
                                        // This counts the full frame size, including headers
  autostruct uint32 total_symbols
  autostruct uint32 total_tile_bits // How many bits have we spent on symbols within tiles?

  // How many bits have we decoded in total? This counts everything
  // which should be decoded within the current operating point -
  // including TU/frame sizes, padding and metadata OBUs, etc. but
  // *not* including skipped OBUs. To include those too, use
  // current_bitposition()
  autostruct uint32 total_bits

  // The maximum number of bits decoded in a frame
  autostruct uint32 max_frame_bits

  autostruct uint64 min_max_display_rate_x100
  autostruct uint64 min_max_decode_rate_x100
  autostruct uint64 min_comp_ratio_x100
  autostruct uint32 total_1d_txfms[NUM_1D_TXFMS][5] // How many 1D transforms we have done, for each type and size
  autostruct uint32 total_blocks[2][BLOCK_SIZES_ALL] // How many total blocks we have decoded, for each size
                                                     // and {intra, inter}
  // How many pixels have we generated with each of {intra pred, block inter pred, warp pred}?
  // Note: Compound, OBMC, and interintra predictions do multiple predictions, and so count multiple times
  // in these values.
  // Note also: For consistency with the later total_*_px values, we only count luma pixels
  autostruct uint32 total_intra_px
  autostruct uint32 total_intrabc_px
  autostruct uint32 total_inter_px
  autostruct uint32 total_inter_preds // Number of distinct calls to convolve_c()
  autostruct uint32 total_warp_preds
  autostruct uint32 total_txfm_blocks[TX_SIZES_ALL] // How many total transform blocks we have done, for each size
  autostruct uint32 total_coeffs[7] // How many coefficients we have decoded, for each of the "size categories"
                                    // {0, 1-2, 3-5, 6-8, 9-11, 12-14, 15+}
                                    // These correspond to the various interesting cases in the coefficient
                                    // coding.
  autostruct uint32 total_deblocks[4] // Number of deblocked edges, for each filter length (4, 6, 8, 13)
                                      // Note: We currently consider each 4-pixel edge segment separately, even
                                      // if it's a boundary between two large transform blocks.
  autostruct uint32 total_cdefs // Number of CDEF units (8x8 luma px) which we've applied CDEF to
  // Various pixel counts.
  // Note: Currently these *only* measure luma pixels. They could fairly easily be modified
  // to count both luma and chroma pixels if desired.
  autostruct uint32 total_decoded_px // Sum of crop_width * crop_height for all decoded frames so far
  autostruct uint32 total_upscaled_px // Sum of upscaled_width * upscaled_height for all decoded frames so far
  autostruct uint32 total_displayed_px // Sum of upscaled_width * upscaled_height for all shown/show-existing frames so far

  // The minimum superres denominator over all the frames we've seen so far. A
  // frame without superres gets a denominator of SUPERRES_NUM
  autostruct uint32 min_superres_denom

  autostruct uint64 total_wiener_px // Total number of luma pixels we've applied the Wiener filter to
  autostruct uint64 total_selfguided_px // Total number of luma pixels we've applied the self-guided filter to

  // There's one value we need which can't simply be accumulated across the stream.
  // That value is the maximum ratio of (max tile size in frame : min tile size in frame)
  // across all frames in the stream.
  // This has to be calculated as we go, using the following variables
  autostruct int64 largest_tile_size_in_frame
  autostruct int64 smallest_tile_size_in_frame
  // Note: Since the Argon Streams language does not support floats,
  // we store the largest ratio seen so far as a fraction
  autostruct int64 max_tile_ratio_num
  autostruct int64 max_tile_ratio_denom
#endif // DECODE && COLLECT_STATS

#if ENCODE
  // This is used to pass information back from
  // builtin_get_cfg_syntax. The weird 16-length array is so that we
  // can pass in up to 16 local variables. The result appears at index
  // 0.
  autostruct int64 cfg_syntax[16]
#endif

  // used to check validity of sequence header and scalability changes
  autostruct uint1 sequenceHeaderChanged
  autostruct int64 prevSequenceHeaderSize
  autostruct uint8 prevSequenceHeader[MAX_SEQUENCE_HEADER_SIZE]
  autostruct uint8 curSequenceHeader[MAX_SEQUENCE_HEADER_SIZE]
  autostruct uint1 scalabilityStructureChanged
  autostruct int64 prevScalabilityStructureSize
  autostruct uint8 prevScalabilityStructure[MAX_SCALABILITY_STRUCTURE_SIZE]
  autostruct uint8 curScalabilityStructure[MAX_SCALABILITY_STRUCTURE_SIZE]

#if ENCODE
  // Ugly hack to allow the config language to compute something per
  // stream and snaffle it away somewhere
  autostruct int64 per_stream_0
  autostruct int64 per_stream_1
  autostruct int64 per_stream_2
  autostruct int64 per_stream_3
  autostruct int64 per_stream_4
  autostruct int64 per_stream_5
  autostruct int64 per_stream_6
  autostruct int64 per_stream_7

  // And the same per-frame
  autostruct int64 per_frame_0
  autostruct int64 per_frame_1
  autostruct int64 per_frame_2
  autostruct int64 per_frame_3
  autostruct int64 per_frame_4
  autostruct int64 per_frame_5
  autostruct int64 per_frame_6
  autostruct int64 per_frame_7
#endif
}

// CABAC probabilities
frame_cdfs() {

  autostruct uint16 tile_kf_y_cdf[KF_MODE_CONTEXTS][KF_MODE_CONTEXTS][INTRA_MODES+1]
  autostruct uint16 frame_y_mode_cdf[BLOCK_SIZE_GROUPS][INTRA_MODES+1]
  autostruct uint16 tile_y_mode_cdf[BLOCK_SIZE_GROUPS][INTRA_MODES+1]
  autostruct uint32 adapted_y_mode_cdf[BLOCK_SIZE_GROUPS][INTRA_MODES]
  autostruct uint16 frame_uv_mode_cdf[2][INTRA_MODES][UV_INTRA_MODES+1]
  autostruct uint16 tile_uv_mode_cdf[2][INTRA_MODES][UV_INTRA_MODES+1]
  autostruct uint32 adapted_uv_mode_cdf[2][INTRA_MODES][UV_INTRA_MODES]
  autostruct uint16 frame_switchable_interp_cdf[SWITCHABLE_FILTER_CONTEXTS][SWITCHABLE_FILTERS + 1]
  autostruct uint16 tile_switchable_interp_cdf[SWITCHABLE_FILTER_CONTEXTS][SWITCHABLE_FILTERS + 1]
  autostruct uint32 adapted_switchable_interp_cdf[SWITCHABLE_FILTER_CONTEXTS][SWITCHABLE_FILTERS]
  autostruct uint16 frame_partition_cdf[PARTITION_CONTEXTS][EXT_PARTITION_TYPES + 1]
  autostruct uint16 tile_partition_cdf[PARTITION_CONTEXTS][EXT_PARTITION_TYPES + 1]
  autostruct uint32 adapted_partition_cdf[PARTITION_CONTEXTS][EXT_PARTITION_TYPES]
  autostruct uint16 frame_joint_cdf[NMV_CONTEXTS][MV_JOINTS+1]
  autostruct uint16 frame_mvcomp_classes_cdf[NMV_CONTEXTS][2][MV_CLASSES+1]
  autostruct uint16 frame_mvcomp_class0_fp_cdf[NMV_CONTEXTS][2][CLASS0_SIZE][MV_FP_SIZE+1]
  autostruct uint16 frame_mvcomp_fp_cdf[NMV_CONTEXTS][2][MV_FP_SIZE+1]
  autostruct uint16 tile_joint_cdf[NMV_CONTEXTS][MV_JOINTS+1]
  autostruct uint16 tile_mvcomp_classes_cdf[NMV_CONTEXTS][2][MV_CLASSES+1]
  autostruct uint16 tile_mvcomp_class0_fp_cdf[NMV_CONTEXTS][2][CLASS0_SIZE][MV_FP_SIZE+1]
  autostruct uint16 tile_mvcomp_fp_cdf[NMV_CONTEXTS][2][MV_FP_SIZE+1]
  autostruct uint32 adapted_joint_cdf[NMV_CONTEXTS][MV_JOINTS]
  autostruct uint32 adapted_mvcomp_classes_cdf[NMV_CONTEXTS][2][MV_CLASSES]
  autostruct uint32 adapted_mvcomp_class0_fp_cdf[NMV_CONTEXTS][2][CLASS0_SIZE][MV_FP_SIZE]
  autostruct uint32 adapted_mvcomp_fp_cdf[NMV_CONTEXTS][2][MV_FP_SIZE]

  autostruct uint16 frame_delta_q_cdf[DELTA_Q_SMALL+2]
  autostruct uint16 tile_delta_q_cdf[DELTA_Q_SMALL+2]
  autostruct uint32 adapted_delta_q_cdf[DELTA_Q_SMALL+1]
  autostruct uint16 frame_delta_lf_cdf[DELTA_LF_SMALL+2]
  autostruct uint16 tile_delta_lf_cdf[DELTA_LF_SMALL+2]
  autostruct uint32 adapted_delta_lf_cdf[DELTA_LF_SMALL+1]
  autostruct uint16 frame_delta_lf_multi_cdf[FRAME_LF_COUNT][DELTA_LF_SMALL+2]
  autostruct uint16 tile_delta_lf_multi_cdf[FRAME_LF_COUNT][DELTA_LF_SMALL+2]
  autostruct uint32 adapted_delta_lf_multi_cdf[FRAME_LF_COUNT][DELTA_LF_SMALL+1]
  autostruct uint16 frame_filter_intra_mode_cdf[FILTER_INTRA_MODES+1]
  autostruct uint16 frame_filter_intra_cdfs[BLOCK_SIZES_ALL][2+1]
  autostruct uint16 tile_filter_intra_mode_cdf[FILTER_INTRA_MODES+1]
  autostruct uint16 tile_filter_intra_cdfs[BLOCK_SIZES_ALL][2+1]
  autostruct uint32 adapted_filter_intra_mode_cdf[FILTER_INTRA_MODES]
  autostruct uint32 adapted_filter_intra_cdfs[BLOCK_SIZES_ALL][2]

  autostruct uint16 frame_angle_delta_cdf[DIRECTIONAL_MODES][(2 * MAX_ANGLE_DELTA + 1)+1]
  autostruct uint16 tile_angle_delta_cdf[DIRECTIONAL_MODES][(2 * MAX_ANGLE_DELTA + 1)+1]
  autostruct uint32 adapted_angle_delta_cdf[DIRECTIONAL_MODES][(2 * MAX_ANGLE_DELTA + 1)]

  autostruct uint16 frame_inter_ext_tx_cdf[EXT_TX_SETS_INTER][EXT_TX_SIZES][TX_TYPES + 1]
  autostruct uint16 frame_intra_ext_tx_cdf[EXT_TX_SETS_INTRA][EXT_TX_SIZES][INTRA_MODES][TX_TYPES + 1]
  autostruct uint16 tile_inter_ext_tx_cdf[EXT_TX_SETS_INTER][EXT_TX_SIZES][TX_TYPES + 1]
  autostruct uint16 tile_intra_ext_tx_cdf[EXT_TX_SETS_INTRA][EXT_TX_SIZES][INTRA_MODES][TX_TYPES + 1]
  autostruct uint32 adapted_inter_ext_tx_cdf[EXT_TX_SETS_INTER][EXT_TX_SIZES][TX_TYPES]
  autostruct uint32 adapted_intra_ext_tx_cdf[EXT_TX_SETS_INTRA][EXT_TX_SIZES][INTRA_MODES][TX_TYPES]
  autostruct uint16 frame_seg_tree_cdf[MAX_SEGMENTS+1]
  autostruct uint16 tile_seg_tree_cdf[MAX_SEGMENTS+1]
  autostruct uint32 adapted_seg_tree_cdf[MAX_SEGMENTS]

  // note that indices < TX_8X8 are never used, and smaller sizes have fewer probabilities, but it is more convenient to have a single array
  autostruct uint16 frame_tx_cdfs[MAX_TX_CATS][TX_SIZE_CONTEXTS][MAX_TX_DEPTH+2]
  autostruct uint16 tile_tx_cdfs[MAX_TX_CATS][TX_SIZE_CONTEXTS][MAX_TX_DEPTH+2]
  autostruct uint32 adapted_tx_cdfs[MAX_TX_CATS][TX_SIZE_CONTEXTS][MAX_TX_DEPTH+1]
  autostruct uint16 frame_spatial_pred_seg_cdf[SPATIAL_PREDICTION_PROBS][MAX_SEGMENTS+1]
  autostruct uint16 tile_spatial_pred_seg_cdf[SPATIAL_PREDICTION_PROBS][MAX_SEGMENTS+1]
  autostruct uint32 adapted_spatial_pred_seg_cdf[SPATIAL_PREDICTION_PROBS][MAX_SEGMENTS]

  autostruct uint16 frame_obmc_cdf[BLOCK_SIZES_ALL][2+1]
  autostruct uint16 tile_obmc_cdf[BLOCK_SIZES_ALL][2+1]
  autostruct uint32 adapted_obmc_cdf[BLOCK_SIZES_ALL][2]
  autostruct uint16 frame_motion_mode_cdf[BLOCK_SIZES_ALL][MOTION_MODES+1]
  autostruct uint16 tile_motion_mode_cdf[BLOCK_SIZES_ALL][MOTION_MODES+1]
  autostruct uint32 adapted_motion_mode_cdf[BLOCK_SIZES_ALL][MOTION_MODES]

  autostruct uint16 frame_palette_y_size_cdf[PALETTE_BSIZE_CTXS][PALETTE_SIZES+1]
  autostruct uint16 frame_palette_uv_size_cdf[PALETTE_BSIZE_CTXS][PALETTE_SIZES+1]
  autostruct uint16 frame_palette_y_color_cdf[PALETTE_MAX_SIZE-1][PALETTE_COLOR_CONTEXTS][PALETTE_COLORS+1]
  autostruct uint16 frame_palette_uv_color_cdf[PALETTE_MAX_SIZE-1][PALETTE_COLOR_CONTEXTS][PALETTE_COLORS+1]
  autostruct uint16 tile_palette_y_size_cdf[PALETTE_BSIZE_CTXS][PALETTE_SIZES+1]
  autostruct uint16 tile_palette_uv_size_cdf[PALETTE_BSIZE_CTXS][PALETTE_SIZES+1]
  autostruct uint16 tile_palette_y_color_cdf[PALETTE_MAX_SIZE-1][PALETTE_COLOR_CONTEXTS][PALETTE_COLORS+1]
  autostruct uint16 tile_palette_uv_color_cdf[PALETTE_MAX_SIZE-1][PALETTE_COLOR_CONTEXTS][PALETTE_COLORS+1]
  autostruct uint32 adapted_palette_y_size_cdf[PALETTE_BSIZE_CTXS][PALETTE_SIZES]
  autostruct uint32 adapted_palette_uv_size_cdf[PALETTE_BSIZE_CTXS][PALETTE_SIZES]
  autostruct uint32 adapted_palette_y_color_cdf[PALETTE_MAX_SIZE-1][PALETTE_COLOR_CONTEXTS][PALETTE_COLORS]
  autostruct uint32 adapted_palette_uv_color_cdf[PALETTE_MAX_SIZE-1][PALETTE_COLOR_CONTEXTS][PALETTE_COLORS]

  // Intrabc can only be used for keyframes and intra-only frames.
  // Thus we don't need to store per-frame or summed CDFs
  autostruct uint16 tile_intrabc_cdf[2+1]

  autostruct uint16 frame_inter_compound_mode_cdf[INTER_MODE_CONTEXTS][INTER_COMPOUND_MODES + 1]
  autostruct uint16 tile_inter_compound_mode_cdf[INTER_MODE_CONTEXTS][INTER_COMPOUND_MODES + 1]
  autostruct uint32 adapted_inter_compound_mode_cdf[INTER_MODE_CONTEXTS][INTER_COMPOUND_MODES]

  autostruct uint16 frame_compound_idx_cdf[COMP_INDEX_CONTEXTS][2+1]
  autostruct uint16 tile_compound_idx_cdf[COMP_INDEX_CONTEXTS][2+1]
  autostruct uint32 adapted_compound_idx_cdf[COMP_INDEX_CONTEXTS][2]

  autostruct uint16 frame_compound_group_idx_cdf[COMP_GROUP_IDX_CONTEXTS][2+1]
  autostruct uint16 tile_compound_group_idx_cdf[COMP_GROUP_IDX_CONTEXTS][2+1]
  autostruct uint32 adapted_compound_group_idx_cdf[COMP_GROUP_IDX_CONTEXTS][2]

  autostruct uint16 frame_compound_mask_type_cdf[BLOCK_SIZES_ALL][2+1]
  autostruct uint16 tile_compound_mask_type_cdf[BLOCK_SIZES_ALL][2+1]
  autostruct uint32 adapted_compound_mask_type_cdf[BLOCK_SIZES_ALL][2]
  autostruct uint16 frame_interintra_mode_cdf[BLOCK_SIZE_GROUPS][INTERINTRA_MODES + 1]
  autostruct uint16 tile_interintra_mode_cdf[BLOCK_SIZE_GROUPS][INTERINTRA_MODES + 1]
  autostruct uint32 adapted_interintra_mode_cdf[BLOCK_SIZE_GROUPS][INTERINTRA_MODES]
  autostruct uint16 frame_interintra_cdf[BLOCK_SIZE_GROUPS][2+1]
  autostruct uint16 tile_interintra_cdf[BLOCK_SIZE_GROUPS][2+1]
  autostruct uint32 adapted_interintra_cdf[BLOCK_SIZE_GROUPS][2]
  autostruct uint16 frame_wedge_idx_cdf[BLOCK_SIZES_ALL][16 + 1]
  autostruct uint16 tile_wedge_idx_cdf[BLOCK_SIZES_ALL][16 + 1]
  autostruct uint32 adapted_wedge_idx_cdf[BLOCK_SIZES_ALL][16]
  autostruct uint16 frame_wedge_interintra_cdf[BLOCK_SIZES_ALL][2+1]
  autostruct uint16 tile_wedge_interintra_cdf[BLOCK_SIZES_ALL][2+1]
  autostruct uint32 adapted_wedge_interintra_cdf[BLOCK_SIZES_ALL][2]

  autostruct uint16 frame_newmv_cdf[NEWMV_MODE_CONTEXTS][2+1]
  autostruct uint16 tile_newmv_cdf[NEWMV_MODE_CONTEXTS][2+1]
  autostruct uint32 adapted_newmv_cdf[NEWMV_MODE_CONTEXTS][2]

  autostruct uint16 frame_globalmv_cdf[GLOBALMV_MODE_CONTEXTS][2+1]
  autostruct uint16 tile_globalmv_cdf[GLOBALMV_MODE_CONTEXTS][2+1]
  autostruct uint32 adapted_globalmv_cdf[GLOBALMV_MODE_CONTEXTS][2]

  autostruct uint16 frame_refmv_cdf[REFMV_MODE_CONTEXTS][2+1]
  autostruct uint16 tile_refmv_cdf[REFMV_MODE_CONTEXTS][2+1]
  autostruct uint32 adapted_refmv_cdf[REFMV_MODE_CONTEXTS][2]

  autostruct uint16 frame_drl_cdf[DRL_MODE_CONTEXTS][2+1]
  autostruct uint16 tile_drl_cdf[DRL_MODE_CONTEXTS][2+1]
  autostruct uint32 adapted_drl_cdf[DRL_MODE_CONTEXTS][2]

  autostruct uint16 frame_intra_inter_cdf[INTRA_INTER_CONTEXTS][2+1]
  autostruct uint16 tile_intra_inter_cdf[INTRA_INTER_CONTEXTS][2+1]
  autostruct uint32 adapted_intra_inter_cdf[INTRA_INTER_CONTEXTS][2]

  autostruct uint16 frame_comp_inter_cdf[COMP_INTER_CONTEXTS][2+1]
  autostruct uint16 tile_comp_inter_cdf[COMP_INTER_CONTEXTS][2+1]
  autostruct uint32 adapted_comp_inter_cdf[COMP_INTER_CONTEXTS][2]

  autostruct uint16 frame_mbskip_cdf[MBSKIP_CONTEXTS][2+1]
  autostruct uint16 tile_mbskip_cdf[MBSKIP_CONTEXTS][2+1]
  autostruct uint32 adapted_mbskip_cdf[MBSKIP_CONTEXTS][2]

  autostruct uint16 frame_skip_mode_cdf[SKIP_MODE_CONTEXTS][2+1]
  autostruct uint16 tile_skip_mode_cdf[SKIP_MODE_CONTEXTS][2+1]
  autostruct uint32 adapted_skip_mode_cdf[SKIP_MODE_CONTEXTS][2]

  autostruct uint16 frame_comp_ref_cdf[REF_CONTEXTS][FWD_REFS-1][2+1]
  autostruct uint16 tile_comp_ref_cdf[REF_CONTEXTS][FWD_REFS-1][2+1]
  autostruct uint32 adapted_comp_ref_cdf[REF_CONTEXTS][FWD_REFS-1][2]

  autostruct uint16 frame_comp_bwdref_cdf[REF_CONTEXTS][BWD_REFS-1][2+1]
  autostruct uint16 tile_comp_bwdref_cdf[REF_CONTEXTS][BWD_REFS-1][2+1]
  autostruct uint32 adapted_comp_bwdref_cdf[REF_CONTEXTS][BWD_REFS-1][2]

  autostruct uint16 frame_single_ref_cdf[REF_CONTEXTS][SINGLE_REFS-1][2+1]
  autostruct uint16 tile_single_ref_cdf[REF_CONTEXTS][SINGLE_REFS-1][2+1]
  autostruct uint32 adapted_single_ref_cdf[REF_CONTEXTS][SINGLE_REFS-1][2]

  autostruct uint16 frame_comp_ref_type_cdf[COMP_REF_TYPE_CONTEXTS][2+1]
  autostruct uint16 tile_comp_ref_type_cdf[COMP_REF_TYPE_CONTEXTS][2+1]
  autostruct uint32 adapted_comp_ref_type_cdf[COMP_REF_TYPE_CONTEXTS][2]

  autostruct uint16 frame_uni_comp_ref_cdf[UNI_COMP_REF_CONTEXTS][SIGNALLED_UNIDIR_COMP_REFS - 1][2+1]
  autostruct uint16 tile_uni_comp_ref_cdf[UNI_COMP_REF_CONTEXTS][SIGNALLED_UNIDIR_COMP_REFS - 1][2+1]
  autostruct uint32 adapted_uni_comp_ref_cdf[UNI_COMP_REF_CONTEXTS][SIGNALLED_UNIDIR_COMP_REFS - 1][2]

  autostruct uint16 frame_segment_pred_cdf[PREDICTION_PROBS][2+1]
  autostruct uint16 tile_segment_pred_cdf[PREDICTION_PROBS][2+1]
  autostruct uint32 adapted_segment_pred_cdf[PREDICTION_PROBS][2]

  autostruct uint16 frame_sign_cdf[NMV_CONTEXTS][2][2+1]
  autostruct uint16 tile_sign_cdf[NMV_CONTEXTS][2][2+1]
  autostruct uint32 adapted_sign_cdf[NMV_CONTEXTS][2][2]

  autostruct uint16 frame_class0_hp_cdf[NMV_CONTEXTS][2][2+1]
  autostruct uint16 tile_class0_hp_cdf[NMV_CONTEXTS][2][2+1]
  autostruct uint32 adapted_class0_hp_cdf[NMV_CONTEXTS][2][2]

  autostruct uint16 frame_hp_cdf[NMV_CONTEXTS][2][2+1]
  autostruct uint16 tile_hp_cdf[NMV_CONTEXTS][2][2+1]
  autostruct uint32 adapted_hp_cdf[NMV_CONTEXTS][2][2]

  autostruct uint16 frame_class0_cdf[NMV_CONTEXTS][2][2+1]
  autostruct uint16 tile_class0_cdf[NMV_CONTEXTS][2][2+1]
  autostruct uint32 adapted_class0_cdf[NMV_CONTEXTS][2][2]

  autostruct uint16 frame_mvcomp_bits_cdf[NMV_CONTEXTS][MV_OFFSET_BITS][2][2+1]
  autostruct uint16 tile_mvcomp_bits_cdf[NMV_CONTEXTS][MV_OFFSET_BITS][2][2+1]
  autostruct uint32 adapted_mvcomp_bits_cdf[NMV_CONTEXTS][MV_OFFSET_BITS][2][2]

  autostruct uint16 frame_txfm_partition_cdf[TXFM_PARTITION_CONTEXTS][2+1]
  autostruct uint16 tile_txfm_partition_cdf[TXFM_PARTITION_CONTEXTS][2+1]
  autostruct uint32 adapted_txfm_partition_cdf[TXFM_PARTITION_CONTEXTS][2]

  autostruct uint16 frame_palette_y_mode_cdf[PALETTE_BSIZE_CTXS][PALETTE_Y_MODE_CONTEXTS][2+1]
  autostruct uint16 tile_palette_y_mode_cdf[PALETTE_BSIZE_CTXS][PALETTE_Y_MODE_CONTEXTS][2+1]
  autostruct uint32 adapted_palette_y_mode_cdf[PALETTE_BSIZE_CTXS][PALETTE_Y_MODE_CONTEXTS][2]

  autostruct uint16 frame_palette_uv_mode_cdf[PALETTE_UV_MODE_CONTEXTS][2+1]
  autostruct uint16 tile_palette_uv_mode_cdf[PALETTE_UV_MODE_CONTEXTS][2+1]
  autostruct uint32 adapted_palette_uv_mode_cdf[PALETTE_UV_MODE_CONTEXTS][2]

  autostruct uint16 frame_txb_skip_cdfs[TX_SIZES][TXB_SKIP_CONTEXTS][2+1]
  autostruct uint16 tile_txb_skip_cdfs[TX_SIZES][TXB_SKIP_CONTEXTS][2+1]
  autostruct uint32 adapted_txb_skip_cdfs[TX_SIZES][TXB_SKIP_CONTEXTS][2]
  autostruct uint16 frame_eob_flag_16_cdfs[PLANE_TYPES][2][5+1]
  autostruct uint16 tile_eob_flag_16_cdfs[PLANE_TYPES][2][5+1]
  autostruct uint32 adapted_eob_flag_16_cdfs[PLANE_TYPES][2][5]
  autostruct uint16 frame_eob_flag_32_cdfs[PLANE_TYPES][2][6+1]
  autostruct uint16 tile_eob_flag_32_cdfs[PLANE_TYPES][2][6+1]
  autostruct uint32 adapted_eob_flag_32_cdfs[PLANE_TYPES][2][6]
  autostruct uint16 frame_eob_flag_64_cdfs[PLANE_TYPES][2][7+1]
  autostruct uint16 tile_eob_flag_64_cdfs[PLANE_TYPES][2][7+1]
  autostruct uint32 adapted_eob_flag_64_cdfs[PLANE_TYPES][2][7]
  autostruct uint16 frame_eob_flag_128_cdfs[PLANE_TYPES][2][8+1]
  autostruct uint16 tile_eob_flag_128_cdfs[PLANE_TYPES][2][8+1]
  autostruct uint32 adapted_eob_flag_128_cdfs[PLANE_TYPES][2][8]
  autostruct uint16 frame_eob_flag_256_cdfs[PLANE_TYPES][2][9+1]
  autostruct uint16 tile_eob_flag_256_cdfs[PLANE_TYPES][2][9+1]
  autostruct uint32 adapted_eob_flag_256_cdfs[PLANE_TYPES][2][9]
  autostruct uint16 frame_eob_flag_512_cdfs[PLANE_TYPES][2][10+1]
  autostruct uint16 tile_eob_flag_512_cdfs[PLANE_TYPES][2][10+1]
  autostruct uint32 adapted_eob_flag_512_cdfs[PLANE_TYPES][2][10]
  autostruct uint16 frame_eob_flag_1024_cdfs[PLANE_TYPES][2][11+1]
  autostruct uint16 tile_eob_flag_1024_cdfs[PLANE_TYPES][2][11+1]
  autostruct uint32 adapted_eob_flag_1024_cdfs[PLANE_TYPES][2][11]
  autostruct uint16 frame_eob_extra_cdfs[TX_SIZES][PLANE_TYPES][EOB_COEF_CONTEXTS][2+1]
  autostruct uint16 tile_eob_extra_cdfs[TX_SIZES][PLANE_TYPES][EOB_COEF_CONTEXTS][2+1]
  autostruct uint32 adapted_eob_extra_cdfs[TX_SIZES][PLANE_TYPES][EOB_COEF_CONTEXTS][2]
  autostruct uint16 frame_dc_sign_cdfs[PLANE_TYPES][DC_SIGN_CONTEXTS][2+1]
  autostruct uint16 tile_dc_sign_cdfs[PLANE_TYPES][DC_SIGN_CONTEXTS][2+1]
  autostruct uint32 adapted_dc_sign_cdfs[PLANE_TYPES][DC_SIGN_CONTEXTS][2]
  autostruct uint16 frame_coeff_base_eob_cdfs[TX_SIZES][PLANE_TYPES][SIG_COEF_CONTEXTS_EOB][3+1]
  autostruct uint16 tile_coeff_base_eob_cdfs[TX_SIZES][PLANE_TYPES][SIG_COEF_CONTEXTS_EOB][3+1]
  autostruct uint32 adapted_coeff_base_eob_cdfs[TX_SIZES][PLANE_TYPES][SIG_COEF_CONTEXTS_EOB][3]
  autostruct uint16 frame_coeff_base_cdfs[TX_SIZES][PLANE_TYPES][SIG_COEF_CONTEXTS][4+1]
  autostruct uint16 tile_coeff_base_cdfs[TX_SIZES][PLANE_TYPES][SIG_COEF_CONTEXTS][4+1]
  autostruct uint32 adapted_coeff_base_cdfs[TX_SIZES][PLANE_TYPES][SIG_COEF_CONTEXTS][4]
  autostruct uint16 frame_coeff_br_cdfs[TX_SIZES][PLANE_TYPES][LEVEL_CONTEXTS][BR_CDF_SIZE+1]
  autostruct uint16 tile_coeff_br_cdfs[TX_SIZES][PLANE_TYPES][LEVEL_CONTEXTS][BR_CDF_SIZE+1]
  autostruct uint32 adapted_coeff_br_cdfs[TX_SIZES][PLANE_TYPES][LEVEL_CONTEXTS][BR_CDF_SIZE]

  autostruct uint16 frame_cfl_sign_cdf[CFL_JOINT_SIGNS+1]
  autostruct uint16 tile_cfl_sign_cdf[CFL_JOINT_SIGNS+1]
  autostruct uint32 adapted_cfl_sign_cdf[CFL_JOINT_SIGNS]

  autostruct uint16 frame_cfl_alpha_cdf[CFL_ALPHA_CONTEXTS][CFL_ALPHABET_SIZE+1]
  autostruct uint16 tile_cfl_alpha_cdf[CFL_ALPHA_CONTEXTS][CFL_ALPHABET_SIZE+1]
  autostruct uint32 adapted_cfl_alpha_cdf[CFL_ALPHA_CONTEXTS][CFL_ALPHABET_SIZE]

  autostruct uint16 frame_switchable_restore_cdf[RESTORE_SWITCHABLE_TYPES+1]
  autostruct uint16 tile_switchable_restore_cdf[RESTORE_SWITCHABLE_TYPES+1]
  autostruct uint32 adapted_switchable_restore_cdf[RESTORE_SWITCHABLE_TYPES]

  autostruct uint16 frame_wiener_cdf[2+1]
  autostruct uint16 tile_wiener_cdf[2+1]
  autostruct uint32 adapted_wiener_cdf[2]

  autostruct uint16 frame_sgrproj_cdf[2+1]
  autostruct uint16 tile_sgrproj_cdf[2+1]
  autostruct uint32 adapted_sgrproj_cdf[2]
}

ivf_frame_header() {
  autostruct uint32 frame_sz
}

// Data for the current frame
frame_data() {
  autostruct uint1 lossless_array[MAX_SEGMENTS]

  autostruct uint64 pos_frame_start
  autostruct uint1  show_existing_frame
  autostruct uint3  index_of_existing_frame
  autostruct uint22 display_frame_id

#if ENCODE
  // For bit-stream generator only
  autostruct uint1 FrameIsIntra
#endif // ENCODE

  autostruct uint_0_7 segment_id
  autostruct uint_0_7 rsegment_ids[]
  rewrite segment_ids[x][y] rsegment_ids[y*mi_cols+x]
  autostruct uint1 seg_update_map
  autostruct uint1 seg_update_data
  autostruct uint1 seg_temporal_update
  autostruct uint1 show_frame
  autostruct uint1 skip_coeff
  autostruct uint_0_4 tx_mode
  autostruct uint1 reduced_tx_set_used
  autostruct uint_0_13 y_mode // Last chosen intra mode, needed by intra_mode_uv cabac
  autostruct uint_0_13 uv_mode
  autostruct int_(-1)_6 ref_frame[2]
  autostruct uint8 ref_frame_type
  autostruct uint1 skip_mode_allowed // Frame-level flag: Can we use ext-skip mode?
  autostruct uint1 skip_mode // Block-level flag: Should we use ext-skip mode for this block?
  autostruct uint_0_7 skip_ref[2] // Ref frame pair used for ext-skip blocks

  autostruct uint1 extra_mode_bit

  autostruct int16 decode_data
  autostruct uint1 FeatureEnabled [MAX_SEGMENTS][SEG_LVL_MAX]

  // AV1 effectively has four separate notions of the "frame size":
  // * The size which is actually decoded.
  //   This is determined by mi_rows and mi_cols, and is always a multiple of 8 luma pixels.
  //   (this means mi_row and mi_col are always even)

  // * The size to which the frame is cropped after applying deblocking + cdef.
  autostruct uint_1_65536 crop_width
  autostruct uint_1_65536 crop_height

  // * The size to which the frame is upscaled before applying loop-restoration and before
  //   storing into the reference buffer.
  //
  //   If not using HORZONLY_FRAME_SUPERRES, this will always equal the crop size,
  //   but we keep the separate variables to help simplify the code.
  autostruct uint_1_65536 upscaled_width
  autostruct uint_1_65536 upscaled_height

  autostruct uint16 frame_width_minus1
  autostruct uint16 frame_height_minus1

  autostruct uint1  has_scaling
  autostruct uint16 display_width_minus1
  autostruct uint16 display_height_minus1

  // * The intended display size. This doesn't affect the decoding process,
  //   it's just some metadata which is passed up to the application.
  autostruct uint_1_65536 display_width
  autostruct uint_1_65536 display_height

  autostruct uint1 frame_size_override_flag
  autostruct uint1 mode_ref_delta_enabled
  autostruct uint1 mode_ref_delta_update
  autostruct uint1 update_ref_delta
  autostruct uint1 update_mode_delta

  autostruct uint_8_65536 stride
  autostruct uint_1_8192 mi_rows
  autostruct uint_1_8192 mi_cols
  autostruct uint_0_7 min_log2_tile_cols // with max width tiles of 64, can have 8192/64 =128 =2**7
  autostruct uint_0_11 max_log2_tile_cols // most tiles with full width and 4 in each tile, 8192/4=2048=2**11
  autostruct uint_0_1024 tile_width_in_mi_clamped // tile width is between (4*64)*8 and (64*64)*8. Clamped to a maximum of 64 for cross
  autostruct uint_0_1024 tile_height_in_mi_clamped // tile_height in mi units. Clamped to a maximum of 64 for cross
  autostruct uint32 tile_width
  autostruct uint32 tile_height

#if ENCODE
  autostruct int32 enc_nonuniform_tile_width[MAX_TILE_COLS]
  autostruct int32 enc_nonuniform_tile_height[MAX_TILE_ROWS]
#endif

  autostruct uint32 max_log2_tile_rows // TODO check range
  autostruct uint32 min_log2_tiles
  autostruct int32 max_tile_width_sb // How many superblocks fit into the maximum tile width?
  autostruct int32 max_tile_area_sb // How many superblocks fit into the maximum tile area?
  // As we may have non-uniform tile sizes (if MAX_TILE is enabled),
  // explicitly track the start coords of each tile row and each tile col.
  // We can simplify some code by having this unconditionally available.
  // See tile_init() for details on how this is used.
  autostruct uint_0_8192 mi_col_starts[MAX_TILE_COLS+1]
  autostruct uint_0_8192 mi_row_starts[MAX_TILE_ROWS+1]

  autostruct uint1     uniform_tile_spacing_flag

  autostruct uint_0_2  log2_tile_rows  // Can have at most 4 rows
  autostruct uint_0_11 log2_tile_cols
  autostruct uint_0_1024 tile_rows
  autostruct uint_0_1024 tile_cols
  autostruct uint1 lossless

  autostruct uint1 coded_lossless // Is the downscaled frame coded in a lossless way?
                                  // (=> no deblock, CDEF, etc. but maybe superres + loop-restoration)
  autostruct uint1 all_lossless   // Is the upscaled frame fully lossless?
                                  // (=> as above, but with no superres or loop-restoration)

  // Which set of probabilities to use.
  // Values: 0..6 (inclusive) = take from LAST_FRAME .. ALTREF_FRAME
  //         7 = use defaults
  autostruct uint3 primary_ref_frame
  autostruct uint8 refresh_frame_flags // Which reference buffers to update
  autostruct uint_0_3 tile_col_sz_mag
  autostruct uint_0_3 tile_sz_mag
#if ENCODE
  autostruct int64 tile_col_sz_mag_location
  autostruct int64 tile_sz_mag_location
#endif // ENCODE
  autostruct uint_0_2 MotionMode
  autostruct uint8 overlappable_neighbors[2]
  // 'pts' in libaom
  autostruct int32 wm_samples_from[SAMPLES_ARRAY_SIZE][2]
  // 'pts_inref' in libaom
  autostruct int32 wm_samples_to[SAMPLES_ARRAY_SIZE][2]
  // 'mbmi->num_proj_ref' in libaom
  autostruct int32 wm_num_samples
  // Number of samples scanned so far for this block
  autostruct uint8 wm_num_samples_scanned
  autostruct int32 wm_fallback_sample_from[2]
  autostruct int32 wm_fallback_sample_to[2]

  autostruct int32 wm_params[6]
  autostruct int16 wm_aux[4]
  autostruct uint1 wm_warpable // Is the model compatible with the warp filter?
  autostruct uint32 context_tile_id
  autostruct uint1 context_updated // Track whether we've updated the relevant tile

  autostruct uint1 ref_frame_sign_bias[MAX_REF_FRAMES]
  autostruct uint_1_15 Precision // set to 1..15, as Range below would need updating if this is exceeded
  autostruct uint16 Range
#if ENCODE
  autostruct uint8 CurByte
  autostruct uint_0_8 NumCurBits
  autostruct uint32 NumBytes
  autostruct uint32 Low
  autostruct uint8 BitData[DAALA_MAX_BITS_BYTES_PER_CHUNK]
  autostruct int32 Count
  autostruct uint32 Offs
  autostruct uint16 PreCarry[DAALA_MAX_SYMBOL_BYTES_PER_CHUNK]
  autostruct uint8 PostCarry[DAALA_MAX_SYMBOL_BYTES_PER_CHUNK]
#endif // ENCODE
#if DECODE
  autostruct uint16 Buffer
  autostruct uint32 Size
  // Track how many more bits are available in the current entropy-coded chunk
  autostruct int32 symbolMaxBits
#endif
  autostruct uint1 intra_only

  autostruct uint_0_8192 mi_row_start
  autostruct uint_0_8192 mi_row_end
  autostruct uint_0_8192 mi_col_start
  autostruct uint_0_8192 mi_col_end

  autostruct uint8 above_seg_context[MAX_MI_COLS]  // This is used to predict the partition size
  autostruct uint1 aU
  autostruct uint1 aL
  autostruct uint1 chroma_aU
  autostruct uint1 chroma_aL
  autostruct uint8 above_pred_flags[MAX_MI_COLS]  // This is used to predict whether the segment prediction is correct

  // The current row and column in MI units
  autostruct uint16 MiRow
  autostruct uint16 MiCol

  autostruct uint8 left_pred_flags[MI_BLOCK_SIZE]
  autostruct uint8 left_seg_context[MI_BLOCK_SIZE]
  autostruct uint_0_9 left_intra_mode_ctx[MI_BLOCK_SIZE*2]
  autostruct uint1 left_context[MAX_MB_PLANE][MI_BLOCK_SIZE*2]

  autostruct uint_0_9 above_intra_mode_ctx[2*MAX_MI_COLS]

  autostruct uint1 above_context[MAX_MB_PLANE][2*MAX_MI_COLS]  // Mark blocks with nonzero coefficients for use in DC coefficient context

  // Note: These are indexed in 'units', which may be either the same as MI_SIZE
  // or half of it, depending on which experiments are enabled.
  autostruct uint8 above_txfm_context[MAX_MI_COLS*2]
  autostruct uint8 left_txfm_context[MI_BLOCK_SIZE*2]

  // Data we need to track to decide on the contexts
  // for the compound_group_idx and compound_idx symbols
  autostruct uint2 above_compound_type[MAX_MI_COLS]
  autostruct uint2 left_compound_type[MI_BLOCK_SIZE]

  // Note: We initialize active_ref_idx[i] with a special value -1 to
  // indicate when the array hasn't been filled out properly yet
  // (eg, early in decoding a frame, or when decoding a key frame).
  autostruct int4 active_ref_idx[ALLOWED_REFS_PER_FRAME]
  autostruct uint1 sign_bias[ALLOWED_REFS_PER_FRAME]

  autostruct uint32 tile_id

  autostruct uint32 current_tile_group // An identifier for the current tile group. This doesn't need to be in
                                       // any particular order, it just needs to distinguish different tile groups.
  autostruct uint32 tg_start           // First tile in the current tilegroup
  autostruct uint32 tg_end             // Last tile in the current tile group

  autostruct uint32 NumTiles
  autostruct uint1  tile_start_and_end_present

#if ENCODE
  autostruct uint64 enc_metadata_type
  autostruct uint32 num_tile_groups
  autostruct int32 tg_starts[MAX_TILES]
  autostruct int32 tg_sizes[MAX_TILES]
  autostruct int tg_data_locations[MAX_TILES]
  autostruct int tg_data_sizes[MAX_TILES]
  autostruct int obu_size_locations[MAX_TILES]
#endif
  autostruct uint1 obu_extension_flag

  autostruct uint32 frame_header_bits

  autostruct uint8 PlaneTxType

  autostruct uint1 error_resilient_mode
  autostruct uint1 frame_parallel_decoding_mode
  autostruct uint1 refresh_frame_context
  autostruct uint_0_3 tx_size
  autostruct uint_0_15 rtxk_type[(MAX_SB_SIZE * MAX_SB_SIZE) / (TX_SIZE_W_MIN * TX_SIZE_H_MIN)]
  rewrite txk_type[x][y] rtxk_type[y*MAX_MIB_SIZE + x]
  autostruct uint_0_3 intra_tx_type
#if ENCODE
  autostruct uint16 enc_last_coef
  autostruct uint16 c_eob
  autostruct uint_0_15 enc_tx_type // Selected type for the current transform block
#endif
  autostruct uint1 seg_enabled
  autostruct uint1 above_skips[MAX_MI_COLS]
  autostruct uint1 above_tx_size[MAX_MI_COLS]

  autostruct uint1 left_skips[MI_BLOCK_SIZE]
  autostruct uint1 left_tx_size[MI_BLOCK_SIZE]

  autostruct uint1 above_skip_mode[MAX_MI_COLS]
  autostruct uint1 left_skip_mode[MI_BLOCK_SIZE]

  autostruct uint1    filter_type_switchable
  autostruct uint_0_4 mcomp_filter_type
  autostruct uint_0_12 sb_size // Type of subblock
  autostruct uint_0_9 sub_modes[4] // Intra mode for 4x4 subblocks if present

  // Transform
  autostruct uint10 scan[1024]

  // Loopfilter
  autostruct uint6 filter_level[FRAME_LF_COUNT] // Y vert, Y horz, U, V
  autostruct uint3 sharpness_level
  autostruct uint8 filter_mask
  autostruct uint8 flat_mask
  autostruct uint8 flat_mask2
  autostruct uint8 hev_mask


  // Frame marker
  autostruct int32 cur_frame_offset
  // Note:
  // ref_decode_order[0] = decode index of the current frame (== cur_frame_offset?)
  // ref_decode_order[i] (i > 0) = decode index of the i'th reference frame
  autostruct int32 ref_decode_order[1+INTER_REFS_PER_FRAME]
  autostruct int24 proj_mv[2]
  autostruct uint_1_8192 pos_mf[2]

  // Frame-level enable-disable flags for coding tools
  autostruct uint1 disable_cdf_update
  autostruct uint1 allow_screen_content_tools
  autostruct uint1 cur_frame_force_integer_mv
  autostruct uint1 allow_intrabc
  autostruct uint1 allow_high_precision_mv
  autostruct uint1 switchable_motion_mode
  autostruct uint1 allow_warped_motion

  autostruct uint1 frame_refs_short_signaling
  autostruct uint3 lstIdx
  autostruct uint3 gldIdx

  autostruct uint32 DeltaFrameId
#if ENCODE
  autostruct uint32 expectedFrameId[ALLOWED_REFS_PER_FRAME]
#endif // ENCODE

  // Motion vectors
  autostruct int24 mv[2][2] // list,comp TODO check range - maybe more for large images as in 8-pel units? motion vectors for the current subblock
  autostruct int24 block_mvs[2][4][2] // list,block_idx,comp motion vectors for the current 8x8 block
  autostruct int24 mv_ref_list[2][2] // number,comp TODO check range - maybe more for large images as in 8-pel units?
  autostruct int24 nearest_mv[2][2] // list,comp TODO check range - maybe more for large images as in 8-pel units?
  autostruct int24 near_mv[2][2] // list,comp TODO check range - maybe more for large images as in 8-pel units?
  autostruct int24 best_mv[2][2] // list,comp TODO check range - maybe more for large images as in 8-pel units?
  autostruct int24 block_pred_mvs[2][4][2] // list,block_idx,comp motion vectors for the current 8x8 block
  autostruct int24 pred_mv[2][2] // list,comp
  autostruct uint8 ref_mv_idx
  autostruct int24 candidate_pred_mv[2][2]  // list,comp
  autostruct int32 rf[2]  // TODO reduce range of type
  autostruct int24 candidate_mv[2+2][2]  // reflist (+2 if scaled),component  (This is still needed for initial search for motion vectors)
#if ENCODE
  autostruct uint_0_2 enc_drl_idx   // Which DRL entry we want to use
  autostruct int24    target_mv[2]  // When encoding a newmv, this holds the intended final MV
  autostruct uint1    mv_overridden
#endif

  autostruct uint_0_13 ry_modes[]
  rewrite y_modes[x][y] ry_modes[y*mi_cols+x]
  autostruct uint_0_13 ruv_modes[]
  rewrite uv_modes[x][y] ruv_modes[y*mi_cols+x]
#if ENCODE
  // Pre-selected superblock sizes for the current tile
  autostruct uint_0_12 renc_sb_sizes[]
  rewrite enc_sb_sizes[x][y] renc_sb_sizes[y*mi_cols+x]
  // Explicitly store the sequence of partition symbols for the current tile.
  // The number of such symbols is at most:
  //    [# 8x8s in tile] + [# 16x16s in tile] + ... + [# 128x128s in tile]
  // == (1 + 1/4 + 1/16 + ... + 1/256) [# 8x8s in tile]
  // == (1/4 + 1/16 + ... + 1/1024) [# MIs in tile]
  // ~= 1/3 [# MIs in tile]
  autostruct uint_0_10 enc_partition_symbols[]
  autostruct uint32 enc_partition_idx // Where are we in this list?
  // For each block, store the lowest partition it's part of
  autostruct uint_0_12 renc_block_partition[]
  rewrite enc_block_partition[x][y] renc_block_partition[y*mi_cols+x]

  autostruct uint1 enc_many_mvs
  autostruct uint1 enc_encourage_not_hasCols
  autostruct uint1 enc_encourage_not_hasRows
  autostruct uint1 enc_encourage_upscale
#endif
  autostruct uint_0_12 rsb_sizes[]
  rewrite sb_sizes[x][y] rsb_sizes[y*mi_cols+x]
  autostruct uint_0_4 rinterp_filters[][]
  rewrite interp_filters[x][y][i] rinterp_filters[y*mi_cols+x][i]
  autostruct uint_0_3 rtx_sizes[]
  rewrite tx_sizes[x][y] rtx_sizes[y*mi_cols+x]
  autostruct uint1 rskip_coeffs[]
  rewrite skip_coeffs[x][y] rskip_coeffs[y*mi_cols+x]
  // Note: in the reference code, there is an 'inter_tx_size' array in each
  // mode info object, which describes what transforms to use at an 8x8 (or, really, (2*unit) x (2*unit))
  // granularity within that mi.
  // We want to store a single array for the whole frame, so that the loopfilter can look into
  // it easily. But then we can't use an 8x8 granularity (see below for an example of why).
  // So here we store the transform sizes per mi.
  //
  // Example: Consider two 4x8 blocks in the same (aligned) 8x8 region,
  // where one uses 4x4 transforms and the other uses a 4x8 transform. In that case, the reference code
  // works fine, since each block gets its own mi structure. But if we tried using an array with 8x8 granularity,
  // these blocks would map to the same location!
  autostruct uint8 rinter_tx_size[]
  rewrite inter_tx_size[x][y] rinter_tx_size[y*mi_cols + x]
  autostruct uint8 min_tx_size
  autostruct int_0_7 cfl_alpha_signs
  autostruct int_(-15)_15 cfl_alpha_u
  autostruct int_(-15)_15 cfl_alpha_v

  // Helpers for predicting CfL blocks
  autostruct int32 cfl_luma_w
  autostruct int32 cfl_luma_h
  autostruct uint8 rpalettesizes[][]
  rewrite PaletteSizes[p][x][y] rpalettesizes[p][y*mi_cols+x]
  autostruct uint8 PaletteSizeY
  autostruct uint8 PaletteSizeUV
#if ENCODE
  // During encoding, we decide quite early whether a block will
  // use palette mode, and use this to influence some intermediate
  // symbols (eg, the selected intra mode)
  autostruct uint1 enc_palette_y
  autostruct uint1 enc_palette_uv
#endif
  autostruct uint12 palette_colors_y[PALETTE_MAX_SIZE]
  autostruct uint12 palette_colors_u[PALETTE_MAX_SIZE]
  autostruct uint12 palette_colors_v[PALETTE_MAX_SIZE]
  autostruct uint12 rpalettecolors[][][]
  rewrite PaletteColors[p][x][y][i] rpalettecolors[p][y*mi_cols+x][i]
  autostruct uint12 palette_cache[2][2 * PALETTE_MAX_SIZE]
  autostruct uint8 palette_cache_len[2]
#if ENCODE
  autostruct uint8 enc_palette_len[2]
  autostruct uint1 enc_use_palette_cache[2][2 * PALETTE_MAX_SIZE]
  // Extra values used if we're using a pattern mode
  autostruct uint1 enc_use_palette_pattern[2]
  autostruct uint16 enc_pattern_colours[2][PALETTE_MAX_SIZE]
#endif
  autostruct uint3 ColorMapY[MAX_SB_SIZE][MAX_SB_SIZE]
  autostruct uint3 ColorMapUV[MAX_SB_SIZE][MAX_SB_SIZE]
  autostruct uint32 CumulatedScore
  autostruct uint32 ColorOrder[PALETTE_COLORS]
  autostruct uint1 use_intrabc
#if ENCODE
    // To help boost coverage, we sometimes generate intrabc vectors
    // on the edge of the allowed region. When we do this, we want to
    // guarantee that intrabc is used
    autostruct uint1 enc_force_intrabc
#endif // ENCODE
  autostruct int32 intraBuffer[33][65]
  autostruct uint1 use_filter_intra
  autostruct uint_0_4 filter_intra_mode
  autostruct int_(-3)_3 rangledeltasy[]
  rewrite AngleDeltasY[x][y] rangledeltasy[y*mi_cols+x]
  autostruct int_(-3)_3 AngleDeltaY
  autostruct int_(-3)_3 AngleDeltaUV
  autostruct uint_0_3 rintrafilters[]
  rewrite IntraFilters[x][y] rintrafilters[y*mi_cols+x]
  autostruct uint6 rlf_lvls[][]
  rewrite lf_lvls[x][y][i] rlf_lvls[y*mi_cols+x][i]
  autostruct uint6 lvl_lookup[MAX_SEGMENTS][MAX_REF_FRAMES][MAX_MODE_LF_DELTAS][FRAME_LF_COUNT]

  autostruct uint1 ris_inters[]
  rewrite is_inters[x][y] ris_inters[y*mi_cols + x]
  autostruct int24 dv_ref[2]
  autostruct uint1 dv_ref_wholepel

  autostruct uint_0_2 refmv_count

  autostruct uint_0_4 left_interp_filters[MI_BLOCK_SIZE][MB_FILTER_COUNT]
  autostruct uint_0_4 above_interp_filters[MAX_MI_COLS][MB_FILTER_COUNT]

  autostruct uint16 plane_dequant[MAX_MB_PLANE][2]
  autostruct uint8 base_qindex
  autostruct uint1 diffUVDelta
  autostruct int_(-15)_15 ac_delta_qindex[MAX_MB_PLANE] // TODO check range
  autostruct int_(-15)_15 dc_delta_qindex[MAX_MB_PLANE]
#if ENCODE
  // For bit-stream generator
  autostruct int_(-15)_15 DeltaQYDc
  autostruct int_(-15)_15 DeltaQUDc
  autostruct int_(-15)_15 DeltaQUAc
  autostruct int_(-15)_15 DeltaQVDc
  autostruct int_(-15)_15 DeltaQVAc
#endif // ENCODE
  autostruct uint1 using_qmatrix
  autostruct uint4 qm_y
  autostruct uint4 qm_u
  autostruct uint4 qm_v
  autostruct uint4 qm_levels[MAX_SEGMENTS][MAX_MB_PLANE]
  autostruct uint_0_5 token_cache[1024]
  autostruct int20 dequant[4096] // TODO check range
#if ENCODE
  autostruct int20 enc_dequant[4096] // TODO check range
#endif // ENCODE
  autostruct uint1 is_inter

  autostruct uint1    reference_select
  autostruct uint_0_2 comp_pred_mode

  autostruct uint1 can_use_ref
  autostruct uint1 can_use_previous
  autostruct uint12 mode_context[MODE_CTX_REF_FRAMES] // Many possible flags added to mode_context
  autostruct uint32 nmv_ctx
  autostruct uint_0_9 compound_mode_context[MODE_CTX_REF_FRAMES]
  autostruct uint_0_3 compound_type
  autostruct uint4 wedge_index
  autostruct uint1 wedge_sign
  autostruct uint1 mask_type
  autostruct uint8 rseg_mask[(MI_SIZE*MI_BLOCK_SIZE)*(MI_SIZE*MI_BLOCK_SIZE)]
  rewrite seg_mask[x][y] rseg_mask[y*MI_SIZE*MI_BLOCK_SIZE + x]
  autostruct uint1 interintra
  autostruct uint_0_3 interintra_mode
  autostruct uint1 wedge_interintra
  autostruct int_(-1)_3 above_ref_frame[2]
  autostruct int_(-1)_3 left_ref_frame[2]

  autostruct uint_0_4 left_interp_filter[MB_FILTER_COUNT]
  autostruct uint_0_4 above_interp_filter[MB_FILTER_COUNT]
  autostruct uint_0_4 interp_filter[MB_FILTER_COUNT]
#if ENCODE
  autostruct uint_0_4 enc_interp_filter[MB_FILTER_COUNT]
#endif
  autostruct uint1 is_reference_frame
  autostruct uint_1_6 comp_fwd_ref[FWD_REFS]
  autostruct uint_1_6 comp_bwd_ref[BWD_REFS]

  // Helper array for determining various contexts which are used
  // by read_ref_frames()
  // Note: Index 0 is unused.
  autostruct uint3 nearby_ref_count[TOTAL_REFS_PER_FRAME]

  // Intra prediction
  autostruct uint16 left_data[MI_BLOCK_SIZE*8*2+2]
  autostruct uint16 above_data[MI_BLOCK_SIZE*8*2+16]
  rewrite above_row[x] above_data[x+16]
  rewrite left_col[x] left_data[x+2]

  autostruct uint16 rframe[][]
  rewrite frame[plane][x][y] rframe[plane][y*stride+x]
  // Output buffer for the CDEF filter
  autostruct uint16 rcdef_frame[][]
  rewrite cdef_frame[plane][x][y] rcdef_frame[plane][y*stride+x]

  // Output buffer for the LR filter
  autostruct uint16 rlr_frame[][]
  rewrite lr_frame[plane][x][y] rlr_frame[plane][y*stride+x]

  autostruct uint16 rupscaled_frame[][]
  rewrite upscaled_frame[plane][x][y] rupscaled_frame[plane][y*stride+x]

  autostruct uint16 rupscaled_cdef_frame[][]
  rewrite upscaled_cdef_frame[plane][x][y] rupscaled_cdef_frame[plane][y*stride+x]

  autostruct int32 rpredSamples[2][MI_BLOCK_SIZE*MI_SIZE*MI_BLOCK_SIZE*MI_SIZE]
  rewrite predSamples[ref][x][y] rpredSamples[ref][y*MI_BLOCK_SIZE*MI_SIZE+x]
  autostruct uint1 block_decoded_map_data[][]
  rewrite block_decoded_map[p][x][y] block_decoded_map_data[p][y*mi_cols*TXFM_PER_MI_COL + x]

  autostruct int_(-255)_255 prev_qindex
  autostruct int_(-255)_255 current_qindex
  autostruct uint_0_8 delta_q_res
  autostruct uint1 delta_q_present
  autostruct uint1 delta_lf_present
    //@todo check the following dimensions
  autostruct uint_0_8 delta_lf_res
  autostruct int_(-255)_255 rcurrent_deltas_lf_from_base[]
  rewrite current_deltas_lf_from_base[x][y] rcurrent_deltas_lf_from_base[y*mi_cols+x]
  autostruct int_(-255)_255 current_delta_lf_from_base
  autostruct int_(-255)_255 prev_delta_lf_from_base
  autostruct uint1 delta_lf_multi
  autostruct int_(-255)_255 rcurrent_deltas_lf_multi[][]
  rewrite current_deltas_lf_multi[x][y][i] rcurrent_deltas_lf_multi[y*mi_cols+x][i]
  autostruct int_(-255)_255 current_delta_lf_multi[FRAME_LF_COUNT]
  autostruct int_(-255)_255 prev_delta_lf_multi[FRAME_LF_COUNT]

  autostruct uint8 ref_mv_count[MODE_CTX_REF_FRAMES]
  autostruct int24 ref_mv_stack[MODE_CTX_REF_FRAMES][MAX_REF_MV_STACK_SIZE][2][2]
  autostruct int24 pred_mv_stack[MODE_CTX_REF_FRAMES][MAX_REF_MV_STACK_SIZE][2][2]  // frame_type / idx / refList / comp
  autostruct uint_0_2 pred_diff_ctx[MODE_CTX_REF_FRAMES][MAX_REF_MV_STACK_SIZE][2] // frame type / idx / refList
  autostruct int32 weight_stack[MODE_CTX_REF_FRAMES][MAX_REF_MV_STACK_SIZE]

  // Values used in the calculation of the ref mv stack
  autostruct int8 rowAdj
  autostruct int8 colAdj
  autostruct int8 maxRowOffset
  autostruct int8 maxColOffset

#if ENCODE
  // Arrays used in enc_propagate_data() - these help determine how many
  // surrounding blocks have the same refs as the current block, or can be
  // modified to have the same refs
  // These are split into "near" and "far", meaning the regions scanned
  // before and after the call to add_tpl_ref_mv() in setup_ref_mv_list()
  autostruct uint16 enc_same_ref[2][MAX_MV_REGION_REFS][2] // (row, col) for each same-ref block
  autostruct uint8 enc_same_ref_cnt[2]
  autostruct uint16 enc_modifiable[2][MAX_MV_REGION_REFS][2] // (row, col) for each modifiable block
  autostruct uint8 enc_modifiable_cnt[2]
#endif
  autostruct uint8 row_match_count
  autostruct uint8 col_match_count
  autostruct uint8 refIdCount[2]
  autostruct uint8 refDiffCount[2]
  autostruct int32 refIdMvs[2][2][2]
  autostruct int32 refDiffMvs[2][2][2]

  // Transform
  autostruct int64 input[64]    // These could be 20bit, but then need to take care that multiplications are done in 64bit mode
  autostruct int64 output[64]
  autostruct int64 halfway[4096]
  autostruct int32 eob
  autostruct int32 eobtotal

  // Inter prediction
  autostruct uint_0_7 ref_idx       // Actual frame buffer to use for reconstruction

  // Temporary area used after horizontal motion compensation filtering
  // Note: When convolve-round is enabled, the values just fit into an int16
  // (actual range is about [-2^13, 1.5*2^14] for 12-bit input)
  autostruct int16 inter_temp[MAX_SB_SIZE][MAX_EXT_SIZE]

  // Intermediate rounding for inter prediction. These depend
  // on the bit depth and whether the current block is compound
  autostruct uint4 interp_round0
  autostruct uint4 interp_round1
  autostruct uint4 interp_post_rounding


  autostruct int_(-16384)_16384 mv_diff_row // Limits because this is the largest symbol possible to be coded with class 10
  autostruct int_(-16384)_16384 mv_diff_col

  //These ranges were determined by brute force, considering all
  //possible values of (p0, p1, q0, q1), filtering out the ones
  //excluded by filter_mask.
  autostruct int_(-136)_135 oq0_base
  autostruct int_(-136)_135 op0_base
  autostruct int_(-129)_127 oq1_base
  autostruct int_(-128)_128 op1_base

  // For warped/global motion, just before calling av1_warp_plane,
  // the parameters to use are stored here
  autostruct uint1 do_warp
  autostruct int32 warp_params[8]
  autostruct int16 warp_aux[4] // alpha, beta, gamma, delta

  // Dual return values from resolve_divisor
  // Note: 'div_factor' includes any - sign which may need to be applied,
  // unlike the return value of libaom's resolve_divisor_*
  autostruct int16 div_factor
  autostruct int16 div_shift

  autostruct uint2 gm_type[TOTAL_REFS_PER_FRAME]
  autostruct int16 gm_aux[TOTAL_REFS_PER_FRAME][4] // alpha, beta, gamma, delta for each ref frame
  autostruct uint1 gm_warpable[TOTAL_REFS_PER_FRAME] // Which models are compatible with the warp filter?

  // For code simplicity, we still use 'globalmv' even if global-motion is disabled,
  // it just always gets assigned a value of 0 in that case.
  autostruct int16 globalmv[2][2]

  autostruct uint8 cdef_damping
  autostruct uint2 cdef_bits

  // For each strength index (of which there are (1 << cdef_bits) many),
  // each of the following values are coded:
  autostruct uint4 cdef_y_pri_strength[CDEF_MAX_STRENGTHS]
  autostruct uint_0_4 cdef_y_sec_strength[CDEF_MAX_STRENGTHS]
  autostruct uint4 cdef_uv_pri_strength[CDEF_MAX_STRENGTHS]
  autostruct uint_0_4 cdef_uv_sec_strength[CDEF_MAX_STRENGTHS]

  // Per-64x64-block index into the above arrays
  autostruct int8 rcdef_idx[]
  rewrite cdef_idx[c][r] rcdef_idx[r*((mi_cols + MI_SIZE_64X64 - 1) >> MI_SIZE_64X64_LOG2) + c]

  // Second return value from cdef_find_dir
  autostruct int32 cdef_var

  autostruct uint1       UsesLr
  autostruct uint_64_256 restoration_unit_size[MAX_MB_PLANE]
  autostruct uint_0_3    frame_restoration_type[MAX_MB_PLANE]

  // Each tile has its own set of restoration units. To keep the memory usage
  // from getting too out of hand, we linearize this into a single array of size
  // MAX_LR_UNITS_IN_FRAME, storing the index of the first restoration unit of
  // each tile in the 'restoration_unit_start' array.
  autostruct uint32 restoration_unit_start[MAX_MB_PLANE][MAX_TILES]
  autostruct uint32 restoration_unit_cols[MAX_MB_PLANE][MAX_TILES]
  autostruct uint32 restoration_unit_rows[MAX_MB_PLANE][MAX_TILES]

  // Array to hold the context for a particular processing unit. This is intended
  // to help with the striped-loop-restoration experiment.
  autostruct uint16 rprocunit_input[(RESTORATION_PROCUNIT_SIZE+6)*(RESTORATION_PROCUNIT_SIZE+6)]
  rewrite procunit_input[x][y] rprocunit_input[(y+3)*(RESTORATION_PROCUNIT_SIZE+6)+(x+3)]

  autostruct uint_0_3 unit_restoration_type[MAX_MB_PLANE][MAX_LR_UNITS_IN_FRAME]
  // Note: Each restoration unit uses wiener_coeffs *xor* the sgr_* values
  autostruct int8 wiener_coeffs[MAX_MB_PLANE][MAX_LR_UNITS_IN_FRAME][2][WIENER_COEFFS]
  autostruct uint8 sgr_param_set[MAX_MB_PLANE][MAX_LR_UNITS_IN_FRAME]
  autostruct int8 sgr_xqd[MAX_MB_PLANE][MAX_LR_UNITS_IN_FRAME][2]

  // Reference for the next parameters to be decoded.
  // This is whatever was read for the previous restoration unit of the appropriate
  // type *within the current tile*; between tiles it is reset to a default value
  autostruct int8 ref_wiener_coeffs[MAX_MB_PLANE][2][WIENER_COEFFS]
  autostruct int8 ref_sgr_xqd[MAX_MB_PLANE][2]

  // Copy a bit of non-normative behaviour from the reference code:
  // We want the number and ordering of calls to log_cdef() to be the same between our
  // code and the reference code. In certain cases, the reference code detects that there
  // is no point applying CDEF filtering to the frame (ie, it will not change the output),
  // and so skips the entire CDEF process.
  // We need to calculate the same set of conditions, and store them in a variable.
  autostruct uint1 skip_cdef

  autostruct uint1 use_superres
  autostruct uint8 codedDenom
  autostruct uint8 superres_denom

#if ENCODE
    autostruct uint1 enc_coef_sign_pattern
    autostruct int2 enc_row_sign[32]
    autostruct int2 enc_col_sign[32]
    autostruct int20 enc_orig_coef[1024]
    autostruct uint16 enc_coef_rounding[1024]
    autostruct int20 enc_coef[1024]
    autostruct uint1 enc_sign_bit[1024]
    autostruct uint16 enc_mvcomp_class
    autostruct uint16 enc_mvcomp_bits
    autostruct uint2 enc_mvcomp_fp
    autostruct uint1 enc_mvcomp_hp

    // Values used when generating a special pattern of pixels
    // The patterns are generated by picking a sign per row and per column,
    // and a function for combining those signs to determine each individual
    // pixel's sign (and, or, xor, or their negations)
    // By "a pixel's sign", we mean that a pixel is given the minimum value if
    // the resulting sign is 0 and the maximum value if the resulting sign is 1.
    autostruct uint1 enc_palette_row_sign[64]
    autostruct uint1 enc_palette_col_sign[64]
    autostruct uint2 enc_pixel_pattern_fn // and, or, xor
    autostruct uint1 enc_pixel_pattern_sign

    // Palette blocks may be up to 64x64 in size. If we pre-select
    // a pattern, then this array is filled out with which index
    // to use at each point.
    autostruct uint1 enc_palette_entries[2][64][64]

    autostruct uint2 enc_chosen_plane
    autostruct uint12 enc_chosen_c
    autostruct uint8 enc_chosen_block_idx

    autostruct int8 renc_segment_id[]
    rewrite enc_segment_id[col][row] renc_segment_id[row * mi_cols + col]
    autostruct int8 renc_is_inter[]
    rewrite enc_is_inter[col][row] renc_is_inter[row * mi_cols + col]
    autostruct int8 renc_skip_mode[]
    rewrite enc_skip_mode[col][row] renc_skip_mode[row * mi_cols + col]
    autostruct int8 renc_skip_coeff[]
    rewrite enc_skip_coeff[col][row] renc_skip_coeff[row * mi_cols + col]
    autostruct int8 renc_ref_frame[][]
    rewrite enc_ref_frame[col][row][idx] renc_ref_frame[row * mi_cols + col][idx]
#endif

  // Moved from frame_probs when removing old probability-based code
  autostruct uint_0_(PARTITION_CONTEXTS-1) cross_ctxIdxOffset
  autostruct uint_0_(MBSKIP_CONTEXTS-1) cross_skip_ctxIdxOffset
  autostruct uint_0_(BLOCK_SIZE_GROUPS-1) cross_block_size_groups_ctxIdxOffset
  autostruct uint_0_(INTER_MODE_CONTEXTS-1) cross_inter_mode_ctxIdxOffset
  autostruct uint_0_(SWITCHABLE_FILTER_CONTEXTS-1) cross_switchable_filter_ctxIdxOffset
  autostruct uint_0_(COMP_INTER_CONTEXTS-1) cross_comp_inter_ctxIdxOffset
  autostruct uint_0_(REF_CONTEXTS-1) cross_ref_ctxIdxOffset
  autostruct uint_0_(INTRA_INTER_CONTEXTS-1) cross_intra_inter_ctxIdxOffset
  autostruct uint_0_(PREDICTION_PROBS-1) cross_prediction_probs_ctxIdxOffset
  autostruct uint_0_(TX_SIZE_CONTEXTS-1) cross_tx_size_ctxIdxOffset
  autostruct uint_0_(PREV_COEF_CONTEXTS-1) cross_prev_coef_ctxIdxOffset
  autostruct uint_0_3 cross_max_tx_size
  autostruct uint_0_(TX_SIZES-1) cross_tx_sz
  autostruct uint_0_(BLOCK_TYPES-1) cross_ptype
  autostruct uint_0_(REF_TYPES-1) cross_ref
  autostruct uint_0_(COEF_BANDS-1) cross_band
  autostruct uint_0_(MV_OFFSET_BITS-1) cross_mvcompi
  autostruct uint_0_(CLASS0_SIZE-1) cross_mvcomp_class0_bits
  autostruct uint_0_(INTRA_MODES - 1) cross_y_mode
  autostruct uint1 cross_mvcomp

#if ENCODE
  autostruct uint_0_2 ValidDrlIndices[3]
  autostruct uint_0_2 NumValidDrlIndices

  // Make partition variable available in bit-stream generator
  autostruct uint_0_9 partition_cfg
#endif
}
