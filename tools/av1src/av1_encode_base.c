/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#line 8 "av1src/av1_encode_base.c"

#include <math.h>
#include "av1_enc_command_line.hh"
#include "av1-cfg.h"

#include <libgen.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#define MAX(a,b) ((a)>(b)?(a):(b))

FILE *ref_log=NULL;
FILE *val_fid=NULL;
int val_count=0;

void validate_stop(void) {
  if (debugMode) printf("Stop");
}

void validate(int x) {
  if (!val_fid) {
    val_fid = fopen("validate_c_enc.txt","wb");
    assert(val_fid);
  }
  fprintf(val_fid,"%d %d\n",val_count,x);
  if (val_count==1799433) {
    validate_stop();
  }
  fflush(val_fid);
  val_count++;
}

void CSV(int id, ...) {
}

static int is_obu_file(const char* fname) {
  size_t len = strlen(fname);
  if (len < 4) return 0;

  const char *suffix = fname + (len-4);
  const char *obu_ext = ".obu";
  return (!strcmp(suffix, obu_ext));
}

int main (int argc, char *argv[])
{
  EncCommandLine cmd_line (argc, argv);

  debugMode = cmd_line.verbose;

  if (! cmd_line.filename) {
    printf("No output file specified.\n");
    cmd_line.syntax(std::cerr);
    exit(2);
  }

  mysrand(cmd_line.seed);
  srand(cmd_line.seed);
  std::srand(cmd_line.seed);

#if !RELEASE
  if (cmd_line.choose_all_filename)
    loadChooseAll (cmd_line.choose_all_filename);
#endif

  std::unique_ptr<TOPLEVEL_T> b (new TOPLEVEL_T ());
  std::unique_ptr<GLOBAL_DATA_T> g (new GLOBAL_DATA_T ());

  b->global_data = g.get ();

  if (cmd_line.config_file) {
      setup_symbol_table (* b);
      if (! read_config_file (cmd_line.config_file,
                              cmd_line.config_selected,
                              cmd_line.seed,
                              cmd_line.config_element_debug)) {
          // If reading the config file failed, we will have spat out
          // some error messages already. Just return 1.
          return 1;
      }
  }

  IVF_FILE_HEADER_T file_hdr;
  int isObu = is_obu_file (cmd_line.filename);

  hevc_parse_global_data (b.get (), g.get (), cmd_line.annex_b, isObu);

  g->enc_stream_seed=cmd_line.seed;
  g->enc_large_scale_tile = cmd_line.large_scale_tile;

#if !RELEASE
  g->enc_decrease_target_bitrate_percent = cmd_line.decrease_target_bitrate_pc;
#else
  g->enc_decrease_target_bitrate_percent = 0;
#endif

  FILE *fd = fopen (cmd_line.filename,"wb");
  if (!fd) {
    printf("Cannot open destination filename %s\n", cmd_line.filename);
    exit(1);
  }

  encode_bitstream_init (b.get ());
  if (isObu) {
    hevc_parse_obu_file_init(b.get (),NULL);
  } else {
    hevc_parse_ivf_file_header(b.get (), &file_hdr);
  }
  fwrite(b->data[0].data,1,b->data[0].len,fd);

  int i = 0;
  while (i<(int)b->global_data->frame_cnt) {
    IVF_FRAME_HEADER_T frame_hdr;

    encode_bitstream_init (b.get ());
    i += hevc_parse_encode_temporal_unit (b.get (), NULL,
                                          b->global_data->frame_cnt-i);

    int framelen = b->data[0].len;

    if (b->global_data->is_ivf_file) {
      // Stash the encoded frame so we can generate a frame header
      char *framedata = (char *)malloc(framelen);
      assert(framedata);
      memcpy(framedata,b->data[0].data,framelen);

      encode_bitstream_init (b.get ());
      hevc_parse_ivf_frame_header (b.get (), &frame_hdr,framelen);
      fwrite(b->data[0].data,1,b->data[0].len,fd);

      fwrite(framedata,1,framelen,fd);
      free(framedata);
    } else {
      // No frame header, so we can write the frame directly
      fwrite(b->data[0].data,1,framelen,fd);
    }

    if (b->global_data->enc_num_shown_frames_required > 0) {
      if (b->global_data->num_shown_frames >= b->global_data->enc_num_shown_frames_required) {
        break;
      }
    } else if (b->global_data->enc_is_stress_stream) {
      printf("enc_num_shown_frames_required not setup for choose_stress_streams()\n");
      exit(1);
    }
  }
  if (b->global_data->enc_num_shown_frames_required > 0) {
    if (b->global_data->num_shown_frames != b->global_data->enc_num_shown_frames_required) {
      printf("Incorrect number of frames generated: enc_num_shown_frames_required %d, num_shown_frames %d\n", b->global_data->enc_num_shown_frames_required,b->global_data->num_shown_frames);
      exit(3);
    }
  }
  fclose(fd);

  // If we failed any checks, stop here with error code 2.
  if (some_check_failed) {
    fflush (stdout);
    fprintf (stderr, "One or more checks failed. Exiting with status 2.\n");
    return 2;
  }

#if !RELEASE
  if (cmd_line.choose_all_filename)
    saveChooseAll(cmd_line.choose_all_filename);
#endif

  return 0;
}


/* Called when a frame is output from the decoded picture buffer.
These will be returned in decode order. */
void video_output(TOPLEVEL_T *b,int bps, int w, int h, int pitch, int srcX, int srcY,int sw,int sh, int is_monochrome) {
}





