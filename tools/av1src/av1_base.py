################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

from optparse import OptionParser
import struct,os

# set this to False to always output the bpp in the first frame
outputBppPerFrame=True
# set this to one to always output two bytes per pixel in 8bpp in LST mode
outputLST2Bytes=False

check_bins    = 0  # auto checking: (In dkboolreader.c LOG_CABAC)
check_mvs     = 0  # auto checking: (In decodemv.c )
check_recon   = 0  # auto checking: (In decodeframe.c LOG_PRED and reconintra ARGON_LOG_ALL)
check_inter   = 0  # auto checking: Use log_recon_cabac.patch  (Ref code clips motion vectors before filtering so this is not always passing)
check_lf      = 0  # auto checking: (need to change define in loopfilter.c to stop edges being merged) Ref code adds extra deblocking off edge
check_loop    = 0  # auto checking: (in loopfilter.c LOG_LOOP), superblock level loopfilter checking
check_cdef    = 0  # auto checking: 64x64 level cdef checking
check_lr      = 0  # auto checking: Processing-unit-level loop-restoration checking
check_upscale = 0  # auto checking: Upscaler input and ouptut, dumped per tile column
check_coeffs  = 0  # auto checking: (In decodeframe.c LOG_COEFFS)
check_output  = 0  # auto checking: Check each output frame before (0) and after (1) film grain synthesis
check_anchor_ref = 0 # auto checking: Check the anchor frame loaded for each tile decoded in ext_tile
check_tile_output = 0 # auto checking: Check the tile output is right
skip_allowed  = 0  # If we have an odd multiple of 8 size, then ref code filters some offscreen edges which we should skip

path_to_ref = r"aom/build/"

if check_bins:
  ref_cabac_log=None
  cabac_bins_count = 0
  def debug_cabac(s):
      global cabac_bins_count,check_bins,ref_cabac_log,dut_cabac_log
      if ref_cabac_log is None:
        ref_cabac_log=open(path_to_ref+'ref_cabac2.txt','rb')
        dut_cabac_log=open(path_to_ref+'dut_cabac.txt','wb')
      print >>dut_cabac_log,s
      if not check_bins:
        cabac_bins_count+=1
        return
      if cabac_bins_count == -122958:
        pdb.set_trace()
      while 1:
        s2=ref_cabac_log.readline().rstrip()
        if s2=='':
          print 'Reached end of reference file'
          break
        if 'bin=' in s2:
          break
      if s2!=s and check_bins:
          # try stripping the buf= part off the end (the ref encoder zero fills, we don't)
          s2BufEnd = s2.find(", buf=")
          sBufEnd = s.find(", buf=")
          s2Strip = s2
          sStrip = s
          if sBufEnd != -1 and s2BufEnd != -1:
            s2Strip = s2[0:s2BufEnd]
            sStrip = s[0:sBufEnd]
          if s2Strip != sStrip:
            dut_cabac_log.flush()
            print 'good',s2
            print 'bad',s
            pdb.set_trace()
            check_bins = 0
      #if cabac_bins_count>=6-1:
      #  pdb.set_trace()
      cabac_bins_count+=1

  def debug_cabac_daala_bits(binVal):
      s='%d bin=%d'%(cabac_bins_count,binVal)
      debug_cabac(s)

  def debug_cabac_daala_symbol(symbol,nsyms,rng,bits,Fh,Fl,buf):
      s='%d bin=%d/%d, rng=0x%x, bits=%d, Fh=%d, Fl=%d, buf=%d'%(cabac_bins_count,symbol,nsyms,rng,bits,Fh,Fl,buf)
      debug_cabac(s)

  def debug_cabac_bins(binVal,startRange,startVal,prob):
      if startRange==255:
        return # Skip marker bits as they appear out of order
      s='%d bin=%d sr=%d, sv=%d p=%d'%(cabac_bins_count,binVal,startRange,startVal,prob)
      debug_cabac(s)
else:
  def debug_cabac_bins(binVal,startRange,startVal,prob):
    pass
  def debug_cabac_daala_bits(binVal):
    pass
  def debug_cabac_daala_symbol(symbol,nsyms,rng,bits,Fh,Fl,buf):
    pass

if check_mvs:
  ref_mv_log=None
  mv_count = 0
  def debug_mv(b,i,mode,compound):
      global mv_count,ref_mv_log,dut_mv_log
      if ref_mv_log is None:
        ref_mv_log=open(path_to_ref+'ref_mv2.txt','rb')
        dut_mv_log=open(path_to_ref+'dut_mv.txt','wb')
      if i and not compound:
        return
      s="%d i=%d mode=%d dx=%d dy=%d"%(mv_count,i,mode,b.video_stream.frame_data.mv[i][1],b.video_stream.frame_data.mv[i][0])
      print >>dut_mv_log,s
      while 1:
        s2=ref_mv_log.readline().rstrip()
        if s2=='':
          print 'Reached end of reference file'
          break
        if 'mode=' in s2:
          break
      if s2!=s:
          dut_mv_log.flush()
          print 'good',s2
          print 'bad',s
          pdb.set_trace()
      #if mv_count==1082:
      #  pdb.set_trace()
      mv_count+=1
else:
  def debug_mv(b,i,mode,compound):
    pass

if check_lf:
  ref_lf_log=None
  lf_count = 0
  def debug_lf(b,phase,sz,x,y,dx,dy,plane):
      global lf_count, check_lf, ref_lf_log, dut_lf_log
      if ref_lf_log is None:
        ref_lf_log=open(path_to_ref+'ref_lf2.txt','rb')
        dut_lf_log=open(path_to_ref+'dut_lf.txt','wb')
      I=b.video_stream.frame_data.rframe
      pitch = b.video_stream.frame_data.stride
      a="%d phase=%d sz=%d x=%d y=%d plane=%d dx=%d dy=%d"%(lf_count,phase,sz,x,y,plane,dx,dy)
      a2=ref_lf_log.readline().rstrip()
      print >>dut_lf_log,a
      s=' '.join( ('%.2x' % I[plane][x+dx*i+(y+dy*i)*pitch]) for i in range(-(sz>>1),(sz>>1)))
      s2=ref_lf_log.readline().rstrip()
      print >>dut_lf_log,s
      if skip_allowed:
        a2_orig=a2
        s2_orig=s2
        num_skips=0
        while s!=s2:
          # Skip in batches of 4
          for i in range(4):
            a2=ref_lf_log.readline().rstrip()
            s2=ref_lf_log.readline().rstrip()
          num_skips+=1
          if num_skips>256:
            print 'skip failed to find match'
            a2=a2_orig
            s2=s2_orig
            break
      if s2!=s and check_lf:
          dut_lf_log.flush()
          print 'good',a2
          print 'good samples',s2
          print 'bad ',a
          print 'bad samples ',s
          pdb.set_trace()
          check_lf = False
      #if lf_count>=1343:
      #  pdb.set_trace()
      lf_count+=1
else:
  lf_count = 0
  def debug_lf(b,phase,sz,x,y,dx,dy,plane):
    #global lf_count
    #I=b.video_stream.frame_data.rframe
    #pitch = b.video_stream.frame_data.stride
    #a="%d phase=%d sz=%d x=%d y=%d plane=%d dx=%d dy=%d"%(lf_count,phase,sz,x,y,plane,dx,dy)
    #print a
    #s=' '.join( ('%.2x' % I[plane][x+dx*i+(y+dy*i)*pitch]) for i in range(-(sz>>1),(sz>>1)))
    #print s
    pass

if check_loop:
  ref_loop_log=None
  loop_count = 0
  def log_loop(b,mi_row,mi_col,step,plane,mi_size,lf_block_size):
      global loop_count, check_loop, ref_loop_log, dut_loop_log
      if ref_loop_log is None:
        ref_loop_log=open(path_to_ref+'ref_loop2.txt','rb')
        dut_loop_log=open(path_to_ref+'dut_loop.txt','wb')
      I = b.video_stream.frame_data.rframe
      pitch = b.video_stream.frame_data.stride
      #if loop_count>=1343:
      #  pdb.set_trace()
      print >>dut_loop_log,loop_count,plane,step
      a2=ref_loop_log.readline().rstrip()
      block_px = lf_block_size * mi_size
      w = min(block_px,(b.video_stream.frame_data.mi_cols - mi_col)*mi_size) >> b.global_data.plane_subsampling_x[plane]
      h = min(block_px,(b.video_stream.frame_data.mi_rows - mi_row)*mi_size) >> b.global_data.plane_subsampling_y[plane]
      basex = (mi_col)*mi_size >> b.global_data.plane_subsampling_x[plane]
      basey = (mi_row)*mi_size >> b.global_data.plane_subsampling_y[plane]

      for y in range(h):
        s = ' '.join('%.2x'%I[plane][x+basex+(y+basey)*pitch] for x in range(w))
        print >>dut_loop_log,s
        s2 = ref_loop_log.readline().rstrip()
        if s2!=s and check_loop:
          dut_loop_log.flush()
          print 'good',a2
          print 'good loop',s2
          print 'bad ',loop_count,plane,step,basex,basey,y,pitch
          print 'bad loop ',s
          pdb.set_trace()
          check_loop = False
      loop_count += 1
else:
  def log_loop(b,mi_row,mi_col,step,plane,mi_size,lf_block_size):
    pass

if check_cdef:
  ref_cdef_log=None
  cdef_count = 0
  def log_cdef(b,mi_row,mi_col,step,mi_size,nplanes):
      global cdef_count, check_cdef, ref_cdef_log, dut_cdef_log
      if ref_cdef_log is None:
        ref_cdef_log=open(path_to_ref+'ref_cdef2.txt','rb')
        dut_cdef_log=open(path_to_ref+'dut_cdef.txt','wb')
      if step == 0:
        I = b.video_stream.frame_data.rframe
      else:
        I = b.video_stream.frame_data.rcdef_frame
      pitch = b.video_stream.frame_data.stride
      for plane in range(nplanes):
        #if cdef_count>=1343:
        #  pdb.set_trace()
        print >>dut_cdef_log,cdef_count,plane,step
        a2=ref_cdef_log.readline().rstrip()
        w = min(64,(b.video_stream.frame_data.mi_cols - mi_col)*mi_size) >> b.global_data.plane_subsampling_x[plane]
        h = min(64,(b.video_stream.frame_data.mi_rows - mi_row)*mi_size) >> b.global_data.plane_subsampling_y[plane]
        x0 = ((mi_col)*mi_size >> b.global_data.plane_subsampling_x[plane])
        y0 = ((mi_row)*mi_size >> b.global_data.plane_subsampling_y[plane])

        for y in range(h):
          s = ' '.join('%.2x'%I[plane][x+x0+(y+y0)*pitch] for x in range(w))
          print >>dut_cdef_log,s
          s2 = ref_cdef_log.readline().rstrip()
          if s2!=s and check_cdef:
            dut_cdef_log.flush()
            print 'good',a2
            print 'good cdef',s2
            print 'bad ',cdef_count,plane,step,x0,y0,y,pitch
            print 'bad cdef ',s
            s3 = ""
            for i in range(max(len(s), len(s2))):
              if i >= min(len(s), len(s2)) or s2[i] != s[i]:
                s3 += "*"
              else:
                s3 += s[i]
            print 'diff     ', s3
            pdb.set_trace()
            check_cdef = False
        cdef_count += 1
else:
  def log_cdef(b,mi_row,mi_col,step,mi_size,nplanes):
    pass

if check_lr:
  ref_lr_log=None
  lr_count = 0
  def log_lr(b,basex,basey,w,h,plane,step):
      global lr_count, check_lr, ref_lr_log, dut_lr_log
      if ref_lr_log is None:
        ref_lr_log=open(path_to_ref+'ref_lr2.txt','rb')
        dut_lr_log=open(path_to_ref+'dut_lr.txt','wb')
      # Set up a few values:
      # I = array to take data from
      # (x0, y0) to (x1, y1) = region of pixels to dump
      # (xoff, yoff) = position to start within I
      # (basex, basey) = position of start within the frame (for info only)
      if step == 0:
        I = b.video_stream.frame_data.rprocunit_input
        xoff = yoff = 3
        x0 = y0 = -3
        x1 = w+3
        y1 = h+3
        pitch = 70
      else:
        I = b.video_stream.frame_data.rlr_frame[plane]
        pitch = b.video_stream.frame_data.stride
        xoff = basex
        yoff = basey
        x0 = y0 = 0
        x1 = w
        y1 = h
      #if lr_count>=1343:
      #  pdb.set_trace()
      print >>dut_lr_log,lr_count,plane,step
      a2=ref_lr_log.readline().rstrip()

      for y in range(y0, y1):
        s = ' '.join('%.2x'%I[x+xoff+(y+yoff)*pitch] for x in range(x0, x1))
        print >>dut_lr_log,s
        s2 = ref_lr_log.readline().rstrip()
        if s2!=s and check_lr:
          dut_lr_log.flush()
          print 'good',a2
          print 'good lr',s2
          print 'bad ',lr_count,plane,step,basex,basey,y,pitch,w,h
          print 'bad lr ',s
          s3 = ""
          for i in range(max(len(s), len(s2))):
            if i >= min(len(s), len(s2)) or s2[i] != s[i]:
              s3 += "*"
            else:
              s3 += s[i]
          print 'diff   ', s3
          pdb.set_trace()
          check_lr = False
      lr_count += 1
else:
  def log_lr(b,x,y,w,h,plane,step):
    pass

if check_inter:
  ref_inter_log=None
  inter_count = 0
  def debug_inter(subpel_x,subpel_y,w,h,mvrow,mvcol):
      global inter_count, ref_inter_log, dut_inter_log
      if ref_inter_log is None:
        ref_inter_log=open(path_to_ref+'ref_inter2.txt','rb')
        dut_inter_log=open(path_to_ref+'dut_inter.txt','wb')

      s="%d subx=%d suby=%d w=%d h=%d mvrow=%d mvcol=%d"%(inter_count,subpel_x, subpel_y, w, h, mvrow, mvcol)
      s2=ref_inter_log.readline().rstrip()
      print >>dut_inter_log,s
      if s2!=s:
          dut_inter_log.flush()
          print 'good',s2
          print 'bad ',s
          pdb.set_trace()
      #if inter_count==103:
      #  pdb.set_trace()
      inter_count+=1
else:
  def debug_inter(subpel_x,subpel_y,w,h,mvrow,mvcol):
    pass

PREDICTION = 0
TRANSFORM = 1
ABOVE = 2
LEFT = 3
OBMC_PRE = 4
OBMC_POST = 5
if check_recon:
  ref_recon_log=None
  recon_count = 0
  def setup_recon_log():
    global recon_count, ref_recon_log, dut_recon_log
    if ref_recon_log is None:
      ref_recon_log=open(path_to_ref+'ref_recon2.txt','rb')
      dut_recon_log=open(path_to_ref+'dut_recon.txt','wb')

  def log_prediction(b,tx_sz,x,y,plane,phase):
      bw = tx_size_wide[tx_sz]
      bh = tx_size_high[tx_sz]
      I=b.video_stream.frame_data.rframe[plane]
      pitch = b.video_stream.frame_data.stride
      log_prediction_rect(b,bw,bh,x,y,plane,phase,I,pitch)

  def log_prediction_rect_pred(b,bw,bh,plane,ref,pitch,phase):
      I=b.video_stream.frame_data.rpredSamples[ref]
      log_prediction_rect(b,bw,bh,0,0,plane,phase,I,pitch)

  def log_prediction_rect_frame(b,bw,bh,x,y,plane,phase):
      I=b.video_stream.frame_data.rframe[plane]
      pitch = b.video_stream.frame_data.stride
      log_prediction_rect(b,bw,bh,x,y,plane,phase,I,pitch)

  def log_prediction_rect(b,bw,bh,x,y,plane,phase,I,pitch):
      global recon_count,check_recon
      if not check_recon:
        return
      setup_recon_log()

      #if plane==0 and x<=40<x+bs and y<=1<y+bs:
      #  for dy in range(4):
      #    print dy,' '.join(('%.2x' % I[plane][x+dx+(y+dy)*pitch]) for dx in range(bs))
      #  pdb.set_trace()

      for dy in range(bh):
        s=' '.join(('%.2x' % I[x+dx+(y+dy)*pitch]) for dx in range(bw))
        print >>dut_recon_log,s
        while 1:
          s2=ref_recon_log.readline().rstrip()
          if s2=='':
            print 'Reached end of reference file'
            break
          if s2[0]=='P':
            s3="Prediction %d %d %d"%(recon_count, plane if phase>=0 else -1, phase)
            if s3!=s2:
              print "Mismatch in prediction marker"
              print 'good',s2
              print 'bad',s3
              pdb.set_trace()
              check_recon = 0
            else:
              continue
          if s2[0]=='R':
            continue
          break
        if s2!=s:
            if phase==PREDICTION:
              print 'Mismatch in prediction'
            elif phase==TRANSFORM:
              print 'Mismatch in transform'
            elif phase==ABOVE:
              print 'Mismatch in above'
            elif phase==LEFT:
              print 'Mismatch in left'
            elif phase==OBMC_PRE:
              print 'Mismatch in obmc pre'
            elif phase==OBMC_POST:
              print 'Mismatch in obmc post'
            else:
              print 'Mismatch in ???'
            dut_recon_log.flush()
            print 'x=',x,'y=',y,'dy=',dy
            print 'good',s2
            print 'bad ',s
            pdb.set_trace()
            check_recon = 0
      #if recon_count==5997 and not phase:
      #  pdb.set_trace()
      if phase==TRANSFORM:
        recon_count+=1

  track_intra_errors = 0
  def log_intra_neighbours(b,txw,txh,mode):
    global track_intra_errors
    setup_recon_log()
    s=('Intra %d left:'%mode)+' '.join(('%.2x' % b.video_stream.frame_data.left_data[x+2]) for x in range(txw+txh))
    print >>dut_recon_log,s
    s2=ref_recon_log.readline().rstrip()
    if track_intra_errors and s2!=s:
      dut_recon_log.flush()
      print 'good',s2
      print 'bad',s
      pdb.set_trace()
      track_intra_errors = 0
    s='Intra above:'+' '.join(('%.2x' % b.video_stream.frame_data.above_data[x+16]) for x in range(-1,txw+txh))
    print >>dut_recon_log,s
    s2=ref_recon_log.readline().rstrip()
    if track_intra_errors and s2!=s:
      dut_recon_log.flush()
      print 'good',s2
      print 'bad',s
      pdb.set_trace()
      track_intra_errors = 0
else:
  recon_count = 0
  def log_prediction(b,tx_sz,x,y,plane,phase):
    global recon_count
    if phase:
        recon_count+=1
  def log_prediction_rect_pred(b,bw,bh,plane,ref,pitch,phase):
    pass
  def log_prediction_rect_frame(b,bw,bh,x,y,plane,phase):
    pass
  def log_prediction_rect(b,bw,bh,x,y,plane,phase,I,pitch):
    pass
  def log_intra_neighbours(b,txw,txh,mode):
    pass

if check_upscale:
  ref_upscale_log=None
  upscale_count = 0
  def log_upscale(b,x0,x1,h,plane,tile_col,phase,I):
    global upscale_count, check_upscale, ref_upscale_log, dut_upscale_log
    if ref_upscale_log is None:
      ref_upscale_log=open(path_to_ref+'ref_upscale2.txt','rb')
      dut_upscale_log=open(path_to_ref+'dut_upscale.txt','wb')
    pitch = b.video_stream.frame_data.stride
    #if upscale_count>=1343:
    #  pdb.set_trace()
    print >>dut_upscale_log,upscale_count,plane,tile_col,plane,x1-x0,h
    a2=ref_upscale_log.readline().rstrip()

    for y in range(h):
      s = ' '.join('%.2x'%I[x+y*pitch] for x in range(x0, x1))
      print >>dut_upscale_log,s
      s2 = ref_upscale_log.readline().rstrip()
      if s2!=s and check_upscale:
        dut_upscale_log.flush()
        print 'good',a2
        print 'good upscale',s2
        print 'bad ',upscale_count,plane,tile_col,plane,x1-x0,h,'row',y
        print 'bad upscale ',s
        s3 = ""
        for i in range(max(len(s), len(s2))):
          if i >= min(len(s), len(s2)) or s2[i] != s[i]:
            s3 += "*"
          else:
            s3 += s[i]
        print 'diff   ', s3
        pdb.set_trace()
        check_upscale = False
    upscale_count += 1
else:
  def log_upscale(b,x0,x1,h,plane,tile_col,phase,I):
    pass

if check_coeffs:
  ref_coeffs_log=None
  coeffs_count = 0
  def log_coeffs(b):
      global coeffs_count, check_coeffs, ref_coeffs_log, dut_coeffs_log
      if ref_coeffs_log is None:
        ref_coeffs_log=open(path_to_ref+'ref_coeffs2.txt','rb')
        dut_coeffs_log=open(path_to_ref+'dut_coeffs.txt','wb')

      eob = b.video_stream.frame_data.eob
      s = "%d: "%(eob)
      s += ' '.join('%d'%b.video_stream.frame_data.dequant[c] for c in range(eob))
      print >>dut_coeffs_log,s

      s = s.rstrip()
      s2 = ref_coeffs_log.readline().rstrip()
      if s2!=s and check_coeffs:
        dut_coeffs_log.flush()
        print 'good coeffs',s2
        print 'bad coeffs ',s
        pdb.set_trace()
        check_coeffs = False
      coeffs_count += 1
else:
  def log_coeffs(b):
    pass

if check_output:
  ref_output_log=None
  output_count = 0
  def log_output(b,phase,nplanes):
    global output_count, check_output, ref_output_log, dut_output_log
    if ref_output_log is None:
      ref_output_log=open(path_to_ref+'ref_output2.txt','rb')
      dut_output_log=open(path_to_ref+'dut_output.txt','wb')
    I = b.video_stream.frame_data.rframe
    pitch = b.video_stream.frame_data.stride

    w = b.video_stream.frame_data.upscaled_width
    h = b.video_stream.frame_data.upscaled_height

    for plane in range(nplanes):
      #if output_count>=1343:
      #  pdb.set_trace()
      print >>dut_output_log,output_count,plane,phase
      a2=ref_output_log.readline().rstrip()
      ssx = b.global_data.plane_subsampling_x[plane]
      ssy = b.global_data.plane_subsampling_y[plane]
      planeW = (w + ssx) >> ssx
      planeH = (h + ssy) >> ssy

      for y in range(planeH):
        s = ' '.join('%.2x'%I[plane][y*pitch+x] for x in range(planeW))
        print >>dut_output_log,s
        s2 = ref_output_log.readline().rstrip()
        if s2!=s and check_output:
          dut_output_log.flush()
          print 'good',a2
          print 'good output',s2
          print 'bad ',output_count,plane,phase,y,pitch
          print 'bad output ',s
          s3 = ""
          for i in range(max(len(s), len(s2))):
            if i >= min(len(s), len(s2)) or s2[i] != s[i]:
              s3 += "*"
            else:
              s3 += s[i]
          print 'diff       ', s3
          pdb.set_trace()
          check_output = False
    output_count += 1
else:
  def log_output(b,phase,nplanes):
    pass

if check_anchor_ref:
  ref_anchor_ref_log = None
  anchor_ref_count = 0
  def log_anchor_ref(b):
    global ref_anchor_ref_log, dut_anchor_ref_log, anchor_ref_count
    if ref_anchor_ref_log is None:
      ref_anchor_ref_log=open(path_to_ref+'ref_anchor_ref2.txt','rb')
      dut_anchor_ref_log=open(path_to_ref+'dut_anchor_ref.txt','wb')

    pitch = b.global_data.ref_width[0]
    w = b.video_stream.frame_data.upscaled_width
    h = b.video_stream.frame_data.upscaled_height
    for plane in range(3):
      ssx = b.global_data.plane_subsampling_x[plane]
      ssy = b.global_data.plane_subsampling_y[plane]
      planeW = (w + ssx) >> ssx
      planeH = (h + ssy) >> ssy

      s = "-- %d p%d w%d h%d"%(anchor_ref_count, plane, planeW, planeH)
      print >>dut_anchor_ref_log,s

      s = s.rstrip()
      s2 = ref_anchor_ref_log.readline().rstrip()
      if s2 != s:
        dut_anchor_ref_log.flush()
        print "Ref anchor_ref marker bad"
        print "s: ", s
        print "s2: ", s2
        pdb.set_trace()

      for y in range(planeH):
        s = ' '.join('%.2x'%b.global_data.rframestore[b.video_stream.frame_data.active_ref_idx[0]][plane][y*pitch+x] for x in range(planeW))
        print >>dut_anchor_ref_log,s

        s = s.rstrip()
        s2 = ref_anchor_ref_log.readline().rstrip()
        if s2 != s:
          dut_anchor_ref_log.flush()
          print "Ref anchor_ref mismatch"
          print "s: ", s
          print "s2: ", s2
          pdb.set_trace()

    anchor_ref_count += 1
else:
  def log_anchor_ref(b):
    pass

if check_tile_output:
  ref_tile_output_log = None
  tile_output_count = 0
  def log_tile_output(b, decTileRow, decTileCol):
    global ref_tile_output_log, dut_tile_output_log, tile_output_count
    if ref_tile_output_log is None:
      ref_tile_output_log=open(path_to_ref+'ref_tile_output2.txt','rb')
      dut_tile_output_log=open(path_to_ref+'dut_tile_output.txt','wb')

    pitch = b.video_stream.frame_data.stride;
    w = b.video_stream.frame_data.tile_width * 4
    h = b.video_stream.frame_data.tile_height * 4
    for plane in range(3):
      ssx = b.global_data.plane_subsampling_x[plane]
      ssy = b.global_data.plane_subsampling_y[plane]
      planeW = (w + ssx) >> ssx
      planeH = (h + ssy) >> ssy
      srcX = (decTileCol * w) >> ssx
      srcY = (decTileRow * h) >> ssy

      s = "-- %d p%d w%d h%d"%(tile_output_count, plane, planeW, planeH)
      print >>dut_tile_output_log,s

      s = s.rstrip()
      s2 = ref_tile_output_log.readline().rstrip()
      if s2 != s:
        dut_tile_output_log.flush()
        print "Ref tile_output marker bad"
        print "s: ", s
        print "s2: ", s2
        pdb.set_trace()

      for y in range(planeH):
        s = ' '.join('%.2x'%b.video_stream.frame_data.rframe[plane][(srcY+y)*pitch+(srcX+x)] for x in range(planeW))
        print >>dut_tile_output_log,s

        s = s.rstrip()
        s2 = ref_tile_output_log.readline().rstrip()
        if s2 != s:
          dut_tile_output_log.flush()
          print "Ref tile_output mismatch"
          print "s: ", s
          print "s2: ", s2
          pdb.set_trace()

      tile_output_count += 1
else:
  def log_tile_output(b, decTileRow, decTileCol):
    pass

val_fid = open('validate_decode_python.txt','wb')
val_count = 0
def validate(x):
  global val_count
  print >>val_fid,val_count,x
  if val_count==-722:
    print 'critical'
    pdb.set_trace()
  val_count+=1

def ASSERT(cond,msg):
  """Check that cond is True, print msg otherwise"""
  if cond:
    return
  print msg
  assert False

def CHECK(cond,msg, *args):
  """Check that cond is True, print msg otherwise"""
  if cond:
    return
  print "Check Failed: {}".format(msg % tuple(args))
  pdb.set_trace()

def CSV(id, val):
  pass

if 1:
  write_yuv_bits = 0
  def video_output(b,bitdepthy,bitdepthc, w, h,crop_left,crop_right,crop_top,crop_bottom,pitch,srcX,srcY,subw,subh,is_monochrome):
    global write_yuv_bits
    print "Output frame",w,h
    # TODO for show bit we need to output a framestore, using different pitch
    #assert crop_left==crop_right==crop_top==crop_bottom==0
    if outputBppPerFrame or write_yuv_bits==0:
      write_yuv_bits = bitdepthy
    if outputLST2Byte and b.global_data.large_scale_tile: # aomedia:1999
      write_yuv_bits = 16
      def transform(x):
        return x
    else:
      upshift=bitdepthy<write_yuv_bits
      if (upshift):
        shift=write_yuv_bits-bitdepthy
        offset=1<<(shift-1)
        def transform(x):
          return (x<<shift)+offset
      else:
        shift=bitdepthy-write_yuv_bits
        def transform(x):
          return x>>shift

    subw=subw-1
    subh=subh-1
    I=b.video_stream.frame_data.rframe
    w2=w-crop_left-crop_right
    h2=h-crop_top-crop_bottom
    baseY=(srcY+crop_top)*(pitch)+srcX+crop_left
    baseC=(((srcY+subh)>>subh)+(crop_top>>1))*(pitch)+((srcX+subw)>>subw)+(crop_left>>1)

    if write_yuv_bits>8:
      for y in xrange(h2):
        out_python.write(''.join(struct.pack('<H',transform(I[0][baseY+y*pitch+x])) for x in xrange(w2)))
      if not is_monochrome:
        for c in [1,2]:
          for y in xrange((h2+subh)>>subh):
            out_python.write(''.join(struct.pack('<H',transform(I[c][baseC+y*pitch+x])) for x in xrange((w2+subw)>>subw)))
    else:
      for y in xrange(h2):
        out_python.write(''.join(chr(transform(I[0][baseY+y*pitch+x])) for x in xrange(w2)))
      if not is_monochrome:
        for c in [1,2]:
          for y in xrange((h2+subh)>>subh):
            out_python.write(''.join(chr(transform(I[c][baseC+y*pitch+x])) for x in xrange((w2+subw)>>subw)))
    out_python.flush()

v='error'
startcode=('\0\0\1')

parser = OptionParser()
parser.add_option("-i","--input",dest="filename",default='../data/out.ivf',help="bitstream file to decode")
parser.add_option("-o","--output",dest="outFilename",default='out_python.yuv',help="YUV file to output")
parser.add_option("--not-annexb",dest="useAnnexB",action="store_const",default=1,const=0,help="use annex-b format")
parser.add_option("--oppoint",dest="oppoint",type="int",default=0,help="which operating point to use")
parser.add_option("--large-scale-tile", dest="largeScaleTile",type="int",nargs=2,help="<num Anchor Frames> <num Tile List OBUs>: provide large scale tile parameters")
parser.add_option("--no-film-grain",dest="overrideNoFilmGrain",action="store_const",default=0,const=1,help="Don't apply the film grain noise process")
(options,args) = parser.parse_args()
out_python=open(options.outFilename,'wb')

isObu = options.filename.endswith(".obu")

numLargeScaleTileAnchorFrames = 0
numLSTTileListObus = 0
if options.largeScaleTile:
  numLargeScaleTileAnchorFrames = options.largeScaleTile[0]
  numLSTTileListObus = options.largeScaleTile[1]

global_data=hevc_parse_global_data(None, options.useAnnexB, isObu, options.oppoint,
                                   numLargeScaleTileAnchorFrames, numLSTTileListObus,
                                   options.overrideNoFilmGrain)

with open(options.filename,'rb') as fd:
    data=fd.read()

file_length = len(data)
raw_bytes = [ord(a) for a in data]
global_data.input_length = len(raw_bytes) # Don't include the extra 0 byte we're about to add
raw_bytes.append(0) # Add a zero at the end so that current_position does not stop early
b = Bitstream(raw_bytes,logging=True)
b.global_data = global_data
b.frame_contexts = checked_dict(8) # NUM_FRAME_CONTEXTS
# b.numbytes()
frame = 0

if isObu:
  while b.current_position() < file_length:
    bytes_remaining = file_length - b.current_position()
    hevc_parse_decode_temporal_unit(b,bytes_remaining)
    frame += 1
else:
  #hevc_parse_obu_file_init(b)
  hevc_parse_ivf_file_header(b)
  while frame < b.global_data.frame_cnt:
    frame_hdr = hevc_parse_ivf_frame_header(b,0)
    hevc_parse_decode_temporal_unit(b,frame_hdr.frame_sz)
    frame += 1
