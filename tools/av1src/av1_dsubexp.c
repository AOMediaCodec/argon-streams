/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

// Compute number of bits needed to represent val
uint32 get_unsigned_bits(uint32 val) {
  if (val == 0)
    return 0
  cat = 0
  while (val > 0) {
    cat++
    val = val>>1
  }
  return cat
}

#if ENCODE
uint32 write_uvlc(uint32 value) {
  ASSERT(value <= (convert_to_int64(1)<<32) - 2, "uvlc() can only encode values up to 2^32 - 2 (inclusive)")

  target_mag = get_msb(value + 1)
  low_bit_mask = (1 << target_mag) - 1
  target_low_bits = (value + 1) & low_bit_mask

  uvlc_mag = 0
  while (1) {
    uvlc_done = (uvlc_mag == target_mag)
    uvlc_done u(1)
    if (!uvlc_done) {
      uvlc_mag += 1
    } else {
      break
    }
  }

  uvlc_low_bits = target_low_bits
  uvlc_low_bits u(uvlc_mag)

  uint32 res
  res = ((1 << uvlc_mag) - 1) + uvlc_low_bits
  ASSERT(res == value, "UVLC encode/decode mismatch")
  return res
}
#else // ENCODE
uint32 read_uvlc() {
  // Read a variable-length code consisting of
  // N zero bits, followed by a 1 bit, then N raw bits.
  // This appears to be the same as an exp-Golomb code:
  // https://en.wikipedia.org/wiki/Exponential-Golomb_coding
  uvlc_mag = 0
  while (1) {
    uvlc_done u(1)
    if (!uvlc_done) {
      uvlc_mag += 1
    } else {
      break
    }
  }

  ASSERT(uvlc_mag <= 31, "uvlc() can only encode values up to 2^32 - 2 (inclusive)")

  uvlc_low_bits u(uvlc_mag)
  return ((1 << uvlc_mag) - 1) + uvlc_low_bits
}
#endif // ENCODE

#if ENCODE
// Work out the smallest number of bytes required to represent
// a particular value in ULEB128 format.
uleb128_min_bytes(uint64 value) {
  maxBits = 7 * LEB128_MAX_SIZE
  ASSERT(value < (convert_to_uint64(1) << maxBits), "ULEB128 value out of range")

  // We need one byte  for values [  0,  2^7),
  //         two bytes for values [2^7, 2^14),
  //         and so on.
  // This works out as:
  if (value == 0) {
    return 1
  } else {
    return (get_msb(value) / 7) + 1
  }
}

// Note: The LEB128 format is non-unique, ie. multiple byte sequences can decode
// to the same value.
// For example, the following are all valid encodings of the decimal value 200:
// 0xc8 0x01
// 0xc8 0x81 0x00
// 0xc8 0x80 0x80 0x00
// and so on (adding an extra 0x80 byte each time after this).
//
// This is useful for us, as it means we can pre-allocate a fixed number of bytes
// for each OBU size, and we can "pad out" short values to fill that fixed length.
// The fixed size is passed here as 'bytes_to_fill', while 'start_pos' is the bit offset
// within the stream where we need to write the OBU size out.
store_uleb128(uint64 value, bytes_to_fill) {
  uint64 limit
  uint8 byte
  CHECK(value <= 0xffffffff, "libaom bug-1891 limits normative behaviour to UINT32_MAX?")
  CHECK(bytes_to_fill <= LEB128_MAX_SIZE, "Too many bytes pre-allocated for LEB128 value")

  // We can encode up to 7 bits of data per allocated byte
  if (bytes_to_fill == -1) { // automatically determine the minimum number of bytes required
    minBytes = uleb128_min_bytes(value)
    bytes_to_fill = Choose(minBytes, LEB128_MAX_SIZE)
  }
  limit = convert_to_int64(1) << (7 * bytes_to_fill)
  CHECK(value < limit, "Not enough bytes pre-allocated for LEB128 value")

  for (i = 0; i < bytes_to_fill; i++) {
    byte = (value >> (7*i)) & 0x7f
    if (i < bytes_to_fill-1) {
      byte |= 0x80
    }
    byte u(8)
  }
}

overwrite_uleb128(uint64 value, uint64 start_pos, bytes_to_fill) {
  uint64 limit
  uint8 byte
  CHECK(value <= 0xffffffff, "libaom bug-1891 limits normative behaviour to UINT32_MAX?")
  CHECK(bytes_to_fill <= LEB128_MAX_SIZE, "Too many bytes pre-allocated for LEB128 value")

  // We can encode up to 7 bits of data per allocated byte
  limit = convert_to_int64(1) << (7 * bytes_to_fill)
  CHECK(value < limit, "Not enough bytes pre-allocated for LEB128 value")

  for (i = 0; i < bytes_to_fill; i++) {
    byte = (value >> (7*i)) & 0x7f
    if (i < bytes_to_fill-1) {
      byte |= 0x80
    }
    unused = overwrite(start_pos+8*i,8,byte)
  }
}
#else // ENCODE
uint64 decode_uleb128() {
  uint64 value
  uint64 uleb128_byte
  value = 0
  for (i = 0; i < LEB128_MAX_SIZE; i++) {
    uleb128_byte u(8)
    value |= ((uleb128_byte & 0x7f) << (i*7))
    if (!(uleb128_byte & 0x80)) {
      CHECK(value <= 0xffffffff, "libaom bug-1891 limits normative behaviour to UINT32_MAX?")
      if (value > 0xffffffff) {
        return -1
      }
      uleb_length = i // Store to a global variable
      return value
    }
  }
  ASSERT(0, "Invalid LEB128 encoding - must use at most 8 bytes")
  return -1
}
#endif // ENCODE

// Quasi-uniform code over [0, n)
#if ENCODE
uint32 encode_quniform_ec(n, v) {
  l = get_unsigned_bits(n)
  m = (1 << l) - n
  if (!l)
    return 0

  if (v < m) {
    baseBits = v
    baseBits ae(v)
  } else {
    baseBits = (v + m) >> 1
    extraBit = (v + m) & 1
    baseBits ae(v)
    extraBit ae(v)
  }
  return v
}

uint32 encode_quniform_bits(n, v) {
  l = get_unsigned_bits(n)
  m = (1 << l) - n
  if (!l)
    return 0

  if (v < m) {
    baseBits = v
    baseBits u(l - 1)
  } else {
    baseBits = (v + m) >> 1
    extraBit = (v + m) & 1
    baseBits u(l - 1)
    extraBit u(1)
  }
  return v
}

uint32 decode_quniform_ec(n) {
  if (n == 0)
    return 0
  return encode_quniform_ec(n, Choose(0, n-1))
}

uint32 decode_quniform_bits(n) {
  if (n == 0)
    return 0
  return encode_quniform_bits(n, Choose(0, n-1))
}
#else // ENCODE
uint32 decode_quniform_ec(n) {
  l = get_unsigned_bits(n)
  m = (1 << l) - n
  if (!l)
    return 0

  baseBits ae(v)
  if (baseBits < m) {
    return baseBits
  } else {
    extraBit ae(v)
    return (baseBits << 1) - m + extraBit
  }
}

uint32 decode_quniform_bits(n) {
  l = get_unsigned_bits(n)
  m = (1 << l) - n
  if (!l)
    return 0

  baseBits u(l - 1)
  if (baseBits < m) {
    return baseBits
  } else {
    extraBit u(1)
    return (baseBits << 1) - m + extraBit
  }
}
#endif // ENCODE

// Finite subexponential code in [0, n), parametrized by k
#if ENCODE
uint32 encode_subexp_ec(n, k, v) {
  i = 0
  mk = 0
  while (1) {
    b2 = (i ? k + i - 1 : k)
    a = (1 << b2)
    if (n <= mk + 3 * a) {
      encode_quniform_ec(n - mk, v - mk)
      return v
    } else {
      subexpMoreBits = (v >= mk + a)
      subexpMoreBits ae(v)
      if (subexpMoreBits) {
        i = i + 1
        mk += a
      } else {
        subexpBits = v - mk
        subexpBits ae(v)
        return v
      }
    }
  }
}

uint32 encode_subexp_bits(n, k, v) {
  i = 0
  mk = 0
  while (1) {
    b2 = (i ? k + i - 1 : k)
    a = (1 << b2)
    if (n <= mk + 3 * a) {
      encode_quniform_bits(n - mk, v - mk)
      return v
    } else {
      subexpMoreBits = (v >= mk + a)
      subexpMoreBits u(1)
      if (subexpMoreBits) {
        i = i + 1
        mk += a
      } else {
        subexpBits = v - mk
        subexpBits u(b2)
        return v
      }
    }
  }
}

uint32 decode_subexp_ec(n, k) {
  if (n == 0)
    return 0
  return encode_subexp_ec(n, k, Choose(0, n-1))
}

uint32 decode_subexp_bits(n, k) {
  if (n == 0)
    return 0
  return encode_subexp_bits(n, k, Choose(0, n-1))
}
#else // ENCODE
uint32 decode_subexp_ec(n, k) {
  i=0
  mk=0
  while (1) {
    b2 = i ? k + i - 1 : k
    a = 1 << b2
    if (n <= mk + 3 * a) {
     return decode_quniform_ec(n - mk) + mk
    } else {
      subexpMoreBits ae(v)
      if (subexpMoreBits) {
        i++
        mk += a
      } else {
        subexpBits ae(v)
        return subexpBits + mk
      }
    }
  }
}

uint32 decode_subexp_bits(n, k) {
  i=0
  mk=0
  while (1) {
    b2 = i ? k + i - 1 : k
    a = 1 << b2
    if (n <= mk + 3 * a) {
     return decode_quniform_bits(n - mk) + mk
    } else {
      subexpMoreBits u(1)
      if (subexpMoreBits) {
        i++
        mk += a
      } else {
        subexpBits u(b2)
        return subexpBits + mk
      }
    }
  }
}
#endif // ENCODE

// Finite subexponential code in [0, n) (*_refsubexp_*) or [min, max) (*_range_refsubexp_*),
// using a parameter k and a reference value r
#if ENCODE
int recenter_nonneg(int r, int v) {
  if (v > (r << 1))
    return v
  else if (v >= r)
    return ((v - r) << 1)
  else
    return ((r - v) << 1) - 1
}

uint16 recenter_finite_nonneg(n, r, v) {
  if ((r << 1) <= n) {
    return recenter_nonneg(r, v)
  } else {
    return recenter_nonneg(n - 1 - r, n - 1 - v)
  }
}

uint16 encode_refsubexp_ec(n, k, r, v) {
  return encode_subexp_ec(n, k, recenter_finite_nonneg(n, r, v))
}

int16 encode_range_refsubexp_ec(min, max, k, ref, v) {
  return encode_refsubexp_ec(max - min, k, ref - min, v - min)
}

uint16 encode_refsubexp_bits(n, k, r, v) {
  return encode_subexp_bits(n, k, recenter_finite_nonneg(n, r, v))
}

int16 encode_range_refsubexp_bits(min, max, k, ref, v) {
  return encode_refsubexp_bits(max - min, k, ref - min, v - min)
}
#endif // ENCODE

int inv_recenter_nonneg(int r, int v) {
  if (v > 2 * r)
    return v
  return (v&1) ? r - ((v + 1) >> 1) : r + (v >> 1)
}

uint16 inv_recenter_finite_nonneg(n, r, v) {
  if ((r << 1) <= n) {
    return inv_recenter_nonneg(r, v)
  } else {
    return n - 1 - inv_recenter_nonneg(n - 1 - r, v)
  }
}

uint16 decode_refsubexp_ec(n, k, ref) {
  return inv_recenter_finite_nonneg(n, ref, decode_subexp_ec(n, k))
}

int16 decode_range_refsubexp_ec(min, max, k, ref) {
  return min + decode_refsubexp_ec(max - min, k, ref - min)
}

uint16 decode_refsubexp_bits(n, k, ref) {
  return inv_recenter_finite_nonneg(n, ref, decode_subexp_bits(n, k))
}

int16 decode_range_refsubexp_bits(min, max, k, ref) {
  return min + decode_refsubexp_bits(max - min, k, ref - min)
}
