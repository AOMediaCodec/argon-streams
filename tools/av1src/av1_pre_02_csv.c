/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define CSV_NEWCVS -1
#define CSV_NEWSTREAM -2
#define MAX_CSV_VALUES 3

enum csv_op_t {
  // Exactly one value allowed before reset. Setting with the same value has no
  // effect. Setting with a different value causes an error. Resets to -1.
  CSV_OP_EXACT,

  // Sum all values given since reset. Resets to 0.
  CSV_OP_SUM,

  // Take the maximum over all values given since reset. Resets to -1.
  CSV_OP_MAX,

  CSV_NUM_OPS
};

struct csv_field_t {
    unsigned    dimension;
    csv_op_t    binop;
    const char *header [MAX_CSV_VALUES];
};

enum {
  CSV_CVS_NUMBER,
  CSV_PROFILE,
  CSV_PICTURE_SIZE,
  CSV_SHOWFRAMECOUNT,
  CSV_FRAMECOUNT,
  CSV_KEYFRAMECOUNT,
  CSV_INTRAONLYCOUNT,
  CSV_INTERCOUNT,
  CSV_SHOWEXISTINGCOUNT,
  CSV_TILES,
  CSV_TILECOLS,
  CSV_TILEROWS,
  CSV_TILEGROUPS,
  CSV_FRAMEBITS,
  CSV_BITDEPTH,
  CSV_FILMGRAIN_NOISE,
  CSV_MONOCHROME,
  CSV_SCALABILITY,
  CSV_LAYER_INFO,
  CSV_LEVEL,
  //Insert new fields here
  CSV_FIELD_COUNT
  //Don't insert anything here!
};

const struct csv_field_t csv_fields [CSV_FIELD_COUNT] = {
  { 1, CSV_OP_SUM,
    { "CVS number", NULL, NULL} },
  { 3, CSV_OP_EXACT,
    { "Profile", "Subsampling X", "Subsampling Y" } },
  { 2, CSV_OP_MAX,
    { "Max picture width", "Max picture height", NULL } },
  { 1, CSV_OP_SUM,
    { "Number of shown frames", NULL, NULL } },
  { 1, CSV_OP_SUM,
    { "Total number of frames", NULL, NULL } },
  { 1, CSV_OP_SUM,
    { "Keyframe count", NULL, NULL } },
  { 1, CSV_OP_SUM,
    { "Intra only frame count", NULL, NULL } },
  { 1, CSV_OP_SUM,
    { "Inter frame count", NULL, NULL } },
  { 1, CSV_OP_SUM,
    { "Show existing frame count", NULL, NULL } },
  { 1, CSV_OP_MAX,
    { "Max tiles", NULL, NULL } },
  { 1, CSV_OP_MAX,
    { "Max tile columns", NULL, NULL } },
  { 1, CSV_OP_MAX,
    { "Max tile rows", NULL, NULL } },
  { 1, CSV_OP_MAX,
    { "Max tile groups", NULL, NULL } },
  { 1, CSV_OP_MAX,
    { "Max bits per frame", NULL, NULL } },
  { 1, CSV_OP_EXACT,
    { "Bit depth", NULL, NULL } },
  { 1, CSV_OP_SUM,
    { "Film grain noise frame count", NULL, NULL } },
  { 1, CSV_OP_EXACT,
    { "Monochrome", NULL, NULL } },
  { 1, CSV_OP_EXACT,
    { "Uses scalability?", NULL, NULL } },
  { 3, CSV_OP_EXACT,
    { "Number of spatial layers", "Number of temporal layers", "Number of operating points"} },
  { 2, CSV_OP_EXACT,
    { "Major level of oppoint 0", "Minor level of oppoint 0", NULL } }
};

void CSV(int id, ...);
