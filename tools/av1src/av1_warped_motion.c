/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_WARP 1
#else
#define VALIDATE_SPEC_WARP 0
#endif

// Warped-motion-only functions
// Validate the output of findSamples() and find_projection() ?
// Note: The encoder calls find_projection() more than the decoder, so we *expect* mismatches in the
// verification file between the encoder and decoder if this is enabled. But we expect the C and
// Python encoders to agree with each other, and similarly for the decoders.
#define VALIDATE_MODEL_SETUP 0

add_sample(x, y, mv_x, mv_y) {
  // Only use the first few samples we find
  // Note: We might throw away some samples due to them being invalid.
  // But we limit the total number scanned, rather than the number stored.
  if (wm_num_samples_scanned >= LEAST_SQUARES_SAMPLES_MAX)
    return 0
  wm_num_samples_scanned += 1

  // Compare samples to a threshold.
  // This is done inline, rather than after-the-fact as in libaom.
  // As a result, the samples may be ordered differently, but they
  // should be otherwise equivalent
  bw = block_size_wide_lookup[sb_size]
  bh = block_size_high_lookup[sb_size]
  threshold = clamp(Max(bw, bh), 16, 112)

  mvDiffRow = Abs(mv_y - mv[0][0])
  mvDiffCol = Abs(mv_x - mv[0][1])
  valid = (mvDiffRow + mvDiffCol <= threshold)

  if (!valid) {
    if (wm_num_samples_scanned == 1) {
      // Even if the first sample is invalid, we need to save it for use as
      // a "fallback", in case there are *no* valid samples.
      // If the first sample is valid, it will be recorded into the main sample
      // array as normal. But, if it is invalid,  we store it into a special
      // "fallback" variable. This is then copied over into the main
      // array at the end of findSamples(), if we need it.
      wm_fallback_sample_from[0] = x*8
      wm_fallback_sample_from[1] = y*8
      wm_fallback_sample_to[0] = x*8 + mv_x
      wm_fallback_sample_to[1] = y*8 + mv_y
    }
    return 0
  }

#if VALIDATE_SPEC_WARP
  //  validate(55005)
  //  validate(x)
  //  validate(y)
  //  validate(mv_x)
  //  validate(mv_y)
#endif

#if VALIDATE_MODEL_SETUP
  validate(x*8)
  validate(y*8)
  validate(mv_x)
  validate(mv_y)
#endif
  wm_samples_from[wm_num_samples][0] = x*8
  wm_samples_from[wm_num_samples][1] = y*8
  wm_samples_to[wm_num_samples][0] = x*8 + mv_x
  wm_samples_to[wm_num_samples][1] = y*8 + mv_y
  wm_num_samples += 1
}

// Hack: Work around the fact that the Argon Streams compiler tries to optimize
//   xyz = 0
//   [some other code, which may modify xyz via other functions]
//   return xyz
// to:
//   xyz = 0
//   [some other code, which may modify xyz via other functions]
//   return 0
wm_reset() {
  wm_num_samples = 0
  wm_num_samples_scanned = 0
}

// Corresponds to findSamples + selectSamples from the reference code
findSamples(mi_row, mi_col, partition) {
  wm_reset()

  up_available = aU
  left_available = aL
  mi_step = 0

  this_mi_wide = mi_size_wide[sb_size]
  this_mi_high = mi_size_high[sb_size]

  do_tl = 1
  do_tr = 1

#if VALIDATE_MODEL_SETUP
  validate(3000)
  validate(mi_row)
  validate(mi_col)
#endif

  // Scan the above row
  if (up_available) {
    src_mi_row = mi_row - 1
    src_mi_col = mi_col
    src_size = sb_sizes[src_mi_col][src_mi_row]
    src_mi_wide = mi_size_wide[src_size]

    // Handle "current block width <= above block width" case.
    if (this_mi_wide <= src_mi_wide) {
      col_offset = -(mi_col & (src_mi_wide - 1))

      if (col_offset < 0) do_tl = 0
      if (col_offset + src_mi_wide > this_mi_wide) do_tr = 0

      if (refframes[0][src_mi_col][src_mi_row][0] == ref_frame[0] &&
          refframes[0][src_mi_col][src_mi_row][1] == NONE) {
        src_center_x = (mi_col + col_offset) * MI_SIZE + Max(block_size_wide_lookup[src_size], MI_SIZE) / 2 - 1
        src_center_y = mi_row * MI_SIZE - Max(block_size_high_lookup[src_size], MI_SIZE) / 2 - 1
        mi_x = mvs[0][src_mi_col][src_mi_row][0][3][1]
        mi_y = mvs[0][src_mi_col][src_mi_row][0][3][0]
#if VALIDATE_MODEL_SETUP
        validate(3001)
#endif
        add_sample(src_center_x, src_center_y, mi_x, mi_y)
      }
    } else {
      // Handle "current block width > above block width" case.
      for (i = 0; i < Min(this_mi_wide, mi_cols - mi_col); i += mi_step) {
        src_mi_col = mi_col + i
        src_size = sb_sizes[src_mi_col][src_mi_row]
        src_mi_wide = mi_size_wide[src_size]
        src_mi_high = mi_size_high[src_size]
        mi_step = Min(this_mi_wide, src_mi_wide)

        if (refframes[0][src_mi_col][src_mi_row][0] == ref_frame[0] &&
            refframes[0][src_mi_col][src_mi_row][1] == NONE) {
          src_center_x = (mi_col + i) * MI_SIZE + Max(block_size_wide_lookup[src_size], MI_SIZE) / 2 - 1
          src_center_y = mi_row * MI_SIZE - Max(block_size_high_lookup[src_size], MI_SIZE) / 2 - 1
          mi_x = mvs[0][src_mi_col][src_mi_row][0][3][1]
          mi_y = mvs[0][src_mi_col][src_mi_row][0][3][0]
#if VALIDATE_MODEL_SETUP
          validate(3002)
#endif
          add_sample(src_center_x, src_center_y, mi_x, mi_y)
        }
      }
    }
  }

  // scan the left column
  if (left_available) {
    src_mi_col = mi_col - 1
    src_mi_row = mi_row
    src_size = sb_sizes[src_mi_col][src_mi_row]
    src_mi_high = mi_size_high[src_size]

    // Handle "current block height <= above block height" case.
    if (this_mi_high <= src_mi_high) {
      row_offset = -(mi_row & (src_mi_high-1))

      if (row_offset < 0) do_tl = 0

      if (refframes[0][src_mi_col][src_mi_row][0] == ref_frame[0] &&
          refframes[0][src_mi_col][src_mi_row][1] == NONE) {
        src_center_x = mi_col * MI_SIZE - Max(block_size_wide_lookup[src_size], MI_SIZE) / 2 - 1
        src_center_y = (mi_row + row_offset) * MI_SIZE + Max(block_size_high_lookup[src_size], MI_SIZE) / 2 - 1
        mi_x = mvs[0][src_mi_col][src_mi_row][0][3][1]
        mi_y = mvs[0][src_mi_col][src_mi_row][0][3][0]
#if VALIDATE_MODEL_SETUP
        validate(3003)
#endif
        add_sample(src_center_x, src_center_y, mi_x, mi_y)
      }
    } else {
      // Handle "current block height > above block height" case.
      for (i = 0; i < Min(this_mi_high, mi_rows - mi_row); i += mi_step) {
        src_mi_row = mi_row + i
        src_size = sb_sizes[src_mi_col][src_mi_row]
        src_mi_wide = mi_size_wide[src_size]
        src_mi_high = mi_size_high[src_size]
        mi_step = Min(this_mi_high, src_mi_high)

        if (refframes[0][src_mi_col][src_mi_row][0] == ref_frame[0] &&
            refframes[0][src_mi_col][src_mi_row][1] == NONE) {
          src_center_x = mi_col * MI_SIZE - Max(block_size_wide_lookup[src_size], MI_SIZE) / 2 - 1
          src_center_y = (mi_row + i) * MI_SIZE + Max(block_size_high_lookup[src_size], MI_SIZE) / 2 - 1
          mi_x = mvs[0][src_mi_col][src_mi_row][0][3][1]
          mi_y = mvs[0][src_mi_col][src_mi_row][0][3][0]
#if VALIDATE_MODEL_SETUP
          validate(3004)
#endif
          add_sample(src_center_x, src_center_y, mi_x, mi_y)
        }
      }
    }
  }

  // Top-left block
  if (do_tl && left_available && up_available) {
    src_mi_row = mi_row - 1
    src_mi_col = mi_col - 1
    src_size = sb_sizes[src_mi_col][src_mi_row]

    if (refframes[0][src_mi_col][src_mi_row][0] == ref_frame[0] &&
        refframes[0][src_mi_col][src_mi_row][1] == NONE) {
      src_center_x = mi_col * MI_SIZE - Max(block_size_wide_lookup[src_size], MI_SIZE) / 2 - 1
      src_center_y = mi_row * MI_SIZE - Max(block_size_high_lookup[src_size], MI_SIZE) / 2 - 1
      mi_x = mvs[0][src_mi_col][src_mi_row][0][3][1]
      mi_y = mvs[0][src_mi_col][src_mi_row][0][3][0]
#if VALIDATE_MODEL_SETUP
      validate(3005)
#endif
      add_sample(src_center_x, src_center_y, mi_x, mi_y)
    }
  }

  // Top-right block
  if (do_tr && has_top_right(mi_row, mi_col, this_mi_wide, this_mi_high, partition)) {
    src_mi_row = mi_row - 1
    src_mi_col = mi_col + this_mi_wide

    if (is_inside(src_mi_col, src_mi_row)) {
      // Don't fetch src_size unless (src_mi_row, src_mi_col) is actually in the frame.
      // This avoids a crash due to fetching from row -1
      src_size = sb_sizes[src_mi_col][src_mi_row]
      if (refframes[0][src_mi_col][src_mi_row][0] == ref_frame[0] &&
          refframes[0][src_mi_col][src_mi_row][1] == NONE) {
        src_center_x = src_mi_col * MI_SIZE + Max(block_size_wide_lookup[src_size], MI_SIZE) / 2 - 1
        src_center_y = mi_row * MI_SIZE - Max(block_size_high_lookup[src_size], MI_SIZE) / 2 - 1
        mi_x = mvs[0][src_mi_col][src_mi_row][0][3][1]
        mi_y = mvs[0][src_mi_col][src_mi_row][0][3][0]
  #if VALIDATE_MODEL_SETUP
        validate(3006)
  #endif
        add_sample(src_center_x, src_center_y, mi_x, mi_y)
      }
    }
  }

  if (wm_num_samples == 0 && wm_num_samples_scanned > 0) {
    // Use the fallback sample
    wm_samples_from[0][0] = wm_fallback_sample_from[0]
    wm_samples_from[0][1] = wm_fallback_sample_from[1]
    wm_samples_to[0][0] = wm_fallback_sample_to[0]
    wm_samples_to[0][1] = wm_fallback_sample_to[1]
    wm_num_samples = 1
  }
}




int32 get_mult_shift_ndiag(int64 v) {
  return Clip3(-WARPEDMODEL_NONDIAGAFFINE_CLAMP + 1,
                WARPEDMODEL_NONDIAGAFFINE_CLAMP - 1,
                ROUND_POWER_OF_TWO_SIGNED(v * div_factor, div_shift))
}

int32 get_mult_shift_diag(int64 v) {
  return Clip3((1 << WARPEDMODEL_PREC_BITS) - WARPEDMODEL_NONDIAGAFFINE_CLAMP + 1,
               (1 << WARPEDMODEL_PREC_BITS) + WARPEDMODEL_NONDIAGAFFINE_CLAMP - 1,
               ROUND_POWER_OF_TWO_SIGNED(v * div_factor, div_shift))
}

LS_SQUARE(x) {
  return ((x * x * 4) + (x * 4 * LS_STEP) + (LS_STEP * LS_STEP * 2)) >> (2 + LS_MAT_DOWN_BITS)
}

LS_PRODUCT1(x, y) {
  return ((x * y * 4) + ((x + y) * 2 * LS_STEP) + (LS_STEP * LS_STEP)) >> (2 + LS_MAT_DOWN_BITS)
}

LS_PRODUCT2(x, y) {
  return ((x * y * 4) + ((x + y) * 2 * LS_STEP) + (LS_STEP * LS_STEP * 2)) >> (2 + LS_MAT_DOWN_BITS)
}

// Note: In line with libaom's behaviour, this returns '1' if the generated model is *invalid*
// and 0 if the generated model is *valid.
find_projection(mi_row, mi_col) {
  int64 A[2][2]
  int64 Bx[2]
  int64 By[2]

#if VALIDATE_SPEC_WARP
  validate(55000)
#endif

#if VALIDATE_MODEL_SETUP
  validate(3100)
  validate(wm_num_samples)
  for (i = 0; i < wm_num_samples; i++) {
    validate(3105)
    validate(wm_samples_from[i][0])
    validate(wm_samples_from[i][1])
    validate(wm_samples_to[i][0])
    validate(wm_samples_to[i][1])
  }
#endif

  for (i = 0; i < 2; i++) {
    for (j = 0; j < 2; j++) {
      A[i][j] = 0
    }
    Bx[i] = 0
    By[i] = 0
  }

  bw = block_size_wide_lookup[sb_size]
  bh = block_size_high_lookup[sb_size]
  isuy = mi_row * MI_SIZE + Max(bh, MI_SIZE) / 2 - 1
  isux = mi_col * MI_SIZE + Max(bw, MI_SIZE) / 2 - 1
  suy = isuy * 8
  sux = isux * 8
  mvx = mv[0][1]
  mvy = mv[0][0]
  duy = suy + mvy
  dux = sux + mvx

  // Fill in A, Bx, By matrices
  CHECK(wm_num_samples <= LEAST_SQUARES_SAMPLES_MAX, "Too many initial samples")
  for (i = 0; i < wm_num_samples; i++) {
    dx = wm_samples_to[i][0] - dux
    dy = wm_samples_to[i][1] - duy
    sx = wm_samples_from[i][0] - sux
    sy = wm_samples_from[i][1] - suy
    if (Abs(sx - dx) < LS_MV_MAX && Abs(sy - dy) < LS_MV_MAX) {
      A[0][0] += LS_SQUARE(sx)
      A[0][1] += LS_PRODUCT1(sx, sy)
      A[1][1] += LS_SQUARE(sy)
      Bx[0] += LS_PRODUCT2(sx, dx)
      Bx[1] += LS_PRODUCT1(sy, dx)
      By[0] += LS_PRODUCT1(sx, dy)
      By[1] += LS_PRODUCT2(sy, dy)
    }
  }

#if VALIDATE_MODEL_SETUP
  validate(3201)
  validate(A[0][0])
  validate(A[0][1])
  validate(A[1][1])
  validate(Bx[0])
  validate(Bx[1])
  validate(By[0])
  validate(By[1])
#endif

  ASSERT(A[0][0] >= LS_MAT_MIN && A[0][0] <= LS_MAT_MAX, "Warped motion bug")
  ASSERT(A[0][1] >= LS_MAT_MIN && A[0][1] <= LS_MAT_MAX, "Warped motion bug")
  ASSERT(A[1][1] >= LS_MAT_MIN && A[1][1] <= LS_MAT_MAX, "Warped motion bug")
  ASSERT(Bx[0] >= LS_MAT_MIN && Bx[0] <= LS_MAT_MAX, "Warped motion bug")
  ASSERT(Bx[1] >= LS_MAT_MIN && Bx[1] <= LS_MAT_MAX, "Warped motion bug")
  ASSERT(By[0] >= LS_MAT_MIN && By[0] <= LS_MAT_MAX, "Warped motion bug")
  ASSERT(By[1] >= LS_MAT_MIN && By[1] <= LS_MAT_MAX, "Warped motion bug")

  // Calculate 1/det(A)
  int64 det // Determinant can be >2^31
  det = A[0][0] * A[1][1] - A[0][1] * A[0][1]
#if VALIDATE_SPEC_WARP
  validate(55001)
  validate(det)
#endif
// Some checks
  if (det == 0) {
    return 1
  }
  ASSERT(det >= 1968, "It has been proven that det should always >= 1968")
  resolve_divisor(det)
  div_shift -= WARPEDMODEL_PREC_BITS
  ASSERT(div_shift >= 1, "Unexpected value of div_shift in find_projection()")

  // Calculate A^-1 * Bx and A^-1 * By
  wm_params[2] = get_mult_shift_diag(  A[1][1] * Bx[0] - A[0][1] * Bx[1])
  wm_params[3] = get_mult_shift_ndiag(-A[0][1] * Bx[0] + A[0][0] * Bx[1])
  wm_params[4] = get_mult_shift_ndiag( A[1][1] * By[0] - A[0][1] * By[1])
  wm_params[5] = get_mult_shift_diag( -A[0][1] * By[0] + A[0][0] * By[1])

  // Calculate translational part of the model
  vx = mvx * (1 << (WARPEDMODEL_PREC_BITS - 3)) -
       (isux * (wm_params[2] - (1 << WARPEDMODEL_PREC_BITS)) + isuy * wm_params[3])
  vy = mvy * (1 << (WARPEDMODEL_PREC_BITS - 3)) -
       (isux * wm_params[4] + isuy * (wm_params[5] - (1 << WARPEDMODEL_PREC_BITS)))
  wm_params[0] = clamp(vx, -WARPEDMODEL_TRANS_CLAMP, WARPEDMODEL_TRANS_CLAMP - 1)
  wm_params[1] = clamp(vy, -WARPEDMODEL_TRANS_CLAMP, WARPEDMODEL_TRANS_CLAMP - 1)


#if VALIDATE_MODEL_SETUP
  validate(wm_params[0])
  validate(wm_params[1])
  validate(wm_params[2])
  validate(wm_params[3])
  validate(wm_params[4])
  validate(wm_params[5])
#endif

#if VALIDATE_SPEC_WARP
  validate(55004)
  for(i=0;i<6;i++)
    validate(wm_params[i])
#endif

  // Calculate auxiliary parameters and model validity
  v = setup_shear_params(wm_params, wm_aux)
#if VALIDATE_SPEC_WARP
  validate(v)
#endif
  return !v
}

