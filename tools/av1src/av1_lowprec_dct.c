/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_SPEC_TRANSFORM 0
#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_RECON 1
#else
#define VALIDATE_SPEC_RECON 0
#endif

// Highbitdepth uses lower precision DCT routines
//   The idea is to tune the amount of shifting to aoverflow
//
// The intermediate step buffers could be 32bit, except we want to be able to detect overflow
// (at least in the encoder)

// cospi[j] = (int)round(cos(M_PI*j/128) * (1<<INV_COS_BIT));
int32 cospi[64] = {
  4096, 4095, 4091, 4085, 4076, 4065, 4052, 4036, 4017, 3996, 3973,
  3948, 3920, 3889, 3857, 3822, 3784, 3745, 3703, 3659, 3612, 3564,
  3513, 3461, 3406, 3349, 3290, 3229, 3166, 3102, 3035, 2967, 2896,
  2824, 2751, 2675, 2598, 2520, 2440, 2359, 2276, 2191, 2106, 2019,
  1931, 1842, 1751, 1660, 1567, 1474, 1380, 1285, 1189, 1092, 995,
  897,  799,  700,  601,  501,  401,  301,  201,  101,
};

//  sinpi[j] = (int)round((sqrt(2) * sin(M_PI*j/9) * 2 / 3) * (1 << INV_COS_BIT))
int32 sinpi[5] = {
  0, 1321, 2482, 3344, 3803
};

round_shift_array(size, bit) {
  if (bit == 0) {
    return 0
  } else {
    if (bit > 0) {
      for (i = 0; i < size; i++) {
        output[i] = ROUND_POWER_OF_TWO(output[i], bit)
      }
    } else {
      for (i = 0; i < size; i++) {
        output[i] = output[i] << (-bit)
      }
    }
  }
}

clamp_array(int64pointer buf, size, bits) {
  int64 min
  int64 max
  min = -(convert_to_int64(1) << (bits-1))
  max = (convert_to_int64(1) << (bits-1)) - 1
  for (i = 0; i < size; i++) {
    buf[i] = clamp(buf[i], min, max)
  }
}

int32 half_btf(int64 w0, int64 in0, int64 w1, int64 in1, bit) {
  int64 result_64
  result_64 = w0 * in0 + w1 * in1
  // Don't check range here - we use range_check_buf() to check
  // that the output of this process is in the relevant range.
  // Note that, due to rounding, it's acceptable for 'result_64'
  // to be slightly outside the range of an int(bd+20).
  res = ROUND_POWER_OF_TWO(result_64, bit)
  return res
}

// The intermediate values within each transform should lie within certain
// ranges (differing per stage). We have two options for each stage:
// * If no valid stream should cause overflow, then we use range_check_buf/range_check_value,
//   which acts as an assertion (on the decoder side) / constraint (on the encoder side).
// * If a valid stream might cause overflow, then we use apply_range/apply_value,
//   which clamps to the appropriate range.
apply_value(int64 v, bit) {
  int64 high
  int64 low
  high = (convert_to_int64(1)<<(bit-1))-1
  low = -(convert_to_int64(1)<<(bit-1))
  return clamp(v, low, high)
}

apply_range(int64pointer buf, size, bit)
{
  clamp_array(buf, size, bit)
}

range_check_value(int64 v, bit) {
  int64 high
  int64 low
  high = (convert_to_int64(1)<<(bit-1))-1
  low = -(convert_to_int64(1)<<(bit-1))
  if (v < low || v > high) {
#if ENCODE
    if (enc_disable_recon)
      enc_seen_saturation = 1
    else
#endif
      CHECK(0, "Intermediate transform values out of range")
  }
  return v
}

range_check_buf(int64pointer buf, size, bit) {
  int64 high
  int64 low
  high = (convert_to_int64(1)<<(bit-1))-1
  low = -(convert_to_int64(1)<<(bit-1))
  for (i = 0; i < size; i++) {
    if (buf[i] < low || buf[i] > high) {
#if ENCODE
      if (enc_disable_recon)
        enc_seen_saturation = 1
      else
#endif
        CHECK(0, "Intermediate transform values out of range")
    }
  }
}

av1_iwht4(isCol) {
  shift = isCol ? 0 : 2

  a1 = input[0] >> shift
  c1 = input[1] >> shift
  d1 = input[2] >> shift
  b1 = input[3] >> shift
  a1 += c1
  d1 -= b1
  e1 = (a1 - d1) >> 1
  b1 = e1 - b1
  c1 = e1 - c1
  a1 -= b1
  d1 += c1
  output[0] = a1
  output[1] = b1
  output[2] = c1
  output[3] = d1

  if (isCol) {
    range_check_buf(output, 4, bit_depth + 1)
  }
}

av1_idct4_new(int8pointer stage_range, int64pointer bf0, int64pointer bf1) {
  int64 step[4]
  size = 4
  stage = 0

  // stage 0
  apply_range(input, size, stage_range[stage])

  // stage 1
  stage++
  bf1 = output
  bf1[0] = input[0]
  bf1[1] = input[2]
  bf1[2] = input[1]
  bf1[3] = input[3]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 2
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = half_btf(cospi[32], bf0[0], cospi[32], bf0[1], INV_COS_BIT)
  bf1[1] = half_btf(cospi[32], bf0[0], -cospi[32], bf0[1], INV_COS_BIT)
  bf1[2] = half_btf(cospi[48], bf0[2], -cospi[16], bf0[3], INV_COS_BIT)
  bf1[3] = half_btf(cospi[16], bf0[2], cospi[48], bf0[3], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 3
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[3], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[2], stage_range[stage])
  bf1[2] = apply_value(bf0[1] - bf0[2], stage_range[stage])
  bf1[3] = apply_value(bf0[0] - bf0[3], stage_range[stage])
  //apply_range(bf1, size, stage_range[stage])
}

av1_idct8_new(int8pointer stage_range, int64pointer bf0, int64pointer bf1) {
  size = 8


  stage = 0
  int64 step[8]

  // stage 0
  apply_range(input, size, stage_range[stage])

  // stage 1
  stage++
  bf1 = output
  bf1[0] = input[0]
  bf1[1] = input[4]
  bf1[2] = input[2]
  bf1[3] = input[6]
  bf1[4] = input[1]
  bf1[5] = input[5]
  bf1[6] = input[3]
  bf1[7] = input[7]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 2
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = half_btf(cospi[56], bf0[4], -cospi[8], bf0[7], INV_COS_BIT)
  bf1[5] = half_btf(cospi[24], bf0[5], -cospi[40], bf0[6], INV_COS_BIT)
  bf1[6] = half_btf(cospi[40], bf0[5], cospi[24], bf0[6], INV_COS_BIT)
  bf1[7] = half_btf(cospi[8], bf0[4], cospi[56], bf0[7], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 3
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = half_btf(cospi[32], bf0[0], cospi[32], bf0[1], INV_COS_BIT)
  bf1[1] = half_btf(cospi[32], bf0[0], -cospi[32], bf0[1], INV_COS_BIT)
  bf1[2] = half_btf(cospi[48], bf0[2], -cospi[16], bf0[3], INV_COS_BIT)
  bf1[3] = half_btf(cospi[16], bf0[2], cospi[48], bf0[3], INV_COS_BIT)
  bf1[4] = apply_value(bf0[4] + bf0[5], stage_range[stage])
  bf1[5] = apply_value(bf0[4] - bf0[5], stage_range[stage])
  bf1[6] = apply_value(-bf0[6] + bf0[7], stage_range[stage])
  bf1[7] = apply_value(bf0[6] + bf0[7], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 4
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = apply_value(bf0[0] + bf0[3], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[2], stage_range[stage])
  bf1[2] = apply_value(bf0[1] - bf0[2], stage_range[stage])
  bf1[3] = apply_value(bf0[0] - bf0[3], stage_range[stage])
  bf1[4] = bf0[4]
  bf1[5] = half_btf(-cospi[32], bf0[5], cospi[32], bf0[6], INV_COS_BIT)
  bf1[6] = half_btf(cospi[32], bf0[5], cospi[32], bf0[6], INV_COS_BIT)
  bf1[7] = bf0[7]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 5
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[7], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[6], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[5], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[4], stage_range[stage])
  bf1[4] = apply_value(bf0[3] - bf0[4], stage_range[stage])
  bf1[5] = apply_value(bf0[2] - bf0[5], stage_range[stage])
  bf1[6] = apply_value(bf0[1] - bf0[6], stage_range[stage])
  bf1[7] = apply_value(bf0[0] - bf0[7], stage_range[stage])
  //apply_range(bf1, size, stage_range[stage])
}

av1_idct16_new(int8pointer stage_range, int64pointer bf0, int64pointer bf1) {
  size = 16
  stage = 0
  int64 step[16]

  // stage 0
  apply_range(input, size, stage_range[stage])

  // stage 1
  stage++
  bf1 = output
  bf1[0] = input[0]
  bf1[1] = input[8]
  bf1[2] = input[4]
  bf1[3] = input[12]
  bf1[4] = input[2]
  bf1[5] = input[10]
  bf1[6] = input[6]
  bf1[7] = input[14]
  bf1[8] = input[1]
  bf1[9] = input[9]
  bf1[10] = input[5]
  bf1[11] = input[13]
  bf1[12] = input[3]
  bf1[13] = input[11]
  bf1[14] = input[7]
  bf1[15] = input[15]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 2
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = bf0[4]
  bf1[5] = bf0[5]
  bf1[6] = bf0[6]
  bf1[7] = bf0[7]
  bf1[8] = half_btf(cospi[60], bf0[8], -cospi[4], bf0[15], INV_COS_BIT)
  bf1[9] = half_btf(cospi[28], bf0[9], -cospi[36], bf0[14], INV_COS_BIT)
  bf1[10] = half_btf(cospi[44], bf0[10], -cospi[20], bf0[13], INV_COS_BIT)
  bf1[11] = half_btf(cospi[12], bf0[11], -cospi[52], bf0[12], INV_COS_BIT)
  bf1[12] = half_btf(cospi[52], bf0[11], cospi[12], bf0[12], INV_COS_BIT)
  bf1[13] = half_btf(cospi[20], bf0[10], cospi[44], bf0[13], INV_COS_BIT)
  bf1[14] = half_btf(cospi[36], bf0[9], cospi[28], bf0[14], INV_COS_BIT)
  bf1[15] = half_btf(cospi[4], bf0[8], cospi[60], bf0[15], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 3
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = half_btf(cospi[56], bf0[4], -cospi[8], bf0[7], INV_COS_BIT)
  bf1[5] = half_btf(cospi[24], bf0[5], -cospi[40], bf0[6], INV_COS_BIT)
  bf1[6] = half_btf(cospi[40], bf0[5], cospi[24], bf0[6], INV_COS_BIT)
  bf1[7] = half_btf(cospi[8], bf0[4], cospi[56], bf0[7], INV_COS_BIT)
  bf1[8] = apply_value(bf0[8] + bf0[9], stage_range[stage])
  bf1[9] = apply_value(bf0[8] - bf0[9], stage_range[stage])
  bf1[10] = apply_value(-bf0[10] + bf0[11], stage_range[stage])
  bf1[11] = apply_value(bf0[10] + bf0[11], stage_range[stage])
  bf1[12] = apply_value(bf0[12] + bf0[13], stage_range[stage])
  bf1[13] = apply_value(bf0[12] - bf0[13], stage_range[stage])
  bf1[14] = apply_value(-bf0[14] + bf0[15], stage_range[stage])
  bf1[15] = apply_value(bf0[14] + bf0[15], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 4
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = half_btf(cospi[32], bf0[0], cospi[32], bf0[1], INV_COS_BIT)
  bf1[1] = half_btf(cospi[32], bf0[0], -cospi[32], bf0[1], INV_COS_BIT)
  bf1[2] = half_btf(cospi[48], bf0[2], -cospi[16], bf0[3], INV_COS_BIT)
  bf1[3] = half_btf(cospi[16], bf0[2], cospi[48], bf0[3], INV_COS_BIT)
  bf1[4] = apply_value(bf0[4] + bf0[5], stage_range[stage])
  bf1[5] = apply_value(bf0[4] - bf0[5], stage_range[stage])
  bf1[6] = apply_value(-bf0[6] + bf0[7], stage_range[stage])
  bf1[7] = apply_value(bf0[6] + bf0[7], stage_range[stage])
  bf1[8] = bf0[8]
  bf1[9] = half_btf(-cospi[16], bf0[9], cospi[48], bf0[14], INV_COS_BIT)
  bf1[10] = half_btf(-cospi[48], bf0[10], -cospi[16], bf0[13], INV_COS_BIT)
  bf1[11] = bf0[11]
  bf1[12] = bf0[12]
  bf1[13] = half_btf(-cospi[16], bf0[10], cospi[48], bf0[13], INV_COS_BIT)
  bf1[14] = half_btf(cospi[48], bf0[9], cospi[16], bf0[14], INV_COS_BIT)
  bf1[15] = bf0[15]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 5
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[3], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[2], stage_range[stage])
  bf1[2] = apply_value(bf0[1] - bf0[2], stage_range[stage])
  bf1[3] = apply_value(bf0[0] - bf0[3], stage_range[stage])
  bf1[4] = bf0[4]
  bf1[5] = half_btf(-cospi[32], bf0[5], cospi[32], bf0[6], INV_COS_BIT)
  bf1[6] = half_btf(cospi[32], bf0[5], cospi[32], bf0[6], INV_COS_BIT)
  bf1[7] = bf0[7]
  bf1[8] = apply_value(bf0[8] + bf0[11], stage_range[stage])
  bf1[9] = apply_value(bf0[9] + bf0[10], stage_range[stage])
  bf1[10] = apply_value(bf0[9] - bf0[10], stage_range[stage])
  bf1[11] = apply_value(bf0[8] - bf0[11], stage_range[stage])
  bf1[12] = apply_value(-bf0[12] + bf0[15], stage_range[stage])
  bf1[13] = apply_value(-bf0[13] + bf0[14], stage_range[stage])
  bf1[14] = apply_value(bf0[13] + bf0[14], stage_range[stage])
  bf1[15] = apply_value(bf0[12] + bf0[15], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 6
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = apply_value(bf0[0] + bf0[7], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[6], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[5], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[4], stage_range[stage])
  bf1[4] = apply_value(bf0[3] - bf0[4], stage_range[stage])
  bf1[5] = apply_value(bf0[2] - bf0[5], stage_range[stage])
  bf1[6] = apply_value(bf0[1] - bf0[6], stage_range[stage])
  bf1[7] = apply_value(bf0[0] - bf0[7], stage_range[stage])
  bf1[8] = bf0[8]
  bf1[9] = bf0[9]
  bf1[10] = half_btf(-cospi[32], bf0[10], cospi[32], bf0[13], INV_COS_BIT)
  bf1[11] = half_btf(-cospi[32], bf0[11], cospi[32], bf0[12], INV_COS_BIT)
  bf1[12] = half_btf(cospi[32], bf0[11], cospi[32], bf0[12], INV_COS_BIT)
  bf1[13] = half_btf(cospi[32], bf0[10], cospi[32], bf0[13], INV_COS_BIT)
  bf1[14] = bf0[14]
  bf1[15] = bf0[15]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 7
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[15], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[14], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[13], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[12], stage_range[stage])
  bf1[4] = apply_value(bf0[4] + bf0[11], stage_range[stage])
  bf1[5] = apply_value(bf0[5] + bf0[10], stage_range[stage])
  bf1[6] = apply_value(bf0[6] + bf0[9], stage_range[stage])
  bf1[7] = apply_value(bf0[7] + bf0[8], stage_range[stage])
  bf1[8] = apply_value(bf0[7] - bf0[8], stage_range[stage])
  bf1[9] = apply_value(bf0[6] - bf0[9], stage_range[stage])
  bf1[10] = apply_value(bf0[5] - bf0[10], stage_range[stage])
  bf1[11] = apply_value(bf0[4] - bf0[11], stage_range[stage])
  bf1[12] = apply_value(bf0[3] - bf0[12], stage_range[stage])
  bf1[13] = apply_value(bf0[2] - bf0[13], stage_range[stage])
  bf1[14] = apply_value(bf0[1] - bf0[14], stage_range[stage])
  bf1[15] = apply_value(bf0[0] - bf0[15], stage_range[stage])
  //apply_range(bf1, size, stage_range[stage])
}

av1_idct32_new(int8pointer stage_range, int64pointer bf0, int64pointer bf1) {
  size = 32

  stage = 0
  int64 step[32]

  // stage 0
  apply_range(input, size, stage_range[stage])

  // stage 1
  stage++
  bf1 = output
  bf1[0] = input[0]
  bf1[1] = input[16]
  bf1[2] = input[8]
  bf1[3] = input[24]
  bf1[4] = input[4]
  bf1[5] = input[20]
  bf1[6] = input[12]
  bf1[7] = input[28]
  bf1[8] = input[2]
  bf1[9] = input[18]
  bf1[10] = input[10]
  bf1[11] = input[26]
  bf1[12] = input[6]
  bf1[13] = input[22]
  bf1[14] = input[14]
  bf1[15] = input[30]
  bf1[16] = input[1]
  bf1[17] = input[17]
  bf1[18] = input[9]
  bf1[19] = input[25]
  bf1[20] = input[5]
  bf1[21] = input[21]
  bf1[22] = input[13]
  bf1[23] = input[29]
  bf1[24] = input[3]
  bf1[25] = input[19]
  bf1[26] = input[11]
  bf1[27] = input[27]
  bf1[28] = input[7]
  bf1[29] = input[23]
  bf1[30] = input[15]
  bf1[31] = input[31]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 2
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = bf0[4]
  bf1[5] = bf0[5]
  bf1[6] = bf0[6]
  bf1[7] = bf0[7]
  bf1[8] = bf0[8]
  bf1[9] = bf0[9]
  bf1[10] = bf0[10]
  bf1[11] = bf0[11]
  bf1[12] = bf0[12]
  bf1[13] = bf0[13]
  bf1[14] = bf0[14]
  bf1[15] = bf0[15]
  bf1[16] = half_btf(cospi[62], bf0[16], -cospi[2], bf0[31], INV_COS_BIT)
  bf1[17] = half_btf(cospi[30], bf0[17], -cospi[34], bf0[30], INV_COS_BIT)
  bf1[18] = half_btf(cospi[46], bf0[18], -cospi[18], bf0[29], INV_COS_BIT)
  bf1[19] = half_btf(cospi[14], bf0[19], -cospi[50], bf0[28], INV_COS_BIT)
  bf1[20] = half_btf(cospi[54], bf0[20], -cospi[10], bf0[27], INV_COS_BIT)
  bf1[21] = half_btf(cospi[22], bf0[21], -cospi[42], bf0[26], INV_COS_BIT)
  bf1[22] = half_btf(cospi[38], bf0[22], -cospi[26], bf0[25], INV_COS_BIT)
  bf1[23] = half_btf(cospi[6], bf0[23], -cospi[58], bf0[24], INV_COS_BIT)
  bf1[24] = half_btf(cospi[58], bf0[23], cospi[6], bf0[24], INV_COS_BIT)
  bf1[25] = half_btf(cospi[26], bf0[22], cospi[38], bf0[25], INV_COS_BIT)
  bf1[26] = half_btf(cospi[42], bf0[21], cospi[22], bf0[26], INV_COS_BIT)
  bf1[27] = half_btf(cospi[10], bf0[20], cospi[54], bf0[27], INV_COS_BIT)
  bf1[28] = half_btf(cospi[50], bf0[19], cospi[14], bf0[28], INV_COS_BIT)
  bf1[29] = half_btf(cospi[18], bf0[18], cospi[46], bf0[29], INV_COS_BIT)
  bf1[30] = half_btf(cospi[34], bf0[17], cospi[30], bf0[30], INV_COS_BIT)
  bf1[31] = half_btf(cospi[2], bf0[16], cospi[62], bf0[31], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 3
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = bf0[4]
  bf1[5] = bf0[5]
  bf1[6] = bf0[6]
  bf1[7] = bf0[7]
  bf1[8] = half_btf(cospi[60], bf0[8], -cospi[4], bf0[15], INV_COS_BIT)
  bf1[9] = half_btf(cospi[28], bf0[9], -cospi[36], bf0[14], INV_COS_BIT)
  bf1[10] = half_btf(cospi[44], bf0[10], -cospi[20], bf0[13], INV_COS_BIT)
  bf1[11] = half_btf(cospi[12], bf0[11], -cospi[52], bf0[12], INV_COS_BIT)
  bf1[12] = half_btf(cospi[52], bf0[11], cospi[12], bf0[12], INV_COS_BIT)
  bf1[13] = half_btf(cospi[20], bf0[10], cospi[44], bf0[13], INV_COS_BIT)
  bf1[14] = half_btf(cospi[36], bf0[9], cospi[28], bf0[14], INV_COS_BIT)
  bf1[15] = half_btf(cospi[4], bf0[8], cospi[60], bf0[15], INV_COS_BIT)
  bf1[16] = apply_value(bf0[16] + bf0[17], stage_range[stage])
  bf1[17] = apply_value(bf0[16] - bf0[17], stage_range[stage])
  bf1[18] = apply_value(-bf0[18] + bf0[19], stage_range[stage])
  bf1[19] = apply_value(bf0[18] + bf0[19], stage_range[stage])
  bf1[20] = apply_value(bf0[20] + bf0[21], stage_range[stage])
  bf1[21] = apply_value(bf0[20] - bf0[21], stage_range[stage])
  bf1[22] = apply_value(-bf0[22] + bf0[23], stage_range[stage])
  bf1[23] = apply_value(bf0[22] + bf0[23], stage_range[stage])
  bf1[24] = apply_value(bf0[24] + bf0[25], stage_range[stage])
  bf1[25] = apply_value(bf0[24] - bf0[25], stage_range[stage])
  bf1[26] = apply_value(-bf0[26] + bf0[27], stage_range[stage])
  bf1[27] = apply_value(bf0[26] + bf0[27], stage_range[stage])
  bf1[28] = apply_value(bf0[28] + bf0[29], stage_range[stage])
  bf1[29] = apply_value(bf0[28] - bf0[29], stage_range[stage])
  bf1[30] = apply_value(-bf0[30] + bf0[31], stage_range[stage])
  bf1[31] = apply_value(bf0[30] + bf0[31], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 4
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = half_btf(cospi[56], bf0[4], -cospi[8], bf0[7], INV_COS_BIT)
  bf1[5] = half_btf(cospi[24], bf0[5], -cospi[40], bf0[6], INV_COS_BIT)
  bf1[6] = half_btf(cospi[40], bf0[5], cospi[24], bf0[6], INV_COS_BIT)
  bf1[7] = half_btf(cospi[8], bf0[4], cospi[56], bf0[7], INV_COS_BIT)
  bf1[8] = apply_value(bf0[8] + bf0[9], stage_range[stage])
  bf1[9] = apply_value(bf0[8] - bf0[9], stage_range[stage])
  bf1[10] = apply_value(-bf0[10] + bf0[11], stage_range[stage])
  bf1[11] = apply_value(bf0[10] + bf0[11], stage_range[stage])
  bf1[12] = apply_value(bf0[12] + bf0[13], stage_range[stage])
  bf1[13] = apply_value(bf0[12] - bf0[13], stage_range[stage])
  bf1[14] = apply_value(-bf0[14] + bf0[15], stage_range[stage])
  bf1[15] = apply_value(bf0[14] + bf0[15], stage_range[stage])
  bf1[16] = apply_value(bf0[16], stage_range[stage])
  bf1[17] = half_btf(-cospi[8], bf0[17], cospi[56], bf0[30], INV_COS_BIT)
  bf1[18] = half_btf(-cospi[56], bf0[18], -cospi[8], bf0[29], INV_COS_BIT)
  bf1[19] = bf0[19]
  bf1[20] = bf0[20]
  bf1[21] = half_btf(-cospi[40], bf0[21], cospi[24], bf0[26], INV_COS_BIT)
  bf1[22] = half_btf(-cospi[24], bf0[22], -cospi[40], bf0[25], INV_COS_BIT)
  bf1[23] = bf0[23]
  bf1[24] = bf0[24]
  bf1[25] = half_btf(-cospi[40], bf0[22], cospi[24], bf0[25], INV_COS_BIT)
  bf1[26] = half_btf(cospi[24], bf0[21], cospi[40], bf0[26], INV_COS_BIT)
  bf1[27] = bf0[27]
  bf1[28] = bf0[28]
  bf1[29] = half_btf(-cospi[8], bf0[18], cospi[56], bf0[29], INV_COS_BIT)
  bf1[30] = half_btf(cospi[56], bf0[17], cospi[8], bf0[30], INV_COS_BIT)
  bf1[31] = bf0[31]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 5
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = half_btf(cospi[32], bf0[0], cospi[32], bf0[1], INV_COS_BIT)
  bf1[1] = half_btf(cospi[32], bf0[0], -cospi[32], bf0[1], INV_COS_BIT)
  bf1[2] = half_btf(cospi[48], bf0[2], -cospi[16], bf0[3], INV_COS_BIT)
  bf1[3] = half_btf(cospi[16], bf0[2], cospi[48], bf0[3], INV_COS_BIT)
  bf1[4] = apply_value(bf0[4] + bf0[5], stage_range[stage])
  bf1[5] = apply_value(bf0[4] - bf0[5], stage_range[stage])
  bf1[6] = apply_value(-bf0[6] + bf0[7], stage_range[stage])
  bf1[7] = apply_value(bf0[6] + bf0[7], stage_range[stage])
  bf1[8] = bf0[8]
  bf1[9] = half_btf(-cospi[16], bf0[9], cospi[48], bf0[14], INV_COS_BIT)
  bf1[10] = half_btf(-cospi[48], bf0[10], -cospi[16], bf0[13], INV_COS_BIT)
  bf1[11] = bf0[11]
  bf1[12] = bf0[12]
  bf1[13] = half_btf(-cospi[16], bf0[10], cospi[48], bf0[13], INV_COS_BIT)
  bf1[14] = half_btf(cospi[48], bf0[9], cospi[16], bf0[14], INV_COS_BIT)
  bf1[15] = bf0[15]
  bf1[16] = apply_value(bf0[16] + bf0[19], stage_range[stage])
  bf1[17] = apply_value(bf0[17] + bf0[18], stage_range[stage])
  bf1[18] = apply_value(bf0[17] - bf0[18], stage_range[stage])
  bf1[19] = apply_value(bf0[16] - bf0[19], stage_range[stage])
  bf1[20] = apply_value(-bf0[20] + bf0[23], stage_range[stage])
  bf1[21] = apply_value(-bf0[21] + bf0[22], stage_range[stage])
  bf1[22] = apply_value(bf0[21] + bf0[22], stage_range[stage])
  bf1[23] = apply_value(bf0[20] + bf0[23], stage_range[stage])
  bf1[24] = apply_value(bf0[24] + bf0[27], stage_range[stage])
  bf1[25] = apply_value(bf0[25] + bf0[26], stage_range[stage])
  bf1[26] = apply_value(bf0[25] - bf0[26], stage_range[stage])
  bf1[27] = apply_value(bf0[24] - bf0[27], stage_range[stage])
  bf1[28] = apply_value(-bf0[28] + bf0[31], stage_range[stage])
  bf1[29] = apply_value(-bf0[29] + bf0[30], stage_range[stage])
  bf1[30] = apply_value(bf0[29] + bf0[30], stage_range[stage])
  bf1[31] = apply_value(bf0[28] + bf0[31], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 6
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = apply_value(bf0[0] + bf0[3], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[2], stage_range[stage])
  bf1[2] = apply_value(bf0[1] - bf0[2], stage_range[stage])
  bf1[3] = apply_value(bf0[0] - bf0[3], stage_range[stage])
  bf1[4] = bf0[4]
  bf1[5] = half_btf(-cospi[32], bf0[5], cospi[32], bf0[6], INV_COS_BIT)
  bf1[6] = half_btf(cospi[32], bf0[5], cospi[32], bf0[6], INV_COS_BIT)
  bf1[7] = bf0[7]
  bf1[8] = apply_value(bf0[8] + bf0[11], stage_range[stage])
  bf1[9] = apply_value(bf0[9] + bf0[10], stage_range[stage])
  bf1[10] = apply_value(bf0[9] - bf0[10], stage_range[stage])
  bf1[11] = apply_value(bf0[8] - bf0[11], stage_range[stage])
  bf1[12] = apply_value(-bf0[12] + bf0[15], stage_range[stage])
  bf1[13] = apply_value(-bf0[13] + bf0[14], stage_range[stage])
  bf1[14] = apply_value(bf0[13] + bf0[14], stage_range[stage])
  bf1[15] = apply_value(bf0[12] + bf0[15], stage_range[stage])
  bf1[16] = bf0[16]
  bf1[17] = bf0[17]
  bf1[18] = half_btf(-cospi[16], bf0[18], cospi[48], bf0[29], INV_COS_BIT)
  bf1[19] = half_btf(-cospi[16], bf0[19], cospi[48], bf0[28], INV_COS_BIT)
  bf1[20] = half_btf(-cospi[48], bf0[20], -cospi[16], bf0[27], INV_COS_BIT)
  bf1[21] = half_btf(-cospi[48], bf0[21], -cospi[16], bf0[26], INV_COS_BIT)
  bf1[22] = bf0[22]
  bf1[23] = bf0[23]
  bf1[24] = bf0[24]
  bf1[25] = bf0[25]
  bf1[26] = half_btf(-cospi[16], bf0[21], cospi[48], bf0[26], INV_COS_BIT)
  bf1[27] = half_btf(-cospi[16], bf0[20], cospi[48], bf0[27], INV_COS_BIT)
  bf1[28] = half_btf(cospi[48], bf0[19], cospi[16], bf0[28], INV_COS_BIT)
  bf1[29] = half_btf(cospi[48], bf0[18], cospi[16], bf0[29], INV_COS_BIT)
  bf1[30] = bf0[30]
  bf1[31] = bf0[31]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 7
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[7], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[6], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[5], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[4], stage_range[stage])
  bf1[4] = apply_value(bf0[3] - bf0[4], stage_range[stage])
  bf1[5] = apply_value(bf0[2] - bf0[5], stage_range[stage])
  bf1[6] = apply_value(bf0[1] - bf0[6], stage_range[stage])
  bf1[7] = apply_value(bf0[0] - bf0[7], stage_range[stage])
  bf1[8] = bf0[8]
  bf1[9] = bf0[9]
  bf1[10] = half_btf(-cospi[32], bf0[10], cospi[32], bf0[13], INV_COS_BIT)
  bf1[11] = half_btf(-cospi[32], bf0[11], cospi[32], bf0[12], INV_COS_BIT)
  bf1[12] = half_btf(cospi[32], bf0[11], cospi[32], bf0[12], INV_COS_BIT)
  bf1[13] = half_btf(cospi[32], bf0[10], cospi[32], bf0[13], INV_COS_BIT)
  bf1[14] = bf0[14]
  bf1[15] = bf0[15]
  bf1[16] = apply_value(bf0[16] + bf0[23], stage_range[stage])
  bf1[17] = apply_value(bf0[17] + bf0[22], stage_range[stage])
  bf1[18] = apply_value(bf0[18] + bf0[21], stage_range[stage])
  bf1[19] = apply_value(bf0[19] + bf0[20], stage_range[stage])
  bf1[20] = apply_value(bf0[19] - bf0[20], stage_range[stage])
  bf1[21] = apply_value(bf0[18] - bf0[21], stage_range[stage])
  bf1[22] = apply_value(bf0[17] - bf0[22], stage_range[stage])
  bf1[23] = apply_value(bf0[16] - bf0[23], stage_range[stage])
  bf1[24] = apply_value(-bf0[24] + bf0[31], stage_range[stage])
  bf1[25] = apply_value(-bf0[25] + bf0[30], stage_range[stage])
  bf1[26] = apply_value(-bf0[26] + bf0[29], stage_range[stage])
  bf1[27] = apply_value(-bf0[27] + bf0[28], stage_range[stage])
  bf1[28] = apply_value(bf0[27] + bf0[28], stage_range[stage])
  bf1[29] = apply_value(bf0[26] + bf0[29], stage_range[stage])
  bf1[30] = apply_value(bf0[25] + bf0[30], stage_range[stage])
  bf1[31] = apply_value(bf0[24] + bf0[31], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 8
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = apply_value(bf0[0] + bf0[15], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[14], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[13], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[12], stage_range[stage])
  bf1[4] = apply_value(bf0[4] + bf0[11], stage_range[stage])
  bf1[5] = apply_value(bf0[5] + bf0[10], stage_range[stage])
  bf1[6] = apply_value(bf0[6] + bf0[9], stage_range[stage])
  bf1[7] = apply_value(bf0[7] + bf0[8], stage_range[stage])
  bf1[8] = apply_value(bf0[7] - bf0[8], stage_range[stage])
  bf1[9] = apply_value(bf0[6] - bf0[9], stage_range[stage])
  bf1[10] = apply_value(bf0[5] - bf0[10], stage_range[stage])
  bf1[11] = apply_value(bf0[4] - bf0[11], stage_range[stage])
  bf1[12] = apply_value(bf0[3] - bf0[12], stage_range[stage])
  bf1[13] = apply_value(bf0[2] - bf0[13], stage_range[stage])
  bf1[14] = apply_value(bf0[1] - bf0[14], stage_range[stage])
  bf1[15] = apply_value(bf0[0] - bf0[15], stage_range[stage])
  bf1[16] = bf0[16]
  bf1[17] = bf0[17]
  bf1[18] = bf0[18]
  bf1[19] = bf0[19]
  bf1[20] = half_btf(-cospi[32], bf0[20], cospi[32], bf0[27], INV_COS_BIT)
  bf1[21] = half_btf(-cospi[32], bf0[21], cospi[32], bf0[26], INV_COS_BIT)
  bf1[22] = half_btf(-cospi[32], bf0[22], cospi[32], bf0[25], INV_COS_BIT)
  bf1[23] = half_btf(-cospi[32], bf0[23], cospi[32], bf0[24], INV_COS_BIT)
  bf1[24] = half_btf(cospi[32], bf0[23], cospi[32], bf0[24], INV_COS_BIT)
  bf1[25] = half_btf(cospi[32], bf0[22], cospi[32], bf0[25], INV_COS_BIT)
  bf1[26] = half_btf(cospi[32], bf0[21], cospi[32], bf0[26], INV_COS_BIT)
  bf1[27] = half_btf(cospi[32], bf0[20], cospi[32], bf0[27], INV_COS_BIT)
  bf1[28] = bf0[28]
  bf1[29] = bf0[29]
  bf1[30] = bf0[30]
  bf1[31] = bf0[31]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 9
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[31], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[30], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[29], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[28], stage_range[stage])
  bf1[4] = apply_value(bf0[4] + bf0[27], stage_range[stage])
  bf1[5] = apply_value(bf0[5] + bf0[26], stage_range[stage])
  bf1[6] = apply_value(bf0[6] + bf0[25], stage_range[stage])
  bf1[7] = apply_value(bf0[7] + bf0[24], stage_range[stage])
  bf1[8] = apply_value(bf0[8] + bf0[23], stage_range[stage])
  bf1[9] = apply_value(bf0[9] + bf0[22], stage_range[stage])
  bf1[10] = apply_value(bf0[10] + bf0[21], stage_range[stage])
  bf1[11] = apply_value(bf0[11] + bf0[20], stage_range[stage])
  bf1[12] = apply_value(bf0[12] + bf0[19], stage_range[stage])
  bf1[13] = apply_value(bf0[13] + bf0[18], stage_range[stage])
  bf1[14] = apply_value(bf0[14] + bf0[17], stage_range[stage])
  bf1[15] = apply_value(bf0[15] + bf0[16], stage_range[stage])
  bf1[16] = apply_value(bf0[15] - bf0[16], stage_range[stage])
  bf1[17] = apply_value(bf0[14] - bf0[17], stage_range[stage])
  bf1[18] = apply_value(bf0[13] - bf0[18], stage_range[stage])
  bf1[19] = apply_value(bf0[12] - bf0[19], stage_range[stage])
  bf1[20] = apply_value(bf0[11] - bf0[20], stage_range[stage])
  bf1[21] = apply_value(bf0[10] - bf0[21], stage_range[stage])
  bf1[22] = apply_value(bf0[9] - bf0[22], stage_range[stage])
  bf1[23] = apply_value(bf0[8] - bf0[23], stage_range[stage])
  bf1[24] = apply_value(bf0[7] - bf0[24], stage_range[stage])
  bf1[25] = apply_value(bf0[6] - bf0[25], stage_range[stage])
  bf1[26] = apply_value(bf0[5] - bf0[26], stage_range[stage])
  bf1[27] = apply_value(bf0[4] - bf0[27], stage_range[stage])
  bf1[28] = apply_value(bf0[3] - bf0[28], stage_range[stage])
  bf1[29] = apply_value(bf0[2] - bf0[29], stage_range[stage])
  bf1[30] = apply_value(bf0[1] - bf0[30], stage_range[stage])
  bf1[31] = apply_value(bf0[0] - bf0[31], stage_range[stage])
  //apply_range(bf1, size, stage_range[stage])
}

av1_iadst4_new(int8pointer stage_range, int64pointer bf0, int64pointer bf1) {
  int64 s0
  int64 s1
  int64 s2
  int64 s3
  int64 s4
  int64 s5
  int64 s6
  int64 s7
  int64 x0
  int64 x1
  int64 x2
  int64 x3

  size = 4
  stage = 0
  apply_range(input, size, stage_range[stage])

  x0 = input[0]
  x1 = input[1]
  x2 = input[2]
  x3 = input[3]

  if (!(x0 | x1 | x2 | x3)) {
    for(i=0;i<4;i++)
      output[i] = 0
    return 0
  }

  // Stage 1
  stage++
  s0 = range_check_value(sinpi[1] * x0, stage_range[stage] + INV_COS_BIT)
  s1 = range_check_value(sinpi[2] * x0, stage_range[stage] + INV_COS_BIT)
  s2 = range_check_value(sinpi[3] * x1, stage_range[stage] + INV_COS_BIT)
  s3 = range_check_value(sinpi[4] * x2, stage_range[stage] + INV_COS_BIT)
  s4 = range_check_value(sinpi[1] * x2, stage_range[stage] + INV_COS_BIT)
  s5 = range_check_value(sinpi[2] * x3, stage_range[stage] + INV_COS_BIT)
  s6 = range_check_value(sinpi[4] * x3, stage_range[stage] + INV_COS_BIT)

  // Stage 2
  // Note: The intermediate value x0-x2 is deliberately allowed an extra
  // bit of range, as per https://aomedia-review.googlesource.com/c/aom/+/49261
  stage++
  s7 = range_check_value(x0 - x2, stage_range[stage] + 1)
  s7 = range_check_value(s7 + x3, stage_range[stage])

  // Stage 3
  stage++
  x0 = range_check_value(s0 + s3, stage_range[stage] + INV_COS_BIT)
  x1 = range_check_value(s1 - s4, stage_range[stage] + INV_COS_BIT)
  x2 = range_check_value(sinpi[3] * s7, stage_range[stage] + INV_COS_BIT)
  x3 = range_check_value(s2, stage_range[stage] + INV_COS_BIT)

  // Stage 4
  stage++
  x0 = range_check_value(x0 + s5, stage_range[stage] + INV_COS_BIT)
  x1 = range_check_value(x1 - s6, stage_range[stage] + INV_COS_BIT)

  // Stage 5
  stage++
  s0 = range_check_value(x0 + x3, stage_range[stage] + INV_COS_BIT)
  s1 = range_check_value(x1 + x3, stage_range[stage] + INV_COS_BIT)
  s2 = range_check_value(x2, stage_range[stage] + INV_COS_BIT)
  s3 = range_check_value(x0 + x1, stage_range[stage] + INV_COS_BIT)

  // Stage 6
  stage++
  s3 = range_check_value(s3 - x3, stage_range[stage] + INV_COS_BIT)

  output[0] = ROUND_POWER_OF_TWO(s0, INV_COS_BIT)
  output[1] = ROUND_POWER_OF_TWO(s1, INV_COS_BIT)
  output[2] = ROUND_POWER_OF_TWO(s2, INV_COS_BIT)
  output[3] = ROUND_POWER_OF_TWO(s3, INV_COS_BIT)
}

av1_iadst8_new(int8pointer stage_range, int64pointer bf0, int64pointer bf1) {
  size = 8

  stage = 0
  int64 step[8]

  // stage 0
  apply_range(input, size, stage_range[stage])

  // stage 1
  stage++
  bf1 = output
  bf1[0] = input[7]
  bf1[1] = input[0]
  bf1[2] = input[5]
  bf1[3] = input[2]
  bf1[4] = input[3]
  bf1[5] = input[4]
  bf1[6] = input[1]
  bf1[7] = input[6]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 2
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = half_btf(cospi[4], bf0[0], cospi[60], bf0[1], INV_COS_BIT)
  bf1[1] = half_btf(cospi[60], bf0[0], -cospi[4], bf0[1], INV_COS_BIT)
  bf1[2] = half_btf(cospi[20], bf0[2], cospi[44], bf0[3], INV_COS_BIT)
  bf1[3] = half_btf(cospi[44], bf0[2], -cospi[20], bf0[3], INV_COS_BIT)
  bf1[4] = half_btf(cospi[36], bf0[4], cospi[28], bf0[5], INV_COS_BIT)
  bf1[5] = half_btf(cospi[28], bf0[4], -cospi[36], bf0[5], INV_COS_BIT)
  bf1[6] = half_btf(cospi[52], bf0[6], cospi[12], bf0[7], INV_COS_BIT)
  bf1[7] = half_btf(cospi[12], bf0[6], -cospi[52], bf0[7], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 3
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[4], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[5], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[6], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[7], stage_range[stage])
  bf1[4] = apply_value(-bf0[4] + bf0[0], stage_range[stage])
  bf1[5] = apply_value(-bf0[5] + bf0[1], stage_range[stage])
  bf1[6] = apply_value(-bf0[6] + bf0[2], stage_range[stage])
  bf1[7] = apply_value(-bf0[7] + bf0[3], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 4
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = half_btf(cospi[16], bf0[4], cospi[48], bf0[5], INV_COS_BIT)
  bf1[5] = half_btf(cospi[48], bf0[4], -cospi[16], bf0[5], INV_COS_BIT)
  bf1[6] = half_btf(-cospi[48], bf0[6], cospi[16], bf0[7], INV_COS_BIT)
  bf1[7] = half_btf(cospi[16], bf0[6], cospi[48], bf0[7], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 5
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[2], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[3], stage_range[stage])
  bf1[2] = apply_value(-bf0[2] + bf0[0], stage_range[stage])
  bf1[3] = apply_value(-bf0[3] + bf0[1], stage_range[stage])
  bf1[4] = apply_value(bf0[4] + bf0[6], stage_range[stage])
  bf1[5] = apply_value(bf0[5] + bf0[7], stage_range[stage])
  bf1[6] = apply_value(-bf0[6] + bf0[4], stage_range[stage])
  bf1[7] = apply_value(-bf0[7] + bf0[5], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 6
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = half_btf(cospi[32], bf0[2], cospi[32], bf0[3], INV_COS_BIT)
  bf1[3] = half_btf(cospi[32], bf0[2], -cospi[32], bf0[3], INV_COS_BIT)
  bf1[4] = bf0[4]
  bf1[5] = bf0[5]
  bf1[6] = half_btf(cospi[32], bf0[6], cospi[32], bf0[7], INV_COS_BIT)
  bf1[7] = half_btf(cospi[32], bf0[6], -cospi[32], bf0[7], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 7
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = bf0[0]
  bf1[1] = -bf0[4]
  bf1[2] = bf0[6]
  bf1[3] = -bf0[2]
  bf1[4] = bf0[3]
  bf1[5] = -bf0[7]
  bf1[6] = bf0[5]
  bf1[7] = -bf0[1]
  //apply_range(bf1, size, stage_range[stage])
}

av1_iadst16_new(int8pointer stage_range, int64pointer bf0, int64pointer bf1) {
  size = 16

  stage = 0
  int64 step[16]

  // stage 0
  apply_range(input, size, stage_range[stage])

  // stage 1
  stage++
  bf1 = output
  bf1[0] = input[15]
  bf1[1] = input[0]
  bf1[2] = input[13]
  bf1[3] = input[2]
  bf1[4] = input[11]
  bf1[5] = input[4]
  bf1[6] = input[9]
  bf1[7] = input[6]
  bf1[8] = input[7]
  bf1[9] = input[8]
  bf1[10] = input[5]
  bf1[11] = input[10]
  bf1[12] = input[3]
  bf1[13] = input[12]
  bf1[14] = input[1]
  bf1[15] = input[14]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 2
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = half_btf(cospi[2], bf0[0], cospi[62], bf0[1], INV_COS_BIT)
  bf1[1] = half_btf(cospi[62], bf0[0], -cospi[2], bf0[1], INV_COS_BIT)
  bf1[2] = half_btf(cospi[10], bf0[2], cospi[54], bf0[3], INV_COS_BIT)
  bf1[3] = half_btf(cospi[54], bf0[2], -cospi[10], bf0[3], INV_COS_BIT)
  bf1[4] = half_btf(cospi[18], bf0[4], cospi[46], bf0[5], INV_COS_BIT)
  bf1[5] = half_btf(cospi[46], bf0[4], -cospi[18], bf0[5], INV_COS_BIT)
  bf1[6] = half_btf(cospi[26], bf0[6], cospi[38], bf0[7], INV_COS_BIT)
  bf1[7] = half_btf(cospi[38], bf0[6], -cospi[26], bf0[7], INV_COS_BIT)
  bf1[8] = half_btf(cospi[34], bf0[8], cospi[30], bf0[9], INV_COS_BIT)
  bf1[9] = half_btf(cospi[30], bf0[8], -cospi[34], bf0[9], INV_COS_BIT)
  bf1[10] = half_btf(cospi[42], bf0[10], cospi[22], bf0[11], INV_COS_BIT)
  bf1[11] = half_btf(cospi[22], bf0[10], -cospi[42], bf0[11], INV_COS_BIT)
  bf1[12] = half_btf(cospi[50], bf0[12], cospi[14], bf0[13], INV_COS_BIT)
  bf1[13] = half_btf(cospi[14], bf0[12], -cospi[50], bf0[13], INV_COS_BIT)
  bf1[14] = half_btf(cospi[58], bf0[14], cospi[6], bf0[15], INV_COS_BIT)
  bf1[15] = half_btf(cospi[6], bf0[14], -cospi[58], bf0[15], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 3
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[8], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[9], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[10], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[11], stage_range[stage])
  bf1[4] = apply_value(bf0[4] + bf0[12], stage_range[stage])
  bf1[5] = apply_value(bf0[5] + bf0[13], stage_range[stage])
  bf1[6] = apply_value(bf0[6] + bf0[14], stage_range[stage])
  bf1[7] = apply_value(bf0[7] + bf0[15], stage_range[stage])
  bf1[8] = apply_value(bf0[0] - bf0[8], stage_range[stage])
  bf1[9] = apply_value(bf0[1] - bf0[9], stage_range[stage])
  bf1[10] = apply_value(bf0[2] - bf0[10], stage_range[stage])
  bf1[11] = apply_value(bf0[3] - bf0[11], stage_range[stage])
  bf1[12] = apply_value(bf0[4] - bf0[12], stage_range[stage])
  bf1[13] = apply_value(bf0[5] - bf0[13], stage_range[stage])
  bf1[14] = apply_value(bf0[6] - bf0[14], stage_range[stage])
  bf1[15] = apply_value(bf0[7] - bf0[15], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 4
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = bf0[4]
  bf1[5] = bf0[5]
  bf1[6] = bf0[6]
  bf1[7] = bf0[7]
  bf1[8] = half_btf(cospi[8], bf0[8], cospi[56], bf0[9], INV_COS_BIT)
  bf1[9] = half_btf(cospi[56], bf0[8], -cospi[8], bf0[9], INV_COS_BIT)
  bf1[10] = half_btf(cospi[40], bf0[10], cospi[24], bf0[11], INV_COS_BIT)
  bf1[11] = half_btf(cospi[24], bf0[10], -cospi[40], bf0[11], INV_COS_BIT)
  bf1[12] = half_btf(-cospi[56], bf0[12], cospi[8], bf0[13], INV_COS_BIT)
  bf1[13] = half_btf(cospi[8], bf0[12], cospi[56], bf0[13], INV_COS_BIT)
  bf1[14] = half_btf(-cospi[24], bf0[14], cospi[40], bf0[15], INV_COS_BIT)
  bf1[15] = half_btf(cospi[40], bf0[14], cospi[24], bf0[15], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 5
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[4], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[5], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[6], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[7], stage_range[stage])
  bf1[4] = apply_value(bf0[0] - bf0[4], stage_range[stage])
  bf1[5] = apply_value(bf0[1] - bf0[5], stage_range[stage])
  bf1[6] = apply_value(bf0[2] - bf0[6], stage_range[stage])
  bf1[7] = apply_value(bf0[3] - bf0[7], stage_range[stage])
  bf1[8] = apply_value(bf0[8] + bf0[12], stage_range[stage])
  bf1[9] = apply_value(bf0[9] + bf0[13], stage_range[stage])
  bf1[10] = apply_value(bf0[10] + bf0[14], stage_range[stage])
  bf1[11] = apply_value(bf0[11] + bf0[15], stage_range[stage])
  bf1[12] = apply_value(bf0[8] - bf0[12], stage_range[stage])
  bf1[13] = apply_value(bf0[9] - bf0[13], stage_range[stage])
  bf1[14] = apply_value(bf0[10] - bf0[14], stage_range[stage])
  bf1[15] = apply_value(bf0[11] - bf0[15], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 6
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = half_btf(cospi[16], bf0[4], cospi[48], bf0[5], INV_COS_BIT)
  bf1[5] = half_btf(cospi[48], bf0[4], -cospi[16], bf0[5], INV_COS_BIT)
  bf1[6] = half_btf(-cospi[48], bf0[6], cospi[16], bf0[7], INV_COS_BIT)
  bf1[7] = half_btf(cospi[16], bf0[6], cospi[48], bf0[7], INV_COS_BIT)
  bf1[8] = bf0[8]
  bf1[9] = bf0[9]
  bf1[10] = bf0[10]
  bf1[11] = bf0[11]
  bf1[12] = half_btf(cospi[16], bf0[12], cospi[48], bf0[13], INV_COS_BIT)
  bf1[13] = half_btf(cospi[48], bf0[12], -cospi[16], bf0[13], INV_COS_BIT)
  bf1[14] = half_btf(-cospi[48], bf0[14], cospi[16], bf0[15], INV_COS_BIT)
  bf1[15] = half_btf(cospi[16], bf0[14], cospi[48], bf0[15], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 7
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[2], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[3], stage_range[stage])
  bf1[2] = apply_value(bf0[0] - bf0[2], stage_range[stage])
  bf1[3] = apply_value(bf0[1] - bf0[3], stage_range[stage])
  bf1[4] = apply_value(bf0[4] + bf0[6], stage_range[stage])
  bf1[5] = apply_value(bf0[5] + bf0[7], stage_range[stage])
  bf1[6] = apply_value(bf0[4] - bf0[6], stage_range[stage])
  bf1[7] = apply_value(bf0[5] - bf0[7], stage_range[stage])
  bf1[8] = apply_value(bf0[8] + bf0[10], stage_range[stage])
  bf1[9] = apply_value(bf0[9] + bf0[11], stage_range[stage])
  bf1[10] = apply_value(bf0[8] - bf0[10], stage_range[stage])
  bf1[11] = apply_value(bf0[9] - bf0[11], stage_range[stage])
  bf1[12] = apply_value(bf0[12] + bf0[14], stage_range[stage])
  bf1[13] = apply_value(bf0[13] + bf0[15], stage_range[stage])
  bf1[14] = apply_value(bf0[12] - bf0[14], stage_range[stage])
  bf1[15] = apply_value(bf0[13] - bf0[15], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 8
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = half_btf(cospi[32], bf0[2], cospi[32], bf0[3], INV_COS_BIT)
  bf1[3] = half_btf(cospi[32], bf0[2], -cospi[32], bf0[3], INV_COS_BIT)
  bf1[4] = bf0[4]
  bf1[5] = bf0[5]
  bf1[6] = half_btf(cospi[32], bf0[6], cospi[32], bf0[7], INV_COS_BIT)
  bf1[7] = half_btf(cospi[32], bf0[6], -cospi[32], bf0[7], INV_COS_BIT)
  bf1[8] = bf0[8]
  bf1[9] = bf0[9]
  bf1[10] = half_btf(cospi[32], bf0[10], cospi[32], bf0[11], INV_COS_BIT)
  bf1[11] = half_btf(cospi[32], bf0[10], -cospi[32], bf0[11], INV_COS_BIT)
  bf1[12] = bf0[12]
  bf1[13] = bf0[13]
  bf1[14] = half_btf(cospi[32], bf0[14], cospi[32], bf0[15], INV_COS_BIT)
  bf1[15] = half_btf(cospi[32], bf0[14], -cospi[32], bf0[15], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 9
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = bf0[0]
  bf1[1] = -bf0[8]
  bf1[2] = bf0[12]
  bf1[3] = -bf0[4]
  bf1[4] = bf0[6]
  bf1[5] = -bf0[14]
  bf1[6] = bf0[10]
  bf1[7] = -bf0[2]
  bf1[8] = bf0[3]
  bf1[9] = -bf0[11]
  bf1[10] = bf0[15]
  bf1[11] = -bf0[7]
  bf1[12] = bf0[5]
  bf1[13] = -bf0[13]
  bf1[14] = bf0[9]
  bf1[15] = -bf0[1]
  //apply_range(bf1, size, stage_range[stage])
}

av1_idct64_new(int8pointer stage_range, int64pointer bf0, int64pointer bf1) {
  size = 64

  stage = 0
  int64 step[64]

  // stage 0
  apply_range(input, size, stage_range[stage])

  // stage 1
  stage++
  bf1 = output
  bf1[0] = input[0]
  bf1[1] = input[32]
  bf1[2] = input[16]
  bf1[3] = input[48]
  bf1[4] = input[8]
  bf1[5] = input[40]
  bf1[6] = input[24]
  bf1[7] = input[56]
  bf1[8] = input[4]
  bf1[9] = input[36]
  bf1[10] = input[20]
  bf1[11] = input[52]
  bf1[12] = input[12]
  bf1[13] = input[44]
  bf1[14] = input[28]
  bf1[15] = input[60]
  bf1[16] = input[2]
  bf1[17] = input[34]
  bf1[18] = input[18]
  bf1[19] = input[50]
  bf1[20] = input[10]
  bf1[21] = input[42]
  bf1[22] = input[26]
  bf1[23] = input[58]
  bf1[24] = input[6]
  bf1[25] = input[38]
  bf1[26] = input[22]
  bf1[27] = input[54]
  bf1[28] = input[14]
  bf1[29] = input[46]
  bf1[30] = input[30]
  bf1[31] = input[62]
  bf1[32] = input[1]
  bf1[33] = input[33]
  bf1[34] = input[17]
  bf1[35] = input[49]
  bf1[36] = input[9]
  bf1[37] = input[41]
  bf1[38] = input[25]
  bf1[39] = input[57]
  bf1[40] = input[5]
  bf1[41] = input[37]
  bf1[42] = input[21]
  bf1[43] = input[53]
  bf1[44] = input[13]
  bf1[45] = input[45]
  bf1[46] = input[29]
  bf1[47] = input[61]
  bf1[48] = input[3]
  bf1[49] = input[35]
  bf1[50] = input[19]
  bf1[51] = input[51]
  bf1[52] = input[11]
  bf1[53] = input[43]
  bf1[54] = input[27]
  bf1[55] = input[59]
  bf1[56] = input[7]
  bf1[57] = input[39]
  bf1[58] = input[23]
  bf1[59] = input[55]
  bf1[60] = input[15]
  bf1[61] = input[47]
  bf1[62] = input[31]
  bf1[63] = input[63]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 2
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = bf0[4]
  bf1[5] = bf0[5]
  bf1[6] = bf0[6]
  bf1[7] = bf0[7]
  bf1[8] = bf0[8]
  bf1[9] = bf0[9]
  bf1[10] = bf0[10]
  bf1[11] = bf0[11]
  bf1[12] = bf0[12]
  bf1[13] = bf0[13]
  bf1[14] = bf0[14]
  bf1[15] = bf0[15]
  bf1[16] = bf0[16]
  bf1[17] = bf0[17]
  bf1[18] = bf0[18]
  bf1[19] = bf0[19]
  bf1[20] = bf0[20]
  bf1[21] = bf0[21]
  bf1[22] = bf0[22]
  bf1[23] = bf0[23]
  bf1[24] = bf0[24]
  bf1[25] = bf0[25]
  bf1[26] = bf0[26]
  bf1[27] = bf0[27]
  bf1[28] = bf0[28]
  bf1[29] = bf0[29]
  bf1[30] = bf0[30]
  bf1[31] = bf0[31]
  bf1[32] = half_btf(cospi[63], bf0[32], -cospi[1], bf0[63], INV_COS_BIT)
  bf1[33] = half_btf(cospi[31], bf0[33], -cospi[33], bf0[62], INV_COS_BIT)
  bf1[34] = half_btf(cospi[47], bf0[34], -cospi[17], bf0[61], INV_COS_BIT)
  bf1[35] = half_btf(cospi[15], bf0[35], -cospi[49], bf0[60], INV_COS_BIT)
  bf1[36] = half_btf(cospi[55], bf0[36], -cospi[9], bf0[59], INV_COS_BIT)
  bf1[37] = half_btf(cospi[23], bf0[37], -cospi[41], bf0[58], INV_COS_BIT)
  bf1[38] = half_btf(cospi[39], bf0[38], -cospi[25], bf0[57], INV_COS_BIT)
  bf1[39] = half_btf(cospi[7], bf0[39], -cospi[57], bf0[56], INV_COS_BIT)
  bf1[40] = half_btf(cospi[59], bf0[40], -cospi[5], bf0[55], INV_COS_BIT)
  bf1[41] = half_btf(cospi[27], bf0[41], -cospi[37], bf0[54], INV_COS_BIT)
  bf1[42] = half_btf(cospi[43], bf0[42], -cospi[21], bf0[53], INV_COS_BIT)
  bf1[43] = half_btf(cospi[11], bf0[43], -cospi[53], bf0[52], INV_COS_BIT)
  bf1[44] = half_btf(cospi[51], bf0[44], -cospi[13], bf0[51], INV_COS_BIT)
  bf1[45] = half_btf(cospi[19], bf0[45], -cospi[45], bf0[50], INV_COS_BIT)
  bf1[46] = half_btf(cospi[35], bf0[46], -cospi[29], bf0[49], INV_COS_BIT)
  bf1[47] = half_btf(cospi[3], bf0[47], -cospi[61], bf0[48], INV_COS_BIT)
  bf1[48] = half_btf(cospi[61], bf0[47], cospi[3], bf0[48], INV_COS_BIT)
  bf1[49] = half_btf(cospi[29], bf0[46], cospi[35], bf0[49], INV_COS_BIT)
  bf1[50] = half_btf(cospi[45], bf0[45], cospi[19], bf0[50], INV_COS_BIT)
  bf1[51] = half_btf(cospi[13], bf0[44], cospi[51], bf0[51], INV_COS_BIT)
  bf1[52] = half_btf(cospi[53], bf0[43], cospi[11], bf0[52], INV_COS_BIT)
  bf1[53] = half_btf(cospi[21], bf0[42], cospi[43], bf0[53], INV_COS_BIT)
  bf1[54] = half_btf(cospi[37], bf0[41], cospi[27], bf0[54], INV_COS_BIT)
  bf1[55] = half_btf(cospi[5], bf0[40], cospi[59], bf0[55], INV_COS_BIT)
  bf1[56] = half_btf(cospi[57], bf0[39], cospi[7], bf0[56], INV_COS_BIT)
  bf1[57] = half_btf(cospi[25], bf0[38], cospi[39], bf0[57], INV_COS_BIT)
  bf1[58] = half_btf(cospi[41], bf0[37], cospi[23], bf0[58], INV_COS_BIT)
  bf1[59] = half_btf(cospi[9], bf0[36], cospi[55], bf0[59], INV_COS_BIT)
  bf1[60] = half_btf(cospi[49], bf0[35], cospi[15], bf0[60], INV_COS_BIT)
  bf1[61] = half_btf(cospi[17], bf0[34], cospi[47], bf0[61], INV_COS_BIT)
  bf1[62] = half_btf(cospi[33], bf0[33], cospi[31], bf0[62], INV_COS_BIT)
  bf1[63] = half_btf(cospi[1], bf0[32], cospi[63], bf0[63], INV_COS_BIT)
  range_check_buf(bf1, size, stage_range[stage])

  // stage 3
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = bf0[4]
  bf1[5] = bf0[5]
  bf1[6] = bf0[6]
  bf1[7] = bf0[7]
  bf1[8] = bf0[8]
  bf1[9] = bf0[9]
  bf1[10] = bf0[10]
  bf1[11] = bf0[11]
  bf1[12] = bf0[12]
  bf1[13] = bf0[13]
  bf1[14] = bf0[14]
  bf1[15] = bf0[15]
  bf1[16] = half_btf(cospi[62], bf0[16], -cospi[2], bf0[31], INV_COS_BIT)
  bf1[17] = half_btf(cospi[30], bf0[17], -cospi[34], bf0[30], INV_COS_BIT)
  bf1[18] = half_btf(cospi[46], bf0[18], -cospi[18], bf0[29], INV_COS_BIT)
  bf1[19] = half_btf(cospi[14], bf0[19], -cospi[50], bf0[28], INV_COS_BIT)
  bf1[20] = half_btf(cospi[54], bf0[20], -cospi[10], bf0[27], INV_COS_BIT)
  bf1[21] = half_btf(cospi[22], bf0[21], -cospi[42], bf0[26], INV_COS_BIT)
  bf1[22] = half_btf(cospi[38], bf0[22], -cospi[26], bf0[25], INV_COS_BIT)
  bf1[23] = half_btf(cospi[6], bf0[23], -cospi[58], bf0[24], INV_COS_BIT)
  bf1[24] = half_btf(cospi[58], bf0[23], cospi[6], bf0[24], INV_COS_BIT)
  bf1[25] = half_btf(cospi[26], bf0[22], cospi[38], bf0[25], INV_COS_BIT)
  bf1[26] = half_btf(cospi[42], bf0[21], cospi[22], bf0[26], INV_COS_BIT)
  bf1[27] = half_btf(cospi[10], bf0[20], cospi[54], bf0[27], INV_COS_BIT)
  bf1[28] = half_btf(cospi[50], bf0[19], cospi[14], bf0[28], INV_COS_BIT)
  bf1[29] = half_btf(cospi[18], bf0[18], cospi[46], bf0[29], INV_COS_BIT)
  bf1[30] = half_btf(cospi[34], bf0[17], cospi[30], bf0[30], INV_COS_BIT)
  bf1[31] = half_btf(cospi[2], bf0[16], cospi[62], bf0[31], INV_COS_BIT)
  bf1[32] = apply_value(bf0[32] + bf0[33], stage_range[stage])
  bf1[33] = apply_value(bf0[32] - bf0[33], stage_range[stage])
  bf1[34] = apply_value(-bf0[34] + bf0[35], stage_range[stage])
  bf1[35] = apply_value(bf0[34] + bf0[35], stage_range[stage])
  bf1[36] = apply_value(bf0[36] + bf0[37], stage_range[stage])
  bf1[37] = apply_value(bf0[36] - bf0[37], stage_range[stage])
  bf1[38] = apply_value(-bf0[38] + bf0[39], stage_range[stage])
  bf1[39] = apply_value(bf0[38] + bf0[39], stage_range[stage])
  bf1[40] = apply_value(bf0[40] + bf0[41], stage_range[stage])
  bf1[41] = apply_value(bf0[40] - bf0[41], stage_range[stage])
  bf1[42] = apply_value(-bf0[42] + bf0[43], stage_range[stage])
  bf1[43] = apply_value(bf0[42] + bf0[43], stage_range[stage])
  bf1[44] = apply_value(bf0[44] + bf0[45], stage_range[stage])
  bf1[45] = apply_value(bf0[44] - bf0[45], stage_range[stage])
  bf1[46] = apply_value(-bf0[46] + bf0[47], stage_range[stage])
  bf1[47] = apply_value(bf0[46] + bf0[47], stage_range[stage])
  bf1[48] = apply_value(bf0[48] + bf0[49], stage_range[stage])
  bf1[49] = apply_value(bf0[48] - bf0[49], stage_range[stage])
  bf1[50] = apply_value(-bf0[50] + bf0[51], stage_range[stage])
  bf1[51] = apply_value(bf0[50] + bf0[51], stage_range[stage])
  bf1[52] = apply_value(bf0[52] + bf0[53], stage_range[stage])
  bf1[53] = apply_value(bf0[52] - bf0[53], stage_range[stage])
  bf1[54] = apply_value(-bf0[54] + bf0[55], stage_range[stage])
  bf1[55] = apply_value(bf0[54] + bf0[55], stage_range[stage])
  bf1[56] = apply_value(bf0[56] + bf0[57], stage_range[stage])
  bf1[57] = apply_value(bf0[56] - bf0[57], stage_range[stage])
  bf1[58] = apply_value(-bf0[58] + bf0[59], stage_range[stage])
  bf1[59] = apply_value(bf0[58] + bf0[59], stage_range[stage])
  bf1[60] = apply_value(bf0[60] + bf0[61], stage_range[stage])
  bf1[61] = apply_value(bf0[60] - bf0[61], stage_range[stage])
  bf1[62] = apply_value(-bf0[62] + bf0[63], stage_range[stage])
  bf1[63] = apply_value(bf0[62] + bf0[63], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 4
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = bf0[4]
  bf1[5] = bf0[5]
  bf1[6] = bf0[6]
  bf1[7] = bf0[7]
  bf1[8] = half_btf(cospi[60], bf0[8], -cospi[4], bf0[15], INV_COS_BIT)
  bf1[9] = half_btf(cospi[28], bf0[9], -cospi[36], bf0[14], INV_COS_BIT)
  bf1[10] = half_btf(cospi[44], bf0[10], -cospi[20], bf0[13], INV_COS_BIT)
  bf1[11] = half_btf(cospi[12], bf0[11], -cospi[52], bf0[12], INV_COS_BIT)
  bf1[12] = half_btf(cospi[52], bf0[11], cospi[12], bf0[12], INV_COS_BIT)
  bf1[13] = half_btf(cospi[20], bf0[10], cospi[44], bf0[13], INV_COS_BIT)
  bf1[14] = half_btf(cospi[36], bf0[9], cospi[28], bf0[14], INV_COS_BIT)
  bf1[15] = half_btf(cospi[4], bf0[8], cospi[60], bf0[15], INV_COS_BIT)
  bf1[16] = apply_value(bf0[16] + bf0[17], stage_range[stage])
  bf1[17] = apply_value(bf0[16] - bf0[17], stage_range[stage])
  bf1[18] = apply_value(-bf0[18] + bf0[19], stage_range[stage])
  bf1[19] = apply_value(bf0[18] + bf0[19], stage_range[stage])
  bf1[20] = apply_value(bf0[20] + bf0[21], stage_range[stage])
  bf1[21] = apply_value(bf0[20] - bf0[21], stage_range[stage])
  bf1[22] = apply_value(-bf0[22] + bf0[23], stage_range[stage])
  bf1[23] = apply_value(bf0[22] + bf0[23], stage_range[stage])
  bf1[24] = apply_value(bf0[24] + bf0[25], stage_range[stage])
  bf1[25] = apply_value(bf0[24] - bf0[25], stage_range[stage])
  bf1[26] = apply_value(-bf0[26] + bf0[27], stage_range[stage])
  bf1[27] = apply_value(bf0[26] + bf0[27], stage_range[stage])
  bf1[28] = apply_value(bf0[28] + bf0[29], stage_range[stage])
  bf1[29] = apply_value(bf0[28] - bf0[29], stage_range[stage])
  bf1[30] = apply_value(-bf0[30] + bf0[31], stage_range[stage])
  bf1[31] = apply_value(bf0[30] + bf0[31], stage_range[stage])
  bf1[32] = bf0[32]
  bf1[33] = half_btf(-cospi[4], bf0[33], cospi[60], bf0[62], INV_COS_BIT)
  bf1[34] = half_btf(-cospi[60], bf0[34], -cospi[4], bf0[61], INV_COS_BIT)
  bf1[35] = bf0[35]
  bf1[36] = bf0[36]
  bf1[37] = half_btf(-cospi[36], bf0[37], cospi[28], bf0[58], INV_COS_BIT)
  bf1[38] = half_btf(-cospi[28], bf0[38], -cospi[36], bf0[57], INV_COS_BIT)
  bf1[39] = bf0[39]
  bf1[40] = bf0[40]
  bf1[41] = half_btf(-cospi[20], bf0[41], cospi[44], bf0[54], INV_COS_BIT)
  bf1[42] = half_btf(-cospi[44], bf0[42], -cospi[20], bf0[53], INV_COS_BIT)
  bf1[43] = bf0[43]
  bf1[44] = bf0[44]
  bf1[45] = half_btf(-cospi[52], bf0[45], cospi[12], bf0[50], INV_COS_BIT)
  bf1[46] = half_btf(-cospi[12], bf0[46], -cospi[52], bf0[49], INV_COS_BIT)
  bf1[47] = bf0[47]
  bf1[48] = bf0[48]
  bf1[49] = half_btf(-cospi[52], bf0[46], cospi[12], bf0[49], INV_COS_BIT)
  bf1[50] = half_btf(cospi[12], bf0[45], cospi[52], bf0[50], INV_COS_BIT)
  bf1[51] = bf0[51]
  bf1[52] = bf0[52]
  bf1[53] = half_btf(-cospi[20], bf0[42], cospi[44], bf0[53], INV_COS_BIT)
  bf1[54] = half_btf(cospi[44], bf0[41], cospi[20], bf0[54], INV_COS_BIT)
  bf1[55] = bf0[55]
  bf1[56] = bf0[56]
  bf1[57] = half_btf(-cospi[36], bf0[38], cospi[28], bf0[57], INV_COS_BIT)
  bf1[58] = half_btf(cospi[28], bf0[37], cospi[36], bf0[58], INV_COS_BIT)
  bf1[59] = bf0[59]
  bf1[60] = bf0[60]
  bf1[61] = half_btf(-cospi[4], bf0[34], cospi[60], bf0[61], INV_COS_BIT)
  bf1[62] = half_btf(cospi[60], bf0[33], cospi[4], bf0[62], INV_COS_BIT)
  bf1[63] = bf0[63]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 5
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = bf0[0]
  bf1[1] = bf0[1]
  bf1[2] = bf0[2]
  bf1[3] = bf0[3]
  bf1[4] = half_btf(cospi[56], bf0[4], -cospi[8], bf0[7], INV_COS_BIT)
  bf1[5] = half_btf(cospi[24], bf0[5], -cospi[40], bf0[6], INV_COS_BIT)
  bf1[6] = half_btf(cospi[40], bf0[5], cospi[24], bf0[6], INV_COS_BIT)
  bf1[7] = half_btf(cospi[8], bf0[4], cospi[56], bf0[7], INV_COS_BIT)
  bf1[8] = apply_value(bf0[8] + bf0[9], stage_range[stage])
  bf1[9] = apply_value(bf0[8] - bf0[9], stage_range[stage])
  bf1[10] = apply_value(-bf0[10] + bf0[11], stage_range[stage])
  bf1[11] = apply_value(bf0[10] + bf0[11], stage_range[stage])
  bf1[12] = apply_value(bf0[12] + bf0[13], stage_range[stage])
  bf1[13] = apply_value(bf0[12] - bf0[13], stage_range[stage])
  bf1[14] = apply_value(-bf0[14] + bf0[15], stage_range[stage])
  bf1[15] = apply_value(bf0[14] + bf0[15], stage_range[stage])
  bf1[16] = bf0[16]
  bf1[17] = half_btf(-cospi[8], bf0[17], cospi[56], bf0[30], INV_COS_BIT)
  bf1[18] = half_btf(-cospi[56], bf0[18], -cospi[8], bf0[29], INV_COS_BIT)
  bf1[19] = bf0[19]
  bf1[20] = bf0[20]
  bf1[21] = half_btf(-cospi[40], bf0[21], cospi[24], bf0[26], INV_COS_BIT)
  bf1[22] = half_btf(-cospi[24], bf0[22], -cospi[40], bf0[25], INV_COS_BIT)
  bf1[23] = bf0[23]
  bf1[24] = bf0[24]
  bf1[25] = half_btf(-cospi[40], bf0[22], cospi[24], bf0[25], INV_COS_BIT)
  bf1[26] = half_btf(cospi[24], bf0[21], cospi[40], bf0[26], INV_COS_BIT)
  bf1[27] = bf0[27]
  bf1[28] = bf0[28]
  bf1[29] = half_btf(-cospi[8], bf0[18], cospi[56], bf0[29], INV_COS_BIT)
  bf1[30] = half_btf(cospi[56], bf0[17], cospi[8], bf0[30], INV_COS_BIT)
  bf1[31] = bf0[31]
  bf1[32] = apply_value(bf0[32] + bf0[35], stage_range[stage])
  bf1[33] = apply_value(bf0[33] + bf0[34], stage_range[stage])
  bf1[34] = apply_value(bf0[33] - bf0[34], stage_range[stage])
  bf1[35] = apply_value(bf0[32] - bf0[35], stage_range[stage])
  bf1[36] = apply_value(-bf0[36] + bf0[39], stage_range[stage])
  bf1[37] = apply_value(-bf0[37] + bf0[38], stage_range[stage])
  bf1[38] = apply_value(bf0[37] + bf0[38], stage_range[stage])
  bf1[39] = apply_value(bf0[36] + bf0[39], stage_range[stage])
  bf1[40] = apply_value(bf0[40] + bf0[43], stage_range[stage])
  bf1[41] = apply_value(bf0[41] + bf0[42], stage_range[stage])
  bf1[42] = apply_value(bf0[41] - bf0[42], stage_range[stage])
  bf1[43] = apply_value(bf0[40] - bf0[43], stage_range[stage])
  bf1[44] = apply_value(-bf0[44] + bf0[47], stage_range[stage])
  bf1[45] = apply_value(-bf0[45] + bf0[46], stage_range[stage])
  bf1[46] = apply_value(bf0[45] + bf0[46], stage_range[stage])
  bf1[47] = apply_value(bf0[44] + bf0[47], stage_range[stage])
  bf1[48] = apply_value(bf0[48] + bf0[51], stage_range[stage])
  bf1[49] = apply_value(bf0[49] + bf0[50], stage_range[stage])
  bf1[50] = apply_value(bf0[49] - bf0[50], stage_range[stage])
  bf1[51] = apply_value(bf0[48] - bf0[51], stage_range[stage])
  bf1[52] = apply_value(-bf0[52] + bf0[55], stage_range[stage])
  bf1[53] = apply_value(-bf0[53] + bf0[54], stage_range[stage])
  bf1[54] = apply_value(bf0[53] + bf0[54], stage_range[stage])
  bf1[55] = apply_value(bf0[52] + bf0[55], stage_range[stage])
  bf1[56] = apply_value(bf0[56] + bf0[59], stage_range[stage])
  bf1[57] = apply_value(bf0[57] + bf0[58], stage_range[stage])
  bf1[58] = apply_value(bf0[57] - bf0[58], stage_range[stage])
  bf1[59] = apply_value(bf0[56] - bf0[59], stage_range[stage])
  bf1[60] = apply_value(-bf0[60] + bf0[63], stage_range[stage])
  bf1[61] = apply_value(-bf0[61] + bf0[62], stage_range[stage])
  bf1[62] = apply_value(bf0[61] + bf0[62], stage_range[stage])
  bf1[63] = apply_value(bf0[60] + bf0[63], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 6
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = half_btf(cospi[32], bf0[0], cospi[32], bf0[1], INV_COS_BIT)
  bf1[1] = half_btf(cospi[32], bf0[0], -cospi[32], bf0[1], INV_COS_BIT)
  bf1[2] = half_btf(cospi[48], bf0[2], -cospi[16], bf0[3], INV_COS_BIT)
  bf1[3] = half_btf(cospi[16], bf0[2], cospi[48], bf0[3], INV_COS_BIT)
  bf1[4] = apply_value(bf0[4] + bf0[5], stage_range[stage])
  bf1[5] = apply_value(bf0[4] - bf0[5], stage_range[stage])
  bf1[6] = apply_value(-bf0[6] + bf0[7], stage_range[stage])
  bf1[7] = apply_value(bf0[6] + bf0[7], stage_range[stage])
  bf1[8] = bf0[8]
  bf1[9] = half_btf(-cospi[16], bf0[9], cospi[48], bf0[14], INV_COS_BIT)
  bf1[10] = half_btf(-cospi[48], bf0[10], -cospi[16], bf0[13], INV_COS_BIT)
  bf1[11] = bf0[11]
  bf1[12] = bf0[12]
  bf1[13] = half_btf(-cospi[16], bf0[10], cospi[48], bf0[13], INV_COS_BIT)
  bf1[14] = half_btf(cospi[48], bf0[9], cospi[16], bf0[14], INV_COS_BIT)
  bf1[15] = bf0[15]
  bf1[16] = apply_value(bf0[16] + bf0[19], stage_range[stage])
  bf1[17] = apply_value(bf0[17] + bf0[18], stage_range[stage])
  bf1[18] = apply_value(bf0[17] - bf0[18], stage_range[stage])
  bf1[19] = apply_value(bf0[16] - bf0[19], stage_range[stage])
  bf1[20] = apply_value(-bf0[20] + bf0[23], stage_range[stage])
  bf1[21] = apply_value(-bf0[21] + bf0[22], stage_range[stage])
  bf1[22] = apply_value(bf0[21] + bf0[22], stage_range[stage])
  bf1[23] = apply_value(bf0[20] + bf0[23], stage_range[stage])
  bf1[24] = apply_value(bf0[24] + bf0[27], stage_range[stage])
  bf1[25] = apply_value(bf0[25] + bf0[26], stage_range[stage])
  bf1[26] = apply_value(bf0[25] - bf0[26], stage_range[stage])
  bf1[27] = apply_value(bf0[24] - bf0[27], stage_range[stage])
  bf1[28] = apply_value(-bf0[28] + bf0[31], stage_range[stage])
  bf1[29] = apply_value(-bf0[29] + bf0[30], stage_range[stage])
  bf1[30] = apply_value(bf0[29] + bf0[30], stage_range[stage])
  bf1[31] = apply_value(bf0[28] + bf0[31], stage_range[stage])
  bf1[32] = bf0[32]
  bf1[33] = bf0[33]
  bf1[34] = half_btf(-cospi[8], bf0[34], cospi[56], bf0[61], INV_COS_BIT)
  bf1[35] = half_btf(-cospi[8], bf0[35], cospi[56], bf0[60], INV_COS_BIT)
  bf1[36] = half_btf(-cospi[56], bf0[36], -cospi[8], bf0[59], INV_COS_BIT)
  bf1[37] = half_btf(-cospi[56], bf0[37], -cospi[8], bf0[58], INV_COS_BIT)
  bf1[38] = bf0[38]
  bf1[39] = bf0[39]
  bf1[40] = bf0[40]
  bf1[41] = bf0[41]
  bf1[42] = half_btf(-cospi[40], bf0[42], cospi[24], bf0[53], INV_COS_BIT)
  bf1[43] = half_btf(-cospi[40], bf0[43], cospi[24], bf0[52], INV_COS_BIT)
  bf1[44] = half_btf(-cospi[24], bf0[44], -cospi[40], bf0[51], INV_COS_BIT)
  bf1[45] = half_btf(-cospi[24], bf0[45], -cospi[40], bf0[50], INV_COS_BIT)
  bf1[46] = bf0[46]
  bf1[47] = bf0[47]
  bf1[48] = bf0[48]
  bf1[49] = bf0[49]
  bf1[50] = half_btf(-cospi[40], bf0[45], cospi[24], bf0[50], INV_COS_BIT)
  bf1[51] = half_btf(-cospi[40], bf0[44], cospi[24], bf0[51], INV_COS_BIT)
  bf1[52] = half_btf(cospi[24], bf0[43], cospi[40], bf0[52], INV_COS_BIT)
  bf1[53] = half_btf(cospi[24], bf0[42], cospi[40], bf0[53], INV_COS_BIT)
  bf1[54] = bf0[54]
  bf1[55] = bf0[55]
  bf1[56] = bf0[56]
  bf1[57] = bf0[57]
  bf1[58] = half_btf(-cospi[8], bf0[37], cospi[56], bf0[58], INV_COS_BIT)
  bf1[59] = half_btf(-cospi[8], bf0[36], cospi[56], bf0[59], INV_COS_BIT)
  bf1[60] = half_btf(cospi[56], bf0[35], cospi[8], bf0[60], INV_COS_BIT)
  bf1[61] = half_btf(cospi[56], bf0[34], cospi[8], bf0[61], INV_COS_BIT)
  bf1[62] = bf0[62]
  bf1[63] = bf0[63]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 7
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[3], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[2], stage_range[stage])
  bf1[2] = apply_value(bf0[1] - bf0[2], stage_range[stage])
  bf1[3] = apply_value(bf0[0] - bf0[3], stage_range[stage])
  bf1[4] = bf0[4]
  bf1[5] = half_btf(-cospi[32], bf0[5], cospi[32], bf0[6], INV_COS_BIT)
  bf1[6] = half_btf(cospi[32], bf0[5], cospi[32], bf0[6], INV_COS_BIT)
  bf1[7] = bf0[7]
  bf1[8] = apply_value(bf0[8] + bf0[11], stage_range[stage])
  bf1[9] = apply_value(bf0[9] + bf0[10], stage_range[stage])
  bf1[10] = apply_value(bf0[9] - bf0[10], stage_range[stage])
  bf1[11] = apply_value(bf0[8] - bf0[11], stage_range[stage])
  bf1[12] = apply_value(-bf0[12] + bf0[15], stage_range[stage])
  bf1[13] = apply_value(-bf0[13] + bf0[14], stage_range[stage])
  bf1[14] = apply_value(bf0[13] + bf0[14], stage_range[stage])
  bf1[15] = apply_value(bf0[12] + bf0[15], stage_range[stage])
  bf1[16] = bf0[16]
  bf1[17] = bf0[17]
  bf1[18] = half_btf(-cospi[16], bf0[18], cospi[48], bf0[29], INV_COS_BIT)
  bf1[19] = half_btf(-cospi[16], bf0[19], cospi[48], bf0[28], INV_COS_BIT)
  bf1[20] = half_btf(-cospi[48], bf0[20], -cospi[16], bf0[27], INV_COS_BIT)
  bf1[21] = half_btf(-cospi[48], bf0[21], -cospi[16], bf0[26], INV_COS_BIT)
  bf1[22] = bf0[22]
  bf1[23] = bf0[23]
  bf1[24] = bf0[24]
  bf1[25] = bf0[25]
  bf1[26] = half_btf(-cospi[16], bf0[21], cospi[48], bf0[26], INV_COS_BIT)
  bf1[27] = half_btf(-cospi[16], bf0[20], cospi[48], bf0[27], INV_COS_BIT)
  bf1[28] = half_btf(cospi[48], bf0[19], cospi[16], bf0[28], INV_COS_BIT)
  bf1[29] = half_btf(cospi[48], bf0[18], cospi[16], bf0[29], INV_COS_BIT)
  bf1[30] = bf0[30]
  bf1[31] = bf0[31]
  bf1[32] = apply_value(bf0[32] + bf0[39], stage_range[stage])
  bf1[33] = apply_value(bf0[33] + bf0[38], stage_range[stage])
  bf1[34] = apply_value(bf0[34] + bf0[37], stage_range[stage])
  bf1[35] = apply_value(bf0[35] + bf0[36], stage_range[stage])
  bf1[36] = apply_value(bf0[35] - bf0[36], stage_range[stage])
  bf1[37] = apply_value(bf0[34] - bf0[37], stage_range[stage])
  bf1[38] = apply_value(bf0[33] - bf0[38], stage_range[stage])
  bf1[39] = apply_value(bf0[32] - bf0[39], stage_range[stage])
  bf1[40] = apply_value(-bf0[40] + bf0[47], stage_range[stage])
  bf1[41] = apply_value(-bf0[41] + bf0[46], stage_range[stage])
  bf1[42] = apply_value(-bf0[42] + bf0[45], stage_range[stage])
  bf1[43] = apply_value(-bf0[43] + bf0[44], stage_range[stage])
  bf1[44] = apply_value(bf0[43] + bf0[44], stage_range[stage])
  bf1[45] = apply_value(bf0[42] + bf0[45], stage_range[stage])
  bf1[46] = apply_value(bf0[41] + bf0[46], stage_range[stage])
  bf1[47] = apply_value(bf0[40] + bf0[47], stage_range[stage])
  bf1[48] = apply_value(bf0[48] + bf0[55], stage_range[stage])
  bf1[49] = apply_value(bf0[49] + bf0[54], stage_range[stage])
  bf1[50] = apply_value(bf0[50] + bf0[53], stage_range[stage])
  bf1[51] = apply_value(bf0[51] + bf0[52], stage_range[stage])
  bf1[52] = apply_value(bf0[51] - bf0[52], stage_range[stage])
  bf1[53] = apply_value(bf0[50] - bf0[53], stage_range[stage])
  bf1[54] = apply_value(bf0[49] - bf0[54], stage_range[stage])
  bf1[55] = apply_value(bf0[48] - bf0[55], stage_range[stage])
  bf1[56] = apply_value(-bf0[56] + bf0[63], stage_range[stage])
  bf1[57] = apply_value(-bf0[57] + bf0[62], stage_range[stage])
  bf1[58] = apply_value(-bf0[58] + bf0[61], stage_range[stage])
  bf1[59] = apply_value(-bf0[59] + bf0[60], stage_range[stage])
  bf1[60] = apply_value(bf0[59] + bf0[60], stage_range[stage])
  bf1[61] = apply_value(bf0[58] + bf0[61], stage_range[stage])
  bf1[62] = apply_value(bf0[57] + bf0[62], stage_range[stage])
  bf1[63] = apply_value(bf0[56] + bf0[63], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 8
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = apply_value(bf0[0] + bf0[7], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[6], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[5], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[4], stage_range[stage])
  bf1[4] = apply_value(bf0[3] - bf0[4], stage_range[stage])
  bf1[5] = apply_value(bf0[2] - bf0[5], stage_range[stage])
  bf1[6] = apply_value(bf0[1] - bf0[6], stage_range[stage])
  bf1[7] = apply_value(bf0[0] - bf0[7], stage_range[stage])
  bf1[8] = bf0[8]
  bf1[9] = bf0[9]
  bf1[10] = half_btf(-cospi[32], bf0[10], cospi[32], bf0[13], INV_COS_BIT)
  bf1[11] = half_btf(-cospi[32], bf0[11], cospi[32], bf0[12], INV_COS_BIT)
  bf1[12] = half_btf(cospi[32], bf0[11], cospi[32], bf0[12], INV_COS_BIT)
  bf1[13] = half_btf(cospi[32], bf0[10], cospi[32], bf0[13], INV_COS_BIT)
  bf1[14] = bf0[14]
  bf1[15] = bf0[15]
  bf1[16] = apply_value(bf0[16] + bf0[23], stage_range[stage])
  bf1[17] = apply_value(bf0[17] + bf0[22], stage_range[stage])
  bf1[18] = apply_value(bf0[18] + bf0[21], stage_range[stage])
  bf1[19] = apply_value(bf0[19] + bf0[20], stage_range[stage])
  bf1[20] = apply_value(bf0[19] - bf0[20], stage_range[stage])
  bf1[21] = apply_value(bf0[18] - bf0[21], stage_range[stage])
  bf1[22] = apply_value(bf0[17] - bf0[22], stage_range[stage])
  bf1[23] = apply_value(bf0[16] - bf0[23], stage_range[stage])
  bf1[24] = apply_value(-bf0[24] + bf0[31], stage_range[stage])
  bf1[25] = apply_value(-bf0[25] + bf0[30], stage_range[stage])
  bf1[26] = apply_value(-bf0[26] + bf0[29], stage_range[stage])
  bf1[27] = apply_value(-bf0[27] + bf0[28], stage_range[stage])
  bf1[28] = apply_value(bf0[27] + bf0[28], stage_range[stage])
  bf1[29] = apply_value(bf0[26] + bf0[29], stage_range[stage])
  bf1[30] = apply_value(bf0[25] + bf0[30], stage_range[stage])
  bf1[31] = apply_value(bf0[24] + bf0[31], stage_range[stage])
  bf1[32] = bf0[32]
  bf1[33] = bf0[33]
  bf1[34] = bf0[34]
  bf1[35] = bf0[35]
  bf1[36] = half_btf(-cospi[16], bf0[36], cospi[48], bf0[59], INV_COS_BIT)
  bf1[37] = half_btf(-cospi[16], bf0[37], cospi[48], bf0[58], INV_COS_BIT)
  bf1[38] = half_btf(-cospi[16], bf0[38], cospi[48], bf0[57], INV_COS_BIT)
  bf1[39] = half_btf(-cospi[16], bf0[39], cospi[48], bf0[56], INV_COS_BIT)
  bf1[40] = half_btf(-cospi[48], bf0[40], -cospi[16], bf0[55], INV_COS_BIT)
  bf1[41] = half_btf(-cospi[48], bf0[41], -cospi[16], bf0[54], INV_COS_BIT)
  bf1[42] = half_btf(-cospi[48], bf0[42], -cospi[16], bf0[53], INV_COS_BIT)
  bf1[43] = half_btf(-cospi[48], bf0[43], -cospi[16], bf0[52], INV_COS_BIT)
  bf1[44] = bf0[44]
  bf1[45] = bf0[45]
  bf1[46] = bf0[46]
  bf1[47] = bf0[47]
  bf1[48] = bf0[48]
  bf1[49] = bf0[49]
  bf1[50] = bf0[50]
  bf1[51] = bf0[51]
  bf1[52] = half_btf(-cospi[16], bf0[43], cospi[48], bf0[52], INV_COS_BIT)
  bf1[53] = half_btf(-cospi[16], bf0[42], cospi[48], bf0[53], INV_COS_BIT)
  bf1[54] = half_btf(-cospi[16], bf0[41], cospi[48], bf0[54], INV_COS_BIT)
  bf1[55] = half_btf(-cospi[16], bf0[40], cospi[48], bf0[55], INV_COS_BIT)
  bf1[56] = half_btf(cospi[48], bf0[39], cospi[16], bf0[56], INV_COS_BIT)
  bf1[57] = half_btf(cospi[48], bf0[38], cospi[16], bf0[57], INV_COS_BIT)
  bf1[58] = half_btf(cospi[48], bf0[37], cospi[16], bf0[58], INV_COS_BIT)
  bf1[59] = half_btf(cospi[48], bf0[36], cospi[16], bf0[59], INV_COS_BIT)
  bf1[60] = bf0[60]
  bf1[61] = bf0[61]
  bf1[62] = bf0[62]
  bf1[63] = bf0[63]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 9
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[15], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[14], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[13], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[12], stage_range[stage])
  bf1[4] = apply_value(bf0[4] + bf0[11], stage_range[stage])
  bf1[5] = apply_value(bf0[5] + bf0[10], stage_range[stage])
  bf1[6] = apply_value(bf0[6] + bf0[9], stage_range[stage])
  bf1[7] = apply_value(bf0[7] + bf0[8], stage_range[stage])
  bf1[8] = apply_value(bf0[7] - bf0[8], stage_range[stage])
  bf1[9] = apply_value(bf0[6] - bf0[9], stage_range[stage])
  bf1[10] = apply_value(bf0[5] - bf0[10], stage_range[stage])
  bf1[11] = apply_value(bf0[4] - bf0[11], stage_range[stage])
  bf1[12] = apply_value(bf0[3] - bf0[12], stage_range[stage])
  bf1[13] = apply_value(bf0[2] - bf0[13], stage_range[stage])
  bf1[14] = apply_value(bf0[1] - bf0[14], stage_range[stage])
  bf1[15] = apply_value(bf0[0] - bf0[15], stage_range[stage])
  bf1[16] = bf0[16]
  bf1[17] = bf0[17]
  bf1[18] = bf0[18]
  bf1[19] = bf0[19]
  bf1[20] = half_btf(-cospi[32], bf0[20], cospi[32], bf0[27], INV_COS_BIT)
  bf1[21] = half_btf(-cospi[32], bf0[21], cospi[32], bf0[26], INV_COS_BIT)
  bf1[22] = half_btf(-cospi[32], bf0[22], cospi[32], bf0[25], INV_COS_BIT)
  bf1[23] = half_btf(-cospi[32], bf0[23], cospi[32], bf0[24], INV_COS_BIT)
  bf1[24] = half_btf(cospi[32], bf0[23], cospi[32], bf0[24], INV_COS_BIT)
  bf1[25] = half_btf(cospi[32], bf0[22], cospi[32], bf0[25], INV_COS_BIT)
  bf1[26] = half_btf(cospi[32], bf0[21], cospi[32], bf0[26], INV_COS_BIT)
  bf1[27] = half_btf(cospi[32], bf0[20], cospi[32], bf0[27], INV_COS_BIT)
  bf1[28] = bf0[28]
  bf1[29] = bf0[29]
  bf1[30] = bf0[30]
  bf1[31] = bf0[31]
  bf1[32] = apply_value(bf0[32] + bf0[47], stage_range[stage])
  bf1[33] = apply_value(bf0[33] + bf0[46], stage_range[stage])
  bf1[34] = apply_value(bf0[34] + bf0[45], stage_range[stage])
  bf1[35] = apply_value(bf0[35] + bf0[44], stage_range[stage])
  bf1[36] = apply_value(bf0[36] + bf0[43], stage_range[stage])
  bf1[37] = apply_value(bf0[37] + bf0[42], stage_range[stage])
  bf1[38] = apply_value(bf0[38] + bf0[41], stage_range[stage])
  bf1[39] = apply_value(bf0[39] + bf0[40], stage_range[stage])
  bf1[40] = apply_value(bf0[39] - bf0[40], stage_range[stage])
  bf1[41] = apply_value(bf0[38] - bf0[41], stage_range[stage])
  bf1[42] = apply_value(bf0[37] - bf0[42], stage_range[stage])
  bf1[43] = apply_value(bf0[36] - bf0[43], stage_range[stage])
  bf1[44] = apply_value(bf0[35] - bf0[44], stage_range[stage])
  bf1[45] = apply_value(bf0[34] - bf0[45], stage_range[stage])
  bf1[46] = apply_value(bf0[33] - bf0[46], stage_range[stage])
  bf1[47] = apply_value(bf0[32] - bf0[47], stage_range[stage])
  bf1[48] = apply_value(-bf0[48] + bf0[63], stage_range[stage])
  bf1[49] = apply_value(-bf0[49] + bf0[62], stage_range[stage])
  bf1[50] = apply_value(-bf0[50] + bf0[61], stage_range[stage])
  bf1[51] = apply_value(-bf0[51] + bf0[60], stage_range[stage])
  bf1[52] = apply_value(-bf0[52] + bf0[59], stage_range[stage])
  bf1[53] = apply_value(-bf0[53] + bf0[58], stage_range[stage])
  bf1[54] = apply_value(-bf0[54] + bf0[57], stage_range[stage])
  bf1[55] = apply_value(-bf0[55] + bf0[56], stage_range[stage])
  bf1[56] = apply_value(bf0[55] + bf0[56], stage_range[stage])
  bf1[57] = apply_value(bf0[54] + bf0[57], stage_range[stage])
  bf1[58] = apply_value(bf0[53] + bf0[58], stage_range[stage])
  bf1[59] = apply_value(bf0[52] + bf0[59], stage_range[stage])
  bf1[60] = apply_value(bf0[51] + bf0[60], stage_range[stage])
  bf1[61] = apply_value(bf0[50] + bf0[61], stage_range[stage])
  bf1[62] = apply_value(bf0[49] + bf0[62], stage_range[stage])
  bf1[63] = apply_value(bf0[48] + bf0[63], stage_range[stage])
  range_check_buf(bf1, size, stage_range[stage])

  // stage 10
  stage++
  bf0 = output
  bf1 = step
  bf1[0] = apply_value(bf0[0] + bf0[31], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[30], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[29], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[28], stage_range[stage])
  bf1[4] = apply_value(bf0[4] + bf0[27], stage_range[stage])
  bf1[5] = apply_value(bf0[5] + bf0[26], stage_range[stage])
  bf1[6] = apply_value(bf0[6] + bf0[25], stage_range[stage])
  bf1[7] = apply_value(bf0[7] + bf0[24], stage_range[stage])
  bf1[8] = apply_value(bf0[8] + bf0[23], stage_range[stage])
  bf1[9] = apply_value(bf0[9] + bf0[22], stage_range[stage])
  bf1[10] = apply_value(bf0[10] + bf0[21], stage_range[stage])
  bf1[11] = apply_value(bf0[11] + bf0[20], stage_range[stage])
  bf1[12] = apply_value(bf0[12] + bf0[19], stage_range[stage])
  bf1[13] = apply_value(bf0[13] + bf0[18], stage_range[stage])
  bf1[14] = apply_value(bf0[14] + bf0[17], stage_range[stage])
  bf1[15] = apply_value(bf0[15] + bf0[16], stage_range[stage])
  bf1[16] = apply_value(bf0[15] - bf0[16], stage_range[stage])
  bf1[17] = apply_value(bf0[14] - bf0[17], stage_range[stage])
  bf1[18] = apply_value(bf0[13] - bf0[18], stage_range[stage])
  bf1[19] = apply_value(bf0[12] - bf0[19], stage_range[stage])
  bf1[20] = apply_value(bf0[11] - bf0[20], stage_range[stage])
  bf1[21] = apply_value(bf0[10] - bf0[21], stage_range[stage])
  bf1[22] = apply_value(bf0[9] - bf0[22], stage_range[stage])
  bf1[23] = apply_value(bf0[8] - bf0[23], stage_range[stage])
  bf1[24] = apply_value(bf0[7] - bf0[24], stage_range[stage])
  bf1[25] = apply_value(bf0[6] - bf0[25], stage_range[stage])
  bf1[26] = apply_value(bf0[5] - bf0[26], stage_range[stage])
  bf1[27] = apply_value(bf0[4] - bf0[27], stage_range[stage])
  bf1[28] = apply_value(bf0[3] - bf0[28], stage_range[stage])
  bf1[29] = apply_value(bf0[2] - bf0[29], stage_range[stage])
  bf1[30] = apply_value(bf0[1] - bf0[30], stage_range[stage])
  bf1[31] = apply_value(bf0[0] - bf0[31], stage_range[stage])
  bf1[32] = bf0[32]
  bf1[33] = bf0[33]
  bf1[34] = bf0[34]
  bf1[35] = bf0[35]
  bf1[36] = bf0[36]
  bf1[37] = bf0[37]
  bf1[38] = bf0[38]
  bf1[39] = bf0[39]
  bf1[40] = half_btf(-cospi[32], bf0[40], cospi[32], bf0[55], INV_COS_BIT)
  bf1[41] = half_btf(-cospi[32], bf0[41], cospi[32], bf0[54], INV_COS_BIT)
  bf1[42] = half_btf(-cospi[32], bf0[42], cospi[32], bf0[53], INV_COS_BIT)
  bf1[43] = half_btf(-cospi[32], bf0[43], cospi[32], bf0[52], INV_COS_BIT)
  bf1[44] = half_btf(-cospi[32], bf0[44], cospi[32], bf0[51], INV_COS_BIT)
  bf1[45] = half_btf(-cospi[32], bf0[45], cospi[32], bf0[50], INV_COS_BIT)
  bf1[46] = half_btf(-cospi[32], bf0[46], cospi[32], bf0[49], INV_COS_BIT)
  bf1[47] = half_btf(-cospi[32], bf0[47], cospi[32], bf0[48], INV_COS_BIT)
  bf1[48] = half_btf(cospi[32], bf0[47], cospi[32], bf0[48], INV_COS_BIT)
  bf1[49] = half_btf(cospi[32], bf0[46], cospi[32], bf0[49], INV_COS_BIT)
  bf1[50] = half_btf(cospi[32], bf0[45], cospi[32], bf0[50], INV_COS_BIT)
  bf1[51] = half_btf(cospi[32], bf0[44], cospi[32], bf0[51], INV_COS_BIT)
  bf1[52] = half_btf(cospi[32], bf0[43], cospi[32], bf0[52], INV_COS_BIT)
  bf1[53] = half_btf(cospi[32], bf0[42], cospi[32], bf0[53], INV_COS_BIT)
  bf1[54] = half_btf(cospi[32], bf0[41], cospi[32], bf0[54], INV_COS_BIT)
  bf1[55] = half_btf(cospi[32], bf0[40], cospi[32], bf0[55], INV_COS_BIT)
  bf1[56] = bf0[56]
  bf1[57] = bf0[57]
  bf1[58] = bf0[58]
  bf1[59] = bf0[59]
  bf1[60] = bf0[60]
  bf1[61] = bf0[61]
  bf1[62] = bf0[62]
  bf1[63] = bf0[63]
  range_check_buf(bf1, size, stage_range[stage])

  // stage 11
  stage++
  bf0 = step
  bf1 = output
  bf1[0] = apply_value(bf0[0] + bf0[63], stage_range[stage])
  bf1[1] = apply_value(bf0[1] + bf0[62], stage_range[stage])
  bf1[2] = apply_value(bf0[2] + bf0[61], stage_range[stage])
  bf1[3] = apply_value(bf0[3] + bf0[60], stage_range[stage])
  bf1[4] = apply_value(bf0[4] + bf0[59], stage_range[stage])
  bf1[5] = apply_value(bf0[5] + bf0[58], stage_range[stage])
  bf1[6] = apply_value(bf0[6] + bf0[57], stage_range[stage])
  bf1[7] = apply_value(bf0[7] + bf0[56], stage_range[stage])
  bf1[8] = apply_value(bf0[8] + bf0[55], stage_range[stage])
  bf1[9] = apply_value(bf0[9] + bf0[54], stage_range[stage])
  bf1[10] = apply_value(bf0[10] + bf0[53], stage_range[stage])
  bf1[11] = apply_value(bf0[11] + bf0[52], stage_range[stage])
  bf1[12] = apply_value(bf0[12] + bf0[51], stage_range[stage])
  bf1[13] = apply_value(bf0[13] + bf0[50], stage_range[stage])
  bf1[14] = apply_value(bf0[14] + bf0[49], stage_range[stage])
  bf1[15] = apply_value(bf0[15] + bf0[48], stage_range[stage])
  bf1[16] = apply_value(bf0[16] + bf0[47], stage_range[stage])
  bf1[17] = apply_value(bf0[17] + bf0[46], stage_range[stage])
  bf1[18] = apply_value(bf0[18] + bf0[45], stage_range[stage])
  bf1[19] = apply_value(bf0[19] + bf0[44], stage_range[stage])
  bf1[20] = apply_value(bf0[20] + bf0[43], stage_range[stage])
  bf1[21] = apply_value(bf0[21] + bf0[42], stage_range[stage])
  bf1[22] = apply_value(bf0[22] + bf0[41], stage_range[stage])
  bf1[23] = apply_value(bf0[23] + bf0[40], stage_range[stage])
  bf1[24] = apply_value(bf0[24] + bf0[39], stage_range[stage])
  bf1[25] = apply_value(bf0[25] + bf0[38], stage_range[stage])
  bf1[26] = apply_value(bf0[26] + bf0[37], stage_range[stage])
  bf1[27] = apply_value(bf0[27] + bf0[36], stage_range[stage])
  bf1[28] = apply_value(bf0[28] + bf0[35], stage_range[stage])
  bf1[29] = apply_value(bf0[29] + bf0[34], stage_range[stage])
  bf1[30] = apply_value(bf0[30] + bf0[33], stage_range[stage])
  bf1[31] = apply_value(bf0[31] + bf0[32], stage_range[stage])
  bf1[32] = apply_value(bf0[31] - bf0[32], stage_range[stage])
  bf1[33] = apply_value(bf0[30] - bf0[33], stage_range[stage])
  bf1[34] = apply_value(bf0[29] - bf0[34], stage_range[stage])
  bf1[35] = apply_value(bf0[28] - bf0[35], stage_range[stage])
  bf1[36] = apply_value(bf0[27] - bf0[36], stage_range[stage])
  bf1[37] = apply_value(bf0[26] - bf0[37], stage_range[stage])
  bf1[38] = apply_value(bf0[25] - bf0[38], stage_range[stage])
  bf1[39] = apply_value(bf0[24] - bf0[39], stage_range[stage])
  bf1[40] = apply_value(bf0[23] - bf0[40], stage_range[stage])
  bf1[41] = apply_value(bf0[22] - bf0[41], stage_range[stage])
  bf1[42] = apply_value(bf0[21] - bf0[42], stage_range[stage])
  bf1[43] = apply_value(bf0[20] - bf0[43], stage_range[stage])
  bf1[44] = apply_value(bf0[19] - bf0[44], stage_range[stage])
  bf1[45] = apply_value(bf0[18] - bf0[45], stage_range[stage])
  bf1[46] = apply_value(bf0[17] - bf0[46], stage_range[stage])
  bf1[47] = apply_value(bf0[16] - bf0[47], stage_range[stage])
  bf1[48] = apply_value(bf0[15] - bf0[48], stage_range[stage])
  bf1[49] = apply_value(bf0[14] - bf0[49], stage_range[stage])
  bf1[50] = apply_value(bf0[13] - bf0[50], stage_range[stage])
  bf1[51] = apply_value(bf0[12] - bf0[51], stage_range[stage])
  bf1[52] = apply_value(bf0[11] - bf0[52], stage_range[stage])
  bf1[53] = apply_value(bf0[10] - bf0[53], stage_range[stage])
  bf1[54] = apply_value(bf0[9] - bf0[54], stage_range[stage])
  bf1[55] = apply_value(bf0[8] - bf0[55], stage_range[stage])
  bf1[56] = apply_value(bf0[7] - bf0[56], stage_range[stage])
  bf1[57] = apply_value(bf0[6] - bf0[57], stage_range[stage])
  bf1[58] = apply_value(bf0[5] - bf0[58], stage_range[stage])
  bf1[59] = apply_value(bf0[4] - bf0[59], stage_range[stage])
  bf1[60] = apply_value(bf0[3] - bf0[60], stage_range[stage])
  bf1[61] = apply_value(bf0[2] - bf0[61], stage_range[stage])
  bf1[62] = apply_value(bf0[1] - bf0[62], stage_range[stage])
  bf1[63] = apply_value(bf0[0] - bf0[63], stage_range[stage])
  //apply_range(bf1, size, stage_range[stage])
}

iidtx4_1d() {
  for (i = 0; i < 4; i++) {
    v = ROUND_POWER_OF_TWO(input[i] * SQRT2_12BIT, 12)
    output[i] = v
  }
}

iidtx8_1d() {
  for (i = 0; i < 8; i++) {
    v = input[i] * 2
    output[i] = v
  }
}

iidtx16_1d() {
  for (i = 0; i < 16; i++) {
    v = ROUND_POWER_OF_TWO(input[i] * 2 * SQRT2_12BIT, 12)
    output[i] = v
  }
}

iidtx32_1d() {
  for (i = 0; i < 32; i++) {
    v = input[i] * 4
    output[i] = v
  }
}

iidtx64_1d() {
  for (i = 0; i < 64; i++) {
    v = ROUND_POWER_OF_TWO(input[i] * 4 * SQRT2_12BIT, 12)
    output[i] = v
  }
}

uint8 inv_start_range[TX_SIZES_ALL] = {
  5, 6, 7, 7,
  7,
  5, 5, 6, 6, 6, 6,
  6, 6,
  6, 6, 7, 7,
  7, 7
};

uint8 inv_row_shifts[TX_SIZES_ALL] = {
  0, 1, 2, 2,
  2,
  0, 0, 1, 1, 1, 1,
  1, 1,
  1, 1, 2, 2,
  2, 2
};

uint8 inv_col_shifts[TX_SIZES_ALL] = {
  4, 4, 4, 4,
  4,
  4, 4, 4, 4, 4, 4,
  4, 4,
  4, 4, 4, 4,
  4, 4
};

inv_txfm1d(n,tx_sz,txfm,isCol) {
  int8 stageRange[12]
  :p.NULL = None
  row_shift = 0
  col_shift = 0
  rectType = 0 // Suppress spurious "May be used uninitialized" warning
  optRangeRow = bit_depth + 8
  optRangeCol = Max(bit_depth + 6, 16)

  szLog2 = Log2(n) - 2
#if DECODE && COLLECT_STATS
  total_1d_txfms[txfm][szLog2] += 1
#endif

  if (txfm != TXFM_WHT) {
    w = tx_size_wide[tx_sz]
    h = tx_size_high[tx_sz]
    rectType = get_rect_tx_log_ratio(w, h)
    row_shift = inv_row_shifts[tx_sz]
    col_shift = inv_col_shifts[tx_sz]
    if ((txfm == TXFM_DCT) || (txfm == TXFM_ADST)) {
      numStages = 6 + szLog2*2 - ((txfm == TXFM_DCT) ? 2 : 0)
      // Special case for iadst4
      if (txfm == TXFM_ADST && n == 4) {
        numStages = 7
      }
      for(i=0;i<numStages;i++) {
        if (isCol) {
          // Note: The libaom code adds some more values here, but those are always 0
          // so we leave them out.
          // Further, row_shift corresponds to -shift[0] in libaom, hence the - sign here.
          stageRange[i] = optRangeCol
        } else {
          stageRange[i] = optRangeRow
        }
      }
    }
  }

  if (!isCol && (rectType == 1)) {
    for(j=0;j<n;j++)
      input[j] = ROUND_POWER_OF_TWO(input[j] * INVSQRT2_12BIT, 12)
  }

  if (n==4) {
    if (txfm == TXFM_WHT) {
      av1_iwht4(isCol)
    } else if (txfm == TXFM_DCT) {
      av1_idct4_new(stageRange,NULL,NULL)
    } else if (txfm == TXFM_ADST) {
      av1_iadst4_new(stageRange,NULL,NULL)
    } else {
      CHECK(txfm == TXFM_IDTX, "Unexpected txfm")
      iidtx4_1d()
    }
  } else if (n==8) {
    if (txfm == TXFM_DCT) {
      av1_idct8_new(stageRange,NULL,NULL)
    } else if (txfm == TXFM_ADST) {
      av1_iadst8_new(stageRange,NULL,NULL)
    } else {
      CHECK(txfm == TXFM_IDTX, "Unexpected txfm")
      iidtx8_1d()
    }
  } else if (n==16) {
    if (txfm == TXFM_DCT) {
      av1_idct16_new(stageRange,NULL,NULL)
    } else if (txfm == TXFM_ADST) {
      av1_iadst16_new(stageRange,NULL,NULL)
    } else {
      CHECK(txfm == TXFM_IDTX, "Unexpected txfm")
      iidtx16_1d()
    }
  } else if (n==32) {
    if (txfm == TXFM_DCT) {
      av1_idct32_new(stageRange,NULL,NULL)
    } else {
      CHECK(txfm == TXFM_IDTX, "Unexpected txfm")
      iidtx32_1d()
    }
  } else if (n==64) {
    if (txfm == TXFM_DCT) {
      av1_idct64_new(stageRange,NULL,NULL)
    } else if (txfm == TXFM_ADST) {
      CHECK(0, "unexpected path")
    } else {
      CHECK(txfm == TXFM_IDTX, "Unexpected txfm")
      iidtx64_1d()
    }
  } else {
    ASSERT(0, "tx_sz not expected")
  }
#if VALIDATE_SPEC_TRANSFORM
  validate(isCol?20007:20006)
  for (i = 0; i < n; i++)
      validate(output[i])
#endif

  round_shift_array(n, isCol ? col_shift : row_shift)

#if VALIDATE_SPEC_TRANSFORM
  validate(isCol?20005:20004)
  for (i = 0; i < n; i++)
      validate(output[i])
#endif

  if (isCol)
    clamp_array(output, n, bit_depth + 1)
  else
    clamp_array(output, n, Max(bit_depth + 6, 16))
}

#define TXFM_PRE 0 // Input array, optionally with scaling
#define TXFM_INT 1
#define TXFM_POST 2

uint_0_2 get_rect_tx_log_ratio(int col, int row) {
  if (col == row) {
    return 0
  }
  if (col > row) {
    if (col == row * 2) {
      return 1
    }
    if (col == row * 4) {
      return 2
    }
    ASSERT(0, "Unsupported transform size")
  } else {
    // note - the ref code returns negative numbers, here we supply the mod
    if (row == col * 2) {
      return 1
    }
    if (row == col * 4) {
      return 2
    }
    ASSERT(0, "Unsupported transform size")
  }
  ASSERT(0, "Bug")
  return 0  // Invalid
}

inv_txfm2d_add(x,y,plane,tx_sz,tx_type) {
  w = tx_size_wide[tx_sz]
  h = tx_size_high[tx_sz]

#if VALIDATE_SPEC_RECON
  for (j = 0; j < h; j++) {
    for (i = 0; i < w; i++) {
      validate(is_inter?20009:20010)
      validate(plane)
      validate(j)
      validate(i)
      validate(y+j)
      validate(x+i)
      validate(frame[plane][x+i][y+j])
    }
  }
#endif

  isRect = (w != h)
  rectType = get_rect_tx_log_ratio(w, h)
  rectType2Shift = 0
  transpose = 0
#if VALIDATE_SPEC_TRANSFORM
  validate(20002)
  validate(tx_sz)
  validate(tx_type)
#endif
  txUp = txsize_sqr_up_map[tx_sz]
  roundBits = 0
  wLimit = 64
  hLimit = 64
  if ((tx_size_wide[tx_sz] * tx_size_high[tx_sz]) > 1024) {
    wLimit = 32
    hLimit = 32
  }
  else if (tx_sz == TX_16X64) {
    ASSERT(h==64 && w==16, "Logic wrong")
    hLimit = 32
  } else if (tx_sz == TX_64X16) {
    ASSERT(h==16 && w==64, "Logic wrong")
    wLimit = 32
  }

  // Rows
  for (i = 0; i < h; i++) {
    if (transpose)
      for(j=0;j<w;j++) {
        input[j] = (j>=wLimit || i>=hLimit) ? 0 : dequant[i+j*((h>hLimit)?hLimit:h)]
#if VALIDATE_SPEC_TRANSFORM
        if ( !(j>=wLimit || i>=hLimit) ) {
            validate(20001)
            validate(i)
            validate(j)
            validate( input[ j ] )
        }
#endif
      }
    else
      for(j=0;j<w;j++) {
        input[j] = (j>=wLimit || i>=hLimit) ? 0 : dequant[i*((w>wLimit)?wLimit:w)+j]
#if VALIDATE_SPEC_TRANSFORM
        if ( !(j>=wLimit || i>=hLimit) ) {
            validate(20001)
            validate(i)
            validate(j)
            validate( input[ j ] )
        }
#endif
      }
    if (lossless) {
      CHECK(tx_type == DCT_DCT && tx_sz==TX_4X4, "Unexpected lossless tx_type and tx_sz")
      txfm = TXFM_WHT
    } else {
      txfm = row_txfm[tx_type]
    }
    inv_txfm1d(w,tx_sz,txfm,0)
    for(j=0;j<w;j++) {
      halfway[i*w+j] = output[j]
    }
  }
  // Columns
  for (i = 0; i < w; i++) {
    for(j=0;j<h;j++)
      input[j] = halfway[j * w + i]
    if (lossless) {
      CHECK(tx_type == DCT_DCT && tx_sz==TX_4X4, "Unexpected lossless tx_type and tx_sz")
      txfm = TXFM_WHT
    } else {
      txfm = col_txfm[tx_type]
    }
    inv_txfm1d(h,tx_sz,txfm,1)
    for (j = 0; j < h; j++) {
      if (roundBits) {
        output[j] = ROUND_POWER_OF_TWO_SATCHECK(output[j], roundBits)
      }
      set_output_maybe_flip_strides(tx_type,x,y,plane,i,j,w,h,transpose)
    }
  }

#if VALIDATE_SPEC_RECON
  validate(20008)
  for (j = 0; j < h; j++) {
    for (i = 0; i < w; i++) {
      validate(frame[plane][x+i][y+j])
    }
  }
#endif
}
