/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************//*


This file contains code related to image planes
*/

uint_0_12 get_plane_block_size(bsize,plane) {
  planeBsize = ss_size_lookup[bsize][plane_subsampling_x[plane]][plane_subsampling_y[plane]]
  planeBsize = Max(BLOCK_4X4, planeBsize)
  return planeBsize
}

reset_skip_context(mi_row,mi_col,bsize) {
  if (monochrome)
    chromaRef = 0
  else
    chromaRef = is_chroma_reference(mi_row, mi_col, bsize, 1)
  nplanes = 1 + (get_num_planes() - 1) * chromaRef
  for (plane = 0; plane < nplanes; plane++) {
    plane_bsize = get_plane_block_size(bsize, plane)
    CHECK(MI_SIZE_LOG2<=4, "Invalid assumption on MI_SIZE to calculate 4x4")
    for(i=0;i<num_4x4_blocks_wide_lookup[plane_bsize];i++)
      above_context[plane][i+((mi_col*(MI_SIZE/4))>>plane_subsampling_x[plane])] = 0
    for(i=0;i<num_4x4_blocks_high_lookup[plane_bsize];i++)
      left_context[plane][(i+((mi_row*(MI_SIZE/4))>>plane_subsampling_y[plane])) & mi_mask2] = 0
    //:if plane==2:print 'clear skip','at',mi_col*MI_SIZE,mi_row*MI_SIZE,num_4x4_blocks_high_lookup[p.plane_bsize]
  }

}
