/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define DECODE 1
#define ENCODE 0
#define COVER 1
#error "Coverage is no longer supported by av1src, use av1bsrc instead"
