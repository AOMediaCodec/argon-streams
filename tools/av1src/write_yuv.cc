/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#include "write_yuv.hh"
#include <assert.h>
#include <sstream>
#include <stdexcept>

static bool
is_bit_depth (unsigned bd)
{
    return bd == 8 || bd == 10 || bd == 12;
}

write_yuv_t::write_yuv_t (const char *path)
    : handle (fopen (path, "wb"))
{
    if (! handle) {
        std::ostringstream oss;
        oss << "Failed to open `" << path << "' for YUV output.";
        throw std::runtime_error (oss.str ());
    }
}

write_yuv_t::~write_yuv_t ()
{
    fclose (handle);
}

static void
write_plane (FILE *file,
             const uint16_t *data,
             size_t width, size_t height, size_t stride, unsigned bit_depth)
{
    assert (is_bit_depth (bit_depth));

    unsigned bytes_out = bit_depth > 8 ? 2 : 1;

    for (size_t y = 0; y < height; y++) {
        for (size_t x = 0; x < width; x++) {

            uint16_t val = data [stride * y + x];
            assert (! (val >> bit_depth));

            fwrite (& val, 1, bytes_out, file);
        }
    }
}

void
write_yuv_t::write (const uint16_t *y, const uint16_t *u, const uint16_t *v,
                    size_t width, size_t height, size_t stride,
                    unsigned bit_depth, bool ss_x, bool ss_y, bool monochrome)
{
    write_plane (handle, y, width, height, stride, bit_depth);

    if (! monochrome) {
        size_t uv_width = (width + ss_x) >> ss_x;
        size_t uv_height = (height + ss_y) >> ss_y;
        write_plane (handle, u, uv_width, uv_height, stride, bit_depth);
        write_plane (handle, v, uv_width, uv_height, stride, bit_depth);
    }

    fflush (handle);
}
