/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#line 8 "av1src/av1_base.cc"

#include <math.h>

#include "av1_dec_command_line.hh"
#if WRITE_YUV
#include "write_yuv.hh"
#endif // WRITE_YUV

#include <libgen.h>

char *realpath(const char *path, char *resolved_path);

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <inttypes.h>

int g_useful=0;

const char *current_file;
long        current_file_size;

struct csv_cell_t
{
    csv_cell_t ()
        : set (false)
    {}

    void reset ()
    {
        values.fill (0);
        set = false;
    }

    bool                                 set;
    std::array<unsigned, MAX_CSV_VALUES> values;
};

static std::array<csv_cell_t, CSV_FIELD_COUNT> csv_row;

FILE *csvFileHandle=NULL;

int readFileList(const char *fileListFile, const char *** streamv_out, int *streamc_out);
void cleanupFileList(const char **streamv, int streamc);

void init_profile(void);

#define MAX(a,b) ((a)>(b)?(a):(b))

uint64_t sum_array(const uint32_t *array, size_t len) {
  uint64_t total = 0;
  for (size_t i = 0; i < len; ++ i) {
    total += array[i];
  }
  return total;
}

void print_array(char* name, const uint32_t *array, size_t len) {
  printf("%s = {", name);
  for (size_t i = 0; i < len; i++) {
    const char* delimiter = (i == 0) ? "" : ", ";
    printf("%s%d", delimiter, array[i]);
  }
  printf("}\n");
}

static void decode_data(int fcount, int ftotal, const char *fname,
                        const uint8 *data, size_t len, bool is_obu,
                        const DecCommandLine &cmd_line)
{
  TOPLEVEL_T *b = new TOPLEVEL_T();
  b->global_data = new GLOBAL_DATA_T();

  IVF_FILE_HEADER_T file_hdr;

  hevc_parse_global_data (b, b->global_data,
                          cmd_line.annex_b,
                          is_obu,
                          cmd_line.operating_point,
                          cmd_line.lst_anchor_frames,
                          cmd_line.lst_tile_list_obus,
                          cmd_line.no_film_grain,
                          cmd_line.frame_rate);

  // If the stream doesn't have any timing information, we don't
  // actually know the (decode or display) frame rate. The spec has
  // some weasel words in E3.3 about what to do when timing
  // information isn't available in the bitstream. If the user has
  // been helpful and passed a frame rate on the command line, we'll
  // use that. Otherwise, we default to 1 fps (defined in
  // av1_dec_command_line.cc). We always set num_units_in_display_tick
  // to 1.
  assert (cmd_line.frame_rate > 0);

  b->global_data->input_length = len;
  b->global_data->startOfFile = 1;
  b->global_data->max_num_oppoints = cmd_line.max_num_oppoints;

  // This is an attempt to flush out any bugs where we use
  // current_operating_point_id before it gets set in read_sequence_header.
  b->global_data->current_operating_point_id = 255;

  bitstream_init (b, data, len);

  size_t frame_limit = (cmd_line.frame_limit < 0 ?
                        (1 << 20) :
                        cmd_line.frame_limit);

  size_t frame = 0;

  if (is_obu) {
    while (bitstream_current_position(b) < len && frame < frame_limit) {
      int bytes_remaining = (uint64_t)len - bitstream_current_position(b);
      hevc_parse_decode_temporal_unit (b, NULL, bytes_remaining);
      ++ frame;
    }
  } else {
    hevc_parse_ivf_file_header(b, &file_hdr);
    while (frame< b->global_data->frame_cnt && frame < frame_limit) {
      IVF_FRAME_HEADER_T frame_hdr;
      hevc_parse_ivf_frame_header(b,&frame_hdr,0);
      hevc_parse_decode_temporal_unit(b, NULL, frame_hdr.frame_sz);
      ++ frame;
    }
  }

  if (b->global_data->max_num_oppoints >= 0) {
     printf("Max num oppoints: %d\n", b->global_data->max_num_oppoints);
  }
#if COLLECT_STATS
  if (cmd_line.output_stats) {
    // TODO output these to the CSV
    printf("Raw counters:\n");
    printf("  total_tiles = %d\n", b->global_data->total_tiles);
    print_array("  total_frames", b->global_data->total_frames, 5);
    print_array("  total_frame_bits", b->global_data->total_frame_bits, 5);
    printf("  total_symbols = %d\n", b->global_data->total_symbols);
    printf("  total_tile_bits = %d\n", b->global_data->total_tile_bits);
    printf("  total_bits = %d\n", b->global_data->total_bits);
    printf("  num_frames = %zu\n", frame);
    for (int i = 0; i < NUM_1D_TXFMS; i++) {
      printf("  total_1d_txfms[%d]", i);
      print_array("", b->global_data->total_1d_txfms[i], 5);
    }
    print_array("  total_blocks[intra]", b->global_data->total_blocks[0], BLOCK_SIZES_ALL);
    print_array("  total_blocks[inter]", b->global_data->total_blocks[1], BLOCK_SIZES_ALL);

    printf("  total_blocks = %" PRIu64 "\n",
           sum_array (b->global_data->total_blocks[0], BLOCK_SIZES_ALL) +
           sum_array (b->global_data->total_blocks[1], BLOCK_SIZES_ALL));

    printf("  total_intra_px = %d\n", b->global_data->total_intra_px);
    printf("  total_intrabc_px = %d\n", b->global_data->total_intrabc_px);
    printf("  total_inter_px = %d\n", b->global_data->total_inter_px);
    printf("  total_warp_preds = %d\n", b->global_data->total_warp_preds);

    print_array("  total_txfm_blocks", b->global_data->total_txfm_blocks, TX_SIZES_ALL);

    printf("  num_transforms = %" PRIu64 "\n",
           sum_array (b->global_data->total_txfm_blocks, TX_SIZES_ALL));

    print_array("  total_coeffs", b->global_data->total_coeffs, 7);
    //print_array("  total_deblocks", b->global_data->total_deblocks, 4);
    printf("  total_deblocks_length4 = %d\n", b->global_data->total_deblocks[0]);
    printf("  total_deblocks_length6 = %d\n", b->global_data->total_deblocks[1]);
    printf("  total_deblocks_length8 = %d\n", b->global_data->total_deblocks[2]);
    printf("  total_deblocks_length13 = %d\n", b->global_data->total_deblocks[3]);
    printf("  total_cdefs = %d\n", b->global_data->total_cdefs);
    printf("  total_decoded_px = %d\n", b->global_data->total_decoded_px);
    printf("  total_upscaled_px = %d\n", b->global_data->total_upscaled_px);
    printf("  total_displayed_px = %d\n", b->global_data->total_displayed_px);
    double min_max_display_rate = ((double)b->global_data->min_max_display_rate_x100) / 100.0;
    double min_max_decode_rate = ((double)b->global_data->min_max_decode_rate_x100) / 100.0;
    double min_comp_ratio = ((double)b->global_data->min_comp_ratio_x100) / 100.0;
    printf("  min_max_display_rate = %.1f\n", min_max_display_rate);
    printf("  min_max_decode_rate = %.1f\n", min_max_decode_rate);
    printf("  min_comp_ratio = %.1f\n", min_comp_ratio);
    printf("  min_superres_denom = %d\n", b->global_data->min_superres_denom);
    printf("  total_wiener_px = %llu\n", (unsigned long long) b->global_data->total_wiener_px);
    printf("  total_selfguided_px = %llu\n", (unsigned long long) b->global_data->total_selfguided_px);
    printf("\n");

    printf("Derived statistics:\n");

    // Calculate the smallest number of pixels covered by any particular transform size
    int min_txfm_px = INT_MAX;
    for (int i = 0; i < TX_SIZES_ALL; i++) {
      int txw = tx_size_wide[i];
      int txh = tx_size_high[i];
      int num = b->global_data->total_txfm_blocks[i];
      int txfm_px = num * txw * txh;
      if (txfm_px < min_txfm_px) {
        min_txfm_px = txfm_px;
      }
    }
    printf("  min_txfm_px = %d\n", min_txfm_px);

    double bits_per_frame = (double)b->global_data->total_bits / (double)frame;
    double bits_per_dec_pixel = (double)b->global_data->total_tile_bits / (double)b->global_data->total_decoded_px;
    double bits_per_disp_pixel = (double)b->global_data->total_tile_bits / (double)b->global_data->total_displayed_px;

    double symbols_per_frame = (double)b->global_data->total_symbols / (double)frame;
    double symbols_per_dec_pixel = (double)b->global_data->total_symbols / (double)b->global_data->total_decoded_px;
    double symbols_per_disp_pixel = (double)b->global_data->total_symbols / (double)b->global_data->total_displayed_px;

    double tiles_per_frame = (double)b->global_data->total_tiles / (double)frame;
    double symbols_per_bit = (double)b->global_data->total_symbols / (double)b->global_data->total_tile_bits;
    double overhead = 1.0 - ((double)b->global_data->total_tile_bits / (double)b->global_data->total_bits);

    printf("  bits_per_frame = %.1f\n", bits_per_frame);
    printf("  max_frame_bits = %lu\n", (unsigned long) b->global_data->max_frame_bits);
    printf("  bits_per_decoded_pixel = %5.3f\n", bits_per_dec_pixel);
    printf("  bits_per_displayed_pixel = %5.3f\n", bits_per_disp_pixel);
    printf("\n");
    printf("  symbols_per_frame = %.1f\n", symbols_per_frame);
    printf("  symbols_per_decoded_pixel = %5.3f\n", symbols_per_dec_pixel);
    printf("  symbols_per_displayed_pixel = %5.3f\n", symbols_per_disp_pixel);
    printf("\n");
    printf("  total_inter_preds = %lu\n", (unsigned long) b->global_data->total_inter_preds);
    printf("  tiles_per_frame = %5.2f\n", tiles_per_frame);
    printf("  symbols_per_bit = %5.3f\n", symbols_per_bit);
    printf("  overhead = %5.1f%%\n", overhead * 100.);
    printf("  (here 'overhead' means (#non-tile data bits / total #bits).\n");
    printf("   non-tile data includes frame headers, metadata, etc.)\n");
    printf("\n");

    printf("  Average frame size (in bits) for each frame type:\n");
    const char* frame_type_names[5] = {"key", "inter", "intra_only", "s", "show_existing"};
    for (int i = 0; i < 5; i++) {
      double avg_size = 0;
      if (b->global_data->total_frames[i] > 0) {
        avg_size = (double)b->global_data->total_frame_bits[i] / (double)b->global_data->total_frames[i];
      }
      printf("    bits_per_%s_frame = %.1f\n", frame_type_names[i], avg_size);
    }
    printf("\n");

    printf(" Largest ratio of tile sizes within a frame:\n");
    printf("  max_tile_size_ratio = %5.2f\n",
           (double)b->global_data->max_tile_ratio_num / (double)b->global_data->max_tile_ratio_denom);
    printf("\n");

    int64_t total_txfms = 0;
    for (int i = 0; i < TX_SIZES_ALL; i++) {
      total_txfms += b->global_data->total_txfm_blocks[i];
    }
    int64_t total_coeffs = 0;
    for (int i = 0; i < 7; i++) {
      total_coeffs += b->global_data->total_coeffs[i];
    }
    int64_t total_nz_coeffs = total_coeffs - b->global_data->total_coeffs[0];

    double coeffs_per_txfm = (double)total_coeffs / (double)total_txfms;
    double nz_coeffs_per_txfm = (double)total_nz_coeffs / (double)total_txfms;
    double nz_coeff_percent = (double)total_nz_coeffs * 100. / (double)total_coeffs;

    printf("  nz_coeffs = %" PRId64 "\n", total_nz_coeffs);
    printf("  coeffs_per_px = %5.3f\n", total_coeffs / (double)b->global_data->total_decoded_px);
    printf("  nz_coeffs_per_px = %5.3f\n", total_nz_coeffs / (double)b->global_data->total_decoded_px);
    printf("  coeffs_per_txfm = %5.2f\n", coeffs_per_txfm);
    printf("  nz_coeffs_per_txfm = %5.2f\n", nz_coeffs_per_txfm);
    printf("  nz_coeff_percent = %5.1f%%\n", nz_coeff_percent);
    printf("\n");
  }
#endif // COLLECT_STATS

  delete b->global_data;
  delete b;
  printf("Processed file #%d: %s                                      \n",fcount,fname);
}

static bool is_obu_file(const char* fname) {
  size_t len = strlen(fname);
  if (len < 4) return 0;

  const char *suffix = fname + (len-4);
  const char *obu_ext = ".obu";
  return strcmp(suffix, obu_ext) ? false : true;
}

// Read the contents of the file at filename
static size_t
slurp (const char                  *filename,
       std::unique_ptr<uint8_t []> *data)
{
    assert (data);

    std::unique_ptr <FILE, decltype(&fclose)>
        fid (fopen (filename, "rb"), & fclose);

    if (! fid) {
        printf ("Input file %s not found\n\n", filename);
        exit (1);
    }

    fseek (fid.get (), 0, SEEK_END);
    current_file_size = ftell (fid.get ());

    // ftell should never fail (fid is always a valid seekable stream).
    assert (current_file_size > 0);

    size_t len = current_file_size;

    data->reset (new uint8_t [len]);
    if (! data->get ()) {
        fprintf (stderr, "Failed to allocate %ld bytes to read file `%s'.\n",
                 current_file_size, filename);
        abort ();
    }

    fseek (fid.get (), 0, SEEK_SET);

    size_t num_bytes = fread (data->get (), 1, current_file_size, fid.get ());
    if (num_bytes != len) {
        fprintf (stderr, "Failed to read %ld bytes from file `%s'.\n",
                 current_file_size, filename);
    }

    return len;
}

static void read_file (int                   fcount,
                       int                   ftotal,
                       const char           *fname,
                       const DecCommandLine &cmd_line)
{
  printf ("Opening %s%s", fname, debugMode? "\n" : "\r");

  std::unique_ptr<uint8_t []> data;
  size_t len = slurp (fname, & data);

  current_file = fname;
  g_useful = 0;

  decode_data (fcount, ftotal, fname,
               data.get (), len, is_obu_file (fname), cmd_line);
}

#define path_to_ref "../../../../work/hevc/HM-9.1rc1/bin/"
FILE *ref_log=NULL;
FILE *val_fid=NULL;
int val_count=0;
void log_init(void) {
  ref_log=fopen(path_to_ref "ref_bins.txt","rb");
}

void decode_log(int r,int off,int s,int v) {
  char buf[256];
  char ref_buf[256];
  //static int count=0;
  //printf("Check%d\n",count++);
  sprintf(buf,"%d %d %d %d",r,off,s,v);
  assert(ref_log);
  char *ch = fgets(ref_buf,256,ref_log);
  (void) ch;
  if (strncmp(buf,ref_buf,strlen(buf))==0)
    return;
  if (debugMode) printf("Ref=%s\nOurs=%s\n",ref_buf,buf);
  assert(0);
}

void validate_stop(void) {
  if (debugMode) printf("Stop");
}

void validate(int x) {
  if (!val_fid) {
    val_fid = fopen("validate_c.txt","wb");
    assert(val_fid);
  }
  fprintf(val_fid,"%d %d\n",val_count,x);
  // if (val_count==413717) validate_stop();
  //fflush(val_fid);
  val_count++;
}

const char *argon_basename(const char *p) {
  const char *pp1;
  const char *pp2=p;
  for (pp1=p; *pp1; pp1++) {
    if ( (*pp1=='/' || *pp1=='\\') && pp1[1]) pp2=pp1+1;
  }
  return pp2;
}

char *argon_dirname(char *p) {
  char *pp1;
  char *pp2=NULL;
  for (pp1=p; *pp1; pp1++) {
    if ((*pp1=='/' || *pp1=='\\') && pp1[1]) pp2=pp1;
  }
  if (!pp2) return "";
  *pp2='\0';
  return p;
}

char fullPathName[32768];

static void csv_newcvs (FILE *file)
{
    fprintf (file, "\"%s\",%zu",
             argon_basename (current_file), current_file_size);

    assert (csv_row.size () == CSV_FIELD_COUNT);
    for (size_t i = 0; i < CSV_FIELD_COUNT; ++ i) {
        csv_cell_t &cell = csv_row [i];

        for (size_t j = 0; j < csv_fields [i].dimension; ++ j) {
            fprintf (file, ",%u", cell.values [j]);
        }

        // Don't reset CSV_CVS_NUMBER (we rely on summing '1's with
        // CSV_OP_SUM to get the CVS count).
        if (i != CSV_CVS_NUMBER)
            cell.reset ();
    }
    fprintf (file, "\n");
}

static unsigned binop_exact (unsigned a, unsigned b)
{
    // Check that a=b. It's a programming error if this doesn't hold. Return
    // either.
    assert (a == b);
    return a;
}

static unsigned binop_plus (unsigned a, unsigned b)
{
    return a + b;
}

static unsigned binop_max (unsigned a, unsigned b)
{
    return MAX (a, (unsigned) b);
}

void CSV (int id, ...)
{
    if (!csvFileHandle) return;

    if (id == CSV_NEWCVS || id == CSV_NEWSTREAM) {
        csv_newcvs (csvFileHandle);
    }
    else {
        int values[MAX_CSV_VALUES];
        va_list vl;
        va_start (vl, id);
        for (unsigned i = 0; i < csv_fields [id].dimension; ++ i) {
            values[i] = va_arg(vl, int);
            assert (values [i] >= 0);
        }
        va_end(vl);

        const csv_field_t &field = csv_fields [id];
        csv_cell_t &cell = csv_row [id];

        assert (field.dimension <= MAX_CSV_VALUES);

        if (! cell.set) {
            for (unsigned i = 0; i < field.dimension; ++ i) {
                cell.values [i] = values [i];
            }
            cell.set = true;
        } else {
            unsigned (* binop) (unsigned, unsigned);
            switch (field.binop) {
            case CSV_OP_EXACT:
                binop = binop_exact;
                break;
            case CSV_OP_SUM:
                binop = binop_plus;
                break;
            case CSV_OP_MAX:
                binop = binop_max;
                break;
            default:
                assert (0);
            }

            for (unsigned i = 0; i < field.dimension; ++ i) {
                cell.values [i] = binop (cell.values [i], values [i]);
            }
        }
    }
}

#if WRITE_YUV
std::unique_ptr<write_yuv_t> yuv_writer;
#endif

int main(int argc, char *argv[])
{
  log_init();

  DecCommandLine cmd_line (argc, argv);

  debugMode = cmd_line.verbose;
  g_checkStreams = cmd_line.check_streams;

  const char **streamv;
  int          streamc;

  if (cmd_line.file_list) {
    streamv=0; streamc=0;
    std::cerr << "File list: " << cmd_line.file_list << "\n";
    int res=readFileList(cmd_line.file_list, &streamv, &streamc);
    if (res!=0) { cleanupFileList(streamv, streamc); return res; }
  }
  else {
    streamv = & cmd_line.inputs [0];
    streamc = cmd_line.inputs.size ();
  }

#if WRITE_YUV
  yuv_writer.reset (new write_yuv_t (cmd_line.yuv_output_file));
#endif

  //Check all the input files exist
  for (int i=0; i<streamc; i++) {
    //Ignore any -b options (for compatibility with reference decoder)
    if (strcmp(streamv[i],"-b")==0) continue;
    FILE *f=fopen(streamv[i], "rb");
    if (!f) {
      printf("Can't open input file `%s' - exiting.\n", streamv[i]);
      return 1;
    }
    fclose(f);
  }
  //Open the CSV file if applicable
  if (cmd_line.csv_file) {
    csvFileHandle=fopen(cmd_line.csv_file,"w");
    if (!csvFileHandle) {
        printf("Can't open CSV file %s for output\n",cmd_line.csv_file);
      return 1;
    }
    if (cmd_line.csv_header_only && cmd_line.csv_headerless) {
      printf("Can't combine -H and --csv-header-only options\n");
      return 1;
    }
    if (! cmd_line.csv_headerless) {
      fprintf(csvFileHandle,"\"Stream name\",\"File size (bytes)\"");

      for (unsigned i = 0; i < CSV_FIELD_COUNT; ++ i) {
          for (unsigned j = 0; j < csv_fields[i].dimension; ++ j) {
              fprintf (csvFileHandle, ",\"%s\"", csv_fields [i].header [j]);
          }
      }
      fprintf(csvFileHandle,"\n");
    }
    if (cmd_line.csv_header_only) {
      // Exit after writing the CSV header
      return 0;
    }
  }
  if (cmd_line.max_num_oppoints >= 0 &&
      streamc != 1) {
     printf("Cannot use --max-num-oppoints switch with multiple streams\n");
     return 1;
  }
  for (int i=0; i<streamc; i++) {
    //Ignore any -b options (for compatibility with reference decoder)
    if (strcmp(streamv[i],"-b")==0) continue;
    read_file (i + 1, streamc, streamv [i], cmd_line);
    CSV(CSV_NEWSTREAM,0);
  }
  if (csvFileHandle) {
    fclose(csvFileHandle);
  }

  // If we failed any checks, stop here with an error code.
  if (some_check_failed) {
    fflush (stdout);
    fprintf (stderr, "One or more checks failed. Exiting with status 2.\n");
    return 2;
  }

  //We must clean this up AFTER profiling has been done, as it needs streamv and streamc.
  if (cmd_line.file_list) {
    cleanupFileList(streamv, streamc);
  }
  return 0;
}

/* Called when a frame is output from the decoded picture buffer.
These will be returned in decode order. */
void video_output (TOPLEVEL_T *b,
                   int bit_depth, int w, int h,
                   int pitch, int srcX, int srcY,
                   int ss_x, int ss_y, int is_monochrome)
{
  int sw = 1 << ss_x;
  int sh = 1 << ss_y;

  static int frame=0;
  uint16 *y=&b->video_stream.frame_data.rframe[0][srcY * pitch + srcX];
  int srcYss = (srcY+sh-1)/sh;
  int srcXss = (srcX+sw-1)/sw;
  uint16 *u=&b->video_stream.frame_data.rframe[1][srcYss * pitch + srcXss];
  uint16 *v=&b->video_stream.frame_data.rframe[2][srcYss * pitch + srcXss];

#if WRITE_YUV
  assert (yuv_writer);
  assert (w >= 0);
  assert (h >= 0);
  assert (pitch >= w);
  assert (bit_depth == 8 || bit_depth == 10 || bit_depth == 12);

  yuv_writer->write (y, u, v, w, h, pitch,
                     bit_depth, ss_x, ss_y, is_monochrome);
#endif

  if (debugMode) {
    printf("Frame %d (Y%d UV%d) (post-crop size: %dx%d)\n",
           frame++, bit_depth, bit_depth, w, h);

    // This flush is useful if you're writing to some log file
    // with output redirection: we wouldn't be line buffered
    // otherwise.
    fflush (stdout);
  }
}

int readFileList(const char *fileListFile, const char *** streamv_out, int *streamc_out) {
  FILE *fileListHandle;
  if (strcmp(fileListFile, "-")==0) {
    fileListHandle=stdin;
  }
  else {
    fileListHandle=fopen(fileListFile, "r");
    if (!fileListHandle) {
      fprintf(stderr, "Could not open file list %s\n", fileListFile);
      return 1;
    }
  }
  //Set up initial sizes and buffers
  int streamvSize=50;
  int bufSize=1024;
  int streamc=0;
  const char **streamv=(const char **)malloc(sizeof(const char *) * streamvSize);
  if (!streamv) return 1;
  memset(streamv,0,sizeof(const char *) * streamvSize);
  char *buf=(char *)malloc(sizeof(char)*bufSize);
  if (!buf) return 1;
  while (!feof(fileListHandle)) {
    //Allocate the buffer
    //Read a line
    buf[bufSize-1]=0xFF;
    buf[bufSize-2]=0xFF;
    if (!fgets(buf, bufSize, fileListHandle)) break;
    if (buf[0]=='\0') break;
    //Did we overflow the buffer?
    while (buf[bufSize-1]==0 && buf[bufSize-2]!='\n') {
      //Double the buffer size
      buf=(char *)realloc(buf, sizeof(char)*bufSize*2);
      if (!buf) return 1;
      //Read again
      buf[bufSize*2-1]=0xFF;
      buf[bufSize*2-2]=0xFF;
      if (!fgets(buf+bufSize-1, bufSize+1, fileListHandle)) {
        buf[bufSize-1]='\0';
        break;
      }
      //Update the buffer size
      bufSize*=2;
    }
    //Now we want to make a buffer to hold just the string length we read
    size_t pathlen=strlen(buf);
    //Remove the LF and potentially a CR
    if (buf[pathlen-1]=='\n') pathlen--;
    if (buf[pathlen-1]=='\r') pathlen--;
    buf[pathlen]='\0';
    //Make the new buffer
    char *pathbuf=(char *)malloc(sizeof(char)*(pathlen+1));
    if (!pathbuf) return 1;
    //Copy the string into the new buffer
    strcpy(pathbuf, buf);
    //Do we need to reallocate streamv?
    if (streamc>=streamvSize) {
      streamv=(const char **)realloc(streamv, sizeof(const char *) * streamvSize * 2);
      if (!streamv) return 1;
      memset(streamv+streamvSize,0,sizeof(const char *) * streamvSize);
      streamvSize*=2;
    }
    streamv[streamc++]=pathbuf;
  }
  //Shrink streamv to size
  if (streamvSize>streamc) streamv=(const char **)realloc(streamv, sizeof(const char *) * streamc);
  free((void *)buf);
  if (fileListHandle!=stdin) {
    fclose(fileListHandle);
  }
  *streamv_out = streamv;
  *streamc_out = streamc;
  return 0;
}

void cleanupFileList(const char **streamv, int streamc) {
    if (!streamv) return;
    int i;
    for (i=0; i<streamc; ++i) {
      if (streamv[i]) free((void *)streamv[i]);
    }
    free((void *)streamv);
}

void hevc_parse_record(void *b, void *b2, int id, int num, ... ) {
  static FILE *record_fid=NULL;
  if (!record_fid) {
    record_fid = fopen("record.txt","wb");
  }
  assert(record_fid);
  va_list args;
  va_start(args, num);
  int i;
  fprintf(record_fid,"\n%d %d ",id, num);
  for(i=0;i<num;i++) {
    int p = va_arg(args,int);
    fprintf(record_fid,"%d ",p);
  }
  va_end(args);
}
