/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

b_skip_coeff() {
  context_fn()
  write_symbol(tile_mbskip_cdf[ctxIdxOffset], syntax, 2)
}

b_skip_mode() {
  context_fn()
  write_symbol(tile_skip_mode_cdf[ctxIdxOffset], syntax, 2)
}

b_tx_depth() {
  context_fn()
  CHECK(maxTxDepth >= 0 && maxTxDepth < MAX_TX_CATS, "Invalid maxTxDepth")
  write_symbol(tile_tx_cdfs[maxTxDepth][ctxIdxOffset], syntax, depthCdfSize)
}

b_intra_tx_type() {
  cdfIdx = ext_tx_set_index_intra[eset]
  ASSERT(cdfIdx >= 0, "Invalid intra tx set")
  symbol = ext_tx_set_ind[eset][syntax]
  write_symbol(tile_intra_ext_tx_cdf[cdfIdx][txsize_sqr_map[txSz]][intraDir], symbol, num_ext_tx_set[eset])
}

b_inter_tx_type() {
  cdfIdx = ext_tx_set_index_inter[eset]
  ASSERT(cdfIdx >= 0, "Invalid inter tx set")
  symbol = ext_tx_set_ind[eset][syntax]
  write_symbol(tile_inter_ext_tx_cdf[cdfIdx][txsize_sqr_map[txSz]], symbol, num_ext_tx_set[eset])
}

b_txfm_split() {
  context_fn()
  write_symbol(tile_txfm_partition_cdf[ctxIdxOffset], syntax, 2)
}

b_use_obmc() {
  write_symbol(tile_obmc_cdf[ sb_size ], syntax, 2)
}

b_motion_mode() {
  write_symbol(tile_motion_mode_cdf[sb_size], syntax, MOTION_MODES)
}

b_all_zero() {
  context_fn()
  write_symbol(tile_txb_skip_cdfs[txSzCtx][ctxIdxOffset], syntax, 2)
}


b_eob_pt_16() {
  context_fn()
  write_symbol(tile_eob_flag_16_cdfs[ptype][ctxIdxOffset], syntax, 5)
}

b_eob_pt_32() {
  context_fn()
  write_symbol(tile_eob_flag_32_cdfs[ptype][ctxIdxOffset], syntax, 6)
}

b_eob_pt_64() {
  context_fn()
  write_symbol(tile_eob_flag_64_cdfs[ptype][ctxIdxOffset], syntax, 7)
}

b_eob_pt_128() {
  context_fn()
  write_symbol(tile_eob_flag_128_cdfs[ptype][ctxIdxOffset], syntax, 8)
}

b_eob_pt_256() {
  context_fn()
  write_symbol(tile_eob_flag_256_cdfs[ptype][ctxIdxOffset], syntax, 9)
}

b_eob_pt_512() {
  context_fn()
  write_symbol(tile_eob_flag_512_cdfs[ptype][ctxIdxOffset], syntax, 10)
}

b_eob_pt_1024() {
  context_fn()
  write_symbol(tile_eob_flag_1024_cdfs[ptype][ctxIdxOffset], syntax, 11)
}

b_eob_extra() {
  context_fn()
  write_symbol(tile_eob_extra_cdfs[txSzCtx][ptype][ctxIdxOffset], syntax, 2)
}
b_coeff_base() {
  context_fn()
  syntax = write_symbol(tile_coeff_base_cdfs[txSzCtx][ptype][ctxIdxOffset],syntax,4)
}
b_coeff_base_eob() {
  context_fn()
  syntax = write_symbol(tile_coeff_base_eob_cdfs[txSzCtx][ptype][ctxIdxOffset],syntax,3)
}
b_dc_sign() {
  context_fn()
  write_symbol(tile_dc_sign_cdfs[ptype][ctxIdxOffset], syntax, 2)
}
b_coeff_br() {
  context_fn()
    write_symbol(tile_coeff_br_cdfs[Min(txSzCtx,TX_32X32)][ptype][ctxIdxOffset], syntax, BR_CDF_SIZE)
}

b_coef_extra_bits() {
  write_symbol_noadapt(coef_extrabit_cdfs[cat][cdf_index], syntax, 1 << bits)
}


b_has_palette_y() {
  context_fn()
  write_symbol(tile_palette_y_mode_cdf[ bsizeCtx ][ ctxIdxOffset ], syntax, 2)
}

b_has_palette_uv() {
  context_fn()
  write_symbol(tile_palette_uv_mode_cdf[ ctxIdxOffset ], syntax, 2)
}

b_palette_size_y() {
  context_fn()
  write_symbol(tile_palette_y_size_cdf[ctxIdxOffset], syntax, PALETTE_SIZES)
}

b_palette_size_uv() {
  context_fn()
  write_symbol(tile_palette_uv_size_cdf[ctxIdxOffset], syntax, PALETTE_SIZES)
}

b_color_idx_y() {
  context_fn()
  write_symbol(tile_palette_y_color_cdf[PaletteSizeY - 2][ctxIdxOffset], syntax, PaletteSizeY)
}

b_color_idx_uv() {
  context_fn()
  write_symbol(tile_palette_uv_color_cdf[PaletteSizeUV - 2][ctxIdxOffset], syntax, PaletteSizeUV)
}

b_use_intrabc() {
  write_symbol(tile_intrabc_cdf, syntax, 2)
}

b_use_filter_intra() {
  context_fn()
  write_symbol(tile_filter_intra_cdfs[sb_size], syntax, 2)
}

b_filter_intra_mode() {
  context_fn()
  write_symbol(tile_filter_intra_mode_cdf, syntax, FILTER_INTRA_MODES)
}


b_angle_delta_y() {
  context_fn()
  write_symbol(tile_angle_delta_cdf[y_mode - V_PRED], syntax, (2*MAX_ANGLE_DELTA + 1))
}

b_angle_delta_uv() {
  context_fn()
  write_symbol(tile_angle_delta_cdf[uv_mode - V_PRED], syntax, (2*MAX_ANGLE_DELTA + 1))
}


b_intra_mode_y() {
  context_fn()
  write_symbol(tile_y_mode_cdf[ctxIdxOffset], syntax, INTRA_MODES)
}

b_default_intra_mode_y() {
  abovectx = above_intra_mode_ctx[mi_col]
  leftctx = left_intra_mode_ctx[mi_row & mi_mask2]

  write_symbol(tile_kf_y_cdf[abovectx][leftctx], syntax, INTRA_MODES)
}

b_intra_mode_uv() {
  cflAllowed = is_cfl_allowed(sb_size)
  nsymbs = INTRA_MODES + cflAllowed
  syntax = write_symbol(tile_uv_mode_cdf[cflAllowed][y_mode], syntax, nsymbs)
}

b_joint_type() {
  write_symbol(tile_joint_cdf[nmv_ctx], syntax, MV_JOINTS)
}

b_mv_class() {
  write_symbol(tile_mvcomp_classes_cdf[nmv_ctx][mvcomp], syntax, MV_CLASSES)
}

b_mvcomp_class0_bits() {
  write_symbol(tile_class0_cdf[nmv_ctx][mvcomp], syntax, 2)
}

b_mvcomp_class0_fp() {
  write_symbol(tile_mvcomp_class0_fp_cdf[nmv_ctx][mvcomp][mvcomp_class0_bits], syntax, MV_FP_SIZE)
}

b_mvcomp_fp() {
  write_symbol(tile_mvcomp_fp_cdf[nmv_ctx][mvcomp], syntax, MV_FP_SIZE)
}

b_drl_mode() {
  context_fn()
  write_symbol(tile_drl_cdf[ctxIdxOffset], syntax, 2)
}


b_inter_mode() {
  // TODO may be better to express this as 3 booleans?  is_newmv, is_globalmv, is_refmv?
  // Would simplify profile generation for inter_mode
  context_fn()
  modectx = ctxIdxOffset & NEWMV_CTX_MASK
  v = syntax==NEWMV ? 0 : 1
  write_symbol(tile_newmv_cdf[modectx], v, 2)
  if (v) {
    modectx = (ctxIdxOffset >> GLOBALMV_OFFSET) & GLOBALMV_CTX_MASK
    v = syntax==GLOBALMV ? 0 : 1
    write_symbol(tile_globalmv_cdf[modectx], v, 2)
    if (v) {
      modectx = (ctxIdxOffset >> REFMV_OFFSET) & REFMV_CTX_MASK
      v = syntax==NEARESTMV ? 0 : 1
      write_symbol(tile_refmv_cdf[modectx], v, 2)
    }
  }
}

b_inter_compound_mode() {
  context_fn()
  write_symbol(tile_inter_compound_mode_cdf[ctxIdxOffset],
               syntax - NEAREST_NEARESTMV, INTER_COMPOUND_MODES)
}

b_compound_group_idx() {
  context_fn()
  write_symbol(tile_compound_group_idx_cdf[ctxIdxOffset], syntax, 2)
}

b_compound_idx() {
  context_fn()
  write_symbol(tile_compound_idx_cdf[ctxIdxOffset], syntax, 2)
}

b_compound_mask_type() {
  context_fn()
  write_symbol(tile_compound_mask_type_cdf[sb_size], syntax, 2)
}


b_interintra() {
  size_group = size_group_lookup[sb_size]
  write_symbol(tile_interintra_cdf[size_group], syntax, 2)
}

b_interintra_mode() {
  size_group = size_group_lookup[sb_size]
  write_symbol(tile_interintra_mode_cdf[size_group], syntax, INTERINTRA_MODES)
}

b_wedge_idx() {
  write_symbol(tile_wedge_idx_cdf[bsize], syntax, 16)
}

b_wedge_interintra() {
  write_symbol(tile_wedge_interintra_cdf[sb_size], syntax, 2)
}

// read_switchable_filter_type
b_interp_filter() {
  context_fn()
  write_symbol(tile_switchable_interp_cdf[ctxIdxOffset], syntax, SWITCHABLE_FILTERS)
}

b_mvcomp_class0_hp() {
  write_symbol(tile_class0_hp_cdf[nmv_ctx][mvcomp], syntax, 2)
}

b_mvcomp_hp() {
  write_symbol(tile_hp_cdf[nmv_ctx][mvcomp], syntax, 2)
}

b_mvcomp_bits() {
  for (mvcompi = 0; mvcompi < numbits; mvcompi++) {
    write_symbol(tile_mvcomp_bits_cdf[nmv_ctx][mvcompi][mvcomp], (syntax>>mvcompi)&1, 2)
  }
}

b_comp_mode() {
  context_fn()
  write_symbol(tile_comp_inter_cdf[ctxIdxOffset], syntax, 2)
}

b_single_ref_p1() {
  context_fn()
  write_symbol(tile_single_ref_cdf[ctxIdxOffset][0], syntax, 2)
}

b_single_ref_p2() {
  context_fn()
  write_symbol(tile_single_ref_cdf[ctxIdxOffset][1], syntax, 2)
}

b_single_ref_p3() {
  context_fn()
  write_symbol(tile_single_ref_cdf[ctxIdxOffset][2], syntax, 2)
}

b_single_ref_p4() {
  context_fn()
  write_symbol(tile_single_ref_cdf[ctxIdxOffset][3], syntax, 2)
}

b_single_ref_p5() {
  context_fn()
  write_symbol(tile_single_ref_cdf[ctxIdxOffset][4], syntax, 2)
}

b_single_ref_p6() {
  context_fn()
  write_symbol(tile_single_ref_cdf[ctxIdxOffset][5], syntax, 2)
}

b_comp_ref_p() {
  context_fn()
  write_symbol(tile_comp_ref_cdf[ctxIdxOffset][0], syntax, 2)
}
b_comp_ref_p1() {
  context_fn()
  write_symbol(tile_comp_ref_cdf[ctxIdxOffset][1], syntax, 2)
}
b_comp_ref_p2() {
  context_fn()
  write_symbol(tile_comp_ref_cdf[ctxIdxOffset][2], syntax, 2)
}
b_comp_bwdref_p() {
  context_fn()
  write_symbol(tile_comp_bwdref_cdf[ctxIdxOffset][0], syntax, 2)
}
b_comp_bwdref_p1() {
  context_fn()
  write_symbol(tile_comp_bwdref_cdf[ctxIdxOffset][1], syntax, 2)
}

b_comp_ref_type() {
  context_fn()
  write_symbol(tile_comp_ref_type_cdf[ctxIdxOffset], syntax, 2)
}

b_uni_comp_ref_p() {
  context_fn()
  write_symbol(tile_uni_comp_ref_cdf[ctxIdxOffset][0], syntax, 2)
}
b_uni_comp_ref_p1() {
  context_fn()
  write_symbol(tile_uni_comp_ref_cdf[ctxIdxOffset][1], syntax, 2)
}
b_uni_comp_ref_p2() {
  context_fn()
  write_symbol(tile_uni_comp_ref_cdf[ctxIdxOffset][2], syntax, 2)
}

b_is_inter() {
  context_fn()
  write_symbol(tile_intra_inter_cdf[ctxIdxOffset], syntax, 2)
}

b_mvcomp_sign() {
  write_symbol(tile_sign_cdf[nmv_ctx][mvcomp], syntax, 2)
}

b_pred_flag() {
  context_fn()
  write_symbol(tile_segment_pred_cdf[ctxIdxOffset], syntax, 2)
}

b_u(numbits) {
  for(unifi=0;unifi<numbits;unifi++)
    write_bit((syntax>>(numbits-1-unifi))&1)
}

// Used for binary symbols with a simple, fixed probability
b_bool(prob) {
  write_bool(prob,syntax)
}

b_partition() {
  context_fn()
  uint16 tmp_cdf[3]
  {
    if (has_rows && has_cols) {
      numPartitionTypes = num_partition_types(bsize)
      write_symbol(tile_partition_cdf[ctxIdxOffset], syntax, numPartitionTypes)
    }
    else if (!has_rows && has_cols) {
      get_partition_vertlike_cdf(tile_partition_cdf[ctxIdxOffset], tmp_cdf, bsize)
      write_symbol_noadapt(tmp_cdf, syntax == PARTITION_SPLIT, 2)
    //else if (has_rows && !has_cols)
    } else if ( has_rows) {
      get_partition_horzlike_cdf(tile_partition_cdf[ctxIdxOffset], tmp_cdf, bsize)
      write_symbol_noadapt(tmp_cdf, syntax == PARTITION_SPLIT, 2)
    }
  }
}

b_coded_id() {
  write_symbol(tile_spatial_pred_seg_cdf[cdfNum], syntax, MAX_SEGMENTS)
}

b_abs() {
  write_symbol(tile_delta_q_cdf, syntax, DELTA_Q_SMALL+1)
}
b_abs_lf() {
  if (deltaLfCdfIdx >= 0)
    write_symbol(tile_delta_lf_multi_cdf[deltaLfCdfIdx], syntax, DELTA_LF_SMALL+1)
  else
    write_symbol(tile_delta_lf_cdf, syntax, DELTA_LF_SMALL+1)
}

b_cfl_alpha_signs() {
  write_symbol(tile_cfl_sign_cdf, syntax, CFL_JOINT_SIGNS)
}

b_cfl_alpha() {
  context_fn()
  write_symbol(tile_cfl_alpha_cdf[ctxIdxOffset], syntax, CFL_ALPHABET_SIZE)
}

b_restoration_type() {
  write_symbol(tile_switchable_restore_cdf, syntax, RESTORE_SWITCHABLE_TYPES)
}

b_use_wiener() {
  write_symbol(tile_wiener_cdf, syntax, 2)
}

b_use_sgrproj() {
  write_symbol(tile_sgrproj_cdf, syntax, 2)
}
