/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_INTER 0
// This file contains the code to scale motion vectors and work out where to launch the interpolation filters

inter_predictor(plane, dstX, dstY, startX, startY, w, h, ref, subpelx, subpely, xs, ys, filter_x, filter_y){
  :debug_inter(subpelx,subpely,w,h,0,0) // TODO: Decide what to do with the last two args
                                        // (was previously mvrow, mvcol, but we now don't calculate those
                                        // in the same way)

  convolve_c(plane, dstX, dstY, startX, startY, subpelx, subpely,ref,xs,ys,w,h,filter_x,filter_y)

#if VALIDATE_INTER
  validate(2001)
  validate(startX*16+subpelx)
  validate(startY*16+subpely)
  for(j=0;j<h;j++)
  for(i=0;i<w;i++)
    validate(predSamples[ref][i+dstX][j+dstY])
#endif
}

int scale_pixel_coord(int64 px, int64 scale) {
  // This differs from the other functions in that we project the center
  // of the given pixel location to find the center of the destination pixel.
  //
  // To do this, we need to add on half a pixel before multiplying, then subtract
  // half a pixel after multiplying. These two actions correspond to the two terms
  // in 'off'.
  off = (scale << (SUBPEL_BITS - 1)) - ((1<<REF_SCALE_SHIFT) << (SUBPEL_BITS - 1))
  return ROUND_POWER_OF_TWO_SIGNED(px * scale + off, REF_SCALE_SHIFT - SCALE_EXTRA_BITS)
}

int round_mv_comp_q4(int value) {
  return (value < 0 ? value - 2 : value + 2) / 4
}

int round_mv_comp_q2(int value) {
  return (value < 0 ? value - 1 : value + 1) / 2
}

// block is 0 to 3 for block_idx
// pred_w,pred_h are log2 of width and height in 4-units
// mi_x,mi_y are luma pixel locations
build_inter_predictors_plane(plane, block, bw, bh, basex, basey, pred_w, pred_h, src_row, src_col, mi_x, mi_y, build_for_obmc, bsize, partition) {
  if (use_intrabc) {
    ASSERT(!build_for_obmc, "Intrabc + OBMC should not be able to be combined")
    build_intrabc_predictor(plane, block, basex, basey, pred_w, pred_h, src_row, src_col, mi_x, mi_y, bsize)
    return 0
  }

  //:print 'inter_count,plane,mi_x,mi_y,x,y,block,bwl',inter_count,plane,mi_x,mi_y,x,y,block,bwl
  fullPredW = pred_w
  fullPredH = pred_h
  dst_row = src_row
  dst_col = src_col
  pre_y = mi_y
  pre_x = mi_x

  // The destination and the reference "pointers" must point to an even mi block (MI grid is 4x4 here)
  if (plane_subsampling_y[plane] && (src_row & 0x01) && (mi_size_high[bsize] == 1)) {
    dst_row = src_row - 1
    pre_y -= MI_SIZE
  }
  if (plane_subsampling_x[plane] && (src_col & 0x01) && (mi_size_wide[bsize] == 1)) {
    dst_col = src_col - 1
    pre_x -= MI_SIZE
  }

  // Determine whether we are doing sub8x8 mode
  sub8x8Inter = (((block_size_wide_lookup[bsize] < 8) && plane_subsampling_x[plane]) ||
                 ((block_size_high_lookup[bsize] < 8) && plane_subsampling_y[plane]))
  rowStart = (block_size_high_lookup[bsize] == 4) && plane_subsampling_y[plane] ? -1 : 0
  colStart = (block_size_wide_lookup[bsize] == 4) && plane_subsampling_x[plane] ? -1 : 0
  if (!build_for_obmc && sub8x8Inter) {
    for (row = rowStart; row <= 0; row++) {
      for (col = colStart; col <= 0; col++) {
        if (refframes[0][src_col+col][src_row+row][0] <= INTRA_FRAME) {
          sub8x8Inter = 0
        }
      }
    }
  }

  // If sub8x8 mode, we iterate over the sub blocks
  if (!build_for_obmc && sub8x8Inter) {
    pred_w = block_size_wide_lookup[bsize] >> plane_subsampling_x[plane]
    pred_h = block_size_high_lookup[bsize] >> plane_subsampling_y[plane]
    lumaPredSize = scale_chroma_bsize(bsize, plane_subsampling_x[plane], plane_subsampling_y[plane])
    CHECK((fullPredW == (block_size_wide_lookup[lumaPredSize] >> plane_subsampling_x[plane])) && (fullPredH == (block_size_high_lookup[lumaPredSize] >> plane_subsampling_y[plane])), "We expect this to be true")
   } else {
    rowStart = 0
    colStart = 0
  }

  preRow = src_row + rowStart
  for (idy = 0; idy < fullPredH; idy += pred_h) {
    preCol = src_col + colStart
    for (idx = 0; idx < fullPredW; idx += pred_w) {
      is_compound = refframes[0][preCol][preRow][1] > INTRA_FRAME && !build_for_obmc
      setup_interp_rounding(is_compound)
      y = basey + idy
      x = basex + idx

      for (ref = 0; ref < 1 + is_compound; ref++) {
#if VALIDATE_INTER
        validate(2000)
#endif

        // Chroma motion vector averages the luma motion vectors
        for(comp=0;comp<2;comp++) {
           mv[ref][comp] = mvs[0][preCol][preRow][ref][block][comp]
        }
        //:if inter_count==190:pdb.set_trace()

        // TODO(jkoleszar): This clamping is done in the incorrect place for the
        // scaling case. It needs to be done on the scaled MV, not the pre-scaling
        // MV. Note however that it performs the subsampling aware scaling so
        // that the result is always q4.
        // mv_precision precision is MV_PRECISION_Q4.
        //const MV mv_q4 = clamp_mv_to_umv_border_sb(xd, &mv, bw, bh, pd->subsampling_x, pd->subsampling_y);
        ref_idx = active_ref_idx[refframes[0][preCol][preRow][ref] - LAST_FRAME]

        ssx = plane_subsampling_x[plane]
        ssy = plane_subsampling_y[plane]
        // Calculate scale factors - the corresponding libaom functions are:
        // get_fixed_point_scale_factor() :
        x_scale_fp = ((ref_width[ref_idx] << REF_SCALE_SHIFT) + (crop_width / 2)) / crop_width
        y_scale_fp = ((ref_height[ref_idx] << REF_SCALE_SHIFT) + (crop_height / 2)) / crop_height
        // fixed_point_scale_to_coarse_point_scale() :
        xs = ROUND_POWER_OF_TWO(x_scale_fp, REF_SCALE_SHIFT - SCALE_SUBPEL_BITS)
        ys = ROUND_POWER_OF_TWO(y_scale_fp, REF_SCALE_SHIFT - SCALE_SUBPEL_BITS)

        // Note: The various inputs here have different units:
        // * pre_x/pre_y are in units of luma pixels
        // * mv is in units of 1/8 luma pixels
        // * x/y are in units of pixels *in the current plane*
        // Here we unify these into a q4-format position within the current
        // plane, then project into the reference frame
        // Note that only 'mv' can be negative, so we have to take special care with that
        // to avoid undefined behaviour
        orig_pos_y = (pre_y << (SUBPEL_BITS - ssy)) + (y << SUBPEL_BITS) + (mv[ref][0] * (1 << (1 - ssy))) // q4
        orig_pos_x = (pre_x << (SUBPEL_BITS - ssx)) + (x << SUBPEL_BITS) + (mv[ref][1] * (1 << (1 - ssx))) // q4
        // Note: The addition of SCALE_EXTRA_OFF here compensates for the fact that we use '>> SCALE_EXTRA_BITS'
        // instead of 'ROUND_POWER_OF_TWO(..., SCALE_EXTRA_BITS)' inside the convolve filter. This may cause the
        // subpel position to "wrap", meaning we do get a tiny difference in behaviour.
        pos_y = scale_pixel_coord(orig_pos_y, y_scale_fp) + SCALE_EXTRA_OFF // q10
        pos_x = scale_pixel_coord(orig_pos_x, x_scale_fp) + SCALE_EXTRA_OFF // q10

        startY = pos_y >> SCALE_SUBPEL_BITS
        startX = pos_x >> SCALE_SUBPEL_BITS
        subpely = pos_y & SCALE_SUBPEL_MASK
        subpelx = pos_x & SCALE_SUBPEL_MASK

        if (allow_warp(block, ref, preRow, preCol, pred_w, pred_h, build_for_obmc)
            && cur_frame_force_integer_mv == 0
            ) {
          av1_warp_plane(plane, (mi_x>>plane_subsampling_x[plane])+x,
                         (mi_y>>plane_subsampling_y[plane])+y, pred_w, pred_h,
                         ref, refframes[0][preCol][preRow][ref])
        } else {
          filter_x = interp_filters[preCol][preRow][1]
          filter_y = interp_filters[preCol][preRow][0]
          inter_predictor(plane, basex, basey, startX, startY, pred_w, pred_h, ref, subpelx, subpely, xs, ys, filter_x, filter_y)
        }
      }
      if (!build_for_obmc) {
        dstX = ((dst_col * MI_SIZE)>>plane_subsampling_x[plane]) + x
        dstY = ((dst_row * MI_SIZE)>>plane_subsampling_y[plane]) + y
#if VALIDATE_SPEC_INTER2
        if (plane == VALIDATE_SPEC_INTER2_PLANE) {
            for (y = 0; y < pred_h; y++)
                  for (x = 0; x < pred_w; x++) {
                      for (refList = 0; refList < (is_compound?2:1); refList++) {
                          validate(81000)
                          validate(x)
                          validate(y)
                          validate(refList)
                          validate(predSamples[refList][x][y])
                      }
                  }
        }
#endif
        if (ref_frame[1] == INTRA_FRAME) {
          ASSERT(!sub8x8Inter, "This shouldn't be logically possible here")
          build_interintra_pred(plane, dstX, dstY, preRow, preCol, partition)
        } else
         combine_predictions(plane, dstX, dstY, preRow, preCol, pred_w, pred_h)
#if VALIDATE_SPEC_INTER2
        if (plane == VALIDATE_SPEC_INTER2_PLANE) {
            for (y = 0; y < pred_h; y++)
                  for (x = 0; x < pred_w; x++) {
                      validate(81001)
                      validate(x+dstX)
                      validate(y+dstY)
                      validate(frame[plane][x+dstX][y+dstY])
                  }
        }
#endif
      }
      preCol++
    }
    preRow++
  }
}

build_intrabc_predictor(plane, block, basex, basey, pred_w, pred_h, src_row, src_col, mi_x, mi_y, bsize) {
  // The intrabc case avoids most complications - no sub8x8 chroma special case, no scaling.
  // But it needs a separate function in Argon Streams due to the way the frame buffers are used
  ssx = plane_subsampling_x[plane]
  ssy = plane_subsampling_y[plane]

  preY = mi_y
  preX = mi_x
  dstRow = src_row
  dstCol = src_col

  // The destination and the reference "pointers" must point to an even mi block (MI grid is 4x4 here)
  if (plane_subsampling_y[plane] && (src_row & 0x01) && (mi_size_high[bsize] == 1)) {
    dstRow = src_row - 1
    preY -= MI_SIZE
  }
  if (plane_subsampling_x[plane] && (src_col & 0x01) && (mi_size_wide[bsize] == 1)) {
    dstCol = src_col - 1
    preX -= MI_SIZE
  }

  for(comp=0;comp<2;comp++) {
    mv[0][comp] = mvs[0][src_col][src_row][0][block][comp]
  }

  pos_y_q4 = (preY << (SUBPEL_BITS - ssy)) + (mv[0][0] * (1 << (1 - ssy)))
  pos_x_q4 = (preX << (SUBPEL_BITS - ssx)) + (mv[0][1] * (1 << (1 - ssx)))

  startY = (pos_y_q4 >> SUBPEL_BITS)
  startX = (pos_x_q4 >> SUBPEL_BITS)
  // Note: Although the motion vector has to be in whole pixels, it is only required
  // to be in whole *luma* pixels. Thus, if we are using subsampling (admittedly not
  // the usual case for intrabc), we may have 1/2 pixel offsets here
  subpely = (pos_y_q4 & SUBPEL_MASK) << SCALE_EXTRA_BITS
  subpelx = (pos_x_q4 & SUBPEL_MASK) << SCALE_EXTRA_BITS

  ASSERT(interp_filters[src_col][src_row][0] == BILINEAR, "Intrabc currently requires bilinear filters")
  ASSERT(interp_filters[src_col][src_row][1] == BILINEAR, "Intrabc currently requires bilinear filters")

  dstX = (dstCol * MI_SIZE)>>plane_subsampling_x[plane]
  dstY = (dstRow * MI_SIZE)>>plane_subsampling_y[plane]
  intrabc_predictor(plane, dstX, dstY, startX, startY, subpelx, subpely, pred_w, pred_h)
}

uint1 is_chroma_reference(mi_row, mi_col, bsize, plane) {
  bw = mi_size_wide[bsize]
  bh = mi_size_high[bsize]
  ref_pos = (((mi_row & 0x01) || !(bh & 0x01) || !plane_subsampling_y[plane]) &&
             ((mi_col & 0x01) || !(bw & 0x01) || !plane_subsampling_x[plane])) ? 1 : 0
  return ref_pos
}

build_inter_predictors(mi_row,mi_col,bsize, partition) {
  miW = mi_size_wide[bsize]
  miH = mi_size_high[bsize]

  for (plane = 0; plane < get_num_planes(); plane++) {
    mi_x = mi_col * MI_SIZE
    mi_y = mi_row * MI_SIZE

    bw = (miW * MI_SIZE)>>plane_subsampling_x[plane]
    bh = (miH * MI_SIZE)>>plane_subsampling_y[plane]
    bw = Max(bw, 4)
    bh = Max(bh, 4)

    if (is_chroma_reference(mi_row, mi_col, bsize, plane)) {
       ValidationInterIsObmc=0
       build_inter_predictors_plane(plane, 0, bw, bh, 0, 0, bw, bh, mi_row, mi_col, mi_x, mi_y, OBMC_NONE, bsize, partition)
    }
  }
}

#if MASK_MASTER_SIZE == 64
uint8 wedge_master_oblique_odd[MASK_MASTER_SIZE] = {
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  2,  6,  18,
  37, 53, 60, 63, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
};
uint8 wedge_master_oblique_even[MASK_MASTER_SIZE] = {
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  4,  11, 27,
  46, 58, 62, 63, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
};
uint8 wedge_master_vertical[MASK_MASTER_SIZE] = {
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  2,  7,  21,
  43, 57, 62, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
};
#else
#error "Argon streams currently only supports MASK_MASTER_SIZE = 64"
#endif // MASK_MASTER_SIZE == 64

shift_copy(uint8pointer src, uint8pointer dst, shift, row) {
  if (shift >= 0) {
    for (i = 0; i < shift; i++)
      dst[row*MASK_MASTER_SIZE + i] = src[0]
    for (i = shift; i < MASK_MASTER_SIZE; i++)
      dst[row*MASK_MASTER_SIZE + i] = src[i - shift]
  } else {
    // shift < 0
    for (i = 0; i < MASK_MASTER_SIZE + shift; i++)
      dst[row*MASK_MASTER_SIZE + i] = src[i - shift]
    for (i = MASK_MASTER_SIZE + shift; i < MASK_MASTER_SIZE; i++)
      dst[row*MASK_MASTER_SIZE + i] = src[MASK_MASTER_SIZE - 1]
  }
}

init_wedge() {
  uint8 master_mask[WEDGE_DIRECTIONS][MASK_MASTER_SIZE*MASK_MASTER_SIZE]

  w = MASK_MASTER_SIZE
  h = MASK_MASTER_SIZE
  s = MASK_MASTER_SIZE
  // Set up one oblique mask and the vertical mask
  shift = MASK_MASTER_SIZE / 4
  for (i = 0; i < h; i += 2) {
    shift_copy(wedge_master_oblique_even, master_mask[WEDGE_OBLIQUE63], shift, i)
    shift -= 1
    shift_copy(wedge_master_oblique_odd, master_mask[WEDGE_OBLIQUE63], shift, i+1)

    shift_copy(wedge_master_vertical, master_mask[WEDGE_VERTICAL], 0, i)
    shift_copy(wedge_master_vertical, master_mask[WEDGE_VERTICAL], 0, i+1)
  }
  // Use the oblique and vertical masks to fill out all other masks
  for (i = 0; i < h; i++) {
    for (j = 0; j < w; j++) {
      msk = master_mask[WEDGE_OBLIQUE63][i * s + j]
      master_mask[WEDGE_OBLIQUE27][j * s + i] = msk
      master_mask[WEDGE_OBLIQUE117][i * s + w - 1 - j] = (1 << WEDGE_WEIGHT_BITS) - msk
      master_mask[WEDGE_OBLIQUE153][(w - 1 - j) * s + i] = (1 << WEDGE_WEIGHT_BITS) - msk

      mskx = master_mask[WEDGE_VERTICAL][i * s + j]
      master_mask[WEDGE_HORIZONTAL][j * s + i] = mskx
    }
  }

  // Construct the per-block-size masks from the master masks
  for (bsize = BLOCK_4X4; bsize < BLOCK_SIZES_ALL; bsize++) {
    if (get_wedge_bits(bsize)) {
      bw = block_size_wide_lookup[bsize]
      bh = block_size_high_lookup[bsize]
      for (w = 0; w < WEDGE_TYPES; w++) {
        dir = get_wedge_direction(bsize, w)
        xoff = MASK_MASTER_SIZE / 2 - ((get_wedge_xoff(bsize, w) * bw) >> 3)
        yoff = MASK_MASTER_SIZE / 2 - ((get_wedge_yoff(bsize, w) * bh) >> 3)
        // Decide if we need to invert the sign of this mask
        sum = 0
        for (i = 0; i < bw; i++)
          sum += master_mask[dir][yoff*s + (xoff+i)]
        for (i = 1; i < bh; i++)
          sum += master_mask[dir][(yoff+i)*s + xoff]
        avg = (sum + (bw + bh - 1) / 2) / (bw + bh - 1)
        flip_sign = (avg < 32)
        // Fill out the mask array
        for (i = 0; i < bh; i++)
          for (j = 0; j < bw; j++) {
            wedge_masks[bsize][flip_sign][w][i*MAX_WEDGE_SIZE + j] = master_mask[dir][(yoff+i)*s + (xoff+j)]
            wedge_masks[bsize][!flip_sign][w][i*MAX_WEDGE_SIZE + j] = (1 << WEDGE_WEIGHT_BITS) - master_mask[dir][(yoff+i)*s + (xoff+j)]
          }
      }
    }
  }
}

setup_compound_segment_mask() {
  w = block_size_wide_lookup[sb_size]
  h = block_size_high_lookup[sb_size]
  invert = mask_type
  for (i = 0; i < h; i++)
    for (j = 0; j < w; j++) {
#if COMPOUND_SEGMENT_TYPE == 0
      // Uniform mask
      seg_mask[j][i] = 45
#else
      // Difference-weighted mask
      diff = Abs(predSamples[0][j][i] - predSamples[1][j][i])
      diff = ROUND_POWER_OF_TWO(diff, (bit_depth - 8) + interp_post_rounding)

      m = Clip3(0, 64, 38 + diff / DIFF_FACTOR)
      if (invert)
        seg_mask[j][i] = 64 - m
      else
        seg_mask[j][i] = m
#endif // COMPOUND_SEGMENT_TYPE == 0
    }
}

blend_mask(plane, dstX, dstY, bw, bh, uint8pointer mask, mask_stride) {
  for (y = 0; y < bh; y++) {
    for (x = 0; x < bw; x++) {
      // Select the mask to use for this pixel. If using subsampling, we average
      // several mask entries
      subw = plane_subsampling_x[plane]
      subh = plane_subsampling_y[plane]
      if (!subw && !subh) {
        m = mask[y*mask_stride + x]
      } else if (subw && !subh) {
        m = ROUND_POWER_OF_TWO(mask[y*mask_stride + (2*x)] + mask[y*mask_stride + (2*x+1)], 1)
      } else if (!subw && subh) {
        m = ROUND_POWER_OF_TWO(mask[(2*y)*mask_stride + x] + mask[(2*y+1)*mask_stride + x], 1)
      } else {
        m = ROUND_POWER_OF_TWO(mask[(2*y)*mask_stride + (2*x)]   + mask[(2*y)*mask_stride + (2*x+1)] +
                               mask[(2*y+1)*mask_stride + (2*x)] + mask[(2*y+1)*mask_stride + (2*x+1)], 2)
      }
      // Perform an alpha blend.
      frame[plane][x+dstX][y+dstY] =
        clip_pixel(ROUND_POWER_OF_TWO(m * predSamples[0][x][y] + (64 - m) * predSamples[1][x][y],
                                      6 + interp_post_rounding))
    }
  }
}


// Note: The ref code has an extra index on this table, called 'order_idx'.
// This is always 0 within the decoder - it exists purely to help out the
// reference encoder (in particular, joint_motion_search(), which can flip the
// two reference frames around temporarily).
// So we just eliminate that extra index entirely from our code.
uint8 quant_dist_lookup_table[4][2] = {
  { 9, 7 }, { 11, 5 }, { 12, 4 }, { 13, 3 }
};

uint8 quant_dist_weight[4][2] = {
  { 2, 3 }, { 2, 5 }, { 2, 7 }, { 1, MAX_FRAME_DISTANCE }
};

// Combine the predictions in 'predSamples' into 'frame'.
combine_predictions(plane, dstX, dstY, srcRow, srcCol, bw, bh) {
  if (refframes[0][srcCol][srcRow][1] > INTRA_FRAME) {
    if (compound_type == COMPOUND_AVERAGE) {
      for (y = 0; y < bh; y++)
        for (x = 0; x < bw; x++)
          frame[plane][x+dstX][y+dstY] =
            clip_pixel(ROUND_POWER_OF_TWO(predSamples[0][x][y] + predSamples[1][x][y], 1 + interp_post_rounding))
    }
    if (compound_type == COMPOUND_DISTANCE) {
      bwd_distance = clamp(Abs(get_relative_dist(ref_decode_order[ref_frame[0]], cur_frame_offset)), 0, MAX_FRAME_DISTANCE)
      fwd_distance = clamp(Abs(get_relative_dist(ref_decode_order[ref_frame[1]], cur_frame_offset)), 0, MAX_FRAME_DISTANCE)

      order = (fwd_distance <= bwd_distance)
      if (fwd_distance == 0 || bwd_distance == 0) {
        i = 3
      } else {
        d0 = fwd_distance
        d1 = bwd_distance
        for (i = 0; i < 3; i++) {
          c0 = quant_dist_weight[i][order]
          c1 = quant_dist_weight[i][!order]
          d0_c0 = d0 * c0
          d1_c1 = d1 * c1
          if ((d0 > d1 && d0_c0 < d1_c1) || (d0 <= d1 && d0_c0 > d1_c1))
            break
        }
      }
      fwd_weight = quant_dist_lookup_table[i][order]
      bwd_weight = quant_dist_lookup_table[i][!order]

      for (y = 0; y < bh; y++)
        for (x = 0; x < bw; x++) {
          frame[plane][x+dstX][y+dstY] =
            clip_pixel(ROUND_POWER_OF_TWO(predSamples[0][x][y] * fwd_weight + predSamples[1][x][y] * bwd_weight,
                                          DIST_PRECISION_BITS + interp_post_rounding))
        }
    }
    if (compound_type == COMPOUND_WEDGE) {
      if (! wedge_initialized) {
        init_wedge()
        wedge_initialized = 1
      }
      blend_mask(plane, dstX, dstY, bw, bh, wedge_masks[sb_size][wedge_sign][wedge_index], MAX_WEDGE_SIZE)
    }
    if (compound_type == COMPOUND_DIFFWTD) {
      if (plane == 0)
        setup_compound_segment_mask()
      blend_mask(plane, dstX, dstY, bw, bh, rseg_mask, MI_SIZE*MI_BLOCK_SIZE)
    }
  } else {
    for (y = 0; y < bh; y++)
      for (x = 0; x < bw; x++)
        frame[plane][x+dstX][y+dstY] = clip_pixel(ROUND_POWER_OF_TWO(predSamples[0][x][y], interp_post_rounding))
  }
}

uint8 ii_weights1d[MAX_SB_SIZE] = {
  60, 58, 56, 54, 52, 50, 48, 47, 45, 44, 42, 41, 39, 38, 37, 35, 34, 33, 32,
  31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 22, 21, 20, 19, 19, 18, 18, 17, 16,
  16, 15, 15, 14, 14, 13, 13, 12, 12, 12, 11, 11, 10, 10, 10,  9,  9,  9,  8,
  8,  8,  8,  7,  7,  7,  7,  6,  6,  6,  6,  6,  5,  5,  5,  5,  5,  4,  4,
  4,  4,  4,  4,  4,  4,  3,  3,  3,  3,  3,  3,  3,  3,  3,  2,  2,  2,  2,
  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2,  1,  1,  1,  1,  1,  1,  1,  1,
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1
};
uint8 ii_size_scales[BLOCK_SIZES_ALL] = {
    32, 16, 16, 16, 8, 8, 8, 4,
    4,  4,  2,  2,  2, 1, 1, 1,
    8,  8,  4,  4,  2, 2
};

/* Note: For interintra, the predictions are laid out as:
   predSamples[0] = inter prediction;
                    needs to be rounded by interp_post_rounding bits and clipped,
                    if CONVOLVE_ROUND/COMPOUND_ROUND is on
   frame = intra prediction; does not need any extra rounding

   The argument 'do_subsample' indicates whether we need to subsample the mask itself
   when generating a prediction for a chroma plane.
*/
blend_mask_interintra(plane, dstX, dstY, bw, bh, uint8pointer mask, mask_stride, do_subsample) {
  for (y = 0; y < bh; y++) {
    for (x = 0; x < bw; x++) {
      // Select the mask to use for this pixel. If using subsampling, we average
      // several mask entries
      if (do_subsample) {
        subw = plane_subsampling_x[plane]
        subh = plane_subsampling_y[plane]
        if (!subw && !subh) {
          m = mask[y*mask_stride + x]
        } else if (subw && !subh) {
          m = ROUND_POWER_OF_TWO(mask[y*mask_stride + (2*x)] + mask[y*mask_stride + (2*x+1)], 1)
        } else if (!subw && subh) {
          m = ROUND_POWER_OF_TWO(mask[(2*y)*mask_stride + x] + mask[(2*y+1)*mask_stride + x], 1)
        } else {
          m = ROUND_POWER_OF_TWO(mask[(2*y)*mask_stride + (2*x)]   + mask[(2*y)*mask_stride + (2*x+1)] +
                                 mask[(2*y+1)*mask_stride + (2*x)] + mask[(2*y+1)*mask_stride + (2*x+1)], 2)
        }
      } else {
        m = mask[y*mask_stride + x]
      }
      // Perform an alpha blend.
      interpred = clip_pixel(ROUND_POWER_OF_TWO(predSamples[0][x][y], interp_post_rounding))
      intrapred = frame[plane][x+dstX][y+dstY]
      frame[plane][x+dstX][y+dstY] =
        ROUND_POWER_OF_TWO(m * intrapred + (64 - m) * interpred, 6)
    }
  }
}

combine_interintra(plane, dstX, dstY) {
  plane_bsize = get_plane_block_size(sb_size, plane)
  bw = block_size_wide_lookup[plane_bsize]
  bh = block_size_high_lookup[plane_bsize]
  size_scale = ii_size_scales[plane_bsize]
  if (wedge_interintra) {
    if (! wedge_initialized) {
      init_wedge()
      wedge_initialized = 1
    }
    blend_mask_interintra(plane, dstX, dstY, bw, bh, wedge_masks[sb_size][wedge_sign][wedge_index], MAX_WEDGE_SIZE, 1)
  } else {
    uint8 mask[MAX_SB_SIZE * MAX_SB_SIZE]

    if (interintra_mode == II_V_PRED) {
      for (i = 0; i < bh; i++) {
        for (j = 0; j < bw; j++) {
          scale = ii_weights1d[i * size_scale]
          mask[i * MAX_SB_SIZE + j] = scale
        }
      }
    } else if (interintra_mode == II_H_PRED) {
      for (i = 0; i < bh; i++) {
        for (j = 0; j < bw; j++) {
          scale = ii_weights1d[j * size_scale]
          mask[i * MAX_SB_SIZE + j] = scale
        }
      }
    }
    else if (interintra_mode == II_SMOOTH_PRED) {
      for (i = 0; i < bh; i++) {
        for (j = 0; j < bw; j++) {
          scale = ii_weights1d[Min(i, j) * size_scale]
          mask[i * MAX_SB_SIZE + j] = scale
        }
      }
    }
    else { // II_PAETH_PRED or II_DC_PRED
      for (i = 0; i < bh; i++) {
        for (j = 0; j < bw; j++) {
          mask[i * MAX_SB_SIZE + j] = 32
        }
      }
    }
    blend_mask_interintra(plane, dstX, dstY, bw, bh, mask, MAX_SB_SIZE, 0)
  }
}

build_interintra_pred(plane, dstX, dstY, mi_row, mi_col, partition) {
  planeBsize = get_plane_block_size(sb_size, plane)
  mode = interintra_to_intra_mode[interintra_mode]
  predict_intra_block(dstX, dstY, plane, planeBsize, mode, sb_size, 0, 0, mi_row, mi_col, partition)
  combine_interintra(plane, dstX, dstY)
}

// When we build OBMC predictions, the reference code currently applies the standard
// rounding to the OBMC pred, *before* blending into the main block prediction.
// In order to match this, we want to explicitly apply that rounding to rpredSamples
// before the blend (and before logging the OBMC prediction)
round_obmc_pred(bw, bh) {
  for (y = 0; y < bh; y++)
    for (x = 0; x < bw; x++)
      predSamples[0][x][y] = clip_pixel(ROUND_POWER_OF_TWO(predSamples[0][x][y], interp_post_rounding))
}

uint8pointer get_obmc_mask(length) {
  if (length == 1) {
    return obmc_mask_1
  } else if (length == 2) {
    return obmc_mask_2
  } else if (length == 4) {
    return obmc_mask_4
  } else if (length == 8) {
    return obmc_mask_8
  } else if (length == 16) {
    return obmc_mask_16
  } else if (length == 32) {
    return obmc_mask_32
  } else {
     CHECK(0, "Invalid length for obmc mask")
  }
}

uint1 skip_u4x4_pred_in_obmc(bsize, plane, left) {
  CHECK(bsize >= BLOCK_8X8, "motion variation is not allowed")

  bsizePlane = get_plane_block_size(bsize, plane)

  if ((bsizePlane == BLOCK_4X4) ||
      (bsizePlane == BLOCK_8X4) ||
      (bsizePlane == BLOCK_4X8)) {
    return (left == 0)
  }
  return 0
}


build_obmc_above_pred(mi_row,mi_col,bsize, partition) {
  if (aU) {
    miW = mi_size_wide[bsize]
    neighborLimit = max_neighbor_obmc[b_width_log2_lookup[bsize]]
    neighborCount = 0

    for (i=mi_col; i<Min(mi_col + miW, mi_cols); i+=step) {
      aboveMiCol = i
      blockSbSize = sb_sizes[aboveMiCol][mi_row-1]
      step = Min(mi_size_wide[blockSbSize], mi_size_wide[BLOCK_64X64])
      // If we're considering a block with width 4, it should be treated as
      // half of a pair of blocks with chroma information in the second. Move
      // above_mi_col back to the start of the pair if needed, set above_mbmi
      // to point at the block with chroma information, and set mi_step to 2 to
      // step over the entire pair at the end of the iteration.
      if (step == 1) {
        i = i & ~1
        aboveMiCol = i + 1
        step = 2
      }

      if (refframes[0][aboveMiCol][mi_row-1][0] > INTRA_FRAME) {
        neighborCount += 1
        if (neighborCount > neighborLimit) {
          break
        }

        refX = i * MI_SIZE
        refY = mi_row * MI_SIZE
        ValidationInterIsObmc=1
        for (plane = 0; plane < get_num_planes(); plane++) {
          if (!skip_u4x4_pred_in_obmc(bsize, plane, 0)) {
#if VALIDATE_SPEC_INTER2
            if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
                validate(81003)
            }
#endif
            bw = (Min(miW, step)*MI_SIZE)>>plane_subsampling_x[plane]
            bh = Min(block_size_high_lookup[bsize], 64) >> (1 + plane_subsampling_y[plane])
            pred_bh = Max(bh, 4) // Always predict at least 4 pixels high, but may use less
            block = 0
            build_inter_predictors_plane(plane, block, bw, pred_bh, 0, 0, bw, pred_bh, mi_row-1, aboveMiCol, refX, refY, OBMC_ABOVE, bsize, partition)
            round_obmc_pred(bw, pred_bh)

            :log_prediction_rect_pred(b,bw,p.pred_bh,plane,0,MI_BLOCK_SIZE*MI_SIZE,ABOVE)
            :C log_prediction_rect_pred(b,bw,pred_bh,plane,0,MI_BLOCK_SIZE*MI_SIZE,ABOVE)

            // vertical blend
            xOff = refX>>plane_subsampling_x[plane]
            yOff = refY>>plane_subsampling_y[plane]
            blend_a64(plane, xOff, yOff, bw, bh, get_obmc_mask(bh), 0)
          }
        }
      }
    }
  }
}

build_obmc_left_pred(mi_row,mi_col,bsize, partition) {
  if (aL) {
    miH = mi_size_high[bsize]
    neighborLimit = max_neighbor_obmc[b_height_log2_lookup[bsize]]
    neighborCount = 0

    for (j=mi_row; j<Min(mi_row + miH, mi_rows); j+=step) {
      leftMiRow = j
      blockSbSize = sb_sizes[mi_col-1][leftMiRow]
      step = Min(mi_size_high[blockSbSize], mi_size_high[BLOCK_64X64])
      // If we're considering a block with width 4, it should be treated as
      // half of a pair of blocks with chroma information in the second. Move
      // above_mi_col back to the start of the pair if needed, set above_mbmi
      // to point at the block with chroma information, and set mi_step to 2 to
      // step over the entire pair at the end of the iteration.
      if (step == 1) {
        j = j & ~1
        leftMiRow = j + 1
        step = 2
      }

      if (refframes[0][mi_col-1][leftMiRow][0] > INTRA_FRAME) {
        neighborCount += 1
        if (neighborCount > neighborLimit) {
          break
        }

        refX = mi_col * MI_SIZE
        refY = j * MI_SIZE
        ValidationInterIsObmc=1
        for (plane = 0; plane < get_num_planes(); plane++) {
          if (!skip_u4x4_pred_in_obmc(bsize, plane, 1)) {
#if VALIDATE_SPEC_INTER2
            if ( plane == VALIDATE_SPEC_INTER2_PLANE ) {
                validate(81004)
            }
#endif
            bw = Min(block_size_wide_lookup[bsize], 64) >> (1 + plane_subsampling_x[plane])
            pred_bw = Max(bw, 4) // Always predict at least 4 pixels wide, but may use less
            bh = (Min(miH, step)*MI_SIZE)>>plane_subsampling_y[plane]
            block = 0
            build_inter_predictors_plane(plane, block, pred_bw, bh, 0, 0, pred_bw, bh, leftMiRow, mi_col-1, refX, refY, OBMC_LEFT, bsize, partition)
            round_obmc_pred(pred_bw, bh)

            :log_prediction_rect_pred(b,p.pred_bw,bh,plane,0,MI_BLOCK_SIZE*MI_SIZE,LEFT)
            :C log_prediction_rect_pred(b,pred_bw,bh,plane,0,MI_BLOCK_SIZE*MI_SIZE,LEFT)

            // horizontal blend
            xOff = refX>>plane_subsampling_x[plane]
            yOff = refY>>plane_subsampling_y[plane]
            blend_a64(plane, xOff, yOff, bw, bh, get_obmc_mask(bw), 1)
          }
        }
      }
    }
  }
}

build_obmc_inter_predictors(mi_row,mi_col,bsize, partition) {
  // for debug
  miW = mi_size_wide[bsize]
  miH = mi_size_high[bsize]
  for (plane = 0; plane < get_num_planes(); plane++) {
    bw = (miW * MI_SIZE)>>plane_subsampling_x[plane]
    bh = (miH * MI_SIZE)>>plane_subsampling_y[plane]
    bw = Max(bw, 4)
    bh = Max(bh, 4)
    xOff = (mi_col * MI_SIZE)>>plane_subsampling_x[plane]
    yOff = (mi_row * MI_SIZE)>>plane_subsampling_y[plane]
    :log_prediction_rect_frame(b,bw,bh,xOff,yOff,plane,OBMC_PRE)
    :C log_prediction_rect_frame(b,bw,bh,xOff,yOff,plane,OBMC_PRE)
  }

  build_obmc_above_pred(mi_row,mi_col,bsize, partition)
  build_obmc_left_pred(mi_row,mi_col,bsize, partition)

  // for debug
  miW = mi_size_wide[bsize]
  miH = mi_size_high[bsize]
  for (plane = 0; plane < get_num_planes(); plane++) {
    bw = (miW * MI_SIZE)>>plane_subsampling_x[plane]
    bh = (miH * MI_SIZE)>>plane_subsampling_y[plane]
    bw = Max(bw, 4)
    bh = Max(bh, 4)
    xOff = (mi_col * MI_SIZE)>>plane_subsampling_x[plane]
    yOff = (mi_row * MI_SIZE)>>plane_subsampling_y[plane]
    :log_prediction_rect_frame(b,bw,bh,xOff,yOff,plane,OBMC_POST)
    :C log_prediction_rect_frame(b,bw,bh,xOff,yOff,plane,OBMC_POST)
  }
#if VALIDATE_SPEC_INTER2
  bw = (miW * MI_SIZE)>>plane_subsampling_x[VALIDATE_SPEC_INTER2_PLANE]
  bh = (miH * MI_SIZE)>>plane_subsampling_y[VALIDATE_SPEC_INTER2_PLANE]
  dstX = (mi_col * MI_SIZE)>>plane_subsampling_x[VALIDATE_SPEC_INTER2_PLANE]
  dstY = (mi_row * MI_SIZE)>>plane_subsampling_y[VALIDATE_SPEC_INTER2_PLANE]
  for (y = 0; y < bh; y++)
        for (x = 0; x < bw; x++) {
            validate(81002)
            validate(x+dstX)
            validate(y+dstY)
            validate(frame[VALIDATE_SPEC_INTER2_PLANE][x+dstX][y+dstY])
        }
#endif
}

