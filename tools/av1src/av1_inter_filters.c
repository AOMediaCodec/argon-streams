/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

// MOTION COMPENSATION
//
// Code in vp9_setup_scale_factors_for_frame defines which type of filter to use
// For unscaled motion at full-pel offsets it switches to a copying mode
//
// Scaling is based on REF_SCALE_SHIFT precision scale factor (14 bits)
// Computes a full precision scaling for the start, and uses a scaled_x/y version of SCALE_SUBPEL_SHIFTS for the steps.
// This is in 4bit fixed point precision
//
// set_offsets_with_scaling
// scaled_mv

#define VALIDATE 0

// In this experiment, the amount of rounding applied during
// the convolve filters differs between 8/10-bit and 12-bit
setup_interp_rounding(is_compound) {
  interp_round0 = INTERP_ROUND0_BASE
  interp_round1 = is_compound ? INTERP_ROUND1_BASE : (2*FILTER_BITS - interp_round0)

  if (bit_depth == 12) {
    interp_round0 += 2
    if (!is_compound) {
      interp_round1 -= 2
    }
  }

  interp_post_rounding = 2*FILTER_BITS - (interp_round0 + interp_round1)

  intbufrange = bit_depth + (FILTER_BITS + 2) - interp_round0
  ASSERT(intbufrange <= 16, "Insufficient rounding to keep intermediate convolve values in 16 bits")
}

// This filters rows of the original image
// Note that we produce many output rows so that the vertical filtering can sample the correct positions
convolve_horiz_c(plane, startX, startY, subpel_x, x_step, w, h, ref, filter) {
  maxw = ((ref_width[ref_idx]+plane_subsampling_x[plane]) >> plane_subsampling_x[plane]) - 1  //TODO should this be aligned to multiples of 8?
  maxh = ((ref_height[ref_idx]+plane_subsampling_y[plane]) >> plane_subsampling_y[plane]) - 1
#if VALIDATE
  validate(1011)
#endif
  for (y = 0; y < h; y++) {
    for (x = 0; x < w; x++) {
      if (subpel_x==0 && x_step==SCALE_SUBPEL_SHIFTS) {
        sum = framestore[ref_idx][plane][Min(maxw,Max(0,x+startX))][Min(maxh,Max(0,y+startY))] * (1 << FILTER_BITS)
      } else {
        src_x = subpel_x + x_step * x
        sum = 0
        for (k = 0; k < 8; k++) {
          if (src_x==0)
            sum += framestore[ref_idx][plane][ Min(maxw,Max(0,startX + (src_x >> SCALE_SUBPEL_BITS) + k - 3))][Min(maxh,Max(0,y+startY))] * subpel_kernels[filter][(src_x&SCALE_SUBPEL_MASK)>>SCALE_EXTRA_BITS][k] // (2-1) [RNG-Horiz{1+(filter<<SCALE_SUBPEL_BITS)+(src_x&SCALE_SUBPEL_MASK)}{1,64,1,17,33,49}]
          else
            sum += framestore[ref_idx][plane][ Min(maxw,Max(0,startX + (src_x >> SCALE_SUBPEL_BITS) + k - 3))][Min(maxh,Max(0,y+startY))] * subpel_kernels[filter][(src_x&SCALE_SUBPEL_MASK)>>SCALE_EXTRA_BITS][k] // (2-1) [RNG-Horiz{1+(filter<<SCALE_SUBPEL_BITS)+(src_x&SCALE_SUBPEL_MASK)}{1,64,2-16,18-32,34-48,50-64}]
#if VALIDATE
          validate(k)
          validate(sum)
#endif
        }
      }
      a = ROUND_POWER_OF_TWO(sum, interp_round0)
      inter_temp[x][y] = a
#if VALIDATE
      validate(a)
#endif
    }
  }
}

// This filters columns of inter_temp to produce an output image
convolve_vert_c(plane, dstX, dstY, subpel_y, y_step, w, h, ref, filter) {
#if VALIDATE
  validate(1012)
#endif
  for (y = 0; y < h; y++) {
    for (x = 0; x < w; x++) {
      if (subpel_y==0 && y_step==SCALE_SUBPEL_SHIFTS) {
        sum = inter_temp[x][y + 3] * (1 << FILTER_BITS)
      } else {
        src_y = subpel_y + y_step * y
        sum = 0
        for (k = 0; k < 8; k++) {
          if (src_y==0)
            sum += inter_temp[x][ (src_y >> SCALE_SUBPEL_BITS) + k] * subpel_kernels[filter][(src_y&SCALE_SUBPEL_MASK)>>SCALE_EXTRA_BITS][k] // (3-1) [RNG-Vert{1+(filter<<SCALE_SUBPEL_BITS)+(src_y&SCALE_SUBPEL_MASK)}{1,64,1,17,33,49}]
          else
            sum += inter_temp[x][ (src_y >> SCALE_SUBPEL_BITS) + k] * subpel_kernels[filter][(src_y&SCALE_SUBPEL_MASK)>>SCALE_EXTRA_BITS][k] // (3-1) [RNG-Vert{1+(filter<<SCALE_SUBPEL_BITS)+(src_y&SCALE_SUBPEL_MASK)}{1,64,2-16,18-32,34-48,50-64}]
        }
      }
      a = ROUND_POWER_OF_TWO(sum, interp_round1)
      // Note: Both convolve-round and compound-round remove the clipping here.
      predSamples[ref][x+dstX][y+dstY] = a // (4-1) [RNG-Weighted1]
#if VALIDATE
      validate(a)
#endif
    }
  }
}

// Generate a block of size w*h at location dstX, dstY
// Reads data starting from startX,startY
convolve_c(plane, dstX, dstY, startX, startY, subpel_x, subpel_y, ref, x_step, y_step, w, h, filter_x, filter_y) {
#if DECODE && COLLECT_STATS
  if (plane == 0) {
    total_inter_px += w * h
  }
  total_inter_preds += 1
#endif
  //:print 'dstx',dstX,'dstY',dstY,'w',w,'h',h
  taps = 8
  if (w<=4) {
    if (filter_x == EIGHTTAP || filter_x == EIGHTTAP_SHARP) {
      filter_x = FOURTAP
    } else if (filter_x == EIGHTTAP_SMOOTH) {
      filter_x = FOURTAP_SMOOTH
    }
  }
  if (h<=4) {
    if (filter_y == EIGHTTAP || filter_y == EIGHTTAP_SHARP) {
      filter_y = FOURTAP
    } else if (filter_y == EIGHTTAP_SMOOTH) {
      filter_y = FOURTAP_SMOOTH
    }
  }
#if VALIDATE_SPEC_INTER2
  if (plane == VALIDATE_SPEC_INTER2_PLANE && ValidationInterIsObmc == 0) {
      validate(81006)
      validate(filter_x)
  }
#endif
  /* Fixed size intermediate buffer places limits on parameters.
   * For AV1:
   * max y_step = 2*SCALE_SUBPEL_SHIFTS, max h = 128, taps = 8
   * so max intermediate_height = 262
   * For VP9:
   * max y_step = 5*SCALE_SUBPEL_SHIFTS, max h = 64, taps = 8
   * so max intermediate_height = 323
   *
   * In both cases, we allow one extra row (why?)
   * TODO the +((1 << SCALE_SUBPEL_BITS) - 1) should maybe be subpel_y?
   */
  intermediate_height = (((h - 1) * y_step + ((1 << SCALE_SUBPEL_BITS) - 1)) >> SCALE_SUBPEL_BITS) + 8

  CHECK(y_step <= 2*SCALE_SUBPEL_SHIFTS,"y_step <= 2*SCALE_SUBPEL_SHIFTS")
  CHECK(x_step <= 2*SCALE_SUBPEL_SHIFTS,"x_step <= 2*SCALE_SUBPEL_SHIFTS")
  CHECK(intermediate_height <= MAX_EXT_SIZE, "intermediate_height <= MAX_EXT_SIZE")

  convolve_horiz_c(plane, startX, startY - (taps / 2 - 1), subpel_x, x_step, w, intermediate_height, ref, filter_x)
#if VALIDATE_SPEC_INTER2
  if (plane == VALIDATE_SPEC_INTER2_PLANE && ValidationInterIsObmc == 0) {
      validate(81007)
      validate(filter_y)
  }
#endif
  convolve_vert_c(plane, dstX, dstY, subpel_y, y_step, w, h, ref, filter_y)
}

blend_a64(plane, xOff, yOff, bw, bh, uint8pointer mask, horiz) {
  for (y = 0; y < bh; y++) {
    for (x = 0; x < bw; x++) {
      m = mask[horiz ? x : y]
#if VALIDATE_SPEC_INTER2
      if (plane == VALIDATE_SPEC_INTER2_PLANE) {
          validate(81005)
          validate(m)
          validate(x+xOff)
          validate(y+yOff)
          validate(frame[plane][x+xOff][y+yOff])
          validate(predSamples[0][x][y])
      }
#endif
      frame[plane][x+xOff][y+yOff] = ROUND_POWER_OF_TWO(m*frame[plane][x+xOff][y+yOff] + (64-m)*predSamples[0][x][y], 6)
#if VALIDATE_SPEC_INTER2
      if (plane == VALIDATE_SPEC_INTER2_PLANE) {
          validate(frame[plane][x+xOff][y+yOff])
      }
#endif
    }
  }
}

intrabc_predictor(plane, dstX, dstY, startX, startY, subpelx, subpely, w, h) {
#if DECODE && COLLECT_STATS
  if (plane == 0) {
    total_intrabc_px += w * h
  }
#endif
  // Note: The subpelx/subpely values should only ever be 0 or half a pixel (== 2^13).
  // Check that here
  CHECK(subpelx == 0 || subpelx == 1 << (SCALE_SUBPEL_BITS - 1), "Intrabc does not support non-1/2-px offsets")
  CHECK(subpely == 0 || subpely == 1 << (SCALE_SUBPEL_BITS - 1), "Intrabc does not support non-1/2-px offsets")

  uint16 tmp_buf[MAX_SB_SIZE+1][MAX_SB_SIZE] // May need one extra row

  // Horizontal filter - either copy pixels, or do a 50-50 blend.
  // We may or may not need to generate one extra row.
  // Due to the way the ref code implements the intrabc filter, there is effectively no rounding
  // after the horizontal filter. Thus we add 1 fractional bit of precision at this stage
  rows = (subpely == 1 << (SCALE_SUBPEL_BITS - 1)) ? (h+1) : h
  if (subpelx == 0) {
    for (i = 0; i < rows; i++)
      for (j = 0; j < w; j++) {
        x = startX + j
        y = startY + i
        tmp_buf[i][j] = frame[plane][x][y] << 1
#if VALIDATE_SPEC_INTER2
        //if (plane == VALIDATE_SPEC_INTER2_PLANE) {
        //  validate(81009)
        //  validate(i)
        //  validate(j)
        //  validate(tmp_buf[i][j])
        //}
#endif
      }
  } else {
    for (i = 0; i < rows; i++) {
      for (j = 0; j < w; j++) {
        x = startX + j
        y = startY + i
        tmp_buf[i][j] = frame[plane][x][y] + frame[x+1][y]
#if VALIDATE_SPEC_INTER2
        //if (plane == VALIDATE_SPEC_INTER2_PLANE) {
        //  validate(81009)
        //  validate(i)
        //  validate(j)
        //  validate(tmp_buf[i][j])
        //}
#endif
      }
    }
  }

  // Vertical filter
  // We again need to either copy or apply a 50-50 blend, but at this point we also need
  // to account for the 1 fractional bit we added in the horizontal filter
  if (subpely == 0) {
    for (i = 0; i < h; i++)
      for (j = 0; j < w; j++) {
        x = dstX + j
        y = dstY + i
        frame[plane][x][y] = ROUND_POWER_OF_TWO(tmp_buf[i][j], 1)
#if VALIDATE_SPEC_INTER2
        if (plane == VALIDATE_SPEC_INTER2_PLANE) {
          validate(81008)
          validate(i)
          validate(j)
          validate(frame[plane][x][y])
        }
#endif
      }
  } else {
    for (i = 0; i < h; i++) {
      for (j = 0; j < w; j++) {
        x = dstX + j
        y = dstY + i
        frame[plane][x][y] = ROUND_POWER_OF_TWO(tmp_buf[i][j] + tmp_buf[i+1][j], 2)
#if VALIDATE_SPEC_INTER2
        if (plane == VALIDATE_SPEC_INTER2_PLANE) {
          validate(81008)
          validate(i)
          validate(j)
          validate(frame[plane][x][y])
        }
#endif
      }
    }
  }
}
