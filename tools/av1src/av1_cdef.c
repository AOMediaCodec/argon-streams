/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_CDEF 1
#else
#define VALIDATE_SPEC_CDEF 0
#endif
#define VALIDATE_SPEC_INTERNAL_CDEF 0
uint1 sb_all_skip(miRow, miCol) {
  allskip = 1
  for(r=miRow;r<Min(mi_rows,miRow+mi_size_high[BLOCK_64X64]);r++)
    for(c=miCol;c<Min(mi_cols,miCol+mi_size_wide[BLOCK_64X64]);c++) {
      allskip &= skip_coeffs[c][r]
    }
  return allskip
}

uint1 is_8x8_block_skip(miRow, miCol) {
  allskip = 1
  for(r=miRow;r<Min(mi_rows,miRow+mi_size_high[BLOCK_8X8]);r++)
    for(c=miCol;c<Min(mi_cols,miCol+mi_size_wide[BLOCK_8X8]);c++) {
      allskip &= skip_coeffs[c][r]
    }
  return allskip
}

cdef_filter_frame() {
  miSize64x64 = 64 >> MI_SIZE_LOG2
  nvfb = (mi_rows + miSize64x64 - 1) / miSize64x64
  nhfb = (mi_cols + miSize64x64 - 1) / miSize64x64

  if (skip_cdef) {
    // CDEF is either disabled or we know it will not affect the output.
    // So we can just walk over each block and copy it to 'frame'
    // We don't call log_cdef() here, as the reference code doesn't call it in this case.
    for (fbr = 0; fbr < nvfb; fbr++) {
      miRow = fbr * miSize64x64
      for (fbc = 0; fbc < nhfb; fbc++) {
        miCol = fbc * miSize64x64
        cdef_skip_fb(miRow, miCol)
      }
    }
  } else {
    nplanes = get_num_planes()
    for (fbr = 0; fbr < nvfb; fbr++) {
      miRow = fbr * miSize64x64
      for (fbc = 0; fbc < nhfb; fbc++) {
        miCol = fbc * miSize64x64
        :log_cdef(b, miRow, miCol, 0, MI_SIZE, nplanes)
        :C log_cdef(b, miRow, miCol, 0, MI_SIZE, nplanes)
        x = miCol >> MI_SIZE_64X64_LOG2
        y = miRow >> MI_SIZE_64X64_LOG2
        if (cdef_idx[x][y] != -1)
          cdef_filter_fb(miRow, miCol, cdef_idx[x][y])
        else
          cdef_skip_fb(miRow, miCol)
        :log_cdef(b, miRow, miCol, 1, MI_SIZE, nplanes)
        :C log_cdef(b, miRow, miCol, 1, MI_SIZE, nplanes)
      }
    }
#if VALIDATE_SPEC_CDEF
    fw = crop_width
    fh = crop_height
    for (plane = 0; plane < get_num_planes(); plane++) {
        subX = ( plane == 0 ) ? 0 : subsampling_x
        subY = ( plane == 0 ) ? 0 : subsampling_y
        for ( i = 0; i < fh >> subY; i++) {
            for ( j = 0; j < fw >> subX; j++ ) {
                validate(100001)
                validate(plane)
                validate(j)
                validate(i)
                validate( cdef_frame[ plane ][ j ][ i ] )
            }
        }
    }
#endif
  }
}

uint8 cdef_uv_bsize[2][2] = {
  {BLOCK_8X8, BLOCK_8X4},
  {BLOCK_4X8, BLOCK_4X4}
};

// If the X and Y subsamplings are unequal, then we need to adjust the
// filter direction for the chroma planes.
// For example, given a slope which is 45 degrees from vertical in the luma plane,
// if subsampling_x = 1 and subsampling_y = 0, then the corresponding slope in
// the chroma planes is 26.5 degrees from vertical.
uint8 cdef_uv_dir[2][2][8] = {
  {{0, 1, 2, 3, 4, 5, 6, 7},  // 4:4:4 subsampling
   {1, 2, 2, 2, 3, 4, 6, 0}}, // 4:4:0 subsampling,
  {{7, 0, 2, 4, 5, 6, 6, 6},  // 4:2:2 subsampling
   {0, 1, 2, 3, 4, 5, 6, 7}}  // 4:2:0 subsamplinbg
};

int32 adjust_strength(strength, var) {
  i = var >> 6 ? Min(get_msb(var >> 6), 12) : 0
  return var ? (strength * (4 + i) + 8) >> 4 : 0
}

cdef_filter_fb(miRow, miCol, idx) {
  miSize64x64 = 64 >> MI_SIZE_LOG2
  coeff_shift = Max(0, bit_depth - 8)

  // Load filter parameters
  y_pri_strength = cdef_y_pri_strength[idx] << coeff_shift
  y_sec_strength = cdef_y_sec_strength[idx] << coeff_shift
  y_damping = cdef_damping + coeff_shift
  y_bsize = BLOCK_8X8

  uv_pri_strength = cdef_uv_pri_strength[idx] << coeff_shift
  uv_sec_strength = cdef_uv_sec_strength[idx] << coeff_shift
  uv_damping = y_damping - 1
  uv_bsize = cdef_uv_bsize[subsampling_x][subsampling_y]

  // Select where to apply CDEF (as per sb_compute_cdef_list)
  r_shift = mi_height_log2_lookup[BLOCK_8X8]
  c_shift = mi_width_log2_lookup[BLOCK_8X8]
  for (r = miRow; r < Min(mi_rows, miRow + miSize64x64); r += (1 << r_shift))
    for (c = miCol; c < Min(mi_cols, miCol + miSize64x64); c += (1 << c_shift)) {
      this_skip = is_8x8_block_skip(r, c)
#if VALIDATE_SPEC_INTERNAL_CDEF
      validate(100002)
      validate(r)
      validate(c)
      validate(this_skip)
#endif
      filter_y = !this_skip
      filter_uv = !this_skip

      if (filter_y || filter_uv) {
        // Calculate main direction in the image and its strength (into cdef_var)
        dir = cdef_find_dir(r, c)
      }

      if (filter_y) {
        // Not sure why this is needed. Also this applies to the *original* y_pri_strength,
        // before applying the strength adjustment, which is strange.
        if (y_pri_strength == 0)
          y_dir = 0
        else
          y_dir = dir
        // Adjust the Y filter strength based on the strength of the directional pattern
        t = adjust_strength(y_pri_strength, cdef_var)
        cdef_filter_block(0, c << MI_SIZE_LOG2, r << MI_SIZE_LOG2, t,
                          y_sec_strength, y_damping, y_dir, y_bsize)
      } else {
        cdef_skip_block(0, c << MI_SIZE_LOG2, r << MI_SIZE_LOG2, 8, 8)
      }

      if (filter_uv) {
        if (uv_pri_strength == 0)
          uv_dir = 0
        else
          uv_dir = cdef_uv_dir[subsampling_x][subsampling_y][dir]
        for (plane = 1; plane < get_num_planes(); plane++)
          cdef_filter_block(plane, c << MI_SIZE_LOG2, r << MI_SIZE_LOG2, uv_pri_strength,
                            uv_sec_strength, uv_damping, uv_dir, uv_bsize)
      } else {
        for (plane = 1; plane < get_num_planes(); plane++)
          cdef_skip_block(plane, c << MI_SIZE_LOG2, r << MI_SIZE_LOG2, 8, 8)
      }
    }
}

cdef_skip_fb(miRow, miCol) {
  miSize64x64 = 64 >> MI_SIZE_LOG2
  w = Min(mi_cols - miCol, miSize64x64) << MI_SIZE_LOG2
  h = Min(mi_rows - miRow, miSize64x64) << MI_SIZE_LOG2
  for (plane = 0; plane < get_num_planes(); plane++)
    cdef_skip_block(plane, miCol << MI_SIZE_LOG2, miRow << MI_SIZE_LOG2, w, h)
}

uint16 div_table[9] = { 0, 840, 420, 280, 210, 168, 140, 120, 105 };
uint8 cdef_pri_taps[2][2] = { { 4, 2 }, { 3, 3 } };
uint8 cdef_sec_taps[2][2] = { { 2, 1 }, { 2, 1 } };

// Calculate the main direction for an 8x8 block, using luma information only.
uint8 cdef_find_dir(miRow, miCol) {
  int32 cost[8]
  int32 partial[8][15]
  for (i = 0; i < 8; i++) {
    cost[i] = 0
    for (j = 0; j < 15; j++)
      partial[i][j] = 0
  }
  best_cost = 0
  best_dir = 0
  x0 = miCol << MI_SIZE_LOG2
  y0 = miRow << MI_SIZE_LOG2
  for (i = 0; i < 8; i++) {
    for (j = 0; j < 8; j++) {
      /* We subtract 128 here to reduce the maximum range of the squared
         partial sums. */
      x = (frame[0][x0 + j][y0 + i] >> (bit_depth - 8)) - 128
      partial[0][i + j] += x
      partial[1][i + j / 2] += x
      partial[2][i] += x
      partial[3][3 + i - j / 2] += x
      partial[4][7 + i - j] += x
      partial[5][3 - i / 2 + j] += x
      partial[6][j] += x
      partial[7][i / 2 + j] += x
    }
  }
  for (i = 0; i < 8; i++) {
    cost[2] += partial[2][i] * partial[2][i]
    cost[6] += partial[6][i] * partial[6][i]
  }
  cost[2] *= div_table[8]
  cost[6] *= div_table[8]
  for (i = 0; i < 7; i++) {
    cost[0] += (partial[0][i] * partial[0][i] +
                partial[0][14 - i] * partial[0][14 - i]) *
               div_table[i + 1]
    cost[4] += (partial[4][i] * partial[4][i] +
                partial[4][14 - i] * partial[4][14 - i]) *
               div_table[i + 1]
  }
  cost[0] += partial[0][7] * partial[0][7] * div_table[8]
  cost[4] += partial[4][7] * partial[4][7] * div_table[8]
  for (i = 1; i < 8; i += 2) {
    for (j = 0; j < 4 + 1; j++) {
      cost[i] += partial[i][3 + j] * partial[i][3 + j]
    }
    cost[i] *= div_table[8]
    for (j = 0; j < 4 - 1; j++) {
      cost[i] += (partial[i][j] * partial[i][j] +
                  partial[i][10 - j] * partial[i][10 - j]) *
                 div_table[2 * j + 2]
    }
  }
  for (i = 0; i < 8; i++) {
    if (cost[i] > best_cost) {
      best_cost = cost[i]
      best_dir = i
    }
  }
  // Return two values
  cdef_var = (best_cost - cost[(best_dir + 4) & 7]) >> 10
  return best_dir
}

int8 cdef_directions[8][2][2] = {
  { { -1, 1 }, { -2,  2 } },
  { {  0, 1 }, { -1,  2 } },
  { {  0, 1 }, {  0,  2 } },
  { {  0, 1 }, {  1,  2 } },
  { {  1, 1 }, {  2,  2 } },
  { {  1, 0 }, {  2,  1 } },
  { {  1, 0 }, {  2,  0 } },
  { {  1, 0 }, {  2, -1 } }
};

// Functions to check whether we can filter across the bottom/right edges
// of a particular mi unit.
//
// Note: Whether or not we can filter the top/left edges is determined
// in the same way as for the deblock filter, so we just reuse
// can_lf_top() and can_lf_left().
can_cdef_bottom(mi_row, mi_col) {
  if (mi_row >= mi_rows - 1)
    return 0
  return 1
}

can_cdef_right(mi_row, mi_col) {
  if (mi_col >= mi_cols - 1)
    return 0
  return 1
}

// The input filter data is:
// * Inside current superblock: Deblocked pixels
// * Outside of frame: CDEF_VERY_LARGE
// * We also fetch a border of 3 pixels around the
//   current superblock - but, if LOOPFILTERING_ACROSS_TILES is enabled,
//   we only use these pixels if we're allowed to use them for loop filtering
//
// Note that (x0, y0) are the coordinates of the top-left of the current 8x8 block,
// and we can only fetch up to 2px outside of that block. This makes it fairly
// straightforward to figure out which pixels are fetchable.
//
// ss2_x is 2 - ss_x and similarly for ss2_y. This avoids some
// unneeded calculation in a tight loop.
uint16 cdef_get_at(plane, ss2_x, ss2_y, x0, y0, x, y) {
#if VALIDATE_SPEC_INTERNAL_CDEF
    validate(100005)
    validate(y0)
    validate(x0)

    // Note: i, j, dir, k and is_neg are no longer passed in so this
    // won't compile. This makes the encoder faster, but if we need to
    // get VALIDATE_SPEC_INTERNAL_CDEF working again, we'll need to
    // #ifdef the function definition to pass it.
    validate(i)
    validate(j)
    validate(dir)
    validate(k)
    validate(is_neg ? -1 : 1)

    validate(cdk0)
    validate(cdk1)
    validate(y)
    validate(x)
#endif
  // Offset to the rightmost/bottommost mi unit within an 8x8. We add
  // 1 MI unit vertically or horizontally because an 8x8 is 2x2 MI
  // units.
  miOffset = 2 - 1

  // Note: We use the *decoded* frame size (determined by mi_rows/cols),
  // rather than the *displayed* frame size (determined by crop_width/height),
  // throughout CDEF processing.
  //
  // Note that a >> (MI_SIZE_LOG2 - ss) = (a << ss) >> MI_SIZE_LOG2 as
  // long as the left shift doesn't overflow, but writing it in the
  // latter form is marginally easier to compute with the x86 ISA (it
  // avoids a move of an immediate into a register).
  miRow = y0 >> ss2_y
  miCol = x0 >> ss2_x

  if (x < x0) {
    if (! can_lf_left (miRow, miCol)) {
      return CDEF_VERY_LARGE
    }
  } else {
    // bw = 8 >> ss_x, but we have ss2_x = 2 - ss_x, so rearrange to
    // get:
    bw = 2 << ss2_x
    if ((x >= x0 + bw) && !can_cdef_right(miRow, miCol + miOffset)) {
      return CDEF_VERY_LARGE
    }
  }

  if (y < y0) {
    if (! can_lf_top (miRow, miCol)) {
      return CDEF_VERY_LARGE
    }
  } else {
    // See note about bw above
    bh = 2 << ss2_y
    if (y >= y0 + bh && !can_cdef_bottom(miRow + miOffset, miCol)) {
      return CDEF_VERY_LARGE
    }
  }

#if VALIDATE_SPEC_INTERNAL_CDEF
  validate(1)
#endif

  return frame[plane][x][y]
}

int32 constrain(diff, threshold, damping) {
  if (! threshold)
    return 0

  damping_adj = Max(0, damping - get_msb(threshold))
  sign = (diff < 0) ? -1 : 1
  return sign * clamp(threshold - (Abs(diff) >> damping_adj), 0, Abs(diff))
}

// Filter a cdef unit (8x8 luma pixels, or the appropriate subsampled size
// for the chroma planes)
cdef_filter_block(plane, x0, y0, pri_strength, sec_strength, damping, dir, bsize) {
#if VALIDATE_SPEC_INTERNAL_CDEF
  validate(100003)
  validate(y0 >> MI_SIZE_LOG2)
  validate(x0 >> MI_SIZE_LOG2)
  validate(plane)
  validate(pri_strength)
  validate(sec_strength)
  validate(damping)
  validate(dir)
#endif

#if DECODE && COLLECT_STATS
  total_cdefs += 1
#endif

  ss_x = plane_subsampling_x[plane]
  ss_y = plane_subsampling_y[plane]

  x0 = x0 >> ss_x
  y0 = y0 >> ss_y
  planeWidth = mi_cols << (MI_SIZE_LOG2 - ss_x)
  planeHeight = mi_rows << (MI_SIZE_LOG2 - ss_y)
  bw = block_size_wide_lookup[bsize]
  bh = block_size_high_lookup[bsize]
  coeffShift = Max(0, bit_depth - 8)
  coeffSet = (pri_strength >> coeffShift) & 1

  ss2_x = 2 - ss_x
  ss2_y = 2 - ss_y

  for (i = 0; i < bh; i++)
    for (j = 0; j < bw; j++) {
      sum = 0
      x = frame[plane][x0 + j][y0 + i]
      max = x
      min = x
      for (k = 0; k < 2; k++) {
        cdk0 = cdef_directions[dir][k][0]
        cdk1 = cdef_directions[dir][k][1]
        p0 = cdef_get_at(plane, ss2_x, ss2_y, x0, y0, x0 + j + cdk1, y0 + i + cdk0)
        p1 = cdef_get_at(plane, ss2_x, ss2_y, x0, y0, x0 + j - cdk1, y0 + i - cdk0)
        sum += cdef_pri_taps[coeffSet][k] * constrain(p0 - x, pri_strength, damping)
        sum += cdef_pri_taps[coeffSet][k] * constrain(p1 - x, pri_strength, damping)
        if (p0 != CDEF_VERY_LARGE) max = Max(p0, max)
        if (p1 != CDEF_VERY_LARGE) max = Max(p1, max)
        min = Min(p0, min)
        min = Min(p1, min)

        cdk0 = cdef_directions[(dir + 2) & 7][k][0]
        cdk1 = cdef_directions[(dir + 2) & 7][k][1]
        s0 = cdef_get_at(plane, ss2_x, ss2_y, x0, y0, x0 + j + cdk1, y0 + i + cdk0)
        s1 = cdef_get_at(plane, ss2_x, ss2_y, x0, y0, x0 + j - cdk1, y0 + i - cdk0)

        cdk0 = cdef_directions[(dir - 2) & 7][k][0]
        cdk1 = cdef_directions[(dir - 2) & 7][k][1]
        s2 = cdef_get_at(plane, ss2_x, ss2_y, x0, y0, x0 + j + cdk1, y0 + i + cdk0)
        s3 = cdef_get_at(plane, ss2_x, ss2_y, x0, y0, x0 + j - cdk1, y0 + i - cdk0)

        if (s0 != CDEF_VERY_LARGE) max = Max(s0, max)
        if (s1 != CDEF_VERY_LARGE) max = Max(s1, max)
        if (s2 != CDEF_VERY_LARGE) max = Max(s2, max)
        if (s3 != CDEF_VERY_LARGE) max = Max(s3, max)
        min = Min(s0, min)
        min = Min(s1, min)
        min = Min(s2, min)
        min = Min(s3, min)
        sum += cdef_sec_taps[coeffSet][k] * constrain(s0 - x, sec_strength, damping)
        sum += cdef_sec_taps[coeffSet][k] * constrain(s1 - x, sec_strength, damping)
        sum += cdef_sec_taps[coeffSet][k] * constrain(s2 - x, sec_strength, damping)
        sum += cdef_sec_taps[coeffSet][k] * constrain(s3 - x, sec_strength, damping)
      }
#if VALIDATE_SPEC_INTERNAL_CDEF
            validate(100004)
            validate(i)
            validate(j)
            validate(min)
            validate(max)
            validate(x)
            validate(sum)
#endif
      cdef_frame[plane][x0 + j][y0 + i] = clamp(x + ((8 + sum - (sum < 0)) >> 4), min, max)
    }
}

// For non-filtered pixels, we still need to copy data from 'frame' to 'cdef_frame'
cdef_skip_block(plane, x0, y0, w, h) {
  x0 = x0 >> plane_subsampling_x[plane]
  y0 = y0 >> plane_subsampling_y[plane]
  w = w >> plane_subsampling_x[plane]
  h = h >> plane_subsampling_y[plane]
  for (i = 0; i < h; i++)
    for (j = 0; j < w; j++) {
      cdef_frame[plane][x0 + j][y0 + i] = frame[plane][x0 + j][y0 + i]
    }
}
