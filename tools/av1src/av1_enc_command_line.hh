/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <iosfwd>
#include <vector>

struct EncCommandLine
{
    EncCommandLine (int argc, char **argv);

    void syntax (std::ostream &os) const;

    const char *app_name;

    int         seed;
    const char *filename;

    typedef std::pair<std::string, unsigned> elt_dbg_pair_t;
    typedef std::vector<elt_dbg_pair_t>      elt_dbg_vec_t;

    const char    *config_file;
    const char    *config_selected;
    elt_dbg_vec_t  config_element_debug;

    bool        large_scale_tile;
    bool        annex_b;

#if !RELEASE
    const char *choose_all_filename;
    unsigned    decrease_target_bitrate_pc;
#endif

    bool        verbose;

private:
    void parse (int argc, char **argv);
    void die (const char *msg) const;
    int read_int_arg (const char *name,
                      const char *arg,
                      bool        allow_negative) const;
    void read_debug_arg (const char *arg);

};
