/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

/*
Routines only used during encode
*/
#if ENCODE
// Check whether dim (either frame width or height) is acceptable. The
// crop tile requirement says that the right-most or bottom-most tile
// must not be between 1 and 7 modulo the superblock size.
//
// When we're doing large scale tile encoding, we actually require the
// right-most and bottom-most tiles (and all the others!) to be
// exactly one superblock wide and high.
can_meet_crop_tile_requirement(dim) {
  res = enc_use_128x128_superblock ? 128 : 64
  rem = dim % res
  return (rem == 0 || (rem >= 8 && ! enc_large_scale_tile))
}

enc_choose_frame_dim(uint32 lo, uint32 hi, isHeight) {
  return Choose(lo, hi)
}

// Choose a value in [lo, hi], biasing toward values
// of the form {2^k - 1, 2^k, or 2^k + 1}
enc_choose_bias_pow2(lo, hi, idx) {
  if (Choose(0, 99) < 20) {
    // Calculate log2(lo) and log2(hi), with two adjustments:
    // * We narrow the range by 1 on each side to account for
    //   the fact that we're going to add a random value in {-1, 0, +1}
    //   at the end
    // * We round the results *inward*.
    shiftLo = av1_ceil_log2(lo + 1)
    shiftHi = floor_log2(hi - 1)
    // If there are no powers of two in the valid range, then
    // shiftLo will be greater than shiftHi.
    // We need to detect this case to avoid Choose(...) breaking
    if (shiftLo <= shiftHi) {
      v = (1 << Choose(shiftLo, shiftHi)) + Choose(-1, 1)
      return v
    }
  }

  return Choose(lo, hi)
}

// Choose a symbol value in, using the same distribution as
// the CDF that symbol is going to be encoded with.
// This will tend to bias toward the cheapest thing to encode,
// without overly restricting our choices
//
// Note: You can deliberately set nsymbs less than the true number
// of symbols in the CDF. In that case, we'll only pick values up
// to nsymbs (inclusive)
enc_choose_from_cdf(uint16pointer cdf, nsymbs) {
  if (nsymbs == 1) {
    return 0
  }

  ASSERT(cdf[nsymbs-1] > 0, "Invalid CDF in enc_choose_from_cdf")
  z = Choose(0, cdf[nsymbs-1] - 1)
  for (i = 0; i < nsymbs; i++) {
    if (z < cdf[i]) {
      return i
    }
  }
}

enc_pick_tx_type(bsize,blocky,blockx,txSz) {
  txType = DCT_DCT
  qindex = seg_enabled ? get_qindex(1, segment_id) : base_qindex
  if (!skip_coeff && qindex > 0) {
    eset = get_ext_tx_set_type(bsize, reduced_tx_set_used, txSz)
    n = num_ext_tx_set[eset]
    txSzSqr = txsize_sqr_map[txSz]
    if (enc_maximize_symbols_per_bit) {
      if (is_inter) {
        cdfIdx = ext_tx_set_index_inter[eset]
        symbol = enc_choose_from_cdf(tile_inter_ext_tx_cdf[cdfIdx][txSzSqr], n)
      } else {
        cdfIdx = ext_tx_set_index_intra[eset]
        if (use_filter_intra) {
          intraDir = fimode_to_intradir[filter_intra_mode]
        } else {
          intraDir = y_mode
        }
        symbol = enc_choose_from_cdf(tile_intra_ext_tx_cdf[cdfIdx][txSzSqr][intraDir], n)
      }
    } else if (enc_force_tx_type != -1) {
      symbol = Max (ext_tx_set_ind [eset][enc_force_tx_type], 0)
    } else {
      w4 = tx_size_wide_unit[txSz]
      h4 = tx_size_high_unit[txSz]
      set = is_inter ? ext_tx_set_index_inter[eset] : ext_tx_set_index_intra[eset]
      ASSERT(set >= 0, "Error in setting tx type.")

      num_allowed_tx_types = 0
      for (tx_type = 0; tx_type < TX_TYPES; tx_type++) {
        tx_type_allowed[tx_type] = 0

        if (ext_tx_set_ind [eset][tx_type] != -1) {
          tx_type_allowed[tx_type] = 1
          allowed_tx_types[num_allowed_tx_types] = tx_type
          num_allowed_tx_types += 1
        }
      }

      enc_tx_type_cfg u(0)
      if (enc_tx_type_cfg != -1) {
        symbol = ext_tx_set_ind [eset][enc_tx_type_cfg]
        ASSERT(symbol >= 0, "The selected transform type is not permitted in this block.")
      } else {
        symbol = Choose (0, n - 1)
      }
    }
    txType = ext_tx_set_inv[eset][symbol]
  }

  return txType
}

encode_single_coefs(mi_row,mi_col,bsize,sb_size) {
  enc_chosen_plane u(0)
  if (enc_chosen_plane==0) {
    txsz = tx_size
    plane_bsize = bsize
  } else {
    txsz = get_uv_tx_size(tx_size,sb_size,mi_row,mi_col)
    plane_bsize = get_plane_block_size(bsize,enc_chosen_plane)
  }
  blockRowsUnit = block_size_high( mi_row, mi_col, plane_bsize, enc_chosen_plane ) >> tx_size_high_log2[0]
  blockColsUnit = block_size_wide( mi_row, mi_col, plane_bsize, enc_chosen_plane ) >> tx_size_wide_log2[0]
  enc_chosen_c = Choose(0,tx_size_wide[txsz]*tx_size_high[txsz]-1)
  enc_chosen_block_idx = Choose(0,blockRowsUnit*blockColsUnit-1)
}

uint_0_9 horizontal_partitions[4] = {
  PARTITION_HORZ, PARTITION_HORZ_A, PARTITION_HORZ_B, PARTITION_HORZ_4
};
uint_0_9 vertical_partitions[4] = {
  PARTITION_VERT, PARTITION_VERT_A, PARTITION_VERT_B, PARTITION_VERT_4
};

uint_0_9 encode_partition(mi_row, mi_col,has_rows,has_cols,bsize) {
  num4x4 = mi_size_wide[bsize]
  halfBlock4x4 = num4x4 >> 1
  quarterBlock4x4 = halfBlock4x4 >> 1

  enc_specify_partitions u(0)
  if (enc_specify_partitions) {
    if (bsize < BLOCK_8X8) {
      return PARTITION_NONE
    } else if (has_rows && has_cols) {
      enc_partition u(0)
      return enc_partition
    } else if (has_cols) {
      enc_split_or_horz u(0)
      return enc_split_or_horz ? PARTITION_SPLIT : PARTITION_HORZ
    } else if (has_rows) {
      enc_split_or_vert u(0)
      return enc_split_or_vert ? PARTITION_SPLIT : PARTITION_VERT
    } else {
      return PARTITION_SPLIT
    }
  }

  if (enc_maximize_symbols_per_bit) {
    // Use special case selection code for arithmetic decode
    // stress streams
    return encode_partition_arith_stress(mi_row,mi_col,has_rows,has_cols,bsize)
  }

  depth = 5 - mi_width_log2_lookup[bsize] // How many times have we split so far?

  minPartitionSize = enc_min_partition_size

  if (Choose(0,99) < enc_bias_to_big_partitions_prob) {
    // bias to choose larger partitions by far the most often
    diff = enc_max_partition_size - enc_min_partition_size
    offset = ChoosePowerLaw(diff+1) - 1  // ChoosePowerLaw choose from [1, high]
    minPartitionSize = enc_max_partition_size - offset
    ASSERT(enc_min_partition_size<=minPartitionSize && minPartitionSize<=enc_max_partition_size,"Choose our of range minPartitionSize")
  }

  minSqrSz = block_size_sqr_up_map[minPartitionSize]
  maxSqrSz = block_size_sqr_up_map[enc_max_partition_size]
  minIsSqr = (minSqrSz == enc_min_partition_size)
  maxIsSqr = (maxSqrSz == enc_max_partition_size)

  atMinSize = (bsize == minSqrSz)
  aboveMinSize = (bsize > minSqrSz)
  atMaxSize = (bsize == maxSqrSz)
  aboveMaxSize = (bsize > maxSqrSz)

  split_partition u(0)

  partition = PARTITION_INVALID // Used here to mean "not yet initialized"

  // Choose the desired partition
  if (aboveMaxSize) {
    partition = PARTITION_SPLIT
  } else if (atMaxSize) {
    if (aboveMinSize) {
      // At the max size, which is larger than the min size
      if (split_partition) {
        partition = PARTITION_SPLIT
      } else {
        if (maxIsSqr) {
          // Max size is square - so any partition type is valid here
          // We'll pick an option later
          :pass
        } else {
          // Max size is rectangular - so only one partition type is allowed
          partition = partition_map[enc_max_partition_size][depth]
        }
      }
    } else if (atMinSize) {
      // The max and min square sizes are equal.
      // Depending on the actual max and min sizes, we either
      // have one or two valid partition types here.
      // In either case, we don't want to split, so ignore 'split_partition'
      if (minIsSqr && maxIsSqr) {
        partition = PARTITION_NONE
      } else if (maxIsSqr) {
        // Min size is some rectangle below the max size
        if (ChooseBit()) {
          partition = PARTITION_NONE // Pick max size
        } else {
          partition = partition_map[enc_min_partition_size][depth] // Pick min size
        }
      } else {
        // Max size is a rectangle - this is only allowed if the min size
        // is the *same* rectangle
        ASSERT(enc_min_partition_size == enc_max_partition_size, "Invalid min/max partition sizes")
        partition = partition_map[enc_min_partition_size][depth]
      }
    } else {
      ASSERT(0, "Min partition size is smaller than max partition size!")
    }
  } else if (aboveMinSize) {
    // Not at max size, but still larger than min size
    if (split_partition) {
      partition = PARTITION_SPLIT
    } else {
      // Any partition type is valid here
      :pass
    }
  } else if (atMinSize) {
    // At the minimum size, and strictly smaller than the max size
    // Depending on the actual minimum size, we either have one or two valid
    // partition types
    if (minIsSqr) {
      partition = PARTITION_NONE
    } else {
      if (ChooseBit()) {
        partition = PARTITION_NONE // Keep the square size
      } else {
        partition = partition_map[enc_min_partition_size][depth] // Pick min size
      }
    }
  } else {
    // Below the minimum size. This can happen if we're near the edge
    // of the frame and we've been forced to split. In that case, we can't
    // stay within the desired range, so just take the largest block possible
    partition = PARTITION_NONE
  }

  // Apply constraints coming from the frame edges, and also
  // pick the partition if we haven't decided on one yet
  if (has_rows && has_cols) {
    if (partition == PARTITION_INVALID) {
      ASSERT(!split_partition, "If split_partition is true, we should have chosen a partition by now")
      // For encoding, we group partitions into three classes
      // ("none", "horizontal", and "vertical"), and give each class
      // a 1/3 chance of being selected.
      // Then we randomly select between the partitions in the chosen class
      if (bsize == BLOCK_8X8) {
        numHorzPartitions = 1
      } else if (bsize == BLOCK_128X128) {
        // 128x128 blocks only
        numHorzPartitions = 3
      } else {
        numHorzPartitions = 4
      }
      numVertPartitions = numHorzPartitions

      partClass = Choose(0, 3)
      if (partClass == 0) {
        partition = PARTITION_NONE
      } else if (partClass == 1) {
        partition = horizontal_partitions[Choose(0, numHorzPartitions-1)]
      } else {
        partition = vertical_partitions[Choose(0, numVertPartitions-1)]
      }
    } else {
      // Keep the selected partition
      :pass
    }
  } else if (!has_rows && has_cols) {
    // Only HORZ or SPLIT are allowed.
    if (partition == PARTITION_NONE || partition == PARTITION_HORZ ||
        partition == PARTITION_HORZ_4 || (enc_encourage_not_hasRows && ChooseBit())) {
      partition = PARTITION_HORZ
    } else {
      partition = PARTITION_SPLIT
    }
  } else if (has_rows && !has_cols) {
    // Only VERT or SPLIT are allowed
    if (partition == PARTITION_NONE || partition == PARTITION_VERT ||
        partition == PARTITION_VERT_4 || (enc_encourage_not_hasCols && ChooseBit())) {
      partition = PARTITION_VERT
    } else {
      partition = PARTITION_SPLIT
    }
  } else {
    // Only SPLIT is allowed
    partition = PARTITION_SPLIT
  }

  // Finally, if we're using 4:2:2 subsampling, then some block sizes
  // are disallowed because they would cause invalid chroma block sizes.
  // If that happens, default to splitting, since that's always valid
  subsize = get_subsize(bsize,partition)
  if (!monochrome && BLOCK_INVALID == get_plane_block_size(subsize, 1)) {
    partition = PARTITION_SPLIT
  }

  return partition
}

// Special partition choice function for the arithmetic decode stress streams.
// Note: We *don't* generate the 'split_partition' symbol here, meaning that
// we ignore enc_split_prob. Instead, we use the split probabilities provided
// by the default CDFs, which vary from ~5% to ~95% depending on the context
// but are mostly concentrated around 10-20%.
uint_0_9 encode_partition_arith_stress(mi_row,mi_col,has_rows,has_cols,bsize) {
  ctx = get_partition_ctx(mi_row, mi_col, bsize)
  uint16 tmp_cdf[3]

  if (has_rows && has_cols) {
    numPartitionTypes = num_partition_types(bsize)
    partition = enc_choose_from_cdf(tile_partition_cdf[ctx], numPartitionTypes)
  } else if (!has_rows && has_cols) {
    get_partition_vertlike_cdf(tile_partition_cdf[ctx], tmp_cdf, bsize)
    partition = enc_choose_from_cdf(tmp_cdf, 2) ? PARTITION_SPLIT : PARTITION_HORZ
  } else if (has_rows && !has_cols) {
    get_partition_horzlike_cdf(tile_partition_cdf[ctx], tmp_cdf, bsize)
    partition = enc_choose_from_cdf(tmp_cdf, 2) ? PARTITION_SPLIT : PARTITION_VERT
  } else {
    partition = PARTITION_SPLIT
  }
  return partition
}

enc_choose_mv(minMv, maxMv, multiple) {
  // we fiddle the distribution here to help with CROSS_MV_BIT coverage
  // bias towards the highest mv_classes if possible
  for (c = MV_CLASS_7; c <= MV_CLASS_10; c++) {
    // positive sign case
    mag = (get_mv_mag(c, 0) + 1 + (multiple - 1)) & ~(multiple - 1)
    if (mag >= minMv && mag <= maxMv) {
      minMv = mag
      // try to set and unset all the bits
      if (c == MV_CLASS_10) {
        bits = 0x3FF // 9 bits all set
        mag2 = (get_mv_mag(c, (bits<<3)) + 1 + (multiple - 1)) & ~(multiple - 1)
        if (mag2 > minMv && mag2 <= maxMv && ChooseBit()) {
          minMv = mag2
          ASSERT(minMv <= maxMv, "minMv <= maxMv should be true here too!")
          break
        } else if (ChooseBit()) { // all unset
          maxMv = minMv
          break
        }
      }
    }
    // negative sign case
    mag = ((get_mv_mag(c, 0) + 1 + (multiple - 1)) & ~(multiple - 1)) * -1
    if (mag >= minMv && mag <= maxMv) {
      maxMv = mag
      // try to set and unset all the bits
      if (c == MV_CLASS_10) {
        bits = 0x3FF // 9 bits all set
        mag2 = ((get_mv_mag(c, (bits<<3)) + 1 + (multiple - 1)) & ~(multiple - 1)) * -1
        if (mag2 > minMv && mag2 <= maxMv && ChooseBit()) {
          maxMv = mag2
          ASSERT(minMv <= maxMv, "minMv <= maxMv should be true here too!")
          break
        } else if (ChooseBit()) { // all unset
          minMv = maxMv
          break
        }
      }
    }
  }
  a = Choose(minMv, maxMv)
  while (a & (multiple-1)) {
    a = Choose(minMv, maxMv)
  }
  return a
}

enc_pick_mv(refList,usehp,useSubPel) {
  // When picking a motion vector, we want to allow a configuration
  // file to choose what happens. Since the normal logic for deciding
  // on an MV is... baroque... we say that the code may choose to
  // completely override the usual behaviour.
  //
  // To do so, there is an enc_specify_mv element. This should be 0 or
  // 1. It can see the components of the predicted MV (in pred_mv_x
  // and pred_mv_y).
  pred_y = best_mv[refList][0]
  pred_x = best_mv[refList][1]

  enc_specify_mv u(0)
  mv_overridden = enc_specify_mv
  if (mv_overridden) {
    for (i = 0; i < 2; i++) {
      enc_mv_coord u(0)
      target_mv[i] = enc_mv_coord
    }
    return 0
  }

  // Reduce the maximum size by 1 if we are not using hp (as this auto sets the bottom bit to 1)
  // Reduce the maximum size by 7 if we are not using fp & hp (as this sets the bottom 3 bits to 1)
  // Also reduce the maximum by 1 because we are not allowed to reach MV_UPP exactly (this seems a little strange)
  reduce = 8
  if (useSubPel) {
    reduce = 2
    if (usehp) {
      reduce = 1
    }
  }

  for (mvcomp = 0; mvcomp < 2; mvcomp++) {
    enc_mv_specific u(0)
    enc_mv_all u(0)
    enc_clamp_mv u(0)
    prediction = best_mv[refList][mvcomp]

    if (mvcomp) {
      range = enc_mv_max_col
    } else {
      range = enc_mv_max_row
    }

    // Normally, we want to limit the coded motion vector differences to [-range, range],
    // and there is a check in enc_mvcomp_sign() to make sure of this.
    // However, for very large streams (roughly, width + height >= 16k pixels),
    // there might not be *any* valid motion vectors in this range.
    //
    // If this happens, we clamp the target motion vector to the nearest valid MV.
    // However, this will lie outside the encoder's preferred range, so we need to
    // set a special variable enc_extend_newmv_range[]. This is used later on
    // to disable the range check in enc_mvcomp_sign().
    if (prediction-range > MV_UPP-reduce) {
      target_mv[mvcomp] = MV_UPP-reduce
      enc_extend_newmv_range[mvcomp] = 1
    } else if (prediction+range < MV_LOW+reduce) {
      target_mv[mvcomp] = MV_LOW+reduce
      enc_extend_newmv_range[mvcomp] = 1
    } else {
      enc_extend_newmv_range[mvcomp] = 0
      minMv = Max(prediction-range,MV_LOW+reduce)
      maxMv = Min(prediction+range,MV_UPP-reduce)
      ASSERT(minMv <= maxMv, "minMv <= maxMv should be true here!")
      if (enc_clamp_mv) {
        target_mv[mvcomp] = clamp(prediction, minMv, maxMv)
      } else if (enc_mv_specific) {
        if (Choose(0,99)>95) {
          if (mvcomp) {
            if ((enc_mv_specific % 8)==0) enc_mv_chosen_value=-15951
            else if ((enc_mv_specific % 8)==1) enc_mv_chosen_value=-15731
            else if ((enc_mv_specific % 8)==2) enc_mv_chosen_value=-11617
            else if ((enc_mv_specific % 8)==3) enc_mv_chosen_value=-4501
            else if ((enc_mv_specific % 8)==4) enc_mv_chosen_value=14115
            else if ((enc_mv_specific % 8)==5) enc_mv_chosen_value=14801
            else if ((enc_mv_specific % 8)==6) enc_mv_chosen_value=16231
            else if ((enc_mv_specific % 8)==7) enc_mv_chosen_value=16383
          }
          else {
            if ((enc_mv_specific % 22)==0) enc_mv_chosen_value=-16171
            else if ((enc_mv_specific % 22)==1) enc_mv_chosen_value=-15075
            else if ((enc_mv_specific % 22)==2) enc_mv_chosen_value=-14691
            else if ((enc_mv_specific % 22)==3) enc_mv_chosen_value=-14205
            else if ((enc_mv_specific % 22)==4) enc_mv_chosen_value=-12701
            else if ((enc_mv_specific % 22)==5) enc_mv_chosen_value=-12687
            else if ((enc_mv_specific % 22)==6) enc_mv_chosen_value=-12449
            else if ((enc_mv_specific % 22)==7) enc_mv_chosen_value=-11799
            else if ((enc_mv_specific % 22)==8) enc_mv_chosen_value=-11733
            else if ((enc_mv_specific % 22)==9) enc_mv_chosen_value=-6041
            else if ((enc_mv_specific % 22)==10) enc_mv_chosen_value=-4725
            else if ((enc_mv_specific % 22)==11) enc_mv_chosen_value=2501
            else if ((enc_mv_specific % 22)==12) enc_mv_chosen_value=4293
            else if ((enc_mv_specific % 22)==13) enc_mv_chosen_value=5231
            else if ((enc_mv_specific % 22)==14) enc_mv_chosen_value=5869
            else if ((enc_mv_specific % 22)==15) enc_mv_chosen_value=8401
            else if ((enc_mv_specific % 22)==16) enc_mv_chosen_value=9283
            else if ((enc_mv_specific % 22)==17) enc_mv_chosen_value=9359
            else if ((enc_mv_specific % 22)==18) enc_mv_chosen_value=11299
            else if ((enc_mv_specific % 22)==19) enc_mv_chosen_value=11977
            else if ((enc_mv_specific % 22)==20) enc_mv_chosen_value=14225
            else if ((enc_mv_specific % 22)==21) enc_mv_chosen_value=16383
          }
        } else {
          enc_mv_chosen_value=Choose(-56, 56)
        }
        target_mv[mvcomp] = clamp(prediction+enc_mv_chosen_value, minMv, maxMv)
      } else if (enc_mv_all) {
        enc_mv_min=minMv-prediction
        enc_mv_max=maxMv-prediction
        if (mvcomp) {
          if (usehp)
            target_mv[mvcomp] = prediction + (ChooseAll(enc_mv_min>>1, (enc_mv_max-1)>>1, MV_DIFF_COL_ODDS)<<1)|1
          else
            target_mv[mvcomp] = prediction + ChooseAll(enc_mv_min>>1, enc_mv_max>>1, MV_DIFF_COL_EVENS)<<1
        } else {
          if (usehp)
            target_mv[mvcomp] = prediction + (ChooseAll(enc_mv_min>>1, (enc_mv_max-1)>>1, MV_DIFF_ROW_ODDS)<<1)|1
          else
            target_mv[mvcomp] = prediction + ChooseAll(enc_mv_min>>1, enc_mv_max>>1, MV_DIFF_ROW_EVENS)<<1
        }
      } else {
        target_mv[mvcomp] = enc_choose_mv(minMv, maxMv, 1)
      }
    }
  }

  // Note: For any particular filter kernel, the largest intermediate values
  // can only be obtained when the fractional part of the motion vector is
  // exactly 1/2. The chance of this happening "naturally" is roughly 1/16
  // if allow_high_precision_mv == 0, or 1/64 if allow_high_precision_mv == 1.
  // Here we boost the probability by an extra 1/8.
  //
  // This is only really useful when doing non-scaled predictions, since
  // otherwise the motion vector will be rescaled anyway so we can't control
  // the filter offsets as easily
  refSlot = active_ref_idx[ref_frame[refList] - LAST_FRAME]
  if (!cur_frame_force_integer_mv &&
      ref_width[refSlot] == crop_width && ref_height[refSlot] == crop_height) {
    enc_pick_half_integer_mv u(0)
    if (enc_pick_half_integer_mv) {
      for (mvcomp = 0; mvcomp < 2; mvcomp++) {
        prediction = best_mv[refList][mvcomp]
        if (mvcomp) {
          minMv=Max(prediction-enc_mv_max_col, MV_LOW+reduce)
          maxMv=Min(prediction+enc_mv_max_col, MV_UPP-reduce)
        } else {
          minMv=Max(prediction-enc_mv_max_row, MV_LOW+reduce)
          maxMv=Min(prediction+enc_mv_max_row, MV_UPP-reduce)
        }
        // Round to the nearest location which has fractional part 1/2
        newValue = (target_mv[mvcomp] & ~7) + 4
        // Only use this new value if it's actually in range
        if (minMv <= newValue && newValue <= maxMv) {
          target_mv[mvcomp] = newValue
        }
      }
    }
  }
}

// This function implements the second half of the "extreme value" coverage
// path for the inter filter: If we coded a NEWMV type component and followed the
// special path which makes both motion vector components have a fractional part
// of 1/2, then we *also* want to set the interpolation filter to EIGHTTAP_SHARP.
//
// The logic here is that EIGHTTAP_SHARP, with fractional offset 1/2, gives the
// widest range of intermediate values in the interpolation filter. In fact,
// it's the only combination which can hit the theoretical min/max values.
enc_pick_switchable_interp_filters(mi_row, mi_col) {
  isCompound = (ref_frame[1] > INTRA_FRAME) ? 1 : 0
  halfPelMv = 0
  for (ref = 0; ref < 1 + isCompound; ref++) {
    refSlot = active_ref_idx[ref_frame[ref] - LAST_FRAME]
    if (ref_width[refSlot] == crop_width && ref_height[refSlot] == crop_height &&
        (mv[ref][0] & 7) == 4 && (mv[ref][1] & 7) == 4) {
      halfPelMv = 1
    }
  }

  if (halfPelMv && Choose(0, 99) < 95 && !enc_maximize_symbols_per_bit) {
    enc_interp_filter[0] = EIGHTTAP_SHARP
    enc_interp_filter[1] = EIGHTTAP_SHARP
  } else {
    dir = 0
    enc_interp_filter[dir] u(0)
    if (enable_dual_filter) {
      dir = 1
      enc_interp_filter[dir] u(0)
    } else {
      enc_interp_filter[1] = enc_interp_filter[0]
    }
  }
}

// Try to select a motion vector for intrabc mode.
// If we can find a valid MV, this function returns 1 and saves the selected
// motion vector into target_mv[].
// If we can't find a valid MV, this function returns 0.
int enc_pick_dv(mi_row, mi_col, bsize) {
  enc_try_pick_large_dv u(0)
  enc_force_intrabc u(0)

  // Work out the "allowable region" -
  // ie, the region of pixels we're allowed to predict from.
  // This region can always be broken down into 64px wide by 1 superblock high
  // (ie, either 64x64 or 64x128) rectangles.
  // So, for simplicity, we consider each superblock row within the tile, and
  // consider how many 64px wide units we're allowed to reference
  tile_sb_high = (mi_row_end - mi_row_start + (mib_size - 1)) >> mib_size_log2
  tile_sb64_wide = (mi_col_end - mi_col_start + (MI_SIZE_64X64 - 1)) >> MI_SIZE_64X64_LOG2

  active_sb_row = (mi_row - mi_row_start) >> mib_size_log2
  active_sb64_col = (mi_col - mi_col_start) >> (6 - MI_SIZE_LOG2)
  active_sb64 = (active_sb_row * tile_sb64_wide + active_sb64_col)
  last_valid_sb64 = active_sb64 - INTRABC_DELAY_SB64

  superblock_px = block_size_high_lookup[superblock_size]

  int32 sb64_cols_valid[MAX_SB_ROWS]
  sb_rows_valid = 0

  for (i = 0; i < tile_sb_high; i++) {
    // Each 64px wide unit is valid iff the following conditions hold:
    // Within the tile
    sb64_cols_valid[i] = tile_sb64_wide

    // Sufficient delay
    src_sb_row = i
    row_start_sb64 = src_sb_row * tile_sb64_wide
    sb64_cols_valid[i] = Min(sb64_cols_valid[i], last_valid_sb64 - row_start_sb64)

    // Wavefront
#if USE_WAVE_FRONT
    gradient = 1 + (superblock_size == BLOCK_128X128) + INTRABC_DELAY_SB64
    wf_offset = -INTRABC_DELAY_SB64 + (active_sb_row - src_sb_row) * gradient
    sb64_cols_valid[i] = Min(sb64_cols_valid[i], active_sb64_col + wf_offset)
#endif

    if (sb64_cols_valid[i] < (superblock_px / 64)) {
      // No further rows are valid
      sb_rows_valid = i
      break
    }
  }

  if (sb_rows_valid == 0) {
    return 0
  } else {
    bw = block_size_wide_lookup[bsize]
    bh = block_size_high_lookup[bsize]

    // Calculate the (x, y) coordinates, relative to the current tile, of:
    // * The top-left corner of the current block
    baseY = ((mi_row - mi_row_start) << MI_SIZE_LOG2)
    baseX = ((mi_col - mi_col_start) << MI_SIZE_LOG2)
    // * The source location if we were to use dv_ref as-is.
    refY = baseY + (dv_ref[0] >> 3)
    refX = baseX + (dv_ref[1] >> 3)

    // If this is a chroma reference block, we may have to constrain the
    // selected X and Y coordinates so that the chroma prediction doesn't run
    // off the top or left of the tile
    if (monochrome || !is_chroma_reference(mi_row, mi_col, bsize, 1)) {
      chromaExtendsLeft = 0
      chromaExtendsAbove = 0
    } else {
      chromaExtendsLeft = (subsampling_x && bw == 4)
      chromaExtendsAbove = (subsampling_y && bh == 4)
    }

    // Extra values to help avoid the source block running off the edge of
    // the tile (for the rightmost / bottommost tiles, which may not necessarily
    // be an integer number of superblocks wide/high)
    tileWidth = (mi_col_end - mi_col_start) << MI_SIZE_LOG2
    tileHeight = (mi_row_end - mi_row_start) << MI_SIZE_LOG2

    // Choose Y position relative to the tile
    // The constraints are:
    // * The resulting motion vector Y component must be in the range (MV_LOW, MV_UPP).
    //   (ie, the largest allowable value is MV_UPP-1 and the smallest is MV_LOW+1.
    //    To convert from 1/8 pel to pixels, we need to divide by 8, *rounding toward 0*)
    // * The coded motion vector delta must be within our allowed range
    // * The source must not extend off the tile edge, even if using an extended chroma prediction
    yMin = Max(baseY + ((MV_LOW + 1 + 7) >> 3), refY - (enc_mv_max_row >> 3))
    yMin = Max(yMin, (chromaExtendsAbove ? 4 : 0))
    yMax = Min(baseY + ((MV_UPP - 1) >> 3), refY + (enc_mv_max_row >> 3))
    yMax = Min(yMax, sb_rows_valid * superblock_px - bh)
    yMax = Min(yMax, tileHeight - bh)
    if (yMin > yMax) {
      return 0
    }
    if (!enc_try_pick_large_dv &&
        Choose(0, 99) < 10) {
      // Add an extra 10% chance of selecting a value on the edge of the allowed range
      y = ChooseBit() ? yMax : yMin
      enc_force_intrabc = 1
      target_px_y = (mi_row_start << MI_SIZE_LOG2) + y
      target_mv[0] = (target_px_y - (mi_row << MI_SIZE_LOG2)) << 3
    } else {
      // choose in terms of the actual thing we are going to encode
      targetY = ((mi_row_start << MI_SIZE_LOG2) - (mi_row << MI_SIZE_LOG2)) << 3
      optY = targetY - dv_ref[0]
      optYMin = optY + (yMin << 3)
      optYMax = optY + (yMax << 3)
      yPx = enc_choose_mv(optYMin, optYMax, 8)
      target_mv[0] = yPx + dv_ref[0]

      // reverse this for the checks
      y = (target_mv[0] >> 3) - ((mi_row_start << MI_SIZE_LOG2) - (mi_row << MI_SIZE_LOG2))
      ASSERT(yMin <= y && y <= yMax, "y out of range")
    }
    sb_row = (y + bh - 1) / superblock_px
    ASSERT(sb_row >= 0 && sb_row < sb_rows_valid, "A")

    // Then choose X position relative to the tile, using similar constraints to the Y position
    xMin = Max(baseX + ((MV_LOW + 1 + 7) >> 3), refX - (enc_mv_max_col >> 3))
    xMin = Max(xMin, (chromaExtendsLeft ? 4 : 0))
    xMax = Min(baseX + ((MV_UPP - 1) >> 3), refX + (enc_mv_max_col >> 3))
    xMax = Min(xMax, sb64_cols_valid[sb_row] * 64 - bw)
    xMax = Min(xMax, tileWidth - bw)
    if (xMin > xMax) {
      return 0
    }
    if (!enc_try_pick_large_dv &&
        Choose(0, 99) < 10) {
      // Add an extra 10% chance of selecting a value on the edge of the allowed range
      x = ChooseBit() ? xMax : xMin
      enc_force_intrabc = 1
      target_px_x = (mi_col_start << MI_SIZE_LOG2) + x
      target_mv[1] = (target_px_x - (mi_col << MI_SIZE_LOG2)) << 3
    } else {
      // choose in terms of the actual thing we are going to encode
      targetX = ((mi_col_start << MI_SIZE_LOG2) - (mi_col << MI_SIZE_LOG2)) << 3
      optX = targetX - dv_ref[1]
      optXMin = optX + (xMin << 3)
      optXMax = optX + (xMax << 3)
      xPx = enc_choose_mv(optXMin, optXMax, 8)
      target_mv[1] = xPx + dv_ref[1]

      // reverse this for the checks
      x = (target_mv[1] >> 3) - ((mi_col_start << MI_SIZE_LOG2) - (mi_col << MI_SIZE_LOG2))
      ASSERT(xMin <= x && x <= xMax, "x out of range")
    }
    sb64_col = (x + bw - 1) / 64
    ASSERT(sb64_col >= 0 && sb64_col < sb64_cols_valid[sb_row], "B")

    ASSERT(is_dv_valid(mi_row, mi_col, target_mv), "this should be valid here")

    // We always select motion vectors within the encoder's selected delta range.
    // So we keep the consistency checks in enc_mvcomp_sign()
    for (mvcomp = 0; mvcomp < 2; mvcomp++) {
      enc_extend_newmv_range[mvcomp] = 0
    }

    return 1
  }
}

uint2 enc_mv_joints(refList) {
  row_nonzero = (target_mv[0] != best_mv[refList][0])
  col_nonzero = (target_mv[1] != best_mv[refList][1])
  return ((row_nonzero << 1) | (col_nonzero))
}

uint1 enc_mvcomp_sign(refList,usehp,useSubPel,mvcomp) {
  mvDelta = target_mv[mvcomp] - best_mv[refList][mvcomp]
  CHECK(mvDelta != 0, "Coded mv diff should not be zero here")
  if (! (enc_stress_memory || mv_overridden || enc_extend_newmv_range[mvcomp])) {
    abs_max = mvcomp ? enc_mv_max_col : enc_mv_max_row
    CHECK (- abs_max <= mvDelta && mvDelta <= abs_max,
           "Coded mv diff is out of range (mvcomp %d; abs_max %d; mvDelta %d)",
           mvcomp, abs_max, mvDelta)
  }

  mag = Abs(mvDelta)-1
  // Determine the correct class
  enc_mvcomp_class = 0
  while (mag>=get_mv_mag(enc_mvcomp_class+1,0))
    enc_mvcomp_class += 1
  mag -= get_mv_mag(enc_mvcomp_class,0)
  enc_mvcomp_bits = mag>>3
  enc_mvcomp_fp = (mag>>1)&3
  enc_mvcomp_hp = mag & 1
  CHECK(enc_mvcomp_class<=10,"[MV_CLASS_TOO_LARGE] Internal error, needs too large a class to encode motion vector")

  return mvDelta<0
}

uint_0_2 enc_choose_drl_idx () {
  ASSERT(NumValidDrlIndices > 0, "We must have at least one valid DRL index.")
  enc_drl_idx_cfg u(0)
  return enc_drl_idx_cfg
}

enc_pick_inter_mode() {
  ft = ref_frame_type
  enc_inter_mode_newmv u(0)

  // deliberately skewed
  if (enc_inter_mode_newmv) {
    // We copy the values to NumValidDrlIndices and ValidDrlIndices[] here rather
    // than in enc_choose_drl_idx () because of issues with passing flat arrays
    NumValidDrlIndices = clamp(ref_mv_count[ft] - 1, 0, 2) + 1
    for (i = 0; i < NumValidDrlIndices; i++) {
      ValidDrlIndices[i] = all_drl_idcs[i]
    }
    enc_drl_idx = enc_many_mvs ? 0 : enc_choose_drl_idx ()
    return NEWMV
  }

  // For very large frames (width + height >= 16K), the global motion
  // vector might be outside the allowed range for block MVs.
  // If so, we're not allowed to use GLOBALMV.
  canUseGlobal = 1
  for (j = 0; j < 2; j++) {
    if (globalmv[0][j] >= MV_UPP || globalmv[0][j] <= MV_LOW) {
      canUseGlobal = 0
      break
    }
  }

  int validDrlIdx[3]
  int numValidDrlIdx
  numValidDrlIdx = 0

  // check validity of nearest and near
  // Note: For single blocks, we either use an entry from the ref mv list
  // (for nearest, or near if ref_mv_idx == 0) or the ref mv stack
  // (for near, if ref_mv_idx > 0). So we need to check these appropriately.
  canUseNearest = (nearest_mv[0][0] < MV_UPP && nearest_mv[0][0] > MV_LOW &&
                   nearest_mv[0][1] < MV_UPP && nearest_mv[0][1] > MV_LOW)

  if (near_mv[0][0] < MV_UPP && near_mv[0][0] > MV_LOW &&
      near_mv[0][1] < MV_UPP && near_mv[0][1] > MV_LOW) {
    // In this case, NEARMV with DRL index 0 is valid
    validDrlIdx[numValidDrlIdx] = 0
    numValidDrlIdx += 1
  }
  for (i = 1; i < Min(ref_mv_count[ft] - 1, 3); i++) {
    valid = 1
    for (j = 0; j < 2; j++) {
      valid = valid && (ref_mv_stack[ft][1 + i][0][j] < MV_UPP &&
                        ref_mv_stack[ft][1 + i][0][j] > MV_LOW)
    }
    if (valid) {
      validDrlIdx[numValidDrlIdx] = i
      numValidDrlIdx += 1
    }
  }

  canUseNear = (numValidDrlIdx > 0)

  int validModes[3]
  int mode_is_valid[4]
  mode_is_valid [0] = canUseNearest
  mode_is_valid [1] = canUseNear
  mode_is_valid [2] = canUseGlobal
  mode_is_valid [3] = 1

  numValidModes = 0
  if (canUseNearest) {
    validModes[numValidModes] = NEARESTMV
    numValidModes += 1
  }
  if (canUseNear) {
    validModes[numValidModes] = NEARMV
    numValidModes += 1
  }
  if (canUseGlobal) {
    validModes[numValidModes] = GLOBALMV
    numValidModes += 1
  }

  if (numValidModes == 0) {
    mode = NEWMV
  } else {
    enc_inter_mode u(0)
    mode = enc_inter_mode

    CHECK (NEARESTMV <= enc_inter_mode && enc_inter_mode <= NEWMV,
           "enc_inter_mode is %d which isn't a single inter-pred mode.",
           enc_inter_mode)
    CHECK (mode_is_valid [enc_inter_mode - NEARESTMV],
           "enc_inter_mode is %d, which isn't valid.", enc_inter_mode)
  }

  // Select a DRL index to use
  if (mode == NEARMV) {
    // We copy the values to NumValidDrlIndices and ValidDrlIndices[] here rather
    // than in enc_choose_drl_idx () because of issues with passing flat arrays
    NumValidDrlIndices = numValidDrlIdx
    for (i = 0; i < NumValidDrlIndices; i++) {
      ValidDrlIndices[i] = validDrlIdx[i]
    }
    enc_drl_idx = enc_choose_drl_idx ()
  } else if (mode == NEWMV) {
    // We copy the values to NumValidDrlIndices and ValidDrlIndices[] here rather
    // than in enc_choose_drl_idx () because of issues with passing flat arrays
    NumValidDrlIndices = clamp(ref_mv_count[ft] - 1, 0, 2) + 1
    for (i = 0; i < NumValidDrlIndices; i++) {
      ValidDrlIndices[i] = all_drl_idcs[i]
    }
    enc_drl_idx = enc_choose_drl_idx ()
  }

  return mode
}

enc_pick_comp_mode() {
  ft = ref_frame_type

  // deliberately skewed
  enc_comp_mode_newmv u(0)
  newMv1 = enc_comp_mode_newmv
  enc_comp_mode_newmv u(0)
  newMv2 = enc_comp_mode_newmv
  if (newMv1 && newMv2) {
    // We copy the values to NumValidDrlIndices and ValidDrlIndices[] here rather
    // than in enc_choose_drl_idx () because of issues with passing flat arrays
    NumValidDrlIndices = clamp(ref_mv_count[ft] - 1, 0, 2) + 1
    for (i = 0; i < NumValidDrlIndices; i++) {
      ValidDrlIndices[i] = all_drl_idcs[i]
    }
    enc_drl_idx = enc_many_mvs ? 0 : enc_choose_drl_idx ()
    return NEW_NEWMV
  }

  // Similarly to the single case, GLOBALMV might be invalid
  canUseGlobal = 1
  for (i = 0; i < 2; i++) {
    for (j = 0; j < 2; j++) {
      if (globalmv[i][j] >= MV_UPP || globalmv[i][j] <= MV_LOW) {
        canUseGlobal = 0
        break
      }
    }
  }

  // check validity of nearest and near
  // Note: For compound blocks, we don't construct a ref mv list, so we
  // always use entries from the ref mv stack.
  canUseNearest1 = 1
  canUseNearest2 = 1
  for (j = 0; j < 2; j++) {
    canUseNearest1 = (canUseNearest1 &&
                      ref_mv_stack[ft][0][0][j] < MV_UPP &&
                      ref_mv_stack[ft][0][0][j] > MV_LOW)
    canUseNearest2 = (canUseNearest2 &&
                      ref_mv_stack[ft][0][1][j] < MV_UPP &&
                      ref_mv_stack[ft][0][1][j] > MV_LOW)
  }

  // Scan the DRL list to find out which values are valid for the three
  // modes {NEAR_NEWMV, NEW_NEARMV, NEAR_NEARMV}. Each mode may have a different
  // set of valid DRL indices, so we track them separately.
  int validDrlIdx[3][3]
  int numValidDrlIdx[3]
  for (i = 0; i < 3; i++) {
    numValidDrlIdx[i] = 0
  }

  for (i = 0; i < Min(ref_mv_count[ft] - 1, 3); i++) {
    valid0 = 1
    valid1 = 1
    for (j = 0; j < 2; j++) {
      valid0 = valid0 && (ref_mv_stack[ft][1 + i][0][j] < MV_UPP &&
                          ref_mv_stack[ft][1 + i][0][j] > MV_LOW)
      valid1 = valid1 && (ref_mv_stack[ft][1 + i][1][j] < MV_UPP &&
                          ref_mv_stack[ft][1 + i][1][j] > MV_LOW)
    }
    if (valid0) {
      validDrlIdx[0][numValidDrlIdx[0]] = i
      numValidDrlIdx[0] += 1
    }
    if (valid1) {
      validDrlIdx[1][numValidDrlIdx[1]] = i
      numValidDrlIdx[1] += 1
    }
    if (valid0 && valid1) {
      validDrlIdx[2][numValidDrlIdx[2]] = i
      numValidDrlIdx[2] += 1
    }
  }

  canUseNearNew  = (numValidDrlIdx[0] > 0) && !newMv1
  canUseNewNear  = (numValidDrlIdx[1] > 0) && !newMv2
  canUseNearNear = (numValidDrlIdx[2] > 0) && !newMv1 && !newMv2
  canUseNearest1 = canUseNearest1 && !newMv1
  canUseNearest2 = canUseNearest2 && !newMv2
  canUseGlobal   = canUseGlobal && !newMv1 && !newMv2

  numValidModes = 0
  int validModes[8]

  uint1 mode_validity [NEW_NEWMV - NEAREST_NEARESTMV + 1]
  mode_validity [NEW_NEWMV - NEAREST_NEARESTMV] = 1

  if (canUseNearest1 && canUseNearest2) {
    validModes[numValidModes] = NEAREST_NEARESTMV
    numValidModes += 1
  }
  mode_validity [0] = canUseNearest1 && canUseNearest2

  if (canUseNearNear) {
    validModes[numValidModes] = NEAR_NEARMV
    numValidModes += 1
  }
  mode_validity [1] = canUseNearNear

  if (canUseNearest1) {
    validModes[numValidModes] = NEAREST_NEWMV
    numValidModes += 1
  }
  mode_validity [2] = canUseNearest1

  if (canUseNearest2) {
    validModes[numValidModes] = NEW_NEARESTMV
    numValidModes += 1
  }
  mode_validity [3] = canUseNearest2

  if (canUseNearNew) {
    validModes[numValidModes] = NEAR_NEWMV
    numValidModes += 1
  }
  mode_validity [4] = canUseNearNew

  if (canUseNewNear) {
    validModes[numValidModes] = NEW_NEARMV
    numValidModes += 1
  }
  mode_validity [5] = canUseNewNear

  if (canUseGlobal) {
    validModes[numValidModes] = GLOBAL_GLOBALMV
    numValidModes += 1
  }
  mode_validity [6] = canUseGlobal

  enc_compound_mode u(0)

  CHECK (0 <= enc_compound_mode &&
         enc_compound_mode <= NEW_NEWMV - NEAREST_NEARESTMV,
         "compound_mode of %d not in range [0, NEW_NEWMV - NEAREST_NEARESTMV].",
         enc_compound_mode)
  CHECK (mode_validity [enc_compound_mode],
         "compound_mode is %d, which is not valid (see can_use_* variables).",
         enc_compound_mode)

  mode = enc_compound_mode + NEAREST_NEARESTMV

  // Pick a DRL index to use
  if (mode == NEAR_NEWMV) {
    // We copy the values to NumValidDrlIndices and ValidDrlIndices[] here rather
    // than in enc_choose_drl_idx () because of issues with passing flat arrays
    NumValidDrlIndices = numValidDrlIdx[0]
    for (i = 0; i < NumValidDrlIndices; i++) {
      ValidDrlIndices[i] = validDrlIdx[0][i]
    }
    enc_drl_idx = enc_choose_drl_idx ()
  } else if (mode == NEW_NEARMV) {
    // We copy the values to NumValidDrlIndices and ValidDrlIndices[] here rather
    // than in enc_choose_drl_idx () because of issues with passing flat arrays
    NumValidDrlIndices = numValidDrlIdx[1]
    for (i = 0; i < NumValidDrlIndices; i++) {
      ValidDrlIndices[i] = validDrlIdx[1][i]
    }
    enc_drl_idx = enc_choose_drl_idx ()
  } else if (mode == NEAR_NEARMV) {
    // We copy the values to NumValidDrlIndices and ValidDrlIndices[] here rather
    // than in enc_choose_drl_idx () because of issues with passing flat arrays
    NumValidDrlIndices = numValidDrlIdx[2]
    for (i = 0; i < NumValidDrlIndices; i++) {
      ValidDrlIndices[i] = validDrlIdx[2][i]
    }
    enc_drl_idx = enc_choose_drl_idx ()
  } else if (mode == NEW_NEWMV) {
    // We copy the values to NumValidDrlIndices and ValidDrlIndices[] here rather
    // than in enc_choose_drl_idx () because of issues with passing flat arrays
    NumValidDrlIndices = clamp(ref_mv_count[ft] - 1, 0, 2) + 1
    for (i = 0; i < NumValidDrlIndices; i++) {
      ValidDrlIndices[i] = all_drl_idcs[i]
    }
    enc_drl_idx = enc_choose_drl_idx ()
  }

  return mode
}

// Given a target MV, work out the best way to encode
// that MV. This requires the MV lists to have already
// been constructed, and target_mv[] to have been set.
// In addition, this assumes that the block in question is
// a single-ref block, to keep things simple
enc_pick_best_mv_mode(mi_row, mi_col) {
  ft = ref_frame_type

  //ty = target_mv[0]
  //tx = target_mv[1]
  //:C printf("Picking best MV for target_mv = {%d, %d} at MI pos (%d, %d)\n", ty, tx, mi_row, mi_col)

  // First, check if NEARESTMV or any of the NEARMVs
  // is equal to the desired MV
  if (nearest_mv[0][0] == target_mv[0] &&
      nearest_mv[0][1] == target_mv[1]) {
    //:C printf("  Matches NEARESTMV\n")
    mode = NEARESTMV
    enc_drl_idx = 0
    return mode
  }

  if (near_mv[0][0] == target_mv[0] &&
      near_mv[0][1] == target_mv[1]) {
    //:C printf("  Matches NEARMV[0]\n")
    mode = NEARMV
    enc_drl_idx = 0
    return mode
  }

  for (i = 1; i < Min(ref_mv_count[ft] - 1, 3); i++) {
    if (ref_mv_stack[ft][1 + i][0][0] == target_mv[0] &&
        ref_mv_stack[ft][1 + i][0][1] == target_mv[1]) {
      //:C printf("  Matches NEARMV[%d]\n", i)
      mode = NEARMV
      enc_drl_idx = i
      return mode
    }
  }

  // Don't bother checking GLOBALMV as we want to avoid warp
  // when running the memory stress tests

  // If we get here, none of the "fixed" modes work, so we're
  // going to use NEWMV. All that remains is to work out which
  // of the DRL entries is closest.
  // We use a very crude estimate, taking the cost to be
  // |deltax| + |deltay|

  // The selection of the base MV for NEWMV is a little weird:
  // * If we found at most one ref mv, then the base MV is the first entry
  //   of the ref MV list (currently stored in nearest_mv[]).
  //   In this case there's no choice anyway, so we can return immediately
  // * If we found more than one ref mv, the base MV is always taken
  //   from the ref MV stack
  mode = NEWMV
  if (ref_mv_count[ft] <= 1) {
    //refY = best_mv[0][0]
    //refX = best_mv[0][1]
    //:C printf("  No match; only one base for NEWMV = {%d, %d}\n", refY, refX)
    enc_drl_idx = 0
  } else if (ref_mv_count[ft] > 1) {
    minCost = 2147483647
    minCostIdx = -1
    //:C printf("  No match; NEWMV costs: ")
    for (i = 0; i < Min(ref_mv_count[ft], 3); i++) {
      refY = ref_mv_stack[ft][i][0][0]
      refX = ref_mv_stack[ft][i][0][1]
      cost = Abs(target_mv[0] - refY) + Abs(target_mv[1] - refX)
      //:C printf("%s{%d, %d} = %d", (i > 0) ? ", " : "", refY, refX, cost)
      if (cost < minCost) {
        minCost = cost
        minCostIdx = i
      }
    }
    //:C printf("; min cost = %d at index %d\n", minCost, minCostIdx)
    enc_drl_idx = minCostIdx
  }
  return mode
}

// Helpers: Calculate x / y and x % y, using *Python's* division
// scheme where the result of x / y is always rounded toward -infinity.
// This is in contrast to C's division scheme (which our '/' operator
// also uses), which rounds toward 0.
int py_divide(int x, int y) {
  v = x / y
  if (v * y > x) {
    v -= 1
  }
  return v
}

int py_modulo(int x, int y) {
  v = x % y
  if (v < 0) {
    v += y
  }
  return v
}

// Special functions to pick {ref frames, mode, DRL index, MV} for the
// memory stress streams
enc_pick_ref_frames_stress(mi_row, mi_col) {
  // Idea: We have a sort of "checkerboard" pattern, where
  // we cycle through refs 0, 1, 2, ..., 6 on the top row,
  // and then (logically) rotate the pattern by one MI column each row.
  // This gives a 2D pattern like:
  // 0, 1, 2, 3, 4, 5, 6, 0, 1, 2, 3, 4, 5, 6, ...
  // 6, 0, 1, 2, 3, ...
  // 5, 6, 0, 1, 2, ...
  // and so on.
  // To do this, we want to calculate (mi_row - mi_col) % 7
  // using *Python's* way of calculating modulo, ie. always giving
  // a positive result.
  v = py_modulo(mi_col - mi_row, 7)

  enc_ref_frame[mi_col][mi_row][0] = LAST_FRAME + v
  enc_ref_frame[mi_col][mi_row][1] = NONE // All blocks are single blocks here
}

enc_pick_inter_mode_stress(mi_row, mi_col) {
  // Work out which "region" to target with this block.
  // Idea: Each diagonal line generated by 'enc_pick_ref_frames_stress'
  // references the *same* source region, so that we can save many bits
  // by copying motion vectors down along that diagonal.
  // Thus the "region index" ends up being:
  v = py_divide(mi_col - mi_row, 7)

  // Work out how many 4x4 units we are "along" this diagonal.
  // Since the diagonals all start at either the top or left edge of
  // the frame, this will be the smaller of mi_row or mi_col.
  offset = Min(mi_row, mi_col)

  // Within a ref frame, we cycle through the available source regions
  slot = active_ref_idx[ref_frame[0]]
  refRegionsW = (ref_width[slot] + 63) / 64
  refRegionsH = (ref_height[slot] + 15) / 16

  targetRegionX = py_modulo(v, refRegionsW)
  targetRegionY = py_modulo(py_divide(v, refRegionsW), refRegionsH)

  // Construct an MV pointing from (0, 4*mi_col) to somewhere
  // in the desired region (currently the top left corner for simplicity)
  // Note: To minimize bitrate, this should depend *only* on mi_col,
  // so that all blocks in a column have the same MV. In particular,
  // we do not want to introduce extra randomness
  srcX = 4*mi_col
  srcY = 4*mi_row
  dstX = 64*targetRegionX + 4*offset
  dstY = 16*targetRegionY + 4*offset

  // Construct the target MV, and also offset by half a pixel
  // so that the decoder has to apply filtering
  target_mv[0] = ((dstY - srcY) << 3) + 4
  target_mv[1] = ((dstX - srcX) << 3) + 4

  // Wrap the motion vector so that it stays in the range
  // [-1024, +1024) pixels.
  // This ensures that no motion vector delta will be larger
  // than 2048 pixels, which is the largest value we can code.
  // The easiest way to wrap into this range is to offset by 1024
  // pixels, reduce modulo 2048 pixels into the range [0, 2048) pixels,
  // then remove the offset.
#define MV_OFF (1024 << 3)
#define MV_WRAP (2048 << 3)

  target_mv[0] = py_modulo((target_mv[0] + MV_OFF), MV_WRAP) - MV_OFF
  target_mv[1] = py_modulo((target_mv[1] + MV_OFF), MV_WRAP) - MV_OFF

#undef MV_OFF
#undef MV_WRAP

  mode = enc_pick_best_mv_mode(mi_row, mi_col)
  return mode
}

enc_pick_comp_inter_mode_stress(mi_col) {
  ASSERT(0, "Memory stress streams shold not contain compound blocks")
}

// Another function, for picking inter modes for "arithmetic decode"
// stress streams
// For simplicity, we assume that the frame sizes are not large enough
// that we have to deal with some options being invalid
int enc_pick_inter_mode_arith_stress() {
  ctx = av1_mode_context_analyzer(-1)

  modectx = ctx & NEWMV_CTX_MASK
  is_newmv = !enc_choose_from_cdf(tile_newmv_cdf[modectx], 2)
  if (is_newmv) {
    syntax = NEWMV
  } else {
    modectx = (ctx >> GLOBALMV_OFFSET) & GLOBALMV_CTX_MASK
    is_globalmv = !enc_choose_from_cdf(tile_globalmv_cdf[modectx], 2)
    if (is_globalmv) {
      syntax = GLOBALMV
    } else {
      modectx = (ctx >> REFMV_OFFSET) & REFMV_CTX_MASK
      is_refmv = !enc_choose_from_cdf(tile_refmv_cdf[modectx], 2)
      if (is_refmv) {
        syntax = NEARESTMV
      } else {
        syntax = NEARMV
      }
    }
  }
  // TODO: Pick DRL indices?
  return syntax
}

encode_frame_init() {
  enc_split_prob u(0)
  enc_skip_prob u(0)
  enc_coef_max u(0)
  enc_zero_coef_prob u(0)
  enc_zero_rowcol_prob u(0)
  enc_tx_coeffs_per_million u(0)
  enc_inter_prob u(0)
  enc_compound_prob u(0)
  enc_empty_block_prob u(0)
  enc_palette_prob u(0)
  enc_target_tx_size u(0) // Only used for "inverse txfm performance" stress streams
  enc_min_partition_size u(0)
  enc_max_partition_size u(0)
  enc_bias_to_big_partitions_prob u(0)
  enc_use_large_coefs_prob u(0)
  enc_mv_small_prob u(0)
  enc_mv_log2 u(0)
  enc_mv_small u(0)
  enc_mv_max_row u(0)
  enc_mv_max_col u(0)

  timebase_low =  timebase_low + 1
  enc_frames_left = enc_frames_left - 1
}

encode_choose_stress_resolution() {
  enc_license_resolution_w u(0)
  enc_license_resolution_h u(0)
  enc_licence_frame_cnt = 50
}

encode_choose_max_bitrate() {
  enc_min_level u(0)
  enc_max_level u(0)
  enc_choose_max_bitrate u(0)
  if (enc_choose_max_bitrate) {
    enc_profile u(0)
    enc_override_tier u(0)
    enc_license_resolution_specific_bitrate = 1
    ASSERT(enc_min_level == enc_max_level, "min (%d) and max (%d) levels must match",enc_min_level,enc_max_level)
    enc_target_bits_per_frame = level_bitrate[enc_min_level][enc_override_tier]
    :C printf("Default is enc_target_bits_per_frame %" PRIu64, b->global_data->enc_target_bits_per_frame);
    if (enc_decrease_target_bitrate_percent) {
      enc_target_bits_per_frame = (enc_target_bits_per_frame * (100 - enc_decrease_target_bitrate_percent)) / 100
      :C printf(", decreasing by %d percent to enc_target_bits_per_frame %" PRIu64, b->global_data->enc_decrease_target_bitrate_percent, b->global_data->enc_target_bits_per_frame);
    }
    :C printf("\n");
  } else {
    enc_license_resolution_specific_bitrate = 0
  }
}

//Code generated by script product/create_special.py
encode_choose_license_resolution() {
  enc_license_resolution_specific_bitrate = 0
  if (enc_client_a_custom_set) {
    enc_license_resolution = enc_stream_seed%25
    if (enc_license_resolution == 0) {
      enc_license_resolution_w = 176
      enc_license_resolution_h = 144
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 1) {
      enc_license_resolution_w = 176
      enc_license_resolution_h = 144
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 2) {
      enc_license_resolution_w = 176
      enc_license_resolution_h = 144
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 3) {
      enc_license_resolution_w = 176
      enc_license_resolution_h = 144
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 4) {
      enc_license_resolution_w = 176
      enc_license_resolution_h = 144
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 5) {
      enc_license_resolution_w = 352
      enc_license_resolution_h = 288
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 6) {
      enc_license_resolution_w = 352
      enc_license_resolution_h = 288
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 7) {
      enc_license_resolution_w = 352
      enc_license_resolution_h = 288
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 8) {
      enc_license_resolution_w = 352
      enc_license_resolution_h = 288
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 9) {
      enc_license_resolution_w = 352
      enc_license_resolution_h = 288
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 10) {
      enc_license_resolution_w = 1280
      enc_license_resolution_h = 720
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 11) {
      enc_license_resolution_w = 1280
      enc_license_resolution_h = 720
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 12) {
      enc_license_resolution_w = 1280
      enc_license_resolution_h = 720
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 13) {
      enc_license_resolution_w = 1280
      enc_license_resolution_h = 720
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 14) {
      enc_license_resolution_w = 1280
      enc_license_resolution_h = 720
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 15) {
      enc_license_resolution_w = 1920
      enc_license_resolution_h = 1080
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 16) {
      enc_license_resolution_w = 1920
      enc_license_resolution_h = 1080
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 17) {
      enc_license_resolution_w = 1920
      enc_license_resolution_h = 1080
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 18) {
      enc_license_resolution_w = 1920
      enc_license_resolution_h = 1080
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 19) {
      enc_license_resolution_w = 1920
      enc_license_resolution_h = 1080
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 20) {
      enc_license_resolution_w = 3840
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 21) {
      enc_license_resolution_w = 3840
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 22) {
      enc_license_resolution_w = 3840
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 23) {
      enc_license_resolution_w = 3840
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 24) {
      enc_license_resolution_w = 3840
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
    }
  } else if (!enc_large_scale_tile) {
    enc_license_resolution = enc_stream_seed%55
    // This list is arranged in three sections:
    // * Seeds 1-18 are the resolutions specified in the AV1 streams contracts
    //   (2 streams per resolution, at 10 frames each)
    // * Seeds 19-25 are the extra streams specified in the ClientF contract, which
    //   have specific resolutions *and* specific numbers of bits per frame
    // * Seeds 26+ are various extra resolutions and can be changed freely
    //
    // Resolutions specified in the AV1 streams contracts - we do two each of these
    if (enc_license_resolution == 0) {
      enc_license_resolution_w = 1280
      enc_license_resolution_h = 720
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 1) {
      enc_license_resolution_w = 1280
      enc_license_resolution_h = 720
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 2) {
      enc_license_resolution_w = 1280
      enc_license_resolution_h = 1024
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 3) {
      enc_license_resolution_w = 1280
      enc_license_resolution_h = 1024
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 4) {
      enc_license_resolution_w = 1600
      enc_license_resolution_h = 1200
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 5) {
      enc_license_resolution_w = 1600
      enc_license_resolution_h = 1200
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 6) {
      enc_license_resolution_w = 1920
      enc_license_resolution_h = 1080
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 7) {
      enc_license_resolution_w = 1920
      enc_license_resolution_h = 1080
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 8) {
      enc_license_resolution_w = 2048
      enc_license_resolution_h = 1080
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 9) {
      enc_license_resolution_w = 2048
      enc_license_resolution_h = 1080
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 10) {
      enc_license_resolution_w = 4096
      enc_license_resolution_h = 2048
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 11) {
      enc_license_resolution_w = 4096
      enc_license_resolution_h = 2048
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 12) {
      enc_license_resolution_w = 4096
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 13) {
      enc_license_resolution_w = 4096
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 14) {
      enc_license_resolution_w = 4096
      enc_license_resolution_h = 3072
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 15) {
      enc_license_resolution_w = 4096
      enc_license_resolution_h = 3072
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 16) {
      enc_license_resolution_w = 8192
      enc_license_resolution_h = 4320
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 17) {
      enc_license_resolution_w = 8192
      enc_license_resolution_h = 4320
      enc_licence_frame_cnt = 10
    }
    // Extra streams specified in the ClientF contract
    // The requested bitrates are:
    //   4K (4096x2160) @ 1Mb per frame and 0.5Mb per frame.
    //   1080p@ 0.667Mb per frame and 0.2667Mb per frame.
    //   3840x2160 @ 1.3Mb and 2.6Mb per frame
    //   7680x4320 @ 5.3Mb per frame
    else if (enc_license_resolution == 18) {
      enc_license_resolution_w = 4096
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
      enc_license_resolution_specific_bitrate = 1
      enc_target_bits_per_frame = 1000000     // ~0.11 bits per luma pixel
    }
    else if (enc_license_resolution == 19) {
      enc_license_resolution_w = 4096
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
      enc_license_resolution_specific_bitrate = 1
      enc_target_bits_per_frame = 500000     // ~0.06 bits per luma pixel
    }
    else if (enc_license_resolution == 20) {
      enc_license_resolution_w = 1920
      enc_license_resolution_h = 1080
      enc_licence_frame_cnt = 10
      enc_license_resolution_specific_bitrate = 1
      enc_target_bits_per_frame = 667000     // ~0.32 bits per luma pixel
    }
    else if (enc_license_resolution == 21) {
      enc_license_resolution_w = 1920
      enc_license_resolution_h = 1080
      enc_licence_frame_cnt = 10
      enc_license_resolution_specific_bitrate = 1
      enc_target_bits_per_frame = 266700     // ~0.13 bits per luma pixel
    }
    else if (enc_license_resolution == 22) {
      enc_license_resolution_w = 3840
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
      enc_license_resolution_specific_bitrate = 1
      enc_target_bits_per_frame = 1300000     // ~0.16 bits per luma pixel
    }
    else if (enc_license_resolution == 23) {
      enc_license_resolution_w = 3840
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
      enc_license_resolution_specific_bitrate = 1
      enc_target_bits_per_frame = 2600000     // ~0.31 bits per luma pixel
    }
    else if (enc_license_resolution == 24) {
      enc_license_resolution_w = 7680
      enc_license_resolution_h = 4320
      enc_licence_frame_cnt = 10
      enc_license_resolution_specific_bitrate = 1
      enc_target_bits_per_frame = 5300000     // ~0.16 bits per luma pixel
    }
    // Extra resolutions not specified in the contracts:
    else if (enc_license_resolution == 25) {
      enc_license_resolution_w = 320
      enc_license_resolution_h = 240
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 26) {
      enc_license_resolution_w = 352
      enc_license_resolution_h = 240
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 27) {
      enc_license_resolution_w = 352
      enc_license_resolution_h = 288
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 28) {
      enc_license_resolution_w = 352
      enc_license_resolution_h = 576
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 29) {
      enc_license_resolution_w = 640
      enc_license_resolution_h = 360
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 30) {
      enc_license_resolution_w = 640
      enc_license_resolution_h = 480
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 31) {
      enc_license_resolution_w = 704
      enc_license_resolution_h = 480
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 32) {
      enc_license_resolution_w = 720
      enc_license_resolution_h = 480
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 33) {
      enc_license_resolution_w = 704
      enc_license_resolution_h = 576
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 34) {
      enc_license_resolution_w = 720
      enc_license_resolution_h = 576
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 35) {
      enc_license_resolution_w = 864
      enc_license_resolution_h = 480
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 36) {
      enc_license_resolution_w = 800
      enc_license_resolution_h = 600
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 37) {
      enc_license_resolution_w = 960
      enc_license_resolution_h = 540
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 38) {
      enc_license_resolution_w = 1024
      enc_license_resolution_h = 768
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 39) {
      enc_license_resolution_w = 1280
      enc_license_resolution_h = 960
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 40) {
      enc_license_resolution_w = 1408
      enc_license_resolution_h = 960
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 41) {
      enc_license_resolution_w = 1408
      enc_license_resolution_h = 1152
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 42) {
      enc_license_resolution_w = 2048
      enc_license_resolution_h = 1024
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 43) {
      enc_license_resolution_w = 2048
      enc_license_resolution_h = 1536
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 44) {
      enc_license_resolution_w = 2560
      enc_license_resolution_h = 1440
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 45) {
      enc_license_resolution_w = 2560
      enc_license_resolution_h = 1920
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 46) {
      enc_license_resolution_w = 3616
      enc_license_resolution_h = 1536
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 47) {
      enc_license_resolution_w = 3680
      enc_license_resolution_h = 1536
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 48) {
      enc_license_resolution_w = 3840
      enc_license_resolution_h = 2160
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 49) {
      enc_license_resolution_w = 3840
      enc_license_resolution_h = 2176
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 50) {
      enc_license_resolution_w = 4096
      enc_license_resolution_h = 2176
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 51) {
      enc_license_resolution_w = 4096
      enc_license_resolution_h = 2304
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 52) {
      enc_license_resolution_w = 7680
      enc_license_resolution_h = 4320
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 53) {
      enc_license_resolution_w = 8192
      enc_license_resolution_h = 4096
      enc_licence_frame_cnt = 10
    }
    else if (enc_license_resolution == 54) {
      enc_license_resolution_w = 8192
      enc_license_resolution_h = 4352
      enc_licence_frame_cnt = 10
    }
  } else { //enc_large_scale_tile
    enc_license_resolution = enc_stream_seed%54

    // nb the tiles w & h need to be powers of two due to the way they are encoded
    if (enc_license_resolution == 0) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 4
      enc_tile_width_sb = 1
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 1) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 4
      enc_tile_width_sb = 8
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 2) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 8
      enc_tile_width_sb = 64
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 3) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 8
      enc_tile_width_sb = 4
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 4) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 16
      enc_tile_width_sb = 16
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 5) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 16
      enc_tile_width_sb = 32
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 6) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 32
      enc_tile_width_sb = 32
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 7) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 32
      enc_tile_width_sb = 64
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 8) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 64
      enc_tile_width_sb = 1
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 9) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 64
      enc_tile_width_sb = 2
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 10) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 4
      enc_tile_rows = 1
      enc_tile_width_sb = 3
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 11) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 4
      enc_tile_rows = 1
      enc_tile_width_sb = 4
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 12) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 8
      enc_tile_rows = 1
      enc_tile_width_sb = 5
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 13) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 8
      enc_tile_rows = 1
      enc_tile_width_sb = 6
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 14) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 16
      enc_tile_rows = 1
      enc_tile_width_sb = 7
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 15) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 16
      enc_tile_rows = 1
      enc_tile_width_sb = 8
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 16) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 1
      enc_tile_width_sb = 9
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 17) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 1
      enc_tile_width_sb = 10
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 18) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 2
      enc_tile_rows = 2
      enc_tile_width_sb = 11
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 19) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 2
      enc_tile_rows = 2
      enc_tile_width_sb = 12
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 20) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 4
      enc_tile_rows = 4
      enc_tile_width_sb = 13
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 21) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 4
      enc_tile_rows = 4
      enc_tile_width_sb = 14
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 22) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 8
      enc_tile_rows = 8
      enc_tile_width_sb = 15
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 23) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 8
      enc_tile_rows = 8
      enc_tile_width_sb = 16
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 24) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 16
      enc_tile_rows = 8
      enc_tile_width_sb = 8
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 25) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 16
      enc_tile_rows = 8
      enc_tile_width_sb = 4
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 26) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 8
      enc_tile_rows = 16
      enc_tile_width_sb = 16
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 27) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 4
      enc_tile_width_sb = 1
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 28) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 4
      enc_tile_width_sb = 8
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 29) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 8
      enc_tile_width_sb = 64
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 30) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 8
      enc_tile_width_sb = 4
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 31) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 16
      enc_tile_width_sb = 16
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 32) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 16
      enc_tile_width_sb = 32
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 33) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 32
      enc_tile_width_sb = 32
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 34) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 32
      enc_tile_width_sb = 64
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 35) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 64
      enc_tile_width_sb = 1
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 36) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 64
      enc_tile_width_sb = 2
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 37) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 4
      enc_tile_rows = 1
      enc_tile_width_sb = 3
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 38) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 4
      enc_tile_rows = 1
      enc_tile_width_sb = 4
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 39) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 8
      enc_tile_rows = 1
      enc_tile_width_sb = 5
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 40) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 8
      enc_tile_rows = 1
      enc_tile_width_sb = 6
      enc_tile_list_frame_cnt = 10
    }
    else if (enc_license_resolution == 41) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 16
      enc_tile_rows = 1
      enc_tile_width_sb = 7
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 42) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 16
      enc_tile_rows = 1
      enc_tile_width_sb = 8
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 43) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 1
      enc_tile_rows = 1
      enc_tile_width_sb = 9
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 44) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 1
      enc_tile_rows = 1
      enc_tile_width_sb = 10
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 45) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 2
      enc_tile_rows = 2
      enc_tile_width_sb = 11
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 46) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 2
      enc_tile_rows = 2
      enc_tile_width_sb = 12
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 47) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 4
      enc_tile_rows = 4
      enc_tile_width_sb = 13
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 48) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 4
      enc_tile_rows = 4
      enc_tile_width_sb = 14
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 49) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 8
      enc_tile_rows = 8
      enc_tile_width_sb = 15
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 50) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 8
      enc_tile_rows = 8
      enc_tile_width_sb = 16
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 51) {
      enc_use_128x128_superblock = 0
      enc_tile_cols = 16
      enc_tile_rows = 8
      enc_tile_width_sb = 8
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 52) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 16
      enc_tile_rows = 8
      enc_tile_width_sb = 4
      enc_tile_list_frame_cnt = 50
    }
    else if (enc_license_resolution == 53) {
      enc_use_128x128_superblock = 1
      enc_tile_cols = 8
      enc_tile_rows = 16
      enc_tile_width_sb = 16
      enc_tile_list_frame_cnt = 50
    }
    enc_tile_height_sb = 1
    res = enc_use_128x128_superblock ? 128 : 64
    enc_license_resolution_w = enc_tile_cols * enc_tile_width_sb * res
    enc_license_resolution_h = enc_tile_rows * enc_tile_height_sb * res
  }
}

// Most of the time, we pick a "good" combination of color parameters.
// That is, a set consisting only of named, non-"reserved" parameter values.
// The following are arrays of the possibilities:
#define NUM_GOOD_COLOR_PRIMARIES 12
#define NUM_GOOD_TRANSFER_CHARACTERISTICS 17
#define NUM_GOOD_MATRIX_COEFFICIENTS 13

uint8 good_color_primaries[NUM_GOOD_COLOR_PRIMARIES] = {
  AOM_CICP_CP_BT_709,
  AOM_CICP_CP_UNSPECIFIED,
  AOM_CICP_CP_BT_470_M,
  AOM_CICP_CP_BT_470_B_G,
  AOM_CICP_CP_BT_601,
  AOM_CICP_CP_SMPTE_240,
  AOM_CICP_CP_GENERIC_FILM,
  AOM_CICP_CP_BT_2020,
  AOM_CICP_CP_XYZ,
  AOM_CICP_CP_SMPTE_431,
  AOM_CICP_CP_SMPTE_432,
  AOM_CICP_CP_EBU_3213
};

uint8 good_transfer_characteristics[NUM_GOOD_TRANSFER_CHARACTERISTICS] = {
  AOM_CICP_TC_BT_709,
  AOM_CICP_TC_UNSPECIFIED,
  AOM_CICP_TC_BT_470_M,
  AOM_CICP_TC_BT_470_B_G,
  AOM_CICP_TC_BT_601,
  AOM_CICP_TC_SMPTE_240,
  AOM_CICP_TC_LINEAR,
  AOM_CICP_TC_LOG_100,
  AOM_CICP_TC_LOG_100_SQRT10,
  AOM_CICP_TC_IEC_61966,
  AOM_CICP_TC_BT_1361,
  AOM_CICP_TC_SRGB,
  AOM_CICP_TC_BT_2020_10_BIT,
  AOM_CICP_TC_BT_2020_12_BIT,
  AOM_CICP_TC_SMPTE_2084,
  AOM_CICP_TC_SMPTE_428,
  AOM_CICP_TC_HLG
};

uint8 good_matrix_coefficients[NUM_GOOD_MATRIX_COEFFICIENTS] = {
  // Don't include MC_IDENTITY - that is handled specially
  AOM_CICP_MC_BT_709,
  AOM_CICP_MC_UNSPECIFIED,
  AOM_CICP_MC_FCC,
  AOM_CICP_MC_BT_470_B_G,
  AOM_CICP_MC_BT_601,
  AOM_CICP_MC_SMPTE_240,
  AOM_CICP_MC_SMPTE_YCGCO,
  AOM_CICP_MC_BT_2020_NCL,
  AOM_CICP_MC_BT_2020_CL,
  AOM_CICP_MC_SMPTE_2085,
  AOM_CICP_MC_CHROMAT_NCL,
  AOM_CICP_MC_CHROMAT_CL,
  AOM_CICP_MC_ICTCP
};

int encode_pick_color_space() {
  enc_hit_color_primaries_branch u(0)

  // Color space: Most color spaces are okay for all profiles,
  // but sRGB requires 4:4:4 subsampling. Thus sRGB can only be selected
  // for profile 1, or for profile 2 with bit_depth==12.
  // To ensure it is used more, we give sRGB a 50% chance of being used
  // when it is valid. To do this, we pick it in advance.
  // Note: This can currently select "reserved" colour spaces - may need to
  // restrict the ranges we generate
  isSRGB = 0
  srgb_not_allowed = (enc_monochrome ||
                      (enc_profile == 0) ||
                      (enc_profile == 2 && enc_bit_depth != 12))
  if (! srgb_not_allowed)
    isSRGB u(0)

  useColorDescription u(0)

  // enc_color_primaries, enc_transfer_characteristics and
  // enc_matrix_coefficients have the default value of 256, meaning "invalid".
  enc_color_primaries u(0)
  enc_transfer_characteristics u(0)
  enc_matrix_coefficients u(0)

  // Have we overridden any of these elements?
  force_cps = enc_color_primaries != 256
  force_tcs = enc_transfer_characteristics != 256
  force_mcs = enc_matrix_coefficients != 256

  // Even though isSRGB and useColorDescription have been set, they haven't yet
  // been used for anything, so we can feel free to redefine them if necessary.

  if (force_mcs) {
      // If enc_matrix_coefficients != AOM_CICP_MC_IDENTITY then isSRGB cannot
      // be 1.
      if (enc_matrix_coefficients != AOM_CICP_MC_IDENTITY)
          isSRGB = 0

      // If enc_matrix_coefficients != AOM_CICP_MC_UNSPECIFIED then we must be
      // using color description.
      if (enc_matrix_coefficients != AOM_CICP_MC_UNSPECIFIED)
          useColorDescription = 1
  }

  if (force_cps) {
      // If enc_color_primaries != AOM_CICP_CP_BT_709 then isSRGB cannot
      // be 1.
      if (enc_color_primaries != AOM_CICP_CP_BT_709)
          isSRGB = 0

      // If enc_color_primaries != AOM_CICP_CP_UNSPECIFIED then we must be
      // using color description.
      if (enc_color_primaries != AOM_CICP_CP_UNSPECIFIED)
        useColorDescription = 1
  }

  if (force_tcs) {
      // If enc_transfer_characteristics != AOM_CICP_TC_SRGB then isSRGB cannot
      // be 1.
      if (enc_transfer_characteristics != AOM_CICP_TC_SRGB)
          isSRGB = 0

      // If enc_transfer_characteristics != AOM_CICP_TC_UNSPECIFIED then we must be
      // using color description.
      if (enc_transfer_characteristics != AOM_CICP_TC_UNSPECIFIED)
        useColorDescription = 1
  }

  can_use_identity = (enc_profile == 1 || (enc_profile == 2 && enc_bit_depth == 12))

  if (isSRGB) {
    // For sRGB, we must use a specific parameter set
    if (! force_cps) enc_color_primaries = AOM_CICP_CP_BT_709
    if (! force_tcs) enc_transfer_characteristics = AOM_CICP_TC_SRGB
    if (! force_mcs) enc_matrix_coefficients = AOM_CICP_MC_IDENTITY
  } else if (!useColorDescription) {
    // Similarly if we don't signal the colour space information at all
    if (! force_cps) enc_color_primaries = AOM_CICP_CP_UNSPECIFIED
    if (! force_tcs) enc_transfer_characteristics = AOM_CICP_TC_UNSPECIFIED
    if (! force_mcs) enc_matrix_coefficients = AOM_CICP_MC_UNSPECIFIED
  } else {
    // We can pick anything except for the sRGB parameter set here,
    // although MC_IDENTITY requires subsampling_x == subsampling_y == 0
    // and so may not be available.

    // Check if we can use MC_IDENTITY
    if (enc_monochrome) {
      // Note: Even though subsampling_x and subsampling_y are somewhat meaningless
      // in monochrome mode, it forces subsampling_x == subsampling_y == 1.
      // But MC_IDENTITY is only allowed if subsampling_x == subsampling_y == 0,
      // meaning MC_IDENTITY is not allowed in monochrome mode.
      can_use_identity = 0
    }

    if (!enc_hit_color_primaries_branch && (Choose(0, 99) < 80)) {
      // 80% of the time, pick a set where all values are "good"
      if (! force_cps) enc_color_primaries = good_color_primaries[Choose(0, NUM_GOOD_COLOR_PRIMARIES-1)]
      if (! force_tcs) enc_transfer_characteristics = good_transfer_characteristics[Choose(0, NUM_GOOD_TRANSFER_CHARACTERISTICS-1)]

      if (! force_mcs) {
        if (enc_color_primaries == AOM_CICP_CP_BT_709 &&
          enc_transfer_characteristics == AOM_CICP_TC_SRGB) {
          can_use_identity = 0
        }

        // We can't always use MC_IDENTITY - so, when it is possible, boost the chances
        // of selecting it to 10%
        if (can_use_identity && Choose(0,9) == 0)
          enc_matrix_coefficients = AOM_CICP_MC_IDENTITY
        else
          enc_matrix_coefficients = good_matrix_coefficients[Choose(0, NUM_GOOD_MATRIX_COEFFICIENTS-1)]
      }
    } else if (enc_hit_color_primaries_branch || ChooseBit()) {
      // 10% of the time try to help catch the false conditions on the special isSRGB set
      if (! force_cps) enc_color_primaries = AOM_CICP_CP_BT_709
      if (! force_tcs) enc_transfer_characteristics = AOM_CICP_TC_SRGB
      if (! force_mcs) enc_matrix_coefficients = good_matrix_coefficients[Choose(0, NUM_GOOD_MATRIX_COEFFICIENTS-1)]

      // now force one of the three branches
      prob = Choose(0,99)
      if (prob < 33) { // nothing to do
        :pass
      } else if (prob < 66) {
        while (! force_tcs && enc_transfer_characteristics == AOM_CICP_TC_SRGB) {
          enc_transfer_characteristics = good_transfer_characteristics[Choose(0, NUM_GOOD_TRANSFER_CHARACTERISTICS-1)]
        }
      } else {
        while (! force_cps && enc_color_primaries == AOM_CICP_CP_BT_709) {
          enc_color_primaries = good_color_primaries[Choose(0, NUM_GOOD_COLOR_PRIMARIES-1)]
        }
      }
    } else {
      // The other 10% of the time, pick an entirely random set
      if (! force_cps) enc_color_primaries = ChooseBits(8)
      if (! force_tcs) enc_transfer_characteristics = ChooseBits(8)

      if (! force_mcs) {
        if (enc_color_primaries == AOM_CICP_CP_BT_709 &&
            enc_transfer_characteristics == AOM_CICP_TC_SRGB) {
            can_use_identity = 0
        }

        if (can_use_identity && Choose(0,9) == 0)
          enc_matrix_coefficients = AOM_CICP_MC_IDENTITY
        else
          enc_matrix_coefficients = Choose(AOM_CICP_MC_BT_709, 255)
      }
    }
  }

  if (force_mcs) {
    // Check that matrix_coefficients == IDENTITY implies can_use_identity
    CHECK(can_use_identity || (enc_matrix_coefficients != AOM_CICP_MC_IDENTITY),
          "matrix_coefficients == IDENTITY either not compatible with profile or with monochrome stream")
  }

  if (! force_cps && ! force_tcs && ! force_mcs && ! enc_monochrome) {
    ASSERT(isSRGB == (enc_color_primaries == AOM_CICP_CP_BT_709 &&
                      enc_transfer_characteristics == AOM_CICP_TC_SRGB &&
                      enc_matrix_coefficients == AOM_CICP_MC_IDENTITY),
           "Invalid combination of isSRGB and colour information")
  }
}

encode_choose_profile() {
  enc_profile u(0)
  enc_bit_depth u(0)

  enc_monochrome = 0
  if (enc_profile != 1 && ! enc_large_scale_tile) {
    enc_monochrome u(0)
  }

  encode_pick_color_space ()

  if (enc_monochrome) {
    enc_subsampling_x = 1
    enc_subsampling_y = 1
  } else {
    // Calculate subsampling mode from profile + color space
    // This mirrors the logic in read_bitdepth_colorspace_subsampling(),
    // so that enc_subsampling_* have the correct values.
    if (enc_matrix_coefficients == AOM_CICP_MC_IDENTITY) {
      // Anything using the IDENTITY matrix coefficients requires 4:4:4 subsampling
      CHECK(enc_profile == 1 || (enc_profile == 2 && enc_bit_depth == 12),
            "matrix_coefficients == IDENTITY not compatible with profile")
      enc_subsampling_x = 0
      enc_subsampling_y = 0
    } else {
      if (enc_profile == 0) {
        // 4:2:0, 8/10 bit
        enc_subsampling_x = 1
        enc_subsampling_y = 1
      } else if (enc_profile == 1) {
        // 4:4:4, 8/10 bit
        enc_subsampling_x = 0
        enc_subsampling_y = 0
      } else {
        if (enc_bit_depth == 12) {
          // 4:4:4 / 4:2:2 / 4:2:0, 12-bit
          enc_subsampling_x u(0)
          if (enc_subsampling_x == 0)
            enc_subsampling_y = 0
          else
            enc_subsampling_y u(0)
        } else {
          // 4:2:2, 8/10 bit
          enc_subsampling_x = 1
          enc_subsampling_y = 0
        }
      }
    }
  }
}

enc_pick_anchor_frame_cnt() {
  maxAnchorFrameCnt = MAX_ANCHOR_FRAMES
  enc_limit_anchor_frames u(0)
  if ((!enc_is_license_resolution && enc_max_level > 0  && enc_limit_anchor_frames)) {
    // minimise the number of anchor frames most of the time
    maxAnchorFrameCnt = 3
  }
  return  Choose(1,maxAnchorFrameCnt)
}

encode_file_init() {
  enc_client_a_custom_set u(0)
  if (enc_client_a_custom_set) {
    for (i=6;i<9;i++) {
      level_max_pic_size[i][0] = 8294400
      level_max_pic_size[i][1] = 3840
      level_max_pic_size[i][2] = 2160
    }
  }

  per_stream_0 u(0)
  per_stream_1 u(0)
  per_stream_2 u(0)
  per_stream_3 u(0)
  per_stream_4 u(0)
  per_stream_5 u(0)
  per_stream_6 u(0)
  per_stream_7 u(0)

  enc_force_bug_libaom_2191 u(0)
  enc_num_shown_frames_required u(0)
  enc_is_stress_stream u(0)
  enc_is_license_resolution u(0)
  encode_choose_max_bitrate()
  if (enc_is_stress_stream) {
    encode_choose_stress_resolution()
  } else if (enc_is_license_resolution) {
    encode_choose_license_resolution()
  }
  enc_use_max_params_level u(0)

  if (enc_large_scale_tile) {
    enc_huge_picture = 0
  } else {
    enc_huge_picture u(0)
  }

  enc_exactly_one_tile u(0)
  if (enc_large_scale_tile) {
    enc_tile_group_focus = 0
  } else {
    enc_tile_group_focus u(0)
    if (enc_tile_group_focus) {
      ASSERT(!enc_large_scale_tile, "Tile group focus not designed to work with large scale tile")
      enc_tile_rows u(0)
      enc_tile_cols u(0)
      enc_tile_width_sb u(0)
      enc_tile_height_sb u(0)
    }
  }
  enc_use_frame_planner u(0)
  enc_stress_memory u(0) // Work out if we're generating a memory stress stream
  enc_maximize_symbols_per_bit u(0) // Work out if we're generating an arithmetic decode stress stream
  enc_force_tx_type u(0)
  enc_allow_too_many_tiles u(0)
  enc_force_max_tile_cols u(0)
  enc_force_max_tile_rows u(0)
  enc_hide_frame_prob u(0)
  enc_pick_still_picture()
  enc_pick_base_level()
  enc_pick_stream_size()
  //CHECK(enc_stream_width >= 384, "[ILLEGAL_RESOLUTION] Client specific size constraint, Width (%d) too small", enc_stream_width) //Client specific constrain - ClientC
  //CHECK(enc_stream_height >= 384, "[ILLEGAL_RESOLUTION] Client specific size constraint, Height (%d) too small", enc_stream_height) //Client specific constrain - ClientC
  //CHECK(enc_stream_width <= 8192, "[ILLEGAL_RESOLUTION] Client specific size constraint, Width (%d) too large",enc_stream_width) //Client specific constrain - ClientD
  //CHECK(enc_stream_height <= 8192, "[ILLEGAL_RESOLUTION] Client specific size constraint, Height (%d) too large",enc_stream_height) //Client specific constrain - ClientD
  //CHECK((enc_stream_width < enc_stream_height || enc_stream_height <= 4352),
  //       "[ILLEGAL_RESOLUTION] Client specific size constraint, Resolution (%d x %d) too large",enc_stream_width, enc_stream_height) //Client specific constrain - ClientD
  //CHECK((enc_stream_height < enc_stream_width || enc_stream_width <= 4352),
  //       "[ILLEGAL_RESOLUTION] Client specific size constraint, Resolution (%d x %d) too large",enc_stream_width, enc_stream_height) //Client specific constrain - ClientD
  enc_lossless_prob u(0)
  enc_force_seg_lvl_ref_frame_last_frame u(0)
  timebase_low = 0
  enc_allow_scaling u(0)
  enc_superres_prob u(0)
  enc_many_mvs u(0)
  encode_choose_profile()
  enc_tile_group_mode u(0)
  enc_force_intra_only_frames u(0)
  enc_use_profile_switching u(0)
  enc_allow_short_signalling u(0)
  if (enc_large_scale_tile) {
    enc_enable_order_hint = 0
  } else {
    enc_enable_order_hint u(0)
  }
  enc_order_hint_bits u(0)
  enc_enable_palette u(0)
  enc_schedule_frames() // Note: Depends on data from encode_choose_profile(), sets enc_frame_cnt

  // Set up our "leaky bit bucket" model (described in generate_one_tile())
  enc_use_bit_bucket u(0)
  if (enc_use_bit_bucket) {
    enc_bit_bucket_offset_factor u(0)
    enc_bit_bucket_leak_factor u(0)
    enc_bit_bucket_capacity = 0
  }
}

enc_setup_frame() {
  enc_show_existing_frame = sched_show_existing_frame[frame_number]
  if (enc_show_existing_frame) {
    enc_index_of_existing_frame = sched_index_of_existing_frame[frame_number]
  } else {
    enc_frame_type = sched_frame_type[frame_number]
    enc_show_frame = sched_show_frame[frame_number]
    enc_refresh_frame_flags = sched_refresh_frame_flags[frame_number]
    if (enc_frame_id_numbers_present_flag) {
      enc_current_frame_id = sched_frame_id[frame_number]
    }
  }

  if (!large_scale_tile) {
    camera_frame_header_ready = 0
  }
}

int floor_log2(int operand) {
  r = 0
  operand = operand >> 1
  while (operand) {
    r++
    operand = operand >> 1
  }
  return r
}

int ceil_log2(int operand) {
  r = floor_log2 (operand)
  if ((1 << r) < operand) {
    r++
  }
  return r
}

enc_pick_decoder_model_mode() {
  // If we can't use a decoder model anyway, it should be disabled.
  if (enc_is_license_resolution || enc_large_scale_tile ||
      enc_reduced_headers || enc_use_max_params_level ||
      enc_force_bug_libaom_2191) {
    enc_frame_min_width_limit u(0)
    enc_profile_min_width u(0)
    enc_profile_max_width u(0)
    enc_frame_min_height_limit u(0)
    enc_profile_min_height u(0)
    enc_profile_max_height u(0)
    return DECODER_MODEL_DISABLED
  }

  // Fall back on enc_use_decoder_model
  enc_use_decoder_model u(0)
  if (! enc_use_decoder_model) {
    return DECODER_MODEL_DISABLED
  }

  enc_decoder_model_use_schedule u(0)
  return (enc_decoder_model_use_schedule ?
          DECODER_MODEL_SCHEDULE :
          DECODER_MODEL_RESOURCE_AVAILABILITY)
}

// Decide on whether to use still picture mode or not
enc_pick_still_picture () {
  enc_still_picture = 0
  if (! enc_large_scale_tile) {
    enc_still_picture u(0)
  }
  if (enc_still_picture) {
    enc_reduced_headers u(0)
  } else {
    enc_reduced_headers = 0
  }
}

// Pick the desired level of operating point 0
enc_pick_base_level() {
  enc_decoder_model_mode = enc_pick_decoder_model_mode()

  enc_force_base_level u(0)
  if (enc_force_base_level >= 0) {
    // If enc_force_base_level is non-negative, this is the
    // (uncompacted) level to use.
    CHECK (enc_force_base_level < NUM_LEVELS,
           "Invalid level specified (%d). Maximum value %d.",
           enc_force_base_level, NUM_LEVELS)

    level = compact_level [enc_force_base_level]
    CHECK (level != -1,
           "Invalid level specified (%d, or %d.%d). Not defined in AV1 spec.",
           enc_force_base_level,
           2 + (enc_force_base_level >> 2),
           enc_force_base_level & 3)

    enc_min_level = level
    enc_max_level = level
  } else {
    // If enc_force_base_level is negative, we read min_level and
    // max_level explicitly.
    enc_min_level u(0)
    enc_max_level u(0)
  }

  // For large-scale-tile streams, we pick the size in a complicated way.
  // So, rather than picking the level early, we allow the level to be picked
  // by enc_pick_operating_point_level()
  //if (enc_large_scale_tile) {
  //  enc_base_level = -1
  //  return 0
  //}

  // Select a level group for this sequence to be in.
  // If the resolution is fixed, we need to find the appropriate size
  // group by scanning; otherwise we can select randomly
  if (enc_is_license_resolution) {
    for (group = level_to_group[enc_min_level]; group <= Min(level_to_group[enc_max_level],NUM_LEVEL_SIZE_GROUPS-1); group++) {
      uint32 maxW
      uint32 maxH
      uint32 maxArea
      level = enc_level_lookup[group][0]
      maxW = level_max_pic_size[level][1]
      maxH = level_max_pic_size[level][2]
      maxArea = level_max_pic_size[level][0]
      if (enc_license_resolution_w <= maxW && enc_license_resolution_h <= maxH &&
          enc_license_resolution_w * enc_license_resolution_h <= maxArea) {
        enc_size_group = group
        break
      }
    }
  } else {
    minGroup = level_to_group[enc_min_level]
    maxGroup = level_to_group[enc_max_level]

    enc_size_group = Choose(minGroup, maxGroup)
  }

  if (enc_decoder_model_mode == DECODER_MODEL_DISABLED) {
    // Take the lowest level within the selected group (unless it violates our min or max).
    enc_base_level = Min(Max(enc_level_lookup[enc_size_group][0],enc_min_level),enc_max_level)
  } else {
    // Pick a "speed category", corresponding to the four minor levels
    // for major levels 5.x and 6.x.
    // That is, we pick either 24-30fps, 48-60fps, 100-144fps, or
    // "100-144fps + extra hidden frames"
    // We can combine any level group and any speed group, as long as
    // we are careful about the frame sizes we pick.
    if (enc_min_level == enc_max_level) {
      for (enc_speed_group=0; enc_speed_group<4; enc_speed_group++) {
        if (enc_min_level == enc_level_lookup[enc_size_group][enc_speed_group]) {
          break
        }
      }
      ASSERT(enc_speed_group<4, "Couldn't find valid speed group")
      enc_framerate_choice = 2 // this is the default one
    } else {
      enc_speed_group u(0)
      enc_framerate_choice u(0)
    }
    enc_frame_rate = enc_preset_framerates[enc_speed_group][enc_framerate_choice]
    enc_base_level = enc_level_lookup[enc_size_group][enc_speed_group]
  }
}

enc_pick_stream_size_huge(choose_width_first) {
  uint32 minWidth
  uint32 minHeight
  enc_force_4096x4096 u(0)
  if (choose_width_first) {
    enc_profile_min_width u(0)
    enc_profile_max_width u(0)
    enc_profile_min_height u(0)
    enc_profile_max_height u(0)
  } else {
    enc_profile_min_height u(0)
    enc_profile_max_height u(0)
    enc_profile_min_width u(0)
    enc_profile_max_width u(0)
  }
  if (enc_force_4096x4096) {
      enc_stream_width = (enc_profile_max_width >= 0) ? Min(4096, enc_profile_max_width) : 4096
      enc_stream_height = (enc_profile_max_height >= 0) ? Min(4096, enc_profile_max_height) : 4096
      enc_use_128x128_superblock = 0
  }
  // In this case, we're creating a still picture which is deliberately too large
  // to fit into level 6.3.
  // We only generate one frame (handled separately) so that this doesn't take an
  // inordinate amount of time.
  // Further, to avoid having to generate many of these streams, we ensure that
  // every single stream hits at least one of the relevant coverage points.
  else if (ChooseBit()) {
    // Pick at least one of width or height to be outside the level 6.3 range,
    // but keep the area within 2x the range of level 6.3 to minimize the extra
    // resources required
    if (choose_width_first) {
      //enc_stream_width = MAX_FRAME_WIDTH
      //maxHeight = MAX_CODED_FRAME_AREA / enc_stream_width
      enc_stream_width = (enc_profile_max_width > 0) ? Min(MAX_FRAME_WIDTH, enc_profile_max_width) : MAX_FRAME_WIDTH
      maxHeight = (enc_profile_max_height > 0) ? Min(MAX_CODED_FRAME_AREA / enc_stream_width, enc_profile_max_height) : MAX_CODED_FRAME_AREA / enc_stream_width
      enc_stream_height = enc_choose_frame_dim(enc_profile_min_height < 0 ? enc_frame_min_height_limit : enc_profile_min_height, maxHeight, 1)
    } else {
      //enc_stream_height = MAX_FRAME_HEIGHT
      //maxWidth = MAX_CODED_FRAME_AREA / enc_stream_height
      enc_stream_height = (enc_profile_max_height > 0) ? Min(MAX_FRAME_HEIGHT, enc_profile_max_height) : MAX_FRAME_HEIGHT
      maxWidth = (enc_profile_max_width > 0) ? Min(MAX_CODED_FRAME_AREA / enc_stream_height, enc_profile_max_width) : MAX_CODED_FRAME_AREA / enc_stream_height
      enc_stream_width = enc_choose_frame_dim(enc_profile_min_width < 0 ? enc_frame_min_width_limit : enc_profile_min_width, maxWidth, 0)
    }
  } else {
    // Pick width and height within the level 6.3 limits, but ensure the area is
    // greater than 32M pixels.
    // To avoid needing too many extra resources, we don't allow the whole way up
    // to 16k x 8k (== 128M pixels), we only allow up to 64M pixels.
    level6MaxWidth = (enc_profile_max_width > 0) ? Min(level_max_pic_size[13][1], enc_profile_max_width) : level_max_pic_size[13][1]
    level6MaxHeight = (enc_profile_max_height > 0) ? Min(level_max_pic_size[13][2], enc_profile_max_height) : level_max_pic_size[13][2]
    level6MaxArea = (enc_profile_max_height > 0 && enc_profile_max_width > 0) ? Min(level_max_pic_size[13][0], enc_profile_max_width * enc_profile_max_height) : level_max_pic_size[13][0]
    if (choose_width_first) {
      minWidth = Min(level6MaxWidth, (level6MaxArea / level6MaxHeight) + 1)
      maxWidth = level6MaxWidth
      // Sometimes, make sure that the width and height are both >= 4096, so we can hit
      // 64x64 tiles
      if ( ChooseBit() ) {
          minWidth = Max(minWidth, Min(level6MaxWidth, 4096))
          maxWidth = Max(minWidth, Min(level6MaxWidth, MAX_CODED_FRAME_AREA/4096))
      }
      // Repeatedly pick widths until we find one which lets us hit the max area or above
      while_count = 0
      while (1) {
        enc_stream_width = Choose(minWidth, maxWidth)
        enc_stream_height = Min(level6MaxHeight, MAX_CODED_FRAME_AREA / enc_stream_width)
        if (enc_stream_height <= MAX_FRAME_HEIGHT && enc_stream_width * enc_stream_height >= Min(level6MaxArea,MAX_CODED_FRAME_AREA)) {
          break
        }
        while_count++
        ASSERT(while_count < 100,
               "Failed to find valid resolution with Width [%d - %d] and Height %d, failed to meet contraint area >= %d",
               minWidth, maxWidth, level6MaxHeight, Min(level6MaxArea,MAX_CODED_FRAME_AREA))
      }
    } else {
      minHeight = Min(level6MaxHeight, (level6MaxArea / level6MaxWidth) + 1)
      maxHeight = level6MaxHeight
      // Sometimes, make sure that the width and height are both >= 4096, so we can hit
      // 64x64 tiles
      if ( ChooseBit() ) {
          minHeight = Max(minHeight, Min(level6MaxHeight, 4096))
          maxHeight = Min(minHeight, Min(level6MaxHeight,MAX_CODED_FRAME_AREA/4096))
      }
      // Repeatedly pick heights until we find one which lets us hit the max area or above
      while_count = 0
      while (1) {
        enc_stream_height = Choose(minHeight, maxHeight)
        enc_stream_width = Min(level6MaxWidth, MAX_CODED_FRAME_AREA / enc_stream_height)
        if (enc_stream_width <= MAX_FRAME_WIDTH && enc_stream_width * enc_stream_height >= Min(level6MaxArea, MAX_CODED_FRAME_AREA)) {
          break
        }
        while_count++
        ASSERT(while_count < 100,
               "Failed to find valid resolution with Height [%d - %d] and Width %d, failed to meet contraint area >= %d",
               minHeight, maxHeight, level6MaxWidth, Min(level6MaxArea,MAX_CODED_FRAME_AREA))
      }
    }
  }
}

ceil_div (int num, int den) {
    return (num + (den - 1)) / den
}

// Work through the various configurable constraints and set soft and
// hard minimums and (just hard) maximums accordingly.
//
// The constraints are passed as an array containing:
//
//     0 min_w_soft
//     1 min_w_hard
//     2 max_w
//     3 min_h_soft
//     4 min_h_hard
//     5 max_h
//     6 min_area_soft
//     7 min_area_hard
//     8 max_area
//     9 max_tiles        (LST only)
//    10 max_tile_cols    (LST only)

enc_get_size_constraints (uint32pointer constraints, uint32 sb_size) {
    uint32 sminw
    uint32 hminw
    uint32 maxw
    uint32 sminh
    uint32 hminh
    uint32 maxh
    uint32 smina
    uint32 hmina
    uint32 maxa
    uint32 max_tiles
    uint32 max_tile_cols

    // Initialise limits from level constraints
    sminw = 0
    hminw = 0
    maxw = level_max_pic_size[enc_base_level][1]

    sminh = 0
    hminh = 0
    maxh = level_max_pic_size[enc_base_level][2]

    smina = 0
    hmina = 0
    maxa = level_max_pic_size[enc_base_level][0]

    max_tiles = level_max_tiles[enc_base_level][0]
    max_tile_cols = level_max_tiles[enc_base_level][1]

    // If enc_base_level is positive, add soft constraints to force
    // the stream into the level in question.
    if (enc_base_level > 0) {
        sminw = Max (sminw, level_max_pic_size[enc_base_level - 1][1] + 1)
        sminh = Max (sminh, level_max_pic_size[enc_base_level - 1][2] + 1)
        smina = Max (smina, level_max_pic_size[enc_base_level - 1][0] + 1)
    }

    // Insert enc_frame_min_*_limit
    enc_frame_min_width_limit u(0)
    enc_frame_min_height_limit u(0)

    ASSERT (enc_frame_min_width_limit <= maxw,
            "enc_frame_min_width_limit = %d > level max width of %d.",
            enc_frame_min_width_limit, maxw)

    ASSERT (enc_frame_min_height_limit <= maxh,
            "enc_frame_min_height_limit = %d > level max height of %d.",
            enc_frame_min_height_limit, maxh)

    hminw = Max (hminw, enc_frame_min_width_limit)
    hminh = Max (hminh, enc_frame_min_height_limit)

    // Take the enc_profile_ min and maxes. A value of -1 means not
    // set. The faffing around with "up" variables is just to convince
    // the C compiler not to moan about signed/unsigned comparisons.
    enc_profile_min_width u(0)
    enc_profile_max_width u(0)
    enc_profile_min_height u(0)
    enc_profile_max_height u(0)

    uint32 upminw
    uint32 upmaxw
    uint32 upminh
    uint32 upmaxh

    upminw = (enc_profile_min_width >= 0) ? enc_profile_min_width : 0
    upmaxw = (enc_profile_max_width >= 0) ? enc_profile_max_width : 100000
    upminh = (enc_profile_min_height >= 0) ? enc_profile_min_height : 0
    upmaxh = (enc_profile_max_height >= 0) ? enc_profile_max_height : 100000

    ASSERT (upminw <= upmaxw,
            "enc_profile_min_width (%d) > enc_profile_max_width (%d).",
            enc_profile_min_width, enc_profile_max_width)

    ASSERT (upminw <= maxw,
            "enc_profile_min_width is %d; range for level is [%d, %d].",
            enc_profile_min_width, hminw, maxw)

    ASSERT (upminh <= upmaxh,
            "enc_profile_min_height (%d) > enc_profile_max_height (%d).",
            enc_profile_min_height, enc_profile_max_height)

    ASSERT (upminh <= maxh,
            "enc_profile_min_height is %d; range for level is [%d, %d].",
            enc_profile_min_height, hminh, maxh)

    ASSERT (upminw * upminh <= maxa,
            "enc_profile_* imply size %d * %d = %d > %d = lvl max area.",
            enc_profile_min_width, enc_profile_min_height,
            enc_profile_min_width * enc_profile_min_height,
            maxa)

    hminw = Max (hminw, upminw)
    maxw = Min (maxw, upmaxw)
    hminh = Max (hminh, upminh)
    maxh = Min (maxh, upmaxh)

    // Apply any constraints based on the decoder model.
    if (enc_decoder_model_mode == DECODER_MODEL_DISABLED) {
        // The only factor influencing level in this case is frame
        // size, so we need to ensure that at least one of width,
        // height, or area exceeds the limits of the next lower size
        // group (as long as enc_size_group is positive). Here we
        // always pick to exceed the *area*, as that adapts best to
        // the timing info case
        if (enc_size_group > 0) {
            lowerLevel = enc_level_lookup[enc_size_group - 1][0]
            smina = Max (smina, level_max_pic_size[lowerLevel][0] + 1)
        }
    } else {
        // Maximum area based on the display rate. We needn't check
        // compatibility with hmina because nothing can set that yet.
        maxa = Min (maxa,
                    level_max_luma_rate[enc_base_level][0] / enc_frame_rate)

        // Maximum area based on the decode rate
        maxa = Min (maxa,
                    level_max_luma_rate[enc_base_level][1] / enc_frame_rate)

        // Set a soft minimum area to try to bump things above the
        // level below.
        if (enc_base_level > 0) {
            min_dis_rate = level_max_luma_rate[enc_base_level - 1][0] + 1
            min_dec_rate = level_max_luma_rate[enc_base_level - 1][1] + 1

            smina = Max (smina, ceil_div (min_dis_rate, enc_frame_rate))
            smina = Max (smina, ceil_div (min_dec_rate, enc_frame_rate))
        }
    }

    // max_tiles and max_tile_cols only apply in large scale tile mode
    if (enc_large_scale_tile) {
        enc_profile_max_tiles u(0)
        max_tiles = Min (max_tiles, enc_profile_max_tiles)

        enc_profile_max_tile_cols u(0)
        max_tile_cols = Min (max_tile_cols, enc_profile_max_tile_cols)

        // If we're too wide then we might use up our tile count
        // horizontally, meaning we can't make the frame as high as
        // was requested. We ignore sminh here, but take notice of
        // hminh.
        max_tile_cols = Min (max_tile_cols,
                             max_tiles / (1 << ceil_log2(hminh / sb_size)))
        ASSERT (max_tile_cols > 0,
                "max_tile_cols forced to zero by hminh=%d; max_tiles=%d.",
                hminh, max_tiles)

        // The frame cannot contain more than maxTiles tiles, so the
        // maximum number of tile rows is maxTiles (assuming just one
        // tile column). Also, the frame must not be more than
        // MAX_TILE_ROWS tiles high. In large scale tile mode, tiles
        // must be exactly one superblock high, so these two constraints
        // give a constraint on the frame height.
        maxh = Min (maxh, Min (max_tiles, MAX_TILE_ROWS) * sb_size)
        ASSERT (hminh <= maxh,
                "maxh forced below hminh (%d < %d) by max_tiles of %d.",
                maxh, hminh, max_tiles)

        // The frame must be at least one tile wide and high (because
        // LST frames must be an integer number of tiles in size)
        ASSERT (sb_size <= maxh,
                "maxh is %d, which is less than sb_size (%d).",
                maxh, sb_size)
        ASSERT (sb_size <= maxw,
                "maxw is %d, which is less than sb_size (%d).",
                maxw, sb_size)

        hminh = Max (hminh, sb_size)
        hminw = Max (hminw, sb_size)
    }

    // The maximum area constraint means that a minimum width implies
    // a maximum height and vice versa.
    maxh = Min (maxh, maxa / hminw)
    maxw = Min (maxw, maxa / hminh)

    // Finally adjust the soft minimums to be no less than the hard
    // minimums (this makes some downstream code a little simpler)
    sminw = Max (sminw, hminw)
    sminh = Max (sminh, hminh)
    smina = Max (smina, hmina)

    constraints[0] = sminw
    constraints[1] = hminw
    constraints[2] = maxw
    constraints[3] = sminh
    constraints[4] = hminh
    constraints[5] = maxh
    constraints[6] = smina
    constraints[7] = hmina
    constraints[8] = maxa
    constraints[9] = max_tiles
    constraints[10] = max_tile_cols
}

// Choose a dimension for a non-large-scale-tile stream
enc_choose_stream_dimension (use_max, smin, hmin, max)
{
    // Search in the range [smin0, max0]. If we can't find a stream
    // size in 500 iterations, switch to [hmin0, max0].
    chosen = 1
    found = 0

    if (smin <= max) {
        // If use_max is true, we'll always try to use as large a
        // value as possible. Of course, if it turns out that max
        // doesn't meet the crop tile requirement, we'll need to
        // decrease it a bit. We do this by choosing
        //
        //     max(smin, (max - count))
        //
        // to iterate over the largest values allowed by the range.
        for (count = 0; count < 500 && ! found; count ++) {
            chosen = (use_max ?
                      Max(smin, max - count) :
                      enc_choose_bias_pow2 (smin, max, 0))

            found = can_meet_crop_tile_requirement(chosen)
        }
    }

    if (! found) {
        ASSERT (hmin <= max,
                "Hard constraint range is empty: [%d, %d].", hmin, max)
        for (count = 0; count < 500 && ! found; count ++) {
            chosen = (use_max ?
                      Max(hmin, max - count) :
                      enc_choose_bias_pow2 (hmin, max, 0))
            found = can_meet_crop_tile_requirement(chosen)
        }
    }
    CHECK (found,
           "[STREAM_SIZE] Can't find a valid stream size in range [%d, %d]",
           hmin, max)

    return chosen
}

// Pick the width for a large-scale-tile stream.
enc_choose_lst_w (sb_size, sminw, hminw, maxw, max_tile_cols) {
    int min_log2
    int max_log2
    int found

    bestTileWidthSb = -1
    bestTileCols = -1
    best_width = -1

    found = 0

    // We pick a tile width (in superblocks) and then try to pick a
    // compatible number of tile columns and corresponding frame
    // width.
    //
    // Rather than being clever, we just do a back-tracking search,
    // trying 1000 times before giving up.
    int iter
    iter = 0
    while (iter < 1000 && ! found) {
        tile_width_sb = Choose (1, MAX_TILE_WIDTH / sb_size)
        tile_width = tile_width_sb * sb_size

        hmin_cols = Max (1, hminw / tile_width)
        smin_cols = Max (1, sminw / tile_width)
        max_cols = Min (max_tile_cols, maxw / tile_width)

        // If hmin_cols > max_cols we may as well skip this tile size.
        if (hmin_cols > max_cols) {
            iter++
            continue
        }

        // Otherwise, we want to pick a power of two in the range
        // [smin_cols, max_cols]. If that range doesn't contain any
        // powers of two, we fudge it by picking one in [hmin_cols,
        // max_cols].
        min_log2 = ceil_log2 (smin_cols)
        max_log2 = floor_log2 (max_cols)
        if (min_log2 <= max_log2) {
            found = 1
        } else {
            min_log2 = ceil_log2 (hmin_cols)
        }

        if (min_log2 > max_log2) {
            // There's no power of 2 in the range [hmin_cols,
            // max_cols]. Boo.
            iter++
            continue
        }

        // If found is false, we've fudged the range. Pick max_log2 to
        // be as close to the soft minimum as we can.
        log_tile_cols = found ? Choose (min_log2, max_log2) : max_log2
        width = (1 << log_tile_cols) * tile_width

        if (found || (width > best_width)) {
            bestTileWidthSb = tile_width_sb
            bestTileCols = 1 << log_tile_cols
            best_width = tile_width * bestTileCols
        }
    }

    // If best_width is still -1, we couldn't find any valid size.
    // Spit out an error.
    ASSERT (best_width >= 0,
            "Couldn't find a valid LST width (%d, %d, %d, %d, %d).",
            sb_size, sminw, hminw, maxw, max_tile_cols)

    ASSERT (hminw <= best_width && best_width <= maxw,
            "enc_choose_lst_w logic error. Expected %d < %d < %d", hminw, best_width, maxw)

    enc_tile_width_sb = bestTileWidthSb
    enc_tile_cols = bestTileCols

    return best_width
}

// Pick the height for a large-scale-tile stream.
//
// This is easy: the tiles are fixed at 1 superblock high, so just aim
// for a power of two.
enc_choose_lst_h (use_max, sminh, hminh, maxh) {
    int min_log2
    int max_log2

    min_log2 = ceil_log2 (sminh)
    max_log2 = floor_log2 (maxh)

    if (min_log2 > max_log2) {
      min_log2 = ceil_log2 (hminh)
      use_max = 1
    }

    ASSERT (min_log2 <= max_log2,
            "Hard height constraints, [%d, %d], include no power of 2.",
            hminh, maxh)

    if (use_max) {
        log_chosen = max_log2
    } else {
        log_chosen = Choose (min_log2, max_log2)
    }

    return 1 << log_chosen
}

enc_pick_stream_size() {
  enc_chooseWidthFirst u(0)
  if (enc_huge_picture) {
    enc_pick_stream_size_huge(enc_chooseWidthFirst)
    return 0
  }

  if (enc_is_license_resolution || enc_force_bug_libaom_2191) {
    // Use the exact size specified
    enc_stream_width = enc_profile_max_width
    enc_stream_height = enc_profile_max_height
    return 0
  }

  // Load per-profile data
  if (enc_large_scale_tile) {
    enc_encourage_not_hasCols = 0
    enc_encourage_not_hasRows = 0
  } else {
    enc_encourage_not_hasCols u(0)
    enc_encourage_not_hasRows u(0)
  }

  enc_use_128x128_superblock u(0)
  res = enc_use_128x128_superblock ? 128 : 64

  uint32 constraints[11]
  enc_get_size_constraints (constraints, res)

  uint32 sminw
  uint32 hminw
  uint32 maxw
  uint32 sminh
  uint32 hminh
  uint32 maxh
  uint32 smina
  uint32 hmina
  uint32 maxa
  uint32 max_tiles
  uint32 max_tile_cols

  sminw = constraints[0]
  hminw = constraints[1]
  maxw  = constraints[2]
  sminh = constraints[3]
  hminh = constraints[4]
  maxh  = constraints[5]
  smina = constraints[6]
  hmina = constraints[7]
  maxa  = constraints[8]
  max_tiles = constraints[9]
  max_tile_cols = constraints[10]

  if (!(enc_client_a_custom_set ||
        enc_encourage_not_hasCols || enc_encourage_not_hasRows)) {

    // This is the "normal path"

    chosen0 = -1
    chosen1 = -1

    uint1 useMax[2]
    useMax[0] = (Choose(0, 99) < 25)
    useMax[1] = (Choose(0, 99) < 25)

    ia = enc_chooseWidthFirst ? 0 : 1
    ib = 1 - ia

    smin0 = enc_chooseWidthFirst ? sminw : sminh
    hmin0 = enc_chooseWidthFirst ? hminw : hminh
    max0 = enc_chooseWidthFirst ? maxw : maxh

    if (enc_large_scale_tile) {
      chosen0 = (enc_chooseWidthFirst ?
                 enc_choose_lst_w (res, sminw, hminw, maxw, max_tile_cols) :
                 enc_choose_lst_h (useMax[ia], sminh, hminh, maxh))
    } else {
      chosen0 = enc_choose_stream_dimension (useMax[ia], smin0, hmin0, max0)
    }

    smin1 = enc_chooseWidthFirst ? sminh : sminw
    hmin1 = enc_chooseWidthFirst ? hminh : hminw
    max1 = enc_chooseWidthFirst ? maxh : maxw

    // Now we've chosen one dimension, we choose the other. We will
    // harden up some of the constraints by interpreting the area
    // constraints with the fixed dimension.
    max1 = Min (max1, maxa / chosen0)
    smin1 = Max (smin1, smina / chosen0)
    hmin1 = Max (hmin1, hmina / chosen0)

    if (enc_large_scale_tile) {
      if (enc_chooseWidthFirst) {
        // If we picked the width first, the maximum height might need
        // updating. We know that there must be at most (maxTiles /
        // enc_tile_cols) tile rows. We know that each tile is one
        // superblock high, which gives a constraint on the height.
        max1 = Min (max1, res * (max_tiles / enc_tile_cols))
      } else {
        // If we picked the height first, we need to update
        // max_tile_cols. This essentially converts the area
        // constraint to a width constraint.
        max_tile_cols = Min (max_tile_cols, max_tiles / (chosen0 / res))
      }

      chosen1 = (enc_chooseWidthFirst ?
                 enc_choose_lst_h (useMax[ib], smin1, hmin1, max1) :
                 enc_choose_lst_w (res, smin1, hmin1, max1, max_tile_cols))

    } else {
      chosen1 = enc_choose_stream_dimension (useMax[ib], smin1, hmin1, max1)
    }

    if (enc_chooseWidthFirst) {
      enc_stream_width = chosen0
      enc_stream_height = chosen1
    } else {
      enc_stream_width = chosen1
      enc_stream_height = chosen0
    }

  } else {

    // ClientA set, hasCols or hasRows

    // The code below doesn't understand soft and hard minimums, so
    // let's combine them. Clamp the soft minimum so that it's not
    // larger than the maximum and then take the maximum of that and
    // the hard minimum.
    uint32 minw
    uint32 minh

    minw = Max (hminw, Min (sminw, maxw))
    minh = Max (hminh, Min (sminh, maxh))

    ASSERT (minw <= maxw,
            "Inconsistent hard width constraints: [%d, %d].", minw, maxw)
    ASSERT (minh <= maxh,
            "Inconsistent hard height constraints: [%d, %d].", minh, maxh)

    // firstly check for the special cases
    enc_encourage_upscale u(0)
    if (enc_encourage_not_hasCols) { // specific width, any height
      numSuperBlocks = (minw + res - 1) / res
      enc_stream_width = res * numSuperBlocks + 8

      ASSERT(enc_stream_width >= minw,
             "enc_encourage_not_hasCols - Must have width (%d) >= minw (%d)",
             enc_stream_width, minw)
      ASSERT(enc_stream_width <= maxw,
             "enc_encourage_not_hasCols - Must have width (%d) <= maxw (%d)",
             enc_stream_width, maxw)

      // Apply max area to the second dimension that we're choosing.
      maxh = Min(maxh, maxa/enc_stream_width)

      enc_stream_height = enc_choose_frame_dim(minh, maxh, 1)
      attempts = 0
      while (!can_meet_crop_tile_requirement(enc_stream_height)) {
        attempts+=1
        ASSERT(attempts < 1000,"Unable to find a framesize that fits crop tile requirement" )
        enc_stream_height = enc_choose_frame_dim(minh, maxh, 1)
      }

    } else if (enc_encourage_not_hasRows || enc_encourage_upscale) {
      numSuperBlocks = (minh + res - 1) / res
      enc_stream_height = res * numSuperBlocks + 8

      ASSERT(enc_stream_height >= minh,
             "enc_encourage_not_hasRows - Must have height (%d) >= minh (%d)",
             enc_stream_height, minh)
      ASSERT(enc_stream_height <= maxh,
             "enc_encourage_not_hasRows - Must have height (%d) <= maxh (%d)",
             enc_stream_height, maxh)

      // Apply max area to the second dimension that we're choosing.
      maxw = Min(maxw, maxa/enc_stream_height)

      enc_stream_width = enc_choose_frame_dim(minw, maxw, 0)
      attempts = 0
      while (!can_meet_crop_tile_requirement(enc_stream_width)) {
        attempts+=1
        ASSERT(attempts < 1000,"Unable to find a framesize that fits crop tile requirement" )
        enc_stream_width = enc_choose_frame_dim(minw, maxw, 1)
      }

    } else {
      // now go select and choose from valid indices
      int valIdcs[NUM_CLIENT_A_RESOLUTIONS]
      valDimsCount = 0
      for (i=0; i<NUM_CLIENT_A_RESOLUTIONS; i++) {
        if (minw <= client_a_resolution_bias[i][0] &&
            maxw >= client_a_resolution_bias[i][0] &&
            minh <= client_a_resolution_bias[i][1] &&
            maxh >= client_a_resolution_bias[i][1] &&
            (client_a_resolution_bias[i][0] * client_a_resolution_bias[i][1]) <= maxa) {
          valIdcs[valDimsCount] = i
          valDimsCount += 1
        }
      }

      if (! valDimsCount) {
        ASSERT(0, "CLIENT_A - normal - What should we do now? [%d %d] < [%d %d] [%d]",
               minw, minh, maxw, maxh, maxa)
      }

      idx = valIdcs[Choose(0,valDimsCount-1)]
      enc_stream_width = client_a_resolution_bias[idx][0]
      enc_stream_height = client_a_resolution_bias[idx][1]
    }
  }

  if (enc_client_a_custom_set) {
    ASSERT(enc_stream_width >= 144, "Client specific size constraint, Width (%d) too small",enc_stream_width)
    ASSERT(enc_stream_height >= 128, "Client specific size constraint, Height (%d) too small",enc_stream_height)
    ASSERT(enc_stream_width <= 3840, "Client specific size constraint, Width (%d) too large",enc_stream_width)
    ASSERT(enc_stream_height <= 2160, "Client specific size constraint, Height (%d) too large",enc_stream_height)
  }

  // Handle large scale tile
  if (enc_large_scale_tile) {
    ASSERT (enc_stream_height % res == 0,
            "Stream height (%d) didn't divide into superblock size (%d).",
            enc_stream_height, res)

    // Tile height is always 1 superblock
    enc_tile_height_sb = 1
    enc_tile_rows = enc_stream_height / res

    ASSERT(enc_tile_rows <= MAX_TILE_ROWS,
           "Tile rows (%d) more than MAX_TILE_ROWS (%d)",
           enc_tile_rows, MAX_TILE_ROWS)

    return 0
  }

  // Now pick enc_hide_frame_prob
  if (enc_decoder_model_mode != DECODER_MODEL_DISABLED) {

    int max_decode_rate
    max_decode_rate = level_max_luma_rate[enc_base_level][1]

    uint32 px_per_second
    px_per_second = enc_stream_width*enc_stream_height*enc_frame_rate

    enc_hide_frame_prob = Min(enc_hide_frame_prob,
                              (100 * (max_decode_rate - px_per_second) - 99)/(max_decode_rate - 1))
  }
}

encode_setup_superres() {
  // Disable superres by default.
  superres_denom = SUPERRES_NUM

  uint32 narrow_width

  // Is superres_denom specified explicitly?
  enc_superres_denom u(0)
  if (enc_superres_denom >= 0) {
    CHECK (SUPERRES_DENOM_MIN <= enc_superres_denom &&
           enc_superres_denom <= SUPERRES_DENOM_MAX,
           "enc_superres_denom is %d, not negative or in [%d, %d].",
           enc_superres_denom, SUPERRES_DENOM_MIN, SUPERRES_DENOM_MAX)

    // If we're doing superres, the frame gets scaled and there are
    // some width requirements that we must satisfy. Firstly, the
    // scaled frame width must met the tile crop requirement (no tile
    // width may be less than 8; we guarantee this by checking the
    // image width modulo the superblock size isn't between 1 and 7
    // pixels).
    //
    // Secondly, if there is more than one column of tiles then all but
    // the rightmost column must have a tile width of at least 128 (see
    // A.3 in the spec). This second requirement will be dealt with when
    // we're deciding on our tile layout later (see the
    // read_tile_info_max_tile function): we just won't pick multiple
    // tile columns if it wouldn't hold.
    narrow_width = ((upscaled_width * SUPERRES_NUM + (enc_superres_denom / 2)) /
                    enc_superres_denom)
    CHECK (can_meet_crop_tile_requirement (narrow_width),
           "enc_superres_denom of %d implies a frame width of %d (%d modulo SB size). Instead upscaled_width is %d",
           enc_superres_denom, narrow_width,
           narrow_width % (enc_use_128x128_superblock ? 128 : 64), upscaled_width)

    // We must also check that the encoded width is at least
    // enc_frame_min_width.
    CHECK (enc_frame_min_width <= narrow_width,
           "[SUPERRES] enc_superres_denom of %d implies a frame width of %d, but enc_frame_min_width is %d.",
           enc_superres_denom, narrow_width, enc_frame_min_width)

    superres_denom = enc_superres_denom
    return 0
  }

  // We need to maintain the constraint that 'crop_width' is at least
  // enc_frame_min_width. We use a slightly stricter requirement, for
  // simplicity
  maxDenom = Min((upscaled_width * SUPERRES_NUM) / enc_frame_min_width, SUPERRES_DENOM_MAX)
  ASSERT(maxDenom >= SUPERRES_NUM, "No valid superres scale exists")
  if (maxDenom == SUPERRES_NUM || (Choose(0, 99) >= enc_superres_prob)) {
    return 0
  }

  // Try to meet the crop tile requirement
  for (i = 0; i < 1000; i++) {
    // Clamping to maxDenom guarantees that the encoded width will be
    // at least enc_frame_min_width. We also need to check that it
    // won't be a difficult size modulo the SB size.
    int denom
    denom = Choose(SUPERRES_DENOM_MIN, maxDenom)

    narrow_width = (upscaled_width * SUPERRES_NUM + (denom / 2)) / denom
    if (can_meet_crop_tile_requirement (narrow_width)) {
      superres_denom = denom
      break
    }
  }
}

enc_pick_frame_size (uint1 choose_width, uint1 choose_height) {
  uint32 refW
  uint32 refH
  if (enc_force_bug_libaom_2191) {
    ASSERT (choose_width && choose_height,
            "Not implemented: overriding frame size and enc_force_bug_libaom_2191")

    if (enc_frame_type == INTER_FRAME || enc_frame_type == S_FRAME) {
      enc_frame_width = enc_profile_min_width
      enc_frame_height = enc_profile_min_height
    } else {
      enc_frame_width = enc_profile_max_width
      enc_frame_height = enc_profile_max_height
    }
    enc_frame_min_width = Max(enc_frame_min_width_limit, enc_frame_width)
    enc_frame_max_width = enc_frame_width
    enc_frame_min_height = Max(enc_frame_min_height_limit, enc_frame_height)
    enc_frame_max_height = enc_frame_height
    return 0
  }

  // Default to not copying the frame size from any ref frame
  enc_copy_frame_from_ref = -1

  // We initialize the frame size bounds based on the sequence-level constraints:
  // * All frames must be at least 16x16
  // * All frames must be within the bounds determined by the scalability information
  //   (if present) or the sequence header (if no scalability info is present).
  //
  // Additionally, to ensure that every inter frame will always be able to pick *some* size,
  // we make all frames at least 1/32 of the largest size, rounded up.
  // This is because a frame of width (max width/16) can reference any frame of
  // width (max_width/32) to (max_width), and similar for height.
  // This ensures that (max_width/16) x (max_height/16) will *always*
  // be a valid size for an inter frame.
  enc_frame_min_width = Max(enc_frame_min_width_limit, (enc_stream_width+31)/32)
  enc_frame_min_height = Max(enc_frame_min_height_limit, (enc_stream_height+31)/32)

  enc_frame_max_width = spatial_layer_max_width[spatial_layer]
  enc_frame_max_height = spatial_layer_max_height[spatial_layer]

  ASSERT(enc_frame_min_width <= enc_frame_max_width, "Max width for spatial layer %d is less than minimum width %d",enc_frame_max_width,enc_frame_min_width)
  ASSERT(enc_frame_min_height <= enc_frame_max_height, "Max height for spatial layer %d is less than minimum height %d",enc_frame_max_height,enc_frame_min_height)

  if (! choose_width) {
      ASSERT (enc_frame_width >= enc_frame_min_width,
              "enc_frame_width is %d but must be >= %d.",
              enc_frame_width, enc_frame_min_width)
      ASSERT (enc_frame_width <= enc_frame_max_width,
              "enc_frame_width is %d but must be <= %d.",
              enc_frame_width, enc_frame_max_width)
  }

  if (! choose_height) {
      ASSERT (enc_frame_height >= enc_frame_min_height,
              "enc_frame_height is %d but must be >= %d.",
              enc_frame_height, enc_frame_min_height)
      ASSERT (enc_frame_height <= enc_frame_max_height,
              "enc_frame_height is %d but must be <= %d.",
              enc_frame_height, enc_frame_max_height)
  }

  if (enc_frame_type == INTER_FRAME || enc_frame_type == S_FRAME) {
    // Apply constraints resulting from the size of each reference frame
    for (i = 0; i < INTER_REFS_PER_FRAME; i++) {
      refSlot = sched_active_ref_idx[frame_number][i]
      refW = ref_width[refSlot]
      refH = ref_height[refSlot]

      enc_frame_min_width = Max(enc_frame_min_width,(refW+1)/2)
      if (! choose_width) {
          ASSERT (enc_frame_width >= enc_frame_min_width,
                  "enc_frame_width is %d but must be >= %d. It is a requirement of bit-stream conformance that 2 * FrameWidth >= RefUpscaledWidth[ ref_frame_idx[ i ] ]. In this case, RefUpscaledWidth[ ref_frame_idx[ %d ] ] = %d.",
                  enc_frame_width, enc_frame_min_width, i, refW)
      }

      enc_frame_min_height = Max(enc_frame_min_height,(refH+1)/2)
      if (! choose_height) {
          ASSERT (enc_frame_height >= enc_frame_min_height,
                  "enc_frame_height is %d but must be >= %d. It is a requirement of bit-stream conformance that 2 * FrameHeight >= RefFrameHeight[ ref_frame_idx[ i ] ]. In this case, RefFrameHeight[ ref_frame_idx[ %d ] ] = %d.",
                  enc_frame_height, enc_frame_min_height, i, refH)
      }

      enc_frame_max_width = Min(enc_frame_max_width,refW*16)
      if (! choose_width) {
          ASSERT (enc_frame_width <= enc_frame_max_width,
                  "enc_frame_width is %d but must be <= %d. It is a requirement of bit-stream conformance that FrameWidth <= 16 * RefUpscaledWidth[ ref_frame_idx[ i ] ]. In this case, RefUpscaledWidth[ ref_frame_idx[ %d ] ] = %d.",
                  enc_frame_width, enc_frame_max_width, i, refW)
      }

      enc_frame_max_height = Min(enc_frame_max_height,refH*16)
      if (! choose_height) {
          ASSERT (enc_frame_height <= enc_frame_max_height,
                  "enc_frame_height is %d but must be <= %d. It is a requirement of bit-stream conformance that FrameHeight >= 16 * RefFrameHeight[ ref_frame_idx[ i ] ]. In this case, RefFrameHeight[ ref_frame_idx[ %d ] ] = %d.",
                  enc_frame_height, enc_frame_max_height, i, refH)
      }
    }

    // In addition, we might be able to copy the size of some reference
    // frame
    numAllowedRefs = 0
    for (i = 0; i < INTER_REFS_PER_FRAME; i++) {
      refSlot = sched_active_ref_idx[frame_number][i]
      refW = ref_width[refSlot]
      refH = ref_height[refSlot]
      if (enc_frame_min_width <= refW && refW <= enc_frame_max_width &&
          enc_frame_min_height <= refH && refH <= enc_frame_max_height) {
        enc_compatible_refs[numAllowedRefs] = i
        numAllowedRefs += 1
      }
    }

    if (choose_width && choose_height && numAllowedRefs > 0) {
      enc_ref_frame_size_ref u(0)

      if (enc_ref_frame_size_ref < -1) {
        // This is the default case where we decide what to do. Choose
        // whether to pick an index and, if so, pick one at random.
        enc_ref_frames_same_res u(0)
        if (enc_ref_frames_same_res) {
          enc_copy_frame_from_ref = enc_compatible_refs[Choose(0, numAllowedRefs - 1)]
        }
      } else if (enc_ref_frame_size_ref >= 0) {
        // We've decided to use a reference frame. Check that it is
        // valid by looking it up in enc_compatible_refs.
        uint1 is_valid
        is_valid = 0
        for (i = 0; i < numAllowedRefs; i++) {
          if (enc_compatible_refs[i] == enc_ref_frame_size_ref) {
            is_valid = 1
            break
          }
        }

        ASSERT (is_valid,
                "enc_ref_frame_size_ref is %d, but that slot does not have a compatible frame size.",
                enc_ref_frame_size_ref)

        enc_copy_frame_from_ref = enc_ref_frame_size_ref
      }

      if (enc_copy_frame_from_ref >= 0) {
        refSlot = sched_active_ref_idx[frame_number][enc_copy_frame_from_ref]
        enc_frame_width = ref_width[refSlot]
        enc_frame_height = ref_height[refSlot]
        return 0
      }
    }
  }

  ASSERT(enc_frame_min_width <= enc_frame_max_width, "Can't find a valid frame width")
  ASSERT(enc_frame_min_height <= enc_frame_max_height, "Can't find a valid frame height")

  if (enc_allow_scaling) {
    if (Choose(0, 9) == 0) {
      // 10% chance of using the maximum size possible
      // This is to make sure we generate streams with frame_size_override_flag == 0
      // sometimes, even in scaling mode
      if (choose_width) {
        enc_frame_width = enc_frame_max_width
      }
      if (choose_height) {
        enc_frame_height = enc_frame_max_height
      }
    } else {
      if (choose_width) {
        enc_frame_width = 1
        while (!can_meet_crop_tile_requirement(enc_frame_width)) {
          enc_frame_width = enc_choose_frame_dim(enc_frame_min_width, enc_frame_max_width, 0)
        }
      }
      if (choose_height) {
        enc_frame_height = 1
        while (!can_meet_crop_tile_requirement(enc_frame_height)) {
          enc_frame_height = enc_choose_frame_dim(enc_frame_min_height, enc_frame_max_height, 1)
        }
      }
    }
  } else {
    // Note: The above code is still useful as enc_frame_min_width helps determine
    // what superres scales we can use. But if enc_allow_scaling == 0, we force
    // the upscaled width to always be the same as the stream size
    ASSERT(enc_frame_max_width == enc_stream_width, "Forced to resize a frame when enc_allow_scaling == 0")
    ASSERT(enc_frame_max_height == enc_stream_height, "Forced to resize a frame when enc_allow_scaling == 0")

    // If scaling is disabled, enc_frame_min_width and
    // enc_frame_max_width will be equal and similarly for height. So
    // we don't have to worry about choose_* flags: if the user tried
    // to override the frame size to something different, they will
    // have failed the asserts above.

    enc_frame_width = enc_stream_width
    enc_frame_height = enc_stream_height
  }
}

enc_pick_base_qindex() {
  if (enc_base_qindex >= 0) {
    return enc_base_qindex
  }

  if (enc_special_lossless) {
    // Pick a nonzero value, but such that the segment quantizers can be set
    // to 0 using SEG_LVL_ALT_Q
    return Choose(1, seg_feature_data_max[SEG_LVL_ALT_Q])
  } else if (enc_lossless) {
    return 0
  } else {
    // Pick a nonzero value of length QINDEX_BITS (map to 1 if we
    // happen to pick zero).
    raw_val = ChooseBits(QINDEX_BITS)
    return raw_val ? raw_val : 1
  }
}

enc_update_segdata() {
  if (enc_lossless) {
    // We can reuse the previous frame's feature data iff it causes all
    // segment quantizers to equal 0.
    canReuse = 1
    for (segId = 0; segId < MAX_SEGMENTS; segId++) {
      if (prev_feature_mask[segId] & (1 << SEG_LVL_ALT_Q)) {
        segQuantizer = base_qindex + prev_feature_data[segId][SEG_LVL_ALT_Q]
      } else {
        segQuantizer = base_qindex
      }
      canReuse = canReuse && (segQuantizer <= 0)
    }

    if (! canReuse) {
      // In this case, we *must* signal new feature data
      return 1
    }
  }

  enc_seg_update_data u(0)
  return enc_seg_update_data
}

enc_pick_feature(segmentId, feature, encNumSegments) {
  if (enc_special_lossless && feature == SEG_LVL_ALT_Q) {
    return 1
  }
  enc_no_seg_lf u(0)
  if (enc_no_seg_lf && feature >= SEG_LVL_ALT_LF_Y_V && feature <= SEG_LVL_ALT_LF_V) {
      return 0
  }
  if (segmentId >= encNumSegments) {
    return 0
  }
  // Ensure that we have at least one segment with none of
  // {SEG_LVL_REF_FRAME, SEG_LVL_SKIP, SEG_LVL_GLOBALMV} set.
  // This is because, if all segments have at least one of those features,
  // it heavily restricts what inter blocks can do - eg, all inter blocks are forced
  // to be single ref, and we may be restricted on which refs we can use and whether
  // blocks need to be skipped or not.
  // This causes a lot of problems for the frame planner, so avoid that case by
  // forcing segment 0 to not have any of those features set.
  enc_no_seg_mode u(0)
  if ((enc_no_seg_mode || segmentId == 0) && feature >= SEG_LVL_REF_FRAME) {
    return 0
  }
  enc_force_seg_lvl_globalmv u(0)
  if (enc_force_seg_lvl_globalmv && feature == SEG_LVL_GLOBALMV) {
    return 1
  }
  enc_enable_feature u(0)
  return enc_enable_feature
}

enc_pick_feature_data(feature, numbits, isSigned) {
  minValue = isSigned ? -(1<<numbits) : 0
  maxValue = (1<<numbits) - 1

  if (feature == SEG_LVL_ALT_Q && enc_lossless) {
    return Choose(minValue, -base_qindex) // Force segment quantizer to 0
  }

  if ((enc_large_scale_tile || enc_force_seg_lvl_ref_frame_last_frame) && feature == SEG_LVL_REF_FRAME) {
    return LAST_FRAME
  }

  return Choose(minValue, maxValue)
}

enc_coded_id(mi_row, mi_col, pred) {
  if (enc_use_frame_planner && preskip_seg_id) {
    // We've alrady picked the segment ID during enc_plan_tile(),
    // so need to code a specific value
    return neg_interleave(enc_segment_id[mi_col][mi_row], pred, last_active_seg_id + 1)
  } else {
    // Any segment ID is valid for this block, so just randomly select one
    return Choose(0, last_active_seg_id)
  }
}

enc_pick_ref_frames(bsize, mi_row, mi_col) {
  // Handle the cases where we're forced to use particular ref frames
  if (skip_mode) {
    enc_ref_frame[mi_col][mi_row][0] = skip_ref[0]
    enc_ref_frame[mi_col][mi_row][1] = skip_ref[1]
  } else if (segfeature_active(SEG_LVL_REF_FRAME)) {
    enc_ref_frame[mi_col][mi_row][0] = get_segdata(SEG_LVL_REF_FRAME)
    enc_ref_frame[mi_col][mi_row][1] = NONE
  } else if (segfeature_active(SEG_LVL_SKIP) ||
             segfeature_active(SEG_LVL_GLOBALMV)) {
    enc_ref_frame[mi_col][mi_row][0] = LAST_FRAME
    enc_ref_frame[mi_col][mi_row][1] = NONE
  } else {
    // In this case, we can use any signal-able ref frame combination.
    isCompound = 0
    if (is_compound_allowed (bsize)) {
      ASSERT(!large_scale_tile, "Not allowed compound in large_scale_tile frames")
      enc_is_compound u(0)
      isCompound = enc_is_compound
    }

    if (isCompound) {
      enc_compound_ref_0 u(0)
      CHECK (enc_compound_ref_0 < 0 ||
             (enc_compound_ref_0 == LAST_FRAME) ||
             (enc_compound_ref_0 == LAST2_FRAME) ||
             (enc_compound_ref_0 == LAST3_FRAME) ||
             (enc_compound_ref_0 == GOLDEN_FRAME) ||
             (enc_compound_ref_0 == BWDREF_FRAME),
             "enc_compound_ref_0 is %d, not a valid first index for a compound reference.",
             enc_compound_ref_0)

      enc_compound_ref_1 u(0)
      CHECK (enc_compound_ref_1 < 0 ||
             (LAST2_FRAME <= enc_compound_ref_1 && enc_compound_ref_1 <= ALTREF_FRAME),
             "enc_compound_ref_1 is %d, not a valid first index for a compound reference.",
             enc_compound_ref_1)

      refPair = -1

      if (enc_compound_ref_0 < 0) {
        if (enc_compound_ref_1 < 0) {
          // Neither reference was specified. Pick a valid ref pair
          refPair = Choose (0, SIGNALLED_COMP_REFS - 1)
        } else {
          // Just ref1 was specified. Pick ref0.
          if (enc_compound_ref_1 <= GOLDEN_FRAME) {
            enc_compound_ref_0 = LAST_FRAME
          } else if (enc_compound_ref_1 <= ALTREF2_FRAME) {
            enc_compound_ref_0 = Choose (LAST_FRAME, GOLDEN_FRAME)
          } else if (enc_compound_ref_1 == ALTREF_FRAME) {
            enc_compound_ref_0 = Choose (LAST_FRAME, BWDREF_FRAME)
          }
        }
      } else {
        if (enc_compound_ref_1 < 0) {
          // Only ref0 was specified. Pick ref1.
          if (enc_compound_ref_0 == LAST_FRAME) {
            enc_compound_ref_1 = Choose (LAST2_FRAME, ALTREF_FRAME)
          } else if (enc_compound_ref_0 == BWDREF_FRAME) {
            enc_compound_ref_1 = ALTREF_FRAME
          } else {
            enc_compound_ref_1 = Choose (BWDREF_FRAME, ALTREF_FRAME)
          }
        } else {
          // Both refs were specified.
          refPair = av1_ref_frame_type (enc_compound_ref_0, enc_compound_ref_1)
          CHECK (MAX_REF_FRAMES <= refPair &&
                 refPair <= MAX_REF_FRAMES + SIGNALLED_COMP_REFS,
                 "The compound ref with slots %d, %d can't be signalled.",
                 enc_compound_ref_0, enc_compound_ref_1)
          refPair -= MAX_REF_FRAMES
        }
      }

      if (refPair < 0) {
        // Exactly one of the references was specified and we picked the other
        // one. Translate it back into a refPair and assert that we didn't pick
        // a dud.
        refPair = av1_ref_frame_type (enc_compound_ref_0, enc_compound_ref_1)
        ASSERT (MAX_REF_FRAMES <= refPair &&
                refPair <= MAX_REF_FRAMES + SIGNALLED_COMP_REFS,
                "Bad compound ref: %d, %d.",
                enc_compound_ref_0, enc_compound_ref_1)
        refPair -= MAX_REF_FRAMES
      }

      av1_set_ref_frame(MAX_REF_FRAMES + refPair)
      enc_ref_frame[mi_col][mi_row][0] = rf[0]
      enc_ref_frame[mi_col][mi_row][1] = rf[1]
    } else {
      enc_single_ref = LAST_FRAME
      if (! large_scale_tile) {
        enc_single_ref u(0)
      }
      CHECK (LAST_FRAME <= enc_single_ref && enc_single_ref <= ALTREF_FRAME,
             "enc_single_ref is %d, between LAST_FRAME and ALTREF_FRAME.",
             enc_single_ref)

      enc_ref_frame[mi_col][mi_row][0] = enc_single_ref
      enc_ref_frame[mi_col][mi_row][1] = NONE
    }
  }
}

uint1 enc_choose_coef_sign(c, adjTxSize) {
  if (enc_coef_sign_pattern) {
    pos = scan[c]
    row=(pos>>tx_size_wide_log2[adjTxSize])
    col=pos-(row<<tx_size_wide_log2[adjTxSize])
    sign = enc_row_sign[row] * enc_col_sign[col]
    return (sign == -1) ? 1 : 0
  } else {
    return ChooseBit()
  }
}

int20 enc_choose_coef(c, last_coef, adjusted_max_coef, adjTxSize) {
  max_coef = Min(adjusted_max_coef,enc_coef_max)
  ASSERT(max_coef <= QCOEFF_MAX, "Maximum coefficient value out of range")

  if (c>=last_coef)
    return -1

  if (enc_coef_sign_pattern) {
    pos = scan[c]
    row=(pos>>tx_size_wide_log2[adjTxSize])
    col=pos-(row<<tx_size_wide_log2[adjTxSize])
    sign = enc_row_sign[row] * enc_col_sign[col]
    if (sign == 0) {
      // This coefficient is in a row or col which is all zeros
      return 0
    }
  }

  if (Choose(0,99)<enc_zero_coef_prob)
    return 0

  if (max_coef == 0)
    return (enc_zero_coef_prob == 0) ? 1 : 0

  if (enc_use_large_coefs) {
    return Choose(1, max_coef)
  } else {
    return ChoosePowerLaw(max_coef)
  }
}

int20 enc_choose_coef_from_cdf(c, last_coef, adjusted_max_coef, txSz, txSzCtx, plane, blockx, blocky) {
  maxCoef = Min(adjusted_max_coef,enc_coef_max)
  ASSERT(maxCoef <= QCOEFF_MAX, "Maximum coefficient value out of range")

  if (c>=last_coef)
    return -1

  if (maxCoef == 0 || Choose(0,99)<enc_zero_coef_prob)
    return 0

  ptype = (plane > 0)

  ctxIdxOffset = get_nz_map_ctx(txSz, plane, blocky, blockx, scan[c], c, 0)
  level = enc_choose_from_cdf(tile_coeff_base_cdfs[txSzCtx][ptype][ctxIdxOffset],
                              Min(4, maxCoef))
  if (level > NUM_BASE_LEVELS) {
    for (idx = 0; idx < COEFF_BASE_RANGE / (BR_CDF_SIZE - 1); idx++) {
      ctxIdxOffset = get_br_ctx(txSz, plane, blocky, blockx, scan[c])
      coeff_br = enc_choose_from_cdf(tile_coeff_br_cdfs[Min(txSzCtx,TX_32X32)][ptype][ctxIdxOffset],
                                     Min(BR_CDF_SIZE, maxCoef - level))
      level += coeff_br
      if (coeff_br < (BR_CDF_SIZE - 1)) {
        break
      }
    }
  }
  if (level > NUM_BASE_LEVELS + COEFF_BASE_RANGE) {
    // The Golomb code used for large coefficients
    // has a distribution p(x) ~ x^-2, similarly to our ChoosePowerLaw
    // builtin. So we can just use that to get a good distribution.
    coef = ChoosePowerLaw(maxCoef - level) - 1
    level = coef + COEFF_BASE_RANGE + 1 + NUM_BASE_LEVELS
  }
  return level
}

enc_pick_num_coefs(seg_eob) {
  if (Choose(0,99) < enc_empty_block_prob) {
    return 0
  }
  enc_small_blocks u(0)
  z = Choose(0, 199)
  if (!enc_small_blocks && z < 5) {
    // 2.5% chance of selecting near-full blocks
    return seg_eob - ChoosePowerLaw(seg_eob)
  } else if (!enc_small_blocks && z < 25) {
    // 10% chance of aiming for medium-size blocks
    return ChooseAll(0,seg_eob,NUM_COEFFS_UNIFORM)
  } else {
    // Most of the time, bias toward small blocks
    return Choose(1, 4*ChoosePowerLaw(seg_eob/4))
  }
}

// Special case for arithmetic decode stress streams - always
// generate either empty coeff blocks or nearly-full coeff blocks
enc_pick_max_coefs(seg_eob) {
  if (Choose(0,99) < enc_empty_block_prob) {
    return 0
  }
  return seg_eob
}

enc_pick_tx_depth(bsize, maxTxDepth) {
  if (enc_target_tx_size == -1) {
    return Choose(0,Min(maxTxDepth + 1, MAX_TX_DEPTH))
  }

  max_depths = Min(maxTxDepth + 1, MAX_TX_DEPTH)

  // We only reach this point if we're doing the special
  // "inverse transform performance" stress streams. In this case,
  // we have a desired size that we're trying to make all transforms,
  // and we need to work out what depth to signal to reach that size.
  targetW = tx_size_wide[enc_target_tx_size]
  targetH = tx_size_high[enc_target_tx_size]

  txSz = max_txsize_rect_lookup[bsize]
  for (depth = 0; depth <= max_depths; depth++) {
    txw = tx_size_wide[txSz]
    txh = tx_size_high[txSz]
    if (txw <= targetW && txh <= targetH) {
      // We've either reached the desired size,
      // or we can't hit that size exactly (eg, if the current
      // block is too small) and we're now at a smaller size.
      // So either way, this is the depth we want to return
      return depth
    }
    txSz = sub_tx_size_map[is_inter][txSz]
  }
}

enc_pick_txfm_split(txSz) {
  if (enc_target_tx_size == -1) {
    return ChooseBit()
  }

  // Similarly to above, we're trying to reach a particular target
  // transform size. In this case the decision is slightly easier,
  // since we only have to decide whether to split at this level or not.
  targetW = tx_size_wide[enc_target_tx_size]
  targetH = tx_size_high[enc_target_tx_size]
  txw = tx_size_wide[txSz]
  txh = tx_size_high[txSz]

  if (txw <= targetW && txh <= targetH) {
    // We've either reached the desired size, or are too small,
    // so don't split
    return 0
  } else {
    return 1
  }
}

// Helper function for CDEF pattern selection:
// What is the maximum value constrain() can return?
enc_constrainMaxPri() {
  if (bit_depth == 8) {
    return 14
  } else if (bit_depth == 10) {
    return 54
  } else {
    return 214
  }
}

enc_constrainMaxSec() {
  if (bit_depth == 8) {
    return 4
  } else if (bit_depth == 10) {
    return 15
  } else {
    return 61
  }
}

enc_lrMinimizingValue() {
  if (bit_depth == 8) {
    return 31
  } else if (bit_depth == 10) {
    return 126
  } else if (bit_depth == 12) {
    return 511
  }
}

enc_lrMaximizingValue() {
  if (bit_depth == 8) {
    return 224
  } else if (bit_depth == 10) {
    return 890
  } else if (bit_depth == 12) {
    return 3584
  }
}

// The available functions for combining row and column signs
// when generating a pixel pattern. The function can be "not"-ed
// by setting enc_pixel_pattern_sign = 1.
#define PIXEL_FN_AND 0
#define PIXEL_FN_OR 1
#define PIXEL_FN_XOR 2

// Helper - most of the patterns we want can be built using
// a sign for each row and column and a combination function.
// This function translates this data into a specification of
// which pixels go where within the palette block.
enc_build_binary_palette_pattern(ptype, bw, bh) {
  for (i = 0; i < bh; i++) {
    for (j = 0; j < bw; j++) {
      // Work out which palette index we want this pixel to use
      sign = 0
      if (enc_pixel_pattern_fn == PIXEL_FN_AND) {
        sign = enc_palette_row_sign[i] & enc_palette_col_sign[j]
      } else if (enc_pixel_pattern_fn == PIXEL_FN_OR) {
        sign = enc_palette_row_sign[i] | enc_palette_col_sign[j]
      } else if (enc_pixel_pattern_fn == PIXEL_FN_XOR) {
        sign = enc_palette_row_sign[i] ^ enc_palette_col_sign[j]
      } else {
        ASSERT(0, "Invalid pixel pattern function")
      }

      // Invert the function if desired
      sign = sign ^ enc_pixel_pattern_sign

      enc_palette_entries[ptype][j][i] = sign ? 1 : 0
    }
  }
}

uint1 enc_pick_palette_pattern(ptype) {
  bsize = get_plane_block_size(sb_size, ptype)
  bw = block_size_wide_lookup[bsize]
  bh = block_size_high_lookup[bsize]

  if (bw < 8 || bh < 8) {
    // All of our patterns are at least 8x8, so smaller blocks are
    // not useful
    return 0
  }
  if (ptype != 0) {
    // We currently have no useful patterns for the UV planes
    // Note: However, all the support should be in place for
    // if we want to add UV plane patterns in the future
    return 0
  }

  pMax = (1 << bit_depth) - 1

  // For most patterns, we only need two values in the palette - 0
  // and the maximum pixel value. Individual patterns may override this
  // if they wish.
  enc_palette_len[ptype] = 2
  for (i = 0; i < palette_cache_len[ptype]; i++) {
    enc_use_palette_cache[ptype][i] = 0
  }
  enc_pattern_colours[ptype][0] = 0
  enc_pattern_colours[ptype][1] = pMax

  enc_pixel_pattern_prob u(0)

  // For all patterns, we want to ensure that the selected pixels are not adjusted
  // before they're useful. This means disabling transforms and optionally loop filtering:
  // * For the CDEF filter pattern, the block *must not* be skipped, because CDEF
  //   does not filter skip blocks. So we need to instead rely on no transform coefficients
  //   being generated.
  // * For the CfL pattern, we don't have to worry about loop filtering, but we do want
  //   to avoid applying a transform so that the pattern is kept for CfL prediction.
  //   Thus we only pick this pattern for skip blocks.
  // * For the other patterns, they need to make it into the frame buffer itself.
  //   CDEF and (to some extent) deblocking ignore skip blocks, so we only generate
  //   these patterns for skip blocks.
  //   But note that the patterns are useful for hitting the extreme values in all of
  //   loop-restoration, superres, and inter prediction. Thus we don't worry about whether
  //   loop-restoration and/or superres are enabled.
  //
  // Thus our patterns are split into two groups: Most are only useful for skip blocks,
  // while the CDEF pattern is only useful for non-skip blocks.

  if (skip_coeff) {
    // Try the CfL pattern first. This only makes sense in a specific set of circumstances.
    if (ptype == 0 && bsize == BLOCK_32X32 && uv_mode == UV_CFL_PRED) {
      if (Choose(0, 99) < enc_pixel_pattern_prob) {
        // For CfL, the most extreme value of 'scaled_luma_ac' occurs when a single
        // "CfL sample" (which may be 1x1 up to 2x2 luma pixels depending on subsampling)
        // has the value maxPx and the others have value 0 (or vice versa).
        // We create this situation by labelling only a single row (or pair of rows)
        // and a single col (or pair of cols) as having sign +1, and using AND
        // as the combination function. This will cause only a single CfL sample to
        // be selected.
        selected_row = Choose(0, 31 >> subsampling_y)
        selected_col = Choose(0, 31 >> subsampling_x)
        for (i = 0; i < bh; i++) {
          enc_palette_row_sign[i] = ((i >> subsampling_y) == selected_row) ? 1 : 0
        }
        for (j = 0; j < bw; j++) {
          enc_palette_col_sign[j] = ((j >> subsampling_x) == selected_col) ? 1 : 0
        }
        enc_pixel_pattern_fn = PIXEL_FN_AND
        enc_pixel_pattern_sign = ChooseBit()
        enc_build_binary_palette_pattern(ptype, bw, bh)
        return 1
      }
      // else fall through
    }

    // Next, try the filter patterns.
    // These only need the block size to be at least 8x8, which is always
    // true if we're using palette mode.
    z = Choose(0, 99)
    if (z < enc_pixel_pattern_prob) {
      // To hit the most extreme intermediate values (particularly 'sum' in the vertical
      // filter) in the inter predictor, we need to use the EIGHTTAP_SHARP filter on both
      // axes. We also need a filter block where all input pixels are either 0 or maxPx,
      // in a specific pattern.
      // The same filter block will work for the warp filter if we set the linear part
      // of the transformation to the identity matrix (see choose_warped()). The warp filter
      // does not have a configurable coefficient set, so our task is a little simpler
      //
      // To work out the pattern, note that the contribution of a particular pixel px[i][j]
      // to the sum is +ve if (sharp_filter_sign_pattern[i] ^ sharp_filter_sign_pattern[j]) == 0,
      // and -ve otherwise. Thus:
      //
      // * To minimize the vertical filter sum, the 8x8 filter block needs to have
      //   px[i][j] = (sharp_filter_sign_pattern[i] ^ sharp_filter_sign_pattern[j]) ? maxPx : 0
      // * To maximize the vertical filter sum, it's the other way around:
      //   px[i][j] = (sharp_filter_sign_pattern[i] ^ sharp_filter_sign_pattern[j]) ? 0 : maxPx
      //
      // Note that we pick one sign per the transform block and "tile" the resulting 8x8 pattern
      // across the whole transform block. To explain why we do this, rather than alternating signs
      // or picking random signs for each 8x8, consider the 1d case:
      //
      // In 1d, the patterns we want are 0 1 0 1 1 0 1 0 and 1 0 1 0 0 1 0 1.
      // If we tile the first pattern twice, we get:
      // /-------------\ /-------------\
      // 0 1 0 1 1 0 1 0 0 1 0 1 1 0 1 0
      //         \-------------/
      // The two marked regions on the top are our two repeats of the first pattern.
      // But - as shown by the bottom row - we've also generated a copy of the second pattern!
      // Thus tiling generates extra overlapping copies of the "other" pattern we want,
      // boosting the chance of getting a useful filter block.
      for (i = 0; i < bh; i++) {
        enc_palette_row_sign[i] = sharp_filter_sign_pattern[i%8]
      }
      for (j = 0; j < bw; j++) {
        enc_palette_col_sign[j] = sharp_filter_sign_pattern[j%8]
      }
      enc_pixel_pattern_fn = PIXEL_FN_XOR
      // Either sense of the pattern is acceptable
      enc_pixel_pattern_sign = ChooseBit()
      enc_build_binary_palette_pattern(ptype, bw, bh)
      return 1
    } else if (z < 2*enc_pixel_pattern_prob) {
      // To hit the most extreme values in the Wiener filter, we do something similar,
      // except that the pattern of signs in the most extreme Wiener filter is
      // {-, -, -, +, - ,- ,-}.
      // We thus create a 4-pixel repeating pattern {-, -, -, +}, since that tiles
      // better and still manages to produce extremal values.
      // We don't get any overlap here, so we have to generate at least one block
      // of each sign for full coverage
      for (i = 0; i < bh; i++) {
        enc_palette_row_sign[i] = wiener_filter_sign_pattern[i%4]
      }
      for (j = 0; j < bw; j++) {
        enc_palette_col_sign[j] = wiener_filter_sign_pattern[j%4]
      }
      enc_pixel_pattern_fn = PIXEL_FN_XOR
      enc_pixel_pattern_sign = ChooseBit()
      enc_build_binary_palette_pattern(ptype, bw, bh)
      return 1
    } else if (z < 3*enc_pixel_pattern_prob) {
      // Selfguided filter
      for (i = 0; i < bh; i++) {
        enc_palette_row_sign[i] = (i % 8 == 3) ? 1 : 0
      }
      for (j = 0; j < bw; j++) {
        enc_palette_col_sign[j] = (j % 8 == 3) ? 1 : 0
      }
      enc_pixel_pattern_fn = PIXEL_FN_AND
      enc_pixel_pattern_sign = ChooseBit()
      if (enc_pixel_pattern_sign) {
        // Central pixel is index 0, others are index 1
        x = enc_lrMinimizingValue()
        enc_pattern_colours[ptype][0] = 0
        enc_pattern_colours[ptype][1] = x
      } else {
        // Central pixel is index 1, others are index 0
        x = enc_lrMaximizingValue()
        enc_pattern_colours[ptype][0] = x
        enc_pattern_colours[ptype][1] = pMax
      }
      enc_build_binary_palette_pattern(ptype, bw, bh)
      return 1
    } else if (z < 4*enc_pixel_pattern_prob) {
      // Sometimes just generate a random pattern out of the set we can generate
      for (i = 0; i < bh; i++) {
        enc_palette_row_sign[i] = ChooseBit()
      }
      for (j = 0; j < bw; j++) {
        enc_palette_col_sign[j] = ChooseBit()
      }
      enc_pixel_pattern_fn = Choose(PIXEL_FN_AND, PIXEL_FN_XOR)
      enc_pixel_pattern_sign = ChooseBit()
      enc_build_binary_palette_pattern(ptype, bw, bh)
      return 1
    }
  } else {
    // Not skip - try the CDEF pattern
    z = Choose(0, 99)
    if (z < enc_pixel_pattern_prob) {
      // CDEF pattern - we want to generate a 5x5 window where all pixels are
      // the min or max value, except for the central pixel, which differs
      // from that by exactly 'constrainMax' (ie, the maximum value which constrain()
      // can return).
      // As a 5x5 tiling is a pain, we expand to an 8x8 tiling. The extra
      // pixels are largely irrelevant, so we just set them to the same colour.
      //
      // We only want to generate this for the Y plane, because reaching the
      // maximum requires the strengths and damping settings to be maximized.
      // This can only happen in the Y plane, as the UV planes have a maximum
      // damping of 5 + (bd-8), while the Y plane can have a damping of 6 + (bd-8)

      // There are four possible patterns, corresponding to the {min, max}
      // value of {sum, final pixel value} in cdef_filter_block().
      //
      // By setting up the surrounding pixels appropriately, we can force the
      // use of whichever direction we want. Take direction 2 (vertical edge,
      // so we're filtering horizontally) for simplicity.
      // Then the four patterns are all of the form:
      //        y . . . y
      //        . y . y .
      //        x x z x x
      //        . y . y .
      //        y . . . y
      // where each pattern uses particular values of x, y, z, and '.' indicates
      // a pixel where we don't care what value it takes.
      // The four patterns are:
      // 0) {x = 0, y = ConstrainMaxPri - constrainMaxSec, z = constrainMaxPri}
      // 1) Same as (0), but with all values mapped v |-> pMax - v
      // 2) {x = 0, y = 0, z = constrainMaxSec}
      // 3) Same as (2), but with all values mapped v |-> pMax - v
      //
      // So that we can cover multiple patterns in one palette block, we create
      // an 8-element palette containing all 8 possible values, and then pick
      // between them for each 8x8.
      //
      // Finally, we have to fill out the extra pixels so that:
      // * The direction process selects direction 2 (vertical edge)
      // * The strength adjustment process (which depends on the detected edge
      //   strength) does not reduce the primary strength below 15 << (bd-8).
      // Fortunately, we can do this by just filling one of the remaining columns
      // with opposite-value pixels
      enc_palette_len[ptype] = 8
      enc_pattern_colours[ptype][0] = 0
      enc_pattern_colours[ptype][1] = enc_constrainMaxSec()
      enc_pattern_colours[ptype][2] = enc_constrainMaxPri() - enc_constrainMaxSec()
      enc_pattern_colours[ptype][3] = enc_constrainMaxPri()
      for (i = 0; i < 4; i++) {
        enc_pattern_colours[ptype][4+i] = pMax - enc_pattern_colours[ptype][3-i]
      }
      for (blocky = 0; blocky < bh; blocky += 8) {
        for (blockx = 0; blockx < bw; blockx += 8) {
          pattern = Choose(0, 3)

          if (pattern == 0) {
            for (i = 0; i < Min(bh-blocky, 8); i++) {
              for (j = 0; j < Min(bw-blockx, 8); j++) {
                enc_palette_entries[ptype][blockx+j][blocky+i] = 0
              }
            }
            enc_palette_entries[ptype][blockx+2][blocky+2] = 3
            for (i = 1; i <= 2; i++) {
              enc_palette_entries[ptype][blockx+2+i][blocky+2+i] = 2
              enc_palette_entries[ptype][blockx+2+i][blocky+2-i] = 2
              enc_palette_entries[ptype][blockx+2-i][blocky+2+i] = 2
              enc_palette_entries[ptype][blockx+2-i][blocky+2-i] = 2
            }
            for (i = 0; i < 8; i++) {
              enc_palette_entries[ptype][blockx+6][blocky+i] = 7
            }
          } else if (pattern == 1) {
            for (i = 0; i < Min(bh-blocky, 8); i++) {
              for (j = 0; j < Min(bw-blockx, 8); j++) {
                enc_palette_entries[ptype][blockx+j][blocky+i] = 7
              }
            }
            enc_palette_entries[ptype][blockx+2][blocky+2] = 4
            for (i = 1; i <= 2; i++) {
              enc_palette_entries[ptype][blockx+2+i][blocky+2+i] = 5
              enc_palette_entries[ptype][blockx+2+i][blocky+2-i] = 5
              enc_palette_entries[ptype][blockx+2-i][blocky+2+i] = 5
              enc_palette_entries[ptype][blockx+2-i][blocky+2-i] = 5
            }
            for (i = 0; i < 8; i++) {
              enc_palette_entries[ptype][blockx+6][blocky+i] = 0
            }
          } else if (pattern == 2) {
            for (i = 0; i < Min(bh-blocky, 8); i++) {
              for (j = 0; j < Min(bw-blockx, 8); j++) {
                enc_palette_entries[ptype][blockx+j][blocky+i] = 0
              }
            }
            enc_palette_entries[ptype][blockx+2][blocky+2] = 1
            for (i = 0; i < 8; i++) {
              enc_palette_entries[ptype][blockx+6][blocky+i] = 7
            }
          } else if (pattern == 3) {
            for (i = 0; i < Min(bh-blocky, 8); i++) {
              for (j = 0; j < Min(bw-blockx, 8); j++) {
                enc_palette_entries[ptype][blockx+j][blocky+i] = 7
              }
            }
            enc_palette_entries[ptype][blockx+2][blocky+2] = 6
            for (i = 0; i < 8; i++) {
              enc_palette_entries[ptype][blockx+6][blocky+i] = 0
            }
          } else {
            ASSERT(0, "Unexpected CDEF pattern number")
          }
        }
      }
      return 1
    }
  }

  return 0
}

// Decide on the colour set to use for a particular block.
// We pick between two options:
// * Sometimes, we want to inject a specific pattern of pixels,
//   in which case we set the palette to have specific values.
//   We only do this for the Y plane, for simplicity.
// * Other times, we pick a mix of colours from the above/left blocks
//   and new colours, and pick randomly among them.
enc_pick_palette_data(ptype) {
  if (enc_maximize_symbols_per_bit) {
    enc_use_palette_pattern[ptype] = 0
    // In this mode, we want to avoid coding new palette entries
    // as much as possible, because coding new entries uses many bits.
    // Instead, we try to reuse existing entries wherever possible
    // The following logic means that:
    // * If there are
    cacheLen = palette_cache_len[ptype]
    if (cacheLen < 2) {
      // If there are 0 or 1 cache entries, only code new ones
      // up to the minimum palette size of 2
      enc_palette_len[ptype] = 2
    } else {
      // If there are >= 2 cache entries, reuse some subset
      // of those entries, and don't code anything new
      enc_palette_len[ptype] = Choose(2, Min(cacheLen, 8))
    }
    for (i = 0; i < cacheLen; i++) {
      enc_use_palette_cache[ptype][i] = 1
    }
    return 0
  }

  // Try out the various patterns available, and decide whether to use
  // one or not.
  enc_use_palette_pattern[ptype] = enc_pick_palette_pattern(ptype)
  if (enc_use_palette_pattern[ptype]) {
    // We selected a pattern
    return 0
  }

  // If not using a pattern, just pick entries randomly. First decide
  // on the total # of colours to use
  enc_palette_size_y = 0
  enc_palette_size_uv = 0
  pspec = 0

  if (ptype == 0) {
    enc_palette_size_y                u(0)
    enc_palette_specify_color_cache_y u(0)
    CHECK (2 <= enc_palette_size_y && enc_palette_size_y <= 8,
           "enc_palette_size_y is %d, not in [2, 8].",
           enc_palette_size_y)
    psize = enc_palette_size_y
    pspec = enc_palette_specify_color_cache_y
  } else {
    enc_palette_size_uv                u(0)
    enc_palette_specify_color_cache_u u(0)
    CHECK (2 <= enc_palette_size_uv && enc_palette_size_uv <= 8,
           "enc_palette_size_uv is %d, not in [2, 8].",
           enc_palette_size_uv)
    psize = enc_palette_size_uv
    pspec = enc_palette_specify_color_cache_u
  }
  enc_palette_len[ptype] = psize

  // Start by zeroing everything
  cacheN = palette_cache_len[ptype]
  for (i = 0; i < cacheN; i++) {
    enc_use_palette_cache[ptype][i] = 0
  }

  if (pspec) {
    // Specify cache flags explicitly?
    idx = 0
    for (i = 0; i < cacheN && idx < psize; i++) {
      pflag = 0
      if (ptype == 0) {
        enc_use_palette_color_cache_y u(0)
        pflag = enc_use_palette_color_cache_y
      } else {
        enc_use_palette_color_cache_u u(0)
        pflag = enc_use_palette_color_cache_u
      }

      if (pflag) {
        enc_use_palette_cache[ptype][i] = 1
        idx ++
      }
    }
  } else {
    // Decide how many entries to take from cache, then use Floyd's
    // algorithm to pick them.
    cacheUsage = 0
    enc_palette_color_cache_y_length = 0
    enc_palette_color_cache_u_length = 0
    if (ptype == 0) {
      enc_palette_color_cache_y_length u(0)
      CHECK (0 <= enc_palette_color_cache_y_length &&
             enc_palette_color_cache_y_length <= Min(psize, cacheN),
             "enc_palette_color_cache_y_length = %d is out of range (PaletteSizeY=%d; cacheN=%d)",
             enc_palette_color_cache_y_length, psize, cacheN)
      cacheUsage = enc_palette_color_cache_y_length
    } else {
      enc_palette_color_cache_u_length u(0)
      CHECK (0 <= enc_palette_color_cache_u_length &&
             enc_palette_color_cache_u_length <= Min(psize, cacheN),
             "enc_palette_color_cache_u_length = %d is out of range (PaletteSizeUV=%d; cacheN=%d)",
             enc_palette_color_cache_u_length, psize, cacheN)
      cacheUsage = enc_palette_color_cache_u_length
    }

    // Select 'cacheUsage' out of 'cacheN' entries to use
    for (i = cacheN - cacheUsage; i < cacheN; i++) {
      j = Choose(0, i)
      if (enc_use_palette_cache[ptype][j]) {
        enc_use_palette_cache[ptype][i] = 1
      } else {
        enc_use_palette_cache[ptype][j] = 1
      }
    }
  }
}

enc_palette_index(ptype, y, x) {
  sz = (ptype == 0) ? PaletteSizeY : PaletteSizeUV
  if (sz == 0) {
    return 0
  }

  if (enc_maximize_symbols_per_bit) {
    // Select according to the underlying distribution
    if (x == 0 && y == 0) {
      // The first value is coded using a quasi-uniform code.
      // So just select uniformly from the available values
      return Choose(0, sz-1)
    } else {
      // The remaining values are coded using a CDF
      ctx = palette_color_context_lookup[CumulatedScore]
      if (ptype == 0) {
        return enc_choose_from_cdf(tile_palette_y_color_cdf[PaletteSizeY - 2][ctx], PaletteSizeY)
      } else {
        return enc_choose_from_cdf(tile_palette_uv_color_cdf[PaletteSizeUV - 2][ctx], PaletteSizeUV)
      }
    }
  }

  if (! enc_use_palette_pattern[ptype]) {
    return Choose(0,sz - 1)
  }

  // Work out the inverse of the "ColorOrder" permutation
  uint8 invColorOrder[PALETTE_COLORS]
  if (x == 0 && y == 0) {
    // No remapping is applied for the first pixel
    for (i = 0; i < PALETTE_COLORS; i++) {
      invColorOrder[i] = i
    }
  } else {
    for (i = 0; i < PALETTE_COLORS; i++) {
      invColorOrder[ColorOrder[i]] = i
    }
  }

  idx = enc_palette_entries[ptype][x][y]
  return invColorOrder[idx]
}

// Old code for generating coefficient blocks which
// max out the intermediate values in each transform.
// Keep this around for now in case it's useful, but note that
// it'll need heavy reworking since this code hasn't been updated
// since VP9.
#if 0
// Return a number between 1 and peak
// Bias towards top end of range
enc_choose_large_coef(peak) {
  v = peak*enc_coef_scale/100
  return Max(1,Choose(v,peak))
}

uint16 idct4_coef_vals_row[2][4] = { //max elements after row transform
    {0, 6270, 0, 6269},  //max 1, Out
    {0, 6270, 0, 6269}};
int20 idct4_high10_coef_vals_row[2][4] = {
    {0, 25080, 0, 25077}, //min 1, Out
    {0, 25079, 0, 25079}};
int20 idct4_high12_coef_vals_row[2][4] = {
    {0, 100320, 0, 100309},
    {0, 100317, 0, 100316}};
uint1 idct4_sign_vals_row[2][4] = {
    {0, 0, 0, 0},
    {0, 1, 0, 1}};

uint16 idct4_coef_vals_col[3][4][4] = { //max elements after col transform
    {{0, 1, 0, 1},       //min 1, max Out
     {0, 3846, 3845, 0},
     {0, 0, 0, 0},
     {0, 3844, 3832, 0}},
    {{0, 1, 0, 1},       //max 1, max Out
     {0, 3846, 3845, 0},
     {0, 0, 0, 0},
     {0, 3844, 3832, 0}},
    {{0, 0, 0, 0},       //min Out
     {0, 3082, 3074, 0},
     {0, 3081, 3081, 0},
     {0, 0, 0, 0}}};

int20 idct4_high10_coef_vals_col[3][4][4] = {
    {{0, 1, 0, 1},
     {0, 15376, 15376, 0},
     {0, 0, 0, 0},
     {0, 15377, 15376, 0}},
    {{0, 1, 0, 1},
     {0, 15376, 15376, 0},
     {0, 0, 0, 0},
     {0, 15377, 15376, 0}},
    {{0, 0, 0, 0},
     {0, 12314, 12314, 0},
     {0, 12323, 12325, 0},
     {0, 0, 0, 0}}};

int20 idct4_high12_coef_vals_col[3][4][4] = {
    {{0, 1, 0, 1},
     {0, 61505, 61507, 0},
     {0, 0, 0, 0},
     {0, 61509, 61506, 0}},
    {{0, 1, 0, 1},
     {0, 61505, 61507, 0},
     {0, 0, 0, 0},
     {0, 61509, 61506, 0}},
    {{0, 0, 0, 0},
     {0, 49275, 49274, 0},
     {0, 49272, 49271, 0},
     {0, 0, 0, 0}}};
uint1 idct4_sign_vals_col[3][4][4] = { //max elements after col transform
    {{0, 1, 0, 1},
     {0, 1, 1, 0},
     {0, 0, 0, 0},
     {0, 1, 1, 0}},
    {{0, 1, 0, 1},
     {0, 0, 0, 0},
     {0, 0, 0, 0},
     {0, 0, 0, 0}},
    {{0, 0, 0, 0},
     {0, 1, 1, 0},
     {0, 1, 1, 0},
     {0, 0, 0, 0}}};

uint16 idct8_coef_vals_row[12][8] = {  // max elements after row transform
    {499, 7470, 0, 5, 0, 0, 0, 796},   // max col 1
    {500, 7470, 0, 0, 0, 0, 4, 805},   // min col 1
    {0, 0, 1634, 0, 1, 0, 8189, 0},    // max cols 2 & 7
    {0, 0, 1632, 0, 1, 0, 8190, 0},    // min cols 2 & 7
    {0, 0, 1632, 0, 2, 0, 8189, 0},    // max cols 3 & 6
    {0, 0, 1624, 0, 3, 0, 8192, 0},    // min cols 3 & 6
    {0, 0, 8192, 0, 2, 0, 1625, 0},    // max cols 4 & 5
    {0, 0, 8190, 0, 6, 0, 1623, 0},    // min cols 4 & 5
    {500, 7470, 0, 0, 0, 0, 0, 812},   // max col 8
    {500, 7470, 0, 0, 0, 0, 2, 809},   // min col 8
    {0, 851, 0, 633, 0, 423, 0, 8183}, // max step1 (&& 2)
    {0,4797,0,2745,0,1834,0,954}};     // min step2

int20 idct8_high10_coef_vals_row[12][8] = {  // max elements after row transform
    {1776, 29879, 0, 20, 0, 0, 0, 3183},     // max col 1
    {1776, 29883, 0, 0, 0, 0, 16, 3220},     // min col 1
    {0, 0, 6538, 0, 4, 0, 32756, 0},         // max cols 2 & 7
    {0, 0, 6531, 0, 8, 0, 32756, 0},         // min cols 2 & 7
    {0, 0, 6540, 0, 8, 0, 32752, 0},         // max cols 3 & 6
    {0, 0, 6495, 0, 12, 0, 32768, 0},        // min cols 3 & 6
    {0, 0, 32768, 0, 9, 0, 6500, 0},         // max cols 4 & 5
    {0, 0, 32760, 0, 24, 0, 6492, 0},        // min cols 4 & 5
    {1776, 29883, 0, 0, 0, 0, 0, 3249},      // max col 8
    {1776, 29882, 0, 0, 0, 0, 0, 3255},      // min col 8
    {0, 3435, 0, 2507, 0, 1675, 0, 32727},   // max step1 (&& 2)
    {0, 19126, 0, 11032, 0, 7371, 0, 3804}}; // min step2

int20 idct8_high12_coef_vals_row[12][8] = {    // max elements after row transform
    {5325, 119523, 0, 80, 0, 0, 0, 12736},     // max col 1
    {5325, 119537, 0, 0, 0, 0, 65, 12881},     // min col 1
    {0, 0, 26154, 0, 16, 0, 131024, 0},        // max cols 2 & 7
    {0, 0, 26124, 0, 31, 0, 131025, 0},        // min cols 2 & 7
    {0, 0, 26160, 0, 32, 0, 131009, 0},        // max cols 3 & 6
    {0, 0, 25979, 0, 48, 0, 131072, 0},        // min cols 3 & 6
    {0, 0, 131072, 0, 36, 0, 26001, 0},        // max cols 4 & 5
    {0, 0, 131026, 0, 114, 0, 25969, 0},       // min cols 4 & 5
    {5325, 119539, 0, 0, 0, 0, 0, 12997},      // max col 8
    {5325, 119534, 0, 0, 0, 0, 0, 13023},      // min col 8
    {0, 13636, 0, 10116, 0, 6759, 0, 130929},  // max step1 (&& 2)
    {0, 76499, 0, 44132, 0, 29486, 0, 15215}}; // min step2

uint1 idct8_sign_vals_row[12][8] = {
    {0,0,0,0,0,0,0,0},  // max col 1
    {1,1,0,0,0,0,1,1},  // min col 1
    {0,0,0,0,1,0,1,0},  // max cols 2 & 7
    {0,0,1,0,0,0,0,0},  // min cols 2 & 7
    {0,0,1,0,1,0,0,0},  // max cols 3 & 6
    {0,0,0,0,0,0,1,0},  // min cols 3 & 6
    {0,0,1,0,0,0,1,0},  // max cols 4 & 5
    {0,0,0,0,1,0,0,0},  // min cols 4 & 5
    {0,1,0,0,0,0,0,1},  // max col 8
    {1,0,0,0,0,0,1,0},  // min col 8
    {0,0,0,1,0,1,0,1},  // max step1 (&& 2)
    {0,1,0,0,0,0,0,1}}; // min step2

uint16 idct8_coef_vals_col[5][8][8] = {
    {{0, 0, 0, 0, 0, 0, 0, 0},           //MINIMISE STEP 2
     {0, 2157, 0, 2160, 0, 1443, 0, 429},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 1237, 1606, 0, 0, 0, 0, 246},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 1081, 792, 0, 0, 0, 0, 215},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 564, 410, 0, 0, 0, 0, 113}},
    {{0, 0, 0, 0, 0, 0, 0, 0},           //MAXIMISE STEP 2 && MAX 3 && MAX OUTPUT
     {0, 3132, 4, 0, 0, 0, 0, 623},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 3127, 2, 0, 0, 0, 0, 622},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 3132, 4, 0, 0, 0, 0, 623},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 3132, 4, 0, 0, 0, 0, 623}},
    {{0, 0, 0, 0, 0, 0, 0, 0},           //MIN 3 && MIN OUTPUT
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 5993, 29, 0, 0, 0, 0, 1192},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 4507, 4, 0, 0, 0, 0, 896},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 2725, 29, 0, 0, 0, 0, 542},
     {0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0},           //MAX STEP 3
     {0, 3228, 16, 0, 0, 0, 0, 642},
     {0, 1, 0, 0, 0, 0, 0, 0},
     {0, 3067, 0, 0, 0, 0, 0, 610},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 3067, 0, 0, 0, 0, 0, 610},
     {0, 3, 0, 0, 0, 0, 0, 0},
     {0, 3067, 0, 0, 0, 0, 0, 610}},
    {{0, 0, 0, 0, 0, 0, 0, 0},           //MAX STEP 2
     {0, 3595, 55, 0, 0, 0, 0, 715},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 3580, 12, 0, 0, 0, 0, 712},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 2399, 0, 0, 0, 0, 0, 478},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 714, 12, 0, 0, 0, 0, 142}}};

int20 idct8_high10_coef_vals_col[5][8][8] = {
    {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 10553, 0, 10553, 0, 0, 0, 6},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 11237, 12, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 7505, 11, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 3868, 11, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 13050, 1, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 13000, 12, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 13019, 22, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 13018, 22, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 25000, 31, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 18721, 38, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 11405, 37, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 13463, 31, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0},
    {0, 12752, 0, 0, 0, 0, 0, 1},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 12752, 0, 0, 0, 0, 0, 1},
    {0, 3, 0, 0, 0, 0, 0, 0},
    {0, 12752, 0, 0, 0, 0, 0, 1}},
    {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 14943, 311, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 14581, 300, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 9750, 0, 0, 0, 0, 0, 910},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 2972, 62, 0, 0, 0, 0, 0}}
};

int20 idct8_high12_coef_vals_col[5][8][8] = {
    {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 42212, 0, 42212, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 44123, 928, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 29480, 620, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 15211, 321, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 51209, 1070, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 51209, 886, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 51202, 1017, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 51194, 1020, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 51505, 51605, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 73573, 1543, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 44879, 935, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 52938, 1106, 0, 0, 0, 0, 0},
    {0, 1, 0, 0, 0, 0, 0, 0},
    {0, 50026, 0, 0, 0, 0, 0, 4930},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 50026, 0, 0, 0, 0, 0, 4930},
    {0, 3, 0, 0, 0, 0, 0, 0},
    {0, 50026, 0, 0, 0, 0, 0, 4930}},
    {{0, 0, 0, 0, 0, 0, 0, 0},
    {0, 59787, 1254, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 58292, 1213, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 38947, 0, 0, 0, 0, 0, 3839},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 11886, 255, 0, 0, 0, 0, 0}}
};

uint1 idct8_sign_vals_col[5][8][8] = {
     {{0, 0, 0, 0, 0, 0, 0, 0},          //MINIMISE STEP 2
     {0, 1, 0, 1, 0, 1, 0, 1},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 1, 1, 0, 0, 0, 0, 1}},
    {{0, 0, 0, 0, 0, 0, 0, 0},           //MAXIMISE STEP 2 && MAX 3 && MAX OUTPUT
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0},           //MIN 3 && MIN OUTPUT
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 1, 1, 0, 0, 0, 0, 1},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0},           //MAX STEP 3
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 1, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 1, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0},           //MAX STEP 2
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 1, 1, 0, 0, 0, 0, 1},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 1, 0, 0, 0, 0, 0, 1},
     {0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0}}};

uint16 idct16_coef_vals_row[9][16] = {                        // max elements after row transform
    {0, 1606, 0, 1606, 0, 1606, 0, 1606, 0, 1606, 0, 1605, 0, 1605, 0, 1606}, // max 5, 6, Output
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8192, 0, 0, 0},                      // min 1, 2, 3
    {0, 0, 1790, 0, 1790, 0, 1790, 0, 1790, 0, 1790, 0, 1790, 0, 1790, 0},    // min 6, Output
    {0, 0, 0, 0, 1906, 0, 0, 0, 0, 0, 0, 0, 8077, 0, 0, 0},                   // max 4, 5, 6, Output
    {0, 0, 1734, 0, 0, 0, 6021, 0, 0, 0, 5829, 0, 0, 0, 345, 0},              // max 3, 4, 5, 6, Output
    {555, 0, 0, 0, 5853, 0, 0, 0, 5048, 0, 0, 0, 0, 0, 0, 0},                 // min 5, 6, Output
    {0, 0, 0, 0, 8192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},                      // min 1, 2, 3
    {0, 0, 2247, 0, 0, 0, 4032, 0, 0, 0, 4153, 0, 0, 0, 1684, 0},             // min 4
    {0, 1141, 0, 7404, 0, 265, 0, 1129, 0, 199, 0, 496, 0, 3811, 0, 1392}};   // max 2 3 4 5 6 Output

int20 idct16_high10_coef_vals_row[9][16] = {                        // max elements after row transform
    {0, 6424, 0, 6424, 0, 6424, 0, 6424, 0, 6424, 0, 6420, 0, 6424, 0, 6424},   // max 5, 6, Output
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32768, 0, 0, 0},                       // min 1, 2, 3
    {0, 0, 7160, 0, 7160, 0, 7160, 0, 7160, 0, 7160, 0, 7160, 0, 7161, 0},      // min 6, Output
    {0, 0, 0, 0, 7627, 0, 0, 0, 0, 0, 0, 0, 32308, 0, 0, 0},                    // max 4, 5, 6, Output
    {0, 0, 6963, 0, 0, 0, 24107, 0, 0, 0, 23302, 0, 0, 0, 1385, 0},             // max 3, 4, 5, 6, Output
    {1974, 0, 0, 0, 23416, 0, 0, 0, 20188, 0, 0, 0, 0, 0, 0, 0},                // min 5, 6, Output
    {0, 0, 0, 0, 32768, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},                       // min 1, 2, 3
    {0, 0, 9041, 0, 0, 0, 16093, 0, 0, 0, 16572, 0, 0, 0, 6732, 0},             // min 4
    {0, 4564, 0, 29617, 0, 1060, 0, 4516, 0, 796, 0, 1983, 0, 15244, 0, 5569}}; // max 2 3 4 5 6 Output

int20 idct16_high12_coef_vals_row[9][16] = {                        // max elements after row transform
    {0, 25696, 0, 25696, 0, 25696, 0, 25696, 0, 25696, 0, 25681, 0, 25696, 0, 25696}, // max 5, 6, Output
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 131072, 0, 0, 0},                            // min 1, 2, 3
    {0, 0, 28640, 0, 28640, 0, 28640, 0, 28640, 0, 28640, 0, 28640, 0, 28641, 0},     // min 6, Output
    {0, 0, 0, 0, 30512, 0, 0, 0, 0, 0, 0, 0, 129231, 0, 0, 0},                        // max 4, 5, 6, Output
    {0, 0, 27804, 0, 0, 0, 96388, 0, 0, 0, 93236, 0, 0, 0, 5530, 0},                  // max 3, 4, 5, 6, Output
    {5920, 0, 0, 0, 93647, 0, 0, 0, 80768, 0, 0, 0, 0, 0, 0, 0},                      // min 5, 6, Output
    {0, 0, 0, 0, 131072, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},                            // min 1, 2, 3
    {0, 0, 36287, 0, 0, 0, 64256, 0, 0, 0, 66231, 0, 0, 0, 26970, 0},                 // min 4
    {0, 18255, 0, 118471, 0, 4243, 0, 18064, 0, 3184, 0, 7938, 0, 60970, 0, 22276}};  // max 2 3 4 5 6 Output

uint1 idct16_sign_vals_row[9][16] = {
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0},
    {0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1}};

uint16 idct16_coef_vals_col[6][16][16] = {
    {{0, 0, 575, 0, 591, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // min 5, 6, Output}
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 3073, 0, 3073, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 2658, 0, 2642, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},     // min 4
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 1185, 0, 1174, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 2117, 0, 2117, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 2180, 0, 2181, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 887, 0, 881, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},     // min 1,2,3
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 4301, 0, 4301, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},     // max 6, Output
     {0, 0, 2210, 0, 2202, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 2203, 0, 2192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},     // max 5
     {0, 0, 2068, 0, 2067, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 1989, 0, 1982, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 522, 0, 514, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 33, 0, 28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},     // max 4
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 16, 0, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 513, 0, 504, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 1722, 0, 1722, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 2633, 0, 2649, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}};

int20 idct16_high10_coef_vals_col[6][16][16] = {
    {{0, 0, 2322, 0, 2341, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 12291, 0, 12293, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 10598, 0, 10605, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 4737, 0, 4731, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 8458, 0, 8456, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 8715, 0, 8706, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 3537, 0, 3535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 17205, 0, 17203, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 8813, 0, 8811, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 8812, 0, 8811, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 8276, 0, 8265, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 7945, 0, 7940, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 2081, 0, 2064, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 130, 0, 113, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 102, 0, 104, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1991, 0, 1995, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 6891, 0, 6879, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 10597, 0, 10594, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}};

int20 idct16_high12_coef_vals_col[6][16][16] = {
    {{0, 0, 9325, 0, 9325, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 49175, 0, 49160, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 42404, 0, 42407, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 18943, 0, 18931, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 33829, 0, 33827, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 34843, 0, 34843, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 14135, 0, 14154, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 68827, 0, 68805, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 35256, 0, 35256, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 35244, 0, 35245, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 33105, 0, 33104, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 31756, 0, 31751, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 8279, 0, 8286, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 480, 0, 495, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 151, 0, 170, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 8340, 0, 8341, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 27552, 0, 27555, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 42113, 0, 42114, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}};

uint1 idct16_sign_vals_col[6][16][16] = {
   {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  //min 5, 6, Output
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

   {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  //min 4
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

   {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  //min 1,2,3
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

   {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},   // max 6, Output
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  // max 5
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  // max 4
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}};

uint16 idct32_coef_vals_row[8][32] = {                                                                       // max elements after row transform
    {10, 4096, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 104},             //max 1
    {0, 0, 0, 6791, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8187, 0, 0, 0, 0, 0, 8191, 0, 145, 0, 0, 0, 1122, 0, 0, 0, 3974, 0, 0},  //max 2 , min 6
    {0, 8191, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 2, 0, 2559, 0, 8191, 0, 0, 0, 1, 0, 1, 0, 2, 0, 0, 0, 0, 0, 1214},       //max 3, 4, 5, 6, 7, Out
    {0, 0, 8192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},                //min 1
    {0, 2621, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8190, 0, 3474, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8191},       //min 2
    {0, 0, 6552, 0, 0, 0, 0, 0, 0, 0, 1032, 0, 0, 0, 6552, 0, 0, 0, 6552, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6552, 0},    //min 3, max 7 Out
    {0, 0, 0, 0, 6393, 0, 0, 0, 0, 0, 0, 0, 6393, 0, 0, 0, 0, 0, 0, 0, 6393, 0, 0, 0, 0, 0, 0, 0, 6393, 0, 0, 0},       //min 4
    {3011, 0, 0, 0, 0, 0, 0, 0, 6022, 0, 0, 0, 0, 0, 0, 0, 6022, 0, 0, 0, 0, 0, 0, 0, 6022, 0, 0, 0, 0, 0, 0, 0}};      //min 5, 6, 7, Out

int20 idct32_high10_coef_vals_row[8][32] = {                                                                       // max elements after row transform
    {320, 65534, 227, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1603},           //max 1
    {0, 0, 0, 27169, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32748, 0, 0, 0, 0, 0, 32764, 0, 578, 0, 0, 0, 4488, 0, 0, 0, 15896, 0, 0}, //max 2 , min 6
    {0, 32765, 0, 0, 0, 8, 0, 0, 0, 12, 0, 0, 0, 6, 0, 10236, 0, 32764, 0, 0, 0, 4, 0, 5, 0, 9, 0, 0, 0, 0, 0, 4856},      //max 3, 4, 5, 6, 7, Out
    {0, 0, 32768, 0, 0, 0, 0, 0, 0, 0, 71, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},                 //min 1
    {0, 10491, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32760, 0, 13896, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32764},      //min 2
    {0, 0, 26208, 0, 0, 0, 0, 0, 0, 0, 4129, 0, 0, 0, 26208, 0, 0, 0, 26208, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 26208, 0},   //min 3, max 7 Out
    {0, 0, 0, 0, 25570, 0, 0, 0, 0, 0, 0, 0, 25572, 0, 0, 0, 0, 0, 0, 0, 25573, 0, 0, 0, 0, 0, 0, 0, 25572, 0, 0, 0},      //min 4
    {10704, 0, 0, 0, 0, 0, 0, 0, 24089, 0, 0, 0, 0, 0, 0, 0, 24088, 0, 0, 0, 0, 0, 0, 0, 24088, 0, 0, 0, 0, 0, 0, 0}};     //min 5, 6, 7, Out

int20 idct32_high12_coef_vals_row[8][32] = {                                                                       // max elements after row transform
    {4, 262137, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6656},                   //max 1
    {0, 0, 0, 108671, 0, 0, 0, 0, 0, 0, 0, 0, 0, 130992, 0, 0, 0, 0, 0, 131056, 0, 2310, 0, 0, 0, 17952, 0, 0, 0, 63586, 0, 0}, //max 2 , min 6
    {0, 131061, 0, 0, 0, 35, 0, 0, 0, 48, 0, 0, 0, 32, 0, 40944, 0, 131056, 0, 0, 0, 16, 0, 16, 0, 32, 0, 0, 0, 0, 0, 19424},   //max 3, 4, 5, 6, 7, Out
    {0, 0, 131072, 0, 0, 0, 0, 0, 0, 0, 358, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},                      //min 1
    {0, 41901, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 131040, 0, 55585, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 131059},         //min 2
    {0, 0, 104775, 0, 0, 0, 0, 0, 0, 0, 16478, 0, 0, 0, 104832, 0, 0, 0, 104835, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 104835, 0},   //min 3, max 7 Out
    {0, 0, 0, 0, 102277, 0, 0, 0, 0, 0, 0, 0, 102291, 0, 0, 0, 0, 0, 0, 0, 102291, 0, 0, 0, 0, 0, 0, 0, 102288, 0, 0, 0},       //min 4
    {32114, 0, 0, 0, 0, 0, 0, 0, 96352, 0, 0, 0, 0, 0, 0, 0, 96352, 0, 0, 0, 0, 0, 0, 0, 96352, 0, 0, 0, 0, 0, 0, 0}};          //min 5, 6, 7, Out

uint1 idct32_sign_vals_row[8][32] = {
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  //max 1
    {0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  //max 2 , min 6
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1},  //max 3, 4, 5, 6, 7, Out
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  //min 1
    {0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  //min 2
    {0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  //min 3, max 7, Out
    {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},  //min 4
    {1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0}   //min 5, 6, 7, Out
    };

uint16 idct32_coef_vals_col[6][32][32] = {
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //min 4
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 3259, 0, 3211, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 3259, 0, 3211, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 3259, 0, 3211, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 3259, 0, 3211, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 3047, 0, 3048, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //min 5, 6, 7, Out
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 3047, 0, 3048, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 3047, 0, 3048, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 3047, 0, 3048, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 6, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, // max 7, Out
    {0, 0, 5030, 0, 5019, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 3321, 0, 3288, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  //max 6
    {0, 0, 4221, 0, 4146, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 4193, 0, 4121, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //max 5
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 2572, 0, 2535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 2573, 0, 2535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 2573, 0, 2535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 2573, 0, 2535, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},  // max 4
    {0, 0, 1620, 0, 1620, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1619, 0, 1620, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1619, 0, 1620, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1619, 0, 1620, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1619, 0, 1620, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1619, 0, 1620, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1619, 0, 1620, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1619, 0, 1620, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
};

uint1 idct32_sign_vals_col[6][32][32] = {
    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},

    {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}};

int20 idct32_high10_coef_vals_col[6][32][32] = {
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 12919, 0, 12965, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 12918, 0, 12965, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 12918, 0, 12965, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 12919, 0, 12965, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 12195, 0, 12183, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 12198, 0, 12183, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 12198, 0, 12183, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 12198, 0, 12183, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 6, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 20132, 0, 20089, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 13213, 0, 13225, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 16735, 0, 16735, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 16631, 0, 16632, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 10217, 0, 10216, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 10218, 0, 10216, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 10218, 0, 10216, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 10217, 0, 10216, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 6468, 0, 6489, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 6468, 0, 6489, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 6468, 0, 6489, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 6468, 0, 6489, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 6468, 0, 6489, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 6468, 0, 6489, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 6468, 0, 6489, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 6468, 0, 6489, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}};

int20 idct32_high12_coef_vals_col[6][32][32] = {
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 51764, 0, 51766, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 51765, 0, 51766, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 51765, 0, 51766, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 51765, 0, 51766, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 48775, 0, 48748, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 48773, 0, 48748, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 48773, 0, 48748, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 48773, 0, 48748, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 6, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 80456, 0, 80455, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 53126, 0, 52624, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 67539, 0, 66338, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 67101, 0, 65939, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 41146, 0, 40583, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 41146, 0, 40583, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 41146, 0, 40585, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 41146, 0, 40583, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 25912, 0, 25915, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 25912, 0, 25915, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 25912, 0, 25915, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 25912, 0, 25915, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 25912, 0, 25915, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 25912, 0, 25915, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 25912, 0, 25915, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 25912, 0, 25915, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}};

uint16 iadst4_coef_vals_row[2][4] = { //max elements after row transform
    {0, 4701, 4689, 0},   //max out
    {0, 4691, 4698, 0}};  //min out
int20 iadst4_high10_coef_vals_row[2][4] = {
    {0, 18799, 18761, 0},
    {0, 18764, 18792, 0}};
int20 iadst4_high12_coef_vals_row[2][4] = {
    {0, 75196, 75045, 0},
    {0, 75113, 75118, 0}};
uint1 iadst4_sign_vals_row[2][4] = {
    {0, 0, 0, 0},
    {0, 1, 1, 0}};

uint16 iadst4_coef_vals_col[2][4][4] = { //max elements after col transform
{{0, 0, 0, 0},
{0, 2689, 2692, 0},
{0, 2690, 2688, 0},
{0, 0, 0, 0}},
{{0, 0, 0, 0},
{0, 2690, 2687, 0},
{0, 2694, 2691, 0},
{0, 0, 0, 0}}};

int20 iadst4_high10_coef_vals_col[2][4][4] = {
{{0, 0, 0, 0},
{0, 10774, 10774, 0},
{0, 10750, 10750, 0},
{0, 0, 0, 0}},
{{0, 0, 0, 0},
{0, 10754, 10753, 0},
{0, 10770, 10769, 0},
{0, 0, 0, 0}}};

int20 iadst4_high12_coef_vals_col[2][4][4] = {
{{0, 0, 0, 0},
{0, 43095, 43095, 0},
{0, 43005, 43005, 0},
{0, 0, 0, 0}},
{{0, 0, 0, 0},
{0, 43049, 43044, 0},
{0, 43047, 43051, 0},
{0, 0, 0, 0}}};

uint1 iadst4_sign_vals_col[2][4][4] = {
{{0, 0, 0, 0},
{0, 0, 0, 0},
{0, 0, 0, 0},
{0, 0, 0, 0}},
{{0, 0, 0, 0},
{0, 1, 1, 0},
{0, 1, 1, 0},
{0, 0, 0, 0}}};

uint16 iadst8_coef_vals_row[6][8] = {
{0, 0, 0, 3414, 3407, 0, 0, 3409},          // min 1
{0, 0, 0, 3411, 3409, 0, 0, 3409},          // max 1
{0, 0, 4365, 4363, 0, 0, 0, 0},             // min 2
{0, 0, 4366, 4362, 0, 0, 0, 0},             // max 2
{0, 0, 4193, 0, 4200, 0, 0, 0},             // min Out
{0, 2044, 2044, 2044, 2044, 2043, 2044, 0}, // max Out
};

int20 iadst8_high10_coef_vals_row[6][8] = {
{0, 0, 0, 13652, 13631, 0, 0, 13636},
{0, 0, 0, 13643, 13638, 0, 0, 13636},
{0, 0, 17458, 17455, 0, 0, 0, 0},
{0, 0, 17464, 17448, 0, 0, 0, 0},
{0, 0, 16772, 0, 16800, 0, 0, 0},
{0, 8176, 8176, 8176, 8175, 8175, 8176, 0},
};

int20 iadst8_high12_coef_vals_row[6][8] = {
{0, 0, 0, 54608, 54524, 0, 0, 54544},
{0, 0, 0, 54570, 54555, 0, 0, 54544},
{0, 0, 69822, 69832, 0, 0, 0, 0},
{0, 0, 69852, 69798, 0, 0, 0, 0},
{0, 0, 67089, 0, 67200, 0, 0, 0},
{0, 32704, 32704, 32704, 32701, 32700, 32704, 0},
};

uint1 iadst8_sign_vals_row[6][8] = {
{0, 0, 0, 0, 0, 0, 0, 1},
{0, 0, 0, 1, 1, 0, 0, 0},
{0, 0, 1, 0, 0, 0, 0, 0},
{0, 0, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 0, 1, 0, 0, 0},
{0, 1, 0, 1, 0, 1, 0, 0},
};

uint16 iadst16_coef_vals_row[7][16] = {
{0, 0, 3169, 100, 1, 3169, 0, 0, 0, 0, 3169, 0, 0, 3172, 0, 0}, // min 1, Out
{0, 0, 3169, 109, 2, 3169, 0, 0, 0, 0, 3170, 0, 0, 3169, 0, 0}, // max 1
{0, 0, 0, 2513, 2515, 0, 0, 2515, 2515, 0, 0, 0, 0, 0, 0, 0},   // min 2
{0, 0, 0, 2515, 2515, 0, 0, 2512, 2515, 0, 0, 0, 0, 0, 0, 0},   // max 2
{0, 0, 2997, 2996, 2996, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},      // min 3, Out
{0, 0, 4374, 0, 4368, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},         // max 3
{0, 0, 4374, 0, 4368, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},         // max Out
};

int20 iadst16_high10_coef_vals_row[7][16] = {
{0, 0, 12676, 399, 1, 12676, 0, 0, 0, 0, 12676, 0, 0, 12687, 0, 0},
{0, 0, 12676, 407, 7, 12676, 0, 0, 0, 0, 12680, 0, 0, 12677, 0, 0},
{0, 0, 0, 10061, 10061, 0, 0, 10053, 10053, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 10067, 10067, 0, 0, 10025, 10067, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 11978, 11990, 11990, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 17493, 0, 17477, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 17504, 0, 17465, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
};

int20 iadst16_high12_coef_vals_row[7][16] = {
{0, 0, 50704, 1595, 3, 50704, 0, 0, 0, 0, 50704, 0, 0, 50747, 0, 0},
{0, 0, 50705, 1600, 28, 50705, 0, 0, 0, 0, 50715, 0, 0, 50715, 0, 0},
{0, 0, 0, 40244, 40244, 0, 0, 40212, 40212, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 40268, 40268, 0, 0, 40101, 40268, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 47911, 47960, 47960, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 69950, 0, 69932, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 69935, 0, 69948, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
};

uint1 iadst16_sign_vals_row[7][16] = {
{0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0},
{0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
};

uint16 iadst8_coef_vals_col[6][8][8] = {
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},                // min 1
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1973, 0, 1974, 0, 0, 0, 0},
{0, 1969, 0, 1970, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1971, 0, 1970, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},                 // max 1
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1969, 0, 1975, 0, 0, 0, 0},
{0, 1967, 0, 1975, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1967, 0, 1975, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},                 // min 2
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 2524, 0, 2522, 0, 0, 0, 0},
{0, 2522, 0, 2522, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},                 // max 2
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 2522, 0, 2526, 0, 0, 0, 0},
{0, 2524, 0, 2518, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},                 // min Out
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 2425, 0, 2422, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 2426, 0, 2430, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},                 // max Out
{0, 1180, 0, 1182, 0, 0, 0, 0},
{0, 1180, 0, 1182, 0, 0, 0, 0},
{0, 1180, 0, 1182, 0, 0, 0, 0},
{0, 1180, 0, 1182, 0, 0, 0, 0},
{0, 1180, 0, 1182, 0, 0, 0, 0},
{0, 1184, 0, 1176, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
};

uint16 iadst8_high10_coef_vals_col[6][8][8] = {
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 7891, 0, 7892, 0, 0, 0, 0},
{0, 7878, 0, 7881, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 7884, 0, 7880, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 7875, 0, 7900, 0, 0, 0, 0},
{0, 7870, 0, 7900, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 7871, 0, 7896, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 10091, 0, 10092, 0, 0, 0, 0},
{0, 10091, 0, 10088, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 10095, 0, 10095, 0, 0, 0, 0},
{0, 10087, 0, 10084, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 9695, 0, 9695, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 9703, 0, 9721, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 4724, 0, 4726, 0, 0, 0, 0},
{0, 4725, 0, 4726, 0, 0, 0, 0},
{0, 4725, 0, 4726, 0, 0, 0, 0},
{0, 4725, 0, 4726, 0, 0, 0, 0},
{0, 4725, 0, 4726, 0, 0, 0, 0},
{0, 4725, 0, 4726, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}}};

uint16 iadst8_high12_coef_vals_col[6][8][8] = {
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 31563, 0, 31569, 0, 0, 0, 0},
{0, 31518, 0, 31516, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 31538, 0, 31517, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 31545, 0, 31542, 0, 0, 0, 0},
{0, 31535, 0, 31535, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 31530, 0, 31527, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 40360, 0, 40360, 0, 0, 0, 0},
{0, 40365, 0, 40367, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 40377, 0, 40378, 0, 0, 0, 0},
{0, 40347, 0, 40345, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 38777, 0, 38784, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 38813, 0, 38883, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 18904, 0, 18902, 0, 0, 0, 0},
{0, 18904, 0, 18902, 0, 0, 0, 0},
{0, 18904, 0, 18902, 0, 0, 0, 0},
{0, 18904, 0, 18902, 0, 0, 0, 0},
{0, 18904, 0, 18902, 0, 0, 0, 0},
{0, 18905, 0, 18902, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
};

uint1 iadst8_sign_vals_col[6][8][8] = {
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0}},
};

uint16 iadst16_coef_vals_col[6][16][16] = {
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},       // min 1, Out
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1639, 0, 1644, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 46, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1639, 0, 1644, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1639, 0, 1644, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1642, 0, 1644, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},       // max 1, Out
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1639, 0, 1644, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 55, 0, 58, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1639, 0, 1644, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1639, 0, 1645, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1639, 0, 1644, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},       // min 2
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1302, 0, 1301, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1304, 0, 1301, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1304, 0, 1301, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1304, 0, 1301, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},       // max 2
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1304, 0, 1301, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1304, 0, 1301, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1301, 0, 1301, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1304, 0, 1301, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},       // min 3, Out
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1559, 0, 1545, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1558, 0, 1545, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1558, 0, 1545, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},       // max 3
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 2266, 0, 2265, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 2260, 0, 2265, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
};

uint16 iadst16_high10_coef_vals_col[6][16][16] = {
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 6565, 0, 6566, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 210, 0, 203, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 6565, 0, 6566, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 6565, 0, 6566, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 6573, 0, 6569, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 6565, 0, 6566, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 206, 0, 216, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 6565, 0, 6566, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 6569, 0, 6566, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 6566, 0, 6566, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 5212, 0, 5210, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 5212, 0, 5210, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 5204, 0, 5210, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 5204, 0, 5210, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 5214, 0, 5214, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 5214, 0, 5214, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 5205, 0, 5179, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 5214, 0, 5214, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 6202, 0, 6206, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 6210, 0, 6210, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 6210, 0, 6210, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 9054, 0, 9067, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 9052, 0, 9052, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
};

uint16 iadst16_high12_coef_vals_col[6][16][16] = {
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 26265, 0, 26258, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 829, 0, 823, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 26265, 0, 26258, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 26265, 0, 26258, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 26283, 0, 26285, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 26266, 0, 26258, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 834, 0, 823, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 31, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 26266, 0, 26258, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 26276, 0, 26258, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 26276, 0, 26258, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 20844, 0, 20844, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 20844, 0, 20844, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 20823, 0, 20832, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 20823, 0, 20832, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 20854, 0, 20859, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 20854, 0, 20859, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 20770, 0, 20770, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 20854, 0, 20859, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 24815, 0, 24815, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 24837, 0, 24844, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 24837, 0, 24844, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 36230, 0, 36230, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 36221, 0, 36220, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
};


uint1 iadst16_sign_vals_col[6][16][16] = {
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
{{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
};

uint16 iwht4_coef_vals_row[2][4] = { //max elements after row transform
    {8192, 8192, 8192, 8192},   //min out
    {8191, 8191, 8192, 8192}};  //max out
int20 iwht4_high10_coef_vals_row[2][4] = {
    {32768, 32768, 32768, 32768},
    {32767, 32767, 32768, 32768}};
int20 iwht4_high12_coef_vals_row[2][4] = {
    {131072, 131072, 131072, 131072},
    {131071, 131071, 131072, 131072}};
uint1 iwht4_sign_vals_row[2][4] = {
    {1, 1, 1, 1},
    {0, 0, 1, 1}};

uint16 iwht4_coef_vals_col[2][4][4] = { //max elements after col transform
{{8192, 8192, 8192, 8192},
 {8192, 8192, 8192, 8192},
 {8192, 8192, 8192, 8192},
 {8192, 8192, 8192, 8192}},
{{8191, 8191, 8191, 8191},
 {8191, 8191, 8191, 8191},
 {8192, 8192, 8192, 8192},
 {8192, 8192, 8192, 8192}}};

int20 iwht4_high10_coef_vals_col[2][4][4] = {
{{32768, 32768, 32768, 32768},
 {32768, 32768, 32768, 32768},
 {32768, 32768, 32768, 32768},
 {32768, 32768, 32768, 32768}},
{{32767, 32767, 32767, 32767},
 {32767, 32767, 32767, 32767},
 {32768, 32768, 32768, 32768},
 {32768, 32768, 32768, 32768}}};


int20 iwht4_high12_coef_vals_col[2][4][4] = {
{{131072, 131072, 131072, 131072},
 {131072, 131072, 131072, 131072},
 {131072, 131072, 131072, 131072},
 {131072, 131072, 131072, 131072}},
{{131071, 131071, 131071, 131071},
 {131071, 131071, 131071, 131071},
 {131072, 131072, 131072, 131072},
 {131072, 131072, 131072, 131072}}};

uint1 iwht4_sign_vals_col[2][4][4] = {
{{1, 1, 1, 1},
 {1, 1, 1, 1},
 {1, 1, 1, 1},
 {1, 1, 1, 1}},
{{0, 0, 0, 0},
 {0, 0, 0, 0},
 {1, 1, 1, 1},
 {1, 1, 1, 1}}};

enc_choose_itransform_maximizing_coef(peak,c,mxx,myy,encCoefPresetSelect,tzSx,txType){
  tzSx = txsize_sqr_up_map[tzSx]

  if (encCoefPresetSelect % 2 || tzSx == TX_32X32){ // DCT_DCT || ADST_DCT || TX_32X32
    return enc_choose_idct_maximizing_coef(peak,c,mxx,myy,encCoefPresetSelect/2,tzSx)
  } else if (tzSx == TX_4X4 && encCoefPresetSelect > 20) { //WHT
    return enc_choose_iwht_maximizing_coef(peak,c,mxx,myy,(encCoefPresetSelect-20)/2)
  } else {  // ADST_ADST || DCT_ADST
    return enc_choose_iadst_maximizing_coef(peak,c,mxx,myy,encCoefPresetSelect/2,tzSx)
  }
}

enc_choose_iwht_maximizing_coef(peak,c,mxx,myy,encCoefPresetSelect) {
  iwht_number_of_row_vals = 2
  iwht_number_of_col_vals = 2
  iwht_max_index = 4

  iwht_sign = 0
  iwht_val = 0
  if (encCoefPresetSelect >= iwht_number_of_row_vals) {
    iwht_index = (encCoefPresetSelect-iwht_number_of_row_vals)
    if (mxx >= iwht_max_index || myy >= iwht_max_index || iwht_index >= iwht_number_of_col_vals)
      return Choose(0,Max(0,peak))
    if (enc_bit_depth == 10) {
      iwht_val = iwht4_high10_coef_vals_col[iwht_index][myy][mxx]
    } else if (enc_bit_depth == 12) {
      iwht_val = iwht4_high12_coef_vals_col[iwht_index][myy][mxx]
    } else {
      iwht_val = iwht4_coef_vals_col[iwht_index][myy][mxx]
    }
    iwht_sign = iwht4_sign_vals_col[iwht_index][myy][mxx]
  } else {
    if (mxx >= iwht_max_index) {
      return Choose(0,Max(0,peak))
    }
    if (enc_bit_depth == 10) {
      iwht_val = iwht4_high10_coef_vals_row[encCoefPresetSelect][mxx]
    } else if (enc_bit_depth == 12) {
      iwht_val = iwht4_high12_coef_vals_row[encCoefPresetSelect][mxx]
    } else {
      iwht_val = iwht4_coef_vals_row[encCoefPresetSelect][mxx]
    }
    iwht_sign = iwht4_sign_vals_row[encCoefPresetSelect][mxx]
  }
  enc_sign_bit[c] = iwht_sign
  return Max(0,Min(iwht_val, peak))
}

enc_choose_idct_maximizing_coef(peak,c,mxx,myy,encCoefPresetSelect,tzSx) {
  if (tzSx == TX_4X4) {
    idct_number_of_row_vals = 2
    idct_number_of_col_vals = 3
    idct_max_index = 4
  } else if (tzSx == TX_8X8) {
    idct_number_of_row_vals = 12
    idct_number_of_col_vals = 5
    idct_max_index = 8
  } else if (tzSx == TX_16X16) {
    idct_number_of_row_vals = 9
    idct_number_of_col_vals = 6
    idct_max_index = 16
  } else if (tzSx == TX_32X32) {
    idct_number_of_row_vals = 8
    idct_number_of_col_vals = 6
    idct_max_index = 32
  } else {
    return Choose(0,Max(0,peak))
  }
  idct_sign = 0
  idct_val = 0
  if (encCoefPresetSelect >= idct_number_of_row_vals) {
    idct_index = (encCoefPresetSelect-idct_number_of_row_vals)
    if (mxx >= idct_max_index || myy >= idct_max_index || idct_index >= idct_number_of_col_vals)
      return Choose(0,Max(0,peak))
    if (tzSx == TX_4X4) {
      if (enc_bit_depth == 10) {
        idct_val = idct4_high10_coef_vals_col[idct_index][myy][mxx]
      } else if (enc_bit_depth == 12) {
        idct_val = idct4_high12_coef_vals_col[idct_index][myy][mxx]
      } else {
        idct_val = idct4_coef_vals_col[idct_index][myy][mxx]
      }
      idct_sign = idct4_sign_vals_col[idct_index][myy][mxx]
    } else if (tzSx == TX_8X8) {
      if (enc_bit_depth == 10) {
        idct_val = idct8_high10_coef_vals_col[idct_index][myy][mxx]
      } else if (enc_bit_depth == 12) {
        idct_val = idct8_high12_coef_vals_col[idct_index][myy][mxx]
      } else {
        idct_val = idct8_coef_vals_col[idct_index][myy][mxx]
      }
      idct_sign = idct8_sign_vals_col[idct_index][myy][mxx]
    } else if (tzSx == TX_16X16) {
      if (enc_bit_depth == 10) {
        idct_val = idct16_high10_coef_vals_col[idct_index][myy][mxx]
      } else if (enc_bit_depth == 12) {
        idct_val = idct16_high12_coef_vals_col[idct_index][myy][mxx]
      } else {
        idct_val = idct16_coef_vals_col[idct_index][myy][mxx]
      }
      idct_sign = idct16_sign_vals_col[idct_index][myy][mxx]
    } else {
      if (enc_bit_depth == 10) {
        idct_val = idct32_high10_coef_vals_col[idct_index][myy][mxx]
      } else if (enc_bit_depth == 12) {
        idct_val = idct32_high12_coef_vals_col[idct_index][myy][mxx]
      } else {
        idct_val = idct32_coef_vals_col[idct_index][myy][mxx]
      }
      idct_sign = idct32_sign_vals_col[idct_index][myy][mxx]
    }
  } else {
    if (mxx >= idct_max_index) {
      return Choose(0,Max(0,peak))
    }
    if (tzSx == TX_4X4) {
      if (enc_bit_depth == 10) {
        idct_val = idct4_high10_coef_vals_row[encCoefPresetSelect][mxx]
      } else if (enc_bit_depth == 12) {
        idct_val = idct4_high12_coef_vals_row[encCoefPresetSelect][mxx]
      } else {
        idct_val = idct4_coef_vals_row[encCoefPresetSelect][mxx]
      }
      idct_sign = idct4_sign_vals_row[encCoefPresetSelect][mxx]
    } else if (tzSx == TX_8X8) {
      if (enc_bit_depth == 10) {
        idct_val = idct8_high10_coef_vals_row[encCoefPresetSelect][mxx]
      } else if (enc_bit_depth == 12) {
        idct_val = idct8_high12_coef_vals_row[encCoefPresetSelect][mxx]
      } else {
        idct_val = idct8_coef_vals_row[encCoefPresetSelect][mxx]
      }
      idct_sign = idct8_sign_vals_row[encCoefPresetSelect][mxx]
    } else if (tzSx == TX_16X16) {
      if (enc_bit_depth == 10) {
        idct_val  = idct16_high10_coef_vals_row[encCoefPresetSelect][mxx]
      } else if (enc_bit_depth == 12) {
        idct_val  = idct16_high12_coef_vals_row[encCoefPresetSelect][mxx]
      } else {
        idct_val  = idct16_coef_vals_row[encCoefPresetSelect][mxx]
      }
      idct_sign = idct16_sign_vals_row[encCoefPresetSelect][mxx]
    } else {
      if (enc_bit_depth == 10) {
        idct_val  = idct32_high10_coef_vals_row[encCoefPresetSelect][mxx]
      } else if (enc_bit_depth == 12) {
        idct_val  = idct32_high12_coef_vals_row[encCoefPresetSelect][mxx]
      } else {
        idct_val  = idct32_coef_vals_row[encCoefPresetSelect][mxx]
      }
      idct_sign = idct32_sign_vals_row[encCoefPresetSelect][mxx]
    }

  }
  enc_sign_bit[c] = idct_sign
  return Max(0,Min(idct_val, peak))
}

enc_choose_iadst_maximizing_coef(peak,c,mxx,myy,encCoefPresetSelect,tzSx) {
  if (tzSx == TX_4X4) {
    iadst_number_of_row_vals = 2
    iadst_number_of_col_vals = 2
    iadst_max_index = 4
  } else if (tzSx == TX_8X8) {
    iadst_number_of_row_vals = 6
    iadst_number_of_col_vals = 6
    iadst_max_index = 8
  } else if (tzSx == TX_16X16) {
    iadst_number_of_row_vals = 7
    iadst_number_of_col_vals = 6
    iadst_max_index = 16
  } else {
    return Choose(0,Max(0,peak))
  }
  iadst_sign = 0
  iadst_val = 0
  if (encCoefPresetSelect >= iadst_number_of_row_vals) {
    iadst_index = (encCoefPresetSelect-iadst_number_of_row_vals)
    if (mxx >= iadst_max_index || myy >= iadst_max_index || iadst_index >= iadst_number_of_col_vals)
      return Choose(0,Max(0,peak))
    if (tzSx == TX_4X4) {
      if (enc_bit_depth == 10) {
        iadst_val = iadst4_high10_coef_vals_col[iadst_index][myy][mxx]
      } else if (enc_bit_depth == 12) {
        iadst_val = iadst4_high12_coef_vals_col[iadst_index][myy][mxx]
      } else {
        iadst_val = iadst4_coef_vals_col[iadst_index][myy][mxx]
      }
      iadst_sign = iadst4_sign_vals_col[iadst_index][myy][mxx]
    } else if (tzSx == TX_8X8) {
      if (enc_bit_depth == 10) {
        iadst_val = iadst8_high10_coef_vals_col[iadst_index][myy][mxx]
      } else if (enc_bit_depth == 12) {
        iadst_val = iadst8_high12_coef_vals_col[iadst_index][myy][mxx]
      } else {
        iadst_val = iadst8_coef_vals_col[iadst_index][myy][mxx]
      }
      iadst_sign = iadst8_sign_vals_col[iadst_index][myy][mxx]
    } else if (tzSx == TX_16X16) {
      if (enc_bit_depth == 10) {
        iadst_val = iadst16_high10_coef_vals_col[iadst_index][myy][mxx]
      } else if (enc_bit_depth == 12) {
        iadst_val = iadst16_high12_coef_vals_col[iadst_index][myy][mxx]
      } else {
        iadst_val = iadst16_coef_vals_col[iadst_index][myy][mxx]
      }
      iadst_sign = iadst16_sign_vals_col[iadst_index][myy][mxx]
    }
  } else {
    if (mxx >= iadst_max_index) {
      return Choose(0,Max(0,peak))
    }
    if (tzSx == TX_4X4) {
      if (enc_bit_depth == 10) {
        iadst_val = iadst4_high10_coef_vals_row[encCoefPresetSelect][mxx]
      } else if (enc_bit_depth == 12) {
        iadst_val = iadst4_high12_coef_vals_row[encCoefPresetSelect][mxx]
      } else {
        iadst_val = iadst4_coef_vals_row[encCoefPresetSelect][mxx]
      }
      iadst_sign = iadst4_sign_vals_row[encCoefPresetSelect][mxx]
    } else if (tzSx == TX_8X8) {
      if (enc_bit_depth == 10) {
        iadst_val = iadst8_high10_coef_vals_row[encCoefPresetSelect][mxx]
      } else if (enc_bit_depth == 12) {
        iadst_val = iadst8_high12_coef_vals_row[encCoefPresetSelect][mxx]
      } else {
        iadst_val = iadst8_coef_vals_row[encCoefPresetSelect][mxx]
      }
      iadst_sign = iadst8_sign_vals_row[encCoefPresetSelect][mxx]
    } else if (tzSx == TX_16X16) {
      if (enc_bit_depth == 10) {
        iadst_val  = iadst16_high10_coef_vals_row[encCoefPresetSelect][mxx]
      } else if (enc_bit_depth == 12) {
        iadst_val  = iadst16_high12_coef_vals_row[encCoefPresetSelect][mxx]
      } else {
        iadst_val  = iadst16_coef_vals_row[encCoefPresetSelect][mxx]
      }
      iadst_sign = iadst16_sign_vals_row[encCoefPresetSelect][mxx]
    }
  }
  enc_sign_bit[c] = iadst_sign
  return Max(0,Min(iadst_val, peak))
}
#endif // 0

//Custom settings of all combinations of ref_frame_sign_bias according to the index
//Index values: 0 LAST_FRAME, 1 GOLDEN_FRAME, 2 ALTREF_FRAME
enc_choose_ref_frame_sign_bias(frame_i) {
    if (enc_stream_seed % 8 == 0) {
        return 0
    } else if (enc_stream_seed % 8 == 1) {
        return frame_i == 2 ? 1 : 0
    } else if (enc_stream_seed % 8 == 2) {
        return frame_i == 1 ? 1 : 0
    } else if (enc_stream_seed % 8 == 3) {
        return (frame_i == 1 || frame_i == 2) ? 1 : 0
    } else if (enc_stream_seed % 8 == 4) {
        return frame_i == 0 ? 1 : 0
    } else if (enc_stream_seed % 8 == 5) {
        return (frame_i == 0 || frame_i == 2) ? 1 : 0
    } else if (enc_stream_seed % 8 == 6) {
        return (frame_i == 0 || frame_i == 1) ? 1 : 0
    } else {
        return 1
    }
}

// Split the tiles up into tile groups.
// We choose between two extreme cases and a simple
// random-length scheme.
enc_setup_tile_groups() {
  uint32 i

  if (show_existing_frame) {
    num_tile_groups = 0
    return 0
  }

  uint32 total_tiles
  total_tiles = tile_rows * tile_cols

  // Note: The limit of MAX_TILES tiles is an encoder-side-only
  // limit, so can be raised without any issues.
  ASSERT(total_tiles <= MAX_TILES, "Need to increase the value of MAX_TILES")

  // First we figure out how many tile groups we want. LST mode
  // implies just one tile group. Otherwise, if the number is
  // explicitly provided, we use that. Otherwise, we look at
  // enc_tile_group_mode.
  if (large_scale_tile) {
    num_tile_groups = 1
  } else {
    enc_num_tile_groups u(0)

    // Clamp negative values of enc_num_tile_groups to 0 so that we
    // can safely assign to the unsigned num_tile_groups and still
    // have the "num_tile_groups < 1" test below do what it's supposed
    // to.
    num_tile_groups = Max (enc_num_tile_groups, 0)

    CHECK (num_tile_groups <= total_tiles,
           "enc_num_tile_groups is %d, but there are only %d x %d = %d tiles.",
           enc_num_tile_groups, tile_rows, tile_cols, total_tiles)
  }

  if (num_tile_groups < 1) {
    if (enc_tile_group_mode == ENC_TG_MODE_ONE_TG) {
      num_tile_groups = 1
    } else if (enc_tile_group_mode == ENC_TG_MODE_MAX_TG) {
      num_tile_groups = total_tiles
    } else {
      ASSERT (enc_tile_group_mode == ENC_TG_MODE_UNIFORM,
              "Unhandled enc_tile_group_mode: %d.")
      num_tile_groups = ChooseAll (1, total_tiles, NUM_TILE_GROUPS)
    }
  }

  ASSERT (0 <= num_tile_groups && num_tile_groups <= total_tiles,
          "num_tile_groups out of range.")

  // Now we have to decide where to place the tile group starts.
  // Normally, we want to do something helpful and random here, but we
  // need to give control to the configuration language in case the
  // user is really determined.
  enc_specify_tile_groups u(0)
  if (enc_specify_tile_groups) {
    uint32 cur_tg_start
    cur_tg_start = 0
    for (i = 0; i < num_tile_groups; i ++) {
      enc_tg_size u(0)

      CHECK (enc_tg_size > 0,
             "enc_tg_size[%d] = %d, but a tile group must have something in it.",
             i, enc_tg_size)
      CHECK (cur_tg_start + enc_tg_size <= total_tiles,
             "enc_tg_size[%d] = %d, which takes us to %d tiles but num tiles is %d.",
             i, enc_tg_size, cur_tg_start + enc_tg_size, total_tiles)

      tg_starts[i] = cur_tg_start
      tg_sizes[i] = enc_tg_size
      cur_tg_start += enc_tg_size
    }

    CHECK (cur_tg_start == total_tiles,
           "The %d tile groups added up to %d tiles in total; there should be %d.",
           num_tile_groups, cur_tg_start, total_tiles)
    return 0
  }

  // It seems the user isn't feeling too masochistic today and we'll
  // pick the tile group starting positions randomly.

  if (num_tile_groups == total_tiles) {
    // If num_tile_groups equals total_tiles, we special case things,
    // placing each tile in its own tile group.
    for (i = 0; i < num_tile_groups; i++) {
      tg_starts[i] = i
      tg_sizes[i] = 1
    }
    return 0
  }

  // If num_tile_groups < total_tiles, we need to place
  // num_tile_groups-1 tile group boundaries. We use Floyd's
  // algorithm to do this uniformly between all possible choices.
  uint1 tg_boundary_after_tile[MAX_TILES]
  for (i = 0; i < total_tiles - 1; i++) {
    tg_boundary_after_tile[i] = 0
  }
  tg_boundary_after_tile[total_tiles - 1] = 1

  for (i = total_tiles - num_tile_groups; i < total_tiles - 1; i++) {
    idx = Choose(0, i)
    if (tg_boundary_after_tile[idx]) {
      idx = i
    }
    tg_boundary_after_tile[idx] = 1
  }

  // At this point, we've set num_tile_groups "flags" and need to
  // write out tg_starts and tg_sizes.
  uint32 tgIdx
  uint32 totalSize
  tgStart = 0
  tgIdx = 0
  totalSize = 0
  for (i = 0; i < total_tiles; i++) {
    if (tg_boundary_after_tile[i]) {
      tg_starts[tgIdx] = tgStart
      tg_sizes[tgIdx] = (i + 1) - tgStart
      totalSize += tg_sizes[tgIdx]

      tgStart = (i + 1)
      tgIdx += 1
    }
  }
  // Double-check that our generated tile groups are valid
  ASSERT(totalSize == total_tiles, "Wrong number of tiles in enc_setup_tile_groups()")
  ASSERT(tgIdx == num_tile_groups, "Wrong number of tile groups in enc_setup_tile_groups()")
}

enc_more_tile_cols(sbCols, log2TileCols, levelMaxTileCols) {
  if (enc_large_scale_tile && enc_is_license_resolution) {
      return ((1<<log2TileCols) < enc_tile_cols)
  }
  tileWidthSb = ROUND_POWER_OF_TWO_UP(sbCols, log2TileCols)
  if (enc_exactly_one_tile) {
    ASSERT(log2TileCols == 0, "Can't restrict to just one tile")
    return 0
  }
  if (large_scale_tile || enc_tile_group_focus) {
    if (tileWidthSb == enc_tile_width_sb) {
      ASSERT(tileWidthSb <= max_tile_width_sb, "tile_width not less than max")
      return 0
    } else {
      ASSERT(tileWidthSb > enc_tile_width_sb, "tile_width not more than target")
      return 1
    }
  }
  if (use_superres == 1 && enc_use_128x128_superblock == 0) {
    // in this case TileWidth must be >= 128
    nextTileWidthSb = ROUND_POWER_OF_TWO_UP(sbCols, (log2TileCols+1))
    if (nextTileWidthSb < 2) {
      return 0
    }
  }
  if (tileWidthSb >= max_tile_width_sb) {
    return 1
  }
  sbSizeLog2 = mib_size_log2 + MI_SIZE_LOG2
  if ((tileWidthSb << sbSizeLog2) >= (MAX_TILE_WIDTH * SUPERRES_NUM / superres_denom)) {
    return 1
  }
  if ((1 << (log2TileCols+1)) > levelMaxTileCols) {
    return 0
  }
  // check for the crop >= 8 condition
  sbCols = ROUND_POWER_OF_TWO_UP(mi_cols, mib_size_log2)
  lastSbCol = (sbCols / tileWidthSb) * tileWidthSb
  rem = crop_width - ((lastSbCol << mib_size_log2) * MI_SIZE)
  if (rem < 8) {
      return 1
  }
  if (enc_force_max_tile_cols) {
    return 1
  }
  return ChooseBit()
}

enc_more_tile_rows(sbRows, log2TileRows, tileWidthSb, levelMaxTileRows) {
  if (enc_large_scale_tile && enc_is_license_resolution) {
      return ((1<<log2TileRows) < enc_tile_rows)
  }
  tileHeightSb = ROUND_POWER_OF_TWO_UP(sbRows, log2TileRows)
  tileArea = tileWidthSb * tileHeightSb
  if (enc_exactly_one_tile) {
    ASSERT(log2TileRows == 0, "Can't restrict to just one tile")
    return 0
  }
  if (large_scale_tile || enc_tile_group_focus) {
    if (tileHeightSb == enc_tile_height_sb) {
      ASSERT(tileArea <= max_tile_area_sb, "tile_height not comptabile with area")
      return 0
    } else {
      ASSERT(tileHeightSb > enc_tile_height_sb, "tile_height not more than target")
      return 1
    }
  }
  if (tileArea >= max_tile_area_sb) {
    return 1
  }
  if ((1 << (log2TileRows+1)) > levelMaxTileRows) {
    return 0
  }
  // check for the crop >= 8 condition
  sbRows = ROUND_POWER_OF_TWO_UP(mi_rows, mib_size_log2)
  lastSbRow = (sbRows / tileHeightSb) * tileHeightSb
  rem = crop_height - ((lastSbRow << mib_size_log2) * MI_SIZE)
  if (rem < 8) {
      return 1
  }
  if (enc_force_max_tile_rows) {
    return 1
  }
  return ChooseBit()
}

enc_pick_nonuniform_tile_sizes(level) {
  sb_cols = ROUND_POWER_OF_TWO_UP(mi_cols, mib_size_log2)
  sb_rows = ROUND_POWER_OF_TWO_UP(mi_rows, mib_size_log2)

  sbSizeLog2 = mib_size_log2 + MI_SIZE_LOG2
  maxTileWidthSb = (MAX_TILE_WIDTH * SUPERRES_NUM / superres_denom) >> sbSizeLog2
  minTileWidthSb = 1
  if (use_superres == 1 && enc_use_128x128_superblock == 0) {
    // Tiles other than the right-most tile must be at least 128
    // pixels wide (this is a bit-stream conformance requirement in
    // A.3 "Levels")
    minTileWidthSb = 2
  }
  maxTileAreaSb = (MAX_TILE_AREA >> (2 * sbSizeLog2))

  minLog2TileCols = tile_log2(maxTileWidthSb, sb_cols)
  minLog2Tiles = Max(minLog2TileCols, tile_log2(maxTileAreaSb, sb_rows * sb_cols))

  level = enc_layers_to_min_level[spatial_layer][temporal_layer]
  levelMaxTiles = level_max_tiles[compact_level[level]][0]
  levelMaxTileCols = level_max_tiles[compact_level[level]][1]
  ASSERT(levelMaxTileCols <= MAX_TILE_COLS, "This should always be true")

  // Select from the valid range of tile sizes, with extra emphasis
  // applied to the smallest and largest sizes possible
  minTileCols = (sb_cols + (maxTileWidthSb-1)) / maxTileWidthSb
  maxTileCols = Min(sb_cols, levelMaxTileCols)
  maxTileCols = Min(maxTileCols, Max(1, (sb_cols / minTileWidthSb)))

  z = Choose(0, 99)
  if (enc_force_max_tile_cols || z < 5) {
    tileCols = maxTileCols
  } else if (z < 35) {
    tileCols = minTileCols
  } else {
    tileCols = Choose(minTileCols, maxTileCols)
  }

  // We want to generate a reasonably uniform distribution of tile sizes,
  // while respecting the max tile width constraint. To do this, we use
  // a relatively simple algorithm:
  // * Initialize all tile widths to (sb_cols / tile_cols) superblocks wide
  //   (with some adjustment for rounding)
  // * Run a configurable number of steps where we:
  //   * Randomly select two indices i and j
  //   * If tile col i has width > 1 and tile col j has width < maxTileWidthSb,
  //     then move one sb of width from i to j.
  //
  // This process will generate tile widths with the following properties:
  // * The tile width distribution is independent of *which* tile column this is
  //   (whereas it will depend on this for a lot of the more obvious algorithms)
  // * The max tile width constraint is always satisfied
  // * By changing the number of iterations performed, we can tune the width distribution:
  //   Low step count => The tiles will be roughly evenly spaced horizontally
  //   High step count => The tiles widths will be more randomly distributed,
  //                      helping to exercise cases like where there's one super wide
  //                      tile and a few narrower ones
  startSb = 0
  for (i = 0; i < tileCols; i++) {
    endSb = (sb_cols * (i+1)) / tileCols
    enc_nonuniform_tile_width[i] = endSb - startSb
    startSb = endSb
  }

  stepCount = 200
  for (step = 0; step < stepCount; step++) {
    i = Choose(0, tileCols - 1)
    j = Choose(0, tileCols - 1)
    if (enc_nonuniform_tile_width[i] > minTileWidthSb && enc_nonuniform_tile_width[j] < maxTileWidthSb) {
      enc_nonuniform_tile_width[i] -= 1
      enc_nonuniform_tile_width[j] += 1
    }
  }

  widest_tile_sb = 0
  for (i = 0; i < tileCols; i++) {
    widest_tile_sb = Max(widest_tile_sb, enc_nonuniform_tile_width[i])
  }

  // Now repeat the process for tile rows
  levelMaxTileRows = Min(levelMaxTiles / tileCols, MAX_TILE_ROWS)

  if (minLog2Tiles)
    maxTileAreaSb = (sb_rows * sb_cols) >> (minLog2Tiles + 1)
  else
    maxTileAreaSb = sb_rows * sb_cols
  max_tile_height_sb = Max(maxTileAreaSb / widest_tile_sb, 1)

  minTileRows = (sb_rows + (max_tile_height_sb-1)) / max_tile_height_sb
  maxTileRows = Min(sb_rows, levelMaxTileRows)
  z = Choose(0, 99)
  if (enc_force_max_tile_rows || z < 5) {
    tileRows = maxTileRows
  } else if (z < 35) {
    tileRows = minTileRows
  } else {
    tileRows = Choose(minTileRows, maxTileRows)
  }

  startSb = 0
  for (i = 0; i < tileRows; i++) {
    endSb = (sb_rows * (i+1)) / tileRows
    enc_nonuniform_tile_height[i] = endSb - startSb
    startSb = endSb
  }

  stepCount = 200
  for (step = 0; step < stepCount; step++) {
    i = Choose(0, tileRows - 1)
    j = Choose(0, tileRows - 1)
    if (enc_nonuniform_tile_height[i] > 1 && enc_nonuniform_tile_height[j] < max_tile_height_sb) {
      enc_nonuniform_tile_height[i] -= 1
      enc_nonuniform_tile_height[j] += 1
    }
  }
}

enc_choose_film_grain_params() {
  if (frame_type != INTER_FRAME) {
    enc_update_params = 1
    return 0
  }

  found = 0
  numIdcs = 0
  for (i=0; i<NUM_REF_FRAMES; i++) {
    refFound = 0
    for (j=0; j<INTER_REFS_PER_FRAME; j++) {
      if (active_ref_idx[j] == i) {
        refFound = 1
      }
    }
    if (refFound && ref_film_grain_params_present[i]) {
      enc_compatible_refs[numIdcs] = i
      numIdcs += 1
    }
  }

  if (numIdcs == 0) {
    // We couldn't find reference frame with film grain parameters, so must
    // force update_params to be 1.
    enc_update_params = 1
    return 0
  }

  // There are numIdcs reference frames with valid film grain parameters. Do we
  // want to send some new parameters anyway?
  enc_update_params u(0)
  if (enc_update_params) {
    return 0
  }

  // We want to copy from another frame.
  enc_film_grain_params_ref_idx u(0)
  CHECK (0 <= enc_film_grain_params_ref_idx &&
         enc_film_grain_params_ref_idx < NUM_REF_FRAMES,
         "enc_film_grain_params_ref_idx is %d, not in [0, %d].",
         enc_film_grain_params_ref_idx, NUM_REF_FRAMES)
}

// Not all metadata types can be encoded in any position in the stream:
// * METADATA_TYPE_SCALABILITY should be immediately after the sequence header.
// The remaining values aren't contiguous, so we remap them to a contiguous
// range here.
#define NUM_ENCODED_METADATA_TYPES 4

uint8 enc_remap_metadata_type[NUM_ENCODED_METADATA_TYPES] = {
  METADATA_TYPE_HDR_CLL,
  METADATA_TYPE_HDR_MDCV,
  METADATA_TYPE_ITUT_T35,
  METADATA_TYPE_TIMECODE
};

uint64 enc_select_metadata_type() {
  // Note: At the point where this function is used, we can't encode a SCALABILITY metadata OBU
  // because those need to be immediately after the sequence header. We also treat RESERVED_0
  // specially. This leaves NUM_METADATA_TYPES - 2 other types to choose from.
  ASSERT(NUM_METADATA_TYPES - 2 == NUM_ENCODED_METADATA_TYPES, "Need to add new type to enc_remap_metadata_type")
  prob = Choose(0,99)
  if (prob < 25) { // 25% of the time choose one of the above types
    return METADATA_TYPE_TIMECODE
  } else if (prob < 80) { // 30% of the time choose one of the above types
    return enc_remap_metadata_type[Choose(0, NUM_ENCODED_METADATA_TYPES-1)]
  } else (prob < 85) { // 5% of the time choose the 0 reserved type
    return METADATA_TYPE_AOM_RESERVED_0
  }
  // the rest of the time choose one of the unregistered prviate or other reserved types
  uint64 value
  // While 'value' is LEB128 encoded, it is limited to the range of [0, 2^32 - 1].
  // Discussion: https://bugs.chromium.org/p/aomedia/issues/detail?id=1891
  value = ChooseBits(32)
  if (value <= METADATA_TYPE_MAX) {
    value = METADATA_TYPE_MAX + Choose(1,255) // some random offset off the bottom
    if (value == METADATA_TYPE_ARGON_DESIGN_LARGE_SCALE_TILE) { // except this one!
      value += 1
    }
  }
  return value
}

enc_pick_operating_point_level(oppoint) {
  // For streams which exceed level 6.3, always set the encoder-side level
  // to "max parameters"
  if (enc_huge_picture) {
    enc_operating_point_level[oppoint] = LEVEL_MAX_PARAMS
    return 0
  }

  // Work out the maximum width, height, and #pixels for this operating point
  if (enc_use_scalability) {
    maxW = 0
    maxH = 0
    maxPx = 0
    for (i = 0; i < enc_spatial_layers; i++) {
      if (enc_spatial_layers_used[oppoint] & (1 << i)) {
        w = spatial_layer_max_width[i]
        h = spatial_layer_max_height[i]
        maxW = Max(maxW, w)
        maxH = Max(maxH, h)
        maxPx = Max(maxPx, w * h)
      }
    }
  } else {
    maxW = enc_stream_width
    maxH = enc_stream_height
    maxPx = maxW * maxH
  }

  int64 dispRate
  int64 decRate
  dispRate = -1
  decRate = -1
  if (enc_decoder_model_mode != DECODER_MODEL_DISABLED) {
    dispRate = maxPx * enc_frame_rate
    denom = (100 - enc_hide_frame_prob)
    decRate = (maxPx * enc_frame_rate * 100 + (denom-1)) / denom
  }

  // Find the lowest level which we can use
  // Note: Here, 'level' is in our special compacted format
  //for (level = 0; level < NUM_DEFINED_LEVELS; level++) {
  //  allowed = level_max_pic_size[level][0] >= maxPx &&
  //            level_max_pic_size[level][1] >= maxW &&
  //            level_max_pic_size[level][2] >= maxH
  //  if (enc_decoder_model_mode != DECODER_MODEL_DISABLED) {
  //    allowed = allowed &&
  //              level_max_luma_rate[level][0] >= dispRate &&
  //              level_max_luma_rate[level][1] >= decRate
  //  }
  //  if (enc_large_scale_tile || enc_tile_group_focus) {
  //    allowed = allowed &&
  //              level_max_tiles[level][1] >= enc_tile_cols &&
  //              level_max_tiles[level][0] >= (enc_tile_cols * enc_tile_rows)
  //  }
  //
  //  if (allowed) {
  //    break
  //  }
  //}

  // The ASSERT at the end of this function (that level == enc_base_level) fails if we
  // deliberately choose a small frame size (so that we could be a much lower level),
  // as the loop commented out above will always pick the smallest level.
  level = enc_base_level

  CHECK (maxPx <= level_max_pic_size[level][0],
         "Stream size of %ld is too big for level %d (max: %ld)",
         maxPx, level, level_max_pic_size[level][0])
  CHECK (maxW <= level_max_pic_size[level][1],
         "Stream width of %d is too big for level %d (max: %ld)",
         maxW, level, level_max_pic_size[level][1])
  CHECK (maxH <= level_max_pic_size[level][1],
         "Stream height of %d is too big for level %d (max: %ld)",
         maxH, level, level_max_pic_size[level][2])

  allowed = level_max_pic_size[level][0] >= maxPx &&
            level_max_pic_size[level][1] >= maxW &&
            level_max_pic_size[level][2] >= maxH

  if (enc_decoder_model_mode != DECODER_MODEL_DISABLED) {
    CHECK (dispRate <= level_max_luma_rate[level][0],
           "Stream display rate of %ld is too big for level %d (max: %ld)",
           dispRate, level, level_max_luma_rate[level][0])
    CHECK (decRate <= level_max_luma_rate[level][1],
           "[DECODE_RATE] Stream decode rate of %ld is too big for level %d (max: %ld) (maxPx = %ld; frame rate = %d; enc_hide_frame_prob = %d)",
           decRate, level, level_max_luma_rate[level][1],
           maxPx, enc_frame_rate, enc_hide_frame_prob)

    allowed = allowed &&
              level_max_luma_rate[level][0] >= dispRate &&
              level_max_luma_rate[level][1] >= decRate
  }

  if (enc_large_scale_tile || enc_tile_group_focus) {
    CHECK (enc_tile_cols <= level_max_tiles[level][1],
           "Stream's enc_tile_cols of %d is too big for level %d (max: %ld)",
           enc_tile_cols, level, level_max_tiles[level][1])
    CHECK (enc_tile_cols * enc_tile_rows <= level_max_tiles[level][0],
           "Stream's number of tiles of %d is too big for level %d (max: %ld)",
           enc_tile_cols * enc_tile_rows, level, level_max_tiles[level][0])

    allowed = allowed &&
              level_max_tiles[level][1] >= enc_tile_cols &&
              level_max_tiles[level][0] >= (enc_tile_cols * enc_tile_rows)
  }

  ASSERT(allowed, "Selected level failed checks in enc_pick_operating_point_level()")

  if (oppoint == 0) {
    CHECK(enc_min_level <= level && level <= enc_max_level, "Stream didn't fit expected level range. Level was %d, expected [%d -> %d]",level,enc_min_level,enc_max_level)
    if (! enc_large_scale_tile) {
      // In this case, we selected the level very early, so double-check
      // that we've generated the correct level
      ASSERT(level == enc_base_level, "Bad level setup")
    }
  }

  enc_operating_point_level[oppoint] = expand_level[level]
}

uint8 enc_preset_framerates[4][3] = {
  { 24,  25,  30},
  { 48,  50,  60},
  {100, 120, 120},
  {100, 120, 120}, // Same as speed group 2, but we use more hidden frames
};

enc_pick_timing_info() {
  if (enc_decoder_model_mode == DECODER_MODEL_DISABLED) {
    return 0
  }

  // Pick the rates of the various clocks
  enc_num_units_in_display_tick  u(0)
  enc_num_ticks_per_picture      u(0)
  enc_num_units_in_decoding_tick u(0)
}

enc_finalise_operating_points() {
  for (i=0; i<enc_num_operating_points; i++) {
    enc_pick_operating_point_level(i)

    // Finally, update the mappings of layers => operating points
    // and layers => minimum level
    for (temporalLayer = 0; temporalLayer <= enc_temporal_layers-1; temporalLayer++) {
      if (enc_temporal_layers_used[i] & (1 << temporalLayer)) {
        for (spatialLayer = 0; spatialLayer <= enc_spatial_layers-1; spatialLayer++) {
          if (enc_spatial_layers_used[i] & (1 << spatialLayer)) {
            enc_layers_to_operating_points[spatialLayer][temporalLayer] |= (1 << i)

            level = enc_operating_point_level[i]
            enc_layers_to_min_level[spatialLayer][temporalLayer] =
            Min(enc_layers_to_min_level[spatialLayer][temporalLayer], level)
          }
        }
      }
    }
  }
}

// Generate the maximal set of operating points - ie, one operating point per
// combination of {max spatial layer, max temporal layer}. Each operating point
// contains every spatial and temporal layer which can be (transitively) referenced
// by frames in the selected maximum spatial/temporal layer.
//
// The operating points should be generated in preference order (with the best option first).
// We decide to sort so that temporal scalability is considered "more preferred" than spatial
// scalability, but this is an arbitrary choice.
enc_generate_operating_points() {
  enc_pick_timing_info()

  // Calculate the transitive closure of enc_spatial_ref_allowed[].
  // This will make determining the list of operating points easier
  uint8 transitive_spatial_ref[MAX_NUM_SPATIAL_LAYERS]
  for (i = 0; i < enc_spatial_layers; i++) {
    transitive_spatial_ref[i] = (1 << i) // Every layer can reference itself
    for (j = 0; j < i; j++) {
      if (enc_spatial_ref_allowed[i] & (1 << j)) {
        // If layer 'i' can reference layer 'j', then layer 'i' can transitively
        // reference anything which layer 'j' can transitively reference.
        // Note that this includes layer 'j', so we don't need to explicitly add that in
        transitive_spatial_ref[i] |= transitive_spatial_ref[j]
      }
    }
  }

  // And the same for temporal references
  uint8 transitive_temporal_ref[MAX_NUM_TEMPORAL_LAYERS]
  for (i = 0; i < enc_temporal_layers; i++) {
    transitive_temporal_ref[i] = (1 << i)
    for (j = 0; j < i; j++) {
      if (enc_temporal_ref_allowed[i] & (1 << j)) {
        transitive_temporal_ref[i] |= transitive_temporal_ref[j]
      }
    }
  }

  i = 0
  for (max_temporal_layer = enc_temporal_layers-1; max_temporal_layer >= 0; max_temporal_layer -= 1) {
    for (max_spatial_layer = enc_spatial_layers-1; max_spatial_layer >= 0; max_spatial_layer -= 1) {
      // Include all spatial/temporal layers which can be transitively referenced by the
      // selected maximum spatial/temporal layer.
      enc_spatial_layers_used[i] = transitive_spatial_ref[max_spatial_layer]
      enc_temporal_layers_used[i] = transitive_temporal_ref[max_temporal_layer]
      i += 1
    }
  }
  ASSERT(i >= 1, "Didn't generate any operating points")
  ASSERT(i <= MAX_NUM_OPERATING_POINTS, "Generated too many operating points")
  enc_num_operating_points = i

  enc_finalise_operating_points()
}

// Set up the appropriate information for when we are not using scalability
enc_no_scalability() {
  // This requires a single operating point, with a special format: We must set all
  // of the spatial and temporal layer flags to 0. This actually means to decode
  // everything.
  enc_spatial_layers = 1
  enc_temporal_layers = 1
  enc_spatial_layers_used[0] = 0
  enc_temporal_layers_used[0] = 0
  enc_num_operating_points = 1
  spatial_layer_max_width[0] = enc_stream_width
  spatial_layer_max_height[0] = enc_stream_height
  enc_layers_to_operating_points[0][0] = 1
  enc_pick_timing_info()
  enc_pick_operating_point_level(0)
  enc_layers_to_min_level[0][0] = enc_operating_point_level[0]

  // special case to send operating point idc of non zero in non
  enc_special_operating_points = 0
  if (enc_decoder_model_mode == DECODER_MODEL_DISABLED) {
    enc_special_operating_points u(0)
    if (enc_special_operating_points) {
      enc_spatial_layers_used[0] = Choose(1,(1<<4)-1) | 1
      enc_temporal_layers_used[0] = Choose(1,(1<<8)-1) | 1
      enc_special_operating_points = 1
    }
  }
}

// Select scalability information (mode, #operating points, etc.)
// and set up some information to help drive the scheduler
enc_pick_scalability() {
  enc_special_operating_points = 0
  if (enc_still_picture || enc_large_scale_tile) {
    enc_use_scalability = 0
  } else {
    enc_use_scalability u(0)
  }

  // Clear the maps of (spatial layer, temporal layer) -> [operating points] and
  // (spatial layer, temporal layer) -> minimum level
  for (i = 0; i < MAX_NUM_SPATIAL_LAYERS; i++) {
    for (j = 0; j < MAX_NUM_TEMPORAL_LAYERS; j++) {
      enc_layers_to_operating_points[i][j] = 0
      enc_layers_to_min_level[i][j] = LEVEL_MAX_PARAMS
    }
  }

  temporal_group_description_present = 0
  if (enc_use_scalability) {
    enc_use_scalability_preset u(0)

    uint32 minWidth
    uint32 minHeight
    uint32 newWidth
    uint32 newHeight
    minWidth = Max(enc_frame_min_width_limit, (enc_stream_width+31)/32)
    minHeight = Max(enc_frame_min_height_limit, (enc_stream_height+31)/32)

    if (enc_use_scalability_preset) {
      enc_scalability_preset = SCALABILITY_SS
      for (i = 0; i < 10; i ++) {
        enc_scalability_preset u(0)
        CHECK ((enc_scalability_preset >= SCALABILITY_L1T2) &&
               (enc_scalability_preset <= SCALABILITY_L4T7_KEY_SHIFT),
               "Invalid enc_scalability_preset (%d).",
               enc_scalability_preset)

        if (enc_scalability_preset != SCALABILITY_SS)
          break
      }
      CHECK (enc_scalability_preset != SCALABILITY_SS,
             "enc_scalability_preset evaluated to SCALABILITY_SS too many times.")

      enc_scalability_mode = enc_scalability_preset
      enc_spatial_layers = preset_spatial_layers[enc_scalability_mode]

      // Each preset mode comes with a fixed ratio of sizes between the two spatial
      // layers (or only has one layer). This may not always be possible - eg, when
      // we are forced to use a particular frame size as in profile_license_resolutions_*.
      // So we need to check if we can use this preset; if not, we fall back to no scalability
      num = preset_resolution_ratio[enc_scalability_mode][0]
      denom = preset_resolution_ratio[enc_scalability_mode][1]

      canUsePreset = 1
      if (!enc_allow_scaling) {
        // If all frames must be the same size, we can only use temporal scalability
        // presets
        canUsePreset = (num == 1 && denom == 1)
      }

      if ((enc_spatial_layers  -bottom_two_spatial_layers_same_dimension[enc_scalability_mode]) > 1) {
        newWidth = enc_stream_width
        newHeight = enc_stream_height
        for (i = (enc_spatial_layers - bottom_two_spatial_layers_same_dimension[enc_scalability_mode] - 2); i >= 0; i -= 1) {
          newWidth = (newWidth * denom + (num/2)) / num
          newHeight = (newHeight * denom + (num/2)) / num
          if (newWidth < minWidth ||
              newHeight < minHeight ||
              !can_meet_crop_tile_requirement(newWidth) ||
              !can_meet_crop_tile_requirement(newHeight)) {
            canUsePreset = 0
            break
          }
        }
      }

      if (! canUsePreset) {
        // Fall back to not using scalability at all
        enc_use_scalability = 0
        enc_no_scalability()
        return 0
      }

      // Fill in the inferred size limits for each layer.
      // The highest layer is of the maximum selected size, and each layer
      // is (num/denom) times the layer below on each side
      maxW = enc_stream_width
      maxH = enc_stream_height
      for (i = enc_spatial_layers-1; i >= 0; i -= 1) {
        spatial_layer_max_width[i] = maxW
        spatial_layer_max_height[i] = maxH

        if (bottom_two_spatial_layers_same_dimension[enc_scalability_mode] == 0 ||
            i>0) {
          maxW = (maxW * denom + (num/2)) / num
          maxH = (maxH * denom + (num/2)) / num
        }
      }

      enc_temporal_layers = preset_temporal_layers[enc_scalability_mode]

      // Fill in the table of which spatial layers can reference which other spatial layers
      for (i = 0; i < enc_spatial_layers; i++) {
        if (preset_inter_layer_dependency[enc_scalability_mode]) {
          // Set all bits up to and including bit 'i'
          enc_spatial_ref_allowed[i] = (1 << (i+1)) - 1
        } else {
          // Set *only* bit 'i'
          enc_spatial_ref_allowed[i] = (1 << i)
        }
      }

      // And for the temporal layers - note that there is no equivalent of
      // the 'inter_layer_dependency' flag, and all temporal layers can always
      // reference any lower temporal layer
      for (i = 0; i < enc_temporal_layers; i++) {
        enc_temporal_ref_allowed[i] = (1 << (i+1)) - 1
      }

      // Set up the fixed temporal unit structure
      temporal_group_size = 1 << (enc_temporal_layers-1)
      if (enc_scalability_mode < SCALABILITY_L3T2_KEY) { // the others are handled in enc_schedule_special_scalability()
        for (i = 0; i < temporal_group_size; i++) {
          // Each temporal layer has exactly double the framerate of the next
          // layer below. Eg, for 3 temporal layers, the pattern is:
          // 0, 2, 1, 2, 0, 2, 1, 2, ...
          temporal_group_temporal_id[i] = Max(0, enc_temporal_layers - 1 - ctz(i))
          temporal_group_temporal_switching_up_point_flag[i] = preset_temporal_switching_up_points[enc_temporal_layers-1][i]
          temporal_group_spatial_switching_up_point_flag[i] = 0
          temporal_group_ref_cnt[i] = 1
          temporal_group_ref_pic_diff[i][0] = preset_temporal_ref_pic_diff[enc_temporal_layers-1][i]
        }

        enc_generate_operating_points()
      }
    } else {
      // Using a custom scalability structure
      enc_scalability_mode = SCALABILITY_SS

      enc_spatial_layers_count  u(0)
      enc_temporal_layers_count u(0)

      enc_spatial_layers = enc_spatial_layers_count
      enc_temporal_layers = enc_temporal_layers_count

      // Decide what sections of the "scalability structure" metadata we're going to use
      spatial_layer_dimensions_present = 0
      if (enc_allow_scaling) {
        enc_spatial_layer_dimensions_present_flag u(0)
        spatial_layer_dimensions_present = enc_spatial_layer_dimensions_present_flag
      }

      if (spatial_layer_dimensions_present) {
        enc_specify_spatial_layer_sizes u(0)
        if (enc_specify_spatial_layer_sizes) {
          for (i = 0; i < enc_spatial_layers; i++) {
            enc_spatial_layer_max_width u(0)
            spatial_layer_max_width[i] = enc_spatial_layer_max_width
            enc_spatial_layer_max_height u(0)
            spatial_layer_max_height[i] = enc_spatial_layer_max_height
          }
        } else {
          maxW = enc_stream_width
          maxH = enc_stream_height
          for (i = enc_spatial_layers-1; i >= 0; i -= 1) {
            spatial_layer_max_width[i] = maxW
            spatial_layer_max_height[i] = maxH

            // Pick a random size ratio (between 1:1 and 2:1) between each layer
            // and the next
            maxW = Choose(Max(minWidth, (maxW+1)/2), maxW)
            maxH = Choose(Max(minHeight, (maxH+1)/2), maxH)

            // rewind if we've gone too far
            if (!can_meet_crop_tile_requirement(maxW) ||
                !can_meet_crop_tile_requirement(maxH)) {
              enc_spatial_layers -= i
              for (j = 0; j<enc_spatial_layers; j++) {
                spatial_layer_max_width[j] = spatial_layer_max_width[j + i]
                spatial_layer_max_height[j] = spatial_layer_max_height[j + i]
              }
              break
            }
          }
        }
      } else {
        // Set all layers to be the same size
        for (i = enc_spatial_layers-1; i >= 0; i -= 1) {
          spatial_layer_max_width[i] = enc_stream_width
          spatial_layer_max_height[i] = enc_stream_height
        }
      }

      enc_spatial_layer_description_present_flag u(0)
      spatial_layer_description_present = enc_spatial_layer_description_present_flag
      if (spatial_layer_description_present) {
        // Each spatial layer can only reference at most one other spatial layer.
        for (i = 0; i < enc_spatial_layers; i++) {
          // The default behaviour of enc_spatial_layer_ref_id is to
          // be either 255 (no dependency) or the index of a layer
          // less than i.
          enc_spatial_layer_ref_id u(0)
          spatial_layer_ref_id[i] = enc_spatial_layer_ref_id

          // Every layer can reference itself, and possibly another named layer.
          enc_spatial_ref_allowed[i] = 1 << i
          if (spatial_layer_ref_id [i] != 255) {
            enc_spatial_ref_allowed[i] |= 1 << spatial_layer_ref_id [i]
          }
        }
      } else {
        // Otherwise, we can select the allowed dependencies randomly,
        // as long as we follow the two main constraints above.
        enc_spatial_ref_allowed[0] = (1<<0)
        for (i = 1; i < enc_spatial_layers; i++) {
          enc_spatial_ref_allowed[i] = (1<<i) | ChooseBits(i-1)
        }
      }

      // Temporal layers work exactly like spatial layers do when we don't use
      // a spatial layer description
      enc_temporal_ref_allowed[0] = 1
      for (i = 1; i < enc_temporal_layers; i++) {
        enc_temporal_ref_allowed[i] u(0)

        mask = (1 << i) | ((1 << i) - 1)
        CHECK (enc_temporal_ref_allowed[i] == (enc_temporal_ref_allowed[i] & mask),
               "enc_temporal_ref_allowed[%d] is 0x%x. Bits greater than %d must not be set.",
               i, enc_temporal_ref_allowed[i], i)

        enc_temporal_ref_allowed[i] |= 1 << i
      }

      enc_temporal_group_description_present u(0)
      temporal_group_description_present = enc_temporal_group_description_present
      if (temporal_group_description_present) {
        // For now, generate something very similar to the preset modes.
        // We do this in two passes so that we can set temporal_group_ref_pic_diff
        // correctly:
        // i) We generate the temporal layer and switching-up point flags for each
        //    temporal unit in the temporal group
        // ii) Then we work out how many temporal references each temporal unit
        //     is allowed while respecting the switching-up point flags (wrapping
        //     around the temporal group is possible, which is why we need the full
        //     group generated first). Then we pick temporal_group_ref_cnt based on this.
        temporal_group_size = 1 << (enc_temporal_layers-1)
        for (i = 0; i < temporal_group_size; i++) {
          temporal_group_temporal_id[i] = Max(0, enc_temporal_layers - 1 - ctz(i))
          enc_temporal_group_temporal_switching_up_point_flag u(0)
          enc_temporal_group_spatial_switching_up_point_flag u(0)

          temporal_group_temporal_switching_up_point_flag[i] =
            enc_temporal_group_temporal_switching_up_point_flag

          temporal_group_spatial_switching_up_point_flag[i] =
            enc_temporal_group_spatial_switching_up_point_flag
        }

        for (i = 0; i < temporal_group_size; i++) {
          // Iterate backward through the temporal group, wrapping around
          // when we reach the start. Count how many temporal references
          // we can use.
          uint8 allowedRefs[256]
          numAllowedRefs = 0
          srcLayer = temporal_group_temporal_id[i]
          // Track the highest temporal layer we're allowed to reference
          // this far back. This starts out as the same temporal layer we're
          // currently in, but may be lowered if we see a temporal switching-up point.
          maxReferenceableLayer = srcLayer
          // Also remember the location of the latest spatial switching up point,
          // if any. Note in particular that the current frame might be a spatial
          // switching up point.
          maxEnhancementRefDist = temporal_group_spatial_switching_up_point_flag[i] ? 0 : -1
          for (offset = 1; offset < 256; offset++) {
            j = mod(i - offset, temporal_group_size)
            refLayer = temporal_group_temporal_id[j]
            if ((enc_temporal_ref_allowed[srcLayer] & (1 << refLayer)) &&
                (refLayer <= maxReferenceableLayer)) {
              allowedRefs[numAllowedRefs] = offset
              numAllowedRefs += 1
            }
            if (temporal_group_temporal_switching_up_point_flag[j]) {
              maxReferenceableLayer = Min(maxReferenceableLayer, refLayer)
            }
            if (maxEnhancementRefDist == -1 && temporal_group_spatial_switching_up_point_flag[j]) {
              maxEnhancementRefDist = offset
            }
          }
          // Pick how many refs to use, and select them randomly from
          // the allowed list, allowing repeats
          if (numAllowedRefs == 0) {
            temporal_group_ref_cnt[i] = 0
          } else {
            enc_temporal_group_ref_cnt u(0)
            temporal_group_ref_cnt[i] = enc_temporal_group_ref_cnt
          }
          for (j = 0; j < temporal_group_ref_cnt[i]; j++) {
            temporal_group_ref_pic_diff[i][j] = allowedRefs[Choose(0, numAllowedRefs-1)]
          }
          enc_max_enhancement_ref_dist[i] = (maxEnhancementRefDist == -1) ? ENC_MAX_FRAMES : maxEnhancementRefDist
        }
      }

      enc_generate_operating_points()
    }
  } else {
    enc_no_scalability()
  }
}

enc_pick_timestamp_flags() {
  prob = Choose(0, 99)
  if (prob<20) {
    enc_full_timestamp_flag = 1
  } else if (prob<40) {
    enc_full_timestamp_flag = 0
    enc_seconds_flag = 0
  } else if (prob<60) {
    enc_full_timestamp_flag = 0
    enc_seconds_flag = 1
    enc_minutes_flag = 0
  } else if (prob<80) {
    enc_full_timestamp_flag = 0
    enc_seconds_flag = 1
    enc_minutes_flag = 1
    enc_hours_flag = 0
  } else {
    enc_full_timestamp_flag = 0
    enc_seconds_flag = 1
    enc_minutes_flag = 1
    enc_hours_flag = 1
  }
}

#endif // ENCODE

// DON'T PUT NEW CODE HERE! Put it inside the #if

empty() {
}

// DON'T PUT NEW CODE HERE! Put it inside the #if
