#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

set -e
set -o pipefail

if [ $# != 3 ]; then
    echo >&2 "Usage: check-run.sh <obu-dir> <cfg-dir> <cfg-triple>"
    exit 1
fi

obu_dir="$1"
cfg_dir="$2"
cfg_triple="$3"
shift 3

if [ x"$cfg_dir" = x. ]; then
    cfg_head=""
else
    cfg_head="$cfg_dir/"
fi

# Split cfg_triple into its 3 parts
cfg_nf="$(echo "$cfg_triple" | awk -F '+' '{ print NF }')"
if [ x"$cfg_nf" != x3 ]; then
    echo >&2 "Error config triple \`${cfg_triple}' doesn't have 3 parts."
    exit 1
fi
cfg_file="${cfg_head}$(echo "$cfg_triple" | awk -F '+' '{ print $1 }').cfg"
config="$(echo "$cfg_triple" | awk -F '+' '{ print $2 }')"
seed="$(echo "$cfg_triple" | awk -F '+' '{ print $3 }')"

# The basename of the config file combines with the config itself to
# give the OBU and STDOUT files that we should be able to look at.
out_base="$(basename "${cfg_file}" .cfg)+${config}+${seed}"
obu="${obu_dir}/${out_base}.obu"
stdout="${obu_dir}/${out_base}.stdout"

if [ ! -e "$obu" ]; then
    echo >&2 "No OBU file at \`$obu'."
    exit 1
fi
if [ ! -e "$stdout" ]; then
    echo >&2 "No captured stdout at \`$stdout'."
    exit 1
fi

# If the encode failed some known check, the obu file will be empty
# (see run-encoder.sh). In which case, we just ignore this seed.
if [ ! -s "$obu" ]; then
    echo "Encode failed a check. Ignoring this seed."
    exit 0
fi

# Make ourselves a temporary working directory in TMPDIR
work_dir=""
cleanup () {
    if [ x"$work_dir" != x ]; then
        rm -rf "$work_dir"
    fi
}
trap cleanup EXIT
work_dir="$(mktemp -d --tmpdir av1-check-run.XXXXXXXXXXX)"

# The encoder run had "debugging" enabled for all overridden
# configuration elements, which means that we get them all printed
# out. Use sed and sort to get the exact list that was generated.
sed -En 's!^([a-zA-Z_0-9-]+) = (-?[0-9]+)$!\1!p' "$stdout" | \
    sort -u -o "${work_dir}/seen"

# Check that this file is nonempty (if there's nothing in the list,
# we've probably done something wrong).
test -s "${work_dir}/seen" || {
    echo >&2 "No elements reported in $stdout."
    exit 1
}

# Now try to get the list of elements that we expected to see, using
# awk to grab the assignments in the config block.
grep -v '<IGNORED>' "$cfg_file" | \
  awk '/^'"${config}"' {/ { show=1 } \
       /^\s*[a-zA-Z_0-9-]+\s*=/ { if (show) { print $1; } } \
       /^}/ { show=0; }' | sort -u -o "${work_dir}/exp"

# We saw the all expected files iff comm -23 is empty. grep -q ^ fails
# if there's nothing on its input. We ignore the check completely if the
# corresponding .failed-check file exists.
comm -23 "${work_dir}/exp" "${work_dir}/seen" >"${work_dir}/unseen"
if [ -s "${work_dir}/unseen" ]; then
    echo -e >&2 "Some elements were not seen in $stdout.\n"
    cat >&2 "${work_dir}/unseen"
    exit 1
fi
