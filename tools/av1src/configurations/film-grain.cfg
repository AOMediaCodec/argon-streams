/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

default {
    // An example that shows how to override film grain parameters.

    // Switch on film grain in the sequence header
    film_grain_params_present = 1;

    // Every frame will use film grain
    apply_grain = 1;

    // Pick some seed based on the frame number.
    grain_seed = 12345 ^ frame_number;

    // Copy existing film grain parameters if possible. Since this is
    // only read on inter frames (and we want to see the element be
    // read), we force as many inter frames as possible.
    enc_update_grain = 0;
    enc_still_picture = 0;
    enc_use_scalability = 0;
    enc_frame_is_inter = 1;
    enc_inter_not_s_frame = 1; // <IGNORED>

    // Copy film grain parameters from the highest valid reference slot
    enc_film_grain_params_ref_idx =
      enc_compatible_refs [numCompatibleRefs - 1];

    // If we're sending film grain parameters, let's always
    // num_y_points to the maximum value (14). Note that we need to
    // make sure that point_y_value is always increasing.
    num_y_points = 14;
    point_y_value = i + 1;
    point_y_scaling = choose (uniform ( 1..10 ));

    // Disable chroma_scaling_from_luma and make sure we're not in
    // monochrom mode (we want to signal CB and CR points too).
    chroma_scaling_from_luma = 0;
    enc_monochrome = 0;

    // Pick a random (positive) number of CB and CR points; set their
    // values increasing in the same way as point_y_value and pick
    // random scalings.
    num_cb_points = choose (uniform ( 1..10 ));
    point_cb_value = i + 1;
    point_cb_scaling = choose (uniform ( 1..10 ));

    num_cr_points = choose (uniform ( 1..10 ));
    point_cr_value = i + 1;
    point_cr_scaling = choose (uniform ( 1..10 ));

    // Now signal grain scaling. Let's pick the minimal value.
    grain_scaling_minus_8 = 0;

    // Let's set ar_coeff_lag to 1 (so we read some of each type of AR
    // coefficient, but not too many).
    ar_coeff_lag = 1;

    // Send completely random AR coefficients
    ar_coeffs_y_plus_128 = choose_bits (8);
    ar_coeffs_cb_plus_128 = choose_bits (8);
    ar_coeffs_cr_plus_128 = choose_bits (8);

    // Send random ar_coeff_shift and grain_scale_shift
    ar_coeff_shift_minus_6 = choose_bits (2);
    grain_scale_shift = choose_bits (2);

    // Again, send random coefficients for CB and CR mult, luma_mult
    // and offset.
    cb_mult = choose_bits (8);
    cb_luma_mult = choose_bits (8);
    cb_offset = choose_bits (9);

    cr_mult = choose_bits (8);
    cr_luma_mult = choose_bits (8);
    cr_offset = choose_bits (9);

    // Let's disable the overlap_flag and clip_to_restricted_range
    overlap_flag = 0;
    clip_to_restricted_range = 0;
}
