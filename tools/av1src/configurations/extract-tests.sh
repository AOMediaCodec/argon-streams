#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This script extracts the configuration parameters of "run it and
# check it doesn't blow up" tests for a given configuration file and
# spits out a Makefile fragment listing them.

if [ $# != 4 ]; then
    echo >&2 "Usage: extract-tests.sh <test-re> <start-seed> <n> <config-file>"
    exit 1
fi

set -e

TEST_RE="$1"
START_SEED="$2"
NSEEDS="$3"
END_SEED=$((START_SEED + NSEEDS - 1))
CFG_IN="$4"
shift 4

base=$(basename $(echo "$CFG_IN" | tr '/' '@') .cfg)
out_base="\$(OUTPUT_DIR)/$base+"

all_configs=$(grep -Ev '//.*<IGNORED>' "$CFG_IN" |
                  sed -En 's!^([a-zA-Z_][a-zA-Z_0-9]*)\s*\{.*$!\1!gp')
config_to_skip=default
if [ x"${all_configs}" = x"default" ]; then
    config_to_skip=""
fi

echo "# Auto-generated file (by extract-tests.sh) for ${CFG_IN}"
echo

# Badness will ensue if the config basename isn't a Make symbol
# constituent.
echo "${base}" | grep -Eq '^[a-zA-Z0-9_-]*$' || {
    echo >&2 'Config base name `'"${base}' is not a valid Make symbol"
    exit 1
}

echo "CFGS_FOR_${base} :="

SEEDS=$(seq $START_SEED $END_SEED)

for cfg in $all_configs; do
    if [ x"${cfg}" == x"${config_to_skip}" ]; then
       continue
    fi

    if [ x"${TEST_RE}" != x ]; then
        if ! echo "${base}+${cfg}+${seed}" | grep -q "${TEST_RE}"; then
            continue
        fi
    fi

    for seed in $SEEDS; do
        out_file="${out_base}$cfg+$seed.obu"
        echo
        echo "${out_file}: ${CFG_IN}"

        echo "PAIRS_FOR_${base} += ${cfg}+${seed}"
    done
done
