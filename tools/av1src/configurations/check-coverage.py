#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A script to check generated OBU files cover the expected branch points.'''

import argparse
import os
import subprocess
import importlib
import re
import sys


# Creates an interval
def ivl(lo, hi):
    return range(lo, hi + 1)


def get_branch_data(stream_data_, name_):
    # Try to find a key (branch point name) matching name_. The match is
    # glob-based.
    name_parts = [re.escape(part) for part in name_.split('*')]
    name_re = re.compile('.*'.join(name_parts))

    # ending with name_, and get its
    # value (branch point coverage data)
    relevant_values = [value for key, value in stream_data_.t.items()
                       if name_re.search(key)]

    if len(relevant_values) == 0:
        # Throw an error if we couldn't find the branch point
        raise RuntimeError('Couldn\'t find "{}" in the stream data.'
                           .format(name_))

    if len(relevant_values) > 1:
        # Throw an error if name_ was ambiguous, i.e. it matched more than one
        # branch point name
        raise RuntimeError("The name `{}' is ambiguous; occurring {} "
                           "times in the stream data."
                           .format(name_, len(relevant_values)))

    return relevant_values[0]


# Main test entry point #######################

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('av1bcover', help='The av1bcover binary')
parser.add_argument('obu', help='.obu file')
parser.add_argument('config_dir', help='directory where all of the configurations are')
parser.add_argument('tmp_dir', help='temporary directory')

args = parser.parse_args()

# Verify that the obu filename is of the form "<config_base>+<config_cfg>+<config_seed>.obu".
# Throw error if not.
config_data = re.search('(.*/)?([-A-Za-z_0-9]+)\+([A-Za-z_0-9]+)\+([0-9]+).obu', args.obu)

if not config_data:
    raise RuntimeError('obu filename "{}" not in the correct format'
                       .format(args.obu))

# Extract the different parts of the obu path
config_base = config_data.group(2)
config_cfg = config_data.group(3)
config_seed = config_data.group(4)

# First determine if there are any coverage tests specified within our config.
# If not, we shouldn't go through the hassle of generating the coverage.

config_path = os.path.join(args.config_dir, '{}.cfg'.format(config_base))
found_coverage_test = False

with open(config_path) as config_file:
    in_cfg = False
    for raw_line in config_file:
        line = raw_line.strip()

        if not in_cfg and re.match(r'^ *' + config_cfg + r' *\{ *$', line):
            in_cfg = True
            continue

        if in_cfg and re.match(r'^\} *$', line):
            break

        if in_cfg:
            test_match = re.match('^ *//@@(.*)$', line)
            if test_match:
                found_coverage_test = True
                break

if not found_coverage_test:
    print('No coverage tests were found for config "{}" in "{}").'
          .format(config_cfg, config_path))
    exit(0)

# At this point, we know that the configuration does contain at least one
# coverage test. Therefore, we now run av1bcover to generate the coverage of
# the stream.

# Set up directory so that we can import modules from it
sys.path.append(os.path.abspath(args.tmp_dir))

# Work out the filename for the coverage file
cover_filename = '{}.py'.format(os.path.basename(args.obu)[:-4])

# Work out the full path of the coverage file
bsg_coverage_path = os.path.join(args.tmp_dir, cover_filename)

# Delete coverage file if it's already there
if os.path.exists(bsg_coverage_path):
    os.remove(bsg_coverage_path)

# Run coverage tool, storing the Python-ized data
cover_cmd = [args.av1bcover, "-p", "/dev/null",
             "--pyf", bsg_coverage_path, args.obu]
try:
    subprocess.check_call(cover_cmd,
                          stdout=subprocess.DEVNULL)
except subprocess.CalledProcessError:
    sys.stderr.write('Error when running av1bcover. Command:\n  {}\nExiting.\n'
                     .format(' '.join(cover_cmd)))

# Import the data from the Python file
stream_data = importlib.import_module(cover_filename[:-3])

# PARSE CONFIG FILE
#
# Configuration files may contain special comments of the form:
#
#   //@@ [IF] HIT  <name> IN    <set>
#   //@@ [IF} HIT  <name> COVER <set>
#   //@@ MISS <name>
#
# These comments apply to the specific configuration they reside within.
#
# <name> is the name of a branch point. Since we need to match branch names
# that look like "fun_name; some_unpredictable_number; condition", we need some
# sort of pattern matching. However, the conditions have lots of regex special
# characters, so a regular expression isn't very nice. Instead, we match globs
# (like a shell), so "*" turns into the regex ".*" and everything else is
# matched literally.
#
# A HIT .. IN .. line tells us that the branch point <name> should have
# been hit, and should contain a value in the set <set>. Of course, we
# only get min and max values from the coverage tool, so this script
# checks that the range min..max is a subset of <set>. If this is not true,
# we throw an error.
#
# A HIT .. COVER .. line tells us that the branch point <name> should have
# been hit, and should cover all values in the set <set>. Of course, we only
# get min and max values from the coverage tool, so this script checks that the
# range min..max is a superset of <set>. If this is not true, we throw an
# error.
#
# By prepending a HIT line with 'IF', the check is only fired if we actually
# hit a branch point, i.e. we allow the branch point to be missed.
#
# <set> is described as a union of comma-separated (inclusive) intervals,
# a..b. A single value a may be used as shorthand for a..a. As an example,
# we may have <set> = 8, or <set> = 1..16, 32.
#
# A MISS line tells us that that the branch point <name> should not have
# been hit. If this is not true, we throw an error.
#

# Extract the lines from the specific configuration that describe the
# coverage tests. All of these lines start with "//@@" (potentially
# after some whitespace).

lines = []
cumul_line = ''
with open(config_path) as config_file:
    in_cfg = False
    for raw_line in config_file:
        line = raw_line.strip()

        if not in_cfg and re.match('^ *' + config_cfg + ' *\{ *$', line):
            in_cfg = True
            continue

        if in_cfg and re.match('^\} *$', line):
            break

        if in_cfg:
            test_match = re.match('^ *//@@(.*)$', line)
            if test_match:
                test_str = test_match.group(1).strip()
                if test_str[-1] == '\\':
                    cumul_line += test_str[:-1]
                else:
                    cumul_line += test_str
                    lines.append(cumul_line)
                    cumul_line = ''

# Throw error if the final coverage test line finished with a \
if cumul_line != '':
    raise RuntimeError('Test comment line finished with "\\" but did not '
                       'continue on the next line. Line = "{}".'
                       .format(cumul_line))

# Parse lines
for line in lines:
    if line[:4] == 'MISS':
        # Here we expect a line of the form
        #   MISS <name>

        # Extract name from line
        name = line[4:].strip()

        # Find the coverage data associated with the branch point specified by
        # <name>
        hits = get_branch_data(stream_data, name)

        # Throw an error if we've hit this branch point (hits[0] is the min
        # value recorded, and hits[1] is the max value recorded).
        if not (hits[0] is None and hits[1] is None):
            raise RuntimeError('We hit "{}" when we weren\'t supposed to.\n'
                               'Line = {}'.format(name, line))

    elif line[:3] == 'HIT' or line[:6] == 'IF HIT':
        # Here we expect a line of the form
        #   [IF] HIT <name> IN    <set>
        # or
        #   [IF] HIT <name> COVER <set>

        # Extract "<name> IN|COVER <set>" from line
        is_IF_HIT = line[:2] == 'IF'
        sub_line = line[(6 if is_IF_HIT else 3):].strip()

        # Attempt to extract <name> and <set> and determine whether this is an
        # IN or COVER query

        # This is made slightly more complicated because it's possible that
        # <name> could contain 'IN' or 'COVER', and we don't want to pick up
        # these instances.

        # Attempt to split the line at the final instance of 'IN' and 'COVER'
        sub_line_parts_IN = [x.strip() for x in sub_line.rsplit('IN', 1)]
        sub_line_parts_COV = [x.strip() for x in sub_line.rsplit('COVER', 1)]

        # Determine if the line contained 'IN' or 'COVER'
        got_IN = len(sub_line_parts_IN) == 2
        got_COV = len(sub_line_parts_COV) == 2

        # Throw an error if neither 'IN' nor 'COVER' was present in the line
        if not got_IN and not got_COV:
            raise RuntimeError('Expecting condition "[IF] HIT <name> IN '
                               '<set>" or "[IF] HIT <name> COVER <set>" '
                               'for line "{}".'.format(line))

        query_type = ''

        if got_IN:
            if got_COV:
                # If both 'IN' and 'COVER' appear, we pick the one that's
                # closest to the end of the line
                query_type = ('COV' if (len(sub_line_parts_COV[1]) <
                                        len(sub_line_parts_IN[1]))
                              else 'IN')
            else:
                query_type = 'IN'
        else:
            query_type = 'COV'

        # Extract <name> and <set> based on the query_type we decided on
        name = sub_line_parts_IN[0] if query_type == 'IN' else sub_line_parts_COV[0]
        set_def = sub_line_parts_IN[1] if query_type == 'IN' else sub_line_parts_COV[1]

        # <set> will have the form: a0..b0, a1..b1, a2..b2, ..., where a single value c is shorthand for
        # c..c. We now separate the different intervals, and construct the set that is the union of them.
        sets = [x.strip() for x in set_def.split(',')]

        allowed_set = set()
        for cfg_set in sets:
            set_boundaries = cfg_set.split('..')
            if len(set_boundaries) == 1:
                allowed_set.add(int(set_boundaries[0]))
            elif len(set_boundaries) == 2:
                start = int(set_boundaries[0])
                end = int(set_boundaries[1])
                if end >= start:
                    allowed_set.update(ivl(start, end))
                else:
                    allowed_set.update(ivl(end, start))
            else:
                raise RuntimeError('Umm...I don\'t understand this line: "{}".'.format(line))

        # Find the coverage data associated with the branch point specified by <name>
        hits = get_branch_data(stream_data, name)

        # If we had an 'IF HIT' statement, then we simply ignore the line if we missed the branch
        # point. Otherwise (we had a 'HIT' statement) we throw an error. Note: hits[0] is the min
        # value recorded, and hits[1] is the max value recorded.
        if hits[0] is None or hits[1] is None:
            if is_IF_HIT:
                continue
            raise RuntimeError('We missed "{}" when we weren\'t supposed to.\nLine = {}'.format(name, line))

        # Generate a range of (possibly) obtained values from the min (hits[0]) and max (hits[1]) we found
        obtained_set = set(ivl(int(hits[0]), int(hits[1])))

        if query_type == 'IN':
            # If the obtained range is not a subset of our allowed range, we throw an error
            if not obtained_set.issubset(allowed_set):
                raise RuntimeError(
                    'For "{}" we obtained values in the set {}, but were only allowed values in the set {}.\n'
                    'Line = {}'.format(
                        name, obtained_set, allowed_set, line))
        else:
            assert (query_type == 'COV')
            # If the obtained range is not a superset of our allowed range, we throw an error
            if not obtained_set.issuperset(allowed_set):
                raise RuntimeError(
                    'For "{}" we obtained values in the set {}, '
                    'but these didn\'t hit all of the values in the set {}.\n'
                    'Line = {}'.format(
                        name, obtained_set, allowed_set, line))

    else:
        raise RuntimeError('Expecting "MISS" or "HIT" on line "{}".'.format(line))

# Remove generated coverage file
os.remove(bsg_coverage_path)
