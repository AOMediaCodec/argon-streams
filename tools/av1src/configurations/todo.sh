#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

CFG_DIR="$(dirname ${BASH_SOURCE})"

set -x

grep -v '^#' "${CFG_DIR}/../cfg_elements.txt" | \
  awk '/^[a-zA-Z]/ { if (xxx != "") { print xxx; } xxx = $1 } \
       /USE/ { xxx = "" } \
       END { if (xxx != "") { print xxx; } }' | \
  sort -u -o elts.lst

find "${CFG_DIR}" -name '*.cfg' | \
  xargs sed -nE 's!^\s*([^ ]*)\s*=([^=].*$|$)!\1!p' | \
  sort -u -o done.lst

comm -3 elts.lst done.lst >todo.lst

wc -l elts.lst done.lst todo.lst | head -n3
