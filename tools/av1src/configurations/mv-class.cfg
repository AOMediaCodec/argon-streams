/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

default {
    // How to override mv_class. The bit-stream generator works
    // internally by picking a motion vector and then working out how
    // to encode it with the class, sign bit, etc. Let's suppose that
    // we want to ensure mv_class is never more than 2. We can
    // guarantee that by making sure the delta in each coordinate is
    // at most 3.
    //
    // One way to enforce this is to override enc_mv_coord explicitly,
    // but this is still a little fiddly because an expression like:
    //
    //    enc_mv_coord = ((is_col ? pred_x : pred_y) +
    //                    (choose_bit () ? 1 : -1) * choose (uniform (1..3)));
    //
    // doesn't guarantee that we won't fail one of the other
    // invariants required of MV coordinates.
    //
    // An easier approach, if you only need to constrain the maximum
    // MV class, is to set enc_mv_max_col and enc_mv_max_row (and
    // don't set enc_specify_mv: the encoder will pick sensible motion
    // vectors on our behalf).

    enc_mv_max_col = 3;
    enc_mv_max_row = 3;

    // Force the frames not to be too big
    enc_profile_min_width  = 64;
    enc_profile_max_width  = 64;
    enc_profile_min_height = 64;
    enc_profile_max_height = 64;

    // We'd better generate some actual motion vectors, for which
    // we're going to need inter frames.
    enc_still_picture = 0;
    enc_use_scalability = 0;
    enc_frame_is_inter = 1;
}
