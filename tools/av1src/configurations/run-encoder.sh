#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

set -e
set -o pipefail

if [ $# != 4 ]; then
    echo >&2 "Usage: run-encoder.sh <encoder> <obu-dir> <cfg-dir> <cfg-triple>"
    exit 1
fi

encoder="$1"
obu_dir="$2"
cfg_dir="$3"
cfg_triple="$4"
shift 4

if [ x"$cfg_dir" = x. ]; then
    cfg_head=""
else
    cfg_head="$cfg_dir/"
fi

# Split cfg_triple into its 3 parts
cfg_nf="$(echo "$cfg_triple" | awk -F '+' '{ print NF }')"
if [ x"$cfg_nf" != x3 ]; then
    echo >&2 "Error config triple \`${cfg_triple}' doesn't have 3 parts."
    exit 1
fi
cfg_file="${cfg_head}$(echo "$cfg_triple" | awk -F '+' '{ print $1 }').cfg"
config="$(echo "$cfg_triple" | awk -F '+' '{ print $2 }')"
seed="$(echo "$cfg_triple" | awk -F '+' '{ print $3 }')"

base_cfg_file="$(basename "$cfg_file" .cfg)"
output_base="$obu_dir/$base_cfg_file+$config+$seed"

obu="${output_base}.obu"
stdout="${output_base}.stdout"

declare -a enc_cmd
enc_cmd=("$encoder" -s "$seed" -o "$obu" -C "$cfg_file" -i "$config" -d .)

# We use large-scale-tile encoding if cfg_triple starts with "lst-"
if [ "$(echo "$cfg_triple" | head -c 4)"x = lst-x ]; then
    enc_cmd+=("--large-scale-tile")
fi

# Actually run the encoder
"${enc_cmd[@]}" >"$stdout" 2>&1 || {
    # If we failed, it might be OK (depending on the contents of
    # stdout). We signal that something went awry by deleting the
    # output file
    rm -f "$obu"
}

# At this point, the output file should never exist and be empty.
test -s "$obu" -o ! -e "$obu" || {
    echo "Generated empty OBU file at \`$obu'."
    exit 1
}

# If there is no output file, something went wrong. But it might be an
# innocuous check, in which case we produce an empty output (so there
# is something there, but we can tell it is bad). The test above makes
# sure this is the only way we can end up with an empty obu.

set -a known_checks
known_checks=(STREAM_SIZE
              LARGE_SCALE_TILE
              DECODER_MODEL
              MV_CLASS_TOO_LARGE)
regex=
for ((i = 0; i < ${#known_checks[@]}; i++)); do
    if [ $i = 0 ]; then
        regex="${known_checks[i]}"
    else
        regex="${regex}"'|'"${known_checks[i]}"
    fi
done
regex="Check Failed:\s+\[${regex}\]"

if grep -qE "${regex}" "${stdout}"; then
    test ! -e "$obu" || {
        echo 1>&2 "Huh? Check failed in $stdout but we exited with retcode 0."
        rm -f "$obu"
        exit 1
    }
    touch "$obu"
fi

# At this point, $obu will exist iff the encode succeeded or was
# ignored.
if [ ! -e "$obu" ]; then
    echo -e 1>&2 "Encode of $obu failed. Command:\n  ${enc_cmd[@]}"
    exit 1
fi
