#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

set -e
set -o pipefail

if [ $# != 5 ]; then
    echo >&2 "Usage: check-decode.sh <av1bdec> <aomdec> <lstdec> <obu> <cfg_triple>"
    exit 1
fi

av1bdec="$1"
aomdec="$2"
lstdec="$3"
obu="$4"
cfg_triple="$5"
shift 5

if [ ! -x "$av1bdec" ]; then
    echo >&2 "av1bdec binary at $av1bdec is not executable."
    exit 1
fi
if [ ! -x "$aomdec" ]; then
    echo >&2 "aomdec binary at $aomdec is not executable."
    exit 1
fi
if [ ! -x "$lstdec" ]; then
    echo >&2 "lstdec binary at $lstdec is not executable."
    exit 1
fi
if [ ! -e "$obu" ]; then
    echo >&2 "OBU file at $obu does not exist."
    exit 1
fi

# We use large-scale-tile encoding if cfg_triple starts with "lst-"
use_lst=0
if [ "$(echo "$cfg_triple" | head -c 4)"x = lst-x ]; then
    use_lst=1
fi

# If the OBU is an empty file, this means that some non-fatal check
# failed when running the encoder and we're ignoring this seed. Skip
# it.
if [ ! -s "$obu" ]; then
    echo "Not checking decodes for empty $obu"
    exit 0
fi

# Make ourselves a temporary working directory in TMPDIR
work_dir=""
cleanup () {
    if [ x"$work_dir" != x ]; then
        rm -rf "$work_dir"
    fi
}
trap cleanup EXIT
work_dir="$(mktemp -d --tmpdir av1-check-run.XXXXXXXXXXX)"

# Decode with av1bdec. Use --estimate-cmd on large scale tile streams.
"$av1bdec" -v -o "$work_dir/av1b.yuv" "$obu" >/dev/null 2>&1 || {
    echo >&2 "Failed to decode $obu with av1bdec."
    exit 1
}

# Decode using reference decoder.
if [ $use_lst -eq 1 ]; then
    # Decode large scale tile streams using the lightfield tile list
    # decoder. Get the LST params from the av1bdec decode.
    av1b_stdout=$("$av1bdec" -v --estimate-cmd "$obu")
    if [ $? -ne 0 ]; then
        echo >&2 "Failed to decode $obu with av1bdec."
        exit 1
    fi

    lst_params=$(echo "$av1b_stdout" \
                     | grep -e "reference decoder command" \
                     | grep -oP '\S+(?:\s+\S+){1} *$')

    "$lstdec" "$obu" "$work_dir/aom.yuv" $lst_params >/dev/null 2>&1 || {
        echo >&2 "Failed to decode $obu with lightfield tile list decoder."
        exit 1
    }
else
    # Decode with aomdec.
    "$aomdec" -o "$work_dir/aom.yuv" --annexb --all-layers --rawvideo "$obu" >/dev/null 2>&1 || {
        echo >&2 "Failed to decode $obu with aomdec."
        exit 1
    }
fi

diff -q "$work_dir/av1b.yuv" "$work_dir/aom.yuv" || {
    echo >&2 "av1bdec and aomdec produced different results on $obu."
    exit 1
}
