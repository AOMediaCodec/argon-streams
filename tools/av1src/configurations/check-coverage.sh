#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

set -e
set -o pipefail

if [ $# != 2 ]; then
    echo >&2 "Usage: check-coverage.sh <av1bcover> <obu>"
    exit 1
fi

av1bcover="$1"
obu="$2"
shift 2

# Check that the OBU file actually exists
if [ ! -e "$obu" ]; then
    echo >&2 "No OBU file at \`$obu'."
    exit 1
fi

# If the encode failed some known check, the obu file will be empty
# (see run-encoder.sh). In which case, we just ignore this seed.
if [ ! -s "$obu" ]; then
    echo "Encode failed a check. Ignoring this seed."
    exit 0
fi

# Make ourselves a temporary working directory in TMPDIR
work_dir=""
cleanup () {
    if [ x"$work_dir" != x ]; then
        rm -rf "$work_dir"
    fi
}
trap cleanup EXIT
work_dir="$(mktemp -d --tmpdir av1-check-coverage.XXXXXXXXXXX)"

CFG_DIR="$(dirname ${BASH_SOURCE})"

# Run the Python script to test the coverage. Note that we don't need
# to know if the stream is a LST stream, as this is determined
# directly by av1bdec
python3 "${CFG_DIR}/check-coverage.py" "$av1bcover" "$obu" "$CFG_DIR" "$work_dir"
