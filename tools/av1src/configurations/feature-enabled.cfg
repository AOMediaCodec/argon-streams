/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

default {
    insert just_alt_q;
}

base { // <IGNORED>
    segmentation_enabled = 1;
}

just_alt_q {
    // Enable the segmentation alt_q feature and nothing else
    insert base;
    feature_enabled = (j == SEG_LVL_ALT_Q);

    // Set the alternative Q index to be equal to the segment ID.
    // Notice that we can't only override feature_value when j is
    // SEG_LVL_ALT_Q. This is a constraint of the configuration
    // language, caused by the fact that we want to inspect the value
    // of a local variable (j), so need an ambient bit-stream element.
    feature_value = (j == SEG_LVL_ALT_Q) ? i : 0;

    // Tell the encoder that we don't want to try to encode lossless
    // frames (which it probably won't manage when when we override
    // the Q index to be positive).
    enc_lossless_prob = 0;
}

just_alt_lf_y_v {
    // Enable the segmentation alt_lf_y_v feature and nothing else
    insert base;
    feature_enabled = (j == SEG_LVL_ALT_LF_Y_V);

    // Override loop filter strengths based on the segment ID. See
    // note in just_alt_q about why we need a ternary operator here.
    feature_value = (j == SEG_LVL_ALT_LF_Y_V) ? i : 0;
}

just_alt_lf_y_h {
    // Enable the segmentation alt_lf_y_h feature and nothing else
    insert base;
    feature_enabled = (j == SEG_LVL_ALT_LF_Y_H);

    // Override loop filter strengths based on the segment ID. See
    // note in just_alt_q about why we need a ternary operator here.
    feature_value = (j == SEG_LVL_ALT_LF_Y_H) ? i : 0;
}

just_alt_lf_u {
    // Enable the segmentation alt_lf_u feature and nothing else
    insert base;
    feature_enabled = (j == SEG_LVL_ALT_LF_U);

    // Override loop filter strengths based on the segment ID. See
    // note in just_alt_q about why we need a ternary operator here.
    feature_value = (j == SEG_LVL_ALT_LF_U) ? i : 0;
}

just_alt_lf_v {
    // Enable the segmentation alt_lf_v feature and nothing else
    insert base;
    feature_enabled = (j == SEG_LVL_ALT_LF_V);

    // Override loop filter strengths based on the segment ID. See
    // note in just_alt_q about why we need a ternary operator here.
    feature_value = (j == SEG_LVL_ALT_LF_V) ? i : 0;
}

just_ref_frame {
    // Enable the segmentation ref_frame feature and nothing else
    insert base;
    feature_enabled = (j == SEG_LVL_REF_FRAME);

    // Override the reference frame to always be INTRA_FRAME
    feature_value = (j == SEG_LVL_REF_FRAME) ? INTRA_FRAME : 0;
}

just_skip {
    // Enable the segmentation skip feature and nothing else
    insert base;
    feature_enabled = (j == SEG_LVL_SKIP);
}

just_globalmv {
    // Enable the GLOBALMV feature and nothing else
    insert base;
    feature_enabled = (j == SEG_LVL_GLOBALMV);
}
