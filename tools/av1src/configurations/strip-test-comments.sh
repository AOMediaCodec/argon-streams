#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# Strip test comments from the directory given as first
# argument.

set -e

if [ $# != 1 ]; then
    echo >&2 "Usage: strip-test-comments.sh <dir>"
    exit 1
fi

DEST="$1"
shift

# This regex is used to find files where we need to run sed. The regex
# "." would work fine, but be rather inefficient.
FILE_REGEX='(<IGNORED>)|(//@@)'

# These are arguments for sed that remove the testing comments
TEST_COMMENT_REPLACEMENTS=(
    -e '/\/\/@@/d'
    -e 's![[:space:]]*//\s<IGNORED>.*$!!g'
)

# These regexes are checked (case insensitively) in order to make sure
# that the testing comments have been properly expunged.
CHECK_REGEXES=(
    '//@@'
    '<IGNORED>'
)

(grep -Eril "${FILE_REGEX}" "${DEST}" || true) | \
    xargs -r sed -i "${TEST_COMMENT_REPLACEMENTS[@]}"

for ((i = 0; i < ${#CHECK_REGEXES[@]}; i++)); do
    set +e
    grep -ril "${CHECK_REGEXES[i]}" "${DEST}" && {
        echo "Error: ${CHECK_REGEXES[i]} not completely stripped."
        exit 1
    }
    set -e
done
