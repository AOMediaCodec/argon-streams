/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_LOOPFILTER 0

#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_LOOPFILTER 1
#else
#define VALIDATE_SPEC_LOOPFILTER 0
#endif
#define VALIDATE_SPEC_LOOPFILTER_INTERNALS 0
#define VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS 0
/*
Feels like the loopfilter decision should be able to be based on
the position and transform size of neighbours.

Deblocks one 64x64 unit at a time (LOOP_FILTER_BLOCK_SIZE indicates how many mi units to do at once)

Works based on 8x8 units, but also has the option of filtering internal 4x4 transform edges.
Decides whether to use 16,8,4 filter based on transform size and closeness to edge of screen.

Each superblock is divided into blocks of different sizes via partitioning.
Intra blocks can be additional divided into 4 subblocks.

Logic in filter_block_plane_non420 is simpler to understand:
  Need to filter at block edges (top and left).
  If inter and skip then ignores transform edges.
  Need to filter at transform edges as well.
  For blocks < 16x16 the u,v edges are set based on the first block: UV mask only computed on blocks down to 16x16 size.
  Size of filter depends on transform size.
  On 32x32 grid boundaries the filter is upgraded from 4x4 to 8x8
  Don't filter outside the image
  Don't apply filter on first column or first row in image
  Downgrade 16 filter to 8 filter for UV if impulse response goes outside image

  Needs mode and ref_frame[0] for each block

Actual filters compute tests based on high edge variance and flatness to decide whether to filter

Need ref frame,segment,mode,skip(including whether all coefficients were 0 in an 8x8 block) to determine the filter level.

Falls back to smaller filters if not detected flatness
*/

uint1 mode_lf_lut[MB_MODE_COUNT + 1] = {
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0,  // INTRA_MODES
  0,
  0, 0,
  0,           // (dummy entry for UV_CFL_PRED)
  1, 1, 0, 1,  // INTER_MODES (GLOBALMV == 0)
  1, 1, 1, 1, 1, 1, 0, 1  // INTER_COMPOUND_MODES (GLOBAL_GLOBALMV == 0)
};

uint6 get_filter_level(col, row, plane, edge_dir) {
  sx = plane_subsampling_x[plane]
  sy = plane_subsampling_y[plane]
  col |= sx
  row |= sy
#if VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS
  validate(180004)
#endif
  if (delta_lf_present) {
    i = (plane == 0) ? edge_dir : (plane + 1)
    baseFilterLevel = filter_level[i]
    if (delta_lf_multi)
      lvlSeg = current_deltas_lf_multi[col][row][i] + baseFilterLevel
    else
      lvlSeg = current_deltas_lf_from_base[col][row] + baseFilterLevel

    lvlSeg = Clip3(0, MAX_LOOP_FILTER, lvlSeg)
#if VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS
    validate(lvlSeg)
#endif
    feature = SEG_LVL_ALT_LF_Y_V + i
    if (segfeature_active_idx(segment_ids[col][row],feature)) {
      data = get_segdata_idx(segment_ids[col][row],feature)
      lvlSeg = Clip3(0, MAX_LOOP_FILTER, lvlSeg + data)
    }
#if VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS
    validate(lvlSeg)
#endif
    if (mode_ref_delta_enabled) {
      scale = 1 << (lvlSeg >> 5)
      lvlSeg += ref_lf_deltas[candidate_ref_frame(col,row,0,0)] * scale
      if (candidate_ref_frame(col,row,0,0) > INTRA_FRAME) {
        lvlSeg += mode_lf_deltas[mode_lf_lut[y_modes[col][row]]] * scale
      }
      lvlSeg = Clip3(0, MAX_LOOP_FILTER, lvlSeg)
    }
#if VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS
    validate(lvlSeg)
#endif
    return lvlSeg
  }
  i = (plane == 0) ? edge_dir : (plane + 1)
  lvlSeg = lf_lvls[col][row][i]
#if VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS
  validate(lvlSeg)
#endif
  return lvlSeg
}

// Check if we're allowed to apply loop filtering/CDEF to the top edge of the
// mi unit at (mi_row, mi_col).
can_lf_top(mi_row, mi_col) {
  if (mi_row == 0)
    return 0
  return 1
}

// Check if we're allowed to apply loop filtering/CDEF to the left edge of the
// mi unit at (mi_row, mi_col).
can_lf_left(mi_row, mi_col) {
  if (mi_col == 0)
    return 0
  return 1
}

#define VERT_EDGE 0
#define HORZ_EDGE 1

get_tx_dim(mi_row, mi_col, plane, edge_dir) {
  bsize = sb_sizes[mi_col][mi_row]
  if (lossless_array[segment_ids[mi_col][mi_row]]) {
    txSz = TX_4X4
  } else
  if (refframes[0][mi_col][mi_row][0] > INTRA_FRAME && !skip_coeffs[mi_col][mi_row]) {
    txSz = inter_tx_size[mi_col][mi_row]
    txSz = plane ? get_uv_tx_size(txSz,bsize,mi_row,mi_col) : txSz
  } else
  {
    txSz = tx_sizes[mi_col][mi_row]
    txSz = plane ? get_uv_tx_size(txSz,bsize,mi_row,mi_col) : txSz
  }

  if (edge_dir == VERT_EDGE)
    return tx_size_wide[txSz]
  else
    return tx_size_high[txSz]
}

apply_filter(mi_row, mi_col, plane, edge_dir) {
  sx = plane_subsampling_x[plane]
  sy = plane_subsampling_y[plane]

  plane_width = ROUND_POWER_OF_TWO(crop_width, sx)
  plane_height = ROUND_POWER_OF_TWO(crop_height, sy)
  x = (mi_col * MI_SIZE) >> sx
  y = (mi_row * MI_SIZE) >> sy
  if (x >= plane_width || y >= plane_height) {
    return 0
  }

  real_mi_row = mi_row
  real_mi_col = mi_col
  mi_row |= plane_subsampling_y[plane]
  mi_col |= plane_subsampling_x[plane]

#if VALIDATE_SPEC_LOOPFILTER_LEVEL_INTERNALS
    // Do checks earlier to avoid generating spurious validate messages
  if (edge_dir == HORZ_EDGE && !can_lf_top(real_mi_row, real_mi_col))
      return 0
  if (edge_dir == VERT_EDGE && !can_lf_left(real_mi_row, real_mi_col))
      return 0
#endif

  // Fetch information about this block
  tx_dim = get_tx_dim(mi_row, mi_col, plane, edge_dir)
  sb_size = sb_sizes[mi_col][mi_row]
  plane_bsize = get_plane_block_size(sb_size, plane)
  lvl = get_filter_level(mi_col, mi_row, plane, edge_dir)
  skip = skip_coeffs[mi_col][mi_row]
  skip = skip && (refframes[0][mi_col][mi_row][0] > INTRA_FRAME)

  // Set up parameters which depend on the edge direction
  if (edge_dir == VERT_EDGE) {
    coord = x
    prev_mi_row = mi_row
    prev_mi_col = mi_col - (1 << sx)
    block_dim = block_size_wide_lookup[plane_bsize]
  } else {
    coord = y
    prev_mi_row = mi_row - (1 << sy)
    prev_mi_col = mi_col
    block_dim = block_size_high_lookup[plane_bsize]
  }

  // Check if we can actually deblock this edge
  if (edge_dir == HORZ_EDGE && !can_lf_top(real_mi_row, real_mi_col))
      return 0
  if (edge_dir == VERT_EDGE && !can_lf_left(real_mi_row, real_mi_col))
      return 0

  // Fetch information about the adjacent block
  prev_tx_dim = get_tx_dim(prev_mi_row, prev_mi_col, plane, edge_dir)
  prev_lvl = get_filter_level(prev_mi_col, prev_mi_row, plane, edge_dir)
  prev_skip = skip_coeffs[prev_mi_col][prev_mi_row]
  prev_skip = prev_skip && (refframes[0][mi_col][mi_row][0] > INTRA_FRAME)

  tx_edge = !(coord & (tx_dim - 1))
  pred_edge = !(coord & (block_dim - 1))

#if VALIDATE_LOOPFILTER
  validate(3000)
  validate(lvl)
  validate(prev_lvl)
  validate(x)
  validate(y)
  validate(plane)
  validate(sharpness_level)
#endif

  // Only deblock across transform boundaries
  if (! tx_edge)
    return 0

  // What filter should we apply, if any?
  if ((lvl || prev_lvl) && (!skip || !prev_skip || pred_edge)) {
    if (PARALLEL_DEBLOCKING_DISABLE_15TAP || (PARALLEL_DEBLOCKING_15TAPLUMAONLY && plane > 0))
      max_filter_len = 8
    else
      max_filter_len = 16

    filter_len = Clip3(4, max_filter_len, Min(tx_dim, prev_tx_dim))
    if (! lvl)
      lvl = prev_lvl

    // Determine remaining filter parameters
    blockInsideLimit = lvl >> ((sharpness_level > 0) + (sharpness_level > 4))
    if (sharpness_level > 0) {
      if (blockInsideLimit > (9 - sharpness_level))
        blockInsideLimit = (9 - sharpness_level)
    }
    if (blockInsideLimit < 1)
      blockInsideLimit = 1
    limit = blockInsideLimit
    blimit = (2 * (lvl + 2) + blockInsideLimit)
    thresh = lvl >> 4
#if VALIDATE_SPEC_LOOPFILTER_INTERNALS
    validate(180003)
    validate(lvl)
    validate(limit)
    validate(blimit)
    validate(thresh)
#endif

#if DECODE && COLLECT_STATS
    if (filter_len == 4) {
      category = 0 // Length 4
    } else if (filter_len == 8) {
      if (plane > 0) {
        category = 1 // Length 6
      } else {
        category = 2 // Length 8
      }
    } else {
      category = 3 // Length 13
    }
    total_deblocks[category] += 1
#endif // DECODE && COLLECT_STATS

    // Apply the filter of interest.
    // Note that the edge is always MI_SIZE pixels wide *in the current plane*,
    // so is either 1 or 2 mi units wide in the luma plane
    for (i = 0; i < MI_SIZE; i++) {
      if (filter_len == 4) {
        if (edge_dir == VERT_EDGE)
          lpf_vertical_edge_4(limit,blimit,thresh,x,y+i,plane)
        else
          lpf_horizontal_edge_4(limit,blimit,thresh,x+i,y,plane)
      } else if (plane != 0) {
        ASSERT(filter_len == 8, "Incorrect filter size for chroma plane")
        if (edge_dir == VERT_EDGE)
          lpf_vertical_edge_6(limit,blimit,thresh,x,y+i,plane)
        else
          lpf_horizontal_edge_6(limit,blimit,thresh,x+i,y,plane)
      } else if (filter_len == 8) {
        if (edge_dir == VERT_EDGE)
          lpf_vertical_edge_8(limit,blimit,thresh,x,y+i,plane)
        else
          lpf_horizontal_edge_8(limit,blimit,thresh,x+i,y,plane)
      } else if (filter_len == 16) {
        if (edge_dir == VERT_EDGE)
          lpf_vertical_edge_16(limit,blimit,thresh,x,y+i,plane)
        else
          lpf_horizontal_edge_16(limit,blimit,thresh,x+i,y,plane)
      }
    }
  }
}

filter_selectively_vert(mi_row,mi_col,plane) {
  colStep = 1 << plane_subsampling_x[plane]
  rowStep = 1 << plane_subsampling_y[plane]

  // Walk over each MI unit in the plane; when subsampling, only visit every second
  // MI unit along the subsampled axis/axes
  for (row = 0; row < LOOP_FILTER_BLOCK_SIZE; row += rowStep) {
    for (col = 0; col < LOOP_FILTER_BLOCK_SIZE; col += colStep) {
#if VALIDATE_SPEC_LOOPFILTER_INTERNALS
      validate(180002)
      validate(plane)
      validate(mi_row+row)
      validate(mi_col+col)
      validate(0)
#endif
      apply_filter(mi_row+row, mi_col+col, plane, VERT_EDGE)
    }
  }
}

filter_selectively_horiz(mi_row,mi_col,plane) {
  colStep = 1 << plane_subsampling_x[plane]
  rowStep = 1 << plane_subsampling_y[plane]

  // Walk over each MI unit in the plane; when subsampling, only visit every second
  // MI unit along the subsampled axis/axes
  for (row = 0; row < LOOP_FILTER_BLOCK_SIZE; row += rowStep) {
    for (col = 0; col < LOOP_FILTER_BLOCK_SIZE; col += colStep) {
#if VALIDATE_SPEC_LOOPFILTER_INTERNALS
      validate(180002)
      validate(plane)
      validate(mi_row+row)
      validate(mi_col+col)
      validate(1)
#endif
      apply_filter(mi_row+row, mi_col+col, plane, HORZ_EDGE)
    }
  }
}

// Deblock a superblock
// mi_row,mi_col are luma positions
filter_block_plane(mi_row,mi_col,cIdx,lfpass) {
  // Vertical pass
  // Choosing whether to filter the vertical boundary by filtering across
  if (lfpass&1)
    filter_selectively_vert(mi_row,mi_col,cIdx)

  if (lfpass&2)
  {
    // Horizontal pass
    // Choosing whether to filter the horizontal boundary by filtering down
    filter_selectively_horiz(mi_row,mi_col,cIdx)
  }
}

// Loop over superblocks and deblock each in turn
loop_filter_frame() {

#if VALIDATE_SPEC_LOOPFILTER
    fw = crop_width
    fh = crop_height
    for ( plane = 0; plane < get_num_planes(); plane++ ) {
        subX = ( plane == 0 ) ? 0 : subsampling_x
        subY = ( plane == 0 ) ? 0 : subsampling_y
        for ( i = 0; i < fh >> subY; i++) {
            for ( j = 0; j < fw >> subX; j++ ) {
                validate(180000)
                validate(plane)
                validate(j)
                validate(i)
                validate( frame[ plane ][ j ][ i ] )
            }
        }
    }
#endif

  // Same logic as the non-LOOPFILTER_LEVEL case, just performed
  // in a different order
  for(plane = 0 ; plane < get_num_planes(); plane++) {
    // Skip filtering any planes which have 0 base deblock strength
    if ((plane == 0 && (filter_level[0] || filter_level[1])) ||
        (plane > 0 && filter_level[1 + plane])) {
      for(lfpass=1;lfpass<=2;lfpass++)
        for(mi_row = 0; mi_row < mi_rows; mi_row += LOOP_FILTER_BLOCK_SIZE)
          for(mi_col = 0; mi_col < mi_cols; mi_col += LOOP_FILTER_BLOCK_SIZE) {
            :log_loop(b,p.mi_row,p.mi_col,2*(lfpass-1)+0,plane,MI_SIZE,LOOP_FILTER_BLOCK_SIZE)
            :C log_loop(b,mi_row,mi_col,2*(lfpass-1)+0,plane,MI_SIZE,LOOP_FILTER_BLOCK_SIZE)
            filter_block_plane(mi_row,mi_col,plane,lfpass)
            :log_loop(b,p.mi_row,p.mi_col,2*(lfpass-1)+1,plane,MI_SIZE,LOOP_FILTER_BLOCK_SIZE)
            :C log_loop(b,mi_row,mi_col,2*(lfpass-1)+1,plane,MI_SIZE,LOOP_FILTER_BLOCK_SIZE)
          }
    }
  }
#if VALIDATE_SPEC_LOOPFILTER
    fw = crop_width
    fh = crop_height
    for ( plane = 0; plane < get_num_planes(); plane++ ) {
        subX = ( plane == 0 ) ? 0 : subsampling_x
        subY = ( plane == 0 ) ? 0 : subsampling_y
        for ( i = 0; i < fh >> subY; i++) {
            for ( j = 0; j < fw >> subX; j++ ) {
                validate(180001)
                validate(plane)
                validate(j)
                validate(i)
                validate( frame[ plane ][ j ][ i ] )
            }
        }
    }
#endif
}


loop_filter_frame_init() {
  for (segment_id = 0; segment_id < MAX_SEGMENTS; segment_id++) {
    num_lf_params = monochrome ? 2 : 4
    for (i = 0; i < num_lf_params; i++) {
      lvl_seg = filter_level[i]
      feature = SEG_LVL_ALT_LF_Y_V + i
      if (segfeature_active(feature)) {
        data = get_segdata(feature)
        lvl_seg = Clip3(0, MAX_LOOP_FILTER, lvl_seg + data)
      }

      if (!mode_ref_delta_enabled) {
        for (ref = INTRA_FRAME; ref < MAX_REF_FRAMES; ref++) {
          for (mode = 0; mode < MAX_MODE_LF_DELTAS; mode++) {
            lvl_lookup[segment_id][ref][mode][i] = lvl_seg
          }
        }
      } else {
        scale = 1 << (lvl_seg >> 5)
        intra_lvl = lvl_seg + ref_lf_deltas[INTRA_FRAME] * scale
        lvl_lookup[segment_id][INTRA_FRAME][0][i] = Clip3(0, MAX_LOOP_FILTER,intra_lvl)

        for (ref = LAST_FRAME; ref < MAX_REF_FRAMES; ref++) {
          for (mode = 0; mode < MAX_MODE_LF_DELTAS; mode++) {
            inter_lvl = lvl_seg + ref_lf_deltas[ref] * scale + mode_lf_deltas[mode] * scale
            lvl_lookup[segment_id][ref][mode][i] = Clip3(0, MAX_LOOP_FILTER, inter_lvl)
          }
        }
      }
    }
  }
}

set_default_lf_deltas() {
  ref_lf_deltas[INTRA_FRAME] = 1
  ref_lf_deltas[LAST_FRAME] = 0
  ref_lf_deltas[LAST2_FRAME] = 0
  ref_lf_deltas[LAST3_FRAME] = 0
  ref_lf_deltas[BWDREF_FRAME] = 0
  ref_lf_deltas[GOLDEN_FRAME] = -1
  ref_lf_deltas[ALTREF2_FRAME] = -1
  ref_lf_deltas[ALTREF_FRAME] = -1

  mode_lf_deltas[0] = 0
  mode_lf_deltas[1] = 0
}

copy_lf_deltas(dst,src) {
  for (i = 0; i < TOTAL_REFS_PER_FRAME; i++)
    frame_ref_lf_deltas[dst][i] = frame_ref_lf_deltas[src][i]
  for (i = 0; i < MAX_MODE_LF_DELTAS; i++)
    frame_mode_lf_deltas[dst][i] = frame_mode_lf_deltas[src][i]
}
