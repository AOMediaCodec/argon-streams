/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_OUTPUT 1
#else
#define VALIDATE_SPEC_OUTPUT 0
#endif

reset_decoder_model()
{
  for (oppoint = 0; oppoint < MAX_NUM_OPERATING_POINTS; oppoint++)
  {
    current_dfg[oppoint] = 0
    for (i = 0; i < ENC_MAX_FRAMES; i++) {
      dfg_bits[oppoint][i] = 0
    }
    current_show_frame_idx[oppoint] = 0
    BufferSize[oppoint] = 0
    InitialPresentationDelay[oppoint] = 0 // Means "unknown"
    for (i = 0; i < DECODER_MODEL_BUFFER_POOL_SIZE; i++) {
      DecoderRefCount[oppoint][i] = 0
      PlayerRefCount[oppoint][i] = 0
      LatestDisplayIdx[oppoint][i] = -1
    }
    for (i = 0; i < DECODER_MODEL_VBI_SIZE; i++) {
      VBI[oppoint][i] = -1
    }
    Time[oppoint] = 0
    BufferTime[oppoint] = 0
    NextRemovalIdx[oppoint] = 0
    for (i = 0; i < ENC_MAX_FRAMES; i++) {
      buffer_removal_delay[oppoint][i] = 0
      frame_presentation_time[oppoint][i] = 0
      RemovalTime[oppoint][i] = 0
      TimeToDecode[oppoint][i] = 0
    }
    for (i = 0; i < DECODER_MODEL_VBI_SIZE; i++) {
      VBI[oppoint][i] = 0
    }
    for (i = 0; i < DECODER_MODEL_BUFFER_POOL_SIZE; i++) {
      DecoderRefCount[oppoint][i] = 0
      PlayerRefCount[oppoint][i] = 0
    }
  }
}

// Global data is used to initialize variables once per stream
#if DECODE
global_data(useAnnexB, isObu, oppoint,
            numLargeScaleTileAnchorFrames, numLSTTileListObus,
            overrideNoFilmGrain, frameRate)
#else
global_data(useAnnexB, isObu)
#endif
{
#if DECODE
  override_no_film_grain = overrideNoFilmGrain
  default_frame_rate = frameRate
#endif

  decoded_key_frame = 0
  frame_type = 0
  frame_number = 0
  current_video_frame = 0
  subsampling_x = 1
  subsampling_y = 1
  wedge_initialized = 0
  // Indicate that we haven't allocated any frame buffers yet
  aloc_frame_width = 0
  aloc_frame_height = 0
  for (i=0; i<TOTAL_REFS_PER_FRAME; i++) {
    refFrameSide[i] = 0
  }
  for (i=0; i<2+NUM_REF_FRAMES; i++) {
    ref_mi_rows[i] = 0
    ref_mi_cols[i] = 0
  }
  for (i = 0; i < NUM_REF_FRAMES; i++) {
    ref_valid[i] = 0
  }
  large_scale_tile = 0
  camera_frame_header_ready = 0

# if DECODE
  num_large_scale_tile_anchor_frames = numLargeScaleTileAnchorFrames
  num_large_scale_tile_tile_list_obus = numLSTTileListObus
#else
  num_large_scale_tile_anchor_frames = 0
  num_large_scale_tile_tile_list_obus = 0
#endif

# if DECODE
  anchor_frame_cnt = 0
# else // DECODE
  enc_large_scale_tile_metadata_sent = 0
  enc_target_tiles_w = -1
  enc_target_tiles_h = -1
# endif // DECODE
  use_annexb = useAnnexB
  is_ivf_file = !isObu
#if ENCODE
  sequenceHeaderGenerated = 0
#endif
  for (i = 0; i < NUM_REF_FRAMES; i++) {
    ref_valid[i] = 0
    saved_ref_decode_order[i][0] = 0
    ref_frame_number[i] = -1
  }

#if DECODE
  max_num_oppoints = -1
  CHECK(0 <= oppoint && oppoint < MAX_NUM_OPERATING_POINTS, "Selected operating point out of range")
  desired_oppoint = oppoint
#endif

  // Initialize the decoder model.
  //reset_decoder_model()

  // For the decoder model, we need to track whether this flag
  // is explicitly signalled or not. If it is not signalled, then
  // the default value is taken to be 0.
  spatial_layer_dimensions_present = 0

  CVSTemporalGroupDescriptionPresentFlag = 0
  sequenceHeaderChanged = 0
  prevSequenceHeaderSize = -1
  scalabilityStructureChanged = 0
  prevScalabilityStructureSize = -1
  SeenSeqHeaderBeforeFirstFrameHeader = 0
  SeenSeqHeader = 0

#if DECODE && COLLECT_STATS
  // Reset counters
  total_tiles = 0
  for (i = 0; i < 5; i++) {
    total_frames[i] = 0
    total_frame_bits[i] = 0
  }
  total_symbols = 0
  total_tile_bits = 0
  total_bits = 0
  max_frame_bits = 0
  min_max_display_rate_x100 = 1000000
  min_max_decode_rate_x100 = 1000000
  min_comp_ratio_x100 = 1000000
  display_rate_hit = 0
  decode_rate_hit = 0
  comp_ratio_hit = 0
  for (i = 0; i < NUM_1D_TXFMS; i++) {
    for (j = 0; j < 5; j++) {
      total_1d_txfms[i][j] = 0
    }
  }
  for (i = 0; i < 2; i++) {
    for (j = 0; j < BLOCK_SIZES_ALL; j++) {
      total_blocks[i][j] = 0
    }
  }
  total_intra_px = 0
  total_intrabc_px = 0
  total_inter_px = 0
  total_inter_preds = 0
  total_warp_preds = 0
  for (i = 0; i < TX_SIZES_ALL; i++) {
    total_txfm_blocks[i] = 0
  }
  for (i = 0; i < 7; i++) {
    total_coeffs[i] = 0
  }
  for (i = 0; i < 4; i++) {
    total_deblocks[i] = 0
  }
  total_cdefs = 0
  total_decoded_px = 0
  total_upscaled_px = 0
  total_displayed_px = 0
  min_superres_denom = 0
  total_wiener_px = 0
  total_selfguided_px = 0

  max_tile_ratio_num = 0
  max_tile_ratio_denom = 1
#endif // DECODE && COLLECT_STATS
  num_shown_frames = 0
#if ENCODE
  enc_is_stress_stream = 0
#endif // ENCODE
}

int clamp(x,low,high) {
  x = Clip3(low,high,x)
  return x
}

// Count the number of trailing bits of 'x'
// If x == 0, this returns 32, since 'x' is interpreted as a uint32.
int ctz(x) {
  i = 0
  for (i = 0; i < 32; i++) {
    // Is the 'i'th bit set?
    if (x & (1<<i)) {
      return i
    }
  }
  return i
}

// Note: Due to the buffering requirements of the various loop filtering
// processes (deblock, cdef, loop-restoration), the finished frame may end up
// in various different buffers (in the non-show-existing-frame case).
//
// Once we're done, we need to figure out which buffer contains the
// finished frame and copy it back into the 'frame' array.
select_frame_buffer() {
  // Final output is in 'lr_frame'
  for (plane = 0; plane < get_num_planes(); plane++) {
    planeW = ROUND_POWER_OF_TWO(upscaled_width, plane_subsampling_x[plane])
    planeH = ROUND_POWER_OF_TWO(upscaled_height, plane_subsampling_y[plane])
    for (i = 0; i < planeH; i++)
      for (j = 0; j < planeW; j++)
        frame[plane][j][i] = lr_frame[plane][j][i]
  }
}

#if ENCODE
// Generate one temporal unit's worth of frames, and return how many frames were encoded.
// The 'max_frames' argument can be used to limit how many frames may be
// included in a single TU, or to keep to the exact number of frames
// we want to generate in the stream as a whole.
int encode_temporal_unit(max_frames) {
  // We must put a temporal delimiter at the start of each temporal unit.
  enc_insert_td = 1

  if (enc_insert_td) {
    SeenSeqHeaderBeforeFirstFrameHeader = 0
    for (i=0; i<=MAX_SPATIAL_ID; i++) {
      TemporalIds[ i ] = -1
    }
  }

  uint64 tuSizeLocation
  tuSizeLocation = current_bitposition()
  tuSizeBytes = 0
  if (use_annexb) {
    // Reserve space for the TU size field
    tuSizeBytes u(0)
    tuSize = 0
    tuSize u(8*tuSizeBytes)
  }

  uint32 thisTU
  thisTU = sched_temporal_unit[frame_number]

  // The size and structure of this temporal unit has already been decided
  // by the scheduler, so just encode frames until we reach the start of the next TU.
  uint64 tuStart
  tuStart = current_position()
  framesInTU = 0
  while (framesInTU < max_frames && sched_temporal_unit[frame_number] == thisTU) {
    // Generate the OBU sequence for this frame
    :C hevc_parse_video_stream(b,&b->video_stream,0);
    :hevc_parse_video_stream(b,0);

    framesInTU++
  }
  uint64 tuEnd
  tuEnd = current_position()

  if (use_annexb) {
    overwrite_uleb128(tuEnd - tuStart, tuSizeLocation, tuSizeBytes)
  }

  :C printf("Encoded temporal unit of %d frame(s)\n", framesInTU)
  :print "Encoded temporal unit of %d frame(s)" % framesInTU
  return framesInTU
}
#else // ENCODE
// Decode a block of frames (with a total size of 'sz'),
// and return how many frames were contained
int decode_temporal_unit(sz) {
  framesInTU = 0

  if (use_annexb) {
    tuSize = decode_uleb128()
#if COLLECT_STATS
    total_bits += uleb_length * 8
#endif
  } else {
    tuSize = sz
  }

  uint64 tuStart
  uint64 tuEnd
  tuStart = current_position()
  tuEnd = tuStart + tuSize
  while (current_position() < tuEnd) {
    maxSize = tuEnd - current_position()
    skipped_final_frame = 0
    :b.log_count = 400 // Print the first 400 decoded symbols from each frame
    :C hevc_parse_video_stream(b,&b->video_stream,maxSize)
    :hevc_parse_video_stream(b,maxSize)
    framesInTU++
  }

  CHECK(current_position() == tuEnd, "Read too far while decoding temporal unit")
  :C printf("Decoded temporal unit of %d frame(s)\n", framesInTU)
  :print "Decoded temporal unit of %d frame(s)" % framesInTU
  return framesInTU
}
#endif // ENCODE

// Decode/encode one frame from the video stream
video_stream (num) {
  // after the camera_frame_ready_header don't overwrite the frame data
  if (!camera_frame_header_ready) {
    :b.video_stream = p
    autostruct frame_data()
    autostruct frame_cdfs()
  }
#if ENCODE
  encode_frame_init()
#endif

  pos_frame_start=current_position()
#if ENCODE
  encode_one_frame()
#else // ENCODE
  decode_one_frame()
  BytesInFrame = num
  if (skipped_final_frame) {
    return 0
  }
#endif // ENCODE
  y=current_position()-pos_frame_start
  CSV(CSV_FRAMEBITS,y<<3)
#if ENCODE
  :print "Bytes encoded=%d"%y
  :C printf("Bytes encoded=%d\n",y)
#else // ENCODE
  :print "Bytes decoded=%d, expected <= %d"%(y,num)
  :C printf("Bytes decoded=%d, expected <= %d\n",y,num)
  CHECK(y<=num,"Frame size exceeds remaining bytes in temporal unit")
#endif // ENCODE

  // Apply loopfiltering, if needed
  // Either way, the data which needs to be shown / stored in the reference buffer
  // is in 'frame' after this block.
  if (!show_existing_frame && !allow_intrabc
      // not the camera frame ready header
      && !(num_large_scale_tile_anchor_frames &&
           num_large_scale_tile_anchor_frames == frame_number)
      ) {
    if (filter_level[0] || filter_level[1])
      loop_filter_frame()

    cdef_filter_frame()

    superres_upscale_frame()

    loop_restoration_frame()

    // Copy the finished frame back into 'frame'.
    select_frame_buffer()
  }

  if (!large_scale_tile) {
    store_ref_buffers()
  }

  //x=show_frame
  //w=crop_width
  //h=crop_height
  //:print "show frame",x,w,h
  //:pdb.set_trace()

  // At this point,
  // i) If this is a show_existing_frame, then we've reloaded the frame buffer,
  //    width/height, etc. as if we had just decoded the frame to show
  // ii) We've saved the frame buffer into any reference slots we need.
  //
  // Thus any modifications we apply from here on will not be applied to the
  // reference slots - which is what we want for film grain synthesis.
  outputThisFrame = (show_existing_frame || show_frame)
  if (num_large_scale_tile_anchor_frames && !large_scale_tile) {
    save_anchor_frame()
    outputThisFrame = 0
  }
  // don't show the camera frame ready header
  if (num_large_scale_tile_anchor_frames &&
      num_large_scale_tile_anchor_frames == frame_number) {
    outputThisFrame = 0
  }
  w=0
  h=0
  if (outputThisFrame) {
    if (!large_scale_tile) {
      w = upscaled_width
      h = upscaled_height
      pitch = stride
      bps = bit_depth
      mono = monochrome

      nplanes = get_num_planes()
      :log_output(b,0,nplanes)
      :C log_output(b,0,nplanes)
#if VALIDATE_SPEC_OUTPUT
      for (plane = 0; plane < get_num_planes(); plane++) {
          subX = ( plane == 0 ) ? 0 : subsampling_x
          subY = ( plane == 0 ) ? 0 : subsampling_y
          for ( i = 0; i < (h + subY) >> subY; i++) {
              for ( j = 0; j < (w + subX) >> subX; j++ ) {
                  validate(90001)
                  validate(j)
                  validate(i)
                  validate(plane)
                  validate( frame[ plane ][ j ][ i ] )
              }
          }
      }
#endif

      really_apply_grain = apply_grain
#if DECODE
      if (override_no_film_grain) {
          really_apply_grain = 0
      }
#endif
      if (apply_grain) {
          if (really_apply_grain) {
              apply_film_grain()
          }
          CSV(CSV_FILMGRAIN_NOISE, 1)
      }
      :log_output(b,1,nplanes)
      :C log_output(b,1,nplanes)

#if VALIDATE_SPEC_OUTPUT
      for (plane = 0; plane < get_num_planes(); plane++) {
          subX = ( plane == 0 ) ? 0 : subsampling_x
          subY = ( plane == 0 ) ? 0 : subsampling_y
          for ( i = 0; i < (h+subY) >> subY; i++) {
              for ( j = 0; j < (w+subX) >> subX; j++ ) {
                  validate(90000)
                  validate(j)
                  validate(i)
                  validate(plane)
                  validate( frame[ plane ][ j ][ i ] )
              }
          }
      }
#endif
      ASSERT(w>=16 && h>=16, "Minimum frame size is 16x16 at the output")
      sw = mono ? 0 : subsampling_x
      sh = mono ? 0 : subsampling_y

      :video_output(b,bps,bps, w, h,0,0,0,0,pitch,0,0,1 << sw, 1 << sh,mono)
      :C video_output(b, bps, w, h, pitch, 0, 0, sw, sh, mono)
    } else { // large_scale_tile
      ASSERT(!apply_grain, "Don't expect film grain here")
      w = tile_width * MI_SIZE
      h = tile_height * MI_SIZE
      ASSERT(w>=16 && h>=16, "Minimum frame size is 16x16 at the output")
      pitch = stride
      bps = bit_depth
      mono = 0
      sw = subsampling_x
      sh = subsampling_y
      for (i = 0; i < tile_count; i++) {
        tileX = i % output_frame_width_in_tiles
        tileY = i / output_frame_width_in_tiles
        srcX = tileX * w
        srcY = tileY * h
        :video_output(b,bps,bps, w, h,0,0,0,0,pitch,srcX,srcY, 1 << sw, 1 << sh,mono)
        :C video_output(b, bps, w, h, pitch, srcX, srcY, sw, sh, mono)
      }
    }
  }

  if (show_frame || show_existing_frame) {
    num_shown_frames++
  }

#if DECODE
  last_width = upscaled_width
  last_height = upscaled_height

  CSV(CSV_FRAMECOUNT,1)
  if (show_frame || show_existing_frame) {
    CSV(CSV_SHOWFRAMECOUNT,1)
  }
#endif

  DisplayedLumaSamplesInTU += w * h
  FramesDisplayedInTU++

  frame_number++
  if (num_large_scale_tile_anchor_frames && !large_scale_tile) {
    if (frame_number == num_large_scale_tile_anchor_frames) {
      // we've decoded all the anchor frames, the next frame should be the camera frame header
      large_scale_tile = 1
    }
  }
#if DECODE
  calculate_rates()
#endif // DECODE
}

#if ENCODE
obu_file_init() {
  ASSERT(!is_ivf_file, "obu_file_init() should not be called for IVF streams")
  // Note: There are a few values which we need early in encoding, which are
  // (in an IVF file) generated while writing the file header.
  // But here's no file header for OBU-format files, so we need a placeholder
  // function to generate the values we need
  encode_file_init()
  g_w u(0)
  g_h u(0)
  frame_cnt u(0)
#if ENCODE
  enc_frames_left = frame_cnt
#endif
}
#endif

ivf_file_header() {
  ASSERT(is_ivf_file, "ivf_file_header() should not be called for non-IVF streams")
#if ENCODE
  encode_file_init()
#endif
  // Output an IVF file header
  uint32 header
  uint32 fourcc
  uint32 unused
  header u(32) CHECK(header==0x444b4946,"IVF header must be DKIF")
  ivf_version le(16) CHECK(ivf_version==0,"IVF version must be 0")
  headersize le(16) CHECK(headersize==32,"Header size must be 32")
  fourcc u(32) CHECK(fourcc==0x41563031,"FourCC must be AV01")
  g_w le(16)
  g_h le(16)
  g_timebase_den le(32)
  g_timebase_num le(32)
  frame_cnt le(32)
  unused u(32)
  //:print 'W=%d, H=%d, Frame count=%d'%(p.g_w,p.g_h,p.frame_cnt)
#if ENCODE
  enc_frames_left = frame_cnt
#endif
}

// num is an encoder only input to give the number of bytes in the frame
ivf_frame_header(num) {
  ASSERT(is_ivf_file, "ivf_frame_header() should not be called for non-IVF streams")
  frame_sz le(32)
  timebase_low le(32)
  timebase_high le(32)
}

// Used for debug to check that Python ad C agree on some variable
validate(x) {
  :validate(x)
  :C validate(x);
}

// Frame data is used to intialize variables for the current frame
frame_data() {
}

frame_cdfs() {
}
