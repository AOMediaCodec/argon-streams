/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_TRANSFORM 0
#define VALIDATE_SPEC_QUANT 0
#if VALIDATE_SPEC_USUAL_SUSPECTS
#define VALIDATE_SPEC_SKIP 1
#else
#define VALIDATE_SPEC_SKIP 0
#endif

/*
There is one quantiser used for DC coefficients, and one for AC coefficients.
Different quantisers used for luma and chroma and for different qindex values.

The neighbour[0] is above, the neighbour[1] is to the left (if available)
However for mixed DCT/ADST scans there is greater correlation if both neighbours set to the DCT neighbour
*/

#define VALIDATE 0

get_scan_nb_4x4(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<16;i++) {
      scan[i] = mrow_scan_4x4[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<16;i++) {
      scan[i] = mcol_scan_4x4[i]
    }
  } else {
    for(i=0;i<16;i++) {
      scan[i] = default_scan_4x4[i]
    }
  }
}

get_scan_nb_8x8(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<64;i++) {
      scan[i] = mrow_scan_8x8[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<64;i++) {
      scan[i] = mcol_scan_8x8[i]
    }
  } else {
    for(i=0;i<64;i++) {
      scan[i] = default_scan_8x8[i]
    }
  }
}

get_scan_nb_16x16(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<256;i++) {
      scan[i] = mrow_scan_16x16[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<256;i++) {
      scan[i] = mcol_scan_16x16[i]
    }
  } else {
    for(i=0;i<256;i++) {
      scan[i] = default_scan_16x16[i]
    }
  }
}

get_scan_nb_32x32(txType) {
  if ((txType==V_DCT)
      || (txType==V_ADST)
      || (txType==V_FLIPADST)) {
    for(i=0;i<1024;i++) {
      scan[i] = mrow_scan_32x32[i]
    }
  } else if ((txType==H_DCT)
             || (txType==H_ADST)
             || (txType==H_FLIPADST)) {
    for(i=0;i<1024;i++) {
      scan[i] = mcol_scan_32x32[i]
    }
 } else {
    CHECK((txType == DCT_DCT) || (txType==ADST_DCT) || (txType==FLIPADST_DCT) || (txType==DCT_ADST) ||
          (txType==DCT_FLIPADST) || (txType==ADST_ADST) || (txType==FLIPADST_FLIPADST) || (txType==ADST_FLIPADST) ||
          (txType==FLIPADST_ADST) || (txType == IDTX), "txType mismatch")
    for(i=0;i<1024;i++) {
      scan[i] = default_scan_32x32[i]
    }
  }
}

get_scan_nb_64x64(txType) {
  for(i=0;i<1024;i++) {
    scan[i] = default_scan_32x32[i]
  }
}

get_scan_nb_4x8(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<32;i++) {
      scan[i] = mrow_scan_4x8[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<32;i++) {
      scan[i] = mcol_scan_4x8[i]
    }
  } else {
    for(i=0;i<32;i++) {
      scan[i] = default_scan_4x8[i]
    }
  }
}

get_scan_nb_8x4(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<32;i++) {
      scan[i] = mrow_scan_8x4[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<32;i++) {
      scan[i] = mcol_scan_8x4[i]
    }
  } else {
    for(i=0;i<32;i++) {
      scan[i] = default_scan_8x4[i]
    }
  }
}

get_scan_nb_8x16(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<128;i++) {
      scan[i] = mrow_scan_8x16[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<128;i++) {
      scan[i] = mcol_scan_8x16[i]
    }
  } else {
    for(i=0;i<128;i++) {
      scan[i] = default_scan_8x16[i]
    }
  }
}

get_scan_nb_16x8(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<128;i++) {
      scan[i] = mrow_scan_16x8[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<128;i++) {
      scan[i] = mcol_scan_16x8[i]
    }
  } else {
    for(i=0;i<128;i++) {
      scan[i] = default_scan_16x8[i]
    }
  }
}

get_scan_nb_16x32(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<512;i++) {
      scan[i] = mrow_scan_16x32[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<512;i++) {
      scan[i] = mcol_scan_16x32[i]
    }
  } else {
    for(i=0;i<512;i++) {
      scan[i] = default_scan_16x32[i]
    }
  }
}

get_scan_nb_32x16(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<512;i++) {
      scan[i] = mrow_scan_32x16[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<512;i++) {
      scan[i] = mcol_scan_32x16[i]
    }
  } else {
    for(i=0;i<512;i++) {
      scan[i] = default_scan_32x16[i]
    }
  }
}

get_scan_nb_4x16(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<64;i++) {
      scan[i] = mrow_scan_4x16[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<64;i++) {
      scan[i] = mcol_scan_4x16[i]
    }
  } else {
    for(i=0;i<64;i++) {
      scan[i] = default_scan_4x16[i]
    }
  }
}

get_scan_nb_16x4(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<64;i++) {
      scan[i] = mrow_scan_16x4[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<64;i++) {
      scan[i] = mcol_scan_16x4[i]
    }
  } else {
    for(i=0;i<64;i++) {
      scan[i] = default_scan_16x4[i]
    }
  }
}

get_scan_nb_8x32(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<256;i++) {
      scan[i] = mrow_scan_8x32[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<256;i++) {
      scan[i] = mcol_scan_8x32[i]
    }
  } else {
    for(i=0;i<256;i++) {
      scan[i] = default_scan_8x32[i]
    }
  }
}

get_scan_nb_32x8(txType) {
  if ((txType==V_DCT) ||
      (txType==V_ADST) ||
      (txType==V_FLIPADST)) {
    for(i=0;i<256;i++) {
      scan[i] = mrow_scan_32x8[i]
    }
  } else if ((txType==H_DCT) ||
             (txType==H_ADST) ||
             (txType==H_FLIPADST)) {
    for(i=0;i<256;i++) {
      scan[i] = mcol_scan_32x8[i]
    }
  } else {
    for(i=0;i<256;i++) {
      scan[i] = default_scan_32x8[i]
    }
  }
}

get_scan_nb_16x64(txType) {
  for(i=0;i<512;i++) {
    scan[i] = default_scan_16x32[i]
  }
}

get_scan_nb_64x16(txType) {
  for(i=0;i<512;i++) {
    scan[i] = default_scan_32x16[i]
  }
}

get_scan_nb_32x64(txType) {
  for(i=0;i<1024;i++) {
    scan[i] = default_scan_32x32[i]
  }
}

get_scan_nb_64x32(txType) {
  for(i=0;i<1024;i++) {
    scan[i] = default_scan_32x32[i]
  }
}

uint8 get_block_mode(plane, blockx, blocky) {
  if (plane > 0)
    return uv_mode
  else
    return y_mode
}

uint_0_3 get_tx_type(txSz,plane,blocky,blockx) {
  plane_bsize = get_plane_block_size(sb_size, plane)
  txSet = get_ext_tx_set_type(plane_bsize, reduced_tx_set_used, txSz)

  if ( lossless || txsize_sqr_up_map[txSz] > TX_32X32 ) {
    txType = DCT_DCT
  } else {
    if (plane == 0) {
      txType = txk_type[blockx][blocky]
    } else if (is_inter) {
      // convert from chroma to luma co-ordinates (read_tx_type only y plane)
      x = blockx << plane_subsampling_x[plane]
      y = blocky << plane_subsampling_y[plane]
      txType = txk_type[x][y]
    } else {
      txType = mode2txfm_map[uv_mode]
    }
  }
  ASSERT(txType >= DCT_DCT && txType < TX_TYPES, "txType out of range")
  if (!av1_ext_tx_used[txSet][txType]) {
    return DCT_DCT
  }

  return txType
}

get_scan(txSz,txType) {
  if(txSz==TX_4X4) {
    get_scan_nb_4x4(txType)
  } else if (txSz==TX_8X8) {
    get_scan_nb_8x8(txType)
  } else if (txSz==TX_16X16) {
    get_scan_nb_16x16(txType)
  } else if (txSz==TX_32X32) {
    get_scan_nb_32x32(txType)
  }
  else if (txSz==TX_64X64) {
    get_scan_nb_64x64(txType)
  }
  else if (txSz==TX_4X8) {
    get_scan_nb_4x8(txType)
  } else if (txSz==TX_8X4) {
    get_scan_nb_8x4(txType)
  } else if (txSz==TX_8X16) {
    get_scan_nb_8x16(txType)
  } else if (txSz==TX_16X8) {
    get_scan_nb_16x8(txType)
  } else if (txSz==TX_16X32) {
    get_scan_nb_16x32(txType)
  } else if (txSz==TX_32X16) {
    get_scan_nb_32x16(txType)
  }
  else if (txSz==TX_4X16) {
    get_scan_nb_4x16(txType)
  } else if (txSz==TX_16X4) {
    get_scan_nb_16x4(txType)
  } else if (txSz==TX_8X32) {
    get_scan_nb_8x32(txType)
  } else if (txSz==TX_32X8) {
    get_scan_nb_32x8(txType)
  }
  else if (txSz==TX_16X64) {
    get_scan_nb_16x64(txType)
  } else if (txSz==TX_64X16) {
    get_scan_nb_64x16(txType)
  }
  else if (txSz==TX_32X64) {
    get_scan_nb_32x64(txType)
  } else if (txSz==TX_64X32) {
    get_scan_nb_64x32(txType)
  }
  else {
    CHECK(0, "Unknown txSz")
  }
}

uint_0_3 token_to_counttoken[MAX_ENTROPY_TOKENS] = {
  ZERO_TOKEN, ONE_TOKEN, TWO_TOKEN, TWO_TOKEN,
  TWO_TOKEN, TWO_TOKEN, TWO_TOKEN, TWO_TOKEN,
  TWO_TOKEN, TWO_TOKEN, TWO_TOKEN, DCT_EOB_MODEL_TOKEN
};

#if ENCODE
clear_saturation() {
  enc_seen_saturation = 0
}

// Copy coefficients with a scaling while remaining in legal limits
// Return 1 if this is a legal scale factor
test_scale(scalefactor,txSz,x,y,plane,seg_eob,dqcoeffMin,dqcoeffMax,txScale,txType) {
  int64 v
  denom = 1 << txScale
  for(i=0;i<seg_eob;i++) {
    dequant[i] = 0
  }
  for(c=0;c<seg_eob;c++) {
    dqv = get_quantizer(c, plane, txSz, txType)
    pos = scan[c]
    if (enc_orig_coef[c]<0) {
      break
    }
    if (enc_orig_coef[c]) {
      v = enc_orig_coef[c]*100+enc_coef_rounding[c]
      v = v*scalefactor/100000
      enc_coef[c] = Max(1,v)
    } else {
      enc_coef[c] = 0
    }
    v = enc_coef[c] & 0xfffff
    v = (v * dqv) & 0xffffff
    v = v >> txScale
    if (enc_sign_bit[c]) {
      v = -v
    }
    dequant[pos] = clamp(v, dqcoeffMin, dqcoeffMax)
  }
  enc_disable_recon = 1  // Don't change the predicted value
  clear_saturation()
  inverse_transform_block(txSz,x,y,plane,txType)
  enc_disable_recon = 0
  return (enc_seen_saturation==0)
}
#endif

#if ENCODE
read_coef ( txSz, token, coef_to_encode )
#else
read_coef ( txSz, token )
#endif
{
  txSzCtx = txsize_sqr_up_map[txSz]
  cat = vp9_extra_bits[token][0]  // Probability category for extra bits
  min_value = vp9_extra_bits[token][2] // Smallest value in this category
  if (cat == 0)
    return token

  if (cat < 6) {
    extra_bits = vp9_extra_bits[token][1]
  } else {
    if (txSzCtx > TX_32X32) txSzCtx = TX_32X32
    extra_bits = bit_depth + 3 + Max(0, txSzCtx - TX_4X4)
    // Round up to a full number of CDFs (each of which 4 bits, except the last one
    // which only covers 2 bits)
    extra_bits = Min(18, (extra_bits + 3) & ~3)
  }

#if ENCODE
  bits_to_encode = coef_to_encode - min_value
#endif
  coef = 0
  count = 0
  cdf_index = 0
  while (count < extra_bits) {
    bits = Min(extra_bits - count, 4)
#if ENCODE
    coef_extra_bits = (bits_to_encode >> count) & ((1 << bits) - 1)
#endif
    coef_extra_bits ae(v)
    coef |= coef_extra_bits << count
    count += bits
    cdf_index += 1
  }
  return min_value + coef
}

uint1 is_2d_transform(txType) {
  return (txType < IDTX)
}

get_quantizer(c, plane, txSz, txType) {
  dqv = plane_dequant[plane][c > 0]

#if VALIDATE_SPEC_QUANT
  validate(dqv)
#endif
  if (using_qmatrix && is_2d_transform(txType) && !lossless_array[segment_id]) {
    qmLevel = qm_levels[segment_id][plane]
#if VALIDATE_SPEC_QUANT
    validate(999)
    validate(qmLevel)
    validate(qm_offset[txSz])
    validate(scan[c])
#endif
    q = Min(NUM_QM_LEVELS-1, qmLevel)
    dqv = ROUND_POWER_OF_TWO(dqv * quantizer_matrix[q][plane_type[plane]][qm_offset[txSz] + scan[c]], AOM_QM_BITS)
  }
  return dqv
}

int get_max_eob(txSz) {
  maxEob = tx_size_wide[txSz] * tx_size_high[txSz]
  if ((txSz == TX_64X64) || (txSz == TX_64X32) || (txSz == TX_32X64)) {
    maxEob = 1024
  }
  if ((txSz == TX_16X64) || (txSz == TX_64X16)) {
    maxEob = 512
  }
  return maxEob
}

int get_txsize_ctx(txSz) {
  return (txsize_sqr_map[txSz] + txsize_sqr_up_map[txSz] + 1) >> 1
}

#if ENCODE
read_golomb(val) {
#else // ENCODE
read_golomb() {
#endif // ENCODE
#if ENCODE
  CHECK(val >= 0, "val must be non negative")
  enc_length = 0
  enc_val = (val+1)
  while (enc_val) {
    enc_val = enc_val >> 1
    enc_length++
  }
  CHECK(enc_length > 0, "must have some length")
#endif // ENCODE

  length = 0
  golomb_length_bit = 0
  while (!golomb_length_bit) {
    length++
#if ENCODE
    golomb_length_bit = (length >= enc_length)
    if (length == 20) {
      ASSERT(golomb_length_bit == 1, "golomb_length_bit must be 1 when length == 20")
    }
#endif //ENCODE
    golomb_length_bit ae(v)
    if (length == 20) {
      CHECK(golomb_length_bit == 1, "golomb_length_bit must be 1 when length == 20")
    }
  }
#if ENCODE
  CHECK(enc_length == length, "Encode length mismatch")
#endif // ENCODE

  x = 1
  for (i = length - 2; i >= 0; i--) {
    x = x<<1
#if ENCODE
    golomb_data_bit = ((val+1)>>i) & 1
#endif // ENCODE
    golomb_data_bit ae(v)
    x = x | golomb_data_bit
  }
#if ENCODE
  CHECK(val == (x-1), "Golomb encode bug")
#endif // ENCODE

  return x - 1
}

uint8 get_adjusted_tx_size(txSz) {
  if (txSz == TX_64X64 || txSz == TX_64X32 || txSz == TX_32X64) {
    return TX_32X32
  }
  if (txSz == TX_16X64) {
    return TX_16X32
  }
  if (txSz == TX_64X16) {
    return TX_32X16
  }
  return txSz
}

get_br_ctx(txSz, plane, blocky, blockx, pos) {
  bwl = tx_size_wide_log2[get_adjusted_tx_size(txSz)]
  width = tx_size_high[get_adjusted_tx_size(txSz)]
  height = tx_size_high[get_adjusted_tx_size(txSz)]
  row = pos >> bwl
  col = pos - (row << bwl)

  int20 nbMag[CONTEXT_MAG_POSITION_NUM]
  ASSERT(CONTEXT_MAG_POSITION_NUM == 3, "Lots of code below makes this assumption")
  nbMag[0] = 0
  nbMag[1] = 0
  nbMag[2] = 0

  txType = get_tx_type(txSz,plane,blocky,blockx)
  txClass = get_tx_class(txType)

  for (idx = 0; idx < CONTEXT_MAG_POSITION_NUM; idx++) {
    refRow = row + mag_ref_offset_with_txclass[txClass][idx][0]
    refCol = col + mag_ref_offset_with_txclass[txClass][idx][1]
    if (refRow >= 0 &&
        refCol >= 0 &&
        refRow < height &&
        refCol < (1 << bwl)) {
      nbPos = (refRow << bwl) + refCol
      nbMag[idx] = dequant[nbPos]
    }
  }

  mag = Min(nbMag[0], COEFF_BASE_RANGE + NUM_BASE_LEVELS + 1) +
        Min(nbMag[1], COEFF_BASE_RANGE + NUM_BASE_LEVELS + 1) +
        Min(nbMag[2], COEFF_BASE_RANGE + NUM_BASE_LEVELS + 1)
  mag = Min((mag + 1) >> 1, 6)
  if (pos == 0) {
      return mag
  }
  if (txClass == 0) {
    if ((row < 2) && (col < 2)) {
      return mag + 7
    }
  } else {
    if (txClass == 1) {
      if (col == 0) {
        return mag + 7
      }
    } else {
      if (txClass == 2) {
        if (row == 0) {
          return mag + 7
        }
      }
    }
  }
  return mag + 14
}

get_nz_map_ctx(txSz, plane, blocky, blockx, pos, c, isEob) {
  bwl = tx_size_wide_log2[get_adjusted_tx_size(txSz)]
  width = 1 << bwl
  height = tx_size_high[get_adjusted_tx_size(txSz)]
  txType = get_tx_type(txSz,plane,blocky,blockx)

  if (isEob) {
    if (c == 0) {
      return SIG_COEF_CONTEXTS - 4
    }
    if (c <= (height << bwl) / 8) {
      return SIG_COEF_CONTEXTS - 3
    }
    if (c <= (height << bwl) / 4) {
      return SIG_COEF_CONTEXTS - 2
    }
    return SIG_COEF_CONTEXTS - 1
  }

  row = pos >> bwl
  col = pos - (row << bwl)
  txClass = get_tx_class(txType)

  mag = 0
  for (idx = 0; idx < SIG_REF_DIFF_OFFSET_NUM; idx++) {
    refRow = row + ((txClass == TX_CLASS_2D) ? sig_ref_diff_offset[idx][0]
                    : ((txClass == TX_CLASS_VERT)
                       ? sig_ref_diff_offset_vert[idx][0]
                       : sig_ref_diff_offset_horiz[idx][0]))
    refCol = col + ((txClass == TX_CLASS_2D)
                    ? sig_ref_diff_offset[idx][1]
                    : ((txClass == TX_CLASS_VERT)
                       ? sig_ref_diff_offset_vert[idx][1]
                       : sig_ref_diff_offset_horiz[idx][1]))
    if (refRow >= 0 &&
        refCol >= 0 &&
        refRow < height &&
        refCol < (1 << bwl)) {
      nbPos = (refRow << bwl) + refCol
      mag += Min(Abs(dequant[nbPos]), 3)
    }
  }

  ctx = Min((mag + 1) >> 1, 4)
  if (txClass == TX_CLASS_2D) {
    if (row == 0 && col == 0) {
      ctxIdxOffset = 0
    } else {
      ctxIdxOffset = ctx + nz_map_ctx_offset[txSz][Min(row, 4)][Min(col, 4)]
    }
  } else {
    idx = (txClass == TX_CLASS_VERT) ? row : col
    ctxIdxOffset = ctx + nz_map_pos_ctx_offset[Min(idx, 2)]
  }
  return ctxIdxOffset
}

get_tx_class(txType) {
  if ((txType == V_DCT) ||
      (txType == V_ADST) ||
      (txType == V_FLIPADST)) {
    return TX_CLASS_VERT
   } else if ((txType == H_DCT) ||
              (txType == H_ADST) ||
              (txType == H_FLIPADST)) {
    return TX_CLASS_HORIZ
  } else
    return TX_CLASS_2D
}

// Fill in a block of the txk_type array
write_txk_block(blocky, blockx, txSz, txType) {
  for (i=0; i<tx_size_wide_unit[txSz]; i++) {
    for (j=0; j<tx_size_high_unit[txSz]; j++) {
      txk_type[blockx+i][blocky+j] = txType
    }
  }
}

#if ENCODE
// A list of 4-element coefficient sets which exercise
// the limits of the various intermediate values in the iwht4.
// Note that all of the values here are -(1<<(bd+1)) or (1<<(bd+1))-1,
// which is due to the structure of the iwht4.
// These values are the quantized coefficients, assuming
// that dqv == 4.
int32 extreme_coeff_block_wht4x4_10bit[22][16] = {
  { -4096,      0,      0,      0,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -2,  -4094,      0,      0,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -2,      0,  -4094,      0,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -2,      0,      0,  -4094,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  { -2048,  -2048,      0,      0,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -1,      1,      0,      0,   0, 0, 0, 0,       0,      0,  -2047,   2047,   0, 0, 0, 0},
  { -2049,  -2047,   2047,  -2047,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  { -1024,  -1024,   1024,   1024,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  { -1024,   1024,   1024,  -1024,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  { -1024,  -1024,  -1024,  -1024,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  { -1024,   1024,  -1024,   1024,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -2,      0,      0,      0,   0, 0, 0, 0,    4094,      0,      0,      0,   0, 0, 0, 0},
  {    -2,      0,      0,      0,   0, 0, 0, 0,       0,   4094,      0,      0,   0, 0, 0, 0},
  {    -2,      0,      0,      0,   0, 0, 0, 0,       0,      0,   4094,      0,   0, 0, 0, 0},
  {    -2,      0,      0,      0,   0, 0, 0, 0,       0,      0,      0,   4094,   0, 0, 0, 0},
  {    -1,     -1,      0,      0,   0, 0, 0, 0,    2047,   2047,      0,      0,   0, 0, 0, 0},
  {    -1,      1,   2047,  -2047,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -2,      0,      0,      0,   0, 0, 0, 0,    2047,   2047,  -2047,   2047,   0, 0, 0, 0},
  {     0,     -1,      0,      0,   0, 0, 0, 0,    1024,   1023,  -1024,  -1024,   0, 0, 0, 0},
  {     0,      0,      0,     -1,   0, 0, 0, 0,    1024,  -1024,  -1024,   1023,   0, 0, 0, 0},
  {     0,     -1,     -1,     -1,   0, 0, 0, 0,    1024,   1023,   1023,   1023,   0, 0, 0, 0},
  {     0,      0,     -1,      0,   0, 0, 0, 0,    1024,  -1024,   1023,  -1024,   0, 0, 0, 0},
};

int32 extreme_coeff_block_wht4x4_12bit[22][16] = {
  {-16384,      0,      0,      0,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -2, -16382,      0,      0,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -2,      0, -16382,      0,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -2,      0,      0, -16382,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  { -8192,  -8192,      0,      0,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -1,      1,      0,      0,   0, 0, 0, 0,       0,      0,  -8191,   8191,   0, 0, 0, 0},
  { -8193,  -8191,   8191,  -8191,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  { -4096,  -4096,   4096,   4096,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  { -4096,   4096,   4096,  -4096,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  { -4096,  -4096,  -4096,  -4096,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  { -4096,   4096,  -4096,   4096,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -2,      0,      0,      0,   0, 0, 0, 0,   16382,      0,      0,      0,   0, 0, 0, 0},
  {    -2,      0,      0,      0,   0, 0, 0, 0,       0,  16382,      0,      0,   0, 0, 0, 0},
  {    -2,      0,      0,      0,   0, 0, 0, 0,       0,      0,  16382,      0,   0, 0, 0, 0},
  {    -2,      0,      0,      0,   0, 0, 0, 0,       0,      0,      0,  16382,   0, 0, 0, 0},
  {    -1,     -1,      0,      0,   0, 0, 0, 0,    8191,   8191,      0,      0,   0, 0, 0, 0},
  {    -1,      1,   8191,  -8191,   0, 0, 0, 0,       0,      0,      0,      0,   0, 0, 0, 0},
  {    -2,      0,      0,      0,   0, 0, 0, 0,    8191,   8191,  -8191,   8191,   0, 0, 0, 0},
  {     0,     -1,      0,      0,   0, 0, 0, 0,    4096,   4095,  -4096,  -4096,   0, 0, 0, 0},
  {     0,      0,      0,     -1,   0, 0, 0, 0,    4096,  -4096,  -4096,   4095,   0, 0, 0, 0},
  {     0,     -1,     -1,     -1,   0, 0, 0, 0,    4096,   4095,   4095,   4095,   0, 0, 0, 0},
  {     0,      0,     -1,      0,   0, 0, 0, 0,    4096,  -4096,   4095,  -4096,   0, 0, 0, 0},
};

// Similar, but for the idct4.
// Note: in av1bsrc, *all* transforms except for the iwht4, iadst4,
// and identity transforms, are expressed in terms of the B() and H()
// functions.
// Thus to hit the most extreme values, we only need to consider
// one transform, and the idct4 is the simplest.
int32 extreme_coeff_block_dct4x4_10bit[4][4] = {
  {-13578, -10072,  32768,  31299},
  { 13577,   7601, -32768, -32322},
  {-32768,  22920, -13578,  30305},
  { 13577, -23464,  32768, -28992},
};

int32 extreme_coeff_block_dct4x4_12bit[5][4] = {
  {-54312, -28312,  131072,  130155},
  { 54312,  88869,  131071,  128009},
  {-54311, -87846, -131072, -130480},
  {-54312,  29625,  131072, -129611},
  { 54311, -28312, -131072,  130155},
};

// The Hadamard1 range maximum can't actually be hit with a 4x4
// transform, because we need a different angle from the butterflies.
// However, we can hit it with an 8x8 transform. Here are canned
// vectors for 10 bits and 12 bits.
int32 extreme_coeff_block_adst8x8_10bit[1][8] = {
  { 12292, 0, 0, 31668, 16385, 0, 0, 31711 }
};

int32 extreme_coeff_block_adst8x8_12bit[1][8] = {
  { 89786, 0, 0, 129558, 63130, 0, 0, 122880 }
};

// Similar but for the iadst4, which has many intermediates.
int32 extreme_coeff_block_adst4x4_10bit[36 + 36][16] = {
  // Row transforms
  {    15567,        0,   -31419,   -14219, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // a7   =       187944
  {   -15570,        0,    31416,    14218, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // a7   =      -187944
  {        0,        0,        0,    32768, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // b7   =       131071
  {   -32768,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // b7   =      -131072
  {    32768,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0   =    173144791
  {   -32768,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0   =   -173146112
  {    14042,        0,    30415,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0_1 =    536870908
  {   -14969,        0,   -30093,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0_1 =   -536870912
  {    10194,        0,    10366,    32768, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0_2 =    536870910
  {        0,        0,   -17760,   -26864, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0_2 =   -536870912
  {    32768,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1   =    325318222
  {   -32768,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1   =   -325320704
  {    32768,        0,   -10321,   -10321, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1_1 =    379854386
  {   -32768,        0,    10321,    10321, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1_1 =   -379856868
  {    32768,        0,   -10194,   -10366, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1_2 =    536870910
  {   -26864,        0,        0,    17760, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1_2 =   -536870912
  {        0,    32768,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s2   =    438301424
  {        0,   -32768,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s2   =   -438304768
  {        0,        0,        0,    32768, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s2_1 =    438301424
  {   -32768,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s2_1 =   -438304768
  {        0,        0,    32768,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s3   =    498463013
  {    -4096,        0,   -32768,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s3   =   -498466816
  {        0,        0,    32768,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s4   =    173144791
  {    -4096,        0,   -32768,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s4   =   -173146112
  {        0,        0,        0,    32768, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s5   =    325318222
  {        0,        0,        0,   -32768, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s5   =   -325320704
  {        0,        0,        0,    32768, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s6   =    498463013
  {        0,        0,        0,   -32768, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s6   =   -498466816
  {    32768,    16389,     6158,     5120, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x0   =    536870911
  {        0,   -21504,   -16384,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x0   =   -536870912
  {    32768,     8642,        0,    -6308, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x1   =    536870910
  {        0,   -21504,        0,    16384, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x1   =   -536870912
  {    32768,        0,     -782,    -8738, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x3   =    536870909
  {   -17760,        0,   -26864,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x3   =   -536870912
  {     6308,    -8642,    32768,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x3_1 =    536870910
  {   -16384,    21504,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }  // x3_1 =   -536870912
  // Column transforms
  {       30,     -941,    -2032,    -2057,        0,        0,        0,        0,
        2320,     3580,     2731,     2720,        7,     4336,       11,        4 }, // a7 = -46986
  {     -391,    -3205,     6487,     1009,        0,        0,        0,        0,
          10,    -2971,    -5851,        0,        0,      -16,    -3301,     -787 }, // a7 = 46986
  {     1254,     9526,    14336,     5046,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // b7 = 32767
  {        0,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,   -18396,        0 }, // b7 = -32768
  {     3327,     1023,    -1020,     2638,        0,        0,        0,        0,
        1787,     6980,     1787,        0,        0,        0,        0,        0 }, // s0_1 = 134217727
  {      222,    -1825,    -2503,        0,        0,        0,        0,        0,
         795,        0,    -8379,        0,        0,        0,        0,        0 }, // s0_1 = -134217728
  {       72,     2660,        0,        0,        0,     -633,      389,      257,
          33,     3133,        0,      241,        1,    12938,      -88,     -244 }, // s0_2 = 134217727
  {     2837,      990,    -1247,    -3971,       12,      159,       71,      111,
           1,    -4794,    -4761,      248,     2558,    -2751,        0,     2345 }, // s0_2 = -134217728
  {        0,    16384,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s0 = 43285207
  {        0,   -32239,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s0 = -43286528
  {     1607,     2487,     5149,     1423,    -2353,     -581,     -401,     1952,
         730,     8701,        2,   -16373,       23,     -820,    -2076,       15 }, // s1_1 = 94963056
  {        6,    -4056,    -8897,    -6959,     1416,        0,       28,        3,
      -13121,    13675,   -10645,     9128,      256,       24,      768,     2913 }, // s1_1 = -94964217
  {        0,        0,        0,        0,    -1591,        0,      370,        0,
         472,     -724,    -3390,      125,   -15875,     5418,    -8400,     1472 }, // s1_2 = 134217727
  {     9926,      303,   -13070,     3252,        0,        0,        0,        0,
           0,        0,        0,        0,     1024,    -1029,     4490,     1289 }, // s1_2 = -134217728
  {        0,    16384,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s1 = 81327694
  {        0,   -19770,        0,   -17384,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s1 = -81330176
  {        0,        0,        0,        0,    30251,        1,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s2 = 109572848
  {        0,        0,        0,        0,        0,  -131072,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s2 = -109576192
  {     1663,    -1024,     2432,      102,        0,      -11,        1,       13,
      -14332,   -10034,     6144,    -1787,    -2825,     1737,    -2689,      -51 }, // s2_1 = 109572848
  {     1792,    -1256,     -214,     1580,    -1017,    -2576,     1142,     1007,
        2716,     2430,     9475,     6759,     -808,     -840,     3344,    -4076 }, // s2_1 = -109576192
  {        0,        0,        0,        0,        0,        0,        0,        0,
         137,     9980,        0,        0,        0,        0,        0,        0 }, // s3 = 124612901
  {        0,        0,        0,        0,     1915,     2281,        0,     -261,
       -2182,        0,    -8191,        0,     1536,        5,    -1332,      236 }, // s3 = -124616704
  {        0,        0,        0,        0,        0,        0,        0,        0,
       26624,        0,        0,        0,        0,        0,        0,        0 }, // s4 = 43285207
  {    -1024,       62,        0,      341,        0,        0,        0,        0,
      -16128,   -16849,        0,   -16518,        0,        0,        0,        0 }, // s4 = -43286528
  {        0,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,    28179,        0,        0,        0 }, // s5 = 81327694
  {        0,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        1,   -32639,      657,      256 }, // s5 = -81330176
  {        0,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,    26856,       20,     1280,    -1913 }, // s6 = 124612901
  {        0,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,     6144,        0,     4170,   -27170 }, // s6 = -124616704
  {        0,     2919,      121,      121,        0,        0,        0,        0,
          41,     9691,        0,        0,        0,        0,        9,        1 }, // x0 = 134217727
  {    -2040,    -2887,     1238,      143,    -7116,     3448,    -5836,     1280,
       -4875,    -4539,      849,    -3182,     3552,     2046,        0,        0 }, // x0 = -134217728
  {       24,     -980,      259,      485,        0,        0,        0,        0,
         569,    -3044,        0,        0,        2,   -27591,        0,        0 }, // x1 = 134217727
  {        0,        0,        0,        0,     1303,   -20607,     9719,     5574,
           7,     2538,     1261,     1254,        0,        6,     2547,     2547 }, // x1 = -134217728
  {     1665,    -1692,     -116,     -441,     4117,     3077,       24,     4879,
           0,        0,        0,        0,     1929,     3034,     1649,        0 }, // x3_1 = -134217728
  {        0,    -1528,     1230,     1617,     -448,    -1363,     -428,    -1998,
       -5704,    25242,     1983,    17707,    -1758,     -867,       -2,     1713 }, // x3_1 = 134217727
  {     4368,     2552,     3865,      863,      259,        1,        0,      143,
           0,        0,        0,        0,       10,    -4304,        0,        0 }, // x3 = 134217727
  {    -1168,    -3004,    -2205,    -1279,        0,     -256,     -310,        0,
           5,   -24574,    16384,        8,        0,       36,        0,        0 }, // x3 = -134217728
};

int32 extreme_coeff_block_adst4x4_12bit[36 + 36][16] = {
  // Row transforms
  {    62268,        0,  -125679,   -56876, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // a7   =       751788
  {   -62267,        0,   125680,    56875, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // a7   =      -751788
  {        0,        0,        0,   131072, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // b7   =       524287
  {  -131072,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // b7   =      -524288
  {   131072,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0   =    692583127
  {  -131072,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0   =   -692584448
  {    62752,        0,   119373,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0_1 =   2147483644
  {   -52270,        0,  -123014,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0_1 =  -2147483648
  {    12694,        0,    51218,   131072, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0_2 =   2147483646
  {        0,        0,   -98342,   -65623, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s0_2 =  -2147483648
  {   131072,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1   =   1301280334
  {  -131072,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1   =  -1301282816
  {   131072,        0,   -41286,   -41286, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1_1 =   1519435558
  {  -131072,        0,    41286,    41286, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1_1 =  -1519438040
  {   131072,        0,   -12694,   -51218, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1_2 =   2147483646
  {   -65623,        0,        0,    98342, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s1_2 =  -2147483648
  {        0,   131072,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s2   =   1753215728
  {        0,  -131072,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s2   =  -1753219072
  {        0,        0,        0,   131072, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s2_1 =   1753215728
  {  -131072,        0,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s2_1 =  -1753219072
  {        0,        0,   131072,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s3   =   1993863461
  {    -8192,        0,  -131072,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s3   =  -1993867264
  {        0,        0,   131072,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s4   =    692583127
  {    -8192,        0,  -131072,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s4   =   -692584448
  {        0,        0,        0,   131072, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s5   =   1301280334
  {        0,        0,        0,  -131072, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s5   =  -1301282816
  {        0,        0,        0,   131072, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s6   =   1993863461
  {        0,        0,        0,  -131072, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // s6   =  -1993867264
  {   131072,    81920,    16422,    11012, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x0   =   2147483647
  {        0,   -86016,   -65536,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x0   =  -2147483648
  {   131072,    65603,     5924,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x1   =   2147483646
  {        0,   -86016,        0,    65536, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x1   =  -2147483648
  {   131072,        0,    11008,    -8390, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x3   =   2147483645
  {   -98342,        0,   -65623,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x3   =  -2147483648
  {        0,   -65603,   131072,     5924, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }, // x3_1 =   2147483646
  {   -65536,    86016,        0,        0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0 }  // x3_1 =  -2147483648
  // Column transforms
  {    -1174,    -8055,    -6181,    -4742,    17927,     4839,   -16059,     8545,
        3632,    13082,    15895,     7938,   -32368,    67469,   -32768,        0 }, // a7 = -187947
  {     3580,     5281,     7997,     4416,        0,        0,     2886,    -4422,
      -52951,   -49152,       46,    42483,      213,    -3012,   -14337,     2447 }, // a7 = 187947
  {   101603,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // b7 = 131071
  {        0,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,  -524032,        0,        0 }, // b7 = -131072
  {      123,      543,     7731,     6971,        0,    -3072,        0,        0,
       31245,     4078,    11942,    11008,       28,      -11,        0,        0 }, // s0_1 = 536870911
  {     1685,    -4659,    -5443,        0,        0,        0,        0,        0,
       24510,     3246,   -41067,    -7971,     2735,      993,     2735,        0 }, // s0_1 = -536870912
  {        0,       31,    21529,    38617,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s0 = 173144791
  {   -39123,    16932,       50,   -72091,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s0 = -173146112
  {     9941,     7273,        0,        0,        0,        0,        0,        0,
         704,    20174,     1722,      160,    14396,     9714,      103,    13794 }, // s0_2 = 536870911
  {        0,        0,        0,        0,        0,        0,        0,        0,
      -20620,     7842,   -13513,   -30723,      509,    -7629,      277,     -232 }, // s0_2 = -536870912
  {    16256,    33811,    12843,    28401,        0,        0,        0,        0,
         732,    -5007,    -6717,     -385,     8668,     4018,    -8192,   -14508 }, // s1_1 = 379857028
  {    27039,   -44307,   -85697,     8447,        0,        0,        0,        0,
         274,     2236,     3709,     8192,      474,        5,     4933,     9216 }, // s1_1 = -379859510
  {     1280,    13559,   -12256,     1543,        0,        0,        0,        0,
        1613,     4096,    -4205,    -8709,   -17646,   -20003,   -17655,        0 }, // s1_2 = 536870911
  {     3077,   -14012,    -6145,      844,        0,        0,        0,        0,
        2566,    36480,        0,        0,     5068,    12426,        0,     4411 }, // s1_2 = -536870912
  {   110592,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s1 = 325318222
  {        0,  -131056,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s1 = -325320704
  {        0,        0,        0,        0,        0,    40290,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s2 = 438301424
  {        0,        0,        0,        0,        0,        0,        0,  -524284,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s2 = -438304768
  {    -2348,        0,    36108,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s2_1 = 438301424
  {    -7383,        0,   -32768,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // s2_1 = -438304768
  {        0,        0,        0,        0,        0,        0,        0,        0,
      101412,    79891,        0,        0,        0,        0,        0,        0 }, // s3 = 498463013
  {        0,        0,        0,        0,        0,     -380,        0,        0,
           0,  -122916,        0,        0,       64,    -4135,        0,        0 }, // s3 = -498466816
  {        0,        0,        0,        0,        0,        0,        0,        0,
       49152,    24576,        0,        0,        0,        0,        0,        0 }, // s4 = 173144791
  {        0,    -1024,        0,        0,        0,        0,        0,        0,
           0,  -130432,        0,        0,        0,        0,        0,        0 }, // s4 = -173146112
  {        0,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,      512,    39941,        0,        0 }, // s5 = 325318222
  {        0,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,  -524160,        0,        0 }, // s5 = -325320704
  {        0,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,     2911,     8192,    36183,    33692 }, // s6 = 498463013
  {        0,        0,        0,        0,        0,        0,        0,        0,
           0,        0,        0,        0,        0,   -77157,    29862,   -34106 }, // s6 = -498466816
  {        0,        0,       35,       52,      974,    10049,    14830,     4320,
       23184,    16320,    -8002,        0,        0,        0,        0,        0 }, // x0 = 536870911
  {     2047,        0,     2386,      509,       10,    -5530,   -16948,   -13519,
        8424,   -40881,    -6128,    41185,        5,       -2,        0,        0 }, // x0 = -536870912
  {    12722,    -8575,    23883,    11161,    39168,     -258,   -12017,    33051,
        2479,        0,        0,        0,        1,      129,        0,       67 }, // x1 = 536870911
  {        0,        0,        0,        0,   100112,   -49585,        0,    11027,
        4082,     4779,     8122,     4045,        7,    14724,    12268,     8690 }, // x1 = -536870912
  {        0,        0,     9529,        0,   -93254,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // x3_1 = 536870911
  {        0,        0,   -10443,        0,    90261,        0,        0,        0,
           0,        0,        0,        0,        0,        0,        0,        0 }, // x3_1 = -536870912
  {    -2399,    10034,    10352,    10832,      450,     3141,        0,        0,
       13121,        0,        0,    15321,      421,   -10317,        0,        0 }, // x3 = 536870911
  {   -20060,   -14322,   -16993,     3078,        0,        0,        0,        0,
           0,        0,        0,        0,        0,    11348,        0,        0 }, // x3 = -536870912
};

int enc_num_special_coeff_sets(rowTxfm, txType, txSz) {
  if (txSz == TX_8X8 && rowTxfm == TXFM_ADST) return 1
  if (txSz != TX_4X4) return 0
  if (rowTxfm == TXFM_WHT) return 22
  if (rowTxfm == TXFM_DCT) return (bit_depth == 12) ? 5 : 4
  // Only return the column vectors if this is ADST in both directions
  // (the coefficients might cause other column transforms to overflow)
  if (rowTxfm == TXFM_ADST) return (txType == ADST_ADST) ? 36 + 36 : 36
  return 0
}

int enc_special_coeff_set_length(rowTxfm, txSz) {
  if (rowTxfm == TXFM_WHT) {
    return 16
  } else if (rowTxfm == TXFM_DCT) {
    return 4
  } else if (rowTxfm == TXFM_ADST) {
    return (txSz == TX_4X4) ? 16 : 8
  } else {
    return 0
  }
}

int enc_get_special_coeff(rowTxfm, txSz, set, pos) {
  ASSERT(bit_depth == 10 || bit_depth == 12, "Unexpected bit depth")

  if (txSz == TX_4X4 && rowTxfm == TXFM_WHT) {
    return ((bit_depth == 10) ?
            extreme_coeff_block_wht4x4_10bit[set][pos] :
            extreme_coeff_block_wht4x4_12bit[set][pos])
  } else if (txSz == TX_4X4 && rowTxfm == TXFM_DCT) {
    return ((bit_depth == 10) ?
            extreme_coeff_block_dct4x4_10bit[set][pos] :
            extreme_coeff_block_dct4x4_12bit[set][pos])
  } else if (txSz == TX_4X4 && rowTxfm == TXFM_ADST) {
    return ((bit_depth == 10) ?
            extreme_coeff_block_adst4x4_10bit[set][pos] :
            extreme_coeff_block_adst4x4_12bit[set][pos])
  } else if (txSz == TX_8X8 && rowTxfm == TXFM_ADST) {
    return ((bit_depth == 10) ?
            extreme_coeff_block_adst8x8_10bit[set][pos] :
            extreme_coeff_block_adst8x8_12bit[set][pos])
  } else {
    ASSERT(0, "Unexpected 1D transform type")
  }
}

// Returns whether we're using a special block or not
uint1 enc_special_coeff_block(txSz,x,y,plane,txType) {
  enc_use_special_coeffs u(0)
  if (bit_depth == 8 || !enc_use_special_coeffs) {
    return 0
  }

  // The coefficient blocks above are all generated assuming that
  // the quantizer is set to 4 for all coefficients.
  // This can be true or false for both lossy and lossless blocks,
  // so we need to explicitly check here.
  if (plane_dequant[plane][0] != 4 || plane_dequant[plane][1] != 4) {
    return 0
  }

  rowTxfm = lossless ? TXFM_WHT : row_txfm[txType]

  // Finally, we also need to check that we're not using a quantizer
  // matrix, as that can change the quantizer of later coefficients.
  // Quantizer matrices are never used for lossless blocks, even if
  // using_qmatrix is set, so we don't need to check in that case.
  if (!lossless && using_qmatrix) {
    return 0
  }

  numSets = enc_num_special_coeff_sets(rowTxfm, txType, txSz)
  if (numSets == 0) return 0

  setLength = enc_special_coeff_set_length(rowTxfm, txSz)

  dqcoeffMin = -(1 << (7 + bit_depth))
  dqcoeffMax = (1 << (7 + bit_depth)) - 1
  dqv = 4

  // Set the top row of the coefficient block to one of the preset rows
  // defined above, and set the rest of the coefficient block to zero.
  enc_last_coef = (txSz == TX_8X8) ? 64 : 16
  set = Choose(0, numSets-1)
  c_eob = 0 // Track EOB as we go

  for (c = 0; c < enc_last_coef; c++) {
    pos = scan[c]
    q = (pos < setLength) ? enc_get_special_coeff(rowTxfm, txSz, set, pos) : 0
    enc_coef[c] = Abs(q)
    enc_sign_bit[c] = (q < 0) ? 1 : 0
    dequant[pos] = clamp(q*dqv, dqcoeffMin, dqcoeffMax)
    enc_dequant[pos] = dequant[pos]
    if (q != 0) {
      c_eob = c + 1
    }
  }

  // Check that we don't overflow when inverse transforming
  enc_disable_recon = 1  // Don't change the predicted value
  clear_saturation()
  inverse_transform_block(txSz,x,y,plane,txType)
  enc_disable_recon = 0
  ASSERT(enc_seen_saturation==0, "Unexpected overflow in preset coeff block")

  return 1
}

enc_pick_coeffs(bsize,txSz,x,y,plane,blocky,blockx) {
  adjTxSize = get_adjusted_tx_size(txSz)

  dqcoeffMin = -(1 << (7 + bit_depth))
  dqcoeffMax = (1 << (7 + bit_depth)) - 1
  maxdequant = 0x8000<<(bit_depth-8)
  maxposdequant = (0x8000<<(bit_depth-8))-1
  maxnegdequant = maxdequant

  txSzCtx = get_txsize_ctx(txSz)
  ptype = plane_type[plane]
  txScale = tx_scale_lookup[txSz]
  denom = 1 << txScale
  seg_eob = get_max_eob(txSz) // Number of coefficients in the block

  if (plane == 0) {
    enc_tx_type = enc_pick_tx_type(bsize,blocky,blockx,txSz)
    txType = enc_tx_type

    // At this point, we fill in the txk_type array. This doesn't look
    // quite the same as the spec (which writes to the TxTypes array
    // just after reading the transform type), but we have to do it
    // here because if we try to pick coefficients with
    // enc_choose_coef_from_cdf, that function will need to look up
    // the transform type from the array. Note that we'll have to fix
    // this up to DCT_DCT later if we choose no transform coefficients
    // ("all_zero").
    write_txk_block (blocky, blockx, txSz, txType)

  } else {
    // We should have already written the tx type for this block, so fetch
    // it the normal way
    txType = get_tx_type(txSz,plane,blocky,blockx)
  }

  w4 = tx_size_wide_unit[txSz]
  h4 = tx_size_high_unit[txSz]

  enc_specify_coeffs      u(0)
  enc_last_coef           u(0) // How many coefficients are allowed to be nonzero

  log2W = tx_size_wide_log2[txSz]
  log2H = tx_size_high_log2[txSz]
  block_width = 1 << log2W
  block_height = 1 << log2H
  tx_width = Min (32, block_width)
  tx_height = Min (32, block_height)

  get_scan(txSz,txType)

  uint64 max_scale
  uint64 dqv

  enc_allow_retry_shift u(0)
  enc_allow_retry_bias  u(0)

  if (enc_specify_coeffs) {
      enc_push_coefs        = 0
      enc_use_large_coeffs  = 0
      enc_coef_sign_pattern = 0
      enc_coef_scale        = 0
  } else {
      enc_push_coefs        u(0)
      enc_use_large_coefs   u(0) // Make a small % of blocks use large coefficients
      enc_coef_sign_pattern u(0) // Choose between picking each coefficient sign randomly,
                                 // or pre-selecting a pattern of signs across the block
      enc_coef_scale        u(0)
  }
  // Keep trying until we manage to transform without triggering saturation
  retry_shift = 0
  retry_bias = 0

  // Special cases for 4x4 or 8x8 transforms
  if (! enc_specify_coeffs) {
      if (enc_special_coeff_block (txSz,x,y,plane,txType)) {
          return 0
      }
  }

  // If using patterned signs, we select a value {-1 / 0 / +1} for each row
  // and each column. Then each coefficient is effectively set to
  // row_sign[row] * col_sign[col] * <random unsigned coeff>.
  // Note that a value of 0 forces a whole row or column to be zero, so we
  // select this option more rarely than +/- 1
  if (enc_coef_sign_pattern && !enc_specify_coeffs) {
    for (i = 0; i < tx_size_high[adjTxSize]; i++) {
      enc_row_sign[i] u(0)
    }
    for (i = 0; i < tx_size_wide[adjTxSize]; i++) {
      enc_col_sign[i] u(0)
    }
  }

  if (enc_push_coefs && !enc_specify_coeffs) {
    // First generate a set of coefficients
    while(1) {
      enc_last_zero_coef_prob = enc_zero_coef_prob
      enc_zero_coef_prob = 1 // 1% of zero DC coef
      for(c=0;c<seg_eob;c++) {
        dqv = get_quantizer(c, plane, txSz, txType)
        enc_sign_bit[c] u(0)
        // Allow slightly out-of-range values in order to test the clamping
        // of dequantized coefficients
        if (enc_sign_bit[c])
          max_coef = ((maxnegdequant) * denom + denom - 1) / dqv + 1
        else
          max_coef = ((maxposdequant) * denom + denom - 1) / dqv + 1
        adjusted_max_coef = Max(0,(max_coef>>retry_shift)-retry_bias)
        enc_coef_rounding[c] u(0)
        enc_coef[c] u(0)
        if (c==0) {
          enc_zero_coef_prob = enc_last_zero_coef_prob
        }
        if (enc_coef[c]<0) {
          enc_orig_coef[c] = enc_coef[c]
          break
        }
        // We have two transformations available, which preserve the dequantized
        // coefficients (and so interact well with our process of starting out
        // with large coefficients then reducing the magnitude until we stop
        // overflowing), but which change the value of enc_coef[c].
        // These are:
        // i) Adding 0x1000000 to enc_coef[c]*dqv
        // ii) Adding 0x100000 to enc_coef[c]
        //
        // We need to be careful to make sure that these transformations interact
        // correctly. There are three things to pay attention to:
        //
        // * We apply transform (i) first, and make sure not to allow enc_coef[c]
        //   to exceed 0xfffff. Then we apply transform (ii). This avoids unintentionally
        //   changing the dequantized value due to the wrapping of enc_coef[c].
        //
        // * Further, when applying transform (i), we *must* round up. This is because
        //   if we round down, we can end up with enc_coef[c]*dqv just below a multiple
        //   of 0x1000000. This will lead to *very* different coefficients than intended!
        //
        // * Finally, the maximum coded value is only slightly larger than 0x100000,
        //   so we can only apply the second transformation to small coefficients.
        //   TODO: Figure out if it's actually possible to apply both transformations together -
        //   from some testing it seems like we can't.
        //
        // These transformations tend to cause huge coefficient values, which take quite
        // a lot of bits to encode. To prevent this becoming too much of a problem, we:
        // i) Limit to applying each transformation at most 3 times to any single coefficient
        // ii) Set the probability of each transformation to 1 per 20000 coefficients.
        //     But we still want to test applying both transformations, so we boost the
        //     probability of this case to 1 per 40000 coefficients
        if (Choose(0, 9999) == 0) {
          if (ChooseBit()) {
            // Try to apply transformation (i) - note that we might be limited in how
            // many times we can apply this due to the interaction with transformation (ii)
            max_scale = ((0xfffff - enc_coef[c]) * dqv - (dqv-1)) / 0x1000000
            if (max_scale >= 1) {
              enc_coef[c] += (0x1000000 * ChoosePowerLaw(max_scale) + (dqv-1)) / dqv
            }
          }
          if (ChooseBit()) {
            // Try to apply transformation (ii)
            maxValue = 0xfffff + (COEFF_BASE_RANGE + 1 + NUM_BASE_LEVELS) - 1
            if (enc_coef[c] + 0x100000 <= maxValue) {
              enc_coef[c] += 0x100000
            }
          }
        }
        enc_orig_coef[c] = enc_coef[c]
      }
      // Now try to find a scaling that results in a legal set of coefficients
      // will multiply by scale/100
      low=1
      high=50000 // value is coef*100/100k = coef*scale/1000, 50k for high allows becoming 50 times greater
      if (test_scale(low,txSz,x,y,plane,seg_eob,dqcoeffMin,dqcoeffMax,txScale,txType))
        break
      if (enc_zero_coef_prob<90) {
        enc_zero_coef_prob += 1
      }
    }
    if (test_scale(high,txSz,x,y,plane,seg_eob,dqcoeffMin,dqcoeffMax,txScale,txType)) {
      // No overflow even with high, just use original values
      low = 1000
    } else {
      while (high>low+1) {
        mid = low+((high-low)>>1)
        if (test_scale(mid,txSz,x,y,plane,seg_eob,dqcoeffMin,dqcoeffMax,txScale,txType)) {
          low = mid
        } else {
          high = mid
        }
      }
    }
    //:print low
    CHECK(test_scale(low,txSz,x,y,plane,seg_eob,dqcoeffMin,dqcoeffMax,txScale,txType),"Unexpected saturation for low value")
  } else { // not enc_push_coefs
    enc_orig_zero_coef_prob = enc_zero_coef_prob
    while(1) {
      // Generate coefficients
      stop_retry_bias = 1
      for (i=0; i<seg_eob; i++) {
        dequant[i] = 0
      }
      for(c=0;c<seg_eob;c++) {
        dqv = get_quantizer(c, plane, txSz, txType)
        pos = scan[c]

        if (enc_specify_coeffs) {
          if (c >= enc_last_coef) {
            enc_coef[c] = -1
            break
          }

          enc_quantized_coeff u(0)

          enc_sign_bit[c] = enc_quantized_coeff < 0
          enc_coef[c] = enc_sign_bit[c] ? -enc_quantized_coeff : enc_quantized_coeff
        } else {
          enc_sign_bit[c] u(0)

          // Allow slightly out-of-range values in order to test the clamping
          // of dequantized coefficients
          if (enc_sign_bit[c])
            max_coef = ((maxnegdequant) * denom + denom - 1) / dqv + 1
          else
            max_coef = ((maxposdequant) * denom + denom - 1) / dqv + 1
          adjusted_max_coef = Max(0,(max_coef>>retry_shift)-retry_bias)
          if (adjusted_max_coef > 1)
            stop_retry_bias = 0

          enc_coef[c] u(0)

          if (enc_coef[c]<0) {
            break
          }

          // Apply transformations to enc_coef[c] - see the large comment above for details.
          if (Choose (0, 999999) < enc_tx_coeffs_per_million) {
            if (ChooseBit()) {
              max_scale = ((0xfffff - enc_coef[c]) * dqv - (dqv-1)) / 0x1000000
              max_scale = Min(max_scale, 3)
              if (max_scale >= 1) {
                enc_coef[c] += (0x1000000 * ChoosePowerLaw(max_scale) + (dqv-1)) / dqv
              }
            }
            if (ChooseBit()) {
              maxValue = 0xfffff + (COEFF_BASE_RANGE + 1 + NUM_BASE_LEVELS) - 1
              if (enc_coef[c] + 0x100000 <= maxValue) {
                enc_coef[c] += 0x100000
              }
            }
          }
        }

        v = enc_coef[c] & 0xfffff
        v = (v * dqv) & 0xffffff
        v = v >> txScale
        if (enc_sign_bit[c]) {
          v = -v
        }
        dequant[pos] = clamp(v, dqcoeffMin, dqcoeffMax)
      }
      enc_disable_recon = 1  // Don't change the predicted value
      clear_saturation()
      inverse_transform_block(txSz,x,y,plane,txType)
      enc_disable_recon = 0
      if (enc_seen_saturation==0)
        break
      if (enc_allow_retry_shift && retry_shift<16) {
        retry_shift += 1
      } else if (enc_allow_retry_bias && !stop_retry_bias) {
        retry_bias += 1
      } else if (enc_zero_coef_prob<90) {
        enc_zero_coef_prob += 1
      }

      CHECK(!enc_specify_coeffs, "The coefficients set for the block at (%d, %d) in plane %d are invalid (they saturated the inverse transform).", x, y, plane)

      //v=max_coef
      //w=plane_dequant[plane][0]
      //u=plane_dequant[plane][1]
      //:print 'retry transform',x,y,plane,v,w,u
      //:C printf( "retry transform %d %d %d %d %d %d\n",x,y,plane,v,w,u);
    }
    enc_zero_coef_prob = enc_orig_zero_coef_prob
  }

  // Calculate the EOB value we need to code
  // This may be zero, in which case we set the all_zero symbol to 1
  c_eob = 0
  for(c=0;c<seg_eob;c++) {
    if (enc_coef[c]<0) {
      break
    }
    if (enc_coef[c]>0) {
      c_eob = c + 1
    }
  }
  for (i=0;i<seg_eob;i++) {
    enc_dequant[i] = dequant[i]
  }
}
#endif

read_coeffs_txb(bsize,txSz,x,y,plane,blocky,blockx) {
  int64_t dqv
  int64_t v
#if VALIDATE_SPEC_STRUCTURE
  validate(30000)
  validate(plane)
  validate(x)
  validate(y)
  validate(txSz)
#endif
  dqcoeffMin = -(1 << (7 + bit_depth))
  dqcoeffMax = (1 << (7 + bit_depth)) - 1
  txSzCtx = get_txsize_ctx(txSz)
  ptype = plane_type[plane]
  txScale = tx_scale_lookup[txSz]

#if ENCODE
  enc_pick_coeffs(bsize,txSz,x,y,plane,blocky,blockx)
  ASSERT(IMPLIES(enc_last_coef == 0, c_eob == 0), "Coefficient selection problem")
#endif

  maxCoeffs = get_max_eob(txSz)
  for (i=0; i<maxCoeffs; i++) {
    dequant[i] = 0
  }

  all_zero ae(v)
  if (all_zero) {
    eob = 0
    culLevel = 0
    dc_coef_no_wrap = 0

    if (plane == PLANE_TYPE_Y_WITH_DC) {
      // Note: When decoding, this matches the spec. When encoding, we
      // already set up this array, but we might have chosen some
      // other transform type (and then decided not to encode any
      // coefficients). Either way, we need to blat the block with
      // transform type DCT_DCT.
      write_txk_block (blocky, blockx, txSz, DCT_DCT)
    }
  } else { // not all_zero
    if (plane == 0) {
      read_tx_type(bsize,blocky,blockx,txSz)
    }
    txType = get_tx_type(txSz,plane,blocky,blockx)
    PlaneTxType = txType
    get_scan(txSz,txType)
    // this section decodes which coefs are zero/non-zero
    eobMultiSize = txsize_log2_minus4[txSz]
#if ENCODE
    for (enc_eobPt = 0; enc_eobPt < 11; enc_eobPt++) {
      if (k_eob_group_start[enc_eobPt+1] > c_eob) {
        break
      }
    }
    ASSERT(enc_eobPt <= 11, "EobPt too large for tests below")
#endif // ENCODE
    if (eobMultiSize == 0) {
      eob_pt_16 ae(v)
      eobPt = eob_pt_16 + 1
    } else if (eobMultiSize == 1) {
      eob_pt_32 ae(v)
      eobPt = eob_pt_32 + 1
    } else if (eobMultiSize == 2) {
      eob_pt_64 ae(v)
      eobPt = eob_pt_64 + 1
    } else if (eobMultiSize == 3) {
      eob_pt_128 ae(v)
      eobPt = eob_pt_128 + 1
    } else if (eobMultiSize == 4) {
      eob_pt_256 ae(v)
      eobPt = eob_pt_256 + 1
    } else if (eobMultiSize == 5) {
      eob_pt_512 ae(v)
      eobPt = eob_pt_512 + 1
    } else {
      eob_pt_1024 ae(v)
      eobPt = eob_pt_1024 + 1
    }
#if ENCODE
    ASSERT(eobPt == enc_eobPt, "eobPt encode failure")
#endif // ENCODE

    eob = k_eob_group_start[eobPt]
#if ENCODE
    CHECK((eob <= c_eob), "eobPt incorrectly encoded - lower bound")
    if (c_eob <= 2) {
      CHECK(c_eob == eob, "Invalid small eob encoding")
    } else {
      CHECK(eob > 2, "Invalid start of bigger eob encoding")
    }
#endif // ENCODE
    eobExtra = 0
    eobShift = k_eob_offset_bits[eobPt] - 1
    if (eobShift >= 0) {
      eob_extra ae(v)
      if (eob_extra) {
        eobExtra += (1 << eobShift)
      }

      for (i = 1; i < k_eob_offset_bits[eobPt]; i++) {
        eobShift = k_eob_offset_bits[eobPt] - 1 - i
        eob_extra_bit ae(v)
        if (eob_extra_bit) {
          eobExtra += (1 << eobShift)
        }
      }
    }
    if (eob > 2) {
      eob += eobExtra
    }
#if ENCODE
    CHECK(eob == c_eob, "eob differs")
#endif // ENCODE

    // decode base and br
    culLevel = 0
    for (i = 0; i < eob; i++) {
      category = 0 // Used for stats collection
      c = eob - 1 - i
      if (c==(eob - 1)) {
        coeff_base_eob ae(v)
        level = coeff_base_eob + 1
      } else {
        coeff_base ae(v)
        level = coeff_base
      }
      if (level > 0) {
        category += 1
      }
      if (level > NUM_BASE_LEVELS) {
        for (idx = 0; idx < COEFF_BASE_RANGE / (BR_CDF_SIZE - 1); idx++) {
          category += 1
          coeff_br ae(v)
          level += coeff_br
          if (coeff_br < (BR_CDF_SIZE - 1)) {
            break
          }
        }
        if (level > NUM_BASE_LEVELS + COEFF_BASE_RANGE) {
          category += 1
        }
      }
      ASSERT(category <= 6, "Incorrect calculation of 'category' for stats collection")
#if DECODE && COLLECT_STATS
      total_coeffs[category] += 1
#endif
      dequant[scan[c]] = level
    }

    // Decode each coefficient in turn
    dc_coef_no_wrap = 0
    for (c = 0; c < eob; c++) {
      level = dequant[scan[c]]
      // decode sign
      sign = 0
      if (level != 0) {
        if (c == 0) {
          dc_sign ae(v)
          sign = dc_sign
          dc_coef_no_wrap = sign ? -level : level
        } else {
          sign_bit ae(v)
          sign = sign_bit
        }
      }

      // decode 0-th order Golomb code where relevant
      if (dequant[scan[c]] > NUM_BASE_LEVELS + COEFF_BASE_RANGE) {
#if ENCODE
        CHECK(enc_coef[c] >= (COEFF_BASE_RANGE + 1 + NUM_BASE_LEVELS), "Coef too small for golomb")
        coef = enc_coef[c] - (COEFF_BASE_RANGE + 1 + NUM_BASE_LEVELS)
        golombCoef = read_golomb(coef)
        CHECK(golombCoef == coef, "Golomb coef corruption")
#else // ENCODE
        coef = read_golomb()
#endif // ENCODE
        dequant[scan[c]] = coef + COEFF_BASE_RANGE + 1 + NUM_BASE_LEVELS
      }
      culLevel += (dequant[scan[c]] & 0xfffff)

      // apply the sign and the quantizer
#if ENCODE
      CHECK(dequant[scan[c]] == enc_coef[c], "TXB coef encode error")
      if (dequant[scan[c]] != 0) {
        CHECK(sign == enc_sign_bit[c], "TXB sign encode error")
      }
#endif // ENCODE
#if VALIDATE_SPEC_QUANT
      validate(20000)
#endif
      dqv = get_quantizer(c, plane, txSz, txType)
      v = dequant[scan[c]] & 0xfffff
#if VALIDATE_SPEC_QUANT
      validate(dqv)
      validate(sign?-v:v)
      validate(culLevel)
#endif
      v = (v * dqv) & 0xffffff
#if VALIDATE_SPEC_QUANT
      validate(v)
#endif
      v = v >> txScale
#if VALIDATE_SPEC_QUANT
      validate(v)
#endif
      if (sign) {
        v = -v
      }
      dequant[scan[c]] = clamp(v, dqcoeffMin, dqcoeffMax)
#if VALIDATE_SPEC_QUANT
      validate(dequant[scan[c]])
#endif

    }
  }

  culLevel = Min(63, culLevel)

  // DC value
  if (dc_coef_no_wrap < 0) {
    culLevel |= 1 << COEFF_CONTEXT_BITS
  } else if (dc_coef_no_wrap > 0) {
    culLevel += 2 << COEFF_CONTEXT_BITS
  }

  // Update context
  for(i=0;i<w;i++) {
    above_context[plane][(x4+i)] = culLevel
  }
  for(i=0;i<h;i++) {
    left_context[plane][(y4+i)&mi_mask2] = culLevel
  }

  for (i = 0; i < eob; i++) {
    ASSERT(dqcoeffMin <= dequant[i] && dequant[i] <= dqcoeffMax, "Out-of-range dqcoeff")
  }
#if DECODE && COLLECT_STATS
  if (eob > 0) {
    total_txfm_blocks[txSz] += 1
  }
#endif
 return eob
}

clear_above_context() {
  // Need to round number of columns up to the superblock size as some blocks may read off the edge
  aligned_mi_cols = ((mi_cols+mib_size-1)>>mib_size_log2)<<mib_size_log2
  for(plane=0;plane<get_num_planes();plane++) {
    for(i=0;i<aligned_mi_cols*2;i++) {
      above_context[plane][i] = 0
      above_intra_mode_ctx[i] = intra_mode_context[DC_PRED]
    }
  }
  for(i=0;i<aligned_mi_cols;i++) {
    above_seg_context[i] = 0
    above_pred_flags[i] = 0
  }
  for(i=0;i<(aligned_mi_cols << TX_UNIT_WIDE_LOG2);i++) {
    above_txfm_context[i] = tx_size_wide[TX_SIZES_LARGEST]
  }
}

clear_left_context() {
  for(plane=0;plane<get_num_planes();plane++) {
    for(i=0;i<(MI_BLOCK_SIZE*2);i++) {
      left_context[plane][i] = 0
      left_intra_mode_ctx[i] = intra_mode_context[DC_PRED]
    }
  }
  for(i=0;i<MI_BLOCK_SIZE;i++) {
    left_seg_context[i] = 0
    left_pred_flags[i] = 0
  }
  for(i=0;i<(MI_BLOCK_SIZE << TX_UNIT_HIGH_LOG2);i++) {
    left_txfm_context[i] = tx_size_high[TX_SIZES_LARGEST]
  }
}

inverse_transform_block(txSz,x,y,plane,txType) {
  //:print recon_count,x,y,plane,txSz,txType
  //:if recon_count==444:pdb.set_trace()
#if VALIDATE_TRANSFORM
  validate(1011)
  for(j=0;j<tx_size_high[txSz];j++)
    for(i=0;i<tx_size_wide[txSz];i++)
      validate(frame[plane][x+i][y+j])
#endif
  inv_txfm2d_add(x,y,plane,txSz,txType)
#if VALIDATE_TRANSFORM
  validate(1012)
  for(j=0;j<tx_size_high[txSz];j++)
    for(i=0;i<tx_size_wide[txSz];i++)
      validate(frame[plane][x+i][y+j])
#endif
}

predict_palette(plane,x,y,blocky,blockx,txSz) {
  w = tx_size_wide[txSz]
  h = tx_size_high[txSz]
  basex = blockx * tx_size_wide[0]
  basey = blocky * tx_size_high[0]
  for(i=0;i<h;i++) {
    for(j=0;j<w;j++) {
      if (plane==0) {
        frame[plane][x+j][y+i] = palette_colors_y[ ColorMapY[basey+i][basex+j] ]
      } else if (plane==1) {
        frame[plane][x+j][y+i] = palette_colors_u[ ColorMapUV[basey+i][basex+j] ]
      } else {
        frame[plane][x+j][y+i] = palette_colors_v[ ColorMapUV[basey+i][basex+j] ]
      }
    }
  }
}

// txSz is size of the transform block 0=4x4 1=8x8
// x,y are in plane coordinates
// blockx and blocky are in units of txfm block from top-left of containing block
predict_and_reconstruct_intra_block(txSz,x,y,plane,block_idx,bsize,blocky,blockx,mi_row,mi_col,partition) {
  // Perform intra prediction
  mode = get_block_mode(plane, blockx, blocky)
  if ( (plane==0 && PaletteSizeY) || (plane>0 && PaletteSizeUV) )
    predict_palette(plane,x,y,blocky,blockx,txSz)
  else
  predict_intra_block_facade(x, y, plane, txSz, mode, bsize,blocky,blockx,mi_row,mi_col,partition)
  :log_prediction(b,txSz,x,y,plane,PREDICTION)
  :C log_prediction(b,txSz,x,y,plane,PREDICTION)
  // Read coefficients
  if (!skip_coeff) {
    eob = read_coeffs_txb(bsize,txSz,x,y,plane,blocky,blockx)
    ASSERT(eob <= get_max_eob(txSz), "eob too large")
    :log_coeffs(b)
    :C log_coeffs(b)
    if (eob > 0) {
      txType = get_tx_type(txSz,plane,blocky,blockx)
      inverse_transform_block(txSz,x,y,plane,txType)
    }
#if VALIDATE_SPEC_SKIP
    else {
      w = tx_size_wide[txSz]
      h = tx_size_high[txSz]

      for (j = 0; j < h; j++) {
        for (i = 0; i < w; i++) {
          validate(20012)
          validate(plane)
          validate(j)
          validate(i)
          validate(y+j)
          validate(x+i)
          validate(frame[plane][x+i][y+j])
        }
      }
    }
#endif
  }
#if VALIDATE_SPEC_SKIP
  else {
    w = tx_size_wide[txSz]
    h = tx_size_high[txSz]

    for (j = 0; j < h; j++) {
      for (i = 0; i < w; i++) {
        validate(20011)
        validate(plane)
        validate(j)
        validate(i)
        validate(y+j)
        validate(x+i)
        validate(frame[plane][x+i][y+j])
      }
    }
  }
#endif
  :log_prediction(b,txSz,x,y,plane,TRANSFORM)
  :C log_prediction(b,txSz,x,y,plane,TRANSFORM)
}

decode_reconstruct_tx(mi_row, mi_col, max_units_wide, max_units_high, txSz, x, y, plane, block_idx, bsize, blocky, blockx) {
  if (blocky >= max_units_high || blockx >= max_units_wide)
    return 0

  tx_row = mi_row + ((blocky << plane_subsampling_y[plane]) >> TX_UNIT_HIGH_LOG2)
  tx_col = mi_col + ((blockx << plane_subsampling_x[plane]) >> TX_UNIT_WIDE_LOG2)
  block_txSz = inter_tx_size[tx_col][tx_row]
  plane_tx_size = plane ? get_uv_tx_size(block_txSz, sb_size, mi_row, mi_col) : block_txSz
  if (txSz == plane_tx_size || plane) {
    // For chroma, always use the initial tx size (== largest rect tx which fits in
    // the subsampled block)
    if (!skip_coeff) {
      :log_prediction(b,txSz,x,y,plane,PREDICTION)
      :C log_prediction(b,txSz,x,y,plane,PREDICTION)
      eob = read_coeffs_txb(bsize,txSz,x,y,plane,blocky,blockx)
      if (eob > 0) {
        txType = get_tx_type(txSz,plane,blocky,blockx)
        inverse_transform_block(txSz,x,y,plane,txType)
      }
#if VALIDATE_SPEC_SKIP
      else {
        w = tx_size_wide[txSz]
        h = tx_size_high[txSz]

        for (j = 0; j < h; j++) {
          for (i = 0; i < w; i++) {
            validate(20012)
            validate(plane)
            validate(j)
            validate(i)
            validate(y+j)
            validate(x+i)
            validate(frame[plane][x+i][y+j])
          }
        }
      }
#endif
      :log_prediction(b,txSz,x,y,plane,TRANSFORM)
      :C log_prediction(b,txSz,x,y,plane,TRANSFORM)
    }
#if VALIDATE_SPEC_SKIP
    else {
      w = tx_size_wide[txSz]
      h = tx_size_high[txSz]

      for (j = 0; j < h; j++) {
        for (i = 0; i < w; i++) {
          validate(20011)
          validate(plane)
          validate(j)
          validate(i)
          validate(y+j)
          validate(x+i)
          validate(frame[plane][x+i][y+j])
        }
      }
    }
#endif
  } else {
    sub_txs = sub_tx_size_map[1][txSz]
    block_step = tx_size_wide_unit[sub_txs] * tx_size_high_unit[sub_txs]
    bsw = tx_size_wide_unit[sub_txs]
    bsh = tx_size_high_unit[sub_txs]
    for (i = 0; i < tx_size_high_unit[txSz]; i += bsh)
      for (j = 0; j < tx_size_wide_unit[txSz]; j += bsw) {
        new_blockx = blockx + j
        new_blocky = blocky + i
        if (new_blocky < max_units_high && new_blockx < max_units_wide) {
          new_x = x + (j << tx_size_wide_log2[0])
          new_y = y + (i << tx_size_high_log2[0])
          decode_reconstruct_tx(mi_row, mi_col, max_units_wide, max_units_high, sub_txs, new_x, new_y, plane, block_idx, bsize, new_blocky, new_blockx)
          block_idx += block_step
        }
      }
  }
}

// txSz is size of the transform block 4x4, 8x8
predict_and_reconstruct_inter_block(txSz,x,y,plane,block_idx,bsize,blocky,blockx) {
  // Read coefficients
  :log_prediction(b,txSz,x,y,plane,PREDICTION)
  :C log_prediction(b,txSz,x,y,plane,PREDICTION)
  if (!skip_coeff) {
    eob = read_coeffs_txb(bsize,txSz,x,y,plane,blocky,blockx)
    if (eob > 0) {
      txType = get_tx_type(txSz,plane,blocky,blockx)
      inverse_transform_block(txSz,x,y,plane,txType)
    }
#if VALIDATE_SPEC_SKIP
    else {
      w = tx_size_wide[txSz]
      h = tx_size_high[txSz]

      for (j = 0; j < h; j++) {
        for (i = 0; i < w; i++) {
          validate(20012)
          validate(plane)
          validate(j)
          validate(i)
          validate(y+j)
          validate(x+i)
          validate(frame[plane][x+i][y+j])
        }
      }
    }
#endif
  }
#if VALIDATE_SPEC_SKIP
  else {
    w = tx_size_wide[txSz]
    h = tx_size_high[txSz]

    for (j = 0; j < h; j++) {
      for (i = 0; i < w; i++) {
        validate(20011)
        validate(plane)
        validate(j)
        validate(i)
        validate(y+j)
        validate(x+i)
        validate(frame[plane][x+i][y+j])
      }
    }
  }
#endif
  :log_prediction(b,txSz,x,y,plane,TRANSFORM)
  :C log_prediction(b,txSz,x,y,plane,TRANSFORM)
}

set_output_maybe_flip_strides(txType,x,y,plane,i,j,bw,bh,transpose) {
  flipLR = 0
  flipUD = 0

  if ((txType==FLIPADST_DCT)
      || (txType==FLIPADST_ADST)
      || (txType==V_FLIPADST)) {
    flipUD = 1
  } else if ((txType==DCT_FLIPADST)
             || (txType==ADST_FLIPADST)
             || (txType==H_FLIPADST)) {
    flipLR = 1
  } else if (txType==FLIPADST_FLIPADST) {
    flipUD = 1
    flipLR = 1
  } else {
    CHECK((txType==DCT_DCT) || (txType==ADST_DCT) || (txType==DCT_ADST) || (txType==ADST_ADST) || (txType==IDTX) || (txType==V_DCT) || (txType==H_DCT) || (txType==V_ADST) || (txType==H_ADST), "Unexpected txType")
  }

  if (transpose) {
    // In this case, all of the values (txType, bw/bh, i/j) apply to the *transposed*
    // transform block. So we need to transpose again on output - the simplest way is just
    // to swap the values of xx and yy here.
    yy = flipLR ? (bw-i-1) : i
    xx = flipUD ? (bh-j-1) : j
  } else {
    xx = flipLR ? (bw-i-1) : i
    yy = flipUD ? (bh-j-1) : j
  }

#if ENCODE
  range_check_value(output[j] + frame[plane][x+xx][y+yy], bit_depth+8)
  if (!enc_disable_recon)
#endif
    frame[plane][x+xx][y+yy] = clip_pixel(output[j] + frame[plane][x+xx][y+yy])
}
