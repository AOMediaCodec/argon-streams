/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

/*
Motion vectors are read once per block, or 2/4 times if the block is 8x8 and has subblocks.

The motion vectors can be used either at an 8x8 or 4x4 resolution.
When used at 8x8, the 8x8 motion vector comes from the final 4x4 subblock.

Only need at most 4 rows of 8x8 motion vectors to be stored.

The previous frame is used as a candidate for motion vectors if:
  Same dimensions
  It was shown
  We are not in error resilient mode
  We are not intra_only

The flow of motion vectors is:
  1) Construct predictions in mv_ref_list, and copy to best/nearest/near
  2) Compute final motion vectors in mv
  3) Copy to block_mvs for each 4x4 subposition
  4) Copy to complete mvs array across whole image

For <8x8 blocks, the best motion vector for the whole 8x8 block is used as the reference for NEWMV,
but NEAR and NEAREST trigger extra predictions.
*/

#define VALIDATE_SPEC_MV 0
#define VALIDATE_SPEC_MV_REF_CANDIDATE 0
#define VALIDATE_SPEC_MFMV 0
#define VALIDATE_SPEC_MFMV_STORAGE 0

// From yv12config.h
#define VP9BORDERINPIXELS      160
#define MVREF_NEIGHBOURS 9

// Note: The comment below (preserved from older code) suggests that we want this to be (32 << 3),
// but the reference code uses (16 << 3). Is there an issue with this?
// Old comment:
//   For an image of 65*65 pixels, we may wish to predict a 128*128 block.
//   Conceptually there is a 32 wide border on both sides (in this part - in other parts the border is twice this)
//   And so the extended image is 129*129.
//   The motion vector clamping of predicted motion vectors moves the block to lie inside the extended image.
//   For images of 64*64 or smaller, the 128*128 superblock is forced to split so blocks become smaller and still fit.
#define MV_BORDER (16 << 3)  // Allow 16 pels in 1/8th pel units

#define  BOTH_ZERO  0
#define  ZERO_PLUS_PREDICTED 1
#define  BOTH_PREDICTED  2
#define  NEW_PLUS_NON_INTRA  3
#define  BOTH_NEW  4
#define  INTRA_PLUS_NON_INTRA  5
#define  BOTH_INTRA  6
#define  INVALID_CASE  9

// There are 3^3 different combinations of 3 counts that can be either 0,1 or
// 2. However the actual count can never be greater than 2 so the highest
// counter we need is 18. 9 is an invalid counter that's never used.
uint_0_9 counter_to_context[19] = {
  BOTH_PREDICTED,  // 0
  NEW_PLUS_NON_INTRA,  // 1
  BOTH_NEW,  // 2
  ZERO_PLUS_PREDICTED,  // 3
  NEW_PLUS_NON_INTRA,  // 4
  INVALID_CASE,  // 5
  BOTH_ZERO,  // 6
  INVALID_CASE,  // 7
  INVALID_CASE,  // 8
  INTRA_PLUS_NON_INTRA,  // 9
  INTRA_PLUS_NON_INTRA,  // 10
  INVALID_CASE,  // 11
  INTRA_PLUS_NON_INTRA,  // 12
  INVALID_CASE,  // 13
  INVALID_CASE,  // 14
  INVALID_CASE,  // 15
  INVALID_CASE,  // 16
  INVALID_CASE,  // 17
  BOTH_INTRA  // 18
};

// range of -3 to 6

uint_1_3 idx_n_column_to_subblock[4][2] = {
  {1, 2},
  {1, 3},
  {3, 2},
  {3, 3}
};

// For block sizes >= 8x8 we store the same information at every idx value
get_sub_block_mv(mvcol, mvrow, which_mv, search_col, block_idx) {
  idx = 3
  for (j=0;j<2;j++)
    candidate_mv[which_mv][j] = mvs[0][mvcol][mvrow][which_mv][idx][j]
  for (j=0;j<2;j++)
    candidate_pred_mv[which_mv][j] =mvs[0][mvcol][mvrow][which_mv][idx][j]
    //candidate_pred_mv[which_mv][j] = pred_mvs[0][mvcol][mvrow][which_mv][idx][j]
}



int24 integer_mv_precision(int24 ref) {
  sign = (ref > 0) ? 1 : -1
  mod = (Abs(ref) % 8)
  if (mod != 0) {
    ref -= (mod * sign)
    if (mod > 4) {
      ref += (8 * sign)
    }
  }
  return ref
}

lower_mv_precision(int32pointer this_mv) {
  if (cur_frame_force_integer_mv) {
    for(i=0;i<2;i++) {
      this_mv[i] = integer_mv_precision(this_mv[i])
    }
  } else
  if (!allow_high_precision_mv) {
    for(i=0;i<2;i++) {
      if (this_mv[i] & 1)
        this_mv[i] += (this_mv[i] > 0 ? -1 : 1)
    }
  }
}

get_block_mv(mvcol, mvrow, which_mv, use_prev) {
  for (j=0;j<2;j++)
    candidate_mv[which_mv][j] = mvs[use_prev][mvcol][mvrow][which_mv][3][j]
  for (j=0;j<2;j++)
    candidate_pred_mv[which_mv][j] = pred_mvs[use_prev][mvcol][mvrow][which_mv][3][j]
}

uint_0_12 candidate_sb_size(mvcol, mvrow) {
  return sb_sizes[mvcol][mvrow]
}

// REF_MV can access some motion vectors that are not yet decoded
// Use NONE to signal that these should be ignored
clear_ref_frames() {
  for(i=0;i<mi_rows;i++)
  for(j=0;j<mi_cols;j++)
    refframes[0][j][i][0] = NONE
}

int candidate_ref_frame(mvcol,mvrow,which_mv,use_prev) {  // -1 to 3
  return refframes[use_prev][mvcol][mvrow][which_mv]
}


// Performs mv sign inversion if indicated by the reference frame combination.
scale_mv(r, mvcol, mvrow, which_mv, this_ref_frame) {
  if (ref_frame_sign_bias[r] != ref_frame_sign_bias[this_ref_frame]) {
    for(j=0;j<2;j++) {
      candidate_mv[which_mv+2][j] = -candidate_mv[which_mv][j]
    }
  } else {
    for(j=0;j<2;j++) {
      candidate_mv[which_mv+2][j] = candidate_mv[which_mv][j]
    }
  }
}

clamp_mv_ref(mi_row, mi_col, bw, bh, which_mv) {
  candidate_mv[which_mv][0] = clamp_mv_row(mi_row, mi_col, candidate_mv[which_mv][0],MV_BORDER+bh*8)
  candidate_mv[which_mv][1] = clamp_mv_col(mi_row, mi_col, candidate_mv[which_mv][1],MV_BORDER+bw*8)
}

clamp_mv_ref_list(mi_row, mi_col, bw, bh, which_mv) {
  mv_ref_list[which_mv][0] = clamp_mv_row(mi_row, mi_col, mv_ref_list[which_mv][0],MV_BORDER+bh*8)
  mv_ref_list[which_mv][1] = clamp_mv_col(mi_row, mi_col, mv_ref_list[which_mv][1],MV_BORDER+bw*8)
}

clamp_stack(mi_row, mi_col, bw, bh, this_ref_frame, idx, which_mv) {
  ref_mv_stack[this_ref_frame][idx][which_mv][0] = clamp_mv_row(mi_row, mi_col, ref_mv_stack[this_ref_frame][idx][which_mv][0],MV_BORDER+bh*8)
  ref_mv_stack[this_ref_frame][idx][which_mv][1] = clamp_mv_col(mi_row, mi_col, ref_mv_stack[this_ref_frame][idx][which_mv][1],MV_BORDER+bw*8)
}

// This macro is used to add a motion vector mv_ref list if it isn't
// already in the list.
// When we load a candidate we load it in a location dependent on which list is active.
// When we scale we use list+2 (because comparisons need to happen before the scaling)
ADD_MV_REF_LIST(mi_row, mi_col, bw, bh, which_mv) {
  if (refmv_count<2) {
    clamp_mv_ref(mi_row, mi_col, bw, bh, which_mv)
    if (refmv_count==0 ||
          ( candidate_mv[which_mv][0] != mv_ref_list[0][0] ||
            candidate_mv[which_mv][1] != mv_ref_list[0][1] )
        ) {
      mv_ref_list[refmv_count][0] = candidate_mv[which_mv][0]
      mv_ref_list[refmv_count][1] = candidate_mv[which_mv][1]
      refmv_count++
      //validate(2000)
      //x = candidate_mv[which_mv][1]
      //y = candidate_mv[which_mv][0]
      //validate(x)
      //validate(y)
    }
  }
}

// If either reference frame is different, not INTRA, and they
// are different from each other scale and add the mv to our list.
IF_DIFF_REF_FRAME_ADD_MV(mi_row, mi_col, bw, bh, refList,mvcol,mvrow,use_prev) {
  r0 = candidate_ref_frame(mvcol,mvrow,0,use_prev)
  if (r0>INTRA_FRAME) {
    get_block_mv(mvcol, mvrow, 0, use_prev)
    if (r0 != refList) {
      scale_mv(r0, mvcol, mvrow, 0, refList)
      // Bug 662: Need to scale motion vectors into separate location
      ADD_MV_REF_LIST(mi_row, mi_col, bw, bh, 2+0)
    }

    r1 = candidate_ref_frame(mvcol,mvrow,1,use_prev)
    if (r1>INTRA_FRAME) {
      get_block_mv(mvcol, mvrow, 1, use_prev)
      if (r1 != refList
          ) {
        scale_mv(r1, mvcol, mvrow, 1, refList)
        ADD_MV_REF_LIST(mi_row, mi_col, bw, bh, 2+1)
      }
    }
  }
}


// Checks that the given mi_row, mi_col and search point
// are inside the borders of the tile.
uint1 is_inside(mvcol, mvrow) {
  return (mvcol >= mi_col_start &&
          mvrow >= mi_row_start &&
          mvcol < mi_col_end &&
          mvrow < mi_row_end)
}

int find_valid_row_offset(mi_row, rowOffset) {
    return Clip3(mi_row_start - mi_row, mi_row_end - mi_row - 1, rowOffset)
}

int find_valid_col_offset(mi_col, colOffset) {
  return Clip3(mi_col_start - mi_col, mi_col_end - mi_col - 1, colOffset)
}

uint8 candidate_mode(mvcol,mvrow) {
  return y_modes[mvcol][mvrow]
}

// Needs to be in a different function to avoid optimisations
clear_refmv_count() {
  refmv_count = 0
}

// Retrieve the motion vector from the previous frame
get_prev_block_mv(mvcol,mvrow,list) {
  get_block_mv(mvcol, mvrow, list, 1)
}

// Looks to see if the candidate motion vector is already in the stack
// Adds weight to the location
// Returns number of new motion vectors added
//
// This is only called for pure frame types
int search_stack(this_ref_frame,ref,weight)
{
// See if we already have this motion vector in our stack
  for (idx = 0; idx < ref_mv_count[this_ref_frame]; idx++)
    if (candidate_mv[ref][0] == ref_mv_stack[this_ref_frame][idx][0][0]
     && candidate_mv[ref][1] == ref_mv_stack[this_ref_frame][idx][0][1])
      break

  // If so, increase the weight
  if (idx < ref_mv_count[this_ref_frame]) {
    weight_stack[this_ref_frame][idx] += weight
    return 0
  }

  // Otherwise, store a new motion vector
  if (idx == ref_mv_count[this_ref_frame] && ref_mv_count[this_ref_frame] < MAX_REF_MV_STACK_SIZE) {
    for(i=0;i<2;i++) {
      ref_mv_stack[this_ref_frame][idx][0][i] = candidate_mv[ref][i]
      pred_mv_stack[this_ref_frame][idx][0][i] = candidate_pred_mv[ref][i]
    }
#if VALIDATE_SPEC_MV_REF_CANDIDATE
    if (ValidateMv) {
      validate(70003)
      validate(ref_mv_count[this_ref_frame])
      validate(candidate_mv[ref][ 0 ])
      validate(candidate_mv[ref][ 1 ])
    }
#endif
    weight_stack[this_ref_frame][idx] = weight
    ref_mv_count[this_ref_frame]++
    return 1
  }
  return 0
}

// Same as search_stack, but testing compound motion vectors
int search_comp_stack(this_ref_frame,weight)
{
// See if we already have this motion vector in our stack
  for (idx = 0; idx < ref_mv_count[this_ref_frame]; idx++)
    if (candidate_mv[0][0] == ref_mv_stack[this_ref_frame][idx][0][0]
     && candidate_mv[0][1] == ref_mv_stack[this_ref_frame][idx][0][1]
     && candidate_mv[1][0] == ref_mv_stack[this_ref_frame][idx][1][0]
     && candidate_mv[1][1] == ref_mv_stack[this_ref_frame][idx][1][1]
     )
      break

  // If so, increase the weight
  if (idx < ref_mv_count[this_ref_frame]) {
    weight_stack[this_ref_frame][idx] += weight
    return 0
  }

  // Otherwise, store a new motion vector
  if (idx == ref_mv_count[this_ref_frame] && ref_mv_count[this_ref_frame] < MAX_REF_MV_STACK_SIZE) {
    for(ref=0;ref<2;ref++) {
      for(i=0;i<2;i++) {
        ref_mv_stack[this_ref_frame][idx][ref][i] = candidate_mv[ref][i]
        pred_mv_stack[this_ref_frame][idx][ref][i] = candidate_pred_mv[ref][i]
      }
#if VALIDATE_SPEC_MV_REF_CANDIDATE
      if (ValidateMv) {
        validate(70004)
        validate(ref_mv_count[this_ref_frame])
        validate(ref)
        validate(candidate_mv[ref][ 0 ])
        validate(candidate_mv[ref][ 1 ])
      }
#endif
    }
    weight_stack[this_ref_frame][idx] = weight
    ref_mv_count[this_ref_frame]++
    return 1
  }
  return 0
}

// Examines a candidate blocks motion vectors
//   col gives mi_col for the candidate
//   len is related to the candidate's size and affects the weight
//   block is -1 for >= 8x8, of the block_idx of the current block
//   Adds into the ref_mv_stack
//   Returns the number of NEWMV candidates found (i.e. when the candidate is from a block that used NEWMV)
uint8 add_ref_mv_candidate(mvcol, mvrow, this_ref_frame, len, block, col, which_globalmv, weight, is_col_scan) {
  if (!is_inters[mvcol][mvrow]) {
    return 0
  }
  ASSERT(weight % 2 == 0, "Invalid weight")
  index = 0
  newmvcount = 0
  if (rf[1] == NONE) {
    // single reference frame
    for (ref = 0; ref < 2; ref++) {
      if (candidate_ref_frame(mvcol,mvrow,ref,0) == rf[0]) {
        // TODO factor this even more?
        get_sub_block_mv(mvcol, mvrow, ref, col, block)
        // don't overwrite pred_mv
        if (is_global_mv_block(mvrow, mvcol, block, rf[0])) {
          for (j=0;j<2;j++) {
            candidate_mv[ref][j] = globalmv[which_globalmv][j]
          }
        }
        lower_mv_precision(candidate_mv[ref])
        search_stack(this_ref_frame,ref,weight*len)
        if (have_newmv_in_inter_mode(candidate_mode(mvcol,mvrow)))
          newmvcount++

        if (is_col_scan)
          col_match_count++
        else
          row_match_count++

      }
    }
  } else {
    // compound reference frame
    if (candidate_ref_frame(mvcol,mvrow,0,0) == rf[0] && candidate_ref_frame(mvcol,mvrow,1,0) == rf[1]) {

      newmv_mode = NEW_NEWMV

      for (ref = 0; ref < 2; ref++) {
        get_sub_block_mv(mvcol, mvrow, ref, col, block)
        // don't overwrite pred_mv
        if (is_global_mv_block(mvrow, mvcol, block, rf[ref])) {
          for (j=0;j<2;j++) {
            candidate_mv[ref][j] = globalmv[ref][j]
          }
        }
        lower_mv_precision(candidate_mv[ref])
      }
        search_comp_stack(this_ref_frame,weight*len)
        if (have_newmv_in_inter_mode(candidate_mode(mvcol,mvrow)))
          newmvcount++

        if (is_col_scan)
          col_match_count++
        else
          row_match_count++

    }
  }
  return newmvcount
}

uint8 scan_row_mbmi(mi_row, mi_col, this_ref_frame, block, deltamvrow, which_globalmv, maxDeltaMvRow, intpointer processedRows) {
  newmvcount = 0
  bw = mi_size_wide[sb_size]
  endMi = Min(Min(bw, mi_cols - mi_col), mi_size_wide[BLOCK_64X64])
  deltamvcol = 0
  shift = 0
  useStep16 = (bw>=16) ? 1 : 0

  if (Abs(deltamvrow) > 1) {
    deltamvrow *= 2
    deltamvrow += 1

    deltamvcol = 1
    if ((mi_size_high[sb_size] < mi_size_high[BLOCK_8X8]) && (mi_row & 0x01)) {
      deltamvrow += 1
    }
    if ((mi_size_wide[sb_size] < mi_size_wide[BLOCK_8X8]) && (mi_col & 0x01)) {
      deltamvcol -= 1
    }
  }
  for (i = 0; i < endMi; i+=0) {
    mvrow = mi_row + deltamvrow
    mvcol = mi_col + deltamvcol + i
    candBw = mi_size_wide[candidate_sb_size(mvcol,mvrow)]
    candBh = mi_size_high[candidate_sb_size(mvcol,mvrow)]
    len = Min(bw, candBw)
    if (Abs(deltamvrow) > 1) {
      len = Max(len, mi_size_wide[BLOCK_8X8])
    }
    if (useStep16) {
      len = Max(mi_size_wide[BLOCK_16X16], len)
    }

    weight = 2
    if ((bw >= mi_size_wide[BLOCK_8X8]) && (bw <= candBw)) {
      inc = Min(-maxDeltaMvRow + deltamvrow + 1, candBh)
      weight = Max(weight, (inc << shift))
      processedRows[0] = inc - deltamvrow - 1
    }

    newmvcount += add_ref_mv_candidate(mvcol, mvrow, this_ref_frame, len, block, deltamvcol, which_globalmv, weight, 0)
    i += len
  }
  return newmvcount
}

uint8 scan_col_mbmi(mi_row, mi_col, this_ref_frame, block, deltamvcol, which_globalmv, maxDeltaMvCol, intpointer processedCols) {
  newmvcount = 0
  bh = mi_size_high[sb_size]
  endMi = Min(Min(bh, mi_rows - mi_row), mi_size_high[BLOCK_64X64])
  deltamvrow = 0
  shift = 0
  useStep16 = (bh>=16) ? 1 : 0

  if (Abs(deltamvcol) > 1) {
    deltamvcol *= 2
    deltamvcol += 1

    deltamvrow = 1
    if ((mi_size_high[sb_size] < mi_size_high[BLOCK_8X8]) && (mi_row & 0x01)) {
      deltamvrow -= 1
    }
    if ((mi_size_wide[sb_size] < mi_size_wide[BLOCK_8X8]) && (mi_col & 0x01)) {
      deltamvcol += 1
    }
  }
  for (i = 0; i < endMi;i+=0) {
    mvrow = mi_row + deltamvrow + i
    mvcol = mi_col + deltamvcol
    candBw = mi_size_wide[candidate_sb_size(mvcol,mvrow)]
    candBh = mi_size_high[candidate_sb_size(mvcol,mvrow)]
    len = Min(bh, candBh)
    if (Abs(deltamvcol) > 1) {
      len = Max(len, mi_size_high[BLOCK_8X8])
    }
    if (useStep16) {
      len = Max(mi_size_high[BLOCK_16X16], len)
    }

    weight = 2
    if ((bh >= mi_size_high[BLOCK_8X8]) && (bh <= candBh)) {
      inc = Min(-maxDeltaMvCol + deltamvcol + 1, candBw)
      weight = Max(weight, (inc << shift))
      processedCols[0] = inc - deltamvcol - 1
    }

    newmvcount += add_ref_mv_candidate(mvcol, mvrow, this_ref_frame, len, block, deltamvcol, which_globalmv, weight, 1)
    i += len
  }
  return newmvcount
}

// Test a single offset
uint8 scan_blk_mbmi(mi_row, mi_col, this_ref_frame, block, deltamvrow, deltamvcol, which_globalmv) {
  newmvcount = 0
  mvrow = mi_row + deltamvrow
  mvcol = mi_col + deltamvcol
  if (is_inside(mvcol, mvrow)) {
#if VALIDATE_SPEC_MV_REF_CANDIDATE
    if (ValidateMv) {
      validate(70021)
      validate(mvrow)
      validate(mvcol)
    }
#endif
    newmvcount += add_ref_mv_candidate(mvcol, mvrow, this_ref_frame, mi_size_wide[BLOCK_8X8], block, deltamvcol, which_globalmv, 2, 0)
  }
  return newmvcount
}


// Determine if the block has an available top right neighbour
// in the standard superblock scan order.
// bw,bh is size of block in mi_units.
uint1 has_top_right(mi_row, mi_col, bw, bh, partition) {
  bs = Max(bw, bh)
  mask_row = mi_row & mi_mask
  mask_col = mi_col & mi_mask

  // Never look at top-right for >64x64 blocks
  if (bs > mi_size_high[BLOCK_64X64])
    return 0

  // In a split partition all apart from the bottom right has a top right
  hastr = !((mask_row & bs) && (mask_col & bs))

  // bs > 0 and bs is a power of 2

  // For each 4x4 group of blocks, when the bottom right is decoded the blocks
  // to the right have not been decoded therefore the bottom right does
  // not have a top right
  while (bs < mib_size) {
    if (mask_col & bs) {
      if ((mask_col & (2 * bs)) && (mask_row & (2 * bs))) {
        hastr = 0
        break
      }
    } else {
      break
    }
    bs = bs << 1
  }

  // For vertical rectangular partitions, the rightmost block may or may not have a
  // top-right available, following the rules above. But the remaining block(s)
  // *always* have a top-right available.
  if (bw < bh) {
    if (((mi_col + bw) & (bh-1)) != 0) hastr = 1
  }

  // For horizontal rectangular partitions, the topmost block may or may not have a
  // top right available, following the rules above. But the remaining block(s) *never*
  // have a top-right available.
  if (bw > bh) {
    if ((mi_row & (bw-1)) != 0) hastr = 0
  }

  // The bottom left square of a Vertical A (in the old format) does
  // not have a top right as it is decoded before the right hand
  // rectangle of the partition
  if (partition == PARTITION_VERT_A) {
    if ((bw == bh) && (mask_row & bs)) hastr = 0
  }

  return hastr
}

add_tpl_ref_mv(mi_row, mi_col, this_ref_frame, delta_row, delta_col) {
  foundValidMv = 0
  mvRow = (mi_row + delta_row) | 1
  mvCol = (mi_col + delta_col) | 1

  if (is_inside(mvCol, mvRow) == 0) {
    return 0
  }

#if VALIDATE_SPEC_MV_REF_CANDIDATE
  if (ValidateMv) {
    validate(70016)
    validate(mvRow)
    validate(mvCol)
  }
#endif

  x8 = mvCol >> 1
  y8 = mvRow >> 1

  if ( rf[1] == NONE ) {
    for (j=0; j<2; j++) {
      candidate_mv[0][j] = motion_field_mvs[this_ref_frame][x8][y8][j]
    }
    if (candidate_mv[0][0] != INVALID_MV) {
      foundValidMv = 1
      lower_mv_precision(candidate_mv[0])
      if (delta_row == 0 && delta_col == 0) {
        if ( Abs(candidate_mv[0][0] - globalmv[0][0]) >= 16 ||
             Abs(candidate_mv[0][1] - globalmv[0][1]) >= 16)
          mode_context[this_ref_frame] |= (1 << GLOBALMV_OFFSET)
      }
      for (idx = 0; idx < ref_mv_count[this_ref_frame]; idx++) {
        if (candidate_mv[0][0] == ref_mv_stack[this_ref_frame][idx][0][0] &&
            candidate_mv[0][1] == ref_mv_stack[this_ref_frame][idx][0][1]) {
          break
        }
      }
      if (idx < ref_mv_count[this_ref_frame]) {
        weight_stack[this_ref_frame][idx] += 2
      } else if (ref_mv_count[this_ref_frame] < MAX_REF_MV_STACK_SIZE) {
        for (j=0; j<2; j++) {
          ref_mv_stack[this_ref_frame][ref_mv_count[this_ref_frame]][0][j] = candidate_mv[0][j]
          pred_mv_stack[this_ref_frame][ref_mv_count[this_ref_frame]][0][j] = candidate_mv[0][j]
        }
        pred_mv_stack[this_ref_frame][ref_mv_count[this_ref_frame]][0][0] += 4
        weight_stack[this_ref_frame][ref_mv_count[this_ref_frame]] = 2
#if VALIDATE_SPEC_MV_REF_CANDIDATE
        if (ValidateMv) {
          validate(70005)
          validate(ref_mv_count[this_ref_frame])
          validate(candidate_mv[0][ 0 ])
          validate(candidate_mv[0][ 1 ])
        }
#endif
        ref_mv_count[this_ref_frame] += 1
      }

      // Don't scan any more MVs
      return 0
    }
  } else {
    for (j=0; j<2; j++) {
      candidate_mv[0][j] = motion_field_mvs[rf[0]][x8][y8][j]
      candidate_mv[1][j] = motion_field_mvs[rf[1]][x8][y8][j]
    }
    cand0Valid = (candidate_mv[0][0] != INVALID_MV)
    cand1Valid = (candidate_mv[1][0] != INVALID_MV)
    ASSERT(cand0Valid == cand1Valid, "The two candidate MFMVs should be either both valid or both invalid")
    if (cand0Valid) {
      foundValidMv = 1
      lower_mv_precision(candidate_mv[0])
      lower_mv_precision(candidate_mv[1])
      if (delta_row == 0 && delta_col == 0) {
        if ( Abs(candidate_mv[0][0] - globalmv[0][0]) >= 16 ||
             Abs(candidate_mv[0][1] - globalmv[0][1]) >= 16 ||
             Abs(candidate_mv[1][0] - globalmv[1][0]) >= 16 ||
             Abs(candidate_mv[1][1] - globalmv[1][1]) >= 16)
          mode_context[this_ref_frame] |= (1 << GLOBALMV_OFFSET)
      }
      for (idx = 0; idx < ref_mv_count[this_ref_frame]; idx++) {
        if ((candidate_mv[0][0] == ref_mv_stack[this_ref_frame][idx][0][0]) &&
            (candidate_mv[0][1] == ref_mv_stack[this_ref_frame][idx][0][1]) &&
            (candidate_mv[1][0] == ref_mv_stack[this_ref_frame][idx][1][0]) &&
            (candidate_mv[1][1] == ref_mv_stack[this_ref_frame][idx][1][1])) {
          break
        }
      }
      if (idx < ref_mv_count[this_ref_frame]) {
        weight_stack[this_ref_frame][idx] += 2
      } else if (ref_mv_count[this_ref_frame] < MAX_REF_MV_STACK_SIZE) {
        for (j=0; j<2; j++) {
          ref_mv_stack[this_ref_frame][ref_mv_count[this_ref_frame]][0][j] = candidate_mv[0][j]
          pred_mv_stack[this_ref_frame][ref_mv_count[this_ref_frame]][0][j] = candidate_mv[0][j]
          ref_mv_stack[this_ref_frame][ref_mv_count[this_ref_frame]][1][j] = candidate_mv[1][j]
          pred_mv_stack[this_ref_frame][ref_mv_count[this_ref_frame]][1][j] = candidate_mv[1][j]
        }
        pred_mv_stack[this_ref_frame][ref_mv_count[this_ref_frame]][0][0] += 4
        pred_mv_stack[this_ref_frame][ref_mv_count[this_ref_frame]][1][0] += 4
        weight_stack[this_ref_frame][ref_mv_count[this_ref_frame]] = 2
#if VALIDATE_SPEC_MV_REF_CANDIDATE
        if (ValidateMv) {
          validate(70006)
          validate(ref_mv_count[this_ref_frame])
          validate(candidate_mv[0][ 0 ])
          validate(candidate_mv[0][ 1 ])
          validate(candidate_mv[1][ 0 ])
          validate(candidate_mv[1][ 1 ])
        }
#endif
        ref_mv_count[this_ref_frame] += 1
      }
      // Don't scan any more MVs
      return 0
    }
  }
  if (delta_row == 0 && delta_col == 0 && !foundValidMv) {
    mode_context[this_ref_frame] |= (1 << GLOBALMV_OFFSET)
  }
}

check_sb_border( mi_row, mi_col, delta_row, delta_col ) {
  mask = mi_size_wide[BLOCK_64X64] - 1
  row = (mi_row & mask) + delta_row
  col = (mi_col & mask) + delta_col
  return ( row >= 0 && row < mi_size_wide[BLOCK_64X64] && col >= 0 && col < mi_size_wide[BLOCK_64X64] )
}

// Warning, ref_frame_map is used for two purposes in the source code!  One is this static array, other is some mapping to ref frame slots
uint8 ref_frame_map[COMP_REFS][2] = {
  { LAST_FRAME, BWDREF_FRAME },  { LAST2_FRAME, BWDREF_FRAME },
  { LAST3_FRAME, BWDREF_FRAME }, { GOLDEN_FRAME, BWDREF_FRAME },

  { LAST_FRAME, ALTREF2_FRAME },  { LAST2_FRAME, ALTREF2_FRAME },
  { LAST3_FRAME, ALTREF2_FRAME }, { GOLDEN_FRAME, ALTREF2_FRAME },

  { LAST_FRAME, ALTREF_FRAME },  { LAST2_FRAME, ALTREF_FRAME },
  { LAST3_FRAME, ALTREF_FRAME }, { GOLDEN_FRAME, ALTREF_FRAME },

  // Signalled unidirectional compound references
  { LAST_FRAME, LAST2_FRAME }, { LAST_FRAME, LAST3_FRAME },
  { LAST_FRAME, GOLDEN_FRAME }, { BWDREF_FRAME, ALTREF_FRAME },

  // Un-signalled unidirectional compound references
  { LAST2_FRAME, LAST3_FRAME }, { LAST2_FRAME, GOLDEN_FRAME },
  { LAST3_FRAME, GOLDEN_FRAME }, { BWDREF_FRAME, ALTREF2_FRAME },
  { ALTREF2_FRAME, ALTREF_FRAME }
};

int8 av1_ref_frame_type(rf0, rf1) {
  if (rf1 <= INTRA_FRAME)
      return rf0

  for (i = 0; i < COMP_REFS; i++) {
    if (ref_frame_map[i][0] == rf0 && ref_frame_map[i][1] == rf1)
      return MAX_REF_FRAMES + i
  }
  return MAX_REF_FRAMES + FWD_RF_OFFSET(rf0) + BWD_RF_OFFSET(rf1) * FWD_REFS
}

av1_set_ref_frame(refFrameType) {
  if (refFrameType >= MAX_REF_FRAMES) {
    rf[0] = ref_frame_map[refFrameType - MAX_REF_FRAMES][0]
    rf[1] = ref_frame_map[refFrameType - MAX_REF_FRAMES][1]
  } else {
    rf[0] = refFrameType
    rf[1] = NONE
  }
}

int16 compound_mode_ctx_map[3][COMP_NEWMV_CTXS] = {
  { 0, 1, 1, 1, 1 },
  { 1, 2, 3, 4, 4 },
  { 4, 4, 5, 6, 7 },
};

int16 av1_mode_context_analyzer(block) {
  if (ref_frame[1] > INTRA_FRAME) {
    ctx = mode_context[ref_frame_type]
    newmv_ctx = Min(ctx & NEWMV_CTX_MASK, COMP_NEWMV_CTXS - 1)
    refmv_ctx = (ctx >> REFMV_OFFSET) & REFMV_CTX_MASK
    return compound_mode_ctx_map[refmv_ctx >> 1][newmv_ctx]
  }
  return mode_context[ref_frame_type]
}

uint8 drl_ctx(refFrameType, refIdx) {
  if (weight_stack[refFrameType][refIdx] >= REF_CAT_LEVEL &&
      weight_stack[refFrameType][refIdx + 1] >= REF_CAT_LEVEL)
    return 0

  if (weight_stack[refFrameType][refIdx] >= REF_CAT_LEVEL &&
      weight_stack[refFrameType][refIdx + 1] < REF_CAT_LEVEL)
    return 1

  if (weight_stack[refFrameType][refIdx] < REF_CAT_LEVEL &&
      weight_stack[refFrameType][refIdx + 1] < REF_CAT_LEVEL)
    return 2

  return 0
}

swap_stack(this_ref_frame,idx,idx2) {
  if (rf[1] == NONE) {
    numRefs=1
  } else {
    numRefs=2
  }
  for(i=0;i<2;i++) {
    for(j=0;j<numRefs;j++) {
      tmp_mv = ref_mv_stack[this_ref_frame][idx2][j][i]
      ref_mv_stack[this_ref_frame][idx2][j][i] = ref_mv_stack[this_ref_frame][idx][j][i]
      ref_mv_stack[this_ref_frame][idx][j][i] = tmp_mv

      tmp_mv = pred_mv_stack[this_ref_frame][idx2][j][i]
      pred_mv_stack[this_ref_frame][idx2][j][i] = pred_mv_stack[this_ref_frame][idx][j][i]
      pred_mv_stack[this_ref_frame][idx][j][i] = tmp_mv
    }
  }
  tmpw = weight_stack[this_ref_frame][idx2]
  weight_stack[this_ref_frame][idx2] = weight_stack[this_ref_frame][idx]
  weight_stack[this_ref_frame][idx] = tmpw
}

// Helper: Work around Argon Streams compiler mistakenly trying to do
// constant propagation if we put this inside setup_ref_mv_list directly.
reset_ref_match_count() {
  row_match_count = 0
  col_match_count = 0
}

calculate_mv_search_range(mi_row, mi_col, bw, bh) {
  rowAdj = (bh < mi_size_high[BLOCK_8X8]) && (mi_row & 0x01)
  colAdj = (bw < mi_size_wide[BLOCK_8X8]) && (mi_col & 0x01)

  maxRowOffset = 0
  maxColOffset = 0
  if (aU) {
    maxRowOffset = -(MVREF_ROWS << 1) + rowAdj
    if (bh < mi_size_high[BLOCK_8X8])
      maxRowOffset = -(2 << 1) + rowAdj
    maxRowOffset = find_valid_row_offset(mi_row, maxRowOffset)
  }
  if (aL) {
    maxColOffset = -(MVREF_COLS << 1) + colAdj
    if (bw < mi_size_wide[BLOCK_8X8])
      maxColOffset = -(2 << 1) + colAdj
    maxColOffset = find_valid_col_offset(mi_col, maxColOffset)
  }
}

// Prepare the stack of motion vectors for this location
// Immediate neighbours result in the near prediction
// Further neighbours result in far prediction
// block==-1 if bsize >= 8x8
setup_ref_mv_list(this_ref_frame, block, mi_row, mi_col, which_globalmv, partition) {
  nearest_refmv_count = 0
  newmvcount = 0
  ref_mv_count[this_ref_frame] = 0
#if VALIDATE_SPEC_MV
  if (ValidateMv) {
    validate(700010)
  }
#endif
  reset_ref_match_count()

  bw = mi_size_wide[sb_size]
  bh = mi_size_high[sb_size]
  calculate_mv_search_range(mi_row, mi_col, bw, bh)
  hastr = has_top_right(mi_row, mi_col, bw, bh, partition)

  int processedRows[1]
  processedRows[0] = 0
  int processedCols[1]
  processedCols[0] = 0

  av1_set_ref_frame(this_ref_frame)
  mode_context[this_ref_frame] = 0

  // Scan the first above row mode info.
  if (Abs(maxRowOffset) >= 1) {
    newmvcount += scan_row_mbmi(mi_row, mi_col, this_ref_frame, block, -1, which_globalmv, maxRowOffset, processedRows)
  }

#if VALIDATE_SPEC_MV
  if (ValidateMv) {
    validate(700011)
    validate(row_match_count > 0 ? 1 : 0)
  }
#endif
  // Scan the first left column mode info.
  if (Abs(maxColOffset) >= 1) {
    newmvcount += scan_col_mbmi(mi_row, mi_col, this_ref_frame, block, -1, which_globalmv, maxColOffset, processedCols)
  }

#if VALIDATE_SPEC_MV
  if (ValidateMv) {
    validate(700012)
    validate(row_match_count > 0 ? 1 : 0)
    validate(col_match_count > 0 ? 1 : 0)
  }
#endif

  // Check top-right boundary
#if VALIDATE_SPEC_MV_REF_CANDIDATE
  if (ValidateMv) {
    validate(70020)
    validate(mi_row)
    validate(mi_col)
    validate(bw)
    validate(bh)
  }
#endif
  if (hastr)
    newmvcount += scan_blk_mbmi(mi_row, mi_col, this_ref_frame, block, -1, bw, which_globalmv)

#if VALIDATE_SPEC_MV
  if (ValidateMv) {
    validate(700013)
    validate(row_match_count > 0 ? 1 : 0)
    validate(col_match_count > 0 ? 1 : 0)
  }
#endif

  nearest_match_count = (row_match_count > 0) + (col_match_count > 0)
  nearest_refmv_count = ref_mv_count[this_ref_frame]

  for (idx = 0; idx < nearest_refmv_count; idx++)
    weight_stack[this_ref_frame][idx] += REF_CAT_LEVEL

  // Search multiple collocated entries for another motion vector
  if (can_use_ref) {
    blkRowEnd = Min(bh, mi_size_high[BLOCK_64X64])
    blkColEnd = Min(bw, mi_size_wide[BLOCK_64X64])
    step_h = (bh >= mi_size_high[BLOCK_64X64]) ? mi_size_high[BLOCK_16X16] : mi_size_high[BLOCK_8X8]
    step_w = (bw >= mi_size_wide[BLOCK_64X64]) ? mi_size_wide[BLOCK_16X16] : mi_size_wide[BLOCK_8X8]
    for (deltaRow = 0; deltaRow < blkRowEnd; deltaRow += step_h) {
      for (deltaCol = 0; deltaCol < blkColEnd; deltaCol += step_w) {
        add_tpl_ref_mv(mi_row, mi_col, this_ref_frame, deltaRow, deltaCol)
      }
    }

    vOffset = Max(mi_size_high[BLOCK_8X8], bh)
    hOffset = Max(mi_size_wide[BLOCK_8X8], bw)
    int tplSamplePos[3][2]
    tplSamplePos[0][0] = vOffset
    tplSamplePos[0][1] = -2
    tplSamplePos[1][0] = vOffset
    tplSamplePos[1][1] = hOffset
    tplSamplePos[2][0] = vOffset - 2
    tplSamplePos[2][1] = hOffset
    allowExtension = ((bh >= mi_size_high[BLOCK_8X8]) && (bh < mi_size_high[BLOCK_64X64]) &&
                      (bw >= mi_size_wide[BLOCK_8X8]) && (bw < mi_size_wide[BLOCK_64X64]))

    for (i = 0; i < 3 && allowExtension; i++) {
      deltaRow = tplSamplePos[i][0]
      deltaCol = tplSamplePos[i][1]

      if (check_sb_border(mi_row, mi_col, deltaRow, deltaCol)) {
        add_tpl_ref_mv(mi_row, mi_col, this_ref_frame, deltaRow, deltaCol)
      }
    }
  }

  // Scan the second outer area.
  scan_blk_mbmi(mi_row, mi_col, this_ref_frame, block, -1, -1, which_globalmv)
  for (idx = 2; idx <= MVREF_ROWS; idx++) {
    rowOffset = -(idx << 1) + 1 + rowAdj
    colOffset = -(idx << 1) + 1 + colAdj
    if ((Abs(rowOffset) <= Abs(maxRowOffset)) &&
        (Abs(rowOffset) > processedRows[0])) {
      scan_row_mbmi(mi_row, mi_col, this_ref_frame, block, -idx, which_globalmv, maxRowOffset, processedRows)
    }
    if ((Abs(colOffset) <= Abs(maxColOffset)) &&
        (Abs(colOffset) > processedCols[0])) {
      scan_col_mbmi(mi_row, mi_col, this_ref_frame, block, -idx, which_globalmv, maxColOffset, processedCols)
    }
  }

#if VALIDATE_SPEC_MV
  if (ValidateMv) {
    validate(70002)
    validate(row_match_count > 0 ? 1 : 0)
    validate(col_match_count > 0 ? 1 : 0)
    for(i=0;i<ref_mv_count[this_ref_frame];i++) {
      validate(i)
      validate(weight_stack[this_ref_frame][i])
      validate(ref_mv_stack[this_ref_frame][i][ 0 ][ 0 ])
      validate(ref_mv_stack[this_ref_frame][i][ 0 ][ 1 ])
    }
  }
#endif

  ref_match_count = (row_match_count > 0) + (col_match_count > 0)

#if VALIDATE_SPEC_MV
  if (ValidateMv) {
    validate(70000)
    validate(nearest_match_count)
    validate(ref_match_count)
    validate(newmvcount)
  }
#endif
  if (nearest_match_count == 0) {
      mode_context[this_ref_frame] |= 0 // This seems a weird operation?
      if (ref_match_count >= 1) mode_context[this_ref_frame] |= 1

      if (ref_match_count == 1)
        mode_context[this_ref_frame] |= (1 << REFMV_OFFSET)
      else if (ref_match_count >= 2)
        mode_context[this_ref_frame] |= (2 << REFMV_OFFSET)
  } else if (nearest_match_count==1) {
      mode_context[this_ref_frame] |= (newmvcount > 0) ? 2 : 3

      if (ref_match_count == 1)
        mode_context[this_ref_frame] |= (3 << REFMV_OFFSET)
      else if (ref_match_count >= 2)
        mode_context[this_ref_frame] |= (4 << REFMV_OFFSET)
  } else {
      if (newmvcount >= 1)
        mode_context[this_ref_frame] |= 4
      else
        mode_context[this_ref_frame] |= 5

      mode_context[this_ref_frame] |= (5 << REFMV_OFFSET)
  }

  // Rank the likelihood and assign nearest and near mvs.

  // len tracks the number that we still need to sort

  // First sort the first part of the array
  len = nearest_refmv_count
  while (len > 0) {
    nr_len = 0
    for (idx = 1; idx < len; idx++) {
      if (weight_stack[this_ref_frame][idx - 1] < weight_stack[this_ref_frame][idx]) {
        swap_stack(this_ref_frame,idx,idx-1)
        nr_len = idx
      }
    }
    len = nr_len
  }

  // Then sort the second part
  len = ref_mv_count[this_ref_frame]
  while (len > nearest_refmv_count) {
    nr_len = nearest_refmv_count
    for (idx = nearest_refmv_count + 1; idx < len; idx++) {
      if (weight_stack[this_ref_frame][idx - 1] < weight_stack[this_ref_frame][idx]) {
        swap_stack(this_ref_frame,idx,idx-1)
        nr_len = idx
      }
    }
    len = nr_len
  }

  // Set up newmv contexts based on how close each ref mv is to its corresponding pred mv.
  // Note: To match the reference code, we have to do this *before* clamping the ref mv stack.
  for (idx = 0; idx < ref_mv_count[this_ref_frame]; idx++) {
    for (refList = 0; refList < 1 + (this_ref_frame > ALTREF_FRAME); refList++) {
      m0 = ref_mv_stack[this_ref_frame][idx][refList][0]
      m1 = ref_mv_stack[this_ref_frame][idx][refList][1]

      b0 = pred_mv_stack[this_ref_frame][idx][refList][0]
      b1 = pred_mv_stack[this_ref_frame][idx][refList][1]

      if (Abs(b0-m0)<=4 && Abs(b1-m1)<=4)
        pred_diff_ctx[this_ref_frame][idx][refList] = 2
      else
        pred_diff_ctx[this_ref_frame][idx][refList] = 1
    }
  }

  if (ref_mv_count[this_ref_frame] < 2) {
    extra_mv_search(mi_row, mi_col, this_ref_frame)
  }

  // Clamp stack entries
  for (idx = 0; idx < ref_mv_count[this_ref_frame]; idx++) {
    clamp_stack(mi_row, mi_col, bw<<MI_SIZE_LOG2, bh<<MI_SIZE_LOG2, this_ref_frame, idx, 0)
    if (rf[1] > NONE)
      clamp_stack(mi_row, mi_col, bw<<MI_SIZE_LOG2, bh<<MI_SIZE_LOG2, this_ref_frame, idx, 1)
  }

  if (rf[1] == NONE) {
    // Copy stack entries into the mv ref list
    for (idx = 0; idx < Min(MAX_MV_REF_CANDIDATES, ref_mv_count[this_ref_frame]); idx++) {
      for(i=0;i<2;i++)
        mv_ref_list[idx][i] = ref_mv_stack[this_ref_frame][idx][0][i]
    }
    // Fill the rest of the mv ref list with globalmv
    for (idx = ref_mv_count[this_ref_frame]; idx < MAX_MV_REF_CANDIDATES; idx++) {
      for (i = 0; i < 2; i++)
        mv_ref_list[idx][i] = globalmv[0][i]
    }
  }
}

extra_mv_search(mi_row, mi_col, this_ref_frame) {
  isCompound = this_ref_frame > ALTREF_FRAME

  if (isCompound) {
    for (list = 0; list < 2; list++) {
      refIdCount[list] = 0
      refDiffCount[list] = 0
    }
  }

  w4 = Min(Min(MI_SIZE_64X64, mi_size_wide[sb_size]), mi_cols - mi_col)
  h4 = Min(Min(MI_SIZE_64X64, mi_size_high[sb_size]), mi_rows - mi_row)

  // Re-scan the above row
  mvRow = mi_row - 1
  for (idx = 0; idx < Min(w4, h4) && ref_mv_count[this_ref_frame] < 2;) {
    mvCol = mi_col + idx
    if (! is_inside(mvCol, mvRow))
      break

    add_extra_mv_candidate(mvRow, mvCol, this_ref_frame, isCompound)

    // Only scan each block once - this does affect the output, as
    // we are able to insert the same mv into the list multiple times!
    bsize = sb_sizes[mvCol][mvRow]
    idx += mi_size_wide[bsize]
  }

  // Re-scan the left column
  mvCol = mi_col - 1
  for (idx = 0; idx < Min(w4, h4) && ref_mv_count[this_ref_frame] < 2;) {
    mvRow = mi_row + idx
    if (! is_inside(mvCol, mvRow))
      break

    add_extra_mv_candidate(mvRow, mvCol, this_ref_frame, isCompound)

    // Only scan each block once - this does affect the output, as
    // we are able to insert the same mv into the list multiple times!
    bsize = sb_sizes[mvCol][mvRow]
    idx += mi_size_high[bsize]
  }

  if (isCompound) {
    // Synthesize some compound MVs out of the single MVs we picked up
    int32 combinedMvs[2][2][2]
    for (list = 0; list < 2; list++) {
      compCount = 0
      for (idx = 0; idx < refIdCount[list] && compCount < 2; idx++) {
        for (i = 0; i < 2; i++) {
          combinedMvs[compCount][list][i] = refIdMvs[list][idx][i]
        }
        compCount++
      }
      for (idx = 0; idx < refDiffCount[list] && compCount < 2; idx++) {
        for (i = 0; i < 2; i++) {
          combinedMvs[compCount][list][i] = refDiffMvs[list][idx][i]
        }
        compCount++
      }
      while (compCount < 2) {
        for (i = 0; i < 2; i++) {
          combinedMvs[compCount][list][i] = globalmv[list][i]
        }
        compCount++
      }
    }

    // Insert MVs into the stack
    if (ref_mv_count[this_ref_frame] == 1) {
      // Skip the first combined MV if it's already in the stack
      if (combinedMvs[0][0][0] == ref_mv_stack[this_ref_frame][0][0][0] &&
          combinedMvs[0][0][1] == ref_mv_stack[this_ref_frame][0][0][1] &&
          combinedMvs[0][1][0] == ref_mv_stack[this_ref_frame][0][1][0] &&
          combinedMvs[0][1][1] == ref_mv_stack[this_ref_frame][0][1][1]) {
        srcList = 1
      } else {
        srcList = 0
      }
      for (idx = 0; idx < 2; idx++) {
        for (i = 0; i < 2; i++) {
          ref_mv_stack[this_ref_frame][1][idx][i] = combinedMvs[srcList][idx][i]
        }
      }
      weight_stack[this_ref_frame][1] = 2
#if VALIDATE_SPEC_MV_REF_CANDIDATE
      if (ValidateMv) {
        validate(70007)
        validate(ref_mv_count[this_ref_frame])
        validate(combinedMvs[srcList][0][ 0 ])
        validate(combinedMvs[srcList][0][ 1 ])
        validate(combinedMvs[srcList][1][ 0 ])
        validate(combinedMvs[srcList][1][ 1 ])
      }
#endif
    } else {
      for (list = 0; list < 2; list++) {
        for (idx = 0; idx < 2; idx++) {
          for (i = 0; i < 2; i++) {
            ref_mv_stack[this_ref_frame][list][idx][i] = combinedMvs[list][idx][i]
          }
        }
        weight_stack[this_ref_frame][list] = 2
#if VALIDATE_SPEC_MV_REF_CANDIDATE
        if (ValidateMv) {
          validate(70008)
          //validate(ref_mv_count[this_ref_frame])
          validate(combinedMvs[list][0][ 0 ])
          validate(combinedMvs[list][0][ 1 ])
          validate(combinedMvs[list][1][ 0 ])
          validate(combinedMvs[list][1][ 1 ])
        }
#endif
      }
    }
    ref_mv_count[this_ref_frame] = 2
  } else {
    // Single ref; just insert copies of the global MV
    for (list = ref_mv_count[this_ref_frame]; list < 2; list++) {
      for (i = 0; i < 2; i++) {
        ref_mv_stack[this_ref_frame][list][0][i] = globalmv[0][i]
      }
    }
  }
}

add_extra_mv_candidate(mvRow, mvCol, this_ref_frame, isCompound) {
#if VALIDATE_SPEC_MV_REF_CANDIDATE
  if (ValidateMv) {
    validate(70030)
    validate(mvRow)
    validate(mvCol)
  }
#endif
  if (isCompound) {
    for (candList = 0; candList < 2; candList++) {
      candRef = refframes[0][mvCol][mvRow][candList]
      for (list = 0; list < 2; list++) {
        if (candRef == ref_frame[list] && refIdCount[list] < 2) {
          for (i = 0; i < 2; i++) {
            refIdMvs[list][refIdCount[list]][i] = mvs[0][mvCol][mvRow][candList][0][i]
          }
#if VALIDATE_SPEC_MV_REF_CANDIDATE
          if (ValidateMv) {
            validate(70020)
            validate(refIdCount[list])
            validate(refIdMvs[list][refIdCount[list]][0])
            validate(refIdMvs[list][refIdCount[list]][1])
          }
#endif
          refIdCount[list]++
        } else if (candRef > INTRA_FRAME && refDiffCount[list] < 2) {
          sign = (ref_frame_sign_bias[candRef] == ref_frame_sign_bias[ref_frame[list]]) ? 1 : -1
          for (i = 0; i < 2; i++) {
            refDiffMvs[list][refDiffCount[list]][i] = sign * mvs[0][mvCol][mvRow][candList][0][i]
          }
#if VALIDATE_SPEC_MV_REF_CANDIDATE
          if (ValidateMv) {
            validate(70021)
            validate(refDiffCount[list])
            validate(refDiffMvs[list][refDiffCount[list]][0])
            validate(refDiffMvs[list][refDiffCount[list]][1])
          }
#endif
          refDiffCount[list]++
        }
      }
    }
  } else {
    for (candList = 0; candList < 2; candList++) {
      candRef = refframes[0][mvCol][mvRow][candList]
      if (candRef > INTRA_FRAME) {
        sign = (ref_frame_sign_bias[candRef] == ref_frame_sign_bias[ref_frame[0]]) ? 1 : -1
        for (idx = 0; idx < ref_mv_count[this_ref_frame]; idx++) {
          if (ref_mv_stack[this_ref_frame][idx][0][0] == sign * mvs[0][mvCol][mvRow][candList][0][0] &&
              ref_mv_stack[this_ref_frame][idx][0][1] == sign * mvs[0][mvCol][mvRow][candList][0][1]) {
            break // Indicate that this mv is already in the stack
          }
        }
        if (idx == ref_mv_count[this_ref_frame]) {
          // New mv candidate
          for (i = 0; i < 2; i++) {
            ref_mv_stack[this_ref_frame][idx][0][i] = sign * mvs[0][mvCol][mvRow][candList][0][i]
          }
          weight_stack[this_ref_frame][idx] = 2
#if VALIDATE_SPEC_MV_REF_CANDIDATE
          if (ValidateMv) {
            validate(70009)
            validate(ref_mv_count[this_ref_frame])
            validate(ref_mv_stack[this_ref_frame][idx][0][0])
            validate(ref_mv_stack[this_ref_frame][idx][0][1])
          }
#endif
          ref_mv_count[this_ref_frame]++
        }
      }
    }
  }
}

// this_ref_frame can be a single frame (e.g. LAST_FRAME) or a compound frame containing two frame references
// We mark a flag if all candidate motion vectors are zero
// Note: This function assumes that it is being called after an appropriate call to
// setup_globalmv(), to set up globalmv.
find_mv_refs(this_ref_frame, mi_row, mi_col, which_globalmv, partition) {
  // Blank the reference vector list
  for(i=0;i<MAX_MV_REF_CANDIDATES;i++)
    for(j=0;j<2;j++)
      mv_ref_list[i][j] = globalmv[which_globalmv][j]

  setup_ref_mv_list(this_ref_frame, -1, mi_row, mi_col, which_globalmv, partition)

}


save_mode_info(dst,src) {
  CHECK(src == 0 || src >= 2, "unexpected src number")
  frnum = (src == 0) ? frame_number : ref_frame_number[src-2]
  if (dst==1) {
    prev_mv_frame_number = frnum
  } else if (dst>=2) {
    ref_frame_number[dst-2] = frnum
    ref_intra_only[dst-2] = intra_only
    ref_type[dst-2] = frame_type
    ref_temporal_layer[dst-2] = temporal_layer
    ref_spatial_layer[dst-2] = spatial_layer
  }
  ref_mi_rows[dst] = ref_mi_rows[src]
  ref_mi_cols[dst] = ref_mi_cols[src]
  for(mi_row=0;mi_row<ref_mi_rows[src];mi_row++) {
    for(mi_col=0;mi_col<ref_mi_cols[src];mi_col++) {
      if (src == 0) {
        for(l=0;l<2;l++) {
          refframes[dst][mi_col][mi_row][l] = -1
          mvs[dst][mi_col][mi_row][l][3][0] = 0
          mvs[dst][mi_col][mi_row][l][3][1] = 0
        }
      }
      for(l=0;l<2;l++) {
        // Workaround: Sometimes the ref frame array can contain entries
        // such as {-1, 1}. In this case, the latter entry should be corrected
        // to -1.
        if (l == 1 && refframes[src][mi_col][mi_row][0] < LAST_FRAME) {
          refframes[dst][mi_col][mi_row][l] = -1
        } else {
          r = refframes[src][mi_col][mi_row][l]
          if (src == 0) {
            if (r>INTRA_FRAME) {
              refIdx = refFrameSide[r]
              mvRow = mvs[src][mi_col][mi_row][l][3][0]
              mvCol = mvs[src][mi_col][mi_row][l][3][1]
#if VALIDATE_SPEC_MFMV_STORAGE
              validate(42003)
              validate(refIdx==0)
              validate(mvRow)
              validate(mvCol)
              validate(mi_row)
              validate(mi_col)
#endif
              if ((refIdx >= 0) &&
                  (Abs(mvRow) <= REFMVS_LIMIT) &&
                  (Abs(mvCol) <= REFMVS_LIMIT)) {
#if VALIDATE_SPEC_MFMV_STORAGE
                if (refIdx==0) validate(r)
#endif
                refframes[dst][mi_col][mi_row][refIdx] = r
                mvs[dst][mi_col][mi_row][refIdx][3][0] = mvRow
                mvs[dst][mi_col][mi_row][refIdx][3][1] = mvCol
              }
            }
          } else {
            refframes[dst][mi_col][mi_row][l] = r
            if (r>INTRA_FRAME) {
              for(j=0;j<2;j++) {
                mvs[dst][mi_col][mi_row][l][3][j] = mvs[src][mi_col][mi_row][l][3][j]
              }
            }
          }
          if (r>INTRA_FRAME) {
            for(j=0;j<2;j++) {
              pred_mvs[dst][mi_col][mi_row][l][3][j] = pred_mvs[src][mi_col][mi_row][l][3][j]
            }
          }
        }
      }
    }
  }
  for (ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++) {
    for (i = 0; i < 6; i++) {
      ref_gm_params[dst][ref][i] = ref_gm_params[src][ref][i]
    }
  }
  for (i=0; i<ref_mi_rows[src]*ref_mi_cols[src]; i++) {
    seg_maps[dst][i] = seg_maps[src][i]
  }
  copy_segfeatures(dst, src)
  copy_lf_deltas(dst, src)
}


// TODO combine with use_mv_hp
uint1 use_mv_hp2(mvcol,mvrow) {
  return 1
}

find_best_ref_mvs(bsize, mi_row, mi_col, list) {
  // Make sure all the candidates are properly clamped etc
  for (i = 0; i < MAX_MV_REF_CANDIDATES; i++) {
    lower_mv_precision(mv_ref_list[i])
  }
  for(j=0;j<2;j++) {
    nearest_mv[list][j] = mv_ref_list[0][j]
    near_mv[list][j] = mv_ref_list[1][j]
  }
}

// refIdx says whether to use list 0 or list 1
//
//  This code updates the motion vector nearest and near for <8x8 blocks
//  The idea is to first copy any unique motion vectors from previous decoded blocks
//  Followed by using the 2 candidates from the mv_ref_list.
//
append_sub8x8_mvs_for_idx( block_idx, refIdx, mi_row, mi_col, which_globalmv) {
  ASSERT(0, "append_sub8x8_mvs_for_idx() should no longer be called")
}

int16 clamp_mv_row(mi_row,mi_col,mvec,border) {
  bh = mi_size_high[sb_size]
  mb_to_top_edge    = -((mi_row * MI_SIZE) * 8)
  mb_to_bottom_edge = ((mi_rows - bh - mi_row) * MI_SIZE) * 8
  return Clip3(mb_to_top_edge-border,mb_to_bottom_edge+border,mvec)
}

int16 clamp_mv_col(mi_row,mi_col,mvec,border) {
  bw = mi_size_wide[sb_size]
  mb_to_left_edge   = -((mi_col * MI_SIZE) * 8)
  mb_to_right_edge  = ((mi_cols - bw - mi_col) * MI_SIZE) * 8
  return Clip3(mb_to_left_edge-border,mb_to_right_edge+border,mvec)
}

// Figure out what motion vector(s) corresponds to GLOBALMV,
// using the relevant global motion models (if any).
//
// This is equivalent to the following sequence in the reference code:
// for (ref = 0; ref < 1 + is_compound; ++ref)
//   globalmv[ref] = gm_get_motion_vector(...)
setup_globalmv(mi_row, mi_col, bsize, block, is_compound) {
  for (ref = 0; ref < 1 + is_compound; ref++) {
#if VALIDATE_SPEC_MV_REF_CANDIDATE
    validate(24000)
#endif
    ref_frm = ref_frame[ref]
    if (gm_type[ref_frm] == IDENTITY) {
      globalmv[ref][0] = 0
      globalmv[ref][1] = 0
    } else if (gm_type[ref_frm] == TRANSLATION) {
      globalmv[ref][0] = gm_params[ref_frm][0] >> GM_TRANS_ONLY_PREC_DIFF
      globalmv[ref][1] = gm_params[ref_frm][1] >> GM_TRANS_ONLY_PREC_DIFF
    } else {
      x = mi_col * MI_SIZE + block_size_wide_lookup[bsize] / 2 - 1
      y = mi_row * MI_SIZE + block_size_high_lookup[bsize] / 2 - 1

      if (gm_type[ref_frm] == ROTZOOM) {
        CHECK(gm_params[ref_frm][5] == gm_params[ref_frm][2], "ROTZOOM model not properly filled out")
        CHECK(gm_params[ref_frm][4] == -gm_params[ref_frm][3], "ROTZOOM model not properly filled out")
      }
      CHECK(gm_type[ref_frm] <= AFFINE, "Argon Streams currently doesn't support model types >AFFINE")

      xc = (gm_params[ref_frm][2] - (1 << WARPEDMODEL_PREC_BITS)) * x +
           gm_params[ref_frm][3] * y +
           gm_params[ref_frm][0]
      yc = gm_params[ref_frm][4] * x +
           (gm_params[ref_frm][5] - (1 << WARPEDMODEL_PREC_BITS)) * y +
           gm_params[ref_frm][1]
      if (allow_high_precision_mv) {
        tx = ROUND_POWER_OF_TWO_SIGNED(xc, WARPEDMODEL_PREC_BITS - 3)
        ty = ROUND_POWER_OF_TWO_SIGNED(yc, WARPEDMODEL_PREC_BITS - 3)
      } else {
        tx = ROUND_POWER_OF_TWO_SIGNED(xc, WARPEDMODEL_PREC_BITS - 2) * 2
        ty = ROUND_POWER_OF_TWO_SIGNED(yc, WARPEDMODEL_PREC_BITS - 2) * 2
      }

      globalmv[ref][0] = ty
      globalmv[ref][1] = tx
    }
    if (cur_frame_force_integer_mv) {
      globalmv[ref][0] = integer_mv_precision(globalmv[ref][0])
      globalmv[ref][1] = integer_mv_precision(globalmv[ref][1])
    }
#if VALIDATE_SPEC_MV_REF_CANDIDATE
      validate(globalmv[ref][0])
      validate(globalmv[ref][1])
#endif
  }

  if (!is_compound) {
    globalmv[1][0] = 0
    globalmv[1][1] = 0
  }
}

int get_relative_dist(x, y) {
#if ENCODE
  orderHintEnabled = enc_enable_order_hint
  orderHintBits = enc_order_hint_bits
#else
  orderHintEnabled = enable_order_hint
  orderHintBits = order_hint_bits_minus1 + 1
#endif

  if (!orderHintEnabled) {
    return 0
  }
  return wrap_int(x - y, orderHintBits)
}

get_mv_projection(int24 refRow, int24 refCol, num, den) {
  den = Min(den, MAX_FRAME_DISTANCE)
  ASSERT(den >= 0 && den <= 63, "den out of range")

  num = Clip3(-MAX_FRAME_DISTANCE, num, MAX_FRAME_DISTANCE)
  ASSERT(num >= -31 && num <= 31, "num out of range")

  proj_mv[0] = Clip3(MV_LOW+1,MV_UPP-1,ROUND_POWER_OF_TWO_SIGNED(refRow * num * div_mult[den], 14))
  proj_mv[1] = Clip3(MV_LOW+1,MV_UPP-1,ROUND_POWER_OF_TWO_SIGNED(refCol * num * div_mult[den], 14))

#if VALIDATE_SPEC_MFMV
  validate(42001)
  validate(refRow)
  validate(refCol)
  validate(num)
  validate(den)
  validate(proj_mv[0])
  validate(proj_mv[1])
#endif
}

project( v8, delta, signBias ) {
  if (delta >= 0) {
    offset8 = delta >> ( 3 + 1 + MI_SIZE_LOG2 )
  } else {
    offset8 = -(( -delta ) >> ( 3 + 1 + MI_SIZE_LOG2 ))
  }
  return v8 + signBias * offset8
}

uint1 get_block_position(y8, x8, signBias) {
  posValid = 1

  pos_mf[0] = project(y8, proj_mv[0], signBias)
  baseH8 = (y8 >> 3) << 3
  if ( pos_mf[0] < 0 ||
       pos_mf[0] >= (mi_rows >> (3-MI_SIZE_LOG2)) ||
       pos_mf[0] < baseH8 - MAX_OFFSET_HEIGHT ||
       pos_mf[0] >= baseH8 + 8 + MAX_OFFSET_HEIGHT ) {
    posValid = 0
  }

  pos_mf[1] = project(x8, proj_mv[1], signBias)
  baseW8 = (x8 >> 3) << 3
  if ( pos_mf[1] < 0 ||
       pos_mf[1] >= (mi_cols >> (3-MI_SIZE_LOG2)) ||
       pos_mf[1] < baseW8 - MAX_OFFSET_WIDTH ||
       pos_mf[1] >= baseW8 + 8 + MAX_OFFSET_WIDTH ) {
    posValid = 0
  }

  return posValid
}

uint1 mfmv_project(src, refStamp, dstSign) {
  w8 = mi_cols >> (3-MI_SIZE_LOG2)
  h8 = mi_rows >> (3-MI_SIZE_LOG2)
  srcIdx = active_ref_idx[src - LAST_FRAME]

#if VALIDATE_SPEC_MFMV
  validate(42011)
  validate(src)
  validate(srcIdx)
#endif

  ASSERT(srcIdx >= 0, "Invalid srcIdx")
  if (ref_intra_only[srcIdx] || ref_mi_rows[2+srcIdx] != mi_rows || ref_mi_cols[2+srcIdx] != mi_cols)
    return 0

#if VALIDATE_SPEC_MFMV
  validate(42000)
  validate(src)
  validate(srcIdx)
#endif

  for ( y8 = 0; y8 < h8; y8++ ) {
    for ( x8 = 0; x8 < w8; x8++ ) {
      row = 2 * y8 + 1
      col = 2 * x8 + 1
      srcRef = refframes[srcIdx + 2][col][row][0]
      if (srcRef > INTRA_FRAME) {
        mvRow = mvs[srcIdx+2][col][row][0][3][0]
        mvCol = mvs[srcIdx+2][col][row][0][3][1]

        refToCur = get_relative_dist(saved_ref_decode_order[srcIdx][0], cur_frame_offset)
        refOffset = get_relative_dist(saved_ref_decode_order[srcIdx][0], saved_ref_decode_order[srcIdx][srcRef])

        posValid = Abs(refToCur) <= MAX_FRAME_DISTANCE && Abs(refOffset) <= MAX_FRAME_DISTANCE
        posValid = posValid && (refOffset > 0)
        if (posValid) {
          get_mv_projection( mvRow, mvCol, refToCur * dstSign, refOffset)
          posValid = get_block_position( y8, x8, dstSign )
          if ( posValid ) {
            for ( dst = LAST_FRAME; dst <= ALTREF_FRAME; dst++ ) {
              refToDst = get_relative_dist(ref_decode_order[0], ref_decode_order[dst])
              get_mv_projection( mvRow, mvCol, refToDst, refOffset)
              motion_field_mvs[ dst ][ pos_mf[1] ][ pos_mf[0] ][ 0 ] = proj_mv[ 0 ]
              motion_field_mvs[ dst ][ pos_mf[1] ][ pos_mf[0] ][ 1 ] = proj_mv[ 1 ]
# if STACKED_MFMV
              motion_field_mvs_stacked[ dst ][ pos_mf[1] ][ pos_mf[0] ][ refStamp ][ 0 ] = proj_mv[ 0 ]
              motion_field_mvs_stacked[ dst ][ pos_mf[1] ][ pos_mf[0] ][ refStamp ][ 1 ] = proj_mv[ 1 ]
# endif // STACKED_MFMV
            }
          }
        }
      }
    }
  }
  return 1
}


setup_motion_field() {
#if VALIDATE_SPEC_MFMV
  if (!can_use_ref)
    return 0
#endif
  w8 = mi_cols >> (3-MI_SIZE_LOG2)
  h8 = mi_rows >> (3-MI_SIZE_LOG2)

  for ( ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++ ) {
    for ( y = 0; y < h8 ; y++ ) {
      for ( x = 0; x < w8; x++ ) {
        for ( j = 0; j < 2; j++ ) {
# if STACKED_MFMV
          for (i=0; i<3; i++) {
            motion_field_mvs_stacked[ ref ][ x ][ y ][ i ][ j ] = INVALID_MV
          }
# endif // STACKED_MFMV
          motion_field_mvs[ ref ][ x ][ y ][ j ] = INVALID_MV
        }
      }
    }
  }

  refStamp = MFMV_STACK_SIZE - 1

  lastIdx = active_ref_idx[LAST_FRAME - LAST_FRAME]
  if (lastIdx >= 0) {
    lastAltDecodeOrder = saved_ref_decode_order[lastIdx][ALTREF_FRAME]
    curGoldDecodeOrder = ref_decode_order[GOLDEN_FRAME]
    if (lastAltDecodeOrder != curGoldDecodeOrder) {
      mfmv_project(LAST_FRAME, refStamp, -1)
    }
    refStamp--
  } else {
    ASSERT(!can_use_previous, "lastIdx should be valid here")
  }

  curBwdDecodeOrder = ref_decode_order[BWDREF_FRAME]
  if (get_relative_dist(curBwdDecodeOrder, cur_frame_offset) > 0) {
    if (mfmv_project(BWDREF_FRAME, refStamp, 1)) {
      refStamp--
    }
  }

  curAlt2DecodeOrder = ref_decode_order[ALTREF2_FRAME]
  if (get_relative_dist(curAlt2DecodeOrder, cur_frame_offset) > 0) {
    if (mfmv_project(ALTREF2_FRAME, refStamp, 1)) {
      refStamp--
    }
  }

  curAltDecodeOrder = ref_decode_order[ALTREF_FRAME]
  if ((get_relative_dist(curAltDecodeOrder, cur_frame_offset) > 0) && (refStamp >= 0)) {
    if (mfmv_project(ALTREF_FRAME, refStamp, 1)) {
      refStamp--
    }
  }

  curLast2DecodeOrder = ref_decode_order[LAST2_FRAME]
  if ((curLast2DecodeOrder >= 0) && (refStamp >= 0)) {
    if (mfmv_project(LAST2_FRAME, refStamp, -1)) {
      refStamp--
    }
  }

# if STACKED_MFMV
  // verify the stacked mfmv matches the none stacked
  for ( ref = LAST_FRAME; ref <= ALTREF_FRAME; ref++ ) {
    for ( y = 0; y < h8 ; y++ ) {
      for ( x = 0; x < w8; x++ ) {
        for (i=0; i<3; i++) {
          testMv = motion_field_mvs_stacked[ ref ][ x ][ y ][ i ][ 0 ]
          if (testMv != INVALID_MV) {
            break
          }
        }
        CHECK(testMv == motion_field_mvs[ ref ][ x ][ y ][0], "Invalid mfmv stack vs non-stack - j==0")
        if (i==3) {
          CHECK(motion_field_mvs[ ref ][ x ][ y ][ 1 ] == INVALID_MV, "2nd mfmv should be invalid here")
        } else {
          CHECK(motion_field_mvs_stacked[ ref ][ x ][ y ][ i ][ 1 ] == motion_field_mvs[ ref ][ x ][ y ][1], "Invalid mfmv stack vs non-stack - j==1")
        }
      }
    }
  }
# endif // STACKED_MFMV

  :C log_mfmv(b, h8, w8)
}

setup_ref_frame_side() {
  for (i=0; i<TOTAL_REFS_PER_FRAME; i++) {
    refFrameSide[i] = 0
  }
  for (refFrame = LAST_FRAME; refFrame <= INTER_REFS_PER_FRAME; refFrame++) {
    frameIndex = ref_decode_order[refFrame]
    curFrameIndex = ref_decode_order[0]
    if (get_relative_dist(frameIndex, curFrameIndex) > 0) {
      refFrameSide[refFrame] = 1
    } else if (frameIndex == curFrameIndex) {
      refFrameSide[refFrame] = -1
    }
  }
}


