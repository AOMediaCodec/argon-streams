/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <iosfwd>
#include <vector>

#ifndef WRITE_YUV
#define WRITE_YUV 1
#endif

#ifndef WITH_RANGES
#define WITH_RANGES 0
#endif

struct DecCommandLine
{
    DecCommandLine (int argc, char **argv);

    const char *app_name;

    // The maximum number of frames. -1 is a special value that means no limit.
    int         frame_limit;

#if WRITE_YUV
    const char *yuv_output_file;
#endif

    const char *csv_file;
    bool        csv_header_only;
    bool        csv_headerless;

    const char *file_list;

    bool        check_streams;

    bool        annex_b;
    int         max_num_oppoints;
    int         operating_point;
    bool        no_film_grain;
    bool        output_stats;

    unsigned    lst_anchor_frames;
    unsigned    lst_tile_list_obus;

    unsigned    frame_rate;

    bool        verbose;

    std::vector<const char *> inputs;

private:
    void parse (int argc, char **argv);
    void die (const char *msg) const;
    int read_int_arg (const char *name,
                              const char *arg,
                              int         min_allowed,
                              int         max_allowed) const;
    void syntax (std::ostream &os) const;
};
