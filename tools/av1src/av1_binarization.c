/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

// vp9_binarization
// calls to read_ae are automatically converted to inline calls to these functions, with the base context and context_fn prepended to the arguments
// The syntax element is also called syntax at the end of the binarisation.
// context_fn is a special keyword and is automatically converted to the syntax tree for calculating the context.

// Take care not to use common loop counters such as x and i here as they can shadow other variables

// Binarisation using a fixed number of bits for
// representing a symbol from 0 to cMax inclusive.
FL(cMax) {
  fixedLength = CeilLog2( cMax+1 )
  syntax=0
  for(binIdx=0;binIdx<fixedLength;binIdx++)
  {
    context_fn()
    decode_bin()
    syntax = (syntax<<1) + binVal
  }
}

b_skip_coeff() {
  context_fn()
  syntax = read_symbol(tile_mbskip_cdf[ctxIdxOffset], 2)
  cross_skip_ctxIdxOffset = ctxIdxOffset
}

b_skip_mode() {
  context_fn()
  syntax = read_symbol(tile_skip_mode_cdf[ctxIdxOffset], 2)
}

b_tx_depth() {
  context_fn()
  // Debugging note: Our 'maxTxDepth' is offset by 1 value
  // relative to libaom's tx_size_cat. ie, maxTxDepth == tx_size_cat + 1
  CHECK(maxTxDepth >= 0 && maxTxDepth < MAX_TX_CATS, "Invalid maxTxDepth")
  syntax = read_symbol(tile_tx_cdfs[maxTxDepth][ctxIdxOffset], depthCdfSize)
  cross_tx_size_ctxIdxOffset = ctxIdxOffset
  cross_max_coded_tx_size = maxTxDepth
}

b_intra_tx_type() {
  cdfIdx = ext_tx_set_index_intra[eset]
  ASSERT(cdfIdx >= 0, "Invalid intra tx set")
  symbol = read_symbol(tile_intra_ext_tx_cdf[cdfIdx][txsize_sqr_map[txSz]][intraDir], num_ext_tx_set[eset])
  syntax = ext_tx_set_inv[eset][symbol]
}

b_inter_tx_type() {
  cdfIdx = ext_tx_set_index_inter[eset]
  ASSERT(cdfIdx >= 0, "Invalid inter tx set")
  symbol = read_symbol(tile_inter_ext_tx_cdf[cdfIdx][txsize_sqr_map[txSz]], num_ext_tx_set[eset])
  syntax = ext_tx_set_inv[eset][symbol]
}

b_txfm_split() {
  context_fn()
  syntax = read_symbol(tile_txfm_partition_cdf[ctxIdxOffset], 2)
}

b_use_obmc() {
  syntax = read_symbol(tile_obmc_cdf[ sb_size ], 2)
}

b_motion_mode() {
  syntax = read_symbol(tile_motion_mode_cdf[sb_size], MOTION_MODES)
}

b_all_zero() {
  context_fn()
  syntax = read_symbol(tile_txb_skip_cdfs[txSzCtx][ctxIdxOffset], 2)
}

b_eob_pt_16() {
  context_fn()
  syntax = read_symbol(tile_eob_flag_16_cdfs[ptype][ctxIdxOffset], 5)
}

b_eob_pt_32() {
  context_fn()
  syntax = read_symbol(tile_eob_flag_32_cdfs[ptype][ctxIdxOffset], 6)
}

b_eob_pt_64() {
  context_fn()
  syntax = read_symbol(tile_eob_flag_64_cdfs[ptype][ctxIdxOffset], 7)
}

b_eob_pt_128() {
  context_fn()
  syntax = read_symbol(tile_eob_flag_128_cdfs[ptype][ctxIdxOffset], 8)
}

b_eob_pt_256() {
  context_fn()
  syntax = read_symbol(tile_eob_flag_256_cdfs[ptype][ctxIdxOffset], 9)
}

b_eob_pt_512() {
  context_fn()
  syntax = read_symbol(tile_eob_flag_512_cdfs[ptype][ctxIdxOffset], 10)
}

b_eob_pt_1024() {
  context_fn()
  syntax = read_symbol(tile_eob_flag_1024_cdfs[ptype][ctxIdxOffset], 11)
}

b_eob_extra() {
  context_fn()
  syntax = read_symbol(tile_eob_extra_cdfs[txSzCtx][ptype][ctxIdxOffset], 2)
}
b_coeff_base() {
  context_fn()
  syntax = read_symbol(tile_coeff_base_cdfs[txSzCtx][ptype][ctxIdxOffset],4)
}
b_coeff_base_eob() {
  context_fn()
  syntax = read_symbol(tile_coeff_base_eob_cdfs[txSzCtx][ptype][ctxIdxOffset],3)
}
b_dc_sign() {
  context_fn()
  syntax = read_symbol(tile_dc_sign_cdfs[ptype][ctxIdxOffset], 2)
}
b_coeff_br() {
  context_fn()
  syntax = read_symbol(tile_coeff_br_cdfs[Min(txSzCtx,TX_32X32)][ptype][ctxIdxOffset], BR_CDF_SIZE)
}

b_coef_extra_bits() {
  syntax = read_symbol_noadapt(coef_extrabit_cdfs[cat][cdf_index], 1 << bits)
}


b_has_palette_y() {
  context_fn()
  syntax = read_symbol(tile_palette_y_mode_cdf[ bsizeCtx ][ ctxIdxOffset ], 2)
}

b_has_palette_uv() {
  context_fn()
  syntax = read_symbol(tile_palette_uv_mode_cdf[ ctxIdxOffset ], 2)
}

b_palette_size_y() {
  context_fn()
  syntax = read_symbol(tile_palette_y_size_cdf[ctxIdxOffset], PALETTE_SIZES)
}

b_palette_size_uv() {
  context_fn()
  syntax = read_symbol(tile_palette_uv_size_cdf[ctxIdxOffset], PALETTE_SIZES)
}

b_color_idx_y() {
  context_fn()
  syntax = read_symbol(tile_palette_y_color_cdf[PaletteSizeY - 2][ctxIdxOffset], PaletteSizeY)
}

b_color_idx_uv() {
  context_fn()
  syntax = read_symbol(tile_palette_uv_color_cdf[PaletteSizeUV - 2][ctxIdxOffset], PaletteSizeUV)
}

b_use_intrabc() {
  syntax = read_symbol(tile_intrabc_cdf, 2)
}

b_use_filter_intra() {
  context_fn()
  syntax = read_symbol(tile_filter_intra_cdfs[sb_size], 2)
}

b_filter_intra_mode() {
  context_fn()
  syntax = read_symbol(tile_filter_intra_mode_cdf, FILTER_INTRA_MODES)
}


b_angle_delta_y() {
  context_fn()
  syntax = read_symbol(tile_angle_delta_cdf[y_mode - V_PRED], (2*MAX_ANGLE_DELTA + 1))
}

b_angle_delta_uv() {
  context_fn()
  syntax = read_symbol(tile_angle_delta_cdf[uv_mode - V_PRED], (2*MAX_ANGLE_DELTA + 1))
}

b_intra_mode_y() {
  context_fn()
  syntax = read_symbol(tile_y_mode_cdf[ctxIdxOffset], INTRA_MODES)
  cross_block_size_groups_ctxIdxOffset = ctxIdxOffset
}

b_default_intra_mode_y() {
  abovectx = above_intra_mode_ctx[mi_col]
  leftctx = left_intra_mode_ctx[mi_row & mi_mask2]

  syntax = read_symbol(tile_kf_y_cdf[abovectx][leftctx], INTRA_MODES)
}

b_intra_mode_uv() {
  cflAllowed = is_cfl_allowed(sb_size)
  nsymbs = INTRA_MODES + cflAllowed
  syntax = read_symbol(tile_uv_mode_cdf[cflAllowed][y_mode], nsymbs)
  cross_y_mode = y_mode
}

b_joint_type() {
  syntax = read_symbol(tile_joint_cdf[nmv_ctx], MV_JOINTS)
}

b_mv_class() {
  syntax = read_symbol(tile_mvcomp_classes_cdf[nmv_ctx][mvcomp], MV_CLASSES)
  cross_mvcomp = mvcomp
}

b_mvcomp_class0_bits() {
  syntax = read_symbol(tile_class0_cdf[nmv_ctx][mvcomp], 2)
  cross_mvcomp = mvcomp
}

b_mvcomp_class0_fp() {
  syntax = read_symbol(tile_mvcomp_class0_fp_cdf[nmv_ctx][mvcomp][mvcomp_class0_bits], MV_FP_SIZE)
  cross_mvcomp = mvcomp
  cross_mvcomp_class0_bits = mvcomp_class0_bits
}

b_mvcomp_fp() {
  syntax = read_symbol(tile_mvcomp_fp_cdf[nmv_ctx][mvcomp], MV_FP_SIZE)
  cross_mvcomp = mvcomp
}

b_drl_mode() {
  context_fn()
  syntax = read_symbol(tile_drl_cdf[ctxIdxOffset], 2)
}

b_inter_mode() {
  // TODO may be better to express this as 3 booleans?  is_newmv, is_globalmv, is_refmv?
  context_fn()
  modectx = ctxIdxOffset & NEWMV_CTX_MASK
  is_newmv = !read_symbol(tile_newmv_cdf[modectx], 2)
  if (is_newmv) {
    syntax = NEWMV
  } else {
    modectx = (ctxIdxOffset >> GLOBALMV_OFFSET) & GLOBALMV_CTX_MASK
    is_globalmv = !read_symbol(tile_globalmv_cdf[modectx], 2)
    if (is_globalmv) {
      syntax = GLOBALMV
    } else {
      modectx = (ctxIdxOffset >> REFMV_OFFSET) & REFMV_CTX_MASK
      is_refmv = !read_symbol(tile_refmv_cdf[modectx], 2)
      if (is_refmv) {
        syntax = NEARESTMV
      } else {
        syntax = NEARMV
      }
    }
  }

  cross_inter_mode_ctxIdxOffset = ctxIdxOffset
}

b_inter_compound_mode() {
  context_fn()
  syntax = NEAREST_NEARESTMV +
           read_symbol(tile_inter_compound_mode_cdf[ctxIdxOffset], INTER_COMPOUND_MODES)
}

b_compound_group_idx() {
  context_fn()
  syntax = read_symbol(tile_compound_group_idx_cdf[ctxIdxOffset], 2)
}

b_compound_idx() {
  context_fn()
  syntax = read_symbol(tile_compound_idx_cdf[ctxIdxOffset], 2)
}

b_compound_mask_type() {
  context_fn()
  syntax = read_symbol(tile_compound_mask_type_cdf[sb_size], 2)
}

b_interintra() {
  size_group = size_group_lookup[sb_size]
  syntax = read_symbol(tile_interintra_cdf[size_group], 2)
}

b_interintra_mode() {
  size_group = size_group_lookup[sb_size]
  syntax = read_symbol(tile_interintra_mode_cdf[size_group], INTERINTRA_MODES)
}

b_wedge_idx() {
  syntax = read_symbol(tile_wedge_idx_cdf[bsize], 16)
}

b_wedge_interintra() {
  syntax = read_symbol(tile_wedge_interintra_cdf[sb_size], 2)
}

// read_switchable_filter_type
b_interp_filter() {
  context_fn()
  syntax = read_symbol(tile_switchable_interp_cdf[ctxIdxOffset], SWITCHABLE_FILTERS)
  cross_switchable_filter_ctxIdxOffset = ctxIdxOffset
}

b_mvcomp_class0_hp() {
  syntax = read_symbol(tile_class0_hp_cdf[nmv_ctx][mvcomp], 2)
  cross_mvcomp = mvcomp
}

b_mvcomp_hp() {
  syntax = read_symbol(tile_hp_cdf[nmv_ctx][mvcomp], 2)
  cross_mvcomp = mvcomp
}

b_mvcomp_bits() {
  d = 0
  for (mvcompi = 0; mvcompi < numbits; mvcompi++) {
    d |= read_symbol(tile_mvcomp_bits_cdf[nmv_ctx][mvcompi][mvcomp], 2) << mvcompi
    cross_mvcomp = mvcomp
    cross_mvcompi = mvcompi
  }
  syntax = d
}

b_comp_mode() {
  context_fn()
  syntax = read_symbol(tile_comp_inter_cdf[ctxIdxOffset], 2)
  cross_comp_inter_ctxIdxOffset = ctxIdxOffset
}

b_comp_ref_p() {
  context_fn()
  syntax = read_symbol(tile_comp_ref_cdf[ctxIdxOffset][0], 2)
  cross_ref_ctxIdxOffset = ctxIdxOffset
}

b_comp_ref_p1() {
  context_fn()
  syntax = read_symbol(tile_comp_ref_cdf[ctxIdxOffset][1], 2)
  cross_ref_ctxIdxOffset = ctxIdxOffset
}

b_comp_ref_p2() {
  context_fn()
  syntax = read_symbol(tile_comp_ref_cdf[ctxIdxOffset][2], 2)
  cross_ref_ctxIdxOffset = ctxIdxOffset
}

b_comp_bwdref_p() {
  context_fn()
  syntax = read_symbol(tile_comp_bwdref_cdf[ctxIdxOffset][0], 2)
  cross_ref_ctxIdxOffset = ctxIdxOffset
}

b_comp_bwdref_p1() {
  context_fn()
  syntax = read_symbol(tile_comp_bwdref_cdf[ctxIdxOffset][1], 2)
  cross_ref_ctxIdxOffset = ctxIdxOffset
}

b_single_ref_p1() {
  context_fn()
  syntax = read_symbol(tile_single_ref_cdf[ctxIdxOffset][0], 2)
  cross_ref_ctxIdxOffset = ctxIdxOffset
}

b_single_ref_p2() {
  context_fn()
  syntax = read_symbol(tile_single_ref_cdf[ctxIdxOffset][1], 2)
  cross_ref_ctxIdxOffset = ctxIdxOffset
}

b_single_ref_p3() {
  context_fn()
  syntax = read_symbol(tile_single_ref_cdf[ctxIdxOffset][2], 2)
  cross_ref_ctxIdxOffset = ctxIdxOffset
}

b_single_ref_p4() {
  context_fn()
  syntax = read_symbol(tile_single_ref_cdf[ctxIdxOffset][3], 2)
  cross_ref_ctxIdxOffset = ctxIdxOffset
}

b_single_ref_p5() {
  context_fn()
  syntax = read_symbol(tile_single_ref_cdf[ctxIdxOffset][4], 2)
  cross_ref_ctxIdxOffset = ctxIdxOffset
}

b_single_ref_p6() {
  context_fn()
  syntax = read_symbol(tile_single_ref_cdf[ctxIdxOffset][5], 2)
  cross_ref_ctxIdxOffset = ctxIdxOffset
}

b_comp_ref_type() {
  context_fn()
  syntax = read_symbol(tile_comp_ref_type_cdf[ctxIdxOffset], 2)
}

b_uni_comp_ref_p() {
  context_fn()
  syntax = read_symbol(tile_uni_comp_ref_cdf[ctxIdxOffset][0], 2)
}
b_uni_comp_ref_p1() {
  context_fn()
  syntax = read_symbol(tile_uni_comp_ref_cdf[ctxIdxOffset][1], 2)
}
b_uni_comp_ref_p2() {
  context_fn()
  syntax = read_symbol(tile_uni_comp_ref_cdf[ctxIdxOffset][2], 2)
}

b_is_inter() {
  context_fn()
  syntax = read_symbol(tile_intra_inter_cdf[ctxIdxOffset], 2)
  cross_intra_inter_ctxIdxOffset = ctxIdxOffset
}

b_mvcomp_sign() {
  syntax = read_symbol(tile_sign_cdf[nmv_ctx][mvcomp], 2)
  cross_mvcomp = mvcomp
}

b_pred_flag() {
  context_fn()
  syntax = read_symbol(tile_segment_pred_cdf[ctxIdxOffset], 2)
  cross_prediction_probs_ctxIdxOffset = ctxIdxOffset
}

b_u(numbits) {
  syntax = 0
  for(unifi=0;unifi<numbits;unifi++)
    syntax = syntax*2+read_bit()
}

// Used for binary symbols with a fixed probability which does not adapt
// and so doesn't need a symbol count or anything like that.
b_bool(prob) {
  syntax = read_bool(prob)
}

b_partition() {
  context_fn()
  uint16 tmp_cdf[3]
  {
    if (has_rows && has_cols) {
      numPartitionTypes = num_partition_types(bsize)
      syntax = read_symbol(tile_partition_cdf[ctxIdxOffset], numPartitionTypes)
      cross_ctxIdxOffset = ctxIdxOffset
    }
    else if (!has_rows && has_cols) {
      get_partition_vertlike_cdf(tile_partition_cdf[ctxIdxOffset], tmp_cdf, bsize)
      syntax = read_symbol_noadapt(tmp_cdf, 2) ? PARTITION_SPLIT : PARTITION_HORZ
      cross_ctxIdxOffset = ctxIdxOffset
    //else if (has_rows && !has_cols)
    } else if (has_rows) {
      get_partition_horzlike_cdf(tile_partition_cdf[ctxIdxOffset], tmp_cdf, bsize)
      syntax = read_symbol_noadapt(tmp_cdf, 2) ? PARTITION_SPLIT : PARTITION_VERT
      cross_ctxIdxOffset = ctxIdxOffset
    } else
      syntax = PARTITION_SPLIT
  }
}

b_coded_id() {
  syntax = read_symbol(tile_spatial_pred_seg_cdf[cdfNum], MAX_SEGMENTS)
}

b_abs() {
  syntax = read_symbol(tile_delta_q_cdf, DELTA_Q_SMALL+1)
}
b_abs_lf() {
  if (deltaLfCdfIdx >= 0)
    syntax = read_symbol(tile_delta_lf_multi_cdf[deltaLfCdfIdx], DELTA_LF_SMALL+1)
  else
    syntax = read_symbol(tile_delta_lf_cdf, DELTA_LF_SMALL+1)
}

b_cfl_alpha_signs() {
  syntax = read_symbol(tile_cfl_sign_cdf, CFL_JOINT_SIGNS)
}

b_cfl_alpha() {
  context_fn()
  syntax = read_symbol(tile_cfl_alpha_cdf[ctxIdxOffset], CFL_ALPHABET_SIZE)
}

b_restoration_type() {
  syntax = read_symbol(tile_switchable_restore_cdf, RESTORE_SWITCHABLE_TYPES)
}

b_use_wiener() {
  syntax = read_symbol(tile_wiener_cdf, 2)
}

b_use_sgrproj() {
  syntax = read_symbol(tile_sgrproj_cdf, 2)
}
