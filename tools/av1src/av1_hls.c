/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define VALIDATE_OBU 0

// AV1 OBU + high-level syntax processing
align_to_byte() {
  uint64 bitpos
  bitpos = current_bitposition()
  if (bitpos % 8) {
    padBits = 8 - (bitpos % 8)
    padding u(padBits)
    CHECK(padding == 0, "Padding within OBUs should consist entirely of 0 bits")
  }
}

obu_trailing_bits() {
  one_bit u(1) CHECK(one_bit == 1, "one_bit should have the value 1")
  align_to_byte()
}

is_frame_in_operating_point(oppoint) {
  // Special case: If no temporal or spatial layers are signalled,
  // then we decode all frames
  if (temporal_layers_used[oppoint] == 0 && spatial_layers_used[oppoint] == 0) {
    return 1
  }

  allowed_temporal_layer = temporal_layers_used[oppoint] & (1 << temporal_layer)
  allowed_spatial_layer = spatial_layers_used[oppoint] & (1 << spatial_layer)

  return (allowed_temporal_layer && allowed_spatial_layer)
}

is_obu_in_operating_point(obu_type, oppoint) {
  if (! obu_can_use_extension_header[obu_type]) {
    return 1
  }

  // Note: When we are reading our first sequence header OBU,
  // current_operating_point_id, passed in as oppoint, will not yet have been
  // initialised (it gets set in read_sequence_header). That won't matter for
  // good streams because they start with a sequence header (which cases a
  // return 1 above).
  CHECK (oppoint <= 32,
         "Stream has skippable OBU before first sequence header.")

  return is_frame_in_operating_point(oppoint)
}


#if ENCODE
enc_post_obu_padding() {
  // Add a random amount of extra 0 bytes after the payload
  extra_obu_padding u(0)
  for (i = 0; i < extra_obu_padding; i++) {
    padding u(8)
  }
}

// Helper: Add trailing bits + some extra padding bytes to the end of an OBU
enc_obu_trailer() {
  obu_trailing_bits()
  enc_post_obu_padding()
}

reset_tile_sz() {
  // TODO can remove tile_col_sz_mag stuff entirely (tile_sz needs to stay)
  tile_col_sz_mag_location = -1
  tile_sz_mag_location = -1
}

int increase_prob(prob, rate) {
  rate = Max(rate, 1)
  return Clip3(0, 100, prob + rate)
}

int decrease_prob(prob, rate) {
  rate = Max(rate, 1)
  return Clip3(0, 100, prob - rate)
}

// Encode a particular tile subject to a maximum size constraint (in bytes)
// This may try coding the tile multiple times, adjusting various encoder parameters
// each time (to try to reduce the generated size on each recode).
// These parameters are restored to their original values before returning, so that
// effects don't propagate between tiles.
generate_one_tile(tileRow, tileCol) {
  int64 size
  int64 tileBits
  int64 minBits
  int64 maxBits
  int64 tileArea

  ASSERT(byte_aligned(), "Bitstream must be byte aligned here")
  tile_locations[tileCol][tileRow] = current_position()
  if (large_scale_tile) {
    if (camera_frame_header_ready) {
      tile_id = tileRow * tile_cols + tileCol
      enc_anchorFrameIdx_cfg u(0)
      enc_anchorFrameIdx[tileCol][tileRow] = enc_anchorFrameIdx_cfg
      load_anchor_frame(enc_anchorFrameIdx[tileCol][tileRow])
    }
  }

  if (!enc_use_bit_bucket) {
    // Simplified code path for when rate control is disabled -
    // just generate a tile and use that!
    size = decode_tile_wrapper(tileRow, tileCol, 0)
    tile_sizes[tileCol][tileRow] = size
    return 0
  }

  // Save encoder parameters
  orig_split_prob = enc_split_prob
  orig_skip_prob = enc_skip_prob
  orig_use_large_coefs_prob = enc_use_large_coefs_prob
  orig_zero_coef_prob = enc_zero_coef_prob
  orig_empty_block_prob = enc_empty_block_prob
  orig_palette_prob = enc_palette_prob
  orig_inter_prob = enc_inter_prob
  orig_compound_prob = enc_compound_prob
  orig_lossless_prob = enc_lossless_prob
  orig_bias_to_big_partitions_prob = enc_bias_to_big_partitions_prob
  orig_mv_small_prob = enc_mv_small_prob

  // Calculate a target size for this tile and an absolute maxium
  // These sizes are allowed to depend on the tile size and the #bytes used so far
  tileW = (mi_col_starts[tileCol+1] - mi_col_starts[tileCol]) << MI_SIZE_LOG2
  tileH = (mi_row_starts[tileRow+1] - mi_row_starts[tileRow]) << MI_SIZE_LOG2
  tileArea = tileW * tileH
  frameArea = crop_width * crop_height

  // To determine the target size of each tile, we use a "leaky bit bucket" model:
  //
  // We maintain a bucket of bits, which starts out empty, and may have either a positive
  // or negative level at any given time (corresponding to us having used "too few" or "too many"
  // bits in the past, respectively).
  //
  // After encoding each tile, we add a certain number of bits (based on the tile area)
  // to the bucket, then take out the number of bits we actually used to encode that tile.
  //
  // Each tile is re-coded until it is within the range of [enc_min_bpp, enc_max_bpp]
  // bits per luma pixel. If the bit bucket is not at zero, then we offset either
  // the minimum or maximum size based on the current bit bucket level, to try to
  // keep around the target bitrate.
  //
  // Finally, at the end of each frame, we allow a fraction of the bits in the bucket
  // to "leak out". This reduces the impact of a single oversized (or undersized)
  // frame on later frames.

  // We re-calculate some of the parameters once per tile, to help with generating
  // stress streams.
  enc_bit_bucket_fill_rate u(0)
  enc_min_bpp u(0)
  enc_max_bpp u(0)

  // Work out the range of tile sizes we're happy with.
  // We have a "base" minimum and maximum determined by the encode profile,
  // and then we adjust one endpoint based on the current capacity of
  // the bit bucket. We only adjust one endpoint to avoid pathological cases
  // where we end up with ridiculously large/small target sizes
  // (capacity > 0 means that we've undershot in the past and should use more bits,
  //  capacity < 0 means that we've overshot in the past and should use fewer bits)
  minBits = (tileArea * enc_min_bpp) / 100
  maxBits = (tileArea * enc_max_bpp) / 100
  offset = (enc_bit_bucket_capacity * enc_bit_bucket_offset_factor * tileArea) / (100 * frameArea)
  if (offset > 0) {
    minBits = Min(minBits + offset, maxBits)
  } else {
    maxBits = Max(maxBits + offset, minBits)
  }

  // Recode loop - try repeatedly generating tiles until the tile falls into
  // the range [minBits, maxBits].
  // If the tile is too large, we adjust a few parameters to bias toward smaller
  // tiles, but we also loosen the maxBits requirement a bit. We do the opposite
  // if the tile is too small.
  enc_max_recode_count u(0)
  count = 0
  while (1) {
    if (large_scale_tile) {
      // In large scale tile mode, the tile size field is always 2 bytes,
      // so tiles must be at most 2^16 bytes in size regardless of pixel count.
      maxBits = Min(maxBits, 8 * (1<<16))
    }

    size = decode_tile_wrapper(tileRow, tileCol, 0) // Returns size in bytes
    tileBits = size*8

    // Prepare for the next recode
    count++
    if (count >= enc_max_recode_count) {
      // If we've not fit into our size requirements within 10 attempts,
      // just use whatever the last recode produced
      break
    }

    if (enc_choose_max_bitrate) { // for max bitrate just go with it
      break
    }
    if (tileBits < minBits) {
      // Tile is too small - reject
      rate = ((100 * (minBits - tileBits) / tileBits) + 4) / 5

      enc_split_prob = increase_prob(enc_split_prob, rate)
      enc_skip_prob = decrease_prob(enc_skip_prob, rate)
      enc_use_large_coefs_prob = increase_prob(enc_use_large_coefs_prob, rate)
      enc_zero_coef_prob = decrease_prob(enc_zero_coef_prob, rate)
      enc_empty_block_prob = decrease_prob(enc_empty_block_prob, rate)
      enc_palette_prob = increase_prob(enc_palette_prob, rate)
      enc_inter_prob = decrease_prob(enc_inter_prob, rate)
      enc_compound_prob = increase_prob(enc_compound_prob, rate)
      enc_lossless_prob = increase_prob(enc_lossless_prob, rate)
      enc_bias_to_big_partitions_prob = decrease_prob(enc_bias_to_big_partitions_prob, rate)
      enc_mv_small_prob = decrease_prob(enc_mv_small_prob, rate)
    } else if (tileBits > maxBits) {
      // Adjust some encoder parameters to bias toward smaller tiles in the
      // next recode
      rate = ((100 * (tileBits - maxBits) / tileBits) + 4) / 5

      enc_split_prob = decrease_prob(enc_split_prob, rate)
      enc_skip_prob = increase_prob(enc_skip_prob, rate)
      enc_use_large_coefs_prob = decrease_prob(enc_use_large_coefs_prob, rate)
      enc_zero_coef_prob = increase_prob(enc_zero_coef_prob, rate)
      enc_empty_block_prob = increase_prob(enc_empty_block_prob, rate)
      enc_palette_prob = decrease_prob(enc_palette_prob, rate)
      enc_inter_prob = increase_prob(enc_inter_prob, rate)
      enc_compound_prob = decrease_prob(enc_compound_prob, rate)
      enc_lossless_prob = decrease_prob(enc_lossless_prob, rate)
      enc_bias_to_big_partitions_prob = increase_prob(enc_bias_to_big_partitions_prob, rate)
      enc_mv_small_prob = increase_prob(enc_mv_small_prob, rate)

      // Also loosen our target size constraint a bit each iteration.
      // If the initial target size is X, then the target size
      // at each iteration is as follows (modulo rounding):
      // X, 1.1*X, 1.21*X, 1.33*X, 1.46*X, 1.61*X, 1.77*X, 1.95*X, 2.14*X, 2.36*X
      maxBits += (maxBits / 10)
    } else {
      // Accept this encoding for the tile
      break
    }

    unused = set_pos(tile_locations[tileCol][tileRow])
  }
  tile_sizes[tileCol][tileRow] = size
  if (large_scale_tile) {
    CHECK(size <= (1 << 16), "[LARGE_SCALE_TILE] Tiles must be <= 2^16 bytes in large-scale-tile mode")
    ASSERT(size <= (1 << 16), "Tiles must be <= 2^16 bytes in large-scale-tile mode")
  }

  // Add in the bit allocation for this tile, then subtract the number
  // of bits we actually used
  cap = enc_bit_bucket_capacity
  enc_bit_bucket_capacity += (tileArea * enc_bit_bucket_fill_rate) / 100
  enc_bit_bucket_capacity -= size*8
  enc_bit_bucket_capacity = Max(enc_bit_bucket_capacity, 0)

  // Restore encoder parameters
  enc_split_prob = orig_split_prob
  enc_skip_prob = orig_skip_prob
  enc_use_large_coefs_prob = orig_use_large_coefs_prob
  enc_zero_coef_prob = orig_zero_coef_prob
  enc_empty_block_prob = orig_empty_block_prob
  enc_palette_prob = orig_palette_prob
  enc_inter_prob = orig_inter_prob
  enc_compound_prob = orig_compound_prob
  enc_lossless_prob = orig_lossless_prob
  enc_bias_to_big_partitions_prob = orig_bias_to_big_partitions_prob
  enc_mv_small_prob = orig_mv_small_prob
}

// Write the frame header and tiles into nest level 1.
// This only generates the raw data - no OBU headers or tile sizes are written.
// This data is then re-packed into nest level 0 by pack_frame()
generate_tiles() {
  unused = start_nest()

  // Reset the variables which track locations we need to fix up
  // after encoding the frame.
  frameHeaderCopies = 0
  for (i = 0; i < enc_num_operating_points; i++) {
    enc_buffer_removal_delay_offset[i] = -1
  }
  enc_tu_presentation_delay_offset = -1

  // Generate frame header. We need to track the exact bit length, since
  // it might or might not need to be followed by a trailing 1 bit, depending
  // on whether we use an OBU_FRAME_HEADER or an OBU_FRAME.
  frameHeaderStartBytes = current_position()
  frameHeaderStart = current_bitposition()
  reset_tile_sz()
  if (!(large_scale_tile && camera_frame_header_ready)) { // no more headers in this case
    decode_frame_headers()
    if (camera_frame_header_ready) {
      ASSERT(!intra_only, "For large scale tile, frames must be INTER_FRAME")
    }
  }
  frameHeaderBits = current_bitposition() - frameHeaderStart
  align_to_byte()
  frameHeaderBytes = current_position() - frameHeaderStartBytes
  frameHeaderRewindBits = (frameHeaderBytes*8) - frameHeaderBits

  if (! show_existing_frame
      && (!large_scale_tile || camera_frame_header_ready) // don't generate for camera frame ready header
      ) {
    // Encode tiles in raster order
    tile_id = 0
    for (tileRow = 0; tileRow < tile_rows; tileRow++) {
      for (tileCol = 0; tileCol < tile_cols; tileCol++) {
        generate_one_tile(tileRow, tileCol)
        tile_id++
      }
    }
    if (enc_use_bit_bucket) {
      // Once we've generated all tiles for this frame, allow some
      // bits to "leak out" from the bit bucket. This makes it so that
      // one giant frame won't force all later frames to be tiny (or vice versa)
      cap = enc_bit_bucket_capacity
      l = enc_bit_bucket_leak_factor
      enc_bit_bucket_capacity -= (enc_bit_bucket_capacity * enc_bit_bucket_leak_factor) / 100
      enc_bit_bucket_capacity = Max(enc_bit_bucket_capacity, 0)
      newCap = enc_bit_bucket_capacity
    }

    // Apply post-frame updates
    wrapup_frame()

    // Find the largest tile and tile column sizes, so we know
    // how wide to make the size fields
    int64 maxTileSize // to ensure the assertions below can be tested
    maxTileSize = 0
    for (tileRow = 0; tileRow < tile_rows; tileRow++) {
      for (tileCol = 0; tileCol < tile_cols; tileCol++) {
        maxTileSize = Max(maxTileSize, tile_sizes[tileCol][tileRow])
      }
    }

    // Calculate the tile size field width and update the frame header
    tile_sz_mag = 0

    if (tile_sz_mag_location >= 0) {
      if (maxTileSize > (1<<8)) {
        tile_sz_mag = 1
      }
      if (maxTileSize > (1<<16)) {
        tile_sz_mag = 2
      }
      if (maxTileSize > (1<<24)) {
        tile_sz_mag = 3
      }
      ASSERT(maxTileSize > 0 && maxTileSize <= (1<<32), "Invalid maxTileSize")
      unused = overwritebit(tile_sz_mag_location, (tile_sz_mag & 2)>>1)
      unused = overwritebit(tile_sz_mag_location + 1, tile_sz_mag & 1)
    }

    // Similar for the tile column size field - but note that this depends
    // on the calculated value of tile_sz_mag
    int64 maxTileColSize
    maxTileColSize = 0
    readTileSize = (tile_cols * tile_rows > 1)
    for (tileCol = 0; tileCol < tile_cols; tileCol++) {
      tile_col_sizes[tileCol] = 0
      for (tileRow = 0; tileRow < tile_rows; tileRow++) {
        size = tile_sizes[tileCol][tileRow]
        tile_col_sizes[tileCol] += size
        if (readTileSize) {
          tile_col_sizes[tileCol] += tile_sz_mag + 1
        }
      }
      maxTileColSize = Max(maxTileColSize, tile_col_sizes[tileCol])
    }

    if (tile_col_sz_mag_location >= 0) {
      if (maxTileColSize > ((1<<8)-1)) {
        tile_col_sz_mag = 1
      }
      if (maxTileColSize > ((1<<16)-1)) {
        tile_col_sz_mag = 2
      }
      if (maxTileColSize > ((1<<24)-1)) {
        tile_col_sz_mag = 3
      }
      ASSERT(maxTileColSize > 0 && maxTileColSize <= ((1<<32)-1), "Invalid maxTileColSize")

      unused = overwritebit(tile_col_sz_mag_location, (tile_col_sz_mag & 2)>>1)
      unused = overwritebit(tile_col_sz_mag_location + 1, tile_col_sz_mag & 1)
    }
  }

  unused = end_nest()
}

// Generate the OBU sequence for a single frame.
// Because there are a lot of rules about what needs to go where in the high-level
// syntax, we currently encode a semi-fixed sequence of OBUs for each frame.
encode_one_frame() {
  // Prepare frame type, whether we show this frame, etc.
  temporal_layer = sched_temporal_layer[frame_number]
  spatial_layer = sched_spatial_layer[frame_number]
  enc_setup_frame()

  uint64 frameSizeLocation
  frameSizeLocation = current_bitposition()
  frameSizeBytes = 0
  if (use_annexb) {
    frameSizeBytes u(0)
    frameSize = 0
    frameSize u(8*frameSizeBytes)
  }
  uint64 frameStart
  frameStart = current_position()

  // Write a temporal delimiter if needed.
  // If we do write one, then we won't need another until the next
  // call to encode_temporal_unit()
  if (enc_insert_td) {
    write_obu(OBU_TD)
    enc_insert_td = 0
  }

  // Manually specify metadata in configuration file
  enc_specify_metadata u(0)

  // We must send a sequence header with the first frame, but have no other constraints.
  // So for the moment, send a repeat with 10% of frames.
  send_seq_header = 0
  if (frame_number == 0) {
    send_seq_header = 1
  } else {
    enc_repeat_sequence_header u(0)
    send_seq_header = enc_repeat_sequence_header
  }

  if (send_seq_header) {
    write_obu(OBU_SEQUENCE_HEADER)
    // We should send scalability metadata (if any) immediately after the sequence header
    if (!enc_specify_metadata && enc_use_scalability) {
      enc_metadata_type = METADATA_TYPE_SCALABILITY
      write_obu(OBU_METADATA)
    }
    if (!enc_specify_metadata && enc_large_scale_tile
        && !enc_large_scale_tile_metadata_sent) {
      enc_metadata_type = METADATA_TYPE_ARGON_DESIGN_LARGE_SCALE_TILE
      write_obu(OBU_METADATA)
      enc_large_scale_tile_metadata_sent = 1
    }
  }

  if (!enc_specify_metadata && Choose(0, 99) < 75) {
    if (Choose(0, 99) < 25) {
      write_obu(OBU_PADDING)
    }
    if (ChooseBit()) {
      enc_metadata_type u(0)
      write_obu(OBU_METADATA)
    }
    if (Choose(0, 99) < 25) {
      write_obu(OBU_PADDING)
    }
  }

  if (enc_specify_metadata) {
    // Specify number of metadata OBUs to write
    enc_num_metadata_obus u(0)

    // Write the metadata OBUs
    for (i = 0; i < enc_num_metadata_obus; i++) {
        enc_metadata_type_cfg u(0)
        enc_metadata_type = enc_metadata_type_cfg
        write_obu (OBU_METADATA)
    }
  }

  // Generate the frame header and tiles, and store them into nest level 1.
  // Then we can re-pack them into OBUs later.
  if (enc_show_existing_frame) {
    ASSERT(!large_scale_tile, "Not expecting show existing frame with large_scale_tile")
  } else {
    enc_frame_width  u(0)
    enc_frame_height u(0)

    enc_pick_frame_size (enc_frame_width == 0, enc_frame_height == 0)
  }
  generate_tiles()

  enc_setup_tile_groups()

  if (large_scale_tile) {
    if (camera_frame_header_ready) {
      ASSERT(frame_number > enc_anchor_frame_cnt, "OBU_TILE_LIST: Should only write one camera frame ready header once")
      write_obu(OBU_TILE_LIST)
    } else {
      ASSERT(frame_number == enc_anchor_frame_cnt, "OBU_FRAME_HEADER: Should only write one camera frame ready header once")
      write_obu(OBU_FRAME_HEADER)
    }
  } else {
    if (num_tile_groups == 1) {
      enc_use_obu_frame u(0)
    } else {
      enc_use_obu_frame = 0
    }

    if (enc_use_obu_frame) {
      current_tile_group = 0
      write_obu(OBU_FRAME)
    } else {
      write_obu(OBU_FRAME_HEADER)
      for (current_tile_group = 0; current_tile_group < num_tile_groups; current_tile_group++) {
        if (Choose(0, 99) == 0 && frameHeaderCopies < ENC_MAX_FRAME_HEADER_COPIES) {
          write_obu(OBU_REDUNDANT_FRAME_HEADER)
        }
        write_obu(OBU_TILE_GROUP)
      }
    }
  }

  uint64 frameEnd
  frameEnd = current_position()
  if (use_annexb) {
    overwrite_uleb128(frameEnd - frameStart, frameSizeLocation, frameSizeBytes)
  }

  if (decoder_model_mode != DECODER_MODEL_DISABLED) {
    if (! equal_picture_interval) {
      enc_reset_min_presentation_time()
    }
    for (i = 0; i < enc_num_operating_points; i++) {
      if (is_frame_in_operating_point(i)) {
        update_decoder_model(i)
      }
    }
    if (show_frame || show_existing_frame) {
      // TODO: Pick a presentation time within a range above the minimum
      if (! equal_picture_interval) {
        presentationTime = enc_min_presentation_time
        enc_set_presentation_time(presentationTime)
      }
      // Prepare for the next shown frame.
      // This is split out on its own because it must be done *after* we set
      // the frame presentation time
      for (i = 0; i < enc_num_operating_points; i++) {
        if (is_frame_in_operating_point(i)) {
          current_show_frame_idx[i] += 1
        }
      }
    }
  }
}

pack_tile_group() {
  ASSERT(!large_scale_tile, "Should call this when large_scale_tile set")

  tile_id = 0
  for (tileRow = 0; tileRow < tile_rows; tileRow++) {
    for (tileCol = 0; tileCol < tile_cols; tileCol++) {
      readTileSize = (tile_id != tg_end)
      if ((tile_id >= tg_start) && (tile_id <= tg_end)) {
        size = tile_sizes[tileCol][tileRow]
        tileLocation = tile_locations[tileCol][tileRow]
        if (readTileSize) {
          codedSize = size - 1
          ASSERT(codedSize < (1<<(8*(tile_sz_mag+1))), "tile_sz_mag isn't big enough")
          codedSize le(8*(tile_sz_mag+1))
        }
        // Copy the tile data from nest level 1
        unused = copy_from_nest(1, tileLocation, size)
      }
      tile_id++
    }
  }
}

write_obu(obu_type) {
  // silence warnings
  uint64 obuSizeLocation
  obuSizeLocation = 0
#if VALIDATE_OBU
  payloadSize = -1
#endif // VALIDATE_OBU

  // For the decoder model - track the full OBU size, including headers and
  // payload size bytes.
  codedBitsStart = current_bitposition()

  obuSizeBytes u(0)
  if (use_annexb) {
    obuSizeLocation = current_bitposition()
    obu_size = 0
    obu_size u(8*obuSizeBytes)
  }
  uint64 obuStart
  obuStart = current_bitposition()

  obu_forbidden_bit u(1)
  CHECK(obu_forbidden_bit == 0, "OBU forbidden bit should not be set")

  obu_type u(4)
#if VALIDATE_OBU
  validate(560)
  validate(obu_type)
#endif // VALIDATE_OBU

  enc_obu_extension_flag u(0)
  obu_extension_flag     u(1)
  /*if (obu_extension_flag) {
    ASSERT(obu_can_use_extension_header[obu_type], "Used an obu_extension flag when we oughtn't")
  }*/

  if (use_annexb) {
    obu_has_size_field_cfg   u(0)
  } else {
    obu_has_size_field_cfg = 1
  }
  obu_has_payload_size_field u(1)
  reserved u(1) CHECK(reserved == 0, "Reserved bits in OBU header should not be set")

  if (enc_use_scalability) {
    temporal_layer = sched_temporal_layer[frame_number]
    spatial_layer = sched_spatial_layer[frame_number]
  } else {
    temporal_layer = 0
    spatial_layer = 0
  }
  if (obu_extension_flag) {
    temporal_layer u(3)
    spatial_layer u(2)
    // Check temporal ids are the same if spatial id is the same
    if ( TemporalIds[ spatial_layer ] != -1 ) {
      CHECK( TemporalIds[ spatial_layer ] == temporal_layer, "All OBU extension headers that are contained in the same temporal unit and have the same spatial_id value must have the same temporal_id value." )
    } else {
      TemporalIds[ spatial_layer ] = temporal_layer
    }
    // Check that if temporal_ids differ, then temporal_group_description_present_flag is always 0 in this CVS
    temporalIdsDiffer = 0
    for (i=0; i<=MAX_SPATIAL_ID; i++) {
      for (j=i+1; j<=MAX_SPATIAL_ID; j++) {
        if ( TemporalIds[ i ] != -1 &&
             TemporalIds[ j ] != -1 &&
             TemporalIds[ i ] != TemporalIds[ j ] ) {
          temporalIdsDiffer = 1
        }
      }
    }
    if ( temporalIdsDiffer ) {
      CHECK( CVSTemporalGroupDescriptionPresentFlag == 0, "When any temporal unit in a coded video sequence contains OBU extension headers that have temporal_id values that are not equal to each other, temporal_group_description_present_flag must be equal to 0 (obu_extension_header())." )
    }
    reserved u(3) CHECK(reserved == 0, "Reserved bits in OBU header should not be set")
  } else {
    /* Don't reset these here, as our encoder requires them
    temporal_layer = 0
    spatial_layer = 0*/
  }

  payloadSizeLocation = current_bitposition()
  payloadSizeBytes u(0)
  if (obu_has_payload_size_field) {
    payload_size = 0
    payload_size u(8*payloadSizeBytes)
  }

  uint64 payloadStart
  payloadStart = current_bitposition()
  if (obu_type == OBU_SEQUENCE_HEADER) {
    // In order to allow sending redundant sequence headers, we need to
    // know when it is valid to generate a new sequence header, and where the
    // latest sequence header was generated.
    //
    // Note: Currently, generating new sequence headers doesn't quite interact correctly
    // with scalability. Until this is resolved, we never refresh the sequence header -
    // we just generate it once at the start of the stream, then copy it for later frames.
    if (frame_number == 0) {
      uint64 sequenceHeaderStart
      sequenceHeaderStart = current_position()
      read_sequence_header() // In av1_uncompressed_header.c
      obu_trailing_bits()
      sequenceHeaderLen = current_position() - sequenceHeaderStart
      // Copy to a side buffer, which is preserved between frames
      ASSERT(sequenceHeaderLen < ENC_MAX_SEQ_HDR_LEN, "Need to increase ENC_MAX_SEQ_HDR_LEN")
      unused = copy_to_buffer(sequenceHeaderData, sequenceHeaderStart, sequenceHeaderLen)
      sequenceHeaderGenerated = 1
    } else {
      ASSERT(sequenceHeaderGenerated == 1, "No earlier sequence header to copy")
      unused = copy_from_buffer(sequenceHeaderData, sequenceHeaderLen)
    }
    // At this point, we have generated the trailing 1 bit + byte alignment, but not
    // any padding bytes. The number of padding bytes is the only thing allowed to vary
    // between an original sequence header and its repeats, so we want to generate
    // those separately:
    enc_post_obu_padding()
  } else if (obu_type == OBU_TD) {
    // This OBU contains no data
    :pass
  } else if (obu_type == OBU_FRAME_HEADER || obu_type == OBU_REDUNDANT_FRAME_HEADER) {
    if (!large_scale_tile ||
        (large_scale_tile && !camera_frame_header_ready)) {
      // Copy the pre-generated frame header
      CHECK(frameHeaderCopies < ENC_MAX_FRAME_HEADER_COPIES, "Generated too many redundant frame headers")
      frameHeaderCopyStart[frameHeaderCopies] = current_bitposition()
      frameHeaderCopies += 1

      unused = copy_from_nest(1, frameHeaderStartBytes, frameHeaderBytes)
      unused = unread_bits(frameHeaderRewindBits)
      if (large_scale_tile) {
        camera_frame_header_ready = 1
      }
    }
  } else if (obu_type == OBU_TILE_GROUP) {
    read_tile_group_obu(0, 0)
  } else if (obu_type == OBU_TILE_LIST) {
    ASSERT(large_scale_tile && camera_frame_header_ready, "Should be in large_scale_tile mode and already had a frame header")
    read_and_decode_one_tile_list(0)
  } else if (obu_type == OBU_METADATA) {
    read_metadata_obu(-1)
  } else if (obu_type == OBU_FRAME) {
    // Pack the frame header + the (single) tile group together
    ASSERT(!show_existing_frame, "OBU_FRAME requires show_existing_frame == 0")
    ASSERT(num_tile_groups == 1, "OBU_FRAME requires there to be exactly one tile group")
    CHECK(frameHeaderCopies == 0, "frameHeaderCopies should be 0 here")
    frameHeaderCopyStart[frameHeaderCopies] = current_bitposition()
    frameHeaderCopies += 1
    unused = copy_from_nest(1, frameHeaderStartBytes, frameHeaderBytes)
    read_tile_group_obu(0, 1)
  } else if (obu_type == OBU_PADDING) {
    enc_padding_size u(0)
    read_padding_obu(enc_padding_size)
  } else {
    ASSERT(0, "Unrecognized OBU type")
  }

  // Add trailing bits and padding.
  //
  // * Zero-length payloads do not require trailing bits.
  //
  // * The trailing bits and padding for OBU_TILE_GROUP and OBU_FRAME OBUs
  //   are generated in exit_bool(), so we don't need to add them here.
  //
  // * For metadata OBUs, how we handle trailing bits depends on the metadata type,
  //   so we handle that in the relevant function.
  //
  // * For sequence header OBUs, we've already generated the trailing bits
  if (current_bitposition() > payloadStart &&
      obu_type != OBU_METADATA &&
      obu_type != OBU_TILE_GROUP &&
      obu_type != OBU_FRAME &&
      obu_type != OBU_SEQUENCE_HEADER &&
      obu_type != OBU_TILE_LIST) {
    enc_obu_trailer()
  }

  uint64 payloadEnd
  payloadEnd = current_bitposition()

  // Write in the correct OBU size
  payloadSize = (payloadEnd - payloadStart) / 8
  if (obu_has_payload_size_field) {
    // Note: This '4' (# bytes for size) needs to be kept synchronized
    // with the 'obu_size le(32)' (# bits) above
    overwrite_uleb128(payloadSize, payloadSizeLocation, payloadSizeBytes)
  }
  if (use_annexb) {
    // Write in the correct OBU size
    obuSize = (payloadEnd - obuStart) / 8
    // Note: This '4' (# bytes for size) needs to be kept synchronized
    // with the 'obu_size le(32)' (# bits) above
    overwrite_uleb128(obuSize, obuSizeLocation, obuSizeBytes)
  }

#if VALIDATE_OBU
  validate(559)
  validate(payloadSize)
#endif // VALIDATE_OBU

  // Update decoder model state
  codedBits = (current_bitposition() - codedBitsStart)
  for (i = 0; i < enc_num_operating_points; i++) {
    if (is_obu_in_operating_point(obu_type, i)) {
      dfgIdx = current_dfg[i]
      dfg_bits[i][dfgIdx] += codedBits
    }
  }
}
#endif // ENCODE

#if DECODE
reset_decode_on_frame_globals() {
  current_tile_group = 0
  frame_header_bits = 0
  if (!large_scale_tile) {
    camera_frame_header_ready = 0
  }
}

decode_one_frame() {
  obuSize = 0 // silence warning

  frameDecodingFinished = 0
  frameHeaderReceived = 0

  reset_decode_on_frame_globals()

  uint64 frameSize
  frameSize = 0
  if (use_annexb) {
    frameSize = decode_uleb128()
#if COLLECT_STATS
    total_bits += uleb_length * 8
#endif
  }

  uint64 frameStart
  frameStart = current_position()
  while (!frameDecodingFinished) {
    if ((use_annexb && current_position() == frameStart + frameSize) ||
        (!use_annexb && current_position() == input_length)) {
      ASSERT(frameHeaderReceived == 0, "EOF in the middle of decoding a frame")
      // To get here, we must have skipped the last frame in the stream
      // (eg, due to it not being included in the current operating point).
      // Assume that things are fine, and return
      skipped_final_frame = 1
      break
    }

    // For the decoder model and stats collection:
    // track the full OBU size, including headers and payload size bytes
    //
    // Note: In annex-b mode, we include the OBU size field in our bit count,
    // but not the frame or temporal unit size fields. This is because the frame
    // and TU sizes can be changed by middleboxes adding or removing un-processed OBUs.
    //
    // On the other hand, for the OBU size field, we definitely include the payload
    // size field in non-annexb mode, so it seems consistent to include the OBU size
    // field in annexb mode.
    codedBitsStart = current_bitposition()

    uint64 obuStart
    if (use_annexb) {
      obuSize = decode_uleb128()
      obuStart = current_bitposition()
    }
    // OBU header:
    reserved u(1) CHECK(reserved == 0, "Reserved bits in OBU header should not be set")
    obu_type u(4)
    if (obu_type == OBU_TD) {
      SeenSeqHeaderBeforeFirstFrameHeader = 0
      for (i=0; i<=MAX_SPATIAL_ID; i++) {
        TemporalIds[ i ] = -1
      }
      LumaSamplesInTU = 0
      FramesDecodedInTU = 0
      FramesDisplayedInTU = 0
      DisplayedLumaSamplesInTU = 0
    }
#if VALIDATE_OBU
    validate(560)
    validate(obu_type)
#endif // VALIDATE_OBU
    obu_extension_flag u(1)
    /*if (obu_extension_flag) {
      ASSERT(obu_can_use_extension_header[obu_type], "Used an obu_extension flag when we oughtn't")
      }*/
    obu_has_payload_size_field u(1)
    reserved u(1) CHECK(reserved == 0, "Reserved bits in OBU header should not be set")
    if (obu_extension_flag) {
      // Note: These flags are used iff the scalability experiment is enabled
      temporal_layer u(3)
      spatial_layer u(2)
      // Check temporal ids are the same if spatial id is the same
      if ( TemporalIds[ spatial_layer ] != -1 ) {
        CHECK( TemporalIds[ spatial_layer ] == temporal_layer, "All OBU extension headers that are contained in the same temporal unit and have the same spatial_id value must have the same temporal_id value." )
      } else {
        TemporalIds[ spatial_layer ] = temporal_layer
      }
      // Check that if temporal_ids differ, then temporal_group_description_present_flag is always 0 in this CVS
      temporalIdsDiffer = 0
      for (i=0; i<=MAX_SPATIAL_ID; i++) {
        for (j=i+1; j<=MAX_SPATIAL_ID; j++) {
          if ( TemporalIds[ i ] != -1 &&
               TemporalIds[ j ] != -1 &&
               TemporalIds[ i ] != TemporalIds[ j ] ) {
            temporalIdsDiffer = 1
          }
        }
      }
      if ( temporalIdsDiffer ) {
        CHECK( CVSTemporalGroupDescriptionPresentFlag == 0, "When any temporal unit in a coded video sequence contains OBU extension headers that have temporal_id values that are not equal to each other, temporal_group_description_present_flag must be equal to 0 (obu_extension_header())." )
      }
      reserved u(3) CHECK(reserved == 0, "Reserved bits in OBU header should not be set")
    } else {
      temporal_layer = 0
      spatial_layer = 0
    }

    if (obu_has_payload_size_field) {
      payloadSize = decode_uleb128()
    } else {
      obuHeaderSize = 1 + obu_extension_flag
      payloadSize = obuSize - obuHeaderSize
    }

    decodePayload = is_obu_in_operating_point(obu_type, current_operating_point_id)

    // seek quickly if calculating the max num oppoints
    if (max_num_oppoints >= 0 &&
        obu_type != OBU_SEQUENCE_HEADER) {
       decodePayload = 0
    }

    if (decodePayload) {
      uint64 payloadStart
      payloadStart = current_bitposition()

      uint1 payload_is_nonempty
      payload_is_nonempty = 0

      if (obu_type == OBU_SEQUENCE_HEADER) {
        int64 sequenceHeaderStartPosition
        int64 sequenceHeaderCurrentPosition
        int64 sequenceHeaderSize
        int64 i
        ASSERT(byte_aligned(), "Expected sequence header to be byte aligned at start")
        sequenceHeaderStartPosition = current_bitposition( )

        read_sequence_header() // In av1_uncompressed_header.c

        if (max_num_oppoints >= 0) {
           max_num_oppoints = Max(max_num_oppoints, num_operating_points)
        }

        // see whether the sequence headers have changed and store this one
        sequenceHeaderCurrentPosition = current_bitposition( )
        sequenceHeaderSize = (sequenceHeaderCurrentPosition - sequenceHeaderStartPosition + 7) / 8
        ASSERT(sequenceHeaderSize <= MAX_SEQUENCE_HEADER_SIZE, "Need to increase MAX_SEQUENCE_HEADER_SIZE, sequenceHeaderSize=%d", sequenceHeaderSize)

        unused = peek_last_bytes(sequenceHeaderSize, curSequenceHeader)

        if (!byte_aligned()) { // zero out additional bytes
          extraBits = 8 - ((sequenceHeaderCurrentPosition - sequenceHeaderStartPosition) % 8)
          mask = ~((1<<extraBits)-1)
          curSequenceHeader[sequenceHeaderSize - 1] &= mask
        }

        if (prevSequenceHeaderSize == -1) {
          // treat this as unchanged, so we can start streams without a shown key frame
          sequenceHeaderChanged = 0
        } else if (prevSequenceHeaderSize != sequenceHeaderSize) {
          sequenceHeaderChanged = 1
        } else {
          sequenceHeaderChanged = 0
          for (i=0; i<sequenceHeaderSize; i++) {
            if (prevSequenceHeader[i] != curSequenceHeader[i]) {
              sequenceHeaderChanged = 1
              break
            }
          }
        }
        prevSequenceHeaderSize = sequenceHeaderSize
        for (i=0; i<sequenceHeaderSize; i++) {
          prevSequenceHeader[i] = curSequenceHeader[i]
        }
      } else if (obu_type == OBU_TD) {
        // This OBU contains no data
        :pass
      } else if (obu_type == OBU_FRAME_HEADER) {
        // Conformance requirement: The first frame header for each frame must
        // be an OBU_FRAME_HEADER, any repeats must be OBU_REDUNDANT_FRAME_HEADER.
        CHECK(frameHeaderReceived == 0, "Received OBU_FRAME_HEADER as a repeat frame header")
        read_frame_header_obu(frameHeaderReceived)
        frameHeaderReceived = 1
        frameDecodingFinished = show_existing_frame || camera_frame_header_ready
      } else if (obu_type == OBU_TILE_GROUP) {
        frameDecodingFinished = read_tile_group_obu(payloadSize, 0)
      } else if (obu_type == OBU_METADATA) {
        read_metadata_obu(payloadSize)
      } else if (obu_type == OBU_FRAME) {
        read_frame_header_obu(frameHeaderReceived)
        frameHeaderReceived = 1
        CHECK(!show_existing_frame, "OBU_FRAME requires show_existing_frame == 0")
        align_to_byte()
        uint64 pos
        pos = current_bitposition()
        remainingSize = payloadSize - (pos - payloadStart) / 8
        frameDecodingFinished = read_tile_group_obu(remainingSize, 1)
      } else if (obu_type == OBU_REDUNDANT_FRAME_HEADER) {
        // Conformance requirement: The first frame header for each frame must
        // be an OBU_FRAME_HEADER, any repeats must be OBU_REDUNDANT_FRAME_HEADER.
        CHECK(frameHeaderReceived == 1, "Received OBU_REDUNDANT_FRAME_HEADER as first frame header")
        // TODO (extra conformance requirement): Check that the contents of any repeated
        // frame headers are the same as when we started the frame
        read_frame_header_obu(frameHeaderReceived)
        frameHeaderReceived = 1
        frameDecodingFinished = show_existing_frame
      } else if (obu_type == OBU_PADDING) {
        payload_is_nonempty = read_padding_obu (payloadSize)
      } else if (obu_type == OBU_TILE_LIST) {
        ASSERT(camera_frame_header_ready, "Camera frame header must be read before OBU_TILE_LIST")
        read_and_decode_one_tile_list(payloadSize)
        frameDecodingFinished = 1
      } else {
        ASSERT(0, "Unrecognized OBU type")
      }

      // Read trailing bits.
      //
      // See the long note in write_obu for the logic that governs
      // when we call obu_trailing_bits here.
      //
      // Note that we don't call out to enc_obu_trailer() when
      // generating a sequence header, because then we call
      // obu_trailing_bits() immediately after read_sequence_header()
      // when encoding. When decoding, we don't do that, so we need to
      // call obu_trailing_bits() here.
      if ((payload_is_nonempty || current_bitposition() > payloadStart) &&
          obu_type != OBU_METADATA &&
          obu_type != OBU_TILE_GROUP &&
          obu_type != OBU_FRAME &&
          obu_type != OBU_TILE_LIST) {
        obu_trailing_bits()
      }

      uint64 payloadEnd
      payloadEnd = current_bitposition()
      decodedPayloadSize = (payloadEnd - payloadStart) / 8
      ASSERT(decodedPayloadSize <= payloadSize, "OBU payload size mismatch")
      while (decodedPayloadSize < payloadSize) {
        padding u(8) CHECK(padding == 0, "Padding at end of OBU should be 0")
        decodedPayloadSize += 1
      }
#if VALIDATE_OBU
      validate(559)
      validate(payloadSize)
#endif // VALIDATE_OBU
    } else { // !decodePayload
      // need to see whether this is a new CVS
      if (obu_type == OBU_FRAME ||
          obu_type == OBU_FRAME_HEADER) {
        uint64 start
        uint64 bitpos
        start = current_position()

        // read just enough of the uncompressed header
        if (reduced_still_picture_hdr) {
          show_existing_frame = 0
        } else {
          show_existing_frame u(1)
        }
        if (show_existing_frame) {
          show_frame = 0
        } else if (reduced_still_picture_hdr) {
          frame_type = KEY_FRAME
          show_frame = 1
        } else {
          frame_type u(2)
          show_frame u(1)
        }

        // check the CVS is valid
        check_new_CVS()

        // skip the rest
        bitpos = current_bitposition()
        if (bitpos % 8) {
          padBits = 8 - (bitpos % 8)
          padding u(padBits)
        }
        frameHeaderSize = current_position() - start
        payloadSize -= frameHeaderSize
      }
      // need to also parse the metadata in the dropped frames to handle scalability structure changes associated with specific layers
      if (obu_type == OBU_METADATA) {
        CHECK(byte_aligned(), "Should be byte aligned at start of OBU")
        uint64 metadataStart
        metadataStart = current_position()
        metadata_type = decode_uleb128()

        if ( metadata_type == METADATA_TYPE_SCALABILITY ) {
          read_scalability( )
        }
        metadataSize = current_position() - metadataStart
        payloadSize -= metadataSize
      }

      // If skipping an OBU, we need to advance the bitstream position
      // to the end of the OBU's payload
      for (i = 0; i < payloadSize; i++)
        padding u(8)
    }

    // Update decoder model state
    codedBits = (current_bitposition() - codedBitsStart)
    if (decodePayload && SeenSeqHeader) {
      dfgIdx = current_dfg[current_operating_point_id]
      dfg_bits[current_operating_point_id][dfgIdx] += codedBits
#if COLLECT_STATS
      total_bits += codedBits
#endif
    }
  }
  uint64 frameEnd
  frameEnd = current_position()
  if (use_annexb) {
    ASSERT(frameEnd - frameStart == frameSize, "Frame size mismatch")
  }

  if (frameHeaderReceived) {
    CSV(CSV_TILEGROUPS, current_tile_group)

    update_decoder_model(current_operating_point_id)
    if (show_frame || show_existing_frame) {
      current_show_frame_idx[current_operating_point_id] += 1
    }
#if DECODE && COLLECT_STATS
    frame_bits = (frameEnd - frameStart) * 8

    frameTypeIdx = show_existing_frame ? 4 : frame_type
    total_frames[frameTypeIdx] += 1
    total_frame_bits[frameTypeIdx] += frame_bits
    if (!show_existing_frame) {
      total_decoded_px += crop_width * crop_height
      total_upscaled_px += upscaled_width * upscaled_height
    }
    if (show_frame || show_existing_frame) {
      total_displayed_px += upscaled_width * upscaled_height
    }
    max_frame_bits = Max (max_frame_bits, frame_bits)
#endif // DECODE && COLLECT_STATS
  }
}

// Not used by encoder
read_frame_header_obu(frameHeaderReceived) {
  if (! frameHeaderReceived
      || (large_scale_tile && !camera_frame_header_ready)
      ) {
    // Read the frame header and remember its size
    uint64 start
    start = current_bitposition()
    decode_frame_headers()
    frame_header_bits = current_bitposition() - start
    // If we are just displaying a previously decoded frame, then we
    // don't need to decode any further OBUs.
    if (show_existing_frame) {
      frameDecodingFinished = 1
    }
    if (large_scale_tile) {
      camera_frame_header_ready = 1
    }
    if (show_existing_frame == 0) {
      LumaSamplesInTU += upscaled_width * upscaled_height
      FramesDecodedInTU++
    }
  } else {
    // TODO: Check that the repeated frame header contains the same
    // data as the original frame header
    uint32 i
    for (i = 0; i < frame_header_bits; i++)
      skipped_frame_header u(1)
  }
}
#endif // DECODE

read_tile_group_obu(sz, isObuFrame) {
  // Read the range of this tile group. Note: This is different to the pre-OBU
  // read_tile_group_range() function, as here we read the start and end tiles,
  // rather than the start and length.
  uint64 tgHeaderStart
  tgHeaderStart = current_bitposition()
  uint32 numTiles
  numTiles = tile_rows * tile_cols
  NumTiles = numTiles

#if ENCODE
  tg_start = tg_starts[current_tile_group]
  tg_end = tg_start + tg_sizes[current_tile_group] - 1
  if (numTiles > 1) {
    if (isObuFrame) {
      ASSERT(tg_start == 0 && tg_end == numTiles - 1,
             "OBU_FRAME requires exactly one tile group, containing all tiles in the frame")
      tile_start_and_end_present = 0
    } else if (tg_start == 0 && tg_end == numTiles - 1) {
      enc_tile_start_and_end_present u(0)
      tile_start_and_end_present = enc_tile_start_and_end_present
    } else {
      tile_start_and_end_present = 1
    }
  }
#endif

  if ((numTiles > 1)
      && !large_scale_tile
      ) {
    tile_start_and_end_present u(1)
  } else {
    tile_start_and_end_present = 0
  }

#if DECODE
  if (isObuFrame) {
    // This flag must be 0 for OBU_FRAME
    CHECK(tile_start_and_end_present == 0, "OBU_FRAME requires tile_start_and_end_present = 0")
  }
#endif

  if (tile_start_and_end_present) {
    tg_start u(log2_tile_rows + log2_tile_cols)
    tg_end u(log2_tile_rows + log2_tile_cols)
  } else {
    tg_start = 0
    tg_end = numTiles - 1
  }

  align_to_byte()
  uint64 tgHeaderEnd
  tgHeaderEnd = current_bitposition()
  tgHeaderBytes = (tgHeaderEnd - tgHeaderStart) / 8
  ASSERT(tg_start <= tg_end, "Tile group should have start <= end")
  ASSERT(tg_end < numTiles, "Tile group extends past last tile in frame")
  frameDecodingFinished = (tg_end == numTiles - 1)


#if ENCODE
  pack_tile_group()
#else
  decode_tiles_and_wrapup(sz - tgHeaderBytes, -1, -1)
  current_tile_group += 1
#endif // ENCODE

  return frameDecodingFinished
}

read_and_decode_one_tile_list(sz) {
  uint32 numTiles
  numTiles = tile_rows * tile_cols

  // represent the frame as a set of sparse tiles
  output_frame_width_in_tiles_minus_1 u(8)
  output_frame_width_in_tiles = output_frame_width_in_tiles_minus_1 + 1
  output_frame_height_in_tiles_minus_1 u(8)
  output_frame_height_in_tiles = output_frame_height_in_tiles_minus_1 + 1
  tile_count_minus_1 u(16)
  tile_count = tile_count_minus_1 + 1
  ASSERT(tile_count <= 512, "tile_count_minus_1 is too large: we must have tile_count_minus_1 <= 511. Here, tile_count_minus_1 = %d.", tile_count_minus_1)
  ASSERT(output_frame_width_in_tiles * output_frame_height_in_tiles >= tile_count, "Tile Count and Frame Size mismatch: we require (output_frame_width_in_tiles_minus_1 + 1) * (output_frame_height_in_tiles_minus_1 + 1) >= tile_count_minus_1 + 1. Here: output_frame_width_in_tiles_minus_1 = %d, output_frame_height_in_tiles_minus_1 = %d, tile_count_minus_1 = %d.", output_frame_width_in_tiles_minus_1, output_frame_height_in_tiles_minus_1, tile_count_minus_1)
  ASSERT(output_frame_width_in_tiles * tile_width <= mi_cols, "Output width mismatch: (output_frame_width_in_tiles + 1) * (tile width in MI units) >= MiCols")
  ASSERT(output_frame_height_in_tiles * tile_height <= mi_rows, "Output height mismatch: (output_frame_height_in_tiles + 1) * (tile height in MI units) >= MiRows")

  tg_start = 0
  tg_end = numTiles - 1

#if DECODE
  sz -= 4
#endif // DECODE

  // set the frame output to something - this is needed for later phases which operate on the entire frame (copying to and from the cdef buffers, for example)
  for(plane=0;plane<get_num_planes();plane++) {
    w = (upscaled_width+plane_subsampling_x[plane])>>plane_subsampling_x[plane]
    h = (upscaled_height+plane_subsampling_y[plane])>>plane_subsampling_y[plane]
    if (!plane) {
      tilesW = tile_width * output_frame_width_in_tiles * MI_SIZE
      tilesH = tile_height * output_frame_height_in_tiles * MI_SIZE
      ASSERT(w >= tilesW, "Undersize width")
      ASSERT(h >= tilesH, "Undersize height")
    }
    for(y=0;y<h;y++) {
      for(x=0;x<w;x++) {
        frame[plane][x][y] = 128
        anchor_framestore[-1][plane][x][y] = 128
      }
    }
  }

  for (i = 0; i < tile_count; i++) {
#if ENCODE
    enc_decTileRow u(0)
    enc_decTileCol u(0)
#endif // ENCODE

    anchorFrameIdx u(8)
    ASSERT(anchorFrameIdx < num_large_scale_tile_anchor_frames, "anchor_frame_idx must be less than the number of large scale tile anchor frames")
    load_anchor_frame(anchorFrameIdx)
    :log_anchor_ref(b)
    :C log_anchor_ref(b)

    decTileRow u(8)
    decTileCol u(8)

 #if ENCODE
    size = tile_sizes[decTileCol][decTileRow]
    tileLocation = tile_locations[decTileCol][decTileRow]
    codedSize = size - 1
    ASSERT(codedSize <= ((1<<16)-1), "codedSize is too big")
    codedSize u(16)
    // Copy the tile data from nest level 1
    unused = copy_from_nest(1, tileLocation, size)
#else // ENCODE
    codedSize u(16)
    codedSize +=1
    sz -= 5
    ASSERT(codedSize <= sz, "Size incorrectly encoded")
    decode_tiles_and_wrapup(codedSize, decTileRow, decTileCol)
    sz -= codedSize
#endif // ENCODE

    // copy the tile to the anchor output (tiles packed in raster order)
    :log_tile_output(b, b.global_data.decTileRow, b.global_data.decTileCol)
    :C log_tile_output(b, b->global_data->decTileRow, b->global_data->decTileCol)
    tileX = i % output_frame_width_in_tiles
    tileY = i / output_frame_width_in_tiles
    for(plane=0;plane<get_num_planes();plane++) {
      w = ((tile_width * MI_SIZE)+plane_subsampling_x[plane])>>plane_subsampling_x[plane]
      h = ((tile_height * MI_SIZE)+plane_subsampling_y[plane])>>plane_subsampling_y[plane]
      srcX = decTileCol * w
      srcY = decTileRow * h
      dstX = tileX * w
      dstY = tileY * h
      for(y=0;y<h;y++) {
        for(x=0;x<w;x++) {
          anchor_framestore[-1][plane][dstX + x][dstY + y] = frame[plane][srcX + x][srcY + y]
        }
      }
    }
  }

  // copy the anchor output back to frame output for the output stage
  for(plane=0;plane<get_num_planes();plane++) {
    w = (upscaled_width+plane_subsampling_x[plane])>>plane_subsampling_x[plane]
    h = (upscaled_height+plane_subsampling_y[plane])>>plane_subsampling_y[plane]
    for(y=0;y<h;y++) {
      for(x=0;x<w;x++) {
        frame[plane][x][y] = anchor_framestore[-1][plane][x][y]
      }
    }
  }
}

int calculate_anchor_frames(numLargeScaleTileTileListObus, lfWidth, lfHeight, lfBlockSize) {
  if (numLargeScaleTileTileListObus == 0) {
    return 0
  }
  ASSERT(lfWidth > 0 && lfHeight > 0 && lfBlockSize > 0, "Invalid light field paramaters")
  uBlocks = (lfWidth + lfBlockSize - 1) / lfBlockSize
  vBlocks = (lfHeight + lfBlockSize - 1) / lfBlockSize
  numAnchorFrames = uBlocks * vBlocks
  return numAnchorFrames
}

read_padding_obu(sz) {
  if (sz == 0) {
      return 0
  }

#if ENCODE
  // When encoding, we can write out any old rubbish.
  for (i = 0; i < sz; i++) {
    obu_padding_byte u(8)
  }
#else
  // When decoding, we don't know how much of sz is actual padding
  // data and how much should be treated as trailing bits. Read all
  // the bytes going (there is at least one, because we checked for
  // zero sz above) and then rewind to just before the last nonzero
  // byte.
  int64 pos
  int max_i

  pos = current_position ()
  max_i = -1

  for (i = 0; i < sz; i++) {
    obu_ignored_byte                         u(8)
    if (obu_ignored_byte) {
      max_i = i
    }
  }

  CHECK (max_i >= 0,
         "nonempty padding_obu should have some nonzero bytes")

  // Rewind to just before the last nonzero byte, to be read by
  // trailing_bits.
  set_pos (pos + max_i)
  return 1
#endif
}

read_scalability() {
#if DECODE
    int64 scalabilityStartPosition
    int64 scalabilityCurrentPosition
    int64 scalabilitySize
    int64 i
    scalabilityStartPosition = current_bitposition( )
#endif // DECODE

  // Note: In the encoder, the information here should be pre-selected by
  // enc_pick_scalability(), and should not be generated in-line (either by
  // code here or in av1_profile.c). This is because we might code multiple
  // scalability metadata OBUs in a stream, but they need to all contain
  // the same information.
  scalability_mode u(8)
  CVSTemporalGroupDescriptionPresentFlag = 0
  if (scalability_mode == SCALABILITY_SS) {
    // This mode means that we explicitly describe the scalability
    // structure
    spatial_layers_count_minus1 u(2)
    spatial_layer_dimensions_present u(1)
    spatial_layer_description_present u(1)
    temporal_group_description_present u(1)
    if ( temporal_group_description_present ) {
      CVSTemporalGroupDescriptionPresentFlag = 1
    }
    reserved u(3) CHECK(reserved == 0, "Reserved bits in scalability metadata should be 0")

    totalLayers = 1 + spatial_layers_count_minus1
    if (spatial_layer_dimensions_present) {
      for (i = 0; i < totalLayers; i++) {
        spatial_layer_max_width[i] u(16)
        spatial_layer_max_height[i] u(16)
      }
    }
    if (spatial_layer_description_present) {
      for (i = 0; i < totalLayers; i++) {
        spatial_layer_ref_id[i] u(8)
      }
    }
    if (temporal_group_description_present) {
      temporal_group_size u(8)
      for (i = 0; i < temporal_group_size; i++) {
        temporal_group_temporal_id[i] u(3)
        temporal_group_temporal_switching_up_point_flag[i] u(1)
        temporal_group_spatial_switching_up_point_flag[i] u(1)
        temporal_group_ref_cnt[i] u(3)
        for (j = 0; j < temporal_group_ref_cnt[i]; j++) {
          temporal_group_ref_pic_diff[i][j] u(8)
        }
      }
    }
  } else {
    // Infer some of the SCALABILITY_SS parameters from preset information
    ASSERT(scalability_mode < SCALABILITY_NUM_PRESETS, "Unknown scalability mode")
    spatial_layers_count = preset_spatial_layers[scalability_mode]

    // Enhancement layer dimensions
    num = preset_resolution_ratio[scalability_mode][0]
    denom = preset_resolution_ratio[scalability_mode][1]
    spatial_layer_max_width[spatial_layers_count-1] = max_frame_width
    spatial_layer_max_height[spatial_layers_count-1] = max_frame_height
    if (spatial_layers_count > 1) {
      for (i = spatial_layers_count - 2; i>=0; i--) {
        if (bottom_two_spatial_layers_same_dimension[scalability_mode] == 0 ||
            i>0) {
          spatial_layer_max_width[i] = (spatial_layer_max_width[i+1] * denom + (num/2)) / num
          spatial_layer_max_height[i] = (spatial_layer_max_height[i+1] * denom + (num/2)) / num
        } else {
          spatial_layer_max_width[i] = spatial_layer_max_width[i+1]
          spatial_layer_max_height[i] = spatial_layer_max_height[i+1]
        }
      }
    }

    // Enhancement layer description
    spatial_layer_ref_id[0] = 255
    if (spatial_layers_count >= 2) {
      if (preset_inter_layer_dependency[scalability_mode]) {
        spatial_layer_ref_id[1] = 0
      } else {
        spatial_layer_ref_id[1] = 255
      }
    }

    // Temporal group structure
    temporalLayers = preset_temporal_layers[scalability_mode]
    temporal_group_size = 1 << (temporalLayers - 1)
    if (scalability_mode < SCALABILITY_L3T2_KEY) { // not valid for higher modes (but unused anyway)
      for (i = 0; i < temporal_group_size; i++) {
        // The temporal units are assigned to a repeating pattern of temporal layers:
        // temporal layer 'i+1' has exactly double the framerate of temporal layer 'i'
        // For example, with 3 temporal layers, the sequence is:
        // 0, 2, 1, 2, 0, 2, 1, 2, 0, 2, 1, 2, ...
        // This is equivalent to the following:
        temporal_group_temporal_id[i] = Max(0, temporalLayers - 1 - ctz(i))

        temporal_group_temporal_switching_up_point_flag[i] = preset_temporal_switching_up_points[temporalLayers-1][i]
        temporal_group_spatial_switching_up_point_flag[i] = 0

        temporal_group_ref_cnt[i] = 1
        temporal_group_ref_pic_diff[i][0] = preset_temporal_ref_pic_diff[temporalLayers-1][i]
      }
    }
  }

#if DECODE
    // see whether the scalability structures have changed and store this one
    ASSERT(byte_aligned(), "Expected scalability structure to be byte aligned at start")
    scalabilityCurrentPosition = current_bitposition( )
    scalabilitySize = (scalabilityCurrentPosition - scalabilityStartPosition + 7) / 8
    ASSERT(scalabilitySize <= MAX_SCALABILITY_STRUCTURE_SIZE, "Need to increase MAX_SCALABILITY_STRUCTURE_SIZE, scalabilitySize=%d", scalabilitySize)

    unused = peek_last_bytes(scalabilitySize, curScalabilityStructure)

    if (!byte_aligned()) { // zero out additional bytes
      extraBits = 8 - ((scalabilityCurrentPosition - scalabilityStartPosition) % 8)
      mask = ~((1<<extraBits)-1)
      curScalabilityStructure[scalabilitySize - 1] &= mask
    }

    if (prevScalabilityStructureSize == -1) {
      // treat this as unchanged, so we can start streams without a shown key frame
      scalabilityStructureChanged = 0
    } else if (prevScalabilityStructureSize != scalabilitySize) {
      scalabilityStructureChanged = 1
    } else {
      scalabilityStructureChanged = 0
      for (i=0; i<scalabilitySize; i++) {
        if (prevScalabilityStructure[i] != curScalabilityStructure[i]) {
          scalabilityStructureChanged = 1
          break
        }
      }
    }
    prevScalabilityStructureSize = scalabilitySize
    for (i=0; i<scalabilitySize; i++) {
      prevScalabilityStructure[i] = curScalabilityStructure[i]
    }
#endif //DECODE
}

// Note: Since AOM_RESERVED_0 metadata OBUs are processed specially, we don't
// apply trailing bits to them. But all other metadata OBUs require trailing bits,
// so we make sure to add them at the end of this function for all non-AOM_RESERVED_0
// metadata OBUs
read_metadata_obu(sz)
{
  uint64 metadata_type
#if ENCODE
  metadata_type u(0)
  metadataTypeBytes u(0)
  store_uleb128(metadata_type, metadataTypeBytes)
#else // ENCODE
  CHECK(byte_aligned(), "Should be byte aligned at start of OBU")
  uint64 metadataTypeStart
  metadataTypeStart = current_position()
  metadata_type = decode_uleb128()
  metadataTypeSize = current_position() - metadataTypeStart
  ambiguousTrailingBitLocation = 0
#endif // ENCODE

  if (metadata_type < 0) {
    CHECK(0, "Do we ever get here?")
#if DECODE
    ambiguousTrailingBitLocation = 1
#else // DECODE
    :pass
#endif // DECODE
  } else if (metadata_type == METADATA_TYPE_AOM_RESERVED_0) {
    // do nothing
    :pass
  } else if (metadata_type == METADATA_TYPE_ITUT_T35) {
    itu_t_t35_country_code u(8)
    if (itu_t_t35_country_code == 0xFF) {
      itu_t_t35_country_code_extension_byte u(8)
    }
#if ENCODE
    itu_t_t35_payload_size u(0)
    for (i = 0; i < itu_t_t35_payload_size; i++) {
      metadata_itut_t35_payload_byte u(8)
    }
#else // ENCODE
    ambiguousTrailingBitLocation = 1
#endif // ENCODE
  } else if (metadata_type == METADATA_TYPE_HDR_CLL) {
    // This contains "HDR content light level" data
    max_cll u(16)
    max_fall u(16)
  } else if (metadata_type == METADATA_TYPE_HDR_MDCV) {
    // This contains "HDR mastering display colour volume" data
    for (i = 0; i < 3; i++) {
      primary_chromaticity_x[i] u(16)
      primary_chromaticity_y[i] u(16)
    }
    white_point_chromaticity_x u(16)
    white_point_chromaticity_y u(16)
    luminance_max u(32)
    luminance_min u(32)
  } else if (metadata_type == METADATA_TYPE_SCALABILITY) {
    read_scalability()
  } else if (metadata_type == METADATA_TYPE_TIMECODE) {
#if ENCODE
    enc_specify_timestamp_flags u(0)
    if (!enc_specify_timestamp_flags) {
      enc_pick_timestamp_flags()
    }
#endif //ENCODE
    counting_type u(5)

#if ENCODE
    if (enc_specify_timestamp_flags) {
      enc_full_timestamp_flag u(0)
    }
#endif
    full_timestamp_flag u(1)

    discontinuity_flag u(1)
    cnt_dropped_flag u(1)
    n_frames u(9)
    if (full_timestamp_flag) {
      seconds_value u(6)
      minutes_value u(6)
      hours_value u(5)
    } else {

#if ENCODE
      if (enc_specify_timestamp_flags) {
        enc_seconds_flag u(0)
      }
#endif
      seconds_flag u(1)

      if (seconds_flag) {
        seconds_value u(6)

#if ENCODE
        if (enc_specify_timestamp_flags) {
          enc_minutes_flag u(0)
        }
#endif
        minutes_flag u(1)

        if (minutes_flag) {
          minutes_value u(6)

#if ENCODE
          if (enc_specify_timestamp_flags) {
            enc_hours_flag u(0)
          }
#endif
          hours_flag u(1)

          if (hours_flag) {
            hours_value u(5)
          }
        }
      }
    }
    time_offset_length u(5)
    if (time_offset_length > 0) {
      time_offset_value u(time_offset_length)
    }
  } else if (metadata_type == METADATA_TYPE_ARGON_DESIGN_LARGE_SCALE_TILE) {
#if ENCODE
    ASSERT(enc_anchor_frame_cnt, "Need anchor frames to be decided before we get here")
#endif // ENCODE
    num_large_scale_tile_anchor_frames u(8)
    num_large_scale_tile_tile_list_obus u(16)

    // Ensure that the decoder model is disabled. This catches the
    // case when we get the sequence header before the large scale
    // tile OBU.
    decoder_model_mode = DECODER_MODEL_DISABLED

  } else { // reserved or unregistered metadata_type
#if ENCODE
    arbitrary_payload_size u(0)
    for (i = 0; i < arbitrary_payload_size; i++) {
      metadata_arbitrary_payload_byte u(8)
    }
#else // ENCODE
    ambiguousTrailingBitLocation = 1
#endif // ENCODE
  }

#if ENCODE
  enc_obu_trailer()
#else // ENCODE
  uint64 endBytePos
  endBytePos = metadataTypeStart + sz
  if (!ambiguousTrailingBitLocation) {
    // trailing bit should be found immediately, followed by padding
    obu_trailing_bits()
    obuPaddingBytes = sz - (current_position() - metadataTypeStart)
    for (i = 0; i < obuPaddingBytes; i++) {
      padding u(8)
      CHECK(padding == 0, "Padding within OBUs should consist entirely of 0 bits")
    }
  } else {
    // check we find trailing bit within remainder
    int64 trailingBitPos
    trailingBitPos = -1
    while (current_position() < endBytePos) {
      bit u(1)
      if (bit) {
        trailingBitPos = current_bitposition()
      }
    }
    CHECK(trailingBitPos >= 0, "Ambiguous trailing bit not found within obu")
  }
  CHECK(byte_aligned(), "Should be byte aligned at end of OBU")
  CHECK(current_position() == endBytePos, "Arithmetic error finding the end")
#endif // ENCODE
}

// Below this are the functions to handle the decoder model updates.
// To allow debugging to be turned on/off, we use a special macro
// instead of the usual CHECK():
#if CHECK_DECODER_MODELS
#define DM_CHECK(x, ...) CHECK(x, __VA_ARGS__)
#else
#define DM_CHECK(x, ...)
#endif

get_free_buffer(oppoint) {
  for (i = 0; i < DECODER_MODEL_BUFFER_POOL_SIZE; i++) {
    if (DecoderRefCount[oppoint][i] == 0 &&
        PlayerRefCount[oppoint][i] == 0) {
      return i
    }
  }
  return -1
}

update_ref_buffers(oppoint, idx) {
  for (i = 0; i < DECODER_MODEL_VBI_SIZE; i++) {
    if (refresh_frame_flags & (1 << i)) {
      if (VBI[oppoint][i] != -1) {
        DecoderRefCount[oppoint][VBI[oppoint][i]]--
      }
      VBI[oppoint][i] = idx
      DecoderRefCount[oppoint][idx]++
    }
  }
}

int64 start_decode_at_removal_time(oppoint, int64 time) {
  int64 bufPresentationTime
  for ( i = 0; i < DECODER_MODEL_BUFFER_POOL_SIZE; i++ ) {
    if (PlayerRefCount[oppoint][i] > 0) {
      ASSERT(LatestDisplayIdx[oppoint][i] != -1, "Internal inconsistency")
      if (InitialPresentationDelay[oppoint] != 0) {
        bufPresentationTime = calculate_presentation_time(oppoint, LatestDisplayIdx[oppoint][i])
        if (bufPresentationTime < time) {
          PlayerRefCount[oppoint][i] = 0
          // Here the spec has some extra logic; I believe we can achieve
          // the same thing by:
          LatestDisplayIdx[oppoint][i] = -1
          break
        }
      }
    }
  }
  return time
}

frames_in_buffer_pool(oppoint) {
  framesInPool = 0
  for (i = 0; i < DECODER_MODEL_BUFFER_POOL_SIZE; i++) {
    if (DecoderRefCount[oppoint][i] != 0 || PlayerRefCount[oppoint][i] != 0) {
      framesInPool++
    }
  }
  return framesInPool
}

// Used in resource availability mode to decide when we can decode the next frame
int64 time_next_buffer_is_free(oppoint, int64 time) {
  int64 bufFreeTime
  if ( current_dfg[oppoint] == 0 ) {
    // Note: In this code, we measure 'time' and 'decoder_buffer_delay' in the
    // same units (1/90000 of a second), whereas in the spec 'time' appears
    // to be measured in seconds. So we don't have a division here.
    time = decoder_buffer_delay[oppoint]
  }
  foundBuffer = 0
  bufFreeTime = time
  for ( k = 0; k < DECODER_MODEL_BUFFER_POOL_SIZE; k++ ) {
    if ( DecoderRefCount[oppoint][k] == 0 ) {
      if ( PlayerRefCount[oppoint][k] == 0 ) {
        return time
      }

      ASSERT(LatestDisplayIdx[oppoint][k] != -1, "Internal inconsistency")

      if (InitialPresentationDelay[oppoint] != 0) {
        bufPresentationTime = calculate_presentation_time(oppoint, LatestDisplayIdx[oppoint][k])
        if (!foundBuffer || bufPresentationTime) {
          bufFreeTime = bufPresentationTime
          foundBuffer = 1
        }
      }
    }
  }
  DM_CHECK(foundBuffer, "[DECODER_MODEL] No remaining frame buffers available!")
  return bufFreeTime
}

// How long does it take to receive the bits for this DFG?
time_to_receive_dfg(oppoint) {
  int64 bits
  int64 bitrate

  bitrate = MaxBitrate[oppoint]
  dfgIdx = current_dfg[oppoint]
  bits = dfg_bits[oppoint][dfgIdx]

  ASSERT(bitrate > 0, "Invalid combination of level and tier")
  // The spec says that the time is (CodedBits[i] / BitRate)
  // seconds, where the division is an exact real-number division.
  // Here, we: i) measure times in units of 1/90000 of a second,
  // and ii) round up
  return (bits * 90000 + (bitrate-1)) / bitrate
}

time_to_decode_frame(oppoint) {
  int64 lumaSamples
  int64 MaxDecodeRate
  ASSERT(!show_existing_frame, "show_existing_frame should be false here")
  if (frame_type == KEY_FRAME || frame_type == INTRA_ONLY_FRAME) {
    lumaSamples = upscaled_width * upscaled_height
  } else {
    if (spatial_layer_dimensions_present) {
      // Note: Even though some of the scalability presets allow us
      // to infer spatial_layer_max_width[] and spatial_layer_max_height[],
      // we only go down this path if they were *explicitly* signalled in the
      // bistream using a SCALABILITY_SS mode.
      lumaSamples = (spatial_layer_max_width[spatial_layer]) *
                    (spatial_layer_max_height[spatial_layer])
    } else {
      lumaSamples = max_frame_width * max_frame_height
    }
  }

  compactedLevel = compact_level[operating_point_level[oppoint]]
  MaxDecodeRate = level_max_luma_rate[compactedLevel][1]
  maxHeaderRate = level_max_headers_per_sec[compactedLevel]
  ASSERT(MaxDecodeRate > 0, "Unexpected value of level at this point")
  // The spec says that the decode time is (lumaSamples / MaxDecodeRate)
  // seconds, where the division is an exact real-number division.
  // Here, we: i) measure times in units of 1/90000 of a second,
  // and ii) round up
  decodeTimeSamples = (lumaSamples * 90000 + (MaxDecodeRate-1)) / MaxDecodeRate
  // An extra constraint added because it seems to be necessary for consistency:
  // We check later on that, for each frame, ScheduledRemovalTime - PrevFrameRemovalTime
  // is at least (1 second) / maxHeaderRate.
  // But, if we're in resource availability mode and a buffer is available immediately,
  // then the actual difference is exactly what this function returned for the previous
  // frame.
  // So, I believe that for consistency we have to return at least this value for
  // any frame, no matter the size:
  decodeTimeHeader = 90000 / maxHeaderRate
  return Max(decodeTimeSamples, decodeTimeHeader)
}

min_time_to_display_frame(oppoint) {
  int64 lumaPels
  int64 MaxDisplayRate

  // Note: The spec doesn't actually define the variable "LumaPels"
  // which is used here. I'm going to assume it means the number of pixels
  // in the displayed frame, though it could be argued that we want to use
  // the display_width and display_height values instead.
  if (show_existing_frame) {
    frameW = ref_width[index_of_existing_frame]
    frameH = ref_height[index_of_existing_frame]
  } else {
    frameW = upscaled_width
    frameH = upscaled_height
  }
  lumaPels = frameW * frameH

  compactedLevel = compact_level[operating_point_level[oppoint]]
  MaxDisplayRate = level_max_luma_rate[compactedLevel][0]
  MaxDecodeRate = level_max_luma_rate[compactedLevel][1]
  ASSERT(MaxDisplayRate > 0, "Unexpected value of level at this point")

  maxHeaderRate = level_max_headers_per_sec[compactedLevel]

  MinFrameTime = (lumaPels * 90000 + (MaxDisplayRate-1)) / MaxDisplayRate
  denom = maxHeaderRate * MaxDisplayRate
  MinFrameTime = Max(MinFrameTime, (MaxDecodeRate * 90000 + (denom-1)) / denom)

  return MinFrameTime
}

int64 calculate_presentation_time(oppoint, j) {
  ASSERT(InitialPresentationDelay[oppoint] != 0, "calculate_presentation_time should not be called before we know InitialPresentationDelay")
  int64 delta
  if (equal_picture_interval) {
    delta = j * num_ticks_per_picture
  } else {
    delta = frame_presentation_time[oppoint][j] - frame_presentation_time[oppoint][0]
  }
  return InitialPresentationDelay[oppoint] + (delta * DispCT)
}

#if ENCODE
enc_set_buffer_removal_delay(oppoint, int64 delay) {
  dfgIdx = current_dfg[oppoint]
  buffer_removal_delay[oppoint][dfgIdx] = delay

  // We only signal the lowest few bits; the rest is inferred by
  // unwrapping the counter. Here we calculate the value to signal,
  // and check that the decoder will reconstruct the correct value
  signalledValue = wrap_uint(delay, buffer_removal_delay_length)
  if (dfgIdx > 0) {
    prevDelay = buffer_removal_delay[oppoint][dfgIdx - 1]
    unwrapped = unwrap_counter(prevDelay, signalledValue, buffer_removal_delay_length)
    DM_CHECK(unwrapped == delay, "[DECODER_MODEL] buffer_removal_delay_length is too short")
  }

  // Update the value written in the frame header.
  offset = enc_buffer_removal_delay_offset[oppoint]
  ASSERT(offset != -1, "Didn't encode buffer_removal_delay when we should have")
  for (i = 0; i < frameHeaderCopies; i++) {
    bitPos = frameHeaderCopyStart[i] + offset
    unused = overwrite(bitPos,buffer_removal_delay_length,signalledValue)
  }
}

enc_reset_min_presentation_time() {
  enc_min_presentation_time = 0
}

enc_set_presentation_time(int64 presentationTime) {
  // We only signal the lowest few bits; the rest is inferred by
  // unwrapping the counter. Here we calculate the value to signal,
  // and check that the decoder will reconstruct the correct value
  signalledValue = wrap_uint(presentationTime, frame_presentation_delay_length)
  for (i = 0; i < enc_num_operating_points; i++) {
    if (is_frame_in_operating_point(i)) {
      dispIdx = current_show_frame_idx[i]
      frame_presentation_time[i][dispIdx] = presentationTime
      if (dispIdx > 0) {
        prevTime = frame_presentation_time[i][dispIdx - 1]
        unwrapped = unwrap_counter(prevTime, signalledValue, frame_presentation_delay_length)
        DM_CHECK(unwrapped == presentationTime, "[DECODER_MODEL] frame_presentation_delay_length is too short")
      }
    }
  }

  // Update the value written in the frame header
  offset = enc_tu_presentation_delay_offset
  ASSERT(offset != -1, "Didn't encode tu_presentation_delay when we should have")
  for (i = 0; i < frameHeaderCopies; i++) {
    bitPos = frameHeaderCopyStart[i] + offset
    unused = overwrite(bitPos,frame_presentation_delay_length,signalledValue)
  }
}
#endif

// Update the decoder model for a particular operating point
// Should be called after fully decoding each frame
update_decoder_model(oppoint) {
#if DECODE
  if (decoder_model_mode == DECODER_MODEL_DISABLED) {
    return 0
  }
#endif

  if (operating_point_level[oppoint] == 31) {
    // Decoder model does not apply for this level
    return 0
  }

  compactedLevel = compact_level[operating_point_level[oppoint]]

  dfgIdx = -1

  // First, update the decoder timings
  if (! show_existing_frame) {
    int64 bits
    dfgIdx = current_dfg[oppoint]
    bits = dfg_bits[oppoint][dfgIdx]

    int64 ScheduledRemovalTime
    int64 ScheduledRemovalAvailability
    ScheduledRemovalTime = 0

    // db_delay and eb_delay are a sign-extended copies of
    // decoder_buffer_delay[oppoint] and encoder_buffer_delay[oppoint] (useful
    // because the other time variables floating around are int64's and this
    // avoids signed/unsigned comparison warnings).
    int64 db_delay
    int64 eb_delay
    db_delay = decoder_buffer_delay[oppoint]
    eb_delay = encoder_buffer_delay[oppoint]

    // Calculate some parameters
    ScheduledRemovalAvailability = time_next_buffer_is_free(oppoint, Time[oppoint])
    ASSERT(ScheduledRemovalAvailability >= db_delay, "Encoder timing inconsistency")
    if (decoder_model_mode == DECODER_MODEL_RESOURCE_AVAILABILITY) {
      ScheduledRemovalTime = ScheduledRemovalAvailability
    } else if (decoder_model_mode == DECODER_MODEL_SCHEDULE) {
#if ENCODE
      if (dfgIdx == 0) {
        enc_set_buffer_removal_delay(oppoint, 0)
      } else {
        minDelta = ScheduledRemovalAvailability - db_delay
        minDelay = (minDelta + (DecCT-1)) / DecCT
        // TODO: Allow randomly selecting delays within a range above minDela
        enc_set_buffer_removal_delay(oppoint, minDelay)
      }
#endif
      // Note: The delta *in seconds* is buffer_removal_delay * DecCT
      // where DecCT = num_units_in_tick / time_scale
      delta = buffer_removal_delay[oppoint][dfgIdx] * DecCT
      ScheduledRemovalTime = db_delay + delta
      DM_CHECK(IMPLIES(dfgIdx == 0, delta == 0),
               "[DECODER_MODEL] Buffer removal delay for OP %d is nonzero %d at DFG zero.",
               oppoint, buffer_removal_delay[oppoint][dfgIdx])

      DM_CHECK(ScheduledRemovalTime >= ScheduledRemovalAvailability,
               "[DECODER_MODEL] Scheduled removal time too early (removal time: %d; buf free: %d)",
               ScheduledRemovalTime, ScheduledRemovalAvailability)
    }

    prevFrameEnd = BufferTime[oppoint]
    if (dfgIdx > 0) {
      int64 minDelta
      maxHeaderRate = level_max_headers_per_sec[compactedLevel]
      minDelta = convert_to_int64(Max(TimeToDecode[oppoint][dfgIdx - 1], 90000/maxHeaderRate))
      DM_CHECK(ScheduledRemovalTime - RemovalTime[oppoint][dfgIdx - 1] >= minDelta, "[DECODER_MODEL] Decode interval is too short")
    }

    // Note: I think that the following check is meant to be interpreted as:
    //
    // * If we start random access decoding here, then we nominally begin
    //   recieving the frame at time 0, and we start decoding the frame
    //   at a time exactly equal to decoder_buffer_delay[oppoint].
    //   For that to work, we must be able to recieve all of the bits for
    //   this DFG within a time of decoder_buffer_delay[oppoint].
    //
    // * Meanwhile, if we are following the normal flow of video decoding, then
    //   TimeDelta is how long we have between fully recieving the *previous*
    //   frame and starting to decode this one. Thus this is how long we have
    //   to recieve the bits of this frame in this mode.
    //
    // So the constraint decoder_buffer_delay <= TimeDelta is a sort of consistency
    // condition, ensuring that we have long enough to decode the frame in either
    // mode.
    //
    // However, we can't (easily) guarantee that this condition is always met.
    // There's an impossible-to-fix case where:
    // * We have two consecutive random access points
    // * We're in resource-availability mode, so decoder_buffer_delay takes
    //   the constant value 70000 rather than being selectable per RAP
    // * We start at the first keyframe, so FirstBitArrival[0] = 0.
    // * For the first keyframe, LastBitArrival[0] > TimeToDecode[0]
    // * Then the removal time of the second keyframe is *always* set to
    //   'decoder_buffer_delay + TimeToDecode[0]', which is less than
    //   'decoder_buffer_delay + LastBitArrival[0]'
    // * Thus this condition fails.
    // There's no way for us to work around this. So we disable the check for now.
    if (frame_type == KEY_FRAME && (show_frame || showable_frame)) {
      int64 TimeDelta
      TimeDelta = ScheduledRemovalTime - prevFrameEnd
      //DM_CHECK(decoder_buffer_delay[oppoint] <= TimeDelta, "[DECODER_MODEL] Missed timing on random-access point")
    }

    int64 LatestArrivalTime
    int64 FirstBitArrival
    int64 LastBitArrival
    LatestArrivalTime = ScheduledRemovalTime - (eb_delay + db_delay)
    FirstBitArrival = Max(prevFrameEnd, LatestArrivalTime)
    LastBitArrival = FirstBitArrival + time_to_receive_dfg(oppoint)

    // Update the decoder buffer state.
    // For each DFG whose *removal* time is between BufferTime and LastBitArrival,
    // we need to update as follows:
    // * Reduce the buffer fullness by the size of the removed DFG
    // * If the removal time is greater than FirstBitArrival, then some bits
    //   of the DFG 'dfgIdx' have appeared and need to be accounted for
    // * Check that the buffer hasn't underflowed or overflowed
    prevBitsArrived = 0
    while (1) {
      rem_idx = NextRemovalIdx[oppoint]
      // Stop when we either run out of DFGs, or reach the current time.
      if (rem_idx == dfgIdx) {
        break
      }
      t = RemovalTime[oppoint][rem_idx]
      if (t >= LastBitArrival) {
        break
      }

      int64 rem_bits
      rem_bits = dfg_bits[oppoint][rem_idx]

      DM_CHECK(BufferSize[oppoint] >= rem_bits,
               "[DECODER_MODEL] Decoder buffer underflow when removing DFG %d at time %d (frame has %d bits; buffer contains just %d)",
               rem_idx, t, bits, BufferSize[oppoint])
      BufferSize[oppoint] -= rem_bits

      if (t > FirstBitArrival) {
        // Calculate in this slightly odd way to ensure that we get the
        // bit counting exactly correct
        // Note that times are measured in 1/90000 of a second, but bitrate is
        // measured in bits per second, hence the division.
        bitsArrived = Min(bits, ((t - FirstBitArrival) * MaxBitrate[oppoint] + (90000-1))/90000)
        BufferSize[oppoint] += (bitsArrived - prevBitsArrived)
        prevBitsArrived = bitsArrived

        DM_CHECK(BufferSize[oppoint] <= MaxBufferSize[oppoint],
                 "[DECODER_MODEL] Decoder buffer overflow at t=%d (buffer size %lld; max %lld)",
                 t, BufferSize[oppoint], MaxBufferSize[oppoint])
      }

      NextRemovalIdx[oppoint] += 1
    }
    // Add in any bits which arrive after the last removed frame
    bitsArrived = bits
    BufferSize[oppoint] += (bitsArrived - prevBitsArrived)
    DM_CHECK(BufferSize[oppoint] <= MaxBufferSize[oppoint],
             "[DECODER_MODEL] Decoder buffer overflow at LastBitArrival=%d (buffer size %lld; max %lld)",
             LastBitArrival, BufferSize[oppoint], MaxBufferSize[oppoint])

    BufferTime[oppoint] = LastBitArrival

    TimeToDecode[oppoint][dfgIdx] = time_to_decode_frame(oppoint)

    if (decoder_model_mode == DECODER_MODEL_RESOURCE_AVAILABILITY) {
      RemovalTime[oppoint][dfgIdx] = ScheduledRemovalTime
    } else {
      if (low_delay_mode_flag[oppoint] && ScheduledRemovalTime < LastBitArrival) {
        tickNum = (LastBitArrival + (DecCT-1)) / DecCT
        RemovalTime[oppoint][dfgIdx] = tickNum * DecCT
      } else {
        RemovalTime[oppoint][dfgIdx] = ScheduledRemovalTime
      }
    }

    if (! low_delay_mode_flag[oppoint]) {
      DM_CHECK (ScheduledRemovalTime >= LastBitArrival,
                "[DECODER_MODEL] Smoothing buffer underflow (ScheduledRemovalTime = %lld < LastBitArrival = %lld)",
                ScheduledRemovalTime, LastBitArrival)
      :pass
    }

    // Now advance the decoding clock
    Time[oppoint] = start_decode_at_removal_time(oppoint, RemovalTime[oppoint][dfgIdx])
    // Note: At this point, the spec includes a check that Time <= PresentationTime.
    // We apply this check later so that we can maintain a cleaner split between decoder
    // and display timings.
    cfbi = get_free_buffer(oppoint)
    DM_CHECK(cfbi != -1, "[DECODER_MODEL] Can't find a free frame buffer")
    Time[oppoint] += TimeToDecode[oppoint][dfgIdx]
    update_ref_buffers(oppoint, cfbi)

    // This is the end of a DFG, so prepare for the next DFG
    current_dfg[oppoint] += 1
  } else {
    cfbi = VBI[oppoint][index_of_existing_frame]
    DM_CHECK(cfbi != -1, "[DECODER_MODEL] Displaying an invalid frame")
    if (frame_type == KEY_FRAME) {
      update_ref_buffers(oppoint, cfbi)
    }
  }

  // Time[oppoint] currently stores the time when we think we've
  // finished decoding the current DFG.

  // Next, update the display timings
  dispIdx = current_show_frame_idx[oppoint]

  if (!show_existing_frame && dfgIdx == initial_display_delay[oppoint] - 1) {
    InitialPresentationDelay[oppoint] = RemovalTime[oppoint][dfgIdx] + TimeToDecode[oppoint][dfgIdx]
  }

  if (show_existing_frame || show_frame) {
    int64 PresentationTime
    int64 base
    int64 delta

    PlayerRefCount[oppoint][cfbi] += 1
    LatestDisplayIdx[oppoint][cfbi] = dispIdx

    if (InitialPresentationDelay[oppoint] != 0) {
      if (equal_picture_interval) {
        base = InitialPresentationDelay[oppoint]
        delta = dispIdx * num_ticks_per_picture
      } else {
        base = InitialPresentationDelay[oppoint] - (frame_presentation_time[oppoint][0] * DispCT)
#if ENCODE
        earliestPresentationTime = ((Time[oppoint] - base) + (DispCT-1)) / DispCT
        enc_min_presentation_time = Max(enc_min_presentation_time, earliestPresentationTime)
        delta = earliestPresentationTime
#else
        delta = frame_presentation_time[oppoint][dispIdx]
#endif
      }
      PresentationTime = base + (delta * DispCT)
      if (!show_existing_frame) {
        DM_CHECK(Time[oppoint] - TimeToDecode[oppoint][dfgIdx] <= PresentationTime, "[DECODER_MODEL] Frame decode started too late")
        // Prevent Python encoder/decoder from erroring out if DM_CHECK is disabled:
        :pass
      }
      DM_CHECK(Time[oppoint] <= PresentationTime,
               "[DECODER_MODEL] Frame finished decode at %d after PresentationTime = %d.",
               Time[oppoint], PresentationTime)
    }

    // Even if we don't know InitialPresentationDelay, we can always work out
    // the presentation interval of the previous frame:
    if (dispIdx > 0) {
      int64 PrevFramePresentationInterval
      if (equal_picture_interval) {
        PrevFramePresentationInterval = num_ticks_per_picture * DispCT
      } else {
#if ENCODE
        minDelay = (minPresentationInterval[oppoint] + (DispCT-1)) / DispCT
        earliestPresentationTime = frame_presentation_time[oppoint][dispIdx-1] + minDelay
        enc_min_presentation_time = Max(enc_min_presentation_time, earliestPresentationTime)
        PresentationTime = earliestPresentationTime * DispCT
#else
        PresentationTime = frame_presentation_time[oppoint][dispIdx] * DispCT
#endif
        PrevFramePresentationTime = frame_presentation_time[oppoint][dispIdx-1] * DispCT
        PrevFramePresentationInterval = (PresentationTime - PrevFramePresentationTime)
      }
      // Note: This also implicitly checks another condition in the decoder model:
      // the presentation times must be increasing within each coded video sequence.
      DM_CHECK(PrevFramePresentationInterval >= minPresentationInterval[oppoint],
               "[DECODER_MODEL] Frame presentation interval too short (%ld < %ld)",
               PrevFramePresentationInterval,
               minPresentationInterval[oppoint])
    }
    // Calculate *this* frame's minimum presentation interval, for checking
    // when we get to the *next* frame.
    minPresentationInterval[oppoint] = min_time_to_display_frame(oppoint)
  }
}

