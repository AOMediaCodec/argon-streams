/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>

#if ENCODE
# include "av1-cfg.h"
#endif

#include "arrays.h"

#include <x86intrin.h>
#define ALIGN16 __attribute__((aligned (16)))

typedef uint8_t uint1;
typedef uint8_t uint2;
typedef uint8_t uint3;
typedef uint8_t uint4;
typedef uint8_t uint5;
typedef uint8_t uint6;
typedef uint8_t uint7;
typedef uint8_t uint8;
typedef uint16_t uint9;
typedef uint16_t uint10;
typedef uint16_t uint11;
typedef uint16_t uint12;
typedef uint16_t uint13;
typedef uint16_t uint14;
typedef uint16_t uint15;
typedef uint16_t uint16;
typedef uint32_t uint17;
typedef uint32_t uint19;
typedef uint32_t uint22;
typedef uint32_t uint24;
typedef uint32_t uint32;
typedef uint64_t uint33;
typedef uint64_t uint64;

typedef int8_t int1;
typedef int8_t int2;
typedef int8_t int3;
typedef int8_t int4;
typedef int8_t int5;
typedef int8_t int6;
typedef int8_t int7;
typedef int8_t int8;
typedef int16_t int9;
typedef int16_t int10;
typedef int16_t int12;
typedef int16_t int16;
typedef int32_t int20;
typedef int32_t int24;
typedef int32_t int32;
typedef int64_t int64;

typedef __m128i int16x4;  // We usse 128bit registers for all internal parallel activites at the moment
typedef __m128i int16x8;
typedef __m128i int32x4;
typedef uint3 uint_4_6;
typedef uint2 uint_0_2;
typedef uint3 uint_0_5;
typedef uint4 uint_0_10;
typedef uint4 uint_10_13;
typedef uint7 uint_0_64;
typedef uint7 uint_2_64;
typedef uint8 uint_1_255;
typedef uint8 uint_0_253;
typedef uint8 uint_0_254;
typedef uint11 uint_0_1024;
typedef uint15 uint_0_32640;
typedef uint9 uint_0_32;
typedef uint8 uint_0_31;
typedef uint15 uint_0_32766;
typedef uint15 uint_0_16450;
typedef uint15 uint_0_16384;
typedef uint19 uint_0_262144;
typedef uint8 uint_3_255;
typedef uint9 uint_0_261;
typedef uint8 uint_2_128;
typedef uint5 uint_0_26;
typedef uint9 uint_64_256;
typedef uint5 uint_0_20;
typedef uint4 uint_4_12;
typedef uint4 uint_0_10;
typedef uint4 uint_0_12;
typedef uint4 uint_0_14;
typedef uint4 uint_0_15;
typedef uint5 uint_0_16;
typedef uint5 uint_0_17;
typedef uint5 uint_0_18;
typedef uint5 uint_0_21;
typedef uint5 uint_0_24;
typedef uint5 uint_0_27;
typedef uint4 uint_1_8;
typedef uint4 uint_0_9;
typedef uint4 uint_0_8;
typedef uint4 uint_2_8;
typedef uint4 uint_8_11;
typedef uint4 uint_6_9;
typedef uint5 uint_1_16;
typedef uint6 uint_1_32;
typedef uint2 uint_0_3;
typedef uint3 uint_0_4;
typedef uint3 uint_1_6;
typedef uint3 uint_0_6;
typedef uint3 uint_0_7;
typedef uint2 uint_1_3;
typedef uint8 uint_1_128;
typedef uint5 uint_1_25;
typedef uint5 uint_2_17;

typedef int* intpointer;
typedef int3* int3pointer;
typedef int8* int8pointer;
typedef uint8* uint8pointer;
typedef uint32* uint32pointer;
typedef int32* int32pointer;
typedef int64* int64pointer;
typedef int16* int16pointer;
typedef uint16* uint16pointer;
typedef uint12* uint12pointer;

typedef int64 translow;
typedef translow* translowpointer;

typedef struct toplevel_s TOPLEVEL_T;
