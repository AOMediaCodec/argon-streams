/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

#define RANGE_VIS_TYPE_COUNT 20
#define MAX_RANGE_EQUATIONS_PER_GROUP 100

typedef struct SRangeEquationVisibilityType {
    const char *id;
    const char *name;
} TRangeEquationVisibilityType;

typedef struct {
    const char *name;
    const char *mathML;
    int extension;
    long long theoreticalRange[RANGE_VIS_TYPE_COUNT*2];
} TRangeEquation;

typedef struct {
    const char *name;
    TRangeEquation equations[MAX_RANGE_EQUATIONS_PER_GROUP];
    int equationCount;
} TRangeEquationGroup;

extern int rangeEquationGroupCount;
extern TRangeEquationGroup rangeEquationGroups[];
extern int rangeEquationVisibilityTypeCount;
extern TRangeEquationVisibilityType rangeEquationVisibilityTypes[];
