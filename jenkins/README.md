[//]: # (Project P8005 HEVC)
[//]: # (\(c\) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company)
[//]: # (All rights reserved.)

# CI Stuff

All scripts should all be run from the project root ("../").

## Old Jenkins Commands

All the Bash scripts in this "jenkins/" directory are based on old Jenkins
jobs.

## Copyright Header Checker

The "check_copyright.py" script uses the ".copyright" file in the project root
directory to check all source files have the correct copyright header
(wherever possible).
