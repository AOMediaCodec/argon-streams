#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# Build an encoder/bitstream generator and generate a few streams for each
# configuration in av1src/configurations.

# Use the "-d" debugging argument to check that each such stream uses all the
# settings specified in the configuration file.

set -x
set -e

av1product/check-configs-jenkins.sh -j4 -n20
