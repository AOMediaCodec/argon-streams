#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# A test that encodes and checks a small number of streams for every profile
# mentioned in av1_profile*.xml

set -x
set -e

profiles="$(python3 autoproduct/extract_profiles.py autoproduct/av1_profile{0,1,2}.xml)"
block_size=5

pfs=""
for prof in $profiles; do
  pfs="$pfs $prof:$block_size"
done

av1product/with-seed.sh \
  av1product/check-profiles-jenkins.sh -j15 --clean --default-seed {} $pfs
