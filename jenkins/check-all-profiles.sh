#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# Combined test for all three regression profiles

set -x
set -e

av1product/with-seed.sh \
    av1product/check-profiles-jenkins.sh -j4 --clean --default-seed {} \
        profile_regression0:250:1 \
        profile_regression1:250:1 \
        profile_regression2:250:1 \
        profile_regression0:50 \
        profile_regression1:50 \
        profile_regression2:50
