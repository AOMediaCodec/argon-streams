#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# Check out and build the tip of the av1-normative and master branches of
# libaom and check them against some streams

set -x
set -e

echo "Start IT Fireware Test >>>>>>>>>>>"
curl https://aomedia.googlesource.com/
echo "<<<<<<<<<<<<< End IT Fireware Test"

zip_dir=".."
version=v2.1
branches=(av1-normative master)

zip_name="$zip_dir/argon_coveragetool_av1_base_and_extended_profiles_$version.zip"

av1product/libaom-jenkins.sh -j4 --limit 100 "$zip_name" "${branches[@]}"
