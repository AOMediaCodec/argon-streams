#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# Build a release of the custom bitstream generator (originally for ClientE)

# Output is: bsg-release/*.tar.xz

set -x
set -e

rm -rf bsg-release
av1product/bsg_jenkins.sh -j4 --coverage
