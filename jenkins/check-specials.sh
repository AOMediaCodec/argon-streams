#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# Combined test for all three special profiles

set -x
set -e

av1product/with-seed.sh \
    av1product/check-profiles-jenkins.sh -j4 --clean --default-seed {} \
        profile_license_resolutions_profile0:55 \
        profile_license_resolutions_profile1:55 \
        profile_license_resolutions_profile2:55

# set -o pipefail

# cd av1product

# PROFILES="profile_license_resolutions_profile0 profile_license_resolutions_profile1 profile_license_resolutions_profile2"

# if [ -f failures.txt ]; then
#   rm failures.txt
# fi

# # Run a fixed block of 1000 tests (250 * {annexb,non-annexb} *
# # {large-scale-tile,non-large-scale-tile}) plus a random block of 200 tests for
# # each profile. Since the -reftest argument tests *all* of the streams in the
# # destination directory, we only need to pass it when generating the final set.
# STARTSEED=$(( 10000 + 100*$RANDOM ))
# ./parallel_makeall \
#     -c -nowait -j 4 -start $STARTSEED -block 55 -reftest -av1b -all-oppoints \
#     $PROFILES || exit 1

# if [ -f failures.txt ]; then
#   exit 1
# else
#   exit 0
# fi
