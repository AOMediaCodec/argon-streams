#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import configparser
from datetime import datetime
import filecmp
from fnmatch import fnmatch
import os
import re
import shutil
import subprocess
import sys
from typing import Dict, List, Optional

class Config:
    ''' Contains the configuration for the copyright check '''
    def __init__(self,
                 ignore_list: List[str]=[],
                 company: Dict[str, str]={},
                 rights_reserved: Dict[str, str]={}):
        # List of globs to ignore
        self.ignore_list = ignore_list
        # Path glob -> company name to use in the copyright notice
        self.company = company
        # Path glob -> 'all rights reserved' message to use in the copyright notice
        self.rights_reserved = rights_reserved

# Section heading in the configuration file, which is INI-style
SECTION_HEADING_RE = re.compile(r"\[(.*)\]")

def load_config() -> Config:
    ''' Load the config file for the copyright scan '''
    # Default config if no config file
    if not os.path.exists(".copyright"):
        return Config()

    heading = None
    ignore_list = []
    company = {}
    rights_reserved = {}
    with open(".copyright", "r") as f:
        for i, l in enumerate(f):
            ls = l.strip()
            if len(ls) == 0:
                continue
            if ls.startswith("#"):
                continue
            m = SECTION_HEADING_RE.fullmatch(ls)
            if m is not None:
                heading = m.group(1).lower()
                continue
            if heading == "ignore":
                ignore_list.append(ls)
            if heading == "companyname":
                parts = ls.partition(":")
                if parts[1] != ":":
                    print("Bad line {} in .copyright".format(i+1))
                    sys.exit(1)
                company[parts[0]] = parts[2]
            if heading == "rightsreserved":
                parts = ls.partition(":")
                if parts[1] != ":":
                    print("Bad line {} in .copyright".format(i+1))
                    sys.exit(1)
                rights_reserved[parts[0]] = parts[2]
    return Config(ignore_list, company, rights_reserved)

# Copyright line regex
COPYRIGHT_RE = re.compile(r"\(c\) Copyright ([0-9]{4}) (.*)$")

def check_notice(copyright_notice: List[str], company: str, rights_reserved: str, prefix: str) -> Optional[str]:
    ''' Check a copyright notice with no prefix on each line '''
    # Check line 1
    if copyright_notice[1] != prefix + "Project P8005 HEVC":
        return "Line 2 should say '{}Project P8005 HEVC'".format(prefix)

    # Check line 2
    m = COPYRIGHT_RE.fullmatch(copyright_notice[2][len(prefix):])
    current_year = datetime.now().year
    if m is None:
        return ("Line 3 not recognised. Use '{}(c) Copyright {} {}'."
               .format(prefix, current_year, company))
    if int(m.group(1)) != current_year:
        return "Use current year {} on line 3".format(current_year)
    if m.group(2) != company:
        return "Company name on line 3 must be {}".format(company)

    # Check line 3
    if copyright_notice[3] != prefix + rights_reserved:
        return "Line 4 should say '{}{}'".format(prefix, rights_reserved)
    return "OK"

def check_slash_star(copyright_notice: List[str], company: str, rights_reserved: str) -> Optional[str]:
    ''' Check C-style copyright notice '''
    # Check line 1
    if copyright_notice[0] != "/*******************************************************************************":
        return "Line 1 incorrect, please fill with asterisks to column 80"
    return check_notice(copyright_notice, company, rights_reserved, "")

def check_md(copyright_notice: List[str], company: str, rights_reserved: str) -> Optional[str]:
    ''' Check Markdown-style copyright notice '''
    return check_notice(copyright_notice, company, rights_reserved, "")

def check_rst(copyright_notice: List[str], company: str, rights_reserved: str) -> Optional[str]:
    ''' Check reStructuredText-style copyright notice '''
    return check_notice(copyright_notice, company, rights_reserved, "")

def check_single_char(copyright_notice: List[str], company: str, rights_reserved: str, char: str) -> Optional[str]:
    ''' Check a copyright notice prefixed by a single character, e.g. # '''
    # Check line 1
    if copyright_notice[0] != char * 80:
        return "Line 1 incorrect, please fill with '{}' to column 80".format(char)
    return check_notice(copyright_notice, company, rights_reserved, char + " ")

def find_globbed_path(d: Dict[str, str], fpath: str) -> str:
    ''' Find the longest globbed path in a dict that matches the given path '''
    paths = [x for x in d if fnmatch(fpath, x)]
    maxPath = max(paths, key=len)
    return d[maxPath]

def process_file_regular(fpath: str, config: Config) -> Optional[str]:
    ''' Process a file that isn't in the aviator/aom directory '''
    # Ignore any on our ignore glob list
    if any(fnmatch(fpath, p) for p in config.ignore_list):
        return "Ignored"
    # Read the file in
    with open(fpath, "r") as f:
        try:
            lines = [l.strip() for l in f.readlines()]
        except UnicodeDecodeError:
            return "Not valid UTF-8"
    # Deal with tiny/empty files
    if len(lines) < 4:
        return "No comment found on first line"
    # We allow the shebang before the copyright notice, but nothing
    # else.
    if lines[0].startswith("#!"):
        firstcomment = -1
        # Look for the first comment after the shebang. If we see anything else,
        # fail.
        for i in range(1, len(lines)):
            if lines[i].startswith("#"):
                firstcomment = i
                break
            if lines[i].startswith("/*"):
                firstcomment = i
                break
            if lines[i].startswith(".."):  # reStructuredText
                firstcomment = i
                break
            if len(lines[i]) > 0:
                return "Copyright notice must come immediately after shebang. Only blank lines are allowed inbetween."
        if firstcomment < 0 or firstcomment+4 > len(lines):
            return "No copyright notice found after shebang"
        copyright_notice = lines[firstcomment:firstcomment+4]
    else:
        copyright_notice = lines[:4]
    # Deal with HTML files
    if ( fpath.endswith(".html") or fpath.endswith(".xml") or
         fpath.endswith(".xslt") ):
        for i in range(0, len(lines)):
            # Allow HTML doctype before the copyright notice
            if lines[i].lower().startswith("<!doctype"):
                continue
            # Allow XML declaration before the copyright notice
            if lines[i].lower().startswith("<?xml"):
                continue
            if lines[i].startswith("<!--"):
                if i+5 > len(lines):
                    return "No copyright notice found"
                copyright_notice = lines[i+1:i+5]
                break
            if len(lines[i]) > 0:
                return "Copyright notice must come immediately after <!doctype>. Only blank lines are allowed inbetween."
    # Find the correct company name and rights-reserved line for this file
    company = find_globbed_path(config.company, fpath)
    rights_reserved = find_globbed_path(config.rights_reserved, fpath)
    # Deal with slash-star notices
    if copyright_notice[0].startswith("/*"):
        return check_slash_star(copyright_notice, company, rights_reserved)
    # Deal with Markdown notices
    if copyright_notice[0].startswith("{::comment}"):
        return check_md(copyright_notice, company, rights_reserved)
    # Deal with reStructuredText notices
    if copyright_notice[0].startswith(".."):
        return check_rst(copyright_notice, company, rights_reserved)
    # Deal with hash, semicolon, and percent sign notices
    for ch in "#;%":
        if copyright_notice[0].startswith(ch):
            return check_single_char(copyright_notice, company, rights_reserved, ch)
    # Otherwise we didn't see anything we liked
    return "No comment found on first line"

def main() -> None:
    config = load_config()

    fail_count = 0
    pass_count = 0
    # Get a list of all the files checked in to git
    files = subprocess.check_output(["git", "ls-files"]).decode("utf8").split("\n")
    for fpath in files:
        if len(fpath) == 0:
            continue
        # Only process regular files
        if not os.path.isfile(fpath):
            continue
        result = process_file_regular(fpath, config)
        if result == "OK":
            pass_count += 1
            continue
        if result == "Ignored":
            continue
        print("FAIL: {} : {}".format(fpath, result))
        fname = os.path.split(fpath)[1]
        fail_count += 1
    print("Passed: {} files".format(pass_count))
    print("Failed: {} files".format(fail_count))
    if fail_count > 0:
        sys.exit(1)

###############################################################################
# Main entry

if __name__ == '__main__':
    main()
