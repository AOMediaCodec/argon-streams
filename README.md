[//]: # (Project P8005 HEVC)
[//]: # (\(c\) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company)
[//]: # (All rights reserved.)

# Introduction

Argon Streams is a set of bit-streams to ensure a video decoder is fully compliant with the specification.  The bit-streams cover all syntax elements, as well as values, combinations of values and ranges of equations to provide confidence that a video decoder has been fully verified, whilst aiming to minimise simulation time.

This repository includes the necessary code and tooling to encode and verify the bit-streams, as well as the coverage tool.  The coverage tool computes the coverage provided by a set of bit-streams and produces a user-readable report.

For more background information see the [history](doc/HISTORY.md).

# AV1 Pseudo Code Specification

A bit accurate, pseudo-code Specification for AV1, derived from the AV1 Reference Code, and verified using Argon Streams AV1 has been written for the Bit-stream Generator (tools/av1src).  Subsequently the AV1 specification was finalised and a pseudo-code version of this has been completed for the Coverage Tool and the Software Decoder (tools/av1bsrc).

The pseudo-specification is a mathematically precise definition of the entire specification that is understandable by the compiler (including recognising section and equation numbers).  It is used to create three software tools: the Bit-stream Generator, the Coverage Tool and a Software Decoder (the latter of which was also used to verify the AV1 specification against AOM's reference decoder).

These specifications match the v1.0.0 errata1 version of the AV1 specification and AOM's reference decoder.

See [pseudocode](doc/PSEUDOCODE.md) for more examples of of the supported pseudo-code.

# The Process

The bit-stream generation process is explained in this diagram and the text below:
![Process](doc/process.png)

The Bit-stream Generator is fed with directed random numbers, to explore the whole space of valid bit-streams - Argon Streams AV1 using a weighted probability distribution for each syntax element.

The Bit-streams are verified against the AV1 specification and the AV1 reference code, rejecting any invalid streams (certain AV1 functionalties are hard to accurately randomly encode, eg the Decoder Model).

The Coverage Tool is then used to measure and verify coverage of all test cases.  Its final output is a coverage report which shows measurable and demonstrable full coverage.

See [executables](doc/EXECUTABLES.md) for more explanation of the different executables
that can be made.

# Setup

See the [setup documentation](doc/SETUP.md).

# Using the Bit-streams and Coverage Tool

Documentation relating to the use of the bit-streams and how to use the coverage tool and the coverage report is included in the public release.

A video explaining the coverage report is available on [Youtube](https://www.youtube.com/watch?v=3Z97dB9FKuw).  This is for VP9, but also applies to AV1.

# Making a Set of Bit-streams

The modern way to generate a set of bit-streams is
to call autoproduct with the relevant profile XML file:

```sh
cd autoproduct/
python3 autoproduct.py av1_profile0.xml && \
python3 autoproduct.py av1_profile1.xml && \
python3 autoproduct.py av1_profile2.xml
# wait for hours and hours...
cd -
```

For more details about options which can be provided to autoproduct:

```sh
cd autoproduct/
python3 autoproduct.py --help
cd -
```

Alternatively, parallel_makeall can be used to generate bit-streams.

# Releasing a Set of Bit-streams

To generate a release from bit-streams generated using autoproduct:

```sh
cd av1product
./release.sh ../release-build
# wait for hours and hours...
cd -
```

The `stream documentation.xlsx` release file can be generated using
`autoproduct/make_spreadsheet.xlsm` and a directory containing all csv files
(`autoproduct/av1_profile*/*.csv`).

# Advanced Usage

It is possible to run the individual components depicted above seperately for a variety of purposes.  See the [advanced usage documentation](doc/ADVANCED.md) for more information.
