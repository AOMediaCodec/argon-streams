#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import argparse
import sys
import re
import pdb


entities = ["&minus;", "&#x2212;", "&times;", "&#x00D7;", "&divide;",
            "&#x00F7;", "&ne;", "&#x2260;", "&asymp;", "&#x2248;",
            "&lt;", "&#x003C;", "&le;", "&#x2264;", "&gt;", "&#x003E;",
            "&ge;", "&#x2265;", "&plusmn;", "&#x00B1;", "&prop;", "&#x221D;",
            "&sum;", "&#x2211;", "&prod;", "&#x220F;", "&lfloor;", "&#x230A;",
            "&rfloor;", "&#x230B;", "&lceil;", "&#x2308;", "&rceil;",
            "&#x2309;", "&sdot;", "&times;", "&divide;", "&prime;", "&#x2032;",
            "&Prime;", "&#x2033;", "&tprime;", "&#x2034;", "&qprime;",
            "&#x2057;", "&part;", "&#x2202;", "&Delta;", "&#x0394;", "&Del;",
            "&#x2207;", "&int;", "&#x222B;", "&Int;", "&#x222C;", "&tint;",
            "&#x222D;", "&qint;", "&#x2A0C;", "&conint;", "&#x222E;",
            "&cwconint;", "&#x2232;", "&awconint;", "&#x2233;", "&Conint;",
            "&#x222F;", "&Cconint;", "&#x2230;", "&infin;", "&#x221E;",
            "&prime;", "&Prime;", "&hellip;", "&#x2026;", "&vellip;",
            "&#x22EE;", "&ctdot;", "&#x22EF;", "&utdot;", "&#x22F0;",
            "&dtdot;", "&#x22F1;", "&deg;", "&#x00B0;", "&ang;", "&#x2220;",
            "&angmsd;", "&#x2221;", "&angrt;", "&#x221F;", "&vangrt;",
            "&#x299C;", "&lrtri;", "&#x22BF;", "&cir;", "&#x25CB;", "&xutri;",
            "&#x25B3;", "&squ;", "&#x25A1;", "&fltns;", "&#x25B1;", "&spar;",
            "&#x2225;", "&npar;", "&#x2226;", "&perp;", "&#x22A5;", "&cong;",
            "&#x2245;", "&rarr;", "&#x2192;", "&harr;", "&#x2194;", "&#x002D;",
            "&Alpha;", "&alpha;", "&#x0391;", "&#x03B1;", "&Beta;", "&beta;",
            "&#x0392;", "&#x03B2;", "&Gamma;", "&gamma;", "&#x0393;",
            "&#x03B3;", "&Delta;", "&delta;", "&#x0394;", "&#x03B4;",
            "&Epsilon;", "&epsilon;", "&#x0395;", "&#x03B5;", "&Zeta;",
            "&zeta;", "&#x0396;", "&#x03B6;", "&Eta;", "&eta;", "&#x0397;",
            "&#x03B7;", "&Theta;", "&theta;", "&#x0398;", "&#x03B8;", "&Iota;",
            "&iota;", "&#x0399;", "&#x03B9;", "&Kappa;", "&kappa;", "&#x039A;",
            "&#x03BA;", "&Lambda;", "&lambda;", "&#x039B;", "&#x03BB;", "&Mu;",
            "&mu;", "&#x039C;", "&#x03BC;", "&Nu;", "&nu;", "&#x039D;",
            "&#x03BD;", "&Xi;", "&xi;", "&#x039E;", "&#x03BE;", "&Omicron;",
            "&omicron;", "&#x039F;", "&#x03BF;", "&Pi;", "&pi;", "&#x03A0;",
            "&#x03C0;", "&Rho;", "&rho;", "&#x03A1;", "&#x03C1;", "&Sigma;",
            "&sigma;", "&#x03A3;", "&#x03C3;", "&Tau;", "&tau;", "&#x03A4;",
            "&#x03C4;", "&Upsilon;", "&upsilon;", "&#x03A5;", "&#x03C5;",
            "&Phi;", "&phi;", "&#x03A6;", "&#x03C6;", "&Chi;", "&chi;",
            "&#x03A7;", "&#x03C7;", "&Psi;", "&psi;", "&#x03A8;", "&#x03C8;",
            "&Omega;", "&omega;", "&#x03A9;", "&#x03C9;", "&ApplyFunction;",
            "&af;", "&#x2061;", "&InvisibleTimes;", "&it;", "&#x2062;",
            "&InvisibleComma;", "&ic;", "&#x2063;", "&#x2064;", "&not;",
            "&#x00AC;", "&and;", "&#x2227;", "&or;", "&#x2228;", "&veebar;",
            "&#x22BB;", "&forall;", "&#x2200;", "&exist;", "&#x2203;",
            "&rArr;", "&#x21D2;", "&hArr;", "&#x21D4;", "&EmptySmallSquare;",
            "&#x25FB;", "&loz;", "&#x25CA;", "&vdash;", "&#x22A2;", "&vDash;",
            "&#x22A8;", "&there4;", "&#x2234;", "&oplus;", "&#x2295;",
            "&empty;", "&#x2205;", "&isin;", "&#x2208;", "&notin;", "&#x2209;",
            "&sube;", "&#x2286;", "&nsube;", "&#x2288;", "&sub;", "&#x2282;",
            "&nsub;", "&#x2284;", "&supe;", "&#x2287;", "&nsupe;", "&#x2289;",
            "&sup;", "&#x2283;", "&nsup;", "&#x2285;", "&cap;", "&#x2229;",
            "&cup;", "&#x222A;", "&ssetmn;", "&#x2216;", "&subne;", "&supne;",
            "&#x228A;", "&#x228B;", "& Function;", "&sdot;", "&#x22C5;",
            "&Cross;", "&#x2A2F;", "&Vert;", "&#x2016;", "&lang;", "&#x27E8;",
            "&rang;", "&#x27E9;", "&compfn;", "&#x2218;", "&rarr;", "&#x2192;",
            "&mapsto;", "&#x21A6;", "&imath;", "&#x0131;", "&jmath;",
            "&#x0237;"]

# profiles is keyed by profile name (of the form 'tag:name') with values a list
# of strings which contain python statements (probably of the form "someVar =
# expr").
#
# ifdefs is keyed by profile tag (the part of the profile name up to the
# colon). If a profile has an entry, this corresponds to a 'CPP' line in the
# input and means we'll surround code to do with this profile with an ifdef in
# the output.
#
# Since we sometimes need to iterate over profiles in the right order, we also
# store profile_names, a list of the keys of profiles in the order we saw in
# the file.
profiles = {}
ifdefs = {}
profile_names = []


def read_min_max(linum, rhs, py_env):
    ret = {}
    for k, p in profiles.items():
        try:
            globs = py_env.copy() if py_env is not None else dict()
            exec('\n'.join(p), globs)
            ret[k] = eval(rhs, globs)
        except Exception:
            sys.stderr.write('Couldn\'t parse min/max on line {}.\n'
                             .format(linum))
            raise

    return ret


def readIdentifier(eqn):
    mathml=["<mi>"]
    while len(eqn)>0:
      m=re.search("^([A-Za-z0-9_]+)",eqn)
      if m:
        g=m.group(1)
        mathml.append(g)
        eqn=eqn[len(g):]
        continue
      foundEntity=False
      for en in entities:
        if eqn.startswith(en):
          mathml.append(en.replace("&","&amp;"))
          eqn=eqn[len(en):]
          foundEntity=True
          break
      if not foundEntity:
        break
    if eqn.startswith("'"):
      mathml.append("&amp;prime;")
      eqn=eqn[1:]
    mathml.append("</mi>")
    return (mathml, eqn)


def writeToJS(x):
    print("document.write(\"{}\");".format(x))


def read_mathml(eqn):
    mathml = []
    while eqn:
        eqn = eqn.strip()
        m = re.search(R'^-?[0-9]+',eqn)
        if m:
          g=m.group(0)
          mathml.append("<mn>{}</mn>".format(g))
          eqn=eqn[len(g):]
          continue
        if eqn.startswith("&"):
          foundEntity=False
          for en in entities:
            if eqn.startswith(en):
              (mathmlAppend,eqn)=readIdentifier(eqn)
              mathml.extend(mathmlAppend)
              foundEntity=True
              break
          if not foundEntity:
            mathml.append("<mo>&amp;</mo>")
            eqn=eqn[1:]
          continue
        m=re.search(R'^(?:<<=?|>>=?|[<>\*\+\-=%/]=?|'
                    R'[\?\:\(\)\[\]\,]|if|else)', eqn)
        if m:
          g=m.group(0)
          snippet=g.replace("<","&lt;").replace(">","&gt;")
          mathml.append("<mo>{}</mo>".format(snippet))
          eqn=eqn[len(g):]
          continue
        m=re.search(R'^([A-Za-z][A-Za-z]*)(-?[0-9]+),(-?[0-9]+)',eqn)
        if m:
          mathml.append("<msub><mi>{}</mi><mrow><mn>{}</mn><mtext>,"
                        "</mtext><mn>{}</mn></mrow></msub>"
                        .format(m.group(1),m.group(2),m.group(3)))
          eqn=eqn[len(m.group(0)):]
          continue
        m=re.search(R'^[A-Za-z_]',eqn)
        if m:
          (mathmlAppend,eqn)=readIdentifier(eqn)
          mathml.extend(mathmlAppend)
          continue

        print("Failed to interpret {}".format(eqn))
        pdb.set_trace()
        sys.exit(0)

    return ''.join(mathml)


class Handler:
    _CPP_RE = re.compile('CPP=([a-zA-Z_]+)$')

    def __init__(self):
        self.py_code = None
        self.cur_profile = None
        self.cur_tag = None

        self.eqn_name = None
        self.mathml = None
        self.mins = None
        self.maxes = None

        self.py_env = None

    def start(self):
        pass

    def end_section(self, line):
        pass

    def write_eqn(self, eqn_name, mathml):
        pass

    def write_min_max(self, tag, name, eq_min, eq_max):
        pass

    def end_eqn(self):
        pass

    def end_file(self):
        pass

    def take_line(self, linum, line, full):
        if self.py_code is not None:
            if line == ':::':
                # We've got to the end of a chunk of Python code. Execute it
                # (eek!)
                py_code = ''.join(self.py_code)
                self.py_env = {}
                exec(py_code, self.py_env)
                self.py_code = None
            else:
                self.py_code.append(full)

            return

        if self.cur_profile is not None:
            indented = full[0] == ' '
            if indented:
                # If the line was indented, it might be of the form
                # CPP=<something>. This is (my rather lame way of) saying that
                # anything to do with this profile should be enclosed with an
                # ifdef.
                match = Handler._CPP_RE.match(line)
                if match:
                    if self.cur_tag in ifdefs:
                        raise RuntimeError('Duplicate CPP line '
                                           'at line {} for profile {}.'
                                           .format(linum, self.cur_profile))
                    ifdefs[self.cur_tag] = match.group(1)
                    return

                # Otherwise add the parameter to the profile and return.
                profiles[self.cur_profile].append(line)
                return

            # Otherwise we aren't indented. This means that we've finished
            # reading the profile arguments. Clear cur_profile and keep
            # going (we need to make sense of this line still)
            self.cur_profile = None
            self.cur_tag = None

        if self.eqn_name is not None:
            # We're reading an equation at the moment. Is this a MIN or a MAX
            # line?
            if line.startswith('MIN:'):
                assert self.mins is None
                self.mins = read_min_max(linum, line[4:].strip(), self.py_env)
                return
            if line.startswith('MAX:'):
                assert self.maxes is None
                self.maxes = read_min_max(linum, line[4:].strip(), self.py_env)
                return

            # Nope? Let's hope the equation is done...
            self.finish_equation()

        # We're not in any sort of special section. Spot the start of such a
        # section.
        if line == ':::':
            if self.py_env is not None:
                raise RuntimeError('Line {} starts a second python block. '
                                   'Not supported.'
                                   .format(linum))
            self.py_code = []
            return

        if line.startswith('*'):
            name = line[1:]
            assert name not in profile_names
            profiles[name] = []
            profile_names.append(name)

            parts = name.split(":", 1)
            if len(parts) != 2:
                raise RuntimeError('Profile name on line {} is {!r}, '
                                   'which contains no colon.'
                                   .format(linum, name))
            tag = parts[0]
            if tag in ifdefs:
                raise RuntimeError('Profile name on line {} has tag '
                                   '{}, which already has an associated '
                                   'CPP entry.'
                                   .format(linum, tag))

            self.cur_profile = name
            self.cur_tag = tag
            return

        eqn_match = re.match(r'^(.*)\s*\[([^\]]*)\]$', line)
        if eqn_match:
            assert self.eqn_name is None
            assert self.mins is None
            assert self.maxes is None
            assert self.mathml is None

            self.eqn_name = eqn_match.group(2)
            self.mathml = read_mathml(eqn_match.group(1))
            if '"' in self.mathml:
                raise RuntimeError("MathML on line {} contains double quotes. "
                                   "Use single quotes instead.".format(linum))
            return

        self.end_section(line)

    def finish_equation(self):
        if self.mins is None:
            raise RuntimeError('Equation with no MIN')
        if self.maxes is None:
            raise RuntimeError('Equation with no MAX')

        self.write_eqn(self.eqn_name, self.mathml)
        for k in profile_names:
            tag, name = k.split(":")
            self.write_min_max(tag, self.eqn_name, self.mins[k], self.maxes[k])
        self.end_eqn()

        self.eqn_name = None
        self.mins = None
        self.maxes = None
        self.mathml = None

    def finish(self):
        if self.py_code is not None:
            raise RuntimeError('Unterminated py code block')

        if self.cur_profile is not None:
            sys.stderr.write('Warning: Still reading a profile at EOF. '
                             'Don\'t you have any ranges?')
            return

        if self.eqn_name is not None:
            self.finish_equation()

        self.end_file()


class CppHandler(Handler):
    def __init__(self):
        super().__init__()
        self.prev_section = None
        self.prev_section_count = None
        self.group_count = 0

    def start(self):
        print('#include "range_equations.h"\n'
              '#include "customers.h"\n'
              '#include <stddef.h>\n'
              '\n'
              'TRangeEquationGroup rangeEquationGroups[] = {')

    def end_section(self, line):
        if self.prev_section == line:
            return

        if self.prev_section is not None:
            assert self.prev_section_count is not None
            print("    }},\n"
                  "    {}\n"
                  "  }},"
                  .format(self.prev_section_count))

        self.group_count += 1

        print("  {{ \"{}\",\n"
              "    {{"
              .format(line))
        self.prev_section = line
        self.prev_section_count = 0

    def write_eqn(self, eqn_name, mathml):
        print("      {")
        print("        \"{}\",".format(eqn_name))
        print("        \"{}\",".format(mathml))
        print("        0,".format(0))
        print("        {")

        assert self.prev_section_count is not None
        self.prev_section_count += 1

    @staticmethod
    def ifdef_print(tag, line):
        ifdef = ifdefs.get(tag)
        if ifdef is not None:
            print('#ifdef {}'.format(ifdef))
        print(line)
        if ifdef is not None:
            print('#endif')

    def write_min_max(self, tag, eq_name, eq_min, eq_max):
        CppHandler.ifdef_print(tag,
                               "        {}LL, {}LL,".format(eq_min, eq_max))

    def end_eqn(self):
        print("        }")
        print("      },")

    def end_file(self):
        if self.prev_section is not None:
            assert self.prev_section_count is not None
            print("    },")
            print("    {}".format(self.prev_section_count))
            print("  }")
            print("};\n")

        print("int rangeEquationGroupCount = {};\n".format(self.group_count))

        print("TRangeEquationVisibilityType "
              "rangeEquationVisibilityTypes[] = {")
        for k in profile_names:
            tag, name = k.split(":")
            CppHandler.ifdef_print(tag,
                                   "  {{ \"{}\", \"{}\" }},".format(tag, name))
        print("  { NULL, NULL }")
        print("};\n")


class PyHandler(Handler):
    def start(self):
        print("rangeEqns={")

    def write_eqn(self, eqn_name, mathml):
        print("  '{}': {{".format(eqn_name))

    def write_min_max(self, tag, eq_name, eq_min, eq_max):
        print("    '{}': ({},{}),".format(tag, eq_min, eq_max))

    def end_eqn(self):
        print("  },")

    def end_file(self):
        print("}")


class JsHandler(Handler):
    def __init__(self):
        super().__init__()
        self.first_section = True

    def end_section(self, line):
        if not self.first_section:
            writeToJS('</table>')
        self.first_section = False

        writeToJS("<a href=\\\"javascript:void(0)\\\" "
                  "onClick=\\\"scrollPipelineToTop()\\\">"
                  "<h1 id=\\\"{}\\\">{}</h1></a>"
                  .format(re.sub(" ", "", line.lower()), line))
        writeToJS("<table border=\\\"1\\\">\\n")
        writeToJS("<tr><th rowspan=\\\"2\\\">Equation number</th>"
                  "<th rowspan=\\\"2\\\"></th>")
        for k in profile_names:
            tag, name = k.split(":")
            writeToJS("<th colspan=\\\"2\\\" "
                      "class=\\\"rangecol {}_col\\\">{}</th>"
                      .format(tag, name))
        writeToJS("</tr>\\n<tr>")

        for k in profile_names:
            tag, name = k.split(":")
            writeToJS("<th style=\\\"width:6em; max-width: 6em\\\" "
                      "class=\\\"rangecol {}_col\\\">Minimum</th>"
                      "<th style=\\\"width:6em; max-width: 6em\\\" "
                      "class=\\\"rangecol {}_col\\\">Maximum</th>"
                      .format(tag, tag))

        writeToJS("</tr>\\n")

    def write_eqn(self, eqn_name, mathml):
        writeToJS("<tr><td style=\\\"width: 3em\\\">"
                  "<a href=\\\"javascript:void(0)\\\" "
                  "onClick=\\\"highlightRangeEquation('{}')\\\" "
                  "class=\\\"rangeEqnLink{}\\\">{}</a></td>"
                  "<td style=\\\"width:70%; padding:0px 10px\\\">"
                  "<math xmlns=\\\"http://www.w3.org/1998/Math/MathML\\\" "
                  "display=\\\"block\\\">{}</math></td>"
                  .format(eqn_name, eqn_name, eqn_name, mathml))

    def write_min_max(self, tag, eq_name, eq_min, eq_max):
        writeToJS("<td class=\\\"uncovered rangecol {}_col\\\">"
                  "<span class=\\\"eqnMin{}\\\"></span>"
                  " (<span class=\\\"eqnTheoreticalMin\\\">{}</span>)</td>"
                  .format(tag, eq_name, eq_min))
        writeToJS("<td class=\\\"uncovered rangecol {}_col\\\">"
                  "<span class=\\\"eqnMax{}\\\"></span> "
                  "(<span class=\\\"eqnTheoreticalMax\\\">{}</span>)</td>"
                  .format(tag, eq_name, eq_max))

    def end_eqn(self):
        writeToJS("</tr>\\n")

    def end_file(self):
        writeToJS("</table>\\n")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('flavour', choices=['cpp', 'py', 'js'])
    parser.add_argument('infile')

    args = parser.parse_args()

    handlers = {'cpp': CppHandler, 'py': PyHandler, 'js': JsHandler}
    mk_handler = handlers.get(args.flavour)
    assert mk_handler is not None

    handler = mk_handler()
    handler.start()

    with open(args.infile, "r") as fin:
        for j, line in enumerate(fin):
            full = line.split('#', 1)[0]
            line = full.strip()
            if not line:
                continue

            handler.take_line(j + 1, line, full)
        handler.finish()


if __name__ == '__main__':
    main()
