..
  Project P8005 HEVC
  (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
  All rights reserved.

========================
 Checklist for releases
========================

In general
==========

- Run ``products/check-release-structure.sh`` to check that we have
  just files that we expect. This checks we don't have any files we're
  not expecting, that the file lists look sane, that all streams have
  the same extension and that all MD5s in layer zero are unique. It
  also checks that every set has the same list of ``md5_<foo>``
  directories.
- Use ``products/check-release-files.sh`` to check that all of the
  streams decode correctly. This script expects a codec-specific "test
  one file" script, which checks a single stream.
- Check user manual and/or release notes are up to date
- Check resolutions are included
- Check bitrate streams are included
- Check coverage report is viewable with coverage folder in the zip
- Check coverage report doesn't list any client-specific views
- Check windows and linux coverage tools work on streams (if included
  in the release)
- Check version numbers in documentation and release notes
- Check only specifications for appropriate standard are included
- Check that the coverage report says what we expect (100% for all
  profiles if a full release). Make sure no spurious customer-specific
  coverage points are included.
- Check coverage tool generates no customer specific coverage (i.e. no
  ClientK crosses)
- Check all streams decode with coverage tool (individually)
- Check no points are over-covered
- Check all profiles mentioned in release notes are included (nb user
  manual will list all optional profiles, which won't be included in
  base versions of product)
- Check user manual has no markup from track changes. Update contents
  table.
- Check user manual is included (as pdf) and has correct version (and
  is for correct codec)
- Check JIRA to see if there's anything that should be done before
  release
- Check that stream documentation spreadsheet is included
- Check manually that md5s can be matched using instructions in the
  user manual (section 5.2 for AV1)
- Create variant ZIPs (zips not 7zips)
- There is a script av1product/make_coverage_src_pkg that does this,
  but not P8005-2256 (current customer names are included)
- Check zip files can be unzipped with Windows Explorer, Winzip and
  linux unzip.
- Check Customers get their appropriate release. Details are in
  P8005-U-001x (Customer Summary).xlsx

AV1 specific
============

The following flavours should be generated for a full AV1 release:

- AV1 Base Profiles Streams + Extended Profiles Streams + Coverage
  Tool
- AV1 Base Profiles Streams + Extended Profiles Streams
- AV1 Base Profiles Streams + Coverage Tool
- AV1 Base Profiles Streams
- AV1 Coverage Tool source release for ClientE (ensure that the build
  number matches the coveragetool.exe and that it is labelled av1 not
  av1b)

Extra checks:

- Check AV1 streams decode correctly with latest AOM ref decoder (as
  installed, configured, patched and built in the user manual)
