#!/usr/bin/env groovy

/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

@Grab('org.jsoup:jsoup:1.11.3')
import org.jsoup.*
import org.jsoup.nodes.*
import org.jsoup.select.*
import groovy.json.StringEscapeUtils

syntaxHighlightClasses = [ "hll", "c", "err", "g", "k", "l", "n", "o", "x", "p", "cm", "cp", "c1", "cs", "gd", "ge", "gr", "gh", "gi", "go", "gp", "gs", "gu", "gt", "kc", "kd", "kn", "kp", "kr", "kt", "ld", "m", "s", "na", "nb", "nc", "no", "nd", "ni", "ne", "nf", "nl", "nn", "nx", "py", "nt", "nv", "ow", "w", "mf", "mh", "mi", "mo", "sb", "sc", "sd", "s2", "se", "sh", "si", "sx", "sr", "s1", "ss", "bp", "vc", "vg", "vi", "il" ]

println "Loading pseudocode functions..."

File file = new File( this.args[0] )
pseudocodeFunctions = [] as Set
file.eachLine { line ->
    if ( line.trim() ) {
        pseudocodeFunctions += line.split(";")[1]
    }
}

println "Fetching web page..."
doc = Jsoup.connect("http://av1-draft-spec.argondesign.com/av1-spec/av1-spec.html").maxBodySize(0).timeout(600000).get()

// Remove the top two divs (these are the Argon Design top bar and a spacer)
println "Removing top two divs..."
doc.body().with {
    selectFirst("div").remove();
    selectFirst("div").remove();
}

// Fix all the relative URLs
println "Fixing relative URLs..."
urlFixes = [
    link: "href",
    script: "src",
    img: "src"
]
urlFixes.each { tag, attr ->
    doc.select(tag).each { e ->
        if (e.hasAttr(attr)) {
            url = e.attr(attr)
            if (!(url.startsWith("http://") || url.startsWith("https://"))) {
                url = "spec_av1b/${url.split("/")[-1]}"
                e.attr(attr, url)
            }
        }
    }
}

// Find all the function links

println "Finding pseudocode links..."
//First, the ones in sections of pseudocode
functionToElem = [:]

invalidFunctionNames = [
    "for",
    "while",
    "if"
]

[".syntax", ".language-c"].each { codeSectionClass ->
    codeSections = doc.select(codeSectionClass)
    codeSections.each { cs ->
        cs.select(":matches(\\w+\\s*\\([^;\\)]*\\)\\s*\\{)").each { fElem ->
            m = (fElem.text() =~ /(?m)^(\w+)\s*\([^;\)]*\)\s*\{/)
            m.each { m2 ->
                f = m2[1]
                if (!invalidFunctionNames.contains(f))
                    functionToElem[f] = fElem
            }
        }
    }
}

println "Finding section title links..."
//Now the ones in 'process' section titles
sections = doc.select("h2, h3, h4, h5, h6")

sections.each { s ->
    title = s.text().toLowerCase()
    if (title.endsWith(" process")) {
        f = title[0..-9].replaceAll(" ", "_")
        if (!functionToElem.containsKey(f))
            functionToElem[f] = s
    }
}

println "Finding text links..."
//Now the ones in blocks of text
search1 = doc.select(":matchesOwn(is a function call)")
search2 = doc.select(":matchesOwn(The function \\w*(\\([^\\)]*\\))? (?!is invoked))")
search3 = doc.select(":matchesOwn(This process is invoked when the function)") // \\w*(\\s*\\(.*\\))?)")
search4 = doc.select(":matchesOwn(When the function \\w*(\\s*\\([^\\)]*\\))? is invoked)")
//search5 = doc.select(":matchesOwn(The function \\w*(\\s*\\(.*\\))? returns)")

search1.each { n ->
    assert n.parent() != null
    m = ( n.text() =~ /^(\w*)\s*/ )
    f = m[0][1]
    if (!functionToElem.containsKey(f))
        functionToElem[f] = n
}

search2.each { n ->
    assert n.parent() != null
    m = ( n.text() =~ /^The function (\w*)\s*/ )
    f = m[0][1]
    if (!functionToElem.containsKey(f))
        functionToElem[f] = n
}

search3.each { n ->
    assert n.parent() != null
    m = ( n.text() =~ /^This process is invoked when the function (\w*)\s*/ )
    f = m[0][1]
    if (!functionToElem.containsKey(f))
        functionToElem[f] = n
}

search4.each { n ->
    assert n.parent() != null
    m = ( n.text() =~ /^When the function (\w*)\s*/ )
    f = m[0][1]
    if (!functionToElem.containsKey(f))
        functionToElem[f] = n
}

specMappings = [
    'lower_precision': 'lower_mv_precision',
    'inverse_walsh-hadamard_transform': 'inverse_wht',
    'intra_prediction': 'predict_intra',
    'inter_prediction': 'predict_inter',
    'temporal_sample': 'add_tpl_ref_mv',
    'palette_prediction': 'predict_palette',
    'symbol_decoding': 'read_symbol_inner',
    'boolean_decoding': 'read_bool',
    'sorting': 'sort_mv_stack',
    '2d_inverse_transform': 'inverse_transform_2d',
]

unusedSpecFunctions = [
    'tile_list_entry',           // Large scale tile
    'CeilLog2',                  // Too trivial to appear in coverage report
    'large_scale_tile_decoding', // Large scale tile
    'is_transpose',              // Not part of the decoder
    'parsing',                   // Umbrella section header
    'reserved_obu',              // Too trivial to appear in coverage report
    'random_number',             // Umbrella section header
    'tile_list_obu',             // Large scale tile
    'le',                        // CABAC function, inlined
    'FloorLog2',                 // Too trivial to appear in coverage report
    'ns',                        // CABAC function, inlined
    'decoding',                  // Umbrella section header
    'NS',                        // CABAC function, inlined
    'decode_camera_tile',        // Large scale tile
    'cdf_selection',             // Umbrella section header
    'uvlc',                      // CABAC function, inlined
    'general_decoding',          // Umbrella section header
    'su',                        // CABAC function, inlined
    'project',                   // Incorporated into get_block_position in the pseudocode
    'intra_filter_type',         // Umbrella section header
    'leb128',                    // CABAC function, inlined
    'inverse_transform',         // Umbrella section header
    'call',                      // Artefact of our function extraction code
]

wrapInnerTags = [ "h1", "h2", "h3", "h4", "h5", "h6", "td" ]

specFunctions = [] as Set

//Wrap all the functions in function anchors
functionToElem.each { f, elem ->
    fn = specMappings.containsKey(f) ? specMappings[f] : f
    specFunctions += fn
    if (!unusedSpecFunctions.contains(fn)) {
        if (wrapInnerTags.contains(elem.tagName())) {
            elem.wrap("<${elem.tagName()} ${elem.attributes().html()}><span class=\"functionAnchor\" id=\"functionAnchor_$f\"></span></${elem.tagName()}").unwrap()
        }
        else {
            elem.wrap("<span class=\"functionAnchor\" id=\"functionAnchor_$f\"></span>")
        }
    }
}

// Strip out syntax highlighting
println "Finding syntax highlighted sections..."
highlights = doc.select(".highlight")
println "Found ${highlights.size()} sections"
println "Stripping syntax highlighting..."

boolean removeSyntaxHighlighting(Element e) {
    if (e.tagName() != "span") return false;

    res = false
    e.classNames().each { c ->
        if (syntaxHighlightClasses.contains(c)) {
            e.removeClass(c)
            res = true
        }
    }
    return res && e.classNames().isEmpty()
}

// Build a separate tree with the syntax highlighting removed - edits to the
// existing tree are slow because everything is stored in arrays under the hood
highlights.each { h ->
    def stack = []
    def topNode = null
    h.traverse(new NodeVisitor() {
        void head(Node node, int depth) {
          def n = node.shallowClone()
          if (n instanceof Element) {
              def e = n as Element
              if (removeSyntaxHighlighting(e)) {
                  stack.push(stack[-1])
                  return
              }
          }
          if (!stack.isEmpty())
            stack[-1].addChildren(n)
          else
            topNode = n
          stack.push(n)
        }
        void tail(Node node, int depth) {
          stack.pop()
        }
    })
    assert stack.isEmpty()
    assert topNode != null
    h.replaceWith(topNode)
}

new File("spec_av1b.html").withWriter("utf-8") {
    writer -> writer.write(doc.outerHtml())
}

unlinkedPseudocodeFunctions = [
    'ignore_obu',
    'get_past_symbol_bit',
    'get_palette',
    'qs_partition',
    'get_partition_cdf',
    'get_sm_weight',
    'read_symbol_data',
    'global_data',
]

pseudocodeFunctions -= unlinkedPseudocodeFunctions

common = pseudocodeFunctions.intersect(specFunctions)
onlyInPseudocode = pseudocodeFunctions - specFunctions
onlyInSpec = specFunctions - (pseudocodeFunctions + unusedSpecFunctions)

println ""
println "Common [${common.size()}]:"
common.each { fn -> println fn }
println ""

println "Only in pseudocode [${onlyInPseudocode.size()}]:"
onlyInPseudocode.each { fn -> println fn }
println ""

println "Only in spec [${onlyInSpec.size()}]:"
onlyInSpec.each { fn -> println fn }
