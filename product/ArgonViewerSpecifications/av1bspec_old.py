################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

from bs4 import BeautifulSoup
from bs4 import NavigableString
import urllib2
import re
import sys
import itertools

if len(sys.argv) != 2:
    print "Syntax: {} /path/to/av1b_all_points.txt".format(sys.argv[0])
    sys.exit(1)

functions = set()

with open(sys.argv[1], "r") as f:
    for line in f:
        parts = line.split(";")
        functions.add(parts[1])

response = urllib2.urlopen("http://av1-draft-spec.argondesign.com/av1-spec/av1-spec.html")
html_src = response.read()

soup = BeautifulSoup(html_src, "html.parser")

# Remove the top two divs (these are the Argon Design top bar and a spacer)
soup.body.div.extract()
soup.body.div.extract()

# Fix all the relative URLs
urlFixes = [
  ("link", "href"),
  ("script", "src"),
  ("img", "src")
]

for tag, attr in urlFixes:
    for s in soup.find_all(tag):
        if attr not in s.attrs:
            continue
        url=s[attr]
        if url.startswith("http://") or url.startswith("https://"):
            continue
        if '/' in url:
            url = url.rpartition('/')[2]
        url = "spec_av1b/{}".format(url)
        s[attr] = url

# Get rid of favicon links
#for s in soup.find_all("link"):
#    print s
#    print "**********"
#    print s.attrs
#    if s["rel"] == "icon":
#        print "Extracted"
#        s.extract()
#    print "-------------------------------------------------------------------"

syntaxTables = soup.find_all(class_="syntax")
cCodeSections = soup.find_all(class_="language-c")
funcRe = re.compile(r"(\w+)\s*\([^;]*\)\s*\{")

invalid_function_names = [
    "for",
    "while",
    "if"
]

unicode_sanitizers = {
    u"\xa0": " ",
    u"\u201c": "\"",
    u"\u201d": "\"",
    u"\u2026": "..."
}
def sanitize_unicode(s):
    for k in unicode_sanitizers:
        s = s.replace(k, unicode_sanitizers[k])
    return s

functionToSoup = {}
specFunctions = set()

for s in itertools.chain(syntaxTables, cCodeSections):
    codePointers = {}
    codeStrings = []
    ptr = 0
    for d in s.descendants:
        if not isinstance(d, NavigableString):
            continue
        st = d.string
        codeStrings.append(st)
        for x in xrange(len(st)):
            codePointers[ptr] = d
            ptr += 1

    code = "".join(codeStrings).split("\n")
    ptr = 0
    for c in code:
        m = funcRe.match(c)
        if m is not None and m.group(1) not in invalid_function_names:
            f = m.group(1)
            specFunctions.add(f)
            if f not in functionToSoup:
                soupElem = codePointers[m.start(1) + ptr]
                functionToSoup[f] = soupElem
        ptr += len(c)+1

def classSelector(c):
    return (c is None) or not ("no_count" in c.split(" "))

sections = soup.find_all(re.compile("^h[2-6]$"), class_=classSelector)

sectionTitles = [(x, str(x.string).lower()) for x in sections]

for s, st in (x for x in sectionTitles if x[1].endswith(" process")):
    f = "_".join(st.split(" ")[:-1])
    if f not in functionToSoup:
        functionToSoup[f] = s
    specFunctions.add(f)

# Text searches
search1 = soup.find_all(string=re.compile(r"is a function call"))
func1 = [x.previous_sibling if x.previous_sibling is not None else x  for x in search1]
search2 = soup.find_all(string=re.compile(r"The function \w*(\(.*\))? (?!is invoked)"))
search3 = soup.find_all(string=re.compile(r"This process is invoked when the function \w*(\s*\(.*\))?"))
search4 = soup.find_all(string=re.compile(r"When the function \w*(\s*\(.*\))? is invoked"))
search5 = soup.find_all(string=re.compile(r"The function \w*(\s*\(.*\))? returns"))

for x in func1:
    f = re.match(r"(\w*)\s*", x.string).group(1)
    if f not in functionToSoup:
        functionToSoup[f] = x
    specFunctions.add(f)

for x in search2 + search5:
    f = re.match(r"The function (\w*)\s*", x).group(1)
    if f not in functionToSoup:
        functionToSoup[f] = x
    specFunctions.add(f)

for x in search3:
    f = re.match(r"This process is invoked when the function (\w*)\s*", x).group(1)
    if f not in functionToSoup:
        functionToSoup[f] = x
    specFunctions.add(f)

for x in search4:
    f = re.match(r"When the function (\w*)\s*", x).group(1)
    if f not in functionToSoup:
        functionToSoup[f] = x
    specFunctions.add(f)


specMappings = {
    'lower_precision': 'lower_mv_precision',
    'inverse_walsh-hadamard_transform': 'inverse_wht',
    'intra_prediction': 'predict_intra',
    'inter_prediction': 'predict_inter',
    'temporal_sample': 'add_tpl_ref_mv',
    'palette_prediction': 'predict_palette',
    'symbol_decoding': 'read_symbol_inner',
    'boolean_decoding': 'read_bool',
    'sorting': 'sort_mv_stack',
    '2d_inverse_transform': 'inverse_transform_2d',

}

unusedSpecFunctions = set([
    'tile_list_entry',           # Large scale tile
    'CeilLog2',                  # Too trivial to appear in coverage report
    'large_scale_tile_decoding', # Large scale tile
    'is_transpose',              # Not part of the decoder
    'parsing',                   # Umbrella section header
    'reserved_obu',              # Too trivial to appear in coverage report
    'random_number',             # Umbrella section header
    'tile_list_obu',             # Large scale tile
    'le',                        # CABAC function, inlined
    'FloorLog2',                 # Too trivial to appear in coverage report
    'ns',                        # CABAC function, inlined
    'decoding',                  # Umbrella section header
    'NS',                        # CABAC function, inlined
    'decode_camera_tile',        # Large scale tile
    'cdf_selection',             # Umbrella section header
    'uvlc',                      # CABAC function, inlined
    'general_decoding',          # Umbrella section header
    'su',                        # CABAC function, inlined
    'project',                   # Incorporated into get_block_position in the pseudocode
    'intra_filter_type',         # Umbrella section header
    'leb128',                    # CABAC function, inlined
    'inverse_transform',         # Umbrella section header
    'call',                      # Artefact of our function extraction code
])

unlinkedPseudocodeFunctions = set([
    'ignore_obu',
    'get_past_symbol_bit',
    'get_palette',
    'qs_partition',
    'get_partition_cdf',
    'get_sm_weight',
    'read_symbol_data',
    'global_data',
])

headingTags = [ "h1", "h2", "h3", "h4", "h5", "h6" ]

for f in functionToSoup:
    s = functionToSoup[f]
    f = specMappings[f] if f in specMappings else f
    if f in unusedSpecFunctions:
        continue
    anchor = soup.new_tag("span")
    anchor["class"]="functionAnchor"
    anchor["id"]="functionAnchor_{}".format(f)
    if s.name in headingTags:
        for c in s.children:
            c.extract()
            anchor.append(c)
        s.append(anchor)
    else:
        s.wrap(anchor)

specFunctions = set(map(lambda x: specMappings[x] if x in specMappings else x, specFunctions)).difference(unusedSpecFunctions)
functions = functions.difference(unlinkedPseudocodeFunctions)

common = functions.intersection(specFunctions)
print "Common ({}):".format(len(common))
for f in common:
    print f
print

onlyInPseudocode = functions.difference(specFunctions)
print "Only in pseudocode ({}):".format(len(onlyInPseudocode))
for f in onlyInPseudocode:
    #print "{} [{}]".format(f, soup.find_all(string=re.compile(f+r"\(")))
    print f
print

onlyInSpec = specFunctions.difference(functions)
print "Only in spec ({}):".format(len(onlyInSpec))
for f in onlyInSpec:
    print f
print

with open("spec_av1b.html", "w") as f:
    f.write(soup.encode("utf-8"))
