#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''This script runs decoders over each stream/layer combination in a release.
The streams and layers to test are run in a random order (see --seed if you
need to control this ordering).

'''

import argparse
import os
import random
import subprocess
import sys
import tempfile


def parse_args():
    '''Parse arguments.'''
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-j', '--jobs', metavar='N', type=int, default=1,
                        help=('Number of jobs to run in parallel. Default 1.'))
    parser.add_argument('--joblog', metavar='L',
                        help=('If set, this is used as a job log for GNU '
                              'parallel.'))
    parser.add_argument('--ensure', action='store_true',
                        help=('The --ensure flag is passed to <testone> if '
                              'given. This is meant to tell <testone> to '
                              'create missing md5 files rather than raising '
                              'an error.'))
    parser.add_argument('--skip-existing', action='store_true',
                        help=('If --skip-existing is passed and the MD5 file '
                              'exists (not guaranteed if --ensure is given) '
                              'and so does the CSV file (if --csv is given) '
                              'then we skip running the commands. Obviously, '
                              'this is no good when checking a release, but '
                              'it\'s rather quicker if you just want to make '
                              'all missing files.'))
    parser.add_argument('--csv',
                        help=('If --csv <root-dir> is given, runs of '
                              '<testone> for layer 0 will be given the '
                              'argument --csv=<C>, where <C> is the path '
                              'below <root-dir> corresponding to the file '
                              'being checked. For example, if the file is '
                              '<release>/<set>/<streams>/<foo>.<ext> then <C> '
                              'will be <root-dir>/<set>/<foo>.csv. Runs for '
                              'layers other than zero will be passed --csv="" '
                              '(because that\'s much easier than only passing '
                              'the argument for layer zero).'))
    parser.add_argument('--limit', type=int, metavar='L',
                        help=('If this argument is given, only the first <L> '
                              'stream/layer pairs are tested. Obviously, this '
                              'is no good for testing a release, but it is '
                              'handy for regression testing.'))
    parser.add_argument('--seed', type=int, metavar='S',
                        help=('The stream/layer pairs are run in a random '
                              'order (to try to smooth out the "big" inputs '
                              'and improve remaining time estimation). With '
                              '--limit, you might want to ensure the '
                              'ordering is reproducible. If given, the '
                              'random number generator is seeded with S.'))
    parser.add_argument('--extra-args', metavar='A',
                        action='append', default=[],
                        help=('If given, these arguments will be split on '
                              'commas (like GCC) and passed to <testone>.'))
    parser.add_argument('--no-progress', action='store_true',
                        help=('If given, the checks that run under GNU '
                              'parallel won\'t print a progress bar. This '
                              'bar makes a right mess of jenkins logs.'))
    parser.add_argument('--keep-going', '-k', action='store_true',
                        help=('If this is not given, check_release_files.py '
                              'will stop after roughly 100 failures.'))
    parser.add_argument('bin_dir',
                        help=('The directory that contains binaries. This is '
                              'passed to <testone>.'))
    parser.add_argument('testone',
                        help=('The path to a script that will test each '
                              'stream/layer combination. This will be given '
                              'any extra arguments, then maybe the csv '
                              'argument (see --csv), then will be given '
                              'arguments <bin_dir>, <set-dir>, <base>, '
                              '<layer>. Here, <bin> is the argument to this '
                              'script and <layer> is the layer to test. The '
                              'other two arguments specify the stream, which '
                              'should be at <set-dir>/streams/<base>.<ext>, '
                              'where <ext> is some codec-specific stream '
                              'extension.'))
    parser.add_argument('release',
                        help=('The directory containing the release.'))

    args = parser.parse_args()

    if args.limit is not None and args.limit < 0:
        raise RuntimeError('--limit is {} but must be non-negative.'
                           .format(args.limit))

    return args


def get_set_names(release):
    '''Get the list of set names in the release'''

    # We could do this ourselves, but we do it by running print_set_names.sh to
    # make sure that the logic is just in one place.
    #
    # We explicitly ignore any sets with 'error' in the name, since error
    # streams don't have any MD5s for us to check.

    psn = os.path.join(os.path.dirname(__file__), 'print-set-names.sh')
    ret = []
    for line in subprocess.check_output([psn, release],
                                        universal_newlines=True).split():
        if 'error' not in line:
            ret.append(line)
    return ret


def get_set_reference_commands(release, set_name):
    '''Get the reference commands in the given set'''
    ret = []
    ref_cmd_dir = os.path.join(release, set_name, 'ref_cmd')
    for root, dirs, files in os.walk(ref_cmd_dir):
        for filename in files:
            if filename.endswith('.sh'):
                ret.append(os.path.join(root, filename))
    return ret


def get_reference_commands(release):
    '''Get all the reference commands in the release as a list'''
    ret = []
    for set_name in get_set_names(release):
        ret += get_set_reference_commands(release, set_name)
    return ret


def unpack_ref_cmd(ref_cmd):
    '''Unpack the path to a ref_cmd to get (set_dir, test_name, layer).'''

    # ref_cmd looks like one of:
    #
    #    /path/to/set/ref_cmd/test123.sh
    #    /path/to/set/ref_cmd/layers/5/test123_layer5.sh
    #
    # We need to know
    #
    #    set name
    #    test name
    #    layer (or zero)

    set_dir, test_name, layer = None, None, None

    parts = ref_cmd.split('/')
    good = ref_cmd[-3:] == '.sh'

    if good and (len(parts) >= 3 and 'ref_cmd' == parts[-2]):
        set_dir = '/'.join(parts[:-2])
        test_name = parts[-1][:-3]
        layer = '0'
    elif good and (len(parts) >= 5 and 'ref_cmd' == parts[-4]):
        suffix = '_layer{}.sh'.format(parts[-2])
        good = 'layers' == parts[-3] and parts[-1].endswith(suffix)
        if good:
            set_dir = '/'.join(parts[:-4])
            test_name = parts[-1][:-len(suffix)]
            layer = parts[-2]

    if not good:
        raise RuntimeError('Invalid ref_cmd format: `{}\'.'.format(ref_cmd))

    assert None not in [set_dir, test_name, layer]
    return (set_dir, test_name, layer)


def enqueue_job(crf, bin_dir, csv_root, ref_cmd, skip_existing):
    '''Write out null-terminated job data to crf'''
    set_dir, test_name, layer = unpack_ref_cmd(ref_cmd)

    can_skip = skip_existing

    csv_file = None
    if csv_root is not None:
        if layer == '0':
            csv_dir = os.path.join(csv_root, os.path.basename(set_dir))
            os.makedirs(csv_dir, exist_ok=True)

            csv_file = os.path.join(csv_dir, test_name + '.csv')
            if not os.path.exists(csv_file):
                can_skip = False
        else:
            csv_file = ''

    output = (os.path.join(set_dir, 'md5_ref', test_name + '.md5')
              if layer == '0'
              else os.path.join(set_dir, 'md5_ref',
                                'layers', layer,
                                '{}_layer{}.md5'.format(test_name, layer)))
    if not os.path.exists(output):
        can_skip = False

    if can_skip:
        return False

    if csv_file is not None:
        crf.write('--csv={}'.format(csv_file).encode('utf8') + b'\0')
    for arg in [bin_dir, set_dir, test_name, layer]:
        crf.write(arg.encode('utf8') + b'\0')
    return True


def main():
    '''Main entry point for script'''
    args = parse_args()

    ref_cmds = get_reference_commands(args.release)
    random.seed(args.seed)
    random.shuffle(ref_cmds)

    if not ref_cmds:
        # Nothing to do?
        return 0

    jobs_queued = 0
    with tempfile.NamedTemporaryFile(prefix='crf-lst-', mode='wb') as crf:
        for ref_cmd in ref_cmds:
            if enqueue_job(crf, args.bin_dir,
                           args.csv, ref_cmd, args.skip_existing):
                jobs_queued += 1

            if args.limit is not None and jobs_queued >= args.limit:
                break

        crf.flush()

        print("Testing {} stream/layer combos with {} threads."
              .format(jobs_queued, args.jobs))
        sys.stdout.flush()

        num_cols = 4 if args.csv is None else 5
        par_cmd = ['parallel', '-n{}'.format(num_cols),
                   '--null', '--max-procs', str(args.jobs),
                   '-a', crf.name, '--will-cite']
        if not args.keep_going:
            par_cmd += ['--halt', 'soon,fail=100']
        if not args.no_progress:
            par_cmd += ['--bar']

        if args.joblog is not None:
            par_cmd += ['--joblog', args.joblog]
        par_cmd.append('--')

        par_cmd.append(args.testone)
        if args.ensure:
            par_cmd.append('--ensure')
        par_cmd += args.extra_args

        try:
            subprocess.check_call(par_cmd)
        except subprocess.CalledProcessError:
            raise RuntimeError('One or more streams failed.')

    return 0


if __name__ == '__main__':
    try:
        exit(main())
    except RuntimeError as err:
        sys.stderr.write('Error: {}\n'.format(err))
        exit(1)
