#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

set -o errexit -o pipefail -o nounset

# Check that the given release has sane-looking filenames

# An allowable file name (alphanumeric plus _, ., -)
SIMPLE_NAME_RE='^[a-zA-Z0-9_.-]*$'
# A path consisting of allowable file names
SIMPLE_PATH_RE='^[a-zA-Z0-9_./-]*$'

# A set that should only have layer zero. This can be overridden with
# the --single-layer-re argument.
SINGLE_LAYER_RE='error'

product_dir="$(dirname ${BASH_SOURCE})"

error_cont () {
    echo 1>&2 "$1"
}

error () {
    error_cont "$1"
    exit 1
}

usage () {
    echo "Usage: check-release-structure.sh [--skip-md5s]"
    echo "                                  [--single-layer-re=<re>]"
    echo "                                  <rel>"
    exit $1
}

check_empty () {
    # If $1 isn't empty, print $2 to stderr and then cat $1, assuming
    # that it has null-terminated records. This replaces \0 with
    # newlines and indents everything by two spaces.
    test -e "$1" -a '!' -s "$1" || {
        echo 1>&2 "$2"
        sed <"$1" -z 's!^!\n  !g' | tr -d '\0' | head -n20 1>&2
        echo 1>&2
        return 1
    }
}

check_empty_nl () {
    # Like check_empty, but expects "$1" to be newline terminated
    # rather than null-terminated.
    test -e "$1" -a '!' -s "$1" || {
        echo 1>&2 "$2"
        sed <"$1" 's!^!  !g' | head -n20 1>&2
        echo 1>&2
        return 1
    }
}

check_diff () {
    comm -3 "$2" "$3" | tr -d '\t ' >"$1"
    check_empty_nl "$1" "$4"
}

match_up () {
    local TMP="$1"
    local streams="$2"
    local ext="$3"
    local other_dir="$4"
    local other_suff="$5"
    local layer_file="$6"
    local what_and_where="$7"
    shift 7

    if [ $# != 0 ]; then
        local keep_going="$1"
    else
        local keep_going=1
    fi

    sed <"$streams" 's!\.'"$ext"'$!'"$other_suff"'!' | sort -o "$TMP/expected"
    find "$other_dir" -maxdepth 1 -type f -printf '%f\n' | sort -o "$TMP/found"
    check_diff "$TMP/X" "$TMP/found" "$TMP/expected" \
               "Mismatches between streams and $what_and_where."

    # We expect 0 or 1 subdirectories. If there is a subdirectory, it
    # should be called layers.
    find "$other_dir" \
         -mindepth 1 -maxdepth 1 -type d -printf '%f\n' >"$TMP/found"

    # If keep_going is zero, we shouldn't have any directories (we're
    # already in some layer)
    if [ x"$keep_going" == x0 ]; then
        check_empty_nl "$TMP/found" \
                       "Unexpected subdirectory in $other_dir."
        return 0
    fi

    set +e
    grep -v '^layers$' <"$TMP/found" >"$TMP/X"
    set -e
    check_empty_nl "$TMP/X" "Unexpected subdirectory in $other_dir."

    if [ ! -s "$TMP/found" ]; then
        local layers=""
        local have_layers=0
        echo -n "" >"$TMP/found"
    else
        local layers="$(ls "$other_dir/layers")"
        local have_layers=1
        echo "$layers" | tr ' ' '\n' | sort -no "$TMP/found"
    fi

    if [ -e "$layer_file" ]; then
        # If layer_file exists already, it's a possibly empty list of
        # the layers for some other subdir or set. In which case, we
        # should check that it matches $layers.
        diff -q "$layer_file" "$TMP/found" >/dev/null || {
            diff -U3 "$layer_file" "$TMP/found" || true
            error "layers at $other_dir don't match some other set/subdir"
        }
    elif [ $have_layers = 0 ]; then
        # If layer_file doesn't exist and have_layers is zero, there's
        # nothing clever to do. Just copy $TMP/found to layer_file.
        cp "$TMP/found" "$layer_file"
    else
        # If layer_file doesn't exist and we have some layers, we need
        # to do a sanity check on them. We expect them to look like 1
        # 2 3 ... N for some N. You can check this by sorting and
        # comparing with seq 1 N.
        echo "$layers" | tr ' ' '\n' | sort -no "$layer_file"
        local num_layers="$(wc -l <"$layer_file")"
        seq 1 "$num_layers" >"$TMP/expected"
        diff -q "$TMP/expected" "$TMP/found" >/dev/null || {
            error "Layers at $other_dir aren't of form 1 .. N"
        }
    fi

    for layer in $layers; do
        layer_dir="$other_dir/layers/$layer"
        match_up "$TMP" "$streams" "$ext" "$layer_dir" \
                 "_layer${layer}${other_suff}" \
                 "$layer_file" \
                 "$what_and_where, for layer $layer" \
                 0
    done
}

check_md5s_unique () {
    local expected_num="$1"
    local path="$2"

    local num_md5s="$(find "$path" -maxdepth 1 -type f | xargs cat | sort -u | wc -l)"
    if [ $num_md5s != $expected_num ]; then
        echo 1>&2 "Expected $expected_num unique MD5s at $path; found $num_md5s"
        return 1
    fi

    return 0
}

OPTS=h
LONGOPTS=help,skip-md5s,single-layer-re:

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

skip_md5s=0

while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        --skip-md5s)
            skip_md5s=1
            shift
            ;;
        --single-layer-re)
            SINGLE_LAYER_RE="error|$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# There should be exactly 1 positional argument left (the release
# directory).
if [ $# != 1 ]; then
    usage 1
fi

# Make a temporary directory that we'll use to hold stuff we're
# working on.
TMP="$(mktemp -d)"
trap "{ rm -rf $TMP; }" EXIT

# Unfortunately, our filenames are full of spaces (eugh!). We can work
# around this with -print0 for find and -z for sed and sort.
#
# Firstly, check there are no files without an extension, except those
# in a 'bin/linux' directory
set +e
find "$1" \
     -type d -path '*/bin/linux' -prune -o \
     -type f -not -name '*.*' -print0 >"$TMP/X"
set -e

check_empty "$TMP/X" "Release contains files without extensions:"

# There should also be no hidden files
find "$1" -type f -name '.*' -print0 >"$TMP/X"
check_empty "$TMP/X" "Release contains hidden files"

# All files at top-level should be PDFs, XML or Excel files, or shell
# scripts.
#
# Check this by grabbing the set of extensions (we know everything has
# one because of the previous check) and then using grep -v to throw
# away the extensions we like.
set +e
find "$1" -maxdepth 1 -type f -print0 | \
    sed -zE 's!.*(\.[^.]*)!\1!' | \
    sort -zu | grep -zvE '^\.(:?pdf|xml|xlsx|sh|lst)$' >"$TMP/X"
set -e
check_empty "$TMP/X" \
            "Release has files at top-level with extensions we don't like"

# Get the set names in the release. The print-set-names.sh script will
# fail with an error if any of the sets don't match SIMPLE_NAME_RE, so
# we can plonk the results in a bash variable and will be able to
# safely iterate over them later.
set_names="$("${product_dir}/print-set-names.sh" "$1")"

# We may have bonkers names at top-level, but shouldn't have anything
# dodgy below that. This means no embedded spaces, which makes
# everything below much easier.
set +e
find "$1" -mindepth 2 -print0 | grep -zvE "$SIMPLE_PATH_RE" >"$TMP/X"
set -e
check_empty "$TMP/X" \
            "Release has files below top-level with dubious names"

# codec_ext is the codec extension we expect to see (kept alive across
# sets)
unset codec_ext

# As the set names are all well behaved, we can just loop over them in
# the shell.
for set_name in $set_names; do
    set_path="$1/$set_name"

    # Error streams and sets that match SINGLE_LAYER_RE have slightly
    # different expected layouts.
    is_error=0
    single_layer=0
    if echo "$set_name" | grep -Eq "$SINGLE_LAYER_RE"; then
        single_layer=1
    fi
    if echo "$set_name" | grep -q "error"; then
        is_error=1
    fi

    # Check that we have the expected directories at top-level in the
    # set. Normally, we require ref_cmd, streams and md5_ref and allow
    # other md5_FOO directories.
    #
    # When it's an error set, we just expect streams and ref_cmd and
    # don't allow anything else.

    if [ $is_error = 0 ]; then
        req_sets="streams ref_cmd md5_ref"
        allowed_sets='^streams|ref_cmd|(:?md5_.*)$'
    else
        req_sets="streams ref_cmd"
        allowed_sets='^streams|ref_cmd$'
    fi

    find "$set_path" -mindepth 1 -maxdepth 1 -type d \
         -printf '%f\n' >"$TMP/dirs"

    for d in $req_sets; do
        grep -q '^'$d'$' "$TMP/dirs" || \
            error "Set $set_path doesn't have $d directory"
    done

    set +e
    grep -Ev "${allowed_sets}" <"$TMP/dirs" >"$TMP/X"
    set -e
    check_empty_nl "$TMP/X" \
                   "Set $set_name has unexpected top-level directories."

    # Normally, we make sure that the list of MD5 directory names
    # matches those in the last set we looked at. We don't do this for
    # sets of error streams (which don't have MD5s at all).
    if [ $is_error = 0 ]; then
        grep md5 <"$TMP/dirs" | sort -o "$TMP/md5_dirs.new"
        if [ -e "$TMP/md5_dirs" ]; then
            diff -q "$TMP/md5_dirs" "$TMP/md5_dirs.new" >/dev/null || {
                error "Set $set_name has different md5 dirs from the previous set."
            }
        fi
        mv "$TMP/md5_dirs.new" "$TMP/md5_dirs"
    fi

    # All the files at top-level in the set should be called
    # FOO_list.txt for some FOO.
    find "$set_path" -maxdepth 1 -type f -not -name "*_list.txt" \
         -print0 >"$TMP/X"
    check_empty "$TMP/X" \
                "Set $set_name has top-level list file with bad name."

    # Grab all the list files.
    find "$set_path" -maxdepth 1 -type f -printf '%f\n' >"$TMP/lists"

    # For each list file, the lines should look something like
    # "test123.obu". Check that there are no weird characters. Note
    # that we don't need to do any clever escaping here for the name
    # of the list because we've already checked that the filenames are
    # nice.
    for list in $(cat "$TMP/lists"); do

        list_path="$set_path/$list"

        # Check that list_path doesn't have any carriage return
        # characters (they just make stuff more painful later on)
        if grep -qP <"$list_path" '\x0d'; then
            error "List at $list_path contains carriage returns."
        fi

        set +e
        grep -v <"$list_path" "$SIMPLE_NAME_RE" >"$TMP/X"
        set -e
        check_empty_nl "$TMP/X" \
                       "List $set_name/$list has some dubious names."

        # Now we can safely iterate over the lines in the list and
        # make sure that each stream they mention exists
        bad=0
        for stream in $(cat "$list_path"); do
            stream_path="$set_path/streams/$stream"
            test -f "$stream_path" || {
                error_cont "No such stream: $stream_path (mentioned in $list_path)"
                bad=1
            }
        done
        if [ $bad = 1 ]; then
            error "There were streams in $list_path that don't exist."
        fi

        # We should also check there are no duplicates in the list
        sort "$list_path" | uniq -d >"$TMP/X"
        check_empty_nl "$TMP/X" \
                       "Repeated lines in $list_path"
    done

    # There should be an all_list.txt
    list_path="$set_path/all_list.txt"
    test -f "$list_path" || {
        error "Set $set_name does not have all_list.txt"
    }

    # We've checked that the listed streams all exist. Now let's
    # check that every stream that exists is listed in
    # all_list.txt. Note this implies all other lists are subsets
    # of all_list.txt.
    ls "$set_path/streams" | sort -o "$TMP/streams"
    sort "$list_path" -o "$TMP/listed-streams"
    check_diff "$TMP/X" "$TMP/streams" "$TMP/listed-streams" \
               "Mismatches between all_list.txt and streams for $set_name."

    # The file lists look good. Check that all the streams have the
    # same extension. Note that we've already checked that the
    # filenames are of the form A.B
    awk <"$TMP/streams" 'BEGIN {FS="."} {print $NF}' | \
        sort -u -o "$TMP/extensions"
    if [ "x$(wc -l <"$TMP/extensions")" != x1 ]; then
        error "Didn't find exactly one extension in $set_path/streams."
    fi

    nce="$(cat "$TMP/extensions")"
    if [ ! -z ${codec_ext+x} ]; then
        # codec_ext was set by the previous loop iteration. Check that
        # it matches the extension in this set.
        if [ x"$nce" != x"$codec_ext" ]; then
            error "Mismatch in extensions. $set_name has $nce, not $codec_ext"
        fi
    fi
    codec_ext="$nce"
    unset nce

    num_streams="$(wc -l <$TMP/streams)"

    # Check we have the expected list of reference commands, unless
    # this is a single layer set. To special-case this, we point the
    # $layer_file in match_up at a dummy file that we create with an
    # empty list.
    set_layers="$TMP/layers"
    if [ $single_layer = 1 ]; then
        set_layers="$TMP/no-layers"
        echo -n "" >"$set_layers"
    fi

    match_up "$TMP" "$TMP/streams" "$codec_ext" "$set_path/ref_cmd" \
             .sh "$set_layers" "ref commands at $set_path"

    # If we had --skip-md5s, we shouldn't check for MD5 files right
    # now. Similarly, if this is a set of error streams, there will
    # never be any MD5s.
    if [ ${skip_md5s}${is_error} != 00 ]; then
        continue
    fi

    bad_md5=0
    for md in $(cat "$TMP/md5_dirs"); do
        # Do the same check for each MD5 directory. As above, single
        # layer sets only expect layer 0.
        match_up "$TMP" "$TMP/streams" "$codec_ext" "$set_path/$md" \
                 .md5 "$set_layers" "md5 checksums at $set_path/$md"

        # We also want to make sure that no two streams have the same
        # MD5 at layer zero (that would probably imply that they are
        # basically the same stream).
        #
        # We don't do the check at higher layers, because we might be
        # throwing away all the frames from more than one stream at
        # those operating points, meaning nothing actually comes out.
        check_md5s_unique "$num_streams" "$set_path/$md" || { bad_md5=1; }
    done
    if [ bad_md5 = 1 ]; then
        error "Some sets didn't have the expected number of unique checksums"
    fi
done

echo "Release looks sane to me"
