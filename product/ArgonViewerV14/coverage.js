/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

coverageReady=0;
equationNameRegex=/^(?:[A-Z]|[0-9]+)\.(?:[0-9]+\.)*[0-9]+/;
sectNameRegex=/_((?:[A-Z]|[0-9]+)_(?:[0-9]+_)*[0-9]+)$/;

var sidePanels= {
  'summary': {
    panelId: "#summaryDiv",
    buttonId: "#toggleSummaryButton"
  },
  'spec': {
    panelId: "#specFrame",
    buttonId: "#toggleSpecButton"
  },
  'pipelineDiagram': {
    panelId: "#pipelineDiagramDiv",
    buttonId: "#togglePipelineDiagramButton",
    onPanelShown: function() { MathJax.Hub.Typeset(document.getElementById("pipelineDiagramDiv")); }
  },
  'crossPoints': {
    panelId: "#crossPointsDiv",
    buttonId: "#toggleCrossPointsButton",
    onPanelShown: function() { crosscover.setCrossCoverType(0); }
  },
  'crossValuePoints': {
    panelId: "#crossValuePointsDiv",
    buttonId: "#toggleCrossValuePointsButton",
    onPanelShown: function() { crosscover.setCrossCoverType(1); }
  }
};

function sidePanelCallback(open, target, complete) {
    if (open) {
        return function() {
            $(target.panelId).fadeIn(400,function() {
                    if (complete!=null) complete();
                    if (target.onPanelShown!=null) target.onPanelShown();
                    $(target.panelId).height($(window).height()-$(target.panelId).offset().top-$("#footerDiv").height());
            });
        }
    }
    else {
        return function() {
            $(target.buttonId).animate({ "opacity": "0.4" });
            if (complete!=null) complete();
        }
    }
}


function showSidePanelLoop(keys, idx, showId, complete) {
  var completeFunction;
  if (idx+1 >= keys.length+1 || (showId==null && idx+1 == keys.length)) {
    completeFunction = function() {
        if (complete!=null) complete();
      };
  }
  else {
    completeFunction = function() {
        showSidePanelLoop(keys, idx+1, showId, complete);
      }
  }
  if (idx == keys.length) {
    var target=sidePanels[showId];
    $(target.buttonId).animate({ "opacity": "1.0" });
    sidePanelCallback(true, target, completeFunction)();
  }
  else {
    if (keys[idx] != showId) {
    var target=sidePanels[keys[idx]];
      $(target.panelId).fadeOut(400, sidePanelCallback(false, target, completeFunction));
    } else {
      completeFunction();
    }
  }
}

function showSidePanel(id, show, complete) {
    if (show==$(sidePanels[id].panelId).is(":visible")) {
        if (complete) complete();
        return;
    }
    keys = Object.keys(sidePanels);
    showSidePanelLoop(keys, 0, show?id:null, complete);
}

function hideSidePanels(complete) {
    keys = Object.keys(sidePanels);
    var shownKey = null;
    for (var i=0; i<keys.length; i++) {
      var key = keys[i];
      if ($(sidePanels[key].panelId).is(":visible")) {
        shownKey = key;
        break;
      }
    }
    showSidePanelLoop(keys, 0, null, function () { if (complete != null) complete(shownKey); });
}

dehighlightFunctions=new Array();
highlightedElements=new Array();

function dehighlight(newDehighlightFunctions) {
    outer:
    for (var dhf in dehighlightFunctions) {
        for (var ndhf in newDehighlightFunctions) {
            if (newDehighlightFunctions[ndhf]['elem'][0]==dehighlightFunctions[dhf]['elem'][0]) {
                continue outer;
            }
        }
        dehighlightFunctions[dhf]['func']();
    }
    dehighlightFunctions=newDehighlightFunctions;
}

function highlightSection(sectionName, clickedTree, clickedElem) {
    var newDehighlightFunctions=new Array();
    var spanElem=$("#sectionAnchorSpan_"+sectionName);
    if (spanElem.length > 0) {
        var listElem=(clickedTree && clickedElem!=null)?clickedElem:spanElem.parent();
        if (!clickedTree) {
            for (var pElem=listElem; pElem.length > 0; pElem=pElem.parent()) {
                if (pElem.prop("tagName")=="LI" && pElem.hasClass("collapseClosed")) {
                    pElem.toggleClass("collapseOpen");
                    pElem.toggleClass("collapseClosed");
                    pElem.children("ul").show();
                }
            }
            var scrollPos=$("#noscrollDiv").scrollTop()+listElem.offset().top-$("#topDiv").height()-50;
            $("#noscrollDiv").scrollTop(scrollPos);
        }
        listElem.css("background","#ffe8cc");
        newDehighlightFunctions.push({'func':function() { listElem.animate({"backgroundColor": "#fff"}, 400, "swing", function() { listElem.css("background",""); }); }, 'elem':listElem});
    }
    var ifr=$("#specFrame");
    var anchor=ifr.contents().find("#sectionAnchor_"+sectionName);
    if (anchor.length > 0) {
        showSidePanel("spec", true, function() {
            anchor.css("background","#ffe8cc");
            newDehighlightFunctions.push({'func':function() { anchor.animate({"backgroundColor": "#fff"}, 400, "swing", function() { anchor.css("background",""); }); }, 'elem':anchor});
            if (clickedTree) ifr.contents().scrollTop(anchor.offset().top-ifr.height()/4);
        });
    }
    dehighlight(newDehighlightFunctions);
}

function highlightRangeEquation(rangeEquationName) {
    var rangeEquationLi=$("li[data-rangeEquation="+rangeEquationName+"]");
    if (rangeEquationLi.length<1) return;
    highlightEquation(rangeEquationLi.attr("data-equationNumber"), false, null, rangeEquationLi);
}

function highlightEquation(equationName, clickedTree, clickedElem, baseElem) {
    var newDehighlightFunctions=new Array();
    var spanElem;
    if (baseElem==null) spanElem=$("#equationAnchorSpan_"+equationName);
    else spanElem=baseElem.find("#equationAnchorSpan_"+equationName);
    if (spanElem.length > 0) {
        var listElem;
        if (clickedTree && clickedElem!=null) {
            listElem=clickedElem;
        }
        else {
            listElem=spanElem.parent();
        }
        if (!clickedTree) {
            for (var pElem=listElem; pElem.length > 0; pElem=pElem.parent()) {
                if (pElem.prop("tagName")=="LI" && pElem.hasClass("collapseClosed")) {
                    pElem.toggleClass("collapseOpen");
                    pElem.toggleClass("collapseClosed");
                    pElem.children("ul").show();
                }
            }
            var scrollPosition=listElem.offset().top+$("#noscrollDiv").scrollTop()-$("#topDiv").height()-50;
            $("#noscrollDiv").scrollTop(scrollPosition);
        }
        listElem.css("background","#ffe8cc");
        newDehighlightFunctions.push({'func':function() { listElem.animate({"backgroundColor": "#fff"}, 400, "swing", function() { listElem.css("background",""); }); }, 'elem':listElem});
    }
    if (clickedTree) {
        var ifr=$("#specFrame");
        var anchor=ifr.contents().find("#equationAnchor_"+equationName);
        if (anchor.length > 0) {
            showSidePanel("spec", true, function() {
                ifr.contents().scrollTop(anchor.offset().top-ifr.height()/4);
                anchor.parent().css("background","#ffe8cc");
                newDehighlightFunctions.push({'func':function() { anchor.parent().animate({"backgroundColor": "#fff"}, 400, "swing", function() { anchor.css("background",""); }); }, 'elem':anchor});
            });
        }
    }
    dehighlight(newDehighlightFunctions);
}

function highlightFunction(functionName, clickedTree, clickedElem) {
    var newDehighlightFunctions=new Array();
    var spanElem=$("#functionAnchorSpan_"+functionName);
    if (spanElem.length > 0) {
        var listElem;
        if (clickedTree && clickedElem!=null) {
            listElem=clickedElem;
        }
        else {
            listElem=spanElem.parent();
        }
        if (!clickedTree) {
            for (var pElem=listElem; pElem.length > 0; pElem=pElem.parent()) {
                if (pElem.prop("tagName")=="LI" && pElem.hasClass("collapseClosed")) {
                    pElem.toggleClass("collapseOpen");
                    pElem.toggleClass("collapseClosed");
                    pElem.children("ul").show();
                }
            }
            var scrollPosition=listElem.offset().top+$("#noscrollDiv").scrollTop()-$("#topDiv").height()-50;
            $("#noscrollDiv").scrollTop(scrollPosition);
        }
        listElem.css("background","#ffe8cc");
        newDehighlightFunctions.push({'func':function() { listElem.animate({"backgroundColor": "#fff"}, 400, "swing", function() { listElem.css("background",""); }); }, 'elem':listElem});
    }
    if (clickedTree) {
        var ifr=$("#specFrame");
        var anchor=ifr.contents().find("#functionAnchor_"+functionName);
        if (anchor.length > 0) {
            showSidePanel("spec", true, function() {
                ifr.contents().scrollTop(anchor.offset().top-ifr.height()/4);
                anchor.parent().css("background","#ffe8cc");
                newDehighlightFunctions.push({'func':function() { anchor.parent().animate({"backgroundColor": "#fff"}, 400, "swing", function() { anchor.css("background",""); }); }, 'elem':anchor});
            });
        }
    }
    dehighlight(newDehighlightFunctions);
}

function setupSpecClickHandlers() {
    $("#specFrame").contents().find(".sectionAnchor").each(function(index) {
        element=$(this);
        var anchorID=element.attr("id");
        var sectionName=anchorID.substring(anchorID.indexOf("_")+1);
        if ($("#sectionAnchorSpan_"+sectionName).length==0) return;
        var headingRegex=/H[1-6]/;
        for (var helem=element.parent(); helem.length>0; helem=helem.parent()) {
            if (headingRegex.exec(helem.prop("tagName"))!=null || helem.prop("className").indexOf("Annex")!=-1) {
                helem.click(function() {
                    highlightSection(sectionName, false, null);
                });
                helem.css("color","blue");
                helem.css("text-decoration", "underline");
                helem.css("cursor","pointer");
                break;
            }
        }
    });
    $("#specFrame").contents().find(".equationAnchor").each(function(index) {
        var element=$(this);
        var anchorID=element.attr("id");
        var equationID=anchorID.substring(anchorID.indexOf("_")+1);
        if ($("#equationAnchorSpan_"+equationID).length==0) return;
        element.click(function () {
            highlightEquation(equationID, false, null, null);
        });
        element.css("color","blue");
        element.css("text-decoration", "underline");
        element.css("cursor","pointer");

    });
    $("#specFrame").contents().find(".functionAnchor").each(function(index) {
        var element=$(this);
        var anchorID=element.attr("id");
        var functionID=anchorID.substring(anchorID.indexOf("_")+1);
        if ($("#functionAnchorSpan_"+functionID).length==0) return;
        element.click(function () {
            highlightFunction(functionID, false, null);
        });
        element.css("color","blue");
        element.css("text-decoration", "underline");
        element.css("cursor","pointer");

    });
}

var pleaseWaitNest=0;
var pleaseWaitNestFullscreen=0;
function pleaseWait(on, fullscreen) {
  if (on) {
    if (fullscreen) {
      if (++pleaseWaitNestFullscreen<2) loadingDots(0);
      $("#pleaseWaitDiv").show();
    }
    else {
      pleaseWaitNest++;
      $("#pleaseWait").show();
    }
    $("body").css("cursor","progress");
  }
  else {
    if (fullscreen) {
      if (--pleaseWaitNestFullscreen<=0) {
          pleaseWaitNestFullscreen=0;
          $("#pleaseWaitDiv").fadeOut();
      }
    }
    else {
      if (--pleaseWaitNest<=0) {
          pleaseWaitNest=0;
          $("#pleaseWait").hide();
      }
    }
    if (pleaseWaitNest==0 && pleaseWaitNestFullscreen==0) $("body").css("cursor","auto");
  }
}

function loadingDots(loadingDotState) {
    if (!($("#pleaseWaitDiv").is(":visible"))) {
        $("#loadingDot1").css("color","");
        $("#loadingDot2").css("color","");
        $("#loadingDot3").css("color","");
    }
    else {
        $("#loadingDot1").css("color",(loadingDotState==3 || loadingDotState==4)?"white":"");
        $("#loadingDot2").css("color",(loadingDotState==0 || loadingDotState==4)?"white":"");
        $("#loadingDot3").css("color",(loadingDotState==0 || loadingDotState==1)?"white":"");

        setTimeout(function() {
            loadingDots((loadingDotState+1)%5);
        },200);
    }
}

$(document).ready(function() {
    $("#specFrame").load(function() {
        pleaseWait(true,true);
        setupSpecClickHandlers();
        scrollbarWidth=$("#noscrollDiv").width()-$("#innerScrollDiv").width();
        $("#topDiv").css("right",scrollbarWidth+"px");
        if (navigator.userAgent.indexOf("MSIE")!=-1) {
            $("#mainBranchList ul ul").css("background-position", "0% 0%");
        }
        $(".collapseClosed").click(function(event) {
            $(this).toggleClass("collapseOpen");
            $(this).toggleClass("collapseClosed");
            $(this).children("ul").slideToggle();
            event.stopPropagation();
        });
        $("ul").click(function(event) {
            event.stopPropagation();
        });
        $("#expandAllButton").click(function() {
            pleaseWait(true,false);
            setTimeout(function() {
                $(".subList").show();
                $(".collapseClosed").addClass("collapseOpen");
                $(".collapseClosed").removeClass("collapseClosed");
                pleaseWait(false,false);
            },100);
        });
        $("#expandIncompleteButton").click(function() {
            pleaseWait(true,false);
            setTimeout(function() {
                $(".subList").hide();
                $(".collapseOpen").addClass("collapseClosed");
                $(".collapseOpen").removeClass("collapseOpen");
                $("li.categoryVisible").each(function(index) {
                    if ($(this).data("coveredTestCases")!=$(this).data("totalTestCases")&&$(this).children("ul").length>0) {
                        $(this).children("ul").show();
                        $(this).addClass("collapseOpen");
                        $(this).removeClass("collapseClosed");
                    }
                });
                pleaseWait(false,false);
            },100);
        });
        $("#collapseAllButton").click(function() {
            pleaseWait(true,false);
            setTimeout(function() {
                $(".subList").hide();
                $(".collapseOpen").addClass("collapseClosed");
                $(".collapseOpen").removeClass("collapseOpen");
                $("#noscrollDiv").scrollTop(0);
                pleaseWait(false,false);
            },100);
        });
        $("#printButton").click(function() {
            doPrint();
        });
        $(".coverageReadout").click(function(event) {
            var ifr=$("#specFrame");
            for (var elem=$(this).parent(); elem.length>0; elem=elem.parent()) {
                var testName=elem.attr("data-testName");
                if (testName!=null) {
                    var sectNameMatch=sectNameRegex.exec(testName);
                    if (sectNameMatch!=null) {
                        highlightSection(sectNameMatch[1], true, elem);
                        event.stopPropagation();
                        return;
                    }
                    var equationNameMatch=equationNameRegex.exec(testName);
                    if (equationNameMatch!=null) {
                        highlightEquation(equationNameMatch[0].replace(".","_"), true, elem, null);
                        event.stopPropagation();
                        return;
                    }
                }
                var funcName=elem.attr("data-functionName");
                if (funcName!=null && ifr.contents().find("#functionAnchor_"+funcName).length) {
                    highlightFunction(funcName, true, elem);
                    event.stopPropagation();
                    return;
                }
            }
        });
        $("#toggleSummaryButton").click(function() {
            showSidePanel("summary", !$("#summaryDiv").is(":visible"));
        });
        $("#toggleSpecButton").click(function() {
            showSidePanel("spec", !$("#specFrame").is(":visible"));
        });
        $("#togglePipelineDiagramButton").click(function() {
            showSidePanel("pipelineDiagram", !$("#pipelineDiagramDiv").is(":visible"));
        });
        $("#toggleCrossPointsButton").click(function() {
            showSidePanel("crossPoints", !$("#crossPointsDiv").is(":visible"));
        });
        $("#toggleCrossValuePointsButton").click(function() {
            showSidePanel("crossValuePoints", !$("#crossValuePointsDiv").is(":visible"));
        });
        $("#pipelineDiagramDiv").resize(function() {
          MathJax.Hub.Typeset(document.getElementById("pipelineDiagramDiv"));
        });
        $(window).resize(function() {
            for (var key in sidePanels) {
                var target=sidePanels[key];
                if($(target.panelId).is(":visible")) {
                    $(target.panelId).height($(window).height()-$(target.panelId).offset().top-$("#footerDiv").height());
                }
            }
        });

        $("#specFrame").hide();
        $("#pipelineDiagramDiv").hide();
        $("#crossPointsDiv").hide();
        initPage();
        initPipelineDiagram();
        $("html").keydown(function(event) {
          if (event.which=="C".charCodeAt(0) && event.shiftKey && event.ctrlKey) {
              event.preventDefault();
              showSidePanel("crossPoints", !$("#crossPointsDiv").is(":visible"));
          }
        });
        pleaseWait(false,true);
    });
});

function setColor(jqElem,color) {
    if (color=="red") {
        jqElem.addClass("red");
        jqElem.removeClass("orange");
        jqElem.removeClass("green");
    }
    else if (color=="orange") {
        jqElem.removeClass("red");
        jqElem.addClass("orange");
        jqElem.removeClass("green");
    }
    else if (color=="green") {
        jqElem.removeClass("red");
        jqElem.removeClass("orange");
        jqElem.addClass("green");
    }
}

function updateCoverage() {
    var visibleTestGroups=$("li.testGroup");
    var visibleTestPoints=$("li.testPoint");
    visibleTestGroups.data("totalTestCases",0);
    visibleTestGroups.data("totalInvisibleTestCases",0);
    visibleTestGroups.data("coveredTestCases",0);
    visibleTestPoints.each(function(index) {
        var testCases=parseInt($(this).attr("data-testCases"));
        var testCasesCovered=parseInt($(this).attr("data-testCasesCovered"));
        var isVisible=$(this).hasClass("categoryVisible");
        if (isVisible) {
          $(this).data("totalTestCases",testCases);
          $(this).data("coveredTestCases",testCasesCovered);
        }
        $(this).parents("li.testGroup").each(function(index2) {
            if (isVisible) {
              $(this).data("totalTestCases",$(this).data("totalTestCases")+testCases);
              $(this).data("coveredTestCases",$(this).data("coveredTestCases")+testCasesCovered);
            }
            else {
              $(this).data("totalInvisibleTestCases",$(this).data("totalInvisibleTestCases")+testCases);
            }
        });
    });
    visibleTestGroups.each(function(index) {
        var coveredTestCases=$(this).data("coveredTestCases");
        var totalTestCases=$(this).data("totalTestCases");
        var coverageReadoutSpan=$(this).children("span.coverageReadout");
        coverageReadoutSpan.children("span.actualCoverageValue").html("<span>"+coveredTestCases+"</span>");
        coverageReadoutSpan.children("span.totalCoverageValue").html("<span>"+totalTestCases+"</span>");
        if (totalTestCases==0) {
            $(this).removeClass("categoryVisible");
            coverageReadoutSpan.children("span.totalCoverageValue").html("<span>"+($(this).data("totalInvisibleTestCases"))+"</span>");
            coverageReadoutSpan.children("span.coveragePercentage").html("<span>0</span>");
        }
        else {
            coverageReadoutSpan.children("span.coveragePercentage").html("<span>"+(Math.floor((1000*coveredTestCases)/totalTestCases)/10).toFixed(1)+"</span>");
        }
        if (coveredTestCases==0) setColor(coverageReadoutSpan,"red");
        else if (coveredTestCases==totalTestCases) setColor(coverageReadoutSpan,"green");
        else setColor(coverageReadoutSpan,"orange");
    });
    var eqnLiDict={};
    $("li.categoryVisible[data-equationNumber]").each(function (index) {
        eqnLiDict[$(this).attr("data-equationNumber")]=$(this);
    });
    $("#specFrame").contents().find(".equationAnchor").each(function (index) {
        var equationNumber=$(this).attr("id").substring("equationAnchor_".length);
        var color;
        if (!(equationNumber in eqnLiDict)) color="white";
        else {
            var eqnLi=eqnLiDict[equationNumber];
            var coveredTestCases=eqnLi.data("coveredTestCases");
            var totalTestCases=eqnLi.data("totalTestCases");
            if (coveredTestCases==0) color="red";
            else if (coveredTestCases==totalTestCases) color="green";
            else color="darkorange";
        }
        for (var topElem=$(this).parent(); topElem.length!=0; topElem=topElem.parent()) {
            if (topElem.hasClass("Equation")) {
                topElem.css({ "border-left": "solid "+color+" 5px", "padding-left": "5px" });
                break;
            }
        }
    });
    $("#specFrame").contents().find(".functionAnchor").each(function (index) {
        var functionName=$(this).attr("id").substring("functionAnchor_".length);
        var color;
        var funcSpan=$("#functionAnchorSpan_"+functionName);
        if (!funcSpan.length) color="white";
        else {
            var funcLi=funcSpan.closest("li");
            var coveredTestCases=funcLi.data("coveredTestCases");
            var totalTestCases=funcLi.data("totalTestCases");
            if (coveredTestCases==0) color="red";
            else if (coveredTestCases==totalTestCases) color="green";
            else color="darkorange";
        }
        $(this).css({ "border-left": "solid "+color+" 5px", "padding-left": "5px" });
    });
}

function getRangeVisibilityType() {
    return $("#visibleCategory").find(":selected").attr("data-rangeVisibilityType");
}

function updateVisibleCategory(startup) {
    pleaseWait(true,startup);
    setTimeout(function() {
        if (!isSummary) {
          //Set up the categoryVisible classes correctly according to the new visibility
          //First set up a little snippet of JS code representing the state of the category booleans
          var invisibleCategories=$("#visibleCategory").find(":selected").attr("data-invisibleCategories").split(",")
          for (var i=0; i<invisibleCategories.length; ++i) {
            invisibleCategories[i]=$.trim(invisibleCategories[i]);
          }
          var categorySetupCode="";
          for (var i=0; i<allVisibilityCategories.length; ++i) {
            categorySetupCode+=allVisibilityCategories[i]+((invisibleCategories.indexOf(allVisibilityCategories[i])>=0)?"=0;":"=1;");
          }
          //Now loop over all conditionally visible elements evaluating their visibility expressions
          var allTestElements=$("li.testPoint").add("li.testGroup");
          allTestElements.each(function (index) {
            //Get the visibility condition
            var visibilityCondition=$(this).attr("data-visibilityCondition");
            //If no visibility condition, look up the ancestry tree
            if (!visibilityCondition) {
              var nearestAncestor=$(this).parent().closest("li.testGroup[data-visibilityCondition]");
              if (nearestAncestor.length==0) visibilityCondition="1";
              else visibilityCondition=nearestAncestor.attr("data-visibilityCondition");
            }
            var isVisible;
            //Deal with simple cases quickly, as eval is relatively expensive
            if (visibilityCondition=="1") isVisible=1;
            else if (visibilityCondition=="0") isVisible=0;
            else {
              // Otherwise eval the supplied expression. We wrap the
              // EVAL in a try/catch block so that we don't die if
              // there's a bad variable name in visibilityCondition.
              try {
                isVisible = eval(categorySetupCode+" ("+visibilityCondition+");");
              } catch (e) {
                isVisible = 1;
              }
            }

            //Switch the categoryVisible class if necessary
            var isAlreadyVisible=$(this).hasClass("categoryVisible");
            if (isAlreadyVisible!=isVisible) $(this).toggleClass("categoryVisible");
          });
          //Now get the list of invisible categories
          var visibilityType=getRangeVisibilityType();
          $("#pipelineDiagramDiv").find(".rangecol:not(."+visibilityType+"_col)").css("display","none");
          $("#pipelineDiagramDiv").find("."+visibilityType+"_col").css("display","table-cell");

          updateCoverage();
          updatePipelineCoverage();
          coverageReady|=1;
        }
        summary.updateSummary();
        pleaseWait(false,startup);
    },100);
}

function updatePipelineCoverage() {
  $("#pipelineDiagramDiv td.rangecol").each(function(index) {
    if (!$(this).hasClass("extension")) {
      $(this).removeClass("invisible covered supercovered");
      $(this).addClass("uncovered");
    }
  });

  $("li[data-rangeEquation]").each(function (index) {
      var equationNumber=$(this).attr("data-rangeEquation");
      if (!(equationNumber in eqnMinSpanDict)) return;
      //First set up the equation number in the link
      if (!useEqnNames)
          $("#pipelineDiagramDiv").find("a.rangeEqnLink"+equationNumber).text($(this).attr("data-equationNumber").replace("_","-"));
      //We now need to drill down to find the element for us to extract the values from
      var valueExtractionElement=$(this);
      while (true) {
        var ulChildren=valueExtractionElement.children("ul:last-child");
        if (ulChildren.length==0) break;
        valueExtractionElement=ulChildren.children("li:last-child");
      }
      //Now we want to track back past anything clipped
      while (true) {
        var veeTestName=valueExtractionElement.attr("data-testName");
        if (veeTestName==null ||
            veeTestName.indexOf("Clip3")==0 ||
            veeTestName.indexOf("ROUND_POWER_OF_TWO")==0 ||
            veeTestName.indexOf("ROUND_POWER_OF_TWO_SATCHECK")==0 ||
            veeTestName.indexOf("clip_pixel")==0) {
              if (valueExtractionElement.prev().length == 0) {
                break;
              }
              valueExtractionElement=valueExtractionElement.prev();
        } else break;
      }
      var eqnMinSpans=eqnMinSpanDict[equationNumber];
      var eqnMaxSpans=eqnMaxSpanDict[equationNumber];
      var eqnMinTds=eqnMinSpans.parent();
      var eqnMaxTds=eqnMaxSpans.parent();
      if (!valueExtractionElement.hasClass("categoryVisible")) {
        eqnMinTds.each(function(index) {
            $(this).removeClass("uncovered");
            $(this).addClass("invisible");
        });
        eqnMaxTds.each(function(index) {
            $(this).removeClass("uncovered");
            $(this).addClass("invisible");
        });
        return;
      }
      if (valueExtractionElement.attr("data-actualMin")==null) return;
      var actualMin=parseInt(valueExtractionElement.attr("data-actualMin"));
      var actualMax=parseInt(valueExtractionElement.attr("data-actualMax"));
      var minCoveringStream=valueExtractionElement.attr("data-minCoveringStream");
      var maxCoveringStream=valueExtractionElement.attr("data-maxCoveringStream");

      eqnMinSpans.html(String(actualMin));
      eqnMaxSpans.html(String(actualMax));
      eqnMinTds.attr("title","Minimum covered by "+minCoveringStream);
      eqnMaxTds.attr("title","Maximum covered by "+maxCoveringStream);
      eqnMinTds.each(function(index) {
          var eqnTheoreticalMin=parseInt($(this).find(".eqnTheoreticalMin").html());
          if (actualMin==eqnTheoreticalMin) {
              $(this).removeClass("uncovered");
              $(this).addClass("covered");
          } else if (actualMin < eqnTheoreticalMin) {
              $(this).removeClass("uncovered");
              $(this).addClass("supercovered");
          }
      });
      eqnMaxTds.each(function(index) {
          var eqnTheoreticalMax=parseInt($(this).find(".eqnTheoreticalMax").html());
          if (actualMax==eqnTheoreticalMax) {
              $(this).removeClass("uncovered");
              $(this).addClass("covered");
          } else if (actualMax > eqnTheoreticalMax) {
              $(this).removeClass("uncovered");
              $(this).addClass("supercovered");
          }
      });
  });
}

function setupPipelineCoverage() {
    eqnMinSpanDict={};
    eqnMaxSpanDict={};
    $("#pipelineDiagramDiv").find("span").each(function (index) {
        var classes=$(this).attr("class").split(" ");
        for (var i=0; i<classes.length; ++i) {
            var theDict=null;
            var eqnNumber=null;
            if (classes[i].indexOf("eqnMin")==0) {
                theDict=eqnMinSpanDict;
                eqnNumber=classes[i].substring("eqnMin".length);
            }
            else if (classes[i].indexOf("eqnMax")==0) {
                theDict=eqnMaxSpanDict;
                eqnNumber=classes[i].substring("eqnMax".length);
            }
            else continue;
            if (eqnNumber in theDict) {
              theDict[eqnNumber]=theDict[eqnNumber].add($(this));
            }
            else theDict[eqnNumber]=$(this);
        }
    });
    updatePipelineCoverage();
    coverageReady|=2;
}

function initPage() {
    if (isSummary) {
      $("#mainBranchList").hide();
      $("#expandAllButton").hide();
      $("#expandIncompleteButton").hide();
      $("#collapseAllButton").hide();
      $("#printButton").hide();
      //$("#visibleCategory").hide();
      $("#toggleSummaryButton").hide();
      $("#toggleSpecButton").hide();
      $("#togglePipelineDiagramButton").hide();
      $("#toggleCrossPointsButton").hide();
      $("#toggleCrossValuePointsButton").hide();
      $("#togglePipelineDiagramButton").hide();
      //summary.updateSummary();
    }
    var initialValueOpt;
    if (initialView != null) initialValueOpt = initialView;
    else initialValueOpt = initialValueSelectorFunc();
    var initialValueIndex=$("#"+initialValueOpt).index();
    $("#visibleCategory").prop("selectedIndex", initialValueIndex);
    if (!isSummary) {
      initCrossCoverSidepanel();
      setupPipelineCoverage();
    }
    updateVisibleCategory(true);
    $("#visibleCategory").change(function() { updateVisibleCategory(false); });
}

function removeRangeButton() {
    $("#togglePipelineDiagramButton").hide();
    summary.updateSummary();
}

function doPrint()
{
    pleaseWait(true,false);
    hideSidePanels(function(openPanel) {
      $("#noscrollDiv").hide();
      $("#printSpan").html($("#innerScrollDiv").html());
      $("#printSpan #mainBranchList").removeClass("mainBranchList");
      $("#printSpan #mainBranchList").css("margin-top", "");
      $("#printPleaseWait").hide();
      $("#printTopSpan").show();
      window.print();
      $("#printPleaseWait").show();
      setTimeout(function () {
        $("#printTopSpan").hide();
        $("#printSpan").html("");
        $("#noscrollDiv").show();
        if (openPanel != null) {
          showSidePanel(openPanel, true, null);
        }
        pleaseWait(false,false);
      }, 100);
    });
}

