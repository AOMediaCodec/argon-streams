<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" encoding="utf-8" indent="yes" />
<xsl:variable name="codecName">
<xsl:choose>
  <xsl:when test="report/codec"><xsl:value-of select="translate(report/codec, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')" /></xsl:when>
  <xsl:otherwise>h265</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<xsl:template match="/">
  <!--<xsl:variable name="allTestCases" select="count(document('all_points.xml')/specification//coverage)" />
  <xsl:variable name="booleanTestCases" select="count(document('all_points.xml')/specification//booleanTestCase)" />
  <xsl:variable name="totalTestCases" select="$allTestCases+$booleanTestCases" />-->
  <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html></xsl:text>
  <html>
  <head>
    <link rel="stylesheet" type="text/css" href="ArgonViewerV14/jquery-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="ArgonViewerV14/coverage.css" />
    <!-- IMPORTED LIBRARIES -->

    <!-- online version -->
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=MML_HTMLorMML"></script>-->

    <!-- offline version -->
    <script src="ArgonViewerV14/jquery.min.js"></script>
    <script src="ArgonViewerV14/jquery-ui.min.js"></script>
    <script type="text/javascript" src="ArgonViewerV14/MathJax/MathJax.js?config=MML_HTMLorMML"></script>

    <!-- END IMPORTED LIBRARIES -->

    <script type="text/x-mathjax-config">
      MathJax.Hub.Config({
        "HTML-CSS": { linebreaks: { automatic: true } },
               SVG: { linebreaks: { automatic: true } }
               , skipStartupTypeset: true
      });
    </script>

    <script type="text/javascript">
      isSummary = false;
      <xsl:if test="report/summary">
        isSummary = true;
        summaryAttributes = {
        <xsl:for-each select="report/summary">
          "<xsl:value-of select="@view" />": {
          <xsl:for-each select="*">
            "<xsl:value-of select="local-name()" />": "<xsl:value-of select="text()" />",
          </xsl:for-each>
          },
        </xsl:for-each>
        };
      </xsl:if>
    </script>

    <script type="text/javascript">
      function findCoveredValue(name, type) {
        var elem=$("li.testPoint[data-testName='"+name+"']");
        if (elem.length==0) elem=$("li.testGroup[data-testName='"+name+"']");
        if (elem.length==0) return 0;
        var retVal=0;
        if (type=="min") retVal=parseInt(elem.attr("data-actualMin"));
        if (type=="max") retVal=parseInt(elem.attr("data-actualMax"));
        return retVal;
      }
      <xsl:choose>
        <xsl:when test="report/initialView">
          var initialView = "<xsl:value-of select="report/initialView" />";
        </xsl:when>
        <xsl:otherwise>
          <xsl:choose>
            <xsl:when test="report/initialViewSelector">
              var initialView = null;
              var initialValueSelectorFunc=
                <xsl:value-of select="report/initialViewSelector" />
              ;
            </xsl:when>
            <xsl:otherwise>
              var initialView = null;
              var initialValueSelectorFunc=function () {
                var sliceSegmentHeaderExtensionLength=findCoveredValue('slice_segment_header_extension_length','max');
                if (sliceSegmentHeaderExtensionLength>0) return 'specextopt';
                var generalProfileIdc=findCoveredValue('general_profile_idc','max');
                if (generalProfileIdc == 1) return 'mainopt';
                if (generalProfileIdc == 2) return 'main10opt';
                if (generalProfileIdc != 4) return 'allopt';
                var chromaFormatIdcMax=findCoveredValue('chroma_format_idc','max');
                if (chromaFormatIdcMax > 2) return 'mainopt_444';
                var chromaFormatIdcMin=findCoveredValue('chroma_format_idc','min');
                var extChroma=(chromaFormatIdcMin!=1 || chromaFormatIdcMax!=1);
                var maxBitDepth=findCoveredValue('bit_depth_luma_minus8','max')+8;
                if (maxBitDepth>10) return extChroma?'main12_422opt':'main12opt';
                else return 'main10_422opt';
              }
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
      var noSummary=
      <xsl:choose>
        <xsl:when test="//noSummary">
          1
        </xsl:when>
        <xsl:otherwise>
          0
        </xsl:otherwise>
      </xsl:choose>;
    </script>
    <script type="text/javascript">
      crossPointData={<xsl:for-each select="report/crossPoint">
        <xsl:apply-templates select="." mode="js"/>
      </xsl:for-each>
      };
    </script>
    <script type="text/javascript">
      allVisibilityCategories = [
      <xsl:choose>
        <xsl:when test="report/visibilityCategories">
          <xsl:for-each select="report/visibilityCategories/visibilityCategory">
            "<xsl:value-of select="." />",
          </xsl:for-each>
        </xsl:when>
        <xsl:otherwise>
          "MAIN","MAIN10","MAIN_422","RANGE_EXTENSIONS","COLOUR_EXTENSIONS","SPEC_EXTENSIONS","NAL_EXTENSIONS","SEI","IMPOSSIBLE"
        </xsl:otherwise>
      </xsl:choose>
      ];
    </script>
    <script type="text/javascript">
      useEqnNames = false;
      <xsl:if test="report/useEqnNames">
        useEqnNames = true;
      </xsl:if>
    </script>
    <script src="ArgonViewerV14/coverage.js"></script>
    <script src="ArgonViewerV14/pipelineDiagram.js"></script>
    <script type="text/javascript">
      setupSectionBoxes("<xsl:value-of select="$codecName" />");
    </script>
    <script src="ArgonViewerV14/crosscover.js"></script>
    <script src="ArgonViewerV14/summary.js"></script>
  </head>
  <body>
  <span id="printTopSpan" style="display:none">
    <p><img style="margin-left: 10px; vertical-align:middle;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAAAZCAYAAABaf0CGAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNS4xIE1hY2ludG9zaCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxNDgxODk5NkUyRDgxMUUxQTlFNUI0N0U4MzE0MjY4MSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoxNDgxODk5N0UyRDgxMUUxQTlFNUI0N0U4MzE0MjY4MSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjE0ODE4OTk0RTJEODExRTFBOUU1QjQ3RTgzMTQyNjgxIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjE0ODE4OTk1RTJEODExRTFBOUU1QjQ3RTgzMTQyNjgxIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+wFCe/QAADrpJREFUeNrsXAmUVMUVfd093TQDzgKCsiiCCugMKhLAqCCrGqMouOXgAqJxjdGIS0yMkQgxcYtK3KI5oogLksQFlwiIoEQQ44K4IIiIB9kUBhhmuqeXn3oz9zM11e9vPT2Ec5J3zj0zXf1//fpVb7nv/fodov8xKTvhYio/dSJla3c4HTJQ4QiFOQqr/fUaolA0Rhvum0CJVUt3+z0NGz6CTj5lFNXW1tL/Zc+WIo/v91MYrRBVSCp8qzBboc7jvBKFg1w1lCihsE1hi0JNHmOvVOilUK7QCm2WQrXCRoWPFDYE6G+MwvkKWYW7FNYEHZCVTknNEYXhCgcrxBXYKmYpbPLZbYVCf/YV6IudwPO413oJhyNO58X144z55/XcrvCdz/lnXejo0J+bfIE1cZIjoSvl0McUxrVe4ROMT5cDFAYptMPxmxWexf04yaEKvXGNuKArH0O3TWmrcLxCF4WYQpXCDOiuH2HH3Q/2EFb40svgblM4x2gbrPCWyzlRKMQQD4NLY5LsG/67wssK37uc1wYO4BKFozwcBvczX+FBhTd2GUU2bR63DwyM7/MJhXEO/bXGeLOCqanpjFCsay9KrvnQ/PJAhX8abacojIJyecnvcawt1VD+qvrJjkapoqKC0ukm9zURaxd1Mbg0HCfP/wdwAgsV1jkY7wKF9nkY3FMK5wrryPd0lcIAjEcSNqYXFSYpfIO2GxQuNY5jw/y30RaDE2VdOQZz4SQ8l/MUHtB1RcmxCn8zjmW9Ps9nMJsBY7elJuxywv4KpwntP/G4EEebQzCJTrAHxBPfQ+FUhccV3oU3l4QX+xWF6ZgIL2fBx5+hcP+uFmUU8YMGkJXZpeeH45psbDcbxlaC+38SCslOga1pJiJhuyZXU32WjbyIwq33khxQxmg7EUbuR0wDT8FbNnQei1FpaSlls00O66spmNv8Fyt0h2KyYbzv4HA6Yz7JY10lVBp9lYMlPQX6HnK59w4KFxoOJ+SDqfE15io8DQOJemUaCqcr3C6wE1PYeVzmK8/Q1gmSdDO4QTAIU84GtXBkVj4op5P0gFfYS4guryK6BpUFu9zqYcOouHIIWXUJm5LOhmNhY5+iLd5Y0JTf4JpHIFL1UTgTxy/D97voZNHeXam4zzBpPqSocCX68mSqbp8tyzKNjZox/7yu0xR+arRnmpG27DQ+T/dgP9L5r7rMh9kWgTEPymOsr3nMvS1TFUbk4SwtN4Mb5hI5jm7BvJJznQuMtuNdIh9z/cWguUuQe6U1RXmgPrjF21LJsPE2pYyivStyyF9qk8O8fS2iWD84gZ4KP1f4l3Zd5vW/g4LWRzsrk6bSkRdSuE2Z33v9C4x4T5M/IT8uhLxr5F/DhWPYQSwHpV0KmmvLLP/Fq3o5BwzCSVeWQFcWQ1d0Wv+qz2uwUd8LZ12Qosk+DnRS57HPB7jO21BYXeKgdNfAyHTha9+nfb5I6HMD+PyLdj4DKYaRDMZCLqvnh0PPp3jPgZTdWX/ojZoHvNUorlRhvLawha4EHoQyXqbRjXEw4PFWKpmKdelNrXsOoJ0fvO6raAqjO0lhawsaUAbMZLUxT5VIEYY45Fh3uPR5F9iIG6VirNDa+mtFC1vmQQc+1Rxle9BijiKPBrzXUUIbF16uVXgJDlafgwOQovB6LgpwnUORrowKktc6GdxZOTlKUzkDdGqHz+tsRR5kyjuoDr0gRNEwoo4UUS1EoDlCnzXwlst3JZU9+lLpiAvIangU0AURjeWzJjmet6RBBdkob9LaxyJpv5uH3KpbpV+DIxR/7nEp1hRCLMy/GSlYwR4GzTtXcKpuBveVw5q6SbHQNs12ikbBay4QNIAcIrRfgdxb0pVPgXzkZOR91/k9wYlSHmd8XmfkBV2QZPqViMt3KzTPpk+cfU43JMG6bPe72EXlnajjhLspFI0rOlmfipyEnNDO71J5TPQtyP/MRS220nUU73U0hYqiQfpj53F9C9PEuMt3v6WGxxW6VGrzJEk0jzF8JkSDKWAkhZCOqODqslZw6IWUa1H0y9vgOgjFCaZR7xltYws04JBHpapM+H6z7+hqZSkUDtf/Fcb9djMo2v1CwWcMZTJU1K4zRUr2DtrnbYKj212ynnKfd5Vj7p0kH0e1DAagy/5IT44pwH2UCk6C87RkC8/fXxUOy9fgxsDodHkd0IXzr5IACuok/YUImBKini47/C54umojbZs3TUW4VnZueqR2jfeaMcmLKPeZ1SlWNkvh4hIqar+f27lfIEqba/GYQqf/gsFZglLyeGIu5/ANVrjgYOGcBOizKX2QX51YAOctXbOQ8pmge+1xX22DGhwr/gSjbZWZE2nhe2SAUD9MwwhQ0lsb8h7R47olonUkPoCWZfuCGZT8ejkbXQ/NSXyP6+QrOyj3YWvP+jm1LKqHs3A17EKhvTsiZ4T2DHG7Cc5bPnQB7/T5oXAeK+YjQns55mUyNe4cKoQUei55g8YkoX2owp1BDa67FgFsmYWJ53Kt+UzlEp+DHIhqlI056JcLDxL3ejYPTyYJP8/rpPKqWM2yeSqvipUYni/dzMnfLORJfha4Le7/ceG70Q4LuqdJGLm2E5jadXU490rkwZL8mhp2e3QvYMpCPteks0Nhx1zjKUIOz8LPLy93c1ZhoTJlVi5f1pLP+cZ3g1wmNV/hyuUzzeyDb/xNVJ+YAiyuXvJCh8y2zYlQ477DqEvRyK+Y1Luq3ohDao1DYT+O7nLcrykTAxal9lRx2qaXhFMZKzgtlqPhmA/bDWM8CwZu68pCj9zVVqCfKXwurCvXO/rBmYfcDI47Mh84bzLynEcMy40jehVKmFKMLwDvPh0FiK5IpPtma7Z1tlLJdcoYbCrajhq3K+UjJQIbWKr6t7hSaSWr/fTBZelLhVwwjnyum8Aq9hThquZWF3wJBXaTp5FevOVArzmv693C9zEU2A9reriHwdnyNTU8jzV39XDeOx391boZ3BGU+7xrC5T3POAA4QJ+NnKm4P2rSN6ZzkrFpdWTUFBoLk3IvUY4ElHGwJNk72JgylPZjIU6Vojuz3FFNFtdRanNa4NU7iY60OFpDpR7T5ApyFmd0NdnjrwMtYAHhO+4gnlfM5mIV3EtIURfv/WBNx3Wjp8F3iWNW6ePUoLL3uVJj4uegAl2M5QF1FiOLwH/7W149F4BjKsNKKHv0nQ2sTOS+u6bVKSs43zKpO2xDNYoc1Axd86wl15EkSilNq6mbDJQYJoJ+vQrgeJbu8F4QiRvgnC7tv1qTyGElfwKOGRzDkbC6T/n8z5MfYm28Nz9GetkpgBnSoYbNpL1fISN5QwfXmQzwFTjaoEXc8XyQF/RqiF3ahNolNlMKPXtCpVaFU03olQ+MgmORveivK8yG4oUUWLlu+q/bJD+LBSQ5jcj6W+OtKHcnUVchd3mI5cppHDB5BWhfYTP82sEJ9ypALm6l1xMubtlpBrJroa+lN9OfL144qdIYAu/H/YPo41LwZMFBftGyGPKPYo1omdObVqjeg8tgtHbUf3HAe6TaR6/n3azUOSYy8USzt8SK/N665vHPA65QUuI27PQ8ZT7THU1uW8uyLbQOG+g3OqxX1q9kZpufCY48b5BdSWgbEEBbAv5NIQJAqVYT437zExUC/lMt4CDvJyabjpm4Y205guvvNfyE4EK30nOpWNxj2jdt6v4XTilRKGrtSrS3ZS77akE+Wop/udFOxu08UbtOB4/PxqZWh+KYnGq/WQhJb76KN+FY+dyJbXMzogSA1wwOgiJ/x+E4xd6KGOx0KcE01GycQ+Aw4wKUbNCcLp+jaJGKNTEQPsC6Uoewo78Gq+DimB00isTXC5d4nAeb3jVK5ptUTyZHGCAG2A0kwVaMVMrzmRR9Rkg5I6s2byncgWOjyPRFl8fSq5ZRnXrVlCsc8/ZVir5EDWUdnsiQk0xFvg05J29Kff9vDrkFDeTvSGYHwUoGln12kP6NrJ85CX0+8cCGlsRqFraaGtDzvsl53j0ybnWVT6u/ZRWWJgEh2LngOxMV8K5swH3QS4bEiKXX+HXgcwdK0eB8n2AWkMCxs5G2L+A88zPVftp9yguBFdUzM2j/HbzOy405E3KfYQwCsYTJERzJYe3kh1pFGpuMZLnWWhrL1C8wT7pcMRKJWj73Meow/jb1SBDyrCtLtT4oJmj6PNa/nIPPjNd3heU1/5JCH4zusnOm1CsNSVXLVUG/XkhFm4qlGR0AZUhSLXzYdJe3HWh13v56MvOv7pQ04q2HQG9yv6sg88GHPs5Qj2gLdbSK/2JNDNvvg6pyg+cKOUFlLuVZrYH5+fnZeb7W1xl7Iyk1e+AEzAkMyRcYVCADZjEDXlOQpXtJWuVUWSqt/Av77CH5V360zDJT4M26rIG0fUOOBP++4RpbPyQW0VM2vrSvdIPCYXyWMBaUK+VvkqMoVAhadITmP+MW/If8F4IrKMsj/NvBL31Kxw1z22GruzQctdQHkWsJPRqo5PBjRfal3h0ulmoJpXAI6cEA457UKipQl+3CIWW4Tje78ua6xEdj7Wpn5XcSVZd0lbSajgc3pnCJe5nUG0MVAENq9yNt44lvnxf+jotVPRiPrq1HYJZuGitswj+iYVMJiOtj19JQ0nngqWME5ztTsp/G9wq/OV9py+Qv2dzKaQLYyj3nbxWPoyA3+bmtw9e9lPI0HSF04STtXMyeRgcIY+cKMxZG7v4MMqY+Nd8dHo9jq8A1dtCjTsGLsXA9wc3n+nR17WYpCFI5KMk71X7FGPtBCMaiGJNqZZbbcINL8LxVWY0otyI8CjGyM9SfoEcgCPaG+T+E2+wuAil1q1wUzh+qP8j0JwIyXsoJVkMCsSOrBK07C3S3jRI1NbS8uUf03FDhuqGdxMc1L7k/KtdNTDmtVC2KpdxLAFFqkD09JM2hHDcAq3qeRqi3KHIyXuD6rbGPG+AoS0h+T1JmzImkAaVYa2lHS2roYOdQfEGohBWpjkavu/Pcb2PBUe+EE5vCBgX69cjPtduBtZ+NNI2tpH5/xFgALnH5Jh/rPieAAAAAElFTkSuQmCC"/>
      <span style='font-size:130%; vertical-align:middle; font-weight:bold; padding-left:10px; font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif; white-space: nowrap;'>
        <xsl:choose>
          <xsl:when test="$codecName='h265' and (report/summary)">HEVC Evaluation Coverage Report</xsl:when>
          <xsl:when test="$codecName='vp9' and (report/summary)">VP9 Evaluation Coverage Report</xsl:when>
          <xsl:when test="$codecName='vp8' and (report/summary)">VP8 Evaluation Coverage Report</xsl:when>
          <xsl:when test="$codecName='av1b' and (report/summary)">AV1 Evaluation Coverage Report</xsl:when>
          <xsl:when test="$codecName='h265'">HEVC Specification Coverage Report</xsl:when>
          <xsl:when test="$codecName='vp9'">VP9 Specification Coverage Report</xsl:when>
          <xsl:when test="$codecName='vp8'">VP8 Specification Coverage Report</xsl:when>
          <xsl:when test="$codecName='av1b'">AV1 Specification Coverage Report</xsl:when>
          <xsl:otherwise>HEVC Specification Coverage Report</xsl:otherwise>
        </xsl:choose>
      </span>

    </p>
    <div id="printPleaseWait" style="color: red; font-size:150%; display:none;">Please wait...</div>
    <span id="printSpan"></span>
  </span>
  <div id="pleaseWaitDiv" style="position:fixed; top:0px; left:0px; width:100%; height:100%; background:rgb(255,255,255); color: #ccc; z-index:10; font-family:'Arial Black',Gadget,sans-serif; font-size:800%;">
    <div style="display:block; position:relative; top:50%; text-align:center; line-height:100px; margin-top:-100px;">Loading<span id="loadingDot1">.</span><span id="loadingDot2" style="color:white">.</span><span id="loadingDot3" style="color:white">.</span></div>
  </div>
  <div id="noscrollDiv" style="position:fixed; top:0; left:0; width:100%; height:100%; overflow-y:scroll;">
  <div id="topDiv" style="position:fixed; top:0; left:0; right:0px; height:7em; background:linear-gradient(to bottom, #a0e0ff, #fff 95%, rgba(255,255,255,0));">
  <p><img style="margin-left: 10px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAAAZCAYAAABaf0CGAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyRpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYxIDY0LjE0MDk0OSwgMjAxMC8xMi8wNy0xMDo1NzowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNS4xIE1hY2ludG9zaCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDoxNDgxODk5NkUyRDgxMUUxQTlFNUI0N0U4MzE0MjY4MSIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDoxNDgxODk5N0UyRDgxMUUxQTlFNUI0N0U4MzE0MjY4MSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjE0ODE4OTk0RTJEODExRTFBOUU1QjQ3RTgzMTQyNjgxIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjE0ODE4OTk1RTJEODExRTFBOUU1QjQ3RTgzMTQyNjgxIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+wFCe/QAADrpJREFUeNrsXAmUVMUVfd093TQDzgKCsiiCCugMKhLAqCCrGqMouOXgAqJxjdGIS0yMkQgxcYtK3KI5oogLksQFlwiIoEQQ44K4IIiIB9kUBhhmuqeXn3oz9zM11e9vPT2Ec5J3zj0zXf1//fpVb7nv/fodov8xKTvhYio/dSJla3c4HTJQ4QiFOQqr/fUaolA0Rhvum0CJVUt3+z0NGz6CTj5lFNXW1tL/Zc+WIo/v91MYrRBVSCp8qzBboc7jvBKFg1w1lCihsE1hi0JNHmOvVOilUK7QCm2WQrXCRoWPFDYE6G+MwvkKWYW7FNYEHZCVTknNEYXhCgcrxBXYKmYpbPLZbYVCf/YV6IudwPO413oJhyNO58X144z55/XcrvCdz/lnXejo0J+bfIE1cZIjoSvl0McUxrVe4ROMT5cDFAYptMPxmxWexf04yaEKvXGNuKArH0O3TWmrcLxCF4WYQpXCDOiuH2HH3Q/2EFb40svgblM4x2gbrPCWyzlRKMQQD4NLY5LsG/67wssK37uc1wYO4BKFozwcBvczX+FBhTd2GUU2bR63DwyM7/MJhXEO/bXGeLOCqanpjFCsay9KrvnQ/PJAhX8abacojIJyecnvcawt1VD+qvrJjkapoqKC0ukm9zURaxd1Mbg0HCfP/wdwAgsV1jkY7wKF9nkY3FMK5wrryPd0lcIAjEcSNqYXFSYpfIO2GxQuNY5jw/y30RaDE2VdOQZz4SQ8l/MUHtB1RcmxCn8zjmW9Ps9nMJsBY7elJuxywv4KpwntP/G4EEebQzCJTrAHxBPfQ+FUhccV3oU3l4QX+xWF6ZgIL2fBx5+hcP+uFmUU8YMGkJXZpeeH45psbDcbxlaC+38SCslOga1pJiJhuyZXU32WjbyIwq33khxQxmg7EUbuR0wDT8FbNnQei1FpaSlls00O66spmNv8Fyt0h2KyYbzv4HA6Yz7JY10lVBp9lYMlPQX6HnK59w4KFxoOJ+SDqfE15io8DQOJemUaCqcr3C6wE1PYeVzmK8/Q1gmSdDO4QTAIU84GtXBkVj4op5P0gFfYS4guryK6BpUFu9zqYcOouHIIWXUJm5LOhmNhY5+iLd5Y0JTf4JpHIFL1UTgTxy/D97voZNHeXam4zzBpPqSocCX68mSqbp8tyzKNjZox/7yu0xR+arRnmpG27DQ+T/dgP9L5r7rMh9kWgTEPymOsr3nMvS1TFUbk4SwtN4Mb5hI5jm7BvJJznQuMtuNdIh9z/cWguUuQe6U1RXmgPrjF21LJsPE2pYyivStyyF9qk8O8fS2iWD84gZ4KP1f4l3Zd5vW/g4LWRzsrk6bSkRdSuE2Z33v9C4x4T5M/IT8uhLxr5F/DhWPYQSwHpV0KmmvLLP/Fq3o5BwzCSVeWQFcWQ1d0Wv+qz2uwUd8LZ12Qosk+DnRS57HPB7jO21BYXeKgdNfAyHTha9+nfb5I6HMD+PyLdj4DKYaRDMZCLqvnh0PPp3jPgZTdWX/ojZoHvNUorlRhvLawha4EHoQyXqbRjXEw4PFWKpmKdelNrXsOoJ0fvO6raAqjO0lhawsaUAbMZLUxT5VIEYY45Fh3uPR5F9iIG6VirNDa+mtFC1vmQQc+1Rxle9BijiKPBrzXUUIbF16uVXgJDlafgwOQovB6LgpwnUORrowKktc6GdxZOTlKUzkDdGqHz+tsRR5kyjuoDr0gRNEwoo4UUS1EoDlCnzXwlst3JZU9+lLpiAvIangU0AURjeWzJjmet6RBBdkob9LaxyJpv5uH3KpbpV+DIxR/7nEp1hRCLMy/GSlYwR4GzTtXcKpuBveVw5q6SbHQNs12ikbBay4QNIAcIrRfgdxb0pVPgXzkZOR91/k9wYlSHmd8XmfkBV2QZPqViMt3KzTPpk+cfU43JMG6bPe72EXlnajjhLspFI0rOlmfipyEnNDO71J5TPQtyP/MRS220nUU73U0hYqiQfpj53F9C9PEuMt3v6WGxxW6VGrzJEk0jzF8JkSDKWAkhZCOqODqslZw6IWUa1H0y9vgOgjFCaZR7xltYws04JBHpapM+H6z7+hqZSkUDtf/Fcb9djMo2v1CwWcMZTJU1K4zRUr2DtrnbYKj212ynnKfd5Vj7p0kH0e1DAagy/5IT44pwH2UCk6C87RkC8/fXxUOy9fgxsDodHkd0IXzr5IACuok/YUImBKini47/C54umojbZs3TUW4VnZueqR2jfeaMcmLKPeZ1SlWNkvh4hIqar+f27lfIEqba/GYQqf/gsFZglLyeGIu5/ANVrjgYOGcBOizKX2QX51YAOctXbOQ8pmge+1xX22DGhwr/gSjbZWZE2nhe2SAUD9MwwhQ0lsb8h7R47olonUkPoCWZfuCGZT8ejkbXQ/NSXyP6+QrOyj3YWvP+jm1LKqHs3A17EKhvTsiZ4T2DHG7Cc5bPnQB7/T5oXAeK+YjQns55mUyNe4cKoQUei55g8YkoX2owp1BDa67FgFsmYWJ53Kt+UzlEp+DHIhqlI056JcLDxL3ejYPTyYJP8/rpPKqWM2yeSqvipUYni/dzMnfLORJfha4Le7/ceG70Q4LuqdJGLm2E5jadXU490rkwZL8mhp2e3QvYMpCPteks0Nhx1zjKUIOz8LPLy93c1ZhoTJlVi5f1pLP+cZ3g1wmNV/hyuUzzeyDb/xNVJ+YAiyuXvJCh8y2zYlQ477DqEvRyK+Y1Luq3ohDao1DYT+O7nLcrykTAxal9lRx2qaXhFMZKzgtlqPhmA/bDWM8CwZu68pCj9zVVqCfKXwurCvXO/rBmYfcDI47Mh84bzLynEcMy40jehVKmFKMLwDvPh0FiK5IpPtma7Z1tlLJdcoYbCrajhq3K+UjJQIbWKr6t7hSaSWr/fTBZelLhVwwjnyum8Aq9hThquZWF3wJBXaTp5FevOVArzmv693C9zEU2A9reriHwdnyNTU8jzV39XDeOx391boZ3BGU+7xrC5T3POAA4QJ+NnKm4P2rSN6ZzkrFpdWTUFBoLk3IvUY4ElHGwJNk72JgylPZjIU6Vojuz3FFNFtdRanNa4NU7iY60OFpDpR7T5ApyFmd0NdnjrwMtYAHhO+4gnlfM5mIV3EtIURfv/WBNx3Wjp8F3iWNW6ePUoLL3uVJj4uegAl2M5QF1FiOLwH/7W149F4BjKsNKKHv0nQ2sTOS+u6bVKSs43zKpO2xDNYoc1Axd86wl15EkSilNq6mbDJQYJoJ+vQrgeJbu8F4QiRvgnC7tv1qTyGElfwKOGRzDkbC6T/n8z5MfYm28Nz9GetkpgBnSoYbNpL1fISN5QwfXmQzwFTjaoEXc8XyQF/RqiF3ahNolNlMKPXtCpVaFU03olQ+MgmORveivK8yG4oUUWLlu+q/bJD+LBSQ5jcj6W+OtKHcnUVchd3mI5cppHDB5BWhfYTP82sEJ9ypALm6l1xMubtlpBrJroa+lN9OfL144qdIYAu/H/YPo41LwZMFBftGyGPKPYo1omdObVqjeg8tgtHbUf3HAe6TaR6/n3azUOSYy8USzt8SK/N665vHPA65QUuI27PQ8ZT7THU1uW8uyLbQOG+g3OqxX1q9kZpufCY48b5BdSWgbEEBbAv5NIQJAqVYT437zExUC/lMt4CDvJyabjpm4Y205guvvNfyE4EK30nOpWNxj2jdt6v4XTilRKGrtSrS3ZS77akE+Wop/udFOxu08UbtOB4/PxqZWh+KYnGq/WQhJb76KN+FY+dyJbXMzogSA1wwOgiJ/x+E4xd6KGOx0KcE01GycQ+Aw4wKUbNCcLp+jaJGKNTEQPsC6Uoewo78Gq+DimB00isTXC5d4nAeb3jVK5ptUTyZHGCAG2A0kwVaMVMrzmRR9Rkg5I6s2byncgWOjyPRFl8fSq5ZRnXrVlCsc8/ZVir5EDWUdnsiQk0xFvg05J29Kff9vDrkFDeTvSGYHwUoGln12kP6NrJ85CX0+8cCGlsRqFraaGtDzvsl53j0ybnWVT6u/ZRWWJgEh2LngOxMV8K5swH3QS4bEiKXX+HXgcwdK0eB8n2AWkMCxs5G2L+A88zPVftp9yguBFdUzM2j/HbzOy405E3KfYQwCsYTJERzJYe3kh1pFGpuMZLnWWhrL1C8wT7pcMRKJWj73Meow/jb1SBDyrCtLtT4oJmj6PNa/nIPPjNd3heU1/5JCH4zusnOm1CsNSVXLVUG/XkhFm4qlGR0AZUhSLXzYdJe3HWh13v56MvOv7pQ04q2HQG9yv6sg88GHPs5Qj2gLdbSK/2JNDNvvg6pyg+cKOUFlLuVZrYH5+fnZeb7W1xl7Iyk1e+AEzAkMyRcYVCADZjEDXlOQpXtJWuVUWSqt/Av77CH5V360zDJT4M26rIG0fUOOBP++4RpbPyQW0VM2vrSvdIPCYXyWMBaUK+VvkqMoVAhadITmP+MW/If8F4IrKMsj/NvBL31Kxw1z22GruzQctdQHkWsJPRqo5PBjRfal3h0ulmoJpXAI6cEA457UKipQl+3CIWW4Tje78ua6xEdj7Wpn5XcSVZd0lbSajgc3pnCJe5nUG0MVAENq9yNt44lvnxf+jotVPRiPrq1HYJZuGitswj+iYVMJiOtj19JQ0nngqWME5ztTsp/G9wq/OV9py+Qv2dzKaQLYyj3nbxWPoyA3+bmtw9e9lPI0HSF04STtXMyeRgcIY+cKMxZG7v4MMqY+Nd8dHo9jq8A1dtCjTsGLsXA9wc3n+nR17WYpCFI5KMk71X7FGPtBCMaiGJNqZZbbcINL8LxVWY0otyI8CjGyM9SfoEcgCPaG+T+E2+wuAil1q1wUzh+qP8j0JwIyXsoJVkMCsSOrBK07C3S3jRI1NbS8uUf03FDhuqGdxMc1L7k/KtdNTDmtVC2KpdxLAFFqkD09JM2hHDcAq3qeRqi3KHIyXuD6rbGPG+AoS0h+T1JmzImkAaVYa2lHS2roYOdQfEGohBWpjkavu/Pcb2PBUe+EE5vCBgX69cjPtduBtZ+NNI2tpH5/xFgALnH5Jh/rPieAAAAAElFTkSuQmCC"/>
  <span style='font-size:200%; font-weight:bold; padding-left:30px; font-family: "Century Gothic", CenturyGothic, AppleGothic, sans-serif; white-space: nowrap;'>
    <xsl:choose>
      <xsl:when test="$codecName='h265' and (report/summary)">HEVC Evaluation Coverage Report</xsl:when>
      <xsl:when test="$codecName='vp9' and (report/summary)">VP9 Evaluation Coverage Report</xsl:when>
      <xsl:when test="$codecName='vp8' and (report/summary)">VP8 Evaluation Coverage Report</xsl:when>
      <xsl:when test="$codecName='av1b' and (report/summary)">AV1 Evaluation Coverage Report</xsl:when>
      <xsl:when test="$codecName='h265'">HEVC Specification Coverage Report</xsl:when>
      <xsl:when test="$codecName='vp9'">VP9 Specification Coverage Report</xsl:when>
      <xsl:when test="$codecName='vp8'">VP8 Specification Coverage Report</xsl:when>
      <xsl:when test="$codecName='av1b'">AV1 Specification Coverage Report</xsl:when>
      <xsl:otherwise>HEVC Specification Coverage Report</xsl:otherwise>
    </xsl:choose>
  </span></p>
  <button title="Expand all" id="expandAllButton" style="margin-left:8px"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gUBDQYJs+KzmwAAAcZJREFUOMulkk1rE2EUhZ+pIRMJJCJatIhlCC5FkfgHxPyBWit0a3XhsqAUXGsLRVBw4UaycF3ippG6Uld+IEERtVonpMEYnMxH48zEznRyXZiGpkkLpQcOnMXlfZ97uLBPKQBX7xiXFEWZB7QBM2URufn49tGFHV+ZmFnRS8u+tNvtPpeWfZmYWdF3Jbgw9VqeP8ry7nOLp68CzKawJ5KzY4vSarXkxqwueyWJAbiuTxRFfP3xm9OZk7z55A0kSaUOaVN3G/pWkhiASIz19Q0syyQMQ54sGlwbO8aZU4k+2g/f/2qz+do8sAAwBKCqaaLowDaSOEEQEAQBF6+/xXEc6vU6Jw57NBoNrWeFZPJI+f2XSNtOEoYhruthWSarq1UAEomDWJbZ08HwWrV4fy4/Oa2q6dGtJOfGl7qD47e+AVB8mMV1/Z61ksBxYCR7uVR59tKRTK4gpumIbbti265kcoVuLpdrkskVuu0OAR7wC9ho/ly6N5dvVjY78f0I349Q1XQ3x+NJVDU98Kj6SAwjEMMIxPfb3Vx8Ycv5Kx/1nhI78joe/k8yOY3ijfZ9I1FlrVp8AIwANWUHktTmme+iPx3vT/8AeWhFXF3n2jkAAAAASUVORK5CYII=" /></button>
  <button id="expandIncompleteButton" title="Expand incomplete nodes"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gUBDRYCbvJ4QgAAAixJREFUOMulk0tIVGEUx3/XhjtjQyo1SmM1eRvShaSls0mCpFKopZJEqyBrEa2CQKhVUAauiha1iIjZDZmF6IA9iBY9TBOj16DOlAOjNc/yzp28t3u/NmojjoH0wYGPcw6H3/mfc+A/nwRw8nK8XZKkHkApkBMRQpy/faG8d9UqHV2T4bGQJizLWmFjIU10dE2G/0lwoPOVGLrp483HHA+e6yR/CtZEsrttQORyOXGmOyzWSmIDUFUN0zT5PPWdXV4Pr99nC5KUlJQpnVcS4XwSG4AQNubnf5NKJTEMA/9AnFNtm6nf6VhBOz7xS+m+E+sBegGKAOz2UkxzHaqq0draukAio+s6uq5z6PQwmUyG2dlZtm7MkkgklGUtOJ2uyOgnUxHChmVZSySGYaCqWVKpJNPTUQAcjmJSqeSyKVRUN1095q4+fu5lYK/4MlZWpfg0ngT9nLg0s6KFwRs+jpwdYXLoqLTocwJuYF89iMFN64X/lueex+OJp9OqSKdV4W3pE4v/SCQmvC19S+oWAVlgBrgeDNWilAgONm9oqqvbUxQMPkpqmondXoqmmWiaiSw7sdtLyS/gAZ7FQrXDAEa1A8CdSTyO9/c/DOu6yUigGVkuRpaLGZ+wcDpdkXwRvQu2H6DiYjkWELhbVbOj8Snvvt3Htf3wXxGE+fVHdPAaUAnEJGA0FqptWIz3NkVpf7FtKb+y5gPAlgKrPQfMScBonrNhlZtpBN4WCvwB2YU0Uf5LMUIAAAAASUVORK5CYII=" /></button>
  <button id="collapseAllButton" title="Collapse all"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gUBDQgOswULtgAAAcJJREFUOMulks9LFGEcxj9T28xuQ7utmJSEMiwdo4g9dRT8B6w8eM0uHYVgoXMpSFDQoUts0DHCLm7YzQ6RiCxCZKbtsEkb5M6q08y7686Pt0Ou7jabID7wwMuXl4fP9+ELx5QCcPvB5g1FUaYBo8sfU0p57/n9c6//mzKaWy8VV4UMwzDi4qqQo7n10qEEQ+Mf5btnWRY/13nzvollS45EcnVkVtbrdXl3siSPShIDcBxBEAR8+faLy5kBFj65XUmSybPG+MNqqZ0kBiBljN1dn1rNwvM8Xs5ucmfkPFcuxSO0y2sNYzJfmQYOAjQtRRCcjJA0m008P0SGPr7v02g0uNhzimq1anSsoOu95tJKYPxL4nketu0ghIsQAoB4PEGtZnV00LezUXg8lR+b0LTUYDvJtZtzkRUKT7M4juiY6cAFoD97q1h+O78tM8Mz0rK25daWE7FpVmRmeGa/3ROAC/wEfPvH3KOpvF1udSJEELGq6mhaivaAltyvH3Kv5l8MXD+dSH9fWglQ1QSqmiCdPrP/Xl4L0fVes6PEVsCe+/6SjE2guIOREmRQ3tkoPAH6gYrS5WR1INk680P0e8/H0x+vaThED5nkYgAAAABJRU5ErkJggg==" /></button>
  <button id="printButton" title="Print"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAACdElEQVR42qSSb2hNcRjHP+fcda/Ftjtb7TLUtiaWbEpIRnklhOSVQl4oSYkSSsoLyb/kjb2ZFxuFkazkZi/8S5GwFTPJ321uG3PPuef8zrnn3PM753hx3c2MN3zr6dfz/Pp+f8/veb5KGIb8D4p+L3R09RSpqronEomc/hspDEM2rpynjCa/xtU73ftevBwww9/g5mT4OaWF15JPwrXbDgVrth6cHYbh+A46unqqgTONDdWMaCZ9778AkNZNHEdSVzODzuRdgMyttuNvJ3wh58mjyxbUoaoKleUlNC+cQ87zcVzJ4LDOm/f9aLoRAocLHKUwxNYbj5uA7u0blpD6mubT4LfR12OxSVSUl3G25SJ2Nus2L10Ui8Wi7N6ySlEBjpy9shnorp1ZQURVqKqIs7ixnsY5NSxe0EDtrOn09L5F2DZN8xti5VMr6br3KL+FAycvrY+XlpyKRqO8+zhMakhH03Q0XUfPGGi6TiZj4OZyxMtKKY+X0df3Gt/38wJVVVU3d2xaTsnk2IR1ST/AkwGe5+N6ATnPp6XtBoYpiETUvEDWFjx49gbpeRimwBQ2hmkhLAshbIRlY2ezOI6L47rU19VgCoFpiryAl5M0za5mxrTKCR2Ydg7NcJibKAPAaYdEzwlMU2BZFgBqGAS6Zlh4/hjR8yEtJKm0ZDCdv+goiAqBIcSYlZXAPd/18PneeEWieEpxFEeC7YSMmJLvRoBuKTjtsO4nwTAF2awD0AlQNDyUSnoysfNd/7fi3t5Xf/T+OSU5lqyG9svXAXaNGmn/sQsrPvQP3JfSx/fzIaUcd/q+TxAEBZldT2+3toxz4r/ixwCdeWZj/16ekgAAAABJRU5ErkJggg==" /></button>
  <select id="visibleCategory">
    <xsl:choose>
      <xsl:when test="report/views">
        <xsl:for-each select="report/views/view">
          <option>
            <xsl:if test="position()=1">
              <xsl:attribute name="selected">selected</xsl:attribute>
            </xsl:if>
            <xsl:attribute name="data-invisibleCategories"><xsl:value-of select="invisibleCategories" /></xsl:attribute>
            <xsl:attribute name="data-rangeVisibilityType"><xsl:value-of select="rangeVisibilityType" /></xsl:attribute>
            <xsl:attribute name="id"><xsl:value-of select="id" /></xsl:attribute>
            <xsl:value-of select="name" />
          </option>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <option data-invisibleCategories="MAIN10,MAIN_422,RANGE_EXTENSIONS,COLOUR_EXTENSIONS,SPEC_EXTENSIONS,NAL_EXTENSIONS,SEI,IMPOSSIBLE" data-rangeVisibilityType="MAIN" id="mainopt" selected="selected">Main profile</option>
        <option data-invisibleCategories="MAIN,MAIN_422,RANGE_EXTENSIONS,COLOUR_EXTENSIONS,SPEC_EXTENSIONS,NAL_EXTENSIONS,SEI,IMPOSSIBLE" data-rangeVisibilityType="MAIN10" id="main10opt">Main10 profile</option>
        <option data-invisibleCategories="MAIN_422,SPEC_EXTENSIONS,NAL_EXTENSIONS,SEI,IMPOSSIBLE" data-rangeVisibilityType="REXT" id="rextopt">Main12 profile (RExt)</option>
        <option data-invisibleCategories="SPEC_EXTENSIONS,NAL_EXTENSIONS,SEI,IMPOSSIBLE" data-rangeVisibilityType="REXT" id="rextopt422">Main10 4:2:2 and Main12 4:2:2 profiles (RExt)</option>
        <option data-invisibleCategories="SEI,IMPOSSIBLE,RANGE_EXTENSIONS,COLOUR_EXTENSIONS,NAL_EXTENSIONS" data-rangeVisibilityType="REXT" id="specextopt">Main + Main10 + spec extensions</option>
        <option data-invisibleCategories="">All cases (including impossible cases)</option>
      </xsl:otherwise>
    </xsl:choose>
  </select>
  <span id="pleaseWait" style="color:red; display:none;">Please wait...</span>
  <span style="float:right">
    <xsl:if test="not(//noSummary)">
      <button id="toggleSummaryButton" style="height:25px; font-size:75%; opacity:0.4;">Summary</button>
    </xsl:if>
    <button id="toggleSpecButton" style="height:25px; font-size:75%; opacity:0.4;">Specification</button>
    <button id="togglePipelineDiagramButton" style="height:25px; font-size:75%; opacity:0.4;">Range coverage</button>
    <button id="toggleCrossPointsButton" style="height:25px; font-size:75%; opacity:0.4;">Cross coverage</button>
    <button id="toggleCrossValuePointsButton" style="height:25px; font-size:75%; opacity:0.4;">Value coverage</button>
  </span>
  </div>
  <div id="footerDiv" style="position:fixed; top:100%; margin-top:-3em; left:0; right:0px; height:3em; background:linear-gradient(to top, rgba(160, 224, 255, 64), rgba(255,255,255,0)); pointer-events:none;">
     <p style="text-align: center;">Copyright &#169; Argon Design 2013-2020</p>
  </div>
  <div id="innerScrollDiv" style="min-height:200%;">
  <ul id="mainBranchList" class="mainBranchList branchList" style="margin-top:7em;">
  <li class="testGroup categoryVisible">
  Total coverage:
  <span class="coverageReadout red">
    <span class="coveragePercentage" id="totalBranchCoverageSpan">0</span>%
    [<span class="actualCoverageValue" id="totalBranchCoverageActualSpan">0</span>/<span class="totalCoverageValue" id="totalBranchCoverageSpecifiedSpan">0</span>]
  </span>
  <ul id="topLevelList" style="width:50%;">
    <xsl:for-each select="report/testGroup">
        <xsl:sort select="id" data-type="number" />
        <xsl:apply-templates select="." mode="html"/>
    </xsl:for-each>
  </ul>
  </li>
  </ul>
  </div>
  </div>
  <div id="summaryDiv" style="display:none; position:fixed; width:45%; height:85%; left:52%; top:6em; overflow:auto; border:solid black 1px; background: rgba(255,255,255,0.7); padding:5px; font-family: sans-serif;">
    <div id="summaryStars"></div>
    <table>
      <tr><th>Coverage type</th><th>Coverage</th></tr>
      <tr><td>Branch coverage</td><td><span id="summaryBranchCoverage"></span>%</td></tr>
      <tr id="summaryRangeCoverageRow"><td>Range coverage</td><td><span id="summaryRangeCoverage"></span>%</td></tr>
      <tr><td>Value coverage</td><td><span id="summaryValueCoverage"></span>%</td></tr>
      <tr><td>Cross coverage</td><td><span id="summaryCrossCoverage"></span>%</td></tr>
      <tr style="font-weight: bold;"><td>Total coverage</td><td><span id="summaryTotalCoverage"></span>%</td></tr>
    </table>
  </div>
  <iframe id="specFrame" style="position:fixed; width:45%; height:85%; left:52%; top:6em; overflow:auto; border:solid black 1px; background: rgba(255,255,255,0.7);">
    <xsl:attribute name="src">
      <xsl:choose>
        <xsl:when test="report/codec">ArgonViewerV14/spec_<xsl:value-of select="$codecName" />.html</xsl:when>
        <xsl:otherwise>ArgonViewerV14/spec_h265.html</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </iframe>
  <div id="pipelineDiagramDiv" style="position:fixed; width:45%; height:85%; left:52%; top:6em; overflow:auto; border:solid black 1px; background: rgba(255,255,255,0.7);">
    <p><canvas id="pipelineCanv" width="600" height="750" style="display: block; margin-left:auto; margin-right:auto; margin-top: 15px; margin-bottom: 15px;" /></p>
    <div style="font-family: sans-serif; font-size: 85%" class="pipelineEquationTable">
      <div style="display: none">
        <xsl:for-each select="report/rangeEquationGroups/rangeEquationGroup/rangeEquations/rangeEquation">
          <xsl:apply-templates select="." />
        </xsl:for-each>
      </div>
      <script onerror="removeRangeButton();">
        var rangeEquationVisibilityTypes = {
        <xsl:for-each select="report/rangeEquationVisibilityTypes/rangeEquationVisibilityType">
          <xsl:apply-templates select="." />
        </xsl:for-each>
        };
        var rangeEquationGroups = {
          <xsl:for-each select="report/rangeEquationGroups/rangeEquationGroup">
            <xsl:apply-templates select="." />
          </xsl:for-each>
        };

        for (var groupName in rangeEquationGroups) {
          var idtag = groupName.toLowerCase();
          while (idtag.indexOf(" ")>=0) idtag = idtag.replace(" ", "");
          document.write("&lt;a href=\"javascript:void(0)\" onClick=\"scrollPipelineToTop()\">&lt;h1 id=\""+ idtag +"\">"+ groupName +"&lt;/h1>&lt;/a>");
          document.write("&lt;table border=\"1\">\n");
          document.write("&lt;tr>&lt;th rowspan=\"2\">Equation number&lt;/th>&lt;th rowspan=\"2\">&lt;/th>");
          for (var visId in rangeEquationVisibilityTypes) {
            document.write("&lt;th colspan=\"2\" class=\"rangecol "+ visId +"_col\">"+ rangeEquationVisibilityTypes[visId] +"&lt;/th>");
          }
          document.write("&lt;/tr>\n&lt;tr>");
          for (var visId in rangeEquationVisibilityTypes) {
            document.write("&lt;th style=\"width:6em; max-width: 6em\" class=\"rangecol "+ visId +"_col\">Minimum&lt;/th>&lt;th style=\"width:6em; max-width: 6em\" class=\"rangecol "+ visId +"_col\">Maximum&lt;/th>");
          }
          document.write("&lt;/tr>\n");
          var group = rangeEquationGroups[groupName];
          for (var i=0; i&lt;group.length; i++) {
            var eqn = group[i];
            document.write("&lt;tr>&lt;td style=\"width: 3em\">&lt;a href=\"javascript:void(0)\" onClick=\"highlightRangeEquation('"+ eqn.name +"')\" class=\"rangeEqnLink"+ eqn.name +"\">"+ eqn.name +"&lt;/a>&lt;/td>&lt;td style=\"width:70%; padding:0px 10px\">&lt;math xmlns=\"http://www.w3.org/1998/Math/MathML\" display=\"block\">"+ $("#rangeEquationMathML_"+eqn.name).html() +"&lt;/math>&lt;/td>");
            for (var visId in rangeEquationVisibilityTypes) {
              document.write("&lt;td class=\""+ (eqn.extension ? "extension":"uncovered")+" rangecol "+ visId +"_col\">&lt;span class=\"eqnMin"+ eqn.name +"\">"+ (eqn.extension ? "N/A (extension)":"" ) +"&lt;/span> (&lt;span class=\"eqnTheoreticalMin\">"+ eqn.theoreticalRanges[visId][0] +"&lt;/span>)&lt;/td>");
              document.write("&lt;td class=\""+ (eqn.extension ? "extension":"uncovered")+" rangecol "+ visId +"_col\">&lt;span class=\"eqnMax"+ eqn.name +"\">"+ (eqn.extension ? "N/A (extension)":"" ) +"&lt;/span> (&lt;span class=\"eqnTheoreticalMax\">"+ eqn.theoreticalRanges[visId][1] +"&lt;/span>)&lt;/td>");
            }
            document.write("&lt;/tr>\n");
          }
          document.write("&lt;/table>");
        }
      </script>
    </div>
  </div>
  <div id="crossPointsDiv" class="cCrossCoverDiv" style="display:none; position:fixed; width:45%; height:85%; left:52%; top:6em; overflow:auto; border:solid black 1px; background: rgba(255,255,255,0.7); padding:5px; font-family: sans-serif;">
    <p><b>Selected cross point:</b><span style="padding-left: 20px"></span><select id="crossPointSelector">
      <xsl:for-each select="report/crossPoint[order>1]">
        <xsl:apply-templates select="." mode="html"/>
      </xsl:for-each>
    </select> <button onClick="crossPrev()">Previous</button> <button onClick="crossNext()">Next</button></p>
    <p>
      <div id="crossCoverHeading" style="clear:both; font-size:150%; font-weight:bold;"></div>
      <canvas id="crossCoverCanv" width="490" height="18000" style="display:none; margin-left:auto; margin-right:auto;"/>
      <div id="crossCoverDiv"></div>
    </p>
  </div>
  <div id="crossValuePointsDiv" class="cCrossCoverDiv" style="display:none; position:fixed; width:45%; height:85%; left:52%; top:6em; overflow:auto; border:solid black 1px; background: rgba(255,255,255,0.7); padding:5px; font-family: sans-serif;">
    <p><b>Selected value point:</b><span style="padding-left: 20px"></span><select id="crossValuePointSelector">
      <xsl:for-each select="report/crossPoint[order=1]">
        <xsl:apply-templates select="." mode="html"/>
      </xsl:for-each>
    </select> <button onClick="crossValuePrev()">Previous</button> <button onClick="crossValueNext()">Next</button></p>
    <p>
      <div id="crossValueCoverHeading" style="clear:both; font-size:150%; font-weight:bold;"></div>
      <canvas id="crossValueCoverCanv" width="490" height="18000" style="display:none; margin-left:auto; margin-right:auto;"/>
      <div id="crossValueCoverDiv"></div>
    </p>
  </div>
  </body>
  </html>
</xsl:template>

<xsl:template match="testGroup" mode="html">
  <li class="collapseClosed incompleteCoverageLi testGroup categoryVisible" data-testName="{name}">
  <xsl:attribute name="data-testIdentifier"><xsl:if test="id">ID<xsl:value-of select="id" />_</xsl:if><xsl:value-of select="name" /></xsl:attribute>
  <xsl:if test="rangeEquation">
    <xsl:attribute name="data-rangeEquation"><xsl:value-of select="rangeEquation" /></xsl:attribute>
  </xsl:if>
  <xsl:if test="equation">
    <xsl:attribute name="data-equationNumber"><xsl:value-of select="translate(equation,'.','_')" /></xsl:attribute>
  </xsl:if>
  <xsl:if test="function">
    <xsl:attribute name="data-functionName"><xsl:value-of select="function" /></xsl:attribute>
  </xsl:if>
  <xsl:if test="equation">
    <span id="equationAnchorSpan_{translate(equation,'.','_')}" />
  </xsl:if>
  <xsl:if test="function">
    <span id="functionAnchorSpan_{function}" />
  </xsl:if>
  <xsl:if test="section">
    <span id="sectionAnchorSpan_{translate(section,'.','_')}" />
  </xsl:if>
  <span class="coverageReadout red">
    <span class="coveragePercentage">0</span>%
    [<span class="actualCoverageValue">0</span>/<span class="totalCoverageValue">0</span>]
    <xsl:if test="id"><xsl:value-of select="id"/>. </xsl:if><span style="font-weight:bold"><xsl:value-of select="name"/></span>
    <xsl:text> </xsl:text>
    <span class="rightArrowButton">&gt;</span>
  </span>
  <ul class="subList incompleteCoverage" style="display:none">
    <xsl:for-each select="./testPoint">
        <xsl:sort select="./id" data-type="number" />
        <xsl:apply-templates select="." mode="html"/>
    </xsl:for-each>
    <xsl:for-each select="./testGroup">
        <xsl:sort select="./id" data-type="number" />
        <xsl:apply-templates select="." mode="html"/>
    </xsl:for-each>
  </ul></li>
</xsl:template>

<xsl:template match="testPoint" mode="html">
  <xsl:variable name="allTestCases" select="count(.//coverage)" />
  <xsl:variable name="booleanTestCases" select="count(.//coverage[specifiedMin='0' and specifiedMax='1'])" />
  <xsl:variable name="totalTestCases" select="$allTestCases+$booleanTestCases" />
  <xsl:variable name="testCasesCovered" select="count(.//coverage/actualMin)" />
  <xsl:variable name="booleanTestCasesCovered" select="count(.//coverage[specifiedMin='0' and specifiedMax='1' and actualMin='0' and number(actualMax)>=1])" />
  <xsl:variable name="totalTestCasesCovered" select="$testCasesCovered+$booleanTestCasesCovered" />
  <li data-testName="{name}">
  <xsl:attribute name="data-testIdentifier"><xsl:if test="id">ID<xsl:value-of select="id" />_</xsl:if><xsl:value-of select="name" /></xsl:attribute>
  <xsl:attribute name="data-specifiedMin"><xsl:value-of select="coverage/specifiedMin" /></xsl:attribute>
  <xsl:attribute name="data-specifiedMax"><xsl:value-of select="coverage/specifiedMax" /></xsl:attribute>
  <xsl:attribute name="data-testCases"><xsl:value-of select="$totalTestCases" /></xsl:attribute>
  <xsl:attribute name="data-testCasesCovered"><xsl:value-of select="$totalTestCasesCovered" /></xsl:attribute>
  <xsl:if test="coverage/actualMin">
    <xsl:attribute name="data-actualMin"><xsl:value-of select="coverage/actualMin" /></xsl:attribute>
    <xsl:attribute name="data-actualMax"><xsl:value-of select="coverage/actualMax" /></xsl:attribute>
    <xsl:attribute name="data-minCoveringStream"><xsl:value-of select="coverage/minCoveringStream" /></xsl:attribute>
    <xsl:attribute name="data-maxCoveringStream"><xsl:value-of select="coverage/maxCoveringStream" /></xsl:attribute>
  </xsl:if>
  <xsl:attribute name="class">
    categoryVisible
    <xsl:choose>
      <xsl:when test="$booleanTestCases!=0">
          collapseClosed incompleteCoverageLi testGroup
      </xsl:when>
      <xsl:otherwise>
        testPoint
      </xsl:otherwise>
    </xsl:choose>
  </xsl:attribute>
  <xsl:choose>
    <xsl:when test="visibilityCondition">
      <xsl:attribute name="data-visibilityCondition">
        <xsl:value-of select="visibilityCondition" />
      </xsl:attribute>
    </xsl:when>
    <xsl:otherwise>
      <xsl:if test="visibilityConditionTrue and visibilityConditionFalse">
        <xsl:attribute name="data-visibilityCondition">
          (<xsl:value-of select="visibilityConditionTrue" />) || (<xsl:value-of select="visibilityConditionFalse" />)
        </xsl:attribute>
      </xsl:if>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:if test="rangeEquation">
    <xsl:attribute name="data-rangeEquation"><xsl:value-of select="rangeEquation" /></xsl:attribute>
  </xsl:if>

  <xsl:if test="equation">
    <xsl:attribute name="data-equationNumber"><xsl:value-of select="translate(equation,'.','_')" /></xsl:attribute>
  </xsl:if>
  <xsl:if test="function">
    <xsl:attribute name="data-functionName"><xsl:value-of select="function" /></xsl:attribute>
  </xsl:if>
  <xsl:if test="equation">
    <span id="equationAnchorSpan_{translate(equation,'.','_')}" />
  </xsl:if>
  <xsl:if test="function">
    <span id="functionAnchorSpan_{function}" />
  </xsl:if>
  <xsl:if test="section">
    <span id="sectionAnchorSpan_{translate(section,'.','_')}" />
  </xsl:if>
  <span data-totalCoverageValue="{$totalTestCases}">
    <xsl:attribute name="class">
      coverageReadout
      <xsl:choose>
        <xsl:when test="$totalTestCasesCovered > 0">
          <xsl:choose>
            <xsl:when test="$totalTestCasesCovered = $totalTestCases">
              green
            </xsl:when>
            <xsl:otherwise>
              orange
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          red
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <span class="coveragePercentage">
    <xsl:value-of select="($totalTestCasesCovered div $totalTestCases)*100" />
    </span>%
    [
      <span class="actualCoverageValue"><xsl:value-of select="$totalTestCasesCovered" /></span>/<span class="totalCoverageValue"><xsl:value-of select="$totalTestCases" /></span>,
      <span class="actualMin">
        <xsl:if test="coverage/minCoveringStream">
          <xsl:attribute name="title"><xsl:value-of select="coverage/minCoveringStream" /></xsl:attribute>
        </xsl:if>
        min=
        <xsl:if test="coverage/actualMin">
          <xsl:value-of select="coverage/actualMin" />
        </xsl:if>
      </span>
      <xsl:text> </xsl:text>
      <span class="actualMax">
        <xsl:if test="coverage/maxCoveringStream">
          <xsl:attribute name="title"><xsl:value-of select="coverage/maxCoveringStream" /></xsl:attribute>
        </xsl:if>
        max=
        <xsl:if test="coverage/actualMax">
          <xsl:value-of select="coverage/actualMax" />
        </xsl:if>
      </span>
    ]
    <xsl:if test="id"><xsl:value-of select="id"/>. </xsl:if><span style="font-weight:bold"><xsl:value-of select="name"/></span><xsl:text> </xsl:text>
    <span class="rightArrowButton">&gt;</span>
  </span>
  <xsl:if test="$booleanTestCases!=0">
  <ul style="display:none" class="subList incompleteCoverage">
    <li data-testCases="1">
    <xsl:choose>
      <xsl:when test="coverage/actualMax and coverage/actualMax > 0">
        <xsl:attribute name="data-testCasesCovered">1</xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="data-testCasesCovered">0</xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="coverage/nonzeroCoveringStream">
      <xsl:attribute name="title"><xsl:value-of select="coverage/nonzeroCoveringStream" /></xsl:attribute>
    </xsl:if>
    <xsl:if test="visibilityConditionTrue">
      <xsl:attribute name="data-visibilityCondition">
        <xsl:value-of select="visibilityConditionTrue" />
      </xsl:attribute>
    </xsl:if>
    <xsl:attribute name="class">
      testPoint categoryVisible
      <xsl:choose>
        <xsl:when test="coverage/actualMax and coverage/actualMax > 0">
          green
        </xsl:when>
        <xsl:otherwise>
          red
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    true<xsl:text> </xsl:text><span class="rightArrowButton">&gt;</span></li>
    <li data-testCases="1">
    <xsl:choose>
      <xsl:when test="coverage/actualMin and coverage/actualMin = 0">
        <xsl:attribute name="data-testCasesCovered">1</xsl:attribute>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="data-testCasesCovered">0</xsl:attribute>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:if test="coverage/zeroCoveringStream">
      <xsl:attribute name="title"><xsl:value-of select="coverage/zeroCoveringStream" /></xsl:attribute>
    </xsl:if>
    <xsl:if test="visibilityConditionFalse">
      <xsl:attribute name="data-visibilityCondition">
        <xsl:value-of select="visibilityConditionFalse" />
      </xsl:attribute>
    </xsl:if>
    <xsl:attribute name="class">
      testPoint categoryVisible
      <xsl:choose>
        <xsl:when test="coverage/actualMin and coverage/actualMin = 0">
          green
        </xsl:when>
        <xsl:otherwise>
          red
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    false<xsl:text> </xsl:text><span class="rightArrowButton">&gt;</span></li>
  </ul>
  </xsl:if>
  </li>
</xsl:template>

<xsl:template match="crossPoint" mode="html">
  <option>
    <xsl:attribute name="id">
      <xsl:value-of select="name" />
    </xsl:attribute>
    <xsl:value-of select="name" />
  </option>
</xsl:template>

<xsl:template match="crossPoint" mode="js">
  '<xsl:value-of select="name" />': {
    order:<xsl:value-of select="order" />,
    bitfield:'<xsl:value-of select="bitfield" />',
    impossibleCasesBitfield:{
      <xsl:for-each select="./impossibleCasesBitfield">
        <xsl:value-of select="@view" />: '<xsl:value-of select="." />',
      </xsl:for-each>
    },
    axes:[
      <xsl:for-each select="./component">
        {
          name: '<xsl:value-of select="name" />',
          low: <xsl:value-of select="low" />,
          high: <xsl:value-of select="high" />,
          step:
          <xsl:choose>
            <xsl:when test="powerOf2Step">
              -1
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="linearStep" />
            </xsl:otherwise>
          </xsl:choose>
        },
      </xsl:for-each>
    ]
  },
</xsl:template>

<xsl:template match="rangeEquationVisibilityType">
  '<xsl:value-of select="id" />': '<xsl:value-of select="name" />',
</xsl:template>

<xsl:template match="rangeEquation">
  <div style="display:none">
    <xsl:attribute name="id">rangeEquationMathML_<xsl:value-of select="name" /></xsl:attribute>
    <xsl:copy-of select="mathml/node()" />
  </div>
</xsl:template>

<xsl:template match="rangeEquationGroup">
  '<xsl:value-of select="name" />': [
  <xsl:for-each select="./rangeEquations/rangeEquation">
    {
      'name': '<xsl:value-of select="name" />',
      'extension': <xsl:choose><xsl:when test="./extension">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose>,
      'theoreticalRanges': {
        <xsl:for-each select="./theoreticalRange">
          '<xsl:value-of select="@type" />': [<xsl:value-of select="min" />, <xsl:value-of select="max" />],
        </xsl:for-each>
      }
    },
  </xsl:for-each>
  ],
</xsl:template>
</xsl:stylesheet>
