/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

summary={}
summary.firstInitialisation=true;

summary.setCoverage=function(id, value, complete) {
    var elem=$(id);
    elem.html(value.toFixed(1));
    if (complete) elem.parent().css("background-color","#efe");
    else elem.parent().css("background-color","#fee");
};

summary.updateSummary=function() {
    var coverageValues=new Array();
    var coverageComplete=new Array();
    if (isSummary) {
        var sa = summaryAttributes[$("#visibleCategory").find(":selected").attr("id")];
        coverageValues[0] = parseFloat(sa.branchPercentage);
        coverageValues[1] = parseFloat(sa.rangePercentage);
        coverageValues[2] = parseFloat(sa.valuePercentage);
        coverageValues[3] = parseFloat(sa.crossPercentage);
        for (var k=0; k < coverageValues.length; k++)
          coverageComplete[k] = (coverageValues[k] >= 100);
        summary.setCoverage("#summaryBranchCoverage", coverageValues[0], coverageComplete[0]);
        summary.setCoverage("#summaryRangeCoverage",  coverageValues[1], coverageComplete[1]);
        summary.setCoverage("#summaryValueCoverage",  coverageValues[2], coverageComplete[2]);
        summary.setCoverage("#summaryCrossCoverage",  coverageValues[3], coverageComplete[3]);

    }
    else {
      if (coverageReady!=7) {
          setTimeout(summary.updateSummary,100);
          return;
      }
      var coverageIdx=0;
      coverageValues[coverageIdx]=parseFloat($("#totalBranchCoverageSpan").text());
      coverageComplete[coverageIdx]=($("#totalBranchCoverageActualSpan").text()==$("#totalBranchCoverageSpecifiedSpan").text());
      summary.setCoverage("#summaryBranchCoverage", coverageValues[coverageIdx], coverageComplete[coverageIdx]);
      var rangeCoverageActive=$("#togglePipelineDiagramButton").is(":visible");
      if (rangeCoverageActive) {
        var visibilityType=getRangeVisibilityType();
        var rangeCoverageClass="."+visibilityType+"_col";
        var rangeCoverageBoxesCovered=$(".pipelineEquationTable td.covered"+rangeCoverageClass).length;
        var rangeCoverageBoxesTotal=rangeCoverageBoxesCovered+($(".pipelineEquationTable td.uncovered"+rangeCoverageClass).add(".pipelineEquationTable td.supercovered"+rangeCoverageClass).length);
        coverageIdx++;
        coverageValues[coverageIdx]=(100.0*rangeCoverageBoxesCovered)/rangeCoverageBoxesTotal;
        coverageComplete[coverageIdx]=(rangeCoverageBoxesCovered==rangeCoverageBoxesTotal);
        summary.setCoverage("#summaryRangeCoverage", coverageValues[coverageIdx], coverageComplete[coverageIdx]);
        $("#summaryRangeCoverageRow").show();
      }
      else {
        $("#summaryRangeCoverageRow").hide();
      }
      coverageIdx++;
      coverageValues[coverageIdx]=(100.0*crosscover.coveredValuePoints)/crosscover.totalValuePoints;
      coverageComplete[coverageIdx]=(crosscover.coveredValuePoints==crosscover.totalValuePoints);
      summary.setCoverage("#summaryValueCoverage", coverageValues[coverageIdx], coverageComplete[coverageIdx]);
      coverageIdx++;
      coverageValues[coverageIdx]=(100.0*crosscover.coveredCrossPoints)/crosscover.totalCrossPoints;
      coverageComplete[coverageIdx]=(crosscover.coveredCrossPoints==crosscover.totalCrossPoints);
      summary.setCoverage("#summaryCrossCoverage", coverageValues[coverageIdx], coverageComplete[coverageIdx]);
      coverageIdx++;
    }
    var totalIdx = coverageValues.length;
    coverageValues[totalIdx]=0;
    coverageComplete[totalIdx]=true;
    var validValueCount=0;
    for (var k=0; k<totalIdx; k++) {
      if (!isNaN(coverageValues[k])) {
        coverageValues[totalIdx]+=coverageValues[k];
        validValueCount++;
        if (!coverageComplete[k]) coverageComplete[totalIdx]=false;
      }
    }
    coverageValues[totalIdx]/=validValueCount;
    summary.setCoverage("#summaryTotalCoverage", coverageValues[totalIdx], coverageComplete[totalIdx]);
    var stars=0;
    for (var k=0; k<coverageValues.length; k++) {
        if (!isNaN(coverageValues[k]) && coverageComplete[k]) stars++;
    }
    var starHTML="";
    for (var k=0; k<=validValueCount; k++) {
        if (k<stars) starHTML+="<span class=\"summaryFullStar\">&#9733;</span>";
        else starHTML+="<span class=\"summaryEmptyStar\">&#9734;</span>";
    }
    $("#summaryStars").html(starHTML);
    if (summary.firstInitialisation && !noSummary) {
        showSidePanel("summary", true);
        summary.firstInitialisation=false;
    }
};
