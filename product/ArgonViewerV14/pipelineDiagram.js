/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

function roundedRect(ctx, x, y, w, h, r) {
  ctx.beginPath();
  ctx.moveTo(x+r,y);
  ctx.lineTo(x+w-r,y);
  ctx.arcTo(x+w,y,x+w,y+r,r);
  ctx.lineTo(x+w,y+h-r);
  ctx.arcTo(x+w,y+h,x+w-r,y+h,r);
  ctx.lineTo(x+r,y+h);
  ctx.arcTo(x,y+h,x,y+h-r,r);
  ctx.lineTo(x,y+r);
  ctx.arcTo(x,y,x+r,y,r);
  ctx.fill();
  ctx.stroke();
}

function arrowHead(ctx, xHead, yHead, dir) {
  ctx.fillStyle="black";
  ctx.beginPath();
  ctx.moveTo(xHead,yHead);
  if (dir=="d") {
    ctx.lineTo(xHead-5,yHead-10);
    ctx.lineTo(xHead+5,yHead-10);
  }
  else if (dir=="l") {
    ctx.lineTo(xHead+10,yHead-5);
    ctx.lineTo(xHead+10,yHead+5);
  }
  else if (dir=="u") {
    ctx.lineTo(xHead+5,yHead+10);
    ctx.lineTo(xHead-5,yHead+10);
  }
  else if (dir=="r") {
    ctx.lineTo(xHead-10,yHead+5);
    ctx.lineTo(xHead-10,yHead-5);
  }
  ctx.closePath();
  ctx.fill();
}

var sectionBoxes;
var bgBoxes;
var rectRadius=5;

function setupSectionBoxes(codecName) {
  if (codecName == "h265") {
    sectionBoxes =[
      { rect: [ 15,40,140,40 ], text: "Filter edge samples", swell: 0, elemid: "filteredgesamples", active: true },
      { rect: [ 15,100,140,40 ], text: "Intra prediction", swell: 0, elemid: "intraprediction", active: true },

      { rect: [ 175,40,140,40 ], text: "Horizontal filter", swell: 0, elemid: "horizontalfilter", active: true },
      { rect: [ 175,100,140,40 ], text: "Vertical filter", swell: 0, elemid: "verticalfilter", active: true },
      { rect: [ 175,160,140,40 ], text: "Weighted prediction", swell: 0, elemid: "weightedprediction", active: true },

      { rect: [ 335,40,140,40 ], text: "Scaling", swell: 0, elemid: "scaling", active: true },
      { rect: [ 335,100,140,40 ], text: "Vertical transform", swell: 0, elemid: "verticaltransform", active: true },
      { rect: [ 335,160,140,40 ], text: "Clipping", swell: 0, elemid: "clipping", active: true },
      { rect: [ 335,220,140,40 ], text: "Horizontal transform", swell: 0, elemid: "horizontaltransform", active: true },
      { rect: [ 335,280,140,40 ], text: "Bit depth scaling", swell: 0, elemid: "bitdepthscaling", active: true },
      { rect: [ 335,340,140,40 ], text: "Directional residual modification", swell: 0, elemid: "directionalresidualmodification", active: true },
      { rect: [ 335,400,140,40 ], text: "Cross component prediction", swell: 0, elemid: "crosscomponentprediction", active: true },

      { rect: [ 175,460,140,40 ], text: "Picture construction", swell: 0, elemid: "pictureconstruction", active: true },

      { rect: [ 100,570,90,40 ], text: "Strong filter", swell: 0, elemid: "strongfilter", active: true },
      { rect: [ 200,570,90,40 ], text: "Weak filter", swell: 0, elemid: "weakfilter", active: true },
      { rect: [ 300,570,90,40 ], text: "Chroma filter", swell: 0, elemid: "chromafilter", active: true },

      { rect: [ 150,690,90,40 ], text: "Edge type", swell: 0, elemid: "edgetype", active: true },
      { rect: [ 250,690,90,40 ], text: "Band type", swell: 0, elemid: "bandtype", active: true },

      { rect: [ 435,570,90,40 ], text: "Image", swell: 0, elemid: "image", active: true },
    ];
    bgBoxes = [
      { action: "roundedRect", params:[10,10,150,150,rectRadius], active: true },
      { action: "roundedRect", params:[170,10,150,210,rectRadius], active: true },
      { action: "roundedRect", params:[330,10,150,450,rectRadius], active: true },
      { action: "roundedRect", params:[80,540,330,80,rectRadius], active: true },
      { action: "roundedRect", params:[130,660,230,80,rectRadius], active: true },
      { action: "roundedRect", params:[430,540,100,80,rectRadius], active: true },
      { action: "path", params:["m", 85,80,
                                "l", 85,480-rectRadius,
                                "a", 85,480,85+rectRadius,480,rectRadius,
                                "l", 175,480
                                //"a", 225,380,225,380+rectRadius,rectRadius,
                                //"l", 225,460
                                ] },
      { action: "path", params:["m", 245,80,
                                "l", 245,540
                                ] },
      { action: "path", params:["m", 405,80,
                                "l", 405,480-rectRadius,
                                "a", 405,480, 405-rectRadius,480,rectRadius,
                                "l", 265+rectRadius,480
                                //"a", 265,480,265,480+rectRadius,rectRadius,
                                //"l", 265,500
                                ] },
      { action: "path", params:["m", 245,620,
                                "l", 245,660
                                ] },
      { action: "arrowhead_r", params:[ 175,480 ] },
      { action: "arrowhead_d", params:[ 245,460 ] },
      { action: "arrowhead_l", params:[ 315,480 ] },
      { action: "arrowhead_d", params:[ 245,540 ] },
      { action: "arrowhead_d", params:[ 245,660 ] },
      { action: "text", params:[ "Intra decoding",15,28 ] },
      { action: "text", params:[ "Inter decoding",175,28 ] },
      { action: "text", params:[ "Residual decoding",335,28 ] },
      { action: "text", params:[ "Deblocking",85,558 ] },
      { action: "text", params:[ "Sample adaptive offset",135,678 ] },
      { action: "text", params:[ "Misc",435,558 ] },
    ];
    $(document).ready(function() {
      $("#pipelineCanv").height(750);
    });
  }
  else if (codecName == "vp9") {
    sectionBoxes =[
      { rect: [ 15,40,140,40 ], text: "Intra prediction", swell: 0, elemid: "intraprediction", active: true },

      { rect: [ 175,40,140,40 ], text: "Horizontal filter", swell: 0, elemid: "horizontalfilter", active: true },
      { rect: [ 175,100,140,40 ], text: "Vertical filter", swell: 0, elemid: "verticalfilter", active: true },
      { rect: [ 175,160,140,40 ], text: "Weighted prediction", swell: 0, elemid: "weightedprediction", active: true },

      { rect: [ 335,40,140,40 ], text: "Scaling", swell: 0, elemid: "scaling", active: true },
      { rect: [ 335,100,140,40 ], text: "Vertical transform", swell: 0, elemid: "verticalidct4", active: true },
      { rect: [ 335,160,140,40 ], text: "Horizontal transform", swell: 0, elemid: "horizontalidct4", active: true },

      { rect: [ 175,400,140,40 ], text: "Picture construction", swell: 0, elemid: "pictureconstruction", active: true },

      { rect: [ 100,510,90,40 ], text: "filter4()", swell: 0, elemid: "filter4", active: true },
      { rect: [ 200,510,90,40 ], text: "filter8()", swell: 0, elemid: "filter8", active: true },
      { rect: [ 300,510,90,40 ], text: "filter16()", swell: 0, elemid: "filter16", active: true },
    ];
    bgBoxes = [
      { action: "roundedRect", params:[10,10,150,90,rectRadius], active: true },
      { action: "roundedRect", params:[170,10,150,210,rectRadius], active: true },
      { action: "roundedRect", params:[330,10,150,210,rectRadius], active: true },
      { action: "roundedRect", params:[80,480,330,80,rectRadius], active: true },
      { action: "path", params:["m", 85,80,
                                "l", 85,380-rectRadius,
                                "a", 85,380,85+rectRadius,380,rectRadius,
                                "l", 225-rectRadius,380,
                                "a", 225,380,225,380+rectRadius,rectRadius,
                                "l", 225,400
                                ] },
      { action: "path", params:["m", 245,80,
                                "l", 245,480
                                ] },
      { action: "path", params:["m", 405,80,
                                "l", 405,380-rectRadius,
                                "a", 405,380, 405-rectRadius,380,rectRadius,
                                "l", 265+rectRadius,380,
                                "a", 265,380,265,380+rectRadius,rectRadius,
                                "l", 265,400
                                ] },
      { action: "arrowhead_d", params:[ 225,400 ] },
      { action: "arrowhead_d", params:[ 245,400 ] },
      { action: "arrowhead_d", params:[ 265,400 ] },
      { action: "arrowhead_d", params:[ 245,480 ] },
      { action: "text", params:[ "Intra decoding",15,28 ] },
      { action: "text", params:[ "Inter decoding",175,28 ] },
      { action: "text", params:[ "Residual decoding",335,28 ] },
      { action: "text", params:[ "Deblocking",85,498 ] },
    ];
    $(document).ready(function() {
      $("#pipelineCanv").attr("height",750);
    });
  }
  else if (codecName == "av1b") {
    sectionBoxes =[
      { rect: [ 15,40,140,40 ], text: "Basic", swell: 0, elemid: "basicintraprediction", active: true },
      { rect: [ 15,100,140,40 ], text: "Recursive", swell: 0, elemid: "recursiveintraprediction", active: true },
      { rect: [ 15,160,140,40 ], text: "Directional", swell: 0, elemid: "directionalintraprediction", active: true },
      { rect: [ 15,220,140,40 ], text: "DC", swell: 0, elemid: "dcintraprediction", active: true },
      { rect: [ 15,280,140,40 ], text: "Smooth", swell: 0, elemid: "smoothintraprediction", active: true },
      { rect: [ 15,340,140,40 ], text: "Filter corner", swell: 0, elemid: "filtercornerprocess", active: true },
      { rect: [ 15,400,140,40 ], text: "Intra edge upsample", swell: 0, elemid: "intraedgeupsampleprocess", active: true },
      { rect: [ 15,460,140,40 ], text: "Intra edge filter", swell: 0, elemid: "intraedgefilterprocess", active: true },
      { rect: [ 15,520,140,40 ], text: "Chroma from luma", swell: 0, elemid: "chromafromluma", active: true },

      { rect: [ 175,40,140,40 ], text: "Block", swell: 0, elemid: "blockinterprediction", active: true },
      { rect: [ 175,100,140,40 ], text: "Warp", swell: 0, elemid: "blockwarpprocess", active: true },
      { rect: [ 175,160,140,40 ], text: "Setup shear", swell: 0, elemid: "setupshearprocess", active: true },
      { rect: [ 175,220,140,40 ], text: "Difference weighted mask", swell: 0, elemid: "differenceweightedmask", active: true },
      { rect: [ 175,280,140,40 ], text: "Blending", swell: 0, elemid: "blending", active: true },

      { rect: [ 335,40,140,40 ], text: "Scaling", swell: 0, elemid: "scaling", active: true },
      { rect: [ 335,100,140,40 ], text: "Butterfly rotation", swell: 0, elemid: "butterflyrotation", active: true },
      { rect: [ 335,160,140,40 ], text: "Hadamard rotation", swell: 0, elemid: "hadamardrotation", active: true },
      { rect: [ 335,220,140,40 ], text: "Inverse ADST4", swell: 0, elemid: "inverseadst4process", active: true },
      { rect: [ 335,280,140,40 ], text: "Inverse WHT", swell: 0, elemid: "inversewalsh-hadamardtransformprocess", active: true },
      { rect: [ 335,340,140,40 ], text: "Inverse IDT", swell: 0, elemid: "inverseidentitytransformprocess", active: true },
      { rect: [ 335,400,140,40 ], text: "2D inverse transform", swell: 0, elemid: "2dinversetransformprocess", active: true },

      { rect: [ 175,640,140,40 ], text: "Picture construction", swell: 0, elemid: "pictureconstruction", active: true },

      { rect: [ 100,750,140,40 ], text: "Narrow filter", swell: 0, elemid: "narrowfilter", active: true },
      { rect: [ 250,750,140,40 ], text: "Wide filter", swell: 0, elemid: "widefilter", active: true },

      { rect: [ 100,870,140,40 ], text: "Direction", swell: 0, elemid: "cdefdirectionprocess", active: true },
      { rect: [ 250,870,140,40 ], text: "Filter", swell: 0, elemid: "cdeffilterprocess", active: true },

      { rect: [ 175,960,140,40 ], text: "Upscaling", swell: 0, elemid: "upscalingprocess", active: true },

      { rect: [ 100,1070,140,40 ], text: "Self-guided filter", swell: 0, elemid: "self-guidedfilterprocess", active: true },
      { rect: [ 250,1070,140,40 ], text: "Wiener filter", swell: 0, elemid: "wienerfilterprocess", active: true },
    ];
    bgBoxes = [
      { action: "roundedRect", params:[10,10,150,570,rectRadius], active: true },
      { action: "roundedRect", params:[170,10,150,320,rectRadius], active: true },
      { action: "roundedRect", params:[330,10,150,450,rectRadius], active: true },
      { action: "roundedRect", params:[80,720,330,80,rectRadius], active: true },
      { action: "roundedRect", params:[80,840,330,80,rectRadius], active: true },
      { action: "roundedRect", params:[80,1040,330,80,rectRadius], active: true },
      { action: "path", params:["m", 85,80,
                                "l", 85,600-rectRadius,
                                "a", 85,600,85+rectRadius,600,rectRadius,
                                "l", 225-rectRadius,600,
                                "a", 225,600,225,600+rectRadius,rectRadius,
                                "l", 225,640
                                ] },
      { action: "path", params:["m", 245,80,
                                "l", 245,720
                                ] },
      { action: "path", params:["m", 245,800,
                                "l", 245,840
                                ] },
      { action: "path", params:["m", 245,920,
                                "l", 245,960
                                ] },
      { action: "path", params:["m", 245,1000,
                                "l", 245,1040
                                ] },
      { action: "path", params:["m", 405,80,
                                "l", 405,600-rectRadius,
                                "a", 405,600, 405-rectRadius,600,rectRadius,
                                "l", 265+rectRadius,600,
                                "a", 265,600,265,600+rectRadius,rectRadius,
                                "l", 265,640
                                ] },
      { action: "arrowhead_d", params:[ 225,640 ] },
      { action: "arrowhead_d", params:[ 245,640 ] },
      { action: "arrowhead_d", params:[ 265,640 ] },
      { action: "arrowhead_d", params:[ 245,720 ] },
      { action: "arrowhead_d", params:[ 245,840 ] },
      { action: "arrowhead_d", params:[ 245,960 ] },
      { action: "arrowhead_d", params:[ 245,1040 ] },
      { action: "text", params:[ "Intra decoding",15,28 ] },
      { action: "text", params:[ "Inter decoding",175,28 ] },
      { action: "text", params:[ "Residual decoding",335,28 ] },
      { action: "text", params:[ "Deblocking",85,738 ] },
      { action: "text", params:[ "CDEF",85,858 ] },
      { action: "text", params:[ "Loop restoration",85,1058 ] },
    ];
    $(document).ready(function() {
      $("#pipelineCanv").attr("height",1130);
    });
  }
  else console.log("ERR: unknown codec name "+codecName);
}

var mouseX=0, mouseY=0;
var mouseOverIndex=-1;
var swellTimeoutSet=false;
var maxSwell=2;
function mouseMoved(e) {
  mouseX=e.clientX-$(pipelineCanvas).offset().left; mouseY=e.clientY-$(pipelineCanvas).offset().top;
  drawPipelineDiagram(false);
}

function pcClick(e) {
  var clickX=e.clientX-$(pipelineCanvas).offset().left;
  var clickY=e.clientY-$(pipelineCanvas).offset().top;
  for (var i=0; i<sectionBoxes.length; ++i) {
    var b=sectionBoxes[i];
    var rx=b.rect[0];
    var ry=b.rect[1];
    var rw=b.rect[2];
    var rh=b.rect[3];
    var mouseInBox=clickX>rx && clickX<rx+rw && clickY>ry && clickY<ry+rh;
    if (mouseInBox) {
      $("#pipelineDiagramDiv").scrollTop($("#pipelineDiagramDiv").scrollTop()+$("#"+b.elemid).position().top);
    }
  }
}

function initPipelineDiagram() {
  pipelineCanvas=document.getElementById("pipelineCanv");
  pipelineCanvas.onmousemove=mouseMoved;
  pipelineCanvas.onclick=pcClick;
  drawPipelineDiagram(true);
}

function updateSwell() {
  var needsDraw=false;
  for (var i=0; i<sectionBoxes.length; ++i) {
    var b=sectionBoxes[i];
    if (!b.active) continue;
    var rx=b.rect[0];
    var ry=b.rect[1];
    var rw=b.rect[2];
    var rh=b.rect[3];
    var mouseInBox=mouseX>rx && mouseX<rx+rw && mouseY>ry && mouseY<ry+rh;
    if (mouseInBox && b.swell<maxSwell) { b.swell++; needsDraw=true; }
    if (!mouseInBox && b.swell>0) { b.swell--; needsDraw=true; }
  }
  if (needsDraw) {
    setTimeout(updateSwell,40);
    swellTimeoutSet=true;
    drawPipelineDiagram(false);
  }
  else swellTimeoutSet=false;
}

function wrapText(context, text, cx, cy, maxWidth, lineHeight, underline) {
  var words = text.split(' ');
  var lines=[];
  var line = '';

  for(var n = 0; n < words.length; n++) {
    var testLine = line + words[n] + ' ';
    var metrics = context.measureText(testLine);
    var testWidth = metrics.width;
    if (testWidth > maxWidth && n > 0) {
      lines.push(line.substring(0, line.length-1));
      line = words[n] + ' ';
    }
    else {
      line = testLine;
    }
  }
  lines.push(line.substring(0, line.length-1));
  cy-=((lines.length-1)*lineHeight)/2.0;
  for (var n = 0; n < lines.length; n++) {
    context.fillText(lines[n], cx, cy);
    if (underline) {
      var tw=context.measureText(lines[n]).width/2.0;
      var th=2+context.measureText("M").width/2.0;
      context.moveTo(cx-tw,cy+th);
      context.lineTo(cx+tw,cy+th);
      context.stroke();
    }
    cy+=lineHeight;
  }

}

function drawPipelineDiagram(force) {
  if (!force && !$("#pipelineDiagramDiv").is(":visible")) return;
  if (!bgBoxes) return;
  if (pipelineCanvas.getContext) {
    var ctx=pipelineCanvas.getContext("2d");
    ctx.clearRect(0,0,pipelineCanvas.width,pipelineCanvas.height);
    ctx.strokeStyle="black";

    for (var k=0; k<bgBoxes.length; ++k) {
      if (bgBoxes[k].action=="roundedRect") {
        ctx.lineWidth=1;
        ctx.fillStyle=bgBoxes[k].active?"rgb(222,184,135)":"rgb(220,220,220)";
        roundedRect(ctx, bgBoxes[k].params[0], bgBoxes[k].params[1], bgBoxes[k].params[2], bgBoxes[k].params[3], bgBoxes[k].params[4]);
      }
      else if (bgBoxes[k].action=="path") {
        var p=bgBoxes[k].params;
        ctx.beginPath();

        for (var l=0; l<p.length; l++) {
          if (p[l]=="m") {
            ctx.moveTo(p[l+1], p[l+2]);
            l+=2;
          }
          else if (p[l]=="l") {
            ctx.lineTo(p[l+1], p[l+2]);
            l+=2;
          }
          else if (p[l]=="a") {
            ctx.arcTo(p[l+1], p[l+2], p[l+3], p[l+4], p[l+5]);
            l+=5;
          }
        }
        ctx.stroke();
      }
      else if (bgBoxes[k].action.indexOf("arrowhead_")==0) {
        arrowHead(ctx, bgBoxes[k].params[0], bgBoxes[k].params[1], bgBoxes[k].action.substring("arrowhead_".length));
      }
      else if (bgBoxes[k].action=="text") {
        ctx.font="bold 14px sans-serif";
        ctx.fillStyle="black";
        ctx.textAlign="start";
        ctx.textBaseline="alphabetic";
        ctx.fillText(bgBoxes[k].params[0], bgBoxes[k].params[1], bgBoxes[k].params[2]);
      }
    }

    ctx.textAlign="center";
    ctx.textBaseline="middle";

    var newMouseOverIndex=-1;
    for (var i=0; i<sectionBoxes.length; ++i) {
      var b=sectionBoxes[i];
      var rx=b.rect[0];
      var ry=b.rect[1];
      var rw=b.rect[2];
      var rh=b.rect[3];
      var mouseInBox=false;
      if (b.active) {
        mouseInBox=mouseX>rx && mouseX<rx+rw && mouseY>ry && mouseY<ry+rh;
        if (mouseInBox) {
          newMouseOverIndex=i;
          ctx.fillStyle="rgb(240,255,255)";
        }
        else {
          ctx.fillStyle="rgb(216,230,230)";
        }
      }
      else {
        ctx.fillStyle="rgb(190,190,190)";
      }
      if (b.swell==maxSwell) ctx.font="14.5px sans-serif";
      else ctx.font="14px sans-serif";
      rx-=b.swell; ry-=b.swell; rw+=2*b.swell; rh+=2*b.swell;
      ctx.lineWidth=mouseInBox?2:1;
      roundedRect(ctx, rx, ry, rw, rh, rectRadius);
      ctx.fillStyle=b.active?"blue":"rgb(150,150,150)";
      ctx.strokeStyle=b.active?"blue":"rgb(150,150,150)";
      ctx.lineWidth=1;
      wrapText(ctx, b.text, rx+rw/2.0,ry+rh/2.0, rw, ctx.measureText("M").width*1.5, true);

      ctx.strokeStyle="black";
    }
    if (newMouseOverIndex!=mouseOverIndex) {
      if (!swellTimeoutSet) setTimeout(updateSwell,40);
      mouseOverIndex=newMouseOverIndex;
    }
  }
}

function scrollPipelineToTop() {
    $("#pipelineDiagramDiv").scrollTop(0);
}
