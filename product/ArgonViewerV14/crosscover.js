/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

function initCrossCoverFunctions() {
    crosscover={}
    crosscover.selector=null;
    crosscover.getData = function(x,y,z) {
      var order=crosscover.pointData.order;
      if (order==1) return crosscover.data[x];
      else if (order==2) return crosscover.data[x][y];
      else if (order==3) return crosscover.data[x][y][z];
      else {
          console.log("Error: requested 3D data from 4+D array");
          return 0;
      }
    };
    crosscover.calculatePoints = function (axisData) {
        if (axisData.step<0) return Math.floor(Math.log(axisData.high/axisData.low)/Math.log(2));
        else return Math.floor((axisData.high-axisData.low)/axisData.step)+1;
    };
    crosscover.pointValue = function (axisData, point) {
      if (axisData.step<0) return axisData.low*(1<<point);
      else return axisData.low+point*axisData.step;
    };
    crosscover.getPointHTML = function() {
      var order=crosscover.pointData.order;
      var sizeX=crosscover.sizes[0];
      var sizeY=order>1?crosscover.sizes[1]:1;
      var sizeZ=order>2?crosscover.sizes[2]:1;
      var divHTML="";
      for (var z=0; z<sizeZ; ++z) {
        if (crosscover.pointData.order>2) {
          divHTML+="<div style=\"clear:both; font-weight:bold; font-size: 120%; padding-top:10px;\">"+crosscover.axisData[2].name+"="+crosscover.pointValue(crosscover.axisData[2],z)+"</div>";
        }
        divHTML+="<div class=\"wrapper\">\n";
        if (crosscover.pointData.order>1) divHTML+="<div class=\"col\"><div class=\"llabel\" >"+(crosscover.axisData[1].name+" &#9658;")+"</div></div>"; //style=\"margin-top:"+(10+(20*sizeY))+"px;\"
        divHTML+="<div class=\"lcol\"><div class=\"ltheading\">"+(crosscover.axisData[0].name+" &#9658;")+"</div>";
        for (var y=0; y<sizeY; ++y) {
          divHTML+="<div class=\"lheading\">";
          if (crosscover.pointData.order>1) divHTML+=crosscover.pointValue(crosscover.axisData[1],y);
          divHTML+="</div>";
        }
        divHTML+="</div>\n";
        for (var x=0; x<sizeX; ++x) {
          divHTML+="<div class=\"col\"><div class=\"heading\">";
          divHTML+=crosscover.pointValue(crosscover.axisData[0],x);
          divHTML+="</div>";
          for (var y=0; y<sizeY; ++y) {
            var title=crosscover.axisData[0].name+"="+crosscover.pointValue(crosscover.axisData[0],x);
            if (crosscover.pointData.order>1) title+=" "+crosscover.axisData[1].name+"="+crosscover.pointValue(crosscover.axisData[1],y);
            if (crosscover.pointData.order>2) title+=" "+crosscover.axisData[2].name+"="+crosscover.pointValue(crosscover.axisData[2],z);
            divHTML+="<div class=\""+(crosscover.getData(x,y,z)<0?"dataImpossible":(crosscover.getData(x,y,z)?"dataYes":"dataNo"))+"\" title=\""+title+"\"></div>";
          }
          divHTML+="</div>\n";
        }
        divHTML+="</div><br />\n";
      }
      return divHTML;
    };
    crosscover.getTruthTableRows= function(currentOrder, rowPrefix, arr) {
        var rowsHTML="";
        if (currentOrder<crosscover.pointData.order-1) {
            for (var i=0; i<crosscover.sizes[currentOrder]; ++i) {
                rowsHTML+=crosscover.getTruthTableRows(currentOrder+1, rowPrefix+"<td>"+crosscover.pointValue(crosscover.axisData[currentOrder],i)+"</td>", arr[i])
            }
        }
        else {
            for (var i=0; i<crosscover.sizes[currentOrder]; ++i) {
                var color="green";
                if (arr[i]<0) color="#ccc";
                else if (!arr[i]) color="red";
                rowsHTML+="<tr><th style=\"background-color:"+color+"\"></th>"+rowPrefix+"<td>"+crosscover.pointValue(crosscover.axisData[currentOrder],i)+"</td></tr>";
            }
        }
        return rowsHTML;
    }
    crosscover.getTruthTableHTML= function() {
        var order=crosscover.pointData.order;
        var divHTML="<div>\n";
        divHTML+="<table border=\"1\" style=\"margin-top:15px\">\n";
        divHTML+="<tr style=\"word-break:break-all\"><th style=\"min-width:20px\"></th>";
        for (var i=0; i<order; ++i) {
            divHTML+="<th>"+crosscover.axisData[i].name+"</th>";
        }
        divHTML+="</tr>\n";
        divHTML+=crosscover.getTruthTableRows(0, "", crosscover.data);
        divHTML+="</table>\n";
        divHTML+="</div><br />\n";
        return divHTML;
    };
    crosscover.getCanvasHeight = function (width) {
      var order=crosscover.pointData.order;
      var sizeX=crosscover.sizes[0];
      var sizeY=order>1?crosscover.sizes[1]:1;
      var sizeZ=order>2?crosscover.sizes[2]:1;
      var ry=35;
      var s=4;
      var maxWidth=Math.floor((width-20)/s);
      for (var z=0; z<sizeZ; ++z) {
        for (var x=0, rx=0; x<sizeX; ++x, rx+=s) {
          if (rx>=maxWidth*s) {
            ry+=s*sizeY+4
            rx=0;
          }
        }
        ry+=35;
      }
      return ry;
    };
    crosscover.drawPointToCanvas = function(ctx, width, height) {
      var order=crosscover.pointData.order;
      var sizeX=crosscover.sizes[0];
      var sizeY=order>1?crosscover.sizes[1]:1;
      var sizeZ=order>2?crosscover.sizes[2]:1;
      ctx.fillStyle="white";
      ctx.fillRect(0,0,width, height);
      ctx.strokeStyle="#cec";
      ctx.lineWidth=1;
      var rx=0;
      var ry=35;
      var toy=-1;
      var s=4;
      var maxWidth=Math.floor((width-20)/s);
      for (var z=0; z<sizeZ; ++z) {
        var toy2=0;
        if (crosscover.pointData.order>2) {
          ctx.fillStyle="black";
          var zStr=crosscover.axisData[2].name+"="+crosscover.pointValue(crosscover.axisData[2],z);
          while (true) {
            var zStrLen=zStr.length;
            while (ctx.measureText(zStr.substring(0, zStrLen)).width>sizeX*s)
              zStrLen--;
            ctx.fillText(zStr.substring(0, zStrLen),rx+10,ry+toy2-5);
            if (zStrLen==zStr.length) break;
            zStr=zStr.substring(zStrLen);
            if (toy<0 || toy2<toy) toy2+=ctx.measureText("M").width+2;
          }
        }
        if (toy<0) toy=toy2;
        ctx.fillStyle="green";
        ctx.strokeRect(rx+9.5,ry+toy-0.5,maxWidth>sizeX?sizeX*s+1:maxWidth*s+1,sizeY*s+1);
        for (var x=0, rx2=0; x<sizeX; ++x, rx2+=s) {
          if (rx+rx2>=maxWidth*s) {
            ry+=s*sizeY+4
            rx2=0;
            ctx.strokeRect(rx+9.5,ry+toy-0.5,maxWidth>(sizeX-x)?(sizeX-x)*s+1:maxWidth*s+1,sizeY*s+1);
          }
          for (var y=0; y<sizeY; ++y) {
            if (crosscover.getData(x,y,z)==1) ctx.fillRect(10+rx+rx2,ry+toy+s*y,s,s);
            else if (crosscover.getData(x,y,z)==-1) {
              ctx.fillStyle="#ccc";
              ctx.fillRect(10+rx+rx2,ry+toy+s*y,s,s);
              ctx.fillStyle="green";
            }
            else {
              ctx.fillStyle="red";
              ctx.fillRect(10+rx+rx2,ry+toy+s*y,s,s);
              ctx.fillStyle="green";
            }
          }
        }
        if (rx+sizeX*s*2+10 < maxWidth*s) {
          rx+=sizeX*s+10;
        }
        else {
          rx=0;
          ry+=s*sizeY;
          ry+=35;
          toy=-1;
        }
      }
    };
    crosscover.getCanvasMouseListener= function(canv, ctx) {
      var order=crosscover.pointData.order;
      var sizeX=crosscover.sizes[0];
      var sizeY=order>1?crosscover.sizes[1]:1;
      var sizeZ=order>2?crosscover.sizes[2]:1;
      var axes=crosscover.pointData.axes;

      var s=4;
      var maxWidth=Math.floor((canv.width-20)/s);
      return function(event) {
        var r=this.getBoundingClientRect();
        var mx=event.clientX-r.left;
        var my=event.clientY-r.top;

        var rx=0;
        var ry=35;
        var toy=-1;
        if (x<10 || x>=10+maxWidth*s) {
          canv.title="";
          return;
        }
        for (var z=0; z<sizeZ; ++z) {
          var toy2=0;
          if (crosscover.pointData.order>2) {
            var zStr=crosscover.axisData[2].name+"="+crosscover.pointValue(crosscover.axisData[2],z)
            while (true) {
              var zStrLen=zStr.length;
              while (ctx.measureText(zStr.substring(0, zStrLen)).width>sizeX*s)
                zStrLen--;
              if (zStrLen==zStr.length) break;
              zStr=zStr.substring(zStrLen);
              if (toy<0 || toy2<toy) toy2+=ctx.measureText("M").width+2;
            }
          }
          if (toy<0) toy=toy2;
          for (var x=0, rx2=0; x<sizeX; ++x, rx2+=s) {
            if (my<ry+toy) {
              canv.title="";
              return;
            }
            if (rx+rx2>=maxWidth*s) {
              ry+=s*sizeY+4
              rx2=0;
            }
            if (my>=ry+toy+s*sizeY) {
              x+=maxWidth-1;
              rx2+=(maxWidth-1)*s;
              continue;
            }
            for (var y=0; y<sizeY; ++y) {
              if (mx>=10+rx+rx2 && mx<10+rx+rx2+s && my>=ry+toy+s*y && my<ry+toy+s*(y+1)) {
                var title=axes[0].name+"="+crosscover.pointValue(axes[0],x);
                if (order>1) title+=" "+axes[1].name+"="+crosscover.pointValue(axes[1],y);
                if (order>2) title+=" "+axes[2].name+"="+crosscover.pointValue(axes[2],z);
                canv.title=title;
                return;
              }
            }
          }
          if (rx+sizeX*s*2+10 < maxWidth*s) {
            rx+=sizeX*s+10;
          }
          else {
            rx=0;
            ry+=s*sizeY;
            ry+=35;
            toy=-1;
          }
        }
      };
    };
    crosscover.setupArray= function(currentOrder, arr) {
        if (currentOrder<crosscover.pointData.order-1) {
            for (var i=0; i<crosscover.sizes[currentOrder]; ++i) {
                arr[i]=new Array();
                crosscover.setupArray(currentOrder+1, arr[i]);
            }
        }
        else {
            for (var i=0; i<crosscover.sizes[currentOrder]; ++i, ++crosscover.su_nybblePtr, crosscover.su_bitmask<<=1) {
                if ((crosscover.su_nybblePtr&3)==0) {
                    crosscover.su_currentNybble=parseInt(crosscover.su_bitfield.charAt(crosscover.su_bitfield.length-(crosscover.su_nybblePtr>>2)-1),16);
                    crosscover.su_currentNybbleImpossible=parseInt(crosscover.su_impossibleBitfield.charAt(crosscover.su_impossibleBitfield.length-(crosscover.su_nybblePtr>>2)-1),16);
                    crosscover.su_bitmask=1;
                }
                if ((crosscover.su_currentNybbleImpossible&crosscover.su_bitmask)!=0) {
                    arr[i]=-1;
                }
                else {
                    arr[i]=((crosscover.su_currentNybble&crosscover.su_bitmask)!=0);
                }
            }
        }
    }

    crosscover.setupData= function (name, view) {
        crosscover.pointData=crossPointData[name];
        crosscover.heading=crosscover.pointData.axes[0].name;
        crosscover.axisData=crosscover.pointData.axes;
        crosscover.sizes=new Array();
        for (var i=0; i<crosscover.pointData.order; ++i) {
            crosscover.sizes[i]=crosscover.calculatePoints(crosscover.pointData.axes[i]);
            if (i>0) crosscover.heading+=" &times; "+crosscover.pointData.axes[i].name;
        }
        crosscover.su_bitfield=crosscover.pointData.bitfield;
        crosscover.su_impossibleBitfield=crosscover.pointData.impossibleCasesBitfield[view];

        crosscover.su_nybblePtr=0
        crosscover.su_bitmask=1;

        crosscover.data=new Array();
        crosscover.setupArray(0,crosscover.data);
    };

    crosscover.setData= function () {
        if (crosscover.selector==null) return;
        var selectedOptionElement=crosscover.selector.find(":selected");
        crosscover.selector.argon_selectmenu("widget").css("background",selectedOptionElement.attr("data-background"));
        var selectedOption=selectedOptionElement.attr("id");
        crosscover.setupData(selectedOption, $("#visibleCategory").find(":selected").attr("id"));
        var order=crosscover.pointData.order;
        var sizeX=crosscover.sizes[0];
        var sizeY=order>1?crosscover.sizes[1]:1;
        var sizeZ=order>2?crosscover.sizes[2]:1;
        crosscover.headingElem.html(crosscover.heading);
        if (order>3) {
            crosscover.div.innerHTML=crosscover.getTruthTableHTML();
            crosscover.div.style.display="block";
            crosscover.canv.style.display="none";
        }
        else if (sizeX*sizeY*sizeZ<2048) {
            crosscover.div.innerHTML=crosscover.getPointHTML();
            crosscover.div.style.display="block";
            crosscover.canv.style.display="none";
        }
        else {
            crosscover.div.innerHTML="";
            crosscover.div.style.display="none";
            crosscover.canv.style.display="block";
            if (crosscover.canvListener!=null) crosscover.canv.removeEventListener('mousemove',crosscover.canvListener);
            crosscover.canvListener=crosscover.getCanvasMouseListener(crosscover.canv, crosscover.ctx)
            crosscover.canv.addEventListener('mousemove', crosscover.canvListener);
            crosscover.drawPointToCanvas(crosscover.ctx, crosscover.canv.width, crosscover.canv.height);
        }
    };
    crosscover.isComplete=function(currentOrder, order, sizes) {
        if (currentOrder<order-1) {
            for (var i=0; i<sizes[currentOrder]; ++i) {
                if (!crosscover.isComplete(currentOrder+1, order, sizes)) return false;
            }
        }
        else {
            for (var i=0; i<sizes[currentOrder]; ++i, ++crosscover.su_nybblePtr, crosscover.su_bitmask<<=1) {
                if ((crosscover.su_nybblePtr&3)==0) {
                    crosscover.su_currentNybble=parseInt(crosscover.su_bitfield.charAt(crosscover.su_bitfield.length-(crosscover.su_nybblePtr>>2)-1),16);
                    crosscover.su_currentNybbleImpossible=parseInt(crosscover.su_impossibleBitfield.charAt(crosscover.su_impossibleBitfield.length-(crosscover.su_nybblePtr>>2)-1),16);
                    crosscover.su_bitmask=1;
                }
                if ((crosscover.su_currentNybbleImpossible&crosscover.su_bitmask)==0 && (crosscover.su_currentNybble&crosscover.su_bitmask)==0) return false;
            }
        }
        return true;
    };

    crosscover.setOptionBackgrounds=function() {
        var view=$("#visibleCategory").find(":selected").attr("id");
        crosscover.coveredCrossPoints=0;
        crosscover.totalCrossPoints=0;
        crosscover.coveredValuePoints=0;
        crosscover.totalValuePoints=0;
        $("#crossPointSelector option").add("#crossValuePointSelector option").each(function(index, value) {
            $(value).css("background-color","#dfd");
            $(value).attr("data-background","#dfd");
            var selectedOption=$(value).attr("id");
            var selectedOptionData=crossPointData[selectedOption];
            var sizes=new Array();
            for (var i=0; i<selectedOptionData.order; ++i) {
                sizes[i]=crosscover.calculatePoints(selectedOptionData.axes[i]);
            }
            crosscover.axisData=selectedOptionData.axes;
            crosscover.su_bitfield=selectedOptionData.bitfield;
            crosscover.su_impossibleBitfield=selectedOptionData.impossibleCasesBitfield[view];

            crosscover.su_nybblePtr=0
            crosscover.su_bitmask=1;
            if (!crosscover.isComplete(0, selectedOptionData.order, sizes)) {
              $(value).css("background-color","#fdd");
              $(value).attr("data-background","#fdd");
            }
            else {
                if (selectedOptionData.order==1) crosscover.coveredValuePoints++;
                else crosscover.coveredCrossPoints++;
            }
            if (selectedOptionData.order==1) crosscover.totalValuePoints++;
            else crosscover.totalCrossPoints++;
        });
        //FIXME: These seem to be broken, so commented out.
        //$("#crossPointSelector").argon_selectmenu("refresh");
        //$("#crossValuePointSelector").argon_selectmenu("refresh");
        coverageReady|=4;
    };

    crosscover.setCrossCoverType=function(type) {
        if (type==0) {
            crosscover.selector=$("#crossPointSelector");
            crosscover.headingElem=$("#crossCoverHeading");
            crosscover.div=document.getElementById("crossCoverDiv");
            crosscover.canv=document.getElementById("crossCoverCanv");
            crosscover.canvListener=null;
            crosscover.ctx=crosscover.canv.getContext("2d");
        }
        else if (type==1) {
            crosscover.selector=$("#crossValuePointSelector");
            crosscover.headingElem=$("#crossValueCoverHeading");
            crosscover.div=document.getElementById("crossValueCoverDiv");
            crosscover.canv=document.getElementById("crossValueCoverCanv");
            crosscover.canvListener=null;
            crosscover.ctx=crosscover.canv.getContext("2d");
        }
        crosscover.setData();
    };
}

function initCrossCoverSidepanel() {
    initCrossCoverFunctions();
    $.widget("custom.argon_selectmenu", $.ui.selectmenu, {
        _renderItem: function ( ul, item ) {
          var it=this._super( ul, item );
          $(it).css('background-color', item.element.attr('data-background'));
          return it;
        },
        _resizeMenu: function () {
          this._super();
          this.menu.css("max-height", "200px");
        }
      });
    $("#crossPointSelector").argon_selectmenu({
      width: 400,
      change: crosscover.setData
    });
    $("#crossPointSelector").argon_selectmenu("widget").css("vertical-align", "middle");
    $("#crossValuePointSelector").argon_selectmenu({
      width: 400,
      change: crosscover.setData
    });
    $("#crossValuePointSelector").argon_selectmenu("widget").css("vertical-align", "middle");
    var crossValuePointSelect=$("#crossValuePointSelector");
    var crossPointSelector=$("#crossValuePointSelector");
    if (crossValuePointSelect[0].length == 0 || crossPointSelector[0].length == 0) {
        console.log("Error: Length of Cross and Value fields must be greater than 0");
        return 0;
    }
    crosscover.setOptionBackgrounds();
    $("#visibleCategory").change(crosscover.setOptionBackgrounds);
    $("#visibleCategory").change(crosscover.setData);
}


function crossPrev() {
    var crossPointSelect=$("#crossPointSelector");
    if (crossPointSelect[0].selectedIndex>0) {
        crossPointSelect[0].selectedIndex--;
        crosscover.setData();
    }
    crossPointSelect.argon_selectmenu("refresh");
}

function crossNext() {
    var crossPointSelect=$("#crossPointSelector");
    if (crossPointSelect[0].selectedIndex<(crossPointSelect[0].length-1)) {
        crossPointSelect[0].selectedIndex++;
        crosscover.setData();
    }
    crossPointSelect.argon_selectmenu("refresh");
}

function crossValuePrev() {
    var crossValuePointSelect=$("#crossValuePointSelector");
    if (crossValuePointSelect[0].selectedIndex>0) {
        crossValuePointSelect[0].selectedIndex--;
        crosscover.setData();
    }
    crossValuePointSelect.argon_selectmenu("refresh");
}

function crossValueNext() {
    var crossValuePointSelect=$("#crossValuePointSelector");
    if (crossValuePointSelect[0].selectedIndex<(crossValuePointSelect[0].length-1)) {
        crossValuePointSelect[0].selectedIndex++;
        crosscover.setData();
    }
    crossValuePointSelect.argon_selectmenu("refresh");
}
