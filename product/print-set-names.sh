#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

set -o errexit -o pipefail -o nounset

# A utility script to print out the set names in a given release.

error () {
    echo 1>&2 "$1"
    exit 1
}

check_empty () {
    # If $1 isn't empty, print $2 to stderr and then cat $1, assuming
    # that it has null-terminated records. This replaces \0 with
    # newlines and indents everything by two spaces.
    test -e "$1" -a '!' -s "$1" || {
        echo 1>&2 "$2"
        sed <"$1" -z 's!^!\n  !g' | tr -d '\0' | head -n20 1>&2
        echo 1>&2
        return 1
    }
}

if [ $# != 1 ]; then
    error "Usage: print-set-names.sh <release-dir>"
fi

rel="$1"
test -d "$rel" || error "No such directory: $rel"

TMP="$(mktemp -d)"
trap "{ rm -rf $TMP; }" EXIT

# Any directory that's not called ArgonViewerV* or bin is treated as a
# set.
find "$1" -mindepth 1 -maxdepth 1 \
     -type d -not -name 'ArgonViewerV*' -not -name 'bin' \
     -printf '%f\0' >"$TMP/sets"

# Set names shouldn't contain any weird characters: they should be
# alphanumeric plus '_', '.' and '-'. This grep searches for anything
# that doesn't match the pattern.
grep -zv '^[a-zA-Z0-9_.-]*$' "$TMP/sets" >"$TMP/X" || true
test '!' -s "$TMP/X" || {
    echo 1>&2 "Release has directories at top-level with disallowed characters"
    sed <"$TMP/X" -z 's!^!\n  !g' | tr -d '\0' | head -n20 1>&2
    echo 1>&2
    exit 1
}

# Otherwise we're good! Print out the directories separated by
# newlines. The cut is to get rid of a leading "./".
exec tr <"$TMP/sets" '\0' '\n'
