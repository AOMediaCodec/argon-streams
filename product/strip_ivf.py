#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A simple tool that extracts bitstreams from IVF files'''

import argparse
import sys
from typing import cast, Iterator, List, Tuple

from bitstring import ConstBitStream, ReadError  # type: ignore


def print_table(entries: List[Tuple[str, str, bool]]) -> None:
    '''Print a table of entries (K, V, left-align)'''
    k_width = 0
    v_width = 0
    for key, value, _ in entries:
        k_width = max(k_width, len(key))
        v_width = max(v_width, len(value))

    for key, value, left in entries:
        # Add 1 to width for colon, then a bit more to space things out
        k_fmt = '{{:<{}}} '.format(k_width + 1 + 4)
        k_txt = k_fmt.format(key + ':')

        if left:
            v_txt = value
        else:
            v_fmt = '{{:>{}}}'.format(v_width)
            v_txt = v_fmt.format(value)

        print(k_txt + v_txt)


def read_header(stream: ConstBitStream) -> int:
    '''Read an IVF header from stream. Returns num frames'''
    pos0 = stream.pos

    magic = stream.read('bytes:4')
    if magic != b'DKIF':
        raise RuntimeError('Magic bytes are {}, not DKIF.'
                           .format(magic))

    version = stream.read('uintle:16')
    if version != 0:
        raise RuntimeError('IVF version is {}, not 0.'
                           .format(version))

    hdr_len = stream.read('uintle:16')
    codec = stream.read('bytes:4')
    width = stream.read('uintle:16')
    height = stream.read('uintle:16')

    time_denom = stream.read('uintle:32')
    time_numer = stream.read('uintle:32')
    num_frames = cast(int, stream.read('uintle:32'))

    table = [
        ('CODEC', str(codec), False),
        ('WIDTH', str(width), False),
        ('HEIGHT', str(height), False),
        ('FPS', ('{}/{} ({})'
                 .format(time_numer, time_denom,
                         time_numer / time_denom)), False),
        ('FRAMES', str(num_frames), False)
    ]
    print_table(table)

    pos1 = stream.pos
    if pos1 - pos0 > hdr_len * 8:
        raise RuntimeError('Header signalled a length of {} bytes, '
                           'but actually took {} bits ({} bytes).'
                           .format(hdr_len, pos1 - pos0,
                                   (pos1 - pos0) / 8))

    end_pos = pos0 + 8 * hdr_len
    if end_pos >= stream.length:
        raise RuntimeError('Stream signalled a header of length {} bytes, '
                           'starting at bit position {}. This would end at '
                           'bit position {} but the stream is only {} '
                           'bits long.'
                           .format(hdr_len, pos0, end_pos, stream.length))

    stream.pos = end_pos

    return num_frames


def read_frame_header(frame_number: int,
                      stream: ConstBitStream) -> Tuple[int, int]:
    '''Read a frame header and return the raw frame size'''
    try:
        frame_size = stream.read('uintle:32')
        timestamp = stream.read('uintle:64')

        return (frame_size, timestamp)
    except ReadError as err:
        raise RuntimeError('Failed to read header of frame {}: {}'
                           .format(frame_number, err)) from None


def file_frames(num_frames: int, stream: ConstBitStream) -> Iterator[bytes]:
    frame_number = 1
    fwidth = len(str(num_frames))
    fmt = '-- Frame {{:>{}}}/{} (time: {{}})'.format(fwidth, num_frames)
    while stream.pos < stream.length:
        size, timestamp = read_frame_header(frame_number, stream)
        print(fmt.format(frame_number, timestamp))

        try:
            yield stream.read('bytes:{}'.format(size))
        except ReadError as err:
            raise RuntimeError('Failed to read frame body ({} bytes)'
                               ' of frame {}: {}'
                               .format(size, frame_number, err)) from None

        frame_number += 1


def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('ivf')
    parser.add_argument('raw')

    args = parser.parse_args()

    stream = ConstBitStream(filename=args.ivf)
    num_frames = read_header(stream)
    with open(args.raw, 'wb') as out_file:
        for frame in file_frames(num_frames, stream):
            out_file.write(frame)

    return 0


if __name__ == '__main__':
    try:
        sys.exit(main())
    except RuntimeError as err:
        sys.stderr.write('Error: {}\n'.format(err))
        sys.exit(1)
