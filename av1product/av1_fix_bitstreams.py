################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import argparse
from bitstring import ConstBitStream, BitStream, Bits, pack
import random

# definitions of AV1 obu_type field
OBU_TYPE_SEQUENCE_HEADER = 1
OBU_TYPE_TEMPORAL_DELIMITER = 2
OBU_TYPE_FRAME_HEADER = 3
OBU_TYPE_TILE_GROUP = 4
OBU_TYPE_METADATA = 5
OBU_TYPE_FRAME = 6
OBU_TYPE_REDUNDANT_FRAME_HEADER = 7
OBU_TYPE_TILE_LIST = 8
OBU_TYPE_PADDING = 15

LEB128_MAX_SIZE = 8


class AV1Handler(object):
    def __init__(self, args):
        self.args = args
        self.stream = None
        self.last_seq_hdr = None
        self.last_obu_type = None

    def dprint(self, string):
        if self.args.verbose:
            print(string)

    def str_obu_type(self, type):
        if (type == OBU_TYPE_SEQUENCE_HEADER):
            result = 'SEQUENCE_HEADER'
        elif (type == OBU_TYPE_TEMPORAL_DELIMITER):
            result = 'TEMPORAL_DELIMITER'
        elif (type == OBU_TYPE_FRAME_HEADER):
            result = 'FRAME_HEADER'
        elif (type == OBU_TYPE_TILE_GROUP):
            result = 'TILE_GROUP'
        elif (type == OBU_TYPE_METADATA):
            result = 'METADATA'
        elif (type == OBU_TYPE_FRAME):
            result = 'FRAME'
        elif (type == OBU_TYPE_REDUNDANT_FRAME_HEADER):
            result = 'REDUNDANT_FRAME_HEADER'
        elif (type == OBU_TYPE_TILE_LIST):
            result = 'TILE_LIST'
        elif (type == OBU_TYPE_PADDING):
            result = 'PADDING'
        else:
            raise ValueError('OBU type = {:d} unknown'.format(type))
        return result

    def dprint_obu_type(self, type):
        self.dprint(self.str_obu_type(type))

    def read_leb128(self):
        result = 0
        shift = 0
        length = 0
        while True:
            val_byte = self.stream.read('uint:8')
            result |= (val_byte & 0x7F) << shift
            shift += 7
            length += 1
            if ((val_byte & 0x80) == 0x00):
                break
        return result, length

    def encode_leb128(self, value, leb128_bytes=-1):
        assert leb128_bytes <= LEB128_MAX_SIZE

        result = BitStream()
        length = 0

        while True:
            bitstring_bottom = pack('uint:7', value & 0x7F)
            value = value >> 7
            length += 1
            moreBytes = (value != 0)
            if leb128_bytes != -1:
                if length != leb128_bytes:
                    moreBytes = True
                else:
                    assert not moreBytes
            elif not moreBytes and length < LEB128_MAX_SIZE:
                moreBytes = (int(round(random.uniform(0, 1))) == 1)

            if moreBytes:
                # more bytes to come
                result.append('0b1')
                result += bitstring_bottom
            else:
                # we are done
                result.append('0b0')
                result += bitstring_bottom
                break

        if leb128_bytes != -1:
            assert length == leb128_bytes
        assert (length <= LEB128_MAX_SIZE)
        return result

    def parse_obu_header(self):
        stream = self.stream
        obu_header_start_pos = stream.pos
        obu_header_start_bytepos = stream.bytepos

        d = {}
        d['obu_forbidden_bit'] = stream.read('bool')
        if d['obu_forbidden_bit']:
            raise ValueError('obu_forbidden_bit set in obu_header at OBU '
                             'starting at pos={0:d} bytepos=0x{1:x}'
                             .format(obu_header_start_pos,
                                     obu_header_start_bytepos))

        d['obu_type'] = stream.read('uint:4')
        d['obu_extension_flag'] = stream.read('bool')
        d['obu_has_size_field'] = stream.read('bool')
        d['obu_reserved_1bit'] = stream.read('bool')
        if d['obu_reserved_1bit']:
            raise ValueError('obu_reserved_1bit set in obu_header at OBU '
                             'starting at pos={0:d} bytepos=0x{1:x}'
                             .format(obu_header_start_pos,
                                     obu_header_start_bytepos))

        if d['obu_extension_flag']:
            d['temporal_id'] = stream.read('uint:3')
            d['spatial_id'] = stream.read('uint:2')
            d['extension_header_reserved_3bits'] = stream.read('uint:3')
            if (d['extension_header_reserved_3bits'] != 0):
                raise ValueError('extension_header_reserved_3bits set to {:d} '
                                 'in obu_extension_header at OBU starting at '
                                 'pos={:d} bytepos=0x{:x}'
                                 .format(d['extension_header_reserved_3bits'],
                                         obu_header_start_pos,
                                         obu_header_start_bytepos))
            d['obu_header_len'] = 2
        else:
            d['obu_header_len'] = 1

        return d

    def insert_temporal_delimiter(self):
        if self.args.non_annexb:
            obu_has_size_field = True
        else:
            obu_has_size_field = (int(round(random.uniform(0, 1))) == 1)

        # OBU Header
        td = BitStream()

        # obu_forbidden_bit     f(1)
        td.append('0b0')
        # obu_type              f(4)
        td += pack('uint:4', OBU_TYPE_TEMPORAL_DELIMITER)
        # obu_extension_flag 	f(1)
        td.append('0b0')
        # obu_has_size_field 	f(1)
        td.append('0b1' if obu_has_size_field else '0b0')
        # obu_reserved_1bit 	f(1)
        td.append('0b0')

        # OBU size field (if required) is set to 0 (no payload)
        if obu_has_size_field:
            td += self.encode_leb128(0)

        return td

    def temporal_delimiter_obu(self, header_len, obu_data):
        # Temporal delimiters get passed straight through
        return (obu_data, False, True)

    def sequence_header_obu(self, header_len, obu_data):
        # We want to spot the case when there is a sequence header with
        # different data from a previous header and doesn't immediately follow
        # a temporal delimiter.
        needs_td = ((self.last_seq_hdr is not None) and
                    (self.last_obu_type != OBU_TYPE_TEMPORAL_DELIMITER) and
                    (self.last_seq_hdr != obu_data))
        if needs_td:
            print("Inserting Missing Temporal Delimiter in file "
                  "(change of seq hdr): {}"
                  .format(self.args.file))

        self.last_seq_hdr = obu_data

        return (obu_data, needs_td, False)

    def padding_obu(self, header_len, obu_data):
        # A well-formed padding OBU has zero or more bytes of arbitrary data,
        # then the byte 0x80 and then zero or more bytes of zeros. Here, we
        # make sure that padding OBUs look like that.
        #
        # If the padding OBU is actually empty (nothing but header), there's
        # nothing to do, since an empty OBU doesn't need the trailing bit
        # sequence.

        assert obu_data.pos == 0
        obu_data.pos = 8 * header_len

        body_len = len(obu_data) - 8 * header_len
        assert body_len >= 0
        assert body_len % 8 == 0

        if not body_len:
            print('Dropped empty padding OBU in file {}'
                  .format(self.args.file))
            return (obu_data, False, False)

        body_data = obu_data.read('bits:{}'.format(body_len))

        # Search backwards for the last nonzero position. This will be returned
        # as (pos,) on success or () on failure.
        last_nz_pos = body_data.rfind('0b1')
        well_formed = False
        if last_nz_pos:
            # If there is a nonzero bit, it's only well-formed if the position
            # is 0 modulo 8. This guarantees that we have an 0x80 byte.
            well_formed = last_nz_pos[0] % 8 == 0

        if well_formed:
            return (obu_data, False, False)

        # Something is bad. Boo.
        obu_data.pos = 0
        hdr_data = Bits(obu_data.read('bits:{}'.format(8 * header_len)))

        # Choose a byte index for the 0x80 in [0, body_len // 8 - 1]. Any stuff
        # before will be random initialised. Any stuff after will be zeroed.
        trail_idx = random.randint(0, body_len // 8 - 1)

        if trail_idx:
            body_data = (Bits(bytearray(random.getrandbits(8)
                                        for _ in range(trail_idx))) +
                         Bits('0x80'))
        else:
            body_data = Bits('0x80')

        num_zeros = body_len // 8 - (1 + trail_idx)
        if num_zeros:
            body_data += Bits(int=0, length=8 * num_zeros)

        assert len(body_data) == body_len

        print('Fixed ill-formed padding OBU in file {}'.format(self.args.file))
        return (hdr_data + body_data, False, False)

    def other_obu(self, header_len, obu_data):
        # This is an OBU type that we don't do anything special to. Just pass
        # it straight back.
        return (obu_data, False, False)

    def open_bitstream_unit(self, sz, first_obu_in_tu):
        obu = []

        obu_start_bytepos = self.stream.bytepos

        d = self.parse_obu_header()
        self.dprint_obu_type(d['obu_type'])

        obu_header_length = d['obu_header_len']

        if d['obu_has_size_field']:
            # OBU has size field, so check if the value there is consistent
            # with obu_length in Annex B frame_nit
            obu_size, obu_size_len = self.read_leb128()
            obu_header_length += obu_size_len
            calc_obu_length = obu_size + obu_header_length

            if (sz > 0 and sz != calc_obu_length):
                raise ValueError('Annex B frame unit sz={:d} is inconsistent '
                                 'with OBU size = {:d} at stream byte '
                                 'position 0x{:x}'
                                 .format(sz, obu_size, obu_start_bytepos))
            sz = calc_obu_length

        # If there is no size field, we need to have been given a size. Either
        # way, the result should be strictly positive.
        assert sz is not None
        assert sz > 0

        obu_handlers = {
            OBU_TYPE_TEMPORAL_DELIMITER: AV1Handler.temporal_delimiter_obu,
            OBU_TYPE_SEQUENCE_HEADER: AV1Handler.sequence_header_obu,
            OBU_TYPE_PADDING: AV1Handler.padding_obu
        }

        # Get this OBU's data
        header_len = self.stream.bytepos - obu_start_bytepos
        self.stream.bytepos = obu_start_bytepos
        this_obu = self.stream.read('bits:' + str(sz*8))

        # Run a handler for the OBU data.
        handler = obu_handlers.get(d['obu_type'], AV1Handler.other_obu)
        this_obu, needs_td, has_td = handler(self, header_len, this_obu)

        # Find missing TDs at the start of a TU (or bit-stream)
        if ((first_obu_in_tu and
             not has_td and
             d['obu_type'] != OBU_TYPE_TEMPORAL_DELIMITER)):
            print("Inserting Missing Temporal Delimiter in "
                  "file (start of tu): {}"
                  .format(self.args.file))
            needs_td = True

        self.last_obu_type = d['obu_type']

        if needs_td:
            obu += [self.insert_temporal_delimiter()]

        # Append this OBU to the bitstream, unless it's None in which case we
        # should just drop it.
        if this_obu is not None:
            obu += [this_obu]

        return obu

    def frame_unit(self, size, first_obu_in_tu):
        fu = BitStream()

        while (size > 0):
            old_obu_length, leb128_bytes = self.read_leb128()
            self.dprint('  Frame unit obu_length={:d}'
                        .format(old_obu_length))
            size -= leb128_bytes

            obus = self.open_bitstream_unit(old_obu_length, first_obu_in_tu)
            first_obu_in_tu = False
            size -= old_obu_length

            for obu in obus:
                obu_len = len(obu) // 8
                if obu_len == old_obu_length:
                    fu += self.encode_leb128(old_obu_length, leb128_bytes)
                    old_obu_length = -1
                else:
                    fu += self.encode_leb128(obu_len)

                fu += obu
                self.dprint("  0: new_obu_length {:d}, fu_size {:d}"
                            .format(obu_len, len(fu) // 8))

        return fu

    def temporal_unit(self, size):
        first_obu_in_tu = True
        tu = BitStream()

        while (size > 0):
            frame_unit_size, leb128_bytes = self.read_leb128()
            self.dprint(' Temporal unit frame_unit_size = {:d}'
                        .format(frame_unit_size))
            size -= leb128_bytes

            fu = self.frame_unit(frame_unit_size, first_obu_in_tu)
            first_obu_in_tu = False
            size -= frame_unit_size

            new_frame_unit_size = len(fu)/8
            if new_frame_unit_size != frame_unit_size:
                tu += self.encode_leb128(new_frame_unit_size)
            else:
                tu += self.encode_leb128(frame_unit_size, leb128_bytes)
            tu += fu
            self.dprint(" frame_unit_size {:d}, new_frame_unit_size {:d}, "
                        "tu_size {:d}"
                        .format(frame_unit_size,
                                new_frame_unit_size, len(tu)/8))

        return tu

    def bitstream(self):
        out = BitStream()

        while True:
            temporal_unit_size, leb128_bytes = self.read_leb128()
            self.dprint('Temporal_unit_size = {:d}'.format(temporal_unit_size))

            tu = self.temporal_unit(temporal_unit_size)
            end_of_stream = (self.stream.pos >= self.stream.length)

            new_temporal_unit_size = len(tu)/8
            if new_temporal_unit_size != temporal_unit_size:
                out += self.encode_leb128(new_temporal_unit_size)
            else:
                out += self.encode_leb128(temporal_unit_size, leb128_bytes)
            out += tu
            self.dprint("temporal_unit_size {:d}, "
                        "new_temporal_unit_size {:d}, out_size {:d}"
                        .format(temporal_unit_size,
                                new_temporal_unit_size, len(out)/8))

            if end_of_stream:
                break

        return out

    def not_annexb_bitstream(self):
        out = BitStream()

        first_obu_in_tu = True
        while True:
            obus = self.open_bitstream_unit(None, first_obu_in_tu)
            first_obu_in_tu = False
            end_of_stream = (self.stream.pos >= self.stream.length)

            for obu in obus:
                out += obu

            if end_of_stream:
                break

        return out

    def go(self):
        self.stream = ConstBitStream(filename=args.file)

        out = (self.not_annexb_bitstream()
               if args.non_annexb
               else self.bitstream())

        self.stream.bytepos = 0
        self.stream.bitpos = 0
        outputChanged = (self.stream != out)
        self.stream = None

        if outputChanged:
            # Write out new file
            with open(self.args.file, 'wb') as f:
                length = len(out)/8
                f.write(out.read('bytes:'+str(length)))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('file',
                        help='The obu file to work on')
    parser.add_argument('-v', dest='verbose',
                        help='Verbose debug output',
                        default=False, action='store_true')
    parser.add_argument('--not-annexb', dest='non_annexb',
                        help="Non annex B packed bit-stream",
                        default=False, action='store_true')

    args = parser.parse_args()
    return args


# We parse the bit-stream and insert missing Temporal Delimiters if we notice
# they are missing
if __name__ == '__main__':
    args = parse_args()

    handler = AV1Handler(args)
    handler.go()
