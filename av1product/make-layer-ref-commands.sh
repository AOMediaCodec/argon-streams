#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This script is for use when building a 2.0 release from v1.3. The
# 1.3 release contains ref commands, which are shell scripts telling
# the user how to run libaom to decode the stream. We need to copy
# them for all the different layers.

set -o errexit -o pipefail -o nounset

error () {
    echo 1>&2 "$1"
    exit 1
}

av1product_dir="$(dirname ${BASH_SOURCE})"
product_dir="${av1product_dir}/../product"

if [ $# != 2 ]; then
    error "Usage: make-layer-ref-commands.sh <release-dir> <single-layer-re>"
fi

TMP="$(mktemp -d)"
trap "{ rm -rf $TMP; }" EXIT

rel="$1"
single_layer_re="$2"
shift 2

# Note that print-set-names.sh checks that the set names have no weird
# characters, so this loop is safe. We throw away all the error stream
# directories, since we won't be generating reference commands for
# those sets.
sets="$("${product_dir}/print-set-names.sh" "$rel" | grep -Ev "${single_layer_re}")"
for set_name in $sets; do
    dir="$rel/${set_name}/ref_cmd"
    echo "Expanding reference commands in $rel/${set_name}"

    # List all the shell scripts in ref_cmd but not its
    # subdirectories.
    find "$dir" -maxdepth 1 -type f -name '*.sh' \
         -printf '%f\0' >"$TMP/ref-cmds"

    # Check that these shell scripts have allowable names.
    grep -zv '^[a-zA-Z0-9_.-]*$' "$TMP/ref-cmds" >"$TMP/X" || true
    test '!' -s "$TMP/X" || {
        echo 1>&2 "Bad shell script names in ${dir}"
        sed <"$TMP/X" -z 's!^!\n  !g' | tr -d '\0' | head -n20 1>&2
        echo 1>&2
        exit 1
    }

    # Ensure all the layer directories exist
    for layer in $(seq 1 31); do
        ldir="${dir}/layers/${layer}"
        mkdir -p "$ldir" || error "Failed to create $ldir"
    done

    # If so, we can iterate over them safely.
    for ref_cmd in $(tr <"$TMP/ref-cmds" '\0' '\n'); do
        src="${dir}/${ref_cmd}"
        base=$(basename "${src}" .sh)
        for layer in $(seq 1 31); do
            dst="${dir}/layers/${layer}/${base}_layer${layer}.sh"
            if [ ! -e "$dst" ]; then
                sed "s/--all-layers/--all-layers --oppoint=${layer}/" \
                    <"$src" >"$dst" || \
                    error "Couldn't expand $src to $dst"
            fi
        done
    done
done
