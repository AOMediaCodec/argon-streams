#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A Python script that tries to help fix decoder model failures

We do this by hacking sequence headers. '''

import argparse
import copy
import filecmp
import math
import os.path
import sys
from typing import Any, Callable, BinaryIO, List, Optional, Tuple

from bitstring import ConstBitStream, BitStream, pack  # type: ignore

COUNTER = 0


LEB128_MAX_SIZE = 8

OBU_SEQUENCE_HEADER = 1
OBU_TEMPORAL_DELIMITER = 2
OBU_FRAME_HEADER = 3
OBU_FRAME = 6

CP_BT_709 = 1
CP_UNSPECIFIED = 2

TC_UNSPECIFIED = 2
TC_SRGB = 13

MC_IDENTITY = 0
MC_UNSPECIFIED = 2

CSP_UNKNOWN = 0

KEY_FRAME = 0
INTER_FRAME = 1
INTRA_ONLY_FRAME = 2
SWITCH_FRAME = 3

PRIMARY_REF_NONE = 7

NUM_REF_FRAMES = 8

SUPERRES_DENOM_BITS = 3
SUPERRES_DENOM_MIN = 9
SUPERRES_NUM = 8

TOTAL_REFS_PER_FRAME = 8
REFS_PER_FRAME = 7
INTRA_FRAME = 0
LAST_FRAME = 1
LAST2_FRAME = 2
LAST3_FRAME = 3
GOLDEN_FRAME = 4
BWDREF_FRAME = 5
ALTREF2_FRAME = 6
ALTREF_FRAME = 7

SWITCHABLE = 4

MAX_TILE_WIDTH = 4096
MAX_TILE_COLS = 64
MAX_TILE_ROWS = 64
MAX_TILE_AREA = 4096 * 2304

MAX_SEGMENTS = 8
SEG_LVL_ALT_Q = 0
SEG_LVL_MAX = 8

IDENTITY = 0
TRANSLATION = 1
ROTZOOM = 2
AFFINE = 3

GM_ABS_TRANS_BITS = 12
GM_ABS_TRANS_ONLY_BITS = 9
GM_ABS_ALPHA_BITS = 12


class RWObject:
    '''An object that can be read from and written to bitstreams'''
    def read(self, stream: ConstBitStream) -> None:
        '''Read object contents from the bitstream'''
        raise NotImplementedError

    def write(self, stream: BitStream) -> None:
        '''Write a binary representation of the object to stream'''
        raise NotImplementedError

    @staticmethod
    def guarded_read_int(width: int,
                         guard: bool,
                         default_value: Any,
                         stream: ConstBitStream) -> Any:
        '''Maybe read width bits from stream'''
        if guard:
            return stream.read('uint:{}'.format(width))
        else:
            return default_value

    @staticmethod
    def guarded_write_int(width: int,
                          guard: bool,
                          default_value: Any,
                          value: Any,
                          stream: BitStream) -> None:
        '''Maybe write width bits to stream'''
        if guard:
            assert isinstance(value, int)
            stream += pack('uint:{}'.format(width), value)
        else:
            assert value == default_value

    @staticmethod
    def guarded_read_bool(guard: bool,
                          stream: ConstBitStream) -> bool:
        '''Maybe read a bool from stream'''
        return (RWObject.guarded_read_int(1, guard, 0, stream) != 0)

    @staticmethod
    def guarded_write_bool(guard: bool,
                           value: bool,
                           stream: BitStream):
        '''Maybe write a bool to stream'''
        RWObject.guarded_write_int(1, guard, 0, 1 if value else 0, stream)

    @staticmethod
    def read_bit_int(width: int,
                     stream: ConstBitStream) -> Optional[int]:
        '''Read a bit and then an int if that's true'''
        present = stream.read('bool')
        if present:
            return stream.read('uint:{}'.format(width))
        else:
            return None

    @staticmethod
    def write_bit_int(width: int,
                      value: Optional[int],
                      stream: BitStream) -> None:
        if value is not None:
            stream += pack('bool', True)
            stream += pack('uint:{}'.format(width), value)
        else:
            stream += pack('bool', False)

    @staticmethod
    def read_inc_range(low: int, high: int, stream: ConstBitStream) -> int:
        '''Read increment bits to get a value in range'''
        assert low <= high

        value = low
        while value < high:
            increment = stream.read('bool')
            if increment:
                value += 1
            else:
                break

        assert low <= value <= high
        return value

    @staticmethod
    def write_inc_range(low: int, high: int,
                        value: int, stream: ConstBitStream) -> None:
        '''Write increment bits to for a value in range'''
        assert low <= value <= high

        sent = low
        while sent < value:
            stream += pack('bool', True)
            sent += 1

        if sent < high:
            stream += pack('bool', False)

        assert sent == value

    @staticmethod
    def read_ns(max_value: int, stream: ConstBitStream) -> int:
        assert max_value > 0

        if max_value == 1:
            return 0

        w = math.floor(math.log2(max_value)) + 1
        assert w > 1
        m = (1 << w) - max_value
        v = stream.read('uint:{}'.format(w - 1))
        if v < m:
            return v
        extra_bit = stream.read('bool')
        return (v << 1) - m + (1 if extra_bit else 0)

    @staticmethod
    def write_ns(max_value: int, value: int, stream: BitStream) -> None:
        assert max_value > 0
        assert 0 <= value < max_value

        if max_value == 1:
            return

        w = math.floor(math.log2(max_value)) + 1
        m = (1 << w) - max_value

        # Let's do a little bit of sanity checking. Write 'n' for max_value.
        # Firstly, we can compare m with n:
        #
        #  m = 2^(floor(log2(n)) + 1) - n    (defn of m, w)
        #    ≤ 2^(log2(n) + 1) - n
        #    = 2 n - n
        #    = n
        #
        # Substituting this in to m = 2^w - n and rearranging gives:
        #
        #  2^(w-1) ≤ n
        #
        # But then:
        #
        #  m = 2^w - n
        #    ≤ 2^w - 2^(w-1)
        #    = 2^(w-1)
        #
        # In the first case, where value is less than m, we know it will fit in
        # 2^(w-1) bits and can signal the value as just 'v'.
        if value < m:
            stream += pack('uint:{}'.format(w - 1), value)
            return

        # If the value is at least 'm', we're going to have to signal both v
        # and extra_bit. We know that:
        #
        #   (v << 1) - m + extra_bit = value
        #
        # Rearrange to: 2v + extra_bit = value + m. Since extra_bit is 0 or 1,
        # this has a unique solution.
        v = (value + m) // 2
        extra_bit = ((value + m) & 1) != 0

        # We'd better hope that v < 2^(w-1). But note that value < n, so:
        #
        #   v = (value + m) // 2
        #     ≤ (n + m) // 2        ((λ x. (x+m) // 2) is weakly increasing)
        #     ≤ (n + m) / 2
        #     = (n + 2^w - n) / 2   (defn of m)
        #     = 2^(w-1)
        #
        # Each inequality is sharp, but it turns out they can't both be. In
        # order to get equality on the first ≤, we need that value = n-1 and
        # for n+m to be odd. But then the second ≤ is strict.
        #
        # As such, we know that v < 2^(w-1), so it can be encoded with w-1
        # bits. We'd better also check that the reader will indeed read
        # extra_bit, which means we need v ≥ m. But we know that value ≥ m, so
        #
        #  v = (value + m) // 2
        #    ≥ (m + m) // 2
        #    = m

        stream += pack('uint:{}'.format(w - 1), v)
        stream += pack('bool', extra_bit)

    @staticmethod
    def read_su(nbits: int, stream: ConstBitStream) -> int:
        assert nbits > 0

        value = stream.read('uint:{}'.format(nbits))
        sign_mask = 1 << (nbits - 1)
        return (value - 2 * sign_mask
                if value & sign_mask
                else value)

    @staticmethod
    def write_su(nbits: int, value: int, stream: BitStream) -> None:
        assert nbits > 0
        min_val = - (1 << (nbits - 1))
        max_val = (1 << (nbits - 1)) - 1
        assert min_val <= value <= max_val

        coded = ((1 << nbits) + value if value < 0 else value)
        assert 0 <= coded < (1 << nbits)

        stream += pack('uint:{}'.format(nbits), coded)

    @staticmethod
    def read_signed_subexp_with_ref(low: int,
                                    high: int,
                                    stream: ConstBitStream) -> int:
        '''Like decode_signed_subexp_with_ref but just reads coded value'''
        assert low < high
        uval = RWObject.read_unsigned_subexp_with_ref(high - low, stream)
        return uval + low

    @staticmethod
    def read_unsigned_subexp_with_ref(mx: int, stream: ConstBitStream) -> int:
        '''Like decode_unsigned_subexp_with_ref but just reads coded value'''
        assert mx > 0
        return RWObject.decode_subexp(mx, stream)

    @staticmethod
    def decode_subexp(num_syms: int, stream: ConstBitStream) -> int:
        i = 0
        mk = 0
        k = 3
        while True:
            b2 = k + i - 1 if i else k
            a = 1 << b2
            if num_syms <= mk + 3 * a:
                assert mk < num_syms
                subexp_final_bits = RWObject.read_ns(num_syms - mk, stream)
                return subexp_final_bits + mk

            subexp_more_bits = stream.read('bool')
            if subexp_more_bits:
                i += 1
                mk += a
            else:
                subexp_bits = stream.read('uint:{}'.format(b2))
                return subexp_bits + mk

    @staticmethod
    def write_signed_subexp_with_ref(low: int,
                                     high: int,
                                     coded_value: int,
                                     stream: BitStream) -> None:
        '''Inverse of read_signed_subexp_with_ref'''
        assert low <= coded_value < high
        return RWObject.write_unsigned_subexp_with_ref(high - low,
                                                       coded_value - low,
                                                       stream)

    @staticmethod
    def write_unsigned_subexp_with_ref(mx: int,
                                       coded_value: int,
                                       stream: BitStream) -> None:
        '''Inverse of read_unsigned_subexp_with_ref'''
        assert 0 <= coded_value < mx
        return RWObject.encode_subexp(mx, coded_value, stream)

    @staticmethod
    def encode_subexp(num_syms: int,
                      coded_value: int,
                      stream: BitStream) -> None:
        assert 0 <= coded_value < num_syms
        i = 0
        mk = 0
        k = 3
        while True:
            assert mk <= coded_value

            b2 = k + i - 1 if i else k
            a = 1 << b2
            if num_syms <= mk + 3 * a:
                subexp_final_bits = coded_value - mk
                RWObject.write_ns(num_syms - mk, subexp_final_bits, stream)
                return

            subexp_more_bits = coded_value - mk >= a
            stream += pack('bool', subexp_more_bits)

            if subexp_more_bits:
                i += 1
                mk += a
            else:
                subexp_bits = coded_value - mk
                stream += pack('uint:{}'.format(b2), subexp_bits)
                return

    @staticmethod
    def read_ints(nbits: int, count: int, stream: ConstBitStream) -> List[int]:
        assert nbits > 0
        assert count >= 0
        fmt = 'uint:{}'.format(nbits)
        ret = []
        for idx in range(count):
            ret.append(stream.read(fmt))
        return ret

    @staticmethod
    def write_ints(nbits: int, ints: List[int], stream: BitStream) -> None:
        assert nbits > 0
        fmt = 'uint:{}'.format(nbits)
        for val in ints:
            stream += pack(fmt, val)

    @staticmethod
    def byte_align_reader(stream: ConstBitStream) -> None:
        length = (8 - stream.pos & 7) & 7
        for idx in range(length):
            bit = stream.read('bool')
            if bit:
                raise RuntimeError('Alignment bit at position {} ({} mod 8) '
                                   'is nonzero'
                                   .format(stream.pos, idx))

    @staticmethod
    def byte_align_writer(stream: BitStream) -> None:
        length = (8 - stream.length & 7) & 7
        for idx in range(length):
            stream += pack('bool', False)


class Leb128(RWObject):
    '''An object representing a leb128-encoded number.

    The non-negative value is the number itself and length is the number of
    bytes used. 0 is a special length that means "use as few bytes as
    possible".

    '''

    def __init__(self) -> None:
        self.value = 0
        self.length = 0

    def read(self, stream: ConstBitStream) -> None:
        result = 0
        shift = 0
        length = 0
        while True:
            val_byte = stream.read('uint:8')
            result |= (val_byte & 0x7F) << shift
            shift += 7
            length += 1
            if (val_byte & 0x80) == 0x00:
                break

        self.value = result
        self.length = length

    def write(self, stream: BitStream) -> None:
        value = self.value
        leb128_bytes = None  # type: Optional[int]
        if self.length > 0:
            assert self.length <= LEB128_MAX_SIZE
            leb128_bytes = self.length

        length = 0

        while True:
            bitstring_bottom = pack('uint:7', value & 0x7F)
            value = value >> 7
            length += 1
            more = (value != 0)
            if leb128_bytes is not None:
                if length != leb128_bytes:
                    more = True
                else:
                    assert not more

            if more:
                # more bytes to come
                stream.append('0b1')
                stream += bitstring_bottom
            else:
                # we are done
                stream.append('0b0')
                stream += bitstring_bottom
                break

        if leb128_bytes is not None:
            assert length == leb128_bytes
        assert length <= LEB128_MAX_SIZE

    def set_value(self, value: int) -> None:
        '''Try to set value, updating length if necessary'''
        assert value >= 0
        min_len = ((int(math.log2(value)) + 6) // 7
                   if value > 0 else 1)
        self.value = value
        if self.length > 0:
            self.length = max(min_len, self.length)


class Uvlc(RWObject):
    '''An object representing a uvlc-encoded number'''
    def __init__(self) -> None:
        self._scale = 0
        self._value = 0

    def read(self, stream: ConstBitStream) -> None:
        self._scale = 0
        self._value = 0
        while not stream.read('bool'):
            self._scale += 1

        if self._scale >= 32 or self._scale == 0:
            return

        self._value = stream.read('uint:{}'.format(self._scale))

    def write(self, stream: BitStream) -> None:
        for idx in range(self._scale):
            stream.append('0b0')
        stream.append('0b1')

        if self._scale >= 32 or self._scale == 0:
            return

        stream += pack('uint:{}'.format(self._scale), self._value)

    def set_num(self, num: int) -> None:
        '''Set the value stored'''
        assert num >= 0
        assert num <= 2**32 - 1

        encoded = (num + 1) // 2
        scale = 0
        while encoded:
            encoded = encoded // 2
            scale += 1

        assert 2**scale - 1 <= num
        assert scale <= 32

        value = num - (2 ** scale - 1)

        if scale == 32 or scale == 0:
            assert value == 0

        self._scale = scale
        self._value = value


class ObuExtension(RWObject):
    '''An object representing an OBU header's extension field'''

    def __init__(self) -> None:
        self.temporal_id = 0
        self.spatial_id = 0

    def read(self, stream: ConstBitStream) -> None:
        self.temporal_id = stream.read('uint:3')
        self.spatial_id = stream.read('uint:2')
        if stream.read('uint:3'):
            raise ValueError('extension_header_reserved_3bits nonzero '
                             'in obu_extension_header ending at '
                             'pos={} bytepos={}'
                             .format(stream.pos, stream.bytepos))

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:3', self.temporal_id)
        stream += pack('uint:2', self.spatial_id)
        stream += pack('uint:3', 0)


class ObuHeader(RWObject):
    '''An object representing an OBU header'''

    def __init__(self) -> None:
        self.obu_type = 0
        self.size = None  # type: Optional[Leb128]
        self.obu_extension = None  # type: Optional[ObuExtension]

    def reset_size(self, body_length: int) -> None:
        '''Reset size if defined'''
        assert body_length >= 0
        if self.size is not None:
            self.size.set_value(body_length)

    def read(self, stream: ConstBitStream) -> None:
        start_pos = stream.pos
        start_bytepos = stream.bytepos

        if stream.read('bool'):
            raise ValueError('obu_forbidden_bit set in obu_header at OBU '
                             'starting at pos={} bytepos={}'
                             .format(start_pos, start_bytepos))

        self.obu_type = stream.read('uint:4')
        obu_extension_flag = stream.read('bool')
        has_size_field = stream.read('bool')
        if stream.read('bool'):
            raise ValueError('obu_reserved_1bit set in obu_header at OBU '
                             'starting at pos={} bytepos={}'
                             .format(start_pos, start_bytepos))

        if obu_extension_flag:
            self.obu_extension = ObuExtension()
            self.obu_extension.read(stream)
        else:
            self.obu_extension = None

        if has_size_field:
            self.size = Leb128()
            self.size.read(stream)
        else:
            self.size = None

    def write(self, stream: BitStream) -> None:
        # obu_forbidden_bit     f(1)
        stream.append('0b0')
        # obu_type              f(4)
        stream += pack('uint:4', self.obu_type)
        # obu_extension_flag 	f(1)
        stream.append('0b0' if self.obu_extension is None else '0b1')
        # obu_has_size_field 	f(1)
        stream.append('0b0' if self.size is None else '0b1')
        # obu_reserved_1bit 	f(1)
        stream.append('0b0')

        if self.obu_extension is not None:
            self.obu_extension.write(stream)
        if self.size is not None:
            self.size.write(stream)

    def byte_len(self) -> int:
        '''The number of bytes to write this header'''
        ret = 1
        if self.obu_extension is not None:
            ret += 1
        if self.size is not None:
            ret += self.size.length

        return ret


class Obu(RWObject):
    '''An object representing an OBU'''

    def __init__(self, length: Optional[int]):
        self.length = length
        self.header = ObuHeader()
        self.body = ConstBitStream()

    def body_len(self, start_pos: int, start_bytepos: int) -> int:
        '''Calculate the remaining length from the header'''
        if self.header.size is not None:
            # The size encoded in the header is the number of bytes in the rest
            # of the OBU (not including the header, which was 1 or 2 bytes,
            # depending on whether there was an extension).
            #
            # Check this matches the length argument if that isn't None.
            if self.length is not None:
                sz_from_obu = self.header.byte_len() + self.header.size.value
                if sz_from_obu != self.length:
                    raise RuntimeError('Size field for OBU starting at '
                                       'pos={}, bytepos={} contains '
                                       '{}, which implies an OBU of {} bytes, '
                                       'but annexb data gives a length of {}.'
                                       .format(start_pos, start_bytepos,
                                               self.header.size,
                                               sz_from_obu, self.length))

            return self.header.size.value

        # We don't have a size field in the header. Let's hope that we have a
        # length.
        if self.length is None:
            raise RuntimeError('OBU starting at '
                               'pos={}, bytepos={} has no size '
                               'field, but there\'s no annexb length '
                               'either.'
                               .format(start_pos, start_bytepos))

        return self.length - self.header.byte_len()

    def _body_length(self) -> int:
        '''Get the length of the body in bytes'''
        assert 0 == self.body.length & 7
        return self.body.length // 8

    def get_length(self) -> int:
        '''Calculate the whole length of the OBU'''
        return self.header.byte_len() + self._body_length()

    def reset_length(self) -> None:
        '''Update fields if necessary to match body'''

    def read(self, stream: ConstBitStream) -> None:
        start_pos = stream.pos
        start_bytepos = stream.bytepos

        self.header.read(stream)
        body_len = self.body_len(start_pos, start_bytepos)
        self.body = stream.read('bits:{}'.format(body_len * 8))

    def write(self, stream: BitStream) -> None:
        start_pos = stream.pos
        start_bytepos = stream.bytepos

        self.header.write(stream)
        body_len = self.body_len(start_pos, start_bytepos)
        assert self.body.length == body_len * 8
        stream += self.body

    def set_body(self, body: ConstBitStream) -> None:
        '''Set the body of the OBU to this bitstream'''
        if body.length & 7:
            raise RuntimeError('Cannot set OBU body a '
                               'non-integral number of bytes.'
                               ' ({} bits, which is {} mod 8)'
                               .format(body.length, body.length & 7))
        self.body = body

        # We might need to fix up the header and length fields to match.
        # Firstly, set the size field of the OBU header. This is just supposed
        # to equal body_len.
        self.header.reset_size(self._body_length())

        # Now set length if necessary. This includes the length of the header.
        if self.length is not None:
            self.length = self.get_length()


class TimingInfo(RWObject):
    '''An object representing the data in timing_info'''
    def __init__(self) -> None:
        self.num_units_in_display_tick = 0
        self.time_scale = 0
        self.equal_picture_interval = False
        self.num_ticks_per_picture_minus_1 = None  # type: Optional[Uvlc]

    def read(self, stream: ConstBitStream) -> None:
        self.num_units_in_display_tick = stream.read('uint:32')
        self.time_scale = stream.read('uint:32')
        self.equal_picture_interval = stream.read('bool')
        if self.equal_picture_interval:
            self.num_ticks_per_picture_minus_1 = Uvlc()
            self.num_ticks_per_picture_minus_1.read(stream)
        else:
            self.num_ticks_per_picture_minus_1 = None

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:32', self.num_units_in_display_tick)
        stream += pack('uint:32', self.time_scale)
        stream += pack('bool', self.equal_picture_interval)
        if self.equal_picture_interval:
            assert self.num_ticks_per_picture_minus_1 is not None
            self.num_ticks_per_picture_minus_1.write(stream)


class DecoderModelInfo(RWObject):
    '''An object representing the data in decoder_model_info'''
    def __init__(self) -> None:
        self.buffer_delay_length_minus_1 = 0
        self.num_units_in_decoding_tick = 0
        self.buffer_removal_time_length_minus_1 = 0
        self.frame_presentation_time_length_minus_1 = 0

    def read(self, stream: ConstBitStream) -> None:
        self.buffer_delay_length_minus_1 = stream.read('uint:5')
        self.num_units_in_decoding_tick = stream.read('uint:32')
        self.buffer_removal_time_length_minus_1 = stream.read('uint:5')
        self.frame_presentation_time_length_minus_1 = stream.read('uint:5')

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:5', self.buffer_delay_length_minus_1)
        stream += pack('uint:32', self.num_units_in_decoding_tick)
        stream += pack('uint:5', self.buffer_removal_time_length_minus_1)
        stream += pack('uint:5', self.frame_presentation_time_length_minus_1)


class MISize:
    '''An object representing a frame's width/height in MI units'''
    def __init__(self, mi_rows: int, mi_cols: int) -> None:
        assert mi_rows > 0 and mi_cols > 0
        self.mi_rows = mi_rows
        self.mi_cols = mi_cols

    @staticmethod
    def mi_to_sb(mi_size: int, use_128x128_sb: bool) -> int:
        return ((mi_size + 31) // 32
                if use_128x128_sb
                else (mi_size + 15) // 16)

    def sb_cols(self, use_128x128_sb: bool) -> int:
        return MISize.mi_to_sb(self.mi_cols, use_128x128_sb)

    def sb_rows(self, use_128x128_sb: bool) -> int:
        return MISize.mi_to_sb(self.mi_rows, use_128x128_sb)

    def sb_area(self, use_128x128_sb: bool) -> int:
        return self.sb_cols(use_128x128_sb) * self.sb_rows(use_128x128_sb)

    @staticmethod
    def tile_log2(blk_size: int, target: int) -> int:
        assert blk_size > 0
        assert target >= 0

        k = 0
        while (blk_size << k) < target:
            k += 1

        return k

    @staticmethod
    def max_tile_width_sb(use_128x128_sb: bool) -> int:
        sb_size_log2 = 7 if use_128x128_sb else 6
        return MAX_TILE_WIDTH >> sb_size_log2

    def min_log2_tile_cols(self, use_128x128_sb: bool) -> int:
        return MISize.tile_log2(MISize.max_tile_width_sb(use_128x128_sb),
                                self.sb_cols(use_128x128_sb))

    def max_log2_tile_cols(self, use_128x128_sb: bool) -> int:
        return MISize.tile_log2(1,
                                min(self.sb_cols(use_128x128_sb),
                                    MAX_TILE_COLS))

    def max_log2_tile_rows(self, use_128x128_sb: bool) -> int:
        return MISize.tile_log2(1,
                                min(self.sb_rows(use_128x128_sb),
                                    MAX_TILE_ROWS))

    def min_log2_tiles(self, use_128x128_sb: bool) -> int:
        sb_size_log2 = 7 if use_128x128_sb else 6
        max_tile_area_sb = MAX_TILE_AREA >> (2 * sb_size_log2)
        return max(self.min_log2_tile_cols(use_128x128_sb),
                   MISize.tile_log2(max_tile_area_sb,
                                    self.sb_area(use_128x128_sb)))


class PixelSize:
    '''An object representing a frame's width/height in luma pixels'''
    def __init__(self, width: int, height: int) -> None:
        assert width >= 0
        assert height >= 0
        self.width = width
        self.height = height

    def in_mi_units(self) -> MISize:
        mi_rows = 2 * ((self.height + 7) // 8)
        mi_cols = 2 * ((self.width + 7) // 8)
        return MISize(mi_rows, mi_cols)


class RefFrameData:
    '''An object containing the data we track about reference buffers'''
    def __init__(self,
                 size: PixelSize,
                 frame_type: int,
                 order_hint: int):
        self.size = size
        self.frame_type = frame_type
        self.order_hint = order_hint


class RefFrames:
    '''An object representing the size of some reference buffers'''
    def __init__(self):
        self.refs = \
            [None] * NUM_REF_FRAMES  # type: List[Optional[RefFrameData]]

    def get_ref_frame(self, idx: int) -> RefFrameData:
        assert 0 <= idx < NUM_REF_FRAMES
        rfd = self.refs[idx]
        if rfd is None:
            raise RuntimeError('Ref frame at index {} is not valid.'
                               .format(idx))
        return rfd

    def store_ref_frame(self,
                        refresh_frame_flags: int,
                        rfd: RefFrameData) -> None:
        assert 0 <= refresh_frame_flags
        assert (refresh_frame_flags >> NUM_REF_FRAMES) == 0

        for idx in range(NUM_REF_FRAMES):
            if (refresh_frame_flags >> idx) & 1:
                self.refs[idx] = rfd

    def on_shown_keyframe(self) -> None:
        '''Clear all reference frames'''
        self.refs = [None] * NUM_REF_FRAMES

    def get_ref_order_hint(self) -> List[int]:
        '''Calculate the RefOrderHint structure for these references'''
        return [ref.order_hint if ref is not None else 0
                for ref in self.refs]

    def check_ref_order_hint(self, idx: int, order_hint: int) -> None:
        '''Clear validity bit of a ref frame if order hint is wrong'''
        assert idx < NUM_REF_FRAMES
        rfd = self.refs[idx]
        if rfd is not None and rfd.order_hint != order_hint:
            self.refs[idx] = None


class SeqFrameSize(RWObject):
    '''An object representing frame width/height in the sequence header'''
    def __init__(self) -> None:
        self.frame_width_bits_minus_1 = 0
        self.frame_height_bits_minus_1 = 0
        self.max_frame_width_minus_1 = 0
        self.max_frame_height_minus_1 = 0

    def read(self, stream: ConstBitStream) -> None:
        self.frame_width_bits_minus_1 = stream.read('uint:4')
        self.frame_height_bits_minus_1 = stream.read('uint:4')
        w_bits = self.frame_width_bits_minus_1 + 1
        self.max_frame_width_minus_1 = stream.read('uint:{}'.format(w_bits))
        h_bits = self.frame_height_bits_minus_1 + 1
        self.max_frame_height_minus_1 = stream.read('uint:{}'.format(h_bits))

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:4', self.frame_width_bits_minus_1)
        stream += pack('uint:4', self.frame_height_bits_minus_1)
        w_bits = self.frame_width_bits_minus_1 + 1
        stream += pack('uint:{}'.format(w_bits), self.max_frame_width_minus_1)
        h_bits = self.frame_height_bits_minus_1 + 1
        stream += pack('uint:{}'.format(h_bits), self.max_frame_height_minus_1)

    def pixel_size(self) -> PixelSize:
        '''Get the sequence-level max frame size'''
        return PixelSize(1 + self.max_frame_width_minus_1,
                         1 + self.max_frame_height_minus_1)


class OperatingParametersInfo(RWObject):
    '''The data in the operating_parameters_info syntax elements'''
    def __init__(self, buffer_delay_length_minus_1: int) -> None:
        self.encoder_buffer_delay = 0
        self.decoder_buffer_delay = 0
        self.low_delay_mode_flag = False
        self._buffer_delay_length_minus_1 = buffer_delay_length_minus_1

    def read(self, stream: ConstBitStream) -> None:
        fmt = 'uint:{}'.format(self._buffer_delay_length_minus_1 + 1)
        self.decoder_buffer_delay = stream.read(fmt)
        self.encoder_buffer_delay = stream.read(fmt)
        self.low_delay_mode_flag = stream.read('bool')

    def write(self, stream: BitStream) -> None:
        fmt = 'uint:{}'.format(self._buffer_delay_length_minus_1 + 1)
        stream += pack(fmt, self.decoder_buffer_delay)
        stream += pack(fmt, self.encoder_buffer_delay)
        stream += pack('bool', self.low_delay_mode_flag)

    def set_buffer_delay_length(self, bdl: int) -> None:
        '''Set the buffer delay length for reading/writing bitstream'''
        assert 1 <= bdl <= 32
        max_delay = 2 ** bdl - 1

        if self.decoder_buffer_delay > max_delay:
            raise RuntimeError('Cannot set buffer_delay_length to {} '
                               'because decoder_buffer_delay is {}.'
                               .format(bdl, self.decoder_buffer_delay))
        if self.encoder_buffer_delay > max_delay:
            raise RuntimeError('Cannot set buffer_delay_length to {} '
                               'because encoder_buffer_delay is {}.'
                               .format(bdl, self.encoder_buffer_delay))

        self._buffer_delay_length_minus_1 = bdl - 1


class Level(RWObject):
    def __init__(self, seq_level_idx: int):
        assert 0 <= seq_level_idx <= 23 or seq_level_idx == 31
        self.seq_level_idx = seq_level_idx

    def major_minor(self) -> Tuple[int, int]:
        '''Get major.minor syntax for level'''
        assert 0 <= self.seq_level_idx <= 23 or self.seq_level_idx == 31
        major = 2 + (self.seq_level_idx >> 2)
        minor = self.seq_level_idx & 3
        return (major, minor)

    def signals_seq_tier(self) -> bool:
        '''True if this level is high enough to signal seq_tier'''
        return self.major_minor()[0] >= 4

    def read(self, stream: ConstBitStream) -> None:
        self.seq_level_idx = stream.read('uint:5')
        assert 0 <= self.seq_level_idx <= 23 or self.seq_level_idx == 31

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:5', self.seq_level_idx)

    @staticmethod
    def from_string(txt: str) -> 'Level':
        '''Parse a level of the form MAJOR.MINOR

        MAJOR should be between 2 and 9 (inclusive) and MINOR should be between
        0 and 3 (inclusive). If MAJOR is 9, MINOR must be 3. If MAJOR is 2, 3
        or 4, MINOR must be 0 or 1.

        '''
        parts = txt.split('.', 1)
        if len(parts) != 2:
            raise ValueError('No \'.\' character in level: `{}\'.'
                             .format(txt))
        major_str, minor_str = parts

        try:
            major = int(major_str)
        except ValueError:
            raise ValueError('Cannot parse major part of level `{}\'.'
                             .format(txt)) from None

        try:
            minor = int(minor_str)
        except ValueError:
            raise ValueError('Cannot parse minor part of level `{}\'.'
                             .format(txt)) from None

        return Level.from_major_minor(major, minor)

    @staticmethod
    def from_major_minor(major: int, minor: int) -> 'Level':
        if not (2 <= major <= 6 or major == 9):
            raise ValueError('Invalid major version: {}.'.format(major))
        if not (0 <= minor <= 3):
            raise ValueError('Invalid minor version: {}.'.format(minor))

        seq_level_idx = 4 * (major - 2) + minor
        if 24 <= seq_level_idx <= 30:
            raise ValueError('Reserved level: {}.{}.'.format(major, minor))
        return Level(seq_level_idx)


class Levels(RWObject):
    def __init__(self, levels: List[Tuple[int, Level, Optional[bool]]]):
        # An entry in levels is (count, level, st) where count is non-negative
        # and is the number of times to use the entry. level is the level to
        # use. st is None if seq_tier should be left unmodified, otherwise is
        # the value to which we should set seq_tier.
        assert levels
        self._levels = levels
        self._idx = 0
        self._sub_idx = 0

    def pop_level(self) -> Tuple[Level, Optional[bool]]:
        assert self._levels
        idx = self._idx % len(self._levels)
        count, level, seq_tier = self._levels[idx]

        if count == 0:
            # "Repeat forever"
            assert self._sub_idx == 0
            return level, seq_tier

        assert self._sub_idx < count

        self._sub_idx += 1

        if self._sub_idx == count:
            self._sub_idx = 0
            self._idx += 1

        return level, seq_tier

    @staticmethod
    def _parse_term(txt: str) -> Tuple[int, Level, Optional[bool]]:
        '''Parse a term definition (see docstring for from_string)'''
        parts = txt.split('*', 1)
        if len(parts) != 2:
            raise ValueError('No \'*\' character in levels spec `{}\'.'
                             .format(txt))

        count_txt, rest_txt = parts[0].strip(), parts[1].strip()
        try:
            count = int(count_txt)
            if count < 0:
                raise ValueError('Count must be non-negative')
        except ValueError:
            raise ValueError('Failed to parse level count `{}\' '
                             'as a non-negative integer.'
                             .format(count_txt))

        seq_tier = None  # type: Optional[bool]
        if rest_txt.endswith('+'):
            seq_tier = True
            level_txt = rest_txt[:-1]
        elif rest_txt.endswith('-'):
            seq_tier = False
            level_txt = rest_txt[:-1]
        else:
            level_txt = rest_txt

        return (count, Level.from_string(level_txt), seq_tier)

    @staticmethod
    def from_string(txt: str) -> 'Levels':
        '''Parse a levels definition from string

             TERM  := COUNT '*' LEVEL ST
             LEVEL := MAJOR '.' MINOR
             MAJOR := '2' | '3' | '4' | '5' | '6' | '9'
             MINOR := '0' | '1' | '2' | '3'
             ST    := '' | '+' | '-'

        A term means "use LEVEL for COUNT sequence headers". If ST is empty,
        seq_tier is unaltered. If it is '-', seq_tier is forced to false. If it
        is '+', seq_tier is forced to true.

        '''

        try:
            return Levels([Levels._parse_term(part.strip())
                           for part in txt.split(',')])
        except ValueError as err:
            raise ValueError('Failed to parse levels from string `{}\' '
                             '(message: {}).'
                             .format(txt, err)) from None


class OperatingPoint(RWObject):
    '''The per-operating-point data in a sequence header'''
    def __init__(self,
                 buffer_delay_length_minus_1: Optional[int],
                 initial_display_delay_present_flag: bool) -> None:
        self.operating_point_idc = 0
        self.level = Level(0)
        self.seq_tier = False
        self.operating_parameters_info = \
            None  # type: Optional[OperatingParametersInfo]
        self.initial_display_delay_minus_1 = \
            None  # type: Optional[int]

        self._buffer_delay_length_minus_1 = buffer_delay_length_minus_1
        self._initial_display_delay_present_flag = \
            initial_display_delay_present_flag

    def read(self, stream: ConstBitStream) -> None:
        self.operating_point_idc = stream.read('uint:12')
        self.level.read(stream)
        self.seq_tier = (RWObject.
                         guarded_read_bool(self.level.signals_seq_tier(),
                                           stream))

        self.operating_parameters_info = None
        decoder_model_info_present_flag = \
            self._buffer_delay_length_minus_1 is not None
        if decoder_model_info_present_flag:
            assert self._buffer_delay_length_minus_1 is not None
            decoder_model_present_for_this_op = stream.read('bool')
            if decoder_model_present_for_this_op:
                self.operating_parameters_info = \
                    OperatingParametersInfo(self._buffer_delay_length_minus_1)
                self.operating_parameters_info.read(stream)

        self.initial_display_delay_minus_1 = None
        if self._initial_display_delay_present_flag:
            self.initial_display_delay_minus_1 = \
                RWObject.read_bit_int(4, stream)

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:12', self.operating_point_idc)
        self.level.write(stream)

        RWObject.guarded_write_bool(self.level.signals_seq_tier(),
                                    self.seq_tier, stream)

        decoder_model_info_present_flag = \
            self._buffer_delay_length_minus_1 is not None
        if decoder_model_info_present_flag:
            decoder_model_present_for_this_op = \
                self.operating_parameters_info is not None

            stream += pack('bool', decoder_model_present_for_this_op)
            if decoder_model_present_for_this_op:
                assert self.operating_parameters_info is not None
                self.operating_parameters_info.write(stream)

        if self._initial_display_delay_present_flag:
            RWObject.write_bit_int(4, self.initial_display_delay_minus_1,
                                   stream)

    def set_buffer_delay_length(self, bdl: Optional[int]) -> None:
        '''Set the buffer delay length to use when reading/writing

        If bdl is None, this means that
        decoder_model_info_present_flag is False.

        '''
        if bdl is None:
            # No decoder model info. Make sure that
            # operating_parameters_info is None.
            if self.operating_parameters_info is not None:
                raise RuntimeError('Can\'t clear buffer delay length when we '
                                   'have operating_parameters_info data.')

            self._buffer_delay_length_minus_1 = None
        else:
            assert bdl > 0
            self._buffer_delay_length_minus_1 = bdl - 1

            # If we have operating_parameters_info, make sure that it
            # knows about the new buffer delay length.
            if self.operating_parameters_info is not None:
                self.operating_parameters_info.set_buffer_delay_length(bdl)

    def set_ldm(self) -> None:
        '''Set low_delay_mode_flag for this operating point.

        This can only be done if decoder_model_info_present_flag is
        true (which we model by having a non-None buffer delay
        length).

        '''
        if self._buffer_delay_length_minus_1 is None:
            raise RuntimeError('Cannot set low_delay_mode_flag without '
                               'decoder_model_info_present_flag.')

        if self.operating_parameters_info is not None:
            self.operating_parameters_info.low_delay_mode_flag = True
        else:
            opi = OperatingParametersInfo(self._buffer_delay_length_minus_1)
            opi.decoder_buffer_delay = 0
            opi.encoder_buffer_delay = 0
            opi.low_delay_mode_flag = True
            self.operating_parameters_info = opi


class FrameIdInfo(RWObject):
    '''An object representing frame ID number info in the sequence header'''
    def __init__(self) -> None:
        self.delta_frame_id_length_minus_2 = 0
        self.additional_frame_id_length_minus_1 = 0

    def read(self, stream: ConstBitStream) -> None:
        self.delta_frame_id_length_minus_2 = stream.read('uint:4')
        self.additional_frame_id_length_minus_1 = stream.read('uint:3')

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:4', self.delta_frame_id_length_minus_2)
        stream += pack('uint:3', self.additional_frame_id_length_minus_1)

    def frame_id_length(self) -> int:
        '''Get the actual frame ID length'''
        return (self.delta_frame_id_length_minus_2 +
                self.additional_frame_id_length_minus_1 + 3)

    def __str__(self) -> str:
        return ('{{ delta_frame_id_length_minus_2: {}, '
                'additional_frame_id_length_minus_1: {} }}'
                .format(self.delta_frame_id_length_minus_2,
                        self.additional_frame_id_length_minus_1))

    def __eq__(self, other) -> bool:
        return ((self.delta_frame_id_length_minus_2 ==
                 other.delta_frame_id_length_minus_2) and
                (self.additional_frame_id_length_minus_1 ==
                 other.additional_frame_id_length_minus_1))


class ForceChoose(RWObject):
    '''An object that can be forced or chosen'''
    def __init__(self) -> None:
        # 0, 1 or 2 means "disable, enable, choose"
        self.value = 2

    def read(self, stream: ConstBitStream) -> None:
        choose = stream.read('bool')
        if choose:
            self.value = 2
        else:
            force = stream.read('bool')
            self.value = (1 if force else 0)

    def write(self, stream: BitStream) -> None:
        choose = self.value == 2
        stream += pack('bool', choose)
        if not choose:
            force = self.value == 1
            stream += pack('bool', force)

    def pick_one(self) -> bool:
        '''Return a compatible value, defaulting to False'''
        return True if self.value == 1 else False

    def read_one(self, stream: ConstBitStream) -> bool:
        '''Read the value for an instance.'''
        if self.value == 0:
            return False
        elif self.value == 1:
            return True
        else:
            assert self.value == 2
            return stream.read('bool')

    def write_one(self, stream: BitStream, val1: bool) -> None:
        '''Read the value for an instance.'''
        if self.value == 0:
            assert val1 is False
        elif self.value == 1:
            assert val1 is True
        else:
            assert self.value == 2
            stream += pack('bool', val1)


class ColorDescription(RWObject):
    '''The data hidden behind color_description_present_flag'''
    def __init__(self) -> None:
        self.color_primaries = CP_UNSPECIFIED
        self.transfer_characteristics = TC_UNSPECIFIED
        self.matrix_coefficients = MC_UNSPECIFIED

    def read(self, stream: ConstBitStream) -> None:
        self.color_primaries = stream.read('uint:8')
        self.transfer_characteristics = stream.read('uint:8')
        self.matrix_coefficients = stream.read('uint:8')

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:8', self.color_primaries)
        stream += pack('uint:8', self.transfer_characteristics)
        stream += pack('uint:8', self.matrix_coefficients)

    def is_100(self) -> bool:
        '''Return true if this is the magic color description'''
        return (self.color_primaries == CP_BT_709 and
                self.transfer_characteristics == TC_SRGB and
                self.matrix_coefficients == MC_IDENTITY)


class ColorConfig(RWObject):
    '''An object representing parsed color_config information'''
    def __init__(self, seq_profile: int) -> None:
        self.bit_depth = 8
        self.mono_chrome = False
        self.color_description = None  # type: Optional[ColorDescription]
        self.color_range = False
        self.subsampling_x = True
        self.subsampling_y = True
        self.chroma_sample_position = CSP_UNKNOWN
        self.separate_uv_delta_q = False

        self._seq_profile = seq_profile

    def is_422_ss(self) -> bool:
        '''Return true if this is 422 subsampling'''
        return self.subsampling_x and self.subsampling_y

    def read(self, stream: ConstBitStream) -> None:
        self.bit_depth = 8
        high_bitdepth = stream.read('bool')
        if high_bitdepth:
            self.bit_depth = 10
            if self._seq_profile == 2:
                twelve_bit = stream.read('bool')
                if twelve_bit:
                    self.bit_depth = 12

        self.mono_chrome = RWObject.guarded_read_bool(self._seq_profile != 1,
                                                      stream)

        color_description_present_flag = stream.read('bool')
        if color_description_present_flag:
            self.color_description = ColorDescription()
            self.color_description.read(stream)
        else:
            self.color_description = None

        if self.mono_chrome:
            self.color_range = stream.read('bool')
            self.subsampling_x = True
            self.subsampling_y = True
            self.chroma_sample_position = CSP_UNKNOWN
            self.separate_uv_delta_q = False
        elif (self.color_description is not None and
              self.color_description.is_100()):
            self.color_range = True
            self.subsampling_x = False
            self.subsampling_y = False
            self.chroma_sample_position = CSP_UNKNOWN
            self.separate_uv_delta_q = stream.read('bool')
        else:
            self.color_range = stream.read('bool')
            if self._seq_profile == 0:
                self.subsampling_x = True
                self.subsampling_y = True
            elif self._seq_profile == 1:
                self.subsampling_x = False
                self.subsampling_y = False
            else:
                assert self._seq_profile == 2
                if self.bit_depth == 12:
                    self.subsampling_x = stream.read('bool')
                    if self.subsampling_x:
                        self.subsampling_y = stream.read('bool')
                    else:
                        self.subsampling_y = False
                else:
                    self.subsampling_x = True
                    self.subsampling_y = False

            if self.subsampling_x and self.subsampling_y:
                self.chroma_sample_position = stream.read('uint:2')
            else:
                self.chroma_sample_position = CSP_UNKNOWN

            self.separate_uv_delta_q = stream.read('bool')

    def write(self, stream: BitStream) -> None:
        assert self.bit_depth in [8, 10, 12]
        if self.bit_depth == 12:
            assert self._seq_profile == 2

        # high_bitdepth
        stream += pack('bool', self.bit_depth > 8)
        if self._seq_profile == 2 and self.bit_depth > 8:
            # twelve_bit
            stream += pack('bool', self.bit_depth == 12)

        RWObject.guarded_write_bool(self._seq_profile != 1,
                                    self.mono_chrome, stream)

        color_description_present_flag = self.color_description is not None
        stream += pack('bool', color_description_present_flag)
        if color_description_present_flag:
            assert self.color_description is not None
            self.color_description.write(stream)

        if self.mono_chrome:
            stream += pack('bool', self.color_range)
            assert self.subsampling_x and self.subsampling_y
            assert self.chroma_sample_position == CSP_UNKNOWN
            assert not self.separate_uv_delta_q

        elif (self.color_description is not None and
              self.color_description.is_100()):
            assert self.color_range
            assert not (self.subsampling_x or self.subsampling_y)
            assert self.chroma_sample_position == CSP_UNKNOWN
            stream += pack('bool', self.separate_uv_delta_q)

        else:
            stream += pack('bool', self.color_range)
            if self._seq_profile == 0:
                assert self.subsampling_x and self.subsampling_y
            elif self._seq_profile == 1:
                assert not (self.subsampling_x or self.subsampling_y)
            else:
                assert self._seq_profile == 2
                if self.bit_depth == 12:
                    stream += pack('bool', self.subsampling_x)
                    if self.subsampling_x:
                        stream += pack('bool', self.subsampling_y)
                    else:
                        assert not self.subsampling_y
                else:
                    assert self.subsampling_x and not self.subsampling_y

            if self.subsampling_x and self.subsampling_y:
                stream += pack('uint:2', self.chroma_sample_position)
            else:
                assert self.chroma_sample_position == CSP_UNKNOWN

            stream += pack('bool', self.separate_uv_delta_q)


class SequenceHeader(RWObject):
    '''An object representing a parsed sequence header'''
    def __init__(self) -> None:
        self.seq_profile = 0
        self.still_picture = False
        self.reduced_still_picture_header = False
        self.timing_info = None  # type: Optional[TimingInfo]
        self.decoder_model_info = None  # type: Optional[DecoderModelInfo]
        self.initial_display_delay_present_flag = False
        self.operating_points = [OperatingPoint(None, False)]
        self.frame_size = SeqFrameSize()
        self.frame_id_info = None  # type: Optional[FrameIdInfo]
        self.use_128x128_superblock = False
        self.enable_filter_intra = False
        self.enable_intra_edge_filter = False
        self.enable_interintra_compound = False
        self.enable_masked_compound = False
        self.enable_warped_motion = False
        self.enable_dual_filter = False
        self.enable_order_hint = False
        self.enable_jnt_comp = False
        self.enable_ref_frame_mvs = False
        self.seq_force_screen_content_tools = ForceChoose()
        self.seq_force_integer_mv = ForceChoose()
        self.order_hint_bits_minus_1 = 0
        self.enable_superres = False
        self.enable_cdef = False
        self.enable_restoration = False
        self.color_config = ColorConfig(0)
        self.film_grain_params_present = False

    def decoder_model_info_present_flag(self) -> bool:
        '''The value of the corresponding syntax element.'''
        return self.decoder_model_info is not None

    def order_hint_bits(self) -> int:
        '''Compute OrderHintBits'''
        return (self.order_hint_bits_minus_1 + 1
                if self.enable_order_hint
                else 0)

    def frame_presentation_time_length_minus_1(self) -> Optional[int]:
        '''Return length from timing info or None'''
        return (self.decoder_model_info.frame_presentation_time_length_minus_1
                if (self.decoder_model_info is not None and
                    self.timing_info is not None and
                    not self.timing_info.equal_picture_interval)
                else None)

    def _read_rsp_init_header(self, stream: ConstBitStream) -> None:
        '''Read data for a reduced still picture header'''
        self.timing_info = None
        self.decoder_model_info = None
        self.initial_display_delay_present_flag = False
        op = OperatingPoint(None, False)
        op.level.read(stream)
        self.operating_points = [op]

    def _write_rsp_init_header(self, stream: BitStream) -> None:
        '''Write data for a reduced still picture header'''
        assert self.timing_info is None
        assert not self.decoder_model_info_present_flag()
        assert not self.initial_display_delay_present_flag
        assert len(self.operating_points) == 1
        self.operating_points[0].level.write(stream)

    def _read_normal_init_header(self, stream: ConstBitStream) -> None:
        '''Read initial data for a non-reduced-still-picture header'''
        self.timing_info = None
        self.decoder_model_info = None

        if stream.read('bool'):
            self.timing_info = TimingInfo()
            self.timing_info.read(stream)

            if stream.read('bool'):
                self.decoder_model_info = DecoderModelInfo()
                self.decoder_model_info.read(stream)

        self.initial_display_delay_present_flag = stream.read('bool')

        buffer_delay_length_minus_1 = \
            (self.decoder_model_info.buffer_delay_length_minus_1
             if self.decoder_model_info is not None
             else None)

        self.operating_points = []
        operating_points_cnt_minus_1 = stream.read('uint:5')

        for op_idx in range(1 + operating_points_cnt_minus_1):
            op = OperatingPoint(buffer_delay_length_minus_1,
                                self.initial_display_delay_present_flag)
            op.read(stream)
            self.operating_points.append(op)

    def _write_normal_init_header(self, stream: BitStream) -> None:
        '''Write initial data for a non-reduced-still-picture header'''
        timing_info_present_flag = self.timing_info is not None
        dmi_present_flag = self.decoder_model_info_present_flag()

        if dmi_present_flag:
            assert timing_info_present_flag

        stream += pack('bool', timing_info_present_flag)
        if timing_info_present_flag:
            assert self.timing_info is not None
            self.timing_info.write(stream)

            stream += pack('bool', dmi_present_flag)
            if dmi_present_flag:
                assert self.decoder_model_info is not None
                self.decoder_model_info.write(stream)

        stream += pack('bool', self.initial_display_delay_present_flag)

        operating_points_cnt_minus_1 = len(self.operating_points) - 1
        assert operating_points_cnt_minus_1 >= 0
        stream += pack('uint:5', operating_points_cnt_minus_1)

        for op in self.operating_points:
            op.write(stream)

    def read(self, stream: ConstBitStream) -> None:
        self.seq_profile = stream.read('uint:3')
        self.still_picture = stream.read('bool')
        self.reduced_still_picture_header = stream.read('bool')
        if self.reduced_still_picture_header:
            self._read_rsp_init_header(stream)
        else:
            self._read_normal_init_header(stream)

        self.frame_size.read(stream)

        frame_id_numbers_present_flag = False
        self.frame_id_info = None
        if not self.reduced_still_picture_header:
            frame_id_numbers_present_flag = stream.read('bool')

        if frame_id_numbers_present_flag:
            self.frame_id_info = FrameIdInfo()
            self.frame_id_info.read(stream)

        self.use_128x128_superblock = stream.read('bool')
        self.enable_filter_intra = stream.read('bool')
        self.enable_intra_edge_filter = stream.read('bool')

        if self.reduced_still_picture_header:
            self.enable_interintra_compound = False
            self.enable_masked_compound = False
            self.enable_warped_motion = False
            self.enable_dual_filter = False
            self.enable_order_hint = False
            self.enable_jnt_comp = False
            self.enable_ref_frame_mvs = False
            self.seq_force_screen_content_tools = ForceChoose()
            self.seq_force_integer_mv = ForceChoose()
            self.order_hint_bits_minus_1 = 0
        else:
            self.enable_interintra_compound = stream.read('bool')
            self.enable_masked_compound = stream.read('bool')
            self.enable_warped_motion = stream.read('bool')
            self.enable_dual_filter = stream.read('bool')
            self.enable_order_hint = stream.read('bool')
            if self.enable_order_hint:
                self.enable_jnt_comp = stream.read('bool')
                self.enable_ref_frame_mvs = stream.read('bool')
            else:
                self.enable_jnt_comp = False
                self.enable_ref_frame_mvs = False

            self.seq_force_screen_content_tools.read(stream)
            if self.seq_force_screen_content_tools.value > 0:
                self.seq_force_integer_mv.read(stream)
            else:
                self.seq_force_integer_mv = ForceChoose()

            if self.enable_order_hint:
                self.order_hint_bits_minus_1 = stream.read('uint:3')

        self.enable_superres = stream.read('bool')
        self.enable_cdef = stream.read('bool')
        self.enable_restoration = stream.read('bool')
        self.color_config = ColorConfig(self.seq_profile)
        self.color_config.read(stream)
        self.film_grain_params_present = stream.read('bool')

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:3', self.seq_profile)
        stream += pack('bool', self.still_picture)
        stream += pack('bool', self.reduced_still_picture_header)
        if self.reduced_still_picture_header:
            self._write_rsp_init_header(stream)
        else:
            self._write_normal_init_header(stream)

        self.frame_size.write(stream)

        frame_id_numbers_present_flag = self.frame_id_info is not None
        if not self.reduced_still_picture_header:
            stream += pack('bool', frame_id_numbers_present_flag)

        if frame_id_numbers_present_flag:
            assert not self.reduced_still_picture_header
            assert self.frame_id_info is not None
            self.frame_id_info.write(stream)

        stream += pack('bool', self.use_128x128_superblock)
        stream += pack('bool', self.enable_filter_intra)
        stream += pack('bool', self.enable_intra_edge_filter)

        if not self.reduced_still_picture_header:
            stream += pack('bool', self.enable_interintra_compound)
            stream += pack('bool', self.enable_masked_compound)
            stream += pack('bool', self.enable_warped_motion)
            stream += pack('bool', self.enable_dual_filter)
            stream += pack('bool', self.enable_order_hint)

            if self.enable_order_hint:
                stream += pack('bool', self.enable_jnt_comp)
                stream += pack('bool', self.enable_ref_frame_mvs)

            self.seq_force_screen_content_tools.write(stream)
            if self.seq_force_screen_content_tools.value > 0:
                self.seq_force_integer_mv.write(stream)

            if self.enable_order_hint:
                stream += pack('uint:3', self.order_hint_bits_minus_1)

        stream += pack('bool', self.enable_superres)
        stream += pack('bool', self.enable_cdef)
        stream += pack('bool', self.enable_restoration)

        self.color_config.write(stream)

        stream += pack('bool', self.film_grain_params_present)

    def set_decoder_model_info(self, dmi: Optional[DecoderModelInfo]) -> None:
        '''Set decoder_model_info. If dmi is None, clear it.'''
        has_dmi1 = dmi is not None

        if has_dmi1 and self.timing_info is None:
            raise RuntimeError('Cannot set decoder_model_info without '
                               'timing_info.')

        self.decoder_model_info = dmi

        bdl = (dmi.buffer_delay_length_minus_1 + 1
               if dmi is not None
               else None)

        for op in self.operating_points:
            op.set_buffer_delay_length(bdl)

    def set_fps(self, fps: int) -> None:
        '''Set a given frame rate'''
        # We need a positive frame rate
        assert fps > 0

        # If the frame rate is too large, our time_scale won't fit.
        assert fps < 2**32 // 100

        # Firstly, we don't support adding timing info to a
        # reduced_still_picture_header stream. Hopefully that won't
        # be needed?
        if self.reduced_still_picture_header:
            raise NotImplementedError('Setting frame rate with '
                                      'reduced_still_picture_header')

        # Otherwise, we can always write timing information. Let's see
        # whether we have some in the header already. If not, we'll make
        # some new.
        tinfo = self.timing_info or TimingInfo()

        # Firstly, we'll pick time_scale. If it was present in the
        # existing header, it will be a positive number. Otherwise, it
        # will be 0. We need at least 10 * fps (to avoid rounding
        # problems) and we'll set it to 100 * fps by default.
        if tinfo.time_scale < 10 * fps:
            tinfo.time_scale = 100 * fps

        # Pick num_units_in_display_tick. If it's not set, we'll make it
        # '1'. Otherwise, we'll leave it as-is as long as the resulting
        # display clock frequency is at least 10 * fps.
        if ((tinfo.num_units_in_display_tick == 0 or
             tinfo.time_scale < 10 * fps * tinfo.num_units_in_display_tick)):
            tinfo.num_units_in_display_tick = 1

        assert 10 * fps <= tinfo.time_scale // tinfo.num_units_in_display_tick

        # We definitely want to set equal_picture_interval. Remember
        # whether we've set it for the first time.
        set_epi = not tinfo.equal_picture_interval
        tinfo.equal_picture_interval = True

        # Finally, we'll set num_ticks_per_picture_minus_1 (let's defined
        # tpp to be the number of ticks per picture). This is measured in
        # display clock ticks so we want:
        #
        #   fps = time_scale / num_units_in_display_tick / tpp
        #
        # or
        #
        #   tpp = time_scale / (num_units_in_display_tick * fps)
        denom = tinfo.num_units_in_display_tick * fps
        tpp = max(1, (tinfo.time_scale + denom // 2) // denom)

        # Let's check that we get a plausible fps number (within 10% of
        # what we wanted)
        real_fps = tinfo.time_scale / tinfo.num_units_in_display_tick / tpp

        if not (0.9 * fps <= real_fps <= 1.1 * fps):
            raise RuntimeError('Failed to hit FPS target of {}. '
                               'Chose time_scale = {}; '
                               'num_units_in_display_tick = {}; '
                               'tpp = {}, which gives a real fps of {}.'
                               .format(fps,
                                       tinfo.time_scale,
                                       tinfo.num_units_in_display_tick,
                                       tpp,
                                       real_fps))

        tinfo.num_ticks_per_picture_minus_1 = Uvlc()
        tinfo.num_ticks_per_picture_minus_1.set_num(tpp - 1)

        # Set the header's timing info
        self.timing_info = tinfo

        # Finally, we need to do some sanity checking to make sure we
        # haven't corrupted the bitstream by setting
        # equal_picture_interval when it wasn't expected.
        #
        # If equal_picture_interval was false and we had a decoder model,
        # the uncompressed headers will contain temporal_point_info, which
        # we can't currently fix. This shouldn't be needed because if
        # decoder_model_info was set, we would have checked the decoder
        # model when generating the stream.
        if set_epi and self.decoder_model_info_present_flag():
            raise RuntimeError('Cannot set equal_picture_interval if '
                               'decoder_model_info exists.')

    def set_ldm(self) -> None:
        '''Set low_delay_mode_flag for all operating points'''

        # If we didn't have any timing info, we'll need some before we can
        # send decoder model data. Let's default to an equal picture
        # interval and a frame rate of 30 fps.
        if self.timing_info is None:
            self.set_fps(30)

        assert self.timing_info is not None

        # If the header doesn't already have a decoder model object, we
        # have to make ourselves a new one.
        if not self.decoder_model_info_present_flag():
            if not self.timing_info.equal_picture_interval:
                raise NotImplementedError('Can\'t add new decoder model info '
                                          'without equal picture interval.')

            dmi = DecoderModelInfo()

            # Make the operating_parameters_info structures we add as
            # small as we can make them.
            dmi.buffer_delay_length_minus_1 = 0

            # Run the decoding clock at double the rate of the display
            # clock, if we can
            dmi.num_units_in_decoding_tick = \
                max(1, self.timing_info.num_units_in_display_tick // 2)

            # Set the timestamps' length 32: this is a bit rubbish, but a
            # 32-bit number is surely going to be enough to signal the length!
            dmi.buffer_removal_time_length_minus_1 = 31
            dmi.frame_presentation_time_length_minus_1 = 31

            self.set_decoder_model_info(dmi)

        assert self.decoder_model_info_present_flag()

        for op in self.operating_points:
            op.set_ldm()


class TemporalPointInfo(RWObject):
    '''A class representing the syntax for temporal_point_info'''
    def __init__(self,
                 frame_presentation_time_length_minus_1: int) -> None:
        assert 0 <= frame_presentation_time_length_minus_1 <= 31
        self._fptlm1 = frame_presentation_time_length_minus_1

        self.frame_presentation_time = 0

    def read(self, stream: ConstBitStream) -> None:
        self.frame_presentation_time = \
            stream.read('uint:{}'.format(self._fptlm1 + 1))

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:{}'.format(self._fptlm1 + 1),
                       self.frame_presentation_time)


class SuperresParams(RWObject):
    def __init__(self,
                 enable_superres: bool) -> None:
        self._enable = enable_superres
        self.coded_denom = None

    def read(self, stream: ConstBitStream) -> None:
        self.coded_denom = None

        if not self._enable:
            return

        use_superres = stream.read('bool')
        if not use_superres:
            return

        self.coded_denom = \
            stream.read('uint:{}'.format(SUPERRES_DENOM_BITS))

    def write(self, stream: BitStream) -> None:
        if not self._enable:
            assert self.coded_denom is None
            return

        use_superres = self.coded_denom is not None
        stream += pack('bool', use_superres)

        if self.coded_denom is not None:
            stream += pack('uint:{}'.format(SUPERRES_DENOM_BITS),
                           self.coded_denom)

    def downscale(self, size: PixelSize) -> PixelSize:
        '''Downscale size (which should be the upscaled frame size)'''
        if self.coded_denom is None:
            return size

        superres_denom = self.coded_denom + SUPERRES_DENOM_MIN

        width = ((size.width * SUPERRES_NUM + (superres_denom // 2)) //
                 superres_denom)

        return PixelSize(width, size.height)


class FrameSize(RWObject):
    '''frame_size for uncompressed headers'''
    def __init__(self,
                 frame_size_override_flag: bool,
                 seq_frame_size: SeqFrameSize,
                 enable_superres: bool):

        self._fsof = frame_size_override_flag
        self._sfs = seq_frame_size

        self.frame_width_minus_1 = 0
        self.frame_height_minus_1 = 0
        self.superres_params = SuperresParams(enable_superres)

    def uses_superres(self) -> bool:
        return (self.frame_width_minus_1 + 1) != self._downscaled_size().width

    def read(self, stream: ConstBitStream) -> None:
        if self._fsof:
            wfmt = 'uint:{}'.format(self._sfs.frame_width_bits_minus_1 + 1)
            hfmt = 'uint:{}'.format(self._sfs.frame_height_bits_minus_1 + 1)
            self.frame_width_minus_1 = stream.read(wfmt)
            self.frame_height_minus_1 = stream.read(hfmt)
        else:
            self.frame_width_minus_1 = self._sfs.max_frame_width_minus_1
            self.frame_height_minus_1 = self._sfs.max_frame_height_minus_1

        self.superres_params.read(stream)

    def write(self, stream: BitStream) -> None:
        if self._fsof:
            wfmt = 'uint:{}'.format(self._sfs.frame_width_bits_minus_1 + 1)
            hfmt = 'uint:{}'.format(self._sfs.frame_height_bits_minus_1 + 1)
            stream += pack(wfmt, self.frame_width_minus_1)
            stream += pack(hfmt, self.frame_height_minus_1)
        else:
            assert ((self.frame_width_minus_1 ==
                     self._sfs.max_frame_width_minus_1) and
                    (self.frame_height_minus_1 ==
                     self._sfs.max_frame_height_minus_1))

        self.superres_params.write(stream)

    def upscaled_size(self) -> PixelSize:
        '''Get upscaled pixel size'''
        return PixelSize(1 + self.frame_width_minus_1,
                         1 + self.frame_height_minus_1)

    def _downscaled_size(self) -> PixelSize:
        '''Get downscaled pixel size'''
        return self.superres_params.downscale(self.upscaled_size())

    def mi_size(self) -> MISize:
        '''Get the downscaled size in MI units'''
        return self._downscaled_size().in_mi_units()


class RenderSize(RWObject):
    def __init__(self) -> None:
        self.render_sizes_minus_1 = None  # type: Optional[Tuple[int, int]]

    def read(self, stream: ConstBitStream) -> None:
        render_and_frame_size_different = stream.read('bool')
        if not render_and_frame_size_different:
            self.render_sizes_minus_1 = None
            return

        render_width_minus_1 = stream.read('uint:16')
        render_height_minus_1 = stream.read('uint:16')
        self.render_sizes_minus_1 = (render_width_minus_1,
                                     render_height_minus_1)

    def write(self, stream: BitStream) -> None:
        render_and_frame_size_different = self.render_sizes_minus_1 is not None
        stream += pack('bool', render_and_frame_size_different)
        if self.render_sizes_minus_1 is not None:
            stream += pack('uint:16', self.render_sizes_minus_1[0])
            stream += pack('uint:16', self.render_sizes_minus_1[1])


class IntraFrameData(RWObject):
    def __init__(self,
                 frame_size_override_flag: bool,
                 seq_frame_size: SeqFrameSize,
                 enable_superres: bool,
                 allow_screen_content_tools: bool) -> None:
        self.frame_size = FrameSize(frame_size_override_flag,
                                    seq_frame_size,
                                    enable_superres)
        self.render_size = RenderSize()
        self.allow_intrabc = False

        self._asct = allow_screen_content_tools

    def read(self, stream: ConstBitStream) -> None:
        self.frame_size.read(stream)
        self.render_size.read(stream)

        if self._asct and not self.frame_size.uses_superres():
            self.allow_intrabc = stream.read('bool')
        else:
            self.allow_intrabc = False

    def write(self, stream: BitStream) -> None:
        self.frame_size.write(stream)
        self.render_size.write(stream)

        if self._asct and not self.frame_size.uses_superres():
            stream += pack('bool', self.allow_intrabc)
        else:
            assert self.allow_intrabc is False

    def uses_superres(self) -> bool:
        return self.frame_size.uses_superres()


class InterFrameSize(RWObject):
    '''Frame size information for inter frames

    This corresponds to this code in the spec:

        if ( frame_size_override_flag && !error_resilient_mode ) {
            frame_size_with_refs( )
        } else {
            frame_size( )
            render_size( )
        }

    '''

    def __init__(self,
                 frame_size_override_flag: bool,
                 error_resilient_mode: bool,
                 seq_frame_size: SeqFrameSize,
                 enable_superres: bool,
                 ref_frame_idx: List[int]) -> None:
        assert len(ref_frame_idx) == REFS_PER_FRAME

        self.fsof = frame_size_override_flag
        self.erm = error_resilient_mode
        self.sfs = seq_frame_size
        self.es = enable_superres
        self.rfi = ref_frame_idx

        self.ref_idx = 0  # type: Optional[int]
        self.frame_size = None  # type: Optional[FrameSize]
        self.render_size = None  # type: Optional[RenderSize]
        self.superres_params = None  # type: Optional[SuperresParams]

    def read_frame_render_size(self, stream: ConstBitStream) -> None:
        '''Read frame_size and render size'''
        self.frame_size = FrameSize(self.fsof, self.sfs, self.es)
        self.frame_size.read(stream)

        self.render_size = RenderSize()
        self.render_size.read(stream)

    def write_frame_render_size(self, stream: BitStream) -> None:
        '''Write frame size and render size'''
        assert self.frame_size is not None
        assert self.render_size is not None
        assert self.superres_params is None

        self.frame_size.write(stream)
        self.render_size.write(stream)

    def read(self, stream: ConstBitStream) -> None:
        self.ref_idx = None
        self.frame_size = None
        self.render_size = None
        self.superres_params = None

        if self.erm or not self.fsof:
            # This is the second branch in the spec
            self.read_frame_render_size(stream)
            return

        # This is frame_size_with_refs
        for idx in range(REFS_PER_FRAME):
            if stream.read('bool'):
                self.ref_idx = idx
                break

        if self.ref_idx is None:
            self.read_frame_render_size(stream)
        else:
            self.superres_params = SuperresParams(self.es)
            self.superres_params.read(stream)

    def write(self, stream: BitStream) -> None:
        if self.erm or not self.fsof:
            # This is the second branch in the spec
            assert self.ref_idx is None

            self.write_frame_render_size(stream)
            return

        # This is frame_size_with_refs
        for idx in range(REFS_PER_FRAME):
            if self.ref_idx == idx:
                stream += pack('bool', True)
                break
            else:
                stream += pack('bool', False)

        if self.ref_idx is None:
            self.write_frame_render_size(stream)
        else:
            assert self.frame_size is None
            assert self.render_size is None
            assert self.superres_params is not None

            self.superres_params.write(stream)

    def upscaled_size(self, ref_frames: RefFrames) -> PixelSize:
        '''Get the upscaled size of this frame'''
        if self.frame_size is not None:
            assert self.ref_idx is None
            return self.frame_size.upscaled_size()
        else:
            assert self.ref_idx is not None

            assert 0 <= self.ref_idx < REFS_PER_FRAME

            buf_idx = self.rfi[self.ref_idx]
            return ref_frames.get_ref_frame(buf_idx).size

    def _get_ref_size(self, ref_frames: RefFrames) -> PixelSize:
        assert self.ref_idx is not None
        assert self.superres_params is not None

        assert 0 <= self.ref_idx < REFS_PER_FRAME

        buf_idx = self.rfi[self.ref_idx]
        return ref_frames.get_ref_frame(buf_idx).size

    def mi_size(self, ref_frames: RefFrames) -> MISize:
        '''Get the downscaled size of this frame in MI units'''
        if self.frame_size is not None:
            assert self.ref_idx is None
            return self.frame_size.mi_size()
        else:
            assert self.superres_params is not None

            ref_size = self._get_ref_size(ref_frames)
            downscaled = self.superres_params.downscale(ref_size)
            return downscaled.in_mi_units()

    def uses_superres(self, ref_frames: RefFrames) -> bool:
        if self.frame_size is not None:
            assert self.ref_idx is None
            return self.frame_size.uses_superres()
        else:
            assert self.superres_params is not None

            ref_size = self._get_ref_size(ref_frames)
            downscaled = self.superres_params.downscale(ref_size)
            return ref_size.width != downscaled.width


class InterpolationFilter(RWObject):
    def __init__(self) -> None:
        self.interpolation_filter = 0

    def read(self, stream: ConstBitStream) -> None:
        is_filter_switchable = stream.read('bool')
        if is_filter_switchable:
            self.interpolation_filter = SWITCHABLE
        else:
            self.interpolation_filter = stream.read('uint:2')

    def write(self, stream: BitStream) -> None:
        is_filter_switchable = (self.interpolation_filter == SWITCHABLE)
        stream += pack('bool', is_filter_switchable)
        if not is_filter_switchable:
            stream += pack('uint:2', self.interpolation_filter)


def relative_dist(order_hint_bits: int, hint_a: int, hint_b: int) -> int:
    assert order_hint_bits > 0
    diff = hint_a - hint_b
    m = 1 << (order_hint_bits - 1)
    return (diff & (m - 1)) - (diff & m)


class ShortRefEngine:
    '''A class that wraps up the set_frame_refs process logic'''
    def __init__(self,
                 order_hint_bits: int,
                 order_hint: int,
                 ref_order_hint: List[int]):
        assert len(ref_order_hint) == NUM_REF_FRAMES
        assert order_hint_bits > 0

        self.order_hint_bits = order_hint_bits
        self.order_hint = order_hint
        self.ref_order_hint = ref_order_hint

        self.cur_frame_hint = 1 << (order_hint_bits - 1)
        self.shifted_order_hints = \
            [self.cur_frame_hint +
             relative_dist(order_hint_bits, h, order_hint)
             for h in ref_order_hint]

    def compute_ref_frame_idx(self,
                              last_frame_idx: int,
                              gold_frame_idx: int) -> List[int]:
        '''Compute ref_frame_idx from short signalling data

        '''
        ref_frame_idx = [None] * REFS_PER_FRAME  # type: List[Optional[int]]
        ref_frame_idx[LAST_FRAME - LAST_FRAME] = last_frame_idx
        ref_frame_idx[GOLDEN_FRAME - LAST_FRAME] = gold_frame_idx

        used = [False] * NUM_REF_FRAMES
        used[last_frame_idx] = True
        used[gold_frame_idx] = True

        last_order_hint = self.shifted_order_hints[last_frame_idx]
        if last_order_hint >= self.cur_frame_hint:
            raise RuntimeError('Computed last_order_hint was {}, but '
                               'cur_frame_hint is {}.'
                               .format(last_order_hint, self.cur_frame_hint))

        gold_order_hint = self.shifted_order_hints[gold_frame_idx]
        if gold_order_hint >= self.cur_frame_hint:
            raise RuntimeError('Computed gold_order_hint was {}, but '
                               'cur_frame_hint is {}.'
                               .format(gold_order_hint, self.cur_frame_hint))

        ref = self.find_latest_backward(used)
        if ref >= 0:
            ref_frame_idx[ALTREF_FRAME - LAST_FRAME] = ref
            used[ref] = True

        ref = self.find_earliest_backward(used)
        if ref >= 0:
            ref_frame_idx[BWDREF_FRAME - LAST_FRAME] = ref
            used[ref] = True

        ref = self.find_earliest_backward(used)
        if ref >= 0:
            ref_frame_idx[ALTREF2_FRAME - LAST_FRAME] = ref
            used[ref] = True

        self.fill_backwards(used, ref_frame_idx)

        earliest_ref = self.earliest_ref()

        return [r if r is not None else earliest_ref for r in ref_frame_idx]

    def find_latest_backward(self, used: List[bool]) -> int:
        assert len(self.shifted_order_hints) == NUM_REF_FRAMES
        assert len(used) == NUM_REF_FRAMES

        ref = -1
        latest_order_hint = 12345
        for idx in range(NUM_REF_FRAMES):
            if used[idx]:
                continue
            hint = self.shifted_order_hints[idx]
            if hint < self.cur_frame_hint:
                continue
            if ref < 0 or hint >= latest_order_hint:
                ref = idx
                latest_order_hint = hint

        return ref

    def find_earliest_backward(self, used: List[bool]) -> int:
        assert len(self.shifted_order_hints) == NUM_REF_FRAMES
        assert len(used) == NUM_REF_FRAMES

        ref = -1
        earliest_order_hint = 12345
        for idx in range(NUM_REF_FRAMES):
            if used[idx]:
                continue
            hint = self.shifted_order_hints[idx]
            if hint < self.cur_frame_hint:
                continue
            if ref < 0 or hint < earliest_order_hint:
                ref = idx
                earliest_order_hint = hint

        return ref

    def find_latest_forward(self, used: List[bool]) -> int:
        assert len(self.shifted_order_hints) == NUM_REF_FRAMES
        assert len(used) == NUM_REF_FRAMES

        ref = -1
        latest_order_hint = 12345
        for idx in range(NUM_REF_FRAMES):
            if used[idx]:
                continue
            hint = self.shifted_order_hints[idx]
            if hint >= self.cur_frame_hint:
                continue
            if ref < 0 or hint >= latest_order_hint:
                ref = idx
                latest_order_hint = hint

        return ref

    def fill_backwards(self,
                       used: List[bool],
                       ref_frame_idx: List[Optional[int]]) -> None:
        '''Update used and ref_frame_idx in anti-chronological order'''
        assert len(self.shifted_order_hints) == NUM_REF_FRAMES
        assert len(used) == NUM_REF_FRAMES

        for ref_frame in [LAST2_FRAME, LAST3_FRAME,
                          BWDREF_FRAME, ALTREF2_FRAME, ALTREF_FRAME]:
            if ref_frame_idx[ref_frame - LAST_FRAME] is not None:
                continue
            ref = self.find_latest_forward(used)
            if ref >= 0:
                ref_frame_idx[ref_frame - LAST_FRAME] = ref
                used[ref] = True

    def earliest_ref(self) -> int:
        '''Find the reference frame with the smallest output order'''
        assert len(self.shifted_order_hints) == NUM_REF_FRAMES
        ref = -1
        earliest_order_hint = 12345
        for idx, hint in enumerate(self.shifted_order_hints):
            if ref < 0 or hint < earliest_order_hint:
                ref = idx
                earliest_order_hint = hint

        assert ref >= 0
        return ref


class InterFrameData(RWObject):
    def __init__(self,
                 order_hint_bits: int,
                 order_hint: int,
                 ref_frames: RefFrames,
                 delta_frame_id_length_minus_2: Optional[int],
                 frame_size_override_flag: bool,
                 error_resilient_mode: bool,
                 seq_frame_size: SeqFrameSize,
                 enable_superres: bool,
                 force_integer_mv: bool,
                 enable_ref_frame_mvs: bool) -> None:
        self.frame_refs_short_signaling = False
        self.last_frame_idx = None  # type: Optional[int]
        self.gold_frame_idx = None  # type: Optional[int]
        self.ref_frame_idx = [0] * REFS_PER_FRAME
        self.delta_frame_id_minus_1 = None  # type: Optional[List[int]]
        self.inter_frame_size = InterFrameSize(frame_size_override_flag,
                                               error_resilient_mode,
                                               seq_frame_size,
                                               enable_superres,
                                               [0] * REFS_PER_FRAME)
        self.allow_high_precision_mv = False
        self.interpolation_filter = InterpolationFilter()
        self.is_motion_mode_switchable = False
        self.use_ref_frame_mvs = False

        self.ohb = order_hint_bits
        self.oh = order_hint
        self.rf = ref_frames
        self.dfilm2 = delta_frame_id_length_minus_2
        self.fsof = frame_size_override_flag
        self.erm = error_resilient_mode
        self.sfs = seq_frame_size
        self.es = enable_superres
        self.fim = force_integer_mv
        self.erfm = enable_ref_frame_mvs

    def read(self, stream: ConstBitStream) -> None:
        rfi = []  # type: List[int]
        if not self.ohb > 0:
            self.frame_refs_short_signaling = False
            self.last_frame_idx = None
            self.gold_frame_idx = None
        else:
            self.frame_refs_short_signaling = stream.read('bool')
            if self.frame_refs_short_signaling:
                self.last_frame_idx = stream.read('uint:3')
                self.gold_frame_idx = stream.read('uint:3')

                assert self.last_frame_idx is not None
                assert self.gold_frame_idx is not None

                # set_frame_refs
                engine = ShortRefEngine(self.ohb, self.oh,
                                        self.rf.get_ref_order_hint())
                rfi = engine.compute_ref_frame_idx(self.last_frame_idx,
                                                   self.gold_frame_idx)
            else:
                self.last_frame_idx = None
                self.gold_frame_idx = None

        self.delta_frame_id_minus_1 = (None if self.dfilm2 is None
                                       else [])

        for idx in range(REFS_PER_FRAME):
            if not self.frame_refs_short_signaling:
                rfi.append(stream.read('uint:3'))
            if self.delta_frame_id_minus_1 is not None:
                assert self.dfilm2 is not None
                val = stream.read('uint:{}'.format(self.dfilm2 + 2))
                self.delta_frame_id_minus_1.append(val)

        self.ref_frame_idx = rfi

        assert (len(self.ref_frame_idx) == REFS_PER_FRAME)
        assert (self.delta_frame_id_minus_1 is None or
                len(self.delta_frame_id_minus_1) == REFS_PER_FRAME)

        self.inter_frame_size = InterFrameSize(self.fsof,
                                               self.erm,
                                               self.sfs,
                                               self.es,
                                               self.ref_frame_idx)
        self.inter_frame_size.read(stream)

        if self.fim:
            self.allow_high_precision_mv = False
        else:
            self.allow_high_precision_mv = stream.read('bool')

        self.interpolation_filter.read(stream)
        self.is_motion_mode_switchable = stream.read('bool')
        self.use_ref_frame_mvs = \
            RWObject.guarded_read_bool(self.erfm and not self.erm, stream)

    def write(self, stream: BitStream) -> None:
        if self.ohb == 0:
            assert self.frame_refs_short_signaling is False
            assert self.last_frame_idx is None
            assert self.gold_frame_idx is None
        else:
            stream += pack('bool', self.frame_refs_short_signaling)
            if self.frame_refs_short_signaling:
                stream += pack('uint:3', self.last_frame_idx)
                stream += pack('uint:3', self.gold_frame_idx)
            else:
                assert self.last_frame_idx is None
                assert self.gold_frame_idx is None

        for idx in range(REFS_PER_FRAME):
            assert ((self.delta_frame_id_minus_1 is None) ==
                    (self.dfilm2 is None))

            if not self.frame_refs_short_signaling:
                stream += pack('uint:3', self.ref_frame_idx[idx])
            if self.delta_frame_id_minus_1 is not None:
                assert self.dfilm2 is not None
                stream += pack('uint:{}'.format(self.dfilm2 + 2),
                               self.delta_frame_id_minus_1[idx])

        self.inter_frame_size.write(stream)

        if self.fim:
            assert self.allow_high_precision_mv is False
        else:
            stream += pack('bool', self.allow_high_precision_mv)

        self.interpolation_filter.write(stream)
        stream += pack('bool', self.is_motion_mode_switchable)
        RWObject.guarded_write_bool(self.erfm and not self.erm,
                                    self.use_ref_frame_mvs,
                                    stream)

    def uses_superres(self, ref_frames: RefFrames) -> bool:
        return self.inter_frame_size.uses_superres(ref_frames)


class UniformTileInfo(RWObject):
    def __init__(self,
                 mi_size: MISize,
                 use_128x128_superblock: bool) -> None:
        self.tile_cols_log2 = 0
        self.tile_rows_log2 = 0

        self.ms = mi_size
        self.u1s = use_128x128_superblock

    def read(self, stream: ConstBitStream) -> None:
        self.tile_cols_log2 = \
            RWObject.read_inc_range(self.ms.min_log2_tile_cols(self.u1s),
                                    self.ms.max_log2_tile_cols(self.u1s),
                                    stream)

        min_log2_tile_rows = \
            max(0, self.ms.min_log2_tiles(self.u1s) - self.tile_cols_log2)
        self.tile_rows_log2 = \
            RWObject.read_inc_range(min_log2_tile_rows,
                                    self.ms.max_log2_tile_rows(self.u1s),
                                    stream)

    def write(self, stream: BitStream) -> None:
        RWObject.write_inc_range(self.ms.min_log2_tile_cols(self.u1s),
                                 self.ms.max_log2_tile_cols(self.u1s),
                                 self.tile_cols_log2,
                                 stream)

        min_log2_tile_rows = \
            max(0, self.ms.min_log2_tiles(self.u1s) - self.tile_cols_log2)
        RWObject.write_inc_range(min_log2_tile_rows,
                                 self.ms.max_log2_tile_rows(self.u1s),
                                 self.tile_rows_log2,
                                 stream)

    def sum_rc_log2(self) -> int:
        '''Return TileColsLog2 + TileRowsLog2'''
        return self.tile_cols_log2 + self.tile_rows_log2


class NonUniformTileInfo(RWObject):
    def __init__(self,
                 mi_size: MISize,
                 use_128x128_superblock: bool) -> None:
        self.tile_widths_in_sb = [0]
        self.tile_heights_in_sb = [0]

        self.ms = mi_size
        self.u1s = use_128x128_superblock

    def _max_tile_height_sb(self, widest_tile_sb) -> int:
        '''Calculate maxTileHeightSb'''
        min_log2_tiles = self.ms.min_log2_tiles(self.u1s)
        max_tile_area_sb = (self.ms.sb_area(self.u1s) >>
                            (0 if min_log2_tiles == 0
                             else 1 + min_log2_tiles))
        return max(1, max_tile_area_sb // widest_tile_sb)

    def read(self, stream: ConstBitStream) -> None:
        self.tile_widths_in_sb = []
        self.tile_heights_in_sb = []

        start_sb = 0
        widest_tile_sb = 0
        sb_cols = self.ms.sb_cols(self.u1s)

        while start_sb < sb_cols:
            max_width = min(sb_cols - start_sb,
                            MISize.max_tile_width_sb(self.u1s))
            width_in_sbs_minus_1 = RWObject.read_ns(max_width, stream)
            size_sb = width_in_sbs_minus_1 + 1

            start_sb += size_sb
            widest_tile_sb = max(widest_tile_sb, size_sb)
            self.tile_widths_in_sb.append(size_sb)

        max_tile_height_sb = self._max_tile_height_sb(widest_tile_sb)

        start_sb = 0
        sb_rows = self.ms.sb_rows(self.u1s)
        while start_sb < sb_rows:
            max_height = min(sb_rows - start_sb, max_tile_height_sb)
            height_in_sbs_minus_1 = RWObject.read_ns(max_height, stream)
            size_sb = height_in_sbs_minus_1 + 1

            start_sb += size_sb
            self.tile_heights_in_sb.append(size_sb)

    def write(self, stream: BitStream) -> None:
        start_sb = 0
        widest_tile_sb = 0
        sb_cols = self.ms.sb_cols(self.u1s)

        for tile_width_in_sb in self.tile_widths_in_sb:
            assert start_sb < sb_cols
            assert tile_width_in_sb >= 1
            max_width = min(sb_cols - start_sb,
                            MISize.max_tile_width_sb(self.u1s))
            RWObject.write_ns(max_width, tile_width_in_sb - 1, stream)

            start_sb += tile_width_in_sb
            widest_tile_sb = max(widest_tile_sb, tile_width_in_sb)
        assert start_sb >= sb_cols

        max_tile_height_sb = self._max_tile_height_sb(widest_tile_sb)

        start_sb = 0
        sb_rows = self.ms.sb_rows(self.u1s)

        for tile_height_in_sb in self.tile_heights_in_sb:
            assert start_sb < sb_rows
            assert tile_height_in_sb >= 1

            max_height = min(sb_rows - start_sb, max_tile_height_sb)
            RWObject.write_ns(max_height, tile_height_in_sb - 1, stream)

            start_sb += tile_height_in_sb
        assert start_sb >= sb_rows

    def sum_rc_log2(self) -> int:
        '''Return TileColsLog2 + TileRowsLog2'''
        tile_cols_log2 = MISize.tile_log2(1, len(self.tile_widths_in_sb))
        tile_rows_log2 = MISize.tile_log2(1, len(self.tile_heights_in_sb))
        return tile_cols_log2 + tile_rows_log2


class TileInfo(RWObject):
    def __init__(self, mi_size: MISize, use_128x128_sb: bool) -> None:
        self.uniform_tile_info = UniformTileInfo(mi_size, use_128x128_sb)  \
            # type: Optional[UniformTileInfo]
        self.non_uniform_tile_info = None  # type: Optional[NonUniformTileInfo]

        self.context_update_tile_id = 0
        self.tile_size_bytes_minus_1 = None  # type: Optional[int]

        self.ms = mi_size
        self.u1s = use_128x128_sb

    def read(self, stream: ConstBitStream) -> None:
        uniform_tile_spacing_flag = stream.read('bool')
        rc_bits = 0

        if uniform_tile_spacing_flag:
            self.uniform_tile_info = UniformTileInfo(self.ms, self.u1s)
            self.non_uniform_tile_info = None

            self.uniform_tile_info.read(stream)
            rc_bits = self.uniform_tile_info.sum_rc_log2()
        else:
            self.uniform_tile_info = None
            self.non_uniform_tile_info = NonUniformTileInfo(self.ms, self.u1s)

            self.non_uniform_tile_info.read(stream)
            rc_bits = self.non_uniform_tile_info.sum_rc_log2()

        if rc_bits:
            self.context_update_tile_id = \
                stream.read('uint:{}'.format(rc_bits))
            self.tile_size_bytes_minus_1 = stream.read('uint:2')
        else:
            self.context_update_tile_id = 0
            self.tile_size_bytes_minus_1 = None

    def write(self, stream: BitStream) -> None:
        rc_bits = 0

        if self.uniform_tile_info is not None:
            assert self.non_uniform_tile_info is None

            stream += pack('bool', True)
            self.uniform_tile_info.write(stream)
            rc_bits = self.uniform_tile_info.sum_rc_log2()
        else:
            assert self.non_uniform_tile_info is not None

            stream += pack('bool', False)
            self.non_uniform_tile_info.write(stream)
            rc_bits = self.non_uniform_tile_info.sum_rc_log2()

        if rc_bits:
            self.tile_size_bytes_minus_1 is not None

            stream += pack('uint:{}'.format(rc_bits),
                           self.context_update_tile_id)
            stream += pack('uint:2', self.tile_size_bytes_minus_1)
        else:
            assert self.context_update_tile_id == 0
            self.tile_size_bytes_minus_1 is None


class DeltaQ(RWObject):
    def __init__(self) -> None:
        self.delta_coded = False
        self.delta_q = 0

    def read(self, stream: ConstBitStream) -> None:
        self.delta_coded = stream.read('bool')
        if self.delta_coded:
            self.delta_q = self.read_su(1 + 6, stream)
        else:
            self.delta_q = 0

    def write(self, stream: BitStream) -> None:
        stream += pack('bool', self.delta_coded)
        if self.delta_coded:
            self.write_su(1 + 6, self.delta_q, stream)
        else:
            assert self.delta_q == 0


class QuantizationParams(RWObject):
    def __init__(self,
                 mono_chrome: bool,
                 separate_uv_delta_q: bool) -> None:
        self.base_q_idx = 0
        self.delta_q_y_dc = DeltaQ()
        self.diff_uv_delta = False

        self.delta_q_u_dc = None if mono_chrome else DeltaQ()
        self.delta_q_u_ac = None if mono_chrome else DeltaQ()
        self.delta_q_v_dc = None  # type: Optional[DeltaQ]
        self.delta_q_v_ac = None  # type: Optional[DeltaQ]

        self.qm_y = None  # type: Optional[int]
        self.qm_u = None  # type: Optional[int]
        self.qm_v = None  # type: Optional[int]

        self.mc = mono_chrome
        self.sudq = separate_uv_delta_q

    def all_delta_qs_zero(self) -> bool:
        '''Return true if every delta_q_*_* field is zero'''
        return (self.delta_q_y_dc.delta_q == 0 and
                (self.delta_q_u_dc is None or
                 self.delta_q_u_dc.delta_q == 0) and
                (self.delta_q_u_ac is None or
                 self.delta_q_u_ac.delta_q == 0) and
                (self.delta_q_v_dc is None or
                 self.delta_q_v_dc.delta_q == 0) and
                (self.delta_q_v_ac is None or
                 self.delta_q_v_ac.delta_q == 0))

    def read(self, stream: ConstBitStream) -> None:
        self.base_q_idx = stream.read('uint:8')
        self.delta_q_y_dc.read(stream)

        if not self.mc:
            if self.sudq:
                self.diff_uv_delta = stream.read('bool')
            else:
                self.diff_uv_delta = False

            self.delta_q_u_dc = DeltaQ()
            self.delta_q_u_dc.read(stream)

            self.delta_q_u_ac = DeltaQ()
            self.delta_q_u_ac.read(stream)

            if self.diff_uv_delta:
                self.delta_q_v_dc = DeltaQ()
                self.delta_q_v_dc.read(stream)

                self.delta_q_v_ac = DeltaQ()
                self.delta_q_v_ac.read(stream)
            else:
                self.delta_q_v_dc = None
                self.delta_q_v_ac = None
        else:
            self.diff_uv_delta = False
            self.delta_q_u_dc = None
            self.delta_q_u_ac = None
            self.delta_q_v_dc = None
            self.delta_q_v_ac = None

        using_qmatrix = stream.read('bool')
        if using_qmatrix:
            self.qm_y = stream.read('uint:4')
            self.qm_u = stream.read('uint:4')
            if self.sudq:
                self.qm_v = stream.read('uint:4')
            else:
                self.qm_v = None
        else:
            self.qm_y = None
            self.qm_u = None
            self.qm_v = None

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:8', self.base_q_idx)
        self.delta_q_y_dc.write(stream)

        if not self.mc:
            if self.sudq:
                stream += pack('bool', self.diff_uv_delta)
            else:
                assert self.diff_uv_delta is False

            assert self.delta_q_u_dc is not None
            self.delta_q_u_dc.write(stream)

            assert self.delta_q_u_ac is not None
            self.delta_q_u_ac.write(stream)

            if self.diff_uv_delta:
                assert self.delta_q_v_dc is not None
                self.delta_q_v_dc.write(stream)

                assert self.delta_q_v_ac is not None
                self.delta_q_v_ac.write(stream)
            else:
                assert self.delta_q_v_dc is None
                assert self.delta_q_v_ac is None
        else:
            assert self.diff_uv_delta is False
            assert self.delta_q_u_dc is None
            assert self.delta_q_u_ac is None
            assert self.delta_q_v_dc is None
            assert self.delta_q_v_ac is None

        using_qmatrix = self.qm_y is not None
        stream += pack('bool', using_qmatrix)
        if using_qmatrix:
            assert self.qm_y is not None
            assert self.qm_u is not None
            stream += pack('uint:4', self.qm_y)
            stream += pack('uint:4', self.qm_u)
            if self.sudq:
                assert self.qm_v is not None
                stream += pack('uint:4', self.qm_v)
            else:
                assert self.qm_v is None
        else:
            assert self.qm_y is None
            assert self.qm_u is None
            assert self.qm_v is None


class Feature(RWObject):
    _SEGMENTATION_FEATURE_BITS = [8, 6, 6, 6, 6, 3, 0, 0]
    _SEGMENTATION_FEATURE_SIGNED = [True, True, True, True,
                                    True, False, False, False]

    def __init__(self, idx: int) -> None:
        assert 0 <= idx < SEG_LVL_MAX
        self._bits = Feature._SEGMENTATION_FEATURE_BITS[idx]
        self._signed = Feature._SEGMENTATION_FEATURE_SIGNED[idx]

        self.feature_value = 0

    def read(self, stream: ConstBitStream) -> None:
        if self._bits == 0:
            assert not self._signed
            self.feature_value = 0
            return

        if self._signed:
            self.feature_value = self.read_su(1 + self._bits, stream)
        else:
            self.feature_value = stream.read('uint:{}'.format(self._bits))

    def write(self, stream: BitStream) -> None:
        if self._bits == 0:
            assert not self._signed
            assert self.feature_value == 0
            return

        if self._signed:
            self.write_su(1 + self._bits, self.feature_value, stream)
        else:
            stream += pack('uint:{}'.format(self._bits), self.feature_value)


class Segment(RWObject):
    def __init__(self) -> None:
        self.features = \
            [None for _ in range(SEG_LVL_MAX)]  # type: List[Optional[Feature]]

    def read(self, stream: ConstBitStream) -> None:
        self.features = []
        for idx in range(SEG_LVL_MAX):
            feature_enabled = stream.read('bool')
            feature = None
            if feature_enabled:
                feature = Feature(idx)
                feature.read(stream)
            self.features.append(feature)
        assert len(self.features) == SEG_LVL_MAX

    def write(self, stream: BitStream) -> None:
        assert len(self.features) == SEG_LVL_MAX
        for feature in self.features:
            stream += pack('bool', feature is not None)
            if feature is not None:
                feature.write(stream)

    def q_idx(self, base_q_idx: int) -> int:
        '''Return the modified base_q_idx for this segment'''
        alt_q_feature = self.features[SEG_LVL_ALT_Q]
        if alt_q_feature is None:
            return base_q_idx

        value = alt_q_feature.feature_value + base_q_idx
        return max(0, min(255, value))


class SegmentationParams(RWObject):
    def __init__(self,
                 primary_ref_frame: int) -> None:
        self.segmentation_enabled = False
        self.segmentation_update_map = None  # type: Optional[bool]
        self.segmentation_temporal_update = None  # type: Optional[bool]
        self.segmentation_update_data = None  # type: Optional[bool]
        self.segments = None  # type: Optional[List[Segment]]

        self.prf = primary_ref_frame

    def all_zero_qindex(self, base_q_idx: int) -> bool:
        '''True if at all segments have a qindex of zero'''
        if self.segments is None:
            return base_q_idx == 0

        for seg in self.segments:
            if seg.q_idx(base_q_idx) != 0:
                return False

        return True

    def read(self, stream: ConstBitStream) -> None:
        self.segmentation_enabled = stream.read('bool')
        if self.segmentation_enabled:
            if self.prf == PRIMARY_REF_NONE:
                self.segmentation_update_map = True
                self.segmentation_temporal_update = False
                self.segmentation_update_data = True
            else:
                self.segmentation_update_map = stream.read('bool')
                if self.segmentation_update_map:
                    self.segmentation_temporal_update = stream.read('bool')
                else:
                    self.segmentation_temporal_update = False
                self.segmentation_update_data = stream.read('bool')

            if self.segmentation_update_data:
                self.segments = []
                for _ in range(MAX_SEGMENTS):
                    seg = Segment()
                    seg.read(stream)
                    self.segments.append(seg)
            else:
                self.segments = None
        else:
            self.segmentation_update_map = None
            self.segmentation_temporal_update = None
            self.segmentation_update_data = None
            self.segments = None

    def write(self, stream: BitStream) -> None:
        stream += pack('bool', self.segmentation_enabled)
        if self.segmentation_enabled:
            assert self.segmentation_update_map is not None
            assert self.segmentation_temporal_update is not None
            assert self.segmentation_update_data is not None

            if self.prf == PRIMARY_REF_NONE:
                assert self.segmentation_update_map is True
                assert self.segmentation_temporal_update is False
                assert self.segmentation_update_data is True
            else:
                stream += pack('bool', self.segmentation_update_map)
                if self.segmentation_update_map:
                    stream += pack('bool', self.segmentation_temporal_update)
                else:
                    assert self.segmentation_temporal_update is False
                stream += pack('bool', self.segmentation_update_data)

            if self.segmentation_update_data:
                assert self.segments is not None
                assert len(self.segments) == MAX_SEGMENTS
                for seg in self.segments:
                    seg.write(stream)
            else:
                assert self.segments is None
        else:
            assert self.segmentation_update_map is None
            assert self.segmentation_temporal_update is None
            assert self.segmentation_update_data is None
            assert self.segments is None


class DeltaQParams(RWObject):
    def __init__(self, base_q_idx: int) -> None:
        self.delta_q_res = None  # type: Optional[int]

        self._bqi = base_q_idx

    def read(self, stream: ConstBitStream) -> None:
        if self._bqi > 0:
            delta_q_present = stream.read('bool')
        else:
            delta_q_present = False

        if delta_q_present:
            self.delta_q_res = stream.read('uint:2')
        else:
            self.delta_q_res = None

    def write(self, stream: BitStream) -> None:
        if self._bqi > 0:
            delta_q_present = self.delta_q_res is not None
            stream += pack('bool', delta_q_present)
        else:
            assert self.delta_q_res is None

        if self.delta_q_res is not None:
            stream += pack('uint:2', self.delta_q_res)


class DeltaLFParams(RWObject):
    def __init__(self,
                 delta_q_present: bool,
                 allow_intrabc: bool) -> None:
        self.delta_lf_res = None  # type: Optional[int]
        self.delta_lf_multi = None  # type: Optional[bool]

        self._allowed = delta_q_present and not allow_intrabc

    def read(self, stream: ConstBitStream) -> None:
        if self._allowed:
            delta_lf_present = stream.read('bool')
        else:
            delta_lf_present = False

        if delta_lf_present:
            self.delta_lf_res = stream.read('uint:2')
            self.delta_lf_multi = stream.read('bool')

    def write(self, stream: BitStream) -> None:
        assert (self.delta_lf_res is None) == (self.delta_lf_multi is None)
        if self._allowed:
            delta_lf_present = self.delta_lf_res is not None
            stream += pack('bool', delta_lf_present)
        else:
            assert self.delta_lf_res is None
            assert self.delta_lf_multi is None

        if self.delta_lf_res is not None:
            assert self.delta_lf_multi is not None
            stream += pack('uint:2', self.delta_lf_res)
            stream += pack('bool', self.delta_lf_multi)


class LoopFilterDeltaUpdate(RWObject):
    def __init__(self) -> None:
        self.loop_filter_ref_deltas = \
            [None] * TOTAL_REFS_PER_FRAME  # type: List[Optional[int]]
        self.loop_filter_mode_deltas = \
            [None] * 2  # type: List[Optional[int]]

    @staticmethod
    def read_sus(num_elts: int, stream: ConstBitStream) -> List[Optional[int]]:
        assert num_elts >= 0
        ret = []
        for _ in range(num_elts):
            exists = stream.read('bool')
            value = None  # type: Optional[int]
            if exists:
                value = RWObject.read_su(1 + 6, stream)
            ret.append(value)

        assert len(ret) == num_elts
        return ret

    @staticmethod
    def write_sus(sus: List[Optional[int]], stream: BitStream) -> None:
        for value in sus:
            exists = value is not None
            stream += pack('bool', exists)
            if value is not None:
                RWObject.write_su(1 + 6, value, stream)

    def read(self, stream: ConstBitStream) -> None:
        self.loop_filter_ref_deltas = \
            LoopFilterDeltaUpdate.read_sus(TOTAL_REFS_PER_FRAME, stream)
        self.loop_filter_mode_deltas = \
            LoopFilterDeltaUpdate.read_sus(2, stream)

    def write(self, stream: BitStream) -> None:
        assert len(self.loop_filter_ref_deltas) == TOTAL_REFS_PER_FRAME
        assert len(self.loop_filter_mode_deltas) == 2

        LoopFilterDeltaUpdate.write_sus(self.loop_filter_ref_deltas,
                                        stream)
        LoopFilterDeltaUpdate.write_sus(self.loop_filter_mode_deltas,
                                        stream)


class LoopFilterParams(RWObject):
    def __init__(self, mono_chrome: bool) -> None:
        self.loop_filter_level = [0, 0]
        self.loop_filter_sharpness = 0
        self.loop_filter_delta_enabled = False
        self.loop_filter_delta_update = \
            None  # type: Optional[LoopFilterDeltaUpdate]

        self._mono_chrome = mono_chrome

    def read(self, stream: ConstBitStream) -> None:
        self.loop_filter_level = RWObject.read_ints(6, 2, stream)
        if not (self._mono_chrome or self.loop_filter_level == [0, 0]):
            self.loop_filter_level += RWObject.read_ints(6, 2, stream)

        self.loop_filter_sharpness = stream.read('uint:3')
        self.loop_filter_delta_enabled = stream.read('bool')
        if self.loop_filter_delta_enabled:
            has_delta_update = stream.read('bool')
        else:
            has_delta_update = False

        if has_delta_update:
            self.loop_filter_delta_update = LoopFilterDeltaUpdate()
            self.loop_filter_delta_update.read(stream)
        else:
            self.loop_filter_delta_update = None

    def write(self, stream: BitStream) -> None:
        assert len(self.loop_filter_level) in [2, 4]
        if len(self.loop_filter_level) == 4:
            assert self.loop_filter_level[:2] != [0, 0]
            assert not self._mono_chrome

        RWObject.write_ints(6, self.loop_filter_level, stream)

        stream += pack('uint:3', self.loop_filter_sharpness)
        stream += pack('bool', self.loop_filter_delta_enabled)

        if self.loop_filter_delta_enabled:
            has_delta_update = self.loop_filter_delta_update is not None
            stream += pack('bool', has_delta_update)

        if self.loop_filter_delta_update is not None:
            assert self.loop_filter_delta_enabled
            self.loop_filter_delta_update.write(stream)


class CdefStrength(RWObject):
    def __init__(self) -> None:
        self.pri = 0
        self.sec = 0

    def read(self, stream: ConstBitStream) -> None:
        self.pri = stream.read('uint:4')
        self.sec = stream.read('uint:2')

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:4', self.pri)
        stream += pack('uint:2', self.sec)


class CdefStrengths(RWObject):
    def __init__(self, mono_chrome: bool) -> None:
        self.y_strength = CdefStrength()
        self.uv_strength = None if mono_chrome else CdefStrength()
        self._mono_chrome = mono_chrome

    def read(self, stream: ConstBitStream) -> None:
        self.y_strength.read(stream)
        if self._mono_chrome:
            self.uv_strength = None
        else:
            self.uv_strength = CdefStrength()
            self.uv_strength.read(stream)

    def write(self, stream: BitStream) -> None:
        self.y_strength.write(stream)
        if self._mono_chrome:
            assert self.uv_strength is None
        else:
            assert self.uv_strength is not None
            self.uv_strength.write(stream)


class CdefParams(RWObject):
    def __init__(self, mono_chrome: bool) -> None:
        self.cdef_damping_minus_3 = 0
        self.cdef_bits = 0
        self.strengths = [CdefStrengths(mono_chrome)]

        self._mono_chrome = mono_chrome

    def read(self, stream: ConstBitStream) -> None:
        self.cdef_damping_minus_3 = stream.read('uint:2')
        self.cdef_bits = stream.read('uint:2')
        num_strengths = 1 << self.cdef_bits
        self.strengths = []
        for _ in range(num_strengths):
            ss = CdefStrengths(self._mono_chrome)
            ss.read(stream)
            self.strengths.append(ss)

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:2', self.cdef_damping_minus_3)
        stream += pack('uint:2', self.cdef_bits)
        num_strengths = 1 << self.cdef_bits
        assert len(self.strengths) == num_strengths
        for ss in self.strengths:
            ss.write(stream)


class LrParams(RWObject):
    def __init__(self,
                 mono_chrome: bool,
                 use_128x128_sb: bool,
                 is_422_ss: bool) -> None:
        num_planes = 1 if mono_chrome else 3

        self.lr_types = [0] * num_planes
        self.lr_unit_shift = None  # type: Optional[bool]
        self.lr_unit_extra_shift = None  # type: Optional[bool]
        self.lr_uv_shift = None  # type: Optional[bool]

        self._mono_chrome = mono_chrome
        self._use_128x128_sb = use_128x128_sb
        self._is_422_ss = is_422_ss

    def read(self, stream: ConstBitStream) -> None:
        num_planes = 1 if self._mono_chrome else 3
        uses_lr = False
        uses_chroma_lr = False

        self.lr_types = []
        for plane in range(num_planes):
            lr_type = stream.read('uint:2')
            self.lr_types.append(lr_type)
            if lr_type:
                uses_lr = True
                if plane:
                    uses_chroma_lr = True

        if uses_lr:
            if self._use_128x128_sb:
                self.lr_unit_shift = stream.read('bool')
                self.lr_unit_extra_shift = None
            else:
                self.lr_unit_shift = stream.read('bool')
                if self.lr_unit_shift:
                    self.lr_unit_extra_shift = stream.read('bool')
                else:
                    self.lr_unit_extra_shift = None

            if self._is_422_ss and uses_chroma_lr:
                self.lr_uv_shift = stream.read('bool')
            else:
                self.lr_uv_shift = None
        else:
            self.lr_unit_shift = None
            self.lr_unit_extra_shift = None
            self.lr_uv_shift = None

    def write(self, stream: BitStream) -> None:
        num_planes = 1 if self._mono_chrome else 3
        uses_lr = False
        uses_chroma_lr = False

        assert len(self.lr_types) == num_planes
        for plane, lr_type in enumerate(self.lr_types):
            stream += pack('uint:2', lr_type)
            if lr_type:
                uses_lr = True
                if plane:
                    uses_chroma_lr = True

        if uses_lr:
            if self._use_128x128_sb:
                stream += pack('bool', self.lr_unit_shift)
                assert self.lr_unit_extra_shift is None
            else:
                stream += pack('bool', self.lr_unit_shift)
                if self.lr_unit_shift:
                    stream += pack('bool', self.lr_unit_extra_shift)
                else:
                    assert self.lr_unit_extra_shift is None

            if self._is_422_ss and uses_chroma_lr:
                stream += pack('bool', self.lr_uv_shift)
            else:
                assert self.lr_uv_shift is None
        else:
            assert self.lr_unit_shift is None
            assert self.lr_unit_extra_shift is None
            assert self.lr_uv_shift is None


class GMParam(RWObject):
    def __init__(self,
                 gm_type: int,
                 idx: int,
                 allow_high_precision_mv: bool) -> None:
        self.gm_type = gm_type
        self.idx = idx
        self.allow_high_precision_mv = allow_high_precision_mv

        self.coded_value = 0

    def get_abs_bits(self) -> int:
        if self.idx < 2:
            if self.gm_type == TRANSLATION:
                return (GM_ABS_TRANS_ONLY_BITS -
                        int(not self.allow_high_precision_mv))
            else:
                return GM_ABS_TRANS_BITS
        else:
            return GM_ABS_ALPHA_BITS

    def read(self, stream: ConstBitStream) -> None:
        mx = 1 << self.get_abs_bits()
        self.coded_value = \
            RWObject.read_signed_subexp_with_ref(- mx, mx + 1, stream)

    def write(self, stream: BitStream) -> None:
        mx = 1 << self.get_abs_bits()
        RWObject.write_signed_subexp_with_ref(- mx, mx + 1,
                                              self.coded_value, stream)


class FrameGlobalMotionParams(RWObject):
    def __init__(self, allow_high_precision_mv: bool) -> None:
        self.gm_type = IDENTITY
        self.gm_params = [None] * 6  # type: List[Optional[GMParam]]

        self.allow_high_precision_mv = allow_high_precision_mv

    def read_param(self, idx: int, stream: ConstBitStream) -> GMParam:
        ret = GMParam(self.gm_type, idx, self.allow_high_precision_mv)
        ret.read(stream)
        return ret

    def read(self, stream: ConstBitStream) -> None:
        is_global = stream.read('bool')
        if is_global:
            is_rot_zoom = stream.read('bool')
            if is_rot_zoom:
                self.gm_type = ROTZOOM
            else:
                is_translation = stream.read('bool')
                self.gm_type = TRANSLATION if is_translation else AFFINE
        else:
            self.gm_type = IDENTITY

        self.gm_params = [None] * 6
        if self.gm_type >= ROTZOOM:
            self.gm_params[2] = self.read_param(2, stream)
            self.gm_params[3] = self.read_param(3, stream)
            if self.gm_type == AFFINE:
                self.gm_params[4] = self.read_param(4, stream)
                self.gm_params[5] = self.read_param(5, stream)
        if self.gm_type >= TRANSLATION:
            self.gm_params[0] = self.read_param(0, stream)
            self.gm_params[1] = self.read_param(1, stream)

    def write(self, stream: BitStream) -> None:
        is_global = self.gm_type > IDENTITY
        stream += pack('bool', is_global)
        if is_global:
            is_rot_zoom = self.gm_type == ROTZOOM
            stream += pack('bool', is_rot_zoom)
            if not is_rot_zoom:
                is_translation = self.gm_type == TRANSLATION
                stream += pack('bool', is_translation)

        if self.gm_type >= ROTZOOM:
            assert self.gm_params[2] is not None
            assert self.gm_params[3] is not None
            self.gm_params[2].write(stream)
            self.gm_params[3].write(stream)
            if self.gm_type == AFFINE:
                assert self.gm_params[4] is not None
                assert self.gm_params[5] is not None
                self.gm_params[4].write(stream)
                self.gm_params[5].write(stream)
            else:
                assert self.gm_params[4] is None
                assert self.gm_params[5] is None
        else:
            assert self.gm_params[2] is None
            assert self.gm_params[3] is None

        if self.gm_type >= TRANSLATION:
            assert self.gm_params[0] is not None
            assert self.gm_params[1] is not None
            self.gm_params[0].write(stream)
            self.gm_params[1].write(stream)
        else:
            assert self.gm_params[0] is None
            assert self.gm_params[1] is None


class GlobalMotionParams(RWObject):
    def __init__(self, allow_high_precision_mv: bool) -> None:
        self.per_frame = [FrameGlobalMotionParams(allow_high_precision_mv)
                          for idx in range(LAST_FRAME, ALTREF_FRAME + 1)]

    def read(self, stream: ConstBitStream) -> None:
        for idx, frame_params in enumerate(self.per_frame):
            frame_params.read(stream)

    def write(self, stream: BitStream) -> None:
        for frame_params in self.per_frame:
            frame_params.write(stream)


class FilmGrainPoint(RWObject):
    def __init__(self) -> None:
        self.value = 0
        self.scaling = 0

    def read(self, stream: ConstBitStream) -> None:
        self.value = stream.read('uint:8')
        self.scaling = stream.read('uint:8')

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:8', self.value)
        stream += pack('uint:8', self.scaling)


class FilmGrainMultOffs(RWObject):
    def __init__(self) -> None:
        self.mult = 0
        self.luma_mult = 0
        self.offset = 0

    def read(self, stream: ConstBitStream) -> None:
        self.mult = stream.read('uint:8')
        self.luma_mult = stream.read('uint:8')
        self.offset = stream.read('uint:9')

    def write(self, stream: BitStream) -> None:
        stream += pack('uint:8', self.mult)
        stream += pack('uint:8', self.luma_mult)
        stream += pack('uint:9', self.offset)


class FilmGrainUpdate(RWObject):
    def __init__(self,
                 mono_chrome: bool,
                 is_422_ss: bool) -> None:
        self.y_points = []  # type: List[FilmGrainPoint]
        self.chroma_scaling_from_luma = False
        self.cb_points = []  # type: List[FilmGrainPoint]
        self.cr_points = []  # type: List[FilmGrainPoint]
        self.grain_scaling_minus_8 = 0
        self.ar_coeff_lag = 0
        self.ar_coeffs_y_plus_128 = []  # type: List[int]
        self.ar_coeffs_cb_plus_128 = []  # type: List[int]
        self.ar_coeffs_cr_plus_128 = []  # type: List[int]
        self.ar_coeff_shift_minus_6 = 0
        self.grain_scale_shift = 0
        self.cb_mult_offs = None  # type: Optional[FilmGrainMultOffs]
        self.cr_mult_offs = None  # type: Optional[FilmGrainMultOffs]
        self.overlap_flag = False
        self.clip_to_restricted_range = False

        self._mono_chrome = mono_chrome
        self._is_422_ss = is_422_ss

    @staticmethod
    def read_points(stream: ConstBitStream) -> List[FilmGrainPoint]:
        num_points = stream.read('uint:4')
        points = []
        for _ in range(num_points):
            pt = FilmGrainPoint()
            pt.read(stream)
            points.append(pt)
        return points

    @staticmethod
    def write_points(points: List[FilmGrainPoint],
                     stream: ConstBitStream) -> None:
        num_points = len(points)
        stream += pack('uint:4', num_points)
        for pt in points:
            pt.write(stream)

    def read(self, stream: ConstBitStream) -> None:
        self.y_points = self.read_points(stream)

        if self._mono_chrome:
            self.chroma_scaling_from_luma = False
        else:
            self.chroma_scaling_from_luma = stream.read('bool')

        no_chroma_points = (self._mono_chrome or
                            self.chroma_scaling_from_luma or
                            (self._is_422_ss and not self.y_points))
        if no_chroma_points:
            self.cb_points = []
            self.cr_points = []
        else:
            self.cb_points = self.read_points(stream)
            self.cr_points = self.read_points(stream)

        self.grain_scaling_minus_8 = stream.read('uint:2')
        self.ar_coeff_lag = stream.read('uint:2')

        num_pos_luma = 2 * self.ar_coeff_lag * (self.ar_coeff_lag + 1)
        if self.y_points:
            num_pos_chroma = num_pos_luma + 1
            self.ar_coeffs_y_plus_128 = \
                RWObject.read_ints(8, num_pos_luma, stream)
        else:
            num_pos_chroma = num_pos_luma
            self.ar_coeffs_y_plus_128 = []

        if self.chroma_scaling_from_luma or self.cb_points:
            self.ar_coeffs_cb_plus_128 = \
                RWObject.read_ints(8, num_pos_chroma, stream)
        else:
            self.ar_coeffs_cb_plus_128 = []

        if self.chroma_scaling_from_luma or self.cr_points:
            self.ar_coeffs_cr_plus_128 = \
                RWObject.read_ints(8, num_pos_chroma, stream)
        else:
            self.ar_coeffs_cr_plus_128 = []

        self.ar_coeff_shift_minus_6 = stream.read('uint:2')
        self.grain_scale_shift = stream.read('uint:2')

        if self.cb_points:
            self.cb_mult_offs = FilmGrainMultOffs()
            self.cb_mult_offs.read(stream)
        else:
            self.cb_mult_offs = None

        if self.cr_points:
            self.cr_mult_offs = FilmGrainMultOffs()
            self.cr_mult_offs.read(stream)
        else:
            self.cr_mult_offs = None

        self.overlap_flag = stream.read('bool')
        self.clip_to_restricted_range = stream.read('bool')

    def write(self, stream: BitStream) -> None:
        self.write_points(self.y_points, stream)

        if self._mono_chrome:
            assert self.chroma_scaling_from_luma is False
        else:
            stream += pack('bool', self.chroma_scaling_from_luma)

        no_chroma_points = (self._mono_chrome or
                            self.chroma_scaling_from_luma or
                            (self._is_422_ss and not self.y_points))
        if no_chroma_points:
            assert self.cb_points == []
            assert self.cr_points == []
        else:
            self.write_points(self.cb_points, stream)
            self.write_points(self.cr_points, stream)

        stream += pack('uint:2', self.grain_scaling_minus_8)
        stream += pack('uint:2', self.ar_coeff_lag)

        num_pos_luma = 2 * self.ar_coeff_lag * (self.ar_coeff_lag + 1)
        if self.y_points:
            num_pos_chroma = num_pos_luma + 1
            assert len(self.ar_coeffs_y_plus_128) == num_pos_luma
            RWObject.write_ints(8, self.ar_coeffs_y_plus_128, stream)
        else:
            num_pos_chroma = num_pos_luma
            assert self.ar_coeffs_y_plus_128 == []

        if self.chroma_scaling_from_luma or self.cb_points:
            assert len(self.ar_coeffs_cb_plus_128) == num_pos_chroma
            RWObject.write_ints(8, self.ar_coeffs_cb_plus_128, stream)
        else:
            assert self.ar_coeffs_cb_plus_128 == []

        if self.chroma_scaling_from_luma or self.cr_points:
            assert len(self.ar_coeffs_cr_plus_128) == num_pos_chroma
            RWObject.write_ints(8, self.ar_coeffs_cr_plus_128, stream)
        else:
            assert self.ar_coeffs_cr_plus_128 == []

        stream += pack('uint:2', self.ar_coeff_shift_minus_6)
        stream += pack('uint:2', self.grain_scale_shift)

        if self.cb_points:
            assert self.cb_mult_offs is not None
            self.cb_mult_offs.write(stream)
        else:
            assert self.cb_mult_offs is None

        if self.cr_points:
            assert self.cr_mult_offs is not None
            self.cr_mult_offs.write(stream)
        else:
            assert self.cr_mult_offs is None

        stream += pack('bool', self.overlap_flag)
        stream += pack('bool', self.clip_to_restricted_range)


class FilmGrainParams(RWObject):
    def __init__(self,
                 frame_type: int,
                 mono_chrome: bool,
                 is_422_ss: bool) -> None:
        self.apply_grain = False
        self.grain_seed = None  # type: Optional[int]
        self.film_grain_params_ref_idx = None  # type: Optional[int]
        self.update = None  # type: Optional[FilmGrainUpdate]

        self._frame_type = frame_type
        self._mono_chrome = mono_chrome
        self._is_422_ss = is_422_ss

    def read(self, stream: ConstBitStream) -> None:
        self.apply_grain = stream.read('bool')
        if not self.apply_grain:
            self.grain_seed = None
            self.film_grain_params_ref_idx = None
            self.update = None
            return

        self.grain_seed = stream.read('uint:16')

        if self._frame_type == INTER_FRAME:
            update_grain = stream.read('bool')
        else:
            update_grain = True

        if not update_grain:
            self.film_grain_params_ref_idx = stream.read('uint:3')
            self.update = None
        else:
            self.film_grain_params_ref_idx = None
            self.update = FilmGrainUpdate(self._mono_chrome,
                                          self._is_422_ss)
            self.update.read(stream)

    def write(self, stream: BitStream) -> None:
        stream += pack('bool', self.apply_grain)
        if not self.apply_grain:
            assert self.grain_seed is None
            assert self.film_grain_params_ref_idx is None
            assert self.update is None
            return

        assert self.grain_seed is not None
        stream += pack('uint:16', self.grain_seed)

        if self._frame_type == INTER_FRAME:
            update_grain = self.update is not None
            stream += pack('bool', update_grain)
        else:
            update_grain = True

        if not update_grain:
            assert self.film_grain_params_ref_idx is not None
            assert self.update is None
            stream += pack('uint:3', self.film_grain_params_ref_idx)
        else:
            assert self.update is not None
            assert self.film_grain_params_ref_idx is None
            self.update.write(stream)


class UncompressedHeader(RWObject):
    '''A class representing (some of) an uncompressed header

    '''
    # If frame_presentation_time_length_minus_1 is not None, this means that we
    # should read/write temporal_point_info.
    #
    # Similarly, if frame_id_len is not None, this is the value that would be
    # computed for idLen at the start of uncompressed_header.
    #
    # decoder_model_info is None if there is no decoder model info. Otherwise,
    # it is a pair (n, flags) where n is buffer_removal_time_length_minus_1 + 1
    # and flags is a list with an bool per operating point which is true iff
    # decoder_model_present_for_this_op[opNum] is true and we are in the right
    # temporal and spatial layer.
    def __init__(self,
                 sh: SequenceHeader,
                 decoder_model_info: Optional[Tuple[int, List[bool]]],
                 ref_frames: RefFrames) -> None:

        self.sh = sh
        self.dmi = decoder_model_info
        self.rf = ref_frames

        # This first block is read by read_initial_header.
        self.show_existing_frame = True
        self.frame_to_show_map_idx = None  # type: Optional[int]
        self.frame_type = KEY_FRAME
        self.show_frame = False
        self.temporal_point_info = None  # type: Optional[TemporalPointInfo]
        self.display_frame_id = None  # type: Optional[int]
        self.showable_frame = False
        self.error_resilient_mode = False

        self.disable_cdf_update = False
        self.allow_screen_content_tools = False
        self.force_integer_mv = False
        self.current_frame_id = None  # type: Optional[int]
        self.frame_size_override_flag = False
        self.order_hint = 0
        self.primary_ref_frame = PRIMARY_REF_NONE
        self.buffer_removal_times = None  # type: Optional[List[int]]
        self.refresh_frame_flags = (1 << NUM_REF_FRAMES) - 1
        self.ref_order_hint = None  # type: Optional[List[int]]

        self.intra_frame_data = None  # type: Optional[IntraFrameData]
        self.inter_frame_data = None  # type: Optional[InterFrameData]

        self.disable_frame_end_update_cdf = False
        self.tile_info = None  # type: Optional[TileInfo]
        self.quantization_params = None  # type: Optional[QuantizationParams]
        self.segmentation_params = None  # type: Optional[SegmentationParams]
        self.delta_q_params = None  # type: Optional[DeltaQParams]
        self.delta_lf_params = None  # type: Optional[DeltaLFParams]
        self.loop_filter_params = None  # type: Optional[LoopFilterParams]
        self.cdef_params = None  # type: Optional[CdefParams]
        self.lr_params = None  # type: Optional[LrParams]
        self.tx_mode_select = None  # type: Optional[bool]
        self.reference_select = None  # type: Optional[bool]
        self.skip_mode_present = None  # type: Optional[bool]
        self.allow_warped_motion = None  # type: Optional[bool]
        self.reduced_tx_set = None  # type: Optional[bool]
        self.global_motion_params = None  # type: Optional[GlobalMotionParams]
        self.film_grain_params = None  # type: Optional[FilmGrainParams]

    def update(self,
               sh: SequenceHeader,
               decoder_model_info: Optional[Tuple[int, List[bool]]],
               ref_frames: RefFrames) -> None:
        '''Update mode so that we serialise correctly'''
        self.sh = copy.deepcopy(sh)
        self.dmi = copy.deepcopy(decoder_model_info)
        self.rf = copy.deepcopy(ref_frames)

    def can_start_cvs(self) -> bool:
        '''Return true if this frame header can start a CVS'''
        return (self.frame_type == KEY_FRAME and
                self.show_frame is True and
                self.show_existing_frame is False)

    def get_ref_frame(self) -> RefFrameData:
        '''Get RefFrameData and downscaled size for this frame'''

        # If we have show_existing_frame set, we actually need to look up
        # frame_to_show_map_idx in our references and use that instead.
        if self.show_existing_frame:
            assert self.frame_to_show_map_idx is not None
            return self.rf.get_ref_frame(self.frame_to_show_map_idx)

        # Otherwise, we have a frame of our own and should make a RefFrameData
        # object to represent it.
        if self.intra_frame_data is not None:
            assert self.inter_frame_data is None
            size = self.intra_frame_data.frame_size.upscaled_size()
        else:
            assert self.inter_frame_data is not None
            size = (self.inter_frame_data.
                    inter_frame_size.upscaled_size(self.rf))

        return RefFrameData(size, self.frame_type, self.order_hint)

    def mi_size(self) -> MISize:
        '''Get the downscaled size of this frame in MI units'''
        assert not self.show_existing_frame
        if self.intra_frame_data is not None:
            assert self.inter_frame_data is None
            return self.intra_frame_data.frame_size.mi_size()

        assert self.inter_frame_data is not None
        return self.inter_frame_data.inter_frame_size.mi_size(self.rf)

    def uses_superres(self) -> bool:
        if self.intra_frame_data is not None:
            assert self.inter_frame_data is None
            return self.intra_frame_data.uses_superres()
        else:
            assert self.inter_frame_data is not None
            return self.inter_frame_data.uses_superres(self.rf)

    def add_decoder_model_info(self,
                               settings: 'Settings',
                               removal_time: int):
        '''Add decoder model information using the given settings'''
        dmi = settings.mk_dmi()

        # Make sure that we don't already have buffer removal times
        if self.buffer_removal_times is not None:
            raise NotImplementedError('Cannot add decoder_model_info '
                                      'with existing removal times.')

        # Make sure that the decoder model info we're adding isn't None
        if dmi is None:
            raise RuntimeError('Can\'t call add_decoder_model_info with '
                               'no decoder_model_info.')

        self.dmi = dmi

        # Signal the given removal time for each operating point.
        n_bits, flags = dmi
        if removal_time >> n_bits:
            raise ValueError('Cannot represent removal time of '
                             '{} in {} bits.'
                             .format(removal_time, n_bits))
        n_times = 0
        for flag in flags:
            if flag:
                n_times += 1

        self.buffer_removal_times = [removal_time] * n_times

    def maybe_read_tpi(self,
                       stream: ConstBitStream) -> Optional[TemporalPointInfo]:
        fptlm1 = self.sh.frame_presentation_time_length_minus_1()
        if fptlm1 is None:
            return None

        tpi = TemporalPointInfo(fptlm1)
        tpi.read(stream)
        return tpi

    def maybe_write_tpi(self,
                        stream: BitStream,
                        tpi: Optional[TemporalPointInfo]) -> None:
        fptlm1 = self.sh.frame_presentation_time_length_minus_1()
        if fptlm1 is None:
            if tpi is not None:
                raise RuntimeError('Cannot write TemporalPointInfo because '
                                   'frame_presentation_time_length_minus_1 '
                                   'is None.')
            return

        if tpi is None:
            raise RuntimeError('Need a TemporalPointInfo object because '
                               'frame_presentation_time_length_minus_1 '
                               'is not None.')
        tpi.write(stream)

    def frame_id_format(self) -> Optional[str]:
        '''Return the string to read/write frame_id_len bits or None'''
        if self.sh.frame_id_info is None:
            return None
        else:
            return 'uint:{}'.format(self.sh.frame_id_info.frame_id_length())

    def maybe_read_id(self, stream: ConstBitStream) -> Optional[int]:
        '''Read frame_id_len bits to an integer if it is not None'''
        fmt = self.frame_id_format()
        return stream.read(fmt) if fmt is not None else None

    def maybe_write_id(self, stream: BitStream, value: Optional[int]) -> None:
        '''Write an integer of frame_id_len bits if not None'''
        fmt = self.frame_id_format()
        if fmt is None:
            if value is not None:
                raise RuntimeError('Cannot write id with no length.')
            return

        if value is None:
            raise RuntimeError('Need an id to write.')

        stream += pack(fmt, value)

    def frame_is_intra(self) -> bool:
        '''Returns true if this is an intra frame'''
        return self.frame_type in [INTRA_ONLY_FRAME, KEY_FRAME]

    def coded_lossless(self) -> bool:
        '''Returns true if this frame is coded lossless'''
        assert self.quantization_params is not None
        assert self.segmentation_params is not None

        return (self.quantization_params.all_delta_qs_zero() and
                (self.segmentation_params.
                 all_zero_qindex(self.quantization_params.base_q_idx)))

    def skip_mode_allowed(self,
                          ref_frame_idx: List[int]) -> bool:
        '''Calculates skipModeAllowed (in skip_mode_params in the spec)'''
        assert len(ref_frame_idx) == REFS_PER_FRAME

        ohb = self.sh.order_hint_bits()

        assert not self.frame_is_intra()

        if ohb == 0 or not self.reference_select:
            return False

        fwd_idx = -1
        fwd_hint = 12345
        bwd_idx = -1
        bwd_hint = 12345

        ref_order_hint = self.rf.get_ref_order_hint()
        assert len(ref_order_hint) == NUM_REF_FRAMES

        for idx, ref_idx in enumerate(ref_frame_idx):
            assert 0 <= ref_idx < NUM_REF_FRAMES
            ref_hint = ref_order_hint[ref_idx]
            ref_to_now = relative_dist(ohb, ref_hint, self.order_hint)
            if ref_to_now < 0:
                if ((fwd_idx < 0 or
                     relative_dist(ohb, ref_hint, fwd_hint)) > 0):
                    fwd_idx = idx
                    fwd_hint = ref_hint
            elif ref_to_now > 0:
                if ((bwd_idx < 0 or
                     relative_dist(ohb, ref_hint, bwd_hint)) > 0):
                    bwd_idx = idx
                    bwd_hint = ref_hint

        if fwd_idx < 0:
            return False

        if bwd_idx >= 0:
            return True

        fwd_idx2 = -1
        fwd_hint2 = 12345

        for idx, ref_idx in enumerate(ref_frame_idx):
            assert 0 <= ref_idx < NUM_REF_FRAMES
            ref_hint = ref_order_hint[ref_idx]
            if relative_dist(ohb, ref_hint, fwd_hint) >= 0:
                continue
            if fwd_idx2 < 0 or relative_dist(ohb, ref_hint, fwd_hint2) >= 0:
                fwd_idx2 = idx
                fwd_hint2 = ref_hint

        return fwd_idx2 >= 0

    def read_initial_header(self, stream: ConstBitStream) -> None:
        '''Read the stuff that's gated by reduced_still_picture_header.'''
        if self.sh.reduced_still_picture_header:
            self.show_existing_frame = False
            self.frame_to_show_map_idx = None
            self.frame_type = KEY_FRAME
            self.show_frame = True
            self.temporal_point_info = None  # unused
            self.showable_frame = False
            self.error_resilient_mode = False  # unused
            return

        self.show_existing_frame = stream.read('bool')
        if self.show_existing_frame:
            self.frame_to_show_map_idx = stream.read('uint:3')
            assert self.frame_to_show_map_idx is not None
            ref_frame = self.rf.get_ref_frame(self.frame_to_show_map_idx)

            # frame_type is the type of the referenced frame
            self.frame_type = ref_frame.frame_type

            self.show_frame = True  # unused

            self.temporal_point_info = self.maybe_read_tpi(stream)
            self.display_frame_id = self.maybe_read_id(stream)

            self.showable_frame = False  # unused
            self.error_resilient_mode = False  # unused

            # This corresponds to the ref code just before we set
            # refresh_frame_flags. In particular, we don't load film grain
            # parameters.
            return

        # Not reduced_still_picture_header or show_existing_frame
        self.frame_to_show_map_idx = None  # unused
        self.frame_type = stream.read('uint:2')
        self.show_frame = stream.read('bool')

        if self.show_frame:
            self.temporal_point_info = self.maybe_read_tpi(stream)

        self.display_frame_id = None  # unused

        if self.show_frame:
            self.showable_frame = self.frame_type != KEY_FRAME
        else:
            self.showable_frame = stream.read('bool')

        force_erm = (self.frame_type == SWITCH_FRAME or
                     (self.frame_type == KEY_FRAME and self.show_frame))
        if force_erm:
            self.error_resilient_mode = True
        else:
            self.error_resilient_mode = stream.read('bool')

    def write_initial_header(self, stream: BitStream) -> None:
        '''The writing version of read_initial_header.'''
        if self.sh.reduced_still_picture_header:
            return

        stream += pack('bool', self.show_existing_frame)
        if self.show_existing_frame:
            stream += pack('uint:3', self.frame_to_show_map_idx)
            self.maybe_write_tpi(stream, self.temporal_point_info)
            self.maybe_write_id(stream, self.display_frame_id)
            return

        stream += pack('uint:2', self.frame_type)
        stream += pack('bool', self.show_frame)
        if self.show_frame:
            self.maybe_write_tpi(stream, self.temporal_point_info)

        if self.show_frame:
            assert self.showable_frame == (self.frame_type != KEY_FRAME)
        else:
            stream += pack('bool', self.showable_frame)

        force_erm = (self.frame_type == SWITCH_FRAME or
                     (self.frame_type == KEY_FRAME and self.show_frame))
        if force_erm:
            assert self.error_resilient_mode
        else:
            stream += pack('bool', self.error_resilient_mode)

    def read(self, stream: ConstBitStream) -> None:
        self.read_initial_header(stream)

        all_frames = (1 << NUM_REF_FRAMES) - 1

        # If this was a show_existing_frame header, we also need to set the
        # other stuff that we won't read because of the early return.
        if self.show_existing_frame:
            self.disable_cdf_update = False
            self.allow_screen_content_tools = \
                self.sh.seq_force_screen_content_tools.pick_one()
            self.force_integer_mv = True
            self.current_frame_id = None
            self.frame_size_override_flag = False
            self.order_hint = 0
            self.primary_ref_frame = PRIMARY_REF_NONE
            self.buffer_removal_times = None
            self.refresh_frame_flags = (all_frames
                                        if self.frame_type == KEY_FRAME
                                        else 0)
            self.ref_order_hint = None
            self.intra_frame_data = None
            self.inter_frame_data = None
            self.disable_frame_end_update_cdf = False
            self.tile_info = None
            self.quantization_params = None
            self.segmentation_params = None
            self.delta_q_params = None
            self.delta_lf_params = None
            self.loop_filter_params = None
            self.cdef_params = None
            self.lr_params = None
            self.tx_mode_select = None
            self.reference_select = None
            self.skip_mode_present = None
            self.allow_warped_motion = None
            self.reduced_tx_set = None
            self.global_motion_params = None
            self.film_grain_params = None
            return

        if self.frame_type == KEY_FRAME and self.show_frame:
            self.rf.on_shown_keyframe()

        self.disable_cdf_update = stream.read('bool')
        self.allow_screen_content_tools = \
            self.sh.seq_force_screen_content_tools.read_one(stream)
        if self.allow_screen_content_tools:
            self.force_integer_mv = \
                self.sh.seq_force_integer_mv.read_one(stream)
        else:
            self.force_integer_mv = False

        # This is in the spec, but we don't want to do it because we'd rather
        # write out the original value to the bitstream.
        #
        # if frame_is_intra:
        #    self.force_integer_mv = True

        self.current_frame_id = self.maybe_read_id(stream)

        if self.frame_type == SWITCH_FRAME:
            self.frame_size_override_flag = True
        elif self.sh.reduced_still_picture_header:
            self.frame_size_override_flag = False
        else:
            self.frame_size_override_flag = stream.read('bool')

        ohb = self.sh.order_hint_bits()
        if ohb:
            self.order_hint = stream.read('uint:{}'.format(ohb))
        else:
            self.order_hint = 0

        is_intra = self.frame_is_intra()
        if is_intra or self.error_resilient_mode:
            self.primary_ref_frame = PRIMARY_REF_NONE
        else:
            self.primary_ref_frame = stream.read('uint:3')

        if self.dmi is None:
            self.buffer_removal_times = None
        else:
            buffer_removal_time_present_flag = stream.read('bool')
            if buffer_removal_time_present_flag:
                removal_times = []
                n_bits, flags = self.dmi
                fmt = 'uint:{}'.format(n_bits)
                for flag in flags:
                    if flag:
                        removal_times.append(stream.read(fmt))
                self.buffer_removal_times = removal_times
            else:
                self.buffer_removal_times = None

        if ((self.frame_type == SWITCH_FRAME or
             (self.frame_type == KEY_FRAME and self.show_frame))):
            self.refresh_frame_flags = all_frames
        else:
            self.refresh_frame_flags = stream.read('uint:8')

        if ((((not is_intra) or self.refresh_frame_flags != all_frames) and
             (self.error_resilient_mode and (ohb > 0)))):
            self.ref_order_hint = []
            for idx in range(NUM_REF_FRAMES):
                ref_order_hint = stream.read('uint:{}'.format(ohb))
                self.ref_order_hint.append(ref_order_hint)
                # Check validity of RefOrderHint as computed.
                self.rf.check_ref_order_hint(idx, ref_order_hint)
        else:
            self.ref_order_hint = None

        if is_intra:
            self.inter_frame_data = None

            self.intra_frame_data = \
                IntraFrameData(self.frame_size_override_flag,
                               self.sh.frame_size,
                               self.sh.enable_superres,
                               self.allow_screen_content_tools)
            self.intra_frame_data.read(stream)
        else:
            self.intra_frame_data = None

            dfilm2 = (self.sh.frame_id_info.delta_frame_id_length_minus_2
                      if self.sh.frame_id_info is not None
                      else None)

            self.inter_frame_data = \
                InterFrameData(ohb,
                               self.order_hint,
                               self.rf,
                               dfilm2,
                               self.frame_size_override_flag,
                               self.error_resilient_mode,
                               self.sh.frame_size,
                               self.sh.enable_superres,
                               # See note above: force_integer_mv gets set to
                               # true for intra frames in the spec, but we
                               # don't do that so we can signal it the other
                               # way around. However, we know we aren't intra
                               # here.
                               self.force_integer_mv,
                               self.sh.enable_ref_frame_mvs)
            self.inter_frame_data.read(stream)

        if self.sh.reduced_still_picture_header or self.disable_cdf_update:
            self.disable_frame_end_update_cdf = True
        else:
            self.disable_frame_end_update_cdf = stream.read('bool')

        mi_size = self.mi_size()

        self.tile_info = TileInfo(mi_size, self.sh.use_128x128_superblock)
        self.tile_info.read(stream)

        self.quantization_params = \
            QuantizationParams(self.sh.color_config.mono_chrome,
                               self.sh.color_config.separate_uv_delta_q)
        self.quantization_params.read(stream)

        self.segmentation_params = SegmentationParams(self.primary_ref_frame)
        self.segmentation_params.read(stream)

        self.delta_q_params = DeltaQParams(self.quantization_params.base_q_idx)
        self.delta_q_params.read(stream)

        delta_q_present = self.delta_q_params.delta_q_res is not None
        allow_intrabc = (self.intra_frame_data is not None and
                         self.intra_frame_data.allow_intrabc)
        self.delta_lf_params = DeltaLFParams(delta_q_present, allow_intrabc)
        self.delta_lf_params.read(stream)

        coded_lossless = self.coded_lossless()
        allow_loop_filter = not (coded_lossless or allow_intrabc)
        if not allow_loop_filter:
            self.loop_filter_params = None
        else:
            self.loop_filter_params = \
                LoopFilterParams(self.sh.color_config.mono_chrome)
            self.loop_filter_params.read(stream)

        allow_cdef = allow_loop_filter and self.sh.enable_cdef
        if not allow_cdef:
            self.cdef_params = None
        else:
            self.cdef_params = CdefParams(self.sh.color_config.mono_chrome)
            self.cdef_params.read(stream)

        all_lossless = coded_lossless and not self.uses_superres()
        allow_lr = not (all_lossless or
                        allow_intrabc or
                        not self.sh.enable_restoration)
        if not allow_lr:
            self.lr_params = None
        else:
            self.lr_params = LrParams(self.sh.color_config.mono_chrome,
                                      self.sh.use_128x128_superblock,
                                      self.sh.color_config.is_422_ss())
            self.lr_params.read(stream)

        if coded_lossless:
            self.tx_mode_select = None
        else:
            self.tx_mode_select = stream.read('bool')

        if is_intra:
            self.reference_select = None
        else:
            self.reference_select = stream.read('bool')

        skip_mode_allowed = False
        if not is_intra:
            assert self.inter_frame_data is not None
            ref_frame_idx = self.inter_frame_data.ref_frame_idx
            skip_mode_allowed = self.skip_mode_allowed(ref_frame_idx)

        if skip_mode_allowed:
            self.skip_mode_present = stream.read('bool')
        else:
            self.skip_mode_present = None

        if ((is_intra or
             self.error_resilient_mode or
             not self.sh.enable_warped_motion)):
            self.allow_warped_motion = None
        else:
            self.allow_warped_motion = stream.read('bool')

        self.reduced_tx_set = stream.read('bool')

        if is_intra:
            self.global_motion_params = None
        else:
            assert self.inter_frame_data is not None
            allow_hp_mv = self.inter_frame_data.allow_high_precision_mv
            self.global_motion_params = GlobalMotionParams(allow_hp_mv)
            self.global_motion_params.read(stream)

        signal_film_grain = (self.sh.film_grain_params_present and
                             (self.show_frame or self.showable_frame))
        if signal_film_grain:
            self.film_grain_params = \
                FilmGrainParams(self.frame_type,
                                self.sh.color_config.mono_chrome,
                                self.sh.color_config.is_422_ss())
            self.film_grain_params.read(stream)
        else:
            self.film_grain_params = None

    def write(self, stream: BitStream) -> None:
        self.write_initial_header(stream)
        if self.show_existing_frame:
            return

        stream += pack('bool', self.disable_cdf_update)
        (self.sh.seq_force_screen_content_tools
         .write_one(stream, self.allow_screen_content_tools))
        if self.allow_screen_content_tools:
            self.sh.seq_force_integer_mv.write_one(stream,
                                                   self.force_integer_mv)

        self.maybe_write_id(stream, self.current_frame_id)

        if self.frame_type == SWITCH_FRAME:
            assert self.frame_size_override_flag
        elif self.sh.reduced_still_picture_header:
            assert not self.frame_size_override_flag
        else:
            stream += pack('bool', self.frame_size_override_flag)

        ohb = self.sh.order_hint_bits()
        if ohb:
            stream += pack('uint:{}'.format(ohb), self.order_hint)
        else:
            assert self.order_hint == 0

        is_intra = self.frame_is_intra()
        if is_intra or self.error_resilient_mode:
            assert self.primary_ref_frame == PRIMARY_REF_NONE
        else:
            stream += pack('uint:3', self.primary_ref_frame)

        if self.dmi is None:
            assert self.buffer_removal_times is None
        else:
            buffer_removal_time_present_flag = \
                self.buffer_removal_times is not None

            stream += pack('bool', buffer_removal_time_present_flag)
            if buffer_removal_time_present_flag:
                assert self.buffer_removal_times is not None
                n_bits, flags = self.dmi
                fmt = 'uint:{}'.format(n_bits)
                removal_times = self.buffer_removal_times
                assert len(removal_times) <= len(flags)
                for flag in flags:
                    if flag:
                        stream += pack(fmt, removal_times[0])
                        removal_times = removal_times[1:]
                assert removal_times == []

        all_frames = (1 << NUM_REF_FRAMES) - 1

        if ((self.frame_type == SWITCH_FRAME or
             (self.frame_type == KEY_FRAME and self.show_frame))):
            assert self.refresh_frame_flags == all_frames
        else:
            stream += pack('uint:8', self.refresh_frame_flags)

        if ((((not is_intra) or self.refresh_frame_flags != all_frames) and
             (self.error_resilient_mode and (ohb > 0)))):

            assert self.ref_order_hint is not None
            assert len(self.ref_order_hint) == NUM_REF_FRAMES
            for roh in self.ref_order_hint:
                stream += pack('uint:{}'.format(ohb), roh)

        if is_intra:
            assert self.intra_frame_data is not None
            assert self.inter_frame_data is None

            self.intra_frame_data.write(stream)
        else:
            assert self.intra_frame_data is None
            assert self.inter_frame_data is not None

            self.inter_frame_data.write(stream)

        if self.sh.reduced_still_picture_header or self.disable_cdf_update:
            assert self.disable_frame_end_update_cdf is True
        else:
            stream += pack('bool', self.disable_frame_end_update_cdf)

        assert self.tile_info is not None
        self.tile_info.write(stream)

        assert self.quantization_params is not None
        self.quantization_params.write(stream)

        assert self.segmentation_params is not None
        self.segmentation_params.write(stream)

        assert self.delta_q_params is not None
        self.delta_q_params.write(stream)

        assert self.delta_lf_params is not None
        self.delta_lf_params.write(stream)

        coded_lossless = self.coded_lossless()
        allow_intrabc = (self.intra_frame_data is not None and
                         self.intra_frame_data.allow_intrabc)
        allow_loop_filter = not (coded_lossless or allow_intrabc)
        if not allow_loop_filter:
            assert self.loop_filter_params is None
        else:
            assert self.loop_filter_params is not None
            self.loop_filter_params.write(stream)

        allow_cdef = allow_loop_filter and self.sh.enable_cdef
        if not allow_cdef:
            assert self.cdef_params is None
        else:
            assert self.cdef_params is not None
            self.cdef_params.write(stream)

        all_lossless = coded_lossless and not self.uses_superres()
        allow_lr = not (all_lossless or
                        allow_intrabc or
                        not self.sh.enable_restoration)
        if not allow_lr:
            assert self.lr_params is None
        else:
            assert self.lr_params is not None
            self.lr_params.write(stream)

        if coded_lossless:
            assert self.tx_mode_select is None
        else:
            stream += pack('bool', self.tx_mode_select)

        if is_intra:
            assert self.reference_select is None
        else:
            stream += pack('bool', self.reference_select)

        skip_mode_allowed = False
        if not is_intra:
            assert self.inter_frame_data is not None
            ref_frame_idx = self.inter_frame_data.ref_frame_idx
            skip_mode_allowed = self.skip_mode_allowed(ref_frame_idx)

        if skip_mode_allowed:
            stream += pack('bool', self.skip_mode_present)
        else:
            assert self.skip_mode_present is None

        if ((is_intra or
             self.error_resilient_mode or
             not self.sh.enable_warped_motion)):
            assert self.allow_warped_motion is None
        else:
            stream += pack('bool', self.allow_warped_motion)

        assert self.reduced_tx_set is not None
        stream += pack('bool', self.reduced_tx_set)

        if is_intra:
            assert self.global_motion_params is None
        else:
            assert self.global_motion_params is not None
            self.global_motion_params.write(stream)

        signal_film_grain = (self.sh.film_grain_params_present and
                             (self.show_frame or self.showable_frame))
        if signal_film_grain:
            assert self.film_grain_params is not None
            self.film_grain_params.write(stream)
        else:
            assert self.film_grain_params is None


class Settings:
    '''A class to track settings for uncompressed headers' bitstream formats'''
    def __init__(self) -> None:
        self.sequence_header = None  # type: Optional[SequenceHeader]
        self.temporal_id = 0
        self.spatial_id = 0
        self.sh_since_td = False
        self.ref_frames = RefFrames()

    def on_sequence_header(self, hdr: SequenceHeader) -> None:
        '''Take note of the contents of a sequence header'''
        self.sequence_header = copy.deepcopy(hdr)
        self.sh_since_td = True

    def on_td(self) -> None:
        '''Take note of the presence of a TD obu'''
        self.sh_since_td = False

    def on_extension_obu(self, ext: ObuExtension) -> None:
        '''Take note of an obu_extension_header'''
        self.temporal_id = ext.temporal_id
        self.spatial_id = ext.spatial_id

    @staticmethod
    def has_bit(bits: int, idx: int) -> bool:
        '''Return true if bit idx is set in bits'''
        assert bits >= 0
        return ((bits >> idx) & 1) != 0

    def get_sh(self) -> SequenceHeader:
        '''Return self.sequence_header, checking it is not None'''
        assert self.sequence_header is not None
        return self.sequence_header

    def mk_dmi(self) -> Optional[Tuple[int, List[bool]]]:
        '''Make the decoder_model_info argument for uhdr'''
        sh = self.get_sh()
        if sh.decoder_model_info is None:
            return None

        n_bits = (1 +
                  sh.decoder_model_info.buffer_removal_time_length_minus_1)
        flags = []
        for op_idx, op in enumerate(sh.operating_points):
            # decoder_model_present_for_this_op[op_idx] is true iff
            # op.operating_parameters_info is not None. If it's false, the
            # flag is definitely false.
            if op.operating_parameters_info is None:
                flags.append(False)
                continue

            # Ahah. We have a decoder model.
            idc = op.operating_point_idc
            in_temporal_layer = Settings.has_bit(idc, self.temporal_id)
            in_spatial_layer = Settings.has_bit(idc, self.spatial_id + 8)
            valid_op = (idc == 0) or (in_temporal_layer and
                                      in_spatial_layer)
            flags.append(valid_op)

        return (n_bits, flags)

    def update_uncompressed_header(self, uh: UncompressedHeader) -> None:
        uh.update(self.get_sh(), self.mk_dmi(), self.ref_frames)

    def make_uncompressed_header(self) -> UncompressedHeader:
        '''Build an uncompressed header object with current settings'''
        if self.sequence_header is None:
            raise RuntimeError('Cannot make uncompressed header before first '
                               'sequence header.')

        return UncompressedHeader(self.get_sh(),
                                  self.mk_dmi(),
                                  copy.deepcopy(self.ref_frames))

    def after_uncompressed_header(self, uh: UncompressedHeader) -> None:
        '''Update ref_frames after an uncompressed header'''
        self.ref_frames.store_ref_frame(uh.refresh_frame_flags,
                                        uh.get_ref_frame())


class Visitor:
    '''An interface for something that visits parsed OBUs'''

    def on_sequence_header(self, hdr: SequenceHeader) -> bool:
        '''Called on each sequence header OBU

        If it returns True, the body of the OBU might have changed and should
        be re-serialized.

        '''
        raise NotImplementedError

    def on_uncompressed_header(self,
                               hdr: UncompressedHeader,
                               write_settings: Settings,
                               is_new_cvs: bool) -> bool:
        '''Called on each uncompressed header

        write_settings is the Settings object that represents the state of the
        bitstream for writing.

        If it returns True, the body of the header might have changed and
        should be re-serialized. Since we only read the first part of the
        header, you must ensure that the resulting length differs by a whole
        number of bytes.

        '''
        raise NotImplementedError


class ObuDriver:
    '''A wrapper for actually interpreting OBUs'''
    def __init__(self, visitor: Visitor):
        self.visitor = visitor
        self.read_settings = Settings()
        self.write_settings = Settings()

    @staticmethod
    def add_trailing_bits(old_len: int, stream: BitStream):
        '''Add trailing_bits to a bitstream

        By default, we try take avoid the OBU changing in size at all. If we
        don't manage that, we'll try to add a multiple of 16 bytes (which makes
        comparing the files in hexdump somewhat less painful).

        old_len is the length of the old bitstream, after adding the trailing
        bits. It should be a whole number of bytes.

        '''
        assert old_len & 7 == 0

        # Write out the trailing one bit
        stream += pack('bool', True)

        # Bump up the length we're aiming for if necessary to make it at least
        # as long as what we've written.
        target_len = old_len
        while target_len < stream.length:
            target_len += 16 * 8

        assert stream.length <= target_len

        # Make us up to the target length by writing zero or more 0 bits.
        for _ in range(target_len - stream.length):
            stream += pack('bool', False)

        # This process should have got us up to a round number of bytes
        assert stream.length & 7 == 0

    def on_obu(self, obu: Obu) -> None:
        '''Run the visitor over a parsed OBU'''
        out = None

        if obu.header.obu_extension is not None:
            self.read_settings.on_extension_obu(obu.header.obu_extension)
            self.write_settings.on_extension_obu(obu.header.obu_extension)

        if obu.header.obu_type == OBU_TEMPORAL_DELIMITER:
            self.read_settings.on_td()
            self.write_settings.on_td()

        elif obu.header.obu_type == OBU_SEQUENCE_HEADER:
            shdr = SequenceHeader()
            shdr.read(obu.body)

            self.read_settings.on_sequence_header(shdr)

            if self.visitor.on_sequence_header(shdr):
                out = BitStream()
                shdr.write(out)
                ObuDriver.add_trailing_bits(obu.body.length, out)

            self.write_settings.on_sequence_header(shdr)

        elif obu.header.obu_type in [OBU_FRAME_HEADER, OBU_FRAME]:
            uhdr = self.read_settings.make_uncompressed_header()
            uhdr.read(obu.body)

            is_new_cvs = (self.read_settings.temporal_id == 0 and
                          self.read_settings.sh_since_td and
                          uhdr.can_start_cvs())

            self.read_settings.after_uncompressed_header(uhdr)

            if obu.header.obu_type == OBU_FRAME:
                RWObject.byte_align_reader(obu.body)
                assert obu.body.pos & 7 == 0

            if self.visitor.on_uncompressed_header(uhdr,
                                                   self.write_settings,
                                                   is_new_cvs):
                # uhdr has changed. Update it so that it serialises properly
                self.write_settings.update_uncompressed_header(uhdr)

                out = BitStream()
                uhdr.write(out)
                self.write_settings.after_uncompressed_header(uhdr)

                if obu.header.obu_type == OBU_FRAME_HEADER:
                    ObuDriver.add_trailing_bits(obu.body.length, out)
                else:
                    assert obu.header.obu_type == OBU_FRAME
                    RWObject.byte_align_writer(out)
                    assert out.length & 7 == 0

                    # Add the rest of obu.body to the back of out. This should
                    # also be a whole number of bytes.
                    to_read = obu.body.length - obu.body.pos
                    assert to_read >= 0
                    out += obu.body.read('bits:{}'.format(to_read))
                    assert out.length & 7 == 0
        else:
            # We're not interested in this OBU type
            pass

        if out is not None:
            # We've got a new bitstring. Set the body of the OBU to it.
            obu.set_body(out)


class OutFile:
    def __init__(self, path: str, split: bool, verbose: bool) -> None:
        self.path = path
        self.split = split
        self.verbose = verbose

        self.handle = None  # type: Optional[BinaryIO]
        self.index = 0

    def _next_path(self) -> str:
        if self.split:
            ret = '{}.{:03}'.format(self.path, self.index)
            self.index += 1
            return ret
        else:
            return self.path

    def __enter__(self) -> 'OutFile':
        assert self.handle is None
        path = self._next_path()
        self.handle = open(path, 'wb')
        if self.verbose:
            print('Writing to {}'.format(path))
        return self

    def __exit__(self, exc_type: Any, value: Any, traceback: Any) -> None:
        assert self.handle is not None
        self.handle.close()

    def bump(self) -> None:
        assert self.handle is not None
        if self.split:
            self.handle.close()
            path = self._next_path()
            self.handle = open(path, 'wb')
            if self.verbose:
                print('Writing to {}'.format(path))

    def write(self, data: bytes) -> None:
        assert self.handle is not None
        self.handle.write(data)


class FileDriver:
    def transform_file(self, outfile: OutFile, verbose: bool) -> None:
        '''Run over the entire file'''
        raise NotImplementedError


class RawDriver(FileDriver):
    def __init__(self, obu_driver: ObuDriver, stream: ConstBitStream):
        self.stream = stream
        self.obu_driver = obu_driver

    def transform_file(self, outfile: OutFile, verbose: bool) -> None:
        seen_sh = False
        while True:
            obu = Obu(None)
            obu.read(self.stream)
            end_of_stream = (self.stream.pos >= self.stream.length)

            self.obu_driver.on_obu(obu)

            if obu.header.obu_type == OBU_SEQUENCE_HEADER:
                if seen_sh:
                    outfile.bump()
                seen_sh = True

            out = BitStream()
            obu.write(out)
            outfile.write(out.bytes)

            if end_of_stream:
                break


class AnnexBDriver(FileDriver):
    def __init__(self, obu_driver: ObuDriver, stream: ConstBitStream):
        self.stream = stream
        self.obu_driver = obu_driver

    def transform_file(self, outfile: OutFile, verbose: bool) -> None:
        seen_sh = False
        while True:
            tu_size = Leb128()
            tu_size.read(self.stream)

            t_unit, start_sh = self._temporal_unit(tu_size.value)
            end_of_stream = (self.stream.pos >= self.stream.length)

            # We want to write out the temporal unit size for the new TU with
            # the same number of bytes as before if possible (indeed, the
            # contents might not have changed at all). If the TU length has
            # changed, however, we'll just write out the LEB data as simply as
            # possible.
            assert t_unit.length & 7 == 0
            tu_size.set_value(t_unit.length // 8)

            out = BitStream()
            tu_size.write(out)
            out += t_unit

            # Check for new sequence header at start of a TU
            if start_sh:
                if seen_sh:
                    outfile.bump()
                seen_sh = True
            if verbose:
                print('TU, length {}'.format(len(out.bytes)))

            # Write the TU data to outfile
            assert out.length & 7 == 0
            outfile.write(out.bytes)

            if end_of_stream:
                break

    def _temporal_unit(self, tu_size: int) -> Tuple[BitStream, bool]:
        '''Read and transform an annexb TU'''
        out = BitStream()
        first = True
        start_sh = False

        while tu_size > 0:
            fu_size = Leb128()
            fu_size.read(self.stream)
            tu_size -= fu_size.length

            f_unit, sh_now = self._frame_unit(fu_size.value)
            tu_size -= fu_size.value

            if first:
                first = False
                start_sh = sh_now

            assert f_unit.length & 7 == 0
            fu_size.set_value(f_unit.length // 8)

            fu_size.write(out)
            out += f_unit

        return [out, start_sh]

    def _frame_unit(self, fu_size: int) -> Tuple[BitStream, bool]:
        '''Read and transform an annexb FU

        Returns a bitstream, together with a flag that says whether
        this frame unit has a sequence header before the first frame
        or frame header OBU.

        '''
        out = BitStream()
        start_sh = False
        seen_frame = False

        while fu_size > 0:
            obu_size = Leb128()
            obu_size.read(self.stream)
            fu_size -= obu_size.length

            # Ahah! We can finally read the OBU.
            obu = Obu(obu_size.value)
            obu.read(self.stream)
            fu_size -= obu.get_length()

            # Call the transform method to mangle the parsed OBU
            self.obu_driver.on_obu(obu)

            # The OBU size might have changed: update obu_size if necessary
            obu_size.set_value(obu.get_length())

            # Write the OBU to fu. First, we need to write the leb128-encoded
            # length.
            obu_size.write(out)

            # Now, write the OBU itself
            obu.write(out)

            if obu.header.obu_type in [OBU_FRAME_HEADER, OBU_FRAME]:
                seen_frame = True
            if obu.header.obu_type == OBU_SEQUENCE_HEADER and not seen_frame:
                start_sh = True

        return (out, start_sh)


def transform_bitstream(annexb: bool,
                        split: bool,
                        verbose: bool,
                        visitor: Visitor,
                        in_path: str,
                        out_path: str) -> None:
    '''Visit each OBU in the bitstream'''
    obu_driver = ObuDriver(visitor)

    if not os.path.exists(in_path):
        raise RuntimeError('No such input file: `{}\'.'.format(in_path))

    stream = ConstBitStream(filename=in_path)
    factory = AnnexBDriver if annexb else RawDriver
    with OutFile(out_path, split, verbose) as out_file:
        factory(obu_driver, stream).transform_file(out_file, verbose)


class SanityCheckFixer(Visitor):
    '''A dummy visitor that reads and writes everything with no changes'''
    def on_sequence_header(self, hdr: SequenceHeader) -> bool:
        return True

    def on_uncompressed_header(self,
                               hdr: UncompressedHeader,
                               write_settings: Settings,
                               is_new_cvs: bool) -> bool:
        return True

    @staticmethod
    def post_check(in_path: str, out_path: str) -> None:
        '''Check that the files at in_path and out_path are identical'''
        if not filecmp.cmp(in_path, out_path, shallow=False):
            raise RuntimeError('Input and output streams are not equal.')


class Fixer(Visitor):
    '''The visitor object that does the fixing'''
    def __init__(self,
                 fps: Optional[int],
                 levels: Optional[Levels]):
        self.fps = fps
        self.levels = levels

        # This is set if we change a sequence header by adding decoder model
        # information. In this case, we'll also have to change the uncompressed
        # headers' bitstream format.
        self._new_decoder_model = False

        # The index of the current dfg. This is incremented at the end of
        # uncompressed_header (unless we are show_existing_frame).
        self._dfg_idx = 0

    def on_sequence_header(self, hdr: SequenceHeader) -> bool:
        self._new_decoder_model = False

        if self.levels is not None:
            for op in hdr.operating_points:
                level, seq_tier = self.levels.pop_level()
                op.level = level
                if seq_tier is not None:
                    if not op.level.signals_seq_tier():
                        major, minor = op.level.major_minor()
                        raise RuntimeError('Cannot set seq_tier for '
                                           'level {}.{}.'
                                           .format(major, minor))
                    op.seq_tier = seq_tier

        if self.fps is not None:
            hdr.set_fps(self.fps)

        return True

    def on_uncompressed_header(self,
                               hdr: UncompressedHeader,
                               write_settings: Settings,
                               is_new_cvs: bool) -> bool:
        # If we're setting low delay mode, we might need to do some work here.
        # We changed the sequence header by adding decoder model info, but that
        # actually changes the uncompressed header bitstream format, so we need
        # to re-serialize.
        if not self._new_decoder_model:
            return False

        if is_new_cvs:
            self._dfg_idx = 0

        # If the _new_decoder_model flag is set, we think that we were reading
        # from the bitstream without a decoder model, but will write with one.
        assert hdr.dmi is None

        # Tell hdr to add decoder_model_info. Just doing this will tell it to
        # signal that buffer_removal_time_present_flag = False. This adds one
        # bit to the uncompressed header length.
        #
        # TODO: The magic 2000 here is a multiple of DecCT and should probably
        #       be related to that and the current notional frame rate?
        hdr.add_decoder_model_info(write_settings, 2000 * self._dfg_idx)

        # Increment DFG index if this isn't a show_existing_frame.
        if not hdr.show_existing_frame:
            self._dfg_idx += 1

        # Returning true here will cause hdr to be re-serialized (with new
        # underlying settings).
        return True


def main() -> int:
    '''Main entry-point for module'''
    parser = argparse.ArgumentParser()
    parser.add_argument('--annexb', action='store_true')
    parser.add_argument('--fps', type=int, default=None)
    parser.add_argument('--levels', type=Levels.from_string, default=None)
    parser.add_argument('--verbose', '-v', action='store_true')
    parser.add_argument('--split', action='store_true',
                        help=('Split the file at sequence headers. The '
                              'output will be to files named X.000, X.001, '
                              'etc, where X is the outfile argument.'))

    parser.add_argument('--sanity-check', action='store_true')

    parser.add_argument('infile',
                        help=('Input file'))
    parser.add_argument('outfile',
                        help=('Output file'))

    args = parser.parse_args()

    post_check = None  # type: Optional[Callable[[str,str],None]]

    if args.sanity_check:
        if args.fps is not None:
            raise RuntimeError('Cannot use --fps argument '
                               'in sanity check mode.')
        if args.levels is not None:
            raise RuntimeError('Cannot use --levels argument '
                               'in sanity check mode.')

        fixer = SanityCheckFixer()  # type: Visitor
        post_check = SanityCheckFixer.post_check
    else:
        if args.fps is not None:
            if args.fps <= 0:
                raise RuntimeError('--fps expects a positive integer.')
            if args.fps > 120:
                raise RuntimeError('--fps argument too large. Maximum 120.')

        fixer = Fixer(args.fps, args.levels)

    transform_bitstream(args.annexb, args.split, args.verbose, fixer,
                        args.infile, args.outfile)

    if post_check is not None:
        post_check(args.infile, args.outfile)

    return 0


if __name__ == '__main__':
    try:
        sys.exit(main())
    except RuntimeError as err:
        sys.stderr.write('Error: {}\n'.format(err))
        sys.exit(1)
