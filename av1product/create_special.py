################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import sys
import math
from random import randint
def syntax():
  print "Syntax: {} <encoder>".format(sys.argv[0])
  print "encoder options:       [vp9    ||    hevc    ||    av1    ||    av1-client_a] [Profiles and folders to skip: 0, 1, core, not-annexb etc.]"
  print
  sys.exit(0)
profile_limits = []
try:
    encoder=sys.argv[1]
    for i in range(2,len(sys.argv)):
      profile_limits.append(sys.argv[i])
except:
    syntax()
if encoder != 'vp9' and encoder != 'hevc' and encoder != 'av1' and encoder != 'av1-client_a':
    syntax()
is_client_a = encoder == 'av1-client_a'
if is_client_a:
  encoder = 'av1'
if encoder == 'vp9':
#   WIDTH  HEIGHT   bitrate_control      frame_cnt
    resolutions = """
    128  96   0 0
    176  144  0 0
    320  240  0 0
    352  240  0 0
    352  288  0 0
    352  480  0 0
    352  576  0 0
    640  360  0 0
    640  480  0 0
    704  480  0 0
    720  480  0 0
    704  576  0 0
    720  576  0 0
    864  480  0 0
    800  600  0 0
    960  540  0 0
    1024 768  0 0
    1280 720  0 0
    1280 720  0 1
    1280 720  0 2
    1280 960  0 0
    1280 1024 0 0
    1408 960  0 0
    1408 1152 0 0
    1600 1200 0 0
    1920 1080 0 0
    1920 1080 0 1
    1920 1080 0 2
    1920 1080 265000 0
    1920 1080 662500 0
    2048 1024 0 0
    2048 1080 0 0
    2048 1536 0 0
    2560 1920 0 0
    3616 1536 0 0
    3680 1536 0 0
    3840 2160 0 0
    3840 2160 0 1
    3840 2160 0 2
    3840 2176 0 0
    4096 2048 0 0
    4096 2160 0 0
    4096 2160 479000 0
    4096 2160 994000 0
    4096 2176 0 0
    4096 2304 0 0
    4096 2304 0 1
    4096 2304 0 2
    4096 3072 0 0
    7680 4320 0 0
    8192 4096 0 0
    8192 4320 0 0"""
    #all_res = resolutions.split('\n')
    all_res = [a for a in resolutions.split('\n') if a != '' ]
elif encoder == 'av1':
    #   WIDTH  HEIGHT   bitrate_control      frame_cnt
    resolutions = """
// This list is arranged in three sections:
// * Seeds 1-18 are the resolutions specified in the AV1 streams contracts
//   (2 streams per resolution, at 10 frames each)
// * Seeds 19-25 are the extra streams specified in the ClientF contract, which
//   have specific resolutions *and* specific numbers of bits per frame
// * Seeds 26+ are various extra resolutions and can be changed freely
//
// Resolutions specified in the AV1 streams contracts - we do two each of these
      1280 720  0 0
      1280 720  0 0
      1280 1024 0 0
      1280 1024 0 0
      1600 1200 0 0
      1600 1200 0 0
      1920 1080 0 0
      1920 1080 0 0
      2048 1080 0 0
      2048 1080 0 0
      4096 2048 0 0
      4096 2048 0 0
      4096 2160 0 0
      4096 2160 0 0
      4096 3072 0 0
      4096 3072 0 0
      8192 4320 0 0
      8192 4320 0 0

// Extra streams specified in the ClientF contract
// The requested bitrates are:
//   4K (4096x2160) @ 1Mb per frame and 0.5Mb per frame.
//   1080p@ 0.667Mb per frame and 0.2667Mb per frame.
//   3840x2160 @ 1.3Mb and 2.6Mb per frame
//   7680x4320 @ 5.3Mb per frame
      4096 2160 1000000 0 // ~0.11 bits per luma pixel
      4096 2160 500000 0 // ~0.06 bits per luma pixel
      1920 1080 667000 0 // ~0.32 bits per luma pixel
      1920 1080 266700 0 // ~0.13 bits per luma pixel
      3840 2160 1300000 0 // ~0.16 bits per luma pixel
      3840 2160 2600000 0 // ~0.31 bits per luma pixel
      7680 4320 5300000 0 // ~0.16 bits per luma pixel

// Extra resolutions not specified in the contracts:
      320  240  0 0
      352  240  0 0
      352  288  0 0
      352  576  0 0
      640  360  0 0
      640  480  0 0
      704  480  0 0
      720  480  0 0
      704  576  0 0
      720  576  0 0
      864  480  0 0
      800  600  0 0
      960  540  0 0
      1024 768  0 0
      1280 960  0 0
      1408 960  0 0
      1408 1152 0 0
      2048 1024 0 0
      2048 1536 0 0
      2560 1440 0 0
      2560 1920 0 0
      3616 1536 0 0
      3680 1536 0 0
      3840 2160 0 0
      3840 2176 0 0
      4096 2176 0 0
      4096 2304 0 0
      7680 4320 0 0
      8192 4096 0 0
      8192 4352 0 0
"""
    #   WIDTH  HEIGHT   bitrate_control      frame_cnt
    client_a_resolutions = """
// Modified special set for client_a only, 5 copies of each of 5 resolutions
    176  144  0 0
    176  144  0 0
    176  144  0 0
    176  144  0 0
    176  144  0 0
    352  288  0 0
    352  288  0 0
    352  288  0 0
    352  288  0 0
    352  288  0 0
    1280 720  0 0
    1280 720  0 0
    1280 720  0 0
    1280 720  0 0
    1280 720  0 0
    1920 1080 0 0
    1920 1080 0 0
    1920 1080 0 0
    1920 1080 0 0
    1920 1080 0 0
    3840 2160 0 0
    3840 2160 0 0
    3840 2160 0 0
    3840 2160 0 0
    3840 2160 0 0
"""

    #   tile_cols  tile_rows   tile_width use_128_sb   tile_list_frame_cnt
    tile_params = """
// nb the tiles w & h need to be powers of two due to the way they are encoded
       1   4   1   0   0
       1   4   8   1   0
       1   8  64   0   0
       1   8   4   1   0
       1  16  16   0   0
       1  16  32   1   0
       1  32  32   0   0
       1  32  64   0   0
       1  64   1   1   0
       1  64   2   1   0
       4   1   3   0   0
       4   1   4   1   0
       8   1   5   0   0
       8   1   6   1   0
      16   1   7   0   0
      16   1   8   1   0
       1   1   9   0   0
       1   1  10   1   0
       2   2  11   0   0
       2   2  12   1   0
       4   4  13   0   0
       4   4  14   1   0
       8   8  15   0   0
       8   8  16   1   0
       16  8   8   0   0
       16  8   4   1   0
        8 16  16   1   0

       1   4   1   0   1
       1   4   8   1   1
       1   8  64   0   1
       1   8   4   1   1
       1  16  16   0   1
       1  16  32   1   1
       1  32  32   0   1
       1  32  64   0   1
       1  64   1   1   1
       1  64   2   1   1
       4   1   3   0   1
       4   1   4   1   1
       8   1   5   0   1
       8   1   6   1   0
      16   1   7   0   1
      16   1   8   1   1
       1   1   9   0   1
       1   1  10   1   1
       2   2  11   0   1
       2   2  12   1   1
       4   4  13   0   1
       4   4  14   1   1
       8   8  15   0   1
       8   8  16   1   1
       16  8   8   0   1
       16  8   4   1   1
        8 16  16   1   1
"""
    #all_res = resolutions.split('\n')
    all_res = []
    all_tiles = []
    all_client_a = []
                #Index, before_flag, comment
    res_comments = []
    tile_comments = []
    client_a_comments = []
    index = 0
    for a in resolutions.split('\n'):
      if a != '':
        if a[:2] == '//':
          res_comments.append((index,True,a))
        else:
          if '//' in a:
            ind = a.index('//')
            res_comments.append((index,False,a[ind:]))
            all_res.append(a[:ind])
          else:
            all_res.append(a)
          index += 1
    for a in client_a_resolutions.split('\n'):
      if a != '':
        if a[:2] == '//':
          client_a_comments.append((index,True,a))
        else:
          if '//' in a:
            ind = a.index('//')
            client_a_comments.append((index,False,a[ind:]))
            all_client_a.append(a[:ind])
          else:
            all_client_a.append(a)
          index += 1
    index = 0
    for a in tile_params.split('\n'):
      if a != '':
        if a[:2] == '//':
          tile_comments.append((index,True,a))
        else:
          if '//' in a:
            ind = a.index('//')
            tile_comments.append((index,False,a[ind:]))
            all_tiles.append(a[:ind])
          else:
            all_tiles.append(a)
          index += 1
else:
#      WIDTH  HEIGHT   NAME      LONG
    resolutions = """
        128    96     sqcif       0
        176    144    qcif        0
        320    240    qvga        0
        352    240    525_sif     0
        352    288    cif         0
        352    480    525_hhr     0
        352    576    625_hhr     0
        640    360    q720p       0
        640    480    vga         0
        704    480    525_4sif    0
        720    480    525_sd      0
        704    576    4cif        0
        720    576    625_sd      0
        864    480    480p        0
        800    600    svga        0
        960    540    qhd         0
        1024   768    xga         0
        1280   720    720p        0
        1280   960    4vga        0
        1280   1024   sxga        0
        1408   960    525_16sif   0
        1408   1152   16cif       0
        1600   1200   4svga       0
        1920   1080   hd          0
        2048   1024   2kx1k       0
        2048   1080   2kx1080     0
        2048   1536   4xga        0
        2560   1920   16vga       0
        3616   1536   3616x1536   0
        3680   1536   3680x1536   0
        3840   2160   4k_uhd      0
        4096   2048   4kx2k       0
        4096   2160   4096x2160   0
        4096   2304   4k          0
        7680   4320   8k_uhd      0
        8192   4096   8192x4096   0
        8192   4320   8192x4320   0
"""
   #LEVEL MAXLUMAPS
    MaxLumaPs = (
     (10, 36864),
     (20, 122880),
     (21, 245760),
     (30, 552960),
     (31, 983040),
     (40, 2228224),
     (41, 2228224),
     (50, 8912896),
     (51, 8912896),
     (52, 8912896),
     (60, 35651584),
     (61, 35651584),
     (62, 35651584),
     )
   #LEVEL TIER1 TIER0
    MaxCPB = (
     (10, (350,   350   )),
     (20, (1500,  1500  )),
     (21, (3000,  3000  )),
     (30, (6000,  6000  )),
     (31, (10000, 10000 )),
     (40, (12000, 30000 )),
     (41, (20000, 50000 )),
     (50, (25000, 100000)),
     (51, (40000, 160000)),
     (52, (60000, 240000)),
     (60, (60000, 240000)),
     (61, (120000, 480000)),
     (62, (240000, 800000)),
     )
   #LEVEL MAXROWS MAXCOLS
    MaxTiles = (
     (30, (2,  2 )),
     (31, (3,  3 )),
     (40, (5,  5 )),
     (50, (11, 10)),
     (60, (22, 20)),
     )

    all_res = [a.split() for a in resolutions.split('\n') if a != '' ]
    tile = 0
    tier = 0
    make_cpb = 0
    long_stream = 0

    for a_res in all_res:
        w,h,name,f = a_res
        w2 = int(w)*int(w)
        h2 = int(h)*int(h)
        picSizeInSamplesY = int(w) * int(h)
        for level, mlp in MaxLumaPs:
            if w2 <= mlp*8 and h2 <= mlp*8 and picSizeInSamplesY< mlp:
                a_res += [level,tier,tile,make_cpb]
                break

    #w*w <= (8*mlp)
    for level, mlp in MaxLumaPs:
        if int(math.sqrt(mlp*8.0)) < 8448:
            calc_dim1 = int(math.sqrt(mlp * 8.0))/8*8
            calc_dim1_16 = int(math.sqrt(mlp * 8.0))/16*16
            calc_dim2 = (mlp / calc_dim1)/8*8
            calc_dim2_16 = (mlp / calc_dim1_16)/16*16

            all_res.append([(calc_dim1,calc_dim1_16), (calc_dim2,calc_dim2_16), 'width_' + str(level), long_stream, level, tier, tile, make_cpb])
            all_res.append([(calc_dim2,calc_dim2_16), (calc_dim1,calc_dim1_16), 'height_' + str(level), long_stream, level, tier, tile, make_cpb])
    tile = 1
    all_res += [[w*256, h*64, 'tiles_' + str(level), long_stream, level, tier, tile, make_cpb] for level, (w,h) in MaxTiles]
    tile = 0
    make_cpb = 1
    make_cpb_index = len(all_res)
    for level, mlp in MaxLumaPs:
        for tier in range(2 if level >= 40 else 1):
            factor = int(math.sqrt(mlp))
            while mlp % factor != 0:
                factor -= 1
            name = 'cpbsize_' + str(level) if tier == 0 else 'cpbsize_' + str(level) + '_high'
            all_res.append([mlp / factor, factor, name, long_stream, level, tier, tile, make_cpb])

index = 0
total = len(all_res)
print 'creating file','create_special_{}.sh'.format(encoder)
fout = open('create_special_{}.sh'.format(encoder),'wb')
if encoder == 'vp9':
  print 'creating code to be added to profile.c:'
  print '//Code generated by script product/create_special.py'
  print 'encode_choose_license_resolution() {'
  print '  enc_license_resolution = enc_stream_seed%{}'.format(total)
  print '  enc_license_resolution_bitrate_control = 0'
  for a_res in all_res:
      if a_res == '':
          continue
      w,h,r,f = a_res.split()
      print '  if' if index == 0 else '  else if',
      print '(enc_license_resolution == {}) {{'.format(index)
      print '    enc_license_resolution_w =',w
      print '    enc_license_resolution_h =',h
      print '    enc_licence_frame_cnt =',10 if int(f) == 0 else 300
      if int(r) > 0:
          print '    enc_license_resolution_bitrate_control = 1'
          print '    enc_target_bits_per_frame =',r
      print '  }'
      index += 1
  print '}'
elif encoder == 'av1':
  print 'creating code to be added to profile.c:'
  print '//Code generated by script product/create_special.py'
  print 'encode_choose_license_resolution() {'
  print '  enc_license_resolution_specific_bitrate = 0'

  print '  if (enc_client_a_custom_set) {'
  print '    enc_license_resolution = enc_stream_seed%{}'.format(len(all_client_a))
  comments = client_a_comments
  next_comment = 0
  for a_res in all_client_a:
    if a_res == '':
      continue
    if next_comment < len(comments):
      comment_index, before_flag,comment = comments[next_comment]
      while comment_index <= index and before_flag:
        print '    {}'.format(comment)
        next_comment += 1
        if next_comment < len(comments):
          comment_index, before_flag, comment = comments[next_comment]
        else:
          break
    w,h,r,f = a_res.split()
    print '    if' if index == 0 else '    else if',
    print '(enc_license_resolution == {}) {{'.format(index)
    print '      enc_license_resolution_w =',w
    print '      enc_license_resolution_h =',h
    print '      enc_licence_frame_cnt =',10 if int(f) == 0 else 50,
    if int(r) > 0:
      print
      print '      enc_license_resolution_specific_bitrate = 1'
      print '      enc_target_bits_per_frame =',r,
    if next_comment < len(comments):
      comment_index, before_flag,comment = comments[next_comment]
      while comment_index <= index and not before_flag:
        print '    {}'.format(comment),
        next_comment += 1
        if next_comment < len(comments):
          comment_index, before_flag, comment = comments[next_comment]
        else:
          break
    print
    print '    }'
    index += 1
  index = 0
  print '  } else if (!enc_large_scale_tile) {'
  print '    enc_license_resolution = enc_stream_seed%{}'.format(total)
  comments = res_comments
  next_comment = 0
  for a_res in all_res:
    if a_res == '':
      continue
    if next_comment < len(comments):
      comment_index, before_flag,comment = comments[next_comment]
      while comment_index <= index and before_flag:
        print '    {}'.format(comment)
        next_comment += 1
        if next_comment < len(comments):
          comment_index, before_flag, comment = comments[next_comment]
        else:
          break
    w,h,r,f = a_res.split()
    print '    if' if index == 0 else '    else if',
    print '(enc_license_resolution == {}) {{'.format(index)
    print '      enc_license_resolution_w =',w
    print '      enc_license_resolution_h =',h
    print '      enc_licence_frame_cnt =',10 if int(f) == 0 else 50,
    if int(r) > 0:
      print
      print '      enc_license_resolution_specific_bitrate = 1'
      print '      enc_target_bits_per_frame =',r,
    if next_comment < len(comments):
      comment_index, before_flag,comment = comments[next_comment]
      while comment_index <= index and not before_flag:
        print '    {}'.format(comment),
        next_comment += 1
        if next_comment < len(comments):
          comment_index, before_flag, comment = comments[next_comment]
        else:
          break
    print
    print '    }'
    index += 1
  print '  } else { //enc_large_scale_tile'
  print '    enc_license_resolution = enc_stream_seed%{}'.format(len(all_tiles))
  print '    enc_anchor_frame_cnt u(0)'
  print '    '
  comments = tile_comments
  next_comment = 0
  index = 0
  for a_tile in all_tiles:
    if a_tile == '':
      continue
    if next_comment < len(comments):
      comment_index, before_flag,comment = comments[next_comment]
      while comment_index <= index and before_flag:
        print '    {}'.format(comment)
        next_comment += 1
        if next_comment < len(comments):
          comment_index, before_flag, comment = comments[next_comment]
        else:
          break
    tile_cols,tile_rows,tile_width,use_128,tile_list_frame_cnt = a_tile.split()
    print '    if' if index == 0 else '    else if',
    print '(enc_license_resolution == {}) {{'.format(index)
    print '      enc_use_128x128_superblock =',use_128
    print '      enc_tile_cols =',tile_cols
    print '      enc_tile_rows =',tile_rows
    print '      enc_tile_width_sb =',tile_width
    print '      enc_tile_list_frame_cnt =',10 if int(tile_list_frame_cnt) == 0 else 50,
    if next_comment < len(comments):
      comment_index, before_flag,comment = comments[next_comment]
      while comment_index <= index and not before_flag:
        print '    {}'.format(comment),
        next_comment += 1
        if next_comment < len(comments):
          comment_index, before_flag, comment = comments[next_comment]
        else:
          break
    print
    print '    }'
    index += 1
  print '    enc_tile_height_sb = 1'
  print '    res = enc_use_128x128_superblock ? 128 : 64'
  print '    enc_license_resolution_w = enc_tile_cols * enc_tile_width_sb * res'
  print '    enc_license_resolution_h = enc_tile_rows * enc_tile_height_sb * res'
  print '  }'
  print '}'

#SCRIPT CODE
if encoder == 'av1':
  profs = ['0'] if is_client_a else ['0','1','2']
  prof_sets = ['core','not-annexb'] if is_client_a else ['core','not-annexb','large_scale_tile']
  additional_choose_functions = ',choose_client_a_custom' if is_client_a else ''
  all_profiles = []
  for prof in profs:
    if profile_limits != [] and prof not in profile_limits:
      print 'Skipping profile',prof
      continue
    for prof_set in prof_sets:
      if profile_limits != [] and prof_set not in profile_limits:
        print 'In profile',prof,'Skipping folder',prof_set
        continue
      all_profiles.append((prof,prof_set))
  for prof,prof_set in all_profiles:
    encode_command = "--not-annexb" if prof_set == 'not-annexb' else ("--large-scale-tile" if prof_set == 'large_scale_tile' else '')
    decode_command = "--not-annexb" if prof_set == 'not-annexb' else ''
    prof_str = 'profile{}_{}'.format(prof,prof_set)
    prof_num = 'profile{}'.format(prof)
    #fout.write('#!/bin/sh\n')
    fout.write('rm -rf ../tools/build/build/av1enc\n')
    fout.write('P8005_BUILD_PREFIX=$(realpath ${P8005_BUILD_PREFIX:-../tools})\n')
    fout.write('export P8005_BUILD_PREFIX\n')
    fout.write('BUILD_PROCESSES=16\n')
    fout.write('echo "Building Encoder"\n')
    fout.write('make -C ../tools -j $BUILD_PROCESSES PROFILE=profile_license_resolutions_{}{} P8005_BUILD_PREFIX=${{P8005_BUILD_PREFIX}} TARGET=17 ${{P8005_BUILD_PREFIX}}/build/av1enc/av1.gen.exe > /dev/null\n'.format(prof_num,additional_choose_functions))
    fout.write('ret=$?\n')
    fout.write('if [ $ret -ne 0 ]\n')
    fout.write('then\n')
    fout.write('  echo "  Failed make encoder."\n')
    fout.write('  exit 1\n')
    fout.write('fi\n')
    fout.write('echo "Building AV1 Decoder"\n')
    fout.write('make -C ../tools TARGET=16 -j $BUILD_PROCESSES $P8005_BUILD_PREFIX/build/av1dec/av1.gen.exe > /dev/null\n')
    fout.write('ret=$?\n')
    fout.write('if [ $ret -ne 0 ]\n')
    fout.write('then\n')
    fout.write('  echo "  Failed make decoder."\n')
    fout.write('  exit 1\n')
    fout.write('fi\n')
    fout.write('echo "Building AV1B Decoder"\n')
    fout.write('make -C ../tools TARGET=20 -j $BUILD_PROCESSES $P8005_BUILD_PREFIX/build/av1bdec/av1b.gen.exe > /dev/null\n')
    fout.write('ret=$?\n')
    fout.write('if [ $ret -ne 0 ]\n')
    fout.write('then\n')
    fout.write('  echo "  Failed make decoder."\n')
    fout.write('  exit 1\n')
    fout.write('fi\n')
    fout.write('LIBVPX=$P8005_BUILD_PREFIX/build-aom\n')
    fout.write('make -C $LIBVPX -j $BUILD_PROCESSES aomdec lightfield_tile_list_decoder\n')
    index = 0
    fdir_base = '{}_special'.format(prof_str)
    fdir_ref = fdir_base + '/md5_ref'
    fdir_ref_fg = fdir_base + '/md5_no_film_grain'
    fdir = fdir_base + '/streams'
    for ff in [fdir_base, fdir_ref, fdir_ref_fg, fdir]:
      fout.write('if [[ ! -e {} ]]\n'.format(ff))
      fout.write('then\n')
      fout.write('  mkdir {}\n'.format(ff))
      fout.write('fi\n')
    special_set = all_client_a if is_client_a else (all_tiles if prof_set == 'large_scale_tile' else all_res)
    for seed_1,a_special in enumerate(special_set):
      #if seed_1+1 not in [30, 33, 35]:
      #  index += 1
      #  continue
      if a_special == '':
        continue
      res = ''
      fname_ext = '{}/test{}.obu'.format(fdir,seed_1+1)
      fname = '{}/test{}'.format(fdir,seed_1+1)
      fout.write('OFFSET=0\n')
      fout.write('while true; do\n')

      number_of_each = 10 if seed_1+1 >= len(special_set)/2 else 40
      fout.write('  {{ for IND in `seq 0 {}`; do\n'.format(number_of_each-1))
      fout.write('    echo "../tools/build/av1enc/av1.gen.exe -v -s $[{0}+$OFFSET] -o {1}_${{IND}}.obu {2} > test_${{IND}}.txt && ../tools/build/av1dec/av1.gen.exe {3} -v {1}_${{IND}}.obu >> test_${{IND}}.txt && ../tools/build/av1bdec/av1b.gen.exe {3} -v {1}_${{IND}}.obu >> test_${{IND}}.txt" || echo \"Check Failed Ret Code was not 0\" >> test_${IND}\n'.format(index,fname,encode_command,decode_command))
      fout.write('    OFFSET=$[$OFFSET+{}]\n'.format(len(special_set)))
      fout.write('  done } | xargs -n 1 -P 12 -I {} bash -c {}\n')
      fout.write('  BEST_IND=0\n')
      fout.write('  MIN_SZ=0\n')
      fout.write('  cp {}_${{BEST_IND}}.obu {}\n'.format(fname,fname_ext))
      fout.write('  for IND in `seq 0 {}`; do\n'.format(number_of_each-1))

      #fout.write('  for IND in `seq 0 4`; do\n')
      #fout.write('    ../tools/build/av1enc/av1.gen.exe -s $[{}+$OFFSET] -o {}_${{IND}}.obu  > /dev/null\n'.format(index,fname))
      fout.write('    touch {}_${{IND}}.obu\n'.format(fname))
      fout.write('    if [ ! -f test_${IND}.txt ]\n')
      fout.write('    then\n')
      fout.write('      echo "test_${IND}.txt not found"\n')
      fout.write('      rm {}_${{IND}}.obu\n'.format(fname))
      fout.write('      continue\n')
      fout.write('    fi\n')
      fout.write('    ARR=$(du {}_${{IND}}.obu | tr "\\t" "\\n")\n'.format(fname))
      fout.write('    for SZ in $ARR; do break; done\n')
      fout.write('    if [[ "$MIN_SZ" -eq "0" || "$SZ" -lt "$MIN_SZ" ]]\n')
      fout.write('    then\n')
      fout.write('      if ! egrep "Check Failed" test_${IND}.txt > /dev/null\n')
      fout.write('      then\n')
      fout.write('        MIN_SZ=$SZ\n')
      fout.write('        BEST_IND=$IND\n')
      fout.write('        mv {}_${{IND}}.obu {}\n'.format(fname,fname_ext))
      fout.write('      else\n')
      fout.write('        echo "Check Failed on ${IND}"\n')
      fout.write('        egrep "Check Failed" test_${IND}.txt | head -1\n')
      fout.write('        rm {}_${{IND}}.obu\n'.format(fname))
      fout.write('      fi\n')
      fout.write('    else\n')
      fout.write('      rm {}_${{IND}}.obu\n'.format(fname))
      fout.write('    fi\n')
      fout.write('  done\n')

      #fout.write('  ../tools/build/av1dec/av1.gen.exe -v {} > test.txt\n'.format(fname_ext))
      fout.write('  if ! egrep "Check Failed" test_${BEST_IND}.txt > /dev/null\n')
      fout.write('  then\n')
      if prof_set != 'large_scale_tile':
        w,h,r,f = a_special.split()
        fout.write('    if ! egrep "post-crop size: {}x{}" test_${{BEST_IND}}.txt > /dev/null\n'.format(w,h))
        fout.write('    then\n')
        fout.write('      echo "Incorrect Size {} $OFFSET"\n'.format(fname_ext))
        fout.write('      OFFSET=$[$OFFSET+{}]\n'.format(len(special_set)*number_of_each))
        fout.write('    else\n')
      fout.write('      ARR=$(du {} | tr "\\t" "\\n")\n'.format(fname_ext))
      fout.write('      for SZ in $ARR; do break; done\n')
      fout.write('      if [[ "$SZ" -gt "524288" || "$SZ" -eq "0" ]]\n')
      fout.write('      then\n')
      fout.write('        if [ "$SZ" -gt "524288" ]\n')
      fout.write('        then\n')
      fout.write('          echo "Filesize $SZ too large {} $OFFSET"\n'.format(fname_ext))
      fout.write('        else\n')
      fout.write('          echo "Filesize $SZ too small {} $OFFSET"\n'.format(fname_ext))
      fout.write('        fi\n')
      fout.write('        OFFSET=$[$OFFSET+{}]\n'.format(len(special_set)*number_of_each))
      fout.write('      else\n')
      fout.write('        echo "Done {}, index chosen: ${{BEST_IND}}"\n'.format(fname_ext))
      fout.write('        break\n'.format(len(special_set)))
      fout.write('      fi\n')
      if prof_set != 'large_scale_tile':
        fout.write('    fi\n')
      fout.write('  else\n')
      fout.write('    echo "Best Stream Check Failed {} $OFFSET"\n'.format(fname_ext))
      fout.write('    egrep "Check Failed" test_${IND}.txt | head -1\n')
      fout.write('    OFFSET=$[$OFFSET+{}]\n'.format(len(special_set)*number_of_each))
      fout.write('  fi\n')
      fout.write('done\n')
      fout.write('rm test_*.txt\n')
      index += 1
    fout.write('echo "Calculating CSV"\n')
    fout.write('../tools/build/av1dec/av1.gen.exe {3} -c {0}/{1}_special.csv {2}/test* > /dev/null\n'.format(fdir_base,prof_str,fdir,decode_command))
    fout.write('echo "Calculating md5_ref {}"\n'.format(fdir_base))
    fout.write('cd ../av1product/\n')
    av1_options = ''
    if prof_set == 'not-annexb':
      av1_options = "--av1_options --not-annexb"
    fout.write('../av1product/test -ref -c -j 10 -do-film-grain-noise -max-oppoints {} {}\n'.format(av1_options,fdir_base))
    fout.write('../av1product/test -c -j 10 -do-film-grain-noise -max-oppoints {} {}\n'.format(av1_options,fdir_base))
    fout.write('cd -\n')
else:
    raise RuntimeError('Unknown encoder: {}'.format(encoder))
