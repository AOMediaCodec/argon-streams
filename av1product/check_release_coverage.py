#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A tool to generate coverage reports across a release'''

# TODO: This sits in av1product at the moment because we run coverbin in a way
#       that needs to understand AV1 (all the --annexb / --not-annexb stuff).
#       It would be nice if we could factor that out, because the rest is not
#       codec specific at all.

import argparse
import os
import shlex
import subprocess
import sys
import tempfile


def parse_args():
    '''Read command line arguments.'''
    parser = argparse.ArgumentParser()
    parser.add_argument('--jobs', '-j', type=int, default=1,
                        help=('The number of coverage tools to run in '
                              'parallel. Defaults to 1.'))
    parser.add_argument('--batch', '-b', type=int, default=1,
                        help=('The number of streams to run through each '
                              'coverage tool invocation. Defaults to 1. No '
                              'effect unless --jobs is greater than one. This '
                              'can be used to avoid massive disk usage for '
                              'XML files when there are lots of streams.'))
    parser.add_argument('--profiles', default='0', dest='p_str')
    parser.add_argument('coverbin', help='Coverage binary')
    parser.add_argument('release', help='Release directory')
    parser.add_argument('xml_pattern',
                        help=('The file that should be written with coverage. '
                              'The substring "{}" will be substituted with '
                              'the name of the profile.'))
    parser.add_argument('set_pattern', nargs='+',
                        help=('A set that to evaluate for coverage. If the '
                              'pattern contains "{}", this will get the value '
                              'of P for each repetition (see the --profiles) '
                              'argument.'))

    args = parser.parse_args()
    setattr(args, 'profiles', args.p_str.split())
    delattr(args, 'p_str')

    # Check that profiles are distinct
    if len(args.profiles) > len(set(args.profiles)):
        raise RuntimeError('Profiles are not distinct.')

    # And check that the xml pattern has at least one '{}' if there is
    # more than one profile.
    if len(args.profiles) > 1 and '{}' not in args.xml_pattern:
        raise RuntimeError('Multiple profiles but no "{}" in xml_pattern.')

    return args


def elaborate_sets(profiles, set_patterns):
    '''Do the cross product to figure out what sets need covering.

    Returns a map from profile to its list of set names, together with
    the total set of set names.

    '''
    sets = set()
    prof_to_sets = {}

    for profile in profiles:
        psets = []
        for pattern in set_patterns:
            set_name = pattern.replace('{}', profile)
            sets.add(set_name)
            psets.append(set_name)
        prof_to_sets[profile] = psets

    return (sets, prof_to_sets)


def get_streams(rel_dir, set_name):
    '''Get the list of streams for the given release and set name.'''
    dir_name = os.path.join(rel_dir, set_name, 'streams')
    return [os.path.join(dir_name, stream)
            for stream in os.listdir(os.path.join(dir_name))
            if stream.endswith('.obu')]


def stream_commands(cov_dir, coverbin, streams, annexb, batch_size):
    '''Make some argoncover commands in dirname.

    Return the commands together with the generated XML files'''
    commands = []
    cov_files = []
    while streams:
        batch = streams[:batch_size]
        streams = streams[batch_size:]

        cmd_file = os.path.join(cov_dir, '{:04}.sh'.format(len(commands)))
        cov_file = os.path.join(cov_dir, '{:04}.xml'.format(len(commands)))
        with open(cmd_file, 'w') as handle:
            handle.write('{} -p {} {} {}\n'
                         .format(shlex.quote(coverbin),
                                 shlex.quote(cov_file),
                                 '--annexb' if annexb else '--not-annexb',
                                 ' '.join(shlex.quote(stream)
                                          for stream in batch)))
        commands.append(cmd_file)
        cov_files.append(cov_file)

    return (commands, cov_files)


def coverage_cmds(cov_dir, rel_dir, coverbin, sets, batch_size):
    '''Generate shell scripts to run the coverage tool

    Return the list of these scripts, together with a dictionary from set name
    to the list of paths of coverage files generated for that set.

    '''
    commands = []
    set_to_cov_files = {}

    for set_name in sorted(sets):
        set_cov_dir = os.path.join(cov_dir, set_name)
        os.makedirs(set_cov_dir, exist_ok=True)

        streams = get_streams(rel_dir, set_name)
        cmds, covs = stream_commands(set_cov_dir, coverbin,
                                     streams,
                                     not ('not_annexb' in set_name),
                                     batch_size)
        commands += cmds
        set_to_cov_files[set_name] = covs

    return (commands, set_to_cov_files)


def write_merge_command(cmd_path, merge_tool, out_path, in_paths):
    '''Write a command to run merge_tool to cmd_path'''
    with open(cmd_path, 'w') as handle:
        handle.write('{} -o {}'
                     .format(shlex.quote(merge_tool),
                             shlex.quote(out_path)))
        for in_path in in_paths:
            handle.write(' ' + shlex.quote(in_path))
        handle.write('\n')


def merge_cmds(cov_dir, merge_tool, set_to_cov_files):
    '''Generate a list of commands to run the merge tool

    Returns the pair (commands, set_to_merged) where commands is a list of
    shell scripts and set_to_merged maps from set name to the merged XML that
    will be created for that set.

    '''
    commands = []
    set_to_merged = {}
    for set_name, cov_files in set_to_cov_files.items():
        cmd_file = os.path.join(cov_dir, set_name + '.merge.sh')
        merged_file = os.path.join(cov_dir, set_name + '.xml')
        write_merge_command(cmd_file, merge_tool, merged_file, cov_files)
        commands.append(cmd_file)
        set_to_merged[set_name] = merged_file

    return (commands, set_to_merged)


def profile_merge_cmds(cov_dir, prof_to_sets,
                       xml_pattern, merge_tool, set_to_merged):
    '''Generate commands to run the merge tool to combine sets for profiles'''
    commands = []
    prof_to_cov = {}
    for profile, set_names in prof_to_sets.items():
        cmd_file = os.path.join(cov_dir, profile + '.pmerge.sh')
        merged_file = xml_pattern.replace('{}', profile)
        inputs = [set_to_merged[set_name] for set_name in set_names]
        write_merge_command(cmd_file, merge_tool, merged_file, inputs)
        commands.append(cmd_file)
        prof_to_cov[profile] = merged_file

    return (commands, prof_to_cov)


def parallel_run(scripts, max_jobs):
    '''Use xargs to run the given list of shell scripts'''
    with tempfile.NamedTemporaryFile() as handle:
        for script in scripts:
            handle.write(script.encode('utf-8') + b'\0')

        # Make sure everything makes it to the file before we start xargs
        # (otherwise it will miss the last entries).
        handle.flush()

        par_args = []
        if max_jobs > 1:
            par_args = ['-P', str(max_jobs)]
        subprocess.check_call(['xargs', '-0',
                               '-a', handle.name,
                               '-n', '1'] + par_args +
                              ['sh'])


def progress(msg):
    '''Print msg and flush stdout'''
    print(msg)
    sys.stdout.flush()


def find_merge_tool():
    '''Return the path to merge_coverage.py'''
    prod_dir = os.path.dirname(__file__)
    path = os.path.normpath(os.path.join(prod_dir, '..',
                                         'autoproduct', 'merge_coverage.py'))
    if not os.path.exists(path):
        raise RuntimeError('Cannot find merge tool at {}'.format(path))
    return path


def main(args):
    '''The main function'''
    merge_tool = find_merge_tool()
    sets, prof_to_sets = elaborate_sets(args.profiles, args.set_pattern)

    batch_size = args.batch if args.jobs > 1 else 10000

    with tempfile.TemporaryDirectory() as cov_dir:
        # Generate all the commands to run the coverage tool
        progress('Calculating coverage commands')
        cmds, set_to_cov = coverage_cmds(cov_dir,
                                         args.release, args.coverbin,
                                         sets, batch_size)

        # Actually run it
        progress('Running {} coverage jobs with {} threads'
                 .format(len(cmds), args.jobs))
        parallel_run(cmds, args.jobs)

        # ... this might take a while

        # Now merge the coverage for each set. We could be clever and do
        # something logarithmic to keep parallelism but let's do the simple
        # thing for now.
        progress('Merging coverage for each set')
        cmds, set_to_merged = merge_cmds(cov_dir,
                                         merge_tool,
                                         set_to_cov)

        parallel_run(cmds, args.jobs)

        # Finally merge sets for each profile
        progress('Merging set coverage for each profile')
        cmds, prof_to_cov = profile_merge_cmds(cov_dir,
                                               prof_to_sets,
                                               args.xml_pattern,
                                               merge_tool,
                                               set_to_merged)

        parallel_run(cmds, args.jobs)

    print('Done. Output files:')
    for profile in args.profiles:
        print('  ' + prof_to_cov[profile])
    return 0


if __name__ == '__main__':
    try:
        exit(main(parse_args()))
    except RuntimeError as err:
        sys.stderr.write('Error: {}'.format(err))
        exit(1)
