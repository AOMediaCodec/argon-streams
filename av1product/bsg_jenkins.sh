#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This Jenkins script builds a release of the bitstream generator,
# running some sanity checks on the way.

set -o errexit -o pipefail -o nounset

usage () {
    echo "Usage: bsg_jenkins.sh [-h] [-j <jobs>] [--coverage]"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hj:C
LONGOPTS=help,jobs:,coverage

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

JOBS=1
COVERAGE_ARG=""

# At this point, our command line arguments are in a sane order and
# easy to read, ending with a '--'.
while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            JOBS="$2"
            shift 2
            ;;
        -C|--coverage)
            COVERAGE_ARG=--coverage
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# We just have no positional arguments left.
if [ $# != 0 ]; then
    echo 1>&2 "No positional arguments expected."
    usage 1
fi

### End of command line parsing

AV1PRODUCT_DIR="$(dirname ${BASH_SOURCE})"

# This is a bit rubbish. We explicitly override CXX to be g++-7
# because Athene's default g++ (4.8.5) has a non-working <regex>
# implementation. Boo.
if [ "x$(hostname)" == xathene ]; then
    export CXX=g++-7
fi

# Make sure aomdec exists
echo "Building aomdec in aom/build"
"$AV1PRODUCT_DIR/get-aomdec.sh" -j"$JOBS" aom/src aom/build

# Build the BSG release using with-seed.sh. This will print the
# command, so we needn't bother doing it here. The {} will be replaced
# by the chosen seed.
"$AV1PRODUCT_DIR/with-seed.sh" \
  "${AV1PRODUCT_DIR}/bsg_release.sh" \
   -s {} -j$JOBS ${COVERAGE_ARG} \
   "aom/build/aomdec" \
   "aom/build/examples/lightfield_tile_list_decoder" \
   bsg-release
