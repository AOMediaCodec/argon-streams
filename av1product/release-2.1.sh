#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# We expect to run from within the hevc directory and to have a
# directory layout that looks like:
#
#   ../releases/v2.0
#              /stress
#     /bin/argoncover
#         /
#     /av1-v2.1-*/bin/linux/argoncover
#     /hevc
#
# We assume that v2.0 contains the version 2.0 release (released as
# argon_streams_av1_base_and_extended_profiles_v2.0.zip) and stress
# contains the stress streams that we generated using stresser.py (and
# lots of blood, sweat and tears).
#
# The argoncover binary is a coverage tool that should have been built
# on athene in release mode. The other files in bin follow the
# autoproduct naming scheme (eugh).
#
#   av1dec                        -> bin/additional_decoder.exe
#   av1bdec                       -> bin/decoder.exe
#   aomdec                        -> bin/av1_ref_c.exe
#   lightfield_tile_list_decoder  -> bin/av1_ref_c_lst.exe
#
# We will create ../releases/v2.1 with the release. We will also
# create ../build containing various logs and binaries.

TMP="$(mktemp -d)"
trap "{ rm -rf $TMP; }" EXIT

set -e

logs=../build/logs
csvs0=../build/csvs
csvs1=../build/ext-csvs

bin_dir=../bin

src_rel=../releases/v2.0
dst_rel=../releases/v2.1
stress=../releases/stress

bin_names="argoncover additional_decoder.exe decoder.exe "
bin_names="$bin_names av1_ref_c.exe av1_ref_c_lst.exe"
for name in $bin_names; do
    if [ ! -x "$bin_dir/$name" ]; then
        echo >&2 "No binary at $bin_dir/$name."
        exit 1
    fi
done
coverage_tool="$bin_dir/argoncover"

if [ -e $dst_rel ]; then
    echo >&2 "Release already exists."
    exit 1
fi

if [ -e ../build ]; then
    echo >&2 "Build directory already exists"
    exit 1
fi

if [ ! -d $stress ]; then
    echo >&2 "Stress streams not at $stress."
    exit 1
fi

J=-j$(nproc)

# Take a copy of the sets from the v2.0 release, plus the
# streams-added-since-v1.2.lst list.
set -x
mkdir "$dst_rel"
cp -r "$src_rel"/profile* "$dst_rel"
cp -r "$src_rel"/streams-added-since-v1.2.lst "$dst_rel"
chmod -R u+w "$dst_rel"
{ set +x; } 2>/dev/null

# Make ourselves a logs directory
set -x
mkdir -p "$logs"
{ set +x; } 2>/dev/null

# Copy the ArgonViewer directory into the release
set -x
cp -r product/ArgonViewerV14 "$dst_rel"
{ set +x; } 2>/dev/null

# Copy stress streams into the release. First, make the target
# directories
mkdir -p $dst_rel/profile{0,1,2}_stress/streams

# Now copy the files
echo "Copying stress streams"
(for d in $stress/*; do
     P=$(echo $d | cut -d - -f 2)
     cp $d/streams/*.obu $dst_rel/profile${P}_stress/streams
 done)

# Check that no streams have been lost.
v20_count="$(find "$src_rel" -name '*.obu' | wc -l)"
stress_count="$(find "$stress" -name '*.obu' | wc -l)"
v21_count="$(find "$dst_rel" -name '*.obu' | wc -l)"
if [ $(($v20_count + $stress_count)) -ne "$v21_count" ]; then
    echo >&2 "Unexpected stream counts!"
    echo >&2 "  We found $v20_count streams in the v2.0 release"
    echo >&2 "  We found $stress_count streams in the stress streams"
    echo >&2 "  We found $v21_count streams in the v2.1 release. Huh?"
    exit 1
fi

# Make the stress streams' file lists. Since all the streams are
# level 6.3 or below, all_list.txt and level6.x_list.txt are the
# same and contain everything:
echo "Making file lists"
(for d in $stress/*; do
     P=$(echo $d | cut -d - -f 2)
     dst=$dst_rel/profile${P}_stress
     (cd $d/streams; ls) >>$dst/all_list.txt
     (cd $d/streams; ls) >>$dst/level6.x_list.txt
 done)
(for d in $stress/5*; do
     P=$(echo $d | cut -d - -f 2)
     dst=$dst_rel/profile${P}_stress
     (cd $d/streams; ls) >>$dst/level5.x_list.txt
 done)
(for p in 0 1 2; do
     dir=$dst_rel/profile${p}_stress/
     sort -o $dir/all_list.txt $dir/all_list.txt
     sort -o $dir/level5.x_list.txt $dir/level5.x_list.txt
     sort -o $dir/level6.x_list.txt $dir/level6.x_list.txt
 done)

# Make reference commands for our new stress streams. This will create
# a ref_cmd directory for each of our new profileX_stress sets. These
# only need layer 0 (in the same way as error streams and LST
# streams).
echo "Making ref commands for stress streams"
(src=$dst_rel/profile0_core/ref_cmd/test10001.sh;
 for p in 0 1 2; do
     st=$dst_rel/profile${p}_stress
     mkdir $st/ref_cmd
     for s in $st/streams/*; do
         base=$(basename $s .obu)
         dst=$st/ref_cmd/$base.sh
         sed -e "s/test10001/$base/g" $src >$dst
     done
 done)

# Make empty md5 directories for stress streams
echo "Making md5 directories"
for p in 0 1 2; do
    mkdir $dst_rel/profile${p}_stress/{md5_ref,md5_no_film_grain}
done

# Bodge some existing streams to fix decoder model conformance.
set -x
av1product/fix_decoder_model.py "$src_rel" "$dst_rel"
{ set +x; } 2>/dev/null

# Put our coverage tool in the release
set -x
mkdir -p "$dst_rel"/bin/linux
cp "$coverage_tool" "$dst_rel"/bin/linux/argoncover
{ set +x; } 2>/dev/null

# At this point, we should have a sane looking release, except that
# there will be no coverage and some MD5s will be missing.
single_layer_re='error|large_scale_tile|stress'

echo "Checking the release looks sane (except missing md5s)"
"product/check-release-structure.sh" \
    --skip-md5s --single-layer-re="$single_layer_re" \
    $dst_rel

echo "Checking and/or creating md5s & CSVs for all streams and layers"
"product/check_release_files.py" \
    $J --joblog "$logs/check_release_files.joblog" --no-progress \
    --ensure --csv "$csvs0" \
    "$bin_dir" "av1product/test-one-file.sh" "$dst_rel"

# At this point, we have CSV files for everything but the error
# streams (whose stream documentation we'll manually copy across from
# the v2.0 release). We do, however, want to add a couple of extra
# columns with special "stressy" information.
echo "Extending CSVs for stress streams"
av1product/stress-streams/extend_stress_csvs.py \
    "$stress" "$csvs0" "$csvs1"

# Generate coverage for each profile. Note that we've hacked some
# streams, so we definitely need to regenerate this. Also note that we
# run from inside the release directory, to avoid the stream names
# including its path.
echo "Generating coverage"
crc="$(readlink -f av1product/check_release_coverage.py)"
(cd $dst_rel;
 "$crc" \
     $J -b 4 --profiles="0 1 2" \
     bin/linux/argoncover \
     . \
     "prof_av1_profile{}.xml" \
     profile{}_core \
     profile{}_large_scale_tile \
     profile{}_not_annexb) >"$logs/coverage.log" 2>&1 || {

    echo >&2 "Running coverage failed. Full details in ../logs/coverage.log,"
    echo >&2 "the tail of which is:"
    tail >&2 -n20 "$logs/coverage.log"
    exit 1
}

# Once that has finished, we should have all the MD5s our hearts could
# desire. Run the check-release-structure script again to make sure
# they've appeared.
echo "Checking the release looks sane (including md5s)"
"product/check-release-structure.sh" \
    --single-layer-re="$single_layer_re" "$dst_rel"

# Finally, smoosh together the CSV files that we created. We'll have
# one per stream, looking like "${csvs1}/<set>/<stream>.csv". Note
# that we know the set and CSV names look fine (because we passed
# check-release-structure.sh above).
smoosh_csv () {
    local set_name="$1"

    # Excel doesn't allow sheet names with more than 31 characters.
    local short_name="$(echo "${set_name}" | head -c 31)"

    local dst="$short_name.csv"
    local first=1

    for csv in $(find ${set_name} -name '*.csv' | sort); do
        if [ $first = 1 ]; then
            first=0
            cp "$csv" "$dst"
        else
            tail -n +2 "$csv" >>"$dst"
        fi
    done
}

echo "Combining CSV files"
$(cd "$csvs1";
  for set_name in $(find -mindepth 1 -maxdepth 1 -type d); do
      smoosh_csv "${set_name}"
  done)

# Let's check that the only files that have newly appeared are in the
# stress stream directories.
echo "Listing v2.0 files"
find "${src_rel}" -type f -printf '%P\n' | sort >"../build/2.0.lst"
echo "Listing v2.1 files"
find "${dst_rel}" -type f -printf '%P\n' | sort >"../build/2.1.lst"

comm -13 "../build/2.0.lst" "../build/2.1.lst" >"../build/new.lst"

if grep -v stress "../build/new.lst" >/dev/null; then
    echo >&2 "There are files in ../build/new.lst that are not stress streams."
    exit 1
fi

echo "Diffing releases"
diff -qr "${src_rel}" "${dst_rel}" >../build/diffs || true

# We're interested in the lines in diffs that look like
#
#   Files path/A/foo.obu and path/B/foo.obu differ
#
# Strip out initial parts of the path and throw away the coverage
# reports (xml) and coverage binary (argoncover).
echo "Making list of changed streams"
change_list="${dst_rel}/streams-changed-in-v2.1.lst"
grep differ ../build/diffs | \
    grep -v coverage.xslt | \
    grep -v xml | grep -v argoncover | \
    awk '{print $2}' | \
    sed 's!.*profile!profile!' | \
    sort >"$change_list"

# The only entries in streams-changed-in-v2.1.lst should be streams.
# Check the filenames all match profile.*\.obu.
if grep -v '^profile.*/streams/test.*\.obu$' "$change_list" >/dev/null; then
    echo >&2 "Some changed files in the list don't look like streams."
    exit 1
fi
