#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This script chooses a random seed and runs the rest of the command
# with SEED set to that number in the environment. The seed is written
# to a file called .last-seed, which is deleted on success but re-used
# as a seed if it exists on the next run.
#
# Any occurrence of {} in the command to run is replaced by $SEED
# (this is a bit like the behaviour of tools like find).

# If there are no arguments, there is nothing to do. Exit silently.
if [ $# = 0 ]; then
    exit 0
fi

set -o errexit -o pipefail -o nounset

# Decide on a seed. This is either set from .last-seed, found in the
# current directory or, if that file doesn't exist, we pick a new
# seed.
SEED=
if [ -s .last-seed ]; then
    echo "Using existing seed from the file .last-seed"
    SEED=$(grep -E '^[0-9]+$' .last-seed | head -n1 || true)
    echo "Read SEED=$SEED"
    if [ x"$SEED" == x ]; then
        echo "Stored seed in .last-seed seems bad for some reason. "
        echo "Picking a new one."
    fi
fi

if [ x"$SEED" == x ]; then
    SEED=$RANDOM
    echo "Random SEED set to $SEED."
    echo "$SEED" >.last-seed
fi
export SEED="$SEED"

# Now try to tidy up the command line
declare -a cmd
nargs="$#"
for ((i = 0; i < "$nargs"; i++)); do
    cmd[i]="$(echo $1 | sed "s!{}!$SEED!g")"
    shift
done

# Run the command
set +e
set -x
"${cmd[@]}"
{ STATUS=$?; set +x; } 2>/dev/null
set +x

if [ $STATUS -ne 0 ]; then
    echo "Command failed. Not deleting .last-seed."
else
    rm -f .last-seed
fi

exit $STATUS
