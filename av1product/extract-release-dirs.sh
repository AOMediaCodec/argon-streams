#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# A script that extracts the bits for a set we need from an
# autoproduct run and adds them to a release.

set -o errexit -o pipefail -o nounset

usage () {
    echo "Usage: extract-release-dirs.sh [-h] [--append] [--continue]"
    echo "                               <src> <dest> <pnum> <set_name>"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hc
LONGOPTS=help,append,continue

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

append=0
continue=0

while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        --append)
            append=1
            shift
            ;;
        -c|--continue)
            continue=1
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# We just have positional arguments left. We expect exactly four (for
# src, dest, profile number and set_name)
if [ $# != 4 ]; then
    echo 1>&2 "Wrong number of positional arguments."
    usage 1
fi

list_names="all_list.txt level5.x_list.txt level6.x_list.txt"

src="$1"
dst="$2"
pnum="$3"
set="$4"
shift 4

test -d "$src/$set" || error "No such source directory: '$dst/$set'."
if [ -e "$dst/$set" -a $append != 1 -a $continue != 1 ]; then
    error "'$dst/$set' already exists. Not overwriting."
fi

TMP="$(mktemp -d)"
trap "{ rm -rf $TMP; }" EXIT

ABS_TMP="$(readlink -f "$TMP")"

# Check that no file in $src/$set has a weird name. This ensure that
# we can loop over them safely below.
(cd "$src/$set";
 find -type f -print0 | grep -zvE '^[a-zA-Z0-9_.-]*$' >"${ABS_TMP}/bad-names")
test -e "$TMP/bad-names" -a -s "$TMP/bad-names" || {
    echo 1>&2 "Bad file name(s) in $src/$set"
    sed <"$TMP/bad-names" -z 's!^!\n  !g' | tr -d '\0' | head -n20 1>&2
    echo 1>&2
    exit 1
}

# Copy the stuff we need
mkdir -p "$dst/$set" || error "Failed to make directory '$dst/$set'."
for subdir in md5_no_film_grain md5_ref ref_cmd streams; do
    for rel_path in $(cd "$src/$set"; find "$subdir" -type f); do
        src_path="$src/$set/$rel_path"
        dst_path="$dst/$set/$rel_path"

        mkdir -p "$(dirname $dst_path)" ||
            error "Couldn't create directory to hold $dst_path."

        if [ -e "$dst_path" -a $continue = 0 ]; then
            error "Refusing to overwrite $dst_path with $src_path."
        fi

        cp "$src_path" "$dst_path" || \
            error "Failed to copy '$src_path' to '$dst_path'."
    done
done

# Now we need to construct the file lists based on the CSV file.
test -f "$src/$set.csv" || error "No CSV file at '$src/$set.csv'."

# We work in stages, first using awk to dump triples of (stream,
# profile, major level of oppoint 0) to a temporary file. We strip '"'
# characters as we go (we don't do comma escaping properly, but this
# shouldn't matter for the CSVs we generate)

awk <"$src/$set.csv" 'BEGIN { FS=","; OFS="," } { print $1,$4,$27 }' | \
    tr -d '"' >"$TMP/mini.csv" \
    || error "Oh dear. Something went wrong with the awk script."

# First, we check that the headings in the CSV match what we expect.
expected_heading='Stream name,Profile,Major level of oppoint 0'
if [ "x$(head -n1 "$TMP/mini.csv")" != "x$expected_heading" ]; then
    error "Headings in CSV don't match what we expect."
fi

# Ok, in which case, drop the head of the CSV to get just the data
# lines.
tail -n +2 "$TMP/mini.csv" >"$TMP/data.csv"

# We expect all the streams to claim the expected profile number (0, 1
# or 2).
awk <"$TMP/data.csv" 'BEGIN {FS=","} {print $2}' | \
    sort -u >"$TMP/profiles.lst"

if [ "x$(cat "$TMP/profiles.lst")" != "x$pnum" ]; then
    error "Not every item in the CSV file had profile number '$pnum'."
fi

# Finally, extract the file lists. Working inside the TMP directory
# like this means that we don't have to worry about splicing variables
# into the awk script.
(cd "$TMP"
 touch $list_names
 awk <"data.csv" \
     'BEGIN {FS=","} \
      ($3 <= 5) { print $1 >>"level5.x_list.txt" } \
      ($3 <= 6) { print $1 >>"level6.x_list.txt" } \
      { print $1 >>"all_list.txt" }')

# Copy or append the file lists that we've made to the destination directory
for lst in $list_names; do
    if [ $append = 1 ]; then
        cat "${TMP}/${lst}" >>"${dst}/${set}/${lst}"
        sort -u -o "${dst}/${set}/${lst}" "${dst}/${set}/${lst}"
    else
        sort -u -o "${dst}/${set}/${lst}" "${TMP}/${lst}"
    fi
done
