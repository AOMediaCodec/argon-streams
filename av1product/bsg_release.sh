#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This script generates (most of) a release of the bit-stream
# generator. It takes a single argument, the directory into which to
# build the release.

set -o errexit -o pipefail -o nounset

usage () {
    echo "Usage: bsg_release.sh [-h] [-j <jobs>] [-c] [-s seed] [--coverage] <aomdec> <lstdec> <release-dir>"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hj:cs:C
LONGOPTS=help,jobs:,continue,coverage

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

MAX_SAMPLE_STREAM_IDX=9
let "SAMPLE_STREAM_COUNT = $MAX_SAMPLE_STREAM_IDX + 1"

JOBS_ARG=""
CONTINUE=false
START_SEED=0
TEST_COVERAGE=0

# At this point, our command line arguments are in a sane order and
# easy to read, ending with a '--'.
while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            JOBS_ARG="-j${2}"
            shift 2
            ;;
        -c|--continue)
            CONTINUE=true
            shift
            ;;
        -s)
            START_SEED="$2"
            shift 2
            ;;
        -C|--coverage)
            TEST_COVERAGE=1
            shift 1
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# We just have positional arguments left. There should be exactly 3
# (aomdec, lstdec and release directory).
if [ $# != 3 ]; then
    echo 1>&2 "Wrong number of positional arguments."
    usage 1
fi

aomdec="$1"
shift

lstdec="$1"
shift

DEST="$1"
shift

# Check that the file named by aomdec actually exists
test -e "$aomdec" || {
    echo "aomdec argument is \`$aomdec', but that file does not exist."
    exit 1
}
abs_aomdec="$(readlink -f ${aomdec})"


# Check that the file named by lstdec actually exists
test -e "$lstdec" || {
    echo "lstdec argument is \`$lstdec', but that file does not exist."
    exit 1
}
abs_lstdec="$(readlink -f ${lstdec})"

### End of command line parsing

if [ $CONTINUE != true ]; then
    if [ -e "$DEST" ]; then
        error "Destination directory (${DEST}) already exists."
    fi
fi

AV1PRODUCT_DIR="$(dirname ${BASH_SOURCE})"
TOOLS_DIR="${AV1PRODUCT_DIR}/../tools"
PRODUCT_DIR="${AV1PRODUCT_DIR}/../product"

mkdir -p "${DEST}/logs"

# Take a copy of the tools directory and manually expunge all mention
# of clients. Using rsync means we don't clobber the modified dates if
# the files haven't changed (which is needed for --continue to work
# properly).
echo "Making copy of tools"
set -x
mkdir -p "${DEST}/tools"
rsync -ur "$TOOLS_DIR/Makefile" "${DEST}/tools"
rsync -ur "$TOOLS_DIR/"*.h "${DEST}/tools"
rsync -ur "$TOOLS_DIR/compiler"      "${DEST}/tools"
rsync -ur "$TOOLS_DIR/config-parser" "${DEST}/tools"
rsync -ur "$TOOLS_DIR/av1src"        "${DEST}/tools"
rsync -ur "$TOOLS_DIR/av1bsrc"       "${DEST}/tools"
{ set +x; } 2>/dev/null

echo
echo "Stripping client names from copy of tools"
${PRODUCT_DIR}/strip-clients.sh "${DEST}/tools"

VERSION=$(cd "$TOOLS_DIR"; git describe --tags --long --always --dirty)
echo
echo "This will be built as version ${VERSION}"
echo "${VERSION}" >"${DEST}/version"

COMMON_MAKE_ARGS="${JOBS_ARG}"
COMMON_MAKE_ARGS="${COMMON_MAKE_ARGS} RELEASE=1"
COMMON_MAKE_ARGS="${COMMON_MAKE_ARGS} P8005_BUILD_PREFIX=../build"
COMMON_MAKE_ARGS="${COMMON_MAKE_ARGS} GIT_VERSION=${VERSION}"

BUILD_ROOT="${DEST}/build/build"

run_make () {
    long_op="$1"
    short_op="$2"
    log_base="$3"
    shift 3

    echo
    echo "${long_op}"
    LOG="${DEST}/logs/${log_base}.log"

    set +e
    set -x
    time make -C "${DEST}/tools" ${COMMON_MAKE_ARGS} >&"${LOG}" "$@"
    { STATUS=$?; set +x; } 2>/dev/null

    if [ $STATUS -ne 0 ]; then
        echo "${short_op} failed. Last lines of log $LOG:"
        tail -n20 "$LOG"
        exit 1
    fi
    set -e
}

run_make "Running config parser unit tests" \
         "Config parser unit tests" \
         "config-test" \
         TARGET=17 PROFILE=configurable config-test

run_make "Building av1enc" "av1enc build" "av1enc-build" \
         TARGET=17 PROFILE=configurable USE_MYRAND=1 c-build

run_make "Building av1bdec" "av1bdec build" "av1bdec-build" \
         TARGET=20 c-build

abs_decoder="$(readlink -f ${BUILD_ROOT}/av1bdec/av1b.gen.exe)"
test -e "$abs_decoder" || {
    echo "av1bdec build did not produce a file at \`$abs_decoder'."
    exit 1
}

if [ $TEST_COVERAGE -eq 1 ]; then
    run_make "Building av1bcover" "av1bcover build" "av1bcover-build" \
             TARGET=21 RELEASE=0 c-build

    abs_av1bcover="$(readlink -f ${BUILD_ROOT}/av1bcover/av1b.gen.exe)"
    test -e "$abs_av1bcover" || {
        echo "abs_av1bcover build did not produce a file at \`$abs_av1bcover'."
        exit 1
    }
else
    abs_av1bcover=
fi

run_make "Checking example configuration files" \
         "Example configuration" \
         "configs" \
         TARGET=17 \
         PROFILE=configurable \
         DECODER="${abs_decoder}" \
         AOMDEC="${abs_aomdec}" \
         LSTDEC="${abs_lstdec}" \
         AV1BCOVER="${abs_av1bcover}" \
         START_SEED="$START_SEED" \
         TEST_COVERAGE="$TEST_COVERAGE" \
         check-configurations

run_make "Making some example streams" \
         "Example stream" \
         "streams" \
         TARGET=17 \
         PROFILE=configurable \
         DECODER="${abs_decoder}" \
         AOMDEC="${abs_aomdec}" \
         LSTDEC="${abs_lstdec}" \
         AV1BCOVER="${abs_av1bcover}" \
         TEST_COVERAGE="$TEST_COVERAGE" \
         START_SEED="0" NUM_SEEDS="${SAMPLE_STREAM_COUNT}" \
         check-configurations

run_make "Generating documentation CSV files" \
         "Docs build" "docs" \
         TARGET=17 PROFILE=configurable cfg-docs

echo
echo "Copying built files"
REL_BASE="release-v${VERSION}"
DOC_BASE="html"

REL_DIR="${DEST}/${REL_BASE}"
DOC_DIR="${DEST}/${DOC_BASE}"

mkdir -p "${REL_DIR}"
cp "${DEST}/version" "${REL_DIR}"
mkdir -p "${REL_DIR}/bin/linux"
cp "${BUILD_ROOT}/av1enc/av1.gen.exe" "${REL_DIR}/bin/linux/argongen"
cp "${BUILD_ROOT}/av1bdec/av1b.gen.exe" "${REL_DIR}/bin/linux/argondec"
mkdir -p "${REL_DIR}/configurations"
cp "${DEST}/tools/av1src/configurations/"*.cfg "${REL_DIR}/configurations"

rm -rf "${DOC_DIR}"
cp -r "${BUILD_ROOT}/av1enc/cfg-docs" "${DOC_DIR}"

echo
echo "Sampling example streams"

STREAM_DIR="${BUILD_ROOT}/av1enc/configurations"
SAMPLE_DEST="${REL_DIR}/sample-streams"
mkdir -p "$SAMPLE_DEST"

p=${MAX_SAMPLE_STREAM_IDX}
n=0
for pair in $(cd "${STREAM_DIR}"; \
              find -name '*.obu' | sed 's!+[0-9]*\.obu!!' | sort -u)
do
    # Start at m=n and then count up (wrapping if necessary) until we
    # find a nonempty OBU file. Empty files correspond to failed
    # decode checks; those seeds are clearly no good.
    found=0
    for m in $(seq $n ${MAX_SAMPLE_STREAM_IDX}; seq 0 $p); do
        obu_path="${STREAM_DIR}/${pair}+${m}.obu"
        if [ ! -e "${obu_path}" ]; then
            echo >&2 "Erk, no OBU file at $obu_path."
            exit 1
        fi

        if [ -s "${obu_path}" ]; then
            found=1
            break
        fi
    done

    if [ $found = 0 ]; then
        echo >&2 "Oh dear. " \
                 "The first ${SAMPLE_STREAM_COUNT} OBUs for ${pair} are empty"
        exit 1
    fi

    # Otherwise we found something (phew!). Copy the OBU to the same
    # directory as the config files.
    cp "${obu_path}" "${SAMPLE_DEST}"

    # n is the iteration counter modulo SAMPLE_STREAM_COUNT and p is
    # one less.
    if [ $n -eq ${MAX_SAMPLE_STREAM_IDX} ]; then
        n=0
    else
        let "n = $n + 1"
    fi
    if [ $p -eq ${MAX_SAMPLE_STREAM_IDX} ]; then
        p=0
    else
        let "p = $p + 1"
    fi
done

echo
echo "Stripping test comments from configuration files"
${DEST}/tools/av1src/configurations/strip-test-comments.sh \
  "${REL_DIR}/configurations"

echo
echo "Setting up windows build"
WIN_BASE="win-build-v${VERSION}"
WIN_DIR="${DEST}/${WIN_BASE}"

mkdir -p "${WIN_DIR}"
cp "${DEST}/version" "${WIN_DIR}"
cp -r "${DEST}/tools" "${WIN_DIR}"
cat >"${WIN_DIR}/cygwin-build.sh" <<EOF
#!/bin/bash
J=4

echo "Building with \$J threads (edit this script if that's wrong)"

set -e

MAKE_ARGS=()
MAKE_ARGS+=("-j\$J")
MAKE_ARGS+=("CHECK_COMPILER_VERSION=0")
MAKE_ARGS+=("CXX=x86_64-w64-mingw32-g++")
MAKE_ARGS+=("CFLAGS=-lm -D__USE_MINGW_ANSI_STDIO=1 -D__STDC_FORMAT_MACROS=1")
MAKE_ARGS+=("LDFLAGS=-static")
MAKE_ARGS+=("RELEASE=1")
MAKE_ARGS+=("GIT_VERSION=${VERSION}")

echo "Building av1enc"
set -x
make -C tools TARGET=17 "\${MAKE_ARGS[@]}" PROFILE=configurable \
     USE_MYRAND=1 c-build
{ set +x; } 2>/dev/null

echo
echo "Building av1bdec"
set -x
make -C tools TARGET=20 "\${MAKE_ARGS[@]}" c-build
{ set +x; } 2>/dev/null

echo
echo "Stripping binaries"
strip tools/build/av1enc/av1.gen.exe
strip tools/build/av1bdec/av1b.gen.exe
EOF

echo
echo "Stripping binaries"
strip "${REL_DIR}/bin/linux/"*

# At this point, we've done everything we can. Tar up the stuff we
# care about (handy in general, but particularly handy for a Jenkins
# job, which can grab it).
#
# We've generated a "proto-release" in ${DEST}/release-v${VERSION}.
# This has everything but the stuff to build windows binaries.
echo
echo "Tarring up the release binary"
(cd "${DEST}"; tar -Jcf "${REL_BASE}.tar.xz" "${REL_BASE}")

# We can also tar up the Windows stuff, which is in
# ${DEST}/win-build-v${VERSION}.
echo
echo "Tarring up files to build windows binaries"
(cd "${DEST}"; tar -Jcf "${WIN_BASE}.tar.xz" "${WIN_BASE}")

# And the docs, from ${DEST}/html
echo
echo "Tarring up documentation files"
(cd "${DEST}"; tar -Jcf "${DOC_BASE}.tar.xz" "${DOC_BASE}")

echo
echo "Done."
echo "  Config files and Linux binaries are in ${REL_DIR}.tar.xz"
echo "  Files to build on Windows are in ${WIN_DIR}.tar.xz"
echo "  HTML snippets for the spec are in ${DOC_DIR}.tar.xz"
echo "  Logs are in ${DEST}/logs"
