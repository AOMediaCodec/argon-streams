################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

#!/usr/bin/env python3

import cvxpy, itertools, collections

def Round2(x, bits):
  r = (1 << bits) >> 1
  return (x + r) >> bits

constraints = None
intermediates = None
feasible = None

intermediate_scale = None

def range_check(v, r, i):
    global feasible

    lo = - ( 1 << ( r - 1 ) )
    hi = ( 1 << ( r - 1 ) ) - 1

    if isinstance(v, int):
        intermediates[i] = v
        # This value depends only on fixed inputs. In this case,
        # we can check the constraint now - if it's not satisfied,
        # then the whole problem can be skipped
        feasible = feasible and lo <= v <= hi
    else:
        # Scale the constraint equations and objectives
        # to help make the problem better conditioned.
        # This leads to more of the sub-problems being solved exactly,
        # rather than returning "optimal_inaccurate" or "infeasible_inaccurate".
        scale_bits = intermediate_scale[i]
        scale = float(1 << scale_bits)
        v2 = v / scale
        lo /= scale
        hi /= scale
        intermediates[i] = v2
        c1 = (v2 >= lo)
        c2 = (v2 <= hi)
        constraints.append(c1)
        constraints.append(c2)

    return v

def iadst4_check(v, r, i):
    return range_check(v, r+12, i)

SINPI_1_9 = 1321
SINPI_2_9 = 2482
SINPI_3_9 = 3344
SINPI_4_9 = 3803

NUM_INTERMEDIATES = 18

def inverse_adst4( T, r ):
    s0 = iadst4_check( SINPI_1_9 * T[ 0 ], r, 0 )
    s1 = iadst4_check( SINPI_2_9 * T[ 0 ], r, 1 )
    s2 = iadst4_check( SINPI_3_9 * T[ 1 ], r, 2 )
    s3 = iadst4_check( SINPI_4_9 * T[ 2 ], r, 3 )
    s4 = iadst4_check( SINPI_1_9 * T[ 2 ], r, 4 )
    s5 = iadst4_check( SINPI_2_9 * T[ 3 ], r, 5 )
    s6 = iadst4_check( SINPI_4_9 * T[ 3 ], r, 6 )

    a7 = range_check(T[ 0 ] - T[ 2 ], r+1, 7)
    b7 = range_check(a7 + T[ 3 ], r, 8)

    s0 = iadst4_check( s0 + s3, r, 9 )
    s1 = iadst4_check( s1 - s4, r, 10 )
    s3 = s2
    s2 = iadst4_check( SINPI_3_9 * b7 , r, 11 )

    s0 = iadst4_check( s0 + s5, r, 12 )
    s1 = iadst4_check( s1 - s6, r, 13 )

    x0 = iadst4_check( s0 + s3, r, 14 )
    x1 = iadst4_check( s1 + s3, r, 15 )
    x2 = s2
    x3 = iadst4_check( s0 + s1, r, 16 )

    x3 = iadst4_check( x3 - s3, r, 17 )

    #res = [0] * 4
    #res[ 0 ] = Round2( x0, 12 )
    #res[ 1 ] = Round2( x1, 12 )
    #res[ 2 ] = Round2( x2, 12 )
    #res[ 3 ] = Round2( x3, 12 )
    #return res

Constant = collections.namedtuple("Constant", ["value"])

def test(bd):
    global constraints, intermediates, feasible, intermediate_scale
    r = bd + 8

    dqv = 4
    lo = -(1 << (bd+7))
    hi = (1 << (bd+7))-1
    # Calculate the smallest quantized values which will be clamped to lo or hi respectively
    loQ = lo // dqv
    hiQ = (hi + (dqv-1)) // dqv

    minima = [(0, [-1, -1, -1, -1])] * NUM_INTERMEDIATES
    maxima = [(0, [-1, -1, -1, -1])] * NUM_INTERMEDIATES

    # Note: The constraints we generate end up being quite ill-conditioned,
    # I think because they have widely varying scales.
    # This causes the solver to sometimes return an "inaccurate" status, ie.
    # it's not sure that the provided result is correct.
    # To minimize the impact of this, we run each query multiple times, with
    # different scalings of the problematic constraints.
    # This is suboptimal, as it means the code takes a long time to run, but seems
    # to give decent results.
    # The only things it misses are:
    # * The minima for intermediates 3 and 4, where the obvious input block of
    #   {0, 0, -32768, 0} causes an overflow in intermediate 7. Instead, we can use
    #   the block { -1, 0, -32768, 0}, but the solver doesn't find this
    # * Sometimes, when the range we get for some intermediate isn't symmetrical,
    #   we can widen it to a symmetrical range by replacing one input by the negation
    #   of the other input. But note that this doesn't always work, since some
    #   of the constraints really are asymmetrical.
    for s in range(4, -1, -1):
        intermediate_scale = [s, s, s, s, s, s, s, 0, 0, s, s, s, s, s, s, s, s, s]
        # cvxpy can't directly handle clamp() type operations on
        # its inputs. So we explicitly split out the min and max
        # values for each variable.
        for combination in itertools.product([0, -1, +1], repeat=4):
            constraints = []
            q = []
            T = []

            # Try a block consisting of all min, max, or 0 values first,
            # before handing over to the full constraint solver
            # This is just to make the generated solutions a little nicer -
            # if a variable isn't needed, it'll be set to 0 rather than some
            # random value
            for i in range(4):
                if combination[i] == -1:
                    q.append(Constant(loQ))
                    T.append(lo)
                elif combination[i] == 1:
                    q.append(Constant(hiQ))
                    T.append(hi)
                else:
                    q.append(Constant(0))
                    T.append(0)

            feasible = True
            intermediates = {}
            inverse_adst4(T, r)

            if feasible:
                inputs = list(map(lambda x: x.value, q))
                for (i, v) in intermediates.items():
                    if v < minima[i][0]:
                        minima[i] = (v, inputs)
                    if v > maxima[i][0]:
                        maxima[i] = (v, inputs)

            # Now try with full constraint solving, if necessary
            needSolver = False
            for i in range(4):
                if combination[i] == 0:
                    needSolver = True
            if not needSolver:
                #print("Combination %r does not need solver" % (combination,))
                continue

            # Reset state
            constraints = []
            q = []
            T = []
            for i in range(4):
                if combination[i] == -1:
                    q.append(Constant(loQ))
                    T.append(lo)
                elif combination[i] == 1:
                    q.append(Constant(hiQ))
                    T.append(hi)
                else:
                    v = cvxpy.Variable(integer=True)
                    constraints.append(v >= loQ + 1)
                    constraints.append(v <= hiQ - 1)
                    q.append(v)
                    T.append(v * dqv)

            # Figure out the set of intermediate values to optimize,
            # and all the relevant constraints
            feasible = True
            intermediates = {}
            inverse_adst4(T, r)

            if not feasible:
                # This combination cannot give any valid transform blocks,
                # so skip it
                #print("Combination %r is infeasible" % (combination,))
                continue

            # Find the minimum and maximum value of each intermediate
            # subject to these conditions.
            # Note that some of the problems generated will be infeasible.
            # If this happens, we need to detect that and not update *any* ranges,
            # even ones which are purely integer valued.
            for (i, v) in intermediates.items():
                scale_bits = intermediate_scale[i]
                scale = float(1 << scale_bits)

                # This value depends on at least some of the variable
                # inputs, so find the extremal values and corresponding
                # input blocks
                prob = cvxpy.Problem(cvxpy.Minimize(v), constraints)
                value = prob.solve()
                if value is not None:
                    value *= scale
                #print(i, "min", prob.status)
                if prob.status == cvxpy.OPTIMAL:
                    inputs = list(map(lambda x: int(round(float(x.value))), q))
                    valid = True
                    for j in range(4):
                        valid = valid and (loQ + 1 <= inputs[j] <= hiQ - 1)
                    if valid and value < minima[i][0]:
                        minima[i] = (int(round(value)), inputs)

                prob = cvxpy.Problem(cvxpy.Maximize(v), constraints)
                value = prob.solve(warm_start=True)
                if value is not None:
                    value *= scale
                #print(i, "max", prob.status)
                if prob.status == cvxpy.OPTIMAL:
                    inputs = list(map(lambda x: int(round(float(x.value))), q))
                    valid = True
                    for j in range(4):
                        valid = valid and (loQ + 1 <= inputs[j] <= hiQ - 1)
                    if valid and value > maxima[i][0]:
                        maxima[i] = (int(round(value)), inputs)

            #print("Done combination %r" % (combination,))
        print("Done bd=%d, s=%d" % (bd, s))

    print("Bit depth = %d:" % bd)
    for i in range(NUM_INTERMEDIATES):
        params = tuple([i, minima[i][0]] + minima[i][1] + [maxima[i][0]] + maxima[i][1])
        print("  Intermediate %2d: min = %11d at input = [%7d, %7d, %7d, %7d], max = %10d at input = [%7d, %7d, %7d, %7d]" % params)
    print()

if __name__ == "__main__":
    test(10)
    test(12)
