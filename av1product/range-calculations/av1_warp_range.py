################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# Script to print the minimum values of 'det' in find_projection() / warp_estimation().
# The idea here is that the minimum value of 'det' must be achievable with
# a *single* warp sample. Roughly speaking, this is because:
# * A is a sum of contributions from each sample
# * The matrices used have the properties that det(A) >= 0 and
#   det(A + B) >= det(A) + det(B)
#
# Thus we need to look for the smallest determinant achievable with a single sample.
# Because A only depends on the relative displacement (sx, sy) between the block centers
# (and is independent of the attached motion vectors!), that means we need to work out
# how close the center of a sample block can be to the center of the current block.
#
# We're only allowed to use warped motion for 8x8 and larger blocks, so it's
# clear that the minimum is going to be one of the following cases:
#
# 8x8 block with an 8x4 block above -> the displacement is (0, -6) pixels
# 8x8 block with a 4x4 blocks above-left or above-right -> the displacement is
#                                                          (+-2, -6) pixels
#
# The tranposed situations (ie, 8x8 with a 4x8 at the left, or a 4x4s at the
# bottom-left or top-left) will give the same determinants, but we also run
# those here just to be sure.
#
# Since the values of 'sx' and 'sy' are measured in units of 1/8 of a pixel,
# these cases correspond to the offset vectors:
# (0, -48), (+-16, -48) and the same with the two components swapped around.
#
# The minimum turns out to be when the displacement is (0, -48) or (-48, 0),
# with a determinant of 1968.



# Copy enough of the warp estimation process to calculate 'det' for a single sample
LS_STEP = 8
LS_MAT_DOWN_BITS = 2

def LS_SQUARE(x):
  return ((x * x * 4) + (x * 4 * LS_STEP) + (LS_STEP * LS_STEP * 2)) >> (2 + LS_MAT_DOWN_BITS)

def LS_PRODUCT1(x, y):
  return ((x * y * 4) + ((x + y) * 2 * LS_STEP) + (LS_STEP * LS_STEP)) >> (2 + LS_MAT_DOWN_BITS)

def LS_PRODUCT2(x, y):
  return ((x * y * 4) + ((x + y) * 2 * LS_STEP) + (LS_STEP * LS_STEP * 2)) >> (2 + LS_MAT_DOWN_BITS)

def calculate(sx, sy):
  A = [[0, 0], [0, 0]]
  A[0][0] = LS_SQUARE(sx)
  A[0][1] = LS_PRODUCT1(sx, sy)
  A[1][0] = A[0][1]
  A[1][1] = LS_SQUARE(sy)
  det = A[0][0] * A[1][1] - A[0][1] * A[1][0]
  print "(sx=%3d, sy=%3d) -> A=[[%3d, %4d], [%4d, %3d]], det=%4d" % \
        (sx, sy, A[0][0], A[0][1], A[1][0], A[1][1], det)

# Calculate the determinant for each case of interest
if __name__ == "__main__":
  calculate(-48, 0)
  calculate(0, -48)
  calculate(-48, -16)
  calculate(-48, +16)
  calculate(-16, -48)
  calculate(+16, -48)
