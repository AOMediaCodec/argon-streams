################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import math

param_sets = [
  (2, 140, 1, 3236), (2, 112, 1, 2158), (2, 93, 1, 1618), (2, 80, 1, 1438),
  (2,  70, 1, 1295), (2,  58, 1, 1177), (2, 47, 1, 1079), (2, 37, 1,  996),
  (2,  30, 1,  925), (2,  25, 1,  863), (0,  0, 1, 2589), (0,  0, 1, 1618),
  (0,   0, 1, 1177), (0,   0, 1,  925), (2, 56, 0,    0), (2, 22, 0,    0)
]

def Round2(x, bits):
  r = (1 << bits) >> 1
  return (x + r) >> bits

# Calculate the narrowest signed integer type which contains 'x'
def bits(x):
  if x == 0: return 0
  offset = 1 if x > 0 else 0 # Range of an iN is -2^(N-1) to +(2^(N-1) - 1)
  return int(math.ceil(math.log(abs(x)+offset, 2))) + 1

# Given a 7x7 window of pixels, calculate the values of
# f0, f1, and f2 at the center of the window
# The parameter 'row' determines whether we act as if
# the center of the window is on an odd pixel row or an even pixel row
def selfguided_filter_center(r, s, row, window, bd):
  if r == 0:
    return window[3][3] << 4

  A = {}
  B = {}
  n = (2*r+1)**2
  one_over_n = ((1 << 12) + (n//2)) // n
  # Iterate over the 3x3 entries of A and B we'll need.
  # We identify these entries by the location of their central pixel
  for cy in (2, 3, 4):
    for cx in (2, 3, 4):
      idx = (cy, cx)
      sum_ = 0
      sum_sq = 0
      for k in range(-r, r+1):
        for l in range(-r, r+1):
          px = window[cy+k][cx+l]
          sum_ += px
          sum_sq += px * px
      u = Round2(sum_sq, 2 * (bd - 8))
      v = Round2(sum_, bd - 8)
      if u*n < v*v:
        q = 0
      else:
        q = u*n - v*v
      z = Round2(q * s, 20)

      if z >= 255:
        A[idx] = 256
      elif z == 0:
        # Special case: Saturate to A[idx] >= 1, so that:
        # i) (256 - A[idx]) fits in a uint8, for hardware implementations
        # ii) The calculation of B[idx] below wont't overflow
        A[idx] = 1
      else:
        A[idx] = ((z << 8) + (z//2)) // (z + 1)

      localMean = sum_ * one_over_n
      blendFactor = 256 - A[idx]
      B[idx] = Round2(localMean * blendFactor, 12)

  # Finally, calculate the filter output for the center pixel
  if r == 2:
    # Because we only calculated the odd rows of A and B, the even
    # and odd output rows need different calculations
    if not (row & 1):
      shift = 5
      u = (6 * (A[(2, 3)] + A[(4, 3)]) +
                5 * (A[(2, 2)] + A[(2, 4)] + A[(4, 2)] + A[(4, 4)]))
      v = (6 * (B[(2, 3)] + B[(4, 3)]) +
                5 * (B[(2, 2)] + B[(2, 4)] + B[(4, 2)] + B[(4, 4)]))
    else:
      shift = 4
      u = (6 * A[(3, 3)] + 5 * (A[(3, 2)] + A[(3, 4)]))
      v = (6 * B[(3, 3)] + 5 * (B[(3, 2)] + B[(3, 4)]))
  else:
    shift = 5
    u = (4*(A[(3, 3)] + A[(3, 2)] + A[(3, 4)] + A[(2, 3)] + A[(4, 3)]) +
            3*(A[(2, 2)] + A[(2, 4)] + A[(4, 2)] + A[(4, 4)]))
    v = (4*(B[(3, 3)] + B[(3, 2)] + B[(3, 4)] + B[(2, 3)] + B[(4, 3)]) +
            3*(B[(2, 2)] + B[(2, 4)] + B[(4, 2)] + B[(4, 4)]))

  px = u * window[3][3] + v
  return Round2(px, 4 + shift)

# Given a particular 7x7 window, figure out the possible range of outputs
# across both even and odd rows and all parameter sets
def sgr_window(window, bd):
  vMin = 10**9
  vMax = 0

  pMax = (1<<bd) - 1
  for row in (0, 1):
    for i in range(16):
      r0, s0, r1, s1 = param_sets[i]
      f0 = window[3][3] << 4
      f1 = selfguided_filter_center(r0, s0, row, window, bd)
      f2 = selfguided_filter_center(r1, s1, row, window, bd)

      # Look at the four most extreme possibilities for xqd
      v0 = -32 * f0 - 96 * f1 + 256 * f2
      v1 = -32 * f0 + 31 * f1 + 129 * f2
      v2 =  95 * f0 - 96 * f1 + 129 * f2
      v3 =  95 * f0 + 31 * f1 +   2 * f2

      vMin = min(vMin, v0, v1, v2, v3)
      vMax = max(vMax, v0, v1, v2, v3)

  return (vMin, vMax)

# Estimate the possible range for a particular bit depth
def ranges(bd):
  trueMin = 10**9
  trueMax = 0
  minX = 0
  maxX = 0

  pMax = (1<<bd) - 1
  print "Bit depth = %d:" % bd

  for x in range(0, pMax+1):
    # Try a window which aims to minimize 'v'
    # (all max-value pixels except for the central one)
    window = [[x for j in range(7)] for i in range(7)]
    window[3][3] = 0
    vMin, vMax = sgr_window(window, bd)
    if vMin < trueMin:
      trueMin = vMin
      minX = x

    # Try a window which aims to maximize 'v'
    window = [[x for j in range(7)] for i in range(7)]
    window[3][3] = pMax
    vMin, vMax = sgr_window(window, bd)
    if vMax > trueMax:
      trueMax = vMax
      maxX = x

  print "  Estimated min value of 'v' = %d (i%d), at x = %d" % (trueMin, bits(trueMin), minX)
  print "  Estimated max value of 'v' = %d (i%d), at x = %d" % (trueMax, bits(trueMax), maxX)

if __name__ == "__main__":
  ranges(8)
  ranges(10)
  ranges(12)
