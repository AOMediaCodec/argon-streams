################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import itertools

def Round2(x, bits):
  r = (1 << bits) >> 1
  return (x + r) >> bits

# Work out the values of p and z we get when
# exactly k of the pixels have value x, (n//2 - k) have the
# value maxPx, and the rest have value 0.
def evaluate(r, bd, block):
  maxPx = (1<<bd) - 1
  n = (2*r+1) ** 2
  a = sum(map(lambda x: x*x, block))
  b = sum(block)
  a = Round2(a, 2*(bd-8))
  b = Round2(b, bd-8)
  p = a*n - b*b
  s = (3236 if r == 1 else 140)
  z = Round2(p*s, 20)
  return (a, b, p, s, z)

def ranges(r, bd):
  assert r == 1 or r == 2
  maxPx = (1<<bd) - 1
  n = (2*r+1) ** 2

  pMax = 0
  pMaxK = 0
  pMaxBlock = 0
  zMax = 0
  zMaxK = 0
  zMaxBlock = 0
  # Since n/2 is not exactly an integer, try pixel blocks where
  # floor(n/2) pixels are nonzero *and* blocks where ceil(n/2) pixels
  # are nonzero
  for k in (n//2, n//2 + 1):
    # Within each value of 'k', try a variety of blocks where each
    # pixel is allowed to vary by up to 1 place from its maximum or
    # minimum value
    # Note that the order of pixels within a block does not matter
    for l in range(k):
      for m in range(n-k):
        block = [maxPx-1] * l + [maxPx] * (k-l) + [1] * m + [0] * (n-m-k)
        assert len(block) == n

        (_, _, p, _, z) = evaluate(r, bd, block)
        if p > pMax:
          pMax = p
          pMaxBlock = block
        if z > zMax:
          zMax = z
          zMaxBlock = block

  block = pMaxBlock
  (a, b, p, s, z) = evaluate(r, bd, block)
  print "  Max p occurs at a=%d b=%d p=%d s=%d z=%d" % (a, b, p, s, z)
  print "    with block=%r" % block
  block = zMaxBlock
  (a, b, p, s, z) = evaluate(r, bd, block)
  print "  Max z occurs at a=%d b=%d p=%d s=%d z=%d" % (a, b, p, s, z)
  print "    with block=%r" % block
  print

for bd in (10, 12):
  for r in (1, 2):
    print "bd = %d, r = %d:" % (bd, r)
    ranges(r, bd)
