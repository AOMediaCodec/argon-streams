;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Project P8005 HEVC
; (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
; All rights reserved.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (set-logic QF_ABV)
;; (set-option :produce-models true)
;; (define-fun bit-depth () (_ BitVec 40) (_ bv10 40))
;;
;; (define-fun zero () (_ BitVec 40) (_ bv0 40))
;; (define-fun one () (_ BitVec 40) (_ bv1 40))
;; (define-fun two () (_ BitVec 40) (_ bv2 40))

;; (0) Declare the input rows
(declare-const t00 (_ BitVec 38))
(declare-const t01 (_ BitVec 38))
(declare-const t02 (_ BitVec 38))
(declare-const t03 (_ BitVec 38))
(declare-const t10 (_ BitVec 38))
(declare-const t11 (_ BitVec 38))
(declare-const t12 (_ BitVec 38))
(declare-const t13 (_ BitVec 38))
(declare-const t20 (_ BitVec 38))
(declare-const t21 (_ BitVec 38))
(declare-const t22 (_ BitVec 38))
(declare-const t23 (_ BitVec 38))
(declare-const t30 (_ BitVec 38))
(declare-const t31 (_ BitVec 38))
(declare-const t32 (_ BitVec 38))
(declare-const t33 (_ BitVec 38))

;; (1) Check that the input rows are in range and that they extend to
;; the input_x rows.
(define-fun row-clamp-range () (_ BitVec 40)
  (bvadd bit-depth (_ bv8 40)))

(define-fun signed-range-max ((bits (_ BitVec 40))) (_ BitVec 40)
  (bvsub (bvshl one (bvsub bits one)) one))

(define-fun signed-range-min ((bits (_ BitVec 40))) (_ BitVec 40)
  (bvneg (bvshl one (bvsub bits one))))

(define-fun clamp ((x (_ BitVec 40)) (bits (_ BitVec 40))) (_ BitVec 40)
  (let ((min (signed-range-min bits))
        (max (signed-range-max bits)))
    (ite (bvsle x min)
         min
         (ite (bvsge x max) max x))))

(define-fun in-coeff-range ((x (_ BitVec 40))) Bool
  (let ((min (signed-range-min (bvadd (_ bv8 40) bit-depth)))
        (max (signed-range-max (bvadd (_ bv8 40) bit-depth))))
    (and (bvsle min x) (bvsle x max))))

(assert (in-coeff-range ((_ sign_extend 2) t00)))
(assert (in-coeff-range ((_ sign_extend 2) t01)))
(assert (in-coeff-range ((_ sign_extend 2) t02)))
(assert (in-coeff-range ((_ sign_extend 2) t03)))
(assert (in-coeff-range ((_ sign_extend 2) t10)))
(assert (in-coeff-range ((_ sign_extend 2) t11)))
(assert (in-coeff-range ((_ sign_extend 2) t12)))
(assert (in-coeff-range ((_ sign_extend 2) t13)))
(assert (in-coeff-range ((_ sign_extend 2) t20)))
(assert (in-coeff-range ((_ sign_extend 2) t21)))
(assert (in-coeff-range ((_ sign_extend 2) t22)))
(assert (in-coeff-range ((_ sign_extend 2) t23)))
(assert (in-coeff-range ((_ sign_extend 2) t30)))
(assert (in-coeff-range ((_ sign_extend 2) t31)))
(assert (in-coeff-range ((_ sign_extend 2) t32)))
(assert (in-coeff-range ((_ sign_extend 2) t33)))

;; (2) Apply row transforms. The iadst function below represents the
;;     whole IADST4 process.
(define-fun sin-pi-1-9 () (_ BitVec 40) (_ bv1321 40))
(define-fun sin-pi-2-9 () (_ BitVec 40) (_ bv2482 40))
(define-fun sin-pi-3-9 () (_ BitVec 40) (_ bv3344 40))
(define-fun sin-pi-4-9 () (_ BitVec 40) (_ bv3803 40))

(define-fun num-representable ((num-bits (_ BitVec 40)) (x (_ BitVec 40)))
  Bool
  ;; The number is representable if either (x >> (num-bits - 1)) == 0
  ;; or (for negative numbers) ((~x) >> (num-bits - 1)) == 0.
  (or (= (bvlshr x (bvsub num-bits one)) zero)
      (= (bvlshr (bvnot x) (bvsub num-bits one)) zero)))

(define-fun rep12 ((x (_ BitVec 40)) (r (_ BitVec 40)))
  Bool
  (num-representable (bvadd r (_ bv12 40)) x))

(define-fun round12 ((x (_ BitVec 40)) (r (_ BitVec 40)))
  (_ BitVec 40)
  (bvashr (bvadd x (_ bv2048 40)) (_ bv12 40)))

(define-fun iadst4 ((t0 (_ BitVec 40)) (t1 (_ BitVec 40))
                    (t2 (_ BitVec 40)) (t3 (_ BitVec 40))

                    (u0 (_ BitVec 40)) (u1 (_ BitVec 40))
                    (u2 (_ BitVec 40)) (u3 (_ BitVec 40))

                    (r (_ BitVec 40)))
  Bool
  (let ((s0 (bvmul sin-pi-1-9 (clamp t0 r)))
        (s1 (bvmul sin-pi-2-9 (clamp t0 r)))
        (s2 (bvmul sin-pi-3-9 (clamp t1 r)))
        (s3 (bvmul sin-pi-4-9 (clamp t2 r)))
        (s4 (bvmul sin-pi-1-9 (clamp t2 r)))
        (s5 (bvmul sin-pi-2-9 (clamp t3 r)))
        (s6 (bvmul sin-pi-4-9 (clamp t3 r)))
        (a7 (bvsub t0 (clamp t2 r))))
    (let ((b7 (bvadd a7 (clamp t3 r))))
      (let ((s0_1 (bvadd s0 s3))
            (s1_1 (bvsub s1 s4))
            (s2_1 (bvmul sin-pi-3-9 b7))
            (s3_1 s2)
            (s4_1 s4)
            (s5_1 s5)
            (s6_1 s6))
        (let ((s0_2 (bvadd s0_1 s5_1))
              (s1_2 (bvsub s1_1 s6_1))
              (s2_2 s2_1)
              (s3_2 s3_1)
              (s4_2 s4_1)
              (s5_2 s5_1)
              (s6_2 s6_1))
          (let ((x0 (bvadd s0_2 s3_2))
                (x1 (bvadd s1_2 s3_2))
                (x2 s2_2)
                (x3 (bvadd s0_2 s1_2)))
            (let ((x0_1 x0)
                  (x1_1 x1)
                  (x2_1 x2)
                  (x3_1 (bvsub x3 s3_2)))
              (and
               ;; The first "and" checks that every intermediate value
               ;; is in range.
               (and (rep12 s0 r) (rep12 s1 r) (rep12 s2 r) (rep12 s3 r)
                    (rep12 s4 r) (rep12 s5 r) (rep12 s6 r)
                    (num-representable (bvadd r (_ bv1 40)) a7)
                    (num-representable r b7)
                    (rep12 s0_1 r) (rep12 s1_1 r) (rep12 s2_1 r)
                    (rep12 s0_2 r) (rep12 s1_2 r)
                    (rep12 x0 r) (rep12 x1 r) (rep12 x2 r) (rep12 x3 r)
                    (rep12 x3_1 r))
               ;; The second "and" checks that the output is the
               ;; transformed input.
               (and (= u0 (round12 x0_1 r))
                    (= u1 (round12 x1_1 r))
                    (= u2 (round12 x2_1 r))
                    (= u3 (round12 x3_1 r)))))))))))


(declare-const tx_00 (_ BitVec 40))
(declare-const tx_01 (_ BitVec 40))
(declare-const tx_02 (_ BitVec 40))
(declare-const tx_03 (_ BitVec 40))
(declare-const tx_10 (_ BitVec 40))
(declare-const tx_11 (_ BitVec 40))
(declare-const tx_12 (_ BitVec 40))
(declare-const tx_13 (_ BitVec 40))
(declare-const tx_20 (_ BitVec 40))
(declare-const tx_21 (_ BitVec 40))
(declare-const tx_22 (_ BitVec 40))
(declare-const tx_23 (_ BitVec 40))
(declare-const tx_30 (_ BitVec 40))
(declare-const tx_31 (_ BitVec 40))
(declare-const tx_32 (_ BitVec 40))
(declare-const tx_33 (_ BitVec 40))

(define-fun ext-input ((t (_ BitVec 38)))
  (_ BitVec 40)
  (concat t (_ bv0 2)))

(assert (iadst4 (ext-input t00) (ext-input t01)
                (ext-input t02) (ext-input t03)
                tx_00 tx_01 tx_02 tx_03
                row-clamp-range))
(assert (iadst4 (ext-input t10) (ext-input t11)
                (ext-input t12) (ext-input t13)
                tx_10 tx_11 tx_12 tx_13
                row-clamp-range))
(assert (iadst4 (ext-input t20) (ext-input t21)
                (ext-input t22) (ext-input t23)
                tx_20 tx_21 tx_22 tx_23
                row-clamp-range))
(assert (iadst4 (ext-input t30) (ext-input t31)
                (ext-input t32) (ext-input t33)
                tx_30 tx_31 tx_32 tx_33
                row-clamp-range))

;; (4) Now we pretty much copy & paste the definition of iadst4 from
;;     above to define a version that exposes the internal ranges.
(define-fun iadst4-ext ((t0 (_ BitVec 40)) (t1 (_ BitVec 40))
                        (t2 (_ BitVec 40)) (t3 (_ BitVec 40))

                        (r (_ BitVec 40))

                        (s0 (_ BitVec 40)) (s1 (_ BitVec 40))
                        (s2 (_ BitVec 40)) (s3 (_ BitVec 40))
                        (s4 (_ BitVec 40)) (s5 (_ BitVec 40))
                        (s6 (_ BitVec 40))

                        (a7 (_ BitVec 40)) (b7 (_ BitVec 40))

                        (s0_1 (_ BitVec 40)) (s1_1 (_ BitVec 40))
                        (s2_1 (_ BitVec 40))

                        (s0_2 (_ BitVec 40)) (s1_2 (_ BitVec 40))

                        (x0 (_ BitVec 40)) (x1 (_ BitVec 40))
                        (x3 (_ BitVec 40))

                        (x3_1 (_ BitVec 40)))
  Bool
  (and (and
        (= s0 (bvmul sin-pi-1-9 (clamp t0 r)))
        (= s1 (bvmul sin-pi-2-9 (clamp t0 r)))
        (= s2 (bvmul sin-pi-3-9 (clamp t1 r)))
        (= s3 (bvmul sin-pi-4-9 (clamp t2 r)))
        (= s4 (bvmul sin-pi-1-9 (clamp t2 r)))
        (= s5 (bvmul sin-pi-2-9 (clamp t3 r)))
        (= s6 (bvmul sin-pi-4-9 (clamp t3 r))))
       (and
        (= a7 (bvsub t0 (clamp t2 r)))
        (= b7 (bvadd a7 (clamp t3 r))))
       (and
        (= s0_1 (bvadd s0 s3))
        (= s1_1 (bvsub s1 s4))
        (= s2_1 (bvmul sin-pi-3-9 b7))
        (let ((s3_1 s2) (s4_1 s4) (s5_1 s5) (s6_1 s6))
          (and
           (= s0_2 (bvadd s0_1 s5_1))
           (= s1_2 (bvsub s1_1 s6_1))
           (let ((s2_2 s2_1) (s3_2 s3_1) (s4_2 s4_1) (s5_2 s5_1) (s6_2 s6_1))
             (and
              (= x0 (bvadd s0_2 s3_2))
              (= x1 (bvadd s1_2 s3_2))
              (= x3 (bvadd s0_2 s1_2))
              (= x3_1 (bvsub x3 s3_2))
              (let ((x2 s2_2))
                (and (rep12 s0 r) (rep12 s1 r) (rep12 s2 r) (rep12 s3 r)
                     (rep12 s4 r) (rep12 s5 r) (rep12 s6 r)
                     (num-representable (bvadd r (_ bv1 40)) a7)
                     (num-representable r b7)
                     (rep12 s0_1 r) (rep12 s1_1 r) (rep12 s2_1 r)
                     (rep12 s0_2 r) (rep12 s1_2 r)
                     (rep12 x0 r) (rep12 x1 r) (rep12 x2 r) (rep12 x3 r)
                     (rep12 x3_1 r))))))))))

;; (5) I think we can hit all the ranges by just looking at col0.
(define-fun col-clamp-range () (_ BitVec 40)
  (let ((col-clamp-range (bvadd bit-depth (_ bv6 40))))
    (ite (bvsle col-clamp-range (_ bv16 40))
         (_ bv16 40)
         col-clamp-range)))

(declare-const s0 (_ BitVec 40))
(declare-const s1 (_ BitVec 40))
(declare-const s2 (_ BitVec 40))
(declare-const s3 (_ BitVec 40))
(declare-const s4 (_ BitVec 40))
(declare-const s5 (_ BitVec 40))
(declare-const s6 (_ BitVec 40))
(declare-const a7 (_ BitVec 40))
(declare-const b7 (_ BitVec 40))
(declare-const s0_1 (_ BitVec 40))
(declare-const s1_1 (_ BitVec 40))
(declare-const s2_1 (_ BitVec 40))
(declare-const s0_2 (_ BitVec 40))
(declare-const s1_2 (_ BitVec 40))
(declare-const x0 (_ BitVec 40))
(declare-const x1 (_ BitVec 40))
(declare-const x3 (_ BitVec 40))
(declare-const x3_1 (_ BitVec 40))

(assert (iadst4-ext tx_00 tx_10 tx_20 tx_30
                    col-clamp-range
                    s0 s1 s2 s3 s4 s5 s6
                    a7 b7
                    s0_1 s1_1 s2_1
                    s0_2 s1_2
                    x0 x1 x3 x3_1))

;; (6) But we *do* have to check that col1..col3 don't overflow
(declare-const xx_10 (_ BitVec 40))
(declare-const xx_11 (_ BitVec 40))
(declare-const xx_12 (_ BitVec 40))
(declare-const xx_13 (_ BitVec 40))
(declare-const xx_20 (_ BitVec 40))
(declare-const xx_21 (_ BitVec 40))
(declare-const xx_22 (_ BitVec 40))
(declare-const xx_23 (_ BitVec 40))
(declare-const xx_30 (_ BitVec 40))
(declare-const xx_31 (_ BitVec 40))
(declare-const xx_32 (_ BitVec 40))
(declare-const xx_33 (_ BitVec 40))

(assert (iadst4 tx_01 tx_11 tx_21 tx_31
                xx_10 xx_11 xx_12 xx_13
                col-clamp-range))
(assert (iadst4 tx_02 tx_12 tx_22 tx_32
                xx_20 xx_21 xx_22 xx_23
                col-clamp-range))
(assert (iadst4 tx_03 tx_13 tx_23 tx_33
                xx_30 xx_31 xx_32 xx_33
                col-clamp-range))


;; (assert (bvsge s2_1 (_ bv1 40)))
;; (check-sat)

;; Local Variables:
;; mode: lisp
;; End:
