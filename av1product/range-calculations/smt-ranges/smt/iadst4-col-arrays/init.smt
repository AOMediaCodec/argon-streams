;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Project P8005 HEVC
; (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
; All rights reserved.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; (set-logic QF_ABV)
;; (define-fun bit-depth () (_ BitVec 40) (_ bv10 40))
;;
;; (define-fun zero () (_ BitVec 40) (_ bv0 40))
;; (define-fun one () (_ BitVec 40) (_ bv1 40))
;; (define-fun two () (_ BitVec 40) (_ bv2 40))

;; (0) Declare the input rows
(declare-const input0 (Array (_ BitVec 2) (_ BitVec 38)))
(declare-const input1 (Array (_ BitVec 2) (_ BitVec 38)))
(declare-const input2 (Array (_ BitVec 2) (_ BitVec 38)))
(declare-const input3 (Array (_ BitVec 2) (_ BitVec 38)))
(declare-const t00 (_ BitVec 38)) (assert (= (select input0 (_ bv0 2)) t00))
(declare-const t01 (_ BitVec 38)) (assert (= (select input0 (_ bv1 2)) t01))
(declare-const t02 (_ BitVec 38)) (assert (= (select input0 (_ bv2 2)) t02))
(declare-const t03 (_ BitVec 38)) (assert (= (select input0 (_ bv3 2)) t03))
(declare-const t10 (_ BitVec 38)) (assert (= (select input1 (_ bv0 2)) t10))
(declare-const t11 (_ BitVec 38)) (assert (= (select input1 (_ bv1 2)) t11))
(declare-const t12 (_ BitVec 38)) (assert (= (select input1 (_ bv2 2)) t12))
(declare-const t13 (_ BitVec 38)) (assert (= (select input1 (_ bv3 2)) t13))
(declare-const t20 (_ BitVec 38)) (assert (= (select input2 (_ bv0 2)) t20))
(declare-const t21 (_ BitVec 38)) (assert (= (select input2 (_ bv1 2)) t21))
(declare-const t22 (_ BitVec 38)) (assert (= (select input2 (_ bv2 2)) t22))
(declare-const t23 (_ BitVec 38)) (assert (= (select input2 (_ bv3 2)) t23))
(declare-const t30 (_ BitVec 38)) (assert (= (select input3 (_ bv0 2)) t30))
(declare-const t31 (_ BitVec 38)) (assert (= (select input3 (_ bv1 2)) t31))
(declare-const t32 (_ BitVec 38)) (assert (= (select input3 (_ bv2 2)) t32))
(declare-const t33 (_ BitVec 38)) (assert (= (select input3 (_ bv3 2)) t33))

;; (1) Check that the input rows are in range and that they extend to
;; the input_x rows.
(define-fun row-clamp-range () (_ BitVec 40)
  (bvadd bit-depth (_ bv8 40)))

(define-fun signed-range-max ((bits (_ BitVec 40))) (_ BitVec 40)
  (bvsub (bvshl one (bvsub bits one)) one))

(define-fun signed-range-min ((bits (_ BitVec 40))) (_ BitVec 40)
  (bvneg (bvshl one (bvsub bits one))))

(define-fun clamp ((x (_ BitVec 40)) (bits (_ BitVec 40))) (_ BitVec 40)
  (let ((min (signed-range-min bits))
        (max (signed-range-max bits)))
    (ite (bvsle x min)
         min
         (ite (bvsge x max) max x))))

(define-fun in-coeff-range ((x (_ BitVec 40))) Bool
  (let ((min (signed-range-min (bvadd (_ bv8 40) bit-depth)))
        (max (signed-range-max (bvadd (_ bv8 40) bit-depth))))
    (and (bvsle min x) (bvsle x max))))

(define-fun is-ext-num ((a (_ BitVec 38))
                        (b (_ BitVec 40))
                        (r (_ BitVec 40)))
  Bool
  (and (in-coeff-range ((_ sign_extend 2) a))
       (= b (clamp (concat a (_ bv0 2)) r))))

(define-fun is-ext-array ((a (Array (_ BitVec 2) (_ BitVec 38)))
                          (b (Array (_ BitVec 2) (_ BitVec 40)))
                          (r (_ BitVec 40)))
  Bool
  (and (is-ext-num (select a (_ bv0 2)) (select b (_ bv0 2)) r)
       (is-ext-num (select a (_ bv1 2)) (select b (_ bv1 2)) r)
       (is-ext-num (select a (_ bv2 2)) (select b (_ bv2 2)) r)
       (is-ext-num (select a (_ bv3 2)) (select b (_ bv3 2)) r)))

(declare-const input0_x (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const input1_x (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const input2_x (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const input3_x (Array (_ BitVec 2) (_ BitVec 40)))

(assert (is-ext-array input0 input0_x row-clamp-range))
(assert (is-ext-array input1 input1_x row-clamp-range))
(assert (is-ext-array input2 input2_x row-clamp-range))
(assert (is-ext-array input3 input3_x row-clamp-range))

;; (2) Apply row transforms. The iadst function below represents the
;;     whole IADST4 process.
(define-fun sin-pi-1-9 () (_ BitVec 40) (_ bv1321 40))
(define-fun sin-pi-2-9 () (_ BitVec 40) (_ bv2482 40))
(define-fun sin-pi-3-9 () (_ BitVec 40) (_ bv3344 40))
(define-fun sin-pi-4-9 () (_ BitVec 40) (_ bv3803 40))

(define-fun num-representable ((num-bits (_ BitVec 40)) (x (_ BitVec 40)))
  Bool
  ;; The number is representable if either (x >> (num-bits - 1)) == 0
  ;; or (for negative numbers) ((~x) >> (num-bits - 1)) == 0.
  (or (= (bvlshr x (bvsub num-bits one)) zero)
      (= (bvlshr (bvnot x) (bvsub num-bits one)) zero)))

(define-fun rep12 ((x (_ BitVec 40)) (r (_ BitVec 40)))
  Bool
  (num-representable (bvadd r (_ bv12 40)) x))

(define-fun round12 ((x (_ BitVec 40)) (r (_ BitVec 40)))
  (_ BitVec 40)
  (bvashr (bvadd x (_ bv2048 40)) (_ bv12 40)))

(define-fun iadst4 ((t (Array (_ BitVec 2) (_ BitVec 40)))
                    (u (Array (_ BitVec 2) (_ BitVec 40)))
                    (r (_ BitVec 40)))
  Bool
  (let ((s0 (bvmul sin-pi-1-9 (clamp (select t (_ bv0 2)) r)))
        (s1 (bvmul sin-pi-2-9 (clamp (select t (_ bv0 2)) r)))
        (s2 (bvmul sin-pi-3-9 (clamp (select t (_ bv1 2)) r)))
        (s3 (bvmul sin-pi-4-9 (clamp (select t (_ bv2 2)) r)))
        (s4 (bvmul sin-pi-1-9 (clamp (select t (_ bv2 2)) r)))
        (s5 (bvmul sin-pi-2-9 (clamp (select t (_ bv3 2)) r)))
        (s6 (bvmul sin-pi-4-9 (clamp (select t (_ bv3 2)) r)))
        (a7 (bvsub (clamp (select t (_ bv0 2)) r)
                   (clamp (select t (_ bv2 2)) r))))
    (let ((b7 (bvadd a7 (clamp (select t (_ bv3 2)) r))))
      (let ((s0_1 (bvadd s0 s3))
            (s1_1 (bvsub s1 s4))
            (s2_1 (bvmul sin-pi-3-9 b7))
            (s3_1 s2)
            (s4_1 s4)
            (s5_1 s5)
            (s6_1 s6))
        (let ((s0_2 (bvadd s0_1 s5_1))
              (s1_2 (bvsub s1_1 s6_1))
              (s2_2 s2_1)
              (s3_2 s3_1)
              (s4_2 s4_1)
              (s5_2 s5_1)
              (s6_2 s6_1))
          (let ((x0 (bvadd s0_2 s3_2))
                (x1 (bvadd s1_2 s3_2))
                (x2 s2_2)
                (x3 (bvadd s0_2 s1_2)))
            (let ((x0_1 x0)
                  (x1_1 x1)
                  (x2_1 x2)
                  (x3_1 (bvsub x3 s3_2)))
              (and
               ;; The first "and" checks that every intermediate value
               ;; is in range.
               (and (rep12 s0 r) (rep12 s1 r) (rep12 s2 r) (rep12 s3 r)
                    (rep12 s4 r) (rep12 s5 r) (rep12 s6 r)
                    (num-representable (bvadd r (_ bv1 40)) a7)
                    (num-representable r b7)
                    (rep12 s0_1 r) (rep12 s1_1 r) (rep12 s2_1 r)
                    (rep12 s0_2 r) (rep12 s1_2 r)
                    (rep12 x0 r) (rep12 x1 r) (rep12 x2 r) (rep12 x3 r)
                    (rep12 x3_1 r))
               ;; The second "and" checks that the output is the
               ;; transformed input.
               (and (= (select u (_ bv0 2)) (round12 x0_1 r))
                    (= (select u (_ bv1 2)) (round12 x1_1 r))
                    (= (select u (_ bv2 2)) (round12 x2_1 r))
                    (= (select u (_ bv3 2)) (round12 x3_1 r)))))))))))

(declare-const tx_row0 (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const tx_row1 (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const tx_row2 (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const tx_row3 (Array (_ BitVec 2) (_ BitVec 40)))

(assert (iadst4 input0_x tx_row0 row-clamp-range))
(assert (iadst4 input1_x tx_row1 row-clamp-range))
(assert (iadst4 input2_x tx_row2 row-clamp-range))
(assert (iadst4 input3_x tx_row3 row-clamp-range))

;; (3) Transpose the 4x4 block to get col0..col3
(declare-const col0 (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const col1 (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const col2 (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const col3 (Array (_ BitVec 2) (_ BitVec 40)))

(define-fun transpose-4x4 ((row0 (Array (_ BitVec 2) (_ BitVec 40)))
                           (row1 (Array (_ BitVec 2) (_ BitVec 40)))
                           (row2 (Array (_ BitVec 2) (_ BitVec 40)))
                           (row3 (Array (_ BitVec 2) (_ BitVec 40)))

                           (col0 (Array (_ BitVec 2) (_ BitVec 40)))
                           (col1 (Array (_ BitVec 2) (_ BitVec 40)))
                           (col2 (Array (_ BitVec 2) (_ BitVec 40)))
                           (col3 (Array (_ BitVec 2) (_ BitVec 40))))
  Bool
  (and (= (select col0 (_ bv0 2)) (select row0 (_ bv0 2)))
       (= (select col0 (_ bv1 2)) (select row1 (_ bv0 2)))
       (= (select col0 (_ bv2 2)) (select row2 (_ bv0 2)))
       (= (select col0 (_ bv3 2)) (select row3 (_ bv0 2)))

       (= (select col1 (_ bv0 2)) (select row0 (_ bv1 2)))
       (= (select col1 (_ bv1 2)) (select row1 (_ bv1 2)))
       (= (select col1 (_ bv2 2)) (select row2 (_ bv1 2)))
       (= (select col1 (_ bv3 2)) (select row3 (_ bv1 2)))

       (= (select col2 (_ bv0 2)) (select row0 (_ bv2 2)))
       (= (select col2 (_ bv1 2)) (select row1 (_ bv2 2)))
       (= (select col2 (_ bv2 2)) (select row2 (_ bv2 2)))
       (= (select col2 (_ bv3 2)) (select row3 (_ bv2 2)))

       (= (select col3 (_ bv0 2)) (select row0 (_ bv3 2)))
       (= (select col3 (_ bv1 2)) (select row1 (_ bv3 2)))
       (= (select col3 (_ bv2 2)) (select row2 (_ bv3 2)))
       (= (select col3 (_ bv3 2)) (select row3 (_ bv3 2)))))

(assert (transpose-4x4 tx_row0 tx_row1 tx_row2 tx_row3
                       col0 col1 col2 col3))

;; (4) Now we pretty much copy & paste the definition of iadst4 from
;;     above to define a version that exposes the internal ranges.
(define-fun iadst4-ext ((t (Array (_ BitVec 2) (_ BitVec 40)))
                        (r (_ BitVec 40))
                        ;; s0..s6
                        (s (Array (_ BitVec 3) (_ BitVec 40)))
                        ;; a7, b7
                        (ab (Array (_ BitVec 1) (_ BitVec 40)))
                        ;; s0_1..s2_1
                        (s_1 (Array (_ BitVec 2) (_ BitVec 40)))
                        ;; s0_2, s1_2
                        (s_2 (Array (_ BitVec 1) (_ BitVec 40)))
                        ;; x0,x1,x3,x3_1
                        (x (Array (_ BitVec 2) (_ BitVec 40))))
  Bool
  (let ((s0 (bvmul sin-pi-1-9 (clamp (select t (_ bv0 2)) r)))
        (s1 (bvmul sin-pi-2-9 (clamp (select t (_ bv0 2)) r)))
        (s2 (bvmul sin-pi-3-9 (clamp (select t (_ bv1 2)) r)))
        (s3 (bvmul sin-pi-4-9 (clamp (select t (_ bv2 2)) r)))
        (s4 (bvmul sin-pi-1-9 (clamp (select t (_ bv2 2)) r)))
        (s5 (bvmul sin-pi-2-9 (clamp (select t (_ bv3 2)) r)))
        (s6 (bvmul sin-pi-4-9 (clamp (select t (_ bv3 2)) r)))
        (a7 (bvsub (clamp (select t (_ bv0 2)) r)
                   (clamp (select t (_ bv2 2)) r))))
    (let ((b7 (bvadd a7 (clamp (select t (_ bv3 2)) r))))
      (let ((s0_1 (bvadd s0 s3))
            (s1_1 (bvsub s1 s4))
            (s2_1 (bvmul sin-pi-3-9 b7))
            (s3_1 s2)
            (s4_1 s4)
            (s5_1 s5)
            (s6_1 s6))
        (let ((s0_2 (bvadd s0_1 s5_1))
              (s1_2 (bvsub s1_1 s6_1))
              (s2_2 s2_1)
              (s3_2 s3_1)
              (s4_2 s4_1)
              (s5_2 s5_1)
              (s6_2 s6_1))
          (let ((x0 (bvadd s0_2 s3_2))
                (x1 (bvadd s1_2 s3_2))
                (x2 s2_2)
                (x3 (bvadd s0_2 s1_2)))
            (let ((x0_1 x0)
                  (x1_1 x1)
                  (x2_1 x2)
                  (x3_1 (bvsub x3 s3_2)))
              (and
               ;; The first "and" checks that every intermediate value
               ;; is in range (just as before)
               (and (rep12 s0 r) (rep12 s1 r) (rep12 s2 r) (rep12 s3 r)
                    (rep12 s4 r) (rep12 s5 r) (rep12 s6 r)
                    (num-representable (bvadd r (_ bv1 40)) a7)
                    (num-representable r b7)
                    (rep12 s0_1 r) (rep12 s1_1 r) (rep12 s2_1 r)
                    (rep12 s0_2 r) (rep12 s1_2 r)
                    (rep12 x0 r) (rep12 x1 r) (rep12 x2 r) (rep12 x3 r)
                    (rep12 x3_1 r))
               ;; This is the point where we do stuff differently,
               ;; extracting the various intermediate values and
               ;; equating them with output arrays.
               (and (and (= (select s (_ bv0 3)) s0)
                         (= (select s (_ bv1 3)) s1)
                         (= (select s (_ bv2 3)) s2)
                         (= (select s (_ bv3 3)) s3)
                         (= (select s (_ bv4 3)) s4)
                         (= (select s (_ bv5 3)) s5)
                         (= (select s (_ bv6 3)) s6))
                    (and (= (select ab (_ bv0 1)) a7)
                         (= (select ab (_ bv1 1)) b7))
                    (and (= (select s_1 (_ bv0 2)) s0_1)
                         (= (select s_1 (_ bv1 2)) s1_1)
                         (= (select s_1 (_ bv2 2)) s2_1))
                    (and (= (select s_2 (_ bv0 1)) s0_2)
                         (= (select s_2 (_ bv1 1)) s1_2))
                    (and (= (select x (_ bv0 2)) x0)
                         (= (select x (_ bv1 2)) x1)
                         (= (select x (_ bv2 2)) x3)
                         (= (select x (_ bv3 2)) x3_1)))))))))))

;; (5) I think we can hit all the ranges by just looking at col0.
(define-fun col-clamp-range () (_ BitVec 40)
  (let ((col-clamp-range (bvadd bit-depth (_ bv6 40))))
    (ite (bvsle col-clamp-range (_ bv16 40))
         (_ bv16 40)
         col-clamp-range)))

(declare-const s   (Array (_ BitVec 3) (_ BitVec 40)))
(declare-const ab  (Array (_ BitVec 1) (_ BitVec 40)))
(declare-const s_1 (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const s_2 (Array (_ BitVec 1) (_ BitVec 40)))
(declare-const x   (Array (_ BitVec 2) (_ BitVec 40)))

(assert (iadst4-ext col0 col-clamp-range
                    s ab s_1 s_2 x))

;; (6) But we *do* have to check that col1..col3 don't overflow
(declare-const tx_col1 (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const tx_col2 (Array (_ BitVec 2) (_ BitVec 40)))
(declare-const tx_col3 (Array (_ BitVec 2) (_ BitVec 40)))

(assert (iadst4 col1 tx_col1 col-clamp-range))
(assert (iadst4 col2 tx_col2 col-clamp-range))
(assert (iadst4 col3 tx_col3 col-clamp-range))

;; (7) Finally, extract the numbers we care about.
(declare-const s0 (_ BitVec 40)) (assert (= (select s (_ bv0 3)) s0))
(declare-const s1 (_ BitVec 40)) (assert (= (select s (_ bv1 3)) s1))
(declare-const s2 (_ BitVec 40)) (assert (= (select s (_ bv2 3)) s2))
(declare-const s3 (_ BitVec 40)) (assert (= (select s (_ bv3 3)) s3))
(declare-const s4 (_ BitVec 40)) (assert (= (select s (_ bv4 3)) s4))
(declare-const s5 (_ BitVec 40)) (assert (= (select s (_ bv5 3)) s5))
(declare-const s6 (_ BitVec 40)) (assert (= (select s (_ bv6 3)) s6))

(declare-const a7 (_ BitVec 40)) (assert (= (select ab (_ bv0 1)) a7))
(declare-const b7 (_ BitVec 40)) (assert (= (select ab (_ bv1 1)) b7))

(declare-const s0_1 (_ BitVec 40)) (assert (= (select s_1 (_ bv0 2)) s0_1))
(declare-const s1_1 (_ BitVec 40)) (assert (= (select s_1 (_ bv1 2)) s1_1))
(declare-const s2_1 (_ BitVec 40)) (assert (= (select s_1 (_ bv2 2)) s2_1))

(declare-const s0_2 (_ BitVec 40)) (assert (= (select s_2 (_ bv0 1)) s0_2))
(declare-const s1_2 (_ BitVec 40)) (assert (= (select s_2 (_ bv1 1)) s1_2))

(declare-const x0   (_ BitVec 40)) (assert (= (select x (_ bv0 2)) x0))
(declare-const x1   (_ BitVec 40)) (assert (= (select x (_ bv1 2)) x1))
(declare-const x3   (_ BitVec 40)) (assert (= (select x (_ bv2 2)) x3))
(declare-const x3_1 (_ BitVec 40)) (assert (= (select x (_ bv3 2)) x3_1))

;; (check-sat)

;; Local Variables:
;; mode: lisp
;; End:
