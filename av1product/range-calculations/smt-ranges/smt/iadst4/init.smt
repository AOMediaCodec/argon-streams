;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Project P8005 HEVC
; (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
; All rights reserved.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; This is the main code for the IADST-4 function.
;;
;; bit-depth is defined externally. The "r" that appears in the code is either
;; rowClampRange or colClampRange.

(define-fun r () (_ BitVec 40) (bvadd bit-depth (_ bv8 40)))

(define-fun sin-pi-1-9 () (_ BitVec 40) (_ bv1321 40))
(define-fun sin-pi-2-9 () (_ BitVec 40) (_ bv2482 40))
(define-fun sin-pi-3-9 () (_ BitVec 40) (_ bv3344 40))
(define-fun sin-pi-4-9 () (_ BitVec 40) (_ bv3803 40))

(define-fun num-representable ((num-bits (_ BitVec 40)) (x (_ BitVec 40)))
  Bool
  ;; The number is representable if either (x >> (num-bits - 1)) == 0
  ;; or (for negative numbers) ((~x) >> (num-bits - 1)) == 0.
  (or (= (bvlshr x (bvsub num-bits one)) zero)
      (= (bvlshr (bvnot x) (bvsub num-bits one)) zero)))

(define-fun rep12 ((x (_ BitVec 40)))
  Bool
  (num-representable (bvadd r (_ bv12 40)) x))

(define-fun round12 ((x (_ BitVec 40)))
  (_ BitVec 40)
  (bvashr (bvadd x (_ bv2048 40)) (_ bv12 40)))

(define-fun coded-coeff-min ()
  (_ BitVec 40)
  (bvneg (bvshl one (bvadd (_ bv5 40) bit-depth))))

(define-fun coded-coeff-max ()
  (_ BitVec 40)
  (bvshl one (bvadd (_ bv5 40) bit-depth)))

(define-fun coeff-min ()
  (_ BitVec 40)
  (bvneg (bvshl one (bvadd (_ bv7 40) bit-depth))))

(define-fun coeff-max ()
  (_ BitVec 40)
  (bvsub (bvshl one (bvadd (_ bv7 40) bit-depth)) one))

(define-fun clamp ((x (_ BitVec 40)))
  (_ BitVec 40)
  (ite (bvsle x coeff-min)
       coeff-min
       (ite (bvsge x coeff-max)
            coeff-max
            x)))

(define-fun in-range ((x (_ BitVec 38)))
  Bool
  (and (bvsle coded-coeff-min ((_ sign_extend 2) x))
       (bvsge coded-coeff-max ((_ sign_extend 2) x))))

(declare-const t0 (_ BitVec 38))
(declare-const t1 (_ BitVec 38))
(declare-const t2 (_ BitVec 38))
(declare-const t3 (_ BitVec 38))

(assert (and (in-range t0) (in-range t1) (in-range t2) (in-range t3)))

(define-fun t0_ext () (_ BitVec 40) (clamp (concat t0 (_ bv0 2))))
(define-fun t1_ext () (_ BitVec 40) (clamp (concat t1 (_ bv0 2))))
(define-fun t2_ext () (_ BitVec 40) (clamp (concat t2 (_ bv0 2))))
(define-fun t3_ext () (_ BitVec 40) (clamp (concat t3 (_ bv0 2))))

(define-fun s0 () (_ BitVec 40) (bvmul sin-pi-1-9 t0_ext))
(define-fun s1 () (_ BitVec 40) (bvmul sin-pi-2-9 t0_ext))
(define-fun s2 () (_ BitVec 40) (bvmul sin-pi-3-9 t1_ext))
(define-fun s3 () (_ BitVec 40) (bvmul sin-pi-4-9 t2_ext))
(define-fun s4 () (_ BitVec 40) (bvmul sin-pi-1-9 t2_ext))
(define-fun s5 () (_ BitVec 40) (bvmul sin-pi-2-9 t3_ext))
(define-fun s6 () (_ BitVec 40) (bvmul sin-pi-4-9 t3_ext))

(assert (and (rep12 s0) (rep12 s1) (rep12 s2) (rep12 s3)
             (rep12 s4) (rep12 s5) (rep12 s6)))

(define-fun a7 () (_ BitVec 40) (bvsub t0_ext t2_ext))
(define-fun b7 () (_ BitVec 40) (bvadd a7 t3_ext))

(assert (and (num-representable (bvadd r (_ bv1 40)) a7)
             (num-representable r b7)))

(define-fun s0_1 () (_ BitVec 40) (bvadd s0 s3))
(define-fun s1_1 () (_ BitVec 40) (bvsub s1 s4))
(define-fun s2_1 () (_ BitVec 40) (bvmul sin-pi-3-9 b7))
(define-fun s3_1 () (_ BitVec 40) s2)
(define-fun s4_1 () (_ BitVec 40) s4)
(define-fun s5_1 () (_ BitVec 40) s5)
(define-fun s6_1 () (_ BitVec 40) s6)

(assert (and (rep12 s0_1) (rep12 s1_1) (rep12 s2_1)))

(define-fun s0_2 () (_ BitVec 40) (bvadd s0_1 s5_1))
(define-fun s1_2 () (_ BitVec 40) (bvsub s1_1 s6_1))
(define-fun s2_2 () (_ BitVec 40) s2_1)
(define-fun s3_2 () (_ BitVec 40) s3_1)
(define-fun s4_2 () (_ BitVec 40) s4_1)
(define-fun s5_2 () (_ BitVec 40) s5_1)
(define-fun s6_2 () (_ BitVec 40) s6_1)

(assert (and (rep12 s0_2) (rep12 s1_2)))

(define-fun x0 () (_ BitVec 40) (bvadd s0_2 s3_2))
(define-fun x1 () (_ BitVec 40) (bvadd s1_2 s3_2))
(define-fun x2 () (_ BitVec 40) s2_2)
(define-fun x3 () (_ BitVec 40) (bvadd s0_2 s1_2))

(assert (and (rep12 x0) (rep12 x1) (rep12 x2) (rep12 x3)))

(define-fun x0_1 () (_ BitVec 40) x0)
(define-fun x1_1 () (_ BitVec 40) x1)
(define-fun x2_1 () (_ BitVec 40) x2)
(define-fun x3_1 () (_ BitVec 40) (bvsub x3 s3_2))

(assert (rep12 x3_1))

(define-fun t0_1 () (_ BitVec 40) (round12 x0_1))
(define-fun t1_1 () (_ BitVec 40) (round12 x1_1))
(define-fun t2_1 () (_ BitVec 40) (round12 x2_1))
(define-fun t3_1 () (_ BitVec 40) (round12 x3_1))

(define-fun nz-count ()
  (_ BitVec 40)
  (bvadd (bvadd (ite (= t0 (_ bv0 38)) zero one)
                (ite (= t1 (_ bv0 38)) zero one))
         (bvadd (ite (= t2 (_ bv0 38)) zero one)
                (ite (= t3 (_ bv0 38)) zero one))))

(define-fun bit-count-1 ((x (_ BitVec 1)))
  (_ BitVec 40)
  (ite (= x (_ bv0 1)) zero one))

(define-fun bit-count-2 ((x (_ BitVec 2)))
  (_ BitVec 40)
  (bvadd (bit-count-1 ((_ extract 1 1) x))
         (bit-count-1 ((_ extract 0 0) x))))

(define-fun bit-count-4 ((x (_ BitVec 4)))
  (_ BitVec 40)
  (bvadd (bit-count-2 ((_ extract 3 2) x))
         (bit-count-2 ((_ extract 1 0) x))))

(define-fun bit-count-9 ((x (_ BitVec 9)))
  (_ BitVec 40)
  (bvadd (bit-count-1 ((_ extract 8 8) x))
         (bvadd (bit-count-4 ((_ extract 7 4) x))
                (bit-count-4 ((_ extract 3 0) x)))))

(define-fun bit-count-18 ((x (_ BitVec 18)))
  (_ BitVec 40)
  (bvadd (bit-count-9 ((_ extract 17 9) x))
         (bit-count-9 ((_ extract 8 0) x))))

(define-fun bit-count-37 ((x (_ BitVec 37)))
  (_ BitVec 40)
  (bvadd (bit-count-1 ((_ extract 36 36) x))
         (bvadd (bit-count-18 ((_ extract 35 18) x))
                (bit-count-18 ((_ extract 17 0) x)))))

(define-fun abs-bit-count-38 ((x (_ BitVec 38)))
  (_ BitVec 40)
  (bit-count-37 ((_ extract 36 0)
                 (ite (= #b1 ((_ extract 37 37) x)) (bvneg x) x))))

(define-fun nz-bits-count ()
  (_ BitVec 40)
  (bvadd (bvadd (abs-bit-count-38 t0)
                (abs-bit-count-38 t1))
         (bvadd (abs-bit-count-38 t2)
                (abs-bit-count-38 t3))))

;; Local Variables:
;; mode: lisp
;; End:
