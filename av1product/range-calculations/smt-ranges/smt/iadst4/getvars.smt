;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Project P8005 HEVC
; (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
; All rights reserved.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(get-value (t0 t1 t2 t3 nz-count nz-bits-count))

;; Local Variables:
;; mode: lisp
;; End:
