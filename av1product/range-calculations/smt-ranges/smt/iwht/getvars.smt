;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Project P8005 HEVC
; (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
; All rights reserved.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(get-value (t00 t01 t02 t03 t20 t21 t22 t23 nz-count))

;; Local Variables:
;; mode: lisp
;; End:
