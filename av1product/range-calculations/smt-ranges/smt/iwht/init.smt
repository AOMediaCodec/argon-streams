;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Project P8005 HEVC
; (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
; All rights reserved.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; This snippet declares the input constants (row0..row3) and the
;; mapping function.
;;
;; Note that it uses bit-depth, which is defined in a header generated
;; by the python code.

(define-fun store4 ((t (Array (_ BitVec 2) (_ BitVec 20)))
                    (a (_ BitVec 20))
                    (b (_ BitVec 20))
                    (c (_ BitVec 20))
                    (d (_ BitVec 20)))
  (Array (_ BitVec 2) (_ BitVec 20))
  (store (store (store (store t (_ bv0 2) a)
                       (_ bv1 2) b)
                (_ bv2 2) c)
         (_ bv3 2) d))

(define-fun iwht ((shift (_ BitVec 20))
                  (t0 (Array (_ BitVec 2) (_ BitVec 20)))
                  (t1 (Array (_ BitVec 2) (_ BitVec 20))))
  Bool
  (let ((xa (bvashr (select t0 (_ bv0 2)) shift))
        (xb (bvashr (select t0 (_ bv1 2)) shift))
        (xc (bvashr (select t0 (_ bv2 2)) shift))
        (xd (bvashr (select t0 (_ bv3 2)) shift)))
    (let ((xa1 (bvadd xa xc))
          (xd1 (bvsub xd xb)))
      (let ((xe (bvashr (bvsub xa1 xd1) one)))
        (let ((xb1 (bvsub xe xb))
              (xc1 (bvsub xe xc)))
          (let ((xa2 (bvsub xa1 xb1))
                (xd2 (bvadd xd1 xc1)))
            (and (= xa2 (select t1 (_ bv0 2)))
                 (= xb1 (select t1 (_ bv1 2)))
                 (= xc1 (select t1 (_ bv2 2)))
                 (= xd2 (select t1 (_ bv3 2))))))))))

(define-fun transpose-4x4 ((row0 (Array (_ BitVec 2) (_ BitVec 20)))
                           (row1 (Array (_ BitVec 2) (_ BitVec 20)))
                           (row2 (Array (_ BitVec 2) (_ BitVec 20)))
                           (row3 (Array (_ BitVec 2) (_ BitVec 20)))

                           (col0 (Array (_ BitVec 2) (_ BitVec 20)))
                           (col1 (Array (_ BitVec 2) (_ BitVec 20)))
                           (col2 (Array (_ BitVec 2) (_ BitVec 20)))
                           (col3 (Array (_ BitVec 2) (_ BitVec 20))))
  Bool
  (and (= (select col0 (_ bv0 2)) (select row0 (_ bv0 2)))
       (= (select col0 (_ bv1 2)) (select row1 (_ bv0 2)))
       (= (select col0 (_ bv2 2)) (select row2 (_ bv0 2)))
       (= (select col0 (_ bv3 2)) (select row3 (_ bv0 2)))

       (= (select col1 (_ bv0 2)) (select row0 (_ bv1 2)))
       (= (select col1 (_ bv1 2)) (select row1 (_ bv1 2)))
       (= (select col1 (_ bv2 2)) (select row2 (_ bv1 2)))
       (= (select col1 (_ bv3 2)) (select row3 (_ bv1 2)))

       (= (select col2 (_ bv0 2)) (select row0 (_ bv2 2)))
       (= (select col2 (_ bv1 2)) (select row1 (_ bv2 2)))
       (= (select col2 (_ bv2 2)) (select row2 (_ bv2 2)))
       (= (select col2 (_ bv3 2)) (select row3 (_ bv2 2)))

       (= (select col3 (_ bv0 2)) (select row0 (_ bv3 2)))
       (= (select col3 (_ bv1 2)) (select row1 (_ bv3 2)))
       (= (select col3 (_ bv2 2)) (select row2 (_ bv3 2)))
       (= (select col3 (_ bv3 2)) (select row3 (_ bv3 2)))))

(define-fun num-representable ((num-bits (_ BitVec 20)) (x (_ BitVec 20)))
  Bool
  ;; The number is representable if either (x >> (num-bits - 1)) == 0
  ;; or (for negative numbers) ((~x) >> (num-bits - 1)) == 0.
  (or (= (bvlshr x (bvsub num-bits one)) zero)
      (= (bvlshr (bvnot x) (bvsub num-bits one)) zero)))

(define-fun array-representable ((num-bits (_ BitVec 20))
                                 (t (Array (_ BitVec 2) (_ BitVec 20))))
  Bool
  (and (num-representable num-bits (select t (_ bv0 2)))
       (num-representable num-bits (select t (_ bv1 2)))
       (num-representable num-bits (select t (_ bv2 2)))
       (num-representable num-bits (select t (_ bv3 2)))))

;; These are the input samples
(declare-const row0 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const row1 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const row2 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const row3 (Array (_ BitVec 2) (_ BitVec 20)))

;; All of the inputs must be representable in depth bit-depth + 7
(assert (array-representable (bvadd bit-depth (_ bv7 20)) row0))
(assert (array-representable (bvadd bit-depth (_ bv7 20)) row1))
(assert (array-representable (bvadd bit-depth (_ bv7 20)) row2))
(assert (array-representable (bvadd bit-depth (_ bv7 20)) row3))

;; These are the results of calling iwht on the rows
(declare-const row0_1 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const row1_1 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const row2_1 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const row3_1 (Array (_ BitVec 2) (_ BitVec 20)))
(assert (iwht two row0 row0_1))
(assert (iwht two row1 row1_1))
(assert (iwht two row2 row2_1))
(assert (iwht two row3 row3_1))

;; Transpose the 4x4 block.
(declare-const col0 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const col1 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const col2 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const col3 (Array (_ BitVec 2) (_ BitVec 20)))
(assert (transpose-4x4 row0_1 row1_1 row2_1 row3_1 col0 col1 col2 col3))

;; Finally call iwht on the columns
(declare-const col0_1 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const col1_1 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const col2_1 (Array (_ BitVec 2) (_ BitVec 20)))
(declare-const col3_1 (Array (_ BitVec 2) (_ BitVec 20)))
(assert (iwht zero col0 col0_1))
(assert (iwht zero col1 col1_1))
(assert (iwht zero col2 col2_1))
(assert (iwht zero col3 col3_1))

;; col0_1 through col3_1 contain the results. There's an extra
;; constraint from bitstream conformance requirements: each result
;; must be representable as a signed integer with 1 + bitDepth bits.
(assert (array-representable (bvadd one bit-depth) col0_1))
(assert (array-representable (bvadd one bit-depth) col1_1))
(assert (array-representable (bvadd one bit-depth) col2_1))
(assert (array-representable (bvadd one bit-depth) col3_1))

;; We also need to actually extract the values.
(declare-const t00 (_ BitVec 18)) (assert (= (concat t00 (_ bv0 2)) (select row0 (_ bv0 2))))
(declare-const t01 (_ BitVec 18)) (assert (= (concat t01 (_ bv0 2)) (select row0 (_ bv1 2))))
(declare-const t02 (_ BitVec 18)) (assert (= (concat t02 (_ bv0 2)) (select row0 (_ bv2 2))))
(declare-const t03 (_ BitVec 18)) (assert (= (concat t03 (_ bv0 2)) (select row0 (_ bv3 2))))

(declare-const t20 (_ BitVec 18)) (assert (= (concat t20 (_ bv0 2)) (select row2 (_ bv0 2))))
(declare-const t21 (_ BitVec 18)) (assert (= (concat t21 (_ bv0 2)) (select row2 (_ bv1 2))))
(declare-const t22 (_ BitVec 18)) (assert (= (concat t22 (_ bv0 2)) (select row2 (_ bv2 2))))
(declare-const t23 (_ BitVec 18)) (assert (= (concat t23 (_ bv0 2)) (select row2 (_ bv3 2))))

;; We don't have any examples where row1 or row3 need to be nonzero.
(assert (= zero (select row1 (_ bv0 2))))
(assert (= zero (select row1 (_ bv1 2))))
(assert (= zero (select row1 (_ bv2 2))))
(assert (= zero (select row1 (_ bv3 2))))
(assert (= zero (select row3 (_ bv0 2))))
(assert (= zero (select row3 (_ bv1 2))))
(assert (= zero (select row3 (_ bv2 2))))
(assert (= zero (select row3 (_ bv3 2))))

(define-fun nz-count ()
  (_ BitVec 20)
  (bvadd (bvadd (bvadd (ite (= t00 (_ bv0 18)) zero one)
                       (ite (= t01 (_ bv0 18)) zero one))
                (bvadd (ite (= t02 (_ bv0 18)) zero one)
                       (ite (= t03 (_ bv0 18)) zero one)))
         (bvadd (bvadd (ite (= t20 (_ bv0 18)) zero one)
                       (ite (= t21 (_ bv0 18)) zero one))
                (bvadd (ite (= t22 (_ bv0 18)) zero one)
                       (ite (= t23 (_ bv0 18)) zero one)))))

;; Local Variables:
;; mode: lisp
;; End:
