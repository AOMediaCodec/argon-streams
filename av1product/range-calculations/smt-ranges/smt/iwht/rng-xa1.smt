;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Project P8005 HEVC
; (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
; All rights reserved.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-fun get-val ((shift (_ BitVec 20))
                     (t (Array (_ BitVec 2) (_ BitVec 20))))
  (_ BitVec 20)
  (let ((xa (bvashr (select t (_ bv0 2)) shift))
        (xb (bvashr (select t (_ bv1 2)) shift))
        (xc (bvashr (select t (_ bv2 2)) shift))
        (xd (bvashr (select t (_ bv3 2)) shift)))
    (let ((xa1 (bvadd xa xc))
          (xd1 (bvsub xd xb)))
      xa1)))

(define-fun is-ge ((val (_ BitVec 20)))
  Bool
  (or (bvsge (get-val two row0) val)
      (bvsge (get-val two row1) val)
      (bvsge (get-val two row2) val)
      (bvsge (get-val two row3) val)
      (bvsge (get-val two col0) val)
      (bvsge (get-val two col1) val)
      (bvsge (get-val two col2) val)
      (bvsge (get-val two col3) val)))

(define-fun is-le ((val (_ BitVec 20)))
  Bool
  (or (bvsle (get-val two row0) val)
      (bvsle (get-val two row1) val)
      (bvsle (get-val two row2) val)
      (bvsle (get-val two row3) val)
      (bvsle (get-val two col0) val)
      (bvsle (get-val two col1) val)
      (bvsle (get-val two col2) val)
      (bvsle (get-val two col3) val)))

(define-fun is-eq ((val (_ BitVec 20)))
  Bool
  (or (= (get-val two row0) val)
      (= (get-val two row1) val)
      (= (get-val two row2) val)
      (= (get-val two row3) val)
      (= (get-val two col0) val)
      (= (get-val two col1) val)
      (= (get-val two col2) val)
      (= (get-val two col3) val)))

;; Local Variables:
;; mode: lisp
;; End:
