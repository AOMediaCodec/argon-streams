################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A wrapper around a the different SMT solvers.'''

import re
import string
import time

import watched_proc

# This comes from
# http://probablyprogramming.com/2009/11/23/a-simple-lisp-parser-in-python.
# There's also a Pip package called sexpy, but that seems a little OTT.
ATOM_END = set('()"') | set(string.whitespace)


def parse_sexp(sexp):
    '''Parse a string to sexps'''
    stack, i, length = [[]], 0, len(sexp)
    while i < length:
        char = sexp[i]

        reading = type(stack[-1])
        if reading == list:
            if char == '(':
                stack.append([])
            elif char == ')':
                stack[-2].append(stack.pop())
            elif char == '"':
                stack.append('')
            elif char in string.whitespace:
                pass
            else:
                stack.append((char,))
        elif reading == str:
            if char == '"':
                stack[-2].append(stack.pop())
            elif char == '\\':
                i += 1
                stack[-1] += sexp[i]
            else:
                stack[-1] += char
        elif reading == tuple:
            if char in ATOM_END:
                atom = stack.pop()
                if atom[0][0].isdigit():
                    stack[-1].append(int(atom[0]))
                else:
                    stack[-1].append(atom)
                continue
            else:
                stack[-1] = ((stack[-1][0] + char),)
        i += 1

    return stack.pop()


def is_str_tuple(sexp):
    '''Returns true if sexp is a length-1 tuple containing a string.'''
    return (isinstance(sexp, tuple)
            and len(sexp) == 1 and isinstance(sexp[0], str))


def interpret_uvalue(uvalue, width):
    '''Interpret uvalue as a 2's complement signed value'''
    return -((1 << width) - uvalue) if uvalue >> (width - 1) else uvalue


def parse_cvc4_value(name, sexp):
    '''Parse a sexp from (_ bv123 10) to a value.'''
    if ((len(sexp) != 3 or sexp[0] != ('_',) or
         not is_str_tuple(sexp[1]) or not isinstance(sexp[2], int))):
        raise RuntimeError('The value to assign to `{}\' was a list, '
                           'but not of the form (_ x y). '
                           'Parsed sexp: {}'
                           .format(name, sexp))

    str_val = sexp[1][0]
    width = sexp[2]

    val_match = re.match(r'bv([0-9]+)$', str_val)
    if not val_match:
        raise RuntimeError('The actual value to assign to `{}\' '
                           'looks odd. Expected (_ bv1234 10). '
                           'Parsed sexp: {}'
                           .format(name, sexp))
    uvalue = int(val_match.group(1))

    return interpret_uvalue(uvalue, width)


def parse_z3_value(name, sexp):
    '''Parse a sexp from #b01001 to a value'''
    str_val = sexp[0]
    bin_match = re.match(r'#b([01]+)$', str_val)
    if bin_match:
        return interpret_uvalue(int(bin_match.group(1), 2),
                                len(bin_match.group(1)))

    hex_match = re.match(r'#x([0-9a-f]+)$', str_val)
    if hex_match:
        return interpret_uvalue(int(hex_match.group(1), 16),
                                4 * len(hex_match.group(1)))

    raise RuntimeError('The actual value to assign to `{}\' '
                       'looks odd. Expected #b10110 or #xa3df. '
                       'Saw string: {}'
                       .format(name, str_val))


def parse_values(text):
    '''Parse all the values returned by an SMT process'''
    values = {}
    sexps = parse_sexp(text)
    if len(sexps) != 1:
        raise RuntimeError('Expected a single list of assignments.')

    for ass_sexp in sexps[0]:
        if len(ass_sexp) != 2:
            raise RuntimeError('Failed to parse value from the sexp {}'
                               ' (wrong length).'
                               .format(ass_sexp))
        name_sexp, val_sexp = ass_sexp
        if not isinstance(name_sexp, tuple) or len(name_sexp) != 1:
            raise RuntimeError('The name sexp in the assignment was '
                               'not a length 1 tuple ({})'
                               .format(name_sexp))
        name = name_sexp[0]
        assert isinstance(name, str)

        # A value from Z3 will look like a string #b00010101. This
        # parsed to ('#b00010101',). A value from CVC4 will look like
        # a tuple (_ bv123 10). This parsed to [('_',), ('bv123',),
        # 10].
        if isinstance(val_sexp, list):
            # This had better be the CVC4 version
            value = parse_cvc4_value(name, val_sexp)
        elif is_str_tuple(val_sexp):
            # This had better be the Z3 version.
            value = parse_z3_value(name, val_sexp)
        else:
            raise RuntimeError('Unknown value format for `{}\': {}'
                               .format(name, val_sexp))

        values[name] = value
    return values


class SMTResult():
    '''The result of running the SMT solver.

    This is either some sort of error (the SMT solver died or wrote out
    garbage), 'UNSAT' or a model.'''
    def __init__(self, error, model):
        self.error = error
        self.model = model

    @staticmethod
    def sat(model):
        '''Return an object representing a SAT result with model.'''
        return SMTResult(None, model)

    @staticmethod
    def unsat():
        '''Return an object representing a UNSAT result.'''
        return SMTResult(None, None)

    @staticmethod
    def err(error):
        '''Return an object representing an error.'''
        return SMTResult(error, None)


class SMTProc(watched_proc.WatchedProc):
    def __init__(self, args, get_vars, timeout, ignore_retcode):
        super().__init__(args=args, timeout=timeout,
                         ignore_retcode=ignore_retcode)
        self.get_vars = get_vars

    def get_result(self):
        '''Make sense of the result of the run.'''
        try:
            res = self.get_output()
            if res is not None:
                return self.read_output(res)
            return None
        except RuntimeError as err:
            return SMTResult.err(err)

    def read_output(self, output):
        '''Read the output from a run.'''
        sat_line, output = (output + '\n').split('\n', 1)
        if sat_line == 'unsat':
            return SMTResult.unsat()
        if sat_line != 'sat':
            return SMTResult.err('Solver output looks odd. First line: {}'
                                 .format(sat_line))

        return SMTResult.sat({} if not self.get_vars
                             else parse_values(output))


class Z3Proc(SMTProc):
    '''A Z3 process'''
    def __init__(self, script, get_vars, timeout=600):
        # z3 is flaky and seems to die (munmap of invalid pointer)
        # after successfully writing its result. Thus we ignore the
        # return status.
        super().__init__((['z3', '-smt2', script]),
                         get_vars=get_vars,
                         timeout=timeout,
                         ignore_retcode=True)


class CVC4Proc(SMTProc):
    '''A CVC4 process'''
    def __init__(self, script, get_vars, timeout=600):
        super().__init__((['cvc4', '--lang=smt2',
                           '--simplification=none', script]),
                         get_vars=get_vars,
                         timeout=timeout,
                         ignore_retcode=True)


class BoolectorProc(SMTProc):
    '''A Z3 process'''
    def __init__(self, script, get_vars, timeout=600):
        super().__init__((['boolector', '--smt2', script]),
                         get_vars=get_vars,
                         timeout=timeout,
                         ignore_retcode=True)


class YicesProc(SMTProc):
    '''A Z3 process'''
    def __init__(self, script, get_vars, timeout=600):
        super().__init__((['yices-smt2', script]),
                         get_vars=get_vars,
                         timeout=timeout,
                         ignore_retcode=True)


def mk_proc(solver, script, get_vars):
    if solver == 'boolector':
        return BoolectorProc(script, get_vars)
    elif solver == 'z3':
        return Z3Proc(script, get_vars)
    elif solver == 'cvc4':
        return CVC4Proc(script, get_vars)
    elif solver == 'yices':
        return YicesProc(script, get_vars)
    else:
        raise ValueError('Unknown solver: {}'.format(solver))
