################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A process that can be started and killed and writes its stdout to a file.'''

import subprocess
import tempfile
import time


class WatchedProc():
    '''A process that can be started and killed.'''
    def __init__(self, args, timeout=600, ignore_retcode=False):
        self.ignore_retcode = ignore_retcode
        self.timeout = timeout
        self.args = args

        self.proc = None
        self.output = None
        self.end_time = None

    def open(self):
        '''Start the process'''
        self.output = tempfile.NamedTemporaryFile()
        self.end_time = time.time() + self.timeout
        try:
            self.proc = subprocess.Popen(self.args,
                                         stdout=self.output,
                                         stderr=subprocess.DEVNULL,
                                         universal_newlines=True)
        except:
            self.output.close()
            self.proc = None
            self.end_time = None
            self.output = None
            raise

    def close(self, terminate):
        '''End the process, kill if terminate is true.'''
        try:
            assert self.proc
            if not terminate:
                self.wait()
            # Kill the process if it's still running.
            if self.proc.returncode is None:
                self.proc.terminate()
                time.sleep(0.01)
                self.proc.poll()
                if self.proc.returncode is None:
                    self.proc.kill()
        finally:
            self.output.close()
            self.proc = None
            self.end_time = None
            self.output = None

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close(exc_type is not None)

    def wait(self):
        '''Try to wait for the process to finish.'''
        now = time.time()
        try:
            if now < self.end_time:
                self.proc.wait(timeout=(self.end_time - now))
        except subprocess.TimeoutExpired:
            pass

    def get_output(self):
        '''Read the output if the process has finished.'''
        assert self.proc

        self.proc.poll()
        if self.proc.returncode is None:
            return None

        if self.proc.returncode != 0 and not self.ignore_retcode:
            raise RuntimeError("Process failed with exit code {}"
                               .format(self.proc.returncode))

        with open(self.output.name, 'r') as output:
            return output.read()
