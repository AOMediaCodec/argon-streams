#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Run CVC4 to find possible ranges and models to hit their endpoints.'''

import argparse
import os
import re
import sys
import tempfile
import time

import smt_proc


class Model():
    '''Represents the algorithm that's being run'''
    def __init__(self, bit_width, extra_vars):
        assert bit_width > 0
        self.bit_width = bit_width
        self.extra_vars = extra_vars

    def ranges(self):
        '''Return a list of the ranges that we might try to hit.'''
        raise NotImplementedError

    def init_str(self):
        '''Return the SMT code for the problem setup'''
        raise NotImplementedError

    def hits_str(self, rng):
        '''Return the SMT code to check whether we hit a value.

        rng should be a range name (from ranges).

        '''
        raise NotImplementedError

    def get_vars_str(self):
        '''Return SMT code to dump the interesting inputs'''
        raise NotImplementedError

    def write_setup(self, stream, rng, get_vars, title):
        '''Write out the initial part of the SMT script'''
        if title:
            stream.write(';; ' + title.strip() + '\n')
        if get_vars:
            stream.write('(set-option :produce-models true)\n')
        stream.write('(set-logic QF_ABV)\n')
        for var, val in self.extra_vars.items():
            stream.write(self.define_bv(var, val))

        stream.write(self.define_bv('zero', 0) +
                     self.define_bv('one', 1) +
                     self.define_bv('two', 2))
        stream.write(self.init_str() + '\n')
        stream.write(self.hits_str(rng) + '\n')

    def run_generator(self, solver, test, value, rng, get_vars,
                      max_nz=None, max_nz_bits=None, title=None):
        '''Make a generator representing the running solver'''
        if title:
            print(title)
            sys.stdout.flush()

        with tempfile.NamedTemporaryFile(mode='w') as temp:
            self.write_setup(temp, rng, get_vars, title)
            temp.write('(assert ({} {}))\n'.format(test, self.lit_bv(value)))
            if max_nz is not None:
                temp.write('(assert (bvsle nz-count {}))\n'
                           .format(self.lit_bv(max_nz)))
            if max_nz_bits is not None:
                temp.write('(assert (bvsle nz-bits-count {}))\n'
                           .format(self.lit_bv(max_nz_bits)))
            temp.write('(check-sat)\n')
            if get_vars:
                temp.write(self.get_vars_str() + '\n')
            temp.flush()

            res = None
            with smt_proc.mk_proc(solver, temp.name, get_vars) as proc:
                while True:
                    res = proc.get_result()
                    if res is not None:
                        yield res
                        return
                    yield None

    def define_bv(self, name, value):
        '''Return the SMT code to define a bitvector with the given name.'''
        return ('(define-fun {} () (_ BitVec {}) {})\n'
                .format(name, self.bit_width, self.lit_bv(value)))

    def lit_bv(self, value):
        '''Return the value rendered as a bitvector'''
        return ('(_ bv{} {})'.format(value, self.bit_width)
                if value >= 0
                else '(bvneg (_ bv{} {}))'.format(-value, self.bit_width))

    def nz_minify_bits_generator(self, solver, rng, value, best_model):
        nz_count = best_model['nz-count']
        best_nz_bits = best_model['nz-bits-count']
        lb = 0

        while lb < best_nz_bits:
            target = (best_nz_bits + lb) // 2
            title = (' minifying bits {}={}; nz={}; nz-bits in ({}, {}] '
                     '(target={}; len={})'
                     .format(rng, value, nz_count,
                             lb, best_nz_bits, target,
                             (best_nz_bits - lb) + 1))
            gen = self.run_generator(solver, 'is-eq',
                                     value, rng, True,
                                     max_nz=nz_count,
                                     max_nz_bits=target,
                                     title=title)
            while True:
                try:
                    res = next(gen)
                    if res is not None:
                        break
                    yield None
                except StopIteration:
                    pass

            if res.error:
                raise RuntimeError('Failed to minify bits for range {}: {}'
                                   .format(rng, res.error))
            if res.model is not None:
                best_model = res.model
                best_nz_bits = best_model['nz-bits-count']
            else:
                lb = target + 1

        del best_model['nz-count']
        del best_model['nz-bits-count']
        yield (value, best_model)

    def nz_minify_generator(self, solver, rng, value, best_model):
        '''Get a model minimising the number of nonzero coeffs'''

        # Try to find a model with more zeros
        best_nz = best_model['nz-count']
        nz_lb = 0

        while nz_lb < best_nz:
            nz_target = (best_nz + nz_lb) // 2
            title = (' minifying {}={}; nz in ({}, {}] (target={}; len={})'
                     .format(rng, value, nz_lb, best_nz, nz_target,
                             (best_nz - nz_lb) + 1))
            gen = self.run_generator(solver, 'is-eq',
                                     value, rng, True,
                                     max_nz=nz_target, title=title)
            while True:
                try:
                    res = next(gen)
                    if res is not None:
                        break
                    yield None
                except StopIteration:
                    pass

            if res.error:
                raise RuntimeError('Failed to minify range {}: {}'
                                   .format(rng, res.error))
            if res.model is not None:
                best_model = res.model
                best_nz = best_model['nz-count']
            else:
                nz_lb = nz_target + 1

        if 'nz-bits-count' not in best_model:
            del best_model['nz-count']
            yield (value, best_model)

        yield from self.nz_minify_bits_generator(solver, rng,
                                                 value, best_model)

    def model_generator(self, solver, rng, value):
        '''Get a model for the given range and value'''
        title = ' getting model for {}={}'.format(rng, value)
        gen = self.run_generator(solver, 'is-eq', value, rng,
                                 True, title=title)
        while True:
            try:
                res = next(gen)
                if res is not None:
                    break
                yield None
            except StopIteration:
                pass

        if res.error:
            raise RuntimeError('Error getting model for {} = {}: {}'
                               .format(rng, value, res.error))

        best_model = res.model
        if best_model is None:
            raise RuntimeError("The solver lied! "
                               "We couldn't get a model for {} = {}."
                               .format(rng, value))

        if 'nz-count' not in best_model:
            print('Initial model for {} = {}: {{ {} }}'
                  .format(rng, value,
                          ' , '.join(["{:8}".format(best_model[var])
                                      for var in sorted(best_model.keys())])))
            sys.stdout.flush()
            yield (value, best_model)
            return

        yield from self.nz_minify_generator(solver, rng, value, best_model)

    def extreme_generator(self, solver, rng, negate):
        '''Search for the max or min value for the given range.'''
        val_lo = 0
        val_hi = (1 << (self.bit_width - 1)) - 1
        hit_lo = False
        test = 'is-le' if negate else 'is-ge'

        while val_hi > val_lo + 1:
            val_mid = (val_hi + val_lo + 1) // 2
            target = -val_mid if negate else val_mid

            if negate:
                title = ('Min {:8} in range ({}, {}] (target={}; len {})'
                         .format(rng, -val_hi, -val_lo,
                                 target, val_hi - val_lo - 1))
            else:
                title = ('Max {:8} in range [{}, {}) (target={}, len {})'
                         .format(rng, val_lo, val_hi,
                                 target, val_hi - val_lo - 1))

            gen = self.run_generator(solver, test, target, rng,
                                     False, title=title)
            while True:
                try:
                    res = next(gen)
                    if res is not None:
                        break
                    yield None
                except StopIteration:
                    pass

            if res.error:
                raise RuntimeError('Error solving {} {} {}: {}'
                                   .format(rng, test, target, res.error))
            if res.model is not None:
                hit_lo = True
                val_lo = val_mid
            else:
                val_hi = val_mid

        if not hit_lo:
            raise RuntimeError('Empty {}ative range for {}.'
                               .format('neg' if negate else 'pos', rng))

        yield from self.model_generator(solver, rng,
                                        -val_lo if negate else val_lo)

    def get_ranges(self, solver, num_jobs):
        '''Get all ranges for this model'''
        jobs = [(rng, is_min)
                for rng in self.ranges()
                for is_min in [False, True]]
        workers = {}
        results = {}

        while jobs or workers:
            # Check for any finished workers
            jobs_done = []
            for job, worker in workers.items():
                res = next(worker)
                if res is not None:
                    results[job] = res
                    jobs_done.append(job)
            for job in jobs_done:
                del workers[job]

            # Start new workers if needed
            if jobs:
                free_slots = num_jobs - len(workers)
                for _ in range(free_slots):
                    job = jobs.pop()
                    rng, is_min = job
                    workers[job] = self.extreme_generator(solver, rng, is_min)
                    if not jobs:
                        break
            # Wait a bit.
            time.sleep(0.1)

        return results


def slurp(path):
    '''Return the contents of the file at path'''
    with open(path) as stream:
        return stream.read()


class FileModel(Model):
    '''A model that reads from files for code snippets.'''
    def __init__(self, dirname, extra_vars):
        super().__init__(FileModel.directory_bitwidth(dirname), extra_vars)
        self.dirname = dirname
        self.range_lists = FileModel.directory_ranges(dirname)

    @staticmethod
    def directory_bitwidth(dirname):
        '''Get a bit width from the given directory.'''
        path = os.path.join(dirname, 'bit-width')
        contents = slurp(path).strip()
        match = re.match(r'([0-9])+$', contents)
        if not match:
            raise RuntimeError('The bit-width file at `{}\' doesn\'t '
                               'contain a valid integer.'
                               .format(path))

        return int(contents, 10)

    @staticmethod
    def directory_ranges(dirname):
        '''Return the list of ranges defined in a directory'''
        # "Snippet ranges" are ones where there is a hunk of SMT code
        # to include to define is-ge and is-le.
        snippet_ranges = []
        with os.scandir(dirname) as dir_fd:
            for entry in dir_fd:
                if not entry.is_file():
                    continue

                match = re.match(r'rng-([a-z][a-z0-9_]+)\.smt$', entry.name)
                if match:
                    snippet_ranges.append(match.group(1))
        snippet_ranges.sort()

        # "Other ranges" are just listed in a file called
        # "other-ranges" (if it exists).
        other_ranges = []
        try:
            with open(os.path.join(dirname, 'other-ranges')) as or_file:
                for or_line in or_file:
                    or_line = or_line.strip()
                    if or_line and not or_line.startswith('#'):
                        other_ranges.append(or_line)
        except FileNotFoundError:
            pass

        return (snippet_ranges, other_ranges)

    def ranges(self):
        return self.range_lists[0] + self.range_lists[1]

    def init_str(self):
        return slurp(os.path.join(self.dirname, 'init.smt'))

    def hits_str(self, rng):
        # If in the first range list, we have a snippet to dump
        if rng in self.range_lists[0]:
            return slurp(os.path.join(self.dirname, 'rng-{}.smt'.format(rng)))

        # If in the second range list ("other-ranges"), there's a
        # default behaviour.
        return ('''(define-fun is-ge ((val (_ BitVec {width})))
                     Bool
                     (bvsge {name} val))

                   (define-fun is-le ((val (_ BitVec {width})))
                     Bool
                     (bvsle {name} val))

                   (define-fun is-eq ((val (_ BitVec {width})))
                     Bool
                     (= {name} val))'''
                .format(width=self.bit_width,
                        name=rng))

    def get_vars_str(self):
        return slurp(os.path.join(self.dirname, 'getvars.smt'))


def main():
    '''The main function.'''
    parser = argparse.ArgumentParser()
    parser.add_argument('--bd',
                        help='Bit depth (probably 8, 10 or 12)',
                        type=int, default=10)
    parser.add_argument('--jobs', '-j', help='Number of concurrent jobs',
                        type=int, default=1)
    parser.add_argument('--solver', help='Solver to use',
                        choices=['boolector', 'z3', 'cvc4', 'yices'],
                        default='boolector')
    parser.add_argument('path', help='Path to directory containing SMT inputs')

    args = parser.parse_args()

    model = FileModel(args.path, {'bit-depth': args.bd})
    results = model.get_ranges(args.solver, args.jobs)
    sys.stdout.write('int32 extreme_coeffs [{}] [] = {{\n'
                     .format(2 * len(results)))
    comment = None
    for job in sorted(results.keys()):
        rng, is_min = job
        extreme, model = results[job]
        if comment:
            sys.stdout.write(', // {}\n'.format(comment))
        sys.stdout.write('  {{ {} }}'
                         .format(', '
                                 .join(["{:8}".format(model[var])
                                        for var in sorted(model.keys())])))
        comment = "{:4} = {:12}".format(rng, extreme)
    if comment:
        sys.stdout.write('  // {:12}\n'.format(comment))
    sys.stdout.write('};\n')


if __name__ == "__main__":
    main()
