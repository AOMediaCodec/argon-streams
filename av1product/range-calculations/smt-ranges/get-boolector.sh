#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

set -e
set -x

mkdir smt-tools
cd smt-tools

wget http://fmv.jku.at/lingeling/lingeling-bcj-78ebb86-180517.tar.gz
tar xf lingeling-bcj-78ebb86-180517.tar.gz
rm lingeling-bcj-78ebb86-180517.tar.gz
mv lingeling-* lingeling
(cd lingeling; ./configure.sh; make -j4)

git clone https://github.com/Boolector/btor2tools.git
(cd btor2tools; ./configure.sh; make -j4)

wget https://github.com/Boolector/boolector/archive/3.0.0.tar.gz
tar xf 3.0.0.tar.gz
rm 3.0.0.tar.gz
mv boolector* boolector
(cd boolector; ./configure.sh --only-lingeling; make -C build -j4)
