################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import itertools, sys

def fwht4(inp):
    a1 = inp[0]
    b1 = inp[1]
    c1 = inp[2]
    d1 = inp[3]
    output = [0]*4

    a1 += b1
    d1 = d1 - c1
    e1 = (a1 - d1) >> 1
    b1 = e1 - b1
    c1 = e1 - c1
    a1 -= c1
    d1 += b1
    output[0] = a1
    output[1] = c1
    output[2] = d1
    output[3] = b1

    return output

N = 11
minima = None
maxima = None

block = None

def upd(i, v):
    if minima is None:
        return
    if v < minima[i][0]:
        minima[i] = (v, block)
    if v > maxima[i][0]:
        maxima[i] = (v, block)

def iwht4(inp):
    a1 = inp[0]
    upd(0, a1)
    c1 = inp[1]
    upd(1, c1)
    d1 = inp[2]
    upd(2, d1)
    b1 = inp[3]
    upd(3, b1)
    a1 += c1
    upd(4, a1)
    d1 -= b1
    upd(5, d1)
    e1 = (a1 - d1) >> 1
    upd(6, e1)
    b1 = e1 - b1
    upd(7, b1)
    c1 = e1 - c1
    upd(8, c1)
    a1 -= b1
    upd(9, a1)
    d1 += c1
    upd(10, d1)
    # We don't need to return a result here - we just want to track
    # the min and max of each intermediate value

def main(bd):
    global minima, maxima, block
    minima = [(0, None)] * N
    maxima = [(0, None)] * N

    lo = -(1<<bd)
    hi = (1<<bd)-1

    print "Bit depth = %d:" % bd

    # Generate potential input blocks by looking at all combinations
    # of 0, min, or max valued outputs. This ensures that all the input
    # blocks we consider are valid.
    # Further, the extremal values are always achieved in the inverse
    # row transform - roughly because its output is allowed to be one
    # bit wider than that of the column transform.
    # With a little bit of rearranging for speed, we get:
    for num, fwd_block in enumerate(itertools.product((0, lo, hi), repeat=16)):
        intermediate = [0] * 16
        # Forward col transform
        for j in range(4):
            col = fwd_block[j:16:4]
            intermediate[j:16:4] = fwht4(col)
        # Joined forward row transform + inverse row transform
        # The inverse row transform logs the achieved range of all intermediates
        for i in range(4):
            row = intermediate[4*i:4*i+4]
            result = fwht4(row)
            block = tuple(result)
            iwht4(result)

    print "Minima: ["
    for i in range(N):
        s = "[" + ", ".join(map(lambda x: "%6d" % x, minima[i][1])) + "]"
        print "  %2d: %6d at %s," % (i, minima[i][0], s)
    print "]"
    print "Maxima: ["
    for i in range(N):
        s = "[" + ", ".join(map(lambda x: "%6d" % x, maxima[i][1])) + "]"
        print "  %2d: %6d at %s," % (i, maxima[i][0], s)
    print "]"

    print "Merged and deduplicated:"
    interesting_blocks = set()
    for i in range(N):
        interesting_blocks.add(minima[i][1])
        interesting_blocks.add(maxima[i][1])
    for block in interesting_blocks:
        print "  [" + ", ".join(map(lambda x: "%6d" % x, block)) + "]"
    print

if __name__ == "__main__":
    main(int(sys.argv[1]))
