################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import math

cdef_pri_taps = [ ( 4, 2 ), ( 3, 3 ) ]
cdef_sec_taps = [ ( 2, 1 ), ( 2, 1 ) ]

cdef_directions = [
  ( ( -1, 1 ), ( -2,  2 ) ),
  ( (  0, 1 ), ( -1,  2 ) ),
  ( (  0, 1 ), (  0,  2 ) ),
  ( (  0, 1 ), (  1,  2 ) ),
  ( (  1, 1 ), (  2,  2 ) ),
  ( (  1, 0 ), (  2,  1 ) ),
  ( (  1, 0 ), (  2,  0 ) ),
  ( (  1, 0 ), (  2, -1 ) )
]

def cdef_get_at(window, dir_, k, sign):
  y = 2 + sign * cdef_directions[dir_][k][0]
  x = 2 + sign * cdef_directions[dir_][k][1]
  return window[x][y]

def get_msb(v):
  return int(math.log(v, 2))

def clamp(v, min_, max_):
  if v < min_: return min_
  if v > max_: return max_
  return v

def constrain(diff, threshold, damping):
  if threshold == 0:
    return 0

  damping_adj = max(0, damping - get_msb(threshold))
  sign = -1 if (diff < 0) else 1
  return sign * clamp(threshold - (abs(diff) >> damping_adj), 0, abs(diff))

# Estimate the bounds on certain intermediate values in the cdef filter.
# This assumes that we're in the luma plane.
# "window" should be a 5x5 window of pixels - we only calculate values
# for the pixel at the center of this window
def cdef_block(window, dir_, priStr, secStr, damping, bd):
  coeffShift = bd - 8
  coeffSet = 1

  sum_ = 0
  x = window[2][2]
  max_ = x
  min_ = x
  minima = [0, 0]
  maxima = [0, 0]
  for k in range(2):
    for sign in (-1, +1):
      p = cdef_get_at(window, dir_, k, sign)
      max_ = max(p, max_)
      min_ = min(p, min_)
      s0 = cdef_get_at(window, (dir_ + 2) & 7, k, sign)
      s1 = cdef_get_at(window, (dir_ + 2) & 7, k, sign)
      max_ = max(s0, max_)
      max_ = max(s1, max_)
      min_ = min(s0, min_)
      min_ = min(s1, min_)

      sum_ += cdef_pri_taps[coeffSet][k] * constrain(p - x, priStr, damping)
      minima[0] = min(minima[0], sum_)
      maxima[0] = max(maxima[0], sum_)
      sum_ += cdef_sec_taps[coeffSet][k] * constrain(s0 - x, secStr, damping)
      sum_ += cdef_sec_taps[coeffSet][k] * constrain(s1 - x, secStr, damping)
      minima[1] = min(minima[1], sum_)
      maxima[1] = max(maxima[1], sum_)

  res = x + ((8 + sum_ - (sum_ < 0)) >> 4)

  print "    pri min = %5d, pri max = %4d, sec min = %5d, sec max = %4d, sum = %5d, res = %4d" % (minima[0], maxima[0], minima[1], maxima[1], sum_, res)

constrainMaxPri = {10: 54, 12: 214}
constrainMaxSec = {10: 15, 12: 61}

# Estimate the minimum and maximum values we can reach, for both the intermediate
# sums (after adding primary and secondary taps), and the overall result.
# Note: To reach these limits, we must be in the UV plane. This is because
# the Y plane filter adjusts its primary strength based on the detected edge strength,
# which means we can't easily guarantee that priStr == 15 << (bd-8). Meanwhile, the UV
# planes have no adjustment and just use the signalled values directly.
if __name__ == "__main__":
  for bd in (10, 12):
    print "bd = %d:" % (bd)
    maxDamping = 6
    shift = bd - 8
    pMax = (1 << bd) - 1
    pri = constrainMaxPri[bd]
    sec = constrainMaxSec[bd]
    # Window which minimizes 'sum' at each step, constructed for direction 2.
    # The idea here is that we want the primary taps (left and right)
    # to be 'constrainMaxPri' less than the central pixel, and the secondary taps
    # (diagonally around the central pixel) to be 'constrainMaxSec' less than the center
    window = [[0 for j in range(5)] for i in range(5)]
    window[2][2] = pri
    for i in range(1,3):
      window[2+i][2+i] = (pri-sec)
      window[2+i][2-i] = (pri-sec)
      window[2-i][2+i] = (pri-sec)
      window[2-i][2-i] = (pri-sec)
    print "  Minimizing intermediate sums:"
    cdef_block(window, 2, 15 << shift, 4 << shift, maxDamping + shift, bd)

    # Window which maximizes 'sum' at each step, constructed for direction 2.
    # This is the opposite of above, where we want the central tap
    # to be lower than the other taps.
    window = [[pMax for j in range(5)] for i in range(5)]
    window[2][2] = pMax - pri
    for i in range(1,3):
      window[2+i][2+i] = pMax - (pri-sec)
      window[2+i][2-i] = pMax - (pri-sec)
      window[2-i][2+i] = pMax - (pri-sec)
      window[2-i][2-i] = pMax - (pri-sec)
    print "  Maximizing intermediate sums:"
    cdef_block(window, 2, 15 << shift, 4 << shift, maxDamping + shift, bd)

    print "  For comparison, the ranges of 'sum' in eqns_av1b.txt are:"
    print "  Primary:"
    print "    MIN:", -(12 * pri + 10 * sec)
    print "    MAX:", +(12 * pri + 10 * sec)
    print "  Secondary:"
    print "    MIN:", -(12 * pri + 12 * sec)
    print "    MAX:", +(12 * pri + 12 * sec)
    print

    # Window which minimizes 'res', constructed for direction 2.
    # This time, we have a balance between wanting to decrease the
    # central pixel (since res = x + [stuff]) and wanting as large
    # a delta as possible between the central pixel and the taps
    # (which will decrease the other term in 'res')
    # It turns out that we reach a balance when the central pixel
    # equals constrainMaxSec and the rest are all zeros.
    window = [[0 for j in range(5)] for i in range(5)]
    window[2][2] = sec
    print "  Minimizing 'res':"
    cdef_block(window, 2, 15 << shift, 4 << shift, maxDamping + shift, bd)

    # Window which maximizes 'res', constructed for direction 2.
    # This is similar, but near the max pixel value instead of near 0.
    window = [[pMax for j in range(5)] for i in range(5)]
    window[2][2] = pMax - sec
    print "  Maximizing 'res':"
    cdef_block(window, 2, 15 << shift, 4 << shift, maxDamping + shift, bd)

    print "  For comparison, the range of 'res' in eqns_av1b.txt is:"
    print "  MIN:", sec + ((8 + -24 * sec - 1) >> 4)
    print "  MAX:", (pMax-sec) + ((8 + 24 * sec) >> 4)
