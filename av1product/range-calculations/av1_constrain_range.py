################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import math

def get_msb(v):
  return int(math.log(v, 2))

def clamp(v, min_, max_):
  if v < min_: return min_
  if v > max_: return max_
  return v

def constrain(diff, threshold, damping):
  if threshold == 0:
    return 0

  damping_adj = max(0, damping - get_msb(threshold))
  sign = -1 if (diff < 0) else 1
  return sign * clamp(threshold - (abs(diff) >> damping_adj), 0, abs(diff))

print "Note: For positive inputs, the result of 'constrain' is the minimum"
print "of two functions:"
print "* An increasing function y = x, and"
print "* A decreasing function y = const - (x >> damping_adj)"
print "Thus, if the maximum is x*, then we can say that:"
print "* constrain(x*) = x*"
print "* constrain(x* + 1) <= x*"
print "and this is enough to prove that a particular x* is the maximum"
print "for that bit depth."
print

# Calculate the max value for the primary taps (threshold <= 15 << (bd-8))
print "Y plane (damping <= 6, pri strength <= 15 << (bd-8) sec strength <= 4 << (bd-8))"
print "  Primary taps:"
print "     8bpp: constrain(12, 15, 5) = %d" % constrain(12, 15, 6)
print "     8bpp: constrain(13, 15, 5) = %d" % constrain(13, 15, 6)
print "    10bpp: constrain(48, 15<<2, 5+2) = %d" % constrain(48, 15<<2, 6+2)
print "    10bpp: constrain(49, 15<<2, 5+2) = %d" % constrain(49, 15<<2, 6+2)
print "    12bpp: constrain(192, 15<<4, 5+4) = %d" % constrain(192, 15<<4, 6+4)
print "    12bpp: constrain(193, 15<<4, 5+4) = %d" % constrain(193, 15<<4, 6+4)
print

# Calculate the max value for the secondary taps (threshold <= 4 << (bd-8))
print "  Secondary taps:"
print "     8bpp: constrain(4, 4, 5) = %d" % constrain(4, 4, 6)
print "     8bpp: constrain(5, 4, 5) = %d" % constrain(5, 4, 6)
print "    10bpp: constrain(15, 4<<2, 5+2) = %d" % constrain(15, 4<<2, 6+2)
print "    10bpp: constrain(16, 4<<2, 5+2) = %d" % constrain(16, 4<<2, 6+2)
print "    12bpp: constrain(57, 4<<4, 5+4) = %d" % constrain(57, 4<<4, 6+4)
print "    12bpp: constrain(58, 4<<4, 5+4) = %d" % constrain(58, 4<<4, 6+4)

print
print "UV planes (damping <= 5, pri strength <= 15 << (bd-8), sec strength <= 4 << (bd-8))"
print "  Primary taps:"
print "     8bpp: constrain(12, 15, 5) = %d" % constrain(12, 15, 5)
print "     8bpp: constrain(13, 15, 5) = %d" % constrain(13, 15, 5)
print "    10bpp: constrain(48, 15<<2, 5+2) = %d" % constrain(48, 15<<2, 5+2)
print "    10bpp: constrain(49, 15<<2, 5+2) = %d" % constrain(49, 15<<2, 5+2)
print "    12bpp: constrain(192, 15<<4, 5+4) = %d" % constrain(192, 15<<4, 5+4)
print "    12bpp: constrain(193, 15<<4, 5+4) = %d" % constrain(193, 15<<4, 5+4)
print

print "  Secondary taps:"
print "     8bpp: constrain(4, 4, 5) = %d" % constrain(4, 4, 5)
print "     8bpp: constrain(5, 4, 5) = %d" % constrain(5, 4, 5)
print "    10bpp: constrain(15, 4<<2, 5+2) = %d" % constrain(15, 4<<2, 5+2)
print "    10bpp: constrain(16, 4<<2, 5+2) = %d" % constrain(16, 4<<2, 5+2)
print "    12bpp: constrain(57, 4<<4, 5+4) = %d" % constrain(57, 4<<4, 5+4)
print "    12bpp: constrain(58, 4<<4, 5+4) = %d" % constrain(58, 4<<4, 5+4)

