################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

Div_Table = [0, 840, 420, 280, 210, 168, 140, 120, 105]

# Count how many pixels contribute to each count entry
def find_dir(block):
  partial = [[0 for j in range(15)] for i in range(8)]
  for i in range(8):
    for j in range(8):
      x = block[i][j] - 128
      partial[0][i + j] += x
      partial[1][i + j // 2] += x
      partial[2][i] += x
      partial[3][3 + i - j // 2] += x
      partial[4][7 + i - j] += x
      partial[5][3 - i // 2 + j] += x
      partial[6][j] += x
      partial[7][i // 2 + j] += x

  for i in range(8):
    print i, partial[i]
  print

  cost = [0] * 8

  for i in range(8):
    cost[2] += (partial[2][i]) * (partial[2][i])
    cost[6] += (partial[6][i]) * (partial[6][i])

  print cost[2], cost[6]

  cost[2] *= Div_Table[8]
  cost[6] *= Div_Table[8]
  print cost[2], cost[6]

  for i in range(7):
    cost[0] += (partial[0][i] * partial[0][i] + partial[0][14 - i] * partial[0][14 - i]) * Div_Table[i + 1]
    cost[4] += (partial[4][i] * partial[4][i] + partial[4][14 - i] * partial[4][14 - i]) * Div_Table[i + 1]

  print cost[0], cost[4]

  cost[0] += partial[0][7] * partial[0][7] * Div_Table[8]
  cost[4] += partial[4][7] * partial[4][7] * Div_Table[8]

  print cost[0], cost[4]

  for i in range(1, 8, 2):
    for j in range(0, 4+1):
      cost[i] += partial[i][3 + j] * partial[i][3 + j]

  print (cost[1], cost[3], cost[5], cost[7])

  for i in range(1, 8, 2):
    cost[i] *= Div_Table[8]

  print (cost[1], cost[3], cost[5], cost[7])

  for i in range(1, 8, 2):
    for j in range(0, 4-1):
      cost[i] += (partial[i][j] * partial[i][j] + partial[i][10 - j] * partial[i][10 - j]) * Div_Table[2 * j + 2]

  print (cost[1], cost[3], cost[5], cost[7])

  max_idx = 0
  max_cost = 0
  for i in range(8):
    if cost[i] > max_cost:
      max_cost = cost[i]
      max_idx = i
  var = (cost[max_idx] - cost[(max_idx+4)&7]) >> 10
  print "Max cost is %d at index %d; variance = %d" % (max_cost, max_idx, var)
  print

# Minimum - for a block of all 128s, all intermediate values should be 0
block = [[128 for j in range(8)] for i in range(8)]
print "Minimum costs:"
find_dir(block)

# Maximum - since all costs are calculated as sums of squares, we want the largest
# possible absolute values in 'partial'. This occurs for a block of all 0s, where
# the value (px - 128) for each pixel is -128. This is slightly better than a block
# of all 255s, where the value is only +127.
block = [[0 for j in range(8)] for i in range(8)]
print "Maximum costs:"
find_dir(block)

# Then, for each of the 8 directions, we want to build a block which
# simultaneously maximizes the cost of that direction and minimizes
# the cost of the direction at 90 degrees to it. This is more difficult.
f = lambda idx: 255 if (idx & 1) else 0

max_block = [[[0 for j in range(8)] for i in range(8)] for k in range(8)]
for i in range(8):
  for j in range(8):
    max_block[0][i][j] = f(i + j)
    max_block[1][i][j] = f(i + j // 2)
    max_block[2][i][j] = f(i)
    max_block[3][i][j] = f(3 + i - j // 2)
    max_block[4][i][j] = f(7 + i - j)
    max_block[5][i][j] = f(3 - i // 2 + j)
    max_block[6][i][j] = f(j)
    max_block[7][i][j] = f(i // 2 + j)

for k in range(8):
  print "Trying to maximize direction %d" % k
  for i in range(8):
    print i, max_block[k][i]
  print
  find_dir(max_block[k])
