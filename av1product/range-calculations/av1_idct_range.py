################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import sys

Cos128_Lookup = [
    4096, 4095, 4091, 4085, 4076, 4065, 4052, 4036,
    4017, 3996, 3973, 3948, 3920, 3889, 3857, 3822,
    3784, 3745, 3703, 3659, 3612, 3564, 3513, 3461,
    3406, 3349, 3290, 3229, 3166, 3102, 3035, 2967,
    2896, 2824, 2751, 2675, 2598, 2520, 2440, 2359,
    2276, 2191, 2106, 2019, 1931, 1842, 1751, 1660,
    1567, 1474, 1380, 1285, 1189, 1092, 995, 897,
    799, 700, 601, 501, 401, 301, 201, 101, 0
]

def cos128(angle):
  angle2 = angle & 255
  if ( angle2 <= 64 ):
    return Cos128_Lookup[ angle2 ]
  elif ( angle2 <= 128 ):
    return Cos128_Lookup[ 128 - angle2 ] * -1
  elif ( angle2 <= 192 ):
    return Cos128_Lookup[ angle2 - 128 ] * -1
  else:
    return Cos128_Lookup[ 256 - angle2 ]

def sin128( angle ):
    return cos128( angle - 64 )

def clamp(x, lo, hi):
  if x < lo:
    return lo
  elif x > hi:
    return hi
  else:
    return x

def Clip3(lo, hi, x):
  return clamp(x, lo, hi)

def Round2(x, bits):
  r = (1 << bits) >> 1
  return (x + r) >> bits

def CHECK(v, msg):
  if not v:
    raise ValueError(msg)

def brev( numBits, x ):
    t = 0
    for i in range(numBits):
        bit = (x >> i) & 1
        t += bit << (numBits - 1 - i)
    return t

def inverse_dct_array_permutation( n, T ):
    copyT = [0] * 64
    n0 = 1 << n
    for i in range(n0):
        copyT[ i ] = T[ i ]
    for i in range(n0):
        T[ i ] = copyT[ brev( n, i ) ]

def B( xa, xb, angle, rev, r, T ):
    tmp = angle & 255

    x = T[ xa ] * cos128( angle ) - T[ xb ] * sin128( angle )
    y = T[ xa ] * sin128( angle ) + T[ xb ] * cos128( angle )

    if angle == 32 and x == -2147485360:
      if output:
        print "  angle=32 min x"
      interesting.add(origBlock)
    if angle == 32 and y == -2147476672:
      if output:
        print "  angle=32 min y"
      interesting.add(origBlock)
    if angle == 32 and x == 2147476672:
      if output:
        print "  angle=32 max x"
      interesting.add(origBlock)
    if angle == 32 and y == 2147476672:
      if output:
        print "  angle=32 max y"
      interesting.add(origBlock)

    if angle == 48 and x == -2147485696:
      if output:
        print "  angle=48 min x"
      interesting.add(origBlock)
    if angle == 48 and y == -2147485696:
      if output:
        print "  angle=48 min y"
      interesting.add(origBlock)
    if angle == 48 and x == 2147481596:
      if output:
        print "  angle=48 max x"
      interesting.add(origBlock)
    if angle == 48 and y == 2147481596:
      if output:
        print "  angle=48 max y"
      interesting.add(origBlock)

    T[ xa ] = Round2( x, 12 )
    T[ xb ] = Round2( y, 12 )

    if rev:
        tmp = T[ xa ]
        T[ xa ] = T[ xb ]
        T[ xb ] = tmp

    CHECK( T[ xa ] >= -( 1 << ( r - 1 ) ), "Value out of range in B()")
    CHECK( T[ xa ] <   ( 1 << ( r - 1 ) ), "Value out of range in B()")
    CHECK( T[ xb ] >= -( 1 << ( r - 1 ) ), "Value out of range in B()")
    CHECK( T[ xb ] <   ( 1 << ( r - 1 ) ), "Value out of range in B()")

def H( xa, xb, rev, r, T ):
    if rev:
        tmp = xa
        xa = xb
        xb = tmp
    x = T[ xa ]
    y = T[ xb ]
    u = x+y
    v = x-y
    if u == 524286 + 524287:
      if output:
        print "  H max u"
      interesting.add(origBlock)
    if v == 524286 - -524288:
      if output:
        print "  H max v"
      interesting.add(origBlock)
    if u == -524288 + -524288:
      if output:
        print "  H min u"
      interesting.add(origBlock)
    if v == -524288 - 524287:
      if output:
        print "  H min v"
      interesting.add(origBlock)
    T[ xa ] = Clip3( - ( 1 << ( r - 1 ) ), ( 1 << ( r - 1 ) ) - 1, u )
    T[ xb ] = Clip3( - ( 1 << ( r - 1 ) ), ( 1 << ( r - 1 ) ) - 1, v )

def inverse_dct4( r, T ):
    inverse_dct_array_permutation( 2, T )
    B( 0, 1, 32, 1, r, T )
    B( 2, 3, 48, 0, r, T )
    H( 0, 3, 0, r, T )
    H( 1, 2, 0, r, T )
    return T

def bsearch_(lo, hi, f, sense):
  assert lo <= hi
  if lo == hi:
    return lo

  mid = (lo + hi + 1) // 2

  if sense: # Searching for a transition true -> false
    if f(mid):
      # Midpoint is true, so try the top half (including mid)
      return bsearch_(mid, hi, f, sense)
    else:
      # Midpoint is false, so try the bottom half (excluding mid)
      return bsearch_(lo, mid-1, f, sense)
  else: # Searching for a transition false -> true
    if not f(mid):
      # Midpoint is false, so try the top half (including mid)
      return bsearch_(mid, hi, f, sense)
    else:
      # Midpoint is true, so try the bottom half (excluding mid)
      return bsearch_(lo, mid-1, f, sense)

def bsearch(lo, hi, f):
  if f(lo):
    sense = 1
    if f(hi): return
  else:
    sense = 0
    if not f(hi): return

  return bsearch_(lo, hi, f, sense)

uMax = 0
uMaxX = 0
uMaxY = 0
uMaxV = 0
vMax = 0
vMaxX = 0
vMaxY = 0
vMaxU = 0
uMin = 0
uMinX = 0
uMinY = 0
uMinV = 0
vMin = 0
vMinX = 0
vMinY = 0
vMinU = 0

def test1():
  global uMax, vMax, uMaxX, uMaxY, vMaxX, vMaxY, uMin, vMin, uMinX, uMinY, vMinX, vMinY
  # Find the inputs which minimize/maximize the outputs from the two B() operations
  for angle in (32, 48):
    for bd in (10, 12):
      a = cos128(angle)
      b = sin128(angle)
      dqv = 4
      lo = -(1 << (bd+7))
      hi = (1 << (bd+7))-1
      # Calculate the smallest quantized values which will be clamped to lo or hi respectively
      loQ = lo // dqv
      hiQ = (hi + (dqv-1)) // dqv

      uMax = 0
      uMaxX = 0
      uMaxY = 0
      uMaxV = 0
      vMax = 0
      vMaxX = 0
      vMaxY = 0
      vMaxU = 0
      uMin = 0
      uMinX = 0
      uMinY = 0
      uMinV = 0
      vMin = 0
      vMinX = 0
      vMinY = 0
      vMinU = 0

      def inner(yQ):
        global uMax, vMax, uMaxX, uMaxY, vMaxX, vMaxY, uMin, vMin, uMinX, uMinY, vMinX, vMinY
        x = clamp(xQ * dqv, lo, hi)
        y = clamp(yQ * dqv, lo, hi)
        u = a*x - b*y
        v = b*x + a*y
        u_ = Round2( u, 12 )
        v_ = Round2( v, 12 )
        try:
          CHECK( u_ >= lo, "Value out of range in B()")
          CHECK( u_ <= hi, "Value out of range in B()")
          CHECK( v_ >= lo, "Value out of range in B()")
          CHECK( v_ <= hi, "Value out of range in B()")
        except ValueError:
          return False
        # Valid, so update limits
        if u > uMax:
          uMax = u
          uMaxX = x
          uMaxY = y
          uMaxV = v
        if v > vMax:
          vMax = v
          vMaxX = x
          vMaxY = y
          vMaxU = u
        if u < uMin:
          uMin = u
          uMinX = x
          uMinY = y
          uMinV = v
        if v < vMin:
          vMin = v
          vMinX = x
          vMinY = y
          vMinU = u
        return True

      # Explore the entire boundary of allowable inputs,
      # with the first parameter going outward from zero
      for xQ in range(0, hiQ+1):
        bsearch(0, hiQ, inner)
        bsearch(loQ, 0, inner)

      for xQ in range(0, loQ-1, -1):
        bsearch(0, hiQ, inner)
        bsearch(loQ, 0, inner)

      print "angle=%d, bd=%d, dqv=%d:" % (angle, bd, dqv)
      print "Min u: x = %d, y = %d => u = %d, v = %d" % (uMinX, uMinY, uMin, uMinV)
      print "Min v: x = %d, y = %d => u = %d, v = %d" % (vMinX, vMinY, vMinU, vMin)
      print "Max u: x = %d, y = %d => u = %d, v = %d" % (uMaxX, uMaxY, uMax, uMaxV)
      print "Max v: x = %d, y = %d => u = %d, v = %d" % (vMaxX, vMaxY, vMaxU, vMax)
      print

output = False
origBlock = None
interesting = set()

def test2():
  global origBlock, output
  # Find the correct permutations of inputs to hit the min/max values
  # of the B() and H() operations
  import itertools

  bd = 12

  # Try all 16 combinations of the limiting blocks from earlier
  if bd == 10:
    first = [
      (-54316, 131068),
      (-54312, -131072),
      (54308,  -131072),
      (54312,  131068)
    ]
    second = [
      (-40288, 125196),
      (-93856, -115968),
      (30404,  -129288),
      (91680,  121220)
    ]
  elif bd == 12:
    first = [
      (-217248, 524287),
      (-217244, -524288),
      (217244,  -524288),
      (217248,  524284)
    ]
    second = [
      (-113248, 520620),
      (-351384, -521920),
      (118500,  -518444),
      (355476,  512036)
    ]
  else:
    assert ValueError("Unexpected bit depth")

  # First pass - search for "interesting" blocks
  # (ie, ones which hit at least one of the extrema)
  for (f, s) in itertools.product(first, second):
    base = (f[0], f[1], s[0], s[1])
    for b in itertools.permutations(base):
      origBlock = b
      res = list(b)
      try:
        inverse_dct4(bd+8, res)
      except:
        pass

  # Second pass - iterate over the interesting blocks,
  # printing *which* extrema each one hits
  # I then manually filtered this list to give a minimal
  # set of 4 blocks which hit all of the limits we need
  output=True

  for b in interesting:
    origBlock = b
    res = list(b)
    try:
      print "block =", b
      inverse_dct4(bd+8, res)
      print "  res =", res
    except:
      print "  (error)"

if __name__ == "__main__":
  test2()
