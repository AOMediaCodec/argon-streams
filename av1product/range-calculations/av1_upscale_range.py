################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# Range of luma widths to consider.
# We should only need to consider a smallish range
# above the (per-profile) minimum width, because
# for larger widths 'stepX' will be close to
# (8 / superresDenom) * 2^14
#MIN_WIDTH = 384
#MAX_WIDTH = MIN_WIDTH + 384
MIN_WIDTH = 16
MAX_WIDTH = MIN_WIDTH + 64


SUPERRES_SCALE_BITS = 14

def main():
  max_luma = None
  max_chroma = None

  for superresDenom in range(9, 17):
    for upscaledWidth in range(MIN_WIDTH, MAX_WIDTH):
      FrameWidth = (upscaledWidth * 8 + (superresDenom // 2)) // superresDenom
      chromaUpscaledWidth = (upscaledWidth + 1) // 2
      chromaWidth = (FrameWidth + 1) // 2
      if FrameWidth < MIN_WIDTH:
        continue

      lumaStepX = ((FrameWidth << SUPERRES_SCALE_BITS) + (upscaledWidth // 2)) // upscaledWidth
      chromaStepX = ((chromaWidth << SUPERRES_SCALE_BITS) + (chromaUpscaledWidth // 2)) // chromaUpscaledWidth

      if max_luma is None or lumaStepX > max_luma[0]:
        max_luma = (lumaStepX, superresDenom, upscaledWidth, FrameWidth)

      if max_chroma is None or chromaStepX > max_chroma[0]:
        max_chroma = (chromaStepX, superresDenom, upscaledWidth, FrameWidth, chromaUpscaledWidth, chromaWidth)

  if max_chroma[0] > max_luma[0]:
    # Profiles 0 and 2 can use X subsampling, so their maximum values
    # are given by max_chroma
    print("Profile 0/2: max stepX = {} for chroma planes,\n"
           " when superresDenom = {}, upscaledWidth = {}\n"
           " => FrameWidth = {}, chromaUpscaledWidth = {}, chromaWidth = {}".format(*max_chroma))

    print

    # Profile 1 cannot use subsampling, so always uses max_luma
    print("Profile  1 : max stepX = {} for luma plane,\n"
           " when superresDenom = {}, upscaledWidth = {}\n"
           " => FrameWidth = {}".format(*max_luma))
  else:
    # In this case, the max always occurs on the luma plane
    print("All profiles: max stepX = %d for luma plane, when superresDenom = %d, upscaledWidth = %d\n"
           " => FrameWidth = %d") % max_luma

if __name__ == "__main__":
  main()
