#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This script is used for Jenkins jobs that fetch and build libaom and
# then decode some of a release to make sure that nothing has come too
# badly unstuck.

set -o errexit -o pipefail -o nounset

usage () {
    echo "Usage: libaom-jenkins.sh [-h] [-j <jobs>] [--limit L]"
    echo "                         rel_zip branch [branch [branch ..]]"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hj:
LONGOPTS=help,jobs:,limit:

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

jobs_args=()
limit_args=()

while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            jobs_args=(-j "$2")
            shift 2
            ;;
        --limit)
            limit_args=(--limit "$2")
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# We just have positional arguments left. There should be at least 2
# (rel_zip and a branch).
if [ $# -lt 2 ]; then
    echo 1>&2 "Wrong number of positional arguments."
    usage 1
fi

rel_zip="$1"
shift

branches=("$@")

# rel_zip is the full path to a zipped up release. We'll unzip to
# ./releases. Using a subdir like this means that if releases/rel_name
# doesn't exist, we can delete releases and start again. This way, we
# don't leak disk space if we change release.
base="$(basename "$rel_zip" .zip)"
rel_dir="releases/$base"
if [ ! -d "$rel_dir" ]; then
    rm -rf releases
    mkdir releases
    echo "Unpacking $rel_zip to releases"
    unzip -qd releases "$rel_zip"

    test -d "$rel_dir" || error "Unzipping $rel_zip didn't create $rel_dir."
fi

av1product_dir="$(dirname ${BASH_SOURCE})"
product_dir="${av1product_dir}/../product"

abs_av1product_dir="$(readlink -f "${av1product_dir}")"
abs_product_dir="$(readlink -f "${product_dir}")"
abs_rel_dir="$(readlink -f "${rel_dir}")"

for branch in "${branches[@]}"; do
    mkdir -p "$branch"

    # Get and build the version of aomdec that we need to
    # $branch/aom-build
    mkdir -p aom
    "${av1product_dir}/get-aomdec.sh" \
        ${jobs_args[@]+"${jobs_args[@]}"} \
        --refspec="remotes/aom/$branch" --fetch --just-fetch="$branch" \
        aom/src "$branch/aom-build"

    echo -n "Branch $branch is at SHA: "
    cat "$branch/aom-build/.built-sha"

    # Use symlinks to cherry-pick the binaries that we need from
    # aom/build, giving them the silly names that we need for
    # test-one-file.sh (which has to match autoproduct)
    rm -rf "$branch/bin"
    mkdir -p "$branch/bin"
    ln -s ../aom-build/aomdec \
       "$branch/bin/av1_ref_c.exe"
    ln -s ../aom-build/examples/lightfield_tile_list_decoder \
       "$branch/bin/av1_ref_c_lst.exe"

    # Finally, run check_release_files.py to actually do some decode
    # checks. The --decoders=libaom extra argument is passed through to
    # test-one-file.sh to tell it to only bother testing libaom.
    #
    # We use with-seed.sh to check that we repeat a seed if something
    # has gone awry and run everything from within $branch so that
    # .last-seed doesn't get trashed by other branches.
    (cd "$branch";
     "${abs_av1product_dir}/with-seed.sh" \
         "${abs_product_dir}/check_release_files.py" \
         ${jobs_args[@]+"${jobs_args[@]}"} \
         ${limit_args[@]+"${limit_args[@]}"} \
         --seed {} \
         --no-progress \
         --extra-args="--decoders=libaom" \
         bin \
         "${abs_av1product_dir}/test-one-file.sh" \
         "${abs_rel_dir}")
done
