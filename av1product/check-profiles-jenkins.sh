#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This script is used for Jenkins jobs that generate some streams with
# a given set of profiles and check that they encode and decode
# correctly.
#
# It creates an ./aom directory at toplevel for libaom.

set -o errexit -o pipefail -o nounset

usage () {
    echo "Usage: check-profiles-jenkins.sh [-h] [-j <jobs>]"
    echo "                                 [--clean]"
    echo "                                 [--default-seed S]"
    echo "                                 profile:N[:S] [profile:N[:S]... ]"
    echo ""
    echo "  Where profile is the name of a profile. N is the number of streams"
    echo "  to generate for that profile. If set, S is the starting seed. If"
    echo "  not, we'll start at --default-seed (if this is not specified, we"
    echo "  pick it at random)."
    echo ""
    echo "  If the --clean argument is given, we run git clean -fxd in the"
    echo "  hevc checkout before doing anything else. This is probably the"
    echo "  right thing to do if this is genuinely a Jenkins job."
    echo

    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hj:
LONGOPTS=help,jobs:,clean,default-seed:

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

jobs_args=()
default_seed="$RANDOM"
clean=0

while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            jobs_args=(-j "$2")
            shift 2
            ;;
        --clean)
            clean=1
            shift
            ;;
        --default-seed)
            default_seed="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# We just have positional arguments left. There should be at least 1
# profile.
if [ $# = 0 ]; then
    echo 1>&2 "No positional arguments."
    usage 1
fi

av1product_dir="$(dirname ${BASH_SOURCE})"
hevc_dir="${av1product_dir}/.."
tools_dir="${hevc_dir}/tools"

if [ $clean = 1 ]; then
    (cd "$hevc_dir"; git clean -fxd)
fi

# Get and build a copy of aomdec
echo "Making sure we have a copy of libaom"
mkdir -p aom
"${av1product_dir}/get-aomdec.sh" \
    ${jobs_args[@]+"${jobs_args[@]}"} aom/src "aom/build"

# Add a symlink in tools/build-aom to aom/build
rm -f "$tools_dir/build-aom"
ln -s "$(readlink -f aom/build)" "$tools_dir/build-aom"

n=0
for pf in "$@"; do
    # Check that pf is of the form profile_name:N[:S] and extract the
    # parts.
    IFS=: read prof limit seed rubbish <<EOF
$pf
EOF
    if [ -z "$limit" -o -n "$rubbish" ]; then
        error "Profile argument '$pf' is not of the form profile:N"
    fi

    # Do we have a starting seed? If not, we use default_seed.
    seed="${seed:-$default_seed}"

    echo "Testing $limit streams for profile $prof starting at $seed"
    (cd "$av1product_dir";
     ./parallel_makeall \
         -c -nowait ${jobs_args[@]+"${jobs_args[@]}"} \
         -start "$seed" -block "$limit" -reftest -av1b \
         -all-oppoints "$prof")

    n=$((n + 1))
done
