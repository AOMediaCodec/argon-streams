#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# A v2.0 release script for use in a Jenkins job
#
# Note that this needs to run on an Argon machine that can see
# /home/public/projects (in order to get a copy of the v1.3 release).
#
# It assumes that it runs with the following directory structure:
#
#    error-streams/release.tar.gz    (results of an error streams run)
#    hevc/                           (a checkout of the streams repository)

set -o errexit -o pipefail -o nounset

usage () {
    echo "Usage: release-2.0-jenkins.sh [-h] [-j <jobs>] [-c]"
    echo "                              [--uzips <dir>] [--coverbin <binary>]"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hj:c
LONGOPTS=help,jobs:,continue,uzips:,coverbin:

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

JOBS_ARG=""
CONTINUE_ARG=""
CB_ARGS=()

# Where can we find the v1.3 release? This might be overridden on the
# command line.
uzips="/home/public/projects/P8005_Hevc/Uploaded zips"

while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            JOBS_ARG="-j$2"
            shift 2
            ;;
        -c|--continue)
            CONTINUE_ARG=--continue
            shift
            ;;
        --uzips)
            uzips="$2"
            shift 2
            ;;
        --coverbin)
            CB_ARGS=(--coverbin "$2")
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

zip_base=argon_streams_av1_base_and_extended_profiles_v1.3

# Cleanups: The previous run might have made error-streams/release by
# un-tarring a previous result, so we should delete that.
#
# Also, unzipping the v1.3 release might yield $zip_base (which we
# should always delete) or a directory called rel-1.3 (depending on
# whether something went wrong with the unzip). Get rid of all of
# that.
#
# We work in the work/ subdirectory. If not continuing, delete that if
# it already exists.
#
# Finally, use git clean to get rid of any rubbish in the checkout.
rm -rf "$zip_base" error-streams/release
(cd hevc; git clean -fxd >/dev/null 2>&1)

if [ -z "$CONTINUE_ARG" ]; then
    rm -rf rel-1.3 work
fi

# Use the get-aomdec script to make sure we have a copy of libaom
# (we'll build it when running autoproduct later)
mkdir -p public_repos
av1product_dir="$(dirname ${BASH_SOURCE})"
${av1product_dir}/get-aomdec.sh public_repos/aom

# Un-tar the error stream release
(cd error-streams; tar xf release.tar.gz)

# Fetch, unzip and rename the v1.3 release, unless we're continuing
# and already have it.
if [ ! -e rel-1.3/.unborked ]; then
    rm -rf rel-1.3

    test -e "${zip_base}.zip" || cp "${uzips}/${zip_base}.zip" .
    unzip -q "${zip_base}.zip"
    mv "${zip_base}" rel-1.3

    # Fix known problems with the streams in the v1.3 release.
    (cd rel-1.3; ../hevc/av1product/unbork-release.sh $JOBS_ARG)

    touch rel-1.3/.unborked
fi

# Finally, run the main release-2.0 script.
exec hevc/av1product/release-2.0.sh \
     $JOBS_ARG $CONTINUE_ARG ${CB_ARGS[@]+"${CB_ARGS[@]}"} \
     rel-1.3 \
     error-streams/release \
     work
