#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This expects to run inside an unpacked (broken) version 1.3 release.
# The script file should live in the av1product directory, next to
# av1_fix_bitstreams.py.

set -o errexit -o pipefail -o nounset

usage () {
    echo "Usage: unbork-release.sh [-h] [-j <J>]"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hj:
LONGOPTS=help,jobs:

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

JOBS=1

# At this point, our command line arguments are in a sane order and
# easy to read, ending with a '--'.
while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            JOBS="${2}"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# We should have no positional arguments.
if [ $# != 0 ]; then
    echo 1>&2 "Wrong number of positional arguments."
    usage 1
fi

source="${BASH_SOURCE[0]}"
av1product="$(dirname "$source")"
fb="$av1product/av1_fix_bitstreams.py"

test -f "$fb" || {
    echo >&2 "Can't find script at $fb."
    exit 1
}

# Make sure we're sitting in a release
test -d ArgonViewerV14 || {
    echo >&2 "This doesn't look like a release."
}

# Fix #! lines in ref_cmd scripts
echo "Fixing ref_cmd shebangs"
find -wholename "*ref_cmd/*.sh" | xargs sed -i 's;^!/bin/bash;#&;'

echo "Adding missing temporal delimiters to non-annexb obus"
find -path '*not_annexb*.obu' -print0 | \
    xargs -r -0 -n1 -P "$JOBS" python2 "$fb" --not-annexb >/dev/null

echo "Adding missing temporal delimiters to annexb obus"
find -name '*.obu' '!' -path '*not_annexb*' -print0 | \
    xargs -r -0 -n1 -P "$JOBS" python2 "$fb" >/dev/null

echo "Fixing ill-formed file lists"
for lst in $(find -name '*.txt'); do
    # The read command fails if its input isn't newline terminated.
    # Since the tail command spits out exactly the last character of
    # the file, the read command fails unless the file is newline
    # terminated.
    tail -c1 $lst | read -r _ || {
        echo >>$lst
    }

    # Remove any carriage returns from the file
    sed -i 's/\r//g' $lst
done

if [ -d profile_switching ]; then
    echo "Fixing profile_switching file lists"
    rm profile_switching/*.txt
    ls profile_switching/streams >profile_switching/all_list.txt

    # TODO: What should be in the level5 and level6 lists? Probably
    #       everything, but should we be checking this properly?
    cp profile_switching/all_list.txt profile_switching/level5.x_list.txt
    cp profile_switching/all_list.txt profile_switching/level6.x_list.txt
fi

TMP="$(mktemp -d)"
trap "{ rm -rf $TMP; }" EXIT

for dir in profile1_not_annexb; do
    if [ -d $dir ]; then
        echo "Uniquifying lines in $dir file lists"
        for file in $dir/*.txt; do
            sort -u $file -o $file
        done
    fi
done

if [ -d profile2_large_scale_tile_special ]; then
    echo "Removing spurious entries in profile2_large_scale_tile_special"
    echo >"$TMP/bad.lst" "test4.obu"
    echo >>"$TMP/bad.lst" "test5.obu"
    echo >>"$TMP/bad.lst" "test6.obu"
    echo >>"$TMP/bad.lst" "test7.obu"
    echo >>"$TMP/bad.lst" "test8.obu"
    echo >>"$TMP/bad.lst" "test9.obu"
    sort "$TMP/bad.lst" -o "$TMP/bad.lst"
    for file in profile2_large_scale_tile_special/*.txt; do
        sort $file -o $file
        comm -13 "$TMP/bad.lst" $file >"$TMP/good.lst"
        mv "$TMP/good.lst" $file
    done
fi
