#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This script fetches the head of the dav1d decoder, builds it, and
# checks how well it works across a streams release.

set -o errexit -o pipefail -o nounset

usage () {
    echo "Usage: test-dav1d.sh [-h] [-j <jobs>] [-f] [-r rev] <build-dir> <streams>"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

###############################################################################
# Argument parsing
###############################################################################

OPTS=hj:r:f
LONGOPTS=help,jobs:,revision:,force-fetch

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

jobs_arg=-j1
procs_arg=--max-procs=1
revision=videolan/master
force_fetch=0

# At this point, our command line arguments are in a sane order and
# easy to read, ending with a '--'.
while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            jobs_arg="-j${2}"
            procs_arg="--max-procs=${2}"
            shift 2
            ;;
        -r|--revision)
            revision="${2}"
            shift 2
            ;;
        -f|--force-fetch)
            force_fetch=1
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# At this point, there should be exactly two positional arguments.
if [ $# != 2 ]; then
    echo >&2 "Wrong number of positional arguments."
    usage 1
fi

build_dir="$1"
streams_dir="$2"
shift 2

###############################################################################
# Fetching dav1d
###############################################################################

srcdir="$build_dir/dav1d"
mkdir -p "$srcdir" || error "Could not make source directory for dav1d."

declare -a GD
GD=(env GIT_DIR="$srcdir/.git")

# Make sure we have a .git directory at all
if [ ! -d "$srcdir/.git" ]; then
    echo "Initialising git directory in $srcdir"
    "${GD[@]}" git init -q
fi

# Make sure we have videolan as a remote
"${GD[@]}" git remote | grep -q videolan || {
    echo "Adding videolan remote"
    "${GD[@]}" git remote add videolan https://code.videolan.org/videolan/dav1d.git
}

# Decide whether to do a fetch. It's either forced with -f or its
# because we don't recognise the revision.
if [ $force_fetch == 1 ]; then
    echo "Fetching from repository."
else
    sha=$("${GD[@]}" git rev-parse -q --verify "$revision^{object}") || {
        echo "The revision '$revision' is unknown. Fetching from videolan."
        force_fetch=1
    }
fi

# If either we have forced a fetch or we don't recognise revision yet,
# fetch from videolan.
if [ $force_fetch == 1 ]; then
    "${GD[@]}" git fetch -q videolan

    # Do we know the sha now? Let's hope so.
    sha=$("${GD[@]}" git rev-parse -q --verify "$revision^{object}") || {
        echo "The revision '$revision' is unknown after fetching. Exiting."
        exit 1
    }
else
    echo "Revision '$revision' is already known; not fetching from remote"
    echo " (use -f to force a fetch)"
fi

needs_rebuild=0

# At this point, our copy of the dav1d repository has the data it
# needs. Now let's check out the right version if we didn't have it
# already.
head_sha=$("${GD[@]}" git rev-parse HEAD 2>&1 | head || true)
if [ "x$head_sha" != "x$sha" ]; then
    echo "HEAD isn't currently at '$revision'. Checking out right version."
    # Note we can't use GD here: if you override GIT_DIR, git
    # checkout uses the repository you say, but checks out in the
    # current directory.
    (cd "$srcdir"; git checkout -q "$sha")
    needs_rebuild=1
fi

###############################################################################
# Building dav1d
###############################################################################

dbuilddir="$build_dir/build-dav1d"
logdir="$build_dir/logs"
built_sha_file="$dbuilddir/.built-sha"
built_sha=''

mkdir -p "$logdir" || error "Failed to make log directory."

if [ $needs_rebuild == 0 -a -e "$built_sha_file" ]; then
    built_sha=$(head -n1 $built_sha_file || true)
fi

if [ x"$built_sha" == x"$sha" ]; then
    echo "Dav1d is already at correct version. Not rebuilding."
else
    needs_rebuild=1
fi

if [ $needs_rebuild == 1 ]; then
    mkdir -p "$dbuilddir" || error "Failed to make dav1d build dir"

    # We need to run meson to configure stuff unless
    # $dbuilddir/build.ninja exists.
    if [ ! -e "$dbuilddir/build.ninja" ]; then
        echo "Running meson"
        meson setup --buildtype release "$dbuilddir" "$srcdir" >&"$logdir/meson.log" || {
            error "Meson failed. Log file at '$logdir/meson.log'."
        }
    fi

    # Now run ninja in the build directory to actually do the build.
    echo "Building dav1d using ninja"
    ninja "$jobs_arg" -C "$dbuilddir" >&"$logdir/ninja.log" || {
        error "Building with ninja failed. Log file at '$logdir/ninja.log'"
    }

    echo "$sha" >"$built_sha_file"
fi

###############################################################################
# Running dav1d
###############################################################################

AV1PRODUCT_DIR="$(dirname ${BASH_SOURCE})"
run_dav1d="$AV1PRODUCT_DIR/run-dav1d.sh"
test -f "$run_dav1d" || error "Can't find dav1d runner script at $run_dav1d"

dav1d="$dbuilddir/tools/dav1d"
test -f "$dav1d" || error "No dav1d binary at $dav1d"

md5s="$build_dir/md5s"

echo "Clearing any existing MD5 files"
rm -rf "$md5s" || error "Couldn't clear md5s from $md5s"
mkdir -p "$md5s" || error "Couldn't make MD5 output directory"

obu_lst="$build_dir/obus.lst0"

(cd $streams_dir; \
 find -name '*.obu' -not -path '*not_annexb*' -print0) >"$obu_lst"

echo "Will run dav1d on $(tr '\0' '\n' <"$obu_lst" | wc -l) streams"
xargs <"$obu_lst" -t -r -n1 -0 "$procs_arg" \
      "$run_dav1d" "$dav1d" "$streams_dir" "$md5s" || {
    echo "WARNING: One or more dav1d runs failed."
}

###############################################################################
# Comparing trees
###############################################################################

check_md5="$AV1PRODUCT_DIR/check-md5.sh"
test -f "$check_md5" || error "Can't find MD5 checking script at $check_md5"

echo "Comparing MD5 files with streams release"
(cd "$md5s"; find -name '*.md5' -not -path '*not_annexb*' -print0) |
    xargs -r -n1 -0 "$procs_arg" \
          "$check_md5" "$streams_dir" "$md5s"

(cd "$md5s"; find -name '*.bad' -print0) >"$build_dir/bad.lst0"
(cd "$md5s"; find -name '*.good' -print0) >"$build_dir/good.lst0"

nbad="$(tr '\0' '\n' <"$build_dir/bad.lst0" | wc -l)"
ngood="$(tr '\0' '\n' <"$build_dir/good.lst0" | wc -l)"

echo "There were $nbad bad MD5s and $ngood good ones."
test nbad == 0
