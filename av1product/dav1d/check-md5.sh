#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

set -e

# This is a simple script used by test-dav1d.sh. It takes the
# following arguments:
#
#   streams-dir
#   md5-dir
#   md5-path

fail () {
    echo >&2 "$@"
    exit 1
}

if [ $# != 3 ]; then
    fail "Wrong number of positional arguments"
fi

sdir="$1"
odir="$2"
md5="$3"
shift 3

# md5 looks something like profile_foo/md5_ref/test123.md5. We need to
# check whether $sdir/$md5 and $odir/$md5 differ. If so, we touch
# $odir/$md5.bad. If not, we touch $odir/$md5.good.

if diff -q "$sdir/$md5" "$odir/$md5" >/dev/null 2>&1; then
    suffix=good
else
    suffix=bad
fi

touch "$odir/$md5.$suffix"
