#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

set -e

# This is a simple script used by test-dav1d.sh. It takes the
# following arguments:
#
#   dav1d-binary
#   streams-dir
#   output-dir
#   stream

fail () {
    echo >&2 "$@"
    exit 1
}

if [ $# != 4 ]; then
    fail "Wrong number of positional arguments"
fi

dav1d="$1"
sdir="$2"
odir="$3"
strm="$4"
shift 4

# strm looks something like profile_foo/streams/test123.obu. We want
# to read $sdir/$strm.
#
# The md5s that we produce should be at these files:
#
#   profile_foo/md5_no_film_grain/test123.md5
#   profile_foo/md5_ref/test123.md5
#
# and we want to append "out.yuv" to each of the md5 lines to match
# the format of our streams releases.

in="$sdir/$strm"

nfg="$(echo "$strm" | sed 's!/streams/\(.*\)\.obu!/md5_no_film_grain/\1.md5!')"
ref="$(echo "$strm" | sed 's!/streams/\(.*\)\.obu!/md5_ref/\1.md5!')"

# Check that we actually made a replacement for each of the output
# paths.
echo "$nfg" | grep -q 'md5$' || fail "Failed to rename $in to nfg"
echo "$ref" | grep -q 'md5$' || fail "Failed to rename $in to ref"

nfg_path="$odir/$nfg"
ref_path="$odir/$ref"

# Make sure that directories exist to put the MD5s in
nfg_dir="$(dirname "$nfg_path")"
ref_dir="$(dirname "$ref_path")"
mkdir -p "$nfg_dir" || fail "Can't make directory '$nfg_dir' for nfg MD5"
mkdir -p "$ref_dir" || fail "Can't make directory '$ref_dir' for ref MD5"

# Run dav1d to produce the two MD5 outputs, logging the results to
# adjacent log files.
declare -a nfg_cmd
declare -a ref_cmd
nfg_cmd=("$dav1d" --muxer md5 -o "$nfg_path" -i "$in" --filmgrain 0)
ref_cmd=("$dav1d" --muxer md5 -o "$ref_path" -i "$in" --filmgrain 1)

nfg_log="$(echo "$nfg_path" | sed 's!.md5$!.log!')"
ref_log="$(echo "$ref_path" | sed 's!.md5$!.log!')"

status=0

echo "${nfg_cmd[@]}" >"$nfg_log"
echo >>"$nfg_log"
(time "${nfg_cmd[@]}") >>"$nfg_log" 2>&1 || {
    echo "Command failed" >>"$nfg_log"
    rm -f "$nfg_path" || true
    status=1
}
if [ -e "$nfg_path" ]; then
    sed -i 's!$!  out.yuv!' "$nfg_path"
fi

echo "${ref_cmd[@]}" >"$ref_log"
echo >>"$ref_log"
(time "${ref_cmd[@]}") >>"$ref_log" 2>&1 || {
    echo "Command failed" >>"$ref_log"
    rm -f "$ref_path" || true
    status=1
}
if [ -e "$ref_path" ]; then
    sed -i 's!$!  out.yuv!' "$ref_path"
fi


exit $status
