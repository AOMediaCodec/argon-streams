#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# Run each decoder on a file and check they all give the same result
# as the MD5 that's stored.

set -o errexit -o pipefail -o nounset

usage () {
    echo "Usage: test-one-file.sh [--ensure] [--csv=<file>]"
    echo "                        [--decoders=list,of,decoders]"
    echo "                        [--just-layer-0]"
    echo "                        <bin-dir> <set-dir> <base> <layer>"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

check_yuv_md5 () {
    local TMP="$1"
    local md5="$2"
    local what="$3"
    local ensure="$4"

    (cd "$TMP"; md5sum "out.yuv" >"md5") || \
        error "$what Failed to create md5sum $md5 of '$TMP/out.yuv'."

    if [ ! -e "$md5" ]; then
        if [ $ensure = 1 ]; then
            # We don't have an MD5 for this stream yet. No problem -
            # we'll create it now. Note that we only ever pass
            # ensure=1 for the libaom tests: we'll check that av1 and
            # av1b have the same md5 in a sec.
            mkdir -p "$(dirname "$md5")" || \
                error "Failed to create directory for missing md5: $md5"

            cp "$TMP/md5" "$md5" || \
                error "Couldn't copy computed md5 to $md5"
        else
            error "Missing md5 file: $md5"
        fi
    else
        diff -q "$md5" "$TMP/md5" || {
            diff -U3 "$md5" "$TMP/md5" 2>&1 || true
            echo -e "\n" 2>&1
            error "MD5 checksum difference for $3."
        }
    fi
    if [ x"$layer" == x0 ]; then
        if grep -q "d41d8cd98f00b204e9800998ecf8427e" "$md5"; then
            echo "$md5 MD5 of an empty stream, no frames found for $3"
        fi
    fi
}

check_libaom () {
    local TMP="$1"
    local ref_cmd="$2"
    local set_dir="$3"
    local bin_dir="$4"
    local md5_ref="$5"
    local md5_nfg="$6"
    local ensure="$7"
    local is_lst="$8"

    # First binary to try is the libaom decoder. The command for that can
    # be found in the ref_cmd directory.
    if [ ! -e "$ref_cmd" ]; then
        error "No such ref_cmd script: $ref_cmd"
    fi

    # We want to do the minimum editing possible of the ref_cmd script,
    # and we also want to run it (rather than running "bash foo.sh"), to
    # make sure we've got the shebang line right etc.
    #
    # It won't be executable (because we make releases from zip files,
    # which don't have permission bits), so copy the ref_cmd to the temp
    # dir and chmod it.
    cp "$ref_cmd" "$TMP/ref_cmd.sh"
    chmod 0755 "$TMP/ref_cmd.sh"

    # We have another problem, which is that the binaries that autoproduct
    # generate have different names and paths from those that the ref_cmd
    # expects. Make some funky symlinks to sort that out.
    mkdir -p "$TMP/bin/examples"
    test -x "$bin_dir/av1_ref_c.exe" || \
        error "No executable at $bin_dir/av1_ref_c.exe"
    test -x "$bin_dir/av1_ref_c_lst.exe" || \
        error "No executable at $bin_dir/av1_ref_c_lst.exe"

    ln -s "$(readlink -f "$bin_dir/av1_ref_c.exe")" \
       "$TMP/bin/aomdec"

    ln -s "$(readlink -f "$bin_dir/av1_ref_c_lst.exe")" \
       "$TMP/bin/examples/lightfield_tile_list_decoder"

    local abs_set_dir="$(readlink -f "$set_dir")"

    # At this point, we should be able to run ref_cmd.sh to generate the
    # (with film grain) md5. We run it from the temporary directory, since
    # the lightfield tile list decoder generates some other cruft
    # (reference YUV files) in cwd by default.
    (mkdir -p "$TMP/libaom";
     cd "$TMP/libaom";
     env LIBAV1="../bin" \
         ARGON_STREAMS_INPUT="$abs_set_dir/streams" \
         ARGON_STREAMS_OUTPUT=".." \
         "../ref_cmd.sh" >/dev/null || \
         {
             error "Failed to run ref_cmd.sh $TMP $ref_cmd $set_dir $md5_ref"
         })

    check_yuv_md5 "$TMP" "$md5_ref" "ref_cmd (with film grain)" "$ensure"

    # If this is a large scale tile stream, there's no need to do a
    # no-film-grain version. When we run check_yuv_md5 below, it will pick
    # up the results without a --skip-film-grain flag. Otherwise, we need
    # to do something.
    if [ $is_lst = 0 ]; then
        # Now we want to do the no-film-grain version, for which we need
        # to add --skip-film-grain. We do this by adding the argument to
        # the first space after LIBAV1. This position should make sure
        # it's not affected by presence or absence of quotes or similar.
        sed -i -E 's!(LIBAV1[^ ]*) !\1 --skip-film-grain !' "$TMP/ref_cmd.sh"

        # Run it!
        env LIBAV1="$TMP/bin" \
            ARGON_STREAMS_INPUT="$set_dir/streams" \
            ARGON_STREAMS_OUTPUT="$TMP" \
            "$TMP/ref_cmd.sh" >/dev/null || \
            {
                cat "$TMP/ref_cmd.sh"
                error "Failed to run ref_cmd.sh $TMP $ref_cmd $set_dir $md5_ref"
            }
    fi

    check_yuv_md5 "$TMP" "$md5_nfg" "ref_cmd (without film grain)" "$ensure"
}

check_decoder () {
    local TMP="$1"
    local binary="$2"
    local stream="$3"
    local md5_ref="$4"
    local md5_nfg="$5"
    local is_lst="$6"
    local csv="$7"
    shift 7

    # Weird bash syntax for "assign all remaining arguments to
    # decode_args as an array"
    local decode_args=("$@")

    local decoder="$(basename "$binary")"

    local csv_args=()
    if [ -n "$csv" ]; then
        csv_args=(-c "$csv")
    fi

    "$binary" >/dev/null \
              "${decode_args[@]}" ${csv_args[@]+"${csv_args[@]}"} || {
        error "Failed to run $decoder on $stream with args ${decode_args[@]} "
    }
    check_yuv_md5 "$TMP" "$md5_ref" "$decoder (with film grain)" 0

    if [ $is_lst = 0 ]; then
        "$binary" >/dev/null --no-film-grain "${decode_args[@]}" || {
            error "Failed to run $decoder on $stream"
        }
    fi
    check_yuv_md5 "$TMP" "$md5_nfg" "$decoder (without film grain)" 0
}

OPTS=h
LONGOPTS=help,ensure,csv:,decoders:,just-layer-0

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

ensure=0
csv=""
decoders="libaom,av1dec,av1bdec"
just_layer_0=0

while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        --ensure)
            ensure=1
            shift
            ;;
        --csv)
            csv="$2"
            shift 2
            ;;
        --decoders)
            decoders="$2"
            shift 2
            ;;
        --just-layer-0)
            just_layer_0=1
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

if [ $# != 4 ]; then
    echo 1>&2 "Wrong number of positional arguments."
    usage 1
fi

bin_dir="$1"
set_dir="$2"
base="$3"
layer="$4"
shift 4

# If --just-layer-0 is passed and layer is not zero, exit with
# success. We're ignoring this layer.
if [ $just_layer_0 = 1 -a "$layer" != 0 ]; then
    exit 0
fi

# Make a temporary directory that we'll use to hold stuff we're
# working on.
TMP="$(mktemp -d)"
trap "{ rm -rf $TMP; }" EXIT

if [ x"$layer" == x0 ]; then
    ref_cmd="$set_dir/ref_cmd/$base.sh"
    md5_ref="$set_dir/md5_ref/$base.md5"
    md5_nfg="$set_dir/md5_no_film_grain/$base.md5"
else
    ref_cmd="$set_dir/ref_cmd/layers/$layer/${base}_layer$layer.sh"
    md5_ref="$set_dir/md5_ref/layers/$layer/${base}_layer$layer.md5"
    md5_nfg="$set_dir/md5_no_film_grain/layers/$layer/${base}_layer$layer.md5"
fi

# If this is a large-scale-tile stream, it doesn't make sense to check
# without film grain: film grain is disallowed with large scale tile.
is_lst=1
(echo "$set_dir" | grep -q large_scale_tile) || is_lst=0

# Start with libaom
if echo "$decoders" | grep -q libaom ; then
    check_libaom "$TMP" "$ref_cmd" "$set_dir" "$bin_dir" \
                 "$md5_ref" "$md5_nfg" "$ensure" "$is_lst"
fi

# Ok, we're done with libaom now. We've still got decoder.exe
# (av1bdec) and additional_decoder.exe (av1dec) to go.
#
# At this point, we have to decide whether we're using annexb or not.
# We assume not iff the basename of set_dir contains "not_annexb".
# Note that the basename command strips trailing '/' characters, so we
# won't have a problem if set_dir looks like "my_release/profile0_foo/".
if basename "$set_dir" | grep -q not_annexb >/dev/null; then
    annexb_arg="--not-annexb"
else
    annexb_arg=""
fi

stream="$set_dir/streams/$base.obu"
decode_args=(-v --oppoint "$layer" $annexb_arg -o "$TMP/out.yuv" "$stream")

if echo "$decoders" | grep -q av1bdec; then
    check_decoder "$TMP" "$bin_dir/decoder.exe" "$stream" \
                  "$md5_ref" "$md5_nfg" "$is_lst" "" \
                  "${decode_args[@]}"
fi

if echo "$decoders" | grep -q av1dec; then
    check_decoder "$TMP" "$bin_dir/additional_decoder.exe" "$stream" \
                  "$md5_ref" "$md5_nfg" "$is_lst" "$csv" \
                  "${decode_args[@]}"
fi
