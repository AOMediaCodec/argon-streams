#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# Download and compile aomdec.

set -o errexit -o pipefail -o nounset

usage () {
    echo "Usage: get-aomdec.sh [-h] [-j <jobs>] [--refspec <ref>] [--fetch]"
    echo "                     [--just-fetch <tag>]"
    echo "                     <srcdir> [<build-dir>]"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hj:
LONGOPTS=help,jobs:,refspec:,fetch,just-fetch:

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

JOBS=1
# This is the blessed sha that we normally want to build (it was the
# tip of av1-normative).
REFSPEC=add4b15580e410c00c927ee366fa65545045a5d9
FETCH=0
JUST_FETCH_ARGS=()

use_default_just_fetch=1

# At this point, our command line arguments are in a sane order and
# easy to read, ending with a '--'.
while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            JOBS="$2"
            shift 2
            ;;
        --refspec)
            REFSPEC="$2"
            use_default_just_fetch=0
            shift 2
            ;;
        --fetch)
            FETCH=1
            shift
            ;;
        --just-fetch)
            JUST_FETCH_ARGS=(--depth=1 "$2")
            use_default_just_fetch=0
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# If we didn't override --refspec or --just-fetch, this will point at
# the blessed av1-normative sha. We know what to fetch to get that.
if [ $use_default_just_fetch = 1 ]; then
    JUST_FETCH_ARGS=(--depth=1 v1.0.0-errata1)
fi

# We just have positional arguments left. There should be 1 or 2
# (srcdir and possibly builddir).
BUILD=0
if [ $# = 2 ]; then
    BUILD=1
    srcdir="$1"
    builddir="$2"
    shift 2
elif [ $# = 1 ]; then
    srcdir="$1"
    shift
else
    echo 1>&2 "Wrong number of positional arguments."
    usage 1
fi

if [ $BUILD$FETCH = 10 ]; then
    # If we're building stuff and not forcing a fetch, check if
    # builddir exists, contains the aomdec binary and has a file
    # called .built-sha whose contents equal REFSPEC. If so, we can
    # exit early, looking smug. If not, we'll have to actually do some
    # work.
    if [ -e "$builddir/.built-sha" -a -e "$builddir/aomdec" ]; then
        echo "$REFSPEC" | diff -q - "$builddir/.built-sha" 2>&1 >/dev/null && {
            # They match!
            echo "We already have the right version of aomdec built."
            exit 0
        }
    fi

    # That's a pity. Blow away builddir if it exists.
    rm -rf "$builddir"
    mkdir -p "$builddir"
fi

# Ensure we have the right version of aomdec in srcdir
mkdir -p "$srcdir"

declare -a GD
GD=(env GIT_DIR="$srcdir/.git")

# Make sure we have a .git directory at all
if [ ! -d "$srcdir/.git" ]; then
    echo "Initialising git directory in $srcdir"
    "${GD[@]}" git init -q
fi

# Make sure we have aom as a remote
"${GD[@]}" git remote | grep -q aom || {
    echo "Adding aom remote"
    "${GD[@]}" git remote add aom https://aomedia.googlesource.com/aom
}

if [ $FETCH = 0 ]; then
    # If we're not forcing a fetch, we should only do the fetching if
    # we don't already recognise REFSPEC.
    "${GD[@]}" git rev-parse -q --verify "$REFSPEC^{object}" >/dev/null || {
        FETCH=1
    }
fi

if [ $FETCH = 1 ]; then
    echo "Fetching from the aom repository."
    "${GD[@]}" git fetch -q aom ${JUST_FETCH_ARGS[@]+"${JUST_FETCH_ARGS[@]}"}
fi

# By this point, we should be able to convert the refspec into a SHA.
SHA="$("${GD[@]}" git rev-parse --verify "$REFSPEC^{object}")" || {
    echo "The refspec $REFSPEC is still unknown. Has something gone awry?"
    exit 1
}

# At this point, our copy of the AOM repository has the data it needs.
# Now let's check out the right version if we didn't have it already.
head_sha=$("${GD[@]}" git rev-parse HEAD 2>&1 | head || true)
if [ "x$head_sha" != "x$SHA" ]; then
    echo "HEAD isn't currently at $SHA. Checking out right version."
    # Note we can't use GD here: if you override GIT_DIR, git
    # checkout uses the repository you say, but checks out in the
    # current directory.
    (cd "$srcdir"; git checkout -q "$SHA")
fi

# There's nothing left to do if we aren't building. Stop here.
if [ $BUILD = 0 ]; then
    exit 0
fi

# If we are building, it's still possible that there's no more work to
# do (if we forced a fetch but nothing had changed) or if REFSPEC was
# a name rather than a SHA.
if [ -e "$builddir/.built-sha" -a -e "$builddir/aomdec" ]; then
    echo "$SHA" | diff -q - "$builddir/.built-sha" 2>&1 >/dev/null && {
        # They match!
        echo "We already have the right version of aomdec built."
        exit 0
    }
fi

declare -a CMAKE_ARGS
CMAKE_ARGS=(-DENABLE_DOCS=0 -DENABLE_TESTS=0 -DENABLE_TOOLS=1
            –DCONFIG_AV1_ENCODER=0 -DENABLE_SSE=0 -DCMAKE_BUILD_TYPE=RELEASE)

# Now go into builddir and run cmake
abs_src_dir="$(readlink -f "$srcdir")"
mkdir -p "$builddir"
(cd "$builddir"; set -x;
 cmake "${CMAKE_ARGS[@]}" "$abs_src_dir" >cfg.log 2>&1) || {
    echo "Configuration failed. Last lines of log:"
    tail -n20 "$builddir/cfg.log"
    exit 1
}

set +e
set -x
make -j"$JOBS" -C "$builddir" \
     aomdec lightfield_tile_list_decoder >"$builddir/build.log" 2>&1
{ STATUS=$?; set +x; } 2>/dev/null
set -e

if [ $STATUS -ne 0 ]; then
    echo "Build failed. Last lines of log:"
    tail -n20 "$builddir/build.log"
    exit 1
fi

echo "$SHA" >"$builddir/.built-sha"

echo "aomdec build finished."
