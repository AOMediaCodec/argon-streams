
#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# A script to build a fresh release.
#
# Cobbled together from parts of other scripts to try replace what was lost,
# because we didn't understand what remained.
#
# Assumes autoproduct.py has already been used to generate av1_profile0,
# av1_profile1, and av1_profile2 streams.
#
# Run inside av1product/


PROFILES="0 1 2"

set -o errexit -o pipefail -o nounset

# Make the lines echoed by "-x" later appear on stdout rather than
# stderr.
exec 4>&1
BASH_XTRACEFD=4

usage () {
    echo "Usage: release.sh [-h] [-j <J>] [-c] [--coverbin <C>]"
    echo "                  <dest>"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hj:c
LONGOPTS=help,jobs:,continue,coverbin:

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

JOBS_ARG=""
PROCESSORS=0
CONTINUE=false
CONT_ARG=
coverbin=''

# At this point, our command line arguments are in a sane order and
# easy to read, ending with a '--'.
while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            JOBS_ARG="-j${2}"
            PROCESSORS="$2"
            shift 2
            ;;
        -c|--continue)
            CONTINUE=true
            CONT_ARG=-c
            shift
            ;;
        --coverbin)
            coverbin="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# We just have positional arguments left. There should be exactly 1
# (destination).
if [ $# != 1 ]; then
    echo 1>&2 "Wrong number of positional arguments."
    usage 1
fi

DEST="$1"
shift

AV1PRODUCT_DIR="$(dirname ${BASH_SOURCE})"
TOOLS_DIR="${AV1PRODUCT_DIR}/../tools"
AUTOPRODUCT_DIR="${AV1PRODUCT_DIR}/../autoproduct"
PRODUCT_DIR="${AV1PRODUCT_DIR}/../product"

ap_dir="${AUTOPRODUCT_DIR}"


TEST_DECODER="-c"
REFTEST="-layers"
REFTEST_EXTRA="-nobuild"
FILE_EXT="obu"
ALL_OPPOINTS=1
MAX_OPPOINTS=1
USE_AV1B=1
CSV="-csv"

declare -a AV1_OPTIONS_ARGON_DEC
declare -a AV1_OPTION_FOLDER_LABELS
AV1_OPTIONS_ARGON_DEC[0]=""
AV1_OPTION_FOLDER_LABELS[0]="core"
AV1_OPTIONS_ARGON_DEC[1]="--not-annexb"
AV1_OPTION_FOLDER_LABELS[1]="not_annexb"
AV1_OPTIONS_ARGON_DEC[2]=""
AV1_OPTION_FOLDER_LABELS[2]="large_scale_tile"

# Make sure aomdec exists
echo "Building aomdec in tools/build-aom"
"${AV1PRODUCT_DIR}/get-aomdec.sh" -j"$PROCESSORS" ../aom-src ${TOOLS_DIR}/build-aom

# A wrapper around the ./test script.
#
#  $1 is the profile ($prof)
#  $2 is the prefix for failure messages
#  The rest of the arguments are arguments to pass to ./test
run_test ()
{
    profile=$1
    shift
    err_pfx=$1
    shift

    logfile="test_$profile.txt"
    retcode=0

    set -o pipefail
    ./test "$@" 2>&1 | tee $logfile && rm $logfile || {
        retcode=$?
        error "$err_pfx in $profile"
    }
    set +o pipefail

    return $retcode
}

# Smoosh together the CSV files that we created. We'll have
# one per stream, looking like "csv/<stream>.csv".
smoosh_csv () {
    local set_name="$1"

    # Excel doesn't allow sheet names with more than 31 characters.
    local short_name="$(echo "${set_name}" | head -c 31)"

    local dst="../$short_name.csv"
    local first=1

    cp csv/header.csv $dst

    for csv in $(find csv -name 'test*.csv' | sort); do
        cat "$csv" >>"$dst"
    done
}

if [ "$REFTEST" != "none" ]; then
  echo "Reffing and testing and CSV smoosh"
  # Note: The ref/test stage used to be parallelized by running {ref; test} for each
  # profile in parallel, but the 'ref' and 'test' scripts now support internal parallelism.
  # So we instead run each profile in turn.
  for ((i = 0; i < ${#AV1_OPTIONS_ARGON_DEC[@]}; i++)); do
    OPT_ARGON=${AV1_OPTIONS_ARGON_DEC[i]}
    FOLDER_LABEL=${AV1_OPTION_FOLDER_LABELS[i]}
    DIRECTORIES=""
    for prof in $PROFILES; do
      SET_NAME="profile${prof}_${FOLDER_LABEL}"
      MERGE_DIR="${AUTOPRODUCT_DIR}/av1_profile${prof}/merged_streams_${SET_NAME}"
      PROF_DIR="${AUTOPRODUCT_DIR}/av1_profile${prof}/${SET_NAME}"
      if [[ -e "${PROF_DIR}/../${SET_NAME}.csv" && $CONTINUE == true ]]; then
        # Looks like this set has already been processed.
        # The release checking run later will should catch if we are wrong.
        echo "Skipping ${PROF_DIR} as is has CSV file in the dir above already"
        continue
      fi
      cp ${MERGE_DIR}/*${FILE_EXT} ${PROF_DIR}/streams
      DIRECTORIES+=" ${PROF_DIR}"
    done
    if [[ "$DIRECTORIES" == "" ]]; then
        if [[ $CONTINUE = true ]]; then
            continue
        fi
        error "No directories"
    fi

    # TESTARGS is an array of arguments that we pass to any invocation of ./test
    set -a TESTARGS
    TESTARGS=($TEST_DECODER $REFTEST $REFTEST_EXTRA -$FILE_EXT -j $PROCESSORS
              --av1_options "$OPT_ARGON")

    if [[ "$MAX_OPPOINTS" == "1" && "$FOLDER_LABEL" != "large_scale_tile" ]]; then
        # Brute force, sanity check approach. A faster way would be to always
        # use `-all-oppoints` and copy everything for oppoints/layers above the
        # highest for each stream.
        TESTARGS+=(-max-oppoints)
    elif [[ "$ALL_OPPOINTS" == "1" ]]; then
        TESTARGS+=(-all-oppoints)
    fi

    run_test $prof "Ref issue" -ref -av1b -do-film-grain-noise "${TESTARGS[@]}" $DIRECTORIES
    run_test $prof "Argon issue" -av1b -do-film-grain-noise "${TESTARGS[@]}" $DIRECTORIES
    run_test $prof "MD5 mismatch" $CSV "${TESTARGS[@]}" $DIRECTORIES
    if [[ "$USE_AV1B" == "1" ]]; then
        run_test $prof "Av1b MD5 mismatch" -av1b "${TESTARGS[@]}" $DIRECTORIES
    fi

    # Ensure there aren't any .yuv files hanging around
    for prof_dir in $DIRECTORIES; do
        rm -f ${prof_dir}/streams/*.yuv
    done

    echo "Combining CSV files"
    for prof_dir in $DIRECTORIES; do
        (cd "${prof_dir}"; smoosh_csv `basename "${prof_dir}"`)
    done
  done
fi

# Now we want to build a new release in "${DEST}/release".
rel_dir="${DEST}/release"
if [ $CONTINUE = false ]; then
    rm -rf "${rel_dir}" || error "Can't delete existing release at ${rel_dir}"
fi
mkdir -p "${rel_dir}" || error "Can't make ${rel_dir}"

# Get the stuff previously built with autoproduct
echo "Copying generated streams/MD5 sums into release"
for p in $PROFILES; do
    for label in ${AV1_OPTION_FOLDER_LABELS[@]}; do
        "${AV1PRODUCT_DIR}/extract-release-dirs.sh" \
            $CONT_ARG \
            "${ap_dir}/av1_profile${p}" \
            "${rel_dir}" \
            $p \
            profile${p}_${label}
    done
done

# If --coverbin was passed, we should use that file. Otherwise, we
# need to build ourselves a release coverage tool.
if [ x"$coverbin" != x ]; then
    if [ ! -f "$coverbin" ]; then
        error "No such file: $coverbin."
    fi
    mkdir -p "${rel_dir}/bin/linux"
    cp "$coverbin" "${rel_dir}/bin/linux/argoncover"
else
    "${AV1PRODUCT_DIR}/build-release-binaries.sh" \
        $JOBS_ARG $CONT_ARG cover "$DEST" || {
        error "Failed to build release binaries"
    }

    # Where did they end up?
    VERSION=$(cat "${DEST}/version") || {
        error "There doesn't seem to be a version at ${DEST}/version"
    }

    echo "Found binaries with version ${VERSION}"

    # Copy the binary that we just built. Obviously, this doesn't get us
    # the Windows binary, but the linux one is a plausible place to start
    # (and we'll use it to generate the final coverage report).
    echo "Copying coverage tool into release dir"
    cp -r "${DEST}/${VERSION}/bin" "${rel_dir}"
fi

# Copy the argon viewer from the product directory
cp -r "${PRODUCT_DIR}/ArgonViewerV14" "${rel_dir}"

# A regex that matches sets that should only have layer zero
single_layer_re='error|large_scale_tile'

# Ensure that every set has reference commands for all layers.
echo "Ensuring reference commands for all sets"
"${AV1PRODUCT_DIR}/make-layer-ref-commands.sh" "$rel_dir" "$single_layer_re"

# At this point, we should have a sane looking release.
echo "Checking the release looks sane"
"${PRODUCT_DIR}/check-release-structure.sh" \
    --single-layer-re="$single_layer_re" \
    "${rel_dir}"

TMP="$(mktemp -d)"
trap "{ rm -rf $TMP; }" EXIT

abs_tmp="$(readlink -f "${TMP}")"

# Generate coverage for each profile, using the RELEASE=1 coverage
# tool that we built earlier
cover_profiles=""
for p in $PROFILES; do
    if [ $CONTINUE = true ]; then
        # If we're continuing, there's no point in running coverage
        # again if the XML file already exists.
        if [ -e "${rel_dir}/prof_av1_profile${p}.xml" ]; then
            echo "Skipping coverage for profile ${p}."
            continue
        fi
    fi
    cover_profiles="${cover_profiles} ${p}"
done

# The stream names printed by the coverage tool are relative paths and
# we don't want them to include ${rel_dir}, so we run from inside
# ${rel_dir}.
abs_av1product_dir="$(readlink -f "${AV1PRODUCT_DIR}")"

echo "Checking release coverage for profiles: $cover_profiles"
(cd "${rel_dir}";
 "${abs_av1product_dir}/check_release_coverage.py" \
     $JOBS_ARG -b 4 \
     --profiles="$cover_profiles" \
     "bin/linux/argoncover" \
     "." \
     "prof_av1_profile{}.xml" \
     profile{}_core profile{}_large_scale_tile profile{}_not_annexb \
     >"$abs_tmp/coverage.log") 2>&1 || {

    echo "Coverage run failed. Log ends with:"
    tail -n50 "$TMP/coverage.log"
    exit 1
}
