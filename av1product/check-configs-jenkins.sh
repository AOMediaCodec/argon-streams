#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This script gets run in a Jenkins job and basically is in charge of
# building av1enc, aomdec and av1bdec and making sure that the
# resulting encoder passes its self-tests with the example
# configuration files.
#
# Note that this is intended as a fast test, so it doesn't force a
# rebuild of everything (hopefully meaning that silly mistakes get
# caught earlier).

set -o errexit -o pipefail -o nounset

usage () {
    echo "Usage: check-configs-jenkins.sh [-h] [-j <jobs>] [-n <num-seeds>]"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hj:n:
LONGOPTS=help,jobs:,num-seeds:

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

JOBS=1
NUM_SEEDS=10

# At this point, our command line arguments are in a sane order and
# easy to read, ending with a '--'.
while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            JOBS="$2"
            shift 2
            ;;
        -n|--num-seeds)
            NUM_SEEDS="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

scripts="$(dirname ${BASH_SOURCE})"
tools="$scripts/../tools"

# We'll need a copy of aomdec
"${scripts}/get-aomdec.sh" -j"$JOBS" aom/src aom/build

# Now build av1enc and av1bdec in the tree. Note that we don't clean
# stuff out, so hopefully this will often be very quick.

logged () {
    log="$1"
    shift

    set +e
    set -x
    time "$@" >&"$log"
    { STATUS=$?; set +x; } 2>/dev/null
    set -e
    if [ $STATUS -ne 0 ]; then
        echo "Test failed. Last lines of log $log:"
        tail -n20 "$log"
        exit 1
    fi
    set -e
}

# A function that wraps up the common "make" logic.
mk () {
    name="$1"
    log="$2"
    shift 2

    echo "Building $name"
    logged "$log" make -j"$JOBS" -C "$tools" "$@"
}

mkdir -p logs

# This is a bit rubbish. We explicitly override CXX to be g++-7
# because Athene's default g++ (4.8.5) has a non-working <regex>
# implementation. Boo.
if [ "x$(hostname)" == xathene ]; then
    export CXX=g++-7
fi

declare -a av1enc_args
av1enc_args=(TARGET=17 PROFILE=configurable)

mk "encoder" "logs/av1enc-build.log"  "${av1enc_args[@]}" c-build
mk "av1bdec" "logs/av1bdec-build.log" TARGET=20 c-build

# $tools/build/av1enc/configurations contains all the generated files
# from the last tests. Unless something has gone wrong, we will be
# running different seeds this time so want to delete anything that's
# in there at the moment.
rm -rf "$tools/build/av1enc/configurations"

# Note that with-seed.sh will set START_SEED by replacing the '{}' with it.
logged \
  "logs/tests.log" \
  "${scripts}/with-seed.sh" \
    make -j"$JOBS" -C "$tools" "${av1enc_args[@]}" \
    AOMDEC="$(readlink -f aom/build/aomdec)" \
    LSTDEC="$(readlink -f aom/build/examples/lightfield_tile_list_decoder)" \
    DECODER=build/av1bdec/av1b.gen.exe \
    NUM_SEEDS="$NUM_SEEDS" \
    START_SEED={} \
    check-configurations || {

    echo "Tests failed. Seed: "
    set +e
    set -x
    cat .last-seed
    { STATUS=$?; set +x; } 2>/dev/null
    exit 1
}

# The test passed. Print the first few lines of the test log file so
# that we can see which seed ran.
echo "Test passed."
set -x
head -n10 "logs/tests.log"
