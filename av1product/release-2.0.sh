#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# A script to build a 2.0 release from an existing 1.3 release

### TODO: This is (unsurprisingly) not yet complete
PROFILES="0 1 2"

set -o errexit -o pipefail -o nounset

# Make the lines echoed by "-x" later appear on stdout rather than
# stderr.
exec 4>&1
BASH_XTRACEFD=4

usage () {
    echo "Usage: release-2.0.sh [-h] [-j <J>] [-c] [--coverbin <C>]"
    echo "                      <rel1.3> <err-strms> <dest>"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hj:c
LONGOPTS=help,jobs:,continue,coverbin:

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

JOBS_ARG=""
CONTINUE=false
CONT_ARG=
coverbin=''

# We need to start at a seed greater than any seed in the v1.3
# release. The largest seed in that release is 17367. Starting at
# something beginning with 2 seems fitting, so we'll start at 20k.
START_SEED=20000

# At this point, our command line arguments are in a sane order and
# easy to read, ending with a '--'.
while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            JOBS_ARG="-j${2}"
            shift 2
            ;;
        -c|--continue)
            CONTINUE=true
            CONT_ARG=-c
            shift
            ;;
        --coverbin)
            coverbin="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# We just have positional arguments left. There should be exactly 3
# (v1.3 release, error-streams directory and destination).
if [ $# != 3 ]; then
    echo 1>&2 "Wrong number of positional arguments."
    usage 1
fi

SOURCE="$1"
ERR_STREAMS="$2"
DEST="$3"
shift 3

AV1PRODUCT_DIR="$(dirname ${BASH_SOURCE})"
TOOLS_DIR="${AV1PRODUCT_DIR}/../tools"
AUTOPRODUCT_DIR="${AV1PRODUCT_DIR}/../autoproduct"
PRODUCT_DIR="${AV1PRODUCT_DIR}/../product"

# Use autoproduct to generate new streams for each profile
abs_autoproduct_dir="$(readlink -f "${AUTOPRODUCT_DIR}")"
ap_dir="${DEST}/build/autoproduct"
mkdir -p "${ap_dir}" || error "Can't make ${ap_dir}"
mkdir -p "${DEST}/logs" || error "Can't make ${DEST}/logs."

for profile in $PROFILES; do
    echo "Running autoproduct for profile $profile"

    # We run autoproduct in the build directory (because it generates
    # a load of stuff in its working directory). The log file is in
    # ${DEST}/logs, but we specify it with a relative dir.
    #
    # We make the generation size reasonably small. It's not
    # particularly hard to hit the new coverage points, and this
    # speeds up ref/testing significantly.
    pushd "${ap_dir}" >/dev/null

    log="../../logs/autoproduct-$profile.log"
    set +e
    set -x
    time \
        "${abs_autoproduct_dir}/autoproduct.py" \
        $JOBS_ARG $CONT_ARG -Dprofile_suffix=$profile \
        --start-seed $START_SEED --gensize 10 \
        --never-merge \
        --other-bindirs "build-av1_profile0/bin" \
        "${abs_autoproduct_dir}/new-entries.xml" >&"$log"
    { STATUS=$?; set +x; } 4>/dev/null
    set -e

    if [ $STATUS != 0 ]; then
        echo 1>&2 "autoproduct prof $profile failed. Last lines of log $log:"
        tail 1>&2 -n20 $log
        exit 1
    fi

    popd >/dev/null
done

# Now we want to build a new v2.0 release in "${DEST}/release".
rel_dir="${DEST}/release"
if [ $CONTINUE = false ]; then
    rm -rf "${rel_dir}" || error "Can't delete existing release at ${rel_dir}"
fi
mkdir -p "${rel_dir}" || error "Can't make ${rel_dir}"

copy_dir() {
    name="$1"
    echo "Copying ${name} directory from existing release"

    test -d "${SOURCE}/${name}" || \
        error "No ${name} directory in existing release."

    cp -r "${SOURCE}/${name}" "${rel_dir}"
}

# Copy each profile that was there before
for profile in $PROFILES; do
    for flavour in core large_scale_tile not_annexb; do
        for special_suffix in '' _special; do
            copy_dir "profile${profile}_${flavour}${special_suffix}"
        done
    done
done
copy_dir "profile_switching"

# Get the stuff we built with autoproduct
echo "Copying generated streams/MD5 sums into release"
for p in $PROFILES; do
    "${AV1PRODUCT_DIR}/extract-release-dirs.sh" \
        --append \
        $CONT_ARG \
        "${ap_dir}/av1_profile${p}" \
        "${rel_dir}" \
        $p \
        profile${p}_core
done

# Now let's copy in the error streams that we were given (not the CSV
# files at the moment). This is pretty easy, since they go in
# completely separate directories.
echo "Copying error streams into release dir"
for profile in $PROFILES; do
    dir="profile${profile}_error"
    cp -r "${ERR_STREAMS}/${dir}" "${rel_dir}"

    # Gonna make me some reference commands...
    rm -rf "${rel_dir}/${dir}/ref_cmd"
    mkdir -p "${rel_dir}/${dir}/ref_cmd"
    for stream in $(find "${rel_dir}/${dir}/streams" -type f -printf '%f '); do
        base=$(basename ${stream} .obu)

        dst="${rel_dir}/${dir}/ref_cmd/${base}.sh"
        echo "#!/bin/bash" >"$dst"
        echo "set -eu" >>"$dst"
        echo -n '$LIBAV1/aomdec $ARGON_STREAMS_INPUT/' >>"$dst"
        echo -n $base.obu >> "$dst"
        echo ' --rawvideo -o $ARGON_STREAMS_OUTPUT/out.yuv --all-layers' >>"$dst"
    done
done

echo "Copying CSVs for error streams into CSV dir"
mkdir -p "${DEST}/csv"
for profile in $PROFILES; do
    cp -r "${ERR_STREAMS}/profile${profile}_error.csv" "${DEST}/csv"
done

# If --coverbin was passed, we should use that file. Otherwise, we
# need to build ourselves a release coverage tool.
if [ x"$coverbin" != x ]; then
    if [ ! -f "$coverbin" ]; then
        error "No such file: $coverbin."
    fi
    mkdir -p "${rel_dir}/bin/linux"
    cp "$coverbin" "${rel_dir}/bin/linux/argoncover"
else
    "${AV1PRODUCT_DIR}/build-release-binaries.sh" \
        $JOBS_ARG $CONT_ARG cover "$DEST" || {
        error "Failed to build release binaries"
    }

    # Where did they end up?
    VERSION=$(cat "${DEST}/version") || {
        error "There doesn't seem to be a version at ${DEST}/version"
    }

    echo "Found binaries with version ${VERSION}"

    # Copy the binary that we just built. Obviously, this doesn't get us
    # the Windows binary, but the linux one is a plausible place to start
    # (and we'll use it to generate the final coverage report).
    echo "Copying coverage tool into release dir"
    cp -r "${DEST}/${VERSION}/bin" "${rel_dir}"
fi

# Copy the argon viewer from the product directory
cp -r "${PRODUCT_DIR}/ArgonViewerV14" "${rel_dir}"

# A regex that matches sets that should only have layer zero
single_layer_re='error|large_scale_tile'

# Ensure that every set has reference commands for all layers.
echo "Ensuring reference commands for all sets"
"${AV1PRODUCT_DIR}/make-layer-ref-commands.sh" "$rel_dir" "$single_layer_re"

# At this point, we should have a sane looking release, except that
# there will be some MD5s missing.
echo "Checking the release looks sane (except missing md5s)"
"${PRODUCT_DIR}/check-release-structure.sh" \
    --skip-md5s --single-layer-re="$single_layer_re" \
    "${rel_dir}"

TMP="$(mktemp -d)"
trap "{ rm -rf $TMP; }" EXIT

abs_tmp="$(readlink -f "${TMP}")"

# If we have the --continue argument, let's check to see whether we've
# got all our MD5s already. If so, we'll set a flag to say that we
# don't have to run stuff.
#
# When do we have everything? If we pass check-release-structure
# (without --skip-md5s) AND we have at least one layer-specific MD5
# checksum.
needs_rebuild=true
if [ $CONTINUE = true -a -d "${rel_dir}/profile0_core/md5_ref/layers/1" ]; then
    echo "Maybe we've got all our MD5 checksums already?"
    if "${PRODUCT_DIR}/check-release-structure.sh" \
           --single-layer-re="$single_layer_re" \
           "${rel_dir}" >/dev/null 2>&1; then
        echo "Woot! We don't have to do any work."
        needs_rebuild=false
    else
        echo "Apparently, there is still something missing. "
        echo "We'll rebuild stuff."
    fi
fi

# Generate coverage for each profile, using the RELEASE=1 coverage
# tool that we built earlier
cover_profiles=""
for p in $PROFILES; do
    if [ $CONTINUE = true ]; then
        # If we're continuing, there's no point in running coverage
        # again if the XML file already exists.
        if [ -e "${rel_dir}/prof_av1_profile${p}.xml" ]; then
            echo "Skipping coverage for profile ${p}."
            continue
        fi
    fi
    cover_profiles="${cover_profiles} ${p}"
done

# The stream names printed by the coverage tool are relative paths and
# we don't want them to include ${rel_dir}, so we run from inside
# ${rel_dir}.
abs_av1product_dir="$(readlink -f "${AV1PRODUCT_DIR}")"

echo "Checking release coverage for profiles: $cover_profiles"
(cd "${rel_dir}";
 "${abs_av1product_dir}/check_release_coverage.py" \
     $JOBS_ARG -b 4 \
     --profiles="$cover_profiles" \
     "bin/linux/argoncover" \
     "." \
     "prof_av1_profile{}.xml" \
     profile{}_core profile{}_large_scale_tile profile{}_not_annexb \
     >"$abs_tmp/coverage.log") 2>&1 || {

    echo "Coverage run failed. Log ends with:"
    tail -n50 "$TMP/coverage.log"
    exit 1
}

# Check or create MD5s for all layers, as well as CSV files. We pass
# the binary directory that autoproduct generated for profile 0.
#
# If needs_rebuild is false, that means that we don't have to re-check
# files that already have an MD5 checksum. We pass the --skip-existing
# flag to check-release-files.sh to tell it not to bother.
crf_flags=""
if [ $needs_rebuild = false ]; then
    crf_flags=--skip-existing
fi

echo "Checking and/or creating md5s for all streams and layers"
"${PRODUCT_DIR}/check-release-files.sh" \
    --ensure $crf_flags $JOBS_ARG \
    --csv "${DEST}/csv" \
    "${ap_dir}/build-av1_profile0/bin" \
    "${AV1PRODUCT_DIR}/test-one-file.sh" "${rel_dir}"

# Once that has finished, we should have all the MD5s our hearts could
# desire. Run the check-release-structure script again to make sure
# they've appeared.
echo "Checking the release looks sane (including md5s)"
"${PRODUCT_DIR}/check-release-structure.sh" \
    --single-layer-re="$single_layer_re" "${rel_dir}"

# Finally, smoosh together the CSV files that we created. We'll have
# one per stream, looking like "${DEST}/csv/<set>/<stream>.csv". Note
# that we know the set and CSV names look fine (because we passed
# check-release-structure.sh above).
smoosh_csv () {
    local set_name="$1"

    # Excel doesn't allow sheet names with more than 31 characters.
    local short_name="$(echo "${set_name}" | head -c 31)"

    local dst="$short_name.csv"
    local first=1

    for csv in $(find ${set_name} -name '*.csv' | sort); do
        if [ $first = 1 ]; then
            first=0
            cp "$csv" "$dst"
        else
            tail -n +2 "$csv" >>"$dst"
        fi
    done
}

echo "Combining CSV files"
for set_name in $(cd "${DEST}/csv"; find -mindepth 1 -maxdepth 1 -type d); do
    (cd "${DEST}/csv"; smoosh_csv "${set_name}")
done

echo "Listing old streams"
find "${SOURCE}" -name '*.obu' -printf '%P\n' | sort >"${TMP}/old.lst"
echo "Listing new streams"
find "${rel_dir}" -name '*.obu' -printf '%P\n' | sort >"${TMP}/new.lst"

# The newly added streams are just those that appear in new.lst and
# not in old.lst
new_lst="${rel_dir}/streams-added-since-v1.2.lst"
comm -13 "${TMP}/old.lst" "${TMP}/new.lst" >"${new_lst}"

echo "Automated steps are done${VERSION+ for version: ${VERSION}}. Still to do:"
echo "  - Copy in the user manual"
echo "  - Generate Excel docs from the CSVs"
echo "  - Build a windows coverage tool"
echo "  - Generate the streams changed list"
