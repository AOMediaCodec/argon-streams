#!/bin/bash

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# A script to build all the binaries for a given codec.

set -o errexit -o pipefail -o nounset

# Make the lines echoed by "-x" later appear on stdout rather than
# stderr.
exec 4>&1
BASH_XTRACEFD=4

usage () {
    echo "Usage: build-release-binaries.sh [-h] [-j <jobs>] [-c] <product> <dest>"
    exit $1
}

error () {
    echo 1>&2 "$1"
    exit 1
}

OPTS=hj:c
LONGOPTS=help,jobs:,continue

! PARSED=$(getopt --options=$OPTS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # getopt has already complained.
    usage 1
fi
eval set -- "$PARSED"

JOBS_ARG=""
CONTINUE=false

# At this point, our command line arguments are in a sane order and
# easy to read, ending with a '--'.
while true; do
    case "$1" in
        -h|--help)
            usage 0
            ;;
        -j|--jobs)
            JOBS_ARG="-j${2}"
            shift 2
            ;;
        -c|--continue)
            CONTINUE=true
            shift
            ;;
        --)
            shift
            break
            ;;
        *)
            echo >&2 "Erk. case statement not in sync with getopt call."
            exit 2
            ;;
    esac
done

# We just have positional arguments left. There should be exactly 2
# (product and dest).
if [ $# != 2 ]; then
    echo 1>&2 "Wrong number of positional arguments."
    usage 1
fi

PRODUCT="$1"
DEST="$2"
shift 2

# Check that we have heard of this product.
declare -a known_products
known_products=(cover)
product_is_known=0
for kp in "${known_products[@]}"; do
    if [ x"$kp" == x"$PRODUCT" ]; then
        product_is_known=1
        break
    fi
done
if [ $product_is_known != 1 ]; then
    echo "Unknown product: '$PRODUCT'. Known: (${known_products[*]})"
    usage 1
fi

###############################################################################
# Begin product-specific code
###############################################################################

# $product_targets
#
# Echos the list of targets (numbers to pass as TARGET in
# tools/Makefile) for the given codec, together with information about
# them. This has the syntax:
#
#    target:exe-path:rel-name
#
# where target is the numerical target, exe-path is the path to the
# built executable in the build directory and rel-name is the name of
# the executable in the release.

cover_targets () {
    echo 21:av1bcover/av1b.gen.exe:argoncover
}

###############################################################################
# End product-specific code
###############################################################################

# These lines echo $restore_x the right thing to pass to set in order
# to "pop" the -x flag.
get_restore_x () {
    shopt -q -o xtrace && echo "-x" || echo "+x"
}

chatty () {
    local log="$1"
    shift

    local rx="$(get_restore_x)"
    local STATUS=

    set -x
    "$@" >&"$log"
    { STATUS=$?; set "$rx"; } 4>/dev/null

    return $STATUS
}

check_new () {
    local rel_path="$1"
    if [ -e "$DEST/$rel_path" ]; then
        error "Destination '$DEST' already exists. Use -c?"
    fi
}

BUILD="$DEST/build"

# If not continuing, make sure that none of the files and directories
# that we want to create exist already.
if [ $CONTINUE != true ]; then
    check_new version
fi

AV1PRODUCT_DIR="$(dirname ${BASH_SOURCE})"
TOOLS_DIR="${AV1PRODUCT_DIR}/../tools"
PRODUCT_DIR="${AV1PRODUCT_DIR}/../product"

# Ensure the build directory and log directory (and their parent DEST
# directory) exist.
mkdir -p "$DEST/logs" "$BUILD"

VERSION=$(cd "$TOOLS_DIR"; git describe --tags --long --always --dirty)
echo
echo "This will be built as version ${VERSION}"
echo "${VERSION}" >"${DEST}/version"

COMMON_MAKE_ARGS="${JOBS_ARG}"
COMMON_MAKE_ARGS="${COMMON_MAKE_ARGS} RELEASE=1"
COMMON_MAKE_ARGS="${COMMON_MAKE_ARGS} P8005_BUILD_PREFIX=${BUILD}"
COMMON_MAKE_ARGS="${COMMON_MAKE_ARGS} GIT_VERSION=${VERSION}"

run_make () {
    long_op="$1"
    log_base="$2"
    shift 2

    echo
    echo "${long_op}"
    LOG="${DEST}/logs/${log_base}.log"

    set +e
    chatty "$LOG" \
           time make -C "$TOOLS_DIR" ${COMMON_MAKE_ARGS} "$@" || {
        echo 1>&2 "${long_op} failed. Last lines of log $LOG:"
        tail 1>&2 -n20 "$LOG"
        exit 1
    }
    set -e
}

do_build () {
    target="$1"
    exe_path="$2"
    shift 2

    exe_dir="$(dirname "$exe_path")"

    run_make "Building $exe_path" \
             "build-$exe_dir" \
             TARGET="$target" c-build
}

RELEASE="${DEST}/${VERSION}"

# If $RELEASE already exists, blow it away. We might want to reuse the
# build directory with '-c', but definitely want to copy release
# artefacts afresh. Since we'll get here near the start of the run,
# let's give the user a few seconds' grace in case this is a disaster.
if [ -e "$RELEASE" ]; then
    echo "Deleting existing release directory at '$RELEASE' in 5 seconds"
    sleep 5
    rm -rf "$RELEASE"
fi

mkdir -p "$RELEASE/bin/linux"
targets="$(${PRODUCT}_targets)"

for tgt in "$targets"; do
    IFS=: read -r target exe_path rel_name <<<"$tgt"
    built_path="$BUILD/build/$exe_path"
    rel_path="$RELEASE/bin/linux/$rel_name"

    do_build "$target" "$exe_path"
    test -f "$built_path" || \
        error "Building $exe_path failed to create '$built_path'"

    cp "$built_path" "$rel_path"
    strip "$rel_path"
done

# Now we should generate a build script for Windows builds (boo,
# hiss!).
echo
echo "Setting up windows build"
WIN_BASE="win-build-${VERSION}"
WIN_DIR="${DEST}/${WIN_BASE}"

# Generate the script header
mkdir -p "${WIN_DIR}"
cp "${DEST}/version" "${WIN_DIR}"
cp -r "$TOOLS_DIR" "${WIN_DIR}"
cat >"${WIN_DIR}/cygwin-build.sh" <<EOF
#!/bin/bash
J=4

echo "Building with \$J threads (edit this script if that's wrong)"
echo "Built binaries will go into the windows subdirectory"

set -e

MAKE_ARGS=()
MAKE_ARGS+=("-j\$J")
MAKE_ARGS+=("CHECK_COMPILER_VERSION=0")
MAKE_ARGS+=("CXX=x86_64-w64-mingw32-g++")
MAKE_ARGS+=("CFLAGS=-lm -D__USE_MINGW_ANSI_STDIO=1 -D__STDC_FORMAT_MACROS=1")
MAKE_ARGS+=("LDFLAGS=-static")
MAKE_ARGS+=("RELEASE=1")
MAKE_ARGS+=("GIT_VERSION=${VERSION}")

mkdir -p windows

EOF

# Generate the rest of the script with each target
for tgt in "$targets"; do
    IFS=: read -r target exe_path rel_name <<<"$tgt"

    cat >>"${WIN_DIR}/cygwin-build.sh" <<EOF
echo "Building $exe_path"
set -x
make -C tools TARGET=$target "\${MAKE_ARGS[@]}" c-build
{ set +x; } 2>/dev/null

echo "Copying to windows/$rel_name and stripping"
cp "build/$exe_path" "windows/$rel_name"
strip "windows/$rel_name"

EOF
done
