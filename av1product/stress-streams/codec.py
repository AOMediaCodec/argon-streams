################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Python code that represents the AV1 codec binaries

This knows how to build an encoder and how to run decoders on the resulting
binary to get its statistics.

'''

import math
import os
import random
import subprocess
import tempfile
import threading
import time
from typing import (Any, AnyStr, Callable, IO, List,
                    NamedTuple, Optional, Tuple, Union)

import buildtools
from build_config import BuildConfig
from run_data import RunData, read_log_file
from database import Database
from level_info import LevelInfo

_BoolThunk = Callable[[], bool]
_LogFunc = Callable[[IO[Any]], Any]


def safe_get_size(path: str) -> int:
    '''Return the size of the file at path or 0 on error'''
    try:
        return os.path.getsize(path)
    except OSError:
        return 0


class BuilderThread(threading.Thread):
    '''A thread class representing an encoder builder'''

    MAX_JOBS = 8

    def __init__(self,
                 build_config: BuildConfig,
                 encoder: 'Encoder',
                 jobs_available: int):
        super().__init__()
        self.build_config = build_config
        self.encoder = encoder
        self.num_jobs = min(jobs_available, BuilderThread.MAX_JOBS)

        self.binary = None  # type: Optional[str]
        self.log_name = None  # type: Optional[str]

    def run(self) -> None:
        with tempfile.TemporaryDirectory() as build_dir:
            os.makedirs(self.build_config.log_dir, exist_ok=True)
            prof_name = self.encoder.profile_name()
            self.log_name = os.path.join(self.build_config.log_dir,
                                         ('build-{}.log'
                                          .format(prof_name)))

            self.binary = (buildtools
                           .build_encoder(self.build_config.bin_dir,
                                          build_dir,
                                          prof_name,
                                          self.log_name,
                                          self.num_jobs))


class Encoder:
    '''An Encoder represents an actual encoder binary'''
    def __init__(self, profile_funs: List[str]):
        self.profile_funs = profile_funs

    def profile_name(self) -> str:
        '''Return the profile name used for the encoder binary'''
        return ','.join(self.profile_funs)

    def find_binary(self, build_config: BuildConfig) -> Optional[str]:
        '''Return the path to a binary for this encoder if it exists'''
        return build_config.find_binary(self.profile_name() + '.enc.exe')

    def start_build(self,
                    build_config: BuildConfig,
                    jobs_available: int) -> BuilderThread:
        '''Start a thread building the encoder

        This uses up to jobs_available jobs and returns the thread object. The
        thread will read build_config.

        '''
        thread = BuilderThread(build_config, self, jobs_available)
        thread.start()
        return thread


RunConfig = NamedTuple('RunConfig',
                       [('encoder_profile', 'EncoderProfile'),
                        ('build_config', BuildConfig),
                        ('seed', int),
                        ('backoff', int),
                        ('targets', List[str])])


class RunThread(threading.Thread):
    '''A thread class representing a running encode/decode job'''
    def __init__(self,
                 run_config: RunConfig,
                 database: Database):
        super().__init__()

        self.run_config = run_config
        self.run_data = None  # type: Optional[RunData]

        # A pointer to the global database that we query to see whether a
        # stream is interesting or not. Note: the database is shared between
        # threads.
        self.database = database

        # Any messages. They will be printed out when handling the results. If
        # this is nonempty, the stream should not be used.
        self.msg = []  # type: List[str]

        # This flag is set to true if something went wrong with the stream
        self.bad = False

        # A list of failed checks. If this is nonempty, the stream should not
        # be used.
        self.failed_checks = []  # type: List[str]

        # A event that tells the thread to stop sharpish. This may be written
        # from the main thread.
        self.emergency_stop = threading.Event()

    def wait_for_proc(
            self,
            process: subprocess.Popen[AnyStr],
            log: Optional[str] = None,
            early_stop: Optional[_BoolThunk] = None) -> Union[int, str]:
        '''Wait for a Popen object to finish, killing it if necessary

        Returns the exit code or a string if killed. If early_stop is given, it
        is guaranteed to be run before we accept a stream.

        '''
        while True:
            # Wake up every second to check that we shouldn't kill the
            # subprocess.
            time.sleep(1)

            # Check the current state of the process
            process.poll()

            # Check that the log file isn't insanely large.
            if log is not None:
                if safe_get_size(log) > 10*1024*1024:
                    self.msg.append('Log file too large. Killing subprocess.')
                    process.kill()
                    process.wait()
                    return 'KILLED'

            # If we have an early stop test, run that.
            if early_stop is not None:
                if early_stop():
                    process.kill()
                    process.wait()
                    return 'EARLY_STOP'

            # If the process had finished when we polled at the top, we can
            # return. Note that we poll before running the log size and early
            # stop tests: this avoids a race condition where the output
            # suddenly changes just before the process finishes.
            if process.returncode is not None:
                return process.returncode

            # The process hasn't finished, but we still need to check we
            # haven't been asked to kill it.
            if self.emergency_stop.is_set():
                self.msg.append('Killed subprocess.')
                process.kill()
                process.wait()
                return 'KILLED'

    def _logged_proc(self,
                     job: Tuple[str, str, List[str]],
                     on_success: Optional[_LogFunc] = None,
                     early_stop: Optional[_BoolThunk] = None) -> Any:
        '''Run command and take a copy of the log if it fails.

        The log file will have a name based on tag and the open file (starting
        at the top) will be yielded on success. If the command fails, the log
        will copied with add_fail_log unless the failure is just down to check
        failures.

        On failure, an error message is appended involving desc and None is
        returned.

        On success, either True is returned or, if on_success is not None,
        on_success is run with the log file as an argument and its return value
        is returned.

        If early_stop is not None, it is called every so often. If the
        function returns true, the encode will stop and we'll add
        EARLY_STOP to the list of failed checks.

        '''
        tag, desc, command = job
        mode = 'w+' if on_success is not None else 'w'
        with tempfile.NamedTemporaryFile(prefix=(tag + '-'), mode=mode) as log:
            log.write(' '.join(command) + '\n')
            log.flush()
            proc = subprocess.Popen(command, stdout=log, stderr=log,
                                    start_new_session=True)
            retcode = self.wait_for_proc(proc,
                                         log=log.name,
                                         early_stop=early_stop)

            # If the return code is 'EARLY_STOP', we stopped early.
            if retcode == 'EARLY_STOP':
                self.failed_checks = ['EARLY_STOP']
                return None

            if retcode == 'KILLED':
                self.bad = True
                return None

            # Those were the only non-integer retcodes possible.
            assert isinstance(retcode, int)

            # If the return code is 2, this might have just been failed checks.
            if retcode == 2:
                self.failed_checks = buildtools.innocuous_checks(log.name)
                if self.failed_checks:
                    # The stream is bad, but we don't take a log because it's
                    # a failed check that we are happy to ignore.
                    return None

            # If the return code is not zero, something went properly wrong and
            # we should take the log.
            if retcode:
                bcfg = self.run_config.build_config
                self.bad = True
                self.msg.append('ERROR: {} failed; Log file at `{}\'.'
                                .format(desc,
                                        bcfg.add_fail_log(log.name, tag)))
                return None

            # We ran successfully.
            if on_success is not None:
                log.seek(0)
                return on_success(log)

            return True

    def stream_is_oversize(self, path: str) -> bool:
        '''Check if the file at path is too big for this level

        This is designed to let us kill off streams early if they are
        getting too big for the maximum bitrate for this level. This
        way, we don't fill up our scratch disk and we also fail much
        quicker.

        '''
        size = safe_get_size(path)
        level_info = self.run_config.encoder_profile.level_info
        return size > level_info.max_stream_size()

    def encode(self) -> Optional[str]:
        '''Run the encoder to create self.stream'''
        run_cfg = self.run_config
        eprof = run_cfg.encoder_profile
        # The path to the stream that we generate.
        strm_handle, strm_name = \
            tempfile.mkstemp(dir=run_cfg.build_config.scratch_dir,
                             prefix='test{}-{}-'.format(run_cfg.seed,
                                                        run_cfg.backoff),
                             suffix='.obu')
        os.close(strm_handle)

        def early_stop() -> bool:
            return self.stream_is_oversize(strm_name)

        keep = True

        with (eprof.level_info.
              config_wrapper(eprof.settings.config_args)) as cfg:
            cmd = eprof.encode_command(run_cfg, strm_name, cfg)
            if cmd is None:
                keep = False
            elif not self._logged_proc(('encode', 'Encoder', cmd),
                                       early_stop=early_stop):
                keep = False

        if not keep:
            buildtools.rm_f(strm_name)
            return None

        assert os.path.exists(strm_name)
        return strm_name

    def run(self) -> None:
        try:
            self.run_data = RunData()

            # First we need to run an encoder.
            stream = self.encode()
            if stream is None:
                return

            # Make sure to record the stream name so that it can be
            # deleted if something goes haywire when getting the
            # stats.
            self.run_data.set_stream(stream)

            # Run av1dec to get some statistics.
            av1dec = self.run_config.build_config.binary_path('av1dec')
            stats = self._logged_proc(('av1dec', 'Decoding stream',
                                       [av1dec,
                                        '-v', '-o', '/dev/null',
                                        '--output-stats', stream]),
                                      on_success=read_log_file)
            self.run_data.set_stats(stats)

            if not stats:
                assert self.msg
                return

            # Do these statistics look interesting? If not, we won't bother
            # checking the stream with av1bdec.
            assert self.run_data.size is not None
            rcfg = self.run_config
            pts, hp_msg = \
                self.database.high_points(stats,
                                          self.run_data.size,
                                          rcfg.encoder_profile.target,
                                          rcfg.targets)
            if not pts:
                # Boring! Return immediately; we know that this stream will be
                # discarded anyway.
                self.msg.append('Not using stream: {}'.format(hp_msg))
                return

            # Run through av1bdec to make sure that the stream is ok
            av1bdec = self.run_config.build_config.binary_path('av1bdec')
            frame_rate = (self.run_config.encoder_profile.
                          level_info.level_max.frame_rate)
            if not self._logged_proc(('av1bdec',
                                      'Decoding stream with av1bdec',
                                      [av1bdec, '-v',
                                       '--display-frame-rate', str(frame_rate),
                                       '-o', '/dev/null', stream])):
                return

        except (RuntimeError, AssertionError) as exception:
            self.msg.append('ERROR: Caught exception: {}'.format(exception))


EncSettings = NamedTuple('EncSettings',
                         [('config_args', Optional[Tuple[str, str]]),
                          ('ub_val', Optional[float])])


class EncoderProfile:
    '''Represents an encoder together with a possible configuration'''
    def __init__(self,
                 target: str,
                 encoder: Encoder,
                 level_info: LevelInfo,
                 settings: EncSettings):
        self.target = target
        self.encoder = encoder
        self.level_info = level_info
        self.settings = settings

    def has_binary(self, build_config: BuildConfig) -> bool:
        '''Return true if the underlying encoder has been built'''
        return self.encoder.find_binary(build_config) is not None

    def describe(self) -> str:
        '''Return a string describing this encoder / configuration pair'''
        return 'profile {}; config {}'.format(self.encoder.profile_name(),
                                              self.settings.config_args)

    def start_run(self,
                  build_config: BuildConfig,
                  database: Database,
                  targets: List[str],
                  backoff: int) -> RunThread:
        '''Start an encode/decode run. Returns the thread.

        The thread started will read build_config and may call
        build_config.add_fail_log and build_config.find_binary (so these must
        be thread safe).

        '''
        seed = random.randint(1, 100000)

        run_cfg = RunConfig(self, build_config, seed, backoff, targets)

        thread = RunThread(run_cfg, database)
        thread.start()
        return thread

    def encode_command(
            self,
            run_config: RunConfig,
            stream: str,
            config_args: Optional[Tuple[str, str]]) -> Optional[List[str]]:
        '''Return a command to build the stream with the given seed'''
        enc_path = self.encoder.find_binary(run_config.build_config)
        if enc_path is None:
            return None

        cmd = [enc_path, '-s', str(run_config.seed), '-o', stream]

        if config_args is None:
            config_args = self.settings.config_args

        if config_args is not None:
            config_file, configuration = config_args
            cmd += ['-C', config_file]
            if configuration is not None:
                cmd += ['-i', configuration]

        if run_config.backoff:
            cmd += ['--decrease-target-bitrate-percent',
                    str(run_config.backoff)]

        return cmd

    def skip_prob(self, best_seen: float) -> float:
        '''Return a probability of skipping this run

        The idea is that the config file might encode the highest
        value attainable. In that case, if we're there already, the
        only benefit of running again is to increase the stream
        size.

        '''
        if self.settings.ub_val is None:
            return 0

        ub_val = self.settings.ub_val  # type: float

        if best_seen > ub_val:
            raise RuntimeError('Incorrect upper bound in config file. '
                               'The upper bound evaluates to {}, but we have '
                               'already seen a value of {}.'
                               .format(ub_val, best_seen))

        # We want a function that starts at 0 and increases to 1 when
        # best_seen equals ub_val. Use an exponential.
        frac = best_seen / ub_val
        assert 0 <= frac <= 1

        # This function of frac starts really near zero and then jumps
        # up suddenly to 1 at frac=1. With a coefficient of 10, you
        # reach 0.1 at about 81% and 0.5 at about 94%.
        if frac < 0.1:
            raw = 0.0
        else:
            raw = math.exp(10) * math.exp(- 10 / (frac))

        # The 1.1 is to get around floating point silliness.
        assert 0 <= raw <= 1.1

        # 10% of the time, we run it anyway.
        return 0.9 * min(raw, 1)
