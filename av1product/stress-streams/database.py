################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A module that represents a stress stream database'''

import os
import shutil
import sys
import threading
from typing import Dict, List, Optional, Tuple

import buildtools
from run_data import RunData


_Triple = Tuple[int, str, Optional[int]]
_HighPt = Tuple[str, int]


class Database:
    '''A class representing the `database' of best values

    Class methods should all be thread safe.'''
    def __init__(self,
                 path: str,
                 stream_dir: Optional[str],
                 header: Optional[str] = None):
        self.path = path
        self.stream_dir = stream_dir

        # self.best may be changed from other threads. This is protected by
        # self.__lock. The lock is also used to serialise writes to the streams
        # directory.
        self.__lock = threading.Lock()
        self.best = {}  # type: Dict[str, _Triple]

        real_header = self.__load(header)

        # If the header argument was none, the database must exist.
        # The __load method will return the header it reads on success
        # or the header it is given if there is no file.
        if real_header is None:
            raise RuntimeError('No database loaded from file `{}\'.'
                               .format(path))

        self.header = real_header

    def __read_first_line(self,
                          line_number: int,
                          line: str,
                          header: Optional[str]) -> str:
        '''Read the initial line of a db file and check profile/level match'''
        if header is not None and line != header:
            raise RuntimeError('{}:{}: Initial line mismatch. '
                               'The file specifies profile/level of {} but '
                               'we expect {}.'
                               .format(self.path, line_number,
                                       line, self.header))

        return line

    def __read_triple(self, line_number: int, line: str) -> None:
        '''Read a triple from the body of a db file.'''
        parts = line.split()
        if len(parts) != 3:
            raise RuntimeError('{}:{}: Doesn\'t have 3 parts.'
                               .format(self.path, line_number))

        name, best_str, stream = parts
        try:
            fbest_val = float(best_str)
        except ValueError:
            raise RuntimeError('{}:{}: Best value '
                               'not parseable as float.'
                               .format(self.path, line_number))

        # We actually know that values are stored as integers, so
        # check that here.
        if fbest_val - int(fbest_val):
            raise RuntimeError('{}:{}: Best value is {}, not actually an '
                               'integer.'
                               .format(self.path, line_number, fbest_val))

        # Figure out the size of the stream
        size = None
        if self.stream_dir is not None:
            try:
                size = os.stat(os.path.join(self.stream_dir, stream)).st_size
            except FileNotFoundError:
                pass

        self.best[name] = (int(fbest_val), stream, size)

    def __load(self, header: Optional[str]) -> Optional[str]:
        '''Load a database file if it already exists.

        Returns header line on success.'''

        # Format is a single line with level and profile, then lines with
        # space-separated name, value, stream triples.
        try:
            first = True
            with open(self.path) as handle:
                for idx, line in enumerate(handle):
                    line = line.strip()
                    if not line:
                        continue

                    if first:
                        header = self.__read_first_line(idx + 1, line, header)
                        first = False
                    else:
                        self.__read_triple(idx + 1, line)

            if first:
                raise RuntimeError('{}: Empty db file.'
                                   .format(self.path))

        except FileNotFoundError:
            # No such file? No problem; this is the first run.
            pass

        return header

    def __save(self) -> None:
        '''Write best out to the database file

        This assumes that the lock has been taken to access self.best.

        '''
        with open(self.path, 'w') as handle:
            handle.write(self.header + '\n')
            for name, (value, stream, _) in sorted(self.best.items()):
                assert ' ' not in name
                handle.write('{:32} {:14} {}\n'
                             .format(name, value, stream))

    @staticmethod
    def _is_better(name: str,
                   best: Dict[str, _Triple],
                   new_val: int,
                   new_size: Optional[int]) -> Tuple[bool, Optional[int]]:
        '''Return true if new_val is better than what we have in best'''
        triple = best.get(name)
        if triple is None:
            return (True, None)

        best_val, _, best_size = triple

        better = best_val < new_val
        smaller = (new_size is not None and
                   (best_size is None or
                    new_size < best_size))

        # We should take it if either the value is better than before OR if the
        # value is equal but the file is smaller.
        return ((better or (new_val == best_val and smaller)), best_val)

    @staticmethod
    def _check_high_point(name: str,
                          value: int,
                          best: Dict[str, _Triple],
                          is_target: bool,
                          file_size: int) -> Tuple[bool, Optional[str]]:
        '''Check a single target for high_points'''
        should_update, best_val = Database._is_better(name, best,
                                                      value, file_size)
        if best_val is None:
            assert should_update
            return (True,
                    'first value for {}: {:,}.'.format(name, value))

        msg = None  # type: Optional[str]
        if is_target:
            if should_update:
                msg = ('improved {}: {:,} better than {:,}.'
                       .format(name, value, best_val))
            else:
                msg = ('{} is {:,} (worse than existing value of {:,}).'
                       .format(name, value, best_val))

        return (should_update, msg)

    def high_points(self,
                    stats: Dict[str, int],
                    file_size: int,
                    target: str,
                    targets: List[str]) -> Tuple[List[_HighPt], str]:
        '''Return a list of new high points in stats.

        The return value is actually a pair (L, m) where L is a list of pairs
        (name, value) for new high points in stats and m is a string describing
        either how the first element of L came about (we were better at X) or
        why this stream is no better at target.

        '''

        # Take a copy of self.best so that we can do most of our work without
        # the lock held. We can do this with a shallow copy because self.best
        # is keyed by (immutable) strings and has float values (which are
        # copied by the shallow copy)
        with self.__lock:
            best_copy = self.best.copy()

        msg = 'no {} and no other improvement.'.format(target)

        ret = []
        for name in targets:
            value = stats.get(name)
            if value is None:
                continue

            good, new_msg = Database._check_high_point(name, value, best_copy,
                                                       name == target,
                                                       file_size)
            if good:
                ret.append((name, value))
            if new_msg is not None:
                msg = new_msg

        return (ret, msg)

    @staticmethod
    def __move_stream(seed: Optional[int], src: str, dst_dir: str) -> str:
        '''Move the stream at src to dst_dir/test<seed>_??.obu

        This is not thread-safe (it checks whether the destination path exists,
        then creates the file). If you have multiple threads calling this with
        one directory, you'll need a lock.

        '''
        os.makedirs(dst_dir, exist_ok=True)
        basename = ('test{}.obu'.format(seed) if seed is not None
                    else os.path.basename(src))
        dst_path = os.path.join(dst_dir, basename)
        if os.path.exists(dst_path):
            # Oh dear! A duplicate? Append a counter prefixed with a zero (to
            # distinguish from a merged stream).
            base, ext = os.path.splitext(basename)
            for suff_idx in range(100):
                long_path = \
                    os.path.join(dst_dir,
                                 '{}_0{}{}'.format(base, suff_idx, ext))
                if not os.path.exists(long_path):
                    dst_path = long_path
                    break

            # This is looking bad: we can't find a name that doesn't collide.
            raise RuntimeError('Couldn\'t find non-colliding name for stream '
                               'with basename: `{}\'.'
                               .format(basename))
        shutil.move(src, dst_path)
        return dst_path

    def take_stream(self,
                    target: str,
                    seed: int,
                    run_data: RunData,
                    targets: List[str]) -> Tuple[Optional[Tuple[List[_HighPt],
                                                                str]],
                                                 str]:
        '''Add a stream to the database if it hits a new best point

        If the stream is of interest, this returns ((L, S), M) where L is a
        list of pairs (name, value) for the best points that we hit and S is
        the name of the new stream. Otherwise, it returns (None, M). In either
        case, M is a string describing why the stream is or is not interesting.

        '''
        assert run_data.stats is not None
        assert run_data.size is not None
        assert run_data.stream is not None

        if self.stream_dir is None:
            raise RuntimeError('Cannot take stream with no stream_dir')

        to_set, msg = self.high_points(run_data.stats,
                                       run_data.size,
                                       target,
                                       targets)
        if not to_set:
            return (None, msg)

        with self.__lock:
            path = Database.__move_stream(seed,
                                          run_data.stream,
                                          self.stream_dir)
            stream = os.path.basename(path)
            for name, value in to_set:
                self.best[name] = (value, stream, run_data.size)

            self.__save()
            self.__prune()

        return ((to_set, stream), msg)

    def drop_stream(self, target: str) -> None:
        '''Remove any stream for target in the database'''
        if self.stream_dir is None:
            raise RuntimeError('Cannot drop stream with no stream_dir')

        with self.__lock:
            if target in self.best:
                del self.best[target]
            self.__save()
            self.__prune()

    def __prune(self) -> None:
        '''Delete any streams in stream_dir that are no longer needed

         This assumes that the lock has been taken to access self.best.

        '''
        if self.stream_dir is None:
            raise RuntimeError('Cannot prune with no stream_dir')

        live_streams = set()
        for _, stream, _ in self.best.values():
            live_streams.add(stream)

        for name in os.listdir(self.stream_dir):
            if name not in live_streams:
                sys.stdout.write('Deleting dead stream `{}\'.\n'
                                 .format(name))
                sys.stdout.flush()
                buildtools.rm_f(os.path.join(self.stream_dir, name))

    def best_value(self, target: str) -> Optional[float]:
        '''Return the best value seen so far for target.

        Returns None if none has been seen.'''
        with self.__lock:
            triple = self.best.get(target)
            return None if triple is None else triple[0]

    def merge(self, src_db: 'Database') -> None:
        '''Take any streams from src_db that are better than our ones.

        Modifies self.best without locking: this needs modification if you want
        to call it from stresser.py

        '''
        if self.stream_dir is None:
            raise RuntimeError('Cannot merge with no stream_dir')
        if src_db.stream_dir is None:
            raise RuntimeError('Cannot merge source with no stream_dir')

        # Build a set of streams to take as a dictionary, keyed by stream name
        # and whose values are (size, prs) where prs is a list of (name, value)
        # pairs.
        to_take = {}  # type: Dict[str, Tuple[Optional[int], List[_HighPt]]]

        for name, (src_val, src_stream, src_size) in src_db.best.items():
            take_it, _ = Database._is_better(name, self.best,
                                             src_val, src_size)
            if take_it:
                elt = to_take.setdefault(src_stream, (src_size, []))
                elt[1].append((name, src_val))

        for src_stream, (src_size, pairs) in to_take.items():
            print('Taking stream {} from {}.'
                  .format(src_stream, src_db.stream_dir))
            src_path = os.path.join(src_db.stream_dir, src_stream)
            path = Database.__move_stream(None, src_path, self.stream_dir)
            for name, src_val in pairs:
                self.best[name] = (src_val, os.path.basename(path), src_size)

        self.__save()
        self.__prune()
