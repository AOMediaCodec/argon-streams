/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

default {
    // Generate as many inter frames as possible
    enc_frame_is_inter = 1;

    // And inter blocks in the inter frames
    enc_inter_prob = 100;

    // Choose small (but nonzero) motion vectors.
    enc_mv_max_row = 4;
    enc_mv_max_col = 4;

    enc_specify_partitions = 1;

    if (frame_number == 0) {
        // For the first frame, we don't want to spend many bits. So
        // we signal partition_split and try to skip as much as we
        // can.
        partition = PARTITION_NONE;

    } else {
        // Aim for 4x4 blocks with enc_backoff = 0, but allow bottoming
        // out in the tree a little earlier otherwise.
        partition =
            choose (weighted (PARTITION_SPLIT weight (100 - enc_backoff),
                              (uniform (PARTITION_NONE ..
                                        (num4x4 <= 2 ? PARTITION_SPLIT :
                                         num4x4 < 32 ? PARTITION_VERT_4 :
                                         PARTITION_VERT_B)) weight enc_backoff)));
    }

    // Pretty much always NEARESTMV if possible; otherwise
    // (grudgingly) choose uniformly from the other options or NEWMV.
    enc_inter_mode =
        choose (weighted (NEARESTMV weight (1000 * can_use_nearest),
                          NEARMV weight can_use_near,
                          GLOBALMV weight can_use_global));

    // Disable segmentation
    segmentation_enabled = 0;

    // Don't use warped motion (it makes predicting motion vectors
    // rather harder)
    enable_warped_motion = 0;

    // Don't waste bits on coefficients.
    enc_num_non_zero_coeffs = 0;

    // Always use a QP of 1 to make sure we don't accidentally code a
    // lossless frame.
    base_q_idx = 1;
    delta_coded = 0;
    separate_uv_delta_q = 0;
    delta_q_present = 0;

    // Don't spend bits on switchable interpolation filters
    is_filter_switchable = 0;

    // Because we're dividing down to 4x4 blocks, we always signal single
    // references. Then we signal the same references most of the time (to train
    // the CDFs)
    enc_single_ref =
        choose (weighted (LAST_FRAME weight 99,
                          uniform (LAST_FRAME .. ALTREF_FRAME) weight 1));
}
