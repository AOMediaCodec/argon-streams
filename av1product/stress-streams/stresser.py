#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A module to generate AV1 stress streams and evaluate them'''

import argparse
import os
import random
import re
import signal
import sys
import tempfile
import threading
import time
# Avoid pylint messages about unused List and Optional: they're only used in
# typing comments, which flake8 understands but pylint doesn't.
#
# pylint: disable=unused-import
from typing import Any, Dict, List, Optional, Tuple
# pylint: enable=unused-import

import buildtools
from stress_config import StressConfig
from level_info import get_level_info, get_levels
from database import Database
from build_config import BuildConfig
from codec import EncoderProfile, RunThread

# As above, this is just used in typing comments
#
# pylint: disable=unused-import
from codec import BuilderThread
# pylint: enable=unused-import


def message(msg: str) -> None:
    '''Print a message to stdout and flush'''
    sys.stdout.write(msg + '\n')
    sys.stdout.flush()


# These are the tags that we get from av1bdec or av1enc when we're "too big" in
# some sense. Each time we get this, we'll dial down our enthusiasm a little by
# increasing enc_backoff for this encoder config.
TOO_BIG_TAGS = ['BITRATE', 'COMPRATIO', 'STREAM_SIZE', 'EARLY_STOP']


class Backoffs:
    '''A backoff tracker'''

    _State = Tuple[float, float]

    def __init__(self, initial_value: float):
        # A dictionary keyed by target name with backoff percentages. Entries
        # are pairs (P, M) where P is the backoff percentage and M is a
        # multiplier that we use on updates. This multiplier gets applied to
        # the change in backoff percentages and drops over time. The idea is
        # that this is like a very simple form of simulated annealing and
        # (hopefully) should make sure the backoff adjustment is always stable
        # in the long term.
        self.vals = {}  # type: Dict[str, Backoffs._State]

        # The initial value for a new target
        self.initial_value = initial_value

        # The initial increment when we see a new target.
        self.initial_increment = 20
        # The smallest increment that we can go down to.
        self.min_value = 0.01

    def update(self, target_name: str, backoff: float, too_big: bool) -> None:
        '''Update our internal backoff counter for this target name

        backoff is the backoff that the run used. too_big is true if the result
        was too large (so the backoff should be increased)

        '''
        minv = self.min_value

        old_val, old_inc = self.vals.get(target_name, (self.initial_value,
                                                       self.initial_increment))

        # Set base to either backoff or old_val, depending on which would
        # push me less.
        base = min(old_val, backoff) if too_big else max(old_val, backoff)

        # Add the increment to old_val
        new_val = base + (1 if too_big else -1) * old_inc
        clamped = min(max(new_val, 0), 100)

        # If clamped pushes old_val in the direction we intended, apply it.
        # Otherwise, reuse old_val.
        use_me = (clamped > old_val) if too_big else (old_val > clamped)
        if use_me:
            self.vals[target_name] = (clamped, max(minv, 0.99 * old_inc))

    def get_val(self, target_name: str) -> float:
        '''Get the current backoff value for the given target name'''
        state = self.vals.get(target_name)
        base, inc = (state if state is not None
                     else (self.initial_value, self.initial_increment))

        # We want a backoff normally distributed about base with standard
        # deviation equal to inc.
        backoff = random.normalvariate(base, inc)

        # But we need to clamp it to 0, 100
        return min(max(backoff, 0), 100)


class FreeSlots:
    '''A class representing the free slots for jobs'''
    def __init__(self, num_slots: int):

        # The number of job slots not currently assigned to a build or
        # encode/decode. Protected by self.__lock.
        self.slots_available = num_slots

        # When building encoders, we might overcommit, which means starting
        # more jobs than we're technically allowed. This is a hack to allow us
        # to "make -j4" even if we're unlucky and start the build with the one
        # remaining slot. To avoid killing the machine, we only allow ourselves
        # to overcommit by 10%. Note that, when this happens, slots_available
        # goes negative.
        self.over_commit = int(0.1 * num_slots)

    def free_slots(self, count: int) -> None:
        '''Mark zero or more slots free'''
        assert count >= 0
        self.slots_available += count

    def take_slots(self, count: int) -> None:
        '''Mark zero or more slots taken'''
        assert count >= 0
        # slots available might go negative because of overcommit but that's
        # fine: we just check that we don't go more negative than
        # self.over_commit.
        assert self.slots_available - count >= - self.over_commit
        self.slots_available -= count

    def count_available(self, for_encode: bool) -> int:
        '''Return the number of slots available'''
        ret = self.slots_available
        if for_encode:
            ret += self.over_commit

        return ret


class Targets:
    '''A class representing the set of targets that we need to hit'''
    def __init__(self, encoder_profiles: Dict[str, EncoderProfile]):
        assert encoder_profiles

        # The next target to run in our round robin scheduler
        self.next_target = 0

        # The encoder profiles of our various targets
        self.encoder_profiles = encoder_profiles

        # The list of targets, indexed by next_target
        self.target_list = list(encoder_profiles.keys())

        # These are jobs that should be run before we go back to our round
        # robin
        self.pending = []  # type: List[str]

        # These are jobs that are currently waiting for an encoder build. We'll
        # add them to pending once the encoder is built.
        self.needs_encoder = []  # type: List[str]

    def _get(self, target: str) -> EncoderProfile:
        '''Get an encoder profile by index'''
        return self.encoder_profiles[target]

    def _next_target(self) -> EncoderProfile:
        '''Get the next target, incrementing the round-robin'''
        key = self.target_list[self.next_target]
        self.next_target += 1
        if self.next_target >= len(self.target_list):
            self.next_target = 0
        return self._get(key)

    def take(self, database: Database) -> EncoderProfile:
        '''Get the next target to work on'''
        if self.pending:
            return self._get(self.pending.pop(0))

        while True:
            enc_profile = self._next_target()

            # Should we skip this target? Maybe we've got pretty close already?
            best_value = database.best_value(enc_profile.target)
            skip_prob = (0 if best_value is None
                         else enc_profile.skip_prob(best_value))

            # Make sure we don't get into an infinite loop
            assert skip_prob <= 0.9

            if random.random() >= skip_prob:
                return enc_profile

    def on_encoder_built(self) -> None:
        '''Add stuff that was waiting for an encoder to the pending list'''
        self.pending = self.needs_encoder + self.pending
        self.needs_encoder = []

    def add_waiting(self, target: str) -> None:
        '''Add a target that's currently waiting for an encoder'''
        self.needs_encoder.append(target)


class SignalTracker:
    '''A class that encapsulates the "are we stopping" logic for dispatcher'''
    def __init__(self, stop_times: Optional[Tuple[int, int]]) -> None:
        # A lock used for handling asynchronous signals. The signal handlers
        # just look like Python threads that run at random times. Take this
        # lock before reading or writing anything.
        self.__lock = threading.Lock()

        # State for SIGINT handling. cleanup_and_die = 0 means we're still
        # going. 1 means not to schedule any more jobs. >= 2 means to kill
        # everything (and return with a nonzero exit code). Protected by
        # self.__lock.
        self._cleanup_and_die = 0

        # Soft and hard time limits. None means to run forever.
        self._stop_times = stop_times

    def on_sigint(self, _: int, __: Any) -> None:
        '''Signal handler used for catching SIGINT'''
        with self.__lock:
            old = self._cleanup_and_die
            self._cleanup_and_die = min(self._cleanup_and_die + 1, 2)

        if old == 0:
            sys.stderr.write('SIGINT caught. Running until all slots done.\n'
                             'Send another interrupt for immediate exit.\n')
        elif old == 1:
            sys.stderr.write('Later SIGINT caught. Killing child processes.\n')
        else:
            # This is the third or later sigint. Note that we never uninstall
            # our signal handler: we're going to try to kill all our child
            # processes, which are in different process groups from us, so
            # might otherwise outlive us. This should work but if it doesn't,
            # the user will have to clean up themselves.
            sys.stderr.write('SIGINT. Already trying to exit. '
                             'If you really must kill me, use SIGKILL and '
                             'clean up my child processes too.')

    def exiting(self) -> bool:
        '''Return true if we have received a SIGINT and are exiting'''
        with self.__lock:
            return self._cleanup_and_die != 0

    def note_time(self) -> Tuple[bool, bool]:
        '''Take a note of the current time and compare with stop_times'''
        with self.__lock:
            cad = self._cleanup_and_die

        if cad == 2:
            # No point in looking at the watch. We should be exiting either
            # way.
            return (True, True)

        if self._stop_times is None:
            # Again, no point at looking at the watch. Just report our current
            # cad value.
            return (cad >= 1, cad >= 2)

        cur_time = time.clock_gettime(time.CLOCK_MONOTONIC)
        st0, st1 = self._stop_times

        new_cad = cad
        if cur_time > st0 and not cad:
            message('Reached soft time limit. '
                    'Remaining jobs will run to completion.')
            new_cad = 1
        if cur_time > st1:
            message('Reached hard time limit. '
                    'Killing any remaining jobs.')
            new_cad = 2

        if cad != new_cad:
            with self.__lock:
                self._cleanup_and_die = new_cad

        return (new_cad >= 1, new_cad >= 2)


class Dispatcher:
    '''The main dispatcher/scheduler for building and running encoders'''
    def __init__(self,
                 num_slots: int,
                 initial_backoff: float,
                 build_config: BuildConfig,
                 targets: Dict[str, EncoderProfile],
                 database: Database):

        assert targets

        self.build_config = build_config
        self.database = database

        self.slots = FreeSlots(num_slots)
        self.targets = Targets(targets)
        self.backoffs = Backoffs(initial_backoff)
        self.signal_tracker = SignalTracker(build_config.stop_times)

        # An alist of running jobs (elements are (target_name, thread)
        # pairs)
        self.running = []  # type: List[Tuple[str, RunThread]]

        # If we need to build an encoder, the (unique) thread that's watching
        # the build will be stored in build_thread. Any jobs that need an
        # encoder won't be enqueued; instead, we will move them to the
        # needs_encoder list and they will be prepended to self.pending when
        # the build finishes.
        self.build_thread = None  # type: Optional[BuilderThread]

    def update_with_results(self, target_name: str, thread: RunThread) -> None:
        '''Take the stats for a finished job'''
        stream_desc = ('Stream with seed {} for {}'
                       .format(thread.run_config.seed, target_name))
        if thread.run_config.backoff:
            stream_desc += ' (backoff {})'.format(thread.run_config.backoff)

        too_big = False
        if thread.failed_checks:
            # The stream failed some innocuous check when encoding or decoding.
            message('{} failed checks: {}'
                    .format(stream_desc, ', '.join(thread.failed_checks)))
            if set(TOO_BIG_TAGS) & set(thread.failed_checks):
                too_big = True

        if thread.msg:
            # Something went wrong or we have something to say. Print out any
            # messages and return.
            message('{} yielded messages:'.format(stream_desc))
            for msg in thread.msg:
                message('  ' + msg)

        # Only update the backoff if either we succeeded in making a stream or
        # we failed in a way that will increase it (we don't want to update the
        # backoff if we failed for some other reason). Note that we can't check
        # for success by checking that thread.msg is empty because we add a
        # message if there's nothing exciting about the stream.
        if too_big or not thread.bad:
            self.backoffs.update(target_name,
                                 thread.run_config.backoff, too_big)

        if thread.failed_checks or thread.msg:
            return

        if not (thread.run_data and
                thread.run_data.stream and
                os.path.exists(thread.run_data.stream)):
            # If not everything exists, something failed so there's nothing to
            # do here. Print a message to mention that something went wrong.
            message('{} produced no stream. Skipping.'.format(stream_desc))
            return

        (hp_pr, hp_msg) = \
            self.database.take_stream(target_name,
                                      thread.run_config.seed,
                                      thread.run_data,
                                      self.targets.target_list)

        if hp_pr is None:
            # This only happens rarely because of the early exit in
            # RunThread.run.
            message('Not using {}; {}'.format(stream_desc, hp_msg))
            return

        high_points, stream = hp_pr
        message('Stream hit new values; saved as `{}\'.'.format(stream))
        for name, value in high_points:
            message('  - `{}\': {}.'.format(name, value))

    def reap(self) -> None:
        '''Reap any finished jobs, returning their slots to the pool.'''

        freed_slots = 0

        if self.build_thread is not None and not self.build_thread.is_alive():
            # A build has finished. Reclaim its job slots and clear it. Also,
            # prepend any jobs that were waiting on the encode to the list of
            # jobs to be run.
            freed_slots += self.build_thread.num_jobs

            if not self.build_thread.binary:
                message('Failed to build encoder for profile `{}\'. '
                        'Log file at `{}\'.'
                        .format(self.build_thread.encoder.profile_name(),
                                self.build_thread.log_name))
            else:
                message('Finished building encoder at `{}\'.'
                        .format(self.build_thread.binary))

            self.build_thread = None
            self.targets.on_encoder_built()

        new_running = []

        for pair in self.running:
            target_name, thread = pair
            if not thread.is_alive():
                # The encode/decode job finished. Take the results,
                # clean up, return the slot to the pool and put the
                # target back on the end of the pending list.
                try:
                    self.update_with_results(target_name, thread)
                finally:
                    if thread.run_data and thread.run_data.stream:
                        buildtools.rm_f(thread.run_data.stream)

                freed_slots += 1
            else:
                new_running.append(pair)

        if freed_slots:
            self.slots.free_slots(freed_slots)
            self.running = new_running

    def enqueue(self) -> bool:
        '''Enqueue a job if it fits. Return True if some progress was made'''

        if self.signal_tracker.exiting():
            # We've had a SIGINT. Don't start anything new.
            return False

        if self.slots.count_available(False) <= 0:
            # No space. (See the note about self.over_commit for why this might
            # be negative).
            return False

        # Pick a job to work on. Either pop from self.pending or use the round
        # robin.
        enc_profile = self.targets.take(self.database)

        # Do we have an encoder yet?
        if not enc_profile.has_binary(self.build_config):
            # Firstly, add the current key to the back of
            # needs_encoder: we want to actually run this stream
            # later!
            self.targets.add_waiting(enc_profile.target)

            # If there is an encoder thread running, we can't start
            # the encode right now. That's fine: we've just added this
            # key to the back of needs_encoder and it will be put back
            # at the front of pending when the encode finishes. In
            # this case, return True (because something changed: we
            # might be able to start the next job).
            if self.build_thread is not None:
                return True

            # Otherwise, we can start an encoder building.
            message('Building encoder for profile `{}\'.'
                    .format(enc_profile.encoder.profile_name()))

            # How many slots should we offer? Don't allow over-commit if
            # we're not actually doing anything else.
            slots = self.slots.count_available(True)

            self.build_thread = \
                enc_profile.encoder.start_build(self.build_config, slots)

            self.slots.take_slots(self.build_thread.num_jobs)

            return True

        # If we get here, the encoder has a binary and we're ready to run.
        # Excellent! Time to start an encode.
        backoff = self.backoffs.get_val(enc_profile.target)
        thread = enc_profile.start_run(self.build_config,
                                       self.database,
                                       self.targets.target_list,
                                       round(backoff))
        backoff_msg = ('' if not backoff else
                       '; backoff {:.2f}'.format(backoff))
        message('Starting run for {}; seed {}{}'
                .format(enc_profile.target,
                        thread.run_config.seed, backoff_msg))
        self.running.append((enc_profile.target, thread))
        self.slots.take_slots(1)

        return True

    def run(self) -> int:
        '''Run the dispatcher (until stopped with SIGINT or timeout)'''
        signal.signal(signal.SIGINT, self.signal_tracker.on_sigint)

        retcode = 0

        while True:
            # Reap anything that's finished
            self.reap()

            # Start as many things as we can
            while self.enqueue():
                pass

            exiting, hard_stop = self.signal_tracker.note_time()

            # If hard_stop is true, this is an emergency stop. Tell all the
            # running threads to clean themselves up sharpish!
            if hard_stop:
                for _, thread in self.running:
                    thread.emergency_stop.set()

            # Should we stop? Yes if there is no longer anything running and
            # exiting is set.
            if exiting and not self.running:
                if hard_stop:
                    retcode = 1
                message('All jobs have finished. Exiting.')
                break

            # Wait a bit before checking again.
            time.sleep(0.1)

        return retcode


def main() -> int:
    '''Main entry point for script'''
    parser = argparse.ArgumentParser()
    parser.add_argument('--jobs', '-j', default=1, type=int)
    parser.add_argument('--bindir', default='bin',
                        help=('Directory in which to place built encoders. If '
                              'not set, this defaults to ./bin. We also '
                              'expect this directory to contain decoders '
                              '(av1dec, av1bdec, aomdec and '
                              'lightfield_tile_list_decoder).'))
    parser.add_argument('--logdir', default='log',
                        help=('Directory in which to place logs. If '
                              'not set, this defaults to ./log.'))
    parser.add_argument('--streams', default='streams',
                        help=('Directory in which to place streams. If not '
                              'set, this defaults to ./streams.'))
    parser.add_argument('--profile', default=0, type=int,
                        help=('Profile to build; defaults to 0.'))
    parser.add_argument('--level', default='5.0',
                        choices=get_levels(),
                        help=('Level in which to generate streams. '
                              'Defaults to 5.0.'))
    parser.add_argument('--backoff', default=0, type=float,
                        help=('Initial backoff percentage. Defaults to 0.'))
    parser.add_argument('--timeout', type=int,
                        help=('Timeout in seconds. After running for this '
                              'long, we behave as if we received a single '
                              'interrupt, finishing up remaining streams '
                              'and then exiting. If we haven\'t finished '
                              'in 150%% of the timeout, we hard-kill all '
                              'remaining processes (in the same way as if '
                              'we received a second SIGINT).'))
    parser.add_argument('--filter', dest='tgt_regex',
                        help=('A filter to apply to target names. '
                              'Default: No filter.'))
    parser.add_argument('config')
    parser.add_argument('db')

    args = parser.parse_args()

    if args.backoff < 0 or args.backoff > 100:
        raise RuntimeError('Invalid initial backoff percentage of {}.'
                           .format(args.backoff))

    # Tell buildtools about the AV1-specific tags that we don't mind
    # failing (we'll reject the stream, but needn't bother storing the
    # output log).
    for tag in ['STREAM_SIZE', 'MV_CLASS_TOO_LARGE', 'BITRATE',
                'DECODER_MODEL', 'LARGE_SCALE_TILE', 'COMPRATIO']:
        buildtools.store_check(tag, 'reject')

    level_info = get_level_info(args.level, args.profile)

    tgt_regex = None
    try:
        if args.tgt_regex is not None:
            tgt_regex = re.compile(args.tgt_regex)
    except re.error as err:
        raise RuntimeError('Bad regex for target filtering: {}.'.format(err))

    with tempfile.TemporaryDirectory(prefix='stresser-scratch-') as scratch:
        stress_config = StressConfig(args.config, level_info, tgt_regex)

        if not stress_config.targets:
            raise RuntimeError('No targets in {} match the regex `{}\'.'
                               .format(args.config, args.tgt_regex))

        for name, encoder_profile in stress_config.targets.items():
            if encoder_profile.settings.ub_val is not None:
                print('Note: Upper bound for {} is {:,}'
                      .format(name, encoder_profile.settings.ub_val))

        database = Database(args.db, args.streams,
                            '{} {}'.format(args.profile, args.level))
        build_config = BuildConfig(args.bindir, args.logdir,
                                   args.timeout, scratch)

        dispatcher = Dispatcher(args.jobs, args.backoff, build_config,
                                stress_config.targets, database)
        return dispatcher.run()


if __name__ == '__main__':
    try:
        sys.exit(main())
    except RuntimeError as error:
        sys.stderr.write('Error: {}\n'.format(error))
        sys.exit(1)
