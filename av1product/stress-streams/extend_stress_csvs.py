#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Code for extending CSVs with stress stream info'''

import argparse
import csv
import os
import shutil
import sys
from typing import Dict, List, Optional, Tuple

import database

_StreamDesc = Tuple[str, List[Tuple[str, int]]]
_StressDBs = Dict[Tuple[str, int], _StreamDesc]


def db_files(path: str) -> List[str]:
    '''Return a list of paths to stress dbs below path'''
    ret = []
    for root, _, files in os.walk(path):
        for name in files:
            if name == 'db':
                ret.append(os.path.join(root, 'db'))
    return ret


def load_stress_db(db_path: str, dbs: _StressDBs) -> None:
    '''Load a stress db at db_path'''
    dbase = database.Database(db_path, None, None)
    hdr_parts = dbase.header.split()
    if len(hdr_parts) != 2:
        raise RuntimeError('{}: Database first line is `{}\', which '
                           'doesn\'t have two parts.'
                           .format(db_path, dbase.header))
    try:
        profile = int(hdr_parts[0])
    except ValueError:
        raise RuntimeError('{}: Cannot parse `{}\' as an integer '
                           'profile.'
                           .format(db_path, hdr_parts[0]))

    for tgt, (value, name, _) in dbase.best.items():
        key = (name, profile)
        if key in dbs and dbs[key][0] != db_path:
            raise RuntimeError('{}: Multiple DB files mention stream '
                               '`{}\', profile {} (already seen in {}).'
                               .format(db_path, name, profile, dbs[key][0]))

        entry = dbs.setdefault(key, (db_path, list()))
        entry[1].append((tgt, value))


def load_stress_dbs(path: str) -> _StressDBs:
    '''Load all the stress databases below path

    Returns a dictionary keyed by stream name whose values are
    (profile, target, value) pairs.

    '''
    ret = {}  # type: _StressDBs
    for db_path in db_files(path):
        load_stress_db(db_path, ret)
    return ret


class Extender:
    '''A helper class to extend rows from a CSV'''

    def __init__(self, in_path: str, dbs: _StressDBs) -> None:
        self.in_path = in_path
        self.dbs = dbs
        self.stream_col = None  # type: Optional[int]
        self.prof_col = None  # type: Optional[int]

    def extend(self, line_number: int, row: List[str]) -> List[str]:
        '''Extend one row'''
        if self.stream_col is None:
            # Headers.
            if 'Stream name' not in row or 'Profile' not in row:
                raise RuntimeError('{}: Can\'t find stream name and '
                                   'profile in header row.'
                                   .format(self.in_path))
            self.stream_col = row.index('Stream name')
            self.prof_col = row.index('Profile')

            return row + ['Stress stream info']

        assert self.stream_col is not None
        assert self.prof_col is not None

        if len(row) <= max(self.stream_col, self.prof_col):
            raise RuntimeError('{}:{}: Row has {} columns '
                               '(not enough for name and index)'
                               .format(self.in_path, line_number, len(row)))

        name = row[self.stream_col]
        txt_profile = row[self.prof_col]

        try:
            profile = int(txt_profile)
        except ValueError:
            raise RuntimeError('{}:{}: Profile `{}\' does not parse as an int.'
                               .format(self.in_path, line_number, txt_profile))

        stream_desc = self.dbs.get((name, profile), None)
        if stream_desc is None:
            raise RuntimeError('{}:{}: Can\'t find stress DB for '
                               'stream {}, profile `{}\'.'
                               .format(self.in_path, line_number,
                                       name, profile))

        return row + ['; '.join('{}={}'.format(tgt, val)
                                for tgt, val in stream_desc[1])]

    @staticmethod
    def extend_csv(in_path: str, out_path: str, dbs: _StressDBs) -> None:
        '''Add stress info to CSV at in_path, writing to out_path'''
        with open(in_path, newline='') as in_file:
            with open(out_path, 'w', newline='') as out_file:
                extender = Extender(in_path, dbs)
                reader = csv.reader(in_file)
                writer = csv.writer(out_file)

                for row_idx, row in enumerate(reader):
                    writer.writerow(extender.extend(row_idx + 1, row))


def patch_set(in_path: str,
              out_path: str,
              dbs: Optional[_StressDBs]) -> None:
    '''Patch or copy a whole set of CSV files'''
    for entry in os.scandir(in_path):
        if entry.name.endswith('.csv') and entry.is_file():
            dst_path = os.path.join(out_path, entry.name)
            if dbs is None:
                shutil.copy(entry.path, dst_path)
            else:
                Extender.extend_csv(entry.path, dst_path, dbs)


def patch_sets(in_path: str, out_path: str, dbs: _StressDBs) -> None:
    '''Patch or copy multiple sets of CSV files'''
    for entry in os.scandir(in_path):
        if not entry.is_dir():
            continue

        is_stress = 'stress' in entry.name
        dst_path = os.path.join(out_path, entry.name)
        try:
            os.mkdir(dst_path)
        except OSError as err:
            raise RuntimeError('Failed to create directory `{}\' '
                               'for set: {}'
                               .format(dst_path, err))

        patch_set(entry.path, dst_path, dbs if is_stress else None)


def main() -> int:
    '''Main function for module'''
    parser = argparse.ArgumentParser()
    parser.add_argument('stress_dir')
    parser.add_argument('csv_in_dir')
    parser.add_argument('csv_out_dir')

    args = parser.parse_args()

    os.makedirs(args.csv_out_dir, exist_ok=True)
    patch_sets(args.csv_in_dir,
               args.csv_out_dir,
               load_stress_dbs(args.stress_dir))
    return 0


if __name__ == '__main__':
    try:
        sys.exit(main())
    except RuntimeError as err:
        sys.stderr.write('Error: {}'.format(err))
        sys.exit(1)
