################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''An expression parser for stresser.py'''

from typing import Any, Dict, List
import pyparsing as pp


Env = Dict[str, int]

# Pylint moans about Node and its subclasses only having one method.
# That's just how it works if you're using pyparsing. Tell it to shut
# up.
#
# pylint: disable=too-few-public-methods


class Node:
    '''The expression node base class'''
    def evaluate(self, env: Env) -> int:
        '''Evaluate this node with the given environment'''
        raise NotImplementedError


class Integer(Node):
    '''An expression node representing a literal integer'''
    def __init__(self, tokens: List[str]):
        self.value = int(tokens[0])

    def evaluate(self, env: Env) -> int:
        return self.value


class Symbol(Node):
    '''An expression node representing a symbol'''
    def __init__(self, tokens: List[str]):
        self.name = tokens[0]

    def evaluate(self, env: Env) -> int:
        if self.name not in env:
            raise RuntimeError('Environment does not define symbol `{}\'.'
                               .format(self.name))

        val = env[self.name]
        if not isinstance(val, int):
            raise RuntimeError('Environment defines symbol `{}\', '
                               'but it is not an integer.'
                               .format(self.name))

        return val


class Binop(Node):
    '''An expression node representing a binary operator'''
    def __init__(self, tokens: List[List[Node]]):
        # Left associative. The input, tokens[0], is a list of the
        # form
        #
        #   V0 OP0 V1 OP1 V2 OP2 V3 OP3 V4
        #
        # but we don't know that all the OPi are equal. We want to
        # collect things sensibly into binary operations of the form
        #
        #   (OP3 (OP2 (OP1 (OP0 V0 V1) V2) V3) V4)

        toks = tokens[0]
        assert len(toks) >= 3
        assert len(toks) & 1

        if len(toks) == 3:
            self.left = toks[0]
        else:
            self.left = Binop([toks[:-2]])

        self.operator = toks[-2]
        self.right = toks[-1]

    def evaluate(self, env: Env) -> int:
        left = self.left.evaluate(env)
        right = self.right.evaluate(env)
        if str(self.operator) == '*':
            return left * right
        if str(self.operator) == '/':
            return left // right
        if str(self.operator) == '+':
            return left + right

        assert str(self.operator) == '-'
        return left - right


def _mk_parser() -> Any:
    symbol = pp.Word(pp.alphas, pp.alphanums + "_")
    symbol.setParseAction(Symbol)

    integer = pp.Word(pp.nums)
    integer.setParseAction(Integer)

    return pp.infixNotation(integer | symbol,
                            [(pp.oneOf("* /"), 2, pp.opAssoc.LEFT, Binop),
                             (pp.oneOf("+ -"), 2, pp.opAssoc.LEFT, Binop)])


_PARSER = _mk_parser()


def evaluate(env: Env, string: str) -> int:
    '''Evaluate string as an expression with the attributes of env.'''
    try:
        ast = _PARSER.parseString(string, parseAll=True)[0]
    except pp.ParseException as err:
        raise RuntimeError('Parse failure: {}.'.format(err)) from None

    ret = ast.evaluate(env)
    assert isinstance(ret, int)
    return ret
