################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A stub class which is easy to import with some buildtools infrastructure'''

import importlib.util
import os
import sys
import types

# We only use the Any type in typing comments, which flake8
# understands but pylint doesn't. Ho hum.
#
# pylint: disable=unused-import
from typing import cast, Any, List, Optional
# pylint: enable=unused-import


def _import_autoproduct_module(name: str) -> types.ModuleType:
    '''Import autoproduct/<name>.py and return the module'''
    av1product_dir = os.path.dirname(__file__)
    ap_dir = os.path.normpath(os.path.join(av1product_dir,
                                           '..', '..', 'autoproduct'))

    old_sys_path = sys.path
    try:
        # We have to hack sys.path so that imports work in the module we're
        # importing work.
        sys.path.append(ap_dir)
        path = os.path.join(ap_dir, name + '.py')

        # The type comments are ugly hacks to work around MyPy not
        # really understanding importlib
        spec = importlib.util.spec_from_file_location(name, path)  # type: Any
        module = importlib.util.module_from_spec(spec)  # type: Any

        spec.loader.exec_module(module)

        return cast(types.ModuleType, module)
    finally:
        sys.path = old_sys_path


_BUILDTOOLS = _import_autoproduct_module('buildtools')  # type: Any


def find_binary(basename: str,
                bin_dir: str,
                other_bindirs: Optional[List[str]],
                force: bool = True) -> Optional[str]:
    '''Search for the given basename in bin_dir then other_bindirs'''
    name = _BUILDTOOLS.find_binary(basename, bin_dir, other_bindirs, force)
    return cast(Optional[str], name)


def rm_f(path: str, recursive: bool = False) -> None:
    '''Like rm -f: delete a file if it exists.

    If path is None, it is ignored (a bit like free() in C)

    '''
    _BUILDTOOLS.rm_f(path, recursive)


def store_check(tag: str, action: str) -> None:
    '''Teach buildtools to ignore some decoder errors'''
    _BUILDTOOLS.store_check(tag, action)


def build_encoder(bin_dir: str, build_dir: str, profile: str,
                  log_name: str, max_threads: int) -> Optional[str]:
    '''Build an encoder'''
    ret = _BUILDTOOLS.buildEncoder(bin_dir, build_dir, None,
                                   profile, 'av1b',
                                   log_name=log_name,
                                   max_threads=max_threads)
    return cast(Optional[str], ret)


def innocuous_checks(stdout_path: str) -> List[str]:
    '''Run this with the stdout from a run.

    The result is true if the failure is innocuous in some way
    (probably, we failed some decode model check and should ignore
    this stream).

    '''
    checks = _BUILDTOOLS.innocuous_checks(stdout_path)
    return cast(List[str], checks)
