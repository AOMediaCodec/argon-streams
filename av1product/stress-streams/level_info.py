################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A class representing stream level information'''

from contextlib import contextmanager
import os
import tempfile
from typing import IO, Iterator, Iterable, Optional, NamedTuple, Tuple

_OptStrStr = Optional[Tuple[str, str]]

LevelMax = NamedTuple('LevelMax',
                      [('size', Tuple[int, int]),
                       ('level_idx', int),
                       ('frame_rate', int),
                       ('high_mbps', int),
                       ('len_divide', int)])


class LevelInfo:
    '''A class that represents maximum sizes etc for a level'''
    def __init__(self, lmax: LevelMax, profile: int):
        assert 0 <= profile <= 2
        self.profile = profile
        self.level_max = lmax
        self.num_frames = ((lmax.frame_rate + lmax.len_divide - 1) //
                           lmax.len_divide)

    def write_bsg_config(self,
                         handle: IO[str],
                         base_config: Optional[Tuple[str, str]]) -> str:
        '''Write a bitstream configuration file for this level's info

        If base_config is not None, base_config[0] will be imported and then
        base_config[1] (or 'default') will be inserted. The configuration in
        the generated file will be called stresser_config (which hopefully
        won't collide with names in the actual configuration file).

        '''
        if base_config is not None:
            # Use abspath here because base_config[0] is specified relative
            # to the working directory, but path might be somewhere else.
            handle.write('import "{}";\n\n'
                         .format(os.path.abspath(base_config[0])))

        config_name = 'stresser_config'
        handle.write('{} {{\n'.format(config_name))

        handle.write('  enc_profile_min_width = {};\n'
                     '  enc_profile_max_width = {};\n'
                     '  enc_profile_min_height = {};\n'
                     '  enc_profile_max_height = {};\n'
                     '  enc_min_level = {};\n'
                     '  enc_max_level = {};\n'
                     '  enc_frame_cnt = 500;\n'
                     '  enc_sched_show_frame = 1;\n'
                     '  enc_num_shown_frames_required = {};\n'
                     '  seq_tier = 1;\n'
                     .format(self.level_max.size[0], self.level_max.size[0],
                             self.level_max.size[1], self.level_max.size[1],
                             self.level_max.level_idx,
                             self.level_max.level_idx,
                             self.num_frames))

        # Stuff that's common to all stress streams. We turn off
        # scalability, mainly because it makes calculating the number of
        # frames rather difficult. We also turn off show-existing frames:
        # these are very easy for decoders to handle. Obviously, we don't
        # want single-frame pictures and we disallow scaling (so we're
        # encoding all the frames at maximum size).
        handle.write('  enc_is_stress_stream = 1;\n'
                     '  enc_use_scalability = 0;\n'
                     '  enc_try_show_existing = 0;\n'
                     '  enc_still_picture = 0;\n'
                     '  enc_allow_scaling = 0;\n'
                     '  disable_cdf_update = 0;\n'
                     '  enc_inter_mode_newmv = 0;\n'
                     '  enc_has_palette_y = 0;\n'
                     '  enc_has_palette_uv = 0;\n'
                     '  enc_use_frame_planner = 0;\n'
                     '  enc_max_recode_count = 1;\n'
                     '  enc_hide_frame_prob = 0;\n')

        # Disable superres by default. The superres streams enable it
        # manually, overriding this setting, but it only make stuff
        # harder for other streams (in the same way as scaling does).
        handle.write('  enable_superres = 0;\n')

        if base_config is not None:
            handle.write('\n  insert {};\n'
                         .format(base_config[1]
                                 if base_config[1] is not None
                                 else 'default'))

        handle.write('}\n')

        return config_name

    def max_stream_size(self) -> float:
        '''Return maximum stream size for this number of frames in bytes

        This assumes that we're decoding at the frame_rate given in LevelMax.
        We don't include any slop for headers. This absolutely ensures that we
        can't produce a stream that blow the bitrate numbers if you repeated
        it.

        '''
        bitrate_profile_factor = (1 + self.profile)
        bitrate = (self.level_max.high_mbps * 10**6) * bitrate_profile_factor
        period = 1.0 / self.level_max.len_divide
        return bitrate * period / 8

    @contextmanager
    def config_wrapper(self,
                       config_args: _OptStrStr) -> Iterator[Tuple[str, str]]:
        '''A context manager to run with a temporary BSG config file

        This yields (tmp_dir, (cfg_file, cfg_name)) where tmp_dir is a
        directory, cfg_file is the path to a file in that directory and
        cfg_name is the name of the configuration in cfg_file to use.

        '''
        with tempfile.NamedTemporaryFile(prefix='stresser-cw-',
                                         suffix='.cfg',
                                         mode='w') as handle:
            cfg = self.write_bsg_config(handle, config_args)
            handle.flush()
            yield (handle.name, cfg)


# pylint: disable=bad-whitespace
_LEVEL_MAXES = {
    '5.0': LevelMax((4096, 2176),  6,  30, 100, 1),
    '5.1': LevelMax((4096, 2176),  7,  60, 160, 1),
    '5.2': LevelMax((4096, 2176),  8, 120, 240, 1),
    '5.3': LevelMax((4096, 2176),  9, 120, 240, 1),
    '6.0': LevelMax((8192, 4352), 10,  30, 240, 1),
    '6.1': LevelMax((8192, 4352), 11,  60, 480, 3),
    '6.2': LevelMax((8192, 4352), 12, 120, 800, 3),
    '6.3': LevelMax((8192, 4352), 13, 120, 800, 3)
}
# pylint: enable=bad-whitespace


def get_level_info(level_name: str, profile: int) -> LevelInfo:
    '''Get a LevelInfo object for the named level and profile'''
    lmax = _LEVEL_MAXES.get(level_name)
    if lmax is None:
        raise RuntimeError('Unknown level: `{}\'.'.format(level_name))
    if profile < 0 or profile > 2:
        raise RuntimeError('Invalid profile: {}.'.format(profile))

    return LevelInfo(lmax, profile)


def get_levels() -> Iterable[str]:
    '''Return a list of allowed level names'''
    return _LEVEL_MAXES.keys()
