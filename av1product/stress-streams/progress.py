#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Report on the progress of the stress streams so far.

This is best used with the directory structure you get from running stress.mk
and/or merge-stress.mk, but all you really need is a load of files called 'db'.

'''

import argparse
import csv
import os
import re
import sys
from typing import List, Optional, NamedTuple

from database import Database
from level_info import get_level_info
from stress_config import StressConfig


CSVRow = NamedTuple('CSVRow',
                    [('level', str),
                     ('profile', int),
                     ('name', str),
                     ('stream_name', Optional[str]),
                     ('best', Optional[float]),
                     ('ub_val', Optional[float]),
                     ('frac', Optional[float])])


def db_rows(xml_path: str, db_path: str) -> List[CSVRow]:
    '''Get CSVRow objects for each entry in the db'''
    database = Database(db_path, None)
    match = re.match(r'([0-2]) ([56]\.[0-3])$', database.header)
    if match is None:
        raise RuntimeError('Failed to parse DB header from `{}\'.'
                           .format(db_path))

    profile = int(match.group(1))
    level = match.group(2)

    config = StressConfig(xml_path, get_level_info(level, profile), None)

    rows = []

    # Each row should be:
    #
    #   level, profile, target_name, best_seen, ub, % of ub
    for name, encoder_profile in config.targets.items():
        ub_val = encoder_profile.settings.ub_val
        best, stream_name, _ = database.best.get(name, [None, None, None])

        frac = None
        if ub_val is not None:
            frac = best / ub_val if best is not None else 0.0

        rows.append(CSVRow(level, profile, name,
                           stream_name, best, ub_val, frac))
    return rows


def main() -> int:
    '''Main entry point'''
    parser = argparse.ArgumentParser()
    parser.add_argument('xml')
    parser.add_argument('root')

    args = parser.parse_args()

    dbs = []
    for root, _, files in os.walk(args.root):
        if 'db' in files:
            dbs.append(os.path.join(root, 'db'))
    sys.stderr.write('Visiting {} databases.\n'.format(len(dbs)))

    rows = []  # type: List[CSVRow]

    for db_path in dbs:
        rows += db_rows(args.xml, db_path)

    # Sort rows in increasing order of frac, moving Nones to the bottom
    def row_key(row: CSVRow) -> float:
        return row.frac if row.frac is not None else 100.0

    rows.sort(key=row_key)

    writer = csv.writer(sys.stdout, delimiter=',',
                        quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(['Level', 'Profile', 'Target name',
                     'Best stream', 'Best value', 'Upper bound', 'Frac seen'])
    for row in rows:
        writer.writerow(row)

    return 0


if __name__ == '__main__':
    try:
        sys.exit(main())
    except RuntimeError as err:
        sys.stderr.write('Error: {}\n'.format(err))
        sys.exit(1)
