################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# A utility makefile to help you co-ordinate multiple machines that
# are running streams. We assume that each machine is working in
# scratch with filenames like:
#
#   /scratch/stress/5.0-0/db
#   /scratch/stress/5.0-0/streams/test123.obu
#
# Set UPDATE=1 to force the rsync and set HOSTS if you're not using
# streams-0 and streams-1.

HOSTS := streams-0 streams-1

MAKEFLAGS += --no-builtin-rules

stress-dir := $(dir $(lastword $(MAKEFILE_LIST)))
merger := $(stress-dir)merge_db.py

copy-tgts := $(addprefix copy-,$(HOSTS))

min-levels := 0 1 2 3
maj-levels := 5 6
profiles   := 0 1 2

levels := $(foreach a,$(maj-levels),$(foreach b,$(min-levels),$(a).$(b)))
combos := $(foreach l,$(levels),$(foreach p,$(profiles),$(l)-$(p)))

combo-dbs := $(foreach c,$(combos),merged/$(c)/db)

all: $(combo-dbs)

UPDATE := 0
ifeq ($(UPDATE),1)
.PHONY: $(HOSTS)
endif
$(HOSTS):
	rsync -t -r -f '- log' -f '- *.log' \
	      --delete $@:/scratch/stress/ $@

host0 := $(firstword $(HOSTS))
other_hosts := $(filter-out $(host0),$(HOSTS))

.SECONDEXPANSION:

hdbs := $(foreach c,$(combos),$(foreach h,$(HOSTS),$(h)/$(c)/db))
$(hdbs): %: $$(firstword $$(subst /, ,$$@))

pc := %

$(combo-dbs): merged/%/db: $$(filter $$(pc)/$$*/db,$(hdbs))
	if [ -e $@ ]; then \
	  $(merger) $@ $(@D)/streams \
	            $(foreach h,$(HOSTS),$(h)/$*/db $(h)/$*/streams); \
	else \
	  rm -rf $(@D); \
	  mkdir -p merged; \
          for h in $(HOSTS); do \
            if [ -e $@ ]; then \
	      $(merger) $@ $(@D)/streams $$h/$*/db $$h/$*/streams; \
	    elif [ -e $$h/$*/db ]; then \
	      cp -r $$h/$*/ merged; \
	    fi; \
	  done; \
	fi
