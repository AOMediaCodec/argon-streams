################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Code for a class representing the output from a run'''

import os
import re

# We only use the Optional type in typing comments, which flake8 understands
# but pylint doesn't. Ho hum.
#
# pylint: disable=unused-import
from typing import Any, Dict, IO, Optional
# pylint: enable=unused-import

Stats = Dict[str, int]


class RunData:
    '''A class that gets gradually filled in with output from a run'''
    def __init__(self) -> None:
        self.stream = None  # type: Optional[str]
        self.size = None  # type: Optional[int]
        self.stats = None  # type: Optional[Stats]

    def set_stream(self, stream: str) -> None:
        '''Set the stored stream to an existing file. Only call this once.'''
        assert self.stream is None
        assert os.path.exists(stream)
        self.stream = stream
        self.size = os.stat(stream).st_size

    def set_stats(self, stats: Stats) -> None:
        '''Set the stored stats after we have a stream. Only call this once.'''
        assert self.stream is not None
        assert self.stats is None
        self.stats = stats


def read_log_file(log_file: IO[Any]) -> Stats:
    '''Read stats from a log file and return the resulting dictionary.

    The lines that we match are of the form A = B where A is a symbol name with
    possible array index. If A is "foo[1]", the returned dictionary will be
    keyed with "foo[1]" (we don't do anything clever to understand the
    structure implied by the name).

    '''
    ret = {}  # type: Stats
    matcher = re.compile(r'\s*([a-z0-9_\[\]]+)\s+=\s+(.+?)\s*$')
    for line in log_file:
        match = matcher.match(line)
        if not match:
            continue

        lhs = match.group(1)
        rhs = match.group(2)

        # Deal with RHS of the form "{1, 2, 3}"
        if rhs.startswith('{'):
            if not rhs.endswith('}'):
                continue
            entries = []
            try:
                for entry in rhs[1:-1].split(','):
                    entries.append(float(entry))
            except ValueError:
                continue

            # Ignore these entries. We don't every actually use them
            # and they just make the types more complicated.
            continue

        # Ignore percentages
        if rhs.endswith('%'):
            continue

        # Anything else should be a number
        try:
            fval = float(rhs)
        except ValueError:
            continue

        # But we just care about the integers. Doing it like this
        # means that we can still happily read 123.0 as 123.
        if not (fval - int(fval)):
            ret[lhs] = int(fval)

    return ret
