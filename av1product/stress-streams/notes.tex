%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Project P8005 HEVC
% (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
% All rights reserved.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{article}
\usepackage[a4paper]{geometry}
\usepackage{parskip}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage[group-separator={,}]{siunitx}

\newcommand{\var}[1]{\texttt{#1}}

\newcommand{\thead}[2][5em]{%
  \vbox{\hsize#1\baselineskip11pt\centering\vspace*{3pt}#2\par}}

\usepackage{tabularx}
\setlength{\extrarowheight}{0.5ex}

\usepackage{DejaVuSerif}
\usepackage[T1]{fontenc}
\pagenumbering{gobble}

\begin{document}

\section{Stress streams}

Argon Streams AV1, version 2.1 comes with stress streams. These are
streams that are crafted to stress particular aspects of a decoder. As
you'd expect, these streams are constrained by maximum bit and sample
rates which are specified per level in the AV1 specification (see
appendix A.3). As such, we have generated stress streams for each
level between 5.0 and 6.3, inclusive, and for each profile (making 24
combinations).

These streams are intended to be realistic tests for a decoder. As
such, they are designed to play at 30, 60 or 120 frames per second,
depending on the level. This is much less than the theoretical maximum
of 300 frames per second implied by \var{MaxHeaderRate}. The streams
signal $\var{seq\_tier} = 1$ and most attain roughly the maximum
bitrate allowed by \var{HighMbps} in the specification. The only
streams that don't attain maximum bitrate are the CDEF streams (see
section \ref{sec:max-cdef}), the superres streams (see section
\ref{sec:max-superres-ratio}), the Wiener filter streams (see section
\ref{sec:max-wiener}) and the self-guided restoration streams (see
section \ref{sec:max-sgr}).

\begin{center}
  \begin{tabular}{c|cc}
    \thead[3em]{Level}
    & \thead[6em]{\var{HighMbps} (MBits/sec)}
    & \thead[4em]{Frame rate}\\
    \hline
    5.0 & $100.0$ &  $30$\\
    5.1 & $160.0$ &  $60$\\
    5.2 & $240.0$ & $120$\\
    5.3 & $240.0$ & $120$\\
    6.0 & $240.0$ &  $30$\\
    6.1 & $480.0$ &  $60$\\
    6.2 & $800.0$ & $120$\\
    6.3 & $800.0$ & $120$
  \end{tabular}
\end{center}

The frame sizes are chosen to attain \var{MaxPicSize}. For levels 5.0
through 5.3, this is $8912896$ samples, which we attain as $4096$ by
$2176$ samples. For levels 6.0 through 6.3, this is $35651584$
samples, which we attain as $8192$ by $4352$ samples. This choice
means that the streams will also attain
\var{TotalDisplayLumaSampleRate}.

The release contains streams for eight levels across three profiles.
For each combination of level and profile, there are $11$ different
types of stress stream, listed below. Most of these streams will be
$1/3$ of a second long (with varying numbers of frames, depending on
the frame rate for that level). The only exception is the streams that
maximise bits in a frame (see section \ref{sec:max-frame-bits}), which
last one second.

\subsection{Maximum frame bits}
\label{sec:max-frame-bits}

This stress stream tries to encode a frame with as many bits as
possible. The idea is that the first frame is a large keyframe and the
following frames are all inter-frames, chosen to be as small as they
can be to give the first frame as much space as possible without being
too large for \var{HighMbps}.

In practice, the size of this initial frame will be dictated by
\var{HighCR}, the minimum compression ratio allowed. This is $4$ for
each of the levels targeted. Since the stream maximises
\var{TotalDecodedLumaSampleRate}, we have $\var{SpeedAdj} = 1$ so long
as $\var{seq\_tier} = 1$, so $\var{MinPicCompressRatio} = 4$. The
fixed frame picture size means that \var{UnCompressedSize} can be
calculated. This depends on profile and level. Assuming that the large
frame attains the minimum compression ratio, the maximum frame size in
bits can be computed by dividing \var{UncompressedSize} by the minimum
compression ratio. Multiplying by $8$ to convert bytes to bits gives
the following maximum sizes in bits:

\begin{center}
  \begin{tabular}{l|rr}
    & \thead{Level 5.x} & \thead{Level 6.x}\\
    \hline
    Profile 0 & \num{33423360} & \num{133693440}\\
    Profile 1 & \num{66846720} & \num{267386880}\\
    Profile 2 & \num{80216064} & \num{320864256}
  \end{tabular}
\end{center}

Since the stress streams are intended to be decoded (and displayed) at
the frame rates in the earlier table, the size of the initial frame
implies a minimum stream length (in order not to break the
\var{HighMbps} constraint). Assuming the size of the other frames is
negligible, the time period must be at least
$S / (10^6 \times \var{HighMbps})$, where $S$ is the initial frame
size in bits. In fact, this calculation results in a period greater
than 1 second for Level 6.0, profiles 1 ($1.11$ seconds) and 2 ($1.34$
seconds). Since the bitrate checks are done every second, these two
cases can't actually attain the frame sizes listed above. Many of the
other cases are a large fraction of a second, so we generate a $1$
second stream in each case.

\subsection{Maximum number of nonzero coefficients}

This type of stream is designed to stress coefficient decode and
transform handling by signalling as many nonzero transform
coefficients per second as possible.

Looking at the \var{coeffs()} function in the specification, note that
any nonzero coefficient must specify its sign, encoded as the syntax
element \var{sign\_bit}. This is encoded as a raw bit, so each
coefficient takes at least one bit to encode. This gives a theoretical
maximum rate of nonzero coefficients at profile 0:

\begin{center}
  \begin{tabular}{c|cc}
    \thead[6em]{Levels}
    & \thead[6em]{\var{HighMbps} (MBits/sec)}
    & \thead[8em]{NZ coeffs in 1/3 sec (Million)}\\
    \hline
    5.0     & $100.0$ & $33.3$ \\
    5.1     & $160.0$ & $53.3$ \\
    5.2-6.0 & $240.0$ & $80.0$ \\
    6.1     & $480.0$ & $160.0$ \\
    6.2-6.3 & $800.0$ & $266.7$
  \end{tabular}
\end{center}

In practice, of course, the stream doesn't just consist of sign bits,
but it's possible to produce streams with around $75\%$ of the total
stream size consisting of these bits.

For profiles 1 and 2, the corresponding limit should be multiplied by
2 or 3, respectively.

\subsection{Maximum number of blocks}

This type of stream uses minimum block size (signalling each
\var{partition} syntax element as \var{PARTITION\_SPLIT}) in order to
generate as many blocks per second as possible. In order to ensure
that the bit budget is not spent on transform coefficients, these
streams signal $\var{skip\_mode} = 1$.

If a stream manages to split all blocks down to \var{BLOCK\_4X4}, the
maximum number of blocks depends only on frame size and number of
frames:

\begin{center}
  \begin{tabular}{c|ccc}
    \thead[6em]{Levels}
    & \thead[6em]{Image size}
    & \thead[5em]{Frames in 1/3 sec}
    & \thead[8em]{Max blocks}\\
    \hline
    5.0        & $4096 \times 2176$ & $10$ & \num{5570560} \\
    5.1        & $4096 \times 2176$ & $20$ & \num{11141120} \\
    5.2 -- 5.3 & $4096 \times 2176$ & $40$ & \num{22282240} \\
    6.0        & $8192 \times 4352$ & $10$ & \num{22282240} \\
    6.1        & $8192 \times 4352$ & $20$ & \num{44564480} \\
    6.2 -- 6.3 & $8192 \times 4352$ & $40$ & \num{89128960}
  \end{tabular}
\end{center}

\subsection{Maximum number of transforms}

This type of stream tries to generate as many transforms per second as
possible. This is done by either signalling small blocks, as in the
last stream type, or signalling blocks up to \var{BLOCK\_8X8} with the
\var{txfm\_split} syntax element set (or \var{tx\_depth}, for inter
blocks). In either case, all transforms will have size \var{TX\_4X4}.

\subsection{Maximum number of symbols}

These streams try to stress the entropy decoder by generating as many
symbols per second as possible. This is done by biasing the encoder so
that whenever it samples from a CDF, it picks the symbol with the
greatest probability. This maximises the number of symbols per bit
and, since the bitrate is fixed at \var{HighMbps}, thus maximises the
number of symbols per second.

\subsection{Maximum number of inter-predictions}

A stream that maximises the number of inter-predictions should stress
motion vector prediction and decoding and also the reference buffers.
In order to fit as many inter-predictions as possible, these streams
use small blocks and try to avoid using \var{NEWMV} (because the
motion vector coefficients take bits to encode).

To maximise the inter-prediction count we use 4x4 blocks and
single-sided predictions. Then each 4x4 block of pixels gives 1 luma
and 2 chroma single-sided predictions (irrespective of chroma
subsampling). In order to use compound predictions, we would need to
use at least 8x8 blocks. But then each 8x8 would give 1 luma and 2
chroma compound predictions, for 6 single-sided predictions in total.
This is half as many as you get from dividing into 4x4 blocks.

The theoretical maximum inter prediction count depends on the maximum
picture area and number of inter-frames (we generate 1/3 of a second's
worth and all but one of the frames is inter).

\begin{center}
  \begin{tabular}{c|ccc}
    \thead[6em]{Levels}
    & \thead[6em]{Image size}
    & \thead[5em]{Frames in 1/3 sec}
    & \thead[8em]{Max inter predictions}\\
    \hline
    5.0        & $4096 \times 2176$ & $10$ & \num{15040512} \\
    5.1        & $4096 \times 2176$ & $20$ & \num{31752192} \\
    5.2 -- 5.3 & $4096 \times 2176$ & $40$ & \num{65175552} \\
    6.0        & $8192 \times 4352$ & $10$ & \num{60162048} \\
    6.1        & $8192 \times 4352$ & $20$ & \num{127008768} \\
    6.2 -- 6.3 & $8192 \times 4352$ & $40$ & \num{260702208}
  \end{tabular}
\end{center}

\subsection{Maximum number of warp predictions}

These streams maximise the number of warp predictions per second. A
warped motion prediction is only allowed if the block (subsampled for
the current plane) is at least 8x8 samples. Since there is a warped
motion prediction per 8x8 sample block, tiling across the single block
in the partition structure, there is no benefit to partitioning
smaller than 16x16 pixel blocks.

The theoretical maximum number of warp predictions per inter frame
depends on the frame size and the subsampling mode (4:2:0 is forced
for profile 0; we pick 4:4:4 for profiles 1 and 2).

\begin{center}
  \begin{tabular}{cc|cccc}
    \thead[4em]{Levels}
    & \thead[4em]{Profiles}
    & \thead[6em]{Image size}
    & \thead[4em]{Luma 8x8s}
    & \thead[4em]{Chroma 8x8s}
    & \thead[8em]{Warp preds / frame}\\
    \hline
    5.x & 0    & $4096 \times 2176$
	& \num{139264} & \num{69632}   & \num{208896} \\
    5.x & 1, 2 & $4096 \times 2176$
	& \num{139264} & \num{278528}  & \num{417792} \\
    6.x & 0    & $8192 \times 4352$
	& \num{557056} & \num{278528}  & \num{835584} \\
    6.x & 1, 2 & $8192 \times 4352$
	& \num{557056} & \num{1114112} & \num{1671168}
  \end{tabular}
\end{center}

Expanding this table with the number of inter frames for 1/3 sec gives:

\begin{center}
  \begin{tabular}{cc|ccc}
    \thead[4em]{Levels}
    & \thead[4em]{Profiles}
    & \thead[6em]{Warp preds / frame}
    & \thead[4em]{Inter frames}
    & \thead[8em]{Total warp preds}\\
    \hline
    5.0        & 0    & \num{208896}  &  9 & \num{1880064}\\
    5.0        & 1, 2 & \num{417792}  &  9 & \num{3760128}\\
    5.1        & 0    & \num{208896}  & 19 & \num{3969024}\\
    5.1        & 1, 2 & \num{417792}  & 19 & \num{7938048}\\
    5.2 -- 5.3 & 0    & \num{208896}  & 39 & \num{8146944}\\
    5.2 -- 5.3 & 1, 2 & \num{417792}  & 39 & \num{16293888}\\
    6.0        & 0    & \num{835584}  &  9 & \num{7520256}\\
    6.0        & 1, 2 & \num{1671168} &  9 & \num{15040512}\\
    6.1        & 0    & \num{835584}  & 19 & \num{15876096}\\
    6.1        & 1, 2 & \num{1671168} & 19 & \num{31752192}\\
    6.2 -- 6.3 & 0    & \num{835584}  & 39 & \num{32587776}\\
    6.2 -- 6.3 & 1, 2 & \num{1671168} & 39 & \num{65175552}\\
  \end{tabular}
\end{center}

\subsection{Maximum number of CDEFs}
\label{sec:max-cdef}

To stress the CDEF engine in a decoder, these streams enable CDEF
filtering on every 8x8 block in the stream. Since the streams attain
\var{TotalDisplayLumaSampleRate}, this is the maximum number of CDEF
filtering operations per second allowed by the level.

The maximum number of CDEF filters that can be run is the number of
8x8 blocks in the stream multiplied by the number of planes. This
depends on level and profile as follows:

\begin{center}
  \begin{tabular}{cc|ccc}
    \thead[4em]{Levels}
    & \thead[6em]{Image size}
    & \thead[6em]{8x8 blocks}
    & \thead[5em]{Frames in 1/3 sec}
    & \thead[6em]{Total CDEFs}\\
    \hline
    5.0        & $4096 \times 2176$ & \num{139264}  & 10 & \num{4177920}\\
    5.1        & $4096 \times 2176$ & \num{139264}  & 20 & \num{8355840}\\
    5.2 -- 5.3 & $4096 \times 2176$ & \num{139264}  & 40 & \num{16711680}\\
    6.0        & $8192 \times 4352$ & \num{557056}  & 10 & \num{16711680}\\
    6.1        & $8192 \times 4352$ & \num{557056}  & 20 & \num{33423360}\\
    6.2 -- 6.3 & $8192 \times 4352$ & \num{557056}  & 40 & \num{66846720}\\
  \end{tabular}
\end{center}

Since the CDEF streams and the other streams that follow are aimed at
a particular part of the decoder which will not be affected by
bitrate, they do not try to maximise bitrate.

\subsection{Maximum superres ratio at full size}
\label{sec:max-superres-ratio}

This type of stream tests superres upscaling by setting a
frame size to attain maximal \var{TotalDisplayLumaSampleRate} and also
signalling $\var{SuperresDenom} = 16$, the largest upscaling that
can be signalled.

These streams do not maximise bitrate.

\subsection{Maximum number of units using Wiener restoration}
\label{sec:max-wiener}

These streams maximise the number of loop restoration units with
Wiener filtering seen per second. To achieve this, the frames are of
maximal size, the frames are signalled with \var{lr\_type} equal to
\var{RESTORE\_WIENER}, and each unit signals $\var{use\_wiener} = 1$.

The test streams track how many samples (luma and chroma) were encoded
with Wiener restoration. The maximum possible depends only on the
number of frames, the maximum frame size and the chroma subsampling:

\begin{center}
  \begin{tabular}{cc|cccc}
    \thead[4em]{Levels}
    & \thead[4em]{Profiles}
    & \thead[6em]{Image size}
    & \thead[6em]{Samples per frame}
    & \thead[5em]{Frames in 1/3 sec}
    & \thead[6em]{Wiener samples}\\
    \hline
    5.0        & 0    & $4096 \times 2176$
      & \num{13369344}  & 10 & \num{133693440}\\
    5.0        & 1, 2 & $4096 \times 2176$
      & \num{26738688}  & 10 & \num{267386880}\\
    5.1        & 0    & $4096 \times 2176$
      & \num{13369344}  & 20 & \num{267386880}\\
    5.1        & 1, 2 & $4096 \times 2176$
      & \num{26738688}  & 20 & \num{534773760}\\
    5.2 -- 5.3 & 0    & $4096 \times 2176$
      & \num{13369344}  & 40 & \num{534773760}\\
    5.2 -- 5.3 & 1, 2 & $4096 \times 2176$
      & \num{26738688}  & 40 & \num{1069547520}\\
    6.0        & 0    & $8192 \times 4352$
      & \num{53477376}  & 10 & \num{534773760}\\
    6.0        & 1, 2 & $8192 \times 4352$
      & \num{106954752}  & 10 & \num{1069547520}\\
    6.1        & 0    & $8192 \times 4352$
      & \num{53477376}  & 20 & \num{1069547520}\\
    6.1        & 1, 2 & $8192 \times 4352$
      & \num{106954752}  & 20 & \num{2139095040}\\
    6.2 -- 6.3 & 0    & $8192 \times 4352$
      & \num{53477376}  & 40 & \num{2139095040}\\
    6.2 -- 6.3 & 1, 2 & $8192 \times 4352$
      & \num{106954752}  & 40 & \num{4278190080}\\
  \end{tabular}
\end{center}

These streams do not maximise bitrate.

\subsection{Maximum number of units using self-guided restoration}
\label{sec:max-sgr}

The self-guided restoration streams are like the Wiener restoration
streams above, but set \var{lr\_type} to \var{RESTORE\_SGRPROJ} in
order to maximise the number of loop restoration units with
self-guided restoration seen per second.

The calculation for the maximum number of samples using self-guided
restoration is exactly the same as for Wiener filtering in section
\ref{sec:max-wiener}.

These streams do not maximise bitrate.

\pagebreak

\section{Compression ratios}

The level of the stream defines \var{MinCompBasis}. This has a minimum
value of $2$ (at level 2.0) and goes up to $8$ or $4$, depending on
\var{seq\_tier}.

To convert this into a minimum compression ratio for videos (as
opposed to still pictures), we need a fudge factor called
\var{SpeedAdj}. This is defined as
\begin{equation}
  \label{eq:SpeedAdj}
  \var{SpeedAdj} = \var{TotalDecodedLumaSampleRate} / \var{MaxDisplayRate}.
\end{equation}
Here, \var{TotalDecodedLumaSampleRate} is the number of shown samples
(not in frames with \var{show\_existing\_frame}) in the last temporal
unit, divided by the unit's length in seconds. \var{MaxDisplayRate} is
given by the level.

With \var{SpeedAdj} defined, we can define
\begin{equation}
  \label{eq:MinPicCompressRatio}
  \var{MinPicCompressRatio} =
  \begin{cases}
    0.8 & \mbox{if $\var{still\_picture} = 1$} \\
    \max(0.8, \var{MinCompBasis}\times \var{SpeedAdj}) & \mbox{otherwise.}
  \end{cases}
\end{equation}
This number is used in a bitstream conformance test, making sure that
the compression ratio of a stream isn't too low.

Note that when the frames of the stream are small, \var{SpeedAdj} will
be very small (because \var{TotalDecodedLumaSampleRate} will be tiny
compared with \var{MaxDisplayRate}). As a result, we can have as
rubbish a compression ratio as we like.

The conformance test above is the requirement that
\begin{equation}
  \label{eq:test}
  \var{CompressedRatio} \ge \var{MinPicCompressRatio},
\end{equation}
where \var{CompressedRatio} is defined as the fraction
\begin{equation}
  \label{eq:CompressedRatio}
  \var{CompressedRatio} = \var{UnCompressedSize} / \var{CompressedSize}.
\end{equation}

These sizes are computed across each temporal unit (at the current
operating point). The definition of \var{CompressedSize} is sort of
what you'd expect: the number of bytes in OBUs related to the frame
(with types \var{OBU\_FRAME}, \var{OBU\_FRAME\_HEADER},
\var{OBU\_METADATA} and \var{OBU\_TILE\_GROUP}). The only slightly odd
thing is that we subtract 128 from the raw count, to allow a little
bit of overhead. Presumably this is to avoid problems with really
small frames where the headers are a large fraction of the total.

The denominator, \var{UnCompressedSize} is defined as
\begin{equation}
  \label{eq:UnCompressedSize}
  \var{UnCompressedSize} =
  (\var{UpscaledWidth} \times \var{FrameHeight}
  \times \var{PicSizeProfileFactor}) / 8
\end{equation}
This is intended as the number of bytes in the frame when it's
unpacked. The fudge factor is defined as
\begin{equation}
  \label{eq:PicSizeProfileFactor}
  \var{PicSizeProfileFactor} =
  \begin{cases}
    15 & \mbox{if $\var{seq\_profile} = 0$}\\
    30 & \mbox{if $\var{seq\_profile} = 1$}\\
    36 & \mbox{if $\var{seq\_profile} = 2$}
  \end{cases}
\end{equation}
Note that in profile 0, we subsample chroma in both directions, so
this is the right number of bits per pixel for 10-bit samples in
profiles 0 and 1. Profile 2 allows 12-bit samples.

\section{Maximising \var{bits\_per\_frame}}

The \var{bits\_per\_frame} stress target is the average number of bits
per frame in the stream. What's the best we might hope to do?
Reproducing part of the levels table from the spec:

\begin{center}
  \begin{tabular}{lrrr}
    Level & \var{MaxPicSize} & \var{MaxDisplayRate} & \var{MaxDecodeRate} \\
    \hline
    5.0 &  8,912,896 &   267,386,880 &   273,715,200\\
    5.1 &  8,912,896 &   534,773,760 &   547,430,400\\
    5.2 &  8,912,896 & 1,069,547,520 & 1,094,860,800\\
    5.3 &  8,912,896 & 1,069,547,520 & 1,176,502,272\\
    6.0 & 35,651,584 & 1,069,547,520 & 1,176,502,272\\
    6.1 & 35,651,584 & 2,139,095,040 & 2,189,721,600\\
    6.2 & 35,651,584 & 4,278,190,080 & 4,379,443,200\\
    6.3 & 35,651,584 & 4,278,190,080 & 4,706,009,088
  \end{tabular}
\end{center}

Dividing the \var{MaxDisplayRate} column by \var{MaxPicSize} gives a
maximum frame rate (30, 60, 120, 120, repeated) so, assuming each
frame has the same dimensions, the \var{MaxDisplayRate} constraint is
just an FPS constraint.

Given this, what is the theoretical maximum value of
\var{bits\_per\_frame}? Since we'll want to send \var{MaxDisplayRate}
samples per second, maximimising the number of bits means maximimising
\var{CompressedSize} for a fixed \var{UnCompressedSize}, so maximising
\var{CompressedRatio}. But we know that \var{HighCR} is 4 for each of
these levels. Thus
\begin{eqnarray}
  \var{bits\_per\_frame}
  &=& 8 \times \var{CompressedSize} \\
  &\le& 8 \times (\var{UnCompressedSize} / 4)\\
  & = & (\var{MaxPicSize} \times \var{PicSizeProfileFactor})/4
\end{eqnarray}


\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
