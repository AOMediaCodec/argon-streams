/*******************************************************************************
Project P8005 HEVC
(c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
All rights reserved.
*******************************************************************************/

base {
    // No need to spend bits on segmentation. Also, it seems that
    // these streams can trigger some weird bug in av1_frame_plan's
    // enc_plan_block which needs segmentation enabled.
    segmentation_enabled = 0;

    // Choose a nice big quantizer to avoid accidentally coding a
    // lossless frame.
    base_q_idx = 10;
}

small_blocks {
    insert base;

    // Generate a stream with as many blocks as possible

    per_stream_0 = Max (0, enc_backoff - 10);

    // Always skip to minimise the number of bits spent on
    // coefficients.
    enc_skip_prob = choose (weighted (100 weight 10,
                                      uniform (80 .. 100) weight 1));

    // Always split (with backoff) and choose PARTITION_SPLIT, rather
    // than some other splitting like PARTITION_HORZ, which would
    // cause us to stop splitting earlier than needs be. So we get
    // something interesting after a big backoff, we start allowing
    // non-SPLIT partitions then.
    enc_specify_partitions = 1;
    partition =
      choose (weighted (PARTITION_SPLIT weight 100,
                        (uniform (PARTITION_NONE ..
                                  (num4x4 <= 2 ? PARTITION_SPLIT :
                                   num4x4 < 32 ? PARTITION_VERT_4 :
                                   PARTITION_VERT_B)) weight per_stream_0)));

    enc_split_prob = 100 - per_stream_0;

    // Avoid spending lots of bits on y_mode and uv_mode by always
    // signalling DC_PRED.
    y_mode = DC_PRED;
    intra_frame_y_mode = DC_PRED;
    uv_mode = DC_PRED;

    // Don't use compound prediction.
    enc_is_compound = 0;

    // Pick inter modes in decreasing order of preference, which
    // should help train the inter mode CDF.
    enc_inter_mode = (can_use_nearest ?
                      NEARESTMV :
                      (can_use_near ?
                       NEARMV :
                       (can_use_global ?
                        GLOBALMV :
                        NEWMV)));

    // Send zeroes as coefficients. It turns out that some streams can
    // signal all 4x4s without hitting the bitrate limit. In order to
    // allow us to get to maximum bitrate, we very occasionally allow
    // a nonzero coefficient.
    enc_num_non_zero_coeffs =
        (enc_backoff < 10) * choose (uniform (0 .. min (segEob, 3)));

    // It turns out that it might actually be cheaper to send an intra
    // frame in this case (because encoding MVs is really expensive).
    // Maybe we should always do so.
    per_stream_1 = choose_bit ();
    enc_frame_is_inter = per_stream_0 & choose_bit ();
}

many_transforms {
    insert base;

    // For profile 0, we'll always split down to 8X8 blocks and then
    // split down to 4X4 half of the time (we'll use txfm_split to go
    // down to 4X4 transforms otherwise).
    //
    // For profiles 1 and 2, we always split all the way down to 4x4
    // blocks, otherwise the chroma transforms will end up being 8x8
    // (see get_tx_size in the spec).

    per_stream_0 = Max (0, enc_backoff - 10);
    enc_specify_partitions = 1;
    partition =
        ((bSize == BLOCK_8X8 && seq_profile == 0) ?
         choose (uniform (PARTITION_NONE, PARTITION_SPLIT)) :
         choose (weighted (PARTITION_SPLIT weight (100 - per_stream_0),
                           (uniform (PARTITION_NONE ..
                                     (num4x4 <= 2 ? PARTITION_SPLIT :
                                      num4x4 < 32 ? PARTITION_VERT_4 :
                                      PARTITION_VERT_B)) weight per_stream_0))));

    enc_use_frame_planner = 0;

    // Don't ever skip (otherwise that block won't have a transform)
    skip = 0;

    // Don't ever show an existing frame (since that won't get us any
    // transforms!)
    enc_try_show_existing = 0;

    // Don't ever use superres (this would mean a smaller encoded size
    // and thus fewer transforms).
    enable_superres = 0;

    // Send exactly 1 non-zero coefficient.
    enc_num_non_zero_coeffs = 1;

    // And make sure it's not too big. In fact, make sure it's always
    // +1. This means we can train the dc_sign CDF.
    enc_specify_coeffs = 1;
    enc_quantized_coeff = (c == 0) ? 1 : (1 - 2 * choose_bit ());
    enc_tx_coeffs_per_million = 0;

    // Enable var_tx
    tx_mode_select = 1;

    // If we're an 8x8 (which must mean we're in profile 0), we want
    // to split the transform. This might be with var_tx (in which
    // case we use txfm_split) or it might be using split depth, which
    // we control with enc_target_tx_size.
    txfm_split = 1;
    enc_target_tx_size = TX_4X4;

    // Avoid spending lots of bits on y_mode and uv_mode by always
    // signalling DC_PRED.
    y_mode = DC_PRED;
    intra_frame_y_mode = DC_PRED;
    uv_mode = DC_PRED;

    // Don't use compound prediction.
    enc_is_compound = 0;

    // Pick inter modes in decreasing order of preference, which
    // should help train the inter mode CDF.
    enc_inter_mode = (can_use_nearest ?
                      NEARESTMV :
                      (can_use_near ?
                       NEARMV :
                       (can_use_global ?
                        GLOBALMV :
                        NEWMV)));

    // Don't spend bits on filter_intra.
    enable_filter_intra = 0;

    // Always signal the first option from the list of allowed
    // transform types. Hopefully this will train the intra_tx_type
    // and inter_tx_type CDFs.
    enc_tx_type = AllowedTxTypes[0];

    // Only send zero motion vectors. This should train the MV
    // component CDFs when we're forced to send NEWMV.
    enc_mv_max_row = 0;
    enc_mv_max_col = 0;

    // Switch off intrabc: it seems to be reasonably large
    allow_intrabc = 0;

    // It seems that it's sometimes much better to just encode intra
    // frames for large streams. Rather than force it, let's have a
    // 50% chance of always picking intra frames and a 50% chance of
    // picking inter frames some of the time.
    per_stream_1 = choose_bit () ? 0 : choose (uniform (0 .. 100));
    enc_frame_is_inter = choose_bernoulli (per_stream_1);
}
