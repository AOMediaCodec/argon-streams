################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

# This Makefile is intended for use on a big worker machine that runs
# stresser.py.
#
# Don't pass -j to it - we're already going to use the entire machine.

# Disable Make's builtin implicit rules
MAKEFLAGS += --no-builtin-rules

ncpus := $(shell cat /proc/cpuinfo | grep processor | wc -l)

all-min-levels := 0 1 2 3
all-maj-levels := 5 6
all-profiles   := 0 1 2

# Which ones do we actually want?
MIN_FILTER := .
MAJ_FILTER := .
PROF_FILTER := .
LEVEL_FILTER := .
COMBO_FILTER := .
TGT_FILTER := .

# The directory containing this Makefile and all the other stress
# stuff.
stress-dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# Timeout in seconds (defaults to "forever")
TIMEOUT :=

run-filt = $(shell echo $(1) | tr ' ' '\n' | grep -E '$(2)' | tr '\n' ' ')

min-levels := $(call run-filt,$(all-min-levels),$(MIN_FILTER))
maj-levels := $(call run-filt,$(all-maj-levels),$(MAJ_FILTER))
profiles   := $(call run-filt,$(all-profiles),$(PROF_FILTER))

# 5.0 through 6.3
all-levels := $(foreach a,$(maj-levels),$(foreach b,$(min-levels),$(a).$(b)))
levels := $(call run-filt,$(all-levels),$(LEVEL_FILTER))

# All combos of the form 5.0-0 through 6.3-2
all-combos := $(foreach l,$(levels),$(foreach p,$(profiles),$(l)-$(p)))
combos := $(call run-filt,$(all-combos),$(COMBO_FILTER))

combo-level = $(firstword $(subst -, ,$(1)))
combo-profile = $(lastword $(subst -, ,$(1)))

# For each combo, C, we will produce $(C)/run.log
logs := $(foreach c,$(combos),$(c)/run.log)

# Mark each log target as phony so that we don't skip ones that have
# run before.
.PHONY: $(logs)

# We expect the user to have built the decoders already and put them
# in bin/ in this directory.
binaries := $(addprefix bin/,av1dec av1bdec)

all: $(logs)

$(logs): %/run.log: $(binaries)
	mkdir -p $*
	(cd $*; \
	 python3 $(stress-dir)stresser.py \
	    --level $(call combo-level,$*) --profile $(call combo-profile,$*) \
	    -j$(ncpus) $(if $(TIMEOUT),--timeout $(TIMEOUT),) --bindir ../bin \
	    --filter $(TGT_FILTER) --backoff 50 \
	    $(stress-dir)stress-config.xml db | \
	  tee -i run.log)
