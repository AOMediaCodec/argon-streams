#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Code to merge two or more stress databases.'''

import argparse
import sys

from database import Database


def main() -> int:
    '''Main entry point'''
    parser = argparse.ArgumentParser()
    parser.add_argument('db', help='DB file to update')
    parser.add_argument('streams', help='Streams dir to update')
    parser.add_argument('pairs', nargs='*',
                        help=('DB, streams dir pairs to use as sources.'))

    args = parser.parse_args()
    if len(args.pairs) % 2:
        raise RuntimeError('Must have even number of pair arguments.')

    database = Database(args.db, args.streams)
    pairs = args.pairs
    while pairs:
        assert len(pairs) >= 2
        src_path, src_streams = pairs[:2]
        database.merge(Database(src_path, src_streams, database.header))
        pairs = pairs[2:]

    return 0


if __name__ == '__main__':
    try:
        sys.exit(main())
    except RuntimeError as err:
        sys.stderr.write('Error: {}\n'.format(err))
        sys.exit(1)
