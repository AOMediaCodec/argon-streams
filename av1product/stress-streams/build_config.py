################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A module with code for BuildConfig'''

import os
import shutil
import threading
import time
from typing import Optional

import buildtools


class BuildConfig:
    '''Storage for setup data (build directories and similar)

    Reading fields can run concurrently with methods (but obviously you can't
    write to them in a thread-safe manner).

    '''
    def __init__(self,
                 bin_dir: str,
                 log_dir: str,
                 timeout: Optional[float],
                 scratch_dir: str):
        self.bin_dir = bin_dir
        self.log_dir = log_dir
        self.scratch_dir = scratch_dir

        self.__lock = threading.Lock()
        self.__log_idx = 0

        self.stop_times = None
        if timeout is not None:
            cur_time = time.clock_gettime(time.CLOCK_MONOTONIC)
            self.stop_times = (int(cur_time + timeout),
                               int(cur_time + 1.5 * max(0, timeout)))

    def find_binary(self, basename: str) -> Optional[str]:
        '''Return the path to a binary with given basename if one exists.

        This is thread-safe.'''
        return buildtools.find_binary(basename,
                                      self.bin_dir, [], force=False)

    def binary_path(self, binary: str) -> str:
        '''Get the path to a binary, which must exist'''
        path = os.path.join(self.bin_dir, binary)
        if not os.path.exists(path):
            raise RuntimeError('No such binary: `{}\'.'.format(path))

        return path

    def take_idx(self) -> int:
        '''Read and increment the log index. Thread-safe.'''
        with self.__lock:
            val = self.__log_idx
            self.__log_idx += 1
        return val

    def add_fail_log(self, src_log: str, prefix: str) -> str:
        '''Take a copy of src_log in fail_dir. Return the new name.'''
        os.makedirs(self.log_dir, exist_ok=True)

        idx = self.take_idx()
        name = '{}-{}.log'.format(prefix, idx)

        dst_path = os.path.join(self.log_dir, name)
        shutil.copy(src_log, dst_path)
        return dst_path
