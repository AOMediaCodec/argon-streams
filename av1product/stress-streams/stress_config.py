################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A module representing stresser.py configuration information'''

import os
import re
import xml.etree.ElementTree as ET

# We only use the List and Optional types in typing comments, which flake8
# understands but pylint doesn't. Ho hum.
#
# pylint: disable=unused-import
from typing import Callable, Dict, Iterable, List, Pattern, Optional
# pylint: enable=unused-import

from codec import Encoder, EncoderProfile, EncSettings
from level_info import LevelInfo
import expr


_Reader = Callable[['StressConfig', ET.Element, Optional[Pattern]], None]
_ReaderDict = Dict[str, _Reader]
_KeyDict = Dict[str, bool]


class StressConfig:
    '''Represents the XML stresser configuration file'''
    def __init__(self,
                 path: str,
                 level_info: LevelInfo,
                 tgt_regex: Optional[Pattern[str]]):
        self.encoders = {}  # type: Dict[str, Encoder]
        self.targets = {}  # type: Dict[str, EncoderProfile]
        self.default_encoder = None  # type: Optional[Encoder]
        self.level_info = level_info
        self.inc_dir = os.path.dirname(path)

        psp = {
            0: 15,
            1: 30,
            2: 36
        }

        max_bitrate = (1000000 *
                       level_info.level_max.high_mbps *
                       (1 + level_info.profile))

        self.env = {
            'num_frames': level_info.num_frames,
            'frame_area': (level_info.level_max.size[0] *
                           level_info.level_max.size[1]),
            'PicSizeProfile': psp[level_info.profile],
            'is_420': 1 if level_info.profile == 0 else 0,
            'MaxBits': round(max_bitrate *
                             level_info.num_frames /
                             level_info.level_max.frame_rate)
        }

        root = ET.parse(path).getroot()
        try:
            self.read_xml(root, tgt_regex)
        except RuntimeError as error:
            raise RuntimeError('Failed to load config file at `{}\': {}'
                               .format(path, error))

        if not self.encoders:
            raise RuntimeError('No encoder defined in file `{}\'.'
                               .format(path))

        # This should have been defined as the first encoder we found.
        assert self.default_encoder

        if not self.targets:
            raise RuntimeError('No targets defined in file `{}\'.'
                               .format(path))

    @staticmethod
    def check_tag(element: ET.Element,
                  expected_tags: Iterable[str], where: str) -> None:
        '''Raise an error if element doesn't have one of the expected tags.'''
        if element.tag not in expected_tags:
            raise RuntimeError('Element {} has tag `{}\', not one of {}.'
                               .format(where, element.tag,
                                       list(expected_tags)))

    @staticmethod
    def get_reader(element: ET.Element,
                   readers: _ReaderDict, where: str) -> _Reader:
        '''Find the right reader for an element or raise an error.'''
        StressConfig.check_tag(element, readers.keys(), where)
        return readers[element.tag]

    @staticmethod
    def check_keys(element: ET.Element, key_dict: _KeyDict) -> None:
        '''Raise an error if element doesn't have the expected keys'''
        keys = set(element.keys())
        if not keys <= set(key_dict.keys()):
            raise RuntimeError('{} element has unknown attributes: {}.'
                               .format(element.tag,
                                       keys - set(key_dict.keys())))
        for key, required in key_dict.items():
            if required and key not in keys:
                raise RuntimeError('{} element is missing '
                                   'the required attribute: {}.'
                                   .format(element.tag, key))

    def read_xml(self,
                 root: ET.Element,
                 tgt_regex: Optional[Pattern[str]]) -> None:
        '''Read data from the configuration xml'''
        StressConfig.check_tag(root, ['stress-config'], 'at root')
        readers = {
            'encoder': StressConfig.read_encoder,
            'target': StressConfig.read_target
        }  # type: _ReaderDict
        for child in root:
            reader = StressConfig.get_reader(child, readers,
                                             'immediately below root')
            reader(self, child, tgt_regex)

    def read_encoder(self,
                     element: ET.Element,
                     _: Optional[Pattern[str]]) -> None:
        '''Read an <encoder> element.'''
        StressConfig.check_keys(element, {'name': True,
                                          'funs': True,
                                          'base': False})
        name = element.get('name')
        funs = element.get('funs')
        base = element.get('base')

        if name in self.encoders:
            raise RuntimeError('Duplicate encoder tag with name {}.'
                               .format(name))

        fun_list = []  # type: List[str]
        if base is not None:
            if base not in self.encoders:
                raise RuntimeError('Encoder tag uses base `{}\', '
                                   'which hasn\'t been seen.'
                                   .format(base))
            fun_list += self.encoders[base].profile_funs

        assert name is not None
        assert funs is not None
        fun_list += funs.replace('{}', str(self.level_info.profile)).split(',')
        self.encoders[name] = Encoder(fun_list)
        if self.default_encoder is None:
            self.default_encoder = self.encoders[name]

    def read_target(self,
                    element: ET.Element,
                    tgt_regex: Optional[Pattern[str]]) -> None:
        '''Read a <target> element.'''
        StressConfig.check_keys(element, {'name': True,
                                          'encoder': False,
                                          'config-file': False,
                                          'config-name': False,
                                          'upper-bound': False})
        name = element.get('name')
        encoder_name = element.get('encoder', None)
        config_file = element.get('config-file', None)
        config_name = element.get('config-name', None)
        upper_bound = element.get('upper-bound', None)

        assert name is not None

        if name in self.targets:
            raise RuntimeError('Duplicate target with name {}.'
                               .format(name))
        if not re.match(r'[0-9a-zA-Z_.-]*$', name):
            raise RuntimeError('Target name `{}\' is ill-formed.'.format(name))

        # pylint: disable=len-as-condition
        if len(element):
            raise RuntimeError('Target element with name {} has children.'
                               .format(name))
        # pylint: enable=len-as-condition

        if self.default_encoder is None:
            raise RuntimeError('Target element with name {} '
                               'appears before any encoders.'
                               .format(name))

        if encoder_name is not None and encoder_name not in self.encoders:
            raise RuntimeError('Target element with name {} '
                               'uses encoder `{}\', which is not defined.'
                               .format(name, encoder_name))

        encoder = (self.default_encoder if encoder_name is None
                   else self.encoders[encoder_name])

        if config_file is not None:
            config_file = os.path.normpath(os.path.join(self.inc_dir,
                                                        config_file))
            if not os.path.exists(config_file):
                raise RuntimeError('No such config file: `{}\'.'
                                   .format(config_file))

        config_args = None
        if config_name is not None:
            if config_file is None:
                raise RuntimeError('Target element with name {} '
                                   'sets config name but not config file.'
                                   .format(name))
            config_args = (config_file, config_name)

        ub_val = None
        if upper_bound is not None:
            ub_val = expr.evaluate(self.env, upper_bound)

        if tgt_regex is not None:
            if not tgt_regex.match(name):
                print('Skipping target `{}\' as it doesn\'t match filter.'
                      .format(name))
                return

        enc_settings = EncSettings(config_args, ub_val)
        self.targets[name] = EncoderProfile(name,
                                            encoder,
                                            self.level_info,
                                            enc_settings)
