################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

#!/usr/bin/env python3

'''Check that all the streams are below the maximum bitrate.

'''

import argparse
import os
import re
import sys

from database import Database
from level_info import get_level_info


def check_db(db_path: str, force: bool) -> None:
    '''Get CSVRow objects for each entry in the db'''
    streams_dir = os.path.join(os.path.dirname(db_path), 'streams')
    database = Database(db_path, streams_dir)
    match = re.match(r'([0-2]) ([56]\.[0-3])$', database.header)
    if match is None:
        raise RuntimeError('Failed to parse DB header from `{}\'.'
                           .format(db_path))

    profile = int(match.group(1))
    level = match.group(2)

    level_info = get_level_info(level, profile)
    max_size = level_info.max_stream_size()
    first = True

    bad_targets = []
    for target, (max_val, path, size) in database.best.items():
        if size is None or size <= max_size:
            continue

        if first:
            print('Oversize streams for DB {}, max size {:,}:'
                  .format(db_path, max_size))
            first = False

        print(' stream {} (target: {}; value: {:,}) has size {:,}'
              .format(path, target, max_val, size))

        bad_targets.append(target)

    if force:
        for target in bad_targets:
            database.drop_stream(target)


def main() -> int:
    '''Main entry point'''
    parser = argparse.ArgumentParser()
    parser.add_argument('root')
    parser.add_argument('--force', action='store_true')

    args = parser.parse_args()

    dbs = []
    for root, _, files in os.walk(args.root):
        if 'db' in files:
            dbs.append(os.path.join(root, 'db'))
    sys.stderr.write('Visiting {} databases.\n'.format(len(dbs)))

    for db_path in dbs:
        check_db(db_path, args.force)

    return 0


if __name__ == '__main__':
    try:
        sys.exit(main())
    except RuntimeError as err:
        sys.stderr.write('Error: {}\n'.format(err))
        sys.exit(1)
