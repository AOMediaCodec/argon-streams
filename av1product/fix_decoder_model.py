#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import argparse
import filecmp
import os.path
import shlex
import shutil
import subprocess
import sys
import tempfile
from typing import List, Optional, Tuple

_HACK_HEADERS = os.path.join(os.path.dirname(__file__), 'hack_headers.py')
if not os.path.exists(_HACK_HEADERS):
    raise RuntimeError('Can\'t find tool at `{}\'.'
                       .format(_HACK_HEADERS))

_LevelDef = Tuple[int, int, int, Optional[bool]]


def render_def(ldef: _LevelDef) -> str:
    '''Render a level def as for the argument parsing in hack_headers.py'''
    count, major, minor, st = ldef
    return ('{}*{}.{}{}'
            .format(count, major, minor,
                    ('' if st is None else ('+' if st else '-'))))


class JobSpec:
    def __init__(self, path: str, levels: Optional[List[_LevelDef]] = None):
        self.path = path
        self.levels = levels

    @staticmethod
    def dump_log(path: str) -> None:
        '''Dump a log file to stderr'''
        with open(path) as handle:
            for line in handle:
                sys.stderr.write('  ' + line)

    @staticmethod
    def run_av1dec(av1dec: str,
                   obu: str,
                   yuv: str,
                   careful: bool,
                   log_path: str) -> None:
        '''Run av1dec to decode the given OBU to a YUV.

        On failure, print out an error message and the logs to stderr before
        raising a RuntimeError.

        '''
        assert os.path.exists(av1dec)
        if not os.path.exists(obu):
            raise RuntimeError('No such OBU file: `{}\'.'.format(obu))

        cmd = [av1dec]
        if careful:
            cmd.append('-v')
        cmd += ['-o', yuv, obu]
        try:
            with open(log_path, 'w') as log_file:
                subprocess.check_call(cmd, stdout=log_file, stderr=log_file)
        except subprocess.CalledProcessError:
            sys.stderr.write('av1dec command failed. Output was:')
            JobSpec.dump_log(log_path)
            raise RuntimeError('Failed command: {}'
                               .format([shlex.quote(arg) for arg in cmd]))

    @staticmethod
    def av1dec_check(av1dec: str,
                     old_stream: str, old_yuv: str,
                     new_stream: str, new_yuv: str,
                     log_path: str) -> None:
        '''Check that the OBU files give the same YUV results'''

        # Run av1dec on the new stream with checking enabled (we're supposed to
        # pass!)
        JobSpec.run_av1dec(av1dec, new_stream, new_yuv, True, log_path)

        # Nothing died. Run av1dec on the original stream (with checking
        # disabled).
        JobSpec.run_av1dec(av1dec, old_stream, old_yuv, False, log_path)

        # Check whether the YUVs match.
        if not filecmp.cmp(old_yuv, new_yuv):
            raise RuntimeError('Old and new YUV files are not equal.')

    def transform(self,
                  av1dec: Optional[str],
                  in_rel: str,
                  out_rel: str) -> None:
        '''Try to run the given job to transform this file'''
        in_path = os.path.join(in_rel, self.path)
        if not os.path.exists(in_path):
            raise RuntimeError('No such file: `{}\'.'.format(in_path))
        out_path = os.path.join(out_rel, self.path)
        if not os.path.isdir(os.path.dirname(out_path)):
            raise RuntimeError('No such destination directory: `{}\'.'
                               .format(os.path.dirname(out_path)))

        with tempfile.TemporaryDirectory() as tdir:
            new_stream = os.path.join(tdir, 'new.obu')
            new_yuv = os.path.join(tdir, 'new.yuv')
            old_yuv = os.path.join(tdir, 'old.yuv')
            log_path = os.path.join(tdir, 'log')

            cmd = ['python3', _HACK_HEADERS, '--annexb']
            if self.levels is not None:
                levels = ','.join(render_def(ldef) for ldef in self.levels)
                cmd.append('--levels={}'.format(levels))
            cmd += [in_path, new_stream]

            try:
                subprocess.check_call(cmd)
            except subprocess.CalledProcessError:
                raise RuntimeError('Header tool failed with command: {}'
                                   .format([shlex.quote(arg) for arg in cmd]))

            if av1dec is not None:
                JobSpec.av1dec_check(av1dec,
                                     in_path, old_yuv,
                                     new_stream, new_yuv, log_path)

            # Write the new stream to out_rel.
            shutil.copyfile(new_stream, out_path)


_MAIN_SPECS = [
    JobSpec('profile0_core/streams/test5497.obu',
            levels=[(0, 2, 1, None)]),
    JobSpec('profile0_core/streams/test5226.obu',
            levels=[(0, 2, 1, None)]),
    JobSpec('profile1_core/streams/test1456_130_5767.obu',
            levels=[(3, 2, 0, None), (1, 2, 1, None), (0, 2, 0, None)]),
    JobSpec('profile1_core/streams/test2926_5183_1051.obu',
            levels=[(3, 2, 0, None), (1, 2, 1, None)]),
    JobSpec('profile1_core/streams/test3425_7295_3070.obu',
            levels=[(2, 3, 0, None), (1, 2, 0, None), (3, 2, 1, None)]),

    # This stream decodes fine at operating point 0, but fails at
    # operating point 1. We bump the level for just that OP from 2.0
    # to 2.1.
    JobSpec('profile2_core/streams/test4898_10049.obu',
            levels=[(2, 2, 0, None),
                    # 12 levels signalled (start of stream 10049)
                    (1, 2, 0, None), (1, 2, 1, None), (10, 2, 0, None),
                    # Sequence header repeated
                    (1, 2, 0, None), (1, 2, 1, None), (10, 2, 0, None)]),

    # Similarly, this stream just fails at operating point 6. Stream
    # 10406 twice signals level 5.3 with 1 OP (and is fine); stream
    # 9013 twice signals level 2.0 with 2 OPs (and is fine); stream
    # 10089 signals level 4.0 with 18 OPs and varying seq_tier flags.
    # OP 6 has seq_tier false and we will force it to be true.
    JobSpec('profile0_core/streams/test10406_9013_10089.obu',
            levels=[(2, 5, 3, None),
                    (4, 2, 0, None),
                    (6, 4, 0, None), (1, 4, 0, True), (11, 4, 0, None)]),

    JobSpec('profile0_core/streams/test10455.obu', levels=[(0, 5, 1, None)]),
    JobSpec('profile0_core/streams/test6097_8804_10466.obu',
            levels=[(3, 2, 0, None), (1, 5, 1, None)]),
    JobSpec('profile2_core/streams/test15606_7573_9999.obu',
            levels=[(1, 2, 0, None), (1, 4, 1, None), (1, 2, 0, None)]),
    JobSpec('profile0_core/streams/test3641_10492_9882.obu',
            levels=[(18, 2, 0, None), (2, 5, 0, True), (0, 2, 0, None)]),
    JobSpec('profile0_core/streams/test9065_5010_10294.obu',
            levels=[(3, 2, 0, None), (1, 5, 0, True)]),
    JobSpec('profile1_core/streams/test7401.obu',
            levels=[(0, 5, 1, None)]),
    JobSpec('profile0_core/streams/test10567_10791.obu',
            levels=[(1, 6, 1, None), (0, 6, 0, None)]),
    JobSpec('profile1_core/streams/test7249.obu',
            levels=[(0, 5, 2, None)]),
    JobSpec('profile0_core_special/streams/test54.obu',
            levels=[(0, 6, 2, None)]),
    JobSpec('profile0_core/streams/test10901_10579_10569.obu',
            levels=[(1, 6, 0, None), (2, 6, 1, None), (1, 6, 0, None)]),
    JobSpec('profile1_core_special/streams/test55.obu',
            levels=[(0, 6, 1, None)]),

    # This stream signals level 6.0 but even bumping up to 6.2 isn't enough.
    # Fortunately, it didn't signal seq_tier. Do that!
    JobSpec('profile0_core_special/streams/test55.obu',
            levels=[(0, 6, 0, True)]),

    JobSpec('profile0_core/streams/test10580_10588.obu',
            levels=[(0, 6, 2, None)]),
    JobSpec('profile1_core/streams/test7510.obu',
            levels=[(0, 6, 2, None)]),
    JobSpec('profile2_core/streams/test15829.obu',
            levels=[(0, 6, 2, None)]),

    # Like test55.obu, this stream signalled level 6.0 but we need seq_tier to
    # get a high enough bitrate.
    JobSpec('profile1_core/streams/test7513.obu',
            levels=[(0, 6, 0, True)])
]

_CLNT_A_SPECS = [
    JobSpec('profile0_core/streams/test19191.obu',
            levels=[(0, 5, 0, True)]),
    JobSpec('profile0_core/streams/test19138_13777_4518.obu',
            levels=[(2, 2, 0, None),
                    (2, 5, 0, True),
                    (1, 2, 0, None)]),
    JobSpec('profile0_core/streams/test18935_18337_9121.obu',
            levels=[(2, 5, 0, True),
                    (4, 5, 0, None),
                    (1, 2, 0, None)]),
    JobSpec('profile0_core/streams/test18772.obu',
            levels=[(1, 5, 0, True)]),
    JobSpec('profile0_core/streams/test2738_9291_18007.obu',
            levels=[(5, 2, 0, None),
                    (1, 5, 0, True)]),
    JobSpec('profile0_core/streams/test17792_18651.obu',
            levels=[(2, 5, 0, None),
                    (1, 5, 0, True)]),
    JobSpec('profile0_core/streams/test18797.obu',
            levels=[(1, 5, 0, True)])
]

_SPEC_DICT = {
    'main': _MAIN_SPECS,
    'client_a': _CLNT_A_SPECS
}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--av1dec')
    parser.add_argument('--release', choices=_SPEC_DICT.keys(), default='main')
    parser.add_argument('inrel')
    parser.add_argument('outrel')

    args = parser.parse_args()

    for spec in _SPEC_DICT[args.release]:
        print('-- Transforming {}'.format(spec.path))
        spec.transform(args.av1dec, args.inrel, args.outrel)

    return 0


if __name__ == '__main__':
    try:
        sys.exit(main())
    except RuntimeError as err:
        sys.stderr.write('Error: {}\n'.format(err))
        sys.exit(1)
