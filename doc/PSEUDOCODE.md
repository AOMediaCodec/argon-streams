[//]: # (Project P8005 HEVC)
[//]: # (\(c\) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company)
[//]: # (All rights reserved.)

# Pseudo-code

## Introduction

Video codec specifications typically contain a mixture of
pseudo-code and carefully written English.

The pseudo-code is usually in a two-column table.
The first column contains a mixture of
code and syntax elements (often syntax elements are written in bold).
The second-column gives an additional description for the rows containing syntax elements.

Syntax elements are computed from bits within the compressed bitstream,
while the code manipulates variables constructed based on these syntax elements.

The process of going from bits in the bitstream to the computed value of a syntax element
often involves a number of intermediate steps (for example, if an arithmetic coding process
is being used) and the description specifies which steps should be used.

The compiler is designed such that this two-column table can be copied from the
specification and pasted into a text file.  This avoids the possibility of bugs
being introduced (or removed!) by turning the pseudo-code into a standard programming language.

A file extension of ".c" is used as the pseudo-code in specifications closely resembles
the C programming language from the perspective of a code editor.

## Natural language

Some parts of the specification (typically the decoding processes) are written in English.
These normally have a straightforward translation into pseudo-code.
(It would be an interesting exercise to see to what extent this translation could also be done
automatically in the compiler, as the tight editorial policy means that the resulting English looks more
like a verbose programming language than natural prose.)

## Av1src vs Av1bsrc

The av1src directory contains the pseudo-code reverse engineered from the AV1 code base.

The av1bsrc directory contains the pseudo-code directly from the AV1 specification.

It is recommended to start by looking at av1bsrc as this matches the spec and does not contain
special code that is needed for constrained bitstream generation.

(We needed both directories because, for AV1, Argon Design was responsible for reverse engineering
the reference code and then creating the specification.  For codecs where the
specification was already written, only one directory was needed.)

## Examples

A good place to see examples is [av1b_syntax.c](tools/av1bsrc/av1b_syntax.c).

Most of the examples in this section are taken from this file.

### Control flow

Section 5.11.21 of the AV1 specification looks like:

![get_segment_id](doc/get_segment_id.png)

This appears in the pseudo-code as:

```
// Get Segment ID Syntax
// The predicted segment id is the smallest value found in the on-screen region of the segmentation map covered by the current block.
get_segment_id( ) {
    bw4 = Num_4x4_Blocks_Wide[ MiSize ]
    bh4 = Num_4x4_Blocks_High[ MiSize ]
    xMis = Min( MiCols - MiCol, bw4 )
    yMis = Min( MiRows - MiRow, bh4 )
    seg = 7
    for ( y = 0; y < yMis; y++ )
        for ( x = 0; x < xMis; x++ )
            seg = Min( seg, PrevSegmentIds[ MiRow + y ][ MiCol + x ] )
    return seg
}
```

The heading and the explanation text are commented out while the code itself is unchanged.

This example illustrates the use of for loops and the built-in function `Min`, as well
as use of constant lookup tables.

### Constant tables

Tables can be defined by copying the definitions from the spec, e.g. Num_4x4_Blocks_Wide
is defined higher up as:

```
//For a block size x, Num_4x4_Blocks_Wide[ x ] gives the width of the block in units of 4 samples.
int Num_4x4_Blocks_Wide[ BLOCK_SIZES ] = {
    1, 1, 2, 2, 2, 4, 4, 4, 8, 8, 8, 16,
    16, 16, 32, 32, 1, 4, 2, 8, 4, 16
};
```

### Syntax elements

Code with syntax elements appears in the spec using the second column, for example:

![byte_alignment](doc/byte_alignment.png)

use the `f(1)` descriptor which reads a single bit from the bitstream.

This appears in the pseudo-code as:
```
byte_alignment( ) {
    while ( get_position( ) & 7 )
          zero_bit                                          u(1)
}
```
This is almost the same expect `f(1)` has been replaced with `u(1)` to be consistent
with how HEVC defines syntax elements that are directly encoded in the bitstream.

### Checks

The specification often contains assertions about values that must be satisfied
by a compliant bitstream.

These are represented by calls to the builtin CHECK function, for example:

```
trailing_bits( nbBits ) {
      trailing_one_bit                                      u(1)
    CHECK( trailing_one_bit == 1, "trailing_one_bit must be 1")
    CHECK( nbBits > 0, "When the syntax element trailing_one_bit is read, it is a requirement that nbBits is greater than zero")
    nbBits--
    while ( nbBits > 0 ) {
          trailing_zero_bit                                 u(1)
        CHECK( trailing_zero_bit == 0, "trailing_zero_bit must be 0")
        nbBits--
    }
}
```

The CHECK function will evaluate the expression in the first argument, and
cause the program to fail if the bitstream does not pass the test.
The second argument is printed when the failure occurs to explain the reason. 

### Coverage expressions

The pseudo-code can be used to produce a coverage executable that produces a report
describing which cases have been seen.

Everything is included in this coverage by default, but sometimes it is useful to exclude
some parts of the pseudo-code.

For example:

```
   if ( COVERCLASS((PROFILE0 || PROFILE2), 1, mono_chrome) ) {
        color_range                                                               u(1)
        subsampling_x = 1
        subsampling_y = 1
        chroma_sample_position = CSP_UNKNOWN
        separate_uv_delta_q = 0
    }
```

In the specification, the first line is just `if (mono_chrome) {`.

The problem is that the specification says
"Monochrome can only be signaled when seq_profile is equal to 0 or 2."
This means that a profile 1 stream is not allowed to cover both branches of this expression.

To be able to exclude certain profiles we use the COVERCLASS built-in which has three arguments:

- the first argument specifies conditions when the expression may be true
- the second argument specifies conditions when the expression may be false
- the third argument contains the expression

In this example, mono_chrome may be true only when profile 0 or 2, so the first argument is `(PROFILE0 || PROFILE2)`.  However, mono_chrome may always be false (in any profile), so the second argument is `1`.

There is also a two argument form `COVERCLASS(guard,expr)` which is the same as `COVERCLASS(guard,guard,expr)`.  This is useful for expressions that will not be reached when the guard is false.

There is also a one argument form `COVERCLASS(guard) { block of code }`.  This form excludes all coverage analysis from the block of code if guard is false.

### Arithmetic decoding

As syntax elements and arithmetic coding is so integral to video codecs, there
are dedicated files that provide a concise way of specifying the information
in section 8 of the specification ("Parsing process").

Section 8 specifies how to determine the context for each arithmetically
encoded syntax element.

For example, the context for syntax element has_palette_uv is defined in the spec as:

![has_palette_uv](doc/has_palette_uv.png)

In `av1b_cabac.c` this is written as:

```
has_palette_uv : unused_init, b_has_palette_uv() = ( PaletteSizeY > 0 ) ? 1 : 0;
```

This gives:

- the name of the syntax element `has_palette_uv`
- a method of initializing the CDFs `unused_init` - this feature is unused in AV1)
- the binarization function to be used `b_has_palette_uv()`
- the expression that can be used to compute the context (or -1 if no context is needed)

A block of code can also be used instead of an expression.  In this case,
the context should be assigned to a variable called ctxIdxOffset.

The binarization functions are found in `av1b_binarization.c`.

This binarization for this syntax element could be written as (the actual code has a few more lines to give more detailed coverage information):

```
b_has_palette_uv() {
    context_fn()
    syntax = read_symbol( TilePaletteUVModeCdf[ ctxIdxOffset ], 2, 1 )
}

```

A call to `context_fn()` will be replaced by the code in `av1b_cabac.c` that corresponds to this syntax element.
The final value is contained in the special variable `syntax`, and this will get assigned to the syntax element has_palette_uv.
The function `read_symbol` is a function that contains the arithmetic symbol decoding
process.

Both the binarization `b_has_palette_uv` and the context expression in
`context_fn` are inlined into the pseudo-code,
meaning that these expressions are free to use any local variables that are in
scope at the point the syntax element is read.
(This matches the way that the specification is also free to use local variables
for syntax elements.)

### Arithmetic encoding

The pseudo-code can also be used for encoding test bitstreams using constrained random generation.

When encoding, a choice needs to be specified, which results in different values
being allowed for the syntax elements.

The different choices are in the [profile file](tools/av1src/av1_profile.c).

A profile consists of function calls and assignments.

For example:

```
choose_motion_mode() {
  choose_warp_local()
  cur_frame_force_integer_mv = 1
  gm_bit_1 = ChooseBit()
  motion_mode = ChooseBit() ? WARPED_CAUSAL : (ChooseBit() ? OBMC_CAUSAL : SIMPLE_TRANSLATION)
}

```

In this code, `choose_warp_local()` refers to another choice in the file that contains additional assignments.  This is just a convenience to avoid having to repeat similar settings.

The assignment `cur_frame_force_integer_mv = 1` means that whenever the syntax element `cur_frame_force_integer_mv` is present in the bitstream, it will be given a value of 1.

The assignment `gm_bit_1 = ChooseBit()` makes use of the builtin function `ChooseBit` which generates a random number.  This means that whenever the syntax element gm_bit_1 is present in the bitstream, it will be randomly assigned.

When built in encode mode, the appropriate assignment will be inlined whenever the syntax element is due to be coded.

Note that these assignments are quite different to assignments elsewhere in the pseudo-code.  They are not run all at the same time, instead the appropriate expression is picked based on the syntax elements encountered during the encode.

When more complicated generation is required, the right hand side expression is allowed to call a function, typically prefixed with `enc_` and contained in the `av1_encode.c` file.

As for decode, any local variables in scope at the point where the syntax element is present
are allowed to be accessed by these encoder expressions.
