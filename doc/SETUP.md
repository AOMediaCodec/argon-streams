[//]: # (Project P8005 HEVC)
[//]: # (\(c\) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company)
[//]: # (All rights reserved.)

# Setup

## Package dependencies

Installations required to get most of the scripts in this directory to work
Ubuntu 20.04 LTS as of May 2021:

```sh
sudo apt-get update
sudo apt-get install cmake g++ python2 python3 bison flex libssl-dev unzip parallel nasm
```

Note: some of the current scripts require Python 2, and will not work under Python 3,
so both versions need to be installed

## Libaom

Several of the scripts here (notably parallel_makeall and autoproduct) require
a copy of libaom so that they can calculate the expected output for each stream.
This requires the following steps:

```sh
cd <root of this repository>
git clone https://aomedia.googlesource.com/aom aom-src
mkdir tools/build-aom
cd tools/build-aom
cmake -DCMAKE_BUILD_TYPE=Release ../../aom-src/
```

Note: You must run the cmake step above manually. This is delibrate, as it
allows you to set the configuration for libaom, for example to enable or
disable experiments when developing a new codec version.

To speed up builds, you may want to add the following flags, but these are not
mandatory:

```sh
cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_CCACHE=1 -DENABLE_TESTS=0 -DENABLE_DOCS=0 ../../aom-src/
```

However, the various scripts here will take care of rerunning `make` as needed,
so that step does not need to be done manually.
