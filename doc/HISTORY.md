[//]: # (Project P8005 HEVC)
[//]: # (\(c\) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company)
[//]: # (All rights reserved.)

# History

## Background

At Argon Design we have been involved in the development of a number of video codec
products and in 2012 we took an interest in the developing HEVC video coding standard.

Video codec standards give a full description of the decoding process, but leave
great flexibility in the encoding process.
Decoders are required to fully support all features of the standard, including ones
that encoders may only decide to use in the future.

The decoding process is typically specified in an ad-hoc manner that is a mixture of
pseudo-code and carefully written English that is intended to be unambiguous and
precisely define all aspects of the decoder.  However, video coding specifications
have grown to hundreds of pages and it is very hard to both prepare and implement
the specification correctly.

## The Idea

Our idea was to address this difficulty by writing a compiler capable of understanding
the pseudo-code directly and translating into different executable programs.

Some examples of the pseudo-code it can handle are described in
[pseudocode](doc/PSEUDOCODE.md), and the different executables it can make are described
in [executables](doc/EXECUTABLES.md).

## The Results

This proved to be very effective and allowed us to find and report over 50 errors in the
HEVC specification and reference code before the standard was finalized.

Once the standard was finished, we were able to use the same pseudo-code to
automatically construct test bitstreams that cover the full range of the specification.
These test bitstreams can be
used by software and hardware decoder implementors to verify that they have implemented
the specification correctly, including features that even the reference encoder
does not yet support.

We applied the same process to produce bitstreams for VP9 and AV1, and were chosen by
the Alliance for Open Media to write the specification for AV1 because our tool
made it so much easier to detect and fix any inconsistencies between the reference code
and the specification.

## Open Source

We are now choosing to open source both a set of test bitstreams for AV1,
and the compiler technology that can be used to generate such test bitstreams
or be used for future codec development to ensure accurate and verified specifications.

If you have an AV1 product to test, you should use the bitstreams (as they take a long
time to generate).

If you have a new video codec, then you will want to understand and replace the pseudo-code
specifications to match the new specification.

