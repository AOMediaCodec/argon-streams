[//]: # (Project P8005 HEVC)
[//]: # (\(c\) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company)
[//]: # (All rights reserved.)

# Executables

This document explains the different executables made by the Argon AV1 Compiler and how they are used.

The bit-stream generation process is illustrated in this diagram:
![Process](doc/process.png)

There are three executables that can be generated:

- Argon Bit-stream generator
- Software Decoder
- Argon Coverage Tool AV1

These executables can be run directly, or run automatically via a Python script
called autoproduct.
(The autoproduct tool makes use of these executables to
produce a small set of bitstreams that produce a full set of coverage streams.)

## Argon Bit-stream generator

This executable generates a bitstream based on particular choice.
The choice controls the range of legal values for syntax elements
as explained [here](PSEUDOCODE.md#Arithmetic-encoding).

For example, the choice may restrict the profile to be baseline,
or restrict the maximum frame size.

The generator is used by autoproduct to produce many bitstreams with different settings.

## Software Decoder

The software decoder takes a bitstream as input and produces a decoded raw video file.
This video is usually passed through a checksum program to avoid needing to store the
raw data.

The software decoder is used to produce a reference checksum.

To test a decoder implementation, you would decode the test bitstreams with your
own decoder and check that the checksums match the ones provided by the software decoder.

We used this method to check that the reference decoder produced the same decoded videos
as the specification, and used this to fix bugs in both the specification and the
reference decoder.

The software decoder is used by autoproduct to check the bitstreams match the reference
decoder and are legal bitstreams.

## Argon Coverage Tool AV1

The coverage tool is very similar to the software decoder except it is quite a bit slower
because it tracks the values taken by every expression in the decode process.

The output is a file which can be viewed in a web browser and gives a UI to allow
the coverage to be measured and explored, including noting excatly which test covered
every coverage point.

The coverage tool is used by autoproduct to identify which test streams a covering interesting
cases and to select a small set of test streams that are nevertheless still able
to hit all coverage points.
