[//]: # "Project P8005 HEVC"
[//]: # "\(c\) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company"
[//]: # "All rights reserved."

# Advanced Usage

It is possible to run the individual components depicted above seperately for a variety of purposes.  This section details how to build and run the executable for each component.

The build system has evolved to cope with the same underlying pseudo-code and code being required for multiple executables or targets.

In the instructions below, firstly set:
```sh
export P8005_BUILD_PREFIX=<build path>
```

# Bit-stream Generator

The bit-stream generator is the tool used to produce a randomly generated bit-stream.  It is compiled from tools/av1src.  Firstly it is necessary to choose the profile, which defines how to randomly sample the values encoded within the bit-stream.  The profiles are listed in tools/av1src/av1_profile.c, for example profile_regression0.

For example to build a bit-stream generator using profile_regression0:
```sh
cd tools
make -j PROFILE=profile_regression0 USE_ENC_PROFILE_DIR=yes TARGET=17 c-build
cd -
```

To generate an Annex-B packed bitstream with the profile_regression0 profile:
```sh
$P8005_BUILD_PREFIX/build/av1enc/profile_regression0/av1.gen.exe -o out.obu
```

nb some encodes may fail if it has been impossible to encode a certain combination of values.  Usually a check failure will signal this.

Salient switches to match different parts of the specification include --not-annexb and --large-scale-tile.

For the full options
```sh
$P8005_BUILD_PREFIX/build/av1enc/profile_regression0/av1.gen.exe --help
```

# Bit-stream Decoder

The bit-stream decoder is the tool used to decode a bit-stream to a raw yuv output.  It can be compiled from tools/av1src or tools/av1bsrc.  The decoder also performs various checks, specified in the specification, to help determine whether a bit-stream is valid.

For example to build a bit-stream decoder using tools/av1src:
```sh
cd tools
make -j TARGET=16 $P8005_BUILD_PREFIX/build/av1dec/av1.gen.exe
cd -
```

For example to build a bit-stream decoder using tools/av1bsrc:
```sh
cd tools
make -j TARGET=20 $P8005_BUILD_PREFIX/build/av1bdec/av1b.gen.exe
cd -
```

To decode:
```sh
$P8005_BUILD_PREFIX/build/av1dec/av1.gen.exe -o out.yuv out.obu
$P8005_BUILD_PREFIX/build/av1bdec/av1b.gen.exe -o out2.yuv out.obu
```

nb some decodes may fail if it has been possible to encode an invalid combination of values.  Usually a check failure will signal this.

To produce an md5sum (should match!):
```sh
md5sum out.yuv out2.yuv
```

To determine the command line which would be required for aomdec (AOM's reference decoder):
```sh
$P8005_BUILD_PREFIX/build/av1bdec/av1b.gen.exe --estimate-cmd out.obu
```

For the full set of options:
```sh
$P8005_BUILD_PREFIX/build/av1dec/av1.gen.exe --help
$P8005_BUILD_PREFIX/build/av1bdec/av1b.gen.exe --help
```

# Coverage Tool

The coverage tool is used to compute the coverage report for a particular bit-stream or set of bit-streams.  It is compiled from tools/av1bsrc.

For example to build a bit-stream decoder using tools/av1bsrc (the warnings printed during compile can be ignored):
```sh
cd tools
make -j TARGET=21 $P8005_BUILD_PREFIX/build/av1bcover/av1b.gen.exe
cd -
```

To produce a coverage for a single input stream:
```sh
$P8005_BUILD_PREFIX/build/av1bcover/av1b.gen.exe -p coverage.xml out.obu
```

For the full set of options:
```sh
$P8005_BUILD_PREFIX/build/av1bcover/av1b.gen.exe --help
```

# Setting up a test or continuous integration system

Building an entire set of bit-streams using the autoproduct scripts is a time and compute intensive process.  As such, it is convenient for an automated test system to use a subset.  Please look under the jenkins/ directory and follow through to the av1product/ directory, to see how this has been previously implemented.
