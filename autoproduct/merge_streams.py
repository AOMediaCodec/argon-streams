#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import sys
import os
import random
import shutil
from av1_stream_allowed_second_merge_file import av1_get_allowed_second_merge

from typing import Dict, List, Set, Tuple

verbose = False
FIRST_STREAM_KEY_FRAME_ONLY = False


#######
# OBU #
#######
def OBUallowedSecondMergeFile(inputFile: str,
                              extraEncodeOptions: List[str] = []) -> bool:
    if verbose:
        print("OBUallowedSecondMergeFile", inputFile, extraEncodeOptions)
    if "--large-scale-tile" in extraEncodeOptions:
        return False
    annexB = "--not-annexb" not in extraEncodeOptions
    return av1_get_allowed_second_merge(inputFile, annexB, False)


def mergeStreamsOBU(outputFile: str,
                    inputFiles: List[str],
                    extraEncodeOptions: List[str] = []) -> None:
    if verbose:
        print("Merging {} files".format(len(inputFiles)))
    ordered_files = []  # type: List[str]
    for fname in inputFiles:
        if not OBUallowedSecondMergeFile(fname, extraEncodeOptions):
            if ordered_files:
                raise RuntimeError("Only the first stream to be merged can "
                                   "have the first frame not shown")
            ordered_files.append(fname)
    for fname in inputFiles:
        if fname not in ordered_files:
            ordered_files.append(fname)
    with open(outputFile, 'wb') as out:
        for fname in ordered_files:
            if verbose:
                print('OBUallowedSecondMergeFile',
                      OBUallowedSecondMergeFile(fname, extraEncodeOptions))
            with open(fname, 'rb') as x:
                shutil.copyfileobj(x, out, 1024*1024)


#####################
# DIRECTORY MERGING #
#####################
def getStreamsToUse(singleStreams: Set[str],
                    count: int,
                    ssAllowSecondMergeFile: Dict[str, bool]) -> Set[str]:
    if FIRST_STREAM_KEY_FRAME_ONLY:
        # Some streams can only be the first in a merge set, we randomly pick
        # any start and then check the condition on the rest
        single_stream_list = list(singleStreams)
        random.shuffle(single_stream_list)
        ret_val = {single_stream_list[0]}
        if verbose:
            print("Selecting", ret_val)
        for a_stream in single_stream_list[1:]:
            if len(ret_val) >= count:
                break
            if ssAllowSecondMergeFile[a_stream]:
                ret_val.add(a_stream)
                if verbose:
                    print("Including", a_stream,
                          ssAllowSecondMergeFile[a_stream])
        if len(ret_val) < count:
            return set()
        return ret_val
    else:
        return set(random.sample(singleStreams, count))


def mergeStreams(streamDirs: List[str],
                 outputStreamDir: str,
                 stream_type: str,
                 just_copy: bool,
                 extraEncodeOptions: List[str] = [],
                 minimalSetStreams: List[Tuple[str, str]]=[]) -> None:
    global FIRST_STREAM_KEY_FRAME_ONLY

    if stream_type != 'obu':
        raise ValueError('Unknown stream type: {}'.format(stream_type))

    extension = "obu"
    mergeFunc = mergeStreamsOBU
    FIRST_STREAM_KEY_FRAME_ONLY = True

    chosen_streams = {streamDir:[stream+"." + extension for subset, stream in minimalSetStreams if subset in streamDir] for streamDir in streamDirs}
    dirLists = [[x for x in os.listdir(streamDir) if x in chosen_streams[streamDir]] for streamDir in streamDirs]

    singleStreams = set(x for dirList in dirLists for x in dirList
                        if x.endswith("." + extension) and "_" not in x)
    stream_paths = {x: os.path.join(streamDirs[streamDirInd], x)
                    for streamDirInd, dirList in enumerate(dirLists)
                    for x in dirList}
    alreadyCombinedStreams = set(x
                                 for dirList in dirLists
                                 for x in dirList
                                 if x.endswith("." + extension) and "_" in x)

    for k in alreadyCombinedStreams:
        shutil.copyfile(stream_paths[k], os.path.join(outputStreamDir, k))

    if just_copy:
        for stream in singleStreams:
            if verbose:
                print("Skipping non-mergeable stream {}".format(stream))
            shutil.copyfile(stream_paths[stream],
                            os.path.join(outputStreamDir, stream))

        return

    if FIRST_STREAM_KEY_FRAME_ONLY:
        ssAllowSecondMergeFile = \
            {x: OBUallowedSecondMergeFile(stream_paths[x], extraEncodeOptions)
             for x in singleStreams}

    while singleStreams:
        # Remove one that we'll leave as a single CVS
        singleCVSFile = singleStreams.pop()
        if verbose:
            print("Skipping {}".format(singleCVSFile))
        shutil.copyfile(stream_paths[singleCVSFile],
                        os.path.join(outputStreamDir, singleCVSFile))

        # Combine three, then two streams if we can
        for num in [3, 2]:
            if len(singleStreams) < num:
                continue

            to_use = getStreamsToUse(singleStreams, 3,
                                     ssAllowSecondMergeFile)
            if not to_use:
                continue

            if verbose:
                print("Joining {}".format(to_use))

            inputs = [stream_paths[x] for x in to_use]
            bases = [x.rpartition(".")[0][4:] for x in to_use]
            dstname = "test{}.{}".format("_".join(bases), extension)
            mergeFunc(os.path.join(outputStreamDir, dstname),
                      inputs, extraEncodeOptions)
            singleStreams -= to_use


def syntax() -> None:
    prog = sys.argv[0]
    print("OBU stream merge tool")
    print()
    print("Syntax:")
    print()
    print("    {} <output stream dir> <input stream dir>".format(prog))
    print()
    print("        Merges streams in the given directories")
    print("        in groups of two or three, also leaving")
    print("        some unmerged, and places the results in")
    print("        the given output directory.")
    print()
    print("    {} <output file> <input files...>".format(prog))
    print()
    print("    Merges the given files, writing the result to")
    print("    the specified output file.")
    print()
    sys.exit(1)


if __name__ == "__main__":
    verbose = True

    if len(sys.argv) < 3:
        syntax()

    # Determine whether we're doing one merge or a whole directory
    if os.path.isdir(sys.argv[2]):
        streamDir = sys.argv[2]
        outputStreamDir = sys.argv[1]

        # Determine stream type
        files = os.listdir(streamDir)
        if not any(x.endswith(".obu") for x in files):
            raise RuntimeError('No .obu files in {}'
                               .format(streamDir))

        if not os.path.exists(outputStreamDir):
            os.makedirs(outputStreamDir)

        mergeStreams([streamDir], outputStreamDir, 'obu', False)
    else:
        # Determine stream type
        outputFile = sys.argv[1]
        inputFiles = sys.argv[2:]

        for infile in inputFiles:
            if not infile.endswith('.obu'):
                raise RuntimeError('Unknown file type: {}'
                                   .format(infile))

        mergeStreamsOBU(outputFile, inputFiles, [])

    print("Done.\n")
