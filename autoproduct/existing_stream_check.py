################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import xml.etree.ElementTree as ET
import sys
import shutil
import os
import importlib
import coveragestore
import buildtools
import signal
import merge_streams
from itertools import islice, ifilterfalse
import random
import multiprocessing
import ap_subprocess
import transform_coverage_report
import re

def sigint_handler(signum, frame):
  global exiting
  if exiting:
    print "Double SIGINT, exiting."
    sys.exit(1)
  else:
    print "Finishing current batch before exiting."
    print "You can press Ctrl-C again to exit immediately, but this may lead to errors if you"
    print "later intend to continue this set."

    exiting=1

ap_subprocess.init()
