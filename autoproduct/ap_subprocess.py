################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import subprocess
import sys
import threading
import time
import re
import signal
import os
import ap_subprocess_launcher

class APProcess:
  def __init__(self):
    self.out=None
    self.err=None
    self.returncode=None
    self.sem=threading.Semaphore()
    self.sem.acquire()
  def check_finished(self):
    if self.sem.acquire(False):
      self.sem.release()
      if self.returncode!=0:
        debugFile.write("DEBUG: wait(): returning {}\n".format(self.returncode))
        debugFile.flush()
      return self.returncode
    return None
  def wait_small(self):
    time.sleep(0.1)
  def wait(self):
    while not self.sem.acquire(False):
      time.sleep(0.1)
    self.sem.release()
    if self.returncode!=0:
      debugFile.write("DEBUG: wait(): returning {}\n".format(self.returncode))
      debugFile.flush()
    return self.returncode

processes={}
launchQueue=[]

def readToEnd(stream):
  out=""
  while True:
    line=stream.readline()
    if line.strip()=="END":
      break
    elif re.match("^-*END$", line.strip())!=None:
      line=line[1:]
    out+=line
  return out

def init(name="ap_subprocess"):
  global monitorThread
  global launcherIn, launcherOut
  global debugFile
  launcherInEnd, launcherIn=os.pipe()
  launcherOut, launcherOutEnd=os.pipe()
  if os.fork()==0:
    os.close(launcherIn)
    os.close(launcherOut)
    ap_subprocess_launcher.launch(os.fdopen(launcherInEnd), os.fdopen(launcherOutEnd, 'w'))
    sys.exit(0)

  os.close(launcherInEnd)
  os.close(launcherOutEnd)
  launcherIn=os.fdopen(launcherIn, 'w')
  launcherOut=os.fdopen(launcherOut)

  debugFile = open("{}_debug.txt".format(name), "w")
  def monitor():
    while True:
      line=launcherOut.readline().strip()
      parts=line.split(" ")
      if parts[0]=="N":
        p=launchQueue.pop(0)
        p.processNumber=int(parts[1])
        processes[p.processNumber]=p
      elif parts[0]=="F":
        debugFile.write("DEBUG: Process finished: {}\n".format(line))
        processNumber=int(parts[1])
        returncode=int(parts[2])
        p=processes[processNumber]
        line=launcherOut.readline().strip()
        if line=="STDOUT":
          p.out=readToEnd(launcherOut)
          line=launcherOut.readline().strip()
        if line=="STDERR":
          p.out=readToEnd(launcherOut)
          line=launcherOut.readline().strip()
        assert line=="END"
        p.returncode=returncode
        p.sem.release()
        del processes[processNumber]

  monitorThread=threading.Thread(target=monitor)
  monitorThread.daemon=True
  monitorThread.start()

def doLaunch(cmd, launcherDict):
  line="{} {}\n".format(cmd, repr(launcherDict))
  launcherIn.write(line)
  launcherIn.flush()
  proc=APProcess()
  launchQueue.append(proc)
  return proc

def launch(args, shell=False, cwd=None):
  launcherDict={'args':args, 'shell':shell, 'cwd':cwd}
  return doLaunch("LAUNCH_NO_OUTPUT", launcherDict)

def launch_with_stdout(args, shell=False, cwd=None):
  launcherDict={'args':args, 'shell':shell, 'cwd':cwd}
  return doLaunch("LAUNCH_WITH_STDOUT", launcherDict)

def launch_with_stderr(args, shell=False, cwd=None):
  launcherDict={'args':args, 'shell':shell, 'cwd':cwd}
  return doLaunch("LAUNCH_WITH_STDERR", launcherDict)

def launch_with_stdout_and_stderr(args, shell=False, cwd=None):
  launcherDict={'args':args, 'shell':shell, 'cwd':cwd}
  return doLaunch("LAUNCH_WITH_STDOUT_AND_STDERR", launcherDict)

def launch_with_grep_stdout(args, grepString, shell=False, cwd=None):
  launcherDict={'args':args, 'shell':shell, 'cwd':cwd, 'grepString':grepString}
  return doLaunch("LAUNCH_WITH_GREP_STDOUT", launcherDict)

def launch_stdout_to_file(args, file, shell=False, cwd=None, append=False):
  launcherDict={'args':args, 'shell':shell, 'cwd':cwd, 'file':file, 'append':append}
  return doLaunch("LAUNCH_WITH_STDOUT_FILE", launcherDict)

def launch_stdout_and_stderr_to_file(args, file, shell=False, cwd=None, append=False):
  launcherDict={'args':args, 'shell':shell, 'cwd':cwd, 'file':file, 'append':append}
  return doLaunch("LAUNCH_WITH_STDOUT_AND_STDERR_FILE", launcherDict)

def launch_stdout_to_console(args, shell=False, cwd=None, append=False):
  launcherDict={'args':args, 'shell':shell, 'cwd':cwd, 'append':append}
  return doLaunch("LAUNCH_WITH_STDOUT_CONSOLE", launcherDict)

def launch_stdout_and_stderr_to_console(args, shell=False, cwd=None, append=False):
  launcherDict={'args':args, 'shell':shell, 'cwd':cwd, 'append':append}
  return doLaunch("LAUNCH_WITH_STDOUT_AND_STDERR_CONSOLE", launcherDict)
