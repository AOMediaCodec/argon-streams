################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Configuration information for an autoproduct run'''

import os
from typing import Callable, Dict, Iterable, List, Optional, Set, Tuple

import xml.etree.ElementTree as ET

from coveragestore import CoverageStore
from ap_macros import Macros

_ProfileDesc = Tuple[str, str, Optional[int], bool, bool]
_CStoreHandler = Callable[[CoverageStore, str, Macros, ET.Element], None]

class View:
    '''Configuration that applies to a single subset'''
    def __init__(self, view: str) -> None:
        self.view = view
        self.set_name = None  # type: Optional[str]
        self.enable_subset_merge = True
        self.encode_options = []  # type: List[str]
        self.decode_options = []  # type: List[str]


class Configuration:
    def __init__(self,
                 coverage_store: CoverageStore,
                 macros: Macros,
                 path: str,
                 never_merge: bool) -> None:
        # The underlying profile we're working for - set with the
        # <profile> tag.
        self._profile_name = None  # type: Optional[str]

        # The name of the codec
        self._codec = None  # type: Optional[str]

        # If never_merge is true, it's like all the profiles have
        # <nonMergeable/>.
        self.never_merge = never_merge

        # The maximum subset index that we've seen mentioned
        self.max_subset = 1

        # Here views[0] corresponds to the default (when we aren't in
        # a subset). (Conveniently, subsets start at 1).
        self.views = {}  # type: Dict[int, View]

        # A list of the profile elements we've seen so far
        self.profile_list = []  # type: List[_ProfileDesc]

        # A list of actions for failed checks
        self.checks = {}  # type: Dict[str, str]

        # A set of options to pass to the build tool.
        self.build_tool_options = set()  # type: Set[str]

        # Trees is a list of pairs: (path, tree) for loaded XML files
        # from deepest include upwards.
        trees = Configuration.load_tree(path)
        for _, tree in trees:
            self.load_macros(macros, tree)
        for path, tree in trees:
            self.read_config(coverage_store, macros, path, tree)

        for k in range(1, self.max_subset + 1):
            if k in self.views:
                continue

            if 0 not in self.views:
                raise RuntimeError("View not specified for subset {} "
                                   "and no default view specified."
                                   .format(k))

            self.views[k] = self.views[0]

        if self._profile_name is None:
            raise RuntimeError('No profile name specified')
        if self._codec is None:
            raise RuntimeError('No codec specified')

        self.profile_name = self._profile_name  # type: str
        self.codec = self._codec  # type: str

    @staticmethod
    def load_tree(path: str) -> List[Tuple[str, ET.Element]]:
        '''Parse the XML at path and recursively process any includes.'''
        tree = ET.parse(path).getroot()
        start_dir = os.path.dirname(path)
        trees = []  # type: List[Tuple[str, ET.Element]]
        for elem in tree.findall('include'):
            inc_txt = elem.text
            if not inc_txt:
                raise RuntimeError('Include element has no text content.')

            inc_path = os.path.normpath(os.path.join(start_dir, inc_txt))
            if not os.path.exists(inc_path):
                raise RuntimeError('The file `{}\' includes `{}\', which '
                                   'resolves to `{}\', which doesn\'t exist.'
                                   .format(path, inc_txt, inc_path))
            trees += Configuration.load_tree(inc_path)

        trees.append((path, tree))
        return trees

    def load_macros(self, macros: Macros, tree: ET.Element) -> None:
        '''Read all the macros from tree and set them in the macros object'''
        for elem in tree:
            if elem.tag == "defmacro":
                name = elem.get('name')
                repl = (elem.text or '').strip()

                if not name:
                    raise RuntimeError('No name attribute for macro')
                if not repl:
                    raise RuntimeError('No replacement text for macro {}.'
                                       .format(name))

                macros.set(name, repl)

    def read_name(self, macros: Macros, element: ET.Element) -> None:
        '''Set the main profile name based on element'''
        txt = macros.replace(element.text)
        if not txt:
            raise RuntimeError('Invalid profile name tag (no text)')

        self._profile_name = txt.strip()

    def read_codec(self, macros: Macros, element: ET.Element) -> None:
        '''Set the codec based on element'''
        txt = macros.replace(element.text)
        if not txt:
            raise RuntimeError('Invalid codec tag (no text)')

        self._codec = txt.strip()

    def read_view(self, macros: Macros, element: ET.Element) -> None:
        '''Read a <view> element'''
        try:
            subset_str = element.attrib.get('subset')
            if subset_str is not None:
                subset = int(subset_str)
                if subset <= 0:
                    raise ValueError()
            else:
                subset = 0
        except ValueError:
            raise RuntimeError('Subset `{}\' for view element is not a '
                               'positive integer.'
                               .format(element.attrib.get('subset', '???')))

        self.max_subset = max(self.max_subset, subset)

        if subset in self.views:
            raise RuntimeError('Duplicate view for subset {}.'.format(subset))

        txt = macros.replace(element.text)
        if not txt:
            raise RuntimeError('View element for subset {} has no text.'
                               .format(subset))

        view = View(txt.strip())
        for child in element:
            txt = macros.replace(child.text)
            child_txt = (txt.strip() if txt is not None else '')
            if child.tag == "encodeCommand":
                view.encode_options = child_txt.split()
            elif child.tag == "decodeCommand":
                view.decode_options = child_txt.split()
            elif child.tag == "setName":
                view.set_name = child_txt
            elif child.tag == "disableSubsetMerge":
                view.enable_subset_merge = False
            else:
                print("Unrecognised child of <view>: {}".format(child.tag))

        self.views[subset] = view

    def read_profile(self, macros: Macros, elem: ET.Element) -> None:
        name = macros.replace(elem.findtext("name"))
        if not name:
            raise RuntimeError('Profile tag has no name.')

        funs = macros.replace(elem.findtext("functions")) or name

        thread_limit = None
        tl_txt = macros.replace(elem.findtext('threadLimit'))
        if tl_txt is not None:
            try:
                thread_limit = int(tl_txt)
            except ValueError:
                raise RuntimeError('Profile tag with name {} has a '
                                   'thread limit of `{}\', not an integer.'
                                   .format(name, tl_txt))

        uses_choose_all = elem.find('usesChooseAll') is not None
        non_mergeable = elem.find('nonMergeable') is not None

        self.profile_list.append((name, funs, thread_limit,
                                  uses_choose_all, non_mergeable))

    def read_check(self, macros: Macros, elem: ET.Element) -> None:
        tag = macros.replace(elem.findtext('tag'))
        if not tag:
            raise RuntimeError('Check element with no tag.')

        what_check = None
        for action in ['reject', 'ignore', 'error']:
            if elem.find(action) is not None:
                if what_check is not None:
                    raise RuntimeError('Multiple actions specified '
                                       'for check with tag `{}\'.'
                                       .format(tag))
                what_check = action
        if what_check is None:
            raise RuntimeError('No valid action specified for check '
                               'with tag `{}\'.'
                               .format(tag))

        if what_check == 'error':
            if tag in self.checks:
                del self.checks[tag]
        else:
            self.checks[tag] = what_check

    def read_build_tool_option(self, macros: Macros, elem: ET.Element) -> None:
        option = macros.replace(elem.text)
        if not option:
            raise RuntimeError('Empty build tool option.')

        if option[0] == '-':
            self.build_tool_options.discard(option[1:])
        else:
            self.build_tool_options.add(option)

    def read_config(self,
                    coverage_store: CoverageStore,
                    macros: Macros,
                    path: str,
                    tree: ET.Element) -> None:
        '''Read and act upon the nodes of tree as a config file'''
        local_handlers = {
            'name': Configuration.read_name,
            'codec': Configuration.read_codec,
            'view': Configuration.read_view
        }

        cstore_handlers = {
            'branch': (lambda cs, path, macros, elem:
                       cs.store_branch(path, macros, elem)),
            'range': CoverageStore.store_range,
            'cross': CoverageStore.store_cross
        }  # type: Dict[str, _CStoreHandler]

        for elem in tree:
            lhandler = local_handlers.get(elem.tag)
            if lhandler is not None:
                lhandler(self, macros, elem)
                continue

            cshandler = cstore_handlers.get(elem.tag)
            if cshandler is not None:
                cshandler(coverage_store, path, macros, elem)
                continue

            # The other tags aren't so uniform. Ho hum.
            if elem.tag == 'filter':
                # The bodies of <filter> tags are ignored. We just
                # care about attributes 'type', 'pattern' and 'action'
                if 'type' not in elem.attrib or 'pattern' not in elem.attrib:
                    raise RuntimeError('Filter element doesn\'t contain both '
                                       '"type" and "pattern" attributes.')
                coverage_store.add_filter(elem.attrib['type'],
                                          elem.attrib['pattern'],
                                          elem.attrib.get('action', 'require'),
                                          elem.attrib.get('values', None))
            elif elem.tag == "profile":
                self.read_profile(macros, elem)
            elif elem.tag == "check":
                self.read_check(macros, elem)
            elif elem.tag == "buildtoolOption":
                self.read_build_tool_option(macros, elem)
            elif elem.tag in ['defmacro', 'include',
                              'coverageReportTransform']:
                pass
            else:
                RuntimeError("Unrecognised element: `{}'".format(elem.tag))

    def subsets(self) -> Iterable[int]:
        '''Return the range of subsets defined'''
        return range(1, self.max_subset + 1)

    def all_set_names(self) -> List[str]:
        '''Return the set names for all subsets'''
        return [view.set_name
                for view in self.views.values()
                if view.set_name is not None]

    def get_set_name(self, subset: int) -> str:
        '''Return the set name for a given subset'''
        view = self.views.get(subset)
        if view is None or view.set_name is None:
            view = self.views.get(0)

        if view is None:
            raise RuntimeError('Set 0 doesn\'t have an associated view.')
        if view.set_name is None:
            raise RuntimeError('Set 0 doesn\t have an associated set name.')

        return view.set_name
