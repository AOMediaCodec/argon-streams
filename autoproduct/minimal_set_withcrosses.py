################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Find a minimal set of streams generating the given coverage.'''

import sys
import importlib
import os
import argparse
from contextlib import contextmanager
import re
import shutil
import subprocess
import tempfile


def parse_args():
    '''Parse command line arguments'''
    parser = argparse.ArgumentParser()
    parser.add_argument('--force', '-f', action='append', default=[],
                        help=('Streams from this directory should always be '
                              'in the minimal set.'))
    parser.add_argument('--extension', '-t', required=True,
                        help=('The extension of the stream files; this is '
                              'used to find the file size'))
    parser.add_argument('--output', '-o', default='minimalset.txt',
                        help='output file')
    parser.add_argument('--filter',
                        dest='filter_file',
                        help=('File with a list of the points of interest. '
                              'Other coverage points will be ignored. The '
                              'file should list branches as "B <name>\n" '
                              'and crosses as "C <name> 1,2,8,9\n" (where the '
                              'list is the bits that need covering.'))
    parser.add_argument('--quiet', '-q', action='store_true',
                        help='don\'t produce verbose output, just progress')
    parser.add_argument('dirs', nargs='+')

    return parser.parse_args()


def coverpy_paths(root):
    '''Return a list of paths to coverage files in root (.py or .py.gz)'''
    ret = []
    dirname = os.path.join(root, 'coverpy')
    for name in os.listdir(dirname):
        if name.startswith('__'):
            continue
        if name.endswith('.py.gz') or name.endswith('.py'):
            ret.append(os.path.join(dirname, name))
    return ret


@contextmanager
def maybe_zipped_cm(path):
    '''A context manager that unzips a file if necessary.

    The yielded path will have the same basename as path, without any
    trailing .gz.

    '''
    if not path.endswith('.gz'):
        yield path
        return

    tmpdir = tempfile.mkdtemp()
    try:
        dst_path = os.path.join(tmpdir, os.path.basename(path)[:-3])
        with open(dst_path, 'wb') as dst_fd:
            subprocess.check_call(['gunzip', '-c', path], stdout=dst_fd)
        yield dst_path

    finally:
        shutil.rmtree(tmpdir)


def load_module(path):
    '''Load data from path, a possibly zipped python file'''
    with maybe_zipped_cm(path) as pyfile:
        mod_name = os.path.basename(pyfile)[:-3]
        assert pyfile.endswith('.py')
        assert mod_name not in sys.modules

        orig_sys_path = sys.path
        try:
            sys.path = [os.path.dirname(pyfile)]
            module_data = importlib.import_module(mod_name)
            del sys.modules[mod_name]
        finally:
            sys.path = orig_sys_path

        pyc_file = pyfile + 'c'
        try:
            os.remove(pyc_file)
        except OSError:
            if not os.path.exists(pyc_file):
                raise

    return module_data


class Progress:
    '''A class to track progress through the 3-phase run'''

    def __init__(self, quiet, dirs):
        self.quiet = quiet
        self.phase = 1
        self.progress = 0
        self.all_count = 0
        self.all_total = sum(len(coverpy_paths(d)) for d in dirs)

        self.started = False
        self.dots_so_far = 0 if quiet and not sys.stdout.isatty() else None

    def step(self, inc=1):
        '''Increment progress (max: 300)'''
        self.progress += inc
        self.all_count += inc

    def phase_done(self):
        '''Increment the phase counter and reset all_count'''
        self.phase += 1
        self.all_count = 0

    def report(self, what, sub_idx=None, sub_len=1):
        '''Report current progress to stdout'''
        if self.dots_so_far is not None:
            # We are in quiet mode and writing to something other than a
            # terminal. Write:
            #
            # |<--- Progress --->|
            # .......
            if not self.started:
                # Draw the first line, which should be an "80 character
                # progress bar"
                hyphens = '-' * 33
                sys.stdout.write('|<' + hyphens +
                                 ' Progress ' + hyphens + '>|\n')
                sys.stdout.flush()
                self.started = True

            dots = int((80.0 * self.progress) / (self.all_total * 3))
            if dots > self.dots_so_far:
                sys.stdout.write('.' * (dots - self.dots_so_far))
                sys.stdout.flush()
                self.dots_so_far = dots
        elif self.quiet:
            # We're in quiet mode, writing to a terminal.
            percent = (100.0 * self.progress) / (self.all_total * 3)
            sys.stdout.write("Progress: {:.1f}%    \r".format(percent))
            sys.stdout.flush()
        else:
            # We're in verbose mode.
            rest = ''
            if sub_idx is not None:
                rest = (' [dir: {}/{} {:.1f}% all: {}/{} {:.1f}%]'
                        .format(1 + sub_idx, sub_len,
                                (100.0 * sub_idx) / sub_len,
                                self.all_count, self.all_total,
                                (100.0*self.all_count)/self.all_total))

            print("Pass {}: {}{}".format(self.phase, what, rest))

    def done(self):
        '''Print something to show we're done'''
        if self.quiet:
            msg = ('\nDone\n'
                   if self.dots_so_far is not None
                   else "Done.                               \n")
            sys.stdout.write(msg)
            sys.stdout.flush()


def branch_ranges(cover_data, point_filter):
    '''Yield seq of (name, (min, max)) triples for branches in cover_data'''
    branch_data = cover_data.t
    for branch_name in branch_data:
        if not point_filter.allows_branch(branch_name):
            continue

        (actual_min, actual_max, _, _, _, _) = branch_data[branch_name]
        if actual_min is None or actual_max is None:
            continue

        yield (branch_name, (actual_min, actual_max))


def gz_duplicates(directory):
    '''Return all A such that directory contains coverpy/A and coverpy/A.gz'''
    seen = set()
    bad = []
    for path in coverpy_paths(directory):
        if path.endswith('.gz'):
            path = path[:-3]
        if path in seen:
            bad.append(path)
        seen.add(path)

    return bad


class PointsMap:
    '''A map from point name to the range of values that we have seen

    This also keeps a counter-based dictionary from point name to a
    unique index.

    '''
    def __init__(self):
        self.range_map = {}
        self.hasher = {}

    def extend_range(self, point, rng):
        '''Extend (or set) the range for key'''
        hval = self.get_hash(point)
        minimum, maximum = rng
        min0, max0 = self.range_map.setdefault(hval, rng)
        if minimum < min0 or max0 < maximum:
            self.range_map[hval] = (min(minimum, min0), max(maximum, max0))

    def get_hash(self, point):
        '''Return the hash (counter value) for a point name'''
        return self.hasher.setdefault(point, len(self.hasher))

    def add_directory(self, dir_name, progress, point_filter):
        '''Add streams from a directory to the map'''
        stream_paths = coverpy_paths(dir_name)
        for idx, stream_path in enumerate(stream_paths):
            progress.report('processing {}'.format(stream_path),
                            idx, len(stream_paths))
            progress.step()

            cover_data = load_module(stream_path)
            for (point, rng) in branch_ranges(cover_data, point_filter):
                self.extend_range(point, rng)


class Points:
    '''An object representing the points hit by a stream'''

    def __init__(self):
        self.min_points = set()
        self.max_points = set()
        self.points = set()
        self.cross_points = set()

    def stream_score(self, covered, file_size):
        '''Calculate a score for a stream as points hit / file size'''
        npoints = 0
        npoints += len(self.min_points - covered.min_points)
        npoints += len(self.max_points - covered.max_points)
        npoints += len(self.points - covered.points)
        npoints += len(self.cross_points - covered.cross_points)

        # Add a byte to avoid dividing by zero.
        return npoints / (file_size + 1.0)

    def add(self, points):
        '''Add the given points to those for this stream'''
        self.min_points |= points.min_points
        self.max_points |= points.max_points
        self.points |= points.points
        self.cross_points |= points.cross_points

    def add_min_max(self, val_range, g_range, point_hash, first_points):
        '''Maybe add val to min_points/max_points

        The value gets added if it attains the global min/max in
        g_range, so long as it doesn't already appear in
        first_points.

        '''
        min_val, max_val = val_range
        gmin, gmax = g_range
        if min_val == gmin and point_hash not in first_points.min_points:
            self.min_points.add(point_hash)

        if max_val == gmax and point_hash not in first_points.max_points:
            self.max_points.add(point_hash)


def stripext(path):
    '''Strip any .py or .py.gz from path'''
    path = path.lower()
    if path.endswith(".gz"):
        path = path[:-3]

    assert path.endswith('.py')
    return path[:-3]


class StreamPoints:
    '''A map from stream to a corresponding Points object'''

    def __init__(self, pmap):
        # stream_points has elements of the form (k, points) where k
        # is (dir, stream) for some dir and stream.
        self.stream_points = []
        self.pmap = pmap

    def add_directory(self, dir_name, progress, point_filter):
        '''Add the points for the streams in dir_name'''

        stream_paths = coverpy_paths(dir_name)
        quiet = progress.quiet
        for idx, stream_path in enumerate(stream_paths):
            stream = stripext(os.path.basename(stream_path))
            progress.report('processing {}/{}'.format(dir_name, stream),
                            idx, len(stream_paths))
            progress.step()

            key = (dir_name, stream)
            self.stream_points.append((key,
                                       self.read_points(stream_path,
                                                        quiet, point_filter)))

    @staticmethod
    def bits(bitfield):
        '''Return the list of indices set in the bitfield'''
        ret = []
        bit = 0
        while bitfield:
            if bitfield & 1:
                ret.append(bit)
            bit += 1
            bitfield >>= 1

        return ret

    def read_points(self, stream_path, quiet, point_filter):
        '''Calculate the Points object for a stream'''

        # first_points is the first Points object we read or we set it
        # to an empty Points object if this is the first one (it's
        # used to answer questions like "Has X been seen already in
        # the first stream?")
        first_points = (Points() if not self.stream_points
                        else self.stream_points[0][1])

        points = Points()
        stream_data = load_module(stream_path)

        for (point, val_range) in branch_ranges(stream_data, point_filter):
            hval = self.pmap.get_hash(point)
            g_range = self.pmap.range_map[hval]

            points.add_min_max(val_range, g_range, hval, first_points)

        for cross in stream_data.c:
            raw_bits = set(StreamPoints.bits(stream_data.c[cross][0]))
            filt_bits = point_filter.filter_cross_bits(cross, raw_bits)
            if not filt_bits:
                continue
            hashes = set(self.pmap.get_hash("{}     [{}]".format(cross, bit))
                         for bit in filt_bits)
            points.cross_points.update(hashes - first_points.cross_points)

        if not quiet:
            print("Points covered: {} min {} max {} range {} cross"
                  .format(len(points.min_points), len(points.max_points),
                          len(points.points), len(points.cross_points)))

        return points


def group_by_first(by_pairs):
    '''Take a list of the form ((a, b), c) and group by a'''
    a_dict = {}
    for ((a_val, b_val), c_val) in by_pairs:
        a_dict.setdefault(a_val, []).append((b_val, c_val))

    # Rather than just "list(a_dict.items())", we do this to make sure
    # the order doesn't depend on the vagaries of Python's hash
    # function
    ret = []
    for ((a_val, _), _) in by_pairs:
        bc_vals = a_dict.pop(a_val, None)
        if bc_vals:
            ret.append((a_val, bc_vals))

    return ret


def split_forced(stream_points, forced_dirs):
    '''Split out forced streams from stream_points'''
    forced = []
    unforced = []
    for dir_name, sp_for_dir in group_by_first(stream_points):
        if dir_name in forced_dirs:
            forced.append((dir_name, sp_for_dir))
        else:
            for stream, points in sp_for_dir:
                unforced.append(((dir_name, stream), points))

    return forced, unforced


def get_stream_sizes(stream_points, extension):
    '''Build a dictionary from key (dir, stream) to stream size'''
    stream_sizes = {}
    for key, _ in stream_points:
        dir_name, stream = key
        path = os.path.join(dir_name, "streams",
                            "{}.{}".format(stream, extension))
        stream_sizes[key] = os.path.getsize(path)

    return stream_sizes


def sift_streams(stream_points, stream_sizes, covered):
    '''Sift out the best remaining stream in stream_points'''
    best = None
    best_score = 0
    next_gen = []

    for key, points in stream_points:
        score = points.stream_score(covered, stream_sizes[key])
        if score > best_score:
            # This is the best stream we've seen so far. If there was a
            # previous "best" stream, it's been knocked off the podium, but we
            # need to make sure we don't lose it, so add it to next_gen. value.
            if best is not None:
                next_gen.append(best)
            best = (key, points)
            best_score = score
        elif score > 0:
            # Meh. But we might need it in a later round.
            next_gen.append((key, points))

    # next_gen should always bet shorter than stream_points (we should
    # always make forward progress)
    assert len(next_gen) < len(stream_points)

    return (best, next_gen)


def get_minimal_set(stream_points, progress, forced_dirs, extension):
    '''Calculate a minimal set of streams

    The stream_points argument is a list with elements of the form
    (key, points) where key is (dir, stream).

    '''
    # Group the streams we know about by directory and then split into
    # the --force directories and the rest.
    forced, stream_points = split_forced(stream_points, forced_dirs)

    minimal_set = []
    covered = Points()

    # Work through the forced directories, adding the contents and
    # keeping track of the coverage we get.
    for dir_name, sp_for_dir in forced:
        for stream, points in sp_for_dir:
            minimal_set.append((dir_name, stream))
            covered.add(points)

    progress.step(len(minimal_set))
    progress.report('added {} forced streams'.format(len(minimal_set)))

    stream_sizes = get_stream_sizes(stream_points, extension)

    # Now we're on the rest of the streams (which aren't grouped by
    # directory). We walk through the remaining list multiple times,
    # throwing out streams with no new coverage, and picking the best
    # stream that does have coverage. "Best" here means maximising
    # num_new_points_covered / file_size.
    while stream_points:
        progress.report('{} streams remaining'.format(len(stream_points)))
        best, next_gen = sift_streams(stream_points, stream_sizes, covered)

        if best is not None:
            key, points = best
            minimal_set.append(key)
            covered.add(points)

            if not progress.quiet:
                print('Added {}\n'
                      '  New points: min {}; max {}; rng {}; crx {}'
                      .format(best[0],
                              len(best[1].min_points),
                              len(best[1].max_points),
                              len(best[1].points),
                              len(best[1].cross_points)))

        progress.step(len(stream_points) - len(next_gen))
        stream_points = next_gen

    return minimal_set


def tee(handle, quiet, msg):
    '''Write a string to handle and stdout too if not quiet'''
    if not quiet:
        print(msg)
    handle.write(msg + "\n")


class Filter:
    '''A filter for coverage points'''

    def __init__(self):
        self.branches = set()
        self.crosses = {}
        self.allow_all = True

    def load(self, path):
        '''Load a filter file at path'''
        self.allow_all = False
        with open(path, 'r') as handle:
            for idx, line in enumerate(handle):
                self.read_input_line('{}:{}'.format(path, idx + 1), line)

    def read_input_line(self, pos_desc, line):
        '''Read a single line of a filter file'''
        line = line.strip()
        if not line:
            return

        match = re.match(r'(.)\s+(.*)$', line)
        if not match:
            raise RuntimeError('Error at {}: '
                               'Line does not start with isolated char.'
                               .format(pos_desc))

        filter_char = match.group(1)
        filter_args = match.group(2)

        # The filter type should be B or C (for branch and cross, respectively)
        if filter_char == 'B':
            self.read_branch(filter_args)
        elif filter_char == 'C':
            self.read_cross(pos_desc, filter_args)
        else:
            raise RuntimeError('Error at {}: '
                               'Filter char of {} is not B or C.'
                               .format(pos_desc, filter_char))

    def read_branch(self, args):
        '''Read the rest of a B line'''
        self.branches.add(args)

    def read_cross(self, pos_desc, args):
        '''Read the rest of a C line'''
        # args should be of the form "NAME 2,3,4" or similar.
        match = re.match(r'([^\s]+)\s*([0-9]+(:?,\s*[0-9]+)*)', args)
        if not match:
            raise RuntimeError('Error at {}: Ill-formed cross line.'
                               .format(pos_desc))
        name = match.group(1)
        bits = set(int(part) for part in match.group(2).split(','))

        if name in self.crosses:
            raise RuntimeError('Error at {}: Duplicate cross line for {}.'
                               .format(pos_desc, name))

        self.crosses[name] = bits

    def allows_branches(self):
        '''Return true if any branches can make it through the filter'''
        return self.allow_all or len(self.branches) > 0

    def allows_branch(self, branch_name):
        '''Return true if this branch passes through the filter'''
        return self.allow_all or branch_name in self.branches

    def filter_cross_bits(self, cross_name, bits):
        '''Return a filtered list of bits for the given cross'''
        if self.allow_all:
            return bits

        return self.crosses.get(cross_name, set()) & bits


def main():
    '''Main entry point'''
    args = parse_args()
    dirs = args.force + args.dirs

    progress = Progress(args.quiet, dirs)

    duplicates = [c for d in dirs for c in gz_duplicates(d)]
    if duplicates:
        raise RuntimeError("Name collisions: both A and A.gz exist for "
                           "A in {}"
                           .format(' '.join(duplicates)))

    point_filter = Filter()
    if args.filter_file is not None:
        point_filter.load(args.filter_file)

    pmap = PointsMap()
    if point_filter.allows_branches():
        for dir_name in dirs:
            pmap.add_directory(dir_name, progress, point_filter)
    else:
        progress.step(progress.all_total)

    progress.phase_done()

    stream_points = StreamPoints(pmap)
    for dir_name in dirs:
        stream_points.add_directory(dir_name, progress, point_filter)

    progress.phase_done()

    minimal_set = get_minimal_set(stream_points.stream_points,
                                  progress, args.force, args.extension)

    msg = ("Minimal set:\n" +
           '\n'.join([repr(k) for k in minimal_set]) + '\n' +
           ("Minimal set contains {} streams; rejected {} streams"
            .format(len(minimal_set),
                    len(stream_points.stream_points) - len(minimal_set))))
    if not args.quiet:
        print(msg)

    with open(args.output, "w") as output:
        output.write(msg + '\n')

    progress.done()

    return 0


if __name__ == '__main__':
    exit(main())
