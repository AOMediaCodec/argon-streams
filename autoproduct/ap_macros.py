################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A macro substitution helper'''

from typing import Dict, Optional, Tuple


class Macros(object):
    '''A table of macro substitutions'''
    def __init__(self) -> None:
        self.table = {}  # type: Dict[str, Tuple[str, int]]

    def set(self, name: str, value: str, level: int = 0) -> None:
        '''Register a substitution

        The level parameter specifies the priority of the definition. If a
        definition is registered at level A, a later definition at level B will
        supersede the first if B > A, be ignored if A < B and cause an error if
        they are the same level.

        '''
        prev = self.table.get(name)
        if prev is None:
            self.table[name] = (value, level)
            return

        old_name, old_level = prev
        if level < old_level:
            return

        if old_level < level:
            self.table[name] = (value, level)
            return

        # Hmm. We have two definitions at the same level.
        if name != old_name:
            raise RuntimeError('Cannot override existing value for '
                               '{} in table: {!r} -> {!r} (both definitions '
                               'are at level {}).'
                               .format(name, self.table[name], value, level))

    def replace(self, string: Optional[str]) -> Optional[str]:
        '''Apply substitutions in string'''
        initial = string

        if string is None:
            return string

        while True:
            i = string.find("${")
            if i < 0:
                break
            j = string.find("}")

            if j <= i:
                raise RuntimeError('Mismatched or unterminated expansion '
                                   'in string. Initial string was {!r}; now '
                                   'we have {!r}.'
                                   .format(initial, string))

            macro_name = string[i+2:j]
            pair = self.table.get(macro_name)
            if pair is None:
                raise RuntimeError('Macro ${{{}}} not defined. '
                                   'Initial string was {!r}; now we have {!r}.'
                                   .format(macro_name, initial, string))

            value = pair[0]
            string = "{}{}{}".format(string[:i], value, string[j+1:])

        return string
