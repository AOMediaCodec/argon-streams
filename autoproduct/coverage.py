################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A coverage engine for autoproduct'''

import os
import re
import shutil
import subprocess
import tempfile
from typing import Dict, IO, Iterable, List, Optional

from coveragestore import CoverageStore
from branch_coverage import BranchCoverage
from cross_coverage import CrossCoverage, show_bitfield
from range_coverage import RangeCoverage
from configuration import Configuration
from buildtools import progress, rm_f, zip_up
from stream_dir import StreamDir


def load_module(contents: str) -> Dict[str, object]:
    '''Use exec to load the given Python code into a module'''

    # Run the statements in the file, which should define stuff in the global
    # dictionary. We're passing that dictionary in as 'ret'.
    ret = {}  # type: Dict[str, object]
    exec(contents, ret)

    # Delete the __builtins__ entry from ret: there's no reason to keep it
    # around and it just pollutes stuff.
    del ret['__builtins__']

    return ret


def read_module(path: str) -> Dict[str, object]:
    '''Use exec to load the contents of the module at path'''
    with open(path, 'r') as handle:
        contents = handle.read()

    return load_module(contents)


def unzip_and_load(path: str) -> Dict[str, object]:
    '''Load the file at <path> after unzipping with gzip'''
    if not os.path.exists(path):
        raise RuntimeError('GZipped module at `{}\' does not exist.'
                           .format(path))

    with tempfile.TemporaryFile(mode='w+') as tmp:
        cmd = ['gunzip', '-c', path]
        subprocess.check_call(cmd, stdout=tmp)
        tmp.flush()
        tmp.seek(0)
        contents = tmp.read()

    return load_module(contents)


def all_coverage_modules(stream_dirs: Dict[str, StreamDir],
                         subsets: Iterable[int]) -> List[str]:
    '''Return all the coverage modules across all stream dirs'''
    ret = []  # type: List[str]
    for k in subsets:
        ret += stream_dirs['subset{}'.format(k)].coverage_modules()
    return ret + stream_dirs['core'].coverage_modules()


class Coverage():
    def __init__(self, cstore: CoverageStore, profile_name: str) -> None:
        self.cstore = cstore
        self.branch_coverage = BranchCoverage(self.cstore)
        self.range_coverage = RangeCoverage(self.cstore, self.branch_coverage)
        self.cross_coverage = CrossCoverage(self.cstore)
        self.processed = []  # type: List[str]

        self.current_holes_file = os.path.join(profile_name,
                                               'current-holes.txt')

    def add_expected(self,
                     branches: Optional[str],
                     crosses: Optional[str]) -> None:
        if branches is not None:
            self.branch_coverage.add_expected(branches)
        if crosses is not None:
            self.cross_coverage.read_cross_list(crosses)

    @staticmethod
    def _cget(coverage: Dict[str, object], key: str) -> object:
        val = coverage.get(key)
        if val is None:
            raise RuntimeError('Loaded coverage doesn\'t contain '
                               'a field called `{}\'.'
                               .format(key))
        return val

    def update(self,
               config: Configuration,
               coverage: Dict[str, object],
               subset: int,
               mod_name: str) -> None:
        '''Update with coverage results from a file'''
        range_visibility_types = {}
        visible_categories = {}  # type: Dict[int, Dict[str, bool]]

        cov_v = Coverage._cget(coverage, 'v')
        if not isinstance(cov_v, dict):
            raise RuntimeError('Loaded coverage\'s v field isn\'t a dict.')

        # vc is supposed to be a list of the names of view classes: these
        # appear as possible entries in the invisible list.
        cov_vc = Coverage._cget(coverage, 'vc')
        if not isinstance(cov_vc, list):
            raise RuntimeError('Loaded coverage\'s vc field isn\'t a list.')
        for vc in cov_vc:
            if not isinstance(vc, str):
                raise RuntimeError('Loaded coverage\'s vc field contains '
                                   'the element {!r}, which is not a string.'
                                   .format(vc))

        for k in config.subsets():
            view_name = config.views[k].view
            v_of_view_name = cov_v.get(view_name)
            if v_of_view_name is None:
                raise RuntimeError('Subset {} has a configured view name of '
                                   '`{}\', but this doesn\'t appear in the '
                                   'dumped coverage (known keys: {}).'
                                   .format(k, view_name,
                                           list(cov_v.keys())))  # type: ignore
            if not (isinstance(v_of_view_name, tuple) and
                    len(v_of_view_name) == 2):
                raise RuntimeError('Loaded coverage\'s v field has an entry '
                                   'for view {} of {!r}, which is not a tuple '
                                   'of length 2.'
                                   .format(view_name, v_of_view_name))

            invisible, rvt = v_of_view_name

            # invisible should be a list of strings. These are the names of
            # coverage classes that are invisible for this view.
            if not isinstance(invisible, list):
                raise RuntimeError('Loaded coverage\'s v field has an entry '
                                   'for view {} whose invisible list is '
                                   '{!r}, which is not a list.'
                                   .format(view_name, invisible))

            # rvt should be a string: it gives the range visibility type for
            # this view.
            if not isinstance(rvt, str):
                raise RuntimeError('Loaded coverage\'s v field has an entry '
                                   'for view {} of with range visibility type '
                                   '{!r}, which is not a string.'
                                   .format(view_name, rvt))

            range_visibility_types[k] = rvt
            visible_categories[k] = {x: x not in invisible for x in cov_vc}

        # The 't' field is supposed to be the dictionary of branch coverage
        # points, keyed by name and with values telling us what got hit. Since
        # the list is enormous, we won't check it's well formed here. We'll
        # just check it's a dict and do more checking downstream.
        cov_t = Coverage._cget(coverage, 't')
        if not isinstance(cov_t, dict):
            raise RuntimeError('Loaded coverage\'s t field is not a dict.')

        self.branch_coverage.update(visible_categories,
                                    cov_t, config.max_subset)

        self.range_coverage.update(range_visibility_types, subset, cov_t)

        view_name = config.views[subset].view

        # Now go through the crosses
        #
        # The 'c' field is supposed to be a dict keyed by cross point name
        # whose values are tuples (see cross_coverage class). Again, we'll just
        # check it's some sort of dict and push the rest of the checking on to
        # the cross_coverage class.
        cov_c = Coverage._cget(coverage, 'c')
        if not isinstance(cov_c, dict):
            raise RuntimeError('Loaded coverage\'s c field is not a dict.')

        self.cross_coverage.update(view_name, subset, config, cov_c)

        # Write the current total coverage out so we can reload it quickly
        self.processed.append(mod_name)

        with tempfile.NamedTemporaryFile(mode='w') as tmp:
            self._dump(tmp)
            tmp.flush()
            shutil.copyfile(tmp.name,
                            os.path.join(config.profile_name,
                                         "all_coverage.py"))

    def write_holes(self, subset: int) -> None:
        '''Write the current list of holes to current_holes_file'''
        b_holes = self.branch_coverage.branch_holes(subset)
        r_holes = self.range_coverage.holes(subset)
        c_holes = self.cross_coverage.holes(subset)
        with open(self.current_holes_file, "w") as f:
            f.write("CURRENT HOLES - SUBSET {}\n\n".format(subset))
            if b_holes:
                f.write("BRANCH HOLES:\n")
                for bh in b_holes:
                    f.write("  {}\n".format(bh))
                f.write("\n")

            if r_holes:
                f.write("RANGE HOLES:\n")
                for eqn_name, (seen, expected) in r_holes:
                    f.write("  {} theoretical: {} actual: {}\n"
                            .format(eqn_name, expected, seen))
                f.write("\n")

            if c_holes:
                f.write("CROSS HOLES:\n")
                for name, missing in c_holes:
                    f.write("  {} {}\n".format(name,
                                               show_bitfield(missing)))
                f.write("\n")

    def list_points(self, path: str) -> None:
        '''Write a list of all interesting points to path'''
        with open(path, 'w') as out:
            self.branch_coverage.list_points(out)
            self.cross_coverage.list_coverage(out)

    def _dump(self, stream: IO[str]) -> None:
        self.branch_coverage.dump(stream)
        self.range_coverage.dump(stream)
        self.cross_coverage.dump(stream)
        stream.write('processed={!r}\n'.format(self.processed))

    def _dump_cumulative(self,
                         profile_name: str,
                         cov_mod: str) -> None:
        '''Dump cumulative coverage data'''
        # cov_mod looks something like "subset1.coverpy.test1_2_3".
        # We should get a max_seed of 3 and need to write
        # cumulative coverage to
        # "$profile_name/subset1/cumulative_cover/test3.py".
        max_seed = StreamDir.coverage_module_max_seed(cov_mod)
        src_file = os.path.join(profile_name, 'all_coverage.py')
        dest_file = os.path.join(profile_name,
                                 os.sep.join(cov_mod.split('.')[:-2]),
                                 'cumulative_cover',
                                 'test{}.py'.format(max_seed))
        shutil.copyfile(src_file, dest_file)
        rm_f(dest_file + '.gz')
        zip_up(dest_file)

    def _load(self, module: Dict[str, object]) -> None:
        self.branch_coverage.load(module)
        self.range_coverage.load(module)
        self.cross_coverage.load(module)

        processed = module.get('processed')
        if not isinstance(processed, list):
            raise RuntimeError('Loading module but processed '
                               'field is not a list.')
        self.processed = []
        for name in processed:
            if not isinstance(name, str):
                raise RuntimeError('Processed field from loaded module '
                                   'contains {!r}, which is not a string.'
                                   .format(name))
            self.processed.append(name)

    def _load_cumulative(self,
                         path: str,
                         cov_mods: List[str],
                         gzipped: bool) -> List[str]:
        '''Load up dumped cumulative coverage from a python file

        Return the subset of modules from cov_mods that aren't listed in the
        loaded cumulative data.

        '''
        # Load up the module so far
        self._load(unzip_and_load(path) if gzipped else read_module(path))

        # There might still be some new coverage modules that we haven't yet
        # loaded. Return the list of what's still to do. We assume that any
        # cumulative coverage modules in the list will be older than the one we
        # just loaded, so strip them out.
        #
        # If there aren't any coverage modules left to load, we try to plonk
        # one item back in because otherwise we won't have populated the branch
        # coverage table properly and we'll end up making some streams for no
        # good reason.

        to_load = []
        first_non_cumul = None

        for cov_mod in cov_mods:
            if 'cumulative_cover' in cov_mod:
                continue

            if first_non_cumul is None:
                first_non_cumul = cov_mod

            if cov_mod not in self.processed:
                to_load.append(cov_mod)

        if first_non_cumul is not None and not to_load:
            to_load = [first_non_cumul]

        return to_load

    def load_previous(self,
                      config: Configuration,
                      stream_dirs: Dict[str, StreamDir]) -> None:
        # What coverage modules exist? Get a list by looking through the
        # filesystem stuff like "$profile_name/subset2/coverpy/test234.py.gz".
        # The elements of the returned list look like "subset2.coverpy.test234"
        # (and correspond to module names that we can pass to importlib).
        #
        # This will list both "coverpy"-style modules (that describe a
        # particular stream) and also "cumulative_cover" modules, which give
        # coverage up to a particular stream.
        to_read = all_coverage_modules(stream_dirs, config.subsets())

        profile_name = config.profile_name
        assert profile_name

        # Start by trying to load up the all_coverage.py file. This file will
        # have been deleted if we did a rewind, but otherwise behaves like a
        # cumulative coverage file up to the "current point", whatever that is.
        all_cov_path = os.path.join(profile_name, "all_coverage.py")
        if os.path.exists(all_cov_path):
            if not to_read:
                raise RuntimeError("No coverage files, but there is {}. "
                                   "To start again, delete all_coverage.py "
                                   "as well."
                                   .format(all_cov_path))

            # Load up cumulative coverage from all_coverage.py. This reads
            # coverage information and trims down our list of modules to read.
            to_read = self._load_cumulative(all_cov_path, to_read, False)

        # Are there are any cumulative modules in to_read? If so, pick the
        # latest one and load that.
        cum_to_read = [x for x in to_read if 'cumulative_cover' in x]
        if cum_to_read:
            # Sorting cum_to_read in descending order of
            # coverage_module_max_seed should mean we see the cumulative module
            # with the highest seed as the first item.
            cum_to_read.sort(key=StreamDir.coverage_module_max_seed,
                             reverse=True)
            ctr_path = cum_to_read[0].replace('.', '/')
            path = os.path.join(profile_name, ctr_path + '.py.gz')
            to_read = self._load_cumulative(path, to_read, True)

        # We're out of plausible cumulative modules. Any remaining entries in
        # to_read should be for single streams. Read them all. We read them in
        # ascending order of maximum seed and dump a cumulative coverage file
        # every so often.
        if to_read:
            to_read.sort(key=StreamDir.coverage_module_max_seed)
            num_mods = len(to_read)
            period = 10

            progress('Loading {} streams\' coverage data...'.format(num_mods))
            for idx, cov_mod in enumerate(to_read):
                # Write out a progress message
                pc_done = (100.0 * idx) / num_mods
                basename = cov_mod.rsplit('.', 1)[-1]
                progress("{}/{} {:.1f}% {}"
                         .format(idx, num_mods, pc_done, basename))

                assert 'cumulative_cover' not in cov_mod

                # Which subset does this file correspond to? We need this in
                # order to figure out visibility.
                #
                # If there's no subset index, that's fine: we'll just treat it
                # as coming from subset 1. This happens when we're loading
                # stuff from a 'core' profile.
                match = re.match(r'subset([0-9]+)\.', cov_mod)
                subset = int(match.group(1)) if match is not None else 1
                path = os.path.join(profile_name,
                                    cov_mod.replace('.', '/') + '.py.gz')

                # Read the module
                self.update(config,
                            unzip_and_load(path),
                            subset,
                            cov_mod)

                # We dump cumulative cover after every period streams and also
                # on the last stream. The cumulative cover is already saved in
                # all_coverage.py (that was done by loadCoverage), so we can
                # just copy that across.
                if (idx % period == period - 1) or (idx == num_mods - 1):
                    self._dump_cumulative(profile_name, cov_mod)

            progress('Done.' + (' ' * 50))
