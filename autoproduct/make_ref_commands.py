#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A module to generate the reference command files for a set of streams'''

import argparse
import os
import shlex
import subprocess
import sys
import tempfile
from typing import cast, List


def quote_command(args: List[str]) -> str:
    '''Return a properly quoted version of args'''
    return ' '.join(shlex.quote(arg) for arg in args)


def mk_extractor_script(stream: str,
                        layer: int,
                        decoder: str,
                        extra_decode_options: List[str],
                        sh_path: str,
                        dest_path: str) -> None:
    '''Write a script to sh_path that prints a ref command to dest_path'''
    abs_decoder = os.path.abspath(decoder)
    stream_dir, stream_base = os.path.split(stream)

    with open(sh_path, 'w') as out:
        cmd = quote_command([abs_decoder,
                             '--estimate-cmd', '-o', '/dev/null'] +
                            extra_decode_options +
                            ([] if layer == 0 else ['--oppoint', str(layer)]) +
                            [stream_base])
        if stream_dir:
            cmd = '(cd {}; {})'.format(shlex.quote(stream_dir), cmd)

        out.write("{} | tr '\r' '\n' | "
                  "grep 'reference decoder command' >{}\n"
                  .format(cmd, shlex.quote(dest_path)))


def setup_extractors(streams: List[str],
                     max_layer: int,
                     decoder: str,
                     extra_decode_options: List[str],
                     tmp_dir: str,
                     out_path: str) -> None:
    '''Set up stuff for running extractor scripts

    This writes little shell scripts to tmp_dir/<S>-<L>.sh, where S ranges over
    stream indices and L over layers. These shell scripts write their results
    to tmp_dir/<S>-<L>.out. It also writes <name>\0 to out_path where name
    ranges over the scripts (for use with xargs -0 -n1 sh).

    '''
    with open(out_path, 'wb') as out_file:
        for stream_idx, stream in enumerate(streams):
            for layer in range(max_layer + 1):
                base = '{}-{}'.format(stream_idx, layer)
                sh_path = os.path.join(tmp_dir, base + '.sh')
                dst_path = os.path.join(tmp_dir, base + '.out')

                mk_extractor_script(stream, layer,
                                    decoder, extra_decode_options,
                                    sh_path, dst_path)

                out_file.write(sh_path.encode() + b'\0')


def ensure_set_subdir(subdir: str, max_layer: int) -> None:
    '''Ensure the usual tree of directories at set_dir/subdir.'''
    os.makedirs(subdir, exist_ok=True)
    if max_layer > 1:
        layers = os.path.join(subdir, 'layers')
        os.makedirs(layers, exist_ok=True)
        for layer in range(1, max_layer + 1):
            os.makedirs(os.path.join(layers, str(layer)), exist_ok=True)


def subdir_path(subdir: str, base: str, layer: int, ext: str) -> str:
    '''Calculate the path for the file <base> in <subdir>

    If layer is zero, this is <subdir>/<base>.<ext>. Otherwise it is
    <subdir>/layers/<base>_layer<layer>.<ext>

    '''
    return (os.path.join(subdir, '{}.{}'.format(base, ext))
            if layer == 0
            else os.path.join(subdir, 'layers', str(layer),
                              '{}_layer{}.{}'.format(base, layer, ext)))


def make_ref_command(dst_path: str, in_path: str) -> None:
    ''' '''
    with open(in_path, 'r') as in_file:
        lines = in_file.readlines()

    if len(lines) != 1:
        raise RuntimeError('Reference command output for `{}\' '
                           'should have exactly 1 line, but actually is '
                           '{}.'
                           .format(dst_path, lines))

    with open(dst_path, 'w') as dst_file:
        dst_file.write('#!/bin/bash\n'
                       'set -eu\n' +
                       lines[0])


def get_ref_commands(streams: List[str],
                     max_layer: int,
                     decoder: str,
                     extra_decode_options: List[str],
                     ref_cmd_dir: str,
                     max_threads: int) -> None:
    '''Generate ref_cmd shell scripts for each stream and layer'''
    ensure_set_subdir(ref_cmd_dir, max_layer)

    with tempfile.TemporaryDirectory() as tmpdir:
        cmds_path = os.path.join(tmpdir, 'cmds')
        setup_extractors(streams, max_layer,
                         decoder, extra_decode_options, tmpdir, cmds_path)

        # Run everything in parallel
        subprocess.check_call(['xargs', '-0', '-n1'] +
                              (['-P', str(max_threads)]
                               if max_threads > 1 else []) +
                              ['-a', cmds_path, 'sh'])

        # Make sense of the results
        for stream_idx, stream in enumerate(streams):
            base = os.path.splitext(os.path.basename(stream))[0]
            for layer in range(max_layer + 1):
                in_path = os.path.join(tmpdir,
                                       '{}-{}.out'.format(stream_idx, layer))

                make_ref_command(subdir_path(ref_cmd_dir, base, layer, 'sh'),
                                 in_path)


def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('--jobs', '-j', type=int, default=1)
    parser.add_argument('--max-layer', type=int, default=0)
    parser.add_argument('decoder')
    parser.add_argument('stream_dir')
    parser.add_argument('ref_cmd_dir')

    args = parser.parse_args()

    stream_dir = cast(str, args.stream_dir)

    streams = []
    for entry in os.scandir(stream_dir):
        if entry.is_file() and not entry.name.startswith('.'):
            streams.append(os.path.join(stream_dir, entry.name))

    get_ref_commands(streams,
                     cast(int, args.max_layer),
                     cast(str, args.decoder),
                     [],
                     cast(str, args.ref_cmd_dir),
                     cast(int, args.jobs))

    return 0


if __name__ == '__main__':
    try:
        sys.exit(main())
    except Exception as err:
        sys.stderr.write('Error: {}\n'.format(err))
        sys.exit(1)
