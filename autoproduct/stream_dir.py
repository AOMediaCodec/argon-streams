################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Code for storing and calculating directories for streams in autoproduct'''


import os
import re
from typing import List, Optional, Tuple


def is_dir_with_files(path: str, ignored_names: Optional[List[str]]) -> bool:
    '''Return true if path names a directory contains regular files

    This ignores files in ignored_names.

    '''
    try:
        for _, _, files in os.walk(path):
            for name in files:
                if ignored_names is None or name not in ignored_names:
                    return True
        # Directory was empty or only contained ignored names.
        return False

    except OSError:
        # If we couldn't list the directory because it doesn't exist or isn't a
        # directory, we should just return False.
        if not os.path.isdir(path):
            return False

        # If something else went awry, we probably just have to raise the error
        # (permission denied, for example)
        raise


def mkdir_p(path: str) -> None:
    '''Like mkdir -p: try to make the named directory and parents.'''
    try:
        os.makedirs(path)
    except OSError:
        # This might just mean that the directory already exists.
        if os.path.isdir(path):
            return
        # Otherwise, raise the error.
        raise


def touch_init_py(dirname: str) -> None:
    '''Ensure a __init__.py file exists in the given directory'''
    path = os.path.join(dirname, '__init__.py')
    if os.path.exists(path):
        return
    with open(path, 'w'):
        pass


def ensure_dir(path: str,
               check_empty: bool,
               what: str,
               ign_names: List[str] = [],
               ensure_nonempty: bool = False) -> str:
    '''Ensure a directory exists; maybe warn to console or error if nonempty

    Return path (to allow chaining)'''
    if check_empty:
        assert what is not None
        if is_dir_with_files(path, ign_names):
            msg = ('{} already exists at {} and is non-empty.'
                   .format(what, path))
            if ensure_nonempty:
                raise RuntimeError(msg)
            else:
                print('Warning: ' + msg)
    mkdir_p(path)
    return path


class StreamDir:
    '''A class representing an autoproduct output directory tree'''

    # File creation modes
    NO_OUTPUT = 0
    ALLOW_CONTINUE = 1
    CHECK_NEW = 2

    def __init__(self,
                 profileName: str,
                 layer_range: Tuple[int, int],
                 name: str,
                 mode: int = CHECK_NEW) -> None:
        assert 0 <= mode <= 2

        check_empty = mode == StreamDir.CHECK_NEW
        needs_output = mode != StreamDir.NO_OUTPUT

        self.name = name
        self.root = os.path.join(profileName, name)
        self.streamDir = ensure_dir(os.path.join(self.root, "streams"),
                                    check_empty, 'Streams dir',
                                    ensure_nonempty=True)

        if needs_output:
            self.outDir = ensure_dir(os.path.join(self.root, "out"),
                                     check_empty, 'Encoder output dir',
                                     ensure_nonempty=True)

        self.coverpyDir = ensure_dir(os.path.join(self.root, "coverpy"),
                                     check_empty, 'Coverage dir',
                                     ign_names=['__init__.py'])
        self.cumulativeCoverageDir = \
            ensure_dir(os.path.join(self.root, "cumulative_cover"),
                       check_empty, 'Cumulative coverage dir',
                       ign_names=['__init__.py'])

        touch_init_py(self.root)
        touch_init_py(self.coverpyDir)
        touch_init_py(self.cumulativeCoverageDir)

    def coverage_modules(self) -> List[str]:
        '''Return the list of existing coverage module names

        If one of the coverage directories contains testfoo.py.gz then
        'N.coverpy.testfoo' will appear in the list, where N is self.name.

        '''
        matcher = re.compile(r'(test[0-9_]+)\.py\.gz')
        ret = []
        for cov_type in ['coverpy', 'cumulative_cover']:
            for fname in os.listdir(os.path.join(self.root, cov_type)):
                match = matcher.match(fname)
                if match:
                    ret.append('.'.join([self.name, cov_type, match.group(1)]))
        return ret

    @staticmethod
    def coverage_module_max_seed(name: str) -> int:
        '''Find max seed contributing to a name returned by coverage_modules'''
        match = re.match(r'.*\.test([0-9]+(?:_[0-9]+)*)$', name)
        if not match:
            raise RuntimeError('Bogus name for coverage module: `{}\''
                               .format(name))
        # Note that the regex should guarantee that each part of the split is a
        # genuine integer.
        return max(int(x) for x in match.group(1).split('_'))
