#!/usr/bin/env python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import argparse
import sys
import shutil
import os
import subprocess
import re
import tempfile

from typing import (cast, Callable, Dict, Iterable,
                    List, Optional, TextIO, Tuple, TypeVar)

from coveragestore import CoverageStore, Profile
from coverage import Coverage, read_module

from configuration import Configuration
from stream_dir import StreamDir
from buildtools import zip_up, progress
import buildtools

from ap_macros import Macros
import signal
import merge_streams
import random
from yaproc import YAProcs


def count1s(v: int) -> int:
    '''Count the number of 1's in v'''
    assert v >= 0
    r = 0
    while v:
        if v & 1:
            r += 1
        v >>= 1
    return r


EXITING = 0


def sigint_handler(signum: int, frame: object) -> None:
    global EXITING
    if EXITING:
        print("Double SIGINT, exiting.")
        sys.exit(1)
    else:
        print("Finishing current batch before exiting.\n"
              "You can press Ctrl-C again to exit immediately, but this may\n"
              "lead to errors if you later intend to continue this set.")
        EXITING = True


def check_positive(strval: str) -> int:
    '''If the string names a positive integer, return it. Else error.'''
    try:
        ival = int(strval)
    except ValueError:
        raise argparse.ArgumentTypeError('Can\'t parse `{}\' as an integer.'
                                         .format(strval))
    if ival <= 0:
        raise argparse.ArgumentTypeError('{} is not a positive integer.'
                                         .format(ival))
    return ival


def parse_args(argv: List[str], macros: Macros) -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('--rebuild',
                        help="Rebuild all tool binaries", action='store_true')
    parser.add_argument('--continue', '-c', action='store_false',
                        dest='restart',
                        help=('Reload existing coverage and '
                              'continue from where we left off'))
    parser.add_argument('--rewind', '-r', metavar='SEED',
                        help=('Delete all streams and associated files which '
                              'have seeds greater than SEED then continue. '
                              'Implies --continue.'),
                        type=int)
    parser.add_argument('--other-bindirs',
                        action='append',
                        default=cast(List[str], []),
                        dest='other_bindirs',
                        help=('Also search in this directory for binaries (to '
                              'avoid building lots of coverage tools).'))
    parser.add_argument('--random-choices', dest='random', action='store_true',
                        help=('Select next branch, cross or range point at '
                              'random from those remaining'))
    parser.add_argument('--max-threads', '-j', dest='threads', metavar='T',
                        type=check_positive,
                        help='Limit number of threads to be used.')
    parser.add_argument('--gensize', '-g',
                        type=check_positive,
                        help=('The number of streams to generate each round. '
                              'Defaults to the number of threads, but you may '
                              'wish to set it higher to make use of '
                              'parallelism at encode time or lower to avoid '
                              'wasted time ref/testing.'))
    parser.add_argument('--debug-process', dest='debug_process',
                        help=('Run autoproduct aborting after each batch of '
                              'streams is created, this is used to debug the '
                              'set creating process'))
    parser.add_argument('--max-streams', '-m',
                        dest='streams', metavar='S',
                        type=check_positive,
                        help=('Limit number of streams to be generated. '
                              '(default: no limit)'))
    parser.add_argument('--just-init', action='store_true', dest='just_init',
                        help='Just make directory structure and exit')
    parser.add_argument('--crosses', dest='crosses', metavar='FILE',
                        help=('A file with a list of '
                              'cross coverage points to hit'))
    parser.add_argument('--branches', dest='branches', metavar='FILE',
                        help=('A file with a list of '
                              'branch coverage points to hit'))
    parser.add_argument('--start-seed', '-s',
                        dest='start_seed', type=check_positive, default=1,
                        help=('Which seed to start at (default: 1)'))
    parser.add_argument('--define', '-D',
                        help='Add a definition for the macro expander',
                        action='append',
                        default=cast(List[str], []))
    parser.add_argument('--never-merge', dest='never_merge',
                        action='store_true',
                        help=('Never merge streams.'))

    parser.add_argument('input', help='Input file')

    args = parser.parse_args(argv)

    # Make --rewind imply --continue.
    if cast(Optional[int], args.rewind) is not None:
        args.restart = False

    if cast(Optional[int], args.threads) is not None:
        buildtools.setMaxThreads(cast(int, args.threads))

    if cast(Optional[int], args.gensize) is None:
        args.gensize = buildtools.getMaxThreads()

    assert isinstance(cast(int, args.gensize), int)

    if cast(bool, args.rebuild) and cast(List[str], args.other_bindirs):
        raise RuntimeError('Cannot combine --rebuild with --other-bindirs.')

    for define in cast(List[str], args.define):
        parts = define.split('=')
        if len(parts) != 2:
            raise RuntimeError('Bad value for define: `{}\''.format(define))

        # Set the definition, at level 1 (overriding any definitions in the XML
        # files)
        macros.set(parts[0], parts[1], level=1)

    return args


T = TypeVar('T')


def pick_hole(holes: List[Tuple[str, T]],
              is_interesting: Callable[[Tuple[str, T]], bool],
              pick_random: bool) -> Tuple[str, T]:
    '''Find the first hole (or a random one) which is "interesting"

    If no holes satisfy is_interesting, return the first (if pick_random is
    false) or a random hole.

    '''
    assert holes
    hole = random.choice(holes) if pick_random else holes[0]
    if is_interesting(hole):
        return hole

    if pick_random:
        random.shuffle(holes)
    for hole in holes:
        if is_interesting(hole):
            return hole

    return holes[0]


def do_branch_coverage(log_file: TextIO,
                       subset: int,
                       pick_random: bool,
                       generator: 'StreamGenerator',
                       cov: Coverage,
                       gen_size: int) -> bool:
    '''Try to generate branch coverage for subset

    Returns true if any streams were generated.'''
    progress("Generating BRANCH coverage for subset {}".format(subset))

    # Try to generate a single stream using the default branch profile to
    # populate our branch points table. We do this in a while loop so that if
    # all the first streams are skipped, we'll keep searching.
    branch_coverage = cov.branch_coverage
    while branch_coverage.visible_view_classes is None:
        branch_profile = cov.cstore.find_branch_profile([], subset)
        if not branch_profile:
            if branch_coverage.expected:
                raise RuntimeError('XML profile has no <branch> information '
                                   'for subset {} but we were run with a '
                                   '--branches argument.'
                                   .format(subset))

            # If there is no branch profile defined in the XML file, that might
            # be OK: we just won't end up with a branch table.
            break

        generator.run(log_file, cov, branch_profile, subset, 1)

    # If branch_coverage is empty at this point, it must be that there wasn't
    # any branch profile defined in the XML file. Never mind; we'll just have
    # to skip branch coverage for this subset.
    if not branch_coverage.raw:
        progress('  (No branch coverage for subset {})'.format(subset))
        return False

    streams_generated = False

    # Loop over the branch table looking for a point we need to fill
    while True:
        holes = branch_coverage.branch_holes(subset)
        if not holes:
            return streams_generated

        cov.write_holes(subset)

        hole = random.choice(holes) if pick_random else holes[0]
        hole_data = branch_coverage.raw.get(hole, None)

        profile = cov.cstore.find_branch_profile(hole.split(";"), subset)
        assert profile
        progress("  Found hole in branch {}{}; using {}; "
                 "{} holes remain\n"
                 .format(hole,
                         (' [{}]'.format(hole_data)
                          if hole_data is not None else ''),
                         profile, len(holes)))

        for name in holes[:20]:
            branch_coverage.print_coverage(sys.stdout, name, subset, indent=4)
        if len(holes) > 20:
            print('    ...')
        progress('')

        generator.run(log_file, cov, profile, subset, gen_size)
        streams_generated = True

    return streams_generated


def do_range_coverage(log_file: TextIO,
                      subset: int,
                      pick_random: bool,
                      generator: 'StreamGenerator',
                      cov: Coverage,
                      gen_size: int) -> bool:
    '''Try to generate range coverage for subset

    Returns true if any streams were generated.'''

    # If range_coverage.visibility_types is None, that means that we're not
    # tracking range coverage at all. Print a message and skip past.
    range_coverage = cov.range_coverage
    if range_coverage.visibility_types is None:
        progress("  (skipping RANGE coverage for subset {})".format(subset))
        return False

    progress("Generating RANGE coverage for subset {}".format(subset))

    streams_generated = False

    while True:
        holes = range_coverage.holes(subset)
        if not holes:
            # We're done!
            return streams_generated

        cov.write_holes(subset)

        name, (seen, expected) = \
            pick_hole(holes,
                      lambda h: cov.cstore.has_specific_range_profile(h[0]),
                      pick_random)

        profile = cov.cstore.find_range_profile(name, subset)
        if profile is None:
            raise RuntimeError("No profile specified for range equation {}"
                               .format(name))

        progress("  Found hole in range equation {} (theoretical range={} "
                 "our range={} branch=\"{}\"); using {}; "
                 "{} holes remain"
                 .format(name, expected, seen,
                         range_coverage.eqn_to_branch.get(name),
                         profile, len(holes)))

        range_coverage.print_holes(sys.stdout, holes, 20)
        sys.stdout.flush()

        generator.run(log_file, cov, profile, subset, gen_size)
        streams_generated = True


def do_cross_coverage(log_file: TextIO,
                      subset: int,
                      pick_random: bool,
                      generator: 'StreamGenerator',
                      cov: Coverage,
                      gen_size: int,
                      config: Configuration) -> bool:
    '''Try to generate cross coverage for subset'''
    print("Generating CROSS coverage for subset {}".format(subset))

    cross_coverage = cov.cross_coverage

    if cov.cstore.default_cross_profiles is None:
        return False

    streams_generated = False

    while True:
        holes = cross_coverage.holes(subset)
        if not holes:
            return streams_generated

        cov.write_holes(subset)

        # Find the first (or a random) hole with a specific cross profile. If
        # there are no such holes, just return the first hole we checked.
        hole = pick_hole(holes,
                         lambda h: cov.cstore.has_specific_cross_profile(h[0]),
                         pick_random)

        name, missing = hole
        profile = cov.cstore.find_cross_profile(name, subset)
        if profile is None:
            raise RuntimeError("No profile specified for cross {}"
                               .format(name))

        print("  Found hole in cross {} ({} bits to fill); "
              "using {}; {} holes remain"
              .format(name, count1s(missing), profile, len(holes)))

        cross_coverage.print_holes(sys.stdout, holes, 20)
        sys.stdout.flush()

        generator.run(log_file, cov, profile, subset, gen_size)
        streams_generated = True


def run_encodes(config: Configuration,
                base_command: List[str],
                profile: Profile,
                profile_name: str,
                stream_dir: StreamDir,
                stream_extension: str,
                seeds: List[int],
                threads: int) -> List[int]:
    '''Encode some streams, checking the output for error messages

    Returns a list of rejected indices'''

    # The profile might have extra arguments that should be added to the encode
    # command.
    base_command = profile.extend_encode_command(base_command)

    pairs = [(seed, os.path.join(stream_dir.streamDir,
                                 "test{}.{}".format(seed, stream_extension)))
             for seed in seeds]

    rejected = []
    fails = []
    with YAProcs([base_command + ['-s', str(seed), "-o", path]
                  for (seed, path) in pairs],
                 use_stdout=True, njobs=threads) as yap:
        for idx, retcode, proc in yap.dribble():
            seed, path = pairs[idx]
            reject = bool(buildtools.
                          innocuous_checks(proc.output_path()))
            proc.cleanup()

            if retcode or reject:
                buildtools.rm_f(path)

            if reject:
                rejected.append(idx)

            if retcode and not reject:
                fails.append(proc.args)

    if fails:
        raise RuntimeError('{} encodes failed:\n\n '.format(len(fails)) +
                           '\n '.join(' '.join(cmd) for cmd in fails))
    return rejected


def do_subset_coverage(log_file: TextIO,
                       subset: int,
                       pick_random: bool,
                       coverage: Coverage,
                       generator: 'StreamGenerator',
                       gen_size: int,
                       config: Configuration) -> None:
    streams_generated = do_branch_coverage(log_file, subset, pick_random,
                                           generator, coverage, gen_size)
    streams_generated |= do_range_coverage(log_file, subset, pick_random,
                                           generator, coverage, gen_size)
    streams_generated |= do_cross_coverage(log_file, subset, pick_random,
                                           generator, coverage, gen_size,
                                           config)

    all_coverage = os.path.join(config.profile_name, "all_coverage.py")
    if streams_generated and os.path.exists(all_coverage):
        shutil.copyfile(all_coverage,
                        os.path.join(config.profile_name,
                                     ("all_coverage_subset{}.py"
                                      .format(subset))))


def do_all_coverage(log_path: str,
                    pick_random: bool,
                    coverage: Coverage,
                    generator: 'StreamGenerator',
                    gen_size: int,
                    config: Configuration) -> None:
    with open(log_path, "a") as log_file:
        for subset in config.subsets():
            do_subset_coverage(log_file, subset, pick_random,
                               coverage, generator, gen_size, config)


def run_coverage_tool(cov_base_cmd: List[str],
                      extension: str,
                      stream_dir: StreamDir,
                      good_seeds: List[int],
                      threads: int) -> List[Tuple[int, str, str]]:
    '''Run the coverage tool on a set of generated streams.'''
    print("  Running coverage...")

    coverpy_dir = stream_dir.coverpyDir
    obu_dir = stream_dir.streamDir

    triples = []
    for seed in good_seeds:
        stream = os.path.join(obu_dir, "test{}.{}".format(seed, extension))
        pyfile = os.path.join(coverpy_dir, "test{}.py".format(seed))
        if os.path.exists(stream):
            triples.append((seed, stream, pyfile))

    fails = []
    with YAProcs([cov_base_cmd + ['--pyf', pyfile, stream]
                  for (_, stream, pyfile) in triples],
                 use_stdout=os.devnull, njobs=threads) as yap:
        for _, retcode, proc in yap.dribble():
            if retcode:
                fails.append(proc.args)

    if fails:
        raise RuntimeError('{} coverage runs failed:\n\n '.format(len(fails)) +
                           '\n '.join(' '.join(cmd) for cmd in fails))

    return triples


def do_merge_streams(config: Configuration, dirs: 'Directories', minimalSetStreams: List[Tuple[str, str]]) -> None:
    '''Merge streams'''
    print("Merging streams...")

    # If we got halfway through a merge, delete the results and start again
    for dirname in dirs.merged_dirs:
        buildtools.rm_f(dirname, recursive=True)
        os.makedirs(dirname)

    merge_types = {
        'h265': 'h265',
        'av1b': 'obu'
    }
    merge_type = merge_types.get(config.codec, 'ivf')

    # If just_copy is true, we won't actually do any merging (but we will copy
    # files into the destination directory)
    just_copy = buildtools.getProfileNonMergeable(config.profile_name)

    # Iterate over the destination directories once each (dirs.merged_dirs will
    # have duplicate entries if more than one subset points at the same
    # directory)
    for dirname in set(dirs.merged_dirs):
        print("Merging {}".format(dirname))

        # Subsets in yes allow merging between subsets; those in no do not.
        yes, no = dirs.merge_inputs[dirname]
        for subsets in [yes] + [[x] for x in no]:
            # Here, subsets is a list of subsets that we can merge between. It
            # might be empty if yes was empty.
            if not subsets:
                continue

            encode_options = config.views[subsets[0]].encode_options

            # Check that all the stream dirs that contribute to this dirname
            # have the same encoder options. If they don't, spit out an error
            # message.
            for subset in subsets[1:]:
                if config.views[subset].encode_options != encode_options:
                    raise RuntimeError('The subsets that contribute to {} are '
                                       '{} and not all of them have the same '
                                       'encoder options.'
                                       .format(dirname, subsets))

            print("Merging subsets: {}"
                  .format(", ".join([str(subset) for subset in subsets])))

            subset_names = ['subset{}'.format(subset) for subset in subsets]
            subset_stream_dirs = [dirs.stream_dirs[name].streamDir
                                  for name in subset_names]

            merge_streams.mergeStreams(subset_stream_dirs,
                                       dirname, merge_type,
                                       just_copy, encode_options,
                                       minimalSetStreams)


class Directories:
    '''An object that keeps track of the various build/dest directories'''
    _LAYERED_CODECS = {
        "h265": (0, 7),
        "av1b": (1, 32)
    }

    def __init__(self,
                 config: Configuration,
                 other_bindirs: List[str],
                 restart: bool):

        self.config = config

        # Where should we put files? Stuff that actually relates to this run
        # will go in ./profile_name, but we need somewhere to build binaries
        # and, indeed, somewhere for the binaries to go. We stick them "next
        # door", in ./build-profile_name.
        #
        # Explicitly setting the build directory to ./build-profile_name/build
        # means that we can do multiple autoproduct runs on the source tree at
        # once. Also, this way, if something has gone horribly wrong, the user
        # can delete the ./profile_name directory without having to rebuild
        # encoder/decoder/etc.
        self.build_root = 'build-' + self.config.profile_name

        self.other_bindirs = other_bindirs

        self.set_names = self.config.all_set_names()

        # Various filenames
        self.minimal_set_path = os.path.join(self.config.profile_name,
                                             'minimalset.txt')

        # A regular expression that matches stuff that corresponds to a stream.
        # A match will have a single group, which can be split by '_' to get a
        # list of stream numbers.
        self.num_regex = re.compile(r'test([0-9_]+)\.')

        # A dictionary of StreamDir objects, keyed by set name
        self.stream_dirs = {}  # type: Dict[str, StreamDir]

        # A list of directories into which we're going to write merged streams.
        # This is indexed by "subset - 1", so subset 1 goes into
        # merged_dirs[0], subset 2 goes into merged_dirs[1] etc.
        self.merged_dirs = []  # type: List[str]

        # A dictionary keyed by elements of merged_dirs. The value at a
        # directory D is a disjoint pair of lists of subsets: (yes, no). Every
        # element, i, of yes + no is the index of a subset where merged_dirs[i]
        # is D. Elements of yes are happy to merge across subsets; those in no
        # are not.
        self.merge_inputs = {}  # type: Dict[str, Tuple[List[int], List[int]]]

        # Setting up stream_dirs and merged_dirs
        self._setup_stream_dirs(restart)
        self._setup_merged_dirs()

        # If we're restarting, we should delete the directories in merged_dirs
        # (which are hopefully empty, otherwise we don't do so since this might
        # cause data loss)
        if restart:
            self._clean_merged_dirs()

        # Finally make our output directories
        self._make_dest_dirs()

    def _setup_stream_dirs(self, restart: bool) -> None:
        layer_range = (Directories._LAYERED_CODECS
                       .get(self.config.codec, (0, 0)))
        subset_names = ['subset{}'.format(ss) for ss in self.config.subsets()]
        sd_mode = StreamDir.CHECK_NEW if restart else StreamDir.ALLOW_CONTINUE

        self.stream_dirs['core'] = StreamDir(self.config.profile_name,
                                             layer_range, 'core',
                                             StreamDir.NO_OUTPUT)

        for name in (['extra', 'extra_dropped'] +
                     subset_names + self.set_names):
            self.stream_dirs[name] = StreamDir(self.config.profile_name,
                                               layer_range, name, sd_mode)

    def _setup_merged_dirs(self) -> None:
        for subset in self.config.subsets():
            set_name = self.config.get_set_name(subset)
            path = os.path.join(self.config.profile_name,
                                'merged_streams_{}'.format(set_name))

            assert len(self.merged_dirs) == subset - 1
            self.merged_dirs.append(path)

            yes, no = self.merge_inputs.setdefault(path, ([], []))
            if self.config.views[subset].enable_subset_merge:
                yes.append(subset)
            else:
                no.append(subset)

    def _clean_merged_dirs(self) -> None:
        '''Delete any merged directories that exist, checking they are empty'''
        for path in self.merged_dirs:
            if os.path.exists(path):
                if os.listdir(path):
                    raise RuntimeError("Merged streams directory {} already "
                                       "exists and is non-empty!"
                                       .format(path))

                os.rmdir(path)

    def _make_dest_dirs(self) -> None:
        for dirname in [self.bin_dir(), self.log_dir()]:
            try:
                os.makedirs(dirname)
            except OSError:
                if not os.path.isdir(dirname):
                    raise
                pass

    def bin_dir(self) -> str:
        '''Return the directory for binaries'''
        return os.path.join(self.build_root, 'bin')

    def binary_path(self, basename: str) -> str:
        '''Return the path to basename inside bin_dir'''
        path = buildtools.find_binary(basename,
                                      self.bin_dir(),
                                      self.other_bindirs)
        if path is None:
            raise RuntimeError('Can\'t find the {} binary in any of '
                               'the following directories: {}.'
                               .format(basename,
                                       [self.bin_dir()] + self.other_bindirs))
        return path

    def log_dir(self) -> str:
        '''Return the directory for log files'''
        return os.path.join(self.build_root, 'log')

    def build_dir(self) -> str:
        '''Return the directory in which to build'''
        return os.path.join(self.build_root, 'build')

    def have_already_merged(self) -> bool:
        '''Returns true if we've already merged the streams in all dirs'''

        # If any of the merged streams directories exists, that means that we
        # stopped half-way through a previous merge.
        for dirname in self.merged_dirs:
            if os.path.exists(dirname):
                return False

        # Otherwise, look at each relevant stream dir: they should all exist
        # and be non-empty.
        for set_name in self.set_names:
            path = self.stream_dirs[set_name].streamDir
            if not os.path.exists(path) or not os.listdir(path):
                return False

        return True

    @staticmethod
    def find_layer_dirs(path: str) -> List[str]:
        '''Return a list of layer directories starting at path

        This looks at path and also path/layers/*, returning any that exist and
        are directories.

        '''
        if not os.path.isdir(path):
            return []

        layers = os.path.join(path, 'layers')
        if not os.path.isdir(layers):
            return [path]

        ret = [path]
        for entry in os.scandir(layers):
            if entry.is_dir():
                ret.append(os.path.join(layers, entry.name))

        return ret

    def _rewind_subset(self, max_seed: int, subset: int) -> Tuple[int, int]:
        '''Delete files to rewind in a subset.

        Returns number of streams deleted.'''
        ss_dir = os.path.join(self.config.profile_name,
                              'subset{}'.format(subset))
        fcount = 0
        scount = 0
        for top_dir in ["streams", "out", "coverpy", "cumulative_cover"]:
            top_path = os.path.join(ss_dir, top_dir)
            for dirname in Directories.find_layer_dirs(top_path):
                for filename in os.listdir(dirname):
                    match = self.num_regex.match(filename)
                    if not match:
                        continue
                    max_num = max(int(part)
                                  for part in match.group(1).split('_'))
                    if max_num <= max_seed:
                        continue

                    fcount += 1
                    if top_dir == 'streams':
                        scount += 1

                    os.remove(os.path.join(dirname, filename))

        return (fcount, scount)

    def rewind(self, max_seed: int) -> None:
        '''Delete files to rewind back to a given maximum seed'''
        progress("Rewinding so that maximum remaining "
                 "stream seed is {}...".format(max_seed))
        fcount = 0
        scount = 0
        for subset in self.config.subsets():
            ss_f, ss_s = self._rewind_subset(max_seed, subset)
            fcount += ss_f
            scount += ss_s

        progress("Removed {} files ({} streams)".format(fcount, scount))

        # If we've rewound, the all_coverage file is no longer valid
        if fcount:
            for filename in os.listdir(self.config.profile_name):
                if filename.startswith('all_coverage.'):
                    os.remove(os.path.join(self.config.profile_name, filename))
            buildtools.rm_f(self.minimal_set_path)

    def max_id_in_dir(self, path: str) -> int:
        '''Return the maximum stream id at path. Returns 0 if there is none'''
        if not os.path.isdir(path):
            return 0
        max_seen = 0
        for filename in os.listdir(path):
            match = self.num_regex.match(filename)
            if not match:
                continue
            max_seen = max(max_seen,
                           max(int(part)
                               for part in match.group(1).split('_')))
        return max_seen

    def start_seed(self) -> int:
        '''Calculate the first seed to use based on stream ids seen'''
        max_seen = 0
        for name in (['core'] + ['subset{}'.format(ss)
                                 for ss in self.config.subsets()]):
            stream_dir = self.stream_dirs[name]
            max_seen = max(max_seen, self.max_id_in_dir(stream_dir.streamDir))

        return (max_seen if max_seen > 0 else 0) + 1


class StreamGenerator:
    '''A stream generator object is in charge of generating streams.

    It keeps track of the current seed and knows how to run the encoder to make
    each new batch of streams.

    '''
    def __init__(self,
                 dirs: Directories,
                 config: Configuration,
                 seed: int,
                 batch_size: int,
                 max_seed: Optional[int],
                 extension: str):

        self.dirs = dirs
        self.config = config
        self.seed = seed
        self.batch_size = batch_size
        self.max_seed = max_seed
        self.ext = extension

        self.choose_all_file = None
        self.last_profile = None  # type: Optional[Profile]

    def update_choose_all(self, profile: Profile) -> None:
        '''Update the current choose all file for profile

        If this profile doesn't use choose all, self.choose_all_file will be
        None. This will delete the (temporary) file when switching away from an
        existing profile's choose all file.

        '''
        choose_all = buildtools.getProfileUsesChooseAll(profile.name)
        new_choose_all = (profile != self.last_profile) or not choose_all

        if self.choose_all_file and new_choose_all:
            buildtools.rm_f(self.choose_all_file)
            self.choose_all_file = None

        self.last_profile = profile

    def _next_batch(self,
                    profile: Profile,
                    subset: int,
                    stream_dir: StreamDir,
                    extra_encode_options: List[str],
                    extra_decode_options: List[str],
                    gen_size: int) -> Tuple[List[Tuple[int, str, str]],
                                            List[int]]:
        '''Generate the next batch of streams

        Returns a list of triples: (seed, stream, pyfile) for the good
        seeds, together with a list of the bad seeds.

        '''
        if self.max_seed and self.seed >= self.max_seed:
            print("Maximum stream limit exceeded, exiting.")
            sys.exit(1)

        self.update_choose_all(profile)

        threads = buildtools.getProfileThreadLimit(profile.name)
        seeds = [self.seed + s for s in range(0, gen_size)]

        progress("  [Subset {}/{}] "
                 "Generating streams {}-{} with {} threads using {}..."
                 .format(subset, self.config.max_subset,
                         seeds[0], seeds[-1], threads, profile.name))

        if self.choose_all_file:
            progress("(Using ChooseAll pass-on file...)")

        encoder = \
            buildtools.buildEncoder(self.dirs.bin_dir(),
                                    self.dirs.build_dir(),
                                    self.dirs.log_dir(),
                                    profile.name,
                                    self.config.codec,
                                    other_bindirs=self.dirs.other_bindirs)
        if encoder is None:
            raise RuntimeError('Failed to build an encoder for profile {}'
                               .format(profile.name))

        enc_base_cmd = [encoder, "-v"] + extra_encode_options

        if self.choose_all_file:
            rejected = []
            for seed in seeds:
                rejected += run_encodes(self.config,
                                        (enc_base_cmd +
                                         ['-c', self.choose_all_file]),
                                        profile, self.config.profile_name,
                                        stream_dir, self.ext, [seed], threads)
        else:
            rejected = run_encodes(self.config, enc_base_cmd, profile,
                                   self.config.profile_name, stream_dir,
                                   self.ext, seeds, threads)

        good_seeds = []
        bad_seeds = []
        for seed in seeds:
            if seed - self.seed in rejected:
                bad_seeds.append(seed)
            else:
                good_seeds.append(seed)

        decode_options = self.config.views[subset].decode_options

        cov_base_cmd = ([self.dirs.binary_path("cover.exe"),
                         '-p', '/dev/null'] +
                        decode_options)

        self.seed += gen_size

        return (run_coverage_tool(cov_base_cmd, self.ext,
                                  stream_dir, good_seeds, threads),
                bad_seeds)

    def run(self,
            log_file: TextIO,
            coverage: Coverage,
            profile: Profile,
            subset: int,
            gen_size: int) -> None:
        '''Generate a batch of streams and update coverage'''
        subset_str = 'subset{}'.format(subset)
        stream_dir = self.dirs.stream_dirs[subset_str]

        view = self.config.views[subset]

        # Generate a batch of streams and associated coverage.
        cov_triples, bad_seeds = \
            self._next_batch(profile,
                             subset,
                             stream_dir,
                             view.encode_options,
                             view.decode_options,
                             gen_size)

        profile_name = self.config.profile_name

        for (seed, stream, pyfile) in cov_triples:
            # Load the coverage we just generated
            mod_name = "{}.coverpy.test{}".format(stream_dir.name, seed)
            path = os.path.join(profile_name,
                                stream_dir.name,
                                'coverpy',
                                "test{}.py".format(seed))

            coverage.update(self.config,
                            read_module(path), subset, mod_name)

            # Gzip the coverage and delete any .pyc file
            zip_up(pyfile)
            buildtools.rm_f(pyfile + 'c')

            # Report successful seed to log file
            log_file.write('({}, {}, {})\n'.format(seed, subset, profile.name))

        for seed in bad_seeds:
            # Report failed seeds to log file
            log_file.write('({}, REJECTED)\n'.format(seed))

        if cov_triples:
            largest_seed = cov_triples[-1][0]
            cumulativeFile = os.path.join(profile_name,
                                          subset_str,
                                          "cumulative_cover",
                                          "test{}.py".format(largest_seed))

            shutil.copyfile(os.path.join(profile_name, "all_coverage.py"),
                            cumulativeFile)
            zip_up(cumulativeFile)

        log_file.flush()

        # Set by sigint handler
        if EXITING:
            sys.exit(0)


def make_minimal_set(profile_name: str,
                     subsets: Iterable[int],
                     extension: str,
                     coverage: Coverage,
                     dest_path: str) -> None:
    '''Calculate a minimal set, writing to dest_path'''
    progress("Finding minimal set...")

    with tempfile.TemporaryDirectory() as tmpdir:
        # We want to find a minimal set, but we don't necessarily want
        # this to be "the minimal set that hits all the coverage from
        # these streams". If the XML file had some filtering, we just want
        # the minimal set that hits the coverage points that we care
        # about.
        points_file = os.path.join(tmpdir, 'coverage-points.txt')
        coverage.list_points(points_file)

        subset_dirs = [os.path.join(profile_name, 'subset{}'.format(x))
                       for x in subsets]
        minimal_set_tmp = os.path.join(tmpdir, 'minimal-set.txt')

        script = os.path.join(os.path.dirname(__file__),
                              'minimal_set_withcrosses.py')
        cmd = (['python2', script, '-q',
                '-t', extension,
                '-o', minimal_set_tmp,
                '--filter', points_file,
                '-f', os.path.join(profile_name, 'core')] +
               subset_dirs)
        try:
            subprocess.check_call(cmd)
        except subprocess.CalledProcessError:  # type: ignore
            print('Command failed: {}'.format(' '.join(cmd)))
            exit(1)

        shutil.copyfile(minimal_set_tmp, dest_path)


def main(argv: List[str]) -> None:
    g_macros = Macros()
    args = parse_args(argv, g_macros)

    progress("Parsing input file...")
    g_cstore = CoverageStore()
    g_config = Configuration(g_cstore, g_macros,
                             cast(str, args.input),
                             cast(bool, args.never_merge))

    _STREAM_EXTENSIONS = {
      'h265': 'bit',
      'vp9': 'ivf',
      'av1b': 'obu'
    }

    stream_extension = _STREAM_EXTENSIONS.get(g_config.codec)
    if stream_extension is None:
        print("Unrecognised codec {}".format(g_config.codec))
        sys.exit(1)

    cov_engine = Coverage(g_cstore, g_config.profile_name)

    for profile in g_config.profile_list:
        (pname, pfuns, plim, pchoose_all, pnon_mergeable) = profile
        buildtools.storeProfile(pname, pfuns, plim,
                                pchoose_all, pnon_mergeable)

    for tag, action in g_config.checks.items():
        buildtools.store_check(tag, action)

    buildtools.buildtoolOptions = list(g_config.build_tool_options)

    # If there's a list of crosses or branches that need reading in, do that
    # now.
    cov_engine.add_expected(cast(Optional[str], args.branches),
                            cast(Optional[str], args.crosses))

    progress("Checking output directories...")

    dirs = Directories(g_config,
                       cast(List[str], args.other_bindirs),
                       cast(bool, args.restart))

    if cast(bool, args.restart):
        if os.path.exists(dirs.minimal_set_path):
            raise RuntimeError('The file `{}\' exists, which probably '
                               'means we shouldn\'t restart. Delete it '
                               'if you really want to.')

    if cast(bool, args.just_init):
        return

    # Do rewind if requested
    if cast(Optional[int], args.rewind) is not None:
        dirs.rewind(cast(int, args.rewind))

    sys.path.append(os.path.realpath(g_config.profile_name))

    buildtools.buildTools(dirs.bin_dir(), dirs.build_dir(), dirs.log_dir(),
                          cov_engine.cstore.all_profiles,
                          cast(bool, args.rebuild), g_config.codec,
                          dirs.other_bindirs)

    if not os.path.exists(dirs.minimal_set_path):
        start_seed = max(cast(int, args.start_seed), dirs.start_seed())
        gen_size = cast(int, args.gensize)

        stream_generator = \
            StreamGenerator(dirs,
                            g_config,
                            start_seed,
                            gen_size,
                            (start_seed + cast(int, args.streams)
                             if cast(Optional[int], args.streams) else None),
                            stream_extension)

        cov_engine.load_previous(g_config, dirs.stream_dirs)

        do_all_coverage(os.path.join(g_config.profile_name, "log.txt"),
                        cast(bool, args.random),
                        cov_engine,
                        stream_generator,
                        gen_size,
                        g_config)

        signal.signal(signal.SIGINT, signal.SIG_DFL)

        make_minimal_set(g_config.profile_name,
                         g_config.subsets(),
                         stream_extension,
                         cov_engine,
                         dirs.minimal_set_path)

    minimalSetStreamsOtherDirs = []

    progress("Reading minimal set file...")
    minimalSetStreams = []
    with open(dirs.minimal_set_path, "r") as f:
        for line in f:
            if not line.startswith("("):
                continue

            line_val = cast(object, eval(line))
            if not (isinstance(line_val, tuple) and
                    len(line_val) == 2):
                raise RuntimeError('Minimal set at {} contains a line that '
                                   'parses as {!r} (not a pair of strings).'
                                   .format(dirs.minimal_set_path, line_val))

            dr, stream = line_val
            if not isinstance(dr, str):
                raise RuntimeError('Minimal set at {} contains a dr value '
                                   'that parses as {!r} (not a string).'
                                   .format(dirs.minimal_set_path, dr))

            dr = dr[len(g_config.profile_name)+1:]
            if re.match("subset[0-9]+", dr) is not None:
                minimalSetStreams.append((dr, stream))
            else:
                minimalSetStreamsOtherDirs.append((dr, stream))

    if not dirs.have_already_merged():
        do_merge_streams(g_config, dirs, minimalSetStreams)

        if minimalSetStreamsOtherDirs:
            progress("Copying in streams from other directories...")

            otherdirs = []  # type: List[str]
            for dr, stream in minimalSetStreamsOtherDirs:
                if dr not in otherdirs:
                    otherdirs.append(dr)
            print("Unsure what to do with extra directories: {}"
                  .format(" ".join(otherdirs)))
            print("Copied extra directories to {}".format(dirs.merged_dirs[0]))
            mergedStreamsDir = dirs.merged_dirs[0]

            for dr, stream in minimalSetStreamsOtherDirs:
                shutil.copy(os.path.join(g_config.profile_name, dr, "streams",
                                         "{}.{}".format(stream,
                                                        stream_extension)),
                            mergedStreamsDir)

    print("Done!")


if __name__ == '__main__':
    try:
        main(sys.argv[1:])
    except Exception as err:
        sys.stderr.write('Error: {}\n'.format(err))
        sys.exit(1)
