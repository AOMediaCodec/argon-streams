################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A module for keeping track of branch coverage.'''

import copy
from typing import cast, Dict, IO, List, Optional, Tuple, Union

from coveragestore import CoverageStore

_BranchCov = Union[Tuple[bool, bool], bool]
_VisDict = Dict[int, Dict[str, bool]]

# The type for branch data that we read from a coverage run. This is:
#
#   (min, max, exp_min, exp_max, visT, visF)
CBData = Tuple[Optional[int], Optional[int], int, int, str, str]


def eval_visibility(expr: str, visible_categories: Dict[str, bool]) -> bool:
    expr = expr.replace("&&", " and ")
    expr = expr.replace("||", " or ")
    expr = expr.replace("!", " not ")

    # This cast is a bit silly: it claims that the result of eval is a bool. In
    # practice, it probably is, but we'll squash whatever we got with the bool
    # constructor immediately, so it doesn't really matter.
    return bool(cast(bool, eval(expr, visible_categories)))


class BranchCoverage:
    def __init__(self, cstore: CoverageStore) -> None:
        self.cstore = cstore

        # raw is a dictionary keyed by branch point name (the crazy string with
        # semicolons in it). Each value is one of:
        #
        #   - (a, b)  This branch is 'boolean'. That's used for an actual
        #             branch (which could be taken or not). a is true if we saw
        #             the value be zero; b is true if we saw the value be
        #             nonzero. (In branch terminology: this is (not_taken,
        #             taken)).
        #
        #   - c       We only care about whether this expression was evaluated
        #             or not (and don't care about its value). c is a boolean.
        self.raw = {}  # type: Dict[str, _BranchCov]

        # visibility is a dictionary keyed by branch point name (like raw).
        # Each element is a pair (a, b). For boolean branches, this pair gives
        # the visibility of the not_taken and taken branches. For non-boolean
        # branches, both entries of the pair are equal.
        self.visibility = {}  # type: Dict[str, Dict[int, Tuple[bool, bool]]]

        self.expected = []  # type: List[str]

        # This is a dictionary keyed by subset. The value at a subset is itself
        # a dictionary from name to bool. This should have a key for each view
        # class whose value is true iff the view class is visible for this
        # subset.
        self.visible_view_classes = None  # type: Optional[_VisDict]

    def add_expected(self, path: str) -> None:
        '''Add each line in the file at path as an expected branch

        Since branch names are a little complicated, with names like:

        'Syntax;tile_list_obu;start...;1351:
         errorStream_lst_lots_of_tiles=(tile_count_minus_1>=512);2;
         (tile_count_minus_1>=512)'

        we say that an expected branch of FOO means that we expect to see some
        branch that contains FOO as a substring.

        '''
        with open(path) as branch_file:
            for line in branch_file:
                self.expected.append(line.strip())

    def update(self,
               visible: _VisDict,
               branch_dict: Dict[str, object],
               max_subset: int) -> Dict[str, CBData]:
        '''Update branch coverage with results from a coverage run.

        branch_dict should actually have type Dict[str, CBData], but we've just
        loaded it up from a file, so will have to do some checking at runtime.

        Returns branch_dict, after checking that its entries are all
        well-typed. The point is that range coverage will use the same
        dictionary shortly afterwards, and this means we don't have to check a
        second time.

        '''

        if self.visible_view_classes is not None:
            # Make sure that our existing set of invisible view classes matches
            # what we've just read.
            if self.visible_view_classes != visible:
                raise RuntimeError('Mismatch in visible view classes. '
                                   'Had {}; just read {}.'
                                   .format(self.visible_view_classes,
                                           visible))

        self.visible_view_classes = visible

        # We take a copy here because the calls to update_visibility below
        # eventually call eval with visible as the globals dict. This turns out
        # to install a '__builtins__' dict as an extra global, which we don't
        # want. It doesn't affect other calls to update_visibility, though, so
        # we can pay the cost of a copy once here and discard when we're done.
        visible = copy.deepcopy(visible)

        for name, cbdata in branch_dict.items():
            # Invert the 2-bit "ignore this stuff" mask
            cp_mask = 3 ^ self.cstore.ignore_mask(name, 'branch', 2)

            # Skip anything that was filtered out. The mask is zero if we don't
            # care about the true or false branch.
            if not cp_mask:
                continue

            # cbdata should be a tuple of type CBData. Let's check that here.
            if not (isinstance(cbdata, tuple) and len(cbdata) == 6):
                raise RuntimeError('Element in branch dict for key `{}\' is '
                                   '{!r} (not a tuple of length 6).'
                                   .format(name, cbdata))

            (actualMin, actualMax,
             theoreticalMin, theoreticalMax,
             visibilityTrue, visibilityFalse) = cbdata

            if not (isinstance(theoreticalMin, int) and
                    isinstance(theoreticalMax, int)):
                raise RuntimeError('Theoretical range for branch {} is '
                                   '{!r}, but we expected a pair of ints.'
                                   .format(name,
                                           (theoreticalMin, theoreticalMax)))
            if not (isinstance(visibilityTrue, str) and
                    isinstance(visibilityFalse, str)):
                raise RuntimeError('Visibility for branch {} is '
                                   '{!r}, but we expected a pair of strings.'
                                   .format(name,
                                           (visibilityTrue, visibilityFalse)))

            isBoolean = theoreticalMin == 0 and theoreticalMax == 1

            # Update visibility
            self.update_visibility(name, isBoolean,
                                   visibilityTrue,
                                   (None if visibilityFalse == 'None'
                                    else visibilityFalse),
                                   visible, max_subset, cp_mask)

            # Update coverage
            self.update_coverage(name, actualMin, actualMax, isBoolean)

        return cast(Dict[str, CBData], branch_dict)

    def update_coverage(self,
                        name: str,
                        actual_min: Optional[int],
                        actual_max: Optional[int],
                        is_bool: bool) -> None:
        '''Update coverage with min/max seen for named branch.'''
        if is_bool:
            val = self.raw.get(name)
            if val is None:
                untaken, taken = False, False
                self.register_seen(name)
            else:
                assert isinstance(val, tuple) and len(val) == 2
                untaken, taken = val

                assert isinstance(untaken, bool)
                assert isinstance(taken, bool)

            if actual_max is not None and actual_max >= 1:
                taken = True
            if actual_max is not None and actual_min == 0:
                untaken = True

            self.raw[name] = (untaken, taken)
        else:
            self.register_seen(name)
            if actual_min is not None:
                self.raw[name] = True

    def calc_visibility(self,
                        parts: List[str],
                        is_boolean: bool,
                        vis_true: str,
                        vis_false: Optional[str],
                        subset: int,
                        visible_categories: Dict[str, bool],
                        mask: int) -> Tuple[bool, bool, bool]:
        '''Return a triple (vf, vt, excluded).'''
        bp = self.cstore.find_branch_profile(parts, subset)
        if bp is None:
            return (False, False, True)

        vis_t = eval_visibility(vis_true, visible_categories)
        vis_t = vis_t and (mask & 2 != 0)

        if vis_false is None:
            vis_f = vis_t
        else:
            vis_f = eval_visibility(vis_false, visible_categories)
            vis_f = vis_f and (mask & 1 != 0)

        if not is_boolean:
            vis_t = vis_t or vis_f
            vis_f = vis_t

        return (vis_f, vis_t, False)

    def update_visibility(self,
                          name: str,
                          is_boolean: bool,
                          vis_true: str,
                          vis_false: Optional[str],
                          visible_view_classes: Dict[int, Dict[str, bool]],
                          max_subset: int,
                          mask: int) -> None:
        '''Update visibility with info for a branch.

        If the name already has visibility information and it has changed, this
        returns a set of visibility changes, each of which has the form

           (name, subset, vis_true, vis_false)

        mask should be a 2-bit mask. Bit zero is false if the true branch is
        ignored and bit one is false if the false branch is ignored.

        '''
        ov = self.visibility.get(name, None)
        nv = {}
        parts = name.split(';')
        for ss in range(1, max_subset + 1):
            visible_categories = visible_view_classes[ss]
            vis_f, vis_t, excl = \
                self.calc_visibility(parts, is_boolean,
                                     vis_true, vis_false, ss,
                                     visible_categories, mask)
            nv[ss] = (vis_f, vis_t)
            if ov is not None and not excl and ov[ss] != nv[ss]:
                # Visibility has changed. That's not supposed to happen.
                raise RuntimeError('Change in visibility for branch {}: '
                                   'from {} to {}.'
                                   .format(name, ov[ss], nv[ss]))
        self.visibility[name] = nv

    def register_seen(self, name: str) -> None:
        '''Spot that a given name has now been added to the raw array.

        This might remove a partial name from the expected list.'''
        if self.expected:
            self.expected = [exp for exp in self.expected if exp not in name]

    def is_visible(self, name: str, subset: int) -> bool:
        '''Return true if the named branch is visible for this subset'''
        vis = self.visibility[name][subset]
        return vis[0] or vis[1]

    def is_covered(self, name: str, subset: int) -> bool:
        '''Return true if this branch is covered or excluded.'''
        val = self.raw.get(name)
        if val is None:
            raise ValueError('Unknown branch name: `{}\'.'.format(name))

        # Is this branch excluded in this subset? In which case, we pretend
        # that it was covered.
        if self.cstore.find_branch_profile(name.split(";"), subset) is None:
            return True

        # Otherwise, val should be a pair or a boolean.
        if isinstance(val, bool):
            return val or self.is_visible(name, subset)

        seen0 = val[0]
        seen1 = val[1]

        # Don't bother checking visibility if both cases have actually been
        # seen.
        if seen0 and seen1:
            return True

        vis0, vis1 = self.visibility[name][subset]
        return (seen0 or not vis0) and (seen1 or not vis1)

    def branch_holes(self, subset: int) -> List[str]:
        '''Return the set of branch holes for this subset.

        Expected branches (added by add_expected) are returned in angle
        brackets to make them stand out differently.

        '''
        ret = []
        for name in self.raw:
            if not self.is_covered(name, subset):
                ret.append(name)
        for exp in self.expected:
            ret.append('<{}>'.format(name))
        return ret

    @staticmethod
    def branch_messages(branch_name: str,
                        seen: bool,
                        visible: bool) -> List[str]:
        '''Return a list of messages to write about the branch.'''
        messages = []
        if not visible:
            messages.append(branch_name + ' invisible')
        if seen:
            messages.append('seen ' + branch_name)
        if not (seen or not visible):
            messages.append('not seen ' + branch_name)
        return messages

    def print_coverage(self,
                       stream: IO[str],
                       name: str, subset: int,
                       indent: int = 0) -> None:
        '''Print information about coverage for a name to stream.'''
        stream.write(' ' * indent)
        val = self.raw.get(name)
        if val is None:
            stream.write('(Unknown branch: {})\n'.format(name))
            return

        stream.write(name)
        if self.is_covered(name, subset):
            stream.write(': covered.\n')
            return

        if isinstance(val, bool):
            stream.write(': [bool val {}; visibility {}]\n'
                         .format(val, self.is_visible(name, subset)))
        else:
            seen0 = val[0]
            seen1 = val[1]

            vis0, vis1 = self.visibility[name][subset]
            messages = (BranchCoverage.branch_messages('F', seen0, vis0) +
                        BranchCoverage.branch_messages('T', seen1, vis1))
            stream.write(': [' + '; '.join(messages) + ']\n')

    def dump(self, stream: IO[str]) -> None:
        '''Dump data to the given file'''
        stream.write("bc_raw={}\n".format(repr(self.raw)))
        stream.write("bc_vis={}\n".format(repr(self.visibility)))

    def load(self, module: Dict[str, object]) -> None:
        '''Load data from the given module'''

        raw = module.get('bc_raw')
        vis = module.get('bc_vis')

        assert raw is not None
        assert vis is not None

        # Danger, Will Robinson! We're using type casts to convince MyPy that
        # all is well here, but we have just loaded some random file. Let's
        # hope it contained the right Python.
        self.raw = cast(Dict[str, _BranchCov], raw)
        self.visibility = cast(Dict[str, Dict[int, Tuple[bool, bool]]], vis)

        if self.expected:
            for name in self.raw:
                self.register_seen(name)

    def list_points(self, handle: IO[str]) -> None:
        '''Write all stored coverage points to handle.'''
        for name in self.raw.keys():
            handle.write('B {}\n'.format(name))
