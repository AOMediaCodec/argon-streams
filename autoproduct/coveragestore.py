################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Classes to implement a coverage store

This tracks the coverage that has been seen across one or more runs of the
coverage tool and allows some coverage to be ignored (with <filter> tags in the
XML that drives it).

'''

# This only applies to pylint3, but we want to inherit from Object for Python 2
# compatibility.
#
# pylint: disable=useless-object-inheritance
#
# pylint: disable=too-few-public-methods

import os
import random
import re

from typing import Dict, List, Mapping, Optional, Pattern, Set, Tuple
import xml.etree.ElementTree as ET

from ap_macros import Macros

# See docstring for get_profile_list
_ProfileList = Dict[Optional[int], Optional[List['Profile']]]
_OptProfList = Optional[_ProfileList]

_Filter = Tuple[Pattern[str], bool, Optional[int], str]


class Profile(object):
    '''A class that represents an encoder profile.

    This profile will correspond to a particular build of the encoder, but
    there are also some runtime arguments: a configuration file to use and
    possibly extra flags to pass to the encoder command.

    '''
    def __init__(self, name: str,
                 cfg: Optional[Tuple[str, Optional[str]]] = None,
                 encode_command: Optional[List[str]] = None) -> None:
        self.name = name
        self.encode_command = encode_command
        self.cfg = cfg

    def __repr__(self) -> str:
        args = [repr(self.name)]
        if self.encode_command is not None:
            args.append('encode_command=' + repr(self.encode_command))
        if self.cfg is not None:
            args.append('cfg=' + repr(self.cfg))
        return 'Profile({})'.format(', '.join(args))

    def extend_encode_command(self, base_command: List[str]) -> List[str]:
        '''Extend a command with the arguments implied by the profile'''
        ret = base_command
        if self.encode_command is not None:
            ret += self.encode_command
        if self.cfg is not None:
            (cfg_file, cfg_name) = self.cfg
            ret += ['-C', cfg_file]
            if cfg_name is not None:
                ret += ['-i', cfg_name]
        return ret


class Matcher(object):
    '''The base class for matchers for <name> or similar guards'''
    def __init__(self, tag: str, pattern: str) -> None:
        self.tag = tag
        self.pattern = pattern

    def match(self, parts: List[str]) -> Optional[List[str]]:
        '''Match parts against the guard.

        On success, return the list of remaining unmatched guards. On failure,
        return None.

        '''
        raise NotImplementedError()


class NameMatcher(Matcher):
    '''A specialised version of Matcher for <name> guards'''
    def __init__(self, name: str) -> None:
        super(NameMatcher, self).__init__('name', name)

    def match(self, parts: List[str]) -> Optional[List[str]]:
        return (parts[1:]
                if parts and parts[0] == self.pattern
                else None)


class PatternMatcher(Matcher):
    '''A specialised version of Matcher for <pattern> guards'''
    def __init__(self, pattern: str) -> None:
        super(PatternMatcher, self).__init__('pattern', pattern)

    def match(self, parts: List[str]) -> Optional[List[str]]:
        return (parts[1:]
                if parts and self.pattern in parts[0]
                else None)


class ReMatcher(Matcher):
    '''A specialised version of Matcher for <regex> guards'''
    def __init__(self, pattern: str) -> None:
        super(ReMatcher, self).__init__('regex', pattern)
        self.regex = re.compile(pattern)

    def match(self, parts: List[str]) -> Optional[List[str]]:
        if not parts:
            return None

        all_parts = ';'.join(parts)
        hit = self.regex.search(all_parts)
        if not hit:
            return None

        end = hit.end()
        assert end >= 0

        next_semi = all_parts.find(';', end)
        return ([] if next_semi < 0
                else all_parts[next_semi + 1:].split(';'))


class CovType:
    def __init__(self,
                 matcher: Optional[Matcher],
                 profiles: _OptProfList):
        self.matcher = matcher
        self.profiles = profiles


class Branch(CovType):
    def __init__(self,
                 matcher: Optional[Matcher],
                 profiles: _OptProfList,
                 parent: Optional['Branch']) -> None:
        super().__init__(matcher, profiles)

        self.parent = parent

        # The children field is a double dictionary keyed by the type (tag) of
        # matcher. Each values is itself a dictionary keyed by the name or
        # pattern whose values are Branch objects.
        self.children = {}  # type: Dict[str, Dict[str, 'Branch']]


class Range(CovType):
    def __init__(self,
                 matcher: Optional[Matcher],
                 profiles: _OptProfList) -> None:
        super().__init__(matcher, profiles)


class Cross(CovType):
    def __init__(self,
                 matcher: Optional[Matcher],
                 profiles: _OptProfList) -> None:
        super().__init__(matcher, profiles)


def read_subset(macros: Macros, elem: ET.Element) -> Optional[int]:
    '''Read the subset attribute of an element, converting to int

    If there is no subset, return None.

    '''
    subset = macros.replace(elem.get('subset'))
    if subset is None:
        return None
    try:
        return int(subset)
    except ValueError:
        raise RuntimeError('Subset `{}\' is not a valid integer.'
                           .format(subset))


def read_profile(macros: Macros,
                 elem: ET.Element,
                 filename: str) -> Tuple[Optional[int], Profile]:
    '''Read a profile tag.

    The full syntax for a profile tag is:

        <profile subset="2">
          <name>foo_bar_baz</name>
          <encodeCommand>some-args</encodeCommand>
          <config>cfg-file-name:cfg-name</config>
        </profile>

    as a short-hand, a profile that doesn't define anything but a name can be
    written

        <profile subset="2">foo_bar_baz</profile>

    The subset attribute says what subset this profile should apply to. If
    omitted, this profile applies to all subsets.


    The function returns a pair (subset, profile), where subset is either None
    or an integer and profile is a Profile object.

    filename is the name of the XML file that we're reading, relative to the
    current working directory. We use it to resolve paths for config files.

    '''
    subset = read_subset(macros, elem)
    if not list(elem):
        # elem has no children. Use its text value as the profile name.
        name = macros.replace(elem.text)
        if not name:
            raise RuntimeError('Empty profile element.')
        profile = Profile(name)
    else:
        num_found = 0

        name_elem = elem.find('name')
        if name_elem is None:
            raise RuntimeError('Non-empty profile element has no <name>.')
        name = macros.replace(name_elem.text)
        if not name:
            raise RuntimeError('Non-empty profile element has empty <name>.')

        num_found += 1

        cmd = None
        cmd_elem = elem.find('encodeCommand')
        if cmd_elem is not None:
            cmd_txt = macros.replace(cmd_elem.text)
            if not cmd_txt:
                raise RuntimeError('No encode command in encodeCommand tag.')
            cmd = cmd_txt.split(' ')
            num_found += 1

        cfg = None
        cfg_string = macros.replace(elem.findtext('config'))
        if cfg_string is not None:
            num_found += 1
            # We expect cfg_string to be FILENAME:CFG or FILENAME.
            cfg_parts = cfg_string.split(':', 1)

            cfg_path = os.path.join(os.path.dirname(filename), cfg_parts[0])
            cfg_path = os.path.normpath(cfg_path)

            cfg = (cfg_path, None if len(cfg_parts) <= 1 else cfg_parts[1])

        if len(elem) != num_found:
            raise RuntimeError('Unexpected children of profile element '
                               '(read {}, but the element has {}).'
                               .format(num_found, len(elem)))

        profile = Profile(name, cfg=cfg, encode_command=cmd)

    return (subset, profile)


def read_matcher(macros: Macros, elem: ET.Element) -> Optional[Matcher]:
    '''Find a Matcher as a child of elem, or return None'''
    matcher = None  # type: Optional[Matcher]
    name = macros.replace(elem.findtext("name"))
    if name is not None:
        matcher = NameMatcher(name)

    pattern = macros.replace(elem.findtext("pattern"))
    if pattern is not None:
        if matcher is not None:
            raise RuntimeError('Both <name> and <pattern> defined.')
        matcher = PatternMatcher(pattern)

    regex = macros.replace(elem.findtext("regex"))
    if regex is not None:
        if matcher is not None:
            raise RuntimeError('<regex> defined as well as '
                               '<name> or <pattern>.')
        matcher = ReMatcher(regex)

    return matcher


def get_profile_list(macros: Macros,
                     elem: ET.Element,
                     filename: str) -> Tuple[_OptProfList, Set[str]]:
    '''Find direct child profiles of elem.

    Returns a pair (d, names). d is a dictionary keyed by subset, with a
    special entry keyed by None. If this level doesn't mention a subset, there
    is no entry for that key. If the level excludes a subset, there is an entry
    with value None. If the level specifies a profile for the subset, the entry
    is a list of one or more Profile objects.

    names is a set of the names of all Profile objects that were found.

    '''
    profile_elts = elem.findall("profile")
    exclude_elts = elem.findall("exclude")
    if not (profile_elts or exclude_elts):
        return (None, set())

    res = {}  # type: _ProfileList
    names = set()
    for profile_elt in profile_elts:
        subset, profile = read_profile(macros, profile_elt, filename)
        so_far = res.setdefault(subset, [])
        if so_far is None:
            raise RuntimeError('Exclude then profile elements found for '
                               'the same subset: {}'.format(elem))
        so_far.append(profile)
        names.add(profile.name)

    for exclude_elt in exclude_elts:
        subset = read_subset(macros, exclude_elt)
        if subset in res:
            raise RuntimeError("Profile and exclude elements "
                               "found for the same subset: {}".format(elem))
        res[subset] = None

    return (res, names)


def choose_profile(profiles: _OptProfList, subset: int) -> Optional[Profile]:
    '''Choose a random profile for this subset

    This has special support for if profiles is None, in which case we treat it
    as an empty dictionary (and return None)

    '''
    if profiles is None:
        return None

    ss_profiles = profiles[subset] if subset in profiles else profiles[None]
    return None if ss_profiles is None else random.choice(ss_profiles)


class CoverageStore(object):
    '''A class representing the coverage we've seen and expect to see'''

    def __init__(self) -> None:
        self.all_profiles = set()  # type: Set[str]
        self.branch_root = None  # type: Optional[Branch]

        self.default_range_profiles = None  # type: _OptProfList
        self.default_cross_profiles = None  # type: _OptProfList

        # Keyed by type of matcher; each value is a dictionary keyed by the
        # name or pattern with value a Range
        self.ranges = {}  # type: Dict[str, Dict[str, Range]]
        # Like ranges, but with Cross objects.
        self.crosses = {}  # type: Dict[str, Dict[str, Cross]]
        self.filters = {}  # type: Dict[str, List[_Filter]]

    def store_branch(self,
                     filename: str,
                     macros: Macros,
                     elem: ET.Element,
                     parent: Optional[Branch] = None) -> None:
        '''Add branch rules from the XML'''
        matcher = read_matcher(macros, elem)
        if matcher is not None and parent is None:
            raise RuntimeError('Found guard in root branch element.')
        if parent is not None and matcher is None:
            raise RuntimeError('Found branch element with parent but '
                               'no guard. Parent: {}.'
                               .format(parent.matcher))
        profiles, all_new = get_profile_list(macros, elem, filename)
        self.all_profiles |= all_new

        dest = None
        if parent:
            assert matcher is not None
            siblings = parent.children.setdefault(matcher.tag, {})
            dest = siblings.get(matcher.pattern)
            if dest:
                dest.profiles = profiles
            else:
                dest = Branch(matcher, profiles, parent)
                siblings[matcher.pattern] = dest
        else:
            assert matcher is None

            if self.branch_root is None:
                dest = Branch(None, profiles, parent)
                self.branch_root = dest
            else:
                self.branch_root.profiles = profiles
                dest = self.branch_root

        assert dest is not None
        for subbranch in elem.findall("branch"):
            self.store_branch(filename, macros, subbranch, dest)

    @staticmethod
    def flat_find_match(to_search: Mapping[str, Mapping[str, CovType]],
                        parts: List[str]) -> Tuple[Optional[CovType],
                                                   Optional[List[str]]]:
        '''Find a match for parts (non-recursive)

        to_search is a dictionary from tag (name, pattern or regex) to a
        dictionary keyed by the pattern to match against with values that are
        Matcher objects.

        There is a precedence order for different types of matcher: name,
        pattern, regex.

        '''
        for tag in ['name', 'pattern', 'regex']:
            # Within each type, search in descending lex order of pattern. (It
            # shouldn't really matter what order we choose here, but it's
            # probably not a good idea to order by Python hash, since that will
            # jump around all over the place).
            for_tag = to_search.get(tag)
            if not for_tag:
                continue
            for key in reversed(sorted(for_tag.keys())):
                elt = for_tag[key]

                # This should only be called with branches that have non-None
                # matchers (which should be everything but the root branch)
                assert elt.matcher is not None

                rem_parts = elt.matcher.match(parts)
                if rem_parts is not None:
                    return (elt, rem_parts)
        return (None, None)

    @staticmethod
    def flat_find_profiles(to_search: Mapping[str, Mapping[str, CovType]],
                           parts: List[str],
                           default: _OptProfList) -> _OptProfList:
        '''Find a profiles map for the given name

        to_search should be a double dictionary of matchers (see
        flat_find_match). The result is a map from subset to profile.

        '''
        ret, _ = CoverageStore.flat_find_match(to_search, parts)
        return ret.profiles if ret is not None else default

    def find_branch_profile(
            self,
            parts: Optional[List[str]],
            subset: int,
            branch_root: Optional[Branch] = None) -> Optional[Profile]:
        '''Find a profile that matches the given branch name'''
        branch_root = branch_root or self.branch_root
        if branch_root is None:
            return None

        if parts:
            child, rem_parts = \
                CoverageStore.flat_find_match(branch_root.children, parts)

            if child is not None:
                assert isinstance(child, Branch)
                return self.find_branch_profile(rem_parts, subset, child)

        # If we get here, either we have run out of branch parts or none of our
        # children match those parts that we have left. Search upwards from
        # this Branch object to find one with some profiles.
        while branch_root.profiles is None:
            branch_root = branch_root.parent
            if branch_root is None:
                return None

        return choose_profile(branch_root.profiles, subset)

    def store_range(self,
                    filename: str,
                    macros: Macros,
                    elem: ET.Element) -> None:
        '''Add range rules from the XML'''
        matcher = read_matcher(macros, elem)
        profiles, all_new = get_profile_list(macros, elem, filename)
        assert profiles is not None
        self.all_profiles |= all_new

        if matcher is None:
            self.default_range_profiles = profiles
        else:
            ranges = self.ranges.setdefault(matcher.tag, {})
            ranges[matcher.pattern] = Range(matcher, profiles)

    def find_range_profiles(self,
                            name: str,
                            allow_default: bool = True) -> _OptProfList:
        '''Find profiles map for the given range name'''
        default = self.default_range_profiles if allow_default else None
        return CoverageStore.flat_find_profiles(self.ranges, [name], default)

    def find_range_profile(self, name: str, subset: int) -> Optional[Profile]:
        '''Find a profile for the given range name and subset'''
        return choose_profile(self.find_range_profiles(name), subset)

    def has_specific_range_profile(self, name: str) -> bool:
        '''Returns true if this range has a profile explicitly associated.'''
        return self.find_range_profiles(name, allow_default=False) is not None

    def store_cross(self,
                    filename: str,
                    macros: Macros,
                    elem: ET.Element) -> None:
        '''Add cross rules from the XML'''
        matcher = read_matcher(macros, elem)
        profiles, all_new = get_profile_list(macros, elem, filename)
        if profiles is None:
            raise RuntimeError('<cross> element has no profiles '
                               'or config elements.')
        self.all_profiles |= all_new

        if matcher is None:
            self.default_cross_profiles = profiles
        else:
            crosses = self.crosses.setdefault(matcher.tag, {})
            crosses[matcher.pattern] = Cross(matcher, profiles)

    def find_cross_profiles(self,
                            name: str,
                            allow_default: bool = True) -> _OptProfList:
        '''Find profiles map for the given cross name'''
        default = self.default_cross_profiles if allow_default else None
        return CoverageStore.flat_find_profiles(self.crosses, [name], default)

    def find_cross_profile(self, name: str, subset: int) -> Optional[Profile]:
        '''Find a profile for the given cross name and subset'''
        return choose_profile(self.find_cross_profiles(name), subset)

    def has_specific_cross_profile(self, name: str) -> bool:
        '''Returns true if this cross has a profile explicitly associated.'''
        return self.find_cross_profiles(name, allow_default=False) is not None

    @staticmethod
    def parse_cross_mask(line: str) -> int:
        '''Parse a cross filter and return the associated mask'''
        match = re.match(r'x[ ]*<[ ]*([0-9]+)$', line)
        if match is not None:
            # A mask of the form x < N
            return (1 << int(match.group(1))) - 1

        match = re.match(r'x\s+in\s*\{\s*([0-9]+\s*(?:,\s*[0-9]+\s*)*)?\}$',
                         line)
        if match is not None:
            # A mask of the form "x in {a,b,c,d}".
            nums_str = match.group(1)
            if nums_str is None:
                # We had "x in {}"
                nums = []
            else:
                # The int conversion should never fail (if we got the regex
                # right)
                nums = [int(x.strip()) for x in nums_str.split(',')]

            mask = 0
            for num in nums:
                mask |= 1 << num

            return mask

        raise ValueError('Cross filter values="{}" ill formed.'.format(line))

    def add_filter(self,
                   filter_type: str,
                   pattern: str,
                   action: str,
                   values: Optional[str]) -> None:
        '''Add a filter to the coverage that we track

        The filter_type must be one of 'branch', 'range', 'cross' or
        'impossible'.

        The 'branch', 'range' and 'cross' types affect branch, range and cross
        coverage. If one of our filters doesn't match a given coverage point,
        we won't bother tracking it (and won't try to fill it either). The
        'impossible' filter type affects the impossible cases check (which is a
        type of cross coverage, but you might want to disable it separately).

        Action should be 'require' or 'reject'. If require, a cover point must
        match this filter in order to be used. If reject, a cover point must
        not match this filter in order to be used.

        values is only used for branch or cross coverage. For branches, it it
        should be None, 'both', 'false' or 'true'. If 'true', the filter only
        applies to the true branch; if 'false', it only applies to the false
        branch. If None or 'both', it applies to both sides.

        For cross coverage, values is a simple filter that describes which bits
        we expect to hit (is_cover_point_relevant). For now, it's expected to
        be of the form "x < N" where "x" is a literal "x" and N is a number or
        of the form "x in {N0,N1,N2}" (for arbitrarily many Ni).

        '''
        known_types = ['branch', 'range', 'cross', 'impossible']
        if filter_type not in known_types:
            raise ValueError('Unknown filter type: `{}\'. '
                             'Supported types: {}.'
                             .format(filter_type, known_types))

        if action == 'require':
            require = True
        elif action == 'reject':
            require = False
        else:
            raise ValueError('Unknown action, `{}\' for `{}\' filter '
                             'with pattern `{}\' '
                             '(expected "require" or "reject").'
                             .format(action, filter_type, pattern))

        mask = None
        if values is not None:
            if filter_type == 'branch':
                masks = {'both': 3, 'false': 1, 'true': 2}
                mask = masks.get(values, None)
                if mask is None:
                    raise ValueError('Unknown side: `{}\'.'.format(values))
            elif filter_type == 'cross':
                mask = CoverageStore.parse_cross_mask(values)
            else:
                raise ValueError('values should not be specified for '
                                 'filter type {}'
                                 .format(filter_type))

        filters = self.filters.setdefault(filter_type, [])
        filters.append((re.compile(pattern), require, mask, pattern))

    def ignore_mask(self, name: str, point_type: str, size: int) -> int:
        '''Return a mask where a bit is set if the coverage point is ignored

        Size is positive and is the number of bits in the mask.'''
        assert size > 0
        all_bits = (1 << size) - 1

        full_mask = 0
        for filt, require, mask, _ in self.filters.get(point_type, []):
            match = filt.match(name)

            if match:
                if not require:
                    # This rule rejects some or all of the current name.
                    if mask is None:
                        mask_delta = all_bits
                    else:
                        mask_delta = mask
                else:
                    # This rule requires some or all of the current name. If
                    # there is no mask, we're good ("I'm only interested in
                    # coverage point FOO and this is FOO"). Otherwise, we need
                    # to invert the mask to figure out what to ignore.
                    if mask is None:
                        mask_delta = 0
                    else:
                        mask_delta = all_bits ^ mask
            else:
                if not require:
                    # This rule rejects stuff but doesn't match. Nothing to see
                    # here.
                    mask_delta = 0
                else:
                    # This match requires stuff and doesn't match. We should
                    # ignore the whole thing.
                    mask_delta = all_bits

            full_mask |= mask_delta

        top_bits = full_mask >> size
        if top_bits:
            raise RuntimeError('The coverage point called `{}\' (type: {}) '
                               'claims a size of {}, but has an ignore mask '
                               'of {:#x} with bits set higher than that. '
                               'After right shifting, we have {:#x}.'
                               .format(name, point_type, size, full_mask,
                                       top_bits))

        return full_mask

    def is_cover_point_relevant(self,
                                name: str,
                                point_type: str,
                                size: int) -> bool:
        '''Returns true if the coverage point isn't completely ignored

        The size parameter gives the width of the possible "ignore mask", so
        we're just interested in bits less than size that are zero in
        ignore_mask.

        '''
        # Note that there won't be any bits above size set (because that would
        # trigger an error in ignore_mask())
        size_mask = (1 << size) - 1
        return (self.ignore_mask(name, point_type, size) ^ size_mask) != 0
