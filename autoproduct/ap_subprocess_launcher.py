################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import threading
import signal
import sys
import time
import re
import os
import subprocess

DEV_NULL = open("/dev/null", "w")


class ProcType:
  def __init__(self, pipe_stdout=False, pipe_stderr=False,
               allow_shell=False, has_grep_str=False):
    self.pipe_stdout = pipe_stdout
    self.pipe_stderr = pipe_stderr
    self.allow_shell = allow_shell
    self.has_grep_str = has_grep_str


PROC_TYPES = {
  'LAUNCH_NO_OUTPUT': ProcType(),
  'LAUNCH_WITH_STDOUT': ProcType(pipe_stdout=True),
  'LAUNCH_WITH_STDERR': ProcType(pipe_stderr=True),
  'LAUNCH_WITH_STDOUT_AND_STDERR': ProcType(pipe_stdout=True,
                                            pipe_stderr=True),
  'LAUNCH_WITH_GREP_STDOUT': ProcType(pipe_stdout=True, has_grep_str=True),
  'LAUNCH_WITH_STDOUT_FILE': ProcType(pipe_stdout='file'),
  'LAUNCH_WITH_STDOUT_AND_STDERR_FILE': ProcType(pipe_stdout='file',
                                                 pipe_stderr='file'),
  'LAUNCH_WITH_STDOUT_CONSOLE': ProcType(pipe_stdout='console'),
  'LAUNCH_WITH_STDOUT_AND_STDERR_CONSOLE': ProcType(pipe_stdout='console',
                                                    pipe_stderr='console')
}


class CAPProcess:
  def __init__(self, arg, processNumber, ptype, process, grepString, oStream):
    self.arg = arg
    self.processNumber = processNumber
    self.ptype = ptype
    self.process = process
    self.grepString = grepString
    self.oStream = oStream

    self.out = None
    self.err = None
    self.rawout = None
    self.thread = None

    if process is None:
      return

    def monitor():
      self.out, self.err = self.process.communicate()
      self.rawout = self.out
      if self.grepString is not None:
        lines = self.out.split("\n")
        lines = [x for x in lines if self.grepString in x]
        self.out = "\n".join(lines)

    self.thread=threading.Thread(target=monitor)
    self.thread.daemon=True
    self.thread.start()


def fd_argument(arg_type, command_data, out_file):
  if arg_type is False:
    return DEV_NULL
  elif arg_type is True:
    return subprocess.PIPE
  elif arg_type == 'file':
    assert out_file is not None
    return out_file
  elif arg_type == 'console':
    return None
  else:
    raise RuntimeError('Unknown fd arg type: `{}\'.'.format(arg_type))


def monitor_stdin():
  global processNumber
  while True:
    rline=_inStream.readline()
    if rline=="":
      return # Pipe closed - kill this thread
    line=rline.strip()
    cmd,sp,args=line.partition(" ")
    try:
      d=eval(args)
    except:
      continue

    if cmd == 'QUIT':
      return

    pt = PROC_TYPES.get(cmd, None)
    if pt is None:
      continue

    oStream = None
    if pt.pipe_stdout == 'file' or pt.pipe_stderr == 'file':
      filename = d.get('file', None)
      append = d.get('append', False)
      if filename is None:
        raise RuntimeError('Command has arg_type=file, but no file entry.')
      oStream = open(filename, 'w+' if append else 'w')

    sp_stdout = fd_argument(pt.pipe_stdout, d, oStream)
    sp_stderr = fd_argument(pt.pipe_stderr, d, oStream)
    strGrep = d['grepString'] if pt.has_grep_str else None

    try:
      pr = subprocess.Popen(d["args"],
                            stdout=sp_stdout, stderr=sp_stderr,
                            shell=d.get('shell', False),
                            cwd=d.get('cwd', None))
    except OSError:
      sys.stderr.write('Failed to run command: {}\n'
                       .format(' '.join(d['args'])))
      pr = None

    with lock:
      _outStream.write("N {}\n".format(processNumber))
      processes.append(CAPProcess(d, processNumber, pt, pr, strGrep, oStream))
      processNumber += 1

def fixends(s):
  lines=s.split("\n")
  for i in xrange(len(lines)):
    if re.match("^-*END$", lines[i])!=None:
      lines[i]="-{}".format(lines[i])
  return "\n".join(lines)

def writeline(str):
  _outStream.write("{}\n".format(str))

def quit():
  #Read and discard remaining input
  while True:
    if _inStream.readline()=="":
      break
  os.killpg(os.getpgid(0), signal.SIGTERM)
  _inStream.close()
  _outStream.close()
  sys.exit(0)

def launch(inStream, outStream):
  global _inStream, _outStream
  global lock, processes, processNumber

  _inStream=inStream
  _outStream=outStream
  os.setpgrp()
  signal.signal(signal.SIGINT, signal.SIG_IGN)
  processNumber=1
  processes=[]
  lock=threading.Lock()
  stdinThread=threading.Thread(target=monitor_stdin)
  stdinThread.daemon=True
  stdinThread.start()

  while True:
    # If the stdin thread has died, kill everything and exit
    if not stdinThread.is_alive():
      quit()
    if os.getppid()==1:
      sys.exit(0)
    with lock:
      finished=[]
      for p in processes:
        if p.process and p.thread.is_alive():
          continue
        finished.append(p)

        retcode = p.process.returncode if p.process else -1
        if retcode != 0:
          with open("ap_subprocess_launcher_debug.txt", "w") as debugf:
            debugf.write("Process returned {}\n".format(retcode))
            debugf.write("Command: {}\n".format(" ".join(p.arg["args"])))
            debugf.write("STDOUT: {}\n".format(p.rawout))
            debugf.write("STDERR: {}\n".format(p.err))

        writeline("F {} {}".format(p.processNumber, retcode))
        if p.ptype.pipe_stdout is True:
          writeline("STDOUT")
          writeline(fixends(p.out or ''))
          writeline("END")
        if p.ptype.pipe_stderr is True:
          writeline("STDERR")
          writeline(fixends(p.err or ''))
          writeline("END")
        writeline('END')
        _outStream.flush()

        if p.oStream is not None:
          p.oStream.close()

      for p in finished:
        processes.remove(p)

    time.sleep(0.1)
