################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import multiprocessing
import shutil
import os
import sys
import re
import subprocess

from typing import Dict, Iterable, List, Optional


def rm_f(path: str, recursive: bool = False) -> None:
    '''Like rm -f: delete a file if it exists.

    If path is None, it is ignored (a bit like free() in C)

    '''
    if path is None:
        return

    try:
        if recursive:
            shutil.rmtree(path)
        else:
            os.remove(path)

        return
    except OSError:
        pass

    # If we get here, we saw an OSError. Since Python 2 isn't massively
    # helpful, we can't easily tell from the error whether that was "permission
    # denied" or "no such file". So we do the other (racy) thing and check
    # whether the file now exists.
    if os.path.exists(path):
        raise RuntimeError('Tried to delete {} but it still exists.'
                           .format(path))


def zip_up(path: str) -> None:
    '''Zip up the data at path (making path.gz)'''
    try:
        cmd = ['gzip', '--fast', '-f', path]
        subprocess.check_call(cmd)
    except subprocess.CalledProcessError:  # type: ignore
        raise RuntimeError('Failed to gzip `{}\'.'.format(path)) from None


def progress(msg: str) -> None:
    '''Print msg to stdout and flush.

    This should be used for "I've got here"-style progress messages and means
    that you can pipe the output of autoproduct through something that implies
    more than just line buffering, and still see something interesting.

    '''
    print(msg)
    sys.stdout.flush()


# This is the path to the root of the HEVC project checkout
ROOT_DIR = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))

# This is the path to the tools directory
TOOLS_DIR = os.path.join(ROOT_DIR, 'tools')

maxThreads = multiprocessing.cpu_count()


def setMaxThreads(mt: int) -> None:
    global maxThreads
    maxThreads = min(maxThreads, mt)


def getMaxThreads() -> int:
    return maxThreads


buildtoolOptions = []  # type: List[str]


CHECK_TABLE = {}  # type: Dict[str, str]


def store_check(tag: str, action: str) -> None:
    '''Teach buildtools to ignore some decoder errors'''
    if action not in ['reject', 'ignore']:
        raise ValueError('Invalid action: {}'.format(action))
    if tag in CHECK_TABLE:
        raise ValueError('Duplicate check tag: {}'.format(tag))

    CHECK_TABLE[tag] = action


class CProfile:
    def __init__(self,
                 name: str,
                 functions: str,
                 threadLimit: Optional[int],
                 usesChooseAll: bool,
                 nonMergeable: bool) -> None:
        self.name = name
        self.functions = functions
        self.threadLimit = threadLimit
        self.usesChooseAll = usesChooseAll
        self.nonMergeable = nonMergeable


profileTable = {}  # type: Dict[str, CProfile]


def storeProfile(name: str,
                 functions: str,
                 threadLimit: Optional[int],
                 usesChooseAll: bool,
                 nonMergeable: bool = False,
                 duplicate_allowed: bool = False) -> None:
    if name in profileTable:
        if duplicate_allowed:
            return

        raise RuntimeError('Duplicate profiles with name {}.'
                           .format(name))

    profileTable[name] = CProfile(name, functions, threadLimit,
                                  usesChooseAll, nonMergeable)


def getProfileThreadLimit(profile_name: str) -> int:
    profile = profileTable.get(profile_name)
    if profile is None or profile.threadLimit is None:
        return maxThreads

    return profile.threadLimit


def getProfileUsesChooseAll(profile_name: str) -> bool:
    profile = profileTable.get(profile_name)
    return (profile is not None and profile.usesChooseAll)


def getProfileNonMergeable(profile_name: str) -> bool:
    profile = profileTable.get(profile_name)
    return (profile is not None and profile.nonMergeable)


def find_binary(basename: str,
                bin_dir: str,
                other_bindirs: List[str],
                force: bool = True) -> Optional[str]:
    '''Search for the given basename in bin_dir then other_bindirs'''
    dirs = [bin_dir]
    if other_bindirs is not None:
        dirs += other_bindirs

    for directory in dirs:
        path = os.path.join(directory, basename)
        if os.path.exists(path):
            return path

    if force:
        raise RuntimeError("Can't find the binary '{}' in any of the "
                           "following directories: {!r}"
                           .format(basename, dirs))
    return None


def buildEncoder(binDir: str,
                 buildDir: str,
                 buildlogDir: str,
                 profile: str,
                 codec: str,
                 other_profiles: List[str] = [],
                 other_bindirs: List[str] = [],
                 log_name: Optional[str] = None,
                 max_threads: Optional[int] = None) -> str:
    # Only build encoders when needed. profile is the PROFILE argument to the
    # Make command, or a key into the ProfileTable other_profiles is used to
    # duplicate identical encoders, other_profiles should be a list of the
    # 'profile' arguments (including this one) that have the same PROFILE
    # argument to the Make command

    # If the binary exists already, there's nothing to do.
    binary = find_binary("{}.enc.exe".format(profile),
                         binDir, other_bindirs, force=False)
    if binary:
        return binary

    binary = os.path.join(binDir, "{}.enc.exe".format(profile))

    if buildDir is None:
        buildDir = os.path.join(TOOLS_DIR, "build")
    buildDir = os.path.realpath(buildDir)
    extraMakeOptions = []
    encTarget = 1
    encDir = "enc"
    enc_codec = codec
    if codec == "av1b":
        encTarget = 17
        encDir = "av1enc"
        enc_codec = 'av1'
    extraMakeOptions.append("P8005_BUILD_PREFIX="+buildDir)

    if max_threads is None:
        max_threads = maxThreads

    print("  Cleaning build directory...")
    command = (["make", "-C", TOOLS_DIR, "-j{}".format(max_threads)] +
               extraMakeOptions + ["clean"])
    try:
        subprocess.check_call(command)
    except subprocess.CalledProcessError:  # type: ignore
        raise RuntimeError("  Failed to clean build directory (command: {})"
                           .format(' '.join(command))) from None

    print("  Building encoder for profile {}...".format(profile))
    if log_name is None:
        log_name = os.path.join(buildlogDir, "{}.buildenc.log".format(profile))
    functions = profile
    if profile in profileTable:
        functions = profileTable[profile].functions

    command = (["make", "-C", TOOLS_DIR, "-j{}".format(max_threads)] +
               extraMakeOptions +
               ["TARGET={}".format(encTarget), "PROFILE={}".format(functions),
                "{}/build/{}/{}.gen.exe".format(buildDir, encDir, enc_codec)])
    try:
        with open(log_name, 'w') as handle:
            subprocess.check_call(command, stdout=handle, stderr=handle)
    except subprocess.CalledProcessError:  # type: ignore
        raise RuntimeError("  Failed to build encoder for profile {}"
                           .format(profile)) from None

    shutil.copy2(os.path.join(buildDir, "build", encDir,
                              "{}.gen.exe".format(enc_codec)),
                 binary)

    # There are duplicate encoders for other profiles:
    for other_profile in other_profiles:
        if profile != other_profile:
            print("  Duplicating encoder for {}".format(other_profile))
            dst = os.path.join(binDir, "{}.enc.exe".format(other_profile))
            shutil.copy2(os.path.join(buildDir, "build", encDir,
                                      "{}.gen.exe".format(enc_codec)),
                         dst)

    return binary


def build_av1b_ref_decoder(build_dir: str,
                           log_dir: str,
                           maxThreads: int,
                           buildtoolOptions: List[str]) -> List[str]:
    '''Fetch and build the libaom decoders'''

    # We want to download the source code to build_dir/aom and then we'll build
    # it in build_dir/build/aom (which matches build_dir/build/av1dec etc.)
    src_dir = os.path.join(build_dir, 'aom')
    aom_build = os.path.join(build_dir, 'build', 'aom')

    # We'll fetch and build with get-aomdec.sh, which is found in the
    # av1product directory.
    script = os.path.join(ROOT_DIR, 'av1product', 'get-aomdec.sh')
    if not os.path.exists(script):
        raise RuntimeError('No script at {}.'.format(script))

    cmd = [script, '-j{}'.format(maxThreads), src_dir, aom_build]
    subprocess.check_call(cmd)

    # Check that we managed to build something
    binaries = [os.path.join(aom_build, 'aomdec'),
                os.path.join(aom_build, 'examples',
                             'lightfield_tile_list_decoder')]
    for path in binaries:
        if not os.path.exists(path):
            raise RuntimeError('Building libaom didn\'t create {}.'
                               .format(path))

    return binaries


class Tool:
    '''A Python object representing an encoder or decoder to be built'''
    def __init__(self,
                 human_name: str,
                 basename: str,
                 build_path: str,
                 target_num: int):
        self.human_name = human_name
        self.basename = basename
        self.build_path = build_path
        self.target_num = target_num

    def find(self, bin_dir: str, other_bindirs: List[str]) -> Optional[str]:
        '''If we can find the tool, return a path to it. Otherwise None.'''
        return find_binary(self.basename, bin_dir, other_bindirs, force=False)


AV1B_TOOLS = {
    'cover': Tool('coverage tool', 'cover.exe', 'av1bcover/av1b.gen.exe', 21),
    'decoder': Tool('C decoder', 'decoder.exe', 'av1bdec/av1b.gen.exe', 20),
}

ALL_TOOLS = {
    'av1b': AV1B_TOOLS
}


def build_tools(codec: str, bin_dir: str, build_dir: str, log_dir: str,
                rebuild: bool, other_bindirs: List[str]) -> None:
    '''Build some tools'''

    tool_dict = ALL_TOOLS.get(codec)
    if tool_dict is None:
        raise RuntimeError('Unknown codec: {}'.format(codec))

    tools_needed = []
    for tool_name, tool in tool_dict.items():
        # Maybe we have the tool already?
        if not rebuild:
            found = tool.find(bin_dir, other_bindirs)
            if found:
                progress("  Using existing {} binary '{}'"
                         .format(tool.human_name, found))
                continue

        tools_needed.append(tool)

    if not tools_needed:
        return

    progress("  Building {} into {}..."
             .format(list(tool.basename for tool in tools_needed),
                     bin_dir))

    abs_build_dir = os.path.realpath(build_dir)

    with open(os.path.join(log_dir, 'build.log'), 'w') as log:
        targets = ' '.join(str(tool.target_num) for tool in tools_needed)
        par_args = ['-j', str(maxThreads)] if maxThreads > 1 else []
        command = (['make', '-C', TOOLS_DIR, '-f', 'many.mk',
                    'P8005_BUILD_PREFIX={}'.format(abs_build_dir),
                    'RELEASE=0', 'TARGETS={}'.format(targets)] +
                   par_args +
                   ['c-build'])

        try:
            subprocess.check_call(command, stdout=log, stderr=log)
        except OSError as e:
            raise RuntimeError('Failed to start command "{}" ({})'
                               .format(' '.join(command), e))
        except subprocess.CalledProcessError as e:  # type: ignore
            raise RuntimeError('Command "{}" failed. ({})'
                               .format(' '.join(command), e))

    for tool in tools_needed:
        src = os.path.join(build_dir, 'build', tool.build_path)
        dst = os.path.join(bin_dir, tool.basename)
        shutil.copy2(src, dst)


def buildTools(binDir: str,
               buildDir: str,
               buildlogDir: str,
               profiles: Iterable[str],
               fullRebuild: bool,
               codec: str,
               other_bindirs: List[str]) -> None:

    # We don't want to delete stuff from elsewhere, but we also wouldn't be
    # force the rebuild properly. Let's just not allow this case.
    assert not (fullRebuild and other_bindirs)

    if fullRebuild:
        print("Removing Encoders for full rebuild")
        for profile in profiles:
            rm_f(os.path.join(binDir, "{}.enc.exe".format(profile)))

    build_tools(codec, binDir, buildDir, buildlogDir,
                rebuild=fullRebuild, other_bindirs=other_bindirs)


def innocuous_checks(stdout_path: str) -> List[str]:
    '''Run this with the stdout from a run.

    The result is true if the failure is innocuous in some way
    (probably, we failed some decode model check and should ignore
    this stream).

    '''
    tags = set()

    with open(stdout_path, 'r') as stdout:
        for line in stdout:
            if 'Check Failed' not in line:
                continue

            match = re.match(r".*\[([^\]]*)\]", line)
            if not match:
                continue

            tag = match.group(1)

            which_check = CHECK_TABLE.get(tag)
            if which_check is None:
                # We don't recognise this tag. Ignore it.
                continue

            # We currently only support which_check == reject
            assert which_check == 'reject'
            tags.add(tag)

    return list(tags)
