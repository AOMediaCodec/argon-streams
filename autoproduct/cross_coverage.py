################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''A module tracking cross coverage for autoproduct'''

from typing import Dict, IO, List, Optional, Tuple

from coveragestore import CoverageStore
from configuration import Configuration

# A map from subset index or view name to the bits that are impossible for that
# subset.
_ImpossibleCaseTable = Dict[int, int]
_SImpossibleCaseTable = Dict[str, int]

_CCTuple = Tuple[int, int, _ImpossibleCaseTable]
_CCSTuple = Tuple[int, int, _SImpossibleCaseTable]


def show_bitfield(bits: int) -> str:
    '''Describe a bitfield as a list of intervals'''
    posedges = bits & ~ (bits << 1)
    negedges = bits & ~ (bits >> 1)

    ivls = []
    start = None
    i = 0

    while negedges:
        if start is None:
            if posedges & 1:
                start = i

        if negedges & 1:
            assert start is not None
            ivls.append((start, i))
            start = None

        i += 1
        posedges >>= 1
        negedges >>= 1

    assert not negedges

    fields = []
    for (lo, hi) in ivls:
        fields.append('{}..{}'.format(lo, hi)
                      if lo < hi else str(lo))

    return 'bits({})'.format(', '.join(fields))


def list_bits(mask: int) -> List[int]:
    '''Return a list of the bits that are set in mask'''
    ret = []
    bit = 0
    while mask:
        if mask & 1:
            ret.append(bit)
        bit += 1
        mask >>= 1
    return ret


class CCPoint:
    def __init__(self, size: Optional[int]) -> None:
        # size is the number of bits in the bitfield.
        self.size = size

        # A bitfield of what we've hit.
        self.seen = 0

        # A dict keyed by subset index whose value is the bitfield of values
        # thought impossible for that subset.
        self.impossible = None  # type: Optional[Dict[int, int]]

    def update(self,
               seen: int,
               impossible: int,
               size: int,
               subset: int) -> None:
        '''Update this coverage point with new bitfields'''
        self.seen |= seen

        # Check if the new coverage tool generates the same impossible cases.
        if self.size is None:
            self.size = size
        elif self.size != size:
            raise RuntimeError('Cross size changed from {} to {}.'
                               .format(self.size, size))

        if self.impossible is None:
            self.impossible = dict()

        ss_impossible = self.impossible.setdefault(subset, impossible)
        if ss_impossible != impossible:
            # Something has gone awry if the impossible table has
            # changed
            raise RuntimeError('Cross impossible table changed. '
                               'Was: {}; Now: {}.'
                               .format(show_bitfield(ss_impossible),
                                       show_bitfield(impossible)))

    def get_coverage(self, subset: int) -> int:
        '''Return bitfield of what is covered or is impossible at subset'''
        return self.seen | (self.impossible.get(subset, 0)
                            if self.impossible is not None
                            else 0)

    def as_tuple(self) -> Optional[_CCTuple]:
        '''Return a tuple representation of the point for serialization

        If we don't yet have a size (in which case, seen should be 0), returns
        None.

        '''
        if self.size is None:
            assert self.seen == 0
            return None

        return (self.size, self.seen, self.impossible or {})


class CrossCoverage:
    def __init__(self, cstore: CoverageStore) -> None:
        self.coverage = dict()  # type: Dict[str, CCPoint]
        self.cstore = cstore

    def update(self,
               view_name: str,
               subset: int,
               config: Configuration,
               new_data: Dict[str, _CCSTuple]) -> None:
        '''Update coverage with dumped data'''
        for name, cct in new_data.items():
            seen, size, impossible_table = cct
            if not self.cstore.is_cover_point_relevant(name,
                                                       'impossible', size):
                continue
            ccpt = self.coverage.get(name, None)
            if ccpt is None:
                # We haven't tracked this point so far. Should we start doing
                # so?
                if not (self.cstore.
                        is_cover_point_relevant(name, 'cross', size)):
                    # Nope. Ignore it.
                    continue

                ccpt = CCPoint(size)
                self.coverage[name] = ccpt
            for k in config.subsets():
                v = config.views[k].view
                if v not in impossible_table:
                    raise RuntimeError('Impossible table for point `{}\' doesn\'t '
                                       'have an entry for view `{}\'.'
                                       .format(name, v))
                impossible = impossible_table[v]
                if k == subset:
                    impossible_seen = seen & impossible
                    if impossible_seen:
                        raise RuntimeError('Cross {} covered impossible cases: {} '
                                           '(view: {}; cases covered: {}; '
                                           'cases thought impossible: {})'
                                           .format(name,
                                                   v,
                                                   show_bitfield(impossible_seen),
                                                   show_bitfield(seen),
                                                   show_bitfield(impossible)))
                ccpt.update(seen, impossible, size, k)

    def holes(self, subset: int) -> List[Tuple[str, int]]:
        '''Return a list of coverage holes for this subset

        A coverage hole is represented as a pair (name, missing) where
        name is the name of the point and missing is the bitfield of
        bits that we think are missing.

        '''
        ret = []
        for name, ccpt in self.coverage.items():
            # We're interested in points that are expected but aren't
            # ignored or covered.
            ignored = self.cstore.ignore_mask(name, 'cross', ccpt.size or 0)
            covered = ccpt.get_coverage(subset)

            size = ccpt.size
            if size is None:
                # Presumably, this point came from read_cross_list, but we
                # haven't actually seen it yet. Treat it as a 1-bit cross for
                # now.
                assert (ignored | covered) == 0
                size = 1

            expected = (1 << size) - 1

            missing = expected & ~ (ignored | covered)
            if missing:
                ret.append((name, missing))

        return ret

    def print_holes(self,
                    handle: IO[str],
                    holes: List[Tuple[str, int]],
                    max_count: int) -> None:
        '''Print messages about a (nonempty list of) holes'''
        assert holes

        # Collect the first max_count holes that we should print, shuffling so
        # that ones with a specific range profile come first.
        specific = []
        generic = []
        for name, missing in holes:
            if self.cstore.has_specific_cross_profile(name):
                specific.append((name, missing))
            else:
                generic.append((name, missing))

            if (((len(specific) > max_count) or
                 (len(specific) >= max_count and generic))):
                break

        shuffled = specific + generic
        for name, missing in shuffled[:max_count]:
            handle.write("    {} {}\n"
                         .format(name, show_bitfield(missing)))
        if len(holes) > max_count:
            handle.write('    ...\n')

    def list_coverage(self, handle: IO[str]) -> None:
        '''Write lines to handle for the cross coverage that has been seen

        Lines are of the form "C NAME 1,2,3,8,9" to mean that the
        cross with name NAME has interesting bits 1,2,3,8 and 9.

        Note that this doesn't use information about impossible bits
        (since that depends on the subset). Instead, we dump all the cross
        coverage that we've seen across all subsets, minus anything that
        should have been ignored.

        '''
        for name, ccpt in self.coverage.items():
            if not ccpt.seen:
                # Optimisation: Nothing to do if this point hasn't
                # been seen yet.
                continue

            size = ccpt.size

            # We shouldn't have any points with empty size left at
            # this point
            assert size is not None

            ignored = self.cstore.ignore_mask(name, 'cross', size)

            # Tracked bits are the ones we care about (it would be "~
            # ignored", but that doesn't take the size into account
            # properly)
            size_mask = (1 << size) - 1
            tracked = (size_mask ^ ignored)

            bitfield = ccpt.seen & tracked
            if not bitfield:
                return

            desc = ','.join(str(b) for b in list_bits(bitfield))
            handle.write('C {} {}\n'.format(name, desc))

    def read_cross_list(self, path: str) -> None:
        '''Read in a list of crosses to force them to appear'''
        with open(path) as cross_file:
            for linum, line in enumerate(cross_file):
                name = line.strip()
                if name in self.coverage:
                    raise RuntimeError('{}:{}: Cross {} already appears'
                                       .format(path, linum + 1, name))

                self.coverage[name] = CCPoint(None)

    def dump(self, stream: IO[str]) -> None:
        '''Dump data to the given file'''
        cc_tuples = {}
        for name, ccpt in self.coverage.items():
            cc_tuple = ccpt.as_tuple()
            if cc_tuple is not None:
                cc_tuples[name] = cc_tuple
        stream.write("cc={}\n".format(repr(cc_tuples)))

    def load(self, module: Dict[str, object]) -> None:
        '''Load data from the given module'''
        cc_tuples = module.get('cc')
        if cc_tuples is None:
            raise RuntimeError('No cc entry in dumped module')

        assert isinstance(cc_tuples, dict)

        for name, cc_tuple in cc_tuples.items():
            assert isinstance(name, str)
            assert (isinstance(cc_tuple, tuple) and len(cc_tuple) == 3)

            size, seen, impossible = cc_tuple
            assert isinstance(size, int)
            assert isinstance(seen, int)
            assert isinstance(impossible, dict)
            for k, v in impossible.items():
                assert isinstance(k, int)
                assert isinstance(v, int)

            ccpt = CCPoint(size)
            ccpt.seen = seen
            ccpt.impossible = impossible
            self.coverage[name] = ccpt
