#!/usr/bin/python3

################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import xml.etree.ElementTree as ET
import sys
import re
import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output',
                        type=argparse.FileType('wb'), default=sys.stdout)
    parser.add_argument('infile', nargs='+')

    return parser.parse_args()


def getArgonViewerVersion(inFile, old_version):
    with open(inFile, "r") as inF:
        text = inF.read()
        viewerVersionRe = re.compile("href=('|\")ArgonViewerV([0-9]+)/")
        reResult = viewerVersionRe.search(text)
        if not reResult:
            raise RuntimeError('Input file {} does not set viewer version.'
                               .format(inFile))

        new_version = int(reResult.group(2))

        if old_version is not None and old_version != new_version:
            raise RuntimeError('Version mismatch at input file {}: '
                               'Previous version was {}, but now we see {}.'
                               .format(inFile, old_version, new_version))

        return new_version


def add_txt_elt(dest, elt_name, text):
    '''Append an XML element to dest called elt_name containing given text'''
    elem = ET.SubElement(dest, elt_name)
    elem.text = text


def maybe_add_txt_elt(dest, elt_name, val, render=None):
    '''If val is not None, add render(val) with to dest with add_txt_elt'''
    if val is not None:
        if render is not None:
            val = render(val)

        add_txt_elt(dest, elt_name, val)


def find_int_elt(elt, name):
    txt = elt.findtext(name)
    if txt is None:
        return None

    try:
        return int(txt)
    except ValueError:
        raise RuntimeError('Cannot parse {} as an integer value from '
                           'element <{}>.'
                           .format(txt, name))


class Coverage:
    '''Data for a <coverage> element'''
    def __init__(self, elt):
        self.spec_rng = (elt.findtext('specifiedMin'),
                         elt.findtext('specifiedMax'))
        if None in self.spec_rng:
            raise RuntimeError('<coverage> element must have '
                               'specifiedMin and specifiedMax')

        self.actual_range = [find_int_elt(elt, 'actualMin'),
                             find_int_elt(elt, 'actualMax')]

        self.all_cov_strms = [x.text for x in elt.findall("coveringStream")]

        self.cov_strms = [elt.findtext(tag)
                          for tag in ['zeroCoveringStream',
                                      'nonzeroCoveringStream',
                                      'minCoveringStream',
                                      'maxCoveringStream']]

    @staticmethod
    def merge_maybe(left, right, fun):
        if left is None:
            return right
        if right is None:
            return left

        return fun(left, right)

    @staticmethod
    def widen_maybe(left, right):
        return (Coverage.merge_maybe(left[0], right[0], min),
                Coverage.merge_maybe(left[1], right[1], max))

    @staticmethod
    def fst_maybe(left, right):
        return Coverage.merge_maybe(left, right, lambda x, y: x)

    def merge(self, cov_data):
        if self.spec_rng != cov_data.spec_rng:
            raise RuntimeError('Cannot merge coverage: '
                               'specifiedMin/specifiedMax is '
                               '{} on left and {} on right.'
                               .format(self.spec_rng, cov_data.spec_rng))

        self.actual_range = Coverage.widen_maybe(self.actual_range,
                                                 cov_data.actual_range)

        for strm in cov_data.all_cov_strms:
            if strm not in self.all_cov_strms:
                self.all_cov_strms.append(strm)

        self.cov_strms = [Coverage.fst_maybe(pr[0], pr[1])
                          for pr in zip(self.cov_strms, cov_data.cov_strms)]

    def as_element(self):
        elt = ET.Element('coverage')
        add_txt_elt(elt, 'specifiedMin', self.spec_rng[0])
        add_txt_elt(elt, 'specifiedMax', self.spec_rng[1])
        maybe_add_txt_elt(elt, 'actualMin', self.actual_range[0], str)
        maybe_add_txt_elt(elt, 'actualMax', self.actual_range[1], str)
        for strm in self.all_cov_strms:
            add_txt_elt(elt, 'coveringStream', strm)
        maybe_add_txt_elt(elt, 'zeroCoveringStream', self.cov_strms[0])
        maybe_add_txt_elt(elt, 'nonzeroCoveringStream', self.cov_strms[1])
        maybe_add_txt_elt(elt, 'minCoveringStream', self.cov_strms[2])
        maybe_add_txt_elt(elt, 'maxCoveringStream', self.cov_strms[3])
        return elt


class TestPoint:
    '''Data for a <testPoint> element'''
    def __init__(self, elt):
        self.idx = None
        self.name = None
        self.coverage = None
        self.other_fields = []

        keys = set()
        for child in elt:
            if child.tag in keys:
                raise RuntimeError('Duplicate testPoint child: {}'
                                   .format(child.tag))
            keys.add(child.tag)

            if child.tag == 'id':
                self.idx = child.text
            elif child.tag == 'name':
                self.name = child.text
            elif child.tag == 'coverage':
                self.coverage = Coverage(child)
            else:
                self.other_fields.append((child.tag, child.text))

        if None in [self.idx, self.name, self.coverage]:
            raise RuntimeError('Incomplete test point element.')

    def merge(self, test_point):
        assert self.idx == test_point.idx
        if self.name != test_point.name:
            raise RuntimeError('Matching index for testPoints but '
                               'names are {} != {}.'
                               .format(self.name, test_point.name))
        self.coverage.merge(test_point.coverage)

        if self.other_fields != test_point.other_fields:
            raise RuntimeError('Matching index for testPoints but '
                               'other_fields are {} != {}.'
                               .format(self.other_fields,
                                       test_point.other_fields))

    def as_element(self):
        elt = ET.Element('testPoint')
        add_txt_elt(elt, 'id', self.idx)
        add_txt_elt(elt, 'name', self.name)
        for key, value in self.other_fields:
            add_txt_elt(elt, key, value)
        elt.append(self.coverage.as_element())
        return elt


class TestGroup:
    '''Data for a testGroup element'''
    def __init__(self, elt):
        self.other_fields = []
        self.idx = None
        self.name = None
        self.children = []

        keys = set()
        for child in elt:
            # The only duplicates allowed are testGroup and testPoint.
            if child.tag == 'testGroup':
                self.children.append((True, TestGroup(child)))
                continue
            if child.tag == 'testPoint':
                self.children.append((False, TestPoint(child)))
                continue

            if child.tag in keys:
                raise RuntimeError('Duplicate testGroup child: {}'
                                   .format(child.tag))
            keys.add(child.tag)

            # Anything else is only allowed if we haven't seen any testGroup or
            # testPoint children yet (because these should appear at the end of
            # the element).
            if self.children:
                raise RuntimeError('Seen <{}> in a testGroup '
                                   'after some test children.'
                                   .format(child.tag))

            if child.tag == 'id':
                if self.name is not None:
                    raise RuntimeError('Seen <id> after <name> in testGroup')
                if self.idx is not None:
                    raise RuntimeError('Duplicate <id> in testGroup')
                self.idx = child.text

            elif child.tag == 'name':
                if self.name is not None:
                    raise RuntimeError('Duplicate <name> in testGroup')
                self.name = child.text

            else:
                if self.name is not None or self.idx is not None:
                    raise RuntimeError('<{}> field in group after name/idx.'
                                       .format(child.tag))
                self.other_fields.append((child.tag, child.text))

    def merge(self, other):
        assert (self.idx, self.name) == (other.idx, other.name)
        if self.other_fields != other.other_fields:
            raise RuntimeError('Matching index/name ({}/{}) for testGroups '
                               'but other_fields are {} != {}.'
                               .format(self.idx, self.name,
                                       self.other_fields,
                                       other.other_fields))
        self.children = merge_trees(self.children, other.children)

    def as_element(self):
        elt = ET.Element('testGroup')
        for key, value in self.other_fields:
            add_txt_elt(elt, key, value)
        maybe_add_txt_elt(elt, 'id', self.idx)
        maybe_add_txt_elt(elt, 'name', self.name)
        for _, child in self.children:
            elt.append(child.as_element())
        return elt


def make_tree_list_idx(tree_list):
    '''Generate a mapping from "name" to index in the list'''
    mapping = {}
    for idx, (is_group, node) in enumerate(tree_list):
        key = (is_group, node.idx, node.name)
        if key in mapping:
            raise RuntimeError('Tree list has duplicate key: {}'
                               .format(key))

        mapping[key] = idx

    return mapping


def merge_trees(list0, list1):
    '''Merge together two testPoint trees

    They are assumed to be ordered the same, but with possibly some branches
    missing. Each tree is given as a list of pairs (G, T) where T is either a
    testPoint or a testGroup and G is a boolean which is true iff T is a
    testGroup.

    '''
    ret = []
    idx1 = make_tree_list_idx(list1)

    pos0 = 0
    pos1 = 0
    while True:
        if pos1 >= len(list1):
            return ret + list0[pos0:]

        if pos0 >= len(list0):
            return ret + list1[pos1:]

        # Both list0 and list1 have stuff left. What should we work on?
        is_group0, node0 = list0[pos0]
        is_group1, node1 = list1[pos1]

        key0 = (is_group0, node0.idx, node0.name)
        key1 = (is_group1, node1.idx, node1.name)

        # If the keys are equal, we should merge these elements, taking both.
        if key0 == key1:
            node0.merge(node1)
            ret.append((is_group0, node0))
            pos0 += 1
            pos1 += 1
            continue

        # If the keys are not equal, we should take exactly one of the
        # elements. Which one? If key0 appears somewhere in list1 (hopefully
        # with a higher index than pos1) then we should take the previous
        # elements from list1 while we wait to get there.
        idx1_key0 = idx1.get(key0)
        if idx1_key0 is not None:
            if idx1_key0 <= pos1:
                raise RuntimeError('Trees we are merging are out of order.')

            ret.append(list1[pos1])
            pos1 += 1
            continue

        # If key0 doesn't appear in list1, we take that element. Note that this
        # is asymmetrical: if list0 and list1 are disjoint, we'll get all of
        # list0 followed by all of list1.
        ret.append(list0[pos0])
        pos0 += 1

    return ret


class Component:
    '''A class modelling the <component> child of <crossPoint>'''
    FIELDS = ['name', 'low', 'high', 'linearStep']

    def __init__(self, elt):
        self.fields = [elt.findtext(key) for key in Component.FIELDS]
        if None in self.fields:
            raise RuntimeError('<component> tag missing a field.')

    def merge(self, cp_name, component):
        if self.fields != component.fields:
            raise RuntimeError('Mismatch between components for {}: {} != {}.'
                               .format(cp_name, self.fields, component.fields))

    def as_element(self):
        elt = ET.Element('component')
        for key, value in zip(Component.FIELDS, self.fields):
            add_txt_elt(elt, key, value)
        return elt


class CrossPoint:
    '''A class modelling a <crossPoint> element'''
    def __init__(self, elt):
        self.idx = elt.findtext('id')
        self.name = elt.findtext('name')
        self.order = elt.findtext('order')
        self.components = [Component(c) for c in elt.findall('component')]
        self.bitfield = elt.findtext('bitfield')
        self.icbs = [(x.get('view'), x.text)
                     for x in elt.findall("impossibleCasesBitfield")]
        if None in [self.idx, self.name, self.order, self.bitfield]:
            raise RuntimeError('<crossPoint> tag missing a field.')
        if not self.components:
            raise RuntimeError('<crossPoint> tag has no components.')
        try:
            self.idx = int(self.idx)
        except ValueError:
            raise RuntimeError('crossPoint <id> is `{}\': '
                               'not a valid integer.'
                               .format(self.idx))

    def merge(self, other):
        key0 = [self.idx, self.name, self.order, self.icbs]
        key1 = [other.idx, other.name, other.order, other.icbs]
        if key0 != key1:
            raise RuntimeError('Mismatch between crossPoints: {} != {}'
                               .format(key0, key1))

        if len(self.components) != len(other.components):
            raise RuntimeError('crossPoints with key {} have '
                               'different numbers of components.'
                               .format(key0))

        for comp0, comp1 in zip(self.components, other.components):
            comp0.merge(self.name, comp1)

        txt_len = max(len(self.bitfield), len(other.bitfield))
        merged = ('{0:x}'
                  .format(int(self.bitfield, 16) | int(other.bitfield, 16)))
        self.bitfield = merged.rjust(txt_len, '0')

    def as_element(self):
        elt = ET.Element('crossPoint')
        add_txt_elt(elt, 'id', str(self.idx))
        add_txt_elt(elt, 'name', self.name)
        add_txt_elt(elt, 'order', self.order)
        for comp in self.components:
            elt.append(comp.as_element())
        add_txt_elt(elt, 'bitfield', self.bitfield)
        for (view, bf) in self.icbs:
            sub_elt = ET.SubElement(elt, 'impossibleCasesBitfield')
            sub_elt.text = bf
            sub_elt.set('view', view)
        return elt


def merge_crosses(crosses0, crosses1):
    '''Merge together two lists of CrossPoints'''

    # The two files might not contain the same list of crosses, so we use their
    # id fields (which are strictly increasing - checked in CoverFile
    # constructor) to merge them.

    pos0 = 0
    pos1 = 0
    ret = []

    while True:
        if pos1 >= len(crosses1):
            return ret + crosses0[pos0:]

        if pos0 >= len(crosses0):
            return ret + crosses1[pos1:]

        cross0 = crosses0[pos0]
        cross1 = crosses1[pos1]

        if cross0.idx == cross1.idx:
            cross0.merge(cross1)
            ret.append(cross0)
            pos0 += 1
            pos1 += 1
            continue

        if cross0.idx < cross1.idx:
            ret.append(cross0)
            pos0 += 1
        else:
            ret.append(cross1)
            pos1 += 1

    return ret


class CoverFile:
    '''A class representing the entirety of a coverage file'''
    def __init__(self, root):
        # We expect to see
        #
        #  codec                 |
        #  visibilityCategories  | A
        #  views                 |
        #
        #  initialView           | B
        #
        #  rangeEquationVisibilityTypes | C
        #  rangeEquationGroups          |
        #
        #  testGroup*  | D
        #  testPoint*  |
        #
        #  crossPoint*  | E
        #
        # Elements can appear in an arbitrary order within each group A, B, C,
        # D.
        #
        # When merging, we have special logic for merging groups B and D (group
        # D is the whole point!), but groups A and C must be exactly equal on
        # both sides.

        self.a_elements = []
        self.c_elements = []
        self.flat_ac = None

        self.initial_view = None
        self.test_tree = []
        self.cross_points = []

        tag_to_phase = {
            'codec': 0,
            'visibilityCategories': 0,
            'views': 0,

            'initialView': 1,

            'rangeEquationVisibilityTypes': 2,
            'rangeEquationGroups': 2,

            'testGroup': 3,
            'testPoint': 3,

            'crossPoint': 4
        }
        last_phase = 0

        for child in root:
            if child.tag not in tag_to_phase:
                raise RuntimeError('Unknown XML tag: {}'
                                   .format(child.tag))
            phase = tag_to_phase[child.tag]
            if phase < last_phase:
                raise RuntimeError('Ordering problem in XML. '
                                   'Saw <{}> (phase {}) in phase {}.'
                                   .format(child.tag, phase, last_phase))
            last_phase = phase

            if child.tag == 'initialView':
                self.initial_view = child.text
            elif child.tag == 'testGroup':
                self.test_tree.append((True, TestGroup(child)))
            elif child.tag == 'testPoint':
                self.test_tree.append((False, TestPoint(child)))
            elif child.tag == 'crossPoint':
                self.cross_points.append(CrossPoint(child))
            else:
                if phase == 0:
                    self.a_elements.append(child)
                else:
                    self.c_elements.append(child)

        ac_strings = [''.join(ET.tostring(hdr, encoding='unicode').split())
                      for hdr in self.a_elements + self.c_elements]
        self.flat_ac = ''.join(ac_strings)

        # Check we got an initialView
        if self.initial_view is None:
            raise RuntimeError('No <initialView> element.')

        # Check that the crossPoint ids are strictly increasing
        if self.cross_points:
            last_id = self.cross_points[0].idx
            for cross in self.cross_points[1:]:
                if cross.idx <= last_id:
                    raise RuntimeError('crossPoint ids are '
                                       ' not strictly increasing.')
                last_id = cross.idx

    def merge(self, other):
        if self.flat_ac != other.flat_ac:
            raise RuntimeError('Mismatch in groups A, C.')
        self.test_tree = merge_trees(self.test_tree, other.test_tree)
        self.cross_points = merge_crosses(self.cross_points,
                                          other.cross_points)
        # Take the alphabetically last initialView. This works for the AV1
        # codec, at least, because the names look like "profile0", "profile1"
        # etc, where the bigger profiles are later in the dictionary.
        self.initial_view = max(self.initial_view, other.initial_view)

    def as_element(self):
        elt = ET.Element('report')
        for hdr in self.a_elements:
            elt.append(hdr)
        iview = ET.Element('initialView')
        iview.text = self.initial_view
        elt.append(iview)
        for hdr in self.c_elements:
            elt.append(hdr)
        for _, tree in self.test_tree:
            elt.append(tree.as_element())
        for point in self.cross_points:
            elt.append(point.as_element())
        return elt


def main():
    args = parse_args()
    viewer_version = None
    so_far = None

    for idx, path in enumerate(args.infile):
        print("Loading input file {} [{}/{}]"
              .format(path, idx + 1, len(args.infile)))
        sys.stdout.flush()
        doc = ET.parse(path)
        viewer_version = getArgonViewerVersion(path, viewer_version)

        print('  Parsing coverage data')
        sys.stdout.flush()
        parsed = CoverFile(doc.getroot())
        doc = None

        if so_far is None:
            so_far = parsed
        else:
            print("  Merging with previous coverage")
            sys.stdout.flush()
            so_far.merge(parsed)

    stylesheet = "ArgonViewerV{}/coverage.xslt".format(viewer_version)
    args.output.write(("<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
                       "<!-- saved from url=(0014)about:internet -->\r\n"
                       "<?xml-stylesheet type=\"text/xsl\" href=\"{0}\"?>\r\n")
                      .format(stylesheet).encode("utf-8"))

    ET.ElementTree(so_far.as_element()).write(args.output, encoding="utf-8")
    return 0


if __name__ == '__main__':
    try:
        exit(main())
    except RuntimeError as e:
        sys.stderr.write('Error: {}\n'.format(e))
        exit(1)
