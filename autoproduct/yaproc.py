################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Yet another subprocess library

These subprocesses redirect zero or more of stdout, stderr to temporary files,
which can be read when the process finishes.

'''

import os
import multiprocessing
import subprocess
import tempfile
import time

from typing import cast, Dict, Iterator, List, Optional, Set, Tuple, Union

_Use = Union[bool, str]
_Used = Tuple[Optional[int], Optional[str]]


def maybe_open_maybe_temp(use_file: _Use) -> _Used:
    '''Open a temporary file if needed.

    The return value is a pair (fd, path). If the file should not be deleted
    when done, path is None, otherwise it's the path to the file. fd is the
    open file.

    '''
    if isinstance(use_file, bool):
        return tempfile.mkstemp() if use_file else (None, None)

    return (os.open(use_file, os.O_WRONLY), None)


class YAProc:
    '''A subprocess that is started and stopped with a context manager.'''

    def __init__(self, args: List[str],
                 use_stdout: _Use = False,
                 use_stderr: _Use = False) -> None:
        self.args = args
        self.use_stdout = use_stdout
        self.use_stderr = use_stderr

        # These fields are None when not in context.
        self.stdout = (None, None)  # type: _Used
        self.stderr = (None, None)  # type: _Used
        self.proc = None  # type: Optional[subprocess.Popen[bytes]]

    def __enter__(self) -> 'YAProc':
        assert self.proc is None

        self.stdout = maybe_open_maybe_temp(self.use_stdout)
        self.stderr = maybe_open_maybe_temp(self.use_stderr)
        try:
            self.proc = subprocess.Popen(self.args,
                                         stdout=self.stdout[0],
                                         stderr=self.stderr[0])
        except OSError as e:
            raise RuntimeError('Failed to start command "{}" ({})'
                               .format(' '.join(self.args),
                                       e))

        return self

    @staticmethod
    def rm_f(path: str) -> None:
        '''Delete a fail, ignoring failures if it doesn't exist'''
        try:
            os.remove(path)
        except OSError:
            if os.path.exists(path):
                raise

    def close(self) -> None:
        '''Close any open fds'''
        for output in [self.stdout, self.stderr]:
            if output[0] is not None:
                os.close(output[0])

        self.stdout = (None, self.stdout[1])
        self.stderr = (None, self.stderr[1])

    def cleanup(self) -> None:
        '''Close any open fds and delete any temporary files'''
        self.close()

        for output in [self.stdout, self.stderr]:
            if output[1] is not None:
                YAProc.rm_f(output[1])

        self.stdout = (None, None)
        self.stderr = (None, None)

    def __exit__(self,
                 exc_type: object,
                 exc_value: object,
                 traceback: object) -> None:
        # exc_type will be None unless there was an exception. If it's None,
        # wait for the child process to finish. Otherwise, kill the child
        # process.
        if self.proc is not None:
            if self.proc.poll() is None:
                if exc_type is None:
                    self.proc.wait()
                else:
                    self.proc.kill()

        self.proc = None
        self.cleanup()

    def wait(self) -> int:
        '''Wait for the subprocess to finish.'''
        assert self.proc
        retcode = self.proc.wait()
        # Close any open fds (since the process is now done)
        self.close()
        return retcode

    def output_path(self, stderr: bool = False) -> str:
        '''Return the path to stdout or stderr'''
        output = (self.stderr if stderr else self.stdout)
        assert output[1]
        return cast(str, output[1])


class YAProcs:
    '''A list of subprocesses, with the same context manager as YAProc.

    This has problems if the exit handler for one of the processes raises an
    Exception (which is why Python 2.7 deprecated contextlib.nested()). If we
    ever start using Python 3.3+, we can use contextlib.ExitStack, which does
    what we actually need. Ho hum.

    '''
    def __init__(self,
                 commands: List[List[str]],
                 use_stdout: _Use = False,
                 use_stderr: _Use = False,
                 njobs: Optional[int] = None):
        if njobs is None:
            njobs = multiprocessing.cpu_count()

        assert njobs > 0
        self.slots = njobs

        self.pending = {i: YAProc(cmd,
                                  use_stdout=use_stdout, use_stderr=use_stderr)
                        for i, cmd in enumerate(commands)}
        self.running = {}  # type: Dict[int, YAProc]
        self.done = {}  # type: Dict[int, YAProc]

        # This is only used with the pop() function. It lists the indices that
        # have already been popped.
        self.popped = set()  # type: Set[int]

    def start_one(self) -> None:
        '''Start one job, decrementing self.slots'''
        assert self.slots and self.pending
        i, proc = self.pending.popitem()
        assert i not in self.running
        self.running[i] = proc.__enter__()
        self.slots -= 1

    def start_some(self) -> None:
        '''Start as many jobs as we can'''
        while self.slots and self.pending:
            self.start_one()

    def reap(self) -> bool:
        '''Reap as many jobs as possible from the running list

        Returns true if at least one process had finished since last time.

        '''
        # We shouldn't run this after the final reap
        something_done = False

        done_jobs = []

        for i, proc in self.running.items():
            assert proc.proc
            if proc.proc.poll() is None:
                continue

            something_done = True
            assert i not in self.done
            self.done[i] = proc

            # Use wait() to close any fds in the subprocess. This would happen
            # when we finally exited, but we might want to do it now if we've
            # got lots of commands to work through and don't want to hit a
            # limit on the maximum number of open files. Note: this should be
            # instant: we aren't actually waiting for a process that hasn't
            # finished.
            proc.wait()

            done_jobs.append(i)
            self.slots += 1

        for i in done_jobs:
            del self.running[i]

        return something_done

    def __enter__(self) -> 'YAProcs':
        self.start_some()
        return self

    def __exit__(self,
                 exc_type: object,
                 exc_value: object,
                 traceback: object) -> None:
        # exc_type will be None unless there was an exception. If it's None,
        # wait for the child process to finish. Otherwise, kill the child
        # process.
        if exc_type is None:
            self.wait()

        for proc in self.running.values():
            proc.__exit__(exc_type, exc_value, traceback)
        for proc in self.done.values():
            proc.__exit__(exc_type, exc_value, traceback)

    def wait_some(self, just_one: bool = False) -> bool:
        '''Wait for one or all of the processes to finish.'''
        something_done = False
        while self.pending or self.running:
            time.sleep(0.1)
            something_done = self.reap()
            self.start_some()

            if just_one and something_done:
                break

        return something_done

    def wait(self) -> List[Tuple[int, YAProc]]:
        '''Wait for all processes to finish

        Return the list of processes, together with their results. Elements
        look like (retcode, proc).

        '''
        self.wait_some()

        assert not (self.pending or self.running)

        # Everything is now done. Move stuff to results.
        results = []
        for idx in range(len(self.done)):
            proc = self.done[idx]
            retcode = proc.wait()
            results.append((retcode, proc))
        return results

    def dribble(self) -> Iterator[Tuple[int, int, YAProc]]:
        '''Like wait(), but yield processes as they finish

        These might not be in order, so the values yielded are triples of the
        form (idx, retcode, proc) where idx is the index of the command in
        self.commands and retcode, proc are as in wait().

        As well as spotting quickly if something went wrong, using dribble()
        means that we can clean up after processes on the fly, to avoid using
        space linear in the number of processes.

        '''
        while True:
            popped = self.pop()
            if popped is None:
                # We're done.
                return

            key, proc = popped
            yield (key, proc.wait(), proc)

    def pop(self) -> Optional[Tuple[int, YAProc]]:
        '''Wait for at least one process to finish and return it

        The return value is a pair: (idx, proc) where idx is the index of a
        command and proc is a YAProc object. If pop returns None, every process
        has been dealt with.

        '''

        # A process is ready to be popped if it appears in self.done, but not
        # in self.popped. Since self.popped is always a subset of the keys of
        # self.done, we can check this by lengths.
        assert len(self.popped) <= len(self.done)

        if len(self.popped) == len(self.done):
            # If we're here, we don't have a process ready for us. Wait to see
            # if one will finish for us.
            something_done = self.wait_some(just_one=True)
            if not something_done:
                # Everything had finished (and been copied to self.done) before
                # we called wait_some this time. Nothing new, so return None.
                assert len(self.popped) == len(self.done)
                return None

        # If we get here, there should be at least one thing ready to be
        # popped.
        assert len(self.popped) < len(self.done)
        diff = set(self.done.keys()) - set(self.popped)
        assert diff

        # Pick an arbitrary element of the set.
        key = diff.pop()
        self.popped.add(key)
        return (key, self.done[key])
