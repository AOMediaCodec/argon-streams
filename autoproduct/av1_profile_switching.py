################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import os
import ap_subprocess
import coveragestore
import importlib
from merge_streams import mergeStreamsOBU,OBUallowedSecondMergeFile
import sys
import shutil
from contextlib import contextmanager
from itertools import permutations
ap_subprocess.init()
verbose = False

@contextmanager
def TemporaryDirectory():
  name = tempfile.mkdtemp(dir='')
  try:
    yield name
  finally:
    shutil.rmtree(name)
@contextmanager
def TemporaryDirector(outDir):
  if os.path.exists(outDir):
    shutil.rmtree(outDir)
  os.mkdir(outDir)
  yield outDir

cross_list = ["CROSS_PROFILE_SWITCH"]
stream_index = 0
def load_coverage(outDir, coverageTool, streamFile):
  global stream_index
  coverpyFileName = 'test_cover{}.py'.format(stream_index)
  stream_index+=1
  coverpyFile = os.path.join(outDir,coverpyFileName)
  coverpycFile = os.path.join(outDir,coverpyFileName+'c')
  if os.path.exists(coverpyFile):
    os.remove(coverpyFile)
  if os.path.exists(coverpycFile):
    os.remove(coverpycFile)
  coverProcess = ap_subprocess.launch_with_stdout([coverageTool,"-p", "/dev/null", "--pyf", coverpyFile, streamFile])
  if coverProcess.wait()!=0:
    print 'Coverage Failed'
    return None
  streamData = importlib.import_module(coverpyFileName[:-3])
  if os.path.exists(coverpyFile):
    os.remove(coverpyFile)
  if os.path.exists(coverpycFile):
    os.remove(coverpycFile)
  for point in cross_list:
    try:
      (bitfield, size, impossibleCaseTable)  = streamData.c[point]
      impossibleCases = int(impossibleCaseTable['allopt'])&((1<<int(size))-1)
      if verbose:
        print 'Coverage of stream',streamFile, 'is',(bitfield, int(size), impossibleCases)
      return (int(bitfield),int(size), impossibleCases)
    except KeyError:
      pass
  return None

def merge_and_useful(outDir,outFile,streamFiles,coverageTool,currentBitfield):
  if verbose:
    print 'Merging',streamFiles
  mergeStreamsOBU(outFile,streamFiles)
  coverage = load_coverage(outDir,coverageTool,outFile)
  if coverage == None:
    return None
  (bitfield, size, impossibleCases) = coverage
  if currentBitfield | bitfield == currentBitfield:
    return False,(currentBitfield,impossibleCases,size)
  if verbose:
    print 'Stream Useful',outFile
  return True,(currentBitfield|bitfield,impossibleCases,size)

if __name__=="__main__":
  verbose=False
  if len(sys.argv)<4:
    print 'Av1 Profile Switching tool'
    print
    print "Syntax:"
    print
    print "    {} <coverageTool> <output stream dir> <input stream dirs ...>".format(sys.argv[0])
    print
    print "        Merges streams in the given directories"
    print "        in groups of two, until all required coverage points are covered"
    print "        and places the results in the given output directory."
    print
    sys.exit(1)
  currentBitfield = 0
  impossibleCases = 0
  size = 1
  coverageTool = sys.argv[1]
  outputDir = sys.argv[2]
  streamDirs = sys.argv[3:]
  with TemporaryDirector(outputDir) as outDir:
    sys.path.append(outDir)
    dirLists = [os.listdir(streamDir) for streamDir in streamDirs]
    singleStreams = [iter(sorted([os.path.join(streamDirs[i],x) for x in dirList if x.endswith(".obu") and "_" not in x],key=lambda y : os.path.getsize(os.path.join(streamDirs[i],y)))) for i,dirList in enumerate(dirLists)]
    while True:
      if (currentBitfield | impossibleCases == ((1<<size)-1)):
        break
      any_still_files = False
      for perm in permutations(range(len(singleStreams)),2):
        if (currentBitfield | impossibleCases == ((1<<size)-1)):
          break
        try:
          streamFiles = [next(singleStreams[ind]) for ind in perm]
        except StopIteration:
          continue
        any_still_files = True
        either_file_allowed = False
        for fname in streamFiles:
          if OBUallowedSecondMergeFile(fname):
            either_file_allowed = True
        if not either_file_allowed:
          continue
        outFile = os.path.join(outDir,"test{}.obu".format("_".join(os.path.basename(x).rpartition(".")[0][4:] for x in streamFiles)))
        useful_params = merge_and_useful(outDir,outFile,streamFiles,coverageTool,currentBitfield)
        if useful_params == None:
          os.remove(outFile)
          continue
        useful,(currentBitfield,impossibleCases,size) = useful_params
        if not useful:
          os.remove(outFile)
        else:
          print "Progress: {}/{}".format(sum([int(i) for i in bin(currentBitfield)[2:]]),size-sum([int(i) for i in bin(impossibleCases)[2:]]))
      if not any_still_files:
        print 'Not enough files to complete coverage'
        break
