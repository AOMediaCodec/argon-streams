################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

'''Code for tracking range coverage in autoproduct'''


import os
import re
import subprocess
from typing import Dict, IO, Iterable, List, Optional, Tuple

from branch_coverage import BranchCoverage, CBData
from coveragestore import CoverageStore

# The type for range equations. This is a dictionary keyed by equation name
# whose value is itself another dictionary. This is keyed by range visibility
# type (a string) and has values (min, max) (both integers) giving the range
# for the equation.
_Range = Tuple[int, int]
_EqnDict = Dict[str, _Range]
_RangeEqns = Dict[str, _EqnDict]
_ORange = Optional[Tuple[int, int]]
_Hole = Tuple[str, Tuple[_ORange, _Range]]


def gen_range_equations(eqn_dir: str,
                        codec: str,
                        cstore: CoverageStore) -> _RangeEqns:
    '''Run eqnToMathML.py to generate range equation info; read the result'''
    script = os.path.join(eqn_dir, 'eqnToMathML.py')
    if not os.path.exists(script):
        raise RuntimeError('Can\'t find {}.'.format(script))

    eqns_file = os.path.join(eqn_dir, 'eqns_{}.txt'.format(codec))
    if not os.path.exists(eqns_file):
        raise RuntimeError('Can\'t find {}.'.format(eqns_file))

    cmd = [script, 'py', eqns_file]
    code = subprocess.check_output(cmd, universal_newlines=True)
    globs = {}  # type: Dict[str, object]

    exec(code, globs)
    range_eqns = globs.get('rangeEqns')

    if range_eqns is None:
        raise RuntimeError('Running eqnToMathML.py generated Python that '
                           'didn\'t define rangeEqns.')

    # Sanity checking on the shape of the generated range equations
    if not isinstance(range_eqns, dict):
        raise RuntimeError('Range equations should be a dict object.')
    for key, ranges in range_eqns.items():
        if not isinstance(key, str):
            raise RuntimeError('Found range eqn with key {!r} (not a str).'
                               .format(key))
        if not isinstance(ranges, dict):
            raise RuntimeError('The range equation for {} had values {!r} '
                               '(not a dict).'
                               .format(key, ranges))
        for prof_name, rng in ranges.items():
            if not isinstance(prof_name, str):
                raise RuntimeError('The range equation for {} had a range '
                                   'with key {!r} (not a str).'
                                   .format(key, prof_name))
            if not (isinstance(rng, tuple) and len(rng) == 2):
                raise RuntimeError('The range equation for {}, profile {} has '
                                   'value {!r} (not a length-2 tuple).'
                                   .format(key, prof_name, rng))
            rng_min, rng_max = rng
            if not isinstance(rng_min, int):
                raise RuntimeError('The range equation for {}, profile {} has '
                                   'minimum {!r} (should be an integer).'
                                   .format(key, prof_name, rng_min))
            if not isinstance(rng_max, int):
                raise RuntimeError('The range equation for {}, profile {} has '
                                   'maximum {!r} (should be an integer).'
                                   .format(key, prof_name, rng_max))
            if rng_max < rng_min:
                raise RuntimeError('The range equation for {}, profile {} has '
                                   'maximum {} less than its minimum, {}.'
                                   .format(key, prof_name, rng_max, rng_min))

    # Now filter out any range equations that we don't care about
    ret = {}
    for eqn_name, ranges in range_eqns.items():
        if cstore.is_cover_point_relevant(eqn_name, 'range', 1):
            ret[eqn_name] = ranges

    return ret


# We're interested in branches whose 3rd part (split by semicolon) mentions
# looks like an equation. For example, if the equation is called XXX, we might
# expect to see a branch name something like
#
#   filename;section;E.123[XXX];idx: statement;sub_idx;subexpr
#
# We want to 'invert' the list of branch names to get a map from equation name
# to the branch names that match it. In fact, we'll make a double map, keyed by
# equation name and then sub_idx (which should be unique), whose values are
# branch names.
_EqnBranches = Dict[int, str]
_EqnToBranches = Dict[str, _EqnBranches]


def mk_eqn_to_branches(branch_names: Iterable[str]) -> _EqnToBranches:
    '''Search through the lists of branch names for ones used by equations'''
    regex = re.compile(r'.*\[(.*)\]$')

    ret = {}  # type: _EqnToBranches
    for branch_name in branch_names:
        parts = branch_name.split(';')
        if len(parts) != 6:
            raise RuntimeError('Unexpected branch name format: {}'
                               .format(branch_name))

        match = regex.match(parts[2])
        if not match:
            continue

        eqn_name = match.group(1)

        # This looks like an equation. Interpret the sub_idx field as an
        # integer.
        try:
            sub_idx = int(parts[4])
        except ValueError:
            raise RuntimeError('Cannot interpret sub-idx field in branch '
                               'name {} as an integer.'
                               .format(branch_name))

        eqn_dict = ret.setdefault(eqn_name, dict())
        if sub_idx in eqn_dict:
            raise RuntimeError('Seeming duplicate branch name for equation '
                               'name {} and sub-idx {}.'
                               .format(eqn_name, sub_idx))

        eqn_dict[sub_idx] = branch_name

    return ret


def find_branch_for_eqn(eqn_name: str, eqn_dict: _EqnBranches) -> str:
    assert len(eqn_dict) > 0

    # We assume that an equation line will correspond to some statement of the
    # form
    #
    #    LHS = expr
    #
    # and we really want to find the range of values taken by 'expr'. The
    # sub-expressions of expr are sorted from small to big (and in increasing
    # order of arguments to operators) and the last sub-expression is expr
    # itself. Thus, we essentially want to take the highest sub-idx.
    #
    # There are a couple of special cases, though: there are some functions
    # that we want to peer inside. Specifically:
    #
    #   Clip3
    #   ROUND_POWER_OF_TWO
    #   ROUND_POWER_OF_TWO_SATCHECK
    #   clip_pixel
    #
    # Conveniently, all these functions have their interesting argument last,
    # so we can just step backwards one index to get that subexpression. This
    # even works for nested subexpressions like
    #
    #   Clip3(0, 255, Clip3(some_min, some_max, my_expr))
    #
    # because all the subexpressions of the internal Clip3 come after both 0
    # and 255. Neat!
    #
    # As a sanity check, we'll start by checking that the keys of eqn_dict are
    # 1..len(eqn_dict).
    if list(eqn_dict.keys()) != list(range(1, len(eqn_dict) + 1)):
        raise RuntimeError('Branches for equation {} seem to be keyed by '
                           'indices {}, rather than 1..{}.'
                           .format(eqn_name,
                                   list(eqn_dict.values()),
                                   len(eqn_dict)))

    k = len(eqn_dict)
    while k:
        branch_name = eqn_dict[k]
        last_part = branch_name.rpartition(';')[2]
        drill_down = False
        for transparent in ['Clip3', 'ROUND_POWER_OF_TWO',
                            'ROUND_POWER_OF_TWO_SATCHECK', 'clip_pixel']:
            if last_part.startswith(transparent):
                drill_down = True
                break
        if not drill_down:
            break

        k -= 1

    # If we get here, the branch we're interested in is at sub_idx k and its
    # name is in branch_name. Or k = 0, which shouldn't ever happen.
    if k == 0:
        raise RuntimeError('The {} branch names for equation {} seem '
                           'to be transparent all the way down.'
                           .format(len(eqn_dict), eqn_name))

    return branch_name


class RangeCoverage:
    def __init__(self,
                 cstore: CoverageStore,
                 branch_coverage: BranchCoverage) -> None:
        self.cstore = cstore
        self.branch_coverage = branch_coverage

        # TODO: For now, we're going to hard-code the codec to av1b. This is
        # because we're currently instantiating RangeCoverage at the top of
        # autoproduct.py and we figure out the codec rather later. Once all the
        # horrible globals are tidied up, we'll easily be able to do this
        # properly.
        autoproduct_dir = os.path.dirname(__file__)
        eqns_dir = os.path.normpath(os.path.join(autoproduct_dir,
                                                 '..', 'product',
                                                 'ArgonViewerEquations'))
        codec = 'av1b'
        self.range_eqns = gen_range_equations(eqns_dir, codec, self.cstore)

        # Here is where we track what we've actually seen. Initially, we set
        # everything to None (meaning that I've not seen anything, so no min or
        # max).
        self.seen_ranges = \
            {eqn_name: None
             for eqn_name in self.range_eqns}  # type: Dict[str, _ORange]

        # Once we've read some coverage data, this will contain a map from
        # subset index to a string which names the 'range visibility type'
        self.visibility_types = None  # type: Optional[Dict[int, str]]

        # Range equations get stored as branch coverage with rather complicated
        # looking names, and we have to faff around searching through the
        # branch coverage in order to find the branch corresponding to the
        # range. Yuck. Here, we cache the results of the search for next time
        # around.
        self.eqn_to_branch = {}  # type: Dict[str, str]

    def update_eqn(self,
                   eqn_name: str,
                   vis_type: str,
                   actual_min: Optional[int],
                   actual_max: Optional[int]) -> None:

        range_eqn = self.range_eqns[eqn_name]
        exp_min, exp_max = range_eqn[vis_type]

        seen = self.seen_ranges[eqn_name]
        seen_min, seen_max = (None, None) if seen is None else seen
        new_data = False

        if actual_min is not None:
            if actual_min < exp_min:
                raise RuntimeError('Seen a minimum value of {} for equation '
                                   '{}, but the range equations predict a '
                                   'possible range of {} with vis_type {}.'
                                   .format(actual_min, eqn_name,
                                           range_eqn[vis_type], vis_type))
            if seen_min is None or actual_min < seen_min:
                seen_min = actual_min
                new_data = True

            if actual_max is None:
                raise RuntimeError('Equation {} reports a minimum value '
                                   'seen in a run, but no maximum.'
                                   .format(eqn_name))

        if actual_max is not None:
            if actual_max > exp_max:
                raise RuntimeError('Seen a maximum value of {} for equation '
                                   '{}, but the range equations predict a '
                                   'possible range of {} with vis_type {}.'
                                   .format(actual_max, eqn_name,
                                           range_eqn[vis_type], vis_type))
            if seen_max is None or actual_max > seen_max:
                seen_max = actual_max
                new_data = True

            if actual_min is None:
                raise RuntimeError('Equation {} reports a maximum value '
                                   'seen in a run, but no minimum.'
                                   .format(eqn_name))

        if new_data:
            assert seen_min is not None
            assert seen_max is not None
            self.seen_ranges[eqn_name] = (seen_min, seen_max)

    def update(self,
               vis_types: Dict[int, str],
               subset: int,
               branch_dict: Dict[str, CBData]) -> None:
        '''Update with new range information'''
        if self.visibility_types is not None:
            if self.visibility_types != vis_types:
                raise RuntimeError('Range visibility types mismatch.')

        self.visibility_types = vis_types

        assert subset in self.visibility_types
        vis_type = self.visibility_types[subset]

        eqn_to_branches = mk_eqn_to_branches(branch_dict.keys())
        for eqn_name, eqn_branches in eqn_to_branches.items():
            branch_name = self.eqn_to_branch.get(eqn_name)
            if branch_name is None:
                branch_name = find_branch_for_eqn(eqn_name, eqn_branches)
                self.eqn_to_branch[eqn_name] = branch_name

            (actual_min, actual_max, _, _, _, _) = branch_dict[branch_name]

            if actual_min is not None or actual_max is not None:
                self.update_eqn(eqn_name, vis_type, actual_min, actual_max)

    def holes(self, subset: int) -> List[_Hole]:
        '''Get a list of all the range coverage holes for subset'''

        # If we've never actually run a range coverage pass, we won't have
        # visibility types. No worries: we just don't track coverage for any
        # equations.
        if self.visibility_types is None:
            return []

        # We should have a visibility type for this subset. This will fail if
        # we have a silly subset index, or if the compiled coverage tool
        # disagrees with eqnToMathML.py about what ranges exist.
        visibility_type = self.visibility_types.get(subset)
        if visibility_type is None:
            raise RuntimeError('Subset {} doesn\'t have an associated '
                               'visibility type. Known subsets: {}'
                               .format(subset,
                                       list(self.visibility_types.keys())))

        ret = []
        for eqn_name, seen in self.seen_ranges.items():
            # If this equation is disabled for this subset, we don't track
            # coverage for it.
            if self.cstore.find_range_profile(eqn_name, subset) is None:
                continue

            # Find the branch name corresponding to this equation. If there is
            # none, that presumably means that we never ran update. I'm not
            # quite sure what is the best thing to return in this case, so
            # let's explode for now (we can always fix it up if we see the
            # explosion!)
            branch_name = self.eqn_to_branch.get(eqn_name)
            assert branch_name is not None

            # If this branch isn't visible for this subset, that means we
            # shouldn't track its associated range.
            if not self.branch_coverage.is_visible(branch_name, subset):
                continue

            # We can be pretty certain that eqn_name appears in range_eqns
            # (otherwise it wouldn't have appeared in seen_ranges either)
            eqn_dict = self.range_eqns[eqn_name]

            expected = eqn_dict.get(visibility_type)
            if expected is None:
                raise RuntimeError('Subset {} has a visibility type of {}, '
                                   'but the equation ranges for equation {} '
                                   'don\'t have that as a key. Known '
                                   'visibility types: {}.'
                                   .format(subset, visibility_type, eqn_name,
                                           list(eqn_dict.keys())))

            # If seen is not None, there's a chance that we've seen the whole
            # range and this isn't a hole.
            if seen is not None:
                seen_min, seen_max = seen
                exp_min, exp_max = expected

                # This should have been checked in the update method, when we
                # updated the seen values, but it can't hurt to be careful.
                assert exp_min <= seen_min
                assert seen_max <= exp_max

                if seen_min == exp_min and seen_max == exp_max:
                    continue

            ret.append((eqn_name, (seen, expected)))

        return ret

    def print_holes(self,
                    handle: IO[str],
                    holes: List[_Hole],
                    max_count: int) -> None:
        '''Print messages about a (nonempty list of) holes'''
        assert holes

        # Collect the first max_count holes that we should print, shuffling so
        # that ones with a specific range profile come first.
        specific = []
        generic = []
        for name, (seen, expected) in holes:
            if self.cstore.has_specific_range_profile(name):
                specific.append((name, seen, expected))
            else:
                generic.append((name, seen, expected))

            if (((len(specific) > max_count) or
                 (len(specific) >= max_count and generic))):
                break

        shuffled = specific + generic
        for name, seen, expected in shuffled[:max_count]:
            handle.write("    {} (theoretical={} current={})\n"
                         .format(name, expected, seen))
        if len(holes) > max_count:
            handle.write('    ...\n')

    def dump(self, stream: IO[str]) -> None:
        '''Dump data to the given file'''
        stream.write('ranges={!r}\n'.format(self.seen_ranges))

    def load(self, module: Dict[str, object]) -> None:
        '''Load data from the given module'''
        ranges = module.get('ranges')
        if ranges is None:
            raise RuntimeError('No ranges entry in dumped module')

        assert isinstance(ranges, dict)
        for eq_name, seen in ranges.items():
            assert isinstance(eq_name, str)
            if eq_name not in self.seen_ranges:
                raise RuntimeError('Loading module with unexpected '
                                   'equation name: `{}\'.'
                                   .format(eq_name))

            if self.seen_ranges[eq_name] is not None:
                raise RuntimeError('Loading module overrides seen range '
                                   'for equation {}, which we have already '
                                   'seen (with range: {}).'
                                   .format(eq_name, self.seen_ranges[eq_name]))

            if seen is None:
                continue

            assert isinstance(seen, tuple) and len(seen) == 2
            seen_min, seen_max = seen

            assert isinstance(seen_min, int)
            assert isinstance(seen_max, int)
            assert seen_min <= seen_max

            self.seen_ranges[eq_name] = (seen_min, seen_max)
