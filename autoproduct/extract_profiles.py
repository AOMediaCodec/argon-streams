################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import xml.etree.ElementTree as ET
import sys
import os
import ap_subprocess
import ap_macros

def processIncludes(r, baseDir, includes):
  for elem in r.findall("include"):
    processIncludes(ET.parse(os.path.join(baseDir, elem.text)).getroot(), baseDir, includes)
  includes.append(r)

def list_profiles(inputFile):
  tree=ET.parse(inputFile)
  includes=[]
  processIncludes(tree.getroot(), os.path.dirname(inputFile), includes)

  macros = ap_macros.Macros()

  #First parse the macros
  for r in includes:
    for elem in r.getchildren():
      if elem.tag=="defmacro":
        macros.set(elem.get("name"), macros.replace(elem.text))

  #Parse the set definition file
  for r in includes:
    for elem in r.getchildren():
      if elem.tag=="profile":
        functions=macros.replace(elem.findtext("functions"))
        print(functions)


if __name__ == "__main__":
  for item in sys.argv[1:]:
    list_profiles(item)
