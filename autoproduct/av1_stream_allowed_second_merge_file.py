################################################################################
# Project P8005 HEVC
# (c) Copyright 2021 Argon Design Ltd. A Broadcom Inc. Company
# All rights reserved.
################################################################################

import sys

from typing import Iterable, List, NamedTuple, Tuple

OBU_hdr = NamedTuple('OBU_hdr',
                     [('obu_forbidden_bit', int),
                      ('obu_type', int),
                      ('obu_extension_flag', int),
                      ('obu_has_size_field', int),
                      ('obu_reserved_1bit', int)])

OBU_ext_hdr = NamedTuple('OBU_ext_hdr',
                         [('temporal_id', int),
                          ('spatial_id', int),
                          ('extension_header_reserved_3bits', int)])

OBU_SEQUENCE_HEADER = 1
OBU_TEMPORAL_DELIMITER = 2
OBU_FRAME_HEADER = 3
OBU_TILE_GROUP = 4
OBU_METADATA = 5
OBU_FRAME = 6
OBU_REDUNDANT_FRAME_HEADER = 7
OBU_TILE_LIST = 8
OBU_PADDING = 15


def lookup_bits(arr: List[int], bits: Iterable[int]) -> int:
    val = 0
    for bit in bits:
        val = (val << 1) | ((arr[bit // 8] >> (7 - bit % 8)) & 1)
    return val


def lookup_range(arr: List[int], start: int, length: int) -> Tuple[int, int]:
    return (start+length, lookup_bits(arr, range(start, start + length)))


def decode_obu_header(arr: List[int], ind: int) -> Tuple[int, OBU_hdr]:
    # < little-endian
    hdr = OBU_hdr(lookup_bits(arr, [ind]),
                  lookup_bits(arr, range(ind+1, ind+5)),
                  lookup_bits(arr, [ind+5]),
                  lookup_bits(arr, [ind+6]),
                  lookup_bits(arr, [ind+7]))
    ind += 8
    assert hdr.obu_forbidden_bit == 0
    assert hdr.obu_reserved_1bit == 0
    if hdr.obu_extension_flag:
        ind += 8
    return ind, hdr


def check_allowed_uncompressed_header(arr: List[int],
                                      ind: int,
                                      verbose: bool) -> bool:
    show_existing_frame = lookup_bits(arr, [ind])
    if (show_existing_frame):
        if verbose:
            print('show_existing_frame', show_existing_frame)
        return False
    frame_type = lookup_bits(arr, range(ind+1, ind+2))
    show_frame = lookup_bits(arr, [ind+3])
    if verbose:
        print('show_existing_frame', show_existing_frame,
              'frame_type', frame_type,
              'show_frame', show_frame)

    return show_frame != 0


def check_allowed_sequence_header(arr: List[int],
                                  ind: int,
                                  verbose: bool) -> bool:
    ind, seq_profile = lookup_range(arr, ind, 3)
    ind, still_picture = lookup_range(arr, ind, 1)
    ind, reduced_still_picture_hdr = lookup_range(arr, ind, 1)
    if (reduced_still_picture_hdr):
        if verbose:
            print('profile', seq_profile,
                  'still_picture', still_picture,
                  'reduced_still_picture_hdr', reduced_still_picture_hdr,
                  'timing_info_present', 0)
        return True
    ind, timing_info_present_flag = lookup_range(arr, ind, 1)
    if verbose:
        print('profile', seq_profile,
              'still_picture', still_picture,
              'reduced_still_picture_hdr', reduced_still_picture_hdr,
              'timing_info_present', timing_info_present_flag)

    return timing_info_present_flag == 0


def decode_uleb128(arr: List[int], ind: int) -> Tuple[int, int]:
    value = 0
    LEB128_MAX_SIZE = 8
    for i in range(LEB128_MAX_SIZE):
        uleb128_byte = lookup_bits(arr, range(ind+i*8, ind+(i+1)*8))
        value |= ((uleb128_byte & 0x7f) << (i*7))
        if not (uleb128_byte & 0x80):
            # "libaom bug-1891 limits normative behaviour to UINT32_MAX?"
            assert (value <= 0xffffffff)
            return (ind+(i+1)*8, value)

    # "Invalid LEB128 encoding - must use at most 8 bytes"
    assert 0


def av1_get_allowed_second_merge(inFile: str,
                                 annexB: bool = True,
                                 verbose_input: bool = False) -> bool:
  global verbose
  verbose=verbose_input
  if verbose:
    print("{}:".format(inFile))
    print("Reading Until first frame")
  with open(inFile,"rb") as f:
    data = [x for x in f.read()]
    i=0
    show_frame_checked = False
    decoder_model_checked = False
    while i<len(data):
      if annexB:
        i, temporal_unit_size = decode_uleb128(data,i)
        temporal_end = i+temporal_unit_size*8
        if verbose:
          print('temporal_unit_size',temporal_unit_size)
        while i < temporal_end:
          i, frame_unit_size = decode_uleb128(data,i)
          frame_end = i+frame_unit_size*8
          if verbose:
            print('frame_unit_size',frame_unit_size)
          while i < frame_end:
            i, obu_length = decode_uleb128(data,i)
            obu_end = i+obu_length*8
            if verbose:
              print('obu_length',obu_length)
            while i < obu_end:
              i, obu_header = decode_obu_header(data,i)
              if (obu_header.obu_has_size_field):
                i, obu_size = decode_uleb128(data,i)
              if verbose:
                print('obu_header',obu_header)
                print('obu_header type',obu_header.obu_type)
              if (obu_header.obu_type == OBU_FRAME_HEADER or obu_header.obu_type == OBU_FRAME) and not show_frame_checked:
                show_frame_checked = True
                allowed = check_allowed_uncompressed_header(data,i,verbose)
                if not allowed:
                  return allowed
              elif obu_header.obu_type == OBU_SEQUENCE_HEADER and not decoder_model_checked:
                decoder_model_checked = True
                allowed = check_allowed_sequence_header(data,i,verbose)
                if not allowed:
                  return allowed
              if show_frame_checked and decoder_model_checked:
                return True
              i = obu_end
      else:
        i, obu_header = decode_obu_header(data,i)
        assert obu_header.obu_has_size_field, "Non Annex B streams must have a size field"
        if verbose:
          print('obu_header',obu_header)
        i, obu_size = decode_uleb128(data,i)
        obu_end = i+obu_size*8
        if obu_header.obu_type == OBU_FRAME_HEADER or obu_header.obu_type == OBU_FRAME and not show_frame_checked:
          show_frame_checked = True
          allowed = check_allowed_uncompressed_header(data,i,verbose)
          if not allowed:
            return allowed
        elif obu_header.obu_type == OBU_SEQUENCE_HEADER and not decoder_model_checked:
          decoder_model_checked = True
          allowed = check_allowed_sequence_header(data,i,verbose)
          if not allowed:
            return allowed
        i = obu_end
        if show_frame_checked and decoder_model_checked:
          return True
  return False


if __name__ == "__main__":
  if len(sys.argv) < 3:
    print("Argon Stream Random Access Test")
    print("Parses streams to determines if the first frame in each is shown")
    print("Input <annexB> is 1 to enable annexB and 0 to disable it")
    print()
    print("Syntax: {} <annexB> <input files>".format(sys.argv[0]))
    sys.exit(1)
  annexB = sys.argv[1] == '1'
  for inFile in sys.argv[2:]:
    verbose = True
    is_showFrame = av1_get_allowed_second_merge(inFile,annexB,verbose)
    print(is_showFrame)
